.class final Lc/q;
.super Ljava/lang/Object;
.source "RequestFactory.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lc/q$a;
    }
.end annotation


# instance fields
.field final a:Ljava/lang/String;

.field private final b:Ljava/lang/reflect/Method;

.field private final c:Lokhttp3/s;

.field private final d:Ljava/lang/String;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final e:Lokhttp3/Headers;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final f:Lokhttp3/u;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final g:Z

.field private final h:Z

.field private final i:Z

.field private final j:[Lc/n;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "Lc/n<",
            "*>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lc/q$a;)V
    .locals 1

    .line 79
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 80
    iget-object v0, p1, Lc/q$a;->b:Ljava/lang/reflect/Method;

    iput-object v0, p0, Lc/q;->b:Ljava/lang/reflect/Method;

    .line 81
    iget-object v0, p1, Lc/q$a;->a:Lc/s;

    iget-object v0, v0, Lc/s;->b:Lokhttp3/s;

    iput-object v0, p0, Lc/q;->c:Lokhttp3/s;

    .line 82
    iget-object v0, p1, Lc/q$a;->n:Ljava/lang/String;

    iput-object v0, p0, Lc/q;->a:Ljava/lang/String;

    .line 83
    iget-object v0, p1, Lc/q$a;->r:Ljava/lang/String;

    iput-object v0, p0, Lc/q;->d:Ljava/lang/String;

    .line 84
    iget-object v0, p1, Lc/q$a;->s:Lokhttp3/Headers;

    iput-object v0, p0, Lc/q;->e:Lokhttp3/Headers;

    .line 85
    iget-object v0, p1, Lc/q$a;->t:Lokhttp3/u;

    iput-object v0, p0, Lc/q;->f:Lokhttp3/u;

    .line 86
    iget-boolean v0, p1, Lc/q$a;->o:Z

    iput-boolean v0, p0, Lc/q;->g:Z

    .line 87
    iget-boolean v0, p1, Lc/q$a;->p:Z

    iput-boolean v0, p0, Lc/q;->h:Z

    .line 88
    iget-boolean v0, p1, Lc/q$a;->q:Z

    iput-boolean v0, p0, Lc/q;->i:Z

    .line 89
    iget-object p1, p1, Lc/q$a;->v:[Lc/n;

    iput-object p1, p0, Lc/q;->j:[Lc/n;

    return-void
.end method

.method static a(Lc/s;Ljava/lang/reflect/Method;)Lc/q;
    .locals 1

    .line 65
    new-instance v0, Lc/q$a;

    invoke-direct {v0, p0, p1}, Lc/q$a;-><init>(Lc/s;Ljava/lang/reflect/Method;)V

    invoke-virtual {v0}, Lc/q$a;->a()Lc/q;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method a([Ljava/lang/Object;)Lokhttp3/z;
    .locals 12
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 94
    iget-object v0, p0, Lc/q;->j:[Lc/n;

    .line 96
    array-length v1, p1

    .line 97
    array-length v2, v0

    if-ne v1, v2, :cond_1

    .line 102
    new-instance v2, Lc/p;

    iget-object v4, p0, Lc/q;->a:Ljava/lang/String;

    iget-object v5, p0, Lc/q;->c:Lokhttp3/s;

    iget-object v6, p0, Lc/q;->d:Ljava/lang/String;

    iget-object v7, p0, Lc/q;->e:Lokhttp3/Headers;

    iget-object v8, p0, Lc/q;->f:Lokhttp3/u;

    iget-boolean v9, p0, Lc/q;->g:Z

    iget-boolean v10, p0, Lc/q;->h:Z

    iget-boolean v11, p0, Lc/q;->i:Z

    move-object v3, v2

    invoke-direct/range {v3 .. v11}, Lc/p;-><init>(Ljava/lang/String;Lokhttp3/s;Ljava/lang/String;Lokhttp3/Headers;Lokhttp3/u;ZZZ)V

    .line 105
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3, v1}, Ljava/util/ArrayList;-><init>(I)V

    const/4 v4, 0x0

    :goto_0
    if-ge v4, v1, :cond_0

    .line 107
    aget-object v5, p1, v4

    invoke-interface {v3, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 108
    aget-object v5, v0, v4

    aget-object v6, p1, v4

    invoke-virtual {v5, v2, v6}, Lc/n;->a(Lc/p;Ljava/lang/Object;)V

    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 111
    :cond_0
    invoke-virtual {v2}, Lc/p;->a()Lokhttp3/z$a;

    move-result-object p1

    const-class v0, Lc/k;

    new-instance v1, Lc/k;

    iget-object v2, p0, Lc/q;->b:Ljava/lang/reflect/Method;

    invoke-direct {v1, v2, v3}, Lc/k;-><init>(Ljava/lang/reflect/Method;Ljava/util/List;)V

    .line 112
    invoke-virtual {p1, v0, v1}, Lokhttp3/z$a;->a(Ljava/lang/Class;Ljava/lang/Object;)Lokhttp3/z$a;

    move-result-object p1

    .line 113
    invoke-virtual {p1}, Lokhttp3/z$a;->a()Lokhttp3/z;

    move-result-object p1

    return-object p1

    .line 98
    :cond_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Argument count ("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ") doesn\'t match expected count ("

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    array-length v0, v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v0, ")"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method
