.class Lc/s$1;
.super Ljava/lang/Object;
.source "Retrofit.java"

# interfaces
.implements Ljava/lang/reflect/InvocationHandler;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lc/s;->a(Ljava/lang/Class;)Ljava/lang/Object;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Ljava/lang/Class;

.field final synthetic b:Lc/s;

.field private final c:Lc/o;

.field private final d:[Ljava/lang/Object;


# direct methods
.method constructor <init>(Lc/s;Ljava/lang/Class;)V
    .locals 0

    .line 134
    iput-object p1, p0, Lc/s$1;->b:Lc/s;

    iput-object p2, p0, Lc/s$1;->a:Ljava/lang/Class;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 135
    invoke-static {}, Lc/o;->a()Lc/o;

    move-result-object p1

    iput-object p1, p0, Lc/s$1;->c:Lc/o;

    const/4 p1, 0x0

    .line 136
    new-array p1, p1, [Ljava/lang/Object;

    iput-object p1, p0, Lc/s$1;->d:[Ljava/lang/Object;

    return-void
.end method


# virtual methods
.method public invoke(Ljava/lang/Object;Ljava/lang/reflect/Method;[Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .param p3    # [Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 141
    invoke-virtual {p2}, Ljava/lang/reflect/Method;->getDeclaringClass()Ljava/lang/Class;

    move-result-object v0

    const-class v1, Ljava/lang/Object;

    if-ne v0, v1, :cond_0

    .line 142
    invoke-virtual {p2, p0, p3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    return-object p1

    .line 144
    :cond_0
    iget-object v0, p0, Lc/s$1;->c:Lc/o;

    invoke-virtual {v0, p2}, Lc/o;->a(Ljava/lang/reflect/Method;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 145
    iget-object v0, p0, Lc/s$1;->c:Lc/o;

    iget-object v1, p0, Lc/s$1;->a:Ljava/lang/Class;

    invoke-virtual {v0, p2, v1, p1, p3}, Lc/o;->a(Ljava/lang/reflect/Method;Ljava/lang/Class;Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    return-object p1

    .line 147
    :cond_1
    iget-object p1, p0, Lc/s$1;->b:Lc/s;

    invoke-virtual {p1, p2}, Lc/s;->a(Ljava/lang/reflect/Method;)Lc/t;

    move-result-object p1

    if-eqz p3, :cond_2

    goto :goto_0

    :cond_2
    iget-object p3, p0, Lc/s$1;->d:[Ljava/lang/Object;

    :goto_0
    invoke-virtual {p1, p3}, Lc/t;->a([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method
