.class Lc/e$a$2;
.super Ljava/lang/Object;
.source "CompletableFutureCallAdapterFactory.java"

# interfaces
.implements Lc/d;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lc/e$a;->b(Lc/b;)Ljava/util/concurrent/CompletableFuture;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lc/d<",
        "TR;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Ljava/util/concurrent/CompletableFuture;

.field final synthetic b:Lc/e$a;


# direct methods
.method constructor <init>(Lc/e$a;Ljava/util/concurrent/CompletableFuture;)V
    .locals 0

    .line 76
    iput-object p1, p0, Lc/e$a$2;->b:Lc/e$a;

    iput-object p2, p0, Lc/e$a$2;->a:Ljava/util/concurrent/CompletableFuture;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFailure(Lc/b;Ljava/lang/Throwable;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lc/b<",
            "TR;>;",
            "Ljava/lang/Throwable;",
            ")V"
        }
    .end annotation

    .line 86
    iget-object p1, p0, Lc/e$a$2;->a:Ljava/util/concurrent/CompletableFuture;

    invoke-virtual {p1, p2}, Ljava/util/concurrent/CompletableFuture;->completeExceptionally(Ljava/lang/Throwable;)Z

    return-void
.end method

.method public onResponse(Lc/b;Lc/r;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lc/b<",
            "TR;>;",
            "Lc/r<",
            "TR;>;)V"
        }
    .end annotation

    .line 78
    invoke-virtual {p2}, Lc/r;->d()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 79
    iget-object p1, p0, Lc/e$a$2;->a:Ljava/util/concurrent/CompletableFuture;

    invoke-virtual {p2}, Lc/r;->e()Ljava/lang/Object;

    move-result-object p2

    invoke-virtual {p1, p2}, Ljava/util/concurrent/CompletableFuture;->complete(Ljava/lang/Object;)Z

    goto :goto_0

    .line 81
    :cond_0
    iget-object p1, p0, Lc/e$a$2;->a:Ljava/util/concurrent/CompletableFuture;

    new-instance v0, Lc/i;

    invoke-direct {v0, p2}, Lc/i;-><init>(Lc/r;)V

    invoke-virtual {p1, v0}, Ljava/util/concurrent/CompletableFuture;->completeExceptionally(Ljava/lang/Throwable;)Z

    :goto_0
    return-void
.end method
