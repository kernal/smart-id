.class final Lc/a;
.super Lc/f$a;
.source "BuiltInConverters.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lc/a$d;,
        Lc/a$a;,
        Lc/a$c;,
        Lc/a$b;,
        Lc/a$e;,
        Lc/a$f;
    }
.end annotation


# instance fields
.field private a:Z


# direct methods
.method constructor <init>()V
    .locals 1

    .line 27
    invoke-direct {p0}, Lc/f$a;-><init>()V

    const/4 v0, 0x1

    .line 29
    iput-boolean v0, p0, Lc/a;->a:Z

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/reflect/Type;[Ljava/lang/annotation/Annotation;Lc/s;)Lc/f;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/reflect/Type;",
            "[",
            "Ljava/lang/annotation/Annotation;",
            "Lc/s;",
            ")",
            "Lc/f<",
            "Lokhttp3/ac;",
            "*>;"
        }
    .end annotation

    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .line 33
    const-class p3, Lokhttp3/ac;

    if-ne p1, p3, :cond_1

    .line 34
    const-class p1, Lc/b/w;

    invoke-static {p2, p1}, Lc/u;->a([Ljava/lang/annotation/Annotation;Ljava/lang/Class;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 35
    sget-object p1, Lc/a$c;->a:Lc/a$c;

    goto :goto_0

    .line 36
    :cond_0
    sget-object p1, Lc/a$a;->a:Lc/a$a;

    :goto_0
    return-object p1

    .line 38
    :cond_1
    const-class p2, Ljava/lang/Void;

    if-ne p1, p2, :cond_2

    .line 39
    sget-object p1, Lc/a$f;->a:Lc/a$f;

    return-object p1

    .line 41
    :cond_2
    iget-boolean p2, p0, Lc/a;->a:Z

    if-eqz p2, :cond_3

    .line 43
    :try_start_0
    const-class p2, Lkotlin/Unit;

    if-ne p1, p2, :cond_3

    .line 44
    sget-object p1, Lc/a$e;->a:Lc/a$e;
    :try_end_0
    .catch Ljava/lang/NoClassDefFoundError; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    const/4 p1, 0x0

    .line 47
    iput-boolean p1, p0, Lc/a;->a:Z

    :cond_3
    const/4 p1, 0x0

    return-object p1
.end method

.method public a(Ljava/lang/reflect/Type;[Ljava/lang/annotation/Annotation;[Ljava/lang/annotation/Annotation;Lc/s;)Lc/f;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/reflect/Type;",
            "[",
            "Ljava/lang/annotation/Annotation;",
            "[",
            "Ljava/lang/annotation/Annotation;",
            "Lc/s;",
            ")",
            "Lc/f<",
            "*",
            "Lokhttp3/aa;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .line 55
    const-class p2, Lokhttp3/aa;

    invoke-static {p1}, Lc/u;->a(Ljava/lang/reflect/Type;)Ljava/lang/Class;

    move-result-object p1

    invoke-virtual {p2, p1}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 56
    sget-object p1, Lc/a$b;->a:Lc/a$b;

    return-object p1

    :cond_0
    const/4 p1, 0x0

    return-object p1
.end method
