.class public final Lc/a/a/a;
.super Lc/f$a;
.source "GsonConverterFactory.java"


# instance fields
.field private final a:Lcom/google/gson/f;


# direct methods
.method private constructor <init>(Lcom/google/gson/f;)V
    .locals 0

    .line 57
    invoke-direct {p0}, Lc/f$a;-><init>()V

    .line 58
    iput-object p1, p0, Lc/a/a/a;->a:Lcom/google/gson/f;

    return-void
.end method

.method public static a(Lcom/google/gson/f;)Lc/a/a/a;
    .locals 1

    if-eqz p0, :cond_0

    .line 52
    new-instance v0, Lc/a/a/a;

    invoke-direct {v0, p0}, Lc/a/a/a;-><init>(Lcom/google/gson/f;)V

    return-object v0

    .line 51
    :cond_0
    new-instance p0, Ljava/lang/NullPointerException;

    const-string v0, "gson == null"

    invoke-direct {p0, v0}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw p0
.end method


# virtual methods
.method public a(Ljava/lang/reflect/Type;[Ljava/lang/annotation/Annotation;Lc/s;)Lc/f;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/reflect/Type;",
            "[",
            "Ljava/lang/annotation/Annotation;",
            "Lc/s;",
            ")",
            "Lc/f<",
            "Lokhttp3/ac;",
            "*>;"
        }
    .end annotation

    .line 64
    iget-object p2, p0, Lc/a/a/a;->a:Lcom/google/gson/f;

    invoke-static {p1}, Lcom/google/gson/b/a;->get(Ljava/lang/reflect/Type;)Lcom/google/gson/b/a;

    move-result-object p1

    invoke-virtual {p2, p1}, Lcom/google/gson/f;->a(Lcom/google/gson/b/a;)Lcom/google/gson/w;

    move-result-object p1

    .line 65
    new-instance p2, Lc/a/a/c;

    iget-object p3, p0, Lc/a/a/a;->a:Lcom/google/gson/f;

    invoke-direct {p2, p3, p1}, Lc/a/a/c;-><init>(Lcom/google/gson/f;Lcom/google/gson/w;)V

    return-object p2
.end method

.method public a(Ljava/lang/reflect/Type;[Ljava/lang/annotation/Annotation;[Ljava/lang/annotation/Annotation;Lc/s;)Lc/f;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/reflect/Type;",
            "[",
            "Ljava/lang/annotation/Annotation;",
            "[",
            "Ljava/lang/annotation/Annotation;",
            "Lc/s;",
            ")",
            "Lc/f<",
            "*",
            "Lokhttp3/aa;",
            ">;"
        }
    .end annotation

    .line 71
    iget-object p2, p0, Lc/a/a/a;->a:Lcom/google/gson/f;

    invoke-static {p1}, Lcom/google/gson/b/a;->get(Ljava/lang/reflect/Type;)Lcom/google/gson/b/a;

    move-result-object p1

    invoke-virtual {p2, p1}, Lcom/google/gson/f;->a(Lcom/google/gson/b/a;)Lcom/google/gson/w;

    move-result-object p1

    .line 72
    new-instance p2, Lc/a/a/b;

    iget-object p3, p0, Lc/a/a/a;->a:Lcom/google/gson/f;

    invoke-direct {p2, p3, p1}, Lc/a/a/b;-><init>(Lcom/google/gson/f;Lcom/google/gson/w;)V

    return-object p2
.end method
