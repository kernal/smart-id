.class Lc/h$a$1;
.super Ljava/lang/Object;
.source "ExecutorCallAdapterFactory.java"

# interfaces
.implements Lc/d;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lc/h$a;->a(Lc/d;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lc/d<",
        "TT;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lc/d;

.field final synthetic b:Lc/h$a;


# direct methods
.method constructor <init>(Lc/h$a;Lc/d;)V
    .locals 0

    .line 63
    iput-object p1, p0, Lc/h$a$1;->b:Lc/h$a;

    iput-object p2, p0, Lc/h$a$1;->a:Lc/d;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFailure(Lc/b;Ljava/lang/Throwable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lc/b<",
            "TT;>;",
            "Ljava/lang/Throwable;",
            ")V"
        }
    .end annotation

    .line 78
    iget-object p1, p0, Lc/h$a$1;->b:Lc/h$a;

    iget-object p1, p1, Lc/h$a;->a:Ljava/util/concurrent/Executor;

    new-instance v0, Lc/h$a$1$2;

    invoke-direct {v0, p0, p2}, Lc/h$a$1$2;-><init>(Lc/h$a$1;Ljava/lang/Throwable;)V

    invoke-interface {p1, v0}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public onResponse(Lc/b;Lc/r;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lc/b<",
            "TT;>;",
            "Lc/r<",
            "TT;>;)V"
        }
    .end annotation

    .line 65
    iget-object p1, p0, Lc/h$a$1;->b:Lc/h$a;

    iget-object p1, p1, Lc/h$a;->a:Ljava/util/concurrent/Executor;

    new-instance v0, Lc/h$a$1$1;

    invoke-direct {v0, p0, p2}, Lc/h$a$1$1;-><init>(Lc/h$a$1;Lc/r;)V

    invoke-interface {p1, v0}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method
