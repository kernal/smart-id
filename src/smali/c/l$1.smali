.class Lc/l$1;
.super Ljava/lang/Object;
.source "OkHttpCall.java"

# interfaces
.implements Lokhttp3/f;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lc/l;->a(Lc/d;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lc/d;

.field final synthetic b:Lc/l;


# direct methods
.method constructor <init>(Lc/l;Lc/d;)V
    .locals 0

    .line 117
    iput-object p1, p0, Lc/l$1;->b:Lc/l;

    iput-object p2, p0, Lc/l$1;->a:Lc/d;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(Ljava/lang/Throwable;)V
    .locals 2

    .line 141
    :try_start_0
    iget-object v0, p0, Lc/l$1;->a:Lc/d;

    iget-object v1, p0, Lc/l$1;->b:Lc/l;

    invoke-interface {v0, v1, p1}, Lc/d;->onFailure(Lc/b;Ljava/lang/Throwable;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 143
    invoke-virtual {p1}, Ljava/lang/Throwable;->printStackTrace()V

    :goto_0
    return-void
.end method


# virtual methods
.method public a(Lokhttp3/e;Ljava/io/IOException;)V
    .locals 0

    .line 136
    invoke-direct {p0, p2}, Lc/l$1;->a(Ljava/lang/Throwable;)V

    return-void
.end method

.method public a(Lokhttp3/e;Lokhttp3/ab;)V
    .locals 1

    .line 121
    :try_start_0
    iget-object p1, p0, Lc/l$1;->b:Lc/l;

    invoke-virtual {p1, p2}, Lc/l;->a(Lokhttp3/ab;)Lc/r;

    move-result-object p1
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1

    .line 129
    :try_start_1
    iget-object p2, p0, Lc/l$1;->a:Lc/d;

    iget-object v0, p0, Lc/l$1;->b:Lc/l;

    invoke-interface {p2, v0, p1}, Lc/d;->onResponse(Lc/b;Lc/r;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 131
    invoke-virtual {p1}, Ljava/lang/Throwable;->printStackTrace()V

    :goto_0
    return-void

    :catch_1
    move-exception p1

    .line 123
    invoke-static {p1}, Lc/u;->a(Ljava/lang/Throwable;)V

    .line 124
    invoke-direct {p0, p1}, Lc/l$1;->a(Ljava/lang/Throwable;)V

    return-void
.end method
