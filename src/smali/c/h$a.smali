.class final Lc/h$a;
.super Ljava/lang/Object;
.source "ExecutorCallAdapterFactory.java"

# interfaces
.implements Lc/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lc/h;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lc/b<",
        "TT;>;"
    }
.end annotation


# instance fields
.field final a:Ljava/util/concurrent/Executor;

.field final b:Lc/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lc/b<",
            "TT;>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Ljava/util/concurrent/Executor;Lc/b;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/Executor;",
            "Lc/b<",
            "TT;>;)V"
        }
    .end annotation

    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    iput-object p1, p0, Lc/h$a;->a:Ljava/util/concurrent/Executor;

    .line 57
    iput-object p2, p0, Lc/h$a;->b:Lc/b;

    return-void
.end method


# virtual methods
.method public a()Lc/r;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lc/r<",
            "TT;>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 92
    iget-object v0, p0, Lc/h$a;->b:Lc/b;

    invoke-interface {v0}, Lc/b;->a()Lc/r;

    move-result-object v0

    return-object v0
.end method

.method public a(Lc/d;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lc/d<",
            "TT;>;)V"
        }
    .end annotation

    const-string v0, "callback == null"

    .line 61
    invoke-static {p1, v0}, Lc/u;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 63
    iget-object v0, p0, Lc/h$a;->b:Lc/b;

    new-instance v1, Lc/h$a$1;

    invoke-direct {v1, p0, p1}, Lc/h$a$1;-><init>(Lc/h$a;Lc/d;)V

    invoke-interface {v0, v1}, Lc/b;->a(Lc/d;)V

    return-void
.end method

.method public b()V
    .locals 1

    .line 96
    iget-object v0, p0, Lc/h$a;->b:Lc/b;

    invoke-interface {v0}, Lc/b;->b()V

    return-void
.end method

.method public c()Z
    .locals 1

    .line 100
    iget-object v0, p0, Lc/h$a;->b:Lc/b;

    invoke-interface {v0}, Lc/b;->c()Z

    move-result v0

    return v0
.end method

.method public synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .line 51
    invoke-virtual {p0}, Lc/h$a;->d()Lc/b;

    move-result-object v0

    return-object v0
.end method

.method public d()Lc/b;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lc/b<",
            "TT;>;"
        }
    .end annotation

    .line 105
    new-instance v0, Lc/h$a;

    iget-object v1, p0, Lc/h$a;->a:Ljava/util/concurrent/Executor;

    iget-object v2, p0, Lc/h$a;->b:Lc/b;

    invoke-interface {v2}, Lc/b;->d()Lc/b;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lc/h$a;-><init>(Ljava/util/concurrent/Executor;Lc/b;)V

    return-object v0
.end method
