.class final Lc/j;
.super Lc/t;
.source "HttpServiceMethod.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<ResponseT:",
        "Ljava/lang/Object;",
        "ReturnT:",
        "Ljava/lang/Object;",
        ">",
        "Lc/t<",
        "TReturnT;>;"
    }
.end annotation


# instance fields
.field private final a:Lc/q;

.field private final b:Lokhttp3/e$a;

.field private final c:Lc/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lc/c<",
            "TResponseT;TReturnT;>;"
        }
    .end annotation
.end field

.field private final d:Lc/f;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lc/f<",
            "Lokhttp3/ac;",
            "TResponseT;>;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lc/q;Lokhttp3/e$a;Lc/c;Lc/f;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lc/q;",
            "Lokhttp3/e$a;",
            "Lc/c<",
            "TResponseT;TReturnT;>;",
            "Lc/f<",
            "Lokhttp3/ac;",
            "TResponseT;>;)V"
        }
    .end annotation

    .line 81
    invoke-direct {p0}, Lc/t;-><init>()V

    .line 82
    iput-object p1, p0, Lc/j;->a:Lc/q;

    .line 83
    iput-object p2, p0, Lc/j;->b:Lokhttp3/e$a;

    .line 84
    iput-object p3, p0, Lc/j;->c:Lc/c;

    .line 85
    iput-object p4, p0, Lc/j;->d:Lc/f;

    return-void
.end method

.method private static a(Lc/s;Ljava/lang/reflect/Method;Ljava/lang/reflect/Type;)Lc/f;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<ResponseT:",
            "Ljava/lang/Object;",
            ">(",
            "Lc/s;",
            "Ljava/lang/reflect/Method;",
            "Ljava/lang/reflect/Type;",
            ")",
            "Lc/f<",
            "Lokhttp3/ac;",
            "TResponseT;>;"
        }
    .end annotation

    .line 66
    invoke-virtual {p1}, Ljava/lang/reflect/Method;->getAnnotations()[Ljava/lang/annotation/Annotation;

    move-result-object v0

    .line 68
    :try_start_0
    invoke-virtual {p0, p2, v0}, Lc/s;->b(Ljava/lang/reflect/Type;[Ljava/lang/annotation/Annotation;)Lc/f;

    move-result-object p0
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p0

    :catch_0
    move-exception p0

    const/4 v0, 0x1

    .line 70
    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p2, v0, v1

    const-string p2, "Unable to create converter for %s"

    invoke-static {p1, p0, p2, v0}, Lc/u;->a(Ljava/lang/reflect/Method;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/RuntimeException;

    move-result-object p0

    throw p0
.end method

.method static a(Lc/s;Ljava/lang/reflect/Method;Lc/q;)Lc/j;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<ResponseT:",
            "Ljava/lang/Object;",
            "ReturnT:",
            "Ljava/lang/Object;",
            ">(",
            "Lc/s;",
            "Ljava/lang/reflect/Method;",
            "Lc/q;",
            ")",
            "Lc/j<",
            "TResponseT;TReturnT;>;"
        }
    .end annotation

    .line 34
    invoke-static {p0, p1}, Lc/j;->b(Lc/s;Ljava/lang/reflect/Method;)Lc/c;

    move-result-object v0

    .line 35
    invoke-interface {v0}, Lc/c;->a()Ljava/lang/reflect/Type;

    move-result-object v1

    .line 36
    const-class v2, Lc/r;

    const/4 v3, 0x0

    if-eq v1, v2, :cond_2

    const-class v2, Lokhttp3/ab;

    if-eq v1, v2, :cond_2

    .line 41
    iget-object v2, p2, Lc/q;->a:Ljava/lang/String;

    const-string v4, "HEAD"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const-class v2, Ljava/lang/Void;

    invoke-virtual {v2, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    goto :goto_0

    .line 42
    :cond_0
    new-array p0, v3, [Ljava/lang/Object;

    const-string p2, "HEAD method must use Void as response type."

    invoke-static {p1, p2, p0}, Lc/u;->a(Ljava/lang/reflect/Method;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/RuntimeException;

    move-result-object p0

    throw p0

    .line 46
    :cond_1
    :goto_0
    invoke-static {p0, p1, v1}, Lc/j;->a(Lc/s;Ljava/lang/reflect/Method;Ljava/lang/reflect/Type;)Lc/f;

    move-result-object p1

    .line 48
    iget-object p0, p0, Lc/s;->a:Lokhttp3/e$a;

    .line 49
    new-instance v1, Lc/j;

    invoke-direct {v1, p2, p0, v0, p1}, Lc/j;-><init>(Lc/q;Lokhttp3/e$a;Lc/c;Lc/f;)V

    return-object v1

    .line 37
    :cond_2
    new-instance p0, Ljava/lang/StringBuilder;

    invoke-direct {p0}, Ljava/lang/StringBuilder;-><init>()V

    const-string p2, "\'"

    invoke-virtual {p0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 38
    invoke-static {v1}, Lc/u;->a(Ljava/lang/reflect/Type;)Ljava/lang/Class;

    move-result-object p2

    invoke-virtual {p2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p2, "\' is not a valid response body type. Did you mean ResponseBody?"

    invoke-virtual {p0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    new-array p2, v3, [Ljava/lang/Object;

    .line 37
    invoke-static {p1, p0, p2}, Lc/u;->a(Ljava/lang/reflect/Method;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/RuntimeException;

    move-result-object p0

    throw p0
.end method

.method private static b(Lc/s;Ljava/lang/reflect/Method;)Lc/c;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<ResponseT:",
            "Ljava/lang/Object;",
            "ReturnT:",
            "Ljava/lang/Object;",
            ">(",
            "Lc/s;",
            "Ljava/lang/reflect/Method;",
            ")",
            "Lc/c<",
            "TResponseT;TReturnT;>;"
        }
    .end annotation

    .line 54
    invoke-virtual {p1}, Ljava/lang/reflect/Method;->getGenericReturnType()Ljava/lang/reflect/Type;

    move-result-object v0

    .line 55
    invoke-virtual {p1}, Ljava/lang/reflect/Method;->getAnnotations()[Ljava/lang/annotation/Annotation;

    move-result-object v1

    .line 58
    :try_start_0
    invoke-virtual {p0, v0, v1}, Lc/s;->a(Ljava/lang/reflect/Type;[Ljava/lang/annotation/Annotation;)Lc/c;

    move-result-object p0
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p0

    :catch_0
    move-exception p0

    const/4 v1, 0x1

    .line 60
    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object v0, v1, v2

    const-string v0, "Unable to create call adapter for %s"

    invoke-static {p1, p0, v0, v1}, Lc/u;->a(Ljava/lang/reflect/Method;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/RuntimeException;

    move-result-object p0

    throw p0
.end method


# virtual methods
.method a([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/Object;",
            ")TReturnT;"
        }
    .end annotation

    .line 89
    iget-object v0, p0, Lc/j;->c:Lc/c;

    new-instance v1, Lc/l;

    iget-object v2, p0, Lc/j;->a:Lc/q;

    iget-object v3, p0, Lc/j;->b:Lokhttp3/e$a;

    iget-object v4, p0, Lc/j;->d:Lc/f;

    invoke-direct {v1, v2, p1, v3, v4}, Lc/l;-><init>(Lc/q;[Ljava/lang/Object;Lokhttp3/e$a;Lc/f;)V

    invoke-interface {v0, v1}, Lc/c;->a(Lc/b;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method
