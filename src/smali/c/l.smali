.class final Lc/l;
.super Ljava/lang/Object;
.source "OkHttpCall.java"

# interfaces
.implements Lc/b;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lc/l$a;,
        Lc/l$b;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lc/b<",
        "TT;>;"
    }
.end annotation


# instance fields
.field private final a:Lc/q;

.field private final b:[Ljava/lang/Object;

.field private final c:Lokhttp3/e$a;

.field private final d:Lc/f;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lc/f<",
            "Lokhttp3/ac;",
            "TT;>;"
        }
    .end annotation
.end field

.field private volatile e:Z

.field private f:Lokhttp3/e;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private g:Ljava/lang/Throwable;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private h:Z
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field


# direct methods
.method constructor <init>(Lc/q;[Ljava/lang/Object;Lokhttp3/e$a;Lc/f;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lc/q;",
            "[",
            "Ljava/lang/Object;",
            "Lokhttp3/e$a;",
            "Lc/f<",
            "Lokhttp3/ac;",
            "TT;>;)V"
        }
    .end annotation

    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    iput-object p1, p0, Lc/l;->a:Lc/q;

    .line 50
    iput-object p2, p0, Lc/l;->b:[Ljava/lang/Object;

    .line 51
    iput-object p3, p0, Lc/l;->c:Lokhttp3/e$a;

    .line 52
    iput-object p4, p0, Lc/l;->d:Lc/f;

    return-void
.end method

.method private f()Lokhttp3/e;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 190
    iget-object v0, p0, Lc/l;->c:Lokhttp3/e$a;

    iget-object v1, p0, Lc/l;->a:Lc/q;

    iget-object v2, p0, Lc/l;->b:[Ljava/lang/Object;

    invoke-virtual {v1, v2}, Lc/q;->a([Ljava/lang/Object;)Lokhttp3/z;

    move-result-object v1

    invoke-interface {v0, v1}, Lokhttp3/e$a;->a(Lokhttp3/z;)Lokhttp3/e;

    move-result-object v0

    if-eqz v0, :cond_0

    return-object v0

    .line 192
    :cond_0
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Call.Factory returned null."

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public a()Lc/r;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lc/r<",
            "TT;>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 156
    monitor-enter p0

    .line 157
    :try_start_0
    iget-boolean v0, p0, Lc/l;->h:Z

    if-nez v0, :cond_5

    const/4 v0, 0x1

    .line 158
    iput-boolean v0, p0, Lc/l;->h:Z

    .line 160
    iget-object v0, p0, Lc/l;->g:Ljava/lang/Throwable;

    if-eqz v0, :cond_2

    .line 161
    iget-object v0, p0, Lc/l;->g:Ljava/lang/Throwable;

    instance-of v0, v0, Ljava/io/IOException;

    if-nez v0, :cond_1

    .line 163
    iget-object v0, p0, Lc/l;->g:Ljava/lang/Throwable;

    instance-of v0, v0, Ljava/lang/RuntimeException;

    if-eqz v0, :cond_0

    .line 164
    iget-object v0, p0, Lc/l;->g:Ljava/lang/Throwable;

    check-cast v0, Ljava/lang/RuntimeException;

    throw v0

    .line 166
    :cond_0
    iget-object v0, p0, Lc/l;->g:Ljava/lang/Throwable;

    check-cast v0, Ljava/lang/Error;

    throw v0

    .line 162
    :cond_1
    iget-object v0, p0, Lc/l;->g:Ljava/lang/Throwable;

    check-cast v0, Ljava/io/IOException;

    throw v0

    .line 170
    :cond_2
    iget-object v0, p0, Lc/l;->f:Lokhttp3/e;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_3

    .line 173
    :try_start_1
    invoke-direct {p0}, Lc/l;->f()Lokhttp3/e;

    move-result-object v0

    iput-object v0, p0, Lc/l;->f:Lokhttp3/e;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/Error; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :catch_0
    move-exception v0

    goto :goto_0

    :catch_1
    move-exception v0

    goto :goto_0

    :catch_2
    move-exception v0

    .line 175
    :goto_0
    :try_start_2
    invoke-static {v0}, Lc/u;->a(Ljava/lang/Throwable;)V

    .line 176
    iput-object v0, p0, Lc/l;->g:Ljava/lang/Throwable;

    .line 177
    throw v0

    .line 180
    :cond_3
    :goto_1
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 182
    iget-boolean v1, p0, Lc/l;->e:Z

    if-eqz v1, :cond_4

    .line 183
    invoke-interface {v0}, Lokhttp3/e;->b()V

    .line 186
    :cond_4
    invoke-interface {v0}, Lokhttp3/e;->a()Lokhttp3/ab;

    move-result-object v0

    invoke-virtual {p0, v0}, Lc/l;->a(Lokhttp3/ab;)Lc/r;

    move-result-object v0

    return-object v0

    .line 157
    :cond_5
    :try_start_3
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Already executed."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :catchall_0
    move-exception v0

    .line 180
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v0
.end method

.method a(Lokhttp3/ab;)Lc/r;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lokhttp3/ab;",
            ")",
            "Lc/r<",
            "TT;>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 198
    invoke-virtual {p1}, Lokhttp3/ab;->g()Lokhttp3/ac;

    move-result-object v0

    .line 201
    invoke-virtual {p1}, Lokhttp3/ab;->h()Lokhttp3/ab$a;

    move-result-object p1

    new-instance v1, Lc/l$b;

    .line 202
    invoke-virtual {v0}, Lokhttp3/ac;->a()Lokhttp3/u;

    move-result-object v2

    invoke-virtual {v0}, Lokhttp3/ac;->b()J

    move-result-wide v3

    invoke-direct {v1, v2, v3, v4}, Lc/l$b;-><init>(Lokhttp3/u;J)V

    invoke-virtual {p1, v1}, Lokhttp3/ab$a;->a(Lokhttp3/ac;)Lokhttp3/ab$a;

    move-result-object p1

    .line 203
    invoke-virtual {p1}, Lokhttp3/ab$a;->a()Lokhttp3/ab;

    move-result-object p1

    .line 205
    invoke-virtual {p1}, Lokhttp3/ab;->b()I

    move-result v1

    const/16 v2, 0xc8

    if-lt v1, v2, :cond_3

    const/16 v2, 0x12c

    if-lt v1, v2, :cond_0

    goto :goto_1

    :cond_0
    const/16 v2, 0xcc

    if-eq v1, v2, :cond_2

    const/16 v2, 0xcd

    if-ne v1, v2, :cond_1

    goto :goto_0

    .line 221
    :cond_1
    new-instance v1, Lc/l$a;

    invoke-direct {v1, v0}, Lc/l$a;-><init>(Lokhttp3/ac;)V

    .line 223
    :try_start_0
    iget-object v0, p0, Lc/l;->d:Lc/f;

    invoke-interface {v0, v1}, Lc/f;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 224
    invoke-static {v0, p1}, Lc/r;->a(Ljava/lang/Object;Lokhttp3/ab;)Lc/r;

    move-result-object p1
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    move-exception p1

    .line 228
    invoke-virtual {v1}, Lc/l$a;->f()V

    .line 229
    throw p1

    .line 217
    :cond_2
    :goto_0
    invoke-virtual {v0}, Lokhttp3/ac;->close()V

    const/4 v0, 0x0

    .line 218
    invoke-static {v0, p1}, Lc/r;->a(Ljava/lang/Object;Lokhttp3/ab;)Lc/r;

    move-result-object p1

    return-object p1

    .line 209
    :cond_3
    :goto_1
    :try_start_1
    invoke-static {v0}, Lc/u;->a(Lokhttp3/ac;)Lokhttp3/ac;

    move-result-object v1

    .line 210
    invoke-static {v1, p1}, Lc/r;->a(Lokhttp3/ac;Lokhttp3/ab;)Lc/r;

    move-result-object p1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 212
    invoke-virtual {v0}, Lokhttp3/ac;->close()V

    return-object p1

    :catchall_0
    move-exception p1

    invoke-virtual {v0}, Lokhttp3/ac;->close()V

    throw p1
.end method

.method public a(Lc/d;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lc/d<",
            "TT;>;)V"
        }
    .end annotation

    const-string v0, "callback == null"

    .line 87
    invoke-static {p1, v0}, Lc/u;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 92
    monitor-enter p0

    .line 93
    :try_start_0
    iget-boolean v0, p0, Lc/l;->h:Z

    if-nez v0, :cond_3

    const/4 v0, 0x1

    .line 94
    iput-boolean v0, p0, Lc/l;->h:Z

    .line 96
    iget-object v0, p0, Lc/l;->f:Lokhttp3/e;

    .line 97
    iget-object v1, p0, Lc/l;->g:Ljava/lang/Throwable;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    if-nez v1, :cond_0

    .line 100
    :try_start_1
    invoke-direct {p0}, Lc/l;->f()Lokhttp3/e;

    move-result-object v2

    iput-object v2, p0, Lc/l;->f:Lokhttp3/e;
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-object v0, v2

    goto :goto_0

    :catch_0
    move-exception v1

    .line 102
    :try_start_2
    invoke-static {v1}, Lc/u;->a(Ljava/lang/Throwable;)V

    .line 103
    iput-object v1, p0, Lc/l;->g:Ljava/lang/Throwable;

    .line 106
    :cond_0
    :goto_0
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    if-eqz v1, :cond_1

    .line 109
    invoke-interface {p1, p0, v1}, Lc/d;->onFailure(Lc/b;Ljava/lang/Throwable;)V

    return-void

    .line 113
    :cond_1
    iget-boolean v1, p0, Lc/l;->e:Z

    if-eqz v1, :cond_2

    .line 114
    invoke-interface {v0}, Lokhttp3/e;->b()V

    .line 117
    :cond_2
    new-instance v1, Lc/l$1;

    invoke-direct {v1, p0, p1}, Lc/l$1;-><init>(Lc/l;Lc/d;)V

    invoke-interface {v0, v1}, Lokhttp3/e;->a(Lokhttp3/f;)V

    return-void

    .line 93
    :cond_3
    :try_start_3
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Already executed."

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    :catchall_0
    move-exception p1

    .line 106
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw p1
.end method

.method public b()V
    .locals 1

    const/4 v0, 0x1

    .line 234
    iput-boolean v0, p0, Lc/l;->e:Z

    .line 237
    monitor-enter p0

    .line 238
    :try_start_0
    iget-object v0, p0, Lc/l;->f:Lokhttp3/e;

    .line 239
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    .line 241
    invoke-interface {v0}, Lokhttp3/e;->b()V

    :cond_0
    return-void

    :catchall_0
    move-exception v0

    .line 239
    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public c()Z
    .locals 2

    .line 246
    iget-boolean v0, p0, Lc/l;->e:Z

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    return v1

    .line 249
    :cond_0
    monitor-enter p0

    .line 250
    :try_start_0
    iget-object v0, p0, Lc/l;->f:Lokhttp3/e;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lc/l;->f:Lokhttp3/e;

    invoke-interface {v0}, Lokhttp3/e;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    :goto_0
    monitor-exit p0

    return v1

    :catchall_0
    move-exception v0

    .line 251
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .line 32
    invoke-virtual {p0}, Lc/l;->e()Lc/l;

    move-result-object v0

    return-object v0
.end method

.method public synthetic d()Lc/b;
    .locals 1

    .line 32
    invoke-virtual {p0}, Lc/l;->e()Lc/l;

    move-result-object v0

    return-object v0
.end method

.method public e()Lc/l;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lc/l<",
            "TT;>;"
        }
    .end annotation

    .line 57
    new-instance v0, Lc/l;

    iget-object v1, p0, Lc/l;->a:Lc/q;

    iget-object v2, p0, Lc/l;->b:[Ljava/lang/Object;

    iget-object v3, p0, Lc/l;->c:Lokhttp3/e$a;

    iget-object v4, p0, Lc/l;->d:Lc/f;

    invoke-direct {v0, v1, v2, v3, v4}, Lc/l;-><init>(Lc/q;[Ljava/lang/Object;Lokhttp3/e$a;Lc/f;)V

    return-object v0
.end method
