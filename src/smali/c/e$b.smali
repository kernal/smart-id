.class final Lc/e$b;
.super Ljava/lang/Object;
.source "CompletableFutureCallAdapterFactory.java"

# interfaces
.implements Lc/c;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lc/e;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lc/c<",
        "TR;",
        "Ljava/util/concurrent/CompletableFuture<",
        "Lc/r<",
        "TR;>;>;>;"
    }
.end annotation

.annotation build Lorg/codehaus/mojo/animal_sniffer/IgnoreJRERequirement;
.end annotation


# instance fields
.field private final a:Ljava/lang/reflect/Type;


# direct methods
.method constructor <init>(Ljava/lang/reflect/Type;)V
    .locals 0

    .line 99
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 100
    iput-object p1, p0, Lc/e$b;->a:Ljava/lang/reflect/Type;

    return-void
.end method


# virtual methods
.method public synthetic a(Lc/b;)Ljava/lang/Object;
    .locals 0

    .line 94
    invoke-virtual {p0, p1}, Lc/e$b;->b(Lc/b;)Ljava/util/concurrent/CompletableFuture;

    move-result-object p1

    return-object p1
.end method

.method public a()Ljava/lang/reflect/Type;
    .locals 1

    .line 104
    iget-object v0, p0, Lc/e$b;->a:Ljava/lang/reflect/Type;

    return-object v0
.end method

.method public b(Lc/b;)Ljava/util/concurrent/CompletableFuture;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lc/b<",
            "TR;>;)",
            "Ljava/util/concurrent/CompletableFuture<",
            "Lc/r<",
            "TR;>;>;"
        }
    .end annotation

    .line 108
    new-instance v0, Lc/e$b$1;

    invoke-direct {v0, p0, p1}, Lc/e$b$1;-><init>(Lc/e$b;Lc/b;)V

    .line 117
    new-instance v1, Lc/e$b$2;

    invoke-direct {v1, p0, v0}, Lc/e$b$2;-><init>(Lc/e$b;Ljava/util/concurrent/CompletableFuture;)V

    invoke-interface {p1, v1}, Lc/b;->a(Lc/d;)V

    return-object v0
.end method
