.class final Lokhttp3/y$a;
.super Lokhttp3/internal/b;
.source "RealCall.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lokhttp3/y;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x10
    name = "a"
.end annotation


# static fields
.field static final synthetic a:Z


# instance fields
.field final synthetic b:Lokhttp3/y;

.field private final d:Lokhttp3/f;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 155
    const-class v0, Lokhttp3/y;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    sput-boolean v0, Lokhttp3/y$a;->a:Z

    return-void
.end method

.method constructor <init>(Lokhttp3/y;Lokhttp3/f;)V
    .locals 2

    .line 158
    iput-object p1, p0, Lokhttp3/y$a;->b:Lokhttp3/y;

    const/4 v0, 0x1

    .line 159
    new-array v0, v0, [Ljava/lang/Object;

    invoke-virtual {p1}, Lokhttp3/y;->f()Ljava/lang/String;

    move-result-object p1

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const-string p1, "OkHttp %s"

    invoke-direct {p0, p1, v0}, Lokhttp3/internal/b;-><init>(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 160
    iput-object p2, p0, Lokhttp3/y$a;->d:Lokhttp3/f;

    return-void
.end method


# virtual methods
.method a()Ljava/lang/String;
    .locals 1

    .line 164
    iget-object v0, p0, Lokhttp3/y$a;->b:Lokhttp3/y;

    iget-object v0, v0, Lokhttp3/y;->d:Lokhttp3/z;

    invoke-virtual {v0}, Lokhttp3/z;->a()Lokhttp3/s;

    move-result-object v0

    invoke-virtual {v0}, Lokhttp3/s;->f()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method a(Ljava/util/concurrent/ExecutorService;)V
    .locals 2

    .line 180
    sget-boolean v0, Lokhttp3/y$a;->a:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lokhttp3/y$a;->b:Lokhttp3/y;

    iget-object v0, v0, Lokhttp3/y;->a:Lokhttp3/w;

    invoke-virtual {v0}, Lokhttp3/w;->u()Lokhttp3/n;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    new-instance p1, Ljava/lang/AssertionError;

    invoke-direct {p1}, Ljava/lang/AssertionError;-><init>()V

    throw p1

    .line 183
    :cond_1
    :goto_0
    :try_start_0
    invoke-interface {p1, p0}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V
    :try_end_0
    .catch Ljava/util/concurrent/RejectedExecutionException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception p1

    goto :goto_2

    :catch_0
    move-exception p1

    .line 186
    :try_start_1
    new-instance v0, Ljava/io/InterruptedIOException;

    const-string v1, "executor rejected"

    invoke-direct {v0, v1}, Ljava/io/InterruptedIOException;-><init>(Ljava/lang/String;)V

    .line 187
    invoke-virtual {v0, p1}, Ljava/io/InterruptedIOException;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    .line 188
    iget-object p1, p0, Lokhttp3/y$a;->b:Lokhttp3/y;

    invoke-static {p1}, Lokhttp3/y;->a(Lokhttp3/y;)Lokhttp3/p;

    move-result-object p1

    iget-object v1, p0, Lokhttp3/y$a;->b:Lokhttp3/y;

    invoke-virtual {p1, v1, v0}, Lokhttp3/p;->a(Lokhttp3/e;Ljava/io/IOException;)V

    .line 189
    iget-object p1, p0, Lokhttp3/y$a;->d:Lokhttp3/f;

    iget-object v1, p0, Lokhttp3/y$a;->b:Lokhttp3/y;

    invoke-interface {p1, v1, v0}, Lokhttp3/f;->a(Lokhttp3/e;Ljava/io/IOException;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 192
    iget-object p1, p0, Lokhttp3/y$a;->b:Lokhttp3/y;

    iget-object p1, p1, Lokhttp3/y;->a:Lokhttp3/w;

    invoke-virtual {p1}, Lokhttp3/w;->u()Lokhttp3/n;

    move-result-object p1

    invoke-virtual {p1, p0}, Lokhttp3/n;->b(Lokhttp3/y$a;)V

    :goto_1
    return-void

    :goto_2
    iget-object v0, p0, Lokhttp3/y$a;->b:Lokhttp3/y;

    iget-object v0, v0, Lokhttp3/y;->a:Lokhttp3/w;

    invoke-virtual {v0}, Lokhttp3/w;->u()Lokhttp3/n;

    move-result-object v0

    invoke-virtual {v0, p0}, Lokhttp3/n;->b(Lokhttp3/y$a;)V

    throw p1
.end method

.method b()Lokhttp3/y;
    .locals 1

    .line 172
    iget-object v0, p0, Lokhttp3/y$a;->b:Lokhttp3/y;

    return-object v0
.end method

.method protected c()V
    .locals 5

    .line 199
    iget-object v0, p0, Lokhttp3/y$a;->b:Lokhttp3/y;

    iget-object v0, v0, Lokhttp3/y;->c:Lb/a;

    invoke-virtual {v0}, Lb/a;->c()V

    const/4 v0, 0x0

    .line 201
    :try_start_0
    iget-object v1, p0, Lokhttp3/y$a;->b:Lokhttp3/y;

    invoke-virtual {v1}, Lokhttp3/y;->g()Lokhttp3/ab;

    move-result-object v0
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v1, 0x1

    .line 203
    :try_start_1
    iget-object v2, p0, Lokhttp3/y$a;->d:Lokhttp3/f;

    iget-object v3, p0, Lokhttp3/y$a;->b:Lokhttp3/y;

    invoke-interface {v2, v3, v0}, Lokhttp3/f;->a(Lokhttp3/e;Lokhttp3/ab;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 214
    :goto_0
    iget-object v0, p0, Lokhttp3/y$a;->b:Lokhttp3/y;

    iget-object v0, v0, Lokhttp3/y;->a:Lokhttp3/w;

    invoke-virtual {v0}, Lokhttp3/w;->u()Lokhttp3/n;

    move-result-object v0

    invoke-virtual {v0, p0}, Lokhttp3/n;->b(Lokhttp3/y$a;)V

    goto :goto_2

    :catch_0
    move-exception v0

    goto :goto_1

    :catchall_0
    move-exception v0

    goto :goto_3

    :catch_1
    move-exception v1

    move-object v0, v1

    const/4 v1, 0x0

    .line 205
    :goto_1
    :try_start_2
    iget-object v2, p0, Lokhttp3/y$a;->b:Lokhttp3/y;

    invoke-virtual {v2, v0}, Lokhttp3/y;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object v0

    if-eqz v1, :cond_0

    .line 208
    invoke-static {}, Lokhttp3/internal/g/f;->c()Lokhttp3/internal/g/f;

    move-result-object v1

    const/4 v2, 0x4

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Callback failure for "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v4, p0, Lokhttp3/y$a;->b:Lokhttp3/y;

    invoke-virtual {v4}, Lokhttp3/y;->e()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3, v0}, Lokhttp3/internal/g/f;->a(ILjava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 210
    :cond_0
    iget-object v1, p0, Lokhttp3/y$a;->b:Lokhttp3/y;

    invoke-static {v1}, Lokhttp3/y;->a(Lokhttp3/y;)Lokhttp3/p;

    move-result-object v1

    iget-object v2, p0, Lokhttp3/y$a;->b:Lokhttp3/y;

    invoke-virtual {v1, v2, v0}, Lokhttp3/p;->a(Lokhttp3/e;Ljava/io/IOException;)V

    .line 211
    iget-object v1, p0, Lokhttp3/y$a;->d:Lokhttp3/f;

    iget-object v2, p0, Lokhttp3/y$a;->b:Lokhttp3/y;

    invoke-interface {v1, v2, v0}, Lokhttp3/f;->a(Lokhttp3/e;Ljava/io/IOException;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :goto_2
    return-void

    .line 214
    :goto_3
    iget-object v1, p0, Lokhttp3/y$a;->b:Lokhttp3/y;

    iget-object v1, v1, Lokhttp3/y;->a:Lokhttp3/w;

    invoke-virtual {v1}, Lokhttp3/w;->u()Lokhttp3/n;

    move-result-object v1

    invoke-virtual {v1, p0}, Lokhttp3/n;->b(Lokhttp3/y$a;)V

    throw v0
.end method
