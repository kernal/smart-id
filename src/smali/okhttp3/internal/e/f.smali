.class public final Lokhttp3/internal/e/f;
.super Ljava/lang/Object;
.source "Http2Codec.java"

# interfaces
.implements Lokhttp3/internal/c/c;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lokhttp3/internal/e/f$a;
    }
.end annotation


# static fields
.field private static final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field final a:Lokhttp3/internal/b/g;

.field private final d:Lokhttp3/t$a;

.field private final e:Lokhttp3/internal/e/g;

.field private f:Lokhttp3/internal/e/i;

.field private final g:Lokhttp3/x;


# direct methods
.method static constructor <clinit>()V
    .locals 12

    const-string v0, "connection"

    const-string v1, "host"

    const-string v2, "keep-alive"

    const-string v3, "proxy-connection"

    const-string v4, "te"

    const-string v5, "transfer-encoding"

    const-string v6, "encoding"

    const-string v7, "upgrade"

    const-string v8, ":method"

    const-string v9, ":path"

    const-string v10, ":scheme"

    const-string v11, ":authority"

    .line 69
    filled-new-array/range {v0 .. v11}, [Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lokhttp3/internal/c;->a([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lokhttp3/internal/e/f;->b:Ljava/util/List;

    const-string v1, "connection"

    const-string v2, "host"

    const-string v3, "keep-alive"

    const-string v4, "proxy-connection"

    const-string v5, "te"

    const-string v6, "transfer-encoding"

    const-string v7, "encoding"

    const-string v8, "upgrade"

    .line 82
    filled-new-array/range {v1 .. v8}, [Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lokhttp3/internal/c;->a([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lokhttp3/internal/e/f;->c:Ljava/util/List;

    return-void
.end method

.method public constructor <init>(Lokhttp3/w;Lokhttp3/t$a;Lokhttp3/internal/b/g;Lokhttp3/internal/e/g;)V
    .locals 0

    .line 99
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 100
    iput-object p2, p0, Lokhttp3/internal/e/f;->d:Lokhttp3/t$a;

    .line 101
    iput-object p3, p0, Lokhttp3/internal/e/f;->a:Lokhttp3/internal/b/g;

    .line 102
    iput-object p4, p0, Lokhttp3/internal/e/f;->e:Lokhttp3/internal/e/g;

    .line 103
    invoke-virtual {p1}, Lokhttp3/w;->v()Ljava/util/List;

    move-result-object p1

    sget-object p2, Lokhttp3/x;->e:Lokhttp3/x;

    invoke-interface {p1, p2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 104
    sget-object p1, Lokhttp3/x;->e:Lokhttp3/x;

    goto :goto_0

    .line 105
    :cond_0
    sget-object p1, Lokhttp3/x;->d:Lokhttp3/x;

    :goto_0
    iput-object p1, p0, Lokhttp3/internal/e/f;->g:Lokhttp3/x;

    return-void
.end method

.method public static a(Lokhttp3/Headers;Lokhttp3/x;)Lokhttp3/ab$a;
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 164
    new-instance v0, Lokhttp3/Headers$a;

    invoke-direct {v0}, Lokhttp3/Headers$a;-><init>()V

    .line 165
    invoke-virtual {p0}, Lokhttp3/Headers;->size()I

    move-result v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v1, :cond_2

    .line 166
    invoke-virtual {p0, v3}, Lokhttp3/Headers;->name(I)Ljava/lang/String;

    move-result-object v4

    .line 167
    invoke-virtual {p0, v3}, Lokhttp3/Headers;->value(I)Ljava/lang/String;

    move-result-object v5

    const-string v6, ":status"

    .line 168
    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 169
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "HTTP/1.1 "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lokhttp3/internal/c/k;->a(Ljava/lang/String;)Lokhttp3/internal/c/k;

    move-result-object v2

    goto :goto_1

    .line 170
    :cond_0
    sget-object v6, Lokhttp3/internal/e/f;->c:Ljava/util/List;

    invoke-interface {v6, v4}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_1

    .line 171
    sget-object v6, Lokhttp3/internal/a;->a:Lokhttp3/internal/a;

    invoke-virtual {v6, v0, v4, v5}, Lokhttp3/internal/a;->a(Lokhttp3/Headers$a;Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_2
    if-eqz v2, :cond_3

    .line 176
    new-instance p0, Lokhttp3/ab$a;

    invoke-direct {p0}, Lokhttp3/ab$a;-><init>()V

    .line 177
    invoke-virtual {p0, p1}, Lokhttp3/ab$a;->a(Lokhttp3/x;)Lokhttp3/ab$a;

    move-result-object p0

    iget p1, v2, Lokhttp3/internal/c/k;->b:I

    .line 178
    invoke-virtual {p0, p1}, Lokhttp3/ab$a;->a(I)Lokhttp3/ab$a;

    move-result-object p0

    iget-object p1, v2, Lokhttp3/internal/c/k;->c:Ljava/lang/String;

    .line 179
    invoke-virtual {p0, p1}, Lokhttp3/ab$a;->a(Ljava/lang/String;)Lokhttp3/ab$a;

    move-result-object p0

    .line 180
    invoke-virtual {v0}, Lokhttp3/Headers$a;->a()Lokhttp3/Headers;

    move-result-object p1

    invoke-virtual {p0, p1}, Lokhttp3/ab$a;->a(Lokhttp3/Headers;)Lokhttp3/ab$a;

    move-result-object p0

    return-object p0

    .line 174
    :cond_3
    new-instance p0, Ljava/net/ProtocolException;

    const-string p1, "Expected \':status\' header not present"

    invoke-direct {p0, p1}, Ljava/net/ProtocolException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method public static b(Lokhttp3/z;)Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lokhttp3/z;",
            ")",
            "Ljava/util/List<",
            "Lokhttp3/internal/e/c;",
            ">;"
        }
    .end annotation

    .line 140
    invoke-virtual {p0}, Lokhttp3/z;->c()Lokhttp3/Headers;

    move-result-object v0

    .line 141
    new-instance v1, Ljava/util/ArrayList;

    invoke-virtual {v0}, Lokhttp3/Headers;->size()I

    move-result v2

    add-int/lit8 v2, v2, 0x4

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 142
    new-instance v2, Lokhttp3/internal/e/c;

    sget-object v3, Lokhttp3/internal/e/c;->c:Lb/f;

    invoke-virtual {p0}, Lokhttp3/z;->b()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lokhttp3/internal/e/c;-><init>(Lb/f;Ljava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 143
    new-instance v2, Lokhttp3/internal/e/c;

    sget-object v3, Lokhttp3/internal/e/c;->d:Lb/f;

    invoke-virtual {p0}, Lokhttp3/z;->a()Lokhttp3/s;

    move-result-object v4

    invoke-static {v4}, Lokhttp3/internal/c/i;->a(Lokhttp3/s;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lokhttp3/internal/e/c;-><init>(Lb/f;Ljava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const-string v2, "Host"

    .line 144
    invoke-virtual {p0, v2}, Lokhttp3/z;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 146
    new-instance v3, Lokhttp3/internal/e/c;

    sget-object v4, Lokhttp3/internal/e/c;->f:Lb/f;

    invoke-direct {v3, v4, v2}, Lokhttp3/internal/e/c;-><init>(Lb/f;Ljava/lang/String;)V

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 148
    :cond_0
    new-instance v2, Lokhttp3/internal/e/c;

    sget-object v3, Lokhttp3/internal/e/c;->e:Lb/f;

    invoke-virtual {p0}, Lokhttp3/z;->a()Lokhttp3/s;

    move-result-object p0

    invoke-virtual {p0}, Lokhttp3/s;->b()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v2, v3, p0}, Lokhttp3/internal/e/c;-><init>(Lb/f;Ljava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const/4 p0, 0x0

    .line 150
    invoke-virtual {v0}, Lokhttp3/Headers;->size()I

    move-result v2

    :goto_0
    if-ge p0, v2, :cond_2

    .line 152
    invoke-virtual {v0, p0}, Lokhttp3/Headers;->name(I)Ljava/lang/String;

    move-result-object v3

    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v3, v4}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lb/f;->a(Ljava/lang/String;)Lb/f;

    move-result-object v3

    .line 153
    sget-object v4, Lokhttp3/internal/e/f;->b:Ljava/util/List;

    invoke-virtual {v3}, Lb/f;->a()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 154
    new-instance v4, Lokhttp3/internal/e/c;

    invoke-virtual {v0, p0}, Lokhttp3/Headers;->value(I)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v3, v5}, Lokhttp3/internal/e/c;-><init>(Lb/f;Ljava/lang/String;)V

    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1
    add-int/lit8 p0, p0, 0x1

    goto :goto_0

    :cond_2
    return-object v1
.end method


# virtual methods
.method public a(Lokhttp3/z;J)Lb/r;
    .locals 0

    .line 109
    iget-object p1, p0, Lokhttp3/internal/e/f;->f:Lokhttp3/internal/e/i;

    invoke-virtual {p1}, Lokhttp3/internal/e/i;->h()Lb/r;

    move-result-object p1

    return-object p1
.end method

.method public a(Z)Lokhttp3/ab$a;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 131
    iget-object v0, p0, Lokhttp3/internal/e/f;->f:Lokhttp3/internal/e/i;

    invoke-virtual {v0}, Lokhttp3/internal/e/i;->d()Lokhttp3/Headers;

    move-result-object v0

    .line 132
    iget-object v1, p0, Lokhttp3/internal/e/f;->g:Lokhttp3/x;

    invoke-static {v0, v1}, Lokhttp3/internal/e/f;->a(Lokhttp3/Headers;Lokhttp3/x;)Lokhttp3/ab$a;

    move-result-object v0

    if-eqz p1, :cond_0

    .line 133
    sget-object p1, Lokhttp3/internal/a;->a:Lokhttp3/internal/a;

    invoke-virtual {p1, v0}, Lokhttp3/internal/a;->a(Lokhttp3/ab$a;)I

    move-result p1

    const/16 v1, 0x64

    if-ne p1, v1, :cond_0

    const/4 p1, 0x0

    return-object p1

    :cond_0
    return-object v0
.end method

.method public a(Lokhttp3/ab;)Lokhttp3/ac;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 184
    iget-object v0, p0, Lokhttp3/internal/e/f;->a:Lokhttp3/internal/b/g;

    iget-object v0, v0, Lokhttp3/internal/b/g;->c:Lokhttp3/p;

    iget-object v1, p0, Lokhttp3/internal/e/f;->a:Lokhttp3/internal/b/g;

    iget-object v1, v1, Lokhttp3/internal/b/g;->b:Lokhttp3/e;

    invoke-virtual {v0, v1}, Lokhttp3/p;->f(Lokhttp3/e;)V

    const-string v0, "Content-Type"

    .line 185
    invoke-virtual {p1, v0}, Lokhttp3/ab;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 186
    invoke-static {p1}, Lokhttp3/internal/c/e;->a(Lokhttp3/ab;)J

    move-result-wide v1

    .line 187
    new-instance p1, Lokhttp3/internal/e/f$a;

    iget-object v3, p0, Lokhttp3/internal/e/f;->f:Lokhttp3/internal/e/i;

    invoke-virtual {v3}, Lokhttp3/internal/e/i;->g()Lb/s;

    move-result-object v3

    invoke-direct {p1, p0, v3}, Lokhttp3/internal/e/f$a;-><init>(Lokhttp3/internal/e/f;Lb/s;)V

    .line 188
    new-instance v3, Lokhttp3/internal/c/h;

    invoke-static {p1}, Lb/l;->a(Lb/s;)Lb/e;

    move-result-object p1

    invoke-direct {v3, v0, v1, v2, p1}, Lokhttp3/internal/c/h;-><init>(Ljava/lang/String;JLb/e;)V

    return-object v3
.end method

.method public a()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 123
    iget-object v0, p0, Lokhttp3/internal/e/f;->e:Lokhttp3/internal/e/g;

    invoke-virtual {v0}, Lokhttp3/internal/e/g;->b()V

    return-void
.end method

.method public a(Lokhttp3/z;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 113
    iget-object v0, p0, Lokhttp3/internal/e/f;->f:Lokhttp3/internal/e/i;

    if-eqz v0, :cond_0

    return-void

    .line 115
    :cond_0
    invoke-virtual {p1}, Lokhttp3/z;->d()Lokhttp3/aa;

    move-result-object v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    .line 116
    :goto_0
    invoke-static {p1}, Lokhttp3/internal/e/f;->b(Lokhttp3/z;)Ljava/util/List;

    move-result-object p1

    .line 117
    iget-object v1, p0, Lokhttp3/internal/e/f;->e:Lokhttp3/internal/e/g;

    invoke-virtual {v1, p1, v0}, Lokhttp3/internal/e/g;->a(Ljava/util/List;Z)Lokhttp3/internal/e/i;

    move-result-object p1

    iput-object p1, p0, Lokhttp3/internal/e/f;->f:Lokhttp3/internal/e/i;

    .line 118
    iget-object p1, p0, Lokhttp3/internal/e/f;->f:Lokhttp3/internal/e/i;

    invoke-virtual {p1}, Lokhttp3/internal/e/i;->e()Lb/t;

    move-result-object p1

    iget-object v0, p0, Lokhttp3/internal/e/f;->d:Lokhttp3/t$a;

    invoke-interface {v0}, Lokhttp3/t$a;->d()I

    move-result v0

    int-to-long v0, v0

    sget-object v2, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {p1, v0, v1, v2}, Lb/t;->a(JLjava/util/concurrent/TimeUnit;)Lb/t;

    .line 119
    iget-object p1, p0, Lokhttp3/internal/e/f;->f:Lokhttp3/internal/e/i;

    invoke-virtual {p1}, Lokhttp3/internal/e/i;->f()Lb/t;

    move-result-object p1

    iget-object v0, p0, Lokhttp3/internal/e/f;->d:Lokhttp3/t$a;

    invoke-interface {v0}, Lokhttp3/t$a;->e()I

    move-result v0

    int-to-long v0, v0

    sget-object v2, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {p1, v0, v1, v2}, Lb/t;->a(JLjava/util/concurrent/TimeUnit;)Lb/t;

    return-void
.end method

.method public b()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 127
    iget-object v0, p0, Lokhttp3/internal/e/f;->f:Lokhttp3/internal/e/i;

    invoke-virtual {v0}, Lokhttp3/internal/e/i;->h()Lb/r;

    move-result-object v0

    invoke-interface {v0}, Lb/r;->close()V

    return-void
.end method

.method public c()V
    .locals 2

    .line 192
    iget-object v0, p0, Lokhttp3/internal/e/f;->f:Lokhttp3/internal/e/i;

    if-eqz v0, :cond_0

    sget-object v1, Lokhttp3/internal/e/b;->f:Lokhttp3/internal/e/b;

    invoke-virtual {v0, v1}, Lokhttp3/internal/e/i;->b(Lokhttp3/internal/e/b;)V

    :cond_0
    return-void
.end method
