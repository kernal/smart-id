.class final Lokhttp3/y;
.super Ljava/lang/Object;
.source "RealCall.java"

# interfaces
.implements Lokhttp3/e;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lokhttp3/y$a;
    }
.end annotation


# instance fields
.field final a:Lokhttp3/w;

.field final b:Lokhttp3/internal/c/j;

.field final c:Lb/a;

.field final d:Lokhttp3/z;

.field final e:Z

.field private f:Lokhttp3/p;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Z


# direct methods
.method private constructor <init>(Lokhttp3/w;Lokhttp3/z;Z)V
    .locals 2

    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 60
    iput-object p1, p0, Lokhttp3/y;->a:Lokhttp3/w;

    .line 61
    iput-object p2, p0, Lokhttp3/y;->d:Lokhttp3/z;

    .line 62
    iput-boolean p3, p0, Lokhttp3/y;->e:Z

    .line 63
    new-instance p2, Lokhttp3/internal/c/j;

    invoke-direct {p2, p1, p3}, Lokhttp3/internal/c/j;-><init>(Lokhttp3/w;Z)V

    iput-object p2, p0, Lokhttp3/y;->b:Lokhttp3/internal/c/j;

    .line 64
    new-instance p2, Lokhttp3/y$1;

    invoke-direct {p2, p0}, Lokhttp3/y$1;-><init>(Lokhttp3/y;)V

    iput-object p2, p0, Lokhttp3/y;->c:Lb/a;

    .line 69
    iget-object p2, p0, Lokhttp3/y;->c:Lb/a;

    invoke-virtual {p1}, Lokhttp3/w;->a()I

    move-result p1

    int-to-long v0, p1

    sget-object p1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {p2, v0, v1, p1}, Lb/a;->a(JLjava/util/concurrent/TimeUnit;)Lb/t;

    return-void
.end method

.method static synthetic a(Lokhttp3/y;)Lokhttp3/p;
    .locals 0

    .line 41
    iget-object p0, p0, Lokhttp3/y;->f:Lokhttp3/p;

    return-object p0
.end method

.method static a(Lokhttp3/w;Lokhttp3/z;Z)Lokhttp3/y;
    .locals 1

    .line 74
    new-instance v0, Lokhttp3/y;

    invoke-direct {v0, p0, p1, p2}, Lokhttp3/y;-><init>(Lokhttp3/w;Lokhttp3/z;Z)V

    .line 75
    invoke-virtual {p0}, Lokhttp3/w;->z()Lokhttp3/p$a;

    move-result-object p0

    invoke-interface {p0, v0}, Lokhttp3/p$a;->a(Lokhttp3/e;)Lokhttp3/p;

    move-result-object p0

    iput-object p0, v0, Lokhttp3/y;->f:Lokhttp3/p;

    return-object v0
.end method

.method private h()V
    .locals 2

    .line 116
    invoke-static {}, Lokhttp3/internal/g/f;->c()Lokhttp3/internal/g/f;

    move-result-object v0

    const-string v1, "response.body().close()"

    invoke-virtual {v0, v1}, Lokhttp3/internal/g/f;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 117
    iget-object v1, p0, Lokhttp3/y;->b:Lokhttp3/internal/c/j;

    invoke-virtual {v1, v0}, Lokhttp3/internal/c/j;->a(Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method a(Ljava/io/IOException;)Ljava/io/IOException;
    .locals 2
    .param p1    # Ljava/io/IOException;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .line 106
    iget-object v0, p0, Lokhttp3/y;->c:Lb/a;

    invoke-virtual {v0}, Lb/a;->i_()Z

    move-result v0

    if-nez v0, :cond_0

    return-object p1

    .line 108
    :cond_0
    new-instance v0, Ljava/io/InterruptedIOException;

    const-string v1, "timeout"

    invoke-direct {v0, v1}, Ljava/io/InterruptedIOException;-><init>(Ljava/lang/String;)V

    if-eqz p1, :cond_1

    .line 110
    invoke-virtual {v0, p1}, Ljava/io/InterruptedIOException;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    :cond_1
    return-object v0
.end method

.method public a()Lokhttp3/ab;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 84
    monitor-enter p0

    .line 85
    :try_start_0
    iget-boolean v0, p0, Lokhttp3/y;->g:Z

    if-nez v0, :cond_1

    const/4 v0, 0x1

    .line 86
    iput-boolean v0, p0, Lokhttp3/y;->g:Z

    .line 87
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 88
    invoke-direct {p0}, Lokhttp3/y;->h()V

    .line 89
    iget-object v0, p0, Lokhttp3/y;->c:Lb/a;

    invoke-virtual {v0}, Lb/a;->c()V

    .line 90
    iget-object v0, p0, Lokhttp3/y;->f:Lokhttp3/p;

    invoke-virtual {v0, p0}, Lokhttp3/p;->a(Lokhttp3/e;)V

    .line 92
    :try_start_1
    iget-object v0, p0, Lokhttp3/y;->a:Lokhttp3/w;

    invoke-virtual {v0}, Lokhttp3/w;->u()Lokhttp3/n;

    move-result-object v0

    invoke-virtual {v0, p0}, Lokhttp3/n;->a(Lokhttp3/y;)V

    .line 93
    invoke-virtual {p0}, Lokhttp3/y;->g()Lokhttp3/ab;

    move-result-object v0
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v0, :cond_0

    .line 101
    iget-object v1, p0, Lokhttp3/y;->a:Lokhttp3/w;

    invoke-virtual {v1}, Lokhttp3/w;->u()Lokhttp3/n;

    move-result-object v1

    invoke-virtual {v1, p0}, Lokhttp3/n;->b(Lokhttp3/y;)V

    return-object v0

    .line 94
    :cond_0
    :try_start_2
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Canceled"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :catchall_0
    move-exception v0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 97
    :try_start_3
    invoke-virtual {p0, v0}, Lokhttp3/y;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object v0

    .line 98
    iget-object v1, p0, Lokhttp3/y;->f:Lokhttp3/p;

    invoke-virtual {v1, p0, v0}, Lokhttp3/p;->a(Lokhttp3/e;Ljava/io/IOException;)V

    .line 99
    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 101
    :goto_0
    iget-object v1, p0, Lokhttp3/y;->a:Lokhttp3/w;

    invoke-virtual {v1}, Lokhttp3/w;->u()Lokhttp3/n;

    move-result-object v1

    invoke-virtual {v1, p0}, Lokhttp3/n;->b(Lokhttp3/y;)V

    throw v0

    .line 85
    :cond_1
    :try_start_4
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Already Executed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :catchall_1
    move-exception v0

    .line 87
    monitor-exit p0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v0
.end method

.method public a(Lokhttp3/f;)V
    .locals 2

    .line 121
    monitor-enter p0

    .line 122
    :try_start_0
    iget-boolean v0, p0, Lokhttp3/y;->g:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 123
    iput-boolean v0, p0, Lokhttp3/y;->g:Z

    .line 124
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 125
    invoke-direct {p0}, Lokhttp3/y;->h()V

    .line 126
    iget-object v0, p0, Lokhttp3/y;->f:Lokhttp3/p;

    invoke-virtual {v0, p0}, Lokhttp3/p;->a(Lokhttp3/e;)V

    .line 127
    iget-object v0, p0, Lokhttp3/y;->a:Lokhttp3/w;

    invoke-virtual {v0}, Lokhttp3/w;->u()Lokhttp3/n;

    move-result-object v0

    new-instance v1, Lokhttp3/y$a;

    invoke-direct {v1, p0, p1}, Lokhttp3/y$a;-><init>(Lokhttp3/y;Lokhttp3/f;)V

    invoke-virtual {v0, v1}, Lokhttp3/n;->a(Lokhttp3/y$a;)V

    return-void

    .line 122
    :cond_0
    :try_start_1
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Already Executed"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    :catchall_0
    move-exception p1

    .line 124
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw p1
.end method

.method public b()V
    .locals 1

    .line 131
    iget-object v0, p0, Lokhttp3/y;->b:Lokhttp3/internal/c/j;

    invoke-virtual {v0}, Lokhttp3/internal/c/j;->a()V

    return-void
.end method

.method public c()Z
    .locals 1

    .line 143
    iget-object v0, p0, Lokhttp3/y;->b:Lokhttp3/internal/c/j;

    invoke-virtual {v0}, Lokhttp3/internal/c/j;->b()Z

    move-result v0

    return v0
.end method

.method public synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .line 41
    invoke-virtual {p0}, Lokhttp3/y;->d()Lokhttp3/y;

    move-result-object v0

    return-object v0
.end method

.method public d()Lokhttp3/y;
    .locals 3

    .line 148
    iget-object v0, p0, Lokhttp3/y;->a:Lokhttp3/w;

    iget-object v1, p0, Lokhttp3/y;->d:Lokhttp3/z;

    iget-boolean v2, p0, Lokhttp3/y;->e:Z

    invoke-static {v0, v1, v2}, Lokhttp3/y;->a(Lokhttp3/w;Lokhttp3/z;Z)Lokhttp3/y;

    move-result-object v0

    return-object v0
.end method

.method e()Ljava/lang/String;
    .locals 2

    .line 224
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lokhttp3/y;->c()Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "canceled "

    goto :goto_0

    :cond_0
    const-string v1, ""

    :goto_0
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 225
    iget-boolean v1, p0, Lokhttp3/y;->e:Z

    if-eqz v1, :cond_1

    const-string v1, "web socket"

    goto :goto_1

    :cond_1
    const-string v1, "call"

    :goto_1
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " to "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 226
    invoke-virtual {p0}, Lokhttp3/y;->f()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method f()Ljava/lang/String;
    .locals 1

    .line 230
    iget-object v0, p0, Lokhttp3/y;->d:Lokhttp3/z;

    invoke-virtual {v0}, Lokhttp3/z;->a()Lokhttp3/s;

    move-result-object v0

    invoke-virtual {v0}, Lokhttp3/s;->n()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method g()Lokhttp3/ab;
    .locals 13
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 235
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 236
    iget-object v0, p0, Lokhttp3/y;->a:Lokhttp3/w;

    invoke-virtual {v0}, Lokhttp3/w;->x()Ljava/util/List;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 237
    iget-object v0, p0, Lokhttp3/y;->b:Lokhttp3/internal/c/j;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 238
    new-instance v0, Lokhttp3/internal/c/a;

    iget-object v2, p0, Lokhttp3/y;->a:Lokhttp3/w;

    invoke-virtual {v2}, Lokhttp3/w;->h()Lokhttp3/m;

    move-result-object v2

    invoke-direct {v0, v2}, Lokhttp3/internal/c/a;-><init>(Lokhttp3/m;)V

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 239
    new-instance v0, Lokhttp3/internal/a/a;

    iget-object v2, p0, Lokhttp3/y;->a:Lokhttp3/w;

    invoke-virtual {v2}, Lokhttp3/w;->i()Lokhttp3/internal/a/e;

    move-result-object v2

    invoke-direct {v0, v2}, Lokhttp3/internal/a/a;-><init>(Lokhttp3/internal/a/e;)V

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 240
    new-instance v0, Lokhttp3/internal/b/a;

    iget-object v2, p0, Lokhttp3/y;->a:Lokhttp3/w;

    invoke-direct {v0, v2}, Lokhttp3/internal/b/a;-><init>(Lokhttp3/w;)V

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 241
    iget-boolean v0, p0, Lokhttp3/y;->e:Z

    if-nez v0, :cond_0

    .line 242
    iget-object v0, p0, Lokhttp3/y;->a:Lokhttp3/w;

    invoke-virtual {v0}, Lokhttp3/w;->y()Ljava/util/List;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 244
    :cond_0
    new-instance v0, Lokhttp3/internal/c/b;

    iget-boolean v2, p0, Lokhttp3/y;->e:Z

    invoke-direct {v0, v2}, Lokhttp3/internal/c/b;-><init>(Z)V

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 246
    new-instance v12, Lokhttp3/internal/c/g;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    iget-object v6, p0, Lokhttp3/y;->d:Lokhttp3/z;

    iget-object v8, p0, Lokhttp3/y;->f:Lokhttp3/p;

    iget-object v0, p0, Lokhttp3/y;->a:Lokhttp3/w;

    .line 247
    invoke-virtual {v0}, Lokhttp3/w;->b()I

    move-result v9

    iget-object v0, p0, Lokhttp3/y;->a:Lokhttp3/w;

    .line 248
    invoke-virtual {v0}, Lokhttp3/w;->c()I

    move-result v10

    iget-object v0, p0, Lokhttp3/y;->a:Lokhttp3/w;

    invoke-virtual {v0}, Lokhttp3/w;->d()I

    move-result v11

    move-object v0, v12

    move-object v7, p0

    invoke-direct/range {v0 .. v11}, Lokhttp3/internal/c/g;-><init>(Ljava/util/List;Lokhttp3/internal/b/g;Lokhttp3/internal/c/c;Lokhttp3/internal/b/c;ILokhttp3/z;Lokhttp3/e;Lokhttp3/p;III)V

    .line 250
    iget-object v0, p0, Lokhttp3/y;->d:Lokhttp3/z;

    invoke-interface {v12, v0}, Lokhttp3/t$a;->a(Lokhttp3/z;)Lokhttp3/ab;

    move-result-object v0

    .line 251
    iget-object v1, p0, Lokhttp3/y;->b:Lokhttp3/internal/c/j;

    invoke-virtual {v1}, Lokhttp3/internal/c/j;->b()Z

    move-result v1

    if-nez v1, :cond_1

    return-object v0

    .line 252
    :cond_1
    invoke-static {v0}, Lokhttp3/internal/c;->a(Ljava/io/Closeable;)V

    .line 253
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Canceled"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
