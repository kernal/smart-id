.class public La/b/b/a;
.super Ljava/util/ArrayList;
.source "JSONArray.java"

# interfaces
.implements La/b/b/c;
.implements La/b/b/f;
.implements Ljava/util/List;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/ArrayList<",
        "Ljava/lang/Object;",
        ">;",
        "La/b/b/c;",
        "La/b/b/f;",
        "Ljava/util/List<",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 30
    invoke-direct {p0}, Ljava/util/ArrayList;-><init>()V

    return-void
.end method

.method public static a(Ljava/util/List;La/b/b/g;)Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Ljava/lang/Object;",
            ">;",
            "La/b/b/g;",
            ")",
            "Ljava/lang/String;"
        }
    .end annotation

    .line 50
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 52
    :try_start_0
    invoke-static {p0, v0, p1}, La/b/b/a;->a(Ljava/lang/Iterable;Ljava/lang/Appendable;La/b/b/g;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 56
    :catch_0
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static a(Ljava/lang/Iterable;Ljava/lang/Appendable;La/b/b/g;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable<",
            "+",
            "Ljava/lang/Object;",
            ">;",
            "Ljava/lang/Appendable;",
            "La/b/b/g;",
            ")V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    if-nez p0, :cond_0

    const-string p0, "null"

    .line 72
    invoke-interface {p1, p0}, Ljava/lang/Appendable;->append(Ljava/lang/CharSequence;)Ljava/lang/Appendable;

    return-void

    .line 75
    :cond_0
    sget-object v0, La/b/b/c/d;->e:La/b/b/c/e;

    invoke-interface {v0, p0, p1, p2}, La/b/b/c/e;->a(Ljava/lang/Object;Ljava/lang/Appendable;La/b/b/g;)V

    return-void
.end method


# virtual methods
.method public a(La/b/b/g;)Ljava/lang/String;
    .locals 0

    .line 106
    invoke-static {p0, p1}, La/b/b/a;->a(Ljava/util/List;La/b/b/g;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public a(Ljava/lang/Appendable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 127
    sget-object v0, La/b/b/i;->a:La/b/b/g;

    invoke-static {p0, p1, v0}, La/b/b/a;->a(Ljava/lang/Iterable;Ljava/lang/Appendable;La/b/b/g;)V

    return-void
.end method

.method public a(Ljava/lang/Appendable;La/b/b/g;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 131
    invoke-static {p0, p1, p2}, La/b/b/a;->a(Ljava/lang/Iterable;Ljava/lang/Appendable;La/b/b/g;)V

    return-void
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .line 102
    sget-object v0, La/b/b/i;->a:La/b/b/g;

    invoke-static {p0, v0}, La/b/b/a;->a(Ljava/util/List;La/b/b/g;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .line 113
    invoke-virtual {p0}, La/b/b/a;->b()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
