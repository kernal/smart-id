.class La/b/b/b/d;
.super La/b/b/b/c;
.source "JSONParserString.java"


# instance fields
.field private x:Ljava/lang/String;


# direct methods
.method public constructor <init>(I)V
    .locals 0

    .line 31
    invoke-direct {p0, p1}, La/b/b/b/c;-><init>(I)V

    return-void
.end method


# virtual methods
.method protected a(CI)I
    .locals 1

    .line 76
    iget-object v0, p0, La/b/b/b/d;->x:Ljava/lang/String;

    invoke-virtual {v0, p1, p2}, Ljava/lang/String;->indexOf(II)I

    move-result p1

    return p1
.end method

.method public a(Ljava/lang/String;La/b/b/d/f;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/String;",
            "La/b/b/d/f<",
            "TT;>;)TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            La/b/b/b/e;
        }
    .end annotation

    .line 55
    iget-object v0, p2, La/b/b/d/f;->q:La/b/b/d/e;

    iput-object v0, p0, La/b/b/b/d;->b:La/b/b/d/e;

    .line 56
    iput-object p1, p0, La/b/b/b/d;->x:Ljava/lang/String;

    .line 57
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result p1

    iput p1, p0, La/b/b/b/d;->w:I

    .line 58
    invoke-virtual {p0, p2}, La/b/b/b/d;->a(La/b/b/d/f;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method protected a(II)V
    .locals 1

    .line 62
    iget-object v0, p0, La/b/b/b/d;->x:Ljava/lang/String;

    invoke-virtual {v0, p1, p2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, La/b/b/b/d;->j:Ljava/lang/String;

    return-void
.end method

.method public b(Ljava/lang/String;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            La/b/b/b/e;
        }
    .end annotation

    .line 39
    sget-object v0, La/b/b/i;->c:La/b/b/d/e;

    iget-object v0, v0, La/b/b/d/e;->a:La/b/b/d/f;

    invoke-virtual {p0, p1, v0}, La/b/b/b/d;->a(Ljava/lang/String;La/b/b/d/f;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method protected b(II)V
    .locals 2

    :goto_0
    add-int/lit8 v0, p2, -0x1

    if-ge p1, v0, :cond_1

    .line 66
    iget-object v0, p0, La/b/b/b/d;->x:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->charAt(I)C

    move-result v0

    invoke-static {v0}, Ljava/lang/Character;->isWhitespace(C)Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_1

    :cond_0
    add-int/lit8 p1, p1, 0x1

    goto :goto_0

    :cond_1
    :goto_1
    add-int/lit8 v0, p2, -0x1

    if-le v0, p1, :cond_3

    .line 69
    iget-object v1, p0, La/b/b/b/d;->x:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v0

    invoke-static {v0}, Ljava/lang/Character;->isWhitespace(C)Z

    move-result v0

    if-nez v0, :cond_2

    goto :goto_2

    :cond_2
    add-int/lit8 p2, p2, -0x1

    goto :goto_1

    .line 72
    :cond_3
    :goto_2
    invoke-virtual {p0, p1, p2}, La/b/b/b/d;->a(II)V

    return-void
.end method

.method protected d()V
    .locals 2

    .line 83
    iget v0, p0, La/b/b/b/d;->k:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, La/b/b/b/d;->k:I

    iget v1, p0, La/b/b/b/d;->w:I

    if-lt v0, v1, :cond_0

    const/16 v0, 0x1a

    .line 84
    iput-char v0, p0, La/b/b/b/d;->a:C

    goto :goto_0

    .line 86
    :cond_0
    iget-object v0, p0, La/b/b/b/d;->x:Ljava/lang/String;

    iget v1, p0, La/b/b/b/d;->k:I

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v0

    iput-char v0, p0, La/b/b/b/d;->a:C

    :goto_0
    return-void
.end method

.method protected e()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            La/b/b/b/e;
        }
    .end annotation

    .line 103
    iget v0, p0, La/b/b/b/d;->k:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, La/b/b/b/d;->k:I

    iget v1, p0, La/b/b/b/d;->w:I

    if-ge v0, v1, :cond_0

    .line 107
    iget-object v0, p0, La/b/b/b/d;->x:Ljava/lang/String;

    iget v1, p0, La/b/b/b/d;->k:I

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v0

    iput-char v0, p0, La/b/b/b/d;->a:C

    return-void

    :cond_0
    const/16 v0, 0x1a

    .line 104
    iput-char v0, p0, La/b/b/b/d;->a:C

    .line 105
    new-instance v0, La/b/b/b/e;

    iget v1, p0, La/b/b/b/d;->k:I

    add-int/lit8 v1, v1, -0x1

    const/4 v2, 0x3

    const-string v3, "EOF"

    invoke-direct {v0, v1, v2, v3}, La/b/b/b/e;-><init>(IILjava/lang/Object;)V

    throw v0
.end method

.method protected f()V
    .locals 2

    .line 93
    iget v0, p0, La/b/b/b/d;->k:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, La/b/b/b/d;->k:I

    iget v1, p0, La/b/b/b/d;->w:I

    if-lt v0, v1, :cond_0

    const/16 v0, 0x1a

    .line 94
    iput-char v0, p0, La/b/b/b/d;->a:C

    goto :goto_0

    .line 96
    :cond_0
    iget-object v0, p0, La/b/b/b/d;->x:Ljava/lang/String;

    iget v1, p0, La/b/b/b/d;->k:I

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v0

    iput-char v0, p0, La/b/b/b/d;->a:C

    :goto_0
    return-void
.end method
