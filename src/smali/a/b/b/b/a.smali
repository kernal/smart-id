.class public La/b/b/b/a;
.super Ljava/lang/Object;
.source "JSONParser.java"


# static fields
.field public static a:I


# instance fields
.field private b:I

.field private c:La/b/b/b/d;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "JSON_SMART_SIMPLE"

    .line 122
    invoke-static {v0}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const/16 v0, 0x7c0

    goto :goto_0

    :cond_0
    const/4 v0, -0x1

    :goto_0
    sput v0, La/b/b/b/a;->a:I

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .line 177
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 178
    sget v0, La/b/b/b/a;->a:I

    iput v0, p0, La/b/b/b/a;->b:I

    return-void
.end method

.method public constructor <init>(I)V
    .locals 0

    .line 181
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 182
    iput p1, p0, La/b/b/b/a;->b:I

    return-void
.end method

.method private a()La/b/b/b/d;
    .locals 2

    .line 158
    iget-object v0, p0, La/b/b/b/a;->c:La/b/b/b/d;

    if-nez v0, :cond_0

    .line 159
    new-instance v0, La/b/b/b/d;

    iget v1, p0, La/b/b/b/a;->b:I

    invoke-direct {v0, v1}, La/b/b/b/d;-><init>(I)V

    iput-object v0, p0, La/b/b/b/a;->c:La/b/b/b/d;

    .line 160
    :cond_0
    iget-object v0, p0, La/b/b/b/a;->c:La/b/b/b/d;

    return-object v0
.end method


# virtual methods
.method public a(Ljava/lang/String;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            La/b/b/b/e;
        }
    .end annotation

    .line 263
    invoke-direct {p0}, La/b/b/b/a;->a()La/b/b/b/d;

    move-result-object v0

    invoke-virtual {v0, p1}, La/b/b/b/d;->b(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method
