.class La/b/b/c/d$16;
.super Ljava/lang/Object;
.source "JsonWriter.java"

# interfaces
.implements La/b/b/c/e;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = La/b/b/c/d;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "La/b/b/c/e<",
        "Ljava/util/Map<",
        "Ljava/lang/String;",
        "+",
        "Ljava/lang/Object;",
        ">;>;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .line 128
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Ljava/lang/Object;Ljava/lang/Appendable;La/b/b/g;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    check-cast p1, Ljava/util/Map;

    invoke-virtual {p0, p1, p2, p3}, La/b/b/c/d$16;->a(Ljava/util/Map;Ljava/lang/Appendable;La/b/b/g;)V

    return-void
.end method

.method public a(Ljava/util/Map;Ljava/lang/Appendable;La/b/b/g;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E::",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "+",
            "Ljava/lang/Object;",
            ">;>(TE;",
            "Ljava/lang/Appendable;",
            "La/b/b/g;",
            ")V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 131
    invoke-virtual {p3, p2}, La/b/b/g;->a(Ljava/lang/Appendable;)V

    .line 135
    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p1

    const/4 v0, 0x1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-nez v1, :cond_0

    .line 148
    invoke-virtual {p3, p2}, La/b/b/g;->b(Ljava/lang/Appendable;)V

    return-void

    .line 135
    :cond_0
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 136
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    if-nez v2, :cond_1

    .line 137
    invoke-virtual {p3}, La/b/b/g;->a()Z

    move-result v3

    if-eqz v3, :cond_1

    goto :goto_0

    :cond_1
    if-eqz v0, :cond_2

    .line 140
    invoke-virtual {p3, p2}, La/b/b/g;->c(Ljava/lang/Appendable;)V

    const/4 v0, 0x0

    goto :goto_1

    .line 143
    :cond_2
    invoke-virtual {p3, p2}, La/b/b/g;->d(Ljava/lang/Appendable;)V

    .line 145
    :goto_1
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v2, p2, p3}, La/b/b/c/d;->a(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Appendable;La/b/b/g;)V

    goto :goto_0
.end method
