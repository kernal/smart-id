.class La/b/b/c/d$10;
.super Ljava/lang/Object;
.source "JsonWriter.java"

# interfaces
.implements La/b/b/c/e;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = La/b/b/c/d;->a()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "La/b/b/c/e<",
        "[Z>;"
    }
.end annotation


# instance fields
.field final synthetic a:La/b/b/c/d;


# direct methods
.method constructor <init>(La/b/b/c/d;)V
    .locals 0

    .line 1
    iput-object p1, p0, La/b/b/c/d$10;->a:La/b/b/c/d;

    .line 293
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Ljava/lang/Object;Ljava/lang/Appendable;La/b/b/g;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    check-cast p1, [Z

    invoke-virtual {p0, p1, p2, p3}, La/b/b/c/d$10;->a([ZLjava/lang/Appendable;La/b/b/g;)V

    return-void
.end method

.method public a([ZLjava/lang/Appendable;La/b/b/g;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 296
    invoke-virtual {p3, p2}, La/b/b/g;->g(Ljava/lang/Appendable;)V

    .line 297
    array-length v0, p1

    const/4 v1, 0x0

    const/4 v2, 0x0

    :goto_0
    if-lt v1, v0, :cond_0

    .line 304
    invoke-virtual {p3, p2}, La/b/b/g;->h(Ljava/lang/Appendable;)V

    return-void

    .line 297
    :cond_0
    aget-boolean v3, p1, v1

    if-eqz v2, :cond_1

    .line 299
    invoke-virtual {p3, p2}, La/b/b/g;->d(Ljava/lang/Appendable;)V

    goto :goto_1

    :cond_1
    const/4 v2, 0x1

    .line 302
    :goto_1
    invoke-static {v3}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v3

    invoke-interface {p2, v3}, Ljava/lang/Appendable;->append(Ljava/lang/CharSequence;)Ljava/lang/Appendable;

    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method
