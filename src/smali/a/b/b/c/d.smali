.class public La/b/b/c/d;
.super Ljava/lang/Object;
.source "JsonWriter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        La/b/b/c/d$a;
    }
.end annotation


# static fields
.field public static final a:La/b/b/c/e;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "La/b/b/c/e<",
            "La/b/b/f;",
            ">;"
        }
    .end annotation
.end field

.field public static final b:La/b/b/c/e;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "La/b/b/c/e<",
            "La/b/b/f;",
            ">;"
        }
    .end annotation
.end field

.field public static final c:La/b/b/c/e;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "La/b/b/c/e<",
            "La/b/b/c;",
            ">;"
        }
    .end annotation
.end field

.field public static final d:La/b/b/c/e;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "La/b/b/c/e<",
            "La/b/b/b;",
            ">;"
        }
    .end annotation
.end field

.field public static final e:La/b/b/c/e;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "La/b/b/c/e<",
            "Ljava/lang/Iterable<",
            "+",
            "Ljava/lang/Object;",
            ">;>;"
        }
    .end annotation
.end field

.field public static final f:La/b/b/c/e;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "La/b/b/c/e<",
            "Ljava/lang/Enum<",
            "*>;>;"
        }
    .end annotation
.end field

.field public static final g:La/b/b/c/e;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "La/b/b/c/e<",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "+",
            "Ljava/lang/Object;",
            ">;>;"
        }
    .end annotation
.end field

.field public static final h:La/b/b/c/e;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "La/b/b/c/e<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field public static final i:La/b/b/c/e;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "La/b/b/c/e<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field public static final j:La/b/b/c/e;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "La/b/b/c/e<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field public static final k:La/b/b/c/e;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "La/b/b/c/e<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private l:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap<",
            "Ljava/lang/Class<",
            "*>;",
            "La/b/b/c/e<",
            "*>;>;"
        }
    .end annotation
.end field

.field private m:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList<",
            "La/b/b/c/d$a;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 75
    new-instance v0, La/b/b/c/d$1;

    invoke-direct {v0}, La/b/b/c/d$1;-><init>()V

    sput-object v0, La/b/b/c/d;->a:La/b/b/c/e;

    .line 81
    new-instance v0, La/b/b/c/d$11;

    invoke-direct {v0}, La/b/b/c/d$11;-><init>()V

    sput-object v0, La/b/b/c/d;->b:La/b/b/c/e;

    .line 87
    new-instance v0, La/b/b/c/d$12;

    invoke-direct {v0}, La/b/b/c/d$12;-><init>()V

    sput-object v0, La/b/b/c/d;->c:La/b/b/c/e;

    .line 93
    new-instance v0, La/b/b/c/d$13;

    invoke-direct {v0}, La/b/b/c/d$13;-><init>()V

    sput-object v0, La/b/b/c/d;->d:La/b/b/c/e;

    .line 99
    new-instance v0, La/b/b/c/d$14;

    invoke-direct {v0}, La/b/b/c/d$14;-><init>()V

    sput-object v0, La/b/b/c/d;->e:La/b/b/c/e;

    .line 120
    new-instance v0, La/b/b/c/d$15;

    invoke-direct {v0}, La/b/b/c/d$15;-><init>()V

    sput-object v0, La/b/b/c/d;->f:La/b/b/c/e;

    .line 128
    new-instance v0, La/b/b/c/d$16;

    invoke-direct {v0}, La/b/b/c/d$16;-><init>()V

    sput-object v0, La/b/b/c/d;->g:La/b/b/c/e;

    .line 157
    new-instance v0, La/b/b/c/c;

    invoke-direct {v0}, La/b/b/c/c;-><init>()V

    sput-object v0, La/b/b/c/d;->h:La/b/b/c/e;

    .line 162
    new-instance v0, La/b/b/c/b;

    invoke-direct {v0}, La/b/b/c/b;-><init>()V

    sput-object v0, La/b/b/c/d;->i:La/b/b/c/e;

    .line 167
    new-instance v0, La/b/b/c/a;

    invoke-direct {v0}, La/b/b/c/a;-><init>()V

    sput-object v0, La/b/b/c/d;->j:La/b/b/c/e;

    .line 172
    new-instance v0, La/b/b/c/d$17;

    invoke-direct {v0}, La/b/b/c/d$17;-><init>()V

    sput-object v0, La/b/b/c/d;->k:La/b/b/c/e;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, La/b/b/c/d;->l:Ljava/util/concurrent/ConcurrentHashMap;

    .line 23
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, La/b/b/c/d;->m:Ljava/util/LinkedList;

    .line 24
    invoke-virtual {p0}, La/b/b/c/d;->a()V

    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Appendable;La/b/b/g;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    if-nez p0, :cond_0

    const-string p0, "null"

    .line 380
    invoke-interface {p2, p0}, Ljava/lang/Appendable;->append(Ljava/lang/CharSequence;)Ljava/lang/Appendable;

    goto :goto_0

    .line 381
    :cond_0
    invoke-virtual {p3, p0}, La/b/b/g;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 382
    invoke-interface {p2, p0}, Ljava/lang/Appendable;->append(Ljava/lang/CharSequence;)Ljava/lang/Appendable;

    goto :goto_0

    :cond_1
    const/16 v0, 0x22

    .line 384
    invoke-interface {p2, v0}, Ljava/lang/Appendable;->append(C)Ljava/lang/Appendable;

    .line 385
    invoke-static {p0, p2, p3}, La/b/b/i;->a(Ljava/lang/String;Ljava/lang/Appendable;La/b/b/g;)V

    .line 386
    invoke-interface {p2, v0}, Ljava/lang/Appendable;->append(C)Ljava/lang/Appendable;

    .line 388
    :goto_0
    invoke-virtual {p3, p2}, La/b/b/g;->f(Ljava/lang/Appendable;)V

    .line 389
    instance-of p0, p1, Ljava/lang/String;

    if-eqz p0, :cond_2

    .line 390
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p3, p2, p1}, La/b/b/g;->a(Ljava/lang/Appendable;Ljava/lang/String;)V

    goto :goto_1

    .line 392
    :cond_2
    invoke-static {p1, p2, p3}, La/b/b/i;->a(Ljava/lang/Object;Ljava/lang/Appendable;La/b/b/g;)V

    .line 393
    :goto_1
    invoke-virtual {p3, p2}, La/b/b/g;->e(Ljava/lang/Appendable;)V

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Class;)La/b/b/c/e;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class<",
            "*>;)",
            "La/b/b/c/e;"
        }
    .end annotation

    .line 63
    iget-object v0, p0, La/b/b/c/d;->m:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-nez v1, :cond_1

    const/4 p1, 0x0

    return-object p1

    :cond_1
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, La/b/b/c/d$a;

    .line 64
    iget-object v2, v1, La/b/b/c/d$a;->a:Ljava/lang/Class;

    invoke-virtual {v2, p1}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 65
    iget-object p1, v1, La/b/b/c/d$a;->b:La/b/b/c/e;

    return-object p1
.end method

.method public a()V
    .locals 6

    .line 179
    new-instance v0, La/b/b/c/d$18;

    invoke-direct {v0, p0}, La/b/b/c/d$18;-><init>(La/b/b/c/d;)V

    const/4 v1, 0x1

    new-array v2, v1, [Ljava/lang/Class;

    .line 183
    const-class v3, Ljava/lang/String;

    const/4 v4, 0x0

    aput-object v3, v2, v4

    .line 179
    invoke-virtual {p0, v0, v2}, La/b/b/c/d;->a(La/b/b/c/e;[Ljava/lang/Class;)V

    .line 185
    new-instance v0, La/b/b/c/d$2;

    invoke-direct {v0, p0}, La/b/b/c/d$2;-><init>(La/b/b/c/d;)V

    new-array v2, v1, [Ljava/lang/Class;

    .line 192
    const-class v3, Ljava/lang/Double;

    aput-object v3, v2, v4

    .line 185
    invoke-virtual {p0, v0, v2}, La/b/b/c/d;->a(La/b/b/c/e;[Ljava/lang/Class;)V

    .line 194
    new-instance v0, La/b/b/c/d$3;

    invoke-direct {v0, p0}, La/b/b/c/d$3;-><init>(La/b/b/c/d;)V

    new-array v2, v1, [Ljava/lang/Class;

    .line 200
    const-class v3, Ljava/util/Date;

    aput-object v3, v2, v4

    .line 194
    invoke-virtual {p0, v0, v2}, La/b/b/c/d;->a(La/b/b/c/e;[Ljava/lang/Class;)V

    .line 202
    new-instance v0, La/b/b/c/d$4;

    invoke-direct {v0, p0}, La/b/b/c/d$4;-><init>(La/b/b/c/d;)V

    new-array v2, v1, [Ljava/lang/Class;

    .line 209
    const-class v3, Ljava/lang/Float;

    aput-object v3, v2, v4

    .line 202
    invoke-virtual {p0, v0, v2}, La/b/b/c/d;->a(La/b/b/c/e;[Ljava/lang/Class;)V

    .line 211
    sget-object v0, La/b/b/c/d;->k:La/b/b/c/e;

    const/4 v2, 0x6

    new-array v2, v2, [Ljava/lang/Class;

    const-class v3, Ljava/lang/Integer;

    aput-object v3, v2, v4

    const-class v3, Ljava/lang/Long;

    aput-object v3, v2, v1

    const-class v3, Ljava/lang/Byte;

    const/4 v5, 0x2

    aput-object v3, v2, v5

    const-class v3, Ljava/lang/Short;

    const/4 v5, 0x3

    aput-object v3, v2, v5

    const-class v3, Ljava/math/BigInteger;

    const/4 v5, 0x4

    aput-object v3, v2, v5

    const-class v3, Ljava/math/BigDecimal;

    const/4 v5, 0x5

    aput-object v3, v2, v5

    invoke-virtual {p0, v0, v2}, La/b/b/c/d;->a(La/b/b/c/e;[Ljava/lang/Class;)V

    .line 212
    sget-object v0, La/b/b/c/d;->k:La/b/b/c/e;

    new-array v2, v1, [Ljava/lang/Class;

    const-class v3, Ljava/lang/Boolean;

    aput-object v3, v2, v4

    invoke-virtual {p0, v0, v2}, La/b/b/c/d;->a(La/b/b/c/e;[Ljava/lang/Class;)V

    .line 218
    new-instance v0, La/b/b/c/d$5;

    invoke-direct {v0, p0}, La/b/b/c/d$5;-><init>(La/b/b/c/d;)V

    new-array v2, v1, [Ljava/lang/Class;

    .line 231
    const-class v3, [I

    aput-object v3, v2, v4

    .line 218
    invoke-virtual {p0, v0, v2}, La/b/b/c/d;->a(La/b/b/c/e;[Ljava/lang/Class;)V

    .line 233
    new-instance v0, La/b/b/c/d$6;

    invoke-direct {v0, p0}, La/b/b/c/d$6;-><init>(La/b/b/c/d;)V

    new-array v2, v1, [Ljava/lang/Class;

    .line 246
    const-class v3, [S

    aput-object v3, v2, v4

    .line 233
    invoke-virtual {p0, v0, v2}, La/b/b/c/d;->a(La/b/b/c/e;[Ljava/lang/Class;)V

    .line 248
    new-instance v0, La/b/b/c/d$7;

    invoke-direct {v0, p0}, La/b/b/c/d$7;-><init>(La/b/b/c/d;)V

    new-array v2, v1, [Ljava/lang/Class;

    .line 261
    const-class v3, [J

    aput-object v3, v2, v4

    .line 248
    invoke-virtual {p0, v0, v2}, La/b/b/c/d;->a(La/b/b/c/e;[Ljava/lang/Class;)V

    .line 263
    new-instance v0, La/b/b/c/d$8;

    invoke-direct {v0, p0}, La/b/b/c/d$8;-><init>(La/b/b/c/d;)V

    new-array v2, v1, [Ljava/lang/Class;

    .line 276
    const-class v3, [F

    aput-object v3, v2, v4

    .line 263
    invoke-virtual {p0, v0, v2}, La/b/b/c/d;->a(La/b/b/c/e;[Ljava/lang/Class;)V

    .line 278
    new-instance v0, La/b/b/c/d$9;

    invoke-direct {v0, p0}, La/b/b/c/d$9;-><init>(La/b/b/c/d;)V

    new-array v2, v1, [Ljava/lang/Class;

    .line 291
    const-class v3, [D

    aput-object v3, v2, v4

    .line 278
    invoke-virtual {p0, v0, v2}, La/b/b/c/d;->a(La/b/b/c/e;[Ljava/lang/Class;)V

    .line 293
    new-instance v0, La/b/b/c/d$10;

    invoke-direct {v0, p0}, La/b/b/c/d$10;-><init>(La/b/b/c/d;)V

    new-array v1, v1, [Ljava/lang/Class;

    .line 306
    const-class v2, [Z

    aput-object v2, v1, v4

    .line 293
    invoke-virtual {p0, v0, v1}, La/b/b/c/d;->a(La/b/b/c/e;[Ljava/lang/Class;)V

    .line 308
    const-class v0, La/b/b/f;

    sget-object v1, La/b/b/c/d;->b:La/b/b/c/e;

    invoke-virtual {p0, v0, v1}, La/b/b/c/d;->b(Ljava/lang/Class;La/b/b/c/e;)V

    .line 309
    const-class v0, La/b/b/e;

    sget-object v1, La/b/b/c/d;->a:La/b/b/c/e;

    invoke-virtual {p0, v0, v1}, La/b/b/c/d;->b(Ljava/lang/Class;La/b/b/c/e;)V

    .line 310
    const-class v0, La/b/b/c;

    sget-object v1, La/b/b/c/d;->c:La/b/b/c/e;

    invoke-virtual {p0, v0, v1}, La/b/b/c/d;->b(Ljava/lang/Class;La/b/b/c/e;)V

    .line 311
    const-class v0, La/b/b/b;

    sget-object v1, La/b/b/c/d;->d:La/b/b/c/e;

    invoke-virtual {p0, v0, v1}, La/b/b/c/d;->b(Ljava/lang/Class;La/b/b/c/e;)V

    .line 312
    const-class v0, Ljava/util/Map;

    sget-object v1, La/b/b/c/d;->g:La/b/b/c/e;

    invoke-virtual {p0, v0, v1}, La/b/b/c/d;->b(Ljava/lang/Class;La/b/b/c/e;)V

    .line 313
    const-class v0, Ljava/lang/Iterable;

    sget-object v1, La/b/b/c/d;->e:La/b/b/c/e;

    invoke-virtual {p0, v0, v1}, La/b/b/c/d;->b(Ljava/lang/Class;La/b/b/c/e;)V

    .line 314
    const-class v0, Ljava/lang/Enum;

    sget-object v1, La/b/b/c/d;->f:La/b/b/c/e;

    invoke-virtual {p0, v0, v1}, La/b/b/c/d;->b(Ljava/lang/Class;La/b/b/c/e;)V

    .line 315
    const-class v0, Ljava/lang/Number;

    sget-object v1, La/b/b/c/d;->k:La/b/b/c/e;

    invoke-virtual {p0, v0, v1}, La/b/b/c/d;->b(Ljava/lang/Class;La/b/b/c/e;)V

    return-void
.end method

.method public varargs a(La/b/b/c/e;[Ljava/lang/Class;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "La/b/b/c/e<",
            "TT;>;[",
            "Ljava/lang/Class<",
            "*>;)V"
        }
    .end annotation

    .line 371
    array-length v0, p2

    const/4 v1, 0x0

    :goto_0
    if-lt v1, v0, :cond_0

    return-void

    :cond_0
    aget-object v2, p2, v1

    .line 372
    iget-object v3, p0, La/b/b/c/d;->l:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v3, v2, p1}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public a(Ljava/lang/Class;La/b/b/c/e;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class<",
            "*>;",
            "La/b/b/c/e<",
            "*>;)V"
        }
    .end annotation

    .line 344
    iget-object v0, p0, La/b/b/c/d;->m:Ljava/util/LinkedList;

    new-instance v1, La/b/b/c/d$a;

    invoke-direct {v1, p1, p2}, La/b/b/c/d$a;-><init>(Ljava/lang/Class;La/b/b/c/e;)V

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->addLast(Ljava/lang/Object;)V

    return-void
.end method

.method public b(Ljava/lang/Class;)La/b/b/c/e;
    .locals 1

    .line 72
    iget-object v0, p0, La/b/b/c/d;->l:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, La/b/b/c/e;

    return-object p1
.end method

.method public b(Ljava/lang/Class;La/b/b/c/e;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class<",
            "*>;",
            "La/b/b/c/e<",
            "*>;)V"
        }
    .end annotation

    .line 362
    invoke-virtual {p0, p1, p2}, La/b/b/c/d;->a(Ljava/lang/Class;La/b/b/c/e;)V

    return-void
.end method
