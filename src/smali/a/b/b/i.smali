.class public La/b/b/i;
.super Ljava/lang/Object;
.source "JSONValue.java"


# static fields
.field public static a:La/b/b/g;

.field public static final b:La/b/b/c/d;

.field public static final c:La/b/b/d/e;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 51
    sget-object v0, La/b/b/g;->a:La/b/b/g;

    sput-object v0, La/b/b/i;->a:La/b/b/g;

    .line 527
    new-instance v0, La/b/b/c/d;

    invoke-direct {v0}, La/b/b/c/d;-><init>()V

    sput-object v0, La/b/b/i;->b:La/b/b/c/d;

    .line 531
    new-instance v0, La/b/b/d/e;

    invoke-direct {v0}, La/b/b/d/e;-><init>()V

    sput-object v0, La/b/b/i;->c:La/b/b/d/e;

    return-void
.end method

.method public static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .line 630
    sget-object v0, La/b/b/i;->a:La/b/b/g;

    invoke-static {p0, v0}, La/b/b/i;->a(Ljava/lang/String;La/b/b/g;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static a(Ljava/lang/String;La/b/b/g;)Ljava/lang/String;
    .locals 1

    if-nez p0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 640
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 641
    invoke-virtual {p1, p0, v0}, La/b/b/g;->a(Ljava/lang/String;Ljava/lang/Appendable;)V

    .line 642
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static a(Ljava/lang/Object;Ljava/lang/Appendable;La/b/b/g;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    if-nez p0, :cond_0

    const-string p0, "null"

    .line 569
    invoke-interface {p1, p0}, Ljava/lang/Appendable;->append(Ljava/lang/CharSequence;)Ljava/lang/Appendable;

    return-void

    .line 572
    :cond_0
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    .line 574
    sget-object v1, La/b/b/i;->b:La/b/b/c/d;

    invoke-virtual {v1, v0}, La/b/b/c/d;->b(Ljava/lang/Class;)La/b/b/c/e;

    move-result-object v1

    if-nez v1, :cond_3

    .line 576
    invoke-virtual {v0}, Ljava/lang/Class;->isArray()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 577
    sget-object v1, La/b/b/c/d;->j:La/b/b/c/e;

    goto :goto_0

    .line 579
    :cond_1
    sget-object v1, La/b/b/i;->b:La/b/b/c/d;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v1, v2}, La/b/b/c/d;->a(Ljava/lang/Class;)La/b/b/c/e;

    move-result-object v1

    if-nez v1, :cond_2

    .line 581
    sget-object v1, La/b/b/c/d;->h:La/b/b/c/e;

    .line 584
    :cond_2
    :goto_0
    sget-object v2, La/b/b/i;->b:La/b/b/c/d;

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Class;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-virtual {v2, v1, v3}, La/b/b/c/d;->a(La/b/b/c/e;[Ljava/lang/Class;)V

    .line 586
    :cond_3
    invoke-interface {v1, p0, p1, p2}, La/b/b/c/e;->a(Ljava/lang/Object;Ljava/lang/Appendable;La/b/b/g;)V

    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/Appendable;La/b/b/g;)V
    .locals 0

    if-nez p0, :cond_0

    return-void

    .line 652
    :cond_0
    invoke-virtual {p2, p0, p1}, La/b/b/g;->a(Ljava/lang/String;Ljava/lang/Appendable;)V

    return-void
.end method
