.class public La/b/b/d/a;
.super La/b/b/d/f;
.source "ArraysMapper.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "La/b/b/d/f<",
        "TT;>;"
    }
.end annotation


# static fields
.field public static a:La/b/b/d/f;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "La/b/b/d/f<",
            "[I>;"
        }
    .end annotation
.end field

.field public static b:La/b/b/d/f;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "La/b/b/d/f<",
            "[",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static c:La/b/b/d/f;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "La/b/b/d/f<",
            "[S>;"
        }
    .end annotation
.end field

.field public static d:La/b/b/d/f;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "La/b/b/d/f<",
            "[",
            "Ljava/lang/Short;",
            ">;"
        }
    .end annotation
.end field

.field public static e:La/b/b/d/f;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "La/b/b/d/f<",
            "[B>;"
        }
    .end annotation
.end field

.field public static f:La/b/b/d/f;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "La/b/b/d/f<",
            "[",
            "Ljava/lang/Byte;",
            ">;"
        }
    .end annotation
.end field

.field public static g:La/b/b/d/f;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "La/b/b/d/f<",
            "[C>;"
        }
    .end annotation
.end field

.field public static h:La/b/b/d/f;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "La/b/b/d/f<",
            "[",
            "Ljava/lang/Character;",
            ">;"
        }
    .end annotation
.end field

.field public static i:La/b/b/d/f;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "La/b/b/d/f<",
            "[J>;"
        }
    .end annotation
.end field

.field public static j:La/b/b/d/f;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "La/b/b/d/f<",
            "[",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public static k:La/b/b/d/f;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "La/b/b/d/f<",
            "[F>;"
        }
    .end annotation
.end field

.field public static l:La/b/b/d/f;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "La/b/b/d/f<",
            "[",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field public static m:La/b/b/d/f;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "La/b/b/d/f<",
            "[D>;"
        }
    .end annotation
.end field

.field public static n:La/b/b/d/f;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "La/b/b/d/f<",
            "[",
            "Ljava/lang/Double;",
            ">;"
        }
    .end annotation
.end field

.field public static o:La/b/b/d/f;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "La/b/b/d/f<",
            "[Z>;"
        }
    .end annotation
.end field

.field public static p:La/b/b/d/f;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "La/b/b/d/f<",
            "[",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 79
    new-instance v0, La/b/b/d/a$1;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, La/b/b/d/a$1;-><init>(La/b/b/d/e;)V

    sput-object v0, La/b/b/d/a;->a:La/b/b/d/f;

    .line 90
    new-instance v0, La/b/b/d/a$9;

    invoke-direct {v0, v1}, La/b/b/d/a$9;-><init>(La/b/b/d/e;)V

    sput-object v0, La/b/b/d/a;->b:La/b/b/d/f;

    .line 108
    new-instance v0, La/b/b/d/a$10;

    invoke-direct {v0, v1}, La/b/b/d/a$10;-><init>(La/b/b/d/e;)V

    sput-object v0, La/b/b/d/a;->c:La/b/b/d/f;

    .line 119
    new-instance v0, La/b/b/d/a$11;

    invoke-direct {v0, v1}, La/b/b/d/a$11;-><init>(La/b/b/d/e;)V

    sput-object v0, La/b/b/d/a;->d:La/b/b/d/f;

    .line 137
    new-instance v0, La/b/b/d/a$12;

    invoke-direct {v0, v1}, La/b/b/d/a$12;-><init>(La/b/b/d/e;)V

    sput-object v0, La/b/b/d/a;->e:La/b/b/d/f;

    .line 148
    new-instance v0, La/b/b/d/a$13;

    invoke-direct {v0, v1}, La/b/b/d/a$13;-><init>(La/b/b/d/e;)V

    sput-object v0, La/b/b/d/a;->f:La/b/b/d/f;

    .line 166
    new-instance v0, La/b/b/d/a$14;

    invoke-direct {v0, v1}, La/b/b/d/a$14;-><init>(La/b/b/d/e;)V

    sput-object v0, La/b/b/d/a;->g:La/b/b/d/f;

    .line 177
    new-instance v0, La/b/b/d/a$15;

    invoke-direct {v0, v1}, La/b/b/d/a$15;-><init>(La/b/b/d/e;)V

    sput-object v0, La/b/b/d/a;->h:La/b/b/d/f;

    .line 192
    new-instance v0, La/b/b/d/a$16;

    invoke-direct {v0, v1}, La/b/b/d/a$16;-><init>(La/b/b/d/e;)V

    sput-object v0, La/b/b/d/a;->i:La/b/b/d/f;

    .line 203
    new-instance v0, La/b/b/d/a$2;

    invoke-direct {v0, v1}, La/b/b/d/a$2;-><init>(La/b/b/d/e;)V

    sput-object v0, La/b/b/d/a;->j:La/b/b/d/f;

    .line 221
    new-instance v0, La/b/b/d/a$3;

    invoke-direct {v0, v1}, La/b/b/d/a$3;-><init>(La/b/b/d/e;)V

    sput-object v0, La/b/b/d/a;->k:La/b/b/d/f;

    .line 232
    new-instance v0, La/b/b/d/a$4;

    invoke-direct {v0, v1}, La/b/b/d/a$4;-><init>(La/b/b/d/e;)V

    sput-object v0, La/b/b/d/a;->l:La/b/b/d/f;

    .line 250
    new-instance v0, La/b/b/d/a$5;

    invoke-direct {v0, v1}, La/b/b/d/a$5;-><init>(La/b/b/d/e;)V

    sput-object v0, La/b/b/d/a;->m:La/b/b/d/f;

    .line 261
    new-instance v0, La/b/b/d/a$6;

    invoke-direct {v0, v1}, La/b/b/d/a$6;-><init>(La/b/b/d/e;)V

    sput-object v0, La/b/b/d/a;->n:La/b/b/d/f;

    .line 279
    new-instance v0, La/b/b/d/a$7;

    invoke-direct {v0, v1}, La/b/b/d/a$7;-><init>(La/b/b/d/e;)V

    sput-object v0, La/b/b/d/a;->o:La/b/b/d/f;

    .line 290
    new-instance v0, La/b/b/d/a$8;

    invoke-direct {v0, v1}, La/b/b/d/a$8;-><init>(La/b/b/d/e;)V

    sput-object v0, La/b/b/d/a;->p:La/b/b/d/f;

    return-void
.end method

.method public constructor <init>(La/b/b/d/e;)V
    .locals 0

    .line 24
    invoke-direct {p0, p1}, La/b/b/d/f;-><init>(La/b/b/d/e;)V

    return-void
.end method


# virtual methods
.method public a()Ljava/lang/Object;
    .locals 1

    .line 29
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    return-object v0
.end method

.method public a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            ")TT;"
        }
    .end annotation

    return-object p1
.end method

.method public a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    .line 35
    check-cast p1, Ljava/util/List;

    invoke-interface {p1, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method
