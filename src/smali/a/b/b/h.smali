.class public La/b/b/h;
.super Ljava/lang/Object;
.source "JSONUtil.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        La/b/b/h$a;
    }
.end annotation


# static fields
.field public static final a:La/b/b/h$a;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 185
    new-instance v0, La/b/b/h$a;

    invoke-direct {v0}, La/b/b/h$a;-><init>()V

    sput-object v0, La/b/b/h;->a:La/b/b/h$a;

    return-void
.end method

.method public static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 6

    .line 232
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    add-int/lit8 v1, v0, 0x3

    .line 233
    new-array v1, v1, [C

    const/4 v2, 0x0

    const/16 v3, 0x67

    .line 234
    aput-char v3, v1, v2

    const/4 v3, 0x1

    const/16 v4, 0x65

    .line 235
    aput-char v4, v1, v3

    const/4 v4, 0x2

    const/16 v5, 0x74

    .line 236
    aput-char v5, v1, v4

    .line 237
    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v2

    const/16 v4, 0x61

    if-lt v2, v4, :cond_0

    const/16 v4, 0x7a

    if-gt v2, v4, :cond_0

    add-int/lit8 v2, v2, -0x20

    int-to-char v2, v2

    :cond_0
    const/4 v4, 0x3

    .line 240
    aput-char v2, v1, v4

    :goto_0
    if-lt v3, v0, :cond_1

    .line 244
    new-instance p0, Ljava/lang/String;

    invoke-direct {p0, v1}, Ljava/lang/String;-><init>([C)V

    return-object p0

    :cond_1
    add-int/lit8 v2, v3, 0x3

    .line 242
    invoke-virtual {p0, v3}, Ljava/lang/String;->charAt(I)C

    move-result v4

    aput-char v4, v1, v2

    add-int/lit8 v3, v3, 0x1

    goto :goto_0
.end method

.method public static b(Ljava/lang/String;)Ljava/lang/String;
    .locals 5

    .line 248
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    add-int/lit8 v1, v0, 0x2

    .line 249
    new-array v1, v1, [C

    const/4 v2, 0x0

    const/16 v3, 0x69

    .line 250
    aput-char v3, v1, v2

    const/4 v3, 0x1

    const/16 v4, 0x73

    .line 251
    aput-char v4, v1, v3

    .line 252
    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v2

    const/16 v4, 0x61

    if-lt v2, v4, :cond_0

    const/16 v4, 0x7a

    if-gt v2, v4, :cond_0

    add-int/lit8 v2, v2, -0x20

    int-to-char v2, v2

    :cond_0
    const/4 v4, 0x2

    .line 255
    aput-char v2, v1, v4

    :goto_0
    if-lt v3, v0, :cond_1

    .line 259
    new-instance p0, Ljava/lang/String;

    invoke-direct {p0, v1}, Ljava/lang/String;-><init>([C)V

    return-object p0

    :cond_1
    add-int/lit8 v2, v3, 0x2

    .line 257
    invoke-virtual {p0, v3}, Ljava/lang/String;->charAt(I)C

    move-result v4

    aput-char v4, v1, v2

    add-int/lit8 v3, v3, 0x1

    goto :goto_0
.end method
