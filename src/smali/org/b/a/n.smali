.class public final Lorg/b/a/n;
.super Lorg/b/a/a/g;
.source "LocalDate.java"

# interfaces
.implements Ljava/io/Serializable;
.implements Lorg/b/a/v;


# static fields
.field private static final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Lorg/b/a/i;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final b:J

.field private final c:Lorg/b/a/a;

.field private transient d:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 96
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, Lorg/b/a/n;->a:Ljava/util/Set;

    .line 98
    sget-object v0, Lorg/b/a/n;->a:Ljava/util/Set;

    invoke-static {}, Lorg/b/a/i;->f()Lorg/b/a/i;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 99
    sget-object v0, Lorg/b/a/n;->a:Ljava/util/Set;

    invoke-static {}, Lorg/b/a/i;->g()Lorg/b/a/i;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 100
    sget-object v0, Lorg/b/a/n;->a:Ljava/util/Set;

    invoke-static {}, Lorg/b/a/i;->i()Lorg/b/a/i;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 101
    sget-object v0, Lorg/b/a/n;->a:Ljava/util/Set;

    invoke-static {}, Lorg/b/a/i;->h()Lorg/b/a/i;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 102
    sget-object v0, Lorg/b/a/n;->a:Ljava/util/Set;

    invoke-static {}, Lorg/b/a/i;->j()Lorg/b/a/i;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 103
    sget-object v0, Lorg/b/a/n;->a:Ljava/util/Set;

    invoke-static {}, Lorg/b/a/i;->k()Lorg/b/a/i;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 105
    sget-object v0, Lorg/b/a/n;->a:Ljava/util/Set;

    invoke-static {}, Lorg/b/a/i;->l()Lorg/b/a/i;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .line 267
    invoke-static {}, Lorg/b/a/e;->a()J

    move-result-wide v0

    invoke-static {}, Lorg/b/a/b/u;->O()Lorg/b/a/b/u;

    move-result-object v2

    invoke-direct {p0, v0, v1, v2}, Lorg/b/a/n;-><init>(JLorg/b/a/a;)V

    return-void
.end method

.method public constructor <init>(JLorg/b/a/a;)V
    .locals 2

    .line 335
    invoke-direct {p0}, Lorg/b/a/a/g;-><init>()V

    .line 336
    invoke-static {p3}, Lorg/b/a/e;->a(Lorg/b/a/a;)Lorg/b/a/a;

    move-result-object p3

    .line 338
    invoke-virtual {p3}, Lorg/b/a/a;->a()Lorg/b/a/f;

    move-result-object v0

    sget-object v1, Lorg/b/a/f;->a:Lorg/b/a/f;

    invoke-virtual {v0, v1, p1, p2}, Lorg/b/a/f;->a(Lorg/b/a/f;J)J

    move-result-wide p1

    .line 339
    invoke-virtual {p3}, Lorg/b/a/a;->b()Lorg/b/a/a;

    move-result-object p3

    .line 340
    invoke-virtual {p3}, Lorg/b/a/a;->u()Lorg/b/a/c;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lorg/b/a/c;->d(J)J

    move-result-wide p1

    iput-wide p1, p0, Lorg/b/a/n;->b:J

    .line 341
    iput-object p3, p0, Lorg/b/a/n;->c:Lorg/b/a/a;

    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    const/4 v0, 0x3

    return v0
.end method

.method public a(I)I
    .locals 3

    if-eqz p1, :cond_2

    const/4 v0, 0x1

    if-eq p1, v0, :cond_1

    const/4 v0, 0x2

    if-ne p1, v0, :cond_0

    .line 530
    invoke-virtual {p0}, Lorg/b/a/n;->c()Lorg/b/a/a;

    move-result-object p1

    invoke-virtual {p1}, Lorg/b/a/a;->u()Lorg/b/a/c;

    move-result-object p1

    invoke-virtual {p0}, Lorg/b/a/n;->b()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Lorg/b/a/c;->a(J)I

    move-result p1

    return p1

    .line 532
    :cond_0
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid index: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 528
    :cond_1
    invoke-virtual {p0}, Lorg/b/a/n;->c()Lorg/b/a/a;

    move-result-object p1

    invoke-virtual {p1}, Lorg/b/a/a;->C()Lorg/b/a/c;

    move-result-object p1

    invoke-virtual {p0}, Lorg/b/a/n;->b()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Lorg/b/a/c;->a(J)I

    move-result p1

    return p1

    .line 526
    :cond_2
    invoke-virtual {p0}, Lorg/b/a/n;->c()Lorg/b/a/a;

    move-result-object p1

    invoke-virtual {p1}, Lorg/b/a/a;->E()Lorg/b/a/c;

    move-result-object p1

    invoke-virtual {p0}, Lorg/b/a/n;->b()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Lorg/b/a/c;->a(J)I

    move-result p1

    return p1
.end method

.method public a(Lorg/b/a/d;)I
    .locals 3

    if-eqz p1, :cond_1

    .line 555
    invoke-virtual {p0, p1}, Lorg/b/a/n;->b(Lorg/b/a/d;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 558
    invoke-virtual {p0}, Lorg/b/a/n;->c()Lorg/b/a/a;

    move-result-object v0

    invoke-virtual {p1, v0}, Lorg/b/a/d;->a(Lorg/b/a/a;)Lorg/b/a/c;

    move-result-object p1

    invoke-virtual {p0}, Lorg/b/a/n;->b()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Lorg/b/a/c;->a(J)I

    move-result p1

    return p1

    .line 556
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Field \'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string p1, "\' is not supported"

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 553
    :cond_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "The DateTimeFieldType must not be null"

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public a(Lorg/b/a/v;)I
    .locals 6

    const/4 v0, 0x0

    if-ne p0, p1, :cond_0

    return v0

    .line 679
    :cond_0
    instance-of v1, p1, Lorg/b/a/n;

    if-eqz v1, :cond_3

    .line 680
    move-object v1, p1

    check-cast v1, Lorg/b/a/n;

    .line 681
    iget-object v2, p0, Lorg/b/a/n;->c:Lorg/b/a/a;

    iget-object v3, v1, Lorg/b/a/n;->c:Lorg/b/a/a;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 682
    iget-wide v2, p0, Lorg/b/a/n;->b:J

    iget-wide v4, v1, Lorg/b/a/n;->b:J

    cmp-long p1, v2, v4

    if-gez p1, :cond_1

    const/4 v0, -0x1

    goto :goto_0

    :cond_1
    cmp-long p1, v2, v4

    if-nez p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x1

    :goto_0
    return v0

    .line 687
    :cond_3
    invoke-super {p0, p1}, Lorg/b/a/a/g;->a(Lorg/b/a/v;)I

    move-result p1

    return p1
.end method

.method protected a(ILorg/b/a/a;)Lorg/b/a/c;
    .locals 2

    if-eqz p1, :cond_2

    const/4 v0, 0x1

    if-eq p1, v0, :cond_1

    const/4 v0, 0x2

    if-ne p1, v0, :cond_0

    .line 505
    invoke-virtual {p2}, Lorg/b/a/a;->u()Lorg/b/a/c;

    move-result-object p1

    return-object p1

    .line 507
    :cond_0
    new-instance p2, Ljava/lang/IndexOutOfBoundsException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Invalid index: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw p2

    .line 503
    :cond_1
    invoke-virtual {p2}, Lorg/b/a/a;->C()Lorg/b/a/c;

    move-result-object p1

    return-object p1

    .line 501
    :cond_2
    invoke-virtual {p2}, Lorg/b/a/a;->E()Lorg/b/a/c;

    move-result-object p1

    return-object p1
.end method

.method protected b()J
    .locals 2

    .line 610
    iget-wide v0, p0, Lorg/b/a/n;->b:J

    return-wide v0
.end method

.method public b(Lorg/b/a/d;)Z
    .locals 6

    const/4 v0, 0x0

    if-nez p1, :cond_0

    return v0

    .line 573
    :cond_0
    invoke-virtual {p1}, Lorg/b/a/d;->y()Lorg/b/a/i;

    move-result-object v1

    .line 574
    sget-object v2, Lorg/b/a/n;->a:Ljava/util/Set;

    invoke-interface {v2, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 575
    invoke-virtual {p0}, Lorg/b/a/n;->c()Lorg/b/a/a;

    move-result-object v2

    invoke-virtual {v1, v2}, Lorg/b/a/i;->a(Lorg/b/a/a;)Lorg/b/a/h;

    move-result-object v1

    invoke-virtual {v1}, Lorg/b/a/h;->d()J

    move-result-wide v1

    .line 576
    invoke-virtual {p0}, Lorg/b/a/n;->c()Lorg/b/a/a;

    move-result-object v3

    invoke-virtual {v3}, Lorg/b/a/a;->s()Lorg/b/a/h;

    move-result-object v3

    invoke-virtual {v3}, Lorg/b/a/h;->d()J

    move-result-wide v3

    cmp-long v5, v1, v3

    if-ltz v5, :cond_1

    goto :goto_0

    :cond_1
    return v0

    .line 577
    :cond_2
    :goto_0
    invoke-virtual {p0}, Lorg/b/a/n;->c()Lorg/b/a/a;

    move-result-object v0

    invoke-virtual {p1, v0}, Lorg/b/a/d;->a(Lorg/b/a/a;)Lorg/b/a/c;

    move-result-object p1

    invoke-virtual {p1}, Lorg/b/a/c;->c()Z

    move-result p1

    return p1
.end method

.method public c()Lorg/b/a/a;
    .locals 1

    .line 619
    iget-object v0, p0, Lorg/b/a/n;->c:Lorg/b/a/a;

    return-object v0
.end method

.method public synthetic compareTo(Ljava/lang/Object;)I
    .locals 0

    .line 82
    check-cast p1, Lorg/b/a/v;

    invoke-virtual {p0, p1}, Lorg/b/a/n;->a(Lorg/b/a/v;)I

    move-result p1

    return p1
.end method

.method public d()I
    .locals 3

    .line 1475
    invoke-virtual {p0}, Lorg/b/a/n;->c()Lorg/b/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lorg/b/a/a;->E()Lorg/b/a/c;

    move-result-object v0

    invoke-virtual {p0}, Lorg/b/a/n;->b()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lorg/b/a/c;->a(J)I

    move-result v0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 6

    const/4 v0, 0x1

    if-ne p0, p1, :cond_0

    return v0

    .line 635
    :cond_0
    instance-of v1, p1, Lorg/b/a/n;

    if-eqz v1, :cond_2

    .line 636
    move-object v1, p1

    check-cast v1, Lorg/b/a/n;

    .line 637
    iget-object v2, p0, Lorg/b/a/n;->c:Lorg/b/a/a;

    iget-object v3, v1, Lorg/b/a/n;->c:Lorg/b/a/a;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 638
    iget-wide v2, p0, Lorg/b/a/n;->b:J

    iget-wide v4, v1, Lorg/b/a/n;->b:J

    cmp-long p1, v2, v4

    if-nez p1, :cond_1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 641
    :cond_2
    invoke-super {p0, p1}, Lorg/b/a/a/g;->equals(Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method public hashCode()I
    .locals 1

    .line 651
    iget v0, p0, Lorg/b/a/n;->d:I

    if-nez v0, :cond_0

    .line 653
    invoke-super {p0}, Lorg/b/a/a/g;->hashCode()I

    move-result v0

    iput v0, p0, Lorg/b/a/n;->d:I

    :cond_0
    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1
    .annotation runtime Lorg/joda/convert/ToString;
    .end annotation

    .line 1832
    invoke-static {}, Lorg/b/a/e/j;->b()Lorg/b/a/e/b;

    move-result-object v0

    invoke-virtual {v0, p0}, Lorg/b/a/e/b;->a(Lorg/b/a/v;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
