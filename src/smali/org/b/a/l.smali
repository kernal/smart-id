.class public final Lorg/b/a/l;
.super Lorg/b/a/a/c;
.source "Instant.java"

# interfaces
.implements Ljava/io/Serializable;
.implements Lorg/b/a/t;


# static fields
.field public static final a:Lorg/b/a/l;


# instance fields
.field private final b:J


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 64
    new-instance v0, Lorg/b/a/l;

    const-wide/16 v1, 0x0

    invoke-direct {v0, v1, v2}, Lorg/b/a/l;-><init>(J)V

    sput-object v0, Lorg/b/a/l;->a:Lorg/b/a/l;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .line 136
    invoke-direct {p0}, Lorg/b/a/a/c;-><init>()V

    .line 137
    invoke-static {}, Lorg/b/a/e;->a()J

    move-result-wide v0

    iput-wide v0, p0, Lorg/b/a/l;->b:J

    return-void
.end method

.method public constructor <init>(J)V
    .locals 0

    .line 146
    invoke-direct {p0}, Lorg/b/a/a/c;-><init>()V

    .line 147
    iput-wide p1, p0, Lorg/b/a/l;->b:J

    return-void
.end method


# virtual methods
.method public b()Lorg/b/a/b;
    .locals 4

    .line 320
    new-instance v0, Lorg/b/a/b;

    invoke-virtual {p0}, Lorg/b/a/l;->c()J

    move-result-wide v1

    invoke-static {}, Lorg/b/a/b/u;->O()Lorg/b/a/b/u;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lorg/b/a/b;-><init>(JLorg/b/a/a;)V

    return-object v0
.end method

.method public c()J
    .locals 2

    .line 288
    iget-wide v0, p0, Lorg/b/a/l;->b:J

    return-wide v0
.end method

.method public d()Lorg/b/a/a;
    .locals 1

    .line 300
    invoke-static {}, Lorg/b/a/b/u;->N()Lorg/b/a/b/u;

    move-result-object v0

    return-object v0
.end method

.method public e()Lorg/b/a/o;
    .locals 4

    .line 366
    new-instance v0, Lorg/b/a/o;

    invoke-virtual {p0}, Lorg/b/a/l;->c()J

    move-result-wide v1

    invoke-static {}, Lorg/b/a/b/u;->O()Lorg/b/a/b/u;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lorg/b/a/o;-><init>(JLorg/b/a/a;)V

    return-object v0
.end method

.method public n_()Lorg/b/a/l;
    .locals 0

    return-object p0
.end method
