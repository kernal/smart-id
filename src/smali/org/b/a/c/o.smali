.class Lorg/b/a/c/o;
.super Lorg/b/a/c/a;
.source "ReadableInstantConverter.java"

# interfaces
.implements Lorg/b/a/c/h;
.implements Lorg/b/a/c/l;


# static fields
.field static final a:Lorg/b/a/c/o;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 36
    new-instance v0, Lorg/b/a/c/o;

    invoke-direct {v0}, Lorg/b/a/c/o;-><init>()V

    sput-object v0, Lorg/b/a/c/o;->a:Lorg/b/a/c/o;

    return-void
.end method

.method protected constructor <init>()V
    .locals 0

    .line 42
    invoke-direct {p0}, Lorg/b/a/c/a;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Object;Lorg/b/a/a;)J
    .locals 0

    .line 100
    check-cast p1, Lorg/b/a/t;

    invoke-interface {p1}, Lorg/b/a/t;->c()J

    move-result-wide p1

    return-wide p1
.end method

.method public a()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class<",
            "*>;"
        }
    .end annotation

    .line 110
    const-class v0, Lorg/b/a/t;

    return-object v0
.end method

.method public a(Ljava/lang/Object;Lorg/b/a/f;)Lorg/b/a/a;
    .locals 1

    .line 58
    check-cast p1, Lorg/b/a/t;

    invoke-interface {p1}, Lorg/b/a/t;->d()Lorg/b/a/a;

    move-result-object p1

    if-nez p1, :cond_0

    .line 60
    invoke-static {p2}, Lorg/b/a/b/u;->b(Lorg/b/a/f;)Lorg/b/a/b/u;

    move-result-object p1

    return-object p1

    .line 62
    :cond_0
    invoke-virtual {p1}, Lorg/b/a/a;->a()Lorg/b/a/f;

    move-result-object v0

    if-eq v0, p2, :cond_1

    .line 64
    invoke-virtual {p1, p2}, Lorg/b/a/a;->a(Lorg/b/a/f;)Lorg/b/a/a;

    move-result-object p1

    if-nez p1, :cond_1

    .line 66
    invoke-static {p2}, Lorg/b/a/b/u;->b(Lorg/b/a/f;)Lorg/b/a/b/u;

    move-result-object p1

    :cond_1
    return-object p1
.end method

.method public b(Ljava/lang/Object;Lorg/b/a/a;)Lorg/b/a/a;
    .locals 0

    if-nez p2, :cond_0

    .line 84
    check-cast p1, Lorg/b/a/t;

    invoke-interface {p1}, Lorg/b/a/t;->d()Lorg/b/a/a;

    move-result-object p1

    .line 85
    invoke-static {p1}, Lorg/b/a/e;->a(Lorg/b/a/a;)Lorg/b/a/a;

    move-result-object p2

    :cond_0
    return-object p2
.end method
