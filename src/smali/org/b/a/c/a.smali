.class public abstract Lorg/b/a/c/a;
.super Ljava/lang/Object;
.source "AbstractConverter.java"

# interfaces
.implements Lorg/b/a/c/c;


# direct methods
.method protected constructor <init>()V
    .locals 0

    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Object;Lorg/b/a/a;)J
    .locals 0

    .line 52
    invoke-static {}, Lorg/b/a/e;->a()J

    move-result-wide p1

    return-wide p1
.end method

.method public a(Ljava/lang/Object;Lorg/b/a/f;)Lorg/b/a/a;
    .locals 0

    .line 67
    invoke-static {p2}, Lorg/b/a/b/u;->b(Lorg/b/a/f;)Lorg/b/a/b/u;

    move-result-object p1

    return-object p1
.end method

.method public b(Ljava/lang/Object;Lorg/b/a/a;)Lorg/b/a/a;
    .locals 0

    .line 82
    invoke-static {p2}, Lorg/b/a/e;->a(Lorg/b/a/a;)Lorg/b/a/a;

    move-result-object p1

    return-object p1
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 158
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Converter["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lorg/b/a/c/a;->a()Ljava/lang/Class;

    move-result-object v1

    if-nez v1, :cond_0

    const-string v1, "null"

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lorg/b/a/c/a;->a()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    :goto_0
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
