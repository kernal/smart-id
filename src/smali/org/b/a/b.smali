.class public final Lorg/b/a/b;
.super Lorg/b/a/a/e;
.source "DateTime.java"

# interfaces
.implements Ljava/io/Serializable;
.implements Lorg/b/a/r;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 171
    invoke-direct {p0}, Lorg/b/a/a/e;-><init>()V

    return-void
.end method

.method public constructor <init>(IIIIIIILorg/b/a/a;)V
    .locals 0

    .line 532
    invoke-direct/range {p0 .. p8}, Lorg/b/a/a/e;-><init>(IIIIIIILorg/b/a/a;)V

    return-void
.end method

.method public constructor <init>(JLorg/b/a/a;)V
    .locals 0

    .line 236
    invoke-direct {p0, p1, p2, p3}, Lorg/b/a/a/e;-><init>(JLorg/b/a/a;)V

    return-void
.end method

.method public constructor <init>(JLorg/b/a/f;)V
    .locals 0

    .line 222
    invoke-direct {p0, p1, p2, p3}, Lorg/b/a/a/e;-><init>(JLorg/b/a/f;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/Object;)V
    .locals 1

    const/4 v0, 0x0

    .line 257
    check-cast v0, Lorg/b/a/a;

    invoke-direct {p0, p1, v0}, Lorg/b/a/a/e;-><init>(Ljava/lang/Object;Lorg/b/a/a;)V

    return-void
.end method

.method public static a()Lorg/b/a/b;
    .locals 1

    .line 89
    new-instance v0, Lorg/b/a/b;

    invoke-direct {v0}, Lorg/b/a/b;-><init>()V

    return-object v0
.end method

.method public static a(Ljava/lang/String;)Lorg/b/a/b;
    .locals 1
    .annotation runtime Lorg/joda/convert/FromString;
    .end annotation

    .line 149
    invoke-static {}, Lorg/b/a/e/j;->a()Lorg/b/a/e/b;

    move-result-object v0

    invoke-virtual {v0}, Lorg/b/a/e/b;->d()Lorg/b/a/e/b;

    move-result-object v0

    invoke-static {p0, v0}, Lorg/b/a/b;->a(Ljava/lang/String;Lorg/b/a/e/b;)Lorg/b/a/b;

    move-result-object p0

    return-object p0
.end method

.method public static a(Ljava/lang/String;Lorg/b/a/e/b;)Lorg/b/a/b;
    .locals 0

    .line 160
    invoke-virtual {p1, p0}, Lorg/b/a/e/b;->b(Ljava/lang/String;)Lorg/b/a/b;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public a(I)Lorg/b/a/b;
    .locals 3

    if-nez p1, :cond_0

    return-object p0

    .line 1203
    :cond_0
    invoke-virtual {p0}, Lorg/b/a/b;->d()Lorg/b/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lorg/b/a/a;->f()Lorg/b/a/h;

    move-result-object v0

    invoke-virtual {p0}, Lorg/b/a/b;->c()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2, p1}, Lorg/b/a/h;->a(JI)J

    move-result-wide v0

    .line 1204
    invoke-virtual {p0, v0, v1}, Lorg/b/a/b;->a_(J)Lorg/b/a/b;

    move-result-object p1

    return-object p1
.end method

.method public a(Lorg/b/a/f;)Lorg/b/a/b;
    .locals 1

    .line 634
    invoke-virtual {p0}, Lorg/b/a/b;->d()Lorg/b/a/a;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/b/a/a;->a(Lorg/b/a/f;)Lorg/b/a/a;

    move-result-object p1

    invoke-virtual {p0, p1}, Lorg/b/a/b;->a_(Lorg/b/a/a;)Lorg/b/a/b;

    move-result-object p1

    return-object p1
.end method

.method public a_(J)Lorg/b/a/b;
    .locals 3

    .line 598
    invoke-virtual {p0}, Lorg/b/a/b;->c()J

    move-result-wide v0

    cmp-long v2, p1, v0

    if-nez v2, :cond_0

    move-object v0, p0

    goto :goto_0

    :cond_0
    new-instance v0, Lorg/b/a/b;

    invoke-virtual {p0}, Lorg/b/a/b;->d()Lorg/b/a/a;

    move-result-object v1

    invoke-direct {v0, p1, p2, v1}, Lorg/b/a/b;-><init>(JLorg/b/a/a;)V

    :goto_0
    return-object v0
.end method

.method public a_(Lorg/b/a/a;)Lorg/b/a/b;
    .locals 3

    .line 611
    invoke-static {p1}, Lorg/b/a/e;->a(Lorg/b/a/a;)Lorg/b/a/a;

    move-result-object p1

    .line 612
    invoke-virtual {p0}, Lorg/b/a/b;->d()Lorg/b/a/a;

    move-result-object v0

    if-ne p1, v0, :cond_0

    move-object v0, p0

    goto :goto_0

    :cond_0
    new-instance v0, Lorg/b/a/b;

    invoke-virtual {p0}, Lorg/b/a/b;->c()J

    move-result-wide v1

    invoke-direct {v0, v1, v2, p1}, Lorg/b/a/b;-><init>(JLorg/b/a/a;)V

    :goto_0
    return-object v0
.end method

.method public b()Lorg/b/a/b;
    .locals 0

    return-object p0
.end method

.method public b(I)Lorg/b/a/b;
    .locals 3

    if-nez p1, :cond_0

    return-object p0

    .line 1229
    :cond_0
    invoke-virtual {p0}, Lorg/b/a/b;->d()Lorg/b/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lorg/b/a/a;->c()Lorg/b/a/h;

    move-result-object v0

    invoke-virtual {p0}, Lorg/b/a/b;->c()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2, p1}, Lorg/b/a/h;->a(JI)J

    move-result-wide v0

    .line 1230
    invoke-virtual {p0, v0, v1}, Lorg/b/a/b;->a_(J)Lorg/b/a/b;

    move-result-object p1

    return-object p1
.end method
