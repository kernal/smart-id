.class Lorg/b/a/e/e$a;
.super Ljava/lang/Object;
.source "DateTimeParserBucket.java"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/b/a/e/e;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable<",
        "Lorg/b/a/e/e$a;",
        ">;"
    }
.end annotation


# instance fields
.field a:Lorg/b/a/c;

.field b:I

.field c:Ljava/lang/String;

.field d:Ljava/util/Locale;


# direct methods
.method constructor <init>()V
    .locals 0

    .line 551
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lorg/b/a/e/e$a;)I
    .locals 2

    .line 586
    iget-object p1, p1, Lorg/b/a/e/e$a;->a:Lorg/b/a/c;

    .line 587
    iget-object v0, p0, Lorg/b/a/e/e$a;->a:Lorg/b/a/c;

    .line 588
    invoke-virtual {v0}, Lorg/b/a/c;->f()Lorg/b/a/h;

    move-result-object v0

    invoke-virtual {p1}, Lorg/b/a/c;->f()Lorg/b/a/h;

    move-result-object v1

    invoke-static {v0, v1}, Lorg/b/a/e/e;->a(Lorg/b/a/h;Lorg/b/a/h;)I

    move-result v0

    if-eqz v0, :cond_0

    return v0

    .line 592
    :cond_0
    iget-object v0, p0, Lorg/b/a/e/e$a;->a:Lorg/b/a/c;

    .line 593
    invoke-virtual {v0}, Lorg/b/a/c;->e()Lorg/b/a/h;

    move-result-object v0

    invoke-virtual {p1}, Lorg/b/a/c;->e()Lorg/b/a/h;

    move-result-object p1

    invoke-static {v0, p1}, Lorg/b/a/e/e;->a(Lorg/b/a/h;Lorg/b/a/h;)I

    move-result p1

    return p1
.end method

.method a(JZ)J
    .locals 3

    .line 569
    iget-object v0, p0, Lorg/b/a/e/e$a;->c:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 570
    iget-object v0, p0, Lorg/b/a/e/e$a;->a:Lorg/b/a/c;

    iget v1, p0, Lorg/b/a/e/e$a;->b:I

    invoke-virtual {v0, p1, p2, v1}, Lorg/b/a/c;->c(JI)J

    move-result-wide p1

    goto :goto_0

    .line 572
    :cond_0
    iget-object v1, p0, Lorg/b/a/e/e$a;->a:Lorg/b/a/c;

    iget-object v2, p0, Lorg/b/a/e/e$a;->d:Ljava/util/Locale;

    invoke-virtual {v1, p1, p2, v0, v2}, Lorg/b/a/c;->a(JLjava/lang/String;Ljava/util/Locale;)J

    move-result-wide p1

    :goto_0
    if-eqz p3, :cond_1

    .line 575
    iget-object p3, p0, Lorg/b/a/e/e$a;->a:Lorg/b/a/c;

    invoke-virtual {p3, p1, p2}, Lorg/b/a/c;->d(J)J

    move-result-wide p1

    :cond_1
    return-wide p1
.end method

.method a(Lorg/b/a/c;I)V
    .locals 0

    .line 555
    iput-object p1, p0, Lorg/b/a/e/e$a;->a:Lorg/b/a/c;

    .line 556
    iput p2, p0, Lorg/b/a/e/e$a;->b:I

    const/4 p1, 0x0

    .line 557
    iput-object p1, p0, Lorg/b/a/e/e$a;->c:Ljava/lang/String;

    .line 558
    iput-object p1, p0, Lorg/b/a/e/e$a;->d:Ljava/util/Locale;

    return-void
.end method

.method a(Lorg/b/a/c;Ljava/lang/String;Ljava/util/Locale;)V
    .locals 0

    .line 562
    iput-object p1, p0, Lorg/b/a/e/e$a;->a:Lorg/b/a/c;

    const/4 p1, 0x0

    .line 563
    iput p1, p0, Lorg/b/a/e/e$a;->b:I

    .line 564
    iput-object p2, p0, Lorg/b/a/e/e$a;->c:Ljava/lang/String;

    .line 565
    iput-object p3, p0, Lorg/b/a/e/e$a;->d:Ljava/util/Locale;

    return-void
.end method

.method public synthetic compareTo(Ljava/lang/Object;)I
    .locals 0

    .line 545
    check-cast p1, Lorg/b/a/e/e$a;

    invoke-virtual {p0, p1}, Lorg/b/a/e/e$a;->a(Lorg/b/a/e/e$a;)I

    move-result p1

    return p1
.end method
