.class Lorg/b/a/e/c$n;
.super Lorg/b/a/e/c$f;
.source "DateTimeFormatterBuilder.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/b/a/e/c;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "n"
.end annotation


# direct methods
.method protected constructor <init>(Lorg/b/a/d;IZ)V
    .locals 0

    .line 1377
    invoke-direct {p0, p1, p2, p3}, Lorg/b/a/e/c$f;-><init>(Lorg/b/a/d;IZ)V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    .line 1381
    iget v0, p0, Lorg/b/a/e/c$n;->b:I

    return v0
.end method

.method public a(Ljava/lang/Appendable;JLorg/b/a/a;ILorg/b/a/f;Ljava/util/Locale;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1388
    :try_start_0
    iget-object p5, p0, Lorg/b/a/e/c$n;->a:Lorg/b/a/d;

    invoke-virtual {p5, p4}, Lorg/b/a/d;->a(Lorg/b/a/a;)Lorg/b/a/c;

    move-result-object p4

    .line 1389
    invoke-virtual {p4, p2, p3}, Lorg/b/a/c;->a(J)I

    move-result p2

    invoke-static {p1, p2}, Lorg/b/a/e/i;->a(Ljava/lang/Appendable;I)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    const p2, 0xfffd

    .line 1391
    invoke-interface {p1, p2}, Ljava/lang/Appendable;->append(C)Ljava/lang/Appendable;

    :goto_0
    return-void
.end method

.method public a(Ljava/lang/Appendable;Lorg/b/a/v;Ljava/util/Locale;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1396
    iget-object p3, p0, Lorg/b/a/e/c$n;->a:Lorg/b/a/d;

    invoke-interface {p2, p3}, Lorg/b/a/v;->b(Lorg/b/a/d;)Z

    move-result p3

    const v0, 0xfffd

    if-eqz p3, :cond_0

    .line 1398
    :try_start_0
    iget-object p3, p0, Lorg/b/a/e/c$n;->a:Lorg/b/a/d;

    invoke-interface {p2, p3}, Lorg/b/a/v;->a(Lorg/b/a/d;)I

    move-result p2

    invoke-static {p1, p2}, Lorg/b/a/e/i;->a(Ljava/lang/Appendable;I)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1400
    :catch_0
    invoke-interface {p1, v0}, Ljava/lang/Appendable;->append(C)Ljava/lang/Appendable;

    goto :goto_0

    .line 1403
    :cond_0
    invoke-interface {p1, v0}, Ljava/lang/Appendable;->append(C)Ljava/lang/Appendable;

    :goto_0
    return-void
.end method
