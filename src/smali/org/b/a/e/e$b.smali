.class Lorg/b/a/e/e$b;
.super Ljava/lang/Object;
.source "DateTimeParserBucket.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/b/a/e/e;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "b"
.end annotation


# instance fields
.field final a:Lorg/b/a/f;

.field final b:Ljava/lang/Integer;

.field final c:[Lorg/b/a/e/e$a;

.field final d:I

.field final synthetic e:Lorg/b/a/e/e;


# direct methods
.method constructor <init>(Lorg/b/a/e/e;)V
    .locals 1

    .line 518
    iput-object p1, p0, Lorg/b/a/e/e$b;->e:Lorg/b/a/e/e;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 519
    invoke-static {p1}, Lorg/b/a/e/e;->a(Lorg/b/a/e/e;)Lorg/b/a/f;

    move-result-object v0

    iput-object v0, p0, Lorg/b/a/e/e$b;->a:Lorg/b/a/f;

    .line 520
    invoke-static {p1}, Lorg/b/a/e/e;->b(Lorg/b/a/e/e;)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lorg/b/a/e/e$b;->b:Ljava/lang/Integer;

    .line 521
    invoke-static {p1}, Lorg/b/a/e/e;->c(Lorg/b/a/e/e;)[Lorg/b/a/e/e$a;

    move-result-object v0

    iput-object v0, p0, Lorg/b/a/e/e$b;->c:[Lorg/b/a/e/e$a;

    .line 522
    invoke-static {p1}, Lorg/b/a/e/e;->d(Lorg/b/a/e/e;)I

    move-result p1

    iput p1, p0, Lorg/b/a/e/e$b;->d:I

    return-void
.end method


# virtual methods
.method a(Lorg/b/a/e/e;)Z
    .locals 3

    .line 526
    iget-object v0, p0, Lorg/b/a/e/e$b;->e:Lorg/b/a/e/e;

    if-eq p1, v0, :cond_0

    const/4 p1, 0x0

    return p1

    .line 530
    :cond_0
    iget-object v0, p0, Lorg/b/a/e/e$b;->a:Lorg/b/a/f;

    invoke-static {p1, v0}, Lorg/b/a/e/e;->a(Lorg/b/a/e/e;Lorg/b/a/f;)Lorg/b/a/f;

    .line 531
    iget-object v0, p0, Lorg/b/a/e/e$b;->b:Ljava/lang/Integer;

    invoke-static {p1, v0}, Lorg/b/a/e/e;->a(Lorg/b/a/e/e;Ljava/lang/Integer;)Ljava/lang/Integer;

    .line 532
    iget-object v0, p0, Lorg/b/a/e/e$b;->c:[Lorg/b/a/e/e$a;

    invoke-static {p1, v0}, Lorg/b/a/e/e;->a(Lorg/b/a/e/e;[Lorg/b/a/e/e$a;)[Lorg/b/a/e/e$a;

    .line 533
    iget v0, p0, Lorg/b/a/e/e$b;->d:I

    invoke-static {p1}, Lorg/b/a/e/e;->d(Lorg/b/a/e/e;)I

    move-result v1

    const/4 v2, 0x1

    if-ge v0, v1, :cond_1

    .line 538
    invoke-static {p1, v2}, Lorg/b/a/e/e;->a(Lorg/b/a/e/e;Z)Z

    .line 540
    :cond_1
    iget v0, p0, Lorg/b/a/e/e$b;->d:I

    invoke-static {p1, v0}, Lorg/b/a/e/e;->a(Lorg/b/a/e/e;I)I

    return v2
.end method
