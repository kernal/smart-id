.class public Lorg/b/a/o;
.super Lorg/b/a/a/e;
.source "MutableDateTime.java"

# interfaces
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;
.implements Lorg/b/a/p;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/b/a/o$a;
    }
.end annotation


# instance fields
.field private a:Lorg/b/a/c;

.field private b:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 171
    invoke-direct {p0}, Lorg/b/a/a/e;-><init>()V

    return-void
.end method

.method public constructor <init>(JLorg/b/a/a;)V
    .locals 0

    .line 236
    invoke-direct {p0, p1, p2, p3}, Lorg/b/a/a/e;-><init>(JLorg/b/a/a;)V

    return-void
.end method

.method public constructor <init>(JLorg/b/a/f;)V
    .locals 0

    .line 222
    invoke-direct {p0, p1, p2, p3}, Lorg/b/a/a/e;-><init>(JLorg/b/a/f;)V

    return-void
.end method


# virtual methods
.method public a(Lorg/b/a/d;)Lorg/b/a/o$a;
    .locals 3

    if-eqz p1, :cond_1

    .line 1049
    invoke-virtual {p0}, Lorg/b/a/o;->d()Lorg/b/a/a;

    move-result-object v0

    invoke-virtual {p1, v0}, Lorg/b/a/d;->a(Lorg/b/a/a;)Lorg/b/a/c;

    move-result-object v0

    .line 1050
    invoke-virtual {v0}, Lorg/b/a/c;->c()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1053
    new-instance p1, Lorg/b/a/o$a;

    invoke-direct {p1, p0, v0}, Lorg/b/a/o$a;-><init>(Lorg/b/a/o;Lorg/b/a/c;)V

    return-object p1

    .line 1051
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Field \'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string p1, "\' is not supported"

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1047
    :cond_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "The DateTimeFieldType must not be null"

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public a(J)V
    .locals 2

    .line 453
    iget v0, p0, Lorg/b/a/o;->b:I

    if-eqz v0, :cond_5

    const/4 v1, 0x1

    if-eq v0, v1, :cond_4

    const/4 v1, 0x2

    if-eq v0, v1, :cond_3

    const/4 v1, 0x3

    if-eq v0, v1, :cond_2

    const/4 v1, 0x4

    if-eq v0, v1, :cond_1

    const/4 v1, 0x5

    if-eq v0, v1, :cond_0

    goto :goto_0

    .line 469
    :cond_0
    iget-object v0, p0, Lorg/b/a/o;->a:Lorg/b/a/c;

    invoke-virtual {v0, p1, p2}, Lorg/b/a/c;->h(J)J

    move-result-wide p1

    goto :goto_0

    .line 466
    :cond_1
    iget-object v0, p0, Lorg/b/a/o;->a:Lorg/b/a/c;

    invoke-virtual {v0, p1, p2}, Lorg/b/a/c;->g(J)J

    move-result-wide p1

    goto :goto_0

    .line 463
    :cond_2
    iget-object v0, p0, Lorg/b/a/o;->a:Lorg/b/a/c;

    invoke-virtual {v0, p1, p2}, Lorg/b/a/c;->f(J)J

    move-result-wide p1

    goto :goto_0

    .line 460
    :cond_3
    iget-object v0, p0, Lorg/b/a/o;->a:Lorg/b/a/c;

    invoke-virtual {v0, p1, p2}, Lorg/b/a/c;->e(J)J

    move-result-wide p1

    goto :goto_0

    .line 457
    :cond_4
    iget-object v0, p0, Lorg/b/a/o;->a:Lorg/b/a/c;

    invoke-virtual {v0, p1, p2}, Lorg/b/a/c;->d(J)J

    move-result-wide p1

    .line 472
    :cond_5
    :goto_0
    invoke-super {p0, p1, p2}, Lorg/b/a/a/e;->a(J)V

    return-void
.end method

.method public a(Lorg/b/a/a;)V
    .locals 0

    .line 562
    invoke-super {p0, p1}, Lorg/b/a/a/e;->a(Lorg/b/a/a;)V

    return-void
.end method

.method public a(Lorg/b/a/f;)V
    .locals 3

    .line 600
    invoke-static {p1}, Lorg/b/a/e;->a(Lorg/b/a/f;)Lorg/b/a/f;

    move-result-object p1

    .line 601
    invoke-virtual {p0}, Lorg/b/a/o;->h()Lorg/b/a/f;

    move-result-object v0

    invoke-static {v0}, Lorg/b/a/e;->a(Lorg/b/a/f;)Lorg/b/a/f;

    move-result-object v0

    if-ne p1, v0, :cond_0

    return-void

    .line 606
    :cond_0
    invoke-virtual {p0}, Lorg/b/a/o;->c()J

    move-result-wide v1

    invoke-virtual {v0, p1, v1, v2}, Lorg/b/a/f;->a(Lorg/b/a/f;J)J

    move-result-wide v0

    .line 607
    invoke-virtual {p0}, Lorg/b/a/o;->d()Lorg/b/a/a;

    move-result-object v2

    invoke-virtual {v2, p1}, Lorg/b/a/a;->a(Lorg/b/a/f;)Lorg/b/a/a;

    move-result-object p1

    invoke-virtual {p0, p1}, Lorg/b/a/o;->a(Lorg/b/a/a;)V

    .line 608
    invoke-virtual {p0, v0, v1}, Lorg/b/a/o;->a(J)V

    return-void
.end method

.method public clone()Ljava/lang/Object;
    .locals 2

    .line 1250
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    .line 1252
    :catch_0
    new-instance v0, Ljava/lang/InternalError;

    const-string v1, "Clone error"

    invoke-direct {v0, v1}, Ljava/lang/InternalError;-><init>(Ljava/lang/String;)V

    throw v0
.end method
