.class public abstract Lorg/b/a/a/c;
.super Ljava/lang/Object;
.source "AbstractInstant.java"

# interfaces
.implements Lorg/b/a/t;


# direct methods
.method protected constructor <init>()V
    .locals 0

    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lorg/b/a/t;)Z
    .locals 2

    .line 378
    invoke-static {p1}, Lorg/b/a/e;->a(Lorg/b/a/t;)J

    move-result-wide v0

    .line 379
    invoke-virtual {p0, v0, v1}, Lorg/b/a/a/c;->c(J)Z

    move-result p1

    return p1
.end method

.method public b(Lorg/b/a/t;)I
    .locals 5

    const/4 v0, 0x0

    if-ne p0, p1, :cond_0

    return v0

    .line 300
    :cond_0
    invoke-interface {p1}, Lorg/b/a/t;->c()J

    move-result-wide v1

    .line 301
    invoke-virtual {p0}, Lorg/b/a/a/c;->c()J

    move-result-wide v3

    cmp-long p1, v3, v1

    if-nez p1, :cond_1

    return v0

    :cond_1
    if-gez p1, :cond_2

    const/4 p1, -0x1

    return p1

    :cond_2
    const/4 p1, 0x1

    return p1
.end method

.method public b()Lorg/b/a/b;
    .locals 4

    .line 146
    new-instance v0, Lorg/b/a/b;

    invoke-virtual {p0}, Lorg/b/a/a/c;->c()J

    move-result-wide v1

    invoke-virtual {p0}, Lorg/b/a/a/c;->h()Lorg/b/a/f;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lorg/b/a/b;-><init>(JLorg/b/a/f;)V

    return-object v0
.end method

.method public b(J)Z
    .locals 3

    .line 323
    invoke-virtual {p0}, Lorg/b/a/a/c;->c()J

    move-result-wide v0

    cmp-long v2, v0, p1

    if-lez v2, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public c(J)Z
    .locals 3

    .line 357
    invoke-virtual {p0}, Lorg/b/a/a/c;->c()J

    move-result-wide v0

    cmp-long v2, v0, p1

    if-gez v2, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public synthetic compareTo(Ljava/lang/Object;)I
    .locals 0

    .line 55
    check-cast p1, Lorg/b/a/t;

    invoke-virtual {p0, p1}, Lorg/b/a/a/c;->b(Lorg/b/a/t;)I

    move-result p1

    return p1
.end method

.method public e()Lorg/b/a/o;
    .locals 4

    .line 191
    new-instance v0, Lorg/b/a/o;

    invoke-virtual {p0}, Lorg/b/a/a/c;->c()J

    move-result-wide v1

    invoke-virtual {p0}, Lorg/b/a/a/c;->h()Lorg/b/a/f;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lorg/b/a/o;-><init>(JLorg/b/a/f;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 7

    const/4 v0, 0x1

    if-ne p0, p1, :cond_0

    return v0

    .line 262
    :cond_0
    instance-of v1, p1, Lorg/b/a/t;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 265
    :cond_1
    check-cast p1, Lorg/b/a/t;

    .line 267
    invoke-virtual {p0}, Lorg/b/a/a/c;->c()J

    move-result-wide v3

    invoke-interface {p1}, Lorg/b/a/t;->c()J

    move-result-wide v5

    cmp-long v1, v3, v5

    if-nez v1, :cond_2

    .line 268
    invoke-virtual {p0}, Lorg/b/a/a/c;->d()Lorg/b/a/a;

    move-result-object v1

    invoke-interface {p1}, Lorg/b/a/t;->d()Lorg/b/a/a;

    move-result-object p1

    invoke-static {v1, p1}, Lorg/b/a/d/h;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public h()Lorg/b/a/f;
    .locals 1

    .line 71
    invoke-virtual {p0}, Lorg/b/a/a/c;->d()Lorg/b/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lorg/b/a/a;->a()Lorg/b/a/f;

    move-result-object v0

    return-object v0
.end method

.method public hashCode()I
    .locals 5

    .line 279
    invoke-virtual {p0}, Lorg/b/a/a/c;->c()J

    move-result-wide v0

    invoke-virtual {p0}, Lorg/b/a/a/c;->c()J

    move-result-wide v2

    const/16 v4, 0x20

    ushr-long/2addr v2, v4

    xor-long/2addr v0, v2

    long-to-int v1, v0

    .line 280
    invoke-virtual {p0}, Lorg/b/a/a/c;->d()Lorg/b/a/a;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    add-int/2addr v1, v0

    return v1
.end method

.method public i()Z
    .locals 2

    .line 333
    invoke-static {}, Lorg/b/a/e;->a()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lorg/b/a/a/c;->b(J)Z

    move-result v0

    return v0
.end method

.method public n_()Lorg/b/a/l;
    .locals 3

    .line 137
    new-instance v0, Lorg/b/a/l;

    invoke-virtual {p0}, Lorg/b/a/a/c;->c()J

    move-result-wide v1

    invoke-direct {v0, v1, v2}, Lorg/b/a/l;-><init>(J)V

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1
    .annotation runtime Lorg/joda/convert/ToString;
    .end annotation

    .line 424
    invoke-static {}, Lorg/b/a/e/j;->c()Lorg/b/a/e/b;

    move-result-object v0

    invoke-virtual {v0, p0}, Lorg/b/a/e/b;->a(Lorg/b/a/t;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
