.class public abstract Lorg/b/a/a/e;
.super Lorg/b/a/a/a;
.source "BaseDateTime.java"

# interfaces
.implements Ljava/io/Serializable;
.implements Lorg/b/a/r;


# instance fields
.field private volatile a:J

.field private volatile b:Lorg/b/a/a;


# direct methods
.method public constructor <init>()V
    .locals 3

    .line 61
    invoke-static {}, Lorg/b/a/e;->a()J

    move-result-wide v0

    invoke-static {}, Lorg/b/a/b/u;->O()Lorg/b/a/b/u;

    move-result-object v2

    invoke-direct {p0, v0, v1, v2}, Lorg/b/a/a/e;-><init>(JLorg/b/a/a;)V

    return-void
.end method

.method public constructor <init>(IIIIIIILorg/b/a/a;)V
    .locals 10

    move-object v0, p0

    .line 255
    invoke-direct {p0}, Lorg/b/a/a/a;-><init>()V

    move-object/from16 v1, p8

    .line 256
    invoke-virtual {p0, v1}, Lorg/b/a/a/e;->b(Lorg/b/a/a;)Lorg/b/a/a;

    move-result-object v1

    iput-object v1, v0, Lorg/b/a/a/e;->b:Lorg/b/a/a;

    .line 257
    iget-object v2, v0, Lorg/b/a/a/e;->b:Lorg/b/a/a;

    move v3, p1

    move v4, p2

    move v5, p3

    move v6, p4

    move v7, p5

    move/from16 v8, p6

    move/from16 v9, p7

    invoke-virtual/range {v2 .. v9}, Lorg/b/a/a;->a(IIIIIII)J

    move-result-wide v1

    .line 259
    iget-object v3, v0, Lorg/b/a/a/e;->b:Lorg/b/a/a;

    invoke-virtual {p0, v1, v2, v3}, Lorg/b/a/a/e;->a(JLorg/b/a/a;)J

    move-result-wide v1

    iput-wide v1, v0, Lorg/b/a/a/e;->a:J

    .line 260
    invoke-direct {p0}, Lorg/b/a/a/e;->j()V

    return-void
.end method

.method public constructor <init>(JLorg/b/a/a;)V
    .locals 0

    .line 124
    invoke-direct {p0}, Lorg/b/a/a/a;-><init>()V

    .line 125
    invoke-virtual {p0, p3}, Lorg/b/a/a/e;->b(Lorg/b/a/a;)Lorg/b/a/a;

    move-result-object p3

    iput-object p3, p0, Lorg/b/a/a/e;->b:Lorg/b/a/a;

    .line 126
    iget-object p3, p0, Lorg/b/a/a/e;->b:Lorg/b/a/a;

    invoke-virtual {p0, p1, p2, p3}, Lorg/b/a/a/e;->a(JLorg/b/a/a;)J

    move-result-wide p1

    iput-wide p1, p0, Lorg/b/a/a/e;->a:J

    .line 127
    invoke-direct {p0}, Lorg/b/a/a/e;->j()V

    return-void
.end method

.method public constructor <init>(JLorg/b/a/f;)V
    .locals 0

    .line 110
    invoke-static {p3}, Lorg/b/a/b/u;->b(Lorg/b/a/f;)Lorg/b/a/b/u;

    move-result-object p3

    invoke-direct {p0, p1, p2, p3}, Lorg/b/a/a/e;-><init>(JLorg/b/a/a;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/Object;Lorg/b/a/a;)V
    .locals 2

    .line 170
    invoke-direct {p0}, Lorg/b/a/a/a;-><init>()V

    .line 171
    invoke-static {}, Lorg/b/a/c/d;->a()Lorg/b/a/c/d;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/b/a/c/d;->a(Ljava/lang/Object;)Lorg/b/a/c/h;

    move-result-object v0

    .line 172
    invoke-interface {v0, p1, p2}, Lorg/b/a/c/h;->b(Ljava/lang/Object;Lorg/b/a/a;)Lorg/b/a/a;

    move-result-object v1

    invoke-virtual {p0, v1}, Lorg/b/a/a/e;->b(Lorg/b/a/a;)Lorg/b/a/a;

    move-result-object v1

    iput-object v1, p0, Lorg/b/a/a/e;->b:Lorg/b/a/a;

    .line 173
    invoke-interface {v0, p1, p2}, Lorg/b/a/c/h;->a(Ljava/lang/Object;Lorg/b/a/a;)J

    move-result-wide p1

    iget-object v0, p0, Lorg/b/a/a/e;->b:Lorg/b/a/a;

    invoke-virtual {p0, p1, p2, v0}, Lorg/b/a/a/e;->a(JLorg/b/a/a;)J

    move-result-wide p1

    iput-wide p1, p0, Lorg/b/a/a/e;->a:J

    .line 174
    invoke-direct {p0}, Lorg/b/a/a/e;->j()V

    return-void
.end method

.method private j()V
    .locals 5

    .line 264
    iget-wide v0, p0, Lorg/b/a/a/e;->a:J

    const-wide/high16 v2, -0x8000000000000000L

    cmp-long v4, v0, v2

    if-eqz v4, :cond_0

    iget-wide v0, p0, Lorg/b/a/a/e;->a:J

    const-wide v2, 0x7fffffffffffffffL

    cmp-long v4, v0, v2

    if-nez v4, :cond_1

    .line 265
    :cond_0
    iget-object v0, p0, Lorg/b/a/a/e;->b:Lorg/b/a/a;

    invoke-virtual {v0}, Lorg/b/a/a;->b()Lorg/b/a/a;

    move-result-object v0

    iput-object v0, p0, Lorg/b/a/a/e;->b:Lorg/b/a/a;

    :cond_1
    return-void
.end method


# virtual methods
.method protected a(JLorg/b/a/a;)J
    .locals 0

    return-wide p1
.end method

.method protected a(J)V
    .locals 1

    .line 327
    iget-object v0, p0, Lorg/b/a/a/e;->b:Lorg/b/a/a;

    invoke-virtual {p0, p1, p2, v0}, Lorg/b/a/a/e;->a(JLorg/b/a/a;)J

    move-result-wide p1

    iput-wide p1, p0, Lorg/b/a/a/e;->a:J

    return-void
.end method

.method protected a(Lorg/b/a/a;)V
    .locals 0

    .line 339
    invoke-virtual {p0, p1}, Lorg/b/a/a/e;->b(Lorg/b/a/a;)Lorg/b/a/a;

    move-result-object p1

    iput-object p1, p0, Lorg/b/a/a/e;->b:Lorg/b/a/a;

    return-void
.end method

.method protected b(Lorg/b/a/a;)Lorg/b/a/a;
    .locals 0

    .line 280
    invoke-static {p1}, Lorg/b/a/e;->a(Lorg/b/a/a;)Lorg/b/a/a;

    move-result-object p1

    return-object p1
.end method

.method public c()J
    .locals 2

    .line 305
    iget-wide v0, p0, Lorg/b/a/a/e;->a:J

    return-wide v0
.end method

.method public d()Lorg/b/a/a;
    .locals 1

    .line 314
    iget-object v0, p0, Lorg/b/a/a/e;->b:Lorg/b/a/a;

    return-object v0
.end method
