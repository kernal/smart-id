.class public abstract Lorg/b/a/d/i;
.super Lorg/b/a/d/b;
.source "ImpreciseDateTimeField.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/b/a/d/i$a;
    }
.end annotation


# instance fields
.field private final a:Lorg/b/a/h;

.field final b:J


# direct methods
.method public constructor <init>(Lorg/b/a/d;J)V
    .locals 0

    .line 56
    invoke-direct {p0, p1}, Lorg/b/a/d/b;-><init>(Lorg/b/a/d;)V

    .line 57
    iput-wide p2, p0, Lorg/b/a/d/i;->b:J

    .line 58
    new-instance p2, Lorg/b/a/d/i$a;

    invoke-virtual {p1}, Lorg/b/a/d;->y()Lorg/b/a/i;

    move-result-object p1

    invoke-direct {p2, p0, p1}, Lorg/b/a/d/i$a;-><init>(Lorg/b/a/d/i;Lorg/b/a/i;)V

    iput-object p2, p0, Lorg/b/a/d/i;->a:Lorg/b/a/h;

    return-void
.end method


# virtual methods
.method public abstract a(JI)J
.end method

.method public abstract a(JJ)J
.end method

.method public final e()Lorg/b/a/h;
    .locals 1

    .line 138
    iget-object v0, p0, Lorg/b/a/d/i;->a:Lorg/b/a/h;

    return-object v0
.end method
