.class public Lorg/b/a/d/f;
.super Lorg/b/a/c;
.source "DelegatedDateTimeField.java"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private final a:Lorg/b/a/c;

.field private final b:Lorg/b/a/h;

.field private final c:Lorg/b/a/d;


# direct methods
.method public constructor <init>(Lorg/b/a/c;)V
    .locals 1

    const/4 v0, 0x0

    .line 54
    invoke-direct {p0, p1, v0}, Lorg/b/a/d/f;-><init>(Lorg/b/a/c;Lorg/b/a/d;)V

    return-void
.end method

.method public constructor <init>(Lorg/b/a/c;Lorg/b/a/d;)V
    .locals 1

    const/4 v0, 0x0

    .line 64
    invoke-direct {p0, p1, v0, p2}, Lorg/b/a/d/f;-><init>(Lorg/b/a/c;Lorg/b/a/h;Lorg/b/a/d;)V

    return-void
.end method

.method public constructor <init>(Lorg/b/a/c;Lorg/b/a/h;Lorg/b/a/d;)V
    .locals 0

    .line 75
    invoke-direct {p0}, Lorg/b/a/c;-><init>()V

    if-eqz p1, :cond_1

    .line 79
    iput-object p1, p0, Lorg/b/a/d/f;->a:Lorg/b/a/c;

    .line 80
    iput-object p2, p0, Lorg/b/a/d/f;->b:Lorg/b/a/h;

    if-nez p3, :cond_0

    .line 81
    invoke-virtual {p1}, Lorg/b/a/c;->a()Lorg/b/a/d;

    move-result-object p3

    :cond_0
    iput-object p3, p0, Lorg/b/a/d/f;->c:Lorg/b/a/d;

    return-void

    .line 77
    :cond_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "The field must not be null"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method


# virtual methods
.method public a(J)I
    .locals 1

    .line 110
    iget-object v0, p0, Lorg/b/a/d/f;->a:Lorg/b/a/c;

    invoke-virtual {v0, p1, p2}, Lorg/b/a/c;->a(J)I

    move-result p1

    return p1
.end method

.method public a(Ljava/util/Locale;)I
    .locals 1

    .line 261
    iget-object v0, p0, Lorg/b/a/d/f;->a:Lorg/b/a/c;

    invoke-virtual {v0, p1}, Lorg/b/a/c;->a(Ljava/util/Locale;)I

    move-result p1

    return p1
.end method

.method public a(JI)J
    .locals 1

    .line 154
    iget-object v0, p0, Lorg/b/a/d/f;->a:Lorg/b/a/c;

    invoke-virtual {v0, p1, p2, p3}, Lorg/b/a/c;->a(JI)J

    move-result-wide p1

    return-wide p1
.end method

.method public a(JJ)J
    .locals 1

    .line 158
    iget-object v0, p0, Lorg/b/a/d/f;->a:Lorg/b/a/c;

    invoke-virtual {v0, p1, p2, p3, p4}, Lorg/b/a/c;->a(JJ)J

    move-result-wide p1

    return-wide p1
.end method

.method public a(JLjava/lang/String;Ljava/util/Locale;)J
    .locals 1

    .line 190
    iget-object v0, p0, Lorg/b/a/d/f;->a:Lorg/b/a/c;

    invoke-virtual {v0, p1, p2, p3, p4}, Lorg/b/a/c;->a(JLjava/lang/String;Ljava/util/Locale;)J

    move-result-wide p1

    return-wide p1
.end method

.method public a(ILjava/util/Locale;)Ljava/lang/String;
    .locals 1

    .line 130
    iget-object v0, p0, Lorg/b/a/d/f;->a:Lorg/b/a/c;

    invoke-virtual {v0, p1, p2}, Lorg/b/a/c;->a(ILjava/util/Locale;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public a(JLjava/util/Locale;)Ljava/lang/String;
    .locals 1

    .line 114
    iget-object v0, p0, Lorg/b/a/d/f;->a:Lorg/b/a/c;

    invoke-virtual {v0, p1, p2, p3}, Lorg/b/a/c;->a(JLjava/util/Locale;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public a(Lorg/b/a/v;Ljava/util/Locale;)Ljava/lang/String;
    .locals 1

    .line 126
    iget-object v0, p0, Lorg/b/a/d/f;->a:Lorg/b/a/c;

    invoke-virtual {v0, p1, p2}, Lorg/b/a/c;->a(Lorg/b/a/v;Ljava/util/Locale;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public a()Lorg/b/a/d;
    .locals 1

    .line 94
    iget-object v0, p0, Lorg/b/a/d/f;->c:Lorg/b/a/d;

    return-object v0
.end method

.method public b(JI)J
    .locals 1

    .line 186
    iget-object v0, p0, Lorg/b/a/d/f;->a:Lorg/b/a/c;

    invoke-virtual {v0, p1, p2, p3}, Lorg/b/a/c;->b(JI)J

    move-result-wide p1

    return-wide p1
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .line 98
    iget-object v0, p0, Lorg/b/a/d/f;->c:Lorg/b/a/d;

    invoke-virtual {v0}, Lorg/b/a/d;->x()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public b(ILjava/util/Locale;)Ljava/lang/String;
    .locals 1

    .line 150
    iget-object v0, p0, Lorg/b/a/d/f;->a:Lorg/b/a/c;

    invoke-virtual {v0, p1, p2}, Lorg/b/a/c;->b(ILjava/util/Locale;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public b(JLjava/util/Locale;)Ljava/lang/String;
    .locals 1

    .line 134
    iget-object v0, p0, Lorg/b/a/d/f;->a:Lorg/b/a/c;

    invoke-virtual {v0, p1, p2, p3}, Lorg/b/a/c;->b(JLjava/util/Locale;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public b(Lorg/b/a/v;Ljava/util/Locale;)Ljava/lang/String;
    .locals 1

    .line 146
    iget-object v0, p0, Lorg/b/a/d/f;->a:Lorg/b/a/c;

    invoke-virtual {v0, p1, p2}, Lorg/b/a/c;->b(Lorg/b/a/v;Ljava/util/Locale;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public b(J)Z
    .locals 1

    .line 217
    iget-object v0, p0, Lorg/b/a/d/f;->a:Lorg/b/a/c;

    invoke-virtual {v0, p1, p2}, Lorg/b/a/c;->b(J)Z

    move-result p1

    return p1
.end method

.method public c(J)I
    .locals 1

    .line 249
    iget-object v0, p0, Lorg/b/a/d/f;->a:Lorg/b/a/c;

    invoke-virtual {v0, p1, p2}, Lorg/b/a/c;->c(J)I

    move-result p1

    return p1
.end method

.method public c()Z
    .locals 1

    .line 102
    iget-object v0, p0, Lorg/b/a/d/f;->a:Lorg/b/a/c;

    invoke-virtual {v0}, Lorg/b/a/c;->c()Z

    move-result v0

    return v0
.end method

.method public d(J)J
    .locals 1

    .line 269
    iget-object v0, p0, Lorg/b/a/d/f;->a:Lorg/b/a/c;

    invoke-virtual {v0, p1, p2}, Lorg/b/a/c;->d(J)J

    move-result-wide p1

    return-wide p1
.end method

.method public d()Z
    .locals 1

    .line 106
    iget-object v0, p0, Lorg/b/a/d/f;->a:Lorg/b/a/c;

    invoke-virtual {v0}, Lorg/b/a/c;->d()Z

    move-result v0

    return v0
.end method

.method public e(J)J
    .locals 1

    .line 273
    iget-object v0, p0, Lorg/b/a/d/f;->a:Lorg/b/a/c;

    invoke-virtual {v0, p1, p2}, Lorg/b/a/c;->e(J)J

    move-result-wide p1

    return-wide p1
.end method

.method public e()Lorg/b/a/h;
    .locals 1

    .line 206
    iget-object v0, p0, Lorg/b/a/d/f;->a:Lorg/b/a/c;

    invoke-virtual {v0}, Lorg/b/a/c;->e()Lorg/b/a/h;

    move-result-object v0

    return-object v0
.end method

.method public f(J)J
    .locals 1

    .line 277
    iget-object v0, p0, Lorg/b/a/d/f;->a:Lorg/b/a/c;

    invoke-virtual {v0, p1, p2}, Lorg/b/a/c;->f(J)J

    move-result-wide p1

    return-wide p1
.end method

.method public f()Lorg/b/a/h;
    .locals 1

    .line 210
    iget-object v0, p0, Lorg/b/a/d/f;->b:Lorg/b/a/h;

    if-eqz v0, :cond_0

    return-object v0

    .line 213
    :cond_0
    iget-object v0, p0, Lorg/b/a/d/f;->a:Lorg/b/a/c;

    invoke-virtual {v0}, Lorg/b/a/c;->f()Lorg/b/a/h;

    move-result-object v0

    return-object v0
.end method

.method public g(J)J
    .locals 1

    .line 281
    iget-object v0, p0, Lorg/b/a/d/f;->a:Lorg/b/a/c;

    invoke-virtual {v0, p1, p2}, Lorg/b/a/c;->g(J)J

    move-result-wide p1

    return-wide p1
.end method

.method public g()Lorg/b/a/h;
    .locals 1

    .line 225
    iget-object v0, p0, Lorg/b/a/d/f;->a:Lorg/b/a/c;

    invoke-virtual {v0}, Lorg/b/a/c;->g()Lorg/b/a/h;

    move-result-object v0

    return-object v0
.end method

.method public h()I
    .locals 1

    .line 229
    iget-object v0, p0, Lorg/b/a/d/f;->a:Lorg/b/a/c;

    invoke-virtual {v0}, Lorg/b/a/c;->h()I

    move-result v0

    return v0
.end method

.method public h(J)J
    .locals 1

    .line 285
    iget-object v0, p0, Lorg/b/a/d/f;->a:Lorg/b/a/c;

    invoke-virtual {v0, p1, p2}, Lorg/b/a/c;->h(J)J

    move-result-wide p1

    return-wide p1
.end method

.method public i()I
    .locals 1

    .line 245
    iget-object v0, p0, Lorg/b/a/d/f;->a:Lorg/b/a/c;

    invoke-virtual {v0}, Lorg/b/a/c;->i()I

    move-result v0

    return v0
.end method

.method public i(J)J
    .locals 1

    .line 289
    iget-object v0, p0, Lorg/b/a/d/f;->a:Lorg/b/a/c;

    invoke-virtual {v0, p1, p2}, Lorg/b/a/c;->i(J)J

    move-result-wide p1

    return-wide p1
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 293
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "DateTimeField["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lorg/b/a/d/f;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x5d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
