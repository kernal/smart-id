.class public Lorg/b/a/d/l;
.super Lorg/b/a/d/m;
.source "PreciseDateTimeField.java"


# instance fields
.field private final b:I

.field private final c:Lorg/b/a/h;


# direct methods
.method public constructor <init>(Lorg/b/a/d;Lorg/b/a/h;Lorg/b/a/h;)V
    .locals 2

    .line 58
    invoke-direct {p0, p1, p2}, Lorg/b/a/d/m;-><init>(Lorg/b/a/d;Lorg/b/a/h;)V

    .line 60
    invoke-virtual {p3}, Lorg/b/a/h;->c()Z

    move-result p1

    if-eqz p1, :cond_1

    .line 64
    invoke-virtual {p3}, Lorg/b/a/h;->d()J

    move-result-wide p1

    .line 65
    invoke-virtual {p0}, Lorg/b/a/d/l;->j()J

    move-result-wide v0

    div-long/2addr p1, v0

    long-to-int p2, p1

    iput p2, p0, Lorg/b/a/d/l;->b:I

    .line 66
    iget p1, p0, Lorg/b/a/d/l;->b:I

    const/4 p2, 0x2

    if-lt p1, p2, :cond_0

    .line 70
    iput-object p3, p0, Lorg/b/a/d/l;->c:Lorg/b/a/h;

    return-void

    .line 67
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "The effective range must be at least 2"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 61
    :cond_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "Range duration field must be precise"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method


# virtual methods
.method public a(J)I
    .locals 3

    const-wide/16 v0, 0x0

    cmp-long v2, p1, v0

    if-ltz v2, :cond_0

    .line 81
    invoke-virtual {p0}, Lorg/b/a/d/l;->j()J

    move-result-wide v0

    div-long/2addr p1, v0

    iget v0, p0, Lorg/b/a/d/l;->b:I

    int-to-long v0, v0

    rem-long/2addr p1, v0

    long-to-int p2, p1

    return p2

    .line 83
    :cond_0
    iget v0, p0, Lorg/b/a/d/l;->b:I

    add-int/lit8 v0, v0, -0x1

    const-wide/16 v1, 0x1

    add-long/2addr p1, v1

    invoke-virtual {p0}, Lorg/b/a/d/l;->j()J

    move-result-wide v1

    div-long/2addr p1, v1

    iget v1, p0, Lorg/b/a/d/l;->b:I

    int-to-long v1, v1

    rem-long/2addr p1, v1

    long-to-int p2, p1

    add-int/2addr v0, p2

    return v0
.end method

.method public b(JI)J
    .locals 4

    .line 112
    invoke-virtual {p0}, Lorg/b/a/d/l;->h()I

    move-result v0

    invoke-virtual {p0}, Lorg/b/a/d/l;->i()I

    move-result v1

    invoke-static {p0, p3, v0, v1}, Lorg/b/a/d/h;->a(Lorg/b/a/c;III)V

    .line 113
    invoke-virtual {p0, p1, p2}, Lorg/b/a/d/l;->a(J)I

    move-result v0

    sub-int/2addr p3, v0

    int-to-long v0, p3

    iget-wide v2, p0, Lorg/b/a/d/l;->a:J

    mul-long v0, v0, v2

    add-long/2addr p1, v0

    return-wide p1
.end method

.method public f()Lorg/b/a/h;
    .locals 1

    .line 123
    iget-object v0, p0, Lorg/b/a/d/l;->c:Lorg/b/a/h;

    return-object v0
.end method

.method public i()I
    .locals 1

    .line 132
    iget v0, p0, Lorg/b/a/d/l;->b:I

    add-int/lit8 v0, v0, -0x1

    return v0
.end method
