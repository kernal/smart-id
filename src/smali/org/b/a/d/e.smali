.class public Lorg/b/a/d/e;
.super Lorg/b/a/d/c;
.source "DecoratedDurationField.java"


# instance fields
.field private final a:Lorg/b/a/h;


# direct methods
.method public constructor <init>(Lorg/b/a/h;Lorg/b/a/i;)V
    .locals 0

    .line 52
    invoke-direct {p0, p2}, Lorg/b/a/d/c;-><init>(Lorg/b/a/i;)V

    if-eqz p1, :cond_1

    .line 56
    invoke-virtual {p1}, Lorg/b/a/h;->b()Z

    move-result p2

    if-eqz p2, :cond_0

    .line 59
    iput-object p1, p0, Lorg/b/a/d/e;->a:Lorg/b/a/h;

    return-void

    .line 57
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "The field must be supported"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 54
    :cond_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "The field must not be null"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method


# virtual methods
.method public a(JI)J
    .locals 1

    .line 89
    iget-object v0, p0, Lorg/b/a/d/e;->a:Lorg/b/a/h;

    invoke-virtual {v0, p1, p2, p3}, Lorg/b/a/h;->a(JI)J

    move-result-wide p1

    return-wide p1
.end method

.method public a(JJ)J
    .locals 1

    .line 93
    iget-object v0, p0, Lorg/b/a/d/e;->a:Lorg/b/a/h;

    invoke-virtual {v0, p1, p2, p3, p4}, Lorg/b/a/h;->a(JJ)J

    move-result-wide p1

    return-wide p1
.end method

.method public c()Z
    .locals 1

    .line 73
    iget-object v0, p0, Lorg/b/a/d/e;->a:Lorg/b/a/h;

    invoke-virtual {v0}, Lorg/b/a/h;->c()Z

    move-result v0

    return v0
.end method

.method public d()J
    .locals 2

    .line 101
    iget-object v0, p0, Lorg/b/a/d/e;->a:Lorg/b/a/h;

    invoke-virtual {v0}, Lorg/b/a/h;->d()J

    move-result-wide v0

    return-wide v0
.end method

.method public final f()Lorg/b/a/h;
    .locals 1

    .line 69
    iget-object v0, p0, Lorg/b/a/d/e;->a:Lorg/b/a/h;

    return-object v0
.end method
