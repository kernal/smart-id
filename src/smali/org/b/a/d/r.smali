.class public final Lorg/b/a/d/r;
.super Lorg/b/a/d/f;
.source "SkipUndoDateTimeField.java"


# instance fields
.field private final a:Lorg/b/a/a;

.field private final b:I

.field private transient c:I


# direct methods
.method public constructor <init>(Lorg/b/a/a;Lorg/b/a/c;)V
    .locals 1

    const/4 v0, 0x0

    .line 53
    invoke-direct {p0, p1, p2, v0}, Lorg/b/a/d/r;-><init>(Lorg/b/a/a;Lorg/b/a/c;I)V

    return-void
.end method

.method public constructor <init>(Lorg/b/a/a;Lorg/b/a/c;I)V
    .locals 0

    .line 64
    invoke-direct {p0, p2}, Lorg/b/a/d/f;-><init>(Lorg/b/a/c;)V

    .line 65
    iput-object p1, p0, Lorg/b/a/d/r;->a:Lorg/b/a/a;

    .line 66
    invoke-super {p0}, Lorg/b/a/d/f;->h()I

    move-result p1

    if-ge p1, p3, :cond_0

    add-int/lit8 p1, p1, 0x1

    .line 68
    iput p1, p0, Lorg/b/a/d/r;->c:I

    goto :goto_0

    :cond_0
    add-int/lit8 p2, p3, 0x1

    if-ne p1, p2, :cond_1

    .line 70
    iput p3, p0, Lorg/b/a/d/r;->c:I

    goto :goto_0

    .line 72
    :cond_1
    iput p1, p0, Lorg/b/a/d/r;->c:I

    .line 74
    :goto_0
    iput p3, p0, Lorg/b/a/d/r;->b:I

    return-void
.end method


# virtual methods
.method public a(J)I
    .locals 0

    .line 79
    invoke-super {p0, p1, p2}, Lorg/b/a/d/f;->a(J)I

    move-result p1

    .line 80
    iget p2, p0, Lorg/b/a/d/r;->b:I

    if-ge p1, p2, :cond_0

    add-int/lit8 p1, p1, 0x1

    :cond_0
    return p1
.end method

.method public b(JI)J
    .locals 2

    .line 87
    iget v0, p0, Lorg/b/a/d/r;->c:I

    invoke-virtual {p0}, Lorg/b/a/d/r;->i()I

    move-result v1

    invoke-static {p0, p3, v0, v1}, Lorg/b/a/d/h;->a(Lorg/b/a/c;III)V

    .line 88
    iget v0, p0, Lorg/b/a/d/r;->b:I

    if-gt p3, v0, :cond_0

    add-int/lit8 p3, p3, -0x1

    .line 91
    :cond_0
    invoke-super {p0, p1, p2, p3}, Lorg/b/a/d/f;->b(JI)J

    move-result-wide p1

    return-wide p1
.end method

.method public h()I
    .locals 1

    .line 95
    iget v0, p0, Lorg/b/a/d/r;->c:I

    return v0
.end method
