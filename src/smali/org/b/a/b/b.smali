.class public abstract Lorg/b/a/b/b;
.super Lorg/b/a/a;
.source "BaseChronology.java"

# interfaces
.implements Ljava/io/Serializable;


# direct methods
.method protected constructor <init>()V
    .locals 0

    .line 54
    invoke-direct {p0}, Lorg/b/a/a;-><init>()V

    return-void
.end method


# virtual methods
.method public A()Lorg/b/a/c;
    .locals 2

    .line 573
    invoke-static {}, Lorg/b/a/d;->q()Lorg/b/a/d;

    move-result-object v0

    invoke-virtual {p0}, Lorg/b/a/b/b;->y()Lorg/b/a/h;

    move-result-object v1

    invoke-static {v0, v1}, Lorg/b/a/d/s;->a(Lorg/b/a/d;Lorg/b/a/h;)Lorg/b/a/d/s;

    move-result-object v0

    return-object v0
.end method

.method public B()Lorg/b/a/h;
    .locals 1

    .line 584
    invoke-static {}, Lorg/b/a/i;->i()Lorg/b/a/i;

    move-result-object v0

    invoke-static {v0}, Lorg/b/a/d/t;->a(Lorg/b/a/i;)Lorg/b/a/d/t;

    move-result-object v0

    return-object v0
.end method

.method public C()Lorg/b/a/c;
    .locals 2

    .line 593
    invoke-static {}, Lorg/b/a/d;->r()Lorg/b/a/d;

    move-result-object v0

    invoke-virtual {p0}, Lorg/b/a/b/b;->B()Lorg/b/a/h;

    move-result-object v1

    invoke-static {v0, v1}, Lorg/b/a/d/s;->a(Lorg/b/a/d;Lorg/b/a/h;)Lorg/b/a/d/s;

    move-result-object v0

    return-object v0
.end method

.method public D()Lorg/b/a/h;
    .locals 1

    .line 604
    invoke-static {}, Lorg/b/a/i;->j()Lorg/b/a/i;

    move-result-object v0

    invoke-static {v0}, Lorg/b/a/d/t;->a(Lorg/b/a/i;)Lorg/b/a/d/t;

    move-result-object v0

    return-object v0
.end method

.method public E()Lorg/b/a/c;
    .locals 2

    .line 613
    invoke-static {}, Lorg/b/a/d;->s()Lorg/b/a/d;

    move-result-object v0

    invoke-virtual {p0}, Lorg/b/a/b/b;->D()Lorg/b/a/h;

    move-result-object v1

    invoke-static {v0, v1}, Lorg/b/a/d/s;->a(Lorg/b/a/d;Lorg/b/a/h;)Lorg/b/a/d/s;

    move-result-object v0

    return-object v0
.end method

.method public F()Lorg/b/a/c;
    .locals 2

    .line 622
    invoke-static {}, Lorg/b/a/d;->t()Lorg/b/a/d;

    move-result-object v0

    invoke-virtual {p0}, Lorg/b/a/b/b;->D()Lorg/b/a/h;

    move-result-object v1

    invoke-static {v0, v1}, Lorg/b/a/d/s;->a(Lorg/b/a/d;Lorg/b/a/h;)Lorg/b/a/d/s;

    move-result-object v0

    return-object v0
.end method

.method public G()Lorg/b/a/c;
    .locals 2

    .line 631
    invoke-static {}, Lorg/b/a/d;->u()Lorg/b/a/d;

    move-result-object v0

    invoke-virtual {p0}, Lorg/b/a/b/b;->D()Lorg/b/a/h;

    move-result-object v1

    invoke-static {v0, v1}, Lorg/b/a/d/s;->a(Lorg/b/a/d;Lorg/b/a/h;)Lorg/b/a/d/s;

    move-result-object v0

    return-object v0
.end method

.method public H()Lorg/b/a/h;
    .locals 1

    .line 642
    invoke-static {}, Lorg/b/a/i;->k()Lorg/b/a/i;

    move-result-object v0

    invoke-static {v0}, Lorg/b/a/d/t;->a(Lorg/b/a/i;)Lorg/b/a/d/t;

    move-result-object v0

    return-object v0
.end method

.method public I()Lorg/b/a/c;
    .locals 2

    .line 651
    invoke-static {}, Lorg/b/a/d;->v()Lorg/b/a/d;

    move-result-object v0

    invoke-virtual {p0}, Lorg/b/a/b/b;->H()Lorg/b/a/h;

    move-result-object v1

    invoke-static {v0, v1}, Lorg/b/a/d/s;->a(Lorg/b/a/d;Lorg/b/a/h;)Lorg/b/a/d/s;

    move-result-object v0

    return-object v0
.end method

.method public J()Lorg/b/a/h;
    .locals 1

    .line 662
    invoke-static {}, Lorg/b/a/i;->l()Lorg/b/a/i;

    move-result-object v0

    invoke-static {v0}, Lorg/b/a/d/t;->a(Lorg/b/a/i;)Lorg/b/a/d/t;

    move-result-object v0

    return-object v0
.end method

.method public K()Lorg/b/a/c;
    .locals 2

    .line 671
    invoke-static {}, Lorg/b/a/d;->w()Lorg/b/a/d;

    move-result-object v0

    invoke-virtual {p0}, Lorg/b/a/b/b;->J()Lorg/b/a/h;

    move-result-object v1

    invoke-static {v0, v1}, Lorg/b/a/d/s;->a(Lorg/b/a/d;Lorg/b/a/h;)Lorg/b/a/d/s;

    move-result-object v0

    return-object v0
.end method

.method public a(IIII)J
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .line 102
    invoke-virtual {p0}, Lorg/b/a/b/b;->E()Lorg/b/a/c;

    move-result-object v0

    const-wide/16 v1, 0x0

    invoke-virtual {v0, v1, v2, p1}, Lorg/b/a/c;->b(JI)J

    move-result-wide v0

    .line 103
    invoke-virtual {p0}, Lorg/b/a/b/b;->C()Lorg/b/a/c;

    move-result-object p1

    invoke-virtual {p1, v0, v1, p2}, Lorg/b/a/c;->b(JI)J

    move-result-wide p1

    .line 104
    invoke-virtual {p0}, Lorg/b/a/b/b;->u()Lorg/b/a/c;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Lorg/b/a/c;->b(JI)J

    move-result-wide p1

    .line 105
    invoke-virtual {p0}, Lorg/b/a/b/b;->e()Lorg/b/a/c;

    move-result-object p3

    invoke-virtual {p3, p1, p2, p4}, Lorg/b/a/c;->b(JI)J

    move-result-wide p1

    return-wide p1
.end method

.method public a(IIIIIII)J
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .line 132
    invoke-virtual {p0}, Lorg/b/a/b/b;->E()Lorg/b/a/c;

    move-result-object v0

    const-wide/16 v1, 0x0

    invoke-virtual {v0, v1, v2, p1}, Lorg/b/a/c;->b(JI)J

    move-result-wide v0

    .line 133
    invoke-virtual {p0}, Lorg/b/a/b/b;->C()Lorg/b/a/c;

    move-result-object p1

    invoke-virtual {p1, v0, v1, p2}, Lorg/b/a/c;->b(JI)J

    move-result-wide p1

    .line 134
    invoke-virtual {p0}, Lorg/b/a/b/b;->u()Lorg/b/a/c;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Lorg/b/a/c;->b(JI)J

    move-result-wide p1

    .line 135
    invoke-virtual {p0}, Lorg/b/a/b/b;->m()Lorg/b/a/c;

    move-result-object p3

    invoke-virtual {p3, p1, p2, p4}, Lorg/b/a/c;->b(JI)J

    move-result-wide p1

    .line 136
    invoke-virtual {p0}, Lorg/b/a/b/b;->j()Lorg/b/a/c;

    move-result-object p3

    invoke-virtual {p3, p1, p2, p5}, Lorg/b/a/c;->b(JI)J

    move-result-wide p1

    .line 137
    invoke-virtual {p0}, Lorg/b/a/b/b;->g()Lorg/b/a/c;

    move-result-object p3

    invoke-virtual {p3, p1, p2, p6}, Lorg/b/a/c;->b(JI)J

    move-result-wide p1

    .line 138
    invoke-virtual {p0}, Lorg/b/a/b/b;->d()Lorg/b/a/c;

    move-result-object p3

    invoke-virtual {p3, p1, p2, p7}, Lorg/b/a/c;->b(JI)J

    move-result-wide p1

    return-wide p1
.end method

.method public a(Lorg/b/a/v;J)J
    .locals 4

    .line 239
    invoke-interface {p1}, Lorg/b/a/v;->a()I

    move-result v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_0

    .line 240
    invoke-interface {p1, v1}, Lorg/b/a/v;->b(I)Lorg/b/a/d;

    move-result-object v2

    invoke-virtual {v2, p0}, Lorg/b/a/d;->a(Lorg/b/a/a;)Lorg/b/a/c;

    move-result-object v2

    invoke-interface {p1, v1}, Lorg/b/a/v;->a(I)I

    move-result v3

    invoke-virtual {v2, p2, p3, v3}, Lorg/b/a/c;->b(JI)J

    move-result-wide p2

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return-wide p2
.end method

.method public c()Lorg/b/a/h;
    .locals 1

    .line 339
    invoke-static {}, Lorg/b/a/i;->a()Lorg/b/a/i;

    move-result-object v0

    invoke-static {v0}, Lorg/b/a/d/t;->a(Lorg/b/a/i;)Lorg/b/a/d/t;

    move-result-object v0

    return-object v0
.end method

.method public d()Lorg/b/a/c;
    .locals 2

    .line 348
    invoke-static {}, Lorg/b/a/d;->a()Lorg/b/a/d;

    move-result-object v0

    invoke-virtual {p0}, Lorg/b/a/b/b;->c()Lorg/b/a/h;

    move-result-object v1

    invoke-static {v0, v1}, Lorg/b/a/d/s;->a(Lorg/b/a/d;Lorg/b/a/h;)Lorg/b/a/d/s;

    move-result-object v0

    return-object v0
.end method

.method public e()Lorg/b/a/c;
    .locals 2

    .line 357
    invoke-static {}, Lorg/b/a/d;->b()Lorg/b/a/d;

    move-result-object v0

    invoke-virtual {p0}, Lorg/b/a/b/b;->c()Lorg/b/a/h;

    move-result-object v1

    invoke-static {v0, v1}, Lorg/b/a/d/s;->a(Lorg/b/a/d;Lorg/b/a/h;)Lorg/b/a/d/s;

    move-result-object v0

    return-object v0
.end method

.method public f()Lorg/b/a/h;
    .locals 1

    .line 368
    invoke-static {}, Lorg/b/a/i;->b()Lorg/b/a/i;

    move-result-object v0

    invoke-static {v0}, Lorg/b/a/d/t;->a(Lorg/b/a/i;)Lorg/b/a/d/t;

    move-result-object v0

    return-object v0
.end method

.method public g()Lorg/b/a/c;
    .locals 2

    .line 377
    invoke-static {}, Lorg/b/a/d;->c()Lorg/b/a/d;

    move-result-object v0

    invoke-virtual {p0}, Lorg/b/a/b/b;->f()Lorg/b/a/h;

    move-result-object v1

    invoke-static {v0, v1}, Lorg/b/a/d/s;->a(Lorg/b/a/d;Lorg/b/a/h;)Lorg/b/a/d/s;

    move-result-object v0

    return-object v0
.end method

.method public h()Lorg/b/a/c;
    .locals 2

    .line 386
    invoke-static {}, Lorg/b/a/d;->d()Lorg/b/a/d;

    move-result-object v0

    invoke-virtual {p0}, Lorg/b/a/b/b;->f()Lorg/b/a/h;

    move-result-object v1

    invoke-static {v0, v1}, Lorg/b/a/d/s;->a(Lorg/b/a/d;Lorg/b/a/h;)Lorg/b/a/d/s;

    move-result-object v0

    return-object v0
.end method

.method public i()Lorg/b/a/h;
    .locals 1

    .line 397
    invoke-static {}, Lorg/b/a/i;->c()Lorg/b/a/i;

    move-result-object v0

    invoke-static {v0}, Lorg/b/a/d/t;->a(Lorg/b/a/i;)Lorg/b/a/d/t;

    move-result-object v0

    return-object v0
.end method

.method public j()Lorg/b/a/c;
    .locals 2

    .line 406
    invoke-static {}, Lorg/b/a/d;->e()Lorg/b/a/d;

    move-result-object v0

    invoke-virtual {p0}, Lorg/b/a/b/b;->i()Lorg/b/a/h;

    move-result-object v1

    invoke-static {v0, v1}, Lorg/b/a/d/s;->a(Lorg/b/a/d;Lorg/b/a/h;)Lorg/b/a/d/s;

    move-result-object v0

    return-object v0
.end method

.method public k()Lorg/b/a/c;
    .locals 2

    .line 415
    invoke-static {}, Lorg/b/a/d;->f()Lorg/b/a/d;

    move-result-object v0

    invoke-virtual {p0}, Lorg/b/a/b/b;->i()Lorg/b/a/h;

    move-result-object v1

    invoke-static {v0, v1}, Lorg/b/a/d/s;->a(Lorg/b/a/d;Lorg/b/a/h;)Lorg/b/a/d/s;

    move-result-object v0

    return-object v0
.end method

.method public l()Lorg/b/a/h;
    .locals 1

    .line 426
    invoke-static {}, Lorg/b/a/i;->d()Lorg/b/a/i;

    move-result-object v0

    invoke-static {v0}, Lorg/b/a/d/t;->a(Lorg/b/a/i;)Lorg/b/a/d/t;

    move-result-object v0

    return-object v0
.end method

.method public m()Lorg/b/a/c;
    .locals 2

    .line 435
    invoke-static {}, Lorg/b/a/d;->g()Lorg/b/a/d;

    move-result-object v0

    invoke-virtual {p0}, Lorg/b/a/b/b;->l()Lorg/b/a/h;

    move-result-object v1

    invoke-static {v0, v1}, Lorg/b/a/d/s;->a(Lorg/b/a/d;Lorg/b/a/h;)Lorg/b/a/d/s;

    move-result-object v0

    return-object v0
.end method

.method public n()Lorg/b/a/c;
    .locals 2

    .line 444
    invoke-static {}, Lorg/b/a/d;->h()Lorg/b/a/d;

    move-result-object v0

    invoke-virtual {p0}, Lorg/b/a/b/b;->l()Lorg/b/a/h;

    move-result-object v1

    invoke-static {v0, v1}, Lorg/b/a/d/s;->a(Lorg/b/a/d;Lorg/b/a/h;)Lorg/b/a/d/s;

    move-result-object v0

    return-object v0
.end method

.method public o()Lorg/b/a/h;
    .locals 1

    .line 455
    invoke-static {}, Lorg/b/a/i;->e()Lorg/b/a/i;

    move-result-object v0

    invoke-static {v0}, Lorg/b/a/d/t;->a(Lorg/b/a/i;)Lorg/b/a/d/t;

    move-result-object v0

    return-object v0
.end method

.method public p()Lorg/b/a/c;
    .locals 2

    .line 464
    invoke-static {}, Lorg/b/a/d;->i()Lorg/b/a/d;

    move-result-object v0

    invoke-virtual {p0}, Lorg/b/a/b/b;->l()Lorg/b/a/h;

    move-result-object v1

    invoke-static {v0, v1}, Lorg/b/a/d/s;->a(Lorg/b/a/d;Lorg/b/a/h;)Lorg/b/a/d/s;

    move-result-object v0

    return-object v0
.end method

.method public q()Lorg/b/a/c;
    .locals 2

    .line 473
    invoke-static {}, Lorg/b/a/d;->j()Lorg/b/a/d;

    move-result-object v0

    invoke-virtual {p0}, Lorg/b/a/b/b;->l()Lorg/b/a/h;

    move-result-object v1

    invoke-static {v0, v1}, Lorg/b/a/d/s;->a(Lorg/b/a/d;Lorg/b/a/h;)Lorg/b/a/d/s;

    move-result-object v0

    return-object v0
.end method

.method public r()Lorg/b/a/c;
    .locals 2

    .line 482
    invoke-static {}, Lorg/b/a/d;->k()Lorg/b/a/d;

    move-result-object v0

    invoke-virtual {p0}, Lorg/b/a/b/b;->o()Lorg/b/a/h;

    move-result-object v1

    invoke-static {v0, v1}, Lorg/b/a/d/s;->a(Lorg/b/a/d;Lorg/b/a/h;)Lorg/b/a/d/s;

    move-result-object v0

    return-object v0
.end method

.method public s()Lorg/b/a/h;
    .locals 1

    .line 493
    invoke-static {}, Lorg/b/a/i;->f()Lorg/b/a/i;

    move-result-object v0

    invoke-static {v0}, Lorg/b/a/d/t;->a(Lorg/b/a/i;)Lorg/b/a/d/t;

    move-result-object v0

    return-object v0
.end method

.method public t()Lorg/b/a/c;
    .locals 2

    .line 506
    invoke-static {}, Lorg/b/a/d;->l()Lorg/b/a/d;

    move-result-object v0

    invoke-virtual {p0}, Lorg/b/a/b/b;->s()Lorg/b/a/h;

    move-result-object v1

    invoke-static {v0, v1}, Lorg/b/a/d/s;->a(Lorg/b/a/d;Lorg/b/a/h;)Lorg/b/a/d/s;

    move-result-object v0

    return-object v0
.end method

.method public u()Lorg/b/a/c;
    .locals 2

    .line 515
    invoke-static {}, Lorg/b/a/d;->m()Lorg/b/a/d;

    move-result-object v0

    invoke-virtual {p0}, Lorg/b/a/b/b;->s()Lorg/b/a/h;

    move-result-object v1

    invoke-static {v0, v1}, Lorg/b/a/d/s;->a(Lorg/b/a/d;Lorg/b/a/h;)Lorg/b/a/d/s;

    move-result-object v0

    return-object v0
.end method

.method public v()Lorg/b/a/c;
    .locals 2

    .line 524
    invoke-static {}, Lorg/b/a/d;->n()Lorg/b/a/d;

    move-result-object v0

    invoke-virtual {p0}, Lorg/b/a/b/b;->s()Lorg/b/a/h;

    move-result-object v1

    invoke-static {v0, v1}, Lorg/b/a/d/s;->a(Lorg/b/a/d;Lorg/b/a/h;)Lorg/b/a/d/s;

    move-result-object v0

    return-object v0
.end method

.method public w()Lorg/b/a/h;
    .locals 1

    .line 535
    invoke-static {}, Lorg/b/a/i;->g()Lorg/b/a/i;

    move-result-object v0

    invoke-static {v0}, Lorg/b/a/d/t;->a(Lorg/b/a/i;)Lorg/b/a/d/t;

    move-result-object v0

    return-object v0
.end method

.method public x()Lorg/b/a/c;
    .locals 2

    .line 544
    invoke-static {}, Lorg/b/a/d;->o()Lorg/b/a/d;

    move-result-object v0

    invoke-virtual {p0}, Lorg/b/a/b/b;->w()Lorg/b/a/h;

    move-result-object v1

    invoke-static {v0, v1}, Lorg/b/a/d/s;->a(Lorg/b/a/d;Lorg/b/a/h;)Lorg/b/a/d/s;

    move-result-object v0

    return-object v0
.end method

.method public y()Lorg/b/a/h;
    .locals 1

    .line 555
    invoke-static {}, Lorg/b/a/i;->h()Lorg/b/a/i;

    move-result-object v0

    invoke-static {v0}, Lorg/b/a/d/t;->a(Lorg/b/a/i;)Lorg/b/a/d/t;

    move-result-object v0

    return-object v0
.end method

.method public z()Lorg/b/a/c;
    .locals 2

    .line 564
    invoke-static {}, Lorg/b/a/d;->p()Lorg/b/a/d;

    move-result-object v0

    invoke-virtual {p0}, Lorg/b/a/b/b;->y()Lorg/b/a/h;

    move-result-object v1

    invoke-static {v0, v1}, Lorg/b/a/d/s;->a(Lorg/b/a/d;Lorg/b/a/h;)Lorg/b/a/d/s;

    move-result-object v0

    return-object v0
.end method
