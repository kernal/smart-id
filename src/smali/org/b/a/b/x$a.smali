.class Lorg/b/a/b/x$a;
.super Lorg/b/a/d/d;
.source "LimitChronology.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/b/a/b/x;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "a"
.end annotation


# instance fields
.field final synthetic a:Lorg/b/a/b/x;

.field private final b:Lorg/b/a/h;

.field private final c:Lorg/b/a/h;

.field private final d:Lorg/b/a/h;


# direct methods
.method constructor <init>(Lorg/b/a/b/x;Lorg/b/a/c;Lorg/b/a/h;Lorg/b/a/h;Lorg/b/a/h;)V
    .locals 0

    .line 452
    iput-object p1, p0, Lorg/b/a/b/x$a;->a:Lorg/b/a/b/x;

    .line 453
    invoke-virtual {p2}, Lorg/b/a/c;->a()Lorg/b/a/d;

    move-result-object p1

    invoke-direct {p0, p2, p1}, Lorg/b/a/d/d;-><init>(Lorg/b/a/c;Lorg/b/a/d;)V

    .line 454
    iput-object p3, p0, Lorg/b/a/b/x$a;->b:Lorg/b/a/h;

    .line 455
    iput-object p4, p0, Lorg/b/a/b/x$a;->c:Lorg/b/a/h;

    .line 456
    iput-object p5, p0, Lorg/b/a/b/x$a;->d:Lorg/b/a/h;

    return-void
.end method


# virtual methods
.method public a(J)I
    .locals 2

    .line 460
    iget-object v0, p0, Lorg/b/a/b/x$a;->a:Lorg/b/a/b/x;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, p2, v1}, Lorg/b/a/b/x;->a(JLjava/lang/String;)V

    .line 461
    invoke-virtual {p0}, Lorg/b/a/b/x$a;->j()Lorg/b/a/c;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lorg/b/a/c;->a(J)I

    move-result p1

    return p1
.end method

.method public a(Ljava/util/Locale;)I
    .locals 1

    .line 596
    invoke-virtual {p0}, Lorg/b/a/b/x$a;->j()Lorg/b/a/c;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/b/a/c;->a(Ljava/util/Locale;)I

    move-result p1

    return p1
.end method

.method public a(JI)J
    .locals 2

    .line 475
    iget-object v0, p0, Lorg/b/a/b/x$a;->a:Lorg/b/a/b/x;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, p2, v1}, Lorg/b/a/b/x;->a(JLjava/lang/String;)V

    .line 476
    invoke-virtual {p0}, Lorg/b/a/b/x$a;->j()Lorg/b/a/c;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Lorg/b/a/c;->a(JI)J

    move-result-wide p1

    .line 477
    iget-object p3, p0, Lorg/b/a/b/x$a;->a:Lorg/b/a/b/x;

    const-string v0, "resulting"

    invoke-virtual {p3, p1, p2, v0}, Lorg/b/a/b/x;->a(JLjava/lang/String;)V

    return-wide p1
.end method

.method public a(JJ)J
    .locals 2

    .line 482
    iget-object v0, p0, Lorg/b/a/b/x$a;->a:Lorg/b/a/b/x;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, p2, v1}, Lorg/b/a/b/x;->a(JLjava/lang/String;)V

    .line 483
    invoke-virtual {p0}, Lorg/b/a/b/x$a;->j()Lorg/b/a/c;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3, p4}, Lorg/b/a/c;->a(JJ)J

    move-result-wide p1

    .line 484
    iget-object p3, p0, Lorg/b/a/b/x$a;->a:Lorg/b/a/b/x;

    const-string p4, "resulting"

    invoke-virtual {p3, p1, p2, p4}, Lorg/b/a/b/x;->a(JLjava/lang/String;)V

    return-wide p1
.end method

.method public a(JLjava/lang/String;Ljava/util/Locale;)J
    .locals 2

    .line 515
    iget-object v0, p0, Lorg/b/a/b/x$a;->a:Lorg/b/a/b/x;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, p2, v1}, Lorg/b/a/b/x;->a(JLjava/lang/String;)V

    .line 516
    invoke-virtual {p0}, Lorg/b/a/b/x$a;->j()Lorg/b/a/c;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3, p4}, Lorg/b/a/c;->a(JLjava/lang/String;Ljava/util/Locale;)J

    move-result-wide p1

    .line 517
    iget-object p3, p0, Lorg/b/a/b/x$a;->a:Lorg/b/a/b/x;

    const-string p4, "resulting"

    invoke-virtual {p3, p1, p2, p4}, Lorg/b/a/b/x;->a(JLjava/lang/String;)V

    return-wide p1
.end method

.method public a(JLjava/util/Locale;)Ljava/lang/String;
    .locals 2

    .line 465
    iget-object v0, p0, Lorg/b/a/b/x$a;->a:Lorg/b/a/b/x;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, p2, v1}, Lorg/b/a/b/x;->a(JLjava/lang/String;)V

    .line 466
    invoke-virtual {p0}, Lorg/b/a/b/x$a;->j()Lorg/b/a/c;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Lorg/b/a/c;->a(JLjava/util/Locale;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public b(JI)J
    .locals 2

    .line 508
    iget-object v0, p0, Lorg/b/a/b/x$a;->a:Lorg/b/a/b/x;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, p2, v1}, Lorg/b/a/b/x;->a(JLjava/lang/String;)V

    .line 509
    invoke-virtual {p0}, Lorg/b/a/b/x$a;->j()Lorg/b/a/c;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Lorg/b/a/c;->b(JI)J

    move-result-wide p1

    .line 510
    iget-object p3, p0, Lorg/b/a/b/x$a;->a:Lorg/b/a/b/x;

    const-string v0, "resulting"

    invoke-virtual {p3, p1, p2, v0}, Lorg/b/a/b/x;->a(JLjava/lang/String;)V

    return-wide p1
.end method

.method public b(JLjava/util/Locale;)Ljava/lang/String;
    .locals 2

    .line 470
    iget-object v0, p0, Lorg/b/a/b/x$a;->a:Lorg/b/a/b/x;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, p2, v1}, Lorg/b/a/b/x;->a(JLjava/lang/String;)V

    .line 471
    invoke-virtual {p0}, Lorg/b/a/b/x$a;->j()Lorg/b/a/c;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Lorg/b/a/c;->b(JLjava/util/Locale;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public b(J)Z
    .locals 2

    .line 530
    iget-object v0, p0, Lorg/b/a/b/x$a;->a:Lorg/b/a/b/x;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, p2, v1}, Lorg/b/a/b/x;->a(JLjava/lang/String;)V

    .line 531
    invoke-virtual {p0}, Lorg/b/a/b/x$a;->j()Lorg/b/a/c;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lorg/b/a/c;->b(J)Z

    move-result p1

    return p1
.end method

.method public c(J)I
    .locals 2

    .line 591
    iget-object v0, p0, Lorg/b/a/b/x$a;->a:Lorg/b/a/b/x;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, p2, v1}, Lorg/b/a/b/x;->a(JLjava/lang/String;)V

    .line 592
    invoke-virtual {p0}, Lorg/b/a/b/x$a;->j()Lorg/b/a/c;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lorg/b/a/c;->c(J)I

    move-result p1

    return p1
.end method

.method public d(J)J
    .locals 2

    .line 544
    iget-object v0, p0, Lorg/b/a/b/x$a;->a:Lorg/b/a/b/x;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, p2, v1}, Lorg/b/a/b/x;->a(JLjava/lang/String;)V

    .line 545
    invoke-virtual {p0}, Lorg/b/a/b/x$a;->j()Lorg/b/a/c;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lorg/b/a/c;->d(J)J

    move-result-wide p1

    .line 546
    iget-object v0, p0, Lorg/b/a/b/x$a;->a:Lorg/b/a/b/x;

    const-string v1, "resulting"

    invoke-virtual {v0, p1, p2, v1}, Lorg/b/a/b/x;->a(JLjava/lang/String;)V

    return-wide p1
.end method

.method public e(J)J
    .locals 2

    .line 551
    iget-object v0, p0, Lorg/b/a/b/x$a;->a:Lorg/b/a/b/x;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, p2, v1}, Lorg/b/a/b/x;->a(JLjava/lang/String;)V

    .line 552
    invoke-virtual {p0}, Lorg/b/a/b/x$a;->j()Lorg/b/a/c;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lorg/b/a/c;->e(J)J

    move-result-wide p1

    .line 553
    iget-object v0, p0, Lorg/b/a/b/x$a;->a:Lorg/b/a/b/x;

    const-string v1, "resulting"

    invoke-virtual {v0, p1, p2, v1}, Lorg/b/a/b/x;->a(JLjava/lang/String;)V

    return-wide p1
.end method

.method public final e()Lorg/b/a/h;
    .locals 1

    .line 522
    iget-object v0, p0, Lorg/b/a/b/x$a;->b:Lorg/b/a/h;

    return-object v0
.end method

.method public f(J)J
    .locals 2

    .line 558
    iget-object v0, p0, Lorg/b/a/b/x$a;->a:Lorg/b/a/b/x;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, p2, v1}, Lorg/b/a/b/x;->a(JLjava/lang/String;)V

    .line 559
    invoke-virtual {p0}, Lorg/b/a/b/x$a;->j()Lorg/b/a/c;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lorg/b/a/c;->f(J)J

    move-result-wide p1

    .line 560
    iget-object v0, p0, Lorg/b/a/b/x$a;->a:Lorg/b/a/b/x;

    const-string v1, "resulting"

    invoke-virtual {v0, p1, p2, v1}, Lorg/b/a/b/x;->a(JLjava/lang/String;)V

    return-wide p1
.end method

.method public final f()Lorg/b/a/h;
    .locals 1

    .line 526
    iget-object v0, p0, Lorg/b/a/b/x$a;->c:Lorg/b/a/h;

    return-object v0
.end method

.method public g(J)J
    .locals 2

    .line 565
    iget-object v0, p0, Lorg/b/a/b/x$a;->a:Lorg/b/a/b/x;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, p2, v1}, Lorg/b/a/b/x;->a(JLjava/lang/String;)V

    .line 566
    invoke-virtual {p0}, Lorg/b/a/b/x$a;->j()Lorg/b/a/c;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lorg/b/a/c;->g(J)J

    move-result-wide p1

    .line 567
    iget-object v0, p0, Lorg/b/a/b/x$a;->a:Lorg/b/a/b/x;

    const-string v1, "resulting"

    invoke-virtual {v0, p1, p2, v1}, Lorg/b/a/b/x;->a(JLjava/lang/String;)V

    return-wide p1
.end method

.method public final g()Lorg/b/a/h;
    .locals 1

    .line 540
    iget-object v0, p0, Lorg/b/a/b/x$a;->d:Lorg/b/a/h;

    return-object v0
.end method

.method public h(J)J
    .locals 2

    .line 572
    iget-object v0, p0, Lorg/b/a/b/x$a;->a:Lorg/b/a/b/x;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, p2, v1}, Lorg/b/a/b/x;->a(JLjava/lang/String;)V

    .line 573
    invoke-virtual {p0}, Lorg/b/a/b/x$a;->j()Lorg/b/a/c;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lorg/b/a/c;->h(J)J

    move-result-wide p1

    .line 574
    iget-object v0, p0, Lorg/b/a/b/x$a;->a:Lorg/b/a/b/x;

    const-string v1, "resulting"

    invoke-virtual {v0, p1, p2, v1}, Lorg/b/a/b/x;->a(JLjava/lang/String;)V

    return-wide p1
.end method

.method public i(J)J
    .locals 2

    .line 579
    iget-object v0, p0, Lorg/b/a/b/x$a;->a:Lorg/b/a/b/x;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, p2, v1}, Lorg/b/a/b/x;->a(JLjava/lang/String;)V

    .line 580
    invoke-virtual {p0}, Lorg/b/a/b/x$a;->j()Lorg/b/a/c;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lorg/b/a/c;->i(J)J

    move-result-wide p1

    .line 581
    iget-object v0, p0, Lorg/b/a/b/x$a;->a:Lorg/b/a/b/x;

    const-string v1, "resulting"

    invoke-virtual {v0, p1, p2, v1}, Lorg/b/a/b/x;->a(JLjava/lang/String;)V

    return-wide p1
.end method
