.class Lorg/b/a/b/n$a;
.super Lorg/b/a/d/b;
.source "GJChronology.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/b/a/b/n;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "a"
.end annotation


# instance fields
.field final a:Lorg/b/a/c;

.field final b:Lorg/b/a/c;

.field final c:J

.field final d:Z

.field protected e:Lorg/b/a/h;

.field protected f:Lorg/b/a/h;

.field final synthetic g:Lorg/b/a/b/n;


# direct methods
.method constructor <init>(Lorg/b/a/b/n;Lorg/b/a/c;Lorg/b/a/c;J)V
    .locals 7

    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-wide v4, p4

    .line 615
    invoke-direct/range {v0 .. v6}, Lorg/b/a/b/n$a;-><init>(Lorg/b/a/b/n;Lorg/b/a/c;Lorg/b/a/c;JZ)V

    return-void
.end method

.method constructor <init>(Lorg/b/a/b/n;Lorg/b/a/c;Lorg/b/a/c;JZ)V
    .locals 8

    const/4 v4, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-wide v5, p4

    move v7, p6

    .line 626
    invoke-direct/range {v0 .. v7}, Lorg/b/a/b/n$a;-><init>(Lorg/b/a/b/n;Lorg/b/a/c;Lorg/b/a/c;Lorg/b/a/h;JZ)V

    return-void
.end method

.method constructor <init>(Lorg/b/a/b/n;Lorg/b/a/c;Lorg/b/a/c;Lorg/b/a/h;JZ)V
    .locals 0

    .line 637
    iput-object p1, p0, Lorg/b/a/b/n$a;->g:Lorg/b/a/b/n;

    .line 638
    invoke-virtual {p3}, Lorg/b/a/c;->a()Lorg/b/a/d;

    move-result-object p1

    invoke-direct {p0, p1}, Lorg/b/a/d/b;-><init>(Lorg/b/a/d;)V

    .line 639
    iput-object p2, p0, Lorg/b/a/b/n$a;->a:Lorg/b/a/c;

    .line 640
    iput-object p3, p0, Lorg/b/a/b/n$a;->b:Lorg/b/a/c;

    .line 641
    iput-wide p5, p0, Lorg/b/a/b/n$a;->c:J

    .line 642
    iput-boolean p7, p0, Lorg/b/a/b/n$a;->d:Z

    .line 645
    invoke-virtual {p3}, Lorg/b/a/c;->e()Lorg/b/a/h;

    move-result-object p1

    iput-object p1, p0, Lorg/b/a/b/n$a;->e:Lorg/b/a/h;

    if-nez p4, :cond_0

    .line 647
    invoke-virtual {p3}, Lorg/b/a/c;->f()Lorg/b/a/h;

    move-result-object p4

    if-nez p4, :cond_0

    .line 649
    invoke-virtual {p2}, Lorg/b/a/c;->f()Lorg/b/a/h;

    move-result-object p4

    .line 652
    :cond_0
    iput-object p4, p0, Lorg/b/a/b/n$a;->f:Lorg/b/a/h;

    return-void
.end method


# virtual methods
.method public a(J)I
    .locals 3

    .line 660
    iget-wide v0, p0, Lorg/b/a/b/n$a;->c:J

    cmp-long v2, p1, v0

    if-ltz v2, :cond_0

    .line 661
    iget-object v0, p0, Lorg/b/a/b/n$a;->b:Lorg/b/a/c;

    invoke-virtual {v0, p1, p2}, Lorg/b/a/c;->a(J)I

    move-result p1

    return p1

    .line 663
    :cond_0
    iget-object v0, p0, Lorg/b/a/b/n$a;->a:Lorg/b/a/c;

    invoke-virtual {v0, p1, p2}, Lorg/b/a/c;->a(J)I

    move-result p1

    return p1
.end method

.method public a(Ljava/util/Locale;)I
    .locals 2

    .line 910
    iget-object v0, p0, Lorg/b/a/b/n$a;->a:Lorg/b/a/c;

    invoke-virtual {v0, p1}, Lorg/b/a/c;->a(Ljava/util/Locale;)I

    move-result v0

    iget-object v1, p0, Lorg/b/a/b/n$a;->b:Lorg/b/a/c;

    .line 911
    invoke-virtual {v1, p1}, Lorg/b/a/c;->a(Ljava/util/Locale;)I

    move-result p1

    .line 910
    invoke-static {v0, p1}, Ljava/lang/Math;->max(II)I

    move-result p1

    return p1
.end method

.method public a(JI)J
    .locals 1

    .line 692
    iget-object v0, p0, Lorg/b/a/b/n$a;->b:Lorg/b/a/c;

    invoke-virtual {v0, p1, p2, p3}, Lorg/b/a/c;->a(JI)J

    move-result-wide p1

    return-wide p1
.end method

.method public a(JJ)J
    .locals 1

    .line 696
    iget-object v0, p0, Lorg/b/a/b/n$a;->b:Lorg/b/a/c;

    invoke-virtual {v0, p1, p2, p3, p4}, Lorg/b/a/c;->a(JJ)J

    move-result-wide p1

    return-wide p1
.end method

.method public a(JLjava/lang/String;Ljava/util/Locale;)J
    .locals 3

    .line 757
    iget-wide v0, p0, Lorg/b/a/b/n$a;->c:J

    cmp-long v2, p1, v0

    if-ltz v2, :cond_0

    .line 758
    iget-object v0, p0, Lorg/b/a/b/n$a;->b:Lorg/b/a/c;

    invoke-virtual {v0, p1, p2, p3, p4}, Lorg/b/a/c;->a(JLjava/lang/String;Ljava/util/Locale;)J

    move-result-wide p1

    .line 759
    iget-wide p3, p0, Lorg/b/a/b/n$a;->c:J

    cmp-long v0, p1, p3

    if-gez v0, :cond_1

    .line 761
    iget-object p3, p0, Lorg/b/a/b/n$a;->g:Lorg/b/a/b/n;

    invoke-static {p3}, Lorg/b/a/b/n;->a(Lorg/b/a/b/n;)J

    move-result-wide p3

    add-long/2addr p3, p1

    iget-wide v0, p0, Lorg/b/a/b/n$a;->c:J

    cmp-long v2, p3, v0

    if-gez v2, :cond_1

    .line 762
    invoke-virtual {p0, p1, p2}, Lorg/b/a/b/n$a;->k(J)J

    move-result-wide p1

    goto :goto_0

    .line 767
    :cond_0
    iget-object v0, p0, Lorg/b/a/b/n$a;->a:Lorg/b/a/c;

    invoke-virtual {v0, p1, p2, p3, p4}, Lorg/b/a/c;->a(JLjava/lang/String;Ljava/util/Locale;)J

    move-result-wide p1

    .line 768
    iget-wide p3, p0, Lorg/b/a/b/n$a;->c:J

    cmp-long v0, p1, p3

    if-ltz v0, :cond_1

    .line 770
    iget-object p3, p0, Lorg/b/a/b/n$a;->g:Lorg/b/a/b/n;

    invoke-static {p3}, Lorg/b/a/b/n;->a(Lorg/b/a/b/n;)J

    move-result-wide p3

    sub-long p3, p1, p3

    iget-wide v0, p0, Lorg/b/a/b/n$a;->c:J

    cmp-long v2, p3, v0

    if-ltz v2, :cond_1

    .line 771
    invoke-virtual {p0, p1, p2}, Lorg/b/a/b/n$a;->j(J)J

    move-result-wide p1

    :cond_1
    :goto_0
    return-wide p1
.end method

.method public a(ILjava/util/Locale;)Ljava/lang/String;
    .locals 1

    .line 676
    iget-object v0, p0, Lorg/b/a/b/n$a;->b:Lorg/b/a/c;

    invoke-virtual {v0, p1, p2}, Lorg/b/a/c;->a(ILjava/util/Locale;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public a(JLjava/util/Locale;)Ljava/lang/String;
    .locals 3

    .line 668
    iget-wide v0, p0, Lorg/b/a/b/n$a;->c:J

    cmp-long v2, p1, v0

    if-ltz v2, :cond_0

    .line 669
    iget-object v0, p0, Lorg/b/a/b/n$a;->b:Lorg/b/a/c;

    invoke-virtual {v0, p1, p2, p3}, Lorg/b/a/c;->a(JLjava/util/Locale;)Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 671
    :cond_0
    iget-object v0, p0, Lorg/b/a/b/n$a;->a:Lorg/b/a/c;

    invoke-virtual {v0, p1, p2, p3}, Lorg/b/a/c;->a(JLjava/util/Locale;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public b(JI)J
    .locals 6

    .line 726
    iget-wide v0, p0, Lorg/b/a/b/n$a;->c:J

    const/4 v2, 0x0

    cmp-long v3, p1, v0

    if-ltz v3, :cond_2

    .line 727
    iget-object v0, p0, Lorg/b/a/b/n$a;->b:Lorg/b/a/c;

    invoke-virtual {v0, p1, p2, p3}, Lorg/b/a/c;->b(JI)J

    move-result-wide p1

    .line 728
    iget-wide v0, p0, Lorg/b/a/b/n$a;->c:J

    cmp-long v3, p1, v0

    if-gez v3, :cond_5

    .line 730
    iget-object v0, p0, Lorg/b/a/b/n$a;->g:Lorg/b/a/b/n;

    invoke-static {v0}, Lorg/b/a/b/n;->a(Lorg/b/a/b/n;)J

    move-result-wide v0

    add-long/2addr v0, p1

    iget-wide v3, p0, Lorg/b/a/b/n$a;->c:J

    cmp-long v5, v0, v3

    if-gez v5, :cond_0

    .line 731
    invoke-virtual {p0, p1, p2}, Lorg/b/a/b/n$a;->k(J)J

    move-result-wide p1

    .line 734
    :cond_0
    invoke-virtual {p0, p1, p2}, Lorg/b/a/b/n$a;->a(J)I

    move-result v0

    if-ne v0, p3, :cond_1

    goto :goto_0

    .line 735
    :cond_1
    new-instance p1, Lorg/b/a/j;

    iget-object p2, p0, Lorg/b/a/b/n$a;->b:Lorg/b/a/c;

    .line 736
    invoke-virtual {p2}, Lorg/b/a/c;->a()Lorg/b/a/d;

    move-result-object p2

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p3

    invoke-direct {p1, p2, p3, v2, v2}, Lorg/b/a/j;-><init>(Lorg/b/a/d;Ljava/lang/Number;Ljava/lang/Number;Ljava/lang/Number;)V

    throw p1

    .line 740
    :cond_2
    iget-object v0, p0, Lorg/b/a/b/n$a;->a:Lorg/b/a/c;

    invoke-virtual {v0, p1, p2, p3}, Lorg/b/a/c;->b(JI)J

    move-result-wide p1

    .line 741
    iget-wide v0, p0, Lorg/b/a/b/n$a;->c:J

    cmp-long v3, p1, v0

    if-ltz v3, :cond_5

    .line 743
    iget-object v0, p0, Lorg/b/a/b/n$a;->g:Lorg/b/a/b/n;

    invoke-static {v0}, Lorg/b/a/b/n;->a(Lorg/b/a/b/n;)J

    move-result-wide v0

    sub-long v0, p1, v0

    iget-wide v3, p0, Lorg/b/a/b/n$a;->c:J

    cmp-long v5, v0, v3

    if-ltz v5, :cond_3

    .line 744
    invoke-virtual {p0, p1, p2}, Lorg/b/a/b/n$a;->j(J)J

    move-result-wide p1

    .line 747
    :cond_3
    invoke-virtual {p0, p1, p2}, Lorg/b/a/b/n$a;->a(J)I

    move-result v0

    if-ne v0, p3, :cond_4

    goto :goto_0

    .line 748
    :cond_4
    new-instance p1, Lorg/b/a/j;

    iget-object p2, p0, Lorg/b/a/b/n$a;->a:Lorg/b/a/c;

    .line 749
    invoke-virtual {p2}, Lorg/b/a/c;->a()Lorg/b/a/d;

    move-result-object p2

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p3

    invoke-direct {p1, p2, p3, v2, v2}, Lorg/b/a/j;-><init>(Lorg/b/a/d;Ljava/lang/Number;Ljava/lang/Number;Ljava/lang/Number;)V

    throw p1

    :cond_5
    :goto_0
    return-wide p1
.end method

.method public b(ILjava/util/Locale;)Ljava/lang/String;
    .locals 1

    .line 688
    iget-object v0, p0, Lorg/b/a/b/n$a;->b:Lorg/b/a/c;

    invoke-virtual {v0, p1, p2}, Lorg/b/a/c;->b(ILjava/util/Locale;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public b(JLjava/util/Locale;)Ljava/lang/String;
    .locals 3

    .line 680
    iget-wide v0, p0, Lorg/b/a/b/n$a;->c:J

    cmp-long v2, p1, v0

    if-ltz v2, :cond_0

    .line 681
    iget-object v0, p0, Lorg/b/a/b/n$a;->b:Lorg/b/a/c;

    invoke-virtual {v0, p1, p2, p3}, Lorg/b/a/c;->b(JLjava/util/Locale;)Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 683
    :cond_0
    iget-object v0, p0, Lorg/b/a/b/n$a;->a:Lorg/b/a/c;

    invoke-virtual {v0, p1, p2, p3}, Lorg/b/a/c;->b(JLjava/util/Locale;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public b(J)Z
    .locals 3

    .line 788
    iget-wide v0, p0, Lorg/b/a/b/n$a;->c:J

    cmp-long v2, p1, v0

    if-ltz v2, :cond_0

    .line 789
    iget-object v0, p0, Lorg/b/a/b/n$a;->b:Lorg/b/a/c;

    invoke-virtual {v0, p1, p2}, Lorg/b/a/c;->b(J)Z

    move-result p1

    return p1

    .line 791
    :cond_0
    iget-object v0, p0, Lorg/b/a/b/n$a;->a:Lorg/b/a/c;

    invoke-virtual {v0, p1, p2}, Lorg/b/a/c;->b(J)Z

    move-result p1

    return p1
.end method

.method public c(J)I
    .locals 4

    .line 846
    iget-wide v0, p0, Lorg/b/a/b/n$a;->c:J

    cmp-long v2, p1, v0

    if-ltz v2, :cond_0

    .line 847
    iget-object v0, p0, Lorg/b/a/b/n$a;->b:Lorg/b/a/c;

    invoke-virtual {v0, p1, p2}, Lorg/b/a/c;->c(J)I

    move-result p1

    return p1

    .line 850
    :cond_0
    iget-object v0, p0, Lorg/b/a/b/n$a;->a:Lorg/b/a/c;

    invoke-virtual {v0, p1, p2}, Lorg/b/a/c;->c(J)I

    move-result v0

    .line 854
    iget-object v1, p0, Lorg/b/a/b/n$a;->a:Lorg/b/a/c;

    invoke-virtual {v1, p1, p2, v0}, Lorg/b/a/c;->b(JI)J

    move-result-wide p1

    .line 855
    iget-wide v1, p0, Lorg/b/a/b/n$a;->c:J

    cmp-long v3, p1, v1

    if-ltz v3, :cond_1

    .line 856
    iget-object p1, p0, Lorg/b/a/b/n$a;->a:Lorg/b/a/c;

    const/4 p2, -0x1

    invoke-virtual {p1, v1, v2, p2}, Lorg/b/a/c;->a(JI)J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Lorg/b/a/c;->a(J)I

    move-result v0

    :cond_1
    return v0
.end method

.method public d(J)J
    .locals 5

    .line 880
    iget-wide v0, p0, Lorg/b/a/b/n$a;->c:J

    cmp-long v2, p1, v0

    if-ltz v2, :cond_0

    .line 881
    iget-object v0, p0, Lorg/b/a/b/n$a;->b:Lorg/b/a/c;

    invoke-virtual {v0, p1, p2}, Lorg/b/a/c;->d(J)J

    move-result-wide p1

    .line 882
    iget-wide v0, p0, Lorg/b/a/b/n$a;->c:J

    cmp-long v2, p1, v0

    if-gez v2, :cond_1

    .line 884
    iget-object v0, p0, Lorg/b/a/b/n$a;->g:Lorg/b/a/b/n;

    invoke-static {v0}, Lorg/b/a/b/n;->a(Lorg/b/a/b/n;)J

    move-result-wide v0

    add-long/2addr v0, p1

    iget-wide v2, p0, Lorg/b/a/b/n$a;->c:J

    cmp-long v4, v0, v2

    if-gez v4, :cond_1

    .line 885
    invoke-virtual {p0, p1, p2}, Lorg/b/a/b/n$a;->k(J)J

    move-result-wide p1

    goto :goto_0

    .line 889
    :cond_0
    iget-object v0, p0, Lorg/b/a/b/n$a;->a:Lorg/b/a/c;

    invoke-virtual {v0, p1, p2}, Lorg/b/a/c;->d(J)J

    move-result-wide p1

    :cond_1
    :goto_0
    return-wide p1
.end method

.method public d()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public e(J)J
    .locals 5

    .line 895
    iget-wide v0, p0, Lorg/b/a/b/n$a;->c:J

    cmp-long v2, p1, v0

    if-ltz v2, :cond_0

    .line 896
    iget-object v0, p0, Lorg/b/a/b/n$a;->b:Lorg/b/a/c;

    invoke-virtual {v0, p1, p2}, Lorg/b/a/c;->e(J)J

    move-result-wide p1

    goto :goto_0

    .line 898
    :cond_0
    iget-object v0, p0, Lorg/b/a/b/n$a;->a:Lorg/b/a/c;

    invoke-virtual {v0, p1, p2}, Lorg/b/a/c;->e(J)J

    move-result-wide p1

    .line 899
    iget-wide v0, p0, Lorg/b/a/b/n$a;->c:J

    cmp-long v2, p1, v0

    if-ltz v2, :cond_1

    .line 901
    iget-object v0, p0, Lorg/b/a/b/n$a;->g:Lorg/b/a/b/n;

    invoke-static {v0}, Lorg/b/a/b/n;->a(Lorg/b/a/b/n;)J

    move-result-wide v0

    sub-long v0, p1, v0

    iget-wide v2, p0, Lorg/b/a/b/n$a;->c:J

    cmp-long v4, v0, v2

    if-ltz v4, :cond_1

    .line 902
    invoke-virtual {p0, p1, p2}, Lorg/b/a/b/n$a;->j(J)J

    move-result-wide p1

    :cond_1
    :goto_0
    return-wide p1
.end method

.method public e()Lorg/b/a/h;
    .locals 1

    .line 780
    iget-object v0, p0, Lorg/b/a/b/n$a;->e:Lorg/b/a/h;

    return-object v0
.end method

.method public f()Lorg/b/a/h;
    .locals 1

    .line 784
    iget-object v0, p0, Lorg/b/a/b/n$a;->f:Lorg/b/a/h;

    return-object v0
.end method

.method public g()Lorg/b/a/h;
    .locals 1

    .line 804
    iget-object v0, p0, Lorg/b/a/b/n$a;->b:Lorg/b/a/c;

    invoke-virtual {v0}, Lorg/b/a/c;->g()Lorg/b/a/h;

    move-result-object v0

    return-object v0
.end method

.method public h()I
    .locals 1

    .line 811
    iget-object v0, p0, Lorg/b/a/b/n$a;->a:Lorg/b/a/c;

    invoke-virtual {v0}, Lorg/b/a/c;->h()I

    move-result v0

    return v0
.end method

.method public i()I
    .locals 1

    .line 842
    iget-object v0, p0, Lorg/b/a/b/n$a;->b:Lorg/b/a/c;

    invoke-virtual {v0}, Lorg/b/a/c;->i()I

    move-result v0

    return v0
.end method

.method protected j(J)J
    .locals 1

    .line 920
    iget-boolean v0, p0, Lorg/b/a/b/n$a;->d:Z

    if-eqz v0, :cond_0

    .line 921
    iget-object v0, p0, Lorg/b/a/b/n$a;->g:Lorg/b/a/b/n;

    invoke-virtual {v0, p1, p2}, Lorg/b/a/b/n;->c(J)J

    move-result-wide p1

    return-wide p1

    .line 923
    :cond_0
    iget-object v0, p0, Lorg/b/a/b/n$a;->g:Lorg/b/a/b/n;

    invoke-virtual {v0, p1, p2}, Lorg/b/a/b/n;->a(J)J

    move-result-wide p1

    return-wide p1
.end method

.method protected k(J)J
    .locals 1

    .line 928
    iget-boolean v0, p0, Lorg/b/a/b/n$a;->d:Z

    if-eqz v0, :cond_0

    .line 929
    iget-object v0, p0, Lorg/b/a/b/n$a;->g:Lorg/b/a/b/n;

    invoke-virtual {v0, p1, p2}, Lorg/b/a/b/n;->d(J)J

    move-result-wide p1

    return-wide p1

    .line 931
    :cond_0
    iget-object v0, p0, Lorg/b/a/b/n$a;->g:Lorg/b/a/b/n;

    invoke-virtual {v0, p1, p2}, Lorg/b/a/b/n;->b(J)J

    move-result-wide p1

    return-wide p1
.end method
