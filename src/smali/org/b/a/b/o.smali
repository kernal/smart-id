.class final Lorg/b/a/b/o;
.super Lorg/b/a/d/m;
.source "GJDayOfWeekDateTimeField.java"


# instance fields
.field private final b:Lorg/b/a/b/c;


# direct methods
.method constructor <init>(Lorg/b/a/b/c;Lorg/b/a/h;)V
    .locals 1

    .line 46
    invoke-static {}, Lorg/b/a/d;->l()Lorg/b/a/d;

    move-result-object v0

    invoke-direct {p0, v0, p2}, Lorg/b/a/d/m;-><init>(Lorg/b/a/d;Lorg/b/a/h;)V

    .line 47
    iput-object p1, p0, Lorg/b/a/b/o;->b:Lorg/b/a/b/c;

    return-void
.end method


# virtual methods
.method public a(J)I
    .locals 1

    .line 57
    iget-object v0, p0, Lorg/b/a/b/o;->b:Lorg/b/a/b/c;

    invoke-virtual {v0, p1, p2}, Lorg/b/a/b/c;->g(J)I

    move-result p1

    return p1
.end method

.method protected a(Ljava/lang/String;Ljava/util/Locale;)I
    .locals 0

    .line 91
    invoke-static {p2}, Lorg/b/a/b/q;->a(Ljava/util/Locale;)Lorg/b/a/b/q;

    move-result-object p2

    invoke-virtual {p2, p1}, Lorg/b/a/b/q;->c(Ljava/lang/String;)I

    move-result p1

    return p1
.end method

.method public a(Ljava/util/Locale;)I
    .locals 0

    .line 123
    invoke-static {p1}, Lorg/b/a/b/q;->a(Ljava/util/Locale;)Lorg/b/a/b/q;

    move-result-object p1

    invoke-virtual {p1}, Lorg/b/a/b/q;->c()I

    move-result p1

    return p1
.end method

.method public a(ILjava/util/Locale;)Ljava/lang/String;
    .locals 0

    .line 68
    invoke-static {p2}, Lorg/b/a/b/q;->a(Ljava/util/Locale;)Lorg/b/a/b/q;

    move-result-object p2

    invoke-virtual {p2, p1}, Lorg/b/a/b/q;->d(I)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public b(ILjava/util/Locale;)Ljava/lang/String;
    .locals 0

    .line 79
    invoke-static {p2}, Lorg/b/a/b/q;->a(Ljava/util/Locale;)Lorg/b/a/b/q;

    move-result-object p2

    invoke-virtual {p2, p1}, Lorg/b/a/b/q;->e(I)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public f()Lorg/b/a/h;
    .locals 1

    .line 95
    iget-object v0, p0, Lorg/b/a/b/o;->b:Lorg/b/a/b/c;

    invoke-virtual {v0}, Lorg/b/a/b/c;->w()Lorg/b/a/h;

    move-result-object v0

    return-object v0
.end method

.method public h()I
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public i()I
    .locals 1

    const/4 v0, 0x7

    return v0
.end method
