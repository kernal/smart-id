.class Lorg/b/a/b/v;
.super Lorg/b/a/d/d;
.source "ISOYearOfEraDateTimeField.java"


# static fields
.field static final a:Lorg/b/a/c;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 42
    new-instance v0, Lorg/b/a/b/v;

    invoke-direct {v0}, Lorg/b/a/b/v;-><init>()V

    sput-object v0, Lorg/b/a/b/v;->a:Lorg/b/a/c;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .line 48
    invoke-static {}, Lorg/b/a/b/t;->Z()Lorg/b/a/b/t;

    move-result-object v0

    invoke-virtual {v0}, Lorg/b/a/b/t;->E()Lorg/b/a/c;

    move-result-object v0

    invoke-static {}, Lorg/b/a/d;->t()Lorg/b/a/d;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lorg/b/a/d/d;-><init>(Lorg/b/a/c;Lorg/b/a/d;)V

    return-void
.end method


# virtual methods
.method public a(J)I
    .locals 1

    .line 57
    invoke-virtual {p0}, Lorg/b/a/b/v;->j()Lorg/b/a/c;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lorg/b/a/c;->a(J)I

    move-result p1

    if-gez p1, :cond_0

    neg-int p1, p1

    :cond_0
    return p1
.end method

.method public a(JI)J
    .locals 1

    .line 62
    invoke-virtual {p0}, Lorg/b/a/b/v;->j()Lorg/b/a/c;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Lorg/b/a/c;->a(JI)J

    move-result-wide p1

    return-wide p1
.end method

.method public a(JJ)J
    .locals 1

    .line 66
    invoke-virtual {p0}, Lorg/b/a/b/v;->j()Lorg/b/a/c;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3, p4}, Lorg/b/a/c;->a(JJ)J

    move-result-wide p1

    return-wide p1
.end method

.method public b(JI)J
    .locals 2

    .line 86
    invoke-virtual {p0}, Lorg/b/a/b/v;->i()I

    move-result v0

    const/4 v1, 0x0

    invoke-static {p0, p3, v1, v0}, Lorg/b/a/d/h;->a(Lorg/b/a/c;III)V

    .line 87
    invoke-virtual {p0}, Lorg/b/a/b/v;->j()Lorg/b/a/c;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lorg/b/a/c;->a(J)I

    move-result v0

    if-gez v0, :cond_0

    neg-int p3, p3

    .line 90
    :cond_0
    invoke-super {p0, p1, p2, p3}, Lorg/b/a/d/d;->b(JI)J

    move-result-wide p1

    return-wide p1
.end method

.method public d(J)J
    .locals 1

    .line 102
    invoke-virtual {p0}, Lorg/b/a/b/v;->j()Lorg/b/a/c;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lorg/b/a/c;->d(J)J

    move-result-wide p1

    return-wide p1
.end method

.method public e(J)J
    .locals 1

    .line 106
    invoke-virtual {p0}, Lorg/b/a/b/v;->j()Lorg/b/a/c;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lorg/b/a/c;->e(J)J

    move-result-wide p1

    return-wide p1
.end method

.method public f()Lorg/b/a/h;
    .locals 1

    .line 53
    invoke-static {}, Lorg/b/a/b/t;->Z()Lorg/b/a/b/t;

    move-result-object v0

    invoke-virtual {v0}, Lorg/b/a/b/t;->J()Lorg/b/a/h;

    move-result-object v0

    return-object v0
.end method

.method public h()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public i()I
    .locals 1

    .line 98
    invoke-virtual {p0}, Lorg/b/a/b/v;->j()Lorg/b/a/c;

    move-result-object v0

    invoke-virtual {v0}, Lorg/b/a/c;->i()I

    move-result v0

    return v0
.end method

.method public i(J)J
    .locals 1

    .line 110
    invoke-virtual {p0}, Lorg/b/a/b/v;->j()Lorg/b/a/c;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lorg/b/a/c;->i(J)J

    move-result-wide p1

    return-wide p1
.end method
