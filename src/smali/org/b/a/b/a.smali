.class public abstract Lorg/b/a/b/a;
.super Lorg/b/a/b/b;
.source "AssembledChronology.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/b/a/b/a$a;
    }
.end annotation


# instance fields
.field private transient A:Lorg/b/a/c;

.field private transient B:Lorg/b/a/c;

.field private transient C:Lorg/b/a/c;

.field private transient D:Lorg/b/a/c;

.field private transient E:Lorg/b/a/c;

.field private transient F:Lorg/b/a/c;

.field private transient G:Lorg/b/a/c;

.field private transient H:Lorg/b/a/c;

.field private transient I:Lorg/b/a/c;

.field private transient J:Lorg/b/a/c;

.field private transient K:Lorg/b/a/c;

.field private transient L:I

.field private final a:Lorg/b/a/a;

.field private final b:Ljava/lang/Object;

.field private transient c:Lorg/b/a/h;

.field private transient d:Lorg/b/a/h;

.field private transient e:Lorg/b/a/h;

.field private transient f:Lorg/b/a/h;

.field private transient g:Lorg/b/a/h;

.field private transient h:Lorg/b/a/h;

.field private transient i:Lorg/b/a/h;

.field private transient j:Lorg/b/a/h;

.field private transient k:Lorg/b/a/h;

.field private transient l:Lorg/b/a/h;

.field private transient m:Lorg/b/a/h;

.field private transient n:Lorg/b/a/h;

.field private transient o:Lorg/b/a/c;

.field private transient p:Lorg/b/a/c;

.field private transient q:Lorg/b/a/c;

.field private transient r:Lorg/b/a/c;

.field private transient s:Lorg/b/a/c;

.field private transient t:Lorg/b/a/c;

.field private transient u:Lorg/b/a/c;

.field private transient v:Lorg/b/a/c;

.field private transient w:Lorg/b/a/c;

.field private transient x:Lorg/b/a/c;

.field private transient y:Lorg/b/a/c;

.field private transient z:Lorg/b/a/c;


# direct methods
.method protected constructor <init>(Lorg/b/a/a;Ljava/lang/Object;)V
    .locals 0

    .line 99
    invoke-direct {p0}, Lorg/b/a/b/b;-><init>()V

    .line 100
    iput-object p1, p0, Lorg/b/a/b/a;->a:Lorg/b/a/a;

    .line 101
    iput-object p2, p0, Lorg/b/a/b/a;->b:Ljava/lang/Object;

    .line 102
    invoke-direct {p0}, Lorg/b/a/b/a;->N()V

    return-void
.end method

.method private N()V
    .locals 4

    .line 319
    new-instance v0, Lorg/b/a/b/a$a;

    invoke-direct {v0}, Lorg/b/a/b/a$a;-><init>()V

    .line 320
    iget-object v1, p0, Lorg/b/a/b/a;->a:Lorg/b/a/a;

    if-eqz v1, :cond_0

    .line 321
    invoke-virtual {v0, v1}, Lorg/b/a/b/a$a;->a(Lorg/b/a/a;)V

    .line 323
    :cond_0
    invoke-virtual {p0, v0}, Lorg/b/a/b/a;->a(Lorg/b/a/b/a$a;)V

    .line 327
    iget-object v1, v0, Lorg/b/a/b/a$a;->a:Lorg/b/a/h;

    if-eqz v1, :cond_1

    goto :goto_0

    :cond_1
    invoke-super {p0}, Lorg/b/a/b/b;->c()Lorg/b/a/h;

    move-result-object v1

    :goto_0
    iput-object v1, p0, Lorg/b/a/b/a;->c:Lorg/b/a/h;

    .line 328
    iget-object v1, v0, Lorg/b/a/b/a$a;->b:Lorg/b/a/h;

    if-eqz v1, :cond_2

    goto :goto_1

    :cond_2
    invoke-super {p0}, Lorg/b/a/b/b;->f()Lorg/b/a/h;

    move-result-object v1

    :goto_1
    iput-object v1, p0, Lorg/b/a/b/a;->d:Lorg/b/a/h;

    .line 329
    iget-object v1, v0, Lorg/b/a/b/a$a;->c:Lorg/b/a/h;

    if-eqz v1, :cond_3

    goto :goto_2

    :cond_3
    invoke-super {p0}, Lorg/b/a/b/b;->i()Lorg/b/a/h;

    move-result-object v1

    :goto_2
    iput-object v1, p0, Lorg/b/a/b/a;->e:Lorg/b/a/h;

    .line 330
    iget-object v1, v0, Lorg/b/a/b/a$a;->d:Lorg/b/a/h;

    if-eqz v1, :cond_4

    goto :goto_3

    :cond_4
    invoke-super {p0}, Lorg/b/a/b/b;->l()Lorg/b/a/h;

    move-result-object v1

    :goto_3
    iput-object v1, p0, Lorg/b/a/b/a;->f:Lorg/b/a/h;

    .line 331
    iget-object v1, v0, Lorg/b/a/b/a$a;->e:Lorg/b/a/h;

    if-eqz v1, :cond_5

    goto :goto_4

    :cond_5
    invoke-super {p0}, Lorg/b/a/b/b;->o()Lorg/b/a/h;

    move-result-object v1

    :goto_4
    iput-object v1, p0, Lorg/b/a/b/a;->g:Lorg/b/a/h;

    .line 332
    iget-object v1, v0, Lorg/b/a/b/a$a;->f:Lorg/b/a/h;

    if-eqz v1, :cond_6

    goto :goto_5

    :cond_6
    invoke-super {p0}, Lorg/b/a/b/b;->s()Lorg/b/a/h;

    move-result-object v1

    :goto_5
    iput-object v1, p0, Lorg/b/a/b/a;->h:Lorg/b/a/h;

    .line 333
    iget-object v1, v0, Lorg/b/a/b/a$a;->g:Lorg/b/a/h;

    if-eqz v1, :cond_7

    goto :goto_6

    :cond_7
    invoke-super {p0}, Lorg/b/a/b/b;->w()Lorg/b/a/h;

    move-result-object v1

    :goto_6
    iput-object v1, p0, Lorg/b/a/b/a;->i:Lorg/b/a/h;

    .line 334
    iget-object v1, v0, Lorg/b/a/b/a$a;->h:Lorg/b/a/h;

    if-eqz v1, :cond_8

    goto :goto_7

    :cond_8
    invoke-super {p0}, Lorg/b/a/b/b;->y()Lorg/b/a/h;

    move-result-object v1

    :goto_7
    iput-object v1, p0, Lorg/b/a/b/a;->j:Lorg/b/a/h;

    .line 335
    iget-object v1, v0, Lorg/b/a/b/a$a;->i:Lorg/b/a/h;

    if-eqz v1, :cond_9

    goto :goto_8

    :cond_9
    invoke-super {p0}, Lorg/b/a/b/b;->B()Lorg/b/a/h;

    move-result-object v1

    :goto_8
    iput-object v1, p0, Lorg/b/a/b/a;->k:Lorg/b/a/h;

    .line 336
    iget-object v1, v0, Lorg/b/a/b/a$a;->j:Lorg/b/a/h;

    if-eqz v1, :cond_a

    goto :goto_9

    :cond_a
    invoke-super {p0}, Lorg/b/a/b/b;->D()Lorg/b/a/h;

    move-result-object v1

    :goto_9
    iput-object v1, p0, Lorg/b/a/b/a;->l:Lorg/b/a/h;

    .line 337
    iget-object v1, v0, Lorg/b/a/b/a$a;->k:Lorg/b/a/h;

    if-eqz v1, :cond_b

    goto :goto_a

    :cond_b
    invoke-super {p0}, Lorg/b/a/b/b;->H()Lorg/b/a/h;

    move-result-object v1

    :goto_a
    iput-object v1, p0, Lorg/b/a/b/a;->m:Lorg/b/a/h;

    .line 338
    iget-object v1, v0, Lorg/b/a/b/a$a;->l:Lorg/b/a/h;

    if-eqz v1, :cond_c

    goto :goto_b

    :cond_c
    invoke-super {p0}, Lorg/b/a/b/b;->J()Lorg/b/a/h;

    move-result-object v1

    :goto_b
    iput-object v1, p0, Lorg/b/a/b/a;->n:Lorg/b/a/h;

    .line 343
    iget-object v1, v0, Lorg/b/a/b/a$a;->m:Lorg/b/a/c;

    if-eqz v1, :cond_d

    goto :goto_c

    :cond_d
    invoke-super {p0}, Lorg/b/a/b/b;->d()Lorg/b/a/c;

    move-result-object v1

    :goto_c
    iput-object v1, p0, Lorg/b/a/b/a;->o:Lorg/b/a/c;

    .line 344
    iget-object v1, v0, Lorg/b/a/b/a$a;->n:Lorg/b/a/c;

    if-eqz v1, :cond_e

    goto :goto_d

    :cond_e
    invoke-super {p0}, Lorg/b/a/b/b;->e()Lorg/b/a/c;

    move-result-object v1

    :goto_d
    iput-object v1, p0, Lorg/b/a/b/a;->p:Lorg/b/a/c;

    .line 345
    iget-object v1, v0, Lorg/b/a/b/a$a;->o:Lorg/b/a/c;

    if-eqz v1, :cond_f

    goto :goto_e

    :cond_f
    invoke-super {p0}, Lorg/b/a/b/b;->g()Lorg/b/a/c;

    move-result-object v1

    :goto_e
    iput-object v1, p0, Lorg/b/a/b/a;->q:Lorg/b/a/c;

    .line 346
    iget-object v1, v0, Lorg/b/a/b/a$a;->p:Lorg/b/a/c;

    if-eqz v1, :cond_10

    goto :goto_f

    :cond_10
    invoke-super {p0}, Lorg/b/a/b/b;->h()Lorg/b/a/c;

    move-result-object v1

    :goto_f
    iput-object v1, p0, Lorg/b/a/b/a;->r:Lorg/b/a/c;

    .line 347
    iget-object v1, v0, Lorg/b/a/b/a$a;->q:Lorg/b/a/c;

    if-eqz v1, :cond_11

    goto :goto_10

    :cond_11
    invoke-super {p0}, Lorg/b/a/b/b;->j()Lorg/b/a/c;

    move-result-object v1

    :goto_10
    iput-object v1, p0, Lorg/b/a/b/a;->s:Lorg/b/a/c;

    .line 348
    iget-object v1, v0, Lorg/b/a/b/a$a;->r:Lorg/b/a/c;

    if-eqz v1, :cond_12

    goto :goto_11

    :cond_12
    invoke-super {p0}, Lorg/b/a/b/b;->k()Lorg/b/a/c;

    move-result-object v1

    :goto_11
    iput-object v1, p0, Lorg/b/a/b/a;->t:Lorg/b/a/c;

    .line 349
    iget-object v1, v0, Lorg/b/a/b/a$a;->s:Lorg/b/a/c;

    if-eqz v1, :cond_13

    goto :goto_12

    :cond_13
    invoke-super {p0}, Lorg/b/a/b/b;->m()Lorg/b/a/c;

    move-result-object v1

    :goto_12
    iput-object v1, p0, Lorg/b/a/b/a;->u:Lorg/b/a/c;

    .line 350
    iget-object v1, v0, Lorg/b/a/b/a$a;->t:Lorg/b/a/c;

    if-eqz v1, :cond_14

    goto :goto_13

    :cond_14
    invoke-super {p0}, Lorg/b/a/b/b;->n()Lorg/b/a/c;

    move-result-object v1

    :goto_13
    iput-object v1, p0, Lorg/b/a/b/a;->v:Lorg/b/a/c;

    .line 351
    iget-object v1, v0, Lorg/b/a/b/a$a;->u:Lorg/b/a/c;

    if-eqz v1, :cond_15

    goto :goto_14

    :cond_15
    invoke-super {p0}, Lorg/b/a/b/b;->p()Lorg/b/a/c;

    move-result-object v1

    :goto_14
    iput-object v1, p0, Lorg/b/a/b/a;->w:Lorg/b/a/c;

    .line 352
    iget-object v1, v0, Lorg/b/a/b/a$a;->v:Lorg/b/a/c;

    if-eqz v1, :cond_16

    goto :goto_15

    :cond_16
    invoke-super {p0}, Lorg/b/a/b/b;->q()Lorg/b/a/c;

    move-result-object v1

    :goto_15
    iput-object v1, p0, Lorg/b/a/b/a;->x:Lorg/b/a/c;

    .line 353
    iget-object v1, v0, Lorg/b/a/b/a$a;->w:Lorg/b/a/c;

    if-eqz v1, :cond_17

    goto :goto_16

    :cond_17
    invoke-super {p0}, Lorg/b/a/b/b;->r()Lorg/b/a/c;

    move-result-object v1

    :goto_16
    iput-object v1, p0, Lorg/b/a/b/a;->y:Lorg/b/a/c;

    .line 354
    iget-object v1, v0, Lorg/b/a/b/a$a;->x:Lorg/b/a/c;

    if-eqz v1, :cond_18

    goto :goto_17

    :cond_18
    invoke-super {p0}, Lorg/b/a/b/b;->t()Lorg/b/a/c;

    move-result-object v1

    :goto_17
    iput-object v1, p0, Lorg/b/a/b/a;->z:Lorg/b/a/c;

    .line 355
    iget-object v1, v0, Lorg/b/a/b/a$a;->y:Lorg/b/a/c;

    if-eqz v1, :cond_19

    goto :goto_18

    :cond_19
    invoke-super {p0}, Lorg/b/a/b/b;->u()Lorg/b/a/c;

    move-result-object v1

    :goto_18
    iput-object v1, p0, Lorg/b/a/b/a;->A:Lorg/b/a/c;

    .line 356
    iget-object v1, v0, Lorg/b/a/b/a$a;->z:Lorg/b/a/c;

    if-eqz v1, :cond_1a

    goto :goto_19

    :cond_1a
    invoke-super {p0}, Lorg/b/a/b/b;->v()Lorg/b/a/c;

    move-result-object v1

    :goto_19
    iput-object v1, p0, Lorg/b/a/b/a;->B:Lorg/b/a/c;

    .line 357
    iget-object v1, v0, Lorg/b/a/b/a$a;->A:Lorg/b/a/c;

    if-eqz v1, :cond_1b

    goto :goto_1a

    :cond_1b
    invoke-super {p0}, Lorg/b/a/b/b;->x()Lorg/b/a/c;

    move-result-object v1

    :goto_1a
    iput-object v1, p0, Lorg/b/a/b/a;->C:Lorg/b/a/c;

    .line 358
    iget-object v1, v0, Lorg/b/a/b/a$a;->B:Lorg/b/a/c;

    if-eqz v1, :cond_1c

    goto :goto_1b

    :cond_1c
    invoke-super {p0}, Lorg/b/a/b/b;->z()Lorg/b/a/c;

    move-result-object v1

    :goto_1b
    iput-object v1, p0, Lorg/b/a/b/a;->D:Lorg/b/a/c;

    .line 359
    iget-object v1, v0, Lorg/b/a/b/a$a;->C:Lorg/b/a/c;

    if-eqz v1, :cond_1d

    goto :goto_1c

    :cond_1d
    invoke-super {p0}, Lorg/b/a/b/b;->A()Lorg/b/a/c;

    move-result-object v1

    :goto_1c
    iput-object v1, p0, Lorg/b/a/b/a;->E:Lorg/b/a/c;

    .line 360
    iget-object v1, v0, Lorg/b/a/b/a$a;->D:Lorg/b/a/c;

    if-eqz v1, :cond_1e

    goto :goto_1d

    :cond_1e
    invoke-super {p0}, Lorg/b/a/b/b;->C()Lorg/b/a/c;

    move-result-object v1

    :goto_1d
    iput-object v1, p0, Lorg/b/a/b/a;->F:Lorg/b/a/c;

    .line 361
    iget-object v1, v0, Lorg/b/a/b/a$a;->E:Lorg/b/a/c;

    if-eqz v1, :cond_1f

    goto :goto_1e

    :cond_1f
    invoke-super {p0}, Lorg/b/a/b/b;->E()Lorg/b/a/c;

    move-result-object v1

    :goto_1e
    iput-object v1, p0, Lorg/b/a/b/a;->G:Lorg/b/a/c;

    .line 362
    iget-object v1, v0, Lorg/b/a/b/a$a;->F:Lorg/b/a/c;

    if-eqz v1, :cond_20

    goto :goto_1f

    :cond_20
    invoke-super {p0}, Lorg/b/a/b/b;->F()Lorg/b/a/c;

    move-result-object v1

    :goto_1f
    iput-object v1, p0, Lorg/b/a/b/a;->H:Lorg/b/a/c;

    .line 363
    iget-object v1, v0, Lorg/b/a/b/a$a;->G:Lorg/b/a/c;

    if-eqz v1, :cond_21

    goto :goto_20

    :cond_21
    invoke-super {p0}, Lorg/b/a/b/b;->G()Lorg/b/a/c;

    move-result-object v1

    :goto_20
    iput-object v1, p0, Lorg/b/a/b/a;->I:Lorg/b/a/c;

    .line 364
    iget-object v1, v0, Lorg/b/a/b/a$a;->H:Lorg/b/a/c;

    if-eqz v1, :cond_22

    goto :goto_21

    :cond_22
    invoke-super {p0}, Lorg/b/a/b/b;->I()Lorg/b/a/c;

    move-result-object v1

    :goto_21
    iput-object v1, p0, Lorg/b/a/b/a;->J:Lorg/b/a/c;

    .line 365
    iget-object v0, v0, Lorg/b/a/b/a$a;->I:Lorg/b/a/c;

    if-eqz v0, :cond_23

    goto :goto_22

    :cond_23
    invoke-super {p0}, Lorg/b/a/b/b;->K()Lorg/b/a/c;

    move-result-object v0

    :goto_22
    iput-object v0, p0, Lorg/b/a/b/a;->K:Lorg/b/a/c;

    .line 369
    iget-object v0, p0, Lorg/b/a/b/a;->a:Lorg/b/a/a;

    const/4 v1, 0x0

    if-nez v0, :cond_24

    goto :goto_25

    .line 372
    :cond_24
    iget-object v2, p0, Lorg/b/a/b/a;->u:Lorg/b/a/c;

    .line 373
    invoke-virtual {v0}, Lorg/b/a/a;->m()Lorg/b/a/c;

    move-result-object v0

    if-ne v2, v0, :cond_25

    iget-object v0, p0, Lorg/b/a/b/a;->s:Lorg/b/a/c;

    iget-object v2, p0, Lorg/b/a/b/a;->a:Lorg/b/a/a;

    .line 374
    invoke-virtual {v2}, Lorg/b/a/a;->j()Lorg/b/a/c;

    move-result-object v2

    if-ne v0, v2, :cond_25

    iget-object v0, p0, Lorg/b/a/b/a;->q:Lorg/b/a/c;

    iget-object v2, p0, Lorg/b/a/b/a;->a:Lorg/b/a/a;

    .line 375
    invoke-virtual {v2}, Lorg/b/a/a;->g()Lorg/b/a/c;

    move-result-object v2

    if-ne v0, v2, :cond_25

    iget-object v0, p0, Lorg/b/a/b/a;->o:Lorg/b/a/c;

    iget-object v2, p0, Lorg/b/a/b/a;->a:Lorg/b/a/a;

    .line 376
    invoke-virtual {v2}, Lorg/b/a/a;->d()Lorg/b/a/c;

    move-result-object v2

    if-ne v0, v2, :cond_25

    const/4 v0, 0x1

    goto :goto_23

    :cond_25
    const/4 v0, 0x0

    :goto_23
    iget-object v2, p0, Lorg/b/a/b/a;->p:Lorg/b/a/c;

    iget-object v3, p0, Lorg/b/a/b/a;->a:Lorg/b/a/a;

    .line 378
    invoke-virtual {v3}, Lorg/b/a/a;->e()Lorg/b/a/c;

    move-result-object v3

    if-ne v2, v3, :cond_26

    const/4 v2, 0x2

    goto :goto_24

    :cond_26
    const/4 v2, 0x0

    :goto_24
    or-int/2addr v0, v2

    iget-object v2, p0, Lorg/b/a/b/a;->G:Lorg/b/a/c;

    iget-object v3, p0, Lorg/b/a/b/a;->a:Lorg/b/a/a;

    .line 380
    invoke-virtual {v3}, Lorg/b/a/a;->E()Lorg/b/a/c;

    move-result-object v3

    if-ne v2, v3, :cond_27

    iget-object v2, p0, Lorg/b/a/b/a;->F:Lorg/b/a/c;

    iget-object v3, p0, Lorg/b/a/b/a;->a:Lorg/b/a/a;

    .line 381
    invoke-virtual {v3}, Lorg/b/a/a;->C()Lorg/b/a/c;

    move-result-object v3

    if-ne v2, v3, :cond_27

    iget-object v2, p0, Lorg/b/a/b/a;->A:Lorg/b/a/c;

    iget-object v3, p0, Lorg/b/a/b/a;->a:Lorg/b/a/a;

    .line 382
    invoke-virtual {v3}, Lorg/b/a/a;->u()Lorg/b/a/c;

    move-result-object v3

    if-ne v2, v3, :cond_27

    const/4 v1, 0x4

    :cond_27
    or-int/2addr v1, v0

    .line 385
    :goto_25
    iput v1, p0, Lorg/b/a/b/a;->L:I

    return-void
.end method


# virtual methods
.method public final A()Lorg/b/a/c;
    .locals 1

    .line 252
    iget-object v0, p0, Lorg/b/a/b/a;->E:Lorg/b/a/c;

    return-object v0
.end method

.method public final B()Lorg/b/a/h;
    .locals 1

    .line 256
    iget-object v0, p0, Lorg/b/a/b/a;->k:Lorg/b/a/h;

    return-object v0
.end method

.method public final C()Lorg/b/a/c;
    .locals 1

    .line 260
    iget-object v0, p0, Lorg/b/a/b/a;->F:Lorg/b/a/c;

    return-object v0
.end method

.method public final D()Lorg/b/a/h;
    .locals 1

    .line 264
    iget-object v0, p0, Lorg/b/a/b/a;->l:Lorg/b/a/h;

    return-object v0
.end method

.method public final E()Lorg/b/a/c;
    .locals 1

    .line 268
    iget-object v0, p0, Lorg/b/a/b/a;->G:Lorg/b/a/c;

    return-object v0
.end method

.method public final F()Lorg/b/a/c;
    .locals 1

    .line 272
    iget-object v0, p0, Lorg/b/a/b/a;->H:Lorg/b/a/c;

    return-object v0
.end method

.method public final G()Lorg/b/a/c;
    .locals 1

    .line 276
    iget-object v0, p0, Lorg/b/a/b/a;->I:Lorg/b/a/c;

    return-object v0
.end method

.method public final H()Lorg/b/a/h;
    .locals 1

    .line 280
    iget-object v0, p0, Lorg/b/a/b/a;->m:Lorg/b/a/h;

    return-object v0
.end method

.method public final I()Lorg/b/a/c;
    .locals 1

    .line 284
    iget-object v0, p0, Lorg/b/a/b/a;->J:Lorg/b/a/c;

    return-object v0
.end method

.method public final J()Lorg/b/a/h;
    .locals 1

    .line 288
    iget-object v0, p0, Lorg/b/a/b/a;->n:Lorg/b/a/h;

    return-object v0
.end method

.method public final K()Lorg/b/a/c;
    .locals 1

    .line 292
    iget-object v0, p0, Lorg/b/a/b/a;->K:Lorg/b/a/c;

    return-object v0
.end method

.method protected final L()Lorg/b/a/a;
    .locals 1

    .line 308
    iget-object v0, p0, Lorg/b/a/b/a;->a:Lorg/b/a/a;

    return-object v0
.end method

.method protected final M()Ljava/lang/Object;
    .locals 1

    .line 315
    iget-object v0, p0, Lorg/b/a/b/a;->b:Ljava/lang/Object;

    return-object v0
.end method

.method public a(IIII)J
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .line 118
    iget-object v0, p0, Lorg/b/a/b/a;->a:Lorg/b/a/a;

    if-eqz v0, :cond_0

    iget v1, p0, Lorg/b/a/b/a;->L:I

    const/4 v2, 0x6

    and-int/2addr v1, v2

    if-ne v1, v2, :cond_0

    .line 120
    invoke-virtual {v0, p1, p2, p3, p4}, Lorg/b/a/a;->a(IIII)J

    move-result-wide p1

    return-wide p1

    .line 122
    :cond_0
    invoke-super {p0, p1, p2, p3, p4}, Lorg/b/a/b/b;->a(IIII)J

    move-result-wide p1

    return-wide p1
.end method

.method public a(IIIIIII)J
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .line 131
    iget-object v0, p0, Lorg/b/a/b/a;->a:Lorg/b/a/a;

    if-eqz v0, :cond_0

    iget v1, p0, Lorg/b/a/b/a;->L:I

    const/4 v2, 0x5

    and-int/2addr v1, v2

    if-ne v1, v2, :cond_0

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    move v6, p6

    move v7, p7

    .line 133
    invoke-virtual/range {v0 .. v7}, Lorg/b/a/a;->a(IIIIIII)J

    move-result-wide p1

    return-wide p1

    .line 136
    :cond_0
    invoke-super/range {p0 .. p7}, Lorg/b/a/b/b;->a(IIIIIII)J

    move-result-wide p1

    return-wide p1
.end method

.method public a()Lorg/b/a/f;
    .locals 1

    .line 107
    iget-object v0, p0, Lorg/b/a/b/a;->a:Lorg/b/a/a;

    if-eqz v0, :cond_0

    .line 108
    invoke-virtual {v0}, Lorg/b/a/a;->a()Lorg/b/a/f;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method protected abstract a(Lorg/b/a/b/a$a;)V
.end method

.method public final c()Lorg/b/a/h;
    .locals 1

    .line 156
    iget-object v0, p0, Lorg/b/a/b/a;->c:Lorg/b/a/h;

    return-object v0
.end method

.method public final d()Lorg/b/a/c;
    .locals 1

    .line 160
    iget-object v0, p0, Lorg/b/a/b/a;->o:Lorg/b/a/c;

    return-object v0
.end method

.method public final e()Lorg/b/a/c;
    .locals 1

    .line 164
    iget-object v0, p0, Lorg/b/a/b/a;->p:Lorg/b/a/c;

    return-object v0
.end method

.method public final f()Lorg/b/a/h;
    .locals 1

    .line 168
    iget-object v0, p0, Lorg/b/a/b/a;->d:Lorg/b/a/h;

    return-object v0
.end method

.method public final g()Lorg/b/a/c;
    .locals 1

    .line 172
    iget-object v0, p0, Lorg/b/a/b/a;->q:Lorg/b/a/c;

    return-object v0
.end method

.method public final h()Lorg/b/a/c;
    .locals 1

    .line 176
    iget-object v0, p0, Lorg/b/a/b/a;->r:Lorg/b/a/c;

    return-object v0
.end method

.method public final i()Lorg/b/a/h;
    .locals 1

    .line 180
    iget-object v0, p0, Lorg/b/a/b/a;->e:Lorg/b/a/h;

    return-object v0
.end method

.method public final j()Lorg/b/a/c;
    .locals 1

    .line 184
    iget-object v0, p0, Lorg/b/a/b/a;->s:Lorg/b/a/c;

    return-object v0
.end method

.method public final k()Lorg/b/a/c;
    .locals 1

    .line 188
    iget-object v0, p0, Lorg/b/a/b/a;->t:Lorg/b/a/c;

    return-object v0
.end method

.method public final l()Lorg/b/a/h;
    .locals 1

    .line 192
    iget-object v0, p0, Lorg/b/a/b/a;->f:Lorg/b/a/h;

    return-object v0
.end method

.method public final m()Lorg/b/a/c;
    .locals 1

    .line 196
    iget-object v0, p0, Lorg/b/a/b/a;->u:Lorg/b/a/c;

    return-object v0
.end method

.method public final n()Lorg/b/a/c;
    .locals 1

    .line 200
    iget-object v0, p0, Lorg/b/a/b/a;->v:Lorg/b/a/c;

    return-object v0
.end method

.method public final o()Lorg/b/a/h;
    .locals 1

    .line 204
    iget-object v0, p0, Lorg/b/a/b/a;->g:Lorg/b/a/h;

    return-object v0
.end method

.method public final p()Lorg/b/a/c;
    .locals 1

    .line 208
    iget-object v0, p0, Lorg/b/a/b/a;->w:Lorg/b/a/c;

    return-object v0
.end method

.method public final q()Lorg/b/a/c;
    .locals 1

    .line 212
    iget-object v0, p0, Lorg/b/a/b/a;->x:Lorg/b/a/c;

    return-object v0
.end method

.method public final r()Lorg/b/a/c;
    .locals 1

    .line 216
    iget-object v0, p0, Lorg/b/a/b/a;->y:Lorg/b/a/c;

    return-object v0
.end method

.method public final s()Lorg/b/a/h;
    .locals 1

    .line 220
    iget-object v0, p0, Lorg/b/a/b/a;->h:Lorg/b/a/h;

    return-object v0
.end method

.method public final t()Lorg/b/a/c;
    .locals 1

    .line 224
    iget-object v0, p0, Lorg/b/a/b/a;->z:Lorg/b/a/c;

    return-object v0
.end method

.method public final u()Lorg/b/a/c;
    .locals 1

    .line 228
    iget-object v0, p0, Lorg/b/a/b/a;->A:Lorg/b/a/c;

    return-object v0
.end method

.method public final v()Lorg/b/a/c;
    .locals 1

    .line 232
    iget-object v0, p0, Lorg/b/a/b/a;->B:Lorg/b/a/c;

    return-object v0
.end method

.method public final w()Lorg/b/a/h;
    .locals 1

    .line 236
    iget-object v0, p0, Lorg/b/a/b/a;->i:Lorg/b/a/h;

    return-object v0
.end method

.method public final x()Lorg/b/a/c;
    .locals 1

    .line 240
    iget-object v0, p0, Lorg/b/a/b/a;->C:Lorg/b/a/c;

    return-object v0
.end method

.method public final y()Lorg/b/a/h;
    .locals 1

    .line 244
    iget-object v0, p0, Lorg/b/a/b/a;->j:Lorg/b/a/h;

    return-object v0
.end method

.method public final z()Lorg/b/a/c;
    .locals 1

    .line 248
    iget-object v0, p0, Lorg/b/a/b/a;->D:Lorg/b/a/c;

    return-object v0
.end method
