.class public final Lorg/b/a/b/x;
.super Lorg/b/a/b/a;
.source "LimitChronology.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/b/a/b/x$a;,
        Lorg/b/a/b/x$b;,
        Lorg/b/a/b/x$c;
    }
.end annotation


# instance fields
.field final a:Lorg/b/a/b;

.field final b:Lorg/b/a/b;

.field private transient c:Lorg/b/a/b/x;


# direct methods
.method private constructor <init>(Lorg/b/a/a;Lorg/b/a/b;Lorg/b/a/b;)V
    .locals 1

    const/4 v0, 0x0

    .line 97
    invoke-direct {p0, p1, v0}, Lorg/b/a/b/a;-><init>(Lorg/b/a/a;Ljava/lang/Object;)V

    .line 99
    iput-object p2, p0, Lorg/b/a/b/x;->a:Lorg/b/a/b;

    .line 100
    iput-object p3, p0, Lorg/b/a/b/x;->b:Lorg/b/a/b;

    return-void
.end method

.method public static a(Lorg/b/a/a;Lorg/b/a/r;Lorg/b/a/r;)Lorg/b/a/b/x;
    .locals 1

    if-eqz p0, :cond_4

    const/4 v0, 0x0

    if-nez p1, :cond_0

    move-object p1, v0

    goto :goto_0

    .line 71
    :cond_0
    invoke-interface {p1}, Lorg/b/a/r;->b()Lorg/b/a/b;

    move-result-object p1

    :goto_0
    if-nez p2, :cond_1

    goto :goto_1

    .line 72
    :cond_1
    invoke-interface {p2}, Lorg/b/a/r;->b()Lorg/b/a/b;

    move-result-object v0

    :goto_1
    if-eqz p1, :cond_3

    if-eqz v0, :cond_3

    .line 74
    invoke-interface {p1, v0}, Lorg/b/a/r;->a(Lorg/b/a/t;)Z

    move-result p2

    if-eqz p2, :cond_2

    goto :goto_2

    .line 75
    :cond_2
    new-instance p0, Ljava/lang/IllegalArgumentException;

    const-string p1, "The lower limit must be come before than the upper limit"

    invoke-direct {p0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p0

    .line 79
    :cond_3
    :goto_2
    new-instance p2, Lorg/b/a/b/x;

    check-cast p1, Lorg/b/a/b;

    check-cast v0, Lorg/b/a/b;

    invoke-direct {p2, p0, p1, v0}, Lorg/b/a/b/x;-><init>(Lorg/b/a/a;Lorg/b/a/b;Lorg/b/a/b;)V

    return-object p2

    .line 68
    :cond_4
    new-instance p0, Ljava/lang/IllegalArgumentException;

    const-string p1, "Must supply a chronology"

    invoke-direct {p0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method private a(Lorg/b/a/c;Ljava/util/HashMap;)Lorg/b/a/c;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/b/a/c;",
            "Ljava/util/HashMap<",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            ">;)",
            "Lorg/b/a/c;"
        }
    .end annotation

    if-eqz p1, :cond_2

    .line 266
    invoke-virtual {p1}, Lorg/b/a/c;->c()Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    .line 269
    :cond_0
    invoke-virtual {p2, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 270
    invoke-virtual {p2, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lorg/b/a/c;

    return-object p1

    .line 272
    :cond_1
    new-instance v6, Lorg/b/a/b/x$a;

    .line 274
    invoke-virtual {p1}, Lorg/b/a/c;->e()Lorg/b/a/h;

    move-result-object v0

    invoke-direct {p0, v0, p2}, Lorg/b/a/b/x;->a(Lorg/b/a/h;Ljava/util/HashMap;)Lorg/b/a/h;

    move-result-object v3

    .line 275
    invoke-virtual {p1}, Lorg/b/a/c;->f()Lorg/b/a/h;

    move-result-object v0

    invoke-direct {p0, v0, p2}, Lorg/b/a/b/x;->a(Lorg/b/a/h;Ljava/util/HashMap;)Lorg/b/a/h;

    move-result-object v4

    .line 276
    invoke-virtual {p1}, Lorg/b/a/c;->g()Lorg/b/a/h;

    move-result-object v0

    invoke-direct {p0, v0, p2}, Lorg/b/a/b/x;->a(Lorg/b/a/h;Ljava/util/HashMap;)Lorg/b/a/h;

    move-result-object v5

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    invoke-direct/range {v0 .. v5}, Lorg/b/a/b/x$a;-><init>(Lorg/b/a/b/x;Lorg/b/a/c;Lorg/b/a/h;Lorg/b/a/h;Lorg/b/a/h;)V

    .line 277
    invoke-virtual {p2, p1, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-object v6

    :cond_2
    :goto_0
    return-object p1
.end method

.method private a(Lorg/b/a/h;Ljava/util/HashMap;)Lorg/b/a/h;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/b/a/h;",
            "Ljava/util/HashMap<",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            ">;)",
            "Lorg/b/a/h;"
        }
    .end annotation

    if-eqz p1, :cond_2

    .line 254
    invoke-virtual {p1}, Lorg/b/a/h;->b()Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    .line 257
    :cond_0
    invoke-virtual {p2, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 258
    invoke-virtual {p2, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lorg/b/a/h;

    return-object p1

    .line 260
    :cond_1
    new-instance v0, Lorg/b/a/b/x$b;

    invoke-direct {v0, p0, p1}, Lorg/b/a/b/x$b;-><init>(Lorg/b/a/b/x;Lorg/b/a/h;)V

    .line 261
    invoke-virtual {p2, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-object v0

    :cond_2
    :goto_0
    return-object p1
.end method


# virtual methods
.method public N()Lorg/b/a/b;
    .locals 1

    .line 109
    iget-object v0, p0, Lorg/b/a/b/x;->a:Lorg/b/a/b;

    return-object v0
.end method

.method public O()Lorg/b/a/b;
    .locals 1

    .line 118
    iget-object v0, p0, Lorg/b/a/b/x;->b:Lorg/b/a/b;

    return-object v0
.end method

.method public a(IIII)J
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .line 175
    invoke-virtual {p0}, Lorg/b/a/b/x;->L()Lorg/b/a/a;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3, p4}, Lorg/b/a/a;->a(IIII)J

    move-result-wide p1

    const-string p3, "resulting"

    .line 176
    invoke-virtual {p0, p1, p2, p3}, Lorg/b/a/b/x;->a(JLjava/lang/String;)V

    return-wide p1
.end method

.method public a(IIIIIII)J
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .line 185
    invoke-virtual {p0}, Lorg/b/a/b/x;->L()Lorg/b/a/a;

    move-result-object v0

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    move v6, p6

    move v7, p7

    .line 186
    invoke-virtual/range {v0 .. v7}, Lorg/b/a/a;->a(IIIIIII)J

    move-result-wide p1

    const-string p3, "resulting"

    .line 188
    invoke-virtual {p0, p1, p2, p3}, Lorg/b/a/b/x;->a(JLjava/lang/String;)V

    return-wide p1
.end method

.method public a(Lorg/b/a/f;)Lorg/b/a/a;
    .locals 3

    if-nez p1, :cond_0

    .line 137
    invoke-static {}, Lorg/b/a/f;->a()Lorg/b/a/f;

    move-result-object p1

    .line 139
    :cond_0
    invoke-virtual {p0}, Lorg/b/a/b/x;->a()Lorg/b/a/f;

    move-result-object v0

    if-ne p1, v0, :cond_1

    return-object p0

    .line 143
    :cond_1
    sget-object v0, Lorg/b/a/f;->a:Lorg/b/a/f;

    if-ne p1, v0, :cond_2

    iget-object v0, p0, Lorg/b/a/b/x;->c:Lorg/b/a/b/x;

    if-eqz v0, :cond_2

    return-object v0

    .line 147
    :cond_2
    iget-object v0, p0, Lorg/b/a/b/x;->a:Lorg/b/a/b;

    if-eqz v0, :cond_3

    .line 149
    invoke-virtual {v0}, Lorg/b/a/b;->e()Lorg/b/a/o;

    move-result-object v0

    .line 150
    invoke-virtual {v0, p1}, Lorg/b/a/o;->a(Lorg/b/a/f;)V

    .line 151
    invoke-virtual {v0}, Lorg/b/a/o;->b()Lorg/b/a/b;

    move-result-object v0

    .line 154
    :cond_3
    iget-object v1, p0, Lorg/b/a/b/x;->b:Lorg/b/a/b;

    if-eqz v1, :cond_4

    .line 156
    invoke-virtual {v1}, Lorg/b/a/b;->e()Lorg/b/a/o;

    move-result-object v1

    .line 157
    invoke-virtual {v1, p1}, Lorg/b/a/o;->a(Lorg/b/a/f;)V

    .line 158
    invoke-virtual {v1}, Lorg/b/a/o;->b()Lorg/b/a/b;

    move-result-object v1

    .line 162
    :cond_4
    invoke-virtual {p0}, Lorg/b/a/b/x;->L()Lorg/b/a/a;

    move-result-object v2

    invoke-virtual {v2, p1}, Lorg/b/a/a;->a(Lorg/b/a/f;)Lorg/b/a/a;

    move-result-object v2

    invoke-static {v2, v0, v1}, Lorg/b/a/b/x;->a(Lorg/b/a/a;Lorg/b/a/r;Lorg/b/a/r;)Lorg/b/a/b/x;

    move-result-object v0

    .line 164
    sget-object v1, Lorg/b/a/f;->a:Lorg/b/a/f;

    if-ne p1, v1, :cond_5

    .line 165
    iput-object v0, p0, Lorg/b/a/b/x;->c:Lorg/b/a/b/x;

    :cond_5
    return-object v0
.end method

.method a(JLjava/lang/String;)V
    .locals 3

    .line 283
    iget-object v0, p0, Lorg/b/a/b/x;->a:Lorg/b/a/b;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lorg/b/a/b;->c()J

    move-result-wide v0

    cmp-long v2, p1, v0

    if-ltz v2, :cond_0

    goto :goto_0

    .line 284
    :cond_0
    new-instance p1, Lorg/b/a/b/x$c;

    const/4 p2, 0x1

    invoke-direct {p1, p0, p3, p2}, Lorg/b/a/b/x$c;-><init>(Lorg/b/a/b/x;Ljava/lang/String;Z)V

    throw p1

    .line 286
    :cond_1
    :goto_0
    iget-object v0, p0, Lorg/b/a/b/x;->b:Lorg/b/a/b;

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Lorg/b/a/b;->c()J

    move-result-wide v0

    cmp-long v2, p1, v0

    if-gez v2, :cond_2

    goto :goto_1

    .line 287
    :cond_2
    new-instance p1, Lorg/b/a/b/x$c;

    const/4 p2, 0x0

    invoke-direct {p1, p0, p3, p2}, Lorg/b/a/b/x$c;-><init>(Lorg/b/a/b/x;Ljava/lang/String;Z)V

    throw p1

    :cond_3
    :goto_1
    return-void
.end method

.method protected a(Lorg/b/a/b/a$a;)V
    .locals 2

    .line 207
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 211
    iget-object v1, p1, Lorg/b/a/b/a$a;->l:Lorg/b/a/h;

    invoke-direct {p0, v1, v0}, Lorg/b/a/b/x;->a(Lorg/b/a/h;Ljava/util/HashMap;)Lorg/b/a/h;

    move-result-object v1

    iput-object v1, p1, Lorg/b/a/b/a$a;->l:Lorg/b/a/h;

    .line 212
    iget-object v1, p1, Lorg/b/a/b/a$a;->k:Lorg/b/a/h;

    invoke-direct {p0, v1, v0}, Lorg/b/a/b/x;->a(Lorg/b/a/h;Ljava/util/HashMap;)Lorg/b/a/h;

    move-result-object v1

    iput-object v1, p1, Lorg/b/a/b/a$a;->k:Lorg/b/a/h;

    .line 213
    iget-object v1, p1, Lorg/b/a/b/a$a;->j:Lorg/b/a/h;

    invoke-direct {p0, v1, v0}, Lorg/b/a/b/x;->a(Lorg/b/a/h;Ljava/util/HashMap;)Lorg/b/a/h;

    move-result-object v1

    iput-object v1, p1, Lorg/b/a/b/a$a;->j:Lorg/b/a/h;

    .line 214
    iget-object v1, p1, Lorg/b/a/b/a$a;->i:Lorg/b/a/h;

    invoke-direct {p0, v1, v0}, Lorg/b/a/b/x;->a(Lorg/b/a/h;Ljava/util/HashMap;)Lorg/b/a/h;

    move-result-object v1

    iput-object v1, p1, Lorg/b/a/b/a$a;->i:Lorg/b/a/h;

    .line 215
    iget-object v1, p1, Lorg/b/a/b/a$a;->h:Lorg/b/a/h;

    invoke-direct {p0, v1, v0}, Lorg/b/a/b/x;->a(Lorg/b/a/h;Ljava/util/HashMap;)Lorg/b/a/h;

    move-result-object v1

    iput-object v1, p1, Lorg/b/a/b/a$a;->h:Lorg/b/a/h;

    .line 216
    iget-object v1, p1, Lorg/b/a/b/a$a;->g:Lorg/b/a/h;

    invoke-direct {p0, v1, v0}, Lorg/b/a/b/x;->a(Lorg/b/a/h;Ljava/util/HashMap;)Lorg/b/a/h;

    move-result-object v1

    iput-object v1, p1, Lorg/b/a/b/a$a;->g:Lorg/b/a/h;

    .line 217
    iget-object v1, p1, Lorg/b/a/b/a$a;->f:Lorg/b/a/h;

    invoke-direct {p0, v1, v0}, Lorg/b/a/b/x;->a(Lorg/b/a/h;Ljava/util/HashMap;)Lorg/b/a/h;

    move-result-object v1

    iput-object v1, p1, Lorg/b/a/b/a$a;->f:Lorg/b/a/h;

    .line 219
    iget-object v1, p1, Lorg/b/a/b/a$a;->e:Lorg/b/a/h;

    invoke-direct {p0, v1, v0}, Lorg/b/a/b/x;->a(Lorg/b/a/h;Ljava/util/HashMap;)Lorg/b/a/h;

    move-result-object v1

    iput-object v1, p1, Lorg/b/a/b/a$a;->e:Lorg/b/a/h;

    .line 220
    iget-object v1, p1, Lorg/b/a/b/a$a;->d:Lorg/b/a/h;

    invoke-direct {p0, v1, v0}, Lorg/b/a/b/x;->a(Lorg/b/a/h;Ljava/util/HashMap;)Lorg/b/a/h;

    move-result-object v1

    iput-object v1, p1, Lorg/b/a/b/a$a;->d:Lorg/b/a/h;

    .line 221
    iget-object v1, p1, Lorg/b/a/b/a$a;->c:Lorg/b/a/h;

    invoke-direct {p0, v1, v0}, Lorg/b/a/b/x;->a(Lorg/b/a/h;Ljava/util/HashMap;)Lorg/b/a/h;

    move-result-object v1

    iput-object v1, p1, Lorg/b/a/b/a$a;->c:Lorg/b/a/h;

    .line 222
    iget-object v1, p1, Lorg/b/a/b/a$a;->b:Lorg/b/a/h;

    invoke-direct {p0, v1, v0}, Lorg/b/a/b/x;->a(Lorg/b/a/h;Ljava/util/HashMap;)Lorg/b/a/h;

    move-result-object v1

    iput-object v1, p1, Lorg/b/a/b/a$a;->b:Lorg/b/a/h;

    .line 223
    iget-object v1, p1, Lorg/b/a/b/a$a;->a:Lorg/b/a/h;

    invoke-direct {p0, v1, v0}, Lorg/b/a/b/x;->a(Lorg/b/a/h;Ljava/util/HashMap;)Lorg/b/a/h;

    move-result-object v1

    iput-object v1, p1, Lorg/b/a/b/a$a;->a:Lorg/b/a/h;

    .line 227
    iget-object v1, p1, Lorg/b/a/b/a$a;->E:Lorg/b/a/c;

    invoke-direct {p0, v1, v0}, Lorg/b/a/b/x;->a(Lorg/b/a/c;Ljava/util/HashMap;)Lorg/b/a/c;

    move-result-object v1

    iput-object v1, p1, Lorg/b/a/b/a$a;->E:Lorg/b/a/c;

    .line 228
    iget-object v1, p1, Lorg/b/a/b/a$a;->F:Lorg/b/a/c;

    invoke-direct {p0, v1, v0}, Lorg/b/a/b/x;->a(Lorg/b/a/c;Ljava/util/HashMap;)Lorg/b/a/c;

    move-result-object v1

    iput-object v1, p1, Lorg/b/a/b/a$a;->F:Lorg/b/a/c;

    .line 229
    iget-object v1, p1, Lorg/b/a/b/a$a;->G:Lorg/b/a/c;

    invoke-direct {p0, v1, v0}, Lorg/b/a/b/x;->a(Lorg/b/a/c;Ljava/util/HashMap;)Lorg/b/a/c;

    move-result-object v1

    iput-object v1, p1, Lorg/b/a/b/a$a;->G:Lorg/b/a/c;

    .line 230
    iget-object v1, p1, Lorg/b/a/b/a$a;->H:Lorg/b/a/c;

    invoke-direct {p0, v1, v0}, Lorg/b/a/b/x;->a(Lorg/b/a/c;Ljava/util/HashMap;)Lorg/b/a/c;

    move-result-object v1

    iput-object v1, p1, Lorg/b/a/b/a$a;->H:Lorg/b/a/c;

    .line 231
    iget-object v1, p1, Lorg/b/a/b/a$a;->I:Lorg/b/a/c;

    invoke-direct {p0, v1, v0}, Lorg/b/a/b/x;->a(Lorg/b/a/c;Ljava/util/HashMap;)Lorg/b/a/c;

    move-result-object v1

    iput-object v1, p1, Lorg/b/a/b/a$a;->I:Lorg/b/a/c;

    .line 232
    iget-object v1, p1, Lorg/b/a/b/a$a;->x:Lorg/b/a/c;

    invoke-direct {p0, v1, v0}, Lorg/b/a/b/x;->a(Lorg/b/a/c;Ljava/util/HashMap;)Lorg/b/a/c;

    move-result-object v1

    iput-object v1, p1, Lorg/b/a/b/a$a;->x:Lorg/b/a/c;

    .line 233
    iget-object v1, p1, Lorg/b/a/b/a$a;->y:Lorg/b/a/c;

    invoke-direct {p0, v1, v0}, Lorg/b/a/b/x;->a(Lorg/b/a/c;Ljava/util/HashMap;)Lorg/b/a/c;

    move-result-object v1

    iput-object v1, p1, Lorg/b/a/b/a$a;->y:Lorg/b/a/c;

    .line 234
    iget-object v1, p1, Lorg/b/a/b/a$a;->z:Lorg/b/a/c;

    invoke-direct {p0, v1, v0}, Lorg/b/a/b/x;->a(Lorg/b/a/c;Ljava/util/HashMap;)Lorg/b/a/c;

    move-result-object v1

    iput-object v1, p1, Lorg/b/a/b/a$a;->z:Lorg/b/a/c;

    .line 235
    iget-object v1, p1, Lorg/b/a/b/a$a;->D:Lorg/b/a/c;

    invoke-direct {p0, v1, v0}, Lorg/b/a/b/x;->a(Lorg/b/a/c;Ljava/util/HashMap;)Lorg/b/a/c;

    move-result-object v1

    iput-object v1, p1, Lorg/b/a/b/a$a;->D:Lorg/b/a/c;

    .line 236
    iget-object v1, p1, Lorg/b/a/b/a$a;->A:Lorg/b/a/c;

    invoke-direct {p0, v1, v0}, Lorg/b/a/b/x;->a(Lorg/b/a/c;Ljava/util/HashMap;)Lorg/b/a/c;

    move-result-object v1

    iput-object v1, p1, Lorg/b/a/b/a$a;->A:Lorg/b/a/c;

    .line 237
    iget-object v1, p1, Lorg/b/a/b/a$a;->B:Lorg/b/a/c;

    invoke-direct {p0, v1, v0}, Lorg/b/a/b/x;->a(Lorg/b/a/c;Ljava/util/HashMap;)Lorg/b/a/c;

    move-result-object v1

    iput-object v1, p1, Lorg/b/a/b/a$a;->B:Lorg/b/a/c;

    .line 238
    iget-object v1, p1, Lorg/b/a/b/a$a;->C:Lorg/b/a/c;

    invoke-direct {p0, v1, v0}, Lorg/b/a/b/x;->a(Lorg/b/a/c;Ljava/util/HashMap;)Lorg/b/a/c;

    move-result-object v1

    iput-object v1, p1, Lorg/b/a/b/a$a;->C:Lorg/b/a/c;

    .line 240
    iget-object v1, p1, Lorg/b/a/b/a$a;->m:Lorg/b/a/c;

    invoke-direct {p0, v1, v0}, Lorg/b/a/b/x;->a(Lorg/b/a/c;Ljava/util/HashMap;)Lorg/b/a/c;

    move-result-object v1

    iput-object v1, p1, Lorg/b/a/b/a$a;->m:Lorg/b/a/c;

    .line 241
    iget-object v1, p1, Lorg/b/a/b/a$a;->n:Lorg/b/a/c;

    invoke-direct {p0, v1, v0}, Lorg/b/a/b/x;->a(Lorg/b/a/c;Ljava/util/HashMap;)Lorg/b/a/c;

    move-result-object v1

    iput-object v1, p1, Lorg/b/a/b/a$a;->n:Lorg/b/a/c;

    .line 242
    iget-object v1, p1, Lorg/b/a/b/a$a;->o:Lorg/b/a/c;

    invoke-direct {p0, v1, v0}, Lorg/b/a/b/x;->a(Lorg/b/a/c;Ljava/util/HashMap;)Lorg/b/a/c;

    move-result-object v1

    iput-object v1, p1, Lorg/b/a/b/a$a;->o:Lorg/b/a/c;

    .line 243
    iget-object v1, p1, Lorg/b/a/b/a$a;->p:Lorg/b/a/c;

    invoke-direct {p0, v1, v0}, Lorg/b/a/b/x;->a(Lorg/b/a/c;Ljava/util/HashMap;)Lorg/b/a/c;

    move-result-object v1

    iput-object v1, p1, Lorg/b/a/b/a$a;->p:Lorg/b/a/c;

    .line 244
    iget-object v1, p1, Lorg/b/a/b/a$a;->q:Lorg/b/a/c;

    invoke-direct {p0, v1, v0}, Lorg/b/a/b/x;->a(Lorg/b/a/c;Ljava/util/HashMap;)Lorg/b/a/c;

    move-result-object v1

    iput-object v1, p1, Lorg/b/a/b/a$a;->q:Lorg/b/a/c;

    .line 245
    iget-object v1, p1, Lorg/b/a/b/a$a;->r:Lorg/b/a/c;

    invoke-direct {p0, v1, v0}, Lorg/b/a/b/x;->a(Lorg/b/a/c;Ljava/util/HashMap;)Lorg/b/a/c;

    move-result-object v1

    iput-object v1, p1, Lorg/b/a/b/a$a;->r:Lorg/b/a/c;

    .line 246
    iget-object v1, p1, Lorg/b/a/b/a$a;->s:Lorg/b/a/c;

    invoke-direct {p0, v1, v0}, Lorg/b/a/b/x;->a(Lorg/b/a/c;Ljava/util/HashMap;)Lorg/b/a/c;

    move-result-object v1

    iput-object v1, p1, Lorg/b/a/b/a$a;->s:Lorg/b/a/c;

    .line 247
    iget-object v1, p1, Lorg/b/a/b/a$a;->u:Lorg/b/a/c;

    invoke-direct {p0, v1, v0}, Lorg/b/a/b/x;->a(Lorg/b/a/c;Ljava/util/HashMap;)Lorg/b/a/c;

    move-result-object v1

    iput-object v1, p1, Lorg/b/a/b/a$a;->u:Lorg/b/a/c;

    .line 248
    iget-object v1, p1, Lorg/b/a/b/a$a;->t:Lorg/b/a/c;

    invoke-direct {p0, v1, v0}, Lorg/b/a/b/x;->a(Lorg/b/a/c;Ljava/util/HashMap;)Lorg/b/a/c;

    move-result-object v1

    iput-object v1, p1, Lorg/b/a/b/a$a;->t:Lorg/b/a/c;

    .line 249
    iget-object v1, p1, Lorg/b/a/b/a$a;->v:Lorg/b/a/c;

    invoke-direct {p0, v1, v0}, Lorg/b/a/b/x;->a(Lorg/b/a/c;Ljava/util/HashMap;)Lorg/b/a/c;

    move-result-object v1

    iput-object v1, p1, Lorg/b/a/b/a$a;->v:Lorg/b/a/c;

    .line 250
    iget-object v1, p1, Lorg/b/a/b/a$a;->w:Lorg/b/a/c;

    invoke-direct {p0, v1, v0}, Lorg/b/a/b/x;->a(Lorg/b/a/c;Ljava/util/HashMap;)Lorg/b/a/c;

    move-result-object v0

    iput-object v0, p1, Lorg/b/a/b/a$a;->w:Lorg/b/a/c;

    return-void
.end method

.method public b()Lorg/b/a/a;
    .locals 1

    .line 127
    sget-object v0, Lorg/b/a/f;->a:Lorg/b/a/f;

    invoke-virtual {p0, v0}, Lorg/b/a/b/x;->a(Lorg/b/a/f;)Lorg/b/a/a;

    move-result-object v0

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p0, p1, :cond_0

    return v0

    .line 304
    :cond_0
    instance-of v1, p1, Lorg/b/a/b/x;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 307
    :cond_1
    check-cast p1, Lorg/b/a/b/x;

    .line 309
    invoke-virtual {p0}, Lorg/b/a/b/x;->L()Lorg/b/a/a;

    move-result-object v1

    invoke-virtual {p1}, Lorg/b/a/b/x;->L()Lorg/b/a/a;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 310
    invoke-virtual {p0}, Lorg/b/a/b/x;->N()Lorg/b/a/b;

    move-result-object v1

    invoke-virtual {p1}, Lorg/b/a/b/x;->N()Lorg/b/a/b;

    move-result-object v3

    invoke-static {v1, v3}, Lorg/b/a/d/h;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 311
    invoke-virtual {p0}, Lorg/b/a/b/x;->O()Lorg/b/a/b;

    move-result-object v1

    invoke-virtual {p1}, Lorg/b/a/b/x;->O()Lorg/b/a/b;

    move-result-object p1

    invoke-static {v1, p1}, Lorg/b/a/d/h;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 322
    invoke-virtual {p0}, Lorg/b/a/b/x;->N()Lorg/b/a/b;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lorg/b/a/b/x;->N()Lorg/b/a/b;

    move-result-object v0

    invoke-virtual {v0}, Lorg/b/a/b;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    const v2, 0x12ea67c5

    add-int/2addr v0, v2

    .line 323
    invoke-virtual {p0}, Lorg/b/a/b/x;->O()Lorg/b/a/b;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {p0}, Lorg/b/a/b/x;->O()Lorg/b/a/b;

    move-result-object v1

    invoke-virtual {v1}, Lorg/b/a/b;->hashCode()I

    move-result v1

    :cond_1
    add-int/2addr v0, v1

    .line 324
    invoke-virtual {p0}, Lorg/b/a/b/x;->L()Lorg/b/a/a;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    mul-int/lit8 v1, v1, 0x7

    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 334
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "LimitChronology["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lorg/b/a/b/x;->L()Lorg/b/a/a;

    move-result-object v1

    invoke-virtual {v1}, Lorg/b/a/a;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 335
    invoke-virtual {p0}, Lorg/b/a/b/x;->N()Lorg/b/a/b;

    move-result-object v2

    const-string v3, "NoLimit"

    if-nez v2, :cond_0

    move-object v2, v3

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lorg/b/a/b/x;->N()Lorg/b/a/b;

    move-result-object v2

    invoke-virtual {v2}, Lorg/b/a/b;->toString()Ljava/lang/String;

    move-result-object v2

    :goto_0
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 336
    invoke-virtual {p0}, Lorg/b/a/b/x;->O()Lorg/b/a/b;

    move-result-object v1

    if-nez v1, :cond_1

    goto :goto_1

    :cond_1
    invoke-virtual {p0}, Lorg/b/a/b/x;->O()Lorg/b/a/b;

    move-result-object v1

    invoke-virtual {v1}, Lorg/b/a/b;->toString()Ljava/lang/String;

    move-result-object v3

    :goto_1
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x5d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
