.class public final Lorg/b/a/b/l;
.super Lorg/b/a/b/a;
.source "BuddhistChronology.java"


# static fields
.field private static final a:Lorg/b/a/c;

.field private static final b:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap<",
            "Lorg/b/a/f;",
            "Lorg/b/a/b/l;",
            ">;"
        }
    .end annotation
.end field

.field private static final c:Lorg/b/a/b/l;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 66
    new-instance v0, Lorg/b/a/b/h;

    const-string v1, "BE"

    invoke-direct {v0, v1}, Lorg/b/a/b/h;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/b/a/b/l;->a:Lorg/b/a/c;

    .line 72
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    sput-object v0, Lorg/b/a/b/l;->b:Ljava/util/concurrent/ConcurrentHashMap;

    .line 75
    sget-object v0, Lorg/b/a/f;->a:Lorg/b/a/f;

    invoke-static {v0}, Lorg/b/a/b/l;->b(Lorg/b/a/f;)Lorg/b/a/b/l;

    move-result-object v0

    sput-object v0, Lorg/b/a/b/l;->c:Lorg/b/a/b/l;

    return-void
.end method

.method private constructor <init>(Lorg/b/a/a;Ljava/lang/Object;)V
    .locals 0

    .line 132
    invoke-direct {p0, p1, p2}, Lorg/b/a/b/a;-><init>(Lorg/b/a/a;Ljava/lang/Object;)V

    return-void
.end method

.method public static b(Lorg/b/a/f;)Lorg/b/a/b/l;
    .locals 12

    if-nez p0, :cond_0

    .line 106
    invoke-static {}, Lorg/b/a/f;->a()Lorg/b/a/f;

    move-result-object p0

    .line 108
    :cond_0
    sget-object v0, Lorg/b/a/b/l;->b:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p0}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/b/a/b/l;

    if-nez v0, :cond_2

    .line 111
    new-instance v0, Lorg/b/a/b/l;

    const/4 v10, 0x0

    invoke-static {p0, v10}, Lorg/b/a/b/n;->a(Lorg/b/a/f;Lorg/b/a/t;)Lorg/b/a/b/n;

    move-result-object v1

    invoke-direct {v0, v1, v10}, Lorg/b/a/b/l;-><init>(Lorg/b/a/a;Ljava/lang/Object;)V

    .line 113
    new-instance v11, Lorg/b/a/b;

    const/4 v2, 0x1

    const/4 v3, 0x1

    const/4 v4, 0x1

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object v1, v11

    move-object v9, v0

    invoke-direct/range {v1 .. v9}, Lorg/b/a/b;-><init>(IIIIIIILorg/b/a/a;)V

    .line 114
    new-instance v1, Lorg/b/a/b/l;

    invoke-static {v0, v11, v10}, Lorg/b/a/b/x;->a(Lorg/b/a/a;Lorg/b/a/r;Lorg/b/a/r;)Lorg/b/a/b/x;

    move-result-object v0

    const-string v2, ""

    invoke-direct {v1, v0, v2}, Lorg/b/a/b/l;-><init>(Lorg/b/a/a;Ljava/lang/Object;)V

    .line 115
    sget-object v0, Lorg/b/a/b/l;->b:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p0, v1}, Ljava/util/concurrent/ConcurrentHashMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    move-object v0, p0

    check-cast v0, Lorg/b/a/b/l;

    if-eqz v0, :cond_1

    goto :goto_0

    :cond_1
    move-object v0, v1

    :cond_2
    :goto_0
    return-object v0
.end method


# virtual methods
.method public a(Lorg/b/a/f;)Lorg/b/a/a;
    .locals 1

    if-nez p1, :cond_0

    .line 162
    invoke-static {}, Lorg/b/a/f;->a()Lorg/b/a/f;

    move-result-object p1

    .line 164
    :cond_0
    invoke-virtual {p0}, Lorg/b/a/b/l;->a()Lorg/b/a/f;

    move-result-object v0

    if-ne p1, v0, :cond_1

    return-object p0

    .line 167
    :cond_1
    invoke-static {p1}, Lorg/b/a/b/l;->b(Lorg/b/a/f;)Lorg/b/a/b/l;

    move-result-object p1

    return-object p1
.end method

.method protected a(Lorg/b/a/b/a$a;)V
    .locals 6

    .line 216
    invoke-virtual {p0}, Lorg/b/a/b/l;->M()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    .line 218
    invoke-static {}, Lorg/b/a/i;->l()Lorg/b/a/i;

    move-result-object v0

    invoke-static {v0}, Lorg/b/a/d/t;->a(Lorg/b/a/i;)Lorg/b/a/d/t;

    move-result-object v0

    iput-object v0, p1, Lorg/b/a/b/a$a;->l:Lorg/b/a/h;

    .line 221
    iget-object v0, p1, Lorg/b/a/b/a$a;->E:Lorg/b/a/c;

    .line 222
    new-instance v1, Lorg/b/a/d/k;

    new-instance v2, Lorg/b/a/d/r;

    invoke-direct {v2, p0, v0}, Lorg/b/a/d/r;-><init>(Lorg/b/a/a;Lorg/b/a/c;)V

    const/16 v0, 0x21f

    invoke-direct {v1, v2, v0}, Lorg/b/a/d/k;-><init>(Lorg/b/a/c;I)V

    iput-object v1, p1, Lorg/b/a/b/a$a;->E:Lorg/b/a/c;

    .line 226
    iget-object v1, p1, Lorg/b/a/b/a$a;->F:Lorg/b/a/c;

    .line 227
    new-instance v1, Lorg/b/a/d/f;

    iget-object v2, p1, Lorg/b/a/b/a$a;->E:Lorg/b/a/c;

    iget-object v3, p1, Lorg/b/a/b/a$a;->l:Lorg/b/a/h;

    .line 228
    invoke-static {}, Lorg/b/a/d;->t()Lorg/b/a/d;

    move-result-object v4

    invoke-direct {v1, v2, v3, v4}, Lorg/b/a/d/f;-><init>(Lorg/b/a/c;Lorg/b/a/h;Lorg/b/a/d;)V

    iput-object v1, p1, Lorg/b/a/b/a$a;->F:Lorg/b/a/c;

    .line 231
    iget-object v1, p1, Lorg/b/a/b/a$a;->B:Lorg/b/a/c;

    .line 232
    new-instance v2, Lorg/b/a/d/k;

    new-instance v3, Lorg/b/a/d/r;

    invoke-direct {v3, p0, v1}, Lorg/b/a/d/r;-><init>(Lorg/b/a/a;Lorg/b/a/c;)V

    invoke-direct {v2, v3, v0}, Lorg/b/a/d/k;-><init>(Lorg/b/a/c;I)V

    iput-object v2, p1, Lorg/b/a/b/a$a;->B:Lorg/b/a/c;

    .line 235
    new-instance v0, Lorg/b/a/d/k;

    iget-object v1, p1, Lorg/b/a/b/a$a;->F:Lorg/b/a/c;

    const/16 v2, 0x63

    invoke-direct {v0, v1, v2}, Lorg/b/a/d/k;-><init>(Lorg/b/a/c;I)V

    .line 236
    new-instance v1, Lorg/b/a/d/g;

    iget-object v2, p1, Lorg/b/a/b/a$a;->l:Lorg/b/a/h;

    .line 237
    invoke-static {}, Lorg/b/a/d;->v()Lorg/b/a/d;

    move-result-object v3

    const/16 v4, 0x64

    invoke-direct {v1, v0, v2, v3, v4}, Lorg/b/a/d/g;-><init>(Lorg/b/a/c;Lorg/b/a/h;Lorg/b/a/d;I)V

    iput-object v1, p1, Lorg/b/a/b/a$a;->H:Lorg/b/a/c;

    .line 238
    iget-object v0, p1, Lorg/b/a/b/a$a;->H:Lorg/b/a/c;

    invoke-virtual {v0}, Lorg/b/a/c;->e()Lorg/b/a/h;

    move-result-object v0

    iput-object v0, p1, Lorg/b/a/b/a$a;->k:Lorg/b/a/h;

    .line 240
    new-instance v0, Lorg/b/a/d/o;

    iget-object v1, p1, Lorg/b/a/b/a$a;->H:Lorg/b/a/c;

    check-cast v1, Lorg/b/a/d/g;

    invoke-direct {v0, v1}, Lorg/b/a/d/o;-><init>(Lorg/b/a/d/g;)V

    .line 242
    new-instance v1, Lorg/b/a/d/k;

    .line 243
    invoke-static {}, Lorg/b/a/d;->u()Lorg/b/a/d;

    move-result-object v2

    const/4 v3, 0x1

    invoke-direct {v1, v0, v2, v3}, Lorg/b/a/d/k;-><init>(Lorg/b/a/c;Lorg/b/a/d;I)V

    iput-object v1, p1, Lorg/b/a/b/a$a;->G:Lorg/b/a/c;

    .line 245
    new-instance v0, Lorg/b/a/d/o;

    iget-object v1, p1, Lorg/b/a/b/a$a;->B:Lorg/b/a/c;

    iget-object v2, p1, Lorg/b/a/b/a$a;->k:Lorg/b/a/h;

    .line 246
    invoke-static {}, Lorg/b/a/d;->q()Lorg/b/a/d;

    move-result-object v5

    invoke-direct {v0, v1, v2, v5, v4}, Lorg/b/a/d/o;-><init>(Lorg/b/a/c;Lorg/b/a/h;Lorg/b/a/d;I)V

    .line 247
    new-instance v1, Lorg/b/a/d/k;

    .line 248
    invoke-static {}, Lorg/b/a/d;->q()Lorg/b/a/d;

    move-result-object v2

    invoke-direct {v1, v0, v2, v3}, Lorg/b/a/d/k;-><init>(Lorg/b/a/c;Lorg/b/a/d;I)V

    iput-object v1, p1, Lorg/b/a/b/a$a;->C:Lorg/b/a/c;

    .line 250
    sget-object v0, Lorg/b/a/b/l;->a:Lorg/b/a/c;

    iput-object v0, p1, Lorg/b/a/b/a$a;->I:Lorg/b/a/c;

    :cond_0
    return-void
.end method

.method public b()Lorg/b/a/a;
    .locals 1

    .line 151
    sget-object v0, Lorg/b/a/b/l;->c:Lorg/b/a/b/l;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1

    if-ne p0, p1, :cond_0

    const/4 p1, 0x1

    return p1

    .line 182
    :cond_0
    instance-of v0, p1, Lorg/b/a/b/l;

    if-eqz v0, :cond_1

    .line 183
    check-cast p1, Lorg/b/a/b/l;

    .line 184
    invoke-virtual {p0}, Lorg/b/a/b/l;->a()Lorg/b/a/f;

    move-result-object v0

    invoke-virtual {p1}, Lorg/b/a/b/l;->a()Lorg/b/a/f;

    move-result-object p1

    invoke-virtual {v0, p1}, Lorg/b/a/f;->equals(Ljava/lang/Object;)Z

    move-result p1

    return p1

    :cond_1
    const/4 p1, 0x0

    return p1
.end method

.method public hashCode()I
    .locals 2

    const-string v0, "Buddhist"

    .line 196
    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0xb

    invoke-virtual {p0}, Lorg/b/a/b/l;->a()Lorg/b/a/f;

    move-result-object v1

    invoke-virtual {v1}, Lorg/b/a/f;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .line 208
    invoke-virtual {p0}, Lorg/b/a/b/l;->a()Lorg/b/a/f;

    move-result-object v0

    const-string v1, "BuddhistChronology"

    if-eqz v0, :cond_0

    .line 210
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x5b

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Lorg/b/a/f;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v0, 0x5d

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    :cond_0
    return-object v1
.end method
