.class final Lorg/b/a/b/n$b;
.super Lorg/b/a/b/n$a;
.source "GJChronology.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/b/a/b/n;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "b"
.end annotation


# instance fields
.field final synthetic h:Lorg/b/a/b/n;


# direct methods
.method constructor <init>(Lorg/b/a/b/n;Lorg/b/a/c;Lorg/b/a/c;J)V
    .locals 8

    const/4 v4, 0x0

    const/4 v7, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-wide v5, p4

    .line 951
    invoke-direct/range {v0 .. v7}, Lorg/b/a/b/n$b;-><init>(Lorg/b/a/b/n;Lorg/b/a/c;Lorg/b/a/c;Lorg/b/a/h;JZ)V

    return-void
.end method

.method constructor <init>(Lorg/b/a/b/n;Lorg/b/a/c;Lorg/b/a/c;Lorg/b/a/h;J)V
    .locals 8

    const/4 v7, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-wide v5, p5

    .line 962
    invoke-direct/range {v0 .. v7}, Lorg/b/a/b/n$b;-><init>(Lorg/b/a/b/n;Lorg/b/a/c;Lorg/b/a/c;Lorg/b/a/h;JZ)V

    return-void
.end method

.method constructor <init>(Lorg/b/a/b/n;Lorg/b/a/c;Lorg/b/a/c;Lorg/b/a/h;JZ)V
    .locals 7

    .line 985
    iput-object p1, p0, Lorg/b/a/b/n$b;->h:Lorg/b/a/b/n;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-wide v4, p5

    move v6, p7

    .line 986
    invoke-direct/range {v0 .. v6}, Lorg/b/a/b/n$a;-><init>(Lorg/b/a/b/n;Lorg/b/a/c;Lorg/b/a/c;JZ)V

    if-nez p4, :cond_0

    .line 988
    new-instance p4, Lorg/b/a/b/n$c;

    iget-object p1, p0, Lorg/b/a/b/n$b;->e:Lorg/b/a/h;

    invoke-direct {p4, p1, p0}, Lorg/b/a/b/n$c;-><init>(Lorg/b/a/h;Lorg/b/a/b/n$b;)V

    .line 990
    :cond_0
    iput-object p4, p0, Lorg/b/a/b/n$b;->e:Lorg/b/a/h;

    return-void
.end method

.method constructor <init>(Lorg/b/a/b/n;Lorg/b/a/c;Lorg/b/a/c;Lorg/b/a/h;Lorg/b/a/h;J)V
    .locals 8

    const/4 v7, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-wide v5, p6

    .line 973
    invoke-direct/range {v0 .. v7}, Lorg/b/a/b/n$b;-><init>(Lorg/b/a/b/n;Lorg/b/a/c;Lorg/b/a/c;Lorg/b/a/h;JZ)V

    .line 974
    iput-object p5, p0, Lorg/b/a/b/n$b;->f:Lorg/b/a/h;

    return-void
.end method


# virtual methods
.method public a(JI)J
    .locals 4

    .line 994
    iget-wide v0, p0, Lorg/b/a/b/n$b;->c:J

    cmp-long v2, p1, v0

    if-ltz v2, :cond_2

    .line 995
    iget-object v0, p0, Lorg/b/a/b/n$b;->b:Lorg/b/a/c;

    invoke-virtual {v0, p1, p2, p3}, Lorg/b/a/c;->a(JI)J

    move-result-wide p1

    .line 996
    iget-wide v0, p0, Lorg/b/a/b/n$b;->c:J

    cmp-long p3, p1, v0

    if-gez p3, :cond_3

    .line 998
    iget-object p3, p0, Lorg/b/a/b/n$b;->h:Lorg/b/a/b/n;

    invoke-static {p3}, Lorg/b/a/b/n;->a(Lorg/b/a/b/n;)J

    move-result-wide v0

    add-long/2addr v0, p1

    iget-wide v2, p0, Lorg/b/a/b/n$b;->c:J

    cmp-long p3, v0, v2

    if-gez p3, :cond_3

    .line 999
    iget-boolean p3, p0, Lorg/b/a/b/n$b;->d:Z

    const/4 v0, -0x1

    if-eqz p3, :cond_0

    .line 1000
    iget-object p3, p0, Lorg/b/a/b/n$b;->h:Lorg/b/a/b/n;

    invoke-static {p3}, Lorg/b/a/b/n;->b(Lorg/b/a/b/n;)Lorg/b/a/b/t;

    move-result-object p3

    invoke-virtual {p3}, Lorg/b/a/b/t;->z()Lorg/b/a/c;

    move-result-object p3

    invoke-virtual {p3, p1, p2}, Lorg/b/a/c;->a(J)I

    move-result p3

    if-gtz p3, :cond_1

    .line 1002
    iget-object p3, p0, Lorg/b/a/b/n$b;->h:Lorg/b/a/b/n;

    invoke-static {p3}, Lorg/b/a/b/n;->b(Lorg/b/a/b/n;)Lorg/b/a/b/t;

    move-result-object p3

    invoke-virtual {p3}, Lorg/b/a/b/t;->z()Lorg/b/a/c;

    move-result-object p3

    invoke-virtual {p3, p1, p2, v0}, Lorg/b/a/c;->a(JI)J

    move-result-wide p1

    goto :goto_0

    .line 1005
    :cond_0
    iget-object p3, p0, Lorg/b/a/b/n$b;->h:Lorg/b/a/b/n;

    invoke-static {p3}, Lorg/b/a/b/n;->b(Lorg/b/a/b/n;)Lorg/b/a/b/t;

    move-result-object p3

    invoke-virtual {p3}, Lorg/b/a/b/t;->E()Lorg/b/a/c;

    move-result-object p3

    invoke-virtual {p3, p1, p2}, Lorg/b/a/c;->a(J)I

    move-result p3

    if-gtz p3, :cond_1

    .line 1007
    iget-object p3, p0, Lorg/b/a/b/n$b;->h:Lorg/b/a/b/n;

    invoke-static {p3}, Lorg/b/a/b/n;->b(Lorg/b/a/b/n;)Lorg/b/a/b/t;

    move-result-object p3

    invoke-virtual {p3}, Lorg/b/a/b/t;->E()Lorg/b/a/c;

    move-result-object p3

    invoke-virtual {p3, p1, p2, v0}, Lorg/b/a/c;->a(JI)J

    move-result-wide p1

    .line 1010
    :cond_1
    :goto_0
    invoke-virtual {p0, p1, p2}, Lorg/b/a/b/n$b;->k(J)J

    move-result-wide p1

    goto :goto_1

    .line 1014
    :cond_2
    iget-object v0, p0, Lorg/b/a/b/n$b;->a:Lorg/b/a/c;

    invoke-virtual {v0, p1, p2, p3}, Lorg/b/a/c;->a(JI)J

    move-result-wide p1

    .line 1015
    iget-wide v0, p0, Lorg/b/a/b/n$b;->c:J

    cmp-long p3, p1, v0

    if-ltz p3, :cond_3

    .line 1017
    iget-object p3, p0, Lorg/b/a/b/n$b;->h:Lorg/b/a/b/n;

    invoke-static {p3}, Lorg/b/a/b/n;->a(Lorg/b/a/b/n;)J

    move-result-wide v0

    sub-long v0, p1, v0

    iget-wide v2, p0, Lorg/b/a/b/n$b;->c:J

    cmp-long p3, v0, v2

    if-ltz p3, :cond_3

    .line 1019
    invoke-virtual {p0, p1, p2}, Lorg/b/a/b/n$b;->j(J)J

    move-result-wide p1

    :cond_3
    :goto_1
    return-wide p1
.end method

.method public a(JJ)J
    .locals 3

    .line 1027
    iget-wide v0, p0, Lorg/b/a/b/n$b;->c:J

    cmp-long v2, p1, v0

    if-ltz v2, :cond_2

    .line 1028
    iget-object v0, p0, Lorg/b/a/b/n$b;->b:Lorg/b/a/c;

    invoke-virtual {v0, p1, p2, p3, p4}, Lorg/b/a/c;->a(JJ)J

    move-result-wide p1

    .line 1029
    iget-wide p3, p0, Lorg/b/a/b/n$b;->c:J

    cmp-long v0, p1, p3

    if-gez v0, :cond_3

    .line 1031
    iget-object p3, p0, Lorg/b/a/b/n$b;->h:Lorg/b/a/b/n;

    invoke-static {p3}, Lorg/b/a/b/n;->a(Lorg/b/a/b/n;)J

    move-result-wide p3

    add-long/2addr p3, p1

    iget-wide v0, p0, Lorg/b/a/b/n$b;->c:J

    cmp-long v2, p3, v0

    if-gez v2, :cond_3

    .line 1032
    iget-boolean p3, p0, Lorg/b/a/b/n$b;->d:Z

    const/4 p4, -0x1

    if-eqz p3, :cond_0

    .line 1033
    iget-object p3, p0, Lorg/b/a/b/n$b;->h:Lorg/b/a/b/n;

    invoke-static {p3}, Lorg/b/a/b/n;->b(Lorg/b/a/b/n;)Lorg/b/a/b/t;

    move-result-object p3

    invoke-virtual {p3}, Lorg/b/a/b/t;->z()Lorg/b/a/c;

    move-result-object p3

    invoke-virtual {p3, p1, p2}, Lorg/b/a/c;->a(J)I

    move-result p3

    if-gtz p3, :cond_1

    .line 1035
    iget-object p3, p0, Lorg/b/a/b/n$b;->h:Lorg/b/a/b/n;

    invoke-static {p3}, Lorg/b/a/b/n;->b(Lorg/b/a/b/n;)Lorg/b/a/b/t;

    move-result-object p3

    invoke-virtual {p3}, Lorg/b/a/b/t;->z()Lorg/b/a/c;

    move-result-object p3

    invoke-virtual {p3, p1, p2, p4}, Lorg/b/a/c;->a(JI)J

    move-result-wide p1

    goto :goto_0

    .line 1038
    :cond_0
    iget-object p3, p0, Lorg/b/a/b/n$b;->h:Lorg/b/a/b/n;

    invoke-static {p3}, Lorg/b/a/b/n;->b(Lorg/b/a/b/n;)Lorg/b/a/b/t;

    move-result-object p3

    invoke-virtual {p3}, Lorg/b/a/b/t;->E()Lorg/b/a/c;

    move-result-object p3

    invoke-virtual {p3, p1, p2}, Lorg/b/a/c;->a(J)I

    move-result p3

    if-gtz p3, :cond_1

    .line 1040
    iget-object p3, p0, Lorg/b/a/b/n$b;->h:Lorg/b/a/b/n;

    invoke-static {p3}, Lorg/b/a/b/n;->b(Lorg/b/a/b/n;)Lorg/b/a/b/t;

    move-result-object p3

    invoke-virtual {p3}, Lorg/b/a/b/t;->E()Lorg/b/a/c;

    move-result-object p3

    invoke-virtual {p3, p1, p2, p4}, Lorg/b/a/c;->a(JI)J

    move-result-wide p1

    .line 1043
    :cond_1
    :goto_0
    invoke-virtual {p0, p1, p2}, Lorg/b/a/b/n$b;->k(J)J

    move-result-wide p1

    goto :goto_1

    .line 1047
    :cond_2
    iget-object v0, p0, Lorg/b/a/b/n$b;->a:Lorg/b/a/c;

    invoke-virtual {v0, p1, p2, p3, p4}, Lorg/b/a/c;->a(JJ)J

    move-result-wide p1

    .line 1048
    iget-wide p3, p0, Lorg/b/a/b/n$b;->c:J

    cmp-long v0, p1, p3

    if-ltz v0, :cond_3

    .line 1050
    iget-object p3, p0, Lorg/b/a/b/n$b;->h:Lorg/b/a/b/n;

    invoke-static {p3}, Lorg/b/a/b/n;->a(Lorg/b/a/b/n;)J

    move-result-wide p3

    sub-long p3, p1, p3

    iget-wide v0, p0, Lorg/b/a/b/n$b;->c:J

    cmp-long v2, p3, v0

    if-ltz v2, :cond_3

    .line 1052
    invoke-virtual {p0, p1, p2}, Lorg/b/a/b/n$b;->j(J)J

    move-result-wide p1

    :cond_3
    :goto_1
    return-wide p1
.end method

.method public c(J)I
    .locals 3

    .line 1118
    iget-wide v0, p0, Lorg/b/a/b/n$b;->c:J

    cmp-long v2, p1, v0

    if-ltz v2, :cond_0

    .line 1119
    iget-object v0, p0, Lorg/b/a/b/n$b;->b:Lorg/b/a/c;

    invoke-virtual {v0, p1, p2}, Lorg/b/a/c;->c(J)I

    move-result p1

    return p1

    .line 1121
    :cond_0
    iget-object v0, p0, Lorg/b/a/b/n$b;->a:Lorg/b/a/c;

    invoke-virtual {v0, p1, p2}, Lorg/b/a/c;->c(J)I

    move-result p1

    return p1
.end method
