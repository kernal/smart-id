.class abstract Lorg/b/a/b/c;
.super Lorg/b/a/b/a;
.source "BasicChronology.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/b/a/b/c$b;,
        Lorg/b/a/b/c$a;
    }
.end annotation


# static fields
.field private static final a:Lorg/b/a/h;

.field private static final b:Lorg/b/a/h;

.field private static final c:Lorg/b/a/h;

.field private static final d:Lorg/b/a/h;

.field private static final e:Lorg/b/a/h;

.field private static final f:Lorg/b/a/h;

.field private static final g:Lorg/b/a/h;

.field private static final h:Lorg/b/a/c;

.field private static final i:Lorg/b/a/c;

.field private static final j:Lorg/b/a/c;

.field private static final k:Lorg/b/a/c;

.field private static final l:Lorg/b/a/c;

.field private static final m:Lorg/b/a/c;

.field private static final n:Lorg/b/a/c;

.field private static final o:Lorg/b/a/c;

.field private static final p:Lorg/b/a/c;

.field private static final q:Lorg/b/a/c;

.field private static final r:Lorg/b/a/c;


# instance fields
.field private final transient s:[Lorg/b/a/b/c$b;

.field private final t:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .line 76
    sget-object v0, Lorg/b/a/d/j;->a:Lorg/b/a/h;

    sput-object v0, Lorg/b/a/b/c;->a:Lorg/b/a/h;

    .line 77
    new-instance v0, Lorg/b/a/d/n;

    .line 78
    invoke-static {}, Lorg/b/a/i;->b()Lorg/b/a/i;

    move-result-object v1

    const-wide/16 v2, 0x3e8

    invoke-direct {v0, v1, v2, v3}, Lorg/b/a/d/n;-><init>(Lorg/b/a/i;J)V

    sput-object v0, Lorg/b/a/b/c;->b:Lorg/b/a/h;

    .line 79
    new-instance v0, Lorg/b/a/d/n;

    .line 80
    invoke-static {}, Lorg/b/a/i;->c()Lorg/b/a/i;

    move-result-object v1

    const-wide/32 v2, 0xea60

    invoke-direct {v0, v1, v2, v3}, Lorg/b/a/d/n;-><init>(Lorg/b/a/i;J)V

    sput-object v0, Lorg/b/a/b/c;->c:Lorg/b/a/h;

    .line 81
    new-instance v0, Lorg/b/a/d/n;

    .line 82
    invoke-static {}, Lorg/b/a/i;->d()Lorg/b/a/i;

    move-result-object v1

    const-wide/32 v2, 0x36ee80

    invoke-direct {v0, v1, v2, v3}, Lorg/b/a/d/n;-><init>(Lorg/b/a/i;J)V

    sput-object v0, Lorg/b/a/b/c;->d:Lorg/b/a/h;

    .line 83
    new-instance v0, Lorg/b/a/d/n;

    .line 84
    invoke-static {}, Lorg/b/a/i;->e()Lorg/b/a/i;

    move-result-object v1

    const-wide/32 v2, 0x2932e00

    invoke-direct {v0, v1, v2, v3}, Lorg/b/a/d/n;-><init>(Lorg/b/a/i;J)V

    sput-object v0, Lorg/b/a/b/c;->e:Lorg/b/a/h;

    .line 85
    new-instance v0, Lorg/b/a/d/n;

    .line 86
    invoke-static {}, Lorg/b/a/i;->f()Lorg/b/a/i;

    move-result-object v1

    const-wide/32 v2, 0x5265c00

    invoke-direct {v0, v1, v2, v3}, Lorg/b/a/d/n;-><init>(Lorg/b/a/i;J)V

    sput-object v0, Lorg/b/a/b/c;->f:Lorg/b/a/h;

    .line 87
    new-instance v0, Lorg/b/a/d/n;

    .line 88
    invoke-static {}, Lorg/b/a/i;->g()Lorg/b/a/i;

    move-result-object v1

    const-wide/32 v2, 0x240c8400

    invoke-direct {v0, v1, v2, v3}, Lorg/b/a/d/n;-><init>(Lorg/b/a/i;J)V

    sput-object v0, Lorg/b/a/b/c;->g:Lorg/b/a/h;

    .line 90
    new-instance v0, Lorg/b/a/d/l;

    .line 91
    invoke-static {}, Lorg/b/a/d;->a()Lorg/b/a/d;

    move-result-object v1

    sget-object v2, Lorg/b/a/b/c;->a:Lorg/b/a/h;

    sget-object v3, Lorg/b/a/b/c;->b:Lorg/b/a/h;

    invoke-direct {v0, v1, v2, v3}, Lorg/b/a/d/l;-><init>(Lorg/b/a/d;Lorg/b/a/h;Lorg/b/a/h;)V

    sput-object v0, Lorg/b/a/b/c;->h:Lorg/b/a/c;

    .line 93
    new-instance v0, Lorg/b/a/d/l;

    .line 94
    invoke-static {}, Lorg/b/a/d;->b()Lorg/b/a/d;

    move-result-object v1

    sget-object v2, Lorg/b/a/b/c;->a:Lorg/b/a/h;

    sget-object v3, Lorg/b/a/b/c;->f:Lorg/b/a/h;

    invoke-direct {v0, v1, v2, v3}, Lorg/b/a/d/l;-><init>(Lorg/b/a/d;Lorg/b/a/h;Lorg/b/a/h;)V

    sput-object v0, Lorg/b/a/b/c;->i:Lorg/b/a/c;

    .line 96
    new-instance v0, Lorg/b/a/d/l;

    .line 97
    invoke-static {}, Lorg/b/a/d;->c()Lorg/b/a/d;

    move-result-object v1

    sget-object v2, Lorg/b/a/b/c;->b:Lorg/b/a/h;

    sget-object v3, Lorg/b/a/b/c;->c:Lorg/b/a/h;

    invoke-direct {v0, v1, v2, v3}, Lorg/b/a/d/l;-><init>(Lorg/b/a/d;Lorg/b/a/h;Lorg/b/a/h;)V

    sput-object v0, Lorg/b/a/b/c;->j:Lorg/b/a/c;

    .line 99
    new-instance v0, Lorg/b/a/d/l;

    .line 100
    invoke-static {}, Lorg/b/a/d;->d()Lorg/b/a/d;

    move-result-object v1

    sget-object v2, Lorg/b/a/b/c;->b:Lorg/b/a/h;

    sget-object v3, Lorg/b/a/b/c;->f:Lorg/b/a/h;

    invoke-direct {v0, v1, v2, v3}, Lorg/b/a/d/l;-><init>(Lorg/b/a/d;Lorg/b/a/h;Lorg/b/a/h;)V

    sput-object v0, Lorg/b/a/b/c;->k:Lorg/b/a/c;

    .line 102
    new-instance v0, Lorg/b/a/d/l;

    .line 103
    invoke-static {}, Lorg/b/a/d;->e()Lorg/b/a/d;

    move-result-object v1

    sget-object v2, Lorg/b/a/b/c;->c:Lorg/b/a/h;

    sget-object v3, Lorg/b/a/b/c;->d:Lorg/b/a/h;

    invoke-direct {v0, v1, v2, v3}, Lorg/b/a/d/l;-><init>(Lorg/b/a/d;Lorg/b/a/h;Lorg/b/a/h;)V

    sput-object v0, Lorg/b/a/b/c;->l:Lorg/b/a/c;

    .line 105
    new-instance v0, Lorg/b/a/d/l;

    .line 106
    invoke-static {}, Lorg/b/a/d;->f()Lorg/b/a/d;

    move-result-object v1

    sget-object v2, Lorg/b/a/b/c;->c:Lorg/b/a/h;

    sget-object v3, Lorg/b/a/b/c;->f:Lorg/b/a/h;

    invoke-direct {v0, v1, v2, v3}, Lorg/b/a/d/l;-><init>(Lorg/b/a/d;Lorg/b/a/h;Lorg/b/a/h;)V

    sput-object v0, Lorg/b/a/b/c;->m:Lorg/b/a/c;

    .line 108
    new-instance v0, Lorg/b/a/d/l;

    .line 109
    invoke-static {}, Lorg/b/a/d;->g()Lorg/b/a/d;

    move-result-object v1

    sget-object v2, Lorg/b/a/b/c;->d:Lorg/b/a/h;

    sget-object v3, Lorg/b/a/b/c;->f:Lorg/b/a/h;

    invoke-direct {v0, v1, v2, v3}, Lorg/b/a/d/l;-><init>(Lorg/b/a/d;Lorg/b/a/h;Lorg/b/a/h;)V

    sput-object v0, Lorg/b/a/b/c;->n:Lorg/b/a/c;

    .line 111
    new-instance v0, Lorg/b/a/d/l;

    .line 112
    invoke-static {}, Lorg/b/a/d;->i()Lorg/b/a/d;

    move-result-object v1

    sget-object v2, Lorg/b/a/b/c;->d:Lorg/b/a/h;

    sget-object v3, Lorg/b/a/b/c;->e:Lorg/b/a/h;

    invoke-direct {v0, v1, v2, v3}, Lorg/b/a/d/l;-><init>(Lorg/b/a/d;Lorg/b/a/h;Lorg/b/a/h;)V

    sput-object v0, Lorg/b/a/b/c;->o:Lorg/b/a/c;

    .line 114
    new-instance v0, Lorg/b/a/d/u;

    sget-object v1, Lorg/b/a/b/c;->n:Lorg/b/a/c;

    .line 115
    invoke-static {}, Lorg/b/a/d;->h()Lorg/b/a/d;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lorg/b/a/d/u;-><init>(Lorg/b/a/c;Lorg/b/a/d;)V

    sput-object v0, Lorg/b/a/b/c;->p:Lorg/b/a/c;

    .line 117
    new-instance v0, Lorg/b/a/d/u;

    sget-object v1, Lorg/b/a/b/c;->o:Lorg/b/a/c;

    .line 118
    invoke-static {}, Lorg/b/a/d;->j()Lorg/b/a/d;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lorg/b/a/d/u;-><init>(Lorg/b/a/c;Lorg/b/a/d;)V

    sput-object v0, Lorg/b/a/b/c;->q:Lorg/b/a/c;

    .line 120
    new-instance v0, Lorg/b/a/b/c$a;

    invoke-direct {v0}, Lorg/b/a/b/c$a;-><init>()V

    sput-object v0, Lorg/b/a/b/c;->r:Lorg/b/a/c;

    return-void
.end method

.method constructor <init>(Lorg/b/a/a;Ljava/lang/Object;I)V
    .locals 1

    .line 131
    invoke-direct {p0, p1, p2}, Lorg/b/a/b/a;-><init>(Lorg/b/a/a;Ljava/lang/Object;)V

    const/16 p1, 0x400

    .line 126
    new-array p1, p1, [Lorg/b/a/b/c$b;

    iput-object p1, p0, Lorg/b/a/b/c;->s:[Lorg/b/a/b/c$b;

    const/4 p1, 0x1

    if-lt p3, p1, :cond_0

    const/4 p1, 0x7

    if-gt p3, p1, :cond_0

    .line 138
    iput p3, p0, Lorg/b/a/b/c;->t:I

    return-void

    .line 134
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "Invalid min days in first week: "

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method static synthetic X()Lorg/b/a/h;
    .locals 1

    .line 50
    sget-object v0, Lorg/b/a/b/c;->e:Lorg/b/a/h;

    return-object v0
.end method

.method static synthetic Y()Lorg/b/a/h;
    .locals 1

    .line 50
    sget-object v0, Lorg/b/a/b/c;->f:Lorg/b/a/h;

    return-object v0
.end method

.method private b(IIII)J
    .locals 6

    .line 186
    invoke-virtual {p0, p1, p2, p3}, Lorg/b/a/b/c;->b(III)J

    move-result-wide v0

    const-wide/high16 v2, -0x8000000000000000L

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    add-int/lit8 p3, p3, 0x1

    .line 189
    invoke-virtual {p0, p1, p2, p3}, Lorg/b/a/b/c;->b(III)J

    move-result-wide v0

    const p1, 0x5265c00

    sub-int/2addr p4, p1

    :cond_0
    int-to-long p1, p4

    add-long/2addr p1, v0

    const-wide/16 p3, 0x0

    cmp-long v4, p1, p3

    if-gez v4, :cond_1

    cmp-long v5, v0, p3

    if-lez v5, :cond_1

    const-wide p1, 0x7fffffffffffffffL

    return-wide p1

    :cond_1
    if-lez v4, :cond_2

    cmp-long v4, v0, p3

    if-gez v4, :cond_2

    return-wide v2

    :cond_2
    return-wide p1
.end method

.method private h(I)Lorg/b/a/b/c$b;
    .locals 4

    .line 782
    iget-object v0, p0, Lorg/b/a/b/c;->s:[Lorg/b/a/b/c$b;

    and-int/lit16 v1, p1, 0x3ff

    aget-object v0, v0, v1

    if-eqz v0, :cond_0

    .line 783
    iget v2, v0, Lorg/b/a/b/c$b;->a:I

    if-eq v2, p1, :cond_1

    .line 784
    :cond_0
    new-instance v0, Lorg/b/a/b/c$b;

    invoke-virtual {p0, p1}, Lorg/b/a/b/c;->f(I)J

    move-result-wide v2

    invoke-direct {v0, p1, v2, v3}, Lorg/b/a/b/c$b;-><init>(IJ)V

    .line 785
    iget-object p1, p0, Lorg/b/a/b/c;->s:[Lorg/b/a/b/c$b;

    aput-object v0, p1, v1

    :cond_1
    return-object v0
.end method


# virtual methods
.method public N()I
    .locals 1

    .line 204
    iget v0, p0, Lorg/b/a/b/c;->t:I

    return v0
.end method

.method O()I
    .locals 1

    const/16 v0, 0x16e

    return v0
.end method

.method P()I
    .locals 1

    const/16 v0, 0x1f

    return v0
.end method

.method abstract Q()I
.end method

.method abstract R()I
.end method

.method S()I
    .locals 1

    const/16 v0, 0xc

    return v0
.end method

.method abstract T()J
.end method

.method abstract U()J
.end method

.method abstract V()J
.end method

.method abstract W()J
.end method

.method a(I)I
    .locals 0

    .line 344
    invoke-virtual {p0, p1}, Lorg/b/a/b/c;->e(I)Z

    move-result p1

    if-eqz p1, :cond_0

    const/16 p1, 0x16e

    goto :goto_0

    :cond_0
    const/16 p1, 0x16d

    :goto_0
    return p1
.end method

.method a(J)I
    .locals 8

    .line 426
    invoke-virtual {p0}, Lorg/b/a/b/c;->U()J

    move-result-wide v0

    const/4 v2, 0x1

    shr-long v2, p1, v2

    .line 427
    invoke-virtual {p0}, Lorg/b/a/b/c;->W()J

    move-result-wide v4

    add-long/2addr v2, v4

    const-wide/16 v4, 0x0

    cmp-long v6, v2, v4

    if-gez v6, :cond_0

    sub-long/2addr v2, v0

    const-wide/16 v6, 0x1

    add-long/2addr v2, v6

    .line 431
    :cond_0
    div-long/2addr v2, v0

    long-to-int v0, v2

    .line 433
    invoke-virtual {p0, v0}, Lorg/b/a/b/c;->d(I)J

    move-result-wide v1

    sub-long v6, p1, v1

    cmp-long v3, v6, v4

    if-gez v3, :cond_1

    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_1
    const-wide v3, 0x757b12c00L

    cmp-long v5, v6, v3

    if-ltz v5, :cond_3

    .line 441
    invoke-virtual {p0, v0}, Lorg/b/a/b/c;->e(I)Z

    move-result v5

    if-eqz v5, :cond_2

    const-wide v3, 0x75cd78800L

    :cond_2
    add-long/2addr v1, v3

    cmp-long v3, v1, p1

    if-gtz v3, :cond_3

    add-int/lit8 v0, v0, 0x1

    :cond_3
    :goto_0
    return v0
.end method

.method abstract a(JI)I
.end method

.method a(JII)I
    .locals 2

    .line 495
    invoke-virtual {p0, p3}, Lorg/b/a/b/c;->d(I)J

    move-result-wide v0

    .line 496
    invoke-virtual {p0, p3, p4}, Lorg/b/a/b/c;->c(II)J

    move-result-wide p3

    add-long/2addr v0, p3

    sub-long/2addr p1, v0

    const-wide/32 p3, 0x5265c00

    .line 497
    div-long/2addr p1, p3

    long-to-int p2, p1

    add-int/lit8 p2, p2, 0x1

    return p2
.end method

.method a(II)J
    .locals 2

    .line 398
    invoke-virtual {p0, p1}, Lorg/b/a/b/c;->d(I)J

    move-result-wide v0

    .line 399
    invoke-virtual {p0, p1, p2}, Lorg/b/a/b/c;->c(II)J

    move-result-wide p1

    add-long/2addr v0, p1

    return-wide v0
.end method

.method a(III)J
    .locals 4

    .line 412
    invoke-virtual {p0, p1}, Lorg/b/a/b/c;->d(I)J

    move-result-wide v0

    .line 413
    invoke-virtual {p0, p1, p2}, Lorg/b/a/b/c;->c(II)J

    move-result-wide p1

    add-long/2addr v0, p1

    add-int/lit8 p3, p3, -0x1

    int-to-long p1, p3

    const-wide/32 v2, 0x5265c00

    mul-long p1, p1, v2

    add-long/2addr v0, p1

    return-wide v0
.end method

.method public a(IIII)J
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .line 154
    invoke-virtual {p0}, Lorg/b/a/b/c;->L()Lorg/b/a/a;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 155
    invoke-virtual {v0, p1, p2, p3, p4}, Lorg/b/a/a;->a(IIII)J

    move-result-wide p1

    return-wide p1

    .line 159
    :cond_0
    invoke-static {}, Lorg/b/a/d;->b()Lorg/b/a/d;

    move-result-object v0

    const/4 v1, 0x0

    const v2, 0x5265bff

    invoke-static {v0, p4, v1, v2}, Lorg/b/a/d/h;->a(Lorg/b/a/d;III)V

    .line 160
    invoke-direct {p0, p1, p2, p3, p4}, Lorg/b/a/b/c;->b(IIII)J

    move-result-wide p1

    return-wide p1
.end method

.method public a(IIIIIII)J
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .line 169
    invoke-virtual {p0}, Lorg/b/a/b/c;->L()Lorg/b/a/a;

    move-result-object v0

    if-eqz v0, :cond_0

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    move v6, p6

    move v7, p7

    .line 170
    invoke-virtual/range {v0 .. v7}, Lorg/b/a/a;->a(IIIIIII)J

    move-result-wide p1

    return-wide p1

    .line 174
    :cond_0
    invoke-static {}, Lorg/b/a/d;->g()Lorg/b/a/d;

    move-result-object v0

    const/16 v1, 0x17

    const/4 v2, 0x0

    invoke-static {v0, p4, v2, v1}, Lorg/b/a/d/h;->a(Lorg/b/a/d;III)V

    .line 175
    invoke-static {}, Lorg/b/a/d;->e()Lorg/b/a/d;

    move-result-object v0

    const/16 v1, 0x3b

    invoke-static {v0, p5, v2, v1}, Lorg/b/a/d/h;->a(Lorg/b/a/d;III)V

    .line 176
    invoke-static {}, Lorg/b/a/d;->c()Lorg/b/a/d;

    move-result-object v0

    invoke-static {v0, p6, v2, v1}, Lorg/b/a/d/h;->a(Lorg/b/a/d;III)V

    .line 177
    invoke-static {}, Lorg/b/a/d;->a()Lorg/b/a/d;

    move-result-object v0

    const/16 v1, 0x3e7

    invoke-static {v0, p7, v2, v1}, Lorg/b/a/d/h;->a(Lorg/b/a/d;III)V

    const v0, 0x36ee80

    mul-int p4, p4, v0

    const v0, 0xea60

    mul-int p5, p5, v0

    add-int/2addr p4, p5

    mul-int/lit16 p6, p6, 0x3e8

    add-int/2addr p4, p6

    add-int/2addr p4, p7

    int-to-long p4, p4

    long-to-int p5, p4

    .line 182
    invoke-direct {p0, p1, p2, p3, p5}, Lorg/b/a/b/c;->b(IIII)J

    move-result-wide p1

    return-wide p1
.end method

.method public a()Lorg/b/a/f;
    .locals 1

    .line 143
    invoke-virtual {p0}, Lorg/b/a/b/c;->L()Lorg/b/a/a;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 144
    invoke-virtual {v0}, Lorg/b/a/a;->a()Lorg/b/a/f;

    move-result-object v0

    return-object v0

    .line 146
    :cond_0
    sget-object v0, Lorg/b/a/f;->a:Lorg/b/a/f;

    return-object v0
.end method

.method protected a(Lorg/b/a/b/a$a;)V
    .locals 6

    .line 269
    sget-object v0, Lorg/b/a/b/c;->a:Lorg/b/a/h;

    iput-object v0, p1, Lorg/b/a/b/a$a;->a:Lorg/b/a/h;

    .line 270
    sget-object v0, Lorg/b/a/b/c;->b:Lorg/b/a/h;

    iput-object v0, p1, Lorg/b/a/b/a$a;->b:Lorg/b/a/h;

    .line 271
    sget-object v0, Lorg/b/a/b/c;->c:Lorg/b/a/h;

    iput-object v0, p1, Lorg/b/a/b/a$a;->c:Lorg/b/a/h;

    .line 272
    sget-object v0, Lorg/b/a/b/c;->d:Lorg/b/a/h;

    iput-object v0, p1, Lorg/b/a/b/a$a;->d:Lorg/b/a/h;

    .line 273
    sget-object v0, Lorg/b/a/b/c;->e:Lorg/b/a/h;

    iput-object v0, p1, Lorg/b/a/b/a$a;->e:Lorg/b/a/h;

    .line 274
    sget-object v0, Lorg/b/a/b/c;->f:Lorg/b/a/h;

    iput-object v0, p1, Lorg/b/a/b/a$a;->f:Lorg/b/a/h;

    .line 275
    sget-object v0, Lorg/b/a/b/c;->g:Lorg/b/a/h;

    iput-object v0, p1, Lorg/b/a/b/a$a;->g:Lorg/b/a/h;

    .line 277
    sget-object v0, Lorg/b/a/b/c;->h:Lorg/b/a/c;

    iput-object v0, p1, Lorg/b/a/b/a$a;->m:Lorg/b/a/c;

    .line 278
    sget-object v0, Lorg/b/a/b/c;->i:Lorg/b/a/c;

    iput-object v0, p1, Lorg/b/a/b/a$a;->n:Lorg/b/a/c;

    .line 279
    sget-object v0, Lorg/b/a/b/c;->j:Lorg/b/a/c;

    iput-object v0, p1, Lorg/b/a/b/a$a;->o:Lorg/b/a/c;

    .line 280
    sget-object v0, Lorg/b/a/b/c;->k:Lorg/b/a/c;

    iput-object v0, p1, Lorg/b/a/b/a$a;->p:Lorg/b/a/c;

    .line 281
    sget-object v0, Lorg/b/a/b/c;->l:Lorg/b/a/c;

    iput-object v0, p1, Lorg/b/a/b/a$a;->q:Lorg/b/a/c;

    .line 282
    sget-object v0, Lorg/b/a/b/c;->m:Lorg/b/a/c;

    iput-object v0, p1, Lorg/b/a/b/a$a;->r:Lorg/b/a/c;

    .line 283
    sget-object v0, Lorg/b/a/b/c;->n:Lorg/b/a/c;

    iput-object v0, p1, Lorg/b/a/b/a$a;->s:Lorg/b/a/c;

    .line 284
    sget-object v0, Lorg/b/a/b/c;->o:Lorg/b/a/c;

    iput-object v0, p1, Lorg/b/a/b/a$a;->u:Lorg/b/a/c;

    .line 285
    sget-object v0, Lorg/b/a/b/c;->p:Lorg/b/a/c;

    iput-object v0, p1, Lorg/b/a/b/a$a;->t:Lorg/b/a/c;

    .line 286
    sget-object v0, Lorg/b/a/b/c;->q:Lorg/b/a/c;

    iput-object v0, p1, Lorg/b/a/b/a$a;->v:Lorg/b/a/c;

    .line 287
    sget-object v0, Lorg/b/a/b/c;->r:Lorg/b/a/c;

    iput-object v0, p1, Lorg/b/a/b/a$a;->w:Lorg/b/a/c;

    .line 292
    new-instance v0, Lorg/b/a/b/k;

    invoke-direct {v0, p0}, Lorg/b/a/b/k;-><init>(Lorg/b/a/b/c;)V

    iput-object v0, p1, Lorg/b/a/b/a$a;->E:Lorg/b/a/c;

    .line 293
    new-instance v0, Lorg/b/a/b/s;

    iget-object v1, p1, Lorg/b/a/b/a$a;->E:Lorg/b/a/c;

    invoke-direct {v0, v1, p0}, Lorg/b/a/b/s;-><init>(Lorg/b/a/c;Lorg/b/a/b/c;)V

    iput-object v0, p1, Lorg/b/a/b/a$a;->F:Lorg/b/a/c;

    .line 296
    new-instance v0, Lorg/b/a/d/k;

    iget-object v1, p1, Lorg/b/a/b/a$a;->F:Lorg/b/a/c;

    const/16 v2, 0x63

    invoke-direct {v0, v1, v2}, Lorg/b/a/d/k;-><init>(Lorg/b/a/c;I)V

    .line 298
    new-instance v1, Lorg/b/a/d/g;

    .line 299
    invoke-static {}, Lorg/b/a/d;->v()Lorg/b/a/d;

    move-result-object v2

    const/16 v3, 0x64

    invoke-direct {v1, v0, v2, v3}, Lorg/b/a/d/g;-><init>(Lorg/b/a/c;Lorg/b/a/d;I)V

    iput-object v1, p1, Lorg/b/a/b/a$a;->H:Lorg/b/a/c;

    .line 300
    iget-object v0, p1, Lorg/b/a/b/a$a;->H:Lorg/b/a/c;

    invoke-virtual {v0}, Lorg/b/a/c;->e()Lorg/b/a/h;

    move-result-object v0

    iput-object v0, p1, Lorg/b/a/b/a$a;->k:Lorg/b/a/h;

    .line 302
    new-instance v0, Lorg/b/a/d/o;

    iget-object v1, p1, Lorg/b/a/b/a$a;->H:Lorg/b/a/c;

    check-cast v1, Lorg/b/a/d/g;

    invoke-direct {v0, v1}, Lorg/b/a/d/o;-><init>(Lorg/b/a/d/g;)V

    .line 304
    new-instance v1, Lorg/b/a/d/k;

    .line 305
    invoke-static {}, Lorg/b/a/d;->u()Lorg/b/a/d;

    move-result-object v2

    const/4 v4, 0x1

    invoke-direct {v1, v0, v2, v4}, Lorg/b/a/d/k;-><init>(Lorg/b/a/c;Lorg/b/a/d;I)V

    iput-object v1, p1, Lorg/b/a/b/a$a;->G:Lorg/b/a/c;

    .line 307
    new-instance v0, Lorg/b/a/b/p;

    invoke-direct {v0, p0}, Lorg/b/a/b/p;-><init>(Lorg/b/a/b/c;)V

    iput-object v0, p1, Lorg/b/a/b/a$a;->I:Lorg/b/a/c;

    .line 308
    new-instance v0, Lorg/b/a/b/o;

    iget-object v1, p1, Lorg/b/a/b/a$a;->f:Lorg/b/a/h;

    invoke-direct {v0, p0, v1}, Lorg/b/a/b/o;-><init>(Lorg/b/a/b/c;Lorg/b/a/h;)V

    iput-object v0, p1, Lorg/b/a/b/a$a;->x:Lorg/b/a/c;

    .line 309
    new-instance v0, Lorg/b/a/b/d;

    iget-object v1, p1, Lorg/b/a/b/a$a;->f:Lorg/b/a/h;

    invoke-direct {v0, p0, v1}, Lorg/b/a/b/d;-><init>(Lorg/b/a/b/c;Lorg/b/a/h;)V

    iput-object v0, p1, Lorg/b/a/b/a$a;->y:Lorg/b/a/c;

    .line 310
    new-instance v0, Lorg/b/a/b/e;

    iget-object v1, p1, Lorg/b/a/b/a$a;->f:Lorg/b/a/h;

    invoke-direct {v0, p0, v1}, Lorg/b/a/b/e;-><init>(Lorg/b/a/b/c;Lorg/b/a/h;)V

    iput-object v0, p1, Lorg/b/a/b/a$a;->z:Lorg/b/a/c;

    .line 311
    new-instance v0, Lorg/b/a/b/r;

    invoke-direct {v0, p0}, Lorg/b/a/b/r;-><init>(Lorg/b/a/b/c;)V

    iput-object v0, p1, Lorg/b/a/b/a$a;->D:Lorg/b/a/c;

    .line 312
    new-instance v0, Lorg/b/a/b/j;

    invoke-direct {v0, p0}, Lorg/b/a/b/j;-><init>(Lorg/b/a/b/c;)V

    iput-object v0, p1, Lorg/b/a/b/a$a;->B:Lorg/b/a/c;

    .line 313
    new-instance v0, Lorg/b/a/b/i;

    iget-object v1, p1, Lorg/b/a/b/a$a;->g:Lorg/b/a/h;

    invoke-direct {v0, p0, v1}, Lorg/b/a/b/i;-><init>(Lorg/b/a/b/c;Lorg/b/a/h;)V

    iput-object v0, p1, Lorg/b/a/b/a$a;->A:Lorg/b/a/c;

    .line 315
    new-instance v0, Lorg/b/a/d/o;

    iget-object v1, p1, Lorg/b/a/b/a$a;->B:Lorg/b/a/c;

    iget-object v2, p1, Lorg/b/a/b/a$a;->k:Lorg/b/a/h;

    .line 316
    invoke-static {}, Lorg/b/a/d;->q()Lorg/b/a/d;

    move-result-object v5

    invoke-direct {v0, v1, v2, v5, v3}, Lorg/b/a/d/o;-><init>(Lorg/b/a/c;Lorg/b/a/h;Lorg/b/a/d;I)V

    .line 317
    new-instance v1, Lorg/b/a/d/k;

    .line 318
    invoke-static {}, Lorg/b/a/d;->q()Lorg/b/a/d;

    move-result-object v2

    invoke-direct {v1, v0, v2, v4}, Lorg/b/a/d/k;-><init>(Lorg/b/a/c;Lorg/b/a/d;I)V

    iput-object v1, p1, Lorg/b/a/b/a$a;->C:Lorg/b/a/c;

    .line 322
    iget-object v0, p1, Lorg/b/a/b/a$a;->E:Lorg/b/a/c;

    invoke-virtual {v0}, Lorg/b/a/c;->e()Lorg/b/a/h;

    move-result-object v0

    iput-object v0, p1, Lorg/b/a/b/a$a;->j:Lorg/b/a/h;

    .line 323
    iget-object v0, p1, Lorg/b/a/b/a$a;->D:Lorg/b/a/c;

    invoke-virtual {v0}, Lorg/b/a/c;->e()Lorg/b/a/h;

    move-result-object v0

    iput-object v0, p1, Lorg/b/a/b/a$a;->i:Lorg/b/a/h;

    .line 324
    iget-object v0, p1, Lorg/b/a/b/a$a;->B:Lorg/b/a/c;

    invoke-virtual {v0}, Lorg/b/a/c;->e()Lorg/b/a/h;

    move-result-object v0

    iput-object v0, p1, Lorg/b/a/b/a$a;->h:Lorg/b/a/h;

    return-void
.end method

.method b(I)I
    .locals 4

    .line 354
    invoke-virtual {p0, p1}, Lorg/b/a/b/c;->c(I)J

    move-result-wide v0

    add-int/lit8 p1, p1, 0x1

    .line 355
    invoke-virtual {p0, p1}, Lorg/b/a/b/c;->c(I)J

    move-result-wide v2

    sub-long/2addr v2, v0

    const-wide/32 v0, 0x240c8400

    .line 356
    div-long/2addr v2, v0

    long-to-int p1, v2

    return p1
.end method

.method abstract b(II)I
.end method

.method b(J)I
    .locals 1

    .line 462
    invoke-virtual {p0, p1, p2}, Lorg/b/a/b/c;->a(J)I

    move-result v0

    invoke-virtual {p0, p1, p2, v0}, Lorg/b/a/b/c;->a(JI)I

    move-result p1

    return p1
.end method

.method b(JI)I
    .locals 1

    .line 485
    invoke-virtual {p0, p1, p2, p3}, Lorg/b/a/b/c;->a(JI)I

    move-result v0

    .line 486
    invoke-virtual {p0, p1, p2, p3, v0}, Lorg/b/a/b/c;->a(JII)I

    move-result p1

    return p1
.end method

.method b(III)J
    .locals 4

    .line 630
    invoke-static {}, Lorg/b/a/d;->s()Lorg/b/a/d;

    move-result-object v0

    invoke-virtual {p0}, Lorg/b/a/b/c;->Q()I

    move-result v1

    const/4 v2, 0x1

    sub-int/2addr v1, v2

    invoke-virtual {p0}, Lorg/b/a/b/c;->R()I

    move-result v3

    add-int/2addr v3, v2

    invoke-static {v0, p1, v1, v3}, Lorg/b/a/d/h;->a(Lorg/b/a/d;III)V

    .line 631
    invoke-static {}, Lorg/b/a/d;->r()Lorg/b/a/d;

    move-result-object v0

    invoke-virtual {p0, p1}, Lorg/b/a/b/c;->g(I)I

    move-result v1

    invoke-static {v0, p2, v2, v1}, Lorg/b/a/d/h;->a(Lorg/b/a/d;III)V

    .line 632
    invoke-static {}, Lorg/b/a/d;->m()Lorg/b/a/d;

    move-result-object v0

    invoke-virtual {p0, p1, p2}, Lorg/b/a/b/c;->b(II)I

    move-result v1

    invoke-static {v0, p3, v2, v1}, Lorg/b/a/d/h;->a(Lorg/b/a/d;III)V

    .line 633
    invoke-virtual {p0, p1, p2, p3}, Lorg/b/a/b/c;->a(III)J

    move-result-wide p2

    const-wide/16 v0, 0x0

    cmp-long v3, p2, v0

    if-gez v3, :cond_0

    .line 635
    invoke-virtual {p0}, Lorg/b/a/b/c;->R()I

    move-result v0

    add-int/2addr v0, v2

    if-ne p1, v0, :cond_0

    const-wide p1, 0x7fffffffffffffffL

    return-wide p1

    :cond_0
    if-lez v3, :cond_1

    .line 637
    invoke-virtual {p0}, Lorg/b/a/b/c;->Q()I

    move-result v0

    sub-int/2addr v0, v2

    if-ne p1, v0, :cond_1

    const-wide/high16 p1, -0x8000000000000000L

    return-wide p1

    :cond_1
    return-wide p2
.end method

.method c(J)I
    .locals 2

    .line 475
    invoke-virtual {p0, p1, p2}, Lorg/b/a/b/c;->a(J)I

    move-result v0

    .line 476
    invoke-virtual {p0, p1, p2, v0}, Lorg/b/a/b/c;->a(JI)I

    move-result v1

    .line 477
    invoke-virtual {p0, p1, p2, v0, v1}, Lorg/b/a/b/c;->a(JII)I

    move-result p1

    return p1
.end method

.method c(JI)I
    .locals 2

    .line 512
    invoke-virtual {p0, p3}, Lorg/b/a/b/c;->d(I)J

    move-result-wide v0

    sub-long/2addr p1, v0

    const-wide/32 v0, 0x5265c00

    .line 513
    div-long/2addr p1, v0

    long-to-int p2, p1

    add-int/lit8 p2, p2, 0x1

    return p2
.end method

.method c(I)J
    .locals 7

    .line 366
    invoke-virtual {p0, p1}, Lorg/b/a/b/c;->d(I)J

    move-result-wide v0

    .line 367
    invoke-virtual {p0, v0, v1}, Lorg/b/a/b/c;->g(J)I

    move-result p1

    .line 369
    iget v2, p0, Lorg/b/a/b/c;->t:I

    rsub-int/lit8 v2, v2, 0x8

    const-wide/32 v3, 0x5265c00

    if-le p1, v2, :cond_0

    rsub-int/lit8 p1, p1, 0x8

    int-to-long v5, p1

    mul-long v5, v5, v3

    add-long/2addr v0, v5

    return-wide v0

    :cond_0
    add-int/lit8 p1, p1, -0x1

    int-to-long v5, p1

    mul-long v5, v5, v3

    sub-long/2addr v0, v5

    return-wide v0
.end method

.method abstract c(II)J
.end method

.method d(J)I
    .locals 1

    .line 504
    invoke-virtual {p0, p1, p2}, Lorg/b/a/b/c;->a(J)I

    move-result v0

    invoke-virtual {p0, p1, p2, v0}, Lorg/b/a/b/c;->c(JI)I

    move-result p1

    return p1
.end method

.method d(JI)I
    .locals 5

    .line 543
    invoke-virtual {p0, p3}, Lorg/b/a/b/c;->c(I)J

    move-result-wide v0

    const/4 v2, 0x1

    cmp-long v3, p1, v0

    if-gez v3, :cond_0

    sub-int/2addr p3, v2

    .line 545
    invoke-virtual {p0, p3}, Lorg/b/a/b/c;->b(I)I

    move-result p1

    return p1

    :cond_0
    add-int/2addr p3, v2

    .line 547
    invoke-virtual {p0, p3}, Lorg/b/a/b/c;->c(I)J

    move-result-wide v3

    cmp-long p3, p1, v3

    if-ltz p3, :cond_1

    return v2

    :cond_1
    sub-long/2addr p1, v0

    const-wide/32 v0, 0x240c8400

    .line 551
    div-long/2addr p1, v0

    long-to-int p2, p1

    add-int/2addr p2, v2

    return p2
.end method

.method d(I)J
    .locals 2

    .line 387
    invoke-direct {p0, p1}, Lorg/b/a/b/c;->h(I)Lorg/b/a/b/c$b;

    move-result-object p1

    iget-wide v0, p1, Lorg/b/a/b/c$b;->b:J

    return-wide v0
.end method

.method e(J)I
    .locals 3

    .line 520
    invoke-virtual {p0, p1, p2}, Lorg/b/a/b/c;->a(J)I

    move-result v0

    .line 521
    invoke-virtual {p0, p1, p2, v0}, Lorg/b/a/b/c;->d(JI)I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    const-wide/32 v0, 0x240c8400

    add-long/2addr p1, v0

    .line 523
    invoke-virtual {p0, p1, p2}, Lorg/b/a/b/c;->a(J)I

    move-result p1

    return p1

    :cond_0
    const/16 v2, 0x33

    if-le v1, v2, :cond_1

    const-wide/32 v0, 0x48190800

    sub-long/2addr p1, v0

    .line 525
    invoke-virtual {p0, p1, p2}, Lorg/b/a/b/c;->a(J)I

    move-result p1

    return p1

    :cond_1
    return v0
.end method

.method e(JI)I
    .locals 0

    .line 617
    invoke-virtual {p0, p1, p2}, Lorg/b/a/b/c;->i(J)I

    move-result p1

    return p1
.end method

.method abstract e(I)Z
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p0, p1, :cond_0

    return v0

    :cond_0
    const/4 v1, 0x0

    if-eqz p1, :cond_2

    .line 219
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-ne v2, v3, :cond_2

    .line 220
    check-cast p1, Lorg/b/a/b/c;

    .line 221
    invoke-virtual {p0}, Lorg/b/a/b/c;->N()I

    move-result v2

    invoke-virtual {p1}, Lorg/b/a/b/c;->N()I

    move-result v3

    if-ne v2, v3, :cond_1

    .line 222
    invoke-virtual {p0}, Lorg/b/a/b/c;->a()Lorg/b/a/f;

    move-result-object v2

    invoke-virtual {p1}, Lorg/b/a/b/c;->a()Lorg/b/a/f;

    move-result-object p1

    invoke-virtual {v2, p1}, Lorg/b/a/f;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_2
    return v1
.end method

.method f(J)I
    .locals 1

    .line 535
    invoke-virtual {p0, p1, p2}, Lorg/b/a/b/c;->a(J)I

    move-result v0

    invoke-virtual {p0, p1, p2, v0}, Lorg/b/a/b/c;->d(JI)I

    move-result p1

    return p1
.end method

.method abstract f(I)J
.end method

.method abstract f(JI)J
.end method

.method g(I)I
    .locals 0

    .line 726
    invoke-virtual {p0}, Lorg/b/a/b/c;->S()I

    move-result p1

    return p1
.end method

.method g(J)I
    .locals 7

    const-wide/16 v0, 0x7

    const-wide/32 v2, 0x5265c00

    const-wide/16 v4, 0x0

    cmp-long v6, p1, v4

    if-ltz v6, :cond_0

    .line 562
    div-long/2addr p1, v2

    goto :goto_0

    :cond_0
    const-wide/32 v4, 0x5265bff

    sub-long/2addr p1, v4

    .line 564
    div-long/2addr p1, v2

    const-wide/16 v2, -0x3

    cmp-long v4, p1, v2

    if-gez v4, :cond_1

    const-wide/16 v2, 0x4

    add-long/2addr p1, v2

    .line 567
    rem-long/2addr p1, v0

    long-to-int p2, p1

    add-int/lit8 p2, p2, 0x7

    return p2

    :cond_1
    :goto_0
    const-wide/16 v2, 0x3

    add-long/2addr p1, v2

    .line 571
    rem-long/2addr p1, v0

    long-to-int p2, p1

    add-int/lit8 p2, p2, 0x1

    return p2
.end method

.method h(J)I
    .locals 5

    const-wide/32 v0, 0x5265c00

    const-wide/16 v2, 0x0

    cmp-long v4, p1, v2

    if-ltz v4, :cond_0

    .line 579
    rem-long/2addr p1, v0

    long-to-int p2, p1

    return p2

    :cond_0
    const v2, 0x5265bff

    const-wide/16 v3, 0x1

    add-long/2addr p1, v3

    .line 581
    rem-long/2addr p1, v0

    long-to-int p2, p1

    add-int/2addr p2, v2

    return p2
.end method

.method public hashCode()I
    .locals 2

    .line 234
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0xb

    invoke-virtual {p0}, Lorg/b/a/b/c;->a()Lorg/b/a/f;

    move-result-object v1

    invoke-virtual {v1}, Lorg/b/a/f;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    invoke-virtual {p0}, Lorg/b/a/b/c;->N()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method i(J)I
    .locals 1

    .line 602
    invoke-virtual {p0, p1, p2}, Lorg/b/a/b/c;->a(J)I

    move-result v0

    .line 603
    invoke-virtual {p0, p1, p2, v0}, Lorg/b/a/b/c;->a(JI)I

    move-result p1

    .line 604
    invoke-virtual {p0, v0, p1}, Lorg/b/a/b/c;->b(II)I

    move-result p1

    return p1
.end method

.method j(J)Z
    .locals 0

    const/4 p1, 0x0

    return p1
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .line 245
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x3c

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 246
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x2e

    .line 247
    invoke-virtual {v1, v2}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v2

    if-ltz v2, :cond_0

    add-int/lit8 v2, v2, 0x1

    .line 249
    invoke-virtual {v1, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    .line 251
    :cond_0
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x5b

    .line 252
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 253
    invoke-virtual {p0}, Lorg/b/a/b/c;->a()Lorg/b/a/f;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 255
    invoke-virtual {v1}, Lorg/b/a/f;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 257
    :cond_1
    invoke-virtual {p0}, Lorg/b/a/b/c;->N()I

    move-result v1

    const/4 v2, 0x4

    if-eq v1, v2, :cond_2

    const-string v1, ",mdfw="

    .line 258
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 259
    invoke-virtual {p0}, Lorg/b/a/b/c;->N()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    :cond_2
    const/16 v1, 0x5d

    .line 261
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 262
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
