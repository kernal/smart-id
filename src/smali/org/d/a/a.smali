.class public final Lorg/d/a/a;
.super Ljava/lang/Object;
.source "Levenshtein.java"

# interfaces
.implements Lorg/d/a;


# instance fields
.field private final a:F

.field private final b:F

.field private final c:F


# direct methods
.method public constructor <init>()V
    .locals 1

    const/high16 v0, 0x3f800000    # 1.0f

    .line 71
    invoke-direct {p0, v0, v0}, Lorg/d/a/a;-><init>(FF)V

    return-void
.end method

.method public constructor <init>(FF)V
    .locals 4

    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    const/4 v1, 0x0

    const/4 v2, 0x0

    cmpl-float v3, p1, v2

    if-lez v3, :cond_0

    const/4 v3, 0x1

    goto :goto_0

    :cond_0
    const/4 v3, 0x0

    .line 60
    :goto_0
    invoke-static {v3}, Lcom/google/a/a/a;->a(Z)V

    cmpl-float v2, p2, v2

    if-ltz v2, :cond_1

    goto :goto_1

    :cond_1
    const/4 v0, 0x0

    .line 61
    :goto_1
    invoke-static {v0}, Lcom/google/a/a/a;->a(Z)V

    .line 62
    invoke-static {p1, p2}, Ljava/lang/Math;->max(FF)F

    move-result v0

    iput v0, p0, Lorg/d/a/a;->a:F

    .line 63
    iput p1, p0, Lorg/d/a/a;->b:F

    .line 64
    iput p2, p0, Lorg/d/a/a;->c:F

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;Ljava/lang/String;)F
    .locals 17

    move-object/from16 v0, p0

    .line 86
    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 87
    invoke-virtual/range {p2 .. p2}, Ljava/lang/String;->length()I

    move-result v1

    :goto_0
    int-to-float v1, v1

    return v1

    .line 88
    :cond_0
    invoke-virtual/range {p2 .. p2}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 89
    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->length()I

    move-result v1

    goto :goto_0

    .line 90
    :cond_1
    invoke-virtual/range {p1 .. p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    const/4 v2, 0x0

    if-eqz v1, :cond_2

    return v2

    .line 93
    :cond_2
    invoke-virtual/range {p2 .. p2}, Ljava/lang/String;->length()I

    move-result v1

    .line 94
    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v4, v1, 0x1

    .line 97
    new-array v5, v4, [F

    .line 98
    new-array v4, v4, [F

    const/4 v6, 0x0

    const/4 v7, 0x0

    .line 103
    :goto_1
    array-length v8, v5

    if-ge v7, v8, :cond_3

    int-to-float v8, v7

    .line 104
    iget v9, v0, Lorg/d/a/a;->b:F

    mul-float v8, v8, v9

    aput v8, v5, v7

    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    :cond_3
    move-object v7, v4

    const/4 v4, 0x0

    :goto_2
    if-ge v4, v3, :cond_6

    add-int/lit8 v8, v4, 0x1

    int-to-float v9, v8

    .line 111
    iget v10, v0, Lorg/d/a/a;->b:F

    mul-float v9, v9, v10

    aput v9, v7, v6

    const/4 v9, 0x0

    :goto_3
    if-ge v9, v1, :cond_5

    add-int/lit8 v10, v9, 0x1

    .line 114
    aget v11, v7, v9

    iget v12, v0, Lorg/d/a/a;->b:F

    add-float/2addr v11, v12

    aget v13, v5, v10

    add-float/2addr v13, v12

    aget v12, v5, v9

    move-object/from16 v14, p1

    .line 117
    invoke-virtual {v14, v4}, Ljava/lang/String;->charAt(I)C

    move-result v15

    move-object/from16 v2, p2

    invoke-virtual {v2, v9}, Ljava/lang/String;->charAt(I)C

    move-result v9

    if-ne v15, v9, :cond_4

    const/4 v9, 0x0

    goto :goto_4

    :cond_4
    iget v9, v0, Lorg/d/a/a;->c:F

    :goto_4
    add-float/2addr v12, v9

    .line 114
    invoke-static {v11, v13, v12}, Lorg/d/a/b;->a(FFF)F

    move-result v9

    aput v9, v7, v10

    move v9, v10

    const/4 v2, 0x0

    goto :goto_3

    :cond_5
    move-object/from16 v14, p1

    move-object/from16 v2, p2

    move v4, v8

    const/4 v2, 0x0

    move-object/from16 v16, v7

    move-object v7, v5

    move-object/from16 v5, v16

    goto :goto_2

    .line 127
    :cond_6
    aget v1, v5, v1

    return v1
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 132
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Levenshtein [insertDelete="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lorg/d/a/a;->b:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v1, ", substitute="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lorg/d/a/a;->c:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
