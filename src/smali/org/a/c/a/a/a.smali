.class public Lorg/a/c/a/a/a;
.super Ljava/lang/Object;


# static fields
.field private static a:Ljava/util/Set;

.field private static b:Ljava/util/Set;

.field private static c:Ljava/util/Set;

.field private static d:Ljava/util/Set;

.field private static e:Ljava/util/Set;

.field private static f:Ljava/util/Set;

.field private static g:Ljava/util/Set;

.field private static h:Ljava/util/Set;

.field private static i:Ljava/util/Set;

.field private static j:Ljava/util/Set;

.field private static k:Ljava/util/Set;

.field private static l:Ljava/util/Set;

.field private static m:Ljava/util/Map;


# direct methods
.method static constructor <clinit>()V
    .locals 18

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, Lorg/a/c/a/a/a;->a:Ljava/util/Set;

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, Lorg/a/c/a/a/a;->b:Ljava/util/Set;

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, Lorg/a/c/a/a/a;->c:Ljava/util/Set;

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, Lorg/a/c/a/a/a;->d:Ljava/util/Set;

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, Lorg/a/c/a/a/a;->e:Ljava/util/Set;

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, Lorg/a/c/a/a/a;->f:Ljava/util/Set;

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, Lorg/a/c/a/a/a;->g:Ljava/util/Set;

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, Lorg/a/c/a/a/a;->h:Ljava/util/Set;

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, Lorg/a/c/a/a/a;->i:Ljava/util/Set;

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, Lorg/a/c/a/a/a;->j:Ljava/util/Set;

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, Lorg/a/c/a/a/a;->k:Ljava/util/Set;

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, Lorg/a/c/a/a/a;->l:Ljava/util/Set;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lorg/a/c/a/a/a;->m:Ljava/util/Map;

    sget-object v0, Lorg/a/c/a/a/a;->a:Ljava/util/Set;

    const-string v1, "MD5"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    sget-object v0, Lorg/a/c/a/a/a;->a:Ljava/util/Set;

    sget-object v2, Lorg/a/a/h/a;->J:Lorg/a/a/e;

    invoke-virtual {v2}, Lorg/a/a/e;->c()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    sget-object v0, Lorg/a/c/a/a/a;->b:Ljava/util/Set;

    const-string v2, "SHA1"

    invoke-interface {v0, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    sget-object v0, Lorg/a/c/a/a/a;->b:Ljava/util/Set;

    const-string v3, "SHA-1"

    invoke-interface {v0, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    sget-object v0, Lorg/a/c/a/a/a;->b:Ljava/util/Set;

    sget-object v4, Lorg/a/a/g/a;->i:Lorg/a/a/e;

    invoke-virtual {v4}, Lorg/a/a/e;->c()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v0, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    sget-object v0, Lorg/a/c/a/a/a;->c:Ljava/util/Set;

    const-string v4, "SHA224"

    invoke-interface {v0, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    sget-object v0, Lorg/a/c/a/a/a;->c:Ljava/util/Set;

    const-string v5, "SHA-224"

    invoke-interface {v0, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    sget-object v0, Lorg/a/c/a/a/a;->c:Ljava/util/Set;

    sget-object v6, Lorg/a/a/f/a;->f:Lorg/a/a/e;

    invoke-virtual {v6}, Lorg/a/a/e;->c()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v0, v6}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    sget-object v0, Lorg/a/c/a/a/a;->d:Ljava/util/Set;

    const-string v6, "SHA256"

    invoke-interface {v0, v6}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    sget-object v0, Lorg/a/c/a/a/a;->d:Ljava/util/Set;

    const-string v7, "SHA-256"

    invoke-interface {v0, v7}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    sget-object v0, Lorg/a/c/a/a/a;->d:Ljava/util/Set;

    sget-object v8, Lorg/a/a/f/a;->c:Lorg/a/a/e;

    invoke-virtual {v8}, Lorg/a/a/e;->c()Ljava/lang/String;

    move-result-object v8

    invoke-interface {v0, v8}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    sget-object v0, Lorg/a/c/a/a/a;->e:Ljava/util/Set;

    const-string v8, "SHA384"

    invoke-interface {v0, v8}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    sget-object v0, Lorg/a/c/a/a/a;->e:Ljava/util/Set;

    const-string v9, "SHA-384"

    invoke-interface {v0, v9}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    sget-object v0, Lorg/a/c/a/a/a;->e:Ljava/util/Set;

    sget-object v10, Lorg/a/a/f/a;->d:Lorg/a/a/e;

    invoke-virtual {v10}, Lorg/a/a/e;->c()Ljava/lang/String;

    move-result-object v10

    invoke-interface {v0, v10}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    sget-object v0, Lorg/a/c/a/a/a;->f:Ljava/util/Set;

    const-string v10, "SHA512"

    invoke-interface {v0, v10}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    sget-object v0, Lorg/a/c/a/a/a;->f:Ljava/util/Set;

    const-string v11, "SHA-512"

    invoke-interface {v0, v11}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    sget-object v0, Lorg/a/c/a/a/a;->f:Ljava/util/Set;

    sget-object v12, Lorg/a/a/f/a;->e:Lorg/a/a/e;

    invoke-virtual {v12}, Lorg/a/a/e;->c()Ljava/lang/String;

    move-result-object v12

    invoke-interface {v0, v12}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    sget-object v0, Lorg/a/c/a/a/a;->g:Ljava/util/Set;

    const-string v12, "SHA512(224)"

    invoke-interface {v0, v12}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    sget-object v0, Lorg/a/c/a/a/a;->g:Ljava/util/Set;

    const-string v13, "SHA-512(224)"

    invoke-interface {v0, v13}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    sget-object v0, Lorg/a/c/a/a/a;->g:Ljava/util/Set;

    sget-object v14, Lorg/a/a/f/a;->g:Lorg/a/a/e;

    invoke-virtual {v14}, Lorg/a/a/e;->c()Ljava/lang/String;

    move-result-object v14

    invoke-interface {v0, v14}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    sget-object v0, Lorg/a/c/a/a/a;->h:Ljava/util/Set;

    const-string v14, "SHA512(256)"

    invoke-interface {v0, v14}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    sget-object v0, Lorg/a/c/a/a/a;->h:Ljava/util/Set;

    const-string v15, "SHA-512(256)"

    invoke-interface {v0, v15}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    sget-object v0, Lorg/a/c/a/a/a;->h:Ljava/util/Set;

    sget-object v16, Lorg/a/a/f/a;->h:Lorg/a/a/e;

    move-object/from16 v17, v15

    invoke-virtual/range {v16 .. v16}, Lorg/a/a/e;->c()Ljava/lang/String;

    move-result-object v15

    invoke-interface {v0, v15}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    sget-object v0, Lorg/a/c/a/a/a;->i:Ljava/util/Set;

    const-string v15, "SHA3-224"

    invoke-interface {v0, v15}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    sget-object v0, Lorg/a/c/a/a/a;->i:Ljava/util/Set;

    sget-object v15, Lorg/a/a/f/a;->i:Lorg/a/a/e;

    invoke-virtual {v15}, Lorg/a/a/e;->c()Ljava/lang/String;

    move-result-object v15

    invoke-interface {v0, v15}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    sget-object v0, Lorg/a/c/a/a/a;->j:Ljava/util/Set;

    const-string v15, "SHA3-256"

    invoke-interface {v0, v15}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    sget-object v0, Lorg/a/c/a/a/a;->j:Ljava/util/Set;

    sget-object v15, Lorg/a/a/f/a;->j:Lorg/a/a/e;

    invoke-virtual {v15}, Lorg/a/a/e;->c()Ljava/lang/String;

    move-result-object v15

    invoke-interface {v0, v15}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    sget-object v0, Lorg/a/c/a/a/a;->k:Ljava/util/Set;

    const-string v15, "SHA3-384"

    invoke-interface {v0, v15}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    sget-object v0, Lorg/a/c/a/a/a;->k:Ljava/util/Set;

    sget-object v15, Lorg/a/a/f/a;->k:Lorg/a/a/e;

    invoke-virtual {v15}, Lorg/a/a/e;->c()Ljava/lang/String;

    move-result-object v15

    invoke-interface {v0, v15}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    sget-object v0, Lorg/a/c/a/a/a;->l:Ljava/util/Set;

    const-string v15, "SHA3-512"

    invoke-interface {v0, v15}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    sget-object v0, Lorg/a/c/a/a/a;->l:Ljava/util/Set;

    sget-object v15, Lorg/a/a/f/a;->l:Lorg/a/a/e;

    invoke-virtual {v15}, Lorg/a/a/e;->c()Ljava/lang/String;

    move-result-object v15

    invoke-interface {v0, v15}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    sget-object v0, Lorg/a/c/a/a/a;->m:Ljava/util/Map;

    sget-object v15, Lorg/a/a/h/a;->J:Lorg/a/a/e;

    invoke-interface {v0, v1, v15}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lorg/a/c/a/a/a;->m:Ljava/util/Map;

    sget-object v1, Lorg/a/a/h/a;->J:Lorg/a/a/e;

    invoke-virtual {v1}, Lorg/a/a/e;->c()Ljava/lang/String;

    move-result-object v1

    sget-object v15, Lorg/a/a/h/a;->J:Lorg/a/a/e;

    invoke-interface {v0, v1, v15}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lorg/a/c/a/a/a;->m:Ljava/util/Map;

    sget-object v1, Lorg/a/a/g/a;->i:Lorg/a/a/e;

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lorg/a/c/a/a/a;->m:Ljava/util/Map;

    sget-object v1, Lorg/a/a/g/a;->i:Lorg/a/a/e;

    invoke-interface {v0, v3, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lorg/a/c/a/a/a;->m:Ljava/util/Map;

    sget-object v1, Lorg/a/a/g/a;->i:Lorg/a/a/e;

    invoke-virtual {v1}, Lorg/a/a/e;->c()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lorg/a/a/g/a;->i:Lorg/a/a/e;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lorg/a/c/a/a/a;->m:Ljava/util/Map;

    sget-object v1, Lorg/a/a/f/a;->f:Lorg/a/a/e;

    invoke-interface {v0, v4, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lorg/a/c/a/a/a;->m:Ljava/util/Map;

    sget-object v1, Lorg/a/a/f/a;->f:Lorg/a/a/e;

    invoke-interface {v0, v5, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lorg/a/c/a/a/a;->m:Ljava/util/Map;

    sget-object v1, Lorg/a/a/f/a;->f:Lorg/a/a/e;

    invoke-virtual {v1}, Lorg/a/a/e;->c()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lorg/a/a/f/a;->f:Lorg/a/a/e;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lorg/a/c/a/a/a;->m:Ljava/util/Map;

    sget-object v1, Lorg/a/a/f/a;->c:Lorg/a/a/e;

    invoke-interface {v0, v6, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lorg/a/c/a/a/a;->m:Ljava/util/Map;

    sget-object v1, Lorg/a/a/f/a;->c:Lorg/a/a/e;

    invoke-interface {v0, v7, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lorg/a/c/a/a/a;->m:Ljava/util/Map;

    sget-object v1, Lorg/a/a/f/a;->c:Lorg/a/a/e;

    invoke-virtual {v1}, Lorg/a/a/e;->c()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lorg/a/a/f/a;->c:Lorg/a/a/e;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lorg/a/c/a/a/a;->m:Ljava/util/Map;

    sget-object v1, Lorg/a/a/f/a;->d:Lorg/a/a/e;

    invoke-interface {v0, v8, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lorg/a/c/a/a/a;->m:Ljava/util/Map;

    sget-object v1, Lorg/a/a/f/a;->d:Lorg/a/a/e;

    invoke-interface {v0, v9, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lorg/a/c/a/a/a;->m:Ljava/util/Map;

    sget-object v1, Lorg/a/a/f/a;->d:Lorg/a/a/e;

    invoke-virtual {v1}, Lorg/a/a/e;->c()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lorg/a/a/f/a;->d:Lorg/a/a/e;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lorg/a/c/a/a/a;->m:Ljava/util/Map;

    sget-object v1, Lorg/a/a/f/a;->e:Lorg/a/a/e;

    invoke-interface {v0, v10, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lorg/a/c/a/a/a;->m:Ljava/util/Map;

    sget-object v1, Lorg/a/a/f/a;->e:Lorg/a/a/e;

    invoke-interface {v0, v11, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lorg/a/c/a/a/a;->m:Ljava/util/Map;

    sget-object v1, Lorg/a/a/f/a;->e:Lorg/a/a/e;

    invoke-virtual {v1}, Lorg/a/a/e;->c()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lorg/a/a/f/a;->e:Lorg/a/a/e;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lorg/a/c/a/a/a;->m:Ljava/util/Map;

    sget-object v1, Lorg/a/a/f/a;->g:Lorg/a/a/e;

    invoke-interface {v0, v12, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lorg/a/c/a/a/a;->m:Ljava/util/Map;

    sget-object v1, Lorg/a/a/f/a;->g:Lorg/a/a/e;

    invoke-interface {v0, v13, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lorg/a/c/a/a/a;->m:Ljava/util/Map;

    sget-object v1, Lorg/a/a/f/a;->g:Lorg/a/a/e;

    invoke-virtual {v1}, Lorg/a/a/e;->c()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lorg/a/a/f/a;->g:Lorg/a/a/e;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lorg/a/c/a/a/a;->m:Ljava/util/Map;

    sget-object v1, Lorg/a/a/f/a;->h:Lorg/a/a/e;

    invoke-interface {v0, v14, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lorg/a/c/a/a/a;->m:Ljava/util/Map;

    sget-object v1, Lorg/a/a/f/a;->h:Lorg/a/a/e;

    move-object/from16 v2, v17

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lorg/a/c/a/a/a;->m:Ljava/util/Map;

    sget-object v1, Lorg/a/a/f/a;->h:Lorg/a/a/e;

    invoke-virtual {v1}, Lorg/a/a/e;->c()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lorg/a/a/f/a;->h:Lorg/a/a/e;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lorg/a/c/a/a/a;->m:Ljava/util/Map;

    sget-object v1, Lorg/a/a/f/a;->i:Lorg/a/a/e;

    const-string v2, "SHA3-224"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lorg/a/c/a/a/a;->m:Ljava/util/Map;

    sget-object v1, Lorg/a/a/f/a;->i:Lorg/a/a/e;

    invoke-virtual {v1}, Lorg/a/a/e;->c()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lorg/a/a/f/a;->i:Lorg/a/a/e;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lorg/a/c/a/a/a;->m:Ljava/util/Map;

    sget-object v1, Lorg/a/a/f/a;->j:Lorg/a/a/e;

    const-string v2, "SHA3-256"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lorg/a/c/a/a/a;->m:Ljava/util/Map;

    sget-object v1, Lorg/a/a/f/a;->j:Lorg/a/a/e;

    invoke-virtual {v1}, Lorg/a/a/e;->c()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lorg/a/a/f/a;->j:Lorg/a/a/e;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lorg/a/c/a/a/a;->m:Ljava/util/Map;

    sget-object v1, Lorg/a/a/f/a;->k:Lorg/a/a/e;

    const-string v2, "SHA3-384"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lorg/a/c/a/a/a;->m:Ljava/util/Map;

    sget-object v1, Lorg/a/a/f/a;->k:Lorg/a/a/e;

    invoke-virtual {v1}, Lorg/a/a/e;->c()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lorg/a/a/f/a;->k:Lorg/a/a/e;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lorg/a/c/a/a/a;->m:Ljava/util/Map;

    sget-object v1, Lorg/a/a/f/a;->l:Lorg/a/a/e;

    const-string v2, "SHA3-512"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lorg/a/c/a/a/a;->m:Ljava/util/Map;

    sget-object v1, Lorg/a/a/f/a;->l:Lorg/a/a/e;

    invoke-virtual {v1}, Lorg/a/a/e;->c()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lorg/a/a/f/a;->l:Lorg/a/a/e;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public static a(Ljava/lang/String;)Lorg/a/b/f;
    .locals 1

    invoke-static {p0}, Lorg/a/f/h;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    sget-object v0, Lorg/a/c/a/a/a;->b:Ljava/util/Set;

    invoke-interface {v0, p0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lorg/a/b/h/a;->b()Lorg/a/b/f;

    move-result-object p0

    return-object p0

    :cond_0
    sget-object v0, Lorg/a/c/a/a/a;->a:Ljava/util/Set;

    invoke-interface {v0, p0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {}, Lorg/a/b/h/a;->a()Lorg/a/b/f;

    move-result-object p0

    return-object p0

    :cond_1
    sget-object v0, Lorg/a/c/a/a/a;->c:Ljava/util/Set;

    invoke-interface {v0, p0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-static {}, Lorg/a/b/h/a;->c()Lorg/a/b/f;

    move-result-object p0

    return-object p0

    :cond_2
    sget-object v0, Lorg/a/c/a/a/a;->d:Ljava/util/Set;

    invoke-interface {v0, p0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-static {}, Lorg/a/b/h/a;->d()Lorg/a/b/f;

    move-result-object p0

    return-object p0

    :cond_3
    sget-object v0, Lorg/a/c/a/a/a;->e:Ljava/util/Set;

    invoke-interface {v0, p0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-static {}, Lorg/a/b/h/a;->e()Lorg/a/b/f;

    move-result-object p0

    return-object p0

    :cond_4
    sget-object v0, Lorg/a/c/a/a/a;->f:Ljava/util/Set;

    invoke-interface {v0, p0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-static {}, Lorg/a/b/h/a;->f()Lorg/a/b/f;

    move-result-object p0

    return-object p0

    :cond_5
    sget-object v0, Lorg/a/c/a/a/a;->g:Ljava/util/Set;

    invoke-interface {v0, p0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-static {}, Lorg/a/b/h/a;->g()Lorg/a/b/f;

    move-result-object p0

    return-object p0

    :cond_6
    sget-object v0, Lorg/a/c/a/a/a;->h:Ljava/util/Set;

    invoke-interface {v0, p0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-static {}, Lorg/a/b/h/a;->h()Lorg/a/b/f;

    move-result-object p0

    return-object p0

    :cond_7
    sget-object v0, Lorg/a/c/a/a/a;->i:Ljava/util/Set;

    invoke-interface {v0, p0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-static {}, Lorg/a/b/h/a;->i()Lorg/a/b/f;

    move-result-object p0

    return-object p0

    :cond_8
    sget-object v0, Lorg/a/c/a/a/a;->j:Ljava/util/Set;

    invoke-interface {v0, p0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-static {}, Lorg/a/b/h/a;->j()Lorg/a/b/f;

    move-result-object p0

    return-object p0

    :cond_9
    sget-object v0, Lorg/a/c/a/a/a;->k:Ljava/util/Set;

    invoke-interface {v0, p0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-static {}, Lorg/a/b/h/a;->k()Lorg/a/b/f;

    move-result-object p0

    return-object p0

    :cond_a
    sget-object v0, Lorg/a/c/a/a/a;->l:Ljava/util/Set;

    invoke-interface {v0, p0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_b

    invoke-static {}, Lorg/a/b/h/a;->l()Lorg/a/b/f;

    move-result-object p0

    return-object p0

    :cond_b
    const/4 p0, 0x0

    return-object p0
.end method
