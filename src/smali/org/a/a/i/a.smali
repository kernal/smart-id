.class public interface abstract Lorg/a/a/i/a;
.super Ljava/lang/Object;


# static fields
.field public static final a:Lorg/a/a/e;

.field public static final b:Lorg/a/a/e;

.field public static final c:Lorg/a/a/e;

.field public static final d:Lorg/a/a/e;

.field public static final e:Lorg/a/a/e;

.field public static final f:Lorg/a/a/e;

.field public static final g:Lorg/a/a/e;

.field public static final h:Lorg/a/a/e;

.field public static final i:Lorg/a/a/e;

.field public static final j:Lorg/a/a/e;

.field public static final k:Lorg/a/a/e;

.field public static final l:Lorg/a/a/e;

.field public static final m:Lorg/a/a/e;

.field public static final n:Lorg/a/a/e;

.field public static final o:Lorg/a/a/e;

.field public static final p:Lorg/a/a/e;

.field public static final q:Lorg/a/a/e;

.field public static final r:Lorg/a/a/e;

.field public static final s:Lorg/a/a/e;

.field public static final t:Lorg/a/a/e;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    new-instance v0, Lorg/a/a/e;

    const-string v1, "1.2.643.7"

    invoke-direct {v0, v1}, Lorg/a/a/e;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/a/a/i/a;->a:Lorg/a/a/e;

    sget-object v0, Lorg/a/a/i/a;->a:Lorg/a/a/e;

    const-string v1, "1"

    invoke-virtual {v0, v1}, Lorg/a/a/e;->a(Ljava/lang/String;)Lorg/a/a/e;

    move-result-object v0

    sput-object v0, Lorg/a/a/i/a;->b:Lorg/a/a/e;

    sget-object v0, Lorg/a/a/i/a;->b:Lorg/a/a/e;

    const-string v2, "1.2.2"

    invoke-virtual {v0, v2}, Lorg/a/a/e;->a(Ljava/lang/String;)Lorg/a/a/e;

    move-result-object v0

    sput-object v0, Lorg/a/a/i/a;->c:Lorg/a/a/e;

    sget-object v0, Lorg/a/a/i/a;->b:Lorg/a/a/e;

    const-string v2, "1.2.3"

    invoke-virtual {v0, v2}, Lorg/a/a/e;->a(Ljava/lang/String;)Lorg/a/a/e;

    move-result-object v0

    sput-object v0, Lorg/a/a/i/a;->d:Lorg/a/a/e;

    sget-object v0, Lorg/a/a/i/a;->b:Lorg/a/a/e;

    const-string v2, "1.4.1"

    invoke-virtual {v0, v2}, Lorg/a/a/e;->a(Ljava/lang/String;)Lorg/a/a/e;

    move-result-object v0

    sput-object v0, Lorg/a/a/i/a;->e:Lorg/a/a/e;

    sget-object v0, Lorg/a/a/i/a;->b:Lorg/a/a/e;

    const-string v2, "1.4.2"

    invoke-virtual {v0, v2}, Lorg/a/a/e;->a(Ljava/lang/String;)Lorg/a/a/e;

    move-result-object v0

    sput-object v0, Lorg/a/a/i/a;->f:Lorg/a/a/e;

    sget-object v0, Lorg/a/a/i/a;->b:Lorg/a/a/e;

    const-string v2, "1.1.1"

    invoke-virtual {v0, v2}, Lorg/a/a/e;->a(Ljava/lang/String;)Lorg/a/a/e;

    move-result-object v0

    sput-object v0, Lorg/a/a/i/a;->g:Lorg/a/a/e;

    sget-object v0, Lorg/a/a/i/a;->b:Lorg/a/a/e;

    const-string v2, "1.1.2"

    invoke-virtual {v0, v2}, Lorg/a/a/e;->a(Ljava/lang/String;)Lorg/a/a/e;

    move-result-object v0

    sput-object v0, Lorg/a/a/i/a;->h:Lorg/a/a/e;

    sget-object v0, Lorg/a/a/i/a;->b:Lorg/a/a/e;

    const-string v2, "1.3.2"

    invoke-virtual {v0, v2}, Lorg/a/a/e;->a(Ljava/lang/String;)Lorg/a/a/e;

    move-result-object v0

    sput-object v0, Lorg/a/a/i/a;->i:Lorg/a/a/e;

    sget-object v0, Lorg/a/a/i/a;->b:Lorg/a/a/e;

    const-string v2, "1.3.3"

    invoke-virtual {v0, v2}, Lorg/a/a/e;->a(Ljava/lang/String;)Lorg/a/a/e;

    move-result-object v0

    sput-object v0, Lorg/a/a/i/a;->j:Lorg/a/a/e;

    sget-object v0, Lorg/a/a/i/a;->b:Lorg/a/a/e;

    const-string v2, "1.6"

    invoke-virtual {v0, v2}, Lorg/a/a/e;->a(Ljava/lang/String;)Lorg/a/a/e;

    move-result-object v0

    sput-object v0, Lorg/a/a/i/a;->k:Lorg/a/a/e;

    sget-object v0, Lorg/a/a/i/a;->k:Lorg/a/a/e;

    invoke-virtual {v0, v1}, Lorg/a/a/e;->a(Ljava/lang/String;)Lorg/a/a/e;

    move-result-object v0

    sput-object v0, Lorg/a/a/i/a;->l:Lorg/a/a/e;

    sget-object v0, Lorg/a/a/i/a;->k:Lorg/a/a/e;

    const-string v2, "2"

    invoke-virtual {v0, v2}, Lorg/a/a/e;->a(Ljava/lang/String;)Lorg/a/a/e;

    move-result-object v0

    sput-object v0, Lorg/a/a/i/a;->m:Lorg/a/a/e;

    sget-object v0, Lorg/a/a/i/a;->b:Lorg/a/a/e;

    const-string v3, "2.1.1"

    invoke-virtual {v0, v3}, Lorg/a/a/e;->a(Ljava/lang/String;)Lorg/a/a/e;

    move-result-object v0

    sput-object v0, Lorg/a/a/i/a;->n:Lorg/a/a/e;

    sget-object v0, Lorg/a/a/i/a;->n:Lorg/a/a/e;

    invoke-virtual {v0, v1}, Lorg/a/a/e;->a(Ljava/lang/String;)Lorg/a/a/e;

    move-result-object v0

    sput-object v0, Lorg/a/a/i/a;->o:Lorg/a/a/e;

    sget-object v0, Lorg/a/a/i/a;->b:Lorg/a/a/e;

    const-string v3, "2.1.2"

    invoke-virtual {v0, v3}, Lorg/a/a/e;->a(Ljava/lang/String;)Lorg/a/a/e;

    move-result-object v0

    sput-object v0, Lorg/a/a/i/a;->p:Lorg/a/a/e;

    sget-object v0, Lorg/a/a/i/a;->p:Lorg/a/a/e;

    invoke-virtual {v0, v1}, Lorg/a/a/e;->a(Ljava/lang/String;)Lorg/a/a/e;

    move-result-object v0

    sput-object v0, Lorg/a/a/i/a;->q:Lorg/a/a/e;

    sget-object v0, Lorg/a/a/i/a;->p:Lorg/a/a/e;

    invoke-virtual {v0, v2}, Lorg/a/a/e;->a(Ljava/lang/String;)Lorg/a/a/e;

    move-result-object v0

    sput-object v0, Lorg/a/a/i/a;->r:Lorg/a/a/e;

    sget-object v0, Lorg/a/a/i/a;->p:Lorg/a/a/e;

    const-string v1, "3"

    invoke-virtual {v0, v1}, Lorg/a/a/e;->a(Ljava/lang/String;)Lorg/a/a/e;

    move-result-object v0

    sput-object v0, Lorg/a/a/i/a;->s:Lorg/a/a/e;

    sget-object v0, Lorg/a/a/i/a;->b:Lorg/a/a/e;

    const-string v1, "2.5.1.1"

    invoke-virtual {v0, v1}, Lorg/a/a/e;->a(Ljava/lang/String;)Lorg/a/a/e;

    move-result-object v0

    sput-object v0, Lorg/a/a/i/a;->t:Lorg/a/a/e;

    return-void
.end method
