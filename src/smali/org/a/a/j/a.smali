.class public interface abstract Lorg/a/a/j/a;
.super Ljava/lang/Object;


# static fields
.field public static final A:Lorg/a/a/e;

.field public static final B:Lorg/a/a/e;

.field public static final a:Lorg/a/a/e;

.field public static final b:Lorg/a/a/e;

.field public static final c:Lorg/a/a/e;

.field public static final d:Lorg/a/a/e;

.field public static final e:Lorg/a/a/e;

.field public static final f:Lorg/a/a/e;

.field public static final g:Lorg/a/a/e;

.field public static final h:Lorg/a/a/e;

.field public static final i:Lorg/a/a/e;

.field public static final j:Lorg/a/a/e;

.field public static final k:Lorg/a/a/e;

.field public static final l:Lorg/a/a/e;

.field public static final m:Lorg/a/a/e;

.field public static final n:Lorg/a/a/e;

.field public static final o:Lorg/a/a/e;

.field public static final p:Lorg/a/a/e;

.field public static final q:Lorg/a/a/e;

.field public static final r:Lorg/a/a/e;

.field public static final s:Lorg/a/a/e;

.field public static final t:Lorg/a/a/e;

.field public static final u:Lorg/a/a/e;

.field public static final v:Lorg/a/a/e;

.field public static final w:Lorg/a/a/e;

.field public static final x:Lorg/a/a/e;

.field public static final y:Lorg/a/a/e;

.field public static final z:Lorg/a/a/e;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    new-instance v0, Lorg/a/a/e;

    const-string v1, "1.3.36.3"

    invoke-direct {v0, v1}, Lorg/a/a/e;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/a/a/j/a;->a:Lorg/a/a/e;

    sget-object v0, Lorg/a/a/j/a;->a:Lorg/a/a/e;

    const-string v1, "2.1"

    invoke-virtual {v0, v1}, Lorg/a/a/e;->a(Ljava/lang/String;)Lorg/a/a/e;

    move-result-object v0

    sput-object v0, Lorg/a/a/j/a;->b:Lorg/a/a/e;

    sget-object v0, Lorg/a/a/j/a;->a:Lorg/a/a/e;

    const-string v1, "2.2"

    invoke-virtual {v0, v1}, Lorg/a/a/e;->a(Ljava/lang/String;)Lorg/a/a/e;

    move-result-object v0

    sput-object v0, Lorg/a/a/j/a;->c:Lorg/a/a/e;

    sget-object v0, Lorg/a/a/j/a;->a:Lorg/a/a/e;

    const-string v1, "2.3"

    invoke-virtual {v0, v1}, Lorg/a/a/e;->a(Ljava/lang/String;)Lorg/a/a/e;

    move-result-object v0

    sput-object v0, Lorg/a/a/j/a;->d:Lorg/a/a/e;

    sget-object v0, Lorg/a/a/j/a;->a:Lorg/a/a/e;

    const-string v1, "3.1"

    invoke-virtual {v0, v1}, Lorg/a/a/e;->a(Ljava/lang/String;)Lorg/a/a/e;

    move-result-object v0

    sput-object v0, Lorg/a/a/j/a;->e:Lorg/a/a/e;

    sget-object v0, Lorg/a/a/j/a;->e:Lorg/a/a/e;

    const-string v1, "2"

    invoke-virtual {v0, v1}, Lorg/a/a/e;->a(Ljava/lang/String;)Lorg/a/a/e;

    move-result-object v0

    sput-object v0, Lorg/a/a/j/a;->f:Lorg/a/a/e;

    sget-object v0, Lorg/a/a/j/a;->e:Lorg/a/a/e;

    const-string v2, "3"

    invoke-virtual {v0, v2}, Lorg/a/a/e;->a(Ljava/lang/String;)Lorg/a/a/e;

    move-result-object v0

    sput-object v0, Lorg/a/a/j/a;->g:Lorg/a/a/e;

    sget-object v0, Lorg/a/a/j/a;->e:Lorg/a/a/e;

    const-string v3, "4"

    invoke-virtual {v0, v3}, Lorg/a/a/e;->a(Ljava/lang/String;)Lorg/a/a/e;

    move-result-object v0

    sput-object v0, Lorg/a/a/j/a;->h:Lorg/a/a/e;

    sget-object v0, Lorg/a/a/j/a;->a:Lorg/a/a/e;

    const-string v4, "3.2"

    invoke-virtual {v0, v4}, Lorg/a/a/e;->a(Ljava/lang/String;)Lorg/a/a/e;

    move-result-object v0

    sput-object v0, Lorg/a/a/j/a;->i:Lorg/a/a/e;

    sget-object v0, Lorg/a/a/j/a;->i:Lorg/a/a/e;

    const-string v4, "1"

    invoke-virtual {v0, v4}, Lorg/a/a/e;->a(Ljava/lang/String;)Lorg/a/a/e;

    move-result-object v0

    sput-object v0, Lorg/a/a/j/a;->j:Lorg/a/a/e;

    sget-object v0, Lorg/a/a/j/a;->i:Lorg/a/a/e;

    invoke-virtual {v0, v1}, Lorg/a/a/e;->a(Ljava/lang/String;)Lorg/a/a/e;

    move-result-object v0

    sput-object v0, Lorg/a/a/j/a;->k:Lorg/a/a/e;

    sget-object v0, Lorg/a/a/j/a;->a:Lorg/a/a/e;

    const-string v5, "3.2.8"

    invoke-virtual {v0, v5}, Lorg/a/a/e;->a(Ljava/lang/String;)Lorg/a/a/e;

    move-result-object v0

    sput-object v0, Lorg/a/a/j/a;->l:Lorg/a/a/e;

    sget-object v0, Lorg/a/a/j/a;->l:Lorg/a/a/e;

    invoke-virtual {v0, v4}, Lorg/a/a/e;->a(Ljava/lang/String;)Lorg/a/a/e;

    move-result-object v0

    sput-object v0, Lorg/a/a/j/a;->m:Lorg/a/a/e;

    sget-object v0, Lorg/a/a/j/a;->m:Lorg/a/a/e;

    invoke-virtual {v0, v4}, Lorg/a/a/e;->a(Ljava/lang/String;)Lorg/a/a/e;

    move-result-object v0

    sput-object v0, Lorg/a/a/j/a;->n:Lorg/a/a/e;

    sget-object v0, Lorg/a/a/j/a;->n:Lorg/a/a/e;

    invoke-virtual {v0, v4}, Lorg/a/a/e;->a(Ljava/lang/String;)Lorg/a/a/e;

    move-result-object v0

    sput-object v0, Lorg/a/a/j/a;->o:Lorg/a/a/e;

    sget-object v0, Lorg/a/a/j/a;->n:Lorg/a/a/e;

    invoke-virtual {v0, v1}, Lorg/a/a/e;->a(Ljava/lang/String;)Lorg/a/a/e;

    move-result-object v0

    sput-object v0, Lorg/a/a/j/a;->p:Lorg/a/a/e;

    sget-object v0, Lorg/a/a/j/a;->n:Lorg/a/a/e;

    invoke-virtual {v0, v2}, Lorg/a/a/e;->a(Ljava/lang/String;)Lorg/a/a/e;

    move-result-object v0

    sput-object v0, Lorg/a/a/j/a;->q:Lorg/a/a/e;

    sget-object v0, Lorg/a/a/j/a;->n:Lorg/a/a/e;

    invoke-virtual {v0, v3}, Lorg/a/a/e;->a(Ljava/lang/String;)Lorg/a/a/e;

    move-result-object v0

    sput-object v0, Lorg/a/a/j/a;->r:Lorg/a/a/e;

    sget-object v0, Lorg/a/a/j/a;->n:Lorg/a/a/e;

    const-string v1, "5"

    invoke-virtual {v0, v1}, Lorg/a/a/e;->a(Ljava/lang/String;)Lorg/a/a/e;

    move-result-object v0

    sput-object v0, Lorg/a/a/j/a;->s:Lorg/a/a/e;

    sget-object v0, Lorg/a/a/j/a;->n:Lorg/a/a/e;

    const-string v1, "6"

    invoke-virtual {v0, v1}, Lorg/a/a/e;->a(Ljava/lang/String;)Lorg/a/a/e;

    move-result-object v0

    sput-object v0, Lorg/a/a/j/a;->t:Lorg/a/a/e;

    sget-object v0, Lorg/a/a/j/a;->n:Lorg/a/a/e;

    const-string v1, "7"

    invoke-virtual {v0, v1}, Lorg/a/a/e;->a(Ljava/lang/String;)Lorg/a/a/e;

    move-result-object v0

    sput-object v0, Lorg/a/a/j/a;->u:Lorg/a/a/e;

    sget-object v0, Lorg/a/a/j/a;->n:Lorg/a/a/e;

    const-string v1, "8"

    invoke-virtual {v0, v1}, Lorg/a/a/e;->a(Ljava/lang/String;)Lorg/a/a/e;

    move-result-object v0

    sput-object v0, Lorg/a/a/j/a;->v:Lorg/a/a/e;

    sget-object v0, Lorg/a/a/j/a;->n:Lorg/a/a/e;

    const-string v1, "9"

    invoke-virtual {v0, v1}, Lorg/a/a/e;->a(Ljava/lang/String;)Lorg/a/a/e;

    move-result-object v0

    sput-object v0, Lorg/a/a/j/a;->w:Lorg/a/a/e;

    sget-object v0, Lorg/a/a/j/a;->n:Lorg/a/a/e;

    const-string v1, "10"

    invoke-virtual {v0, v1}, Lorg/a/a/e;->a(Ljava/lang/String;)Lorg/a/a/e;

    move-result-object v0

    sput-object v0, Lorg/a/a/j/a;->x:Lorg/a/a/e;

    sget-object v0, Lorg/a/a/j/a;->n:Lorg/a/a/e;

    const-string v1, "11"

    invoke-virtual {v0, v1}, Lorg/a/a/e;->a(Ljava/lang/String;)Lorg/a/a/e;

    move-result-object v0

    sput-object v0, Lorg/a/a/j/a;->y:Lorg/a/a/e;

    sget-object v0, Lorg/a/a/j/a;->n:Lorg/a/a/e;

    const-string v1, "12"

    invoke-virtual {v0, v1}, Lorg/a/a/e;->a(Ljava/lang/String;)Lorg/a/a/e;

    move-result-object v0

    sput-object v0, Lorg/a/a/j/a;->z:Lorg/a/a/e;

    sget-object v0, Lorg/a/a/j/a;->n:Lorg/a/a/e;

    const-string v1, "13"

    invoke-virtual {v0, v1}, Lorg/a/a/e;->a(Ljava/lang/String;)Lorg/a/a/e;

    move-result-object v0

    sput-object v0, Lorg/a/a/j/a;->A:Lorg/a/a/e;

    sget-object v0, Lorg/a/a/j/a;->n:Lorg/a/a/e;

    const-string v1, "14"

    invoke-virtual {v0, v1}, Lorg/a/a/e;->a(Ljava/lang/String;)Lorg/a/a/e;

    move-result-object v0

    sput-object v0, Lorg/a/a/j/a;->B:Lorg/a/a/e;

    return-void
.end method
