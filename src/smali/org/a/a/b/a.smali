.class public interface abstract Lorg/a/a/b/a;
.super Ljava/lang/Object;


# static fields
.field public static final a:Lorg/a/a/e;

.field public static final b:Lorg/a/a/e;

.field public static final c:Lorg/a/a/e;

.field public static final d:Lorg/a/a/e;

.field public static final e:Lorg/a/a/e;

.field public static final f:Lorg/a/a/e;

.field public static final g:Lorg/a/a/e;

.field public static final h:Lorg/a/a/e;

.field public static final i:Lorg/a/a/e;

.field public static final j:Lorg/a/a/e;

.field public static final k:Lorg/a/a/e;

.field public static final l:Lorg/a/a/e;

.field public static final m:Lorg/a/a/e;

.field public static final n:Lorg/a/a/e;

.field public static final o:Lorg/a/a/e;

.field public static final p:Lorg/a/a/e;

.field public static final q:Lorg/a/a/e;

.field public static final r:Lorg/a/a/e;

.field public static final s:Lorg/a/a/e;

.field public static final t:Lorg/a/a/e;

.field public static final u:Lorg/a/a/e;

.field public static final v:Lorg/a/a/e;

.field public static final w:Lorg/a/a/e;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    new-instance v0, Lorg/a/a/e;

    const-string v1, "0.4.0.127.0.7"

    invoke-direct {v0, v1}, Lorg/a/a/e;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/a/a/b/a;->a:Lorg/a/a/e;

    sget-object v0, Lorg/a/a/b/a;->a:Lorg/a/a/e;

    const-string v1, "1.1"

    invoke-virtual {v0, v1}, Lorg/a/a/e;->a(Ljava/lang/String;)Lorg/a/a/e;

    move-result-object v0

    sput-object v0, Lorg/a/a/b/a;->b:Lorg/a/a/e;

    sget-object v0, Lorg/a/a/b/a;->b:Lorg/a/a/e;

    const-string v1, "4.1"

    invoke-virtual {v0, v1}, Lorg/a/a/e;->a(Ljava/lang/String;)Lorg/a/a/e;

    move-result-object v0

    sput-object v0, Lorg/a/a/b/a;->c:Lorg/a/a/e;

    sget-object v0, Lorg/a/a/b/a;->c:Lorg/a/a/e;

    const-string v1, "1"

    invoke-virtual {v0, v1}, Lorg/a/a/e;->a(Ljava/lang/String;)Lorg/a/a/e;

    move-result-object v0

    sput-object v0, Lorg/a/a/b/a;->d:Lorg/a/a/e;

    sget-object v0, Lorg/a/a/b/a;->c:Lorg/a/a/e;

    const-string v2, "2"

    invoke-virtual {v0, v2}, Lorg/a/a/e;->a(Ljava/lang/String;)Lorg/a/a/e;

    move-result-object v0

    sput-object v0, Lorg/a/a/b/a;->e:Lorg/a/a/e;

    sget-object v0, Lorg/a/a/b/a;->c:Lorg/a/a/e;

    const-string v3, "3"

    invoke-virtual {v0, v3}, Lorg/a/a/e;->a(Ljava/lang/String;)Lorg/a/a/e;

    move-result-object v0

    sput-object v0, Lorg/a/a/b/a;->f:Lorg/a/a/e;

    sget-object v0, Lorg/a/a/b/a;->c:Lorg/a/a/e;

    const-string v4, "4"

    invoke-virtual {v0, v4}, Lorg/a/a/e;->a(Ljava/lang/String;)Lorg/a/a/e;

    move-result-object v0

    sput-object v0, Lorg/a/a/b/a;->g:Lorg/a/a/e;

    sget-object v0, Lorg/a/a/b/a;->c:Lorg/a/a/e;

    const-string v5, "5"

    invoke-virtual {v0, v5}, Lorg/a/a/e;->a(Ljava/lang/String;)Lorg/a/a/e;

    move-result-object v0

    sput-object v0, Lorg/a/a/b/a;->h:Lorg/a/a/e;

    sget-object v0, Lorg/a/a/b/a;->c:Lorg/a/a/e;

    const-string v6, "6"

    invoke-virtual {v0, v6}, Lorg/a/a/e;->a(Ljava/lang/String;)Lorg/a/a/e;

    move-result-object v0

    sput-object v0, Lorg/a/a/b/a;->i:Lorg/a/a/e;

    sget-object v0, Lorg/a/a/b/a;->a:Lorg/a/a/e;

    invoke-virtual {v0, v1}, Lorg/a/a/e;->a(Ljava/lang/String;)Lorg/a/a/e;

    move-result-object v0

    sput-object v0, Lorg/a/a/b/a;->j:Lorg/a/a/e;

    sget-object v0, Lorg/a/a/b/a;->b:Lorg/a/a/e;

    const-string v7, "5.1"

    invoke-virtual {v0, v7}, Lorg/a/a/e;->a(Ljava/lang/String;)Lorg/a/a/e;

    move-result-object v0

    sput-object v0, Lorg/a/a/b/a;->k:Lorg/a/a/e;

    sget-object v0, Lorg/a/a/b/a;->k:Lorg/a/a/e;

    invoke-virtual {v0, v1}, Lorg/a/a/e;->a(Ljava/lang/String;)Lorg/a/a/e;

    move-result-object v0

    sput-object v0, Lorg/a/a/b/a;->l:Lorg/a/a/e;

    sget-object v0, Lorg/a/a/b/a;->l:Lorg/a/a/e;

    invoke-virtual {v0, v1}, Lorg/a/a/e;->a(Ljava/lang/String;)Lorg/a/a/e;

    move-result-object v0

    sput-object v0, Lorg/a/a/b/a;->m:Lorg/a/a/e;

    sget-object v0, Lorg/a/a/b/a;->l:Lorg/a/a/e;

    invoke-virtual {v0, v2}, Lorg/a/a/e;->a(Ljava/lang/String;)Lorg/a/a/e;

    move-result-object v0

    sput-object v0, Lorg/a/a/b/a;->n:Lorg/a/a/e;

    sget-object v0, Lorg/a/a/b/a;->l:Lorg/a/a/e;

    invoke-virtual {v0, v3}, Lorg/a/a/e;->a(Ljava/lang/String;)Lorg/a/a/e;

    move-result-object v0

    sput-object v0, Lorg/a/a/b/a;->o:Lorg/a/a/e;

    sget-object v0, Lorg/a/a/b/a;->l:Lorg/a/a/e;

    invoke-virtual {v0, v4}, Lorg/a/a/e;->a(Ljava/lang/String;)Lorg/a/a/e;

    move-result-object v0

    sput-object v0, Lorg/a/a/b/a;->p:Lorg/a/a/e;

    sget-object v0, Lorg/a/a/b/a;->l:Lorg/a/a/e;

    invoke-virtual {v0, v5}, Lorg/a/a/e;->a(Ljava/lang/String;)Lorg/a/a/e;

    move-result-object v0

    sput-object v0, Lorg/a/a/b/a;->q:Lorg/a/a/e;

    sget-object v0, Lorg/a/a/b/a;->l:Lorg/a/a/e;

    invoke-virtual {v0, v6}, Lorg/a/a/e;->a(Ljava/lang/String;)Lorg/a/a/e;

    move-result-object v0

    sput-object v0, Lorg/a/a/b/a;->r:Lorg/a/a/e;

    sget-object v0, Lorg/a/a/b/a;->k:Lorg/a/a/e;

    invoke-virtual {v0, v2}, Lorg/a/a/e;->a(Ljava/lang/String;)Lorg/a/a/e;

    move-result-object v0

    sput-object v0, Lorg/a/a/b/a;->s:Lorg/a/a/e;

    sget-object v0, Lorg/a/a/b/a;->s:Lorg/a/a/e;

    invoke-virtual {v0, v1}, Lorg/a/a/e;->a(Ljava/lang/String;)Lorg/a/a/e;

    move-result-object v0

    sput-object v0, Lorg/a/a/b/a;->t:Lorg/a/a/e;

    sget-object v0, Lorg/a/a/b/a;->s:Lorg/a/a/e;

    invoke-virtual {v0, v2}, Lorg/a/a/e;->a(Ljava/lang/String;)Lorg/a/a/e;

    move-result-object v0

    sput-object v0, Lorg/a/a/b/a;->u:Lorg/a/a/e;

    sget-object v0, Lorg/a/a/b/a;->s:Lorg/a/a/e;

    invoke-virtual {v0, v3}, Lorg/a/a/e;->a(Ljava/lang/String;)Lorg/a/a/e;

    move-result-object v0

    sput-object v0, Lorg/a/a/b/a;->v:Lorg/a/a/e;

    sget-object v0, Lorg/a/a/b/a;->s:Lorg/a/a/e;

    invoke-virtual {v0, v4}, Lorg/a/a/e;->a(Ljava/lang/String;)Lorg/a/a/e;

    move-result-object v0

    sput-object v0, Lorg/a/a/b/a;->w:Lorg/a/a/e;

    return-void
.end method
