.class public abstract Lorg/a/a/d;
.super Ljava/lang/Object;

# interfaces
.implements Lorg/a/a/a;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract a()Lorg/a/a/i;
.end method

.method public b()[B
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    new-instance v1, Lorg/a/a/h;

    invoke-direct {v1, v0}, Lorg/a/a/h;-><init>(Ljava/io/OutputStream;)V

    invoke-virtual {v1, p0}, Lorg/a/a/h;->a(Lorg/a/a/a;)V

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1

    if-ne p0, p1, :cond_0

    const/4 p1, 0x1

    return p1

    :cond_0
    instance-of v0, p1, Lorg/a/a/a;

    if-nez v0, :cond_1

    const/4 p1, 0x0

    return p1

    :cond_1
    check-cast p1, Lorg/a/a/a;

    invoke-virtual {p0}, Lorg/a/a/d;->a()Lorg/a/a/i;

    move-result-object v0

    invoke-interface {p1}, Lorg/a/a/a;->a()Lorg/a/a/i;

    move-result-object p1

    invoke-virtual {v0, p1}, Lorg/a/a/i;->equals(Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method public hashCode()I
    .locals 1

    invoke-virtual {p0}, Lorg/a/a/d;->a()Lorg/a/a/i;

    move-result-object v0

    invoke-virtual {v0}, Lorg/a/a/i;->hashCode()I

    move-result v0

    return v0
.end method
