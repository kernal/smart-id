.class public abstract Lorg/a/a/j;
.super Lorg/a/a/i;

# interfaces
.implements Lorg/a/f/d;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lorg/a/a/i;",
        "Lorg/a/f/d<",
        "Lorg/a/a/a;",
        ">;"
    }
.end annotation


# instance fields
.field protected a:Ljava/util/Vector;


# direct methods
.method protected constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lorg/a/a/i;-><init>()V

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lorg/a/a/j;->a:Ljava/util/Vector;

    return-void
.end method

.method protected constructor <init>(Lorg/a/a/b;)V
    .locals 3

    invoke-direct {p0}, Lorg/a/a/i;-><init>()V

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lorg/a/a/j;->a:Ljava/util/Vector;

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1}, Lorg/a/a/b;->a()I

    move-result v1

    if-eq v0, v1, :cond_0

    iget-object v1, p0, Lorg/a/a/j;->a:Ljava/util/Vector;

    invoke-virtual {p1, v0}, Lorg/a/a/b;->a(I)Lorg/a/a/a;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method private a(Ljava/util/Enumeration;)Lorg/a/a/a;
    .locals 0

    invoke-interface {p1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lorg/a/a/a;

    return-object p1
.end method


# virtual methods
.method public a(I)Lorg/a/a/a;
    .locals 1

    iget-object v0, p0, Lorg/a/a/j;->a:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lorg/a/a/a;

    return-object p1
.end method

.method abstract a(Lorg/a/a/h;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method a(Lorg/a/a/i;)Z
    .locals 4

    instance-of v0, p1, Lorg/a/a/j;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    :cond_0
    check-cast p1, Lorg/a/a/j;

    invoke-virtual {p0}, Lorg/a/a/j;->g()I

    move-result v0

    invoke-virtual {p1}, Lorg/a/a/j;->g()I

    move-result v2

    if-eq v0, v2, :cond_1

    return v1

    :cond_1
    invoke-virtual {p0}, Lorg/a/a/j;->f()Ljava/util/Enumeration;

    move-result-object v0

    invoke-virtual {p1}, Lorg/a/a/j;->f()Ljava/util/Enumeration;

    move-result-object p1

    :cond_2
    :goto_0
    invoke-interface {v0}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-direct {p0, v0}, Lorg/a/a/j;->a(Ljava/util/Enumeration;)Lorg/a/a/a;

    move-result-object v2

    invoke-direct {p0, p1}, Lorg/a/a/j;->a(Ljava/util/Enumeration;)Lorg/a/a/a;

    move-result-object v3

    invoke-interface {v2}, Lorg/a/a/a;->a()Lorg/a/a/i;

    move-result-object v2

    invoke-interface {v3}, Lorg/a/a/a;->a()Lorg/a/a/i;

    move-result-object v3

    if-eq v2, v3, :cond_2

    invoke-virtual {v2, v3}, Lorg/a/a/i;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_0

    :cond_3
    return v1

    :cond_4
    const/4 p1, 0x1

    return p1
.end method

.method public c()[Lorg/a/a/a;
    .locals 3

    invoke-virtual {p0}, Lorg/a/a/j;->g()I

    move-result v0

    new-array v0, v0, [Lorg/a/a/a;

    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0}, Lorg/a/a/j;->g()I

    move-result v2

    if-eq v1, v2, :cond_0

    invoke-virtual {p0, v1}, Lorg/a/a/j;->a(I)Lorg/a/a/a;

    move-result-object v2

    aput-object v2, v0, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return-object v0
.end method

.method public f()Ljava/util/Enumeration;
    .locals 1

    iget-object v0, p0, Lorg/a/a/j;->a:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->elements()Ljava/util/Enumeration;

    move-result-object v0

    return-object v0
.end method

.method public g()I
    .locals 1

    iget-object v0, p0, Lorg/a/a/j;->a:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    return v0
.end method

.method public hashCode()I
    .locals 3

    invoke-virtual {p0}, Lorg/a/a/j;->f()Ljava/util/Enumeration;

    move-result-object v0

    invoke-virtual {p0}, Lorg/a/a/j;->g()I

    move-result v1

    :goto_0
    invoke-interface {v0}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-direct {p0, v0}, Lorg/a/a/j;->a(Ljava/util/Enumeration;)Lorg/a/a/a;

    move-result-object v2

    mul-int/lit8 v1, v1, 0x11

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    xor-int/2addr v1, v2

    goto :goto_0

    :cond_0
    return v1
.end method

.method public iterator()Ljava/util/Iterator;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator<",
            "Lorg/a/a/a;",
            ">;"
        }
    .end annotation

    new-instance v0, Lorg/a/f/a$a;

    invoke-virtual {p0}, Lorg/a/a/j;->c()[Lorg/a/a/a;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/a/f/a$a;-><init>([Ljava/lang/Object;)V

    return-object v0
.end method

.method m_()Lorg/a/a/i;
    .locals 2

    new-instance v0, Lorg/a/a/n;

    invoke-direct {v0}, Lorg/a/a/n;-><init>()V

    iget-object v1, p0, Lorg/a/a/j;->a:Ljava/util/Vector;

    iput-object v1, v0, Lorg/a/a/j;->a:Ljava/util/Vector;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lorg/a/a/j;->a:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
