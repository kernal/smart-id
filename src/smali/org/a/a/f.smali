.class public abstract Lorg/a/a/f;
.super Lorg/a/a/i;

# interfaces
.implements Lorg/a/a/g;


# instance fields
.field a:[B


# direct methods
.method public constructor <init>([B)V
    .locals 1

    invoke-direct {p0}, Lorg/a/a/i;-><init>()V

    if-eqz p1, :cond_0

    iput-object p1, p0, Lorg/a/a/f;->a:[B

    return-void

    :cond_0
    new-instance p1, Ljava/lang/NullPointerException;

    const-string v0, "string cannot be null"

    invoke-direct {p1, v0}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw p1
.end method


# virtual methods
.method abstract a(Lorg/a/a/h;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method a(Lorg/a/a/i;)Z
    .locals 1

    instance-of v0, p1, Lorg/a/a/f;

    if-nez v0, :cond_0

    const/4 p1, 0x0

    return p1

    :cond_0
    check-cast p1, Lorg/a/a/f;

    iget-object v0, p0, Lorg/a/a/f;->a:[B

    iget-object p1, p1, Lorg/a/a/f;->a:[B

    invoke-static {v0, p1}, Lorg/a/f/a;->a([B[B)Z

    move-result p1

    return p1
.end method

.method public c()[B
    .locals 1

    iget-object v0, p0, Lorg/a/a/f;->a:[B

    return-object v0
.end method

.method public hashCode()I
    .locals 1

    invoke-virtual {p0}, Lorg/a/a/f;->c()[B

    move-result-object v0

    invoke-static {v0}, Lorg/a/f/a;->a([B)I

    move-result v0

    return v0
.end method

.method m_()Lorg/a/a/i;
    .locals 2

    new-instance v0, Lorg/a/a/l;

    iget-object v1, p0, Lorg/a/a/f;->a:[B

    invoke-direct {v0, v1}, Lorg/a/a/l;-><init>([B)V

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "#"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lorg/a/a/f;->a:[B

    invoke-static {v1}, Lorg/a/f/a/f;->a([B)[B

    move-result-object v1

    invoke-static {v1}, Lorg/a/f/h;->a([B)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
