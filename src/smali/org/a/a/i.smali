.class public abstract Lorg/a/a/i;
.super Lorg/a/a/d;


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lorg/a/a/d;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Lorg/a/a/i;
    .locals 0

    return-object p0
.end method

.method abstract a(Lorg/a/a/h;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method abstract a(Lorg/a/a/i;)Z
.end method

.method abstract d()I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    const/4 v0, 0x1

    if-ne p0, p1, :cond_0

    return v0

    :cond_0
    instance-of v1, p1, Lorg/a/a/a;

    if-eqz v1, :cond_1

    check-cast p1, Lorg/a/a/a;

    invoke-interface {p1}, Lorg/a/a/a;->a()Lorg/a/a/i;

    move-result-object p1

    invoke-virtual {p0, p1}, Lorg/a/a/i;->a(Lorg/a/a/i;)Z

    move-result p1

    if-eqz p1, :cond_1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public abstract hashCode()I
.end method

.method m_()Lorg/a/a/i;
    .locals 0

    return-object p0
.end method
