.class public interface abstract Lorg/a/a/d/a;
.super Ljava/lang/Object;


# static fields
.field public static final a:Lorg/a/a/e;

.field public static final b:Lorg/a/a/e;

.field public static final c:Lorg/a/a/e;

.field public static final d:Lorg/a/a/e;

.field public static final e:Lorg/a/a/e;

.field public static final f:Lorg/a/a/e;

.field public static final g:Lorg/a/a/e;

.field public static final h:Lorg/a/a/e;

.field public static final i:Lorg/a/a/e;

.field public static final j:Lorg/a/a/e;

.field public static final k:Lorg/a/a/e;

.field public static final l:Lorg/a/a/e;

.field public static final m:Lorg/a/a/e;

.field public static final n:Lorg/a/a/e;

.field public static final o:Lorg/a/a/e;

.field public static final p:Lorg/a/a/e;

.field public static final q:Lorg/a/a/e;

.field public static final r:Lorg/a/a/e;

.field public static final s:Lorg/a/a/e;

.field public static final t:Lorg/a/a/e;

.field public static final u:Lorg/a/a/e;

.field public static final v:Lorg/a/a/e;

.field public static final w:Lorg/a/a/e;

.field public static final x:Lorg/a/a/e;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    new-instance v0, Lorg/a/a/e;

    const-string v1, "0.4.0.127.0.7"

    invoke-direct {v0, v1}, Lorg/a/a/e;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/a/a/d/a;->a:Lorg/a/a/e;

    sget-object v0, Lorg/a/a/d/a;->a:Lorg/a/a/e;

    const-string v1, "2.2.1"

    invoke-virtual {v0, v1}, Lorg/a/a/e;->a(Ljava/lang/String;)Lorg/a/a/e;

    move-result-object v0

    sput-object v0, Lorg/a/a/d/a;->b:Lorg/a/a/e;

    sget-object v0, Lorg/a/a/d/a;->b:Lorg/a/a/e;

    const-string v1, "1"

    invoke-virtual {v0, v1}, Lorg/a/a/e;->a(Ljava/lang/String;)Lorg/a/a/e;

    move-result-object v0

    sput-object v0, Lorg/a/a/d/a;->c:Lorg/a/a/e;

    sget-object v0, Lorg/a/a/d/a;->b:Lorg/a/a/e;

    const-string v2, "2"

    invoke-virtual {v0, v2}, Lorg/a/a/e;->a(Ljava/lang/String;)Lorg/a/a/e;

    move-result-object v0

    sput-object v0, Lorg/a/a/d/a;->d:Lorg/a/a/e;

    sget-object v0, Lorg/a/a/d/a;->a:Lorg/a/a/e;

    const-string v3, "2.2.3"

    invoke-virtual {v0, v3}, Lorg/a/a/e;->a(Ljava/lang/String;)Lorg/a/a/e;

    move-result-object v0

    sput-object v0, Lorg/a/a/d/a;->e:Lorg/a/a/e;

    sget-object v0, Lorg/a/a/d/a;->e:Lorg/a/a/e;

    invoke-virtual {v0, v1}, Lorg/a/a/e;->a(Ljava/lang/String;)Lorg/a/a/e;

    move-result-object v0

    sput-object v0, Lorg/a/a/d/a;->f:Lorg/a/a/e;

    sget-object v0, Lorg/a/a/d/a;->f:Lorg/a/a/e;

    invoke-virtual {v0, v1}, Lorg/a/a/e;->a(Ljava/lang/String;)Lorg/a/a/e;

    move-result-object v0

    sput-object v0, Lorg/a/a/d/a;->g:Lorg/a/a/e;

    sget-object v0, Lorg/a/a/d/a;->e:Lorg/a/a/e;

    invoke-virtual {v0, v2}, Lorg/a/a/e;->a(Ljava/lang/String;)Lorg/a/a/e;

    move-result-object v0

    sput-object v0, Lorg/a/a/d/a;->h:Lorg/a/a/e;

    sget-object v0, Lorg/a/a/d/a;->h:Lorg/a/a/e;

    invoke-virtual {v0, v1}, Lorg/a/a/e;->a(Ljava/lang/String;)Lorg/a/a/e;

    move-result-object v0

    sput-object v0, Lorg/a/a/d/a;->i:Lorg/a/a/e;

    sget-object v0, Lorg/a/a/d/a;->a:Lorg/a/a/e;

    const-string v3, "2.2.2"

    invoke-virtual {v0, v3}, Lorg/a/a/e;->a(Ljava/lang/String;)Lorg/a/a/e;

    move-result-object v0

    sput-object v0, Lorg/a/a/d/a;->j:Lorg/a/a/e;

    sget-object v0, Lorg/a/a/d/a;->j:Lorg/a/a/e;

    invoke-virtual {v0, v1}, Lorg/a/a/e;->a(Ljava/lang/String;)Lorg/a/a/e;

    move-result-object v0

    sput-object v0, Lorg/a/a/d/a;->k:Lorg/a/a/e;

    sget-object v0, Lorg/a/a/d/a;->k:Lorg/a/a/e;

    invoke-virtual {v0, v1}, Lorg/a/a/e;->a(Ljava/lang/String;)Lorg/a/a/e;

    move-result-object v0

    sput-object v0, Lorg/a/a/d/a;->l:Lorg/a/a/e;

    sget-object v0, Lorg/a/a/d/a;->k:Lorg/a/a/e;

    invoke-virtual {v0, v2}, Lorg/a/a/e;->a(Ljava/lang/String;)Lorg/a/a/e;

    move-result-object v0

    sput-object v0, Lorg/a/a/d/a;->m:Lorg/a/a/e;

    sget-object v0, Lorg/a/a/d/a;->k:Lorg/a/a/e;

    const-string v3, "3"

    invoke-virtual {v0, v3}, Lorg/a/a/e;->a(Ljava/lang/String;)Lorg/a/a/e;

    move-result-object v0

    sput-object v0, Lorg/a/a/d/a;->n:Lorg/a/a/e;

    sget-object v0, Lorg/a/a/d/a;->k:Lorg/a/a/e;

    const-string v4, "4"

    invoke-virtual {v0, v4}, Lorg/a/a/e;->a(Ljava/lang/String;)Lorg/a/a/e;

    move-result-object v0

    sput-object v0, Lorg/a/a/d/a;->o:Lorg/a/a/e;

    sget-object v0, Lorg/a/a/d/a;->k:Lorg/a/a/e;

    const-string v5, "5"

    invoke-virtual {v0, v5}, Lorg/a/a/e;->a(Ljava/lang/String;)Lorg/a/a/e;

    move-result-object v0

    sput-object v0, Lorg/a/a/d/a;->p:Lorg/a/a/e;

    sget-object v0, Lorg/a/a/d/a;->k:Lorg/a/a/e;

    const-string v6, "6"

    invoke-virtual {v0, v6}, Lorg/a/a/e;->a(Ljava/lang/String;)Lorg/a/a/e;

    move-result-object v0

    sput-object v0, Lorg/a/a/d/a;->q:Lorg/a/a/e;

    sget-object v0, Lorg/a/a/d/a;->j:Lorg/a/a/e;

    invoke-virtual {v0, v2}, Lorg/a/a/e;->a(Ljava/lang/String;)Lorg/a/a/e;

    move-result-object v0

    sput-object v0, Lorg/a/a/d/a;->r:Lorg/a/a/e;

    sget-object v0, Lorg/a/a/d/a;->r:Lorg/a/a/e;

    invoke-virtual {v0, v1}, Lorg/a/a/e;->a(Ljava/lang/String;)Lorg/a/a/e;

    move-result-object v0

    sput-object v0, Lorg/a/a/d/a;->s:Lorg/a/a/e;

    sget-object v0, Lorg/a/a/d/a;->r:Lorg/a/a/e;

    invoke-virtual {v0, v2}, Lorg/a/a/e;->a(Ljava/lang/String;)Lorg/a/a/e;

    move-result-object v0

    sput-object v0, Lorg/a/a/d/a;->t:Lorg/a/a/e;

    sget-object v0, Lorg/a/a/d/a;->r:Lorg/a/a/e;

    invoke-virtual {v0, v3}, Lorg/a/a/e;->a(Ljava/lang/String;)Lorg/a/a/e;

    move-result-object v0

    sput-object v0, Lorg/a/a/d/a;->u:Lorg/a/a/e;

    sget-object v0, Lorg/a/a/d/a;->r:Lorg/a/a/e;

    invoke-virtual {v0, v4}, Lorg/a/a/e;->a(Ljava/lang/String;)Lorg/a/a/e;

    move-result-object v0

    sput-object v0, Lorg/a/a/d/a;->v:Lorg/a/a/e;

    sget-object v0, Lorg/a/a/d/a;->r:Lorg/a/a/e;

    invoke-virtual {v0, v5}, Lorg/a/a/e;->a(Ljava/lang/String;)Lorg/a/a/e;

    move-result-object v0

    sput-object v0, Lorg/a/a/d/a;->w:Lorg/a/a/e;

    sget-object v0, Lorg/a/a/d/a;->a:Lorg/a/a/e;

    const-string v1, "3.1.2.1"

    invoke-virtual {v0, v1}, Lorg/a/a/e;->a(Ljava/lang/String;)Lorg/a/a/e;

    move-result-object v0

    sput-object v0, Lorg/a/a/d/a;->x:Lorg/a/a/e;

    return-void
.end method
