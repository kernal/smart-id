.class public Lorg/a/a/n;
.super Lorg/a/a/j;


# instance fields
.field private b:I


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lorg/a/a/j;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Lorg/a/a/n;->b:I

    return-void
.end method

.method public constructor <init>(Lorg/a/a/b;)V
    .locals 0

    invoke-direct {p0, p1}, Lorg/a/a/j;-><init>(Lorg/a/a/b;)V

    const/4 p1, -0x1

    iput p1, p0, Lorg/a/a/n;->b:I

    return-void
.end method

.method private h()I
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget v0, p0, Lorg/a/a/n;->b:I

    if-gez v0, :cond_1

    const/4 v0, 0x0

    invoke-virtual {p0}, Lorg/a/a/n;->f()Ljava/util/Enumeration;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/a/a/a;

    invoke-interface {v2}, Lorg/a/a/a;->a()Lorg/a/a/i;

    move-result-object v2

    invoke-virtual {v2}, Lorg/a/a/i;->m_()Lorg/a/a/i;

    move-result-object v2

    invoke-virtual {v2}, Lorg/a/a/i;->d()I

    move-result v2

    add-int/2addr v0, v2

    goto :goto_0

    :cond_0
    iput v0, p0, Lorg/a/a/n;->b:I

    :cond_1
    iget v0, p0, Lorg/a/a/n;->b:I

    return v0
.end method


# virtual methods
.method a(Lorg/a/a/h;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p1}, Lorg/a/a/h;->a()Lorg/a/a/h;

    move-result-object v0

    invoke-direct {p0}, Lorg/a/a/n;->h()I

    move-result v1

    const/16 v2, 0x30

    invoke-virtual {p1, v2}, Lorg/a/a/h;->b(I)V

    invoke-virtual {p1, v1}, Lorg/a/a/h;->a(I)V

    invoke-virtual {p0}, Lorg/a/a/n;->f()Ljava/util/Enumeration;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/a/a/a;

    invoke-virtual {v0, v1}, Lorg/a/a/h;->a(Lorg/a/a/a;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method d()I
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-direct {p0}, Lorg/a/a/n;->h()I

    move-result v0

    invoke-static {v0}, Lorg/a/a/p;->a(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v1, v0

    return v1
.end method
