.class public interface abstract Lorg/a/a/c/a;
.super Ljava/lang/Object;


# static fields
.field public static final A:Lorg/a/a/e;

.field public static final B:Lorg/a/a/e;

.field public static final C:Lorg/a/a/e;

.field public static final D:Lorg/a/a/e;

.field public static final E:Lorg/a/a/e;

.field public static final F:Lorg/a/a/e;

.field public static final a:Lorg/a/a/e;

.field public static final b:Lorg/a/a/e;

.field public static final c:Lorg/a/a/e;

.field public static final d:Lorg/a/a/e;

.field public static final e:Lorg/a/a/e;

.field public static final f:Lorg/a/a/e;

.field public static final g:Lorg/a/a/e;

.field public static final h:Lorg/a/a/e;

.field public static final i:Lorg/a/a/e;

.field public static final j:Lorg/a/a/e;

.field public static final k:Lorg/a/a/e;

.field public static final l:Lorg/a/a/e;

.field public static final m:Lorg/a/a/e;

.field public static final n:Lorg/a/a/e;

.field public static final o:Lorg/a/a/e;

.field public static final p:Lorg/a/a/e;

.field public static final q:Lorg/a/a/e;

.field public static final r:Lorg/a/a/e;

.field public static final s:Lorg/a/a/e;

.field public static final t:Lorg/a/a/e;

.field public static final u:Lorg/a/a/e;

.field public static final v:Lorg/a/a/e;

.field public static final w:Lorg/a/a/e;

.field public static final x:Lorg/a/a/e;

.field public static final y:Lorg/a/a/e;

.field public static final z:Lorg/a/a/e;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Lorg/a/a/e;

    const-string v1, "1.2.643.2.2"

    invoke-direct {v0, v1}, Lorg/a/a/e;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/a/a/c/a;->a:Lorg/a/a/e;

    sget-object v0, Lorg/a/a/c/a;->a:Lorg/a/a/e;

    const-string v1, "9"

    invoke-virtual {v0, v1}, Lorg/a/a/e;->a(Ljava/lang/String;)Lorg/a/a/e;

    move-result-object v0

    sput-object v0, Lorg/a/a/c/a;->b:Lorg/a/a/e;

    sget-object v0, Lorg/a/a/c/a;->a:Lorg/a/a/e;

    const-string v1, "10"

    invoke-virtual {v0, v1}, Lorg/a/a/e;->a(Ljava/lang/String;)Lorg/a/a/e;

    move-result-object v0

    sput-object v0, Lorg/a/a/c/a;->c:Lorg/a/a/e;

    sget-object v0, Lorg/a/a/c/a;->a:Lorg/a/a/e;

    const-string v1, "13.0"

    invoke-virtual {v0, v1}, Lorg/a/a/e;->a(Ljava/lang/String;)Lorg/a/a/e;

    move-result-object v0

    sput-object v0, Lorg/a/a/c/a;->d:Lorg/a/a/e;

    sget-object v0, Lorg/a/a/c/a;->a:Lorg/a/a/e;

    const-string v1, "13.1"

    invoke-virtual {v0, v1}, Lorg/a/a/e;->a(Ljava/lang/String;)Lorg/a/a/e;

    move-result-object v0

    sput-object v0, Lorg/a/a/c/a;->e:Lorg/a/a/e;

    sget-object v0, Lorg/a/a/c/a;->a:Lorg/a/a/e;

    const-string v1, "21"

    invoke-virtual {v0, v1}, Lorg/a/a/e;->a(Ljava/lang/String;)Lorg/a/a/e;

    move-result-object v0

    sput-object v0, Lorg/a/a/c/a;->f:Lorg/a/a/e;

    sget-object v0, Lorg/a/a/c/a;->a:Lorg/a/a/e;

    const-string v1, "31.0"

    invoke-virtual {v0, v1}, Lorg/a/a/e;->a(Ljava/lang/String;)Lorg/a/a/e;

    move-result-object v0

    sput-object v0, Lorg/a/a/c/a;->g:Lorg/a/a/e;

    sget-object v0, Lorg/a/a/c/a;->a:Lorg/a/a/e;

    const-string v1, "31.1"

    invoke-virtual {v0, v1}, Lorg/a/a/e;->a(Ljava/lang/String;)Lorg/a/a/e;

    move-result-object v0

    sput-object v0, Lorg/a/a/c/a;->h:Lorg/a/a/e;

    sget-object v0, Lorg/a/a/c/a;->a:Lorg/a/a/e;

    const-string v1, "31.2"

    invoke-virtual {v0, v1}, Lorg/a/a/e;->a(Ljava/lang/String;)Lorg/a/a/e;

    move-result-object v0

    sput-object v0, Lorg/a/a/c/a;->i:Lorg/a/a/e;

    sget-object v0, Lorg/a/a/c/a;->a:Lorg/a/a/e;

    const-string v1, "31.3"

    invoke-virtual {v0, v1}, Lorg/a/a/e;->a(Ljava/lang/String;)Lorg/a/a/e;

    move-result-object v0

    sput-object v0, Lorg/a/a/c/a;->j:Lorg/a/a/e;

    sget-object v0, Lorg/a/a/c/a;->a:Lorg/a/a/e;

    const-string v1, "31.4"

    invoke-virtual {v0, v1}, Lorg/a/a/e;->a(Ljava/lang/String;)Lorg/a/a/e;

    move-result-object v0

    sput-object v0, Lorg/a/a/c/a;->k:Lorg/a/a/e;

    sget-object v0, Lorg/a/a/c/a;->a:Lorg/a/a/e;

    const-string v1, "20"

    invoke-virtual {v0, v1}, Lorg/a/a/e;->a(Ljava/lang/String;)Lorg/a/a/e;

    move-result-object v0

    sput-object v0, Lorg/a/a/c/a;->l:Lorg/a/a/e;

    sget-object v0, Lorg/a/a/c/a;->a:Lorg/a/a/e;

    const-string v1, "19"

    invoke-virtual {v0, v1}, Lorg/a/a/e;->a(Ljava/lang/String;)Lorg/a/a/e;

    move-result-object v0

    sput-object v0, Lorg/a/a/c/a;->m:Lorg/a/a/e;

    sget-object v0, Lorg/a/a/c/a;->a:Lorg/a/a/e;

    const-string v1, "4"

    invoke-virtual {v0, v1}, Lorg/a/a/e;->a(Ljava/lang/String;)Lorg/a/a/e;

    move-result-object v0

    sput-object v0, Lorg/a/a/c/a;->n:Lorg/a/a/e;

    sget-object v0, Lorg/a/a/c/a;->a:Lorg/a/a/e;

    const-string v1, "3"

    invoke-virtual {v0, v1}, Lorg/a/a/e;->a(Ljava/lang/String;)Lorg/a/a/e;

    move-result-object v0

    sput-object v0, Lorg/a/a/c/a;->o:Lorg/a/a/e;

    sget-object v0, Lorg/a/a/c/a;->a:Lorg/a/a/e;

    const-string v1, "30.1"

    invoke-virtual {v0, v1}, Lorg/a/a/e;->a(Ljava/lang/String;)Lorg/a/a/e;

    move-result-object v0

    sput-object v0, Lorg/a/a/c/a;->p:Lorg/a/a/e;

    sget-object v0, Lorg/a/a/c/a;->a:Lorg/a/a/e;

    const-string v1, "32.2"

    invoke-virtual {v0, v1}, Lorg/a/a/e;->a(Ljava/lang/String;)Lorg/a/a/e;

    move-result-object v0

    sput-object v0, Lorg/a/a/c/a;->q:Lorg/a/a/e;

    sget-object v0, Lorg/a/a/c/a;->a:Lorg/a/a/e;

    const-string v1, "32.3"

    invoke-virtual {v0, v1}, Lorg/a/a/e;->a(Ljava/lang/String;)Lorg/a/a/e;

    move-result-object v0

    sput-object v0, Lorg/a/a/c/a;->r:Lorg/a/a/e;

    sget-object v0, Lorg/a/a/c/a;->a:Lorg/a/a/e;

    const-string v1, "32.4"

    invoke-virtual {v0, v1}, Lorg/a/a/e;->a(Ljava/lang/String;)Lorg/a/a/e;

    move-result-object v0

    sput-object v0, Lorg/a/a/c/a;->s:Lorg/a/a/e;

    sget-object v0, Lorg/a/a/c/a;->a:Lorg/a/a/e;

    const-string v1, "32.5"

    invoke-virtual {v0, v1}, Lorg/a/a/e;->a(Ljava/lang/String;)Lorg/a/a/e;

    move-result-object v0

    sput-object v0, Lorg/a/a/c/a;->t:Lorg/a/a/e;

    sget-object v0, Lorg/a/a/c/a;->a:Lorg/a/a/e;

    const-string v1, "33.1"

    invoke-virtual {v0, v1}, Lorg/a/a/e;->a(Ljava/lang/String;)Lorg/a/a/e;

    move-result-object v0

    sput-object v0, Lorg/a/a/c/a;->u:Lorg/a/a/e;

    sget-object v0, Lorg/a/a/c/a;->a:Lorg/a/a/e;

    const-string v1, "33.2"

    invoke-virtual {v0, v1}, Lorg/a/a/e;->a(Ljava/lang/String;)Lorg/a/a/e;

    move-result-object v0

    sput-object v0, Lorg/a/a/c/a;->v:Lorg/a/a/e;

    sget-object v0, Lorg/a/a/c/a;->a:Lorg/a/a/e;

    const-string v1, "33.3"

    invoke-virtual {v0, v1}, Lorg/a/a/e;->a(Ljava/lang/String;)Lorg/a/a/e;

    move-result-object v0

    sput-object v0, Lorg/a/a/c/a;->w:Lorg/a/a/e;

    sget-object v0, Lorg/a/a/c/a;->a:Lorg/a/a/e;

    const-string v1, "35.1"

    invoke-virtual {v0, v1}, Lorg/a/a/e;->a(Ljava/lang/String;)Lorg/a/a/e;

    move-result-object v0

    sput-object v0, Lorg/a/a/c/a;->x:Lorg/a/a/e;

    sget-object v0, Lorg/a/a/c/a;->a:Lorg/a/a/e;

    const-string v1, "35.2"

    invoke-virtual {v0, v1}, Lorg/a/a/e;->a(Ljava/lang/String;)Lorg/a/a/e;

    move-result-object v0

    sput-object v0, Lorg/a/a/c/a;->y:Lorg/a/a/e;

    sget-object v0, Lorg/a/a/c/a;->a:Lorg/a/a/e;

    const-string v1, "35.3"

    invoke-virtual {v0, v1}, Lorg/a/a/e;->a(Ljava/lang/String;)Lorg/a/a/e;

    move-result-object v0

    sput-object v0, Lorg/a/a/c/a;->z:Lorg/a/a/e;

    sget-object v0, Lorg/a/a/c/a;->a:Lorg/a/a/e;

    const-string v1, "36.0"

    invoke-virtual {v0, v1}, Lorg/a/a/e;->a(Ljava/lang/String;)Lorg/a/a/e;

    move-result-object v0

    sput-object v0, Lorg/a/a/c/a;->A:Lorg/a/a/e;

    sget-object v0, Lorg/a/a/c/a;->a:Lorg/a/a/e;

    const-string v2, "36.1"

    invoke-virtual {v0, v2}, Lorg/a/a/e;->a(Ljava/lang/String;)Lorg/a/a/e;

    move-result-object v0

    sput-object v0, Lorg/a/a/c/a;->B:Lorg/a/a/e;

    sget-object v0, Lorg/a/a/c/a;->a:Lorg/a/a/e;

    invoke-virtual {v0, v1}, Lorg/a/a/e;->a(Ljava/lang/String;)Lorg/a/a/e;

    move-result-object v0

    sput-object v0, Lorg/a/a/c/a;->C:Lorg/a/a/e;

    sget-object v0, Lorg/a/a/c/a;->a:Lorg/a/a/e;

    invoke-virtual {v0, v2}, Lorg/a/a/e;->a(Ljava/lang/String;)Lorg/a/a/e;

    move-result-object v0

    sput-object v0, Lorg/a/a/c/a;->D:Lorg/a/a/e;

    sget-object v0, Lorg/a/a/c/a;->a:Lorg/a/a/e;

    const-string v1, "96"

    invoke-virtual {v0, v1}, Lorg/a/a/e;->a(Ljava/lang/String;)Lorg/a/a/e;

    move-result-object v0

    sput-object v0, Lorg/a/a/c/a;->E:Lorg/a/a/e;

    sget-object v0, Lorg/a/a/c/a;->a:Lorg/a/a/e;

    const-string v1, "98"

    invoke-virtual {v0, v1}, Lorg/a/a/e;->a(Ljava/lang/String;)Lorg/a/a/e;

    move-result-object v0

    sput-object v0, Lorg/a/a/c/a;->F:Lorg/a/a/e;

    return-void
.end method
