.class Lee/cyber/smartid/SmartIdService$1;
.super Ljava/lang/Object;
.source "SmartIdService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lee/cyber/smartid/SmartIdService;->initService(Ljava/lang/String;Lee/cyber/smartid/inter/InitServiceListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:Lee/cyber/smartid/SmartIdService;


# direct methods
.method constructor <init>(Lee/cyber/smartid/SmartIdService;Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lee/cyber/smartid/SmartIdService$1;->b:Lee/cyber/smartid/SmartIdService;

    iput-object p2, p0, Lee/cyber/smartid/SmartIdService$1;->a:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 13

    .line 1
    invoke-static {}, Landroid/os/Looper;->prepare()V

    .line 2
    iget-object v0, p0, Lee/cyber/smartid/SmartIdService$1;->b:Lee/cyber/smartid/SmartIdService;

    invoke-static {v0}, Lee/cyber/smartid/SmartIdService;->A(Lee/cyber/smartid/SmartIdService;)Ljava/lang/Object;

    move-result-object v0

    monitor-enter v0

    .line 3
    :try_start_0
    invoke-static {}, Lee/cyber/smartid/SmartIdService;->a()Lee/cyber/smartid/util/Log;

    move-result-object v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const-string v2, "initService - thread start"

    :try_start_1
    invoke-virtual {v1, v2}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    .line 4
    iget-object v1, p0, Lee/cyber/smartid/SmartIdService$1;->b:Lee/cyber/smartid/SmartIdService;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lee/cyber/smartid/SmartIdService;->a(Lee/cyber/smartid/SmartIdService;Z)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    const-wide/16 v1, 0x0

    const/4 v3, 0x0

    .line 5
    :try_start_2
    invoke-static {}, Lee/cyber/smartid/SmartIdService;->a()Lee/cyber/smartid/util/Log;

    move-result-object v4
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1a
    .catch Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException; {:try_start_2 .. :try_end_2} :catch_19
    .catch Ljava/lang/SecurityException; {:try_start_2 .. :try_end_2} :catch_18
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_17
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_16
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    const-string v5, "initService - reading service.properties file .."

    :try_start_3
    invoke-virtual {v4, v5}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1a
    .catch Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException; {:try_start_3 .. :try_end_3} :catch_19
    .catch Ljava/lang/SecurityException; {:try_start_3 .. :try_end_3} :catch_18
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_17
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_16
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    const-wide/16 v4, 0x3f6

    .line 6
    :try_start_4
    iget-object v6, p0, Lee/cyber/smartid/SmartIdService$1;->b:Lee/cyber/smartid/SmartIdService;

    invoke-static {v6}, Lee/cyber/smartid/SmartIdService;->k(Lee/cyber/smartid/SmartIdService;)Lee/cyber/smartid/manager/inter/PropertiesManager;

    move-result-object v6

    invoke-interface {v6}, Lee/cyber/smartid/manager/inter/PropertiesManager;->parseProperties()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_15
    .catch Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException; {:try_start_4 .. :try_end_4} :catch_14
    .catch Ljava/lang/SecurityException; {:try_start_4 .. :try_end_4} :catch_13
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_12
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_11
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 7
    :try_start_5
    invoke-static {}, Lee/cyber/smartid/SmartIdService;->a()Lee/cyber/smartid/util/Log;

    move-result-object v4
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1a
    .catch Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException; {:try_start_5 .. :try_end_5} :catch_19
    .catch Ljava/lang/SecurityException; {:try_start_5 .. :try_end_5} :catch_18
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_17
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_16
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    const-string v5, "initService - creating the StoredAccountManager .."

    :try_start_6
    invoke-virtual {v4, v5}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    .line 8
    iget-object v4, p0, Lee/cyber/smartid/SmartIdService$1;->b:Lee/cyber/smartid/SmartIdService;

    iget-object v5, p0, Lee/cyber/smartid/SmartIdService$1;->b:Lee/cyber/smartid/SmartIdService;

    invoke-static {v5}, Lee/cyber/smartid/SmartIdService;->n(Lee/cyber/smartid/SmartIdService;)Lee/cyber/smartid/inter/ServiceAccess;

    move-result-object v5

    iget-object v6, p0, Lee/cyber/smartid/SmartIdService$1;->b:Lee/cyber/smartid/SmartIdService;

    invoke-static {v6}, Lee/cyber/smartid/SmartIdService;->B(Lee/cyber/smartid/SmartIdService;)Lee/cyber/smartid/inter/ListenerAccess;

    move-result-object v6

    iget-object v7, p0, Lee/cyber/smartid/SmartIdService$1;->b:Lee/cyber/smartid/SmartIdService;

    invoke-static {v7}, Lee/cyber/smartid/SmartIdService;->C(Lee/cyber/smartid/SmartIdService;)Lee/cyber/smartid/cryptolib/CryptoLib;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lee/cyber/smartid/manager/impl/StoredAccountManagerImpl;->getInstance(Lee/cyber/smartid/inter/ServiceAccess;Lee/cyber/smartid/inter/ListenerAccess;Lee/cyber/smartid/cryptolib/inter/StorageOp;)Lee/cyber/smartid/manager/impl/StoredAccountManagerImpl;

    move-result-object v5

    invoke-static {v4, v5}, Lee/cyber/smartid/SmartIdService;->a(Lee/cyber/smartid/SmartIdService;Lee/cyber/smartid/manager/inter/StoredAccountManager;)Lee/cyber/smartid/manager/inter/StoredAccountManager;

    .line 9
    invoke-static {}, Lee/cyber/smartid/SmartIdService;->a()Lee/cyber/smartid/util/Log;

    move-result-object v4
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_1a
    .catch Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException; {:try_start_6 .. :try_end_6} :catch_19
    .catch Ljava/lang/SecurityException; {:try_start_6 .. :try_end_6} :catch_18
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_17
    .catch Ljava/lang/Throwable; {:try_start_6 .. :try_end_6} :catch_16
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    const-string v5, "initService - upgrading SmartIdService storage .."

    :try_start_7
    invoke-virtual {v4, v5}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_1a
    .catch Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException; {:try_start_7 .. :try_end_7} :catch_19
    .catch Ljava/lang/SecurityException; {:try_start_7 .. :try_end_7} :catch_18
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_17
    .catch Ljava/lang/Throwable; {:try_start_7 .. :try_end_7} :catch_16
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    const-wide/16 v4, 0x400

    .line 10
    :try_start_8
    iget-object v6, p0, Lee/cyber/smartid/SmartIdService$1;->b:Lee/cyber/smartid/SmartIdService;

    invoke-static {v6}, Lee/cyber/smartid/SmartIdService;->e(Lee/cyber/smartid/SmartIdService;)Lee/cyber/smartid/dto/upgrade/InteractiveUpgradeState;

    move-result-object v4
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_15
    .catch Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException; {:try_start_8 .. :try_end_8} :catch_14
    .catch Ljava/lang/SecurityException; {:try_start_8 .. :try_end_8} :catch_13
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_12
    .catch Ljava/lang/Throwable; {:try_start_8 .. :try_end_8} :catch_11
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 11
    :try_start_9
    invoke-static {}, Lee/cyber/smartid/SmartIdService;->a()Lee/cyber/smartid/util/Log;

    move-result-object v5
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_10
    .catch Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException; {:try_start_9 .. :try_end_9} :catch_f
    .catch Ljava/lang/SecurityException; {:try_start_9 .. :try_end_9} :catch_e
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_d
    .catch Ljava/lang/Throwable; {:try_start_9 .. :try_end_9} :catch_c
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    const-string v6, "initService - initializing TSE .."

    :try_start_a
    invoke-virtual {v5, v6}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_10
    .catch Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException; {:try_start_a .. :try_end_a} :catch_f
    .catch Ljava/lang/SecurityException; {:try_start_a .. :try_end_a} :catch_e
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_d
    .catch Ljava/lang/Throwable; {:try_start_a .. :try_end_a} :catch_c
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    const-wide/16 v5, 0x408

    .line 12
    :try_start_b
    iget-object v7, p0, Lee/cyber/smartid/SmartIdService$1;->b:Lee/cyber/smartid/SmartIdService;

    invoke-static {v7}, Lee/cyber/smartid/SmartIdService;->C(Lee/cyber/smartid/SmartIdService;)Lee/cyber/smartid/cryptolib/CryptoLib;

    move-result-object v7
    :try_end_b
    .catch Lee/cyber/smartid/tse/dto/InitTSEException; {:try_start_b .. :try_end_b} :catch_6
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_5
    .catch Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException; {:try_start_b .. :try_end_b} :catch_4
    .catch Ljava/lang/SecurityException; {:try_start_b .. :try_end_b} :catch_3
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_2
    .catch Ljava/lang/Throwable; {:try_start_b .. :try_end_b} :catch_1
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    const-string v8, "ee.cyber.smartid.TAG_TSE_STORAGE_VERSION"

    :try_start_c
    new-instance v9, Lee/cyber/smartid/SmartIdService$1$1;

    invoke-direct {v9, p0}, Lee/cyber/smartid/SmartIdService$1$1;-><init>(Lee/cyber/smartid/SmartIdService$1;)V

    .line 13
    invoke-virtual {v9}, Lcom/google/gson/b/a;->getType()Ljava/lang/reflect/Type;

    move-result-object v9

    .line 14
    invoke-virtual {v7, v8, v9}, Lee/cyber/smartid/cryptolib/CryptoLib;->retrieveData(Ljava/lang/String;Ljava/lang/reflect/Type;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Integer;

    if-nez v7, :cond_0

    .line 15
    iget-object v7, p0, Lee/cyber/smartid/SmartIdService$1;->b:Lee/cyber/smartid/SmartIdService;
    :try_end_c
    .catch Lee/cyber/smartid/tse/dto/InitTSEException; {:try_start_c .. :try_end_c} :catch_6
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_5
    .catch Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException; {:try_start_c .. :try_end_c} :catch_4
    .catch Ljava/lang/SecurityException; {:try_start_c .. :try_end_c} :catch_3
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_c} :catch_2
    .catch Ljava/lang/Throwable; {:try_start_c .. :try_end_c} :catch_1
    .catchall {:try_start_c .. :try_end_c} :catchall_0

    const-string v8, "ee.cyber.smartid.TAG_TSE_STORAGE_VERSION"

    const/4 v9, 0x5

    :try_start_d
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-static {v7, v8, v9}, Lee/cyber/smartid/SmartIdService;->a(Lee/cyber/smartid/SmartIdService;Ljava/lang/String;Ljava/lang/Object;)V

    .line 16
    :cond_0
    iget-object v7, p0, Lee/cyber/smartid/SmartIdService$1;->b:Lee/cyber/smartid/SmartIdService;

    invoke-static {v7}, Lee/cyber/smartid/SmartIdService;->d(Lee/cyber/smartid/SmartIdService;)Lee/cyber/smartid/tse/SmartIdTSE;

    move-result-object v7

    invoke-virtual {v7}, Lee/cyber/smartid/tse/SmartIdTSE;->initializeTSE()V

    .line 17
    iget-object v7, p0, Lee/cyber/smartid/SmartIdService$1;->b:Lee/cyber/smartid/SmartIdService;

    invoke-static {v7}, Lee/cyber/smartid/SmartIdService;->d(Lee/cyber/smartid/SmartIdService;)Lee/cyber/smartid/tse/SmartIdTSE;

    move-result-object v7
    :try_end_d
    .catch Lee/cyber/smartid/tse/dto/InitTSEException; {:try_start_d .. :try_end_d} :catch_6
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_d} :catch_5
    .catch Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException; {:try_start_d .. :try_end_d} :catch_4
    .catch Ljava/lang/SecurityException; {:try_start_d .. :try_end_d} :catch_3
    .catch Ljava/lang/Exception; {:try_start_d .. :try_end_d} :catch_2
    .catch Ljava/lang/Throwable; {:try_start_d .. :try_end_d} :catch_1
    .catchall {:try_start_d .. :try_end_d} :catchall_0

    const-string v8, "ee.cyber.smartid.TAG_SMARTIDSERVICE_SYSTEM_EVENT_LISTENER"

    :try_start_e
    new-instance v9, Lee/cyber/smartid/SmartIdService$1$2;

    invoke-direct {v9, p0}, Lee/cyber/smartid/SmartIdService$1$2;-><init>(Lee/cyber/smartid/SmartIdService$1;)V

    invoke-virtual {v7, v8, v9}, Lee/cyber/smartid/tse/SmartIdTSE;->setListener(Ljava/lang/String;Lee/cyber/smartid/tse/inter/TSEListener;)V
    :try_end_e
    .catch Lee/cyber/smartid/tse/dto/InitTSEException; {:try_start_e .. :try_end_e} :catch_6
    .catch Ljava/io/IOException; {:try_start_e .. :try_end_e} :catch_5
    .catch Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException; {:try_start_e .. :try_end_e} :catch_4
    .catch Ljava/lang/SecurityException; {:try_start_e .. :try_end_e} :catch_3
    .catch Ljava/lang/Exception; {:try_start_e .. :try_end_e} :catch_2
    .catch Ljava/lang/Throwable; {:try_start_e .. :try_end_e} :catch_1
    .catchall {:try_start_e .. :try_end_e} :catchall_0

    .line 18
    :try_start_f
    invoke-static {}, Lee/cyber/smartid/SmartIdService;->a()Lee/cyber/smartid/util/Log;

    move-result-object v5
    :try_end_f
    .catch Ljava/io/IOException; {:try_start_f .. :try_end_f} :catch_10
    .catch Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException; {:try_start_f .. :try_end_f} :catch_f
    .catch Ljava/lang/SecurityException; {:try_start_f .. :try_end_f} :catch_e
    .catch Ljava/lang/Exception; {:try_start_f .. :try_end_f} :catch_d
    .catch Ljava/lang/Throwable; {:try_start_f .. :try_end_f} :catch_c
    .catchall {:try_start_f .. :try_end_f} :catchall_0

    const-string v6, "initService - creating the API interface .."

    :try_start_10
    invoke-virtual {v5, v6}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    .line 19
    iget-object v5, p0, Lee/cyber/smartid/SmartIdService$1;->b:Lee/cyber/smartid/SmartIdService;

    iget-object v6, p0, Lee/cyber/smartid/SmartIdService$1;->b:Lee/cyber/smartid/SmartIdService;

    invoke-static {v6}, Lee/cyber/smartid/SmartIdService;->d(Lee/cyber/smartid/SmartIdService;)Lee/cyber/smartid/tse/SmartIdTSE;

    move-result-object v6

    const-class v7, Lee/cyber/smartid/network/SmartIdAPI;

    invoke-virtual {v6, v7}, Lee/cyber/smartid/tse/SmartIdTSE;->createAPI(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lee/cyber/smartid/network/SmartIdAPI;

    invoke-static {v5, v6}, Lee/cyber/smartid/SmartIdService;->a(Lee/cyber/smartid/SmartIdService;Lee/cyber/smartid/network/SmartIdAPI;)Lee/cyber/smartid/network/SmartIdAPI;

    .line 20
    invoke-static {}, Lee/cyber/smartid/SmartIdService;->a()Lee/cyber/smartid/util/Log;

    move-result-object v5
    :try_end_10
    .catch Ljava/io/IOException; {:try_start_10 .. :try_end_10} :catch_10
    .catch Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException; {:try_start_10 .. :try_end_10} :catch_f
    .catch Ljava/lang/SecurityException; {:try_start_10 .. :try_end_10} :catch_e
    .catch Ljava/lang/Exception; {:try_start_10 .. :try_end_10} :catch_d
    .catch Ljava/lang/Throwable; {:try_start_10 .. :try_end_10} :catch_c
    .catchall {:try_start_10 .. :try_end_10} :catchall_0

    const-string v6, "initService - creating the DeleteAccountManager .."

    :try_start_11
    invoke-virtual {v5, v6}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    .line 21
    iget-object v5, p0, Lee/cyber/smartid/SmartIdService$1;->b:Lee/cyber/smartid/SmartIdService;

    iget-object v6, p0, Lee/cyber/smartid/SmartIdService$1;->b:Lee/cyber/smartid/SmartIdService;

    invoke-static {v6}, Lee/cyber/smartid/SmartIdService;->D(Lee/cyber/smartid/SmartIdService;)Lee/cyber/smartid/network/SmartIdAPI;

    move-result-object v6

    iget-object v7, p0, Lee/cyber/smartid/SmartIdService$1;->b:Lee/cyber/smartid/SmartIdService;

    invoke-static {v7}, Lee/cyber/smartid/SmartIdService;->d(Lee/cyber/smartid/SmartIdService;)Lee/cyber/smartid/tse/SmartIdTSE;

    move-result-object v7

    iget-object v8, p0, Lee/cyber/smartid/SmartIdService$1;->b:Lee/cyber/smartid/SmartIdService;

    invoke-static {v8}, Lee/cyber/smartid/SmartIdService;->n(Lee/cyber/smartid/SmartIdService;)Lee/cyber/smartid/inter/ServiceAccess;

    move-result-object v8

    iget-object v9, p0, Lee/cyber/smartid/SmartIdService$1;->b:Lee/cyber/smartid/SmartIdService;

    invoke-static {v9}, Lee/cyber/smartid/SmartIdService;->B(Lee/cyber/smartid/SmartIdService;)Lee/cyber/smartid/inter/ListenerAccess;

    move-result-object v9

    iget-object v10, p0, Lee/cyber/smartid/SmartIdService$1;->b:Lee/cyber/smartid/SmartIdService;

    invoke-static {v10}, Lee/cyber/smartid/SmartIdService;->E(Lee/cyber/smartid/SmartIdService;)Lee/cyber/smartid/tse/inter/WallClock;

    move-result-object v10

    invoke-static {v6, v7, v8, v9, v10}, Lee/cyber/smartid/manager/impl/DeleteAccountManagerImpl;->getInstance(Lee/cyber/smartid/network/SmartIdAPI;Lee/cyber/smartid/tse/SmartIdTSE;Lee/cyber/smartid/inter/ServiceAccess;Lee/cyber/smartid/inter/ListenerAccess;Lee/cyber/smartid/tse/inter/WallClock;)Lee/cyber/smartid/manager/inter/DeleteAccountManager;

    move-result-object v6

    invoke-static {v5, v6}, Lee/cyber/smartid/SmartIdService;->a(Lee/cyber/smartid/SmartIdService;Lee/cyber/smartid/manager/inter/DeleteAccountManager;)Lee/cyber/smartid/manager/inter/DeleteAccountManager;

    .line 22
    invoke-static {}, Lee/cyber/smartid/SmartIdService;->a()Lee/cyber/smartid/util/Log;

    move-result-object v5
    :try_end_11
    .catch Ljava/io/IOException; {:try_start_11 .. :try_end_11} :catch_10
    .catch Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException; {:try_start_11 .. :try_end_11} :catch_f
    .catch Ljava/lang/SecurityException; {:try_start_11 .. :try_end_11} :catch_e
    .catch Ljava/lang/Exception; {:try_start_11 .. :try_end_11} :catch_d
    .catch Ljava/lang/Throwable; {:try_start_11 .. :try_end_11} :catch_c
    .catchall {:try_start_11 .. :try_end_11} :catchall_0

    const-string v6, "initService - creating the GetAccountStatusManager .."

    :try_start_12
    invoke-virtual {v5, v6}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    .line 23
    iget-object v5, p0, Lee/cyber/smartid/SmartIdService$1;->b:Lee/cyber/smartid/SmartIdService;

    iget-object v6, p0, Lee/cyber/smartid/SmartIdService$1;->b:Lee/cyber/smartid/SmartIdService;

    invoke-static {v6}, Lee/cyber/smartid/SmartIdService;->D(Lee/cyber/smartid/SmartIdService;)Lee/cyber/smartid/network/SmartIdAPI;

    move-result-object v6

    iget-object v7, p0, Lee/cyber/smartid/SmartIdService$1;->b:Lee/cyber/smartid/SmartIdService;

    invoke-static {v7}, Lee/cyber/smartid/SmartIdService;->d(Lee/cyber/smartid/SmartIdService;)Lee/cyber/smartid/tse/SmartIdTSE;

    move-result-object v7

    iget-object v8, p0, Lee/cyber/smartid/SmartIdService$1;->b:Lee/cyber/smartid/SmartIdService;

    invoke-static {v8}, Lee/cyber/smartid/SmartIdService;->n(Lee/cyber/smartid/SmartIdService;)Lee/cyber/smartid/inter/ServiceAccess;

    move-result-object v8

    iget-object v9, p0, Lee/cyber/smartid/SmartIdService$1;->b:Lee/cyber/smartid/SmartIdService;

    invoke-static {v9}, Lee/cyber/smartid/SmartIdService;->B(Lee/cyber/smartid/SmartIdService;)Lee/cyber/smartid/inter/ListenerAccess;

    move-result-object v9

    iget-object v10, p0, Lee/cyber/smartid/SmartIdService$1;->b:Lee/cyber/smartid/SmartIdService;

    invoke-static {v10}, Lee/cyber/smartid/SmartIdService;->E(Lee/cyber/smartid/SmartIdService;)Lee/cyber/smartid/tse/inter/WallClock;

    move-result-object v10

    invoke-static {v6, v7, v8, v9, v10}, Lee/cyber/smartid/manager/impl/GetAccountStatusManagerImpl;->getInstance(Lee/cyber/smartid/network/SmartIdAPI;Lee/cyber/smartid/tse/SmartIdTSE;Lee/cyber/smartid/inter/ServiceAccess;Lee/cyber/smartid/inter/ListenerAccess;Lee/cyber/smartid/tse/inter/WallClock;)Lee/cyber/smartid/manager/inter/GetAccountStatusManager;

    move-result-object v6

    invoke-static {v5, v6}, Lee/cyber/smartid/SmartIdService;->a(Lee/cyber/smartid/SmartIdService;Lee/cyber/smartid/manager/inter/GetAccountStatusManager;)Lee/cyber/smartid/manager/inter/GetAccountStatusManager;

    .line 24
    invoke-static {}, Lee/cyber/smartid/SmartIdService;->a()Lee/cyber/smartid/util/Log;

    move-result-object v5
    :try_end_12
    .catch Ljava/io/IOException; {:try_start_12 .. :try_end_12} :catch_10
    .catch Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException; {:try_start_12 .. :try_end_12} :catch_f
    .catch Ljava/lang/SecurityException; {:try_start_12 .. :try_end_12} :catch_e
    .catch Ljava/lang/Exception; {:try_start_12 .. :try_end_12} :catch_d
    .catch Ljava/lang/Throwable; {:try_start_12 .. :try_end_12} :catch_c
    .catchall {:try_start_12 .. :try_end_12} :catchall_0

    const-string v6, "initService - creating the AddAccountManager .."

    :try_start_13
    invoke-virtual {v5, v6}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    .line 25
    iget-object v5, p0, Lee/cyber/smartid/SmartIdService$1;->b:Lee/cyber/smartid/SmartIdService;

    iget-object v6, p0, Lee/cyber/smartid/SmartIdService$1;->b:Lee/cyber/smartid/SmartIdService;

    invoke-static {v6}, Lee/cyber/smartid/SmartIdService;->D(Lee/cyber/smartid/SmartIdService;)Lee/cyber/smartid/network/SmartIdAPI;

    move-result-object v6

    iget-object v7, p0, Lee/cyber/smartid/SmartIdService$1;->b:Lee/cyber/smartid/SmartIdService;

    invoke-static {v7}, Lee/cyber/smartid/SmartIdService;->d(Lee/cyber/smartid/SmartIdService;)Lee/cyber/smartid/tse/SmartIdTSE;

    move-result-object v7

    iget-object v8, p0, Lee/cyber/smartid/SmartIdService$1;->b:Lee/cyber/smartid/SmartIdService;

    invoke-static {v8}, Lee/cyber/smartid/SmartIdService;->n(Lee/cyber/smartid/SmartIdService;)Lee/cyber/smartid/inter/ServiceAccess;

    move-result-object v8

    iget-object v9, p0, Lee/cyber/smartid/SmartIdService$1;->b:Lee/cyber/smartid/SmartIdService;

    invoke-static {v9}, Lee/cyber/smartid/SmartIdService;->B(Lee/cyber/smartid/SmartIdService;)Lee/cyber/smartid/inter/ListenerAccess;

    move-result-object v9

    iget-object v10, p0, Lee/cyber/smartid/SmartIdService$1;->b:Lee/cyber/smartid/SmartIdService;

    invoke-static {v10}, Lee/cyber/smartid/SmartIdService;->E(Lee/cyber/smartid/SmartIdService;)Lee/cyber/smartid/tse/inter/WallClock;

    move-result-object v10

    invoke-static {v6, v7, v8, v9, v10}, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;->getInstance(Lee/cyber/smartid/network/SmartIdAPI;Lee/cyber/smartid/tse/SmartIdTSE;Lee/cyber/smartid/inter/ServiceAccess;Lee/cyber/smartid/inter/ListenerAccess;Lee/cyber/smartid/tse/inter/WallClock;)Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;

    move-result-object v6

    invoke-static {v5, v6}, Lee/cyber/smartid/SmartIdService;->a(Lee/cyber/smartid/SmartIdService;Lee/cyber/smartid/manager/inter/AddAccountManager;)Lee/cyber/smartid/manager/inter/AddAccountManager;

    .line 26
    invoke-static {}, Lee/cyber/smartid/SmartIdService;->a()Lee/cyber/smartid/util/Log;

    move-result-object v5
    :try_end_13
    .catch Ljava/io/IOException; {:try_start_13 .. :try_end_13} :catch_10
    .catch Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException; {:try_start_13 .. :try_end_13} :catch_f
    .catch Ljava/lang/SecurityException; {:try_start_13 .. :try_end_13} :catch_e
    .catch Ljava/lang/Exception; {:try_start_13 .. :try_end_13} :catch_d
    .catch Ljava/lang/Throwable; {:try_start_13 .. :try_end_13} :catch_c
    .catchall {:try_start_13 .. :try_end_13} :catchall_0

    const-string v6, "initService - creating the TransactionAndRPRequestManager .."

    :try_start_14
    invoke-virtual {v5, v6}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    .line 27
    iget-object v5, p0, Lee/cyber/smartid/SmartIdService$1;->b:Lee/cyber/smartid/SmartIdService;

    iget-object v6, p0, Lee/cyber/smartid/SmartIdService$1;->b:Lee/cyber/smartid/SmartIdService;

    invoke-static {v6}, Lee/cyber/smartid/SmartIdService;->D(Lee/cyber/smartid/SmartIdService;)Lee/cyber/smartid/network/SmartIdAPI;

    move-result-object v6

    iget-object v7, p0, Lee/cyber/smartid/SmartIdService$1;->b:Lee/cyber/smartid/SmartIdService;

    invoke-static {v7}, Lee/cyber/smartid/SmartIdService;->d(Lee/cyber/smartid/SmartIdService;)Lee/cyber/smartid/tse/SmartIdTSE;

    move-result-object v7

    iget-object v8, p0, Lee/cyber/smartid/SmartIdService$1;->b:Lee/cyber/smartid/SmartIdService;

    invoke-static {v8}, Lee/cyber/smartid/SmartIdService;->n(Lee/cyber/smartid/SmartIdService;)Lee/cyber/smartid/inter/ServiceAccess;

    move-result-object v8

    iget-object v9, p0, Lee/cyber/smartid/SmartIdService$1;->b:Lee/cyber/smartid/SmartIdService;

    invoke-static {v9}, Lee/cyber/smartid/SmartIdService;->B(Lee/cyber/smartid/SmartIdService;)Lee/cyber/smartid/inter/ListenerAccess;

    move-result-object v9

    iget-object v10, p0, Lee/cyber/smartid/SmartIdService$1;->b:Lee/cyber/smartid/SmartIdService;

    invoke-static {v10}, Lee/cyber/smartid/SmartIdService;->E(Lee/cyber/smartid/SmartIdService;)Lee/cyber/smartid/tse/inter/WallClock;

    move-result-object v10

    invoke-static {v6, v7, v8, v9, v10}, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;->getInstance(Lee/cyber/smartid/network/SmartIdAPI;Lee/cyber/smartid/tse/SmartIdTSE;Lee/cyber/smartid/inter/ServiceAccess;Lee/cyber/smartid/inter/ListenerAccess;Lee/cyber/smartid/tse/inter/WallClock;)Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;

    move-result-object v6

    invoke-static {v5, v6}, Lee/cyber/smartid/SmartIdService;->a(Lee/cyber/smartid/SmartIdService;Lee/cyber/smartid/manager/inter/TransactionAndRPRequestManager;)Lee/cyber/smartid/manager/inter/TransactionAndRPRequestManager;

    .line 28
    invoke-static {}, Lee/cyber/smartid/SmartIdService;->a()Lee/cyber/smartid/util/Log;

    move-result-object v5
    :try_end_14
    .catch Ljava/io/IOException; {:try_start_14 .. :try_end_14} :catch_10
    .catch Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException; {:try_start_14 .. :try_end_14} :catch_f
    .catch Ljava/lang/SecurityException; {:try_start_14 .. :try_end_14} :catch_e
    .catch Ljava/lang/Exception; {:try_start_14 .. :try_end_14} :catch_d
    .catch Ljava/lang/Throwable; {:try_start_14 .. :try_end_14} :catch_c
    .catchall {:try_start_14 .. :try_end_14} :catchall_0

    const-string v6, "initService - creating the VerificationCodeEvaluator .."

    :try_start_15
    invoke-virtual {v5, v6}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    .line 29
    iget-object v5, p0, Lee/cyber/smartid/SmartIdService$1;->b:Lee/cyber/smartid/SmartIdService;

    invoke-static {}, Lee/cyber/smartid/manager/impl/VerificationCodeEvaluatorImpl;->getInstance()Lee/cyber/smartid/manager/impl/VerificationCodeEvaluatorImpl;

    move-result-object v6

    invoke-static {v5, v6}, Lee/cyber/smartid/SmartIdService;->a(Lee/cyber/smartid/SmartIdService;Lee/cyber/smartid/manager/inter/VerificationCodeEvaluator;)Lee/cyber/smartid/manager/inter/VerificationCodeEvaluator;

    .line 30
    invoke-static {}, Lee/cyber/smartid/SmartIdService;->a()Lee/cyber/smartid/util/Log;

    move-result-object v5
    :try_end_15
    .catch Ljava/io/IOException; {:try_start_15 .. :try_end_15} :catch_10
    .catch Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException; {:try_start_15 .. :try_end_15} :catch_f
    .catch Ljava/lang/SecurityException; {:try_start_15 .. :try_end_15} :catch_e
    .catch Ljava/lang/Exception; {:try_start_15 .. :try_end_15} :catch_d
    .catch Ljava/lang/Throwable; {:try_start_15 .. :try_end_15} :catch_c
    .catchall {:try_start_15 .. :try_end_15} :catchall_0

    const-string v6, "initService - creating the VerificationCodeGenerator .."

    :try_start_16
    invoke-virtual {v5, v6}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    .line 31
    iget-object v5, p0, Lee/cyber/smartid/SmartIdService$1;->b:Lee/cyber/smartid/SmartIdService;

    iget-object v6, p0, Lee/cyber/smartid/SmartIdService$1;->b:Lee/cyber/smartid/SmartIdService;

    invoke-static {v6}, Lee/cyber/smartid/SmartIdService;->n(Lee/cyber/smartid/SmartIdService;)Lee/cyber/smartid/inter/ServiceAccess;

    move-result-object v6

    iget-object v7, p0, Lee/cyber/smartid/SmartIdService$1;->b:Lee/cyber/smartid/SmartIdService;

    invoke-static {v7}, Lee/cyber/smartid/SmartIdService;->d(Lee/cyber/smartid/SmartIdService;)Lee/cyber/smartid/tse/SmartIdTSE;

    move-result-object v7

    iget-object v8, p0, Lee/cyber/smartid/SmartIdService$1;->b:Lee/cyber/smartid/SmartIdService;

    invoke-static {v8}, Lee/cyber/smartid/SmartIdService;->C(Lee/cyber/smartid/SmartIdService;)Lee/cyber/smartid/cryptolib/CryptoLib;

    move-result-object v8

    invoke-static {v6, v7, v8}, Lee/cyber/smartid/manager/impl/VerificationCodeGeneratorImpl;->getInstance(Lee/cyber/smartid/inter/ServiceAccess;Lee/cyber/smartid/tse/SmartIdTSE;Lee/cyber/smartid/cryptolib/CryptoLib;)Lee/cyber/smartid/manager/impl/VerificationCodeGeneratorImpl;

    move-result-object v6

    invoke-static {v5, v6}, Lee/cyber/smartid/SmartIdService;->a(Lee/cyber/smartid/SmartIdService;Lee/cyber/smartid/manager/inter/VerificationCodeGenerator;)Lee/cyber/smartid/manager/inter/VerificationCodeGenerator;

    .line 32
    invoke-static {}, Lee/cyber/smartid/SmartIdService;->a()Lee/cyber/smartid/util/Log;

    move-result-object v5
    :try_end_16
    .catch Ljava/io/IOException; {:try_start_16 .. :try_end_16} :catch_10
    .catch Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException; {:try_start_16 .. :try_end_16} :catch_f
    .catch Ljava/lang/SecurityException; {:try_start_16 .. :try_end_16} :catch_e
    .catch Ljava/lang/Exception; {:try_start_16 .. :try_end_16} :catch_d
    .catch Ljava/lang/Throwable; {:try_start_16 .. :try_end_16} :catch_c
    .catchall {:try_start_16 .. :try_end_16} :catchall_0

    const-string v6, "initService - creating the TransactionRespMapper .."

    :try_start_17
    invoke-virtual {v5, v6}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    .line 33
    iget-object v5, p0, Lee/cyber/smartid/SmartIdService$1;->b:Lee/cyber/smartid/SmartIdService;

    iget-object v6, p0, Lee/cyber/smartid/SmartIdService$1;->b:Lee/cyber/smartid/SmartIdService;

    invoke-static {v6}, Lee/cyber/smartid/SmartIdService;->n(Lee/cyber/smartid/SmartIdService;)Lee/cyber/smartid/inter/ServiceAccess;

    move-result-object v6

    invoke-static {v6}, Lee/cyber/smartid/manager/impl/TransactionRespMapperImpl;->getInstance(Lee/cyber/smartid/inter/ServiceAccess;)Lee/cyber/smartid/manager/impl/TransactionRespMapperImpl;

    move-result-object v6

    invoke-static {v5, v6}, Lee/cyber/smartid/SmartIdService;->a(Lee/cyber/smartid/SmartIdService;Lee/cyber/smartid/manager/inter/TransactionRespMapper;)Lee/cyber/smartid/manager/inter/TransactionRespMapper;

    .line 34
    invoke-static {}, Lee/cyber/smartid/SmartIdService;->a()Lee/cyber/smartid/util/Log;

    move-result-object v5
    :try_end_17
    .catch Ljava/io/IOException; {:try_start_17 .. :try_end_17} :catch_10
    .catch Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException; {:try_start_17 .. :try_end_17} :catch_f
    .catch Ljava/lang/SecurityException; {:try_start_17 .. :try_end_17} :catch_e
    .catch Ljava/lang/Exception; {:try_start_17 .. :try_end_17} :catch_d
    .catch Ljava/lang/Throwable; {:try_start_17 .. :try_end_17} :catch_c
    .catchall {:try_start_17 .. :try_end_17} :catchall_0

    const-string v6, "initService - creating the SafetyNetManager .."

    :try_start_18
    invoke-virtual {v5, v6}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    .line 35
    iget-object v5, p0, Lee/cyber/smartid/SmartIdService$1;->b:Lee/cyber/smartid/SmartIdService;

    iget-object v6, p0, Lee/cyber/smartid/SmartIdService$1;->b:Lee/cyber/smartid/SmartIdService;

    invoke-static {v6}, Lee/cyber/smartid/SmartIdService;->n(Lee/cyber/smartid/SmartIdService;)Lee/cyber/smartid/inter/ServiceAccess;

    move-result-object v6

    iget-object v7, p0, Lee/cyber/smartid/SmartIdService$1;->b:Lee/cyber/smartid/SmartIdService;

    invoke-static {v7}, Lee/cyber/smartid/SmartIdService;->d(Lee/cyber/smartid/SmartIdService;)Lee/cyber/smartid/tse/SmartIdTSE;

    move-result-object v7

    iget-object v8, p0, Lee/cyber/smartid/SmartIdService$1;->b:Lee/cyber/smartid/SmartIdService;

    invoke-static {v8}, Lee/cyber/smartid/SmartIdService;->B(Lee/cyber/smartid/SmartIdService;)Lee/cyber/smartid/inter/ListenerAccess;

    move-result-object v8

    iget-object v9, p0, Lee/cyber/smartid/SmartIdService$1;->b:Lee/cyber/smartid/SmartIdService;

    invoke-static {v9}, Lee/cyber/smartid/SmartIdService;->E(Lee/cyber/smartid/SmartIdService;)Lee/cyber/smartid/tse/inter/WallClock;

    move-result-object v9

    iget-object v10, p0, Lee/cyber/smartid/SmartIdService$1;->b:Lee/cyber/smartid/SmartIdService;

    invoke-static {v10}, Lee/cyber/smartid/SmartIdService;->C(Lee/cyber/smartid/SmartIdService;)Lee/cyber/smartid/cryptolib/CryptoLib;

    move-result-object v10

    invoke-static {v6, v7, v8, v9, v10}, Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl;->getInstance(Lee/cyber/smartid/inter/ServiceAccess;Lee/cyber/smartid/tse/SmartIdTSE;Lee/cyber/smartid/inter/ListenerAccess;Lee/cyber/smartid/tse/inter/WallClock;Lee/cyber/smartid/cryptolib/CryptoLib;)Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl;

    move-result-object v6

    invoke-static {v5, v6}, Lee/cyber/smartid/SmartIdService;->a(Lee/cyber/smartid/SmartIdService;Lee/cyber/smartid/manager/inter/SafetyNetManager;)Lee/cyber/smartid/manager/inter/SafetyNetManager;

    .line 36
    invoke-static {}, Lee/cyber/smartid/SmartIdService;->a()Lee/cyber/smartid/util/Log;

    move-result-object v5
    :try_end_18
    .catch Ljava/io/IOException; {:try_start_18 .. :try_end_18} :catch_10
    .catch Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException; {:try_start_18 .. :try_end_18} :catch_f
    .catch Ljava/lang/SecurityException; {:try_start_18 .. :try_end_18} :catch_e
    .catch Ljava/lang/Exception; {:try_start_18 .. :try_end_18} :catch_d
    .catch Ljava/lang/Throwable; {:try_start_18 .. :try_end_18} :catch_c
    .catchall {:try_start_18 .. :try_end_18} :catchall_0

    const-string v6, "initService - creating the PostInitManager .."

    :try_start_19
    invoke-virtual {v5, v6}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    .line 37
    iget-object v5, p0, Lee/cyber/smartid/SmartIdService$1;->b:Lee/cyber/smartid/SmartIdService;

    iget-object v6, p0, Lee/cyber/smartid/SmartIdService$1;->b:Lee/cyber/smartid/SmartIdService;

    invoke-static {v6}, Lee/cyber/smartid/SmartIdService;->n(Lee/cyber/smartid/SmartIdService;)Lee/cyber/smartid/inter/ServiceAccess;

    move-result-object v6

    invoke-static {v6}, Lee/cyber/smartid/manager/impl/PostInitManagerImpl;->getInstance(Lee/cyber/smartid/inter/ServiceAccess;)Lee/cyber/smartid/manager/impl/PostInitManagerImpl;

    move-result-object v6

    invoke-static {v5, v6}, Lee/cyber/smartid/SmartIdService;->a(Lee/cyber/smartid/SmartIdService;Lee/cyber/smartid/manager/inter/PostInitManager;)Lee/cyber/smartid/manager/inter/PostInitManager;

    .line 38
    invoke-virtual {v4}, Lee/cyber/smartid/dto/upgrade/InteractiveUpgradeState;->isUpgradePending()Z

    move-result v5

    if-nez v5, :cond_1

    .line 39
    iget-object v5, p0, Lee/cyber/smartid/SmartIdService$1;->b:Lee/cyber/smartid/SmartIdService;

    const/4 v6, 0x1

    invoke-static {v5, v6}, Lee/cyber/smartid/SmartIdService;->a(Lee/cyber/smartid/SmartIdService;Z)V

    .line 40
    invoke-static {}, Lee/cyber/smartid/SmartIdService;->a()Lee/cyber/smartid/util/Log;

    move-result-object v5
    :try_end_19
    .catch Ljava/io/IOException; {:try_start_19 .. :try_end_19} :catch_10
    .catch Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException; {:try_start_19 .. :try_end_19} :catch_f
    .catch Ljava/lang/SecurityException; {:try_start_19 .. :try_end_19} :catch_e
    .catch Ljava/lang/Exception; {:try_start_19 .. :try_end_19} :catch_d
    .catch Ljava/lang/Throwable; {:try_start_19 .. :try_end_19} :catch_c
    .catchall {:try_start_19 .. :try_end_19} :catchall_0

    const-string v6, "initService - done"

    :try_start_1a
    invoke-virtual {v5, v6}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V
    :try_end_1a
    .catch Ljava/io/IOException; {:try_start_1a .. :try_end_1a} :catch_10
    .catch Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException; {:try_start_1a .. :try_end_1a} :catch_f
    .catch Ljava/lang/SecurityException; {:try_start_1a .. :try_end_1a} :catch_e
    .catch Ljava/lang/Exception; {:try_start_1a .. :try_end_1a} :catch_d
    .catch Ljava/lang/Throwable; {:try_start_1a .. :try_end_1a} :catch_c
    .catchall {:try_start_1a .. :try_end_1a} :catchall_0

    .line 41
    :try_start_1b
    iget-object v5, p0, Lee/cyber/smartid/SmartIdService$1;->b:Lee/cyber/smartid/SmartIdService;

    invoke-static {v5}, Lee/cyber/smartid/SmartIdService;->v(Lee/cyber/smartid/SmartIdService;)Lee/cyber/smartid/manager/inter/PostInitManager;

    move-result-object v5

    invoke-interface {v5}, Lee/cyber/smartid/manager/inter/PostInitManager;->executePostInitServiceTasks()V
    :try_end_1b
    .catch Ljava/lang/Error; {:try_start_1b .. :try_end_1b} :catch_0
    .catch Ljava/io/IOException; {:try_start_1b .. :try_end_1b} :catch_10
    .catch Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException; {:try_start_1b .. :try_end_1b} :catch_f
    .catch Ljava/lang/SecurityException; {:try_start_1b .. :try_end_1b} :catch_e
    .catch Ljava/lang/Exception; {:try_start_1b .. :try_end_1b} :catch_d
    .catch Ljava/lang/Throwable; {:try_start_1b .. :try_end_1b} :catch_c
    .catchall {:try_start_1b .. :try_end_1b} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v5

    .line 42
    :try_start_1c
    invoke-static {}, Lee/cyber/smartid/SmartIdService;->a()Lee/cyber/smartid/util/Log;

    move-result-object v6
    :try_end_1c
    .catch Ljava/io/IOException; {:try_start_1c .. :try_end_1c} :catch_10
    .catch Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException; {:try_start_1c .. :try_end_1c} :catch_f
    .catch Ljava/lang/SecurityException; {:try_start_1c .. :try_end_1c} :catch_e
    .catch Ljava/lang/Exception; {:try_start_1c .. :try_end_1c} :catch_d
    .catch Ljava/lang/Throwable; {:try_start_1c .. :try_end_1c} :catch_c
    .catchall {:try_start_1c .. :try_end_1c} :catchall_0

    const-string v7, "initService - executePostInitServiceTasks"

    :try_start_1d
    invoke-virtual {v6, v7, v5}, Lee/cyber/smartid/util/Log;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 43
    :cond_1
    invoke-static {}, Lee/cyber/smartid/SmartIdService;->a()Lee/cyber/smartid/util/Log;

    move-result-object v5
    :try_end_1d
    .catch Ljava/io/IOException; {:try_start_1d .. :try_end_1d} :catch_10
    .catch Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException; {:try_start_1d .. :try_end_1d} :catch_f
    .catch Ljava/lang/SecurityException; {:try_start_1d .. :try_end_1d} :catch_e
    .catch Ljava/lang/Exception; {:try_start_1d .. :try_end_1d} :catch_d
    .catch Ljava/lang/Throwable; {:try_start_1d .. :try_end_1d} :catch_c
    .catchall {:try_start_1d .. :try_end_1d} :catchall_0

    const-string v6, "initService - interactive upgrade required .."

    :try_start_1e
    invoke-virtual {v5, v6}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    .line 44
    iget-object v5, p0, Lee/cyber/smartid/SmartIdService$1;->b:Lee/cyber/smartid/SmartIdService;

    invoke-static {v5}, Lee/cyber/smartid/SmartIdService;->f(Lee/cyber/smartid/SmartIdService;)Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;

    move-result-object v5

    iget-object v6, p0, Lee/cyber/smartid/SmartIdService$1;->b:Lee/cyber/smartid/SmartIdService;

    invoke-static {v6}, Lee/cyber/smartid/SmartIdService;->D(Lee/cyber/smartid/SmartIdService;)Lee/cyber/smartid/network/SmartIdAPI;

    move-result-object v6

    invoke-virtual {v5, v6}, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->startInteractiveUpgrade(Lee/cyber/smartid/network/SmartIdAPI;)V
    :try_end_1e
    .catch Ljava/io/IOException; {:try_start_1e .. :try_end_1e} :catch_10
    .catch Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException; {:try_start_1e .. :try_end_1e} :catch_f
    .catch Ljava/lang/SecurityException; {:try_start_1e .. :try_end_1e} :catch_e
    .catch Ljava/lang/Exception; {:try_start_1e .. :try_end_1e} :catch_d
    .catch Ljava/lang/Throwable; {:try_start_1e .. :try_end_1e} :catch_c
    .catchall {:try_start_1e .. :try_end_1e} :catchall_0

    :goto_0
    move-wide v5, v1

    move-object v1, v3

    goto :goto_2

    :catch_1
    move-exception v1

    goto :goto_1

    :catch_2
    move-exception v1

    goto :goto_1

    :catch_3
    move-exception v1

    goto :goto_1

    :catch_4
    move-exception v1

    goto :goto_1

    :catch_5
    move-exception v1

    :goto_1
    move-wide v11, v5

    move-object v5, v1

    move-wide v1, v11

    goto :goto_3

    :catch_6
    move-exception v1

    .line 45
    :try_start_1f
    invoke-static {}, Lee/cyber/smartid/SmartIdService;->a()Lee/cyber/smartid/util/Log;

    move-result-object v2
    :try_end_1f
    .catch Ljava/io/IOException; {:try_start_1f .. :try_end_1f} :catch_5
    .catch Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException; {:try_start_1f .. :try_end_1f} :catch_4
    .catch Ljava/lang/SecurityException; {:try_start_1f .. :try_end_1f} :catch_3
    .catch Ljava/lang/Exception; {:try_start_1f .. :try_end_1f} :catch_2
    .catch Ljava/lang/Throwable; {:try_start_1f .. :try_end_1f} :catch_1
    .catchall {:try_start_1f .. :try_end_1f} :catchall_0

    const-string v7, "initService - initializing TSE"

    :try_start_20
    invoke-virtual {v2, v7, v1}, Lee/cyber/smartid/util/Log;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 46
    invoke-virtual {v1}, Lee/cyber/smartid/tse/dto/InitTSEException;->getErrorCode()J

    move-result-wide v5
    :try_end_20
    .catch Ljava/io/IOException; {:try_start_20 .. :try_end_20} :catch_5
    .catch Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException; {:try_start_20 .. :try_end_20} :catch_4
    .catch Ljava/lang/SecurityException; {:try_start_20 .. :try_end_20} :catch_3
    .catch Ljava/lang/Exception; {:try_start_20 .. :try_end_20} :catch_2
    .catch Ljava/lang/Throwable; {:try_start_20 .. :try_end_20} :catch_1
    .catchall {:try_start_20 .. :try_end_20} :catchall_0

    .line 47
    :try_start_21
    throw v1
    :try_end_21
    .catch Ljava/io/IOException; {:try_start_21 .. :try_end_21} :catch_b
    .catch Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException; {:try_start_21 .. :try_end_21} :catch_a
    .catch Ljava/lang/SecurityException; {:try_start_21 .. :try_end_21} :catch_9
    .catch Ljava/lang/Exception; {:try_start_21 .. :try_end_21} :catch_8
    .catch Ljava/lang/Throwable; {:try_start_21 .. :try_end_21} :catch_7
    .catchall {:try_start_21 .. :try_end_21} :catchall_0

    :catch_7
    move-exception v1

    goto :goto_2

    :catch_8
    move-exception v1

    goto :goto_2

    :catch_9
    move-exception v1

    goto :goto_2

    :catch_a
    move-exception v1

    goto :goto_2

    :catch_b
    move-exception v1

    :goto_2
    move-object v2, v4

    goto :goto_6

    :catch_c
    move-exception v5

    goto :goto_3

    :catch_d
    move-exception v5

    goto :goto_3

    :catch_e
    move-exception v5

    goto :goto_3

    :catch_f
    move-exception v5

    goto :goto_3

    :catch_10
    move-exception v5

    :goto_3
    move-wide v11, v1

    move-object v2, v4

    move-object v1, v5

    move-wide v5, v11

    goto :goto_6

    :catch_11
    move-exception v1

    goto :goto_4

    :catch_12
    move-exception v1

    goto :goto_4

    :catch_13
    move-exception v1

    goto :goto_4

    :catch_14
    move-exception v1

    goto :goto_4

    :catch_15
    move-exception v1

    :goto_4
    move-wide v11, v4

    move-object v4, v1

    move-wide v1, v11

    goto :goto_5

    :catch_16
    move-exception v4

    goto :goto_5

    :catch_17
    move-exception v4

    goto :goto_5

    :catch_18
    move-exception v4

    goto :goto_5

    :catch_19
    move-exception v4

    goto :goto_5

    :catch_1a
    move-exception v4

    :goto_5
    move-wide v5, v1

    move-object v2, v3

    move-object v1, v4

    :goto_6
    if-eqz v1, :cond_2

    .line 48
    :try_start_22
    invoke-static {}, Lee/cyber/smartid/SmartIdService;->a()Lee/cyber/smartid/util/Log;

    move-result-object v4

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V
    :try_end_22
    .catchall {:try_start_22 .. :try_end_22} :catchall_0

    const-string v8, "initService - errorCode: "

    :try_start_23
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7, v1}, Lee/cyber/smartid/util/Log;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_2
    const/4 v9, 0x0

    if-eqz v1, :cond_3

    .line 49
    iget-object v4, p0, Lee/cyber/smartid/SmartIdService$1;->b:Lee/cyber/smartid/SmartIdService;

    invoke-static {v4}, Lee/cyber/smartid/SmartIdService;->E(Lee/cyber/smartid/SmartIdService;)Lee/cyber/smartid/tse/inter/WallClock;

    move-result-object v4

    invoke-virtual {v1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    invoke-static/range {v4 .. v9}, Lee/cyber/smartid/dto/SmartIdError;->from(Lee/cyber/smartid/tse/inter/WallClock;JLjava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Lee/cyber/smartid/dto/SmartIdError;

    move-result-object v1

    goto :goto_7

    :cond_3
    move-object v1, v3

    .line 50
    :goto_7
    iget-object v4, p0, Lee/cyber/smartid/SmartIdService$1;->b:Lee/cyber/smartid/SmartIdService;

    new-instance v5, Lee/cyber/smartid/SmartIdService$1$3;

    invoke-direct {v5, p0, v1, v2, v3}, Lee/cyber/smartid/SmartIdService$1$3;-><init>(Lee/cyber/smartid/SmartIdService$1;Lee/cyber/smartid/dto/SmartIdError;Lee/cyber/smartid/dto/upgrade/InteractiveUpgradeState;[Lee/cyber/smartid/dto/SmartIdError;)V

    invoke-static {v4, v5}, Lee/cyber/smartid/SmartIdService;->a(Lee/cyber/smartid/SmartIdService;Ljava/lang/Runnable;)V

    .line 51
    invoke-static {}, Lee/cyber/smartid/SmartIdService;->a()Lee/cyber/smartid/util/Log;

    move-result-object v1
    :try_end_23
    .catchall {:try_start_23 .. :try_end_23} :catchall_0

    const-string v2, "initService - thread end"

    :try_start_24
    invoke-virtual {v1, v2}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    .line 52
    monitor-exit v0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_24
    .catchall {:try_start_24 .. :try_end_24} :catchall_0

    throw v1
.end method
