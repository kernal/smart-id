.class Lee/cyber/smartid/SmartIdService$17;
.super Ljava/lang/Object;
.source "SmartIdService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lee/cyber/smartid/SmartIdService;->checkLocalPendingStateForAccountRegistration(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/inter/CheckLocalPendingStateListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:Ljava/lang/String;

.field final synthetic c:Ljava/lang/String;

.field final synthetic d:Lee/cyber/smartid/SmartIdService;


# direct methods
.method constructor <init>(Lee/cyber/smartid/SmartIdService;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lee/cyber/smartid/SmartIdService$17;->d:Lee/cyber/smartid/SmartIdService;

    iput-object p2, p0, Lee/cyber/smartid/SmartIdService$17;->a:Ljava/lang/String;

    iput-object p3, p0, Lee/cyber/smartid/SmartIdService$17;->b:Ljava/lang/String;

    iput-object p4, p0, Lee/cyber/smartid/SmartIdService$17;->c:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/SmartIdService$17;->a:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lee/cyber/smartid/SmartIdService$17;->b:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto/16 :goto_2

    .line 2
    :cond_0
    iget-object v0, p0, Lee/cyber/smartid/SmartIdService$17;->d:Lee/cyber/smartid/SmartIdService;

    invoke-static {v0}, Lee/cyber/smartid/SmartIdService;->r(Lee/cyber/smartid/SmartIdService;)Lee/cyber/smartid/manager/inter/StoredAccountManager;

    move-result-object v0

    invoke-interface {v0}, Lee/cyber/smartid/manager/inter/StoredAccountManager;->loadAccountsFromStorage()Ljava/util/ArrayList;

    move-result-object v0

    const/4 v1, 0x0

    .line 3
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_2

    .line 4
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    :goto_0
    if-ltz v2, :cond_2

    .line 5
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lee/cyber/smartid/dto/AccountState;

    invoke-virtual {v3}, Lee/cyber/smartid/dto/AccountState;->getAuthKeyReference()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lee/cyber/smartid/SmartIdService$17;->a:Ljava/lang/String;

    invoke-static {v3, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lee/cyber/smartid/dto/AccountState;

    invoke-virtual {v3}, Lee/cyber/smartid/dto/AccountState;->getSignKeyReference()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lee/cyber/smartid/SmartIdService$17;->b:Ljava/lang/String;

    invoke-static {v3, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 6
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lee/cyber/smartid/dto/AccountState;

    goto :goto_1

    :cond_1
    add-int/lit8 v2, v2, -0x1

    goto :goto_0

    :cond_2
    :goto_1
    if-nez v1, :cond_3

    .line 7
    iget-object v0, p0, Lee/cyber/smartid/SmartIdService$17;->d:Lee/cyber/smartid/SmartIdService;

    new-instance v1, Lee/cyber/smartid/SmartIdService$17$2;

    invoke-direct {v1, p0}, Lee/cyber/smartid/SmartIdService$17$2;-><init>(Lee/cyber/smartid/SmartIdService$17;)V

    invoke-static {v0, v1}, Lee/cyber/smartid/SmartIdService;->a(Lee/cyber/smartid/SmartIdService;Ljava/lang/Runnable;)V

    return-void

    .line 8
    :cond_3
    new-instance v0, Lee/cyber/smartid/dto/jsonrpc/resp/CheckLocalPendingStateResp;

    iget-object v1, p0, Lee/cyber/smartid/SmartIdService$17;->c:Ljava/lang/String;

    iget-object v2, p0, Lee/cyber/smartid/SmartIdService$17;->d:Lee/cyber/smartid/SmartIdService;

    iget-object v3, p0, Lee/cyber/smartid/SmartIdService$17;->a:Ljava/lang/String;

    iget-object v4, p0, Lee/cyber/smartid/SmartIdService$17;->b:Ljava/lang/String;

    const-string v5, "add_account"

    invoke-static {v2, v5, v3, v4}, Lee/cyber/smartid/SmartIdService;->a(Lee/cyber/smartid/SmartIdService;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "addAccount"

    invoke-direct {v0, v1, v2, v3}, Lee/cyber/smartid/dto/jsonrpc/resp/CheckLocalPendingStateResp;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 9
    iget-object v1, p0, Lee/cyber/smartid/SmartIdService$17;->d:Lee/cyber/smartid/SmartIdService;

    new-instance v2, Lee/cyber/smartid/SmartIdService$17$3;

    invoke-direct {v2, p0, v0}, Lee/cyber/smartid/SmartIdService$17$3;-><init>(Lee/cyber/smartid/SmartIdService$17;Lee/cyber/smartid/dto/jsonrpc/resp/CheckLocalPendingStateResp;)V

    invoke-static {v1, v2}, Lee/cyber/smartid/SmartIdService;->a(Lee/cyber/smartid/SmartIdService;Ljava/lang/Runnable;)V

    return-void

    .line 10
    :cond_4
    :goto_2
    iget-object v0, p0, Lee/cyber/smartid/SmartIdService$17;->d:Lee/cyber/smartid/SmartIdService;

    new-instance v1, Lee/cyber/smartid/SmartIdService$17$1;

    invoke-direct {v1, p0}, Lee/cyber/smartid/SmartIdService$17$1;-><init>(Lee/cyber/smartid/SmartIdService$17;)V

    invoke-static {v0, v1}, Lee/cyber/smartid/SmartIdService;->a(Lee/cyber/smartid/SmartIdService;Ljava/lang/Runnable;)V

    return-void
.end method
