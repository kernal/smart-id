.class Lee/cyber/smartid/SmartIdService$18;
.super Ljava/lang/Object;
.source "SmartIdService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lee/cyber/smartid/SmartIdService;->checkLocalPendingStateForKey(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/inter/CheckLocalPendingStateListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lee/cyber/smartid/inter/CheckLocalPendingStateListener;

.field final synthetic b:Ljava/lang/String;

.field final synthetic c:Ljava/lang/String;

.field final synthetic d:Lee/cyber/smartid/SmartIdService;


# direct methods
.method constructor <init>(Lee/cyber/smartid/SmartIdService;Lee/cyber/smartid/inter/CheckLocalPendingStateListener;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lee/cyber/smartid/SmartIdService$18;->d:Lee/cyber/smartid/SmartIdService;

    iput-object p2, p0, Lee/cyber/smartid/SmartIdService$18;->a:Lee/cyber/smartid/inter/CheckLocalPendingStateListener;

    iput-object p3, p0, Lee/cyber/smartid/SmartIdService$18;->b:Ljava/lang/String;

    iput-object p4, p0, Lee/cyber/smartid/SmartIdService$18;->c:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 10

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/SmartIdService$18;->a:Lee/cyber/smartid/inter/CheckLocalPendingStateListener;

    if-eqz v0, :cond_0

    .line 2
    iget-object v1, p0, Lee/cyber/smartid/SmartIdService$18;->b:Ljava/lang/String;

    iget-object v2, p0, Lee/cyber/smartid/SmartIdService$18;->d:Lee/cyber/smartid/SmartIdService;

    invoke-static {v2}, Lee/cyber/smartid/SmartIdService;->b(Lee/cyber/smartid/SmartIdService;)Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lee/cyber/smartid/SmartIdService$18;->d:Lee/cyber/smartid/SmartIdService;

    invoke-static {v3}, Lee/cyber/smartid/SmartIdService;->E(Lee/cyber/smartid/SmartIdService;)Lee/cyber/smartid/tse/inter/WallClock;

    move-result-object v3

    new-instance v4, Lee/cyber/smartid/dto/UnknownKeyTypeException;

    iget-object v5, p0, Lee/cyber/smartid/SmartIdService$18;->d:Lee/cyber/smartid/SmartIdService;

    invoke-static {v5}, Lee/cyber/smartid/SmartIdService;->b(Lee/cyber/smartid/SmartIdService;)Landroid/content/Context;

    move-result-object v5

    sget v6, Lee/cyber/smartid/R$string;->err_unknown_key_type:I

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    iget-object v8, p0, Lee/cyber/smartid/SmartIdService$18;->c:Ljava/lang/String;

    const/4 v9, 0x0

    aput-object v8, v7, v9

    invoke-virtual {v5, v6, v7}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Lee/cyber/smartid/dto/UnknownKeyTypeException;-><init>(Ljava/lang/String;)V

    invoke-static {v2, v3, v4}, Lee/cyber/smartid/dto/SmartIdError;->from(Landroid/content/Context;Lee/cyber/smartid/tse/inter/WallClock;Ljava/lang/Throwable;)Lee/cyber/smartid/dto/SmartIdError;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lee/cyber/smartid/inter/CheckLocalPendingStateListener;->onCheckLocalPendingStateFailed(Ljava/lang/String;Lee/cyber/smartid/dto/SmartIdError;)V

    :cond_0
    return-void
.end method
