.class Lee/cyber/smartid/SmartIdService$SmartIdServiceAlarmAccess;
.super Ljava/lang/Object;
.source "SmartIdService.java"

# interfaces
.implements Lee/cyber/smartid/tse/inter/AlarmAccess;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lee/cyber/smartid/SmartIdService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SmartIdServiceAlarmAccess"
.end annotation


# instance fields
.field final synthetic a:Lee/cyber/smartid/SmartIdService;


# direct methods
.method private constructor <init>(Lee/cyber/smartid/SmartIdService;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lee/cyber/smartid/SmartIdService$SmartIdServiceAlarmAccess;->a:Lee/cyber/smartid/SmartIdService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lee/cyber/smartid/SmartIdService;Lee/cyber/smartid/SmartIdService$1;)V
    .locals 0

    .line 2
    invoke-direct {p0, p1}, Lee/cyber/smartid/SmartIdService$SmartIdServiceAlarmAccess;-><init>(Lee/cyber/smartid/SmartIdService;)V

    return-void
.end method


# virtual methods
.method public addAlarmHandler(Lee/cyber/smartid/tse/inter/AlarmAccess$AlarmHandler;)V
    .locals 2

    if-nez p1, :cond_0

    return-void

    .line 1
    :cond_0
    iget-object v0, p0, Lee/cyber/smartid/SmartIdService$SmartIdServiceAlarmAccess;->a:Lee/cyber/smartid/SmartIdService;

    invoke-static {v0}, Lee/cyber/smartid/SmartIdService;->z(Lee/cyber/smartid/SmartIdService;)Ljava/util/ArrayList;

    move-result-object v0

    monitor-enter v0

    .line 2
    :try_start_0
    iget-object v1, p0, Lee/cyber/smartid/SmartIdService$SmartIdServiceAlarmAccess;->a:Lee/cyber/smartid/SmartIdService;

    invoke-static {v1}, Lee/cyber/smartid/SmartIdService;->z(Lee/cyber/smartid/SmartIdService;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 3
    iget-object v1, p0, Lee/cyber/smartid/SmartIdService$SmartIdServiceAlarmAccess;->a:Lee/cyber/smartid/SmartIdService;

    invoke-static {v1}, Lee/cyber/smartid/SmartIdService;->z(Lee/cyber/smartid/SmartIdService;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 4
    :cond_1
    monitor-exit v0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method public clearAlarmFor(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/SmartIdService$SmartIdServiceAlarmAccess;->a:Lee/cyber/smartid/SmartIdService;

    invoke-static {v0}, Lee/cyber/smartid/SmartIdService;->b(Lee/cyber/smartid/SmartIdService;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p1, p2}, Lee/cyber/smartid/SmartIdService;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public removeAlarmHandler(Lee/cyber/smartid/tse/inter/AlarmAccess$AlarmHandler;)V
    .locals 2

    if-nez p1, :cond_0

    return-void

    .line 1
    :cond_0
    iget-object v0, p0, Lee/cyber/smartid/SmartIdService$SmartIdServiceAlarmAccess;->a:Lee/cyber/smartid/SmartIdService;

    invoke-static {v0}, Lee/cyber/smartid/SmartIdService;->z(Lee/cyber/smartid/SmartIdService;)Ljava/util/ArrayList;

    move-result-object v0

    monitor-enter v0

    .line 2
    :try_start_0
    iget-object v1, p0, Lee/cyber/smartid/SmartIdService$SmartIdServiceAlarmAccess;->a:Lee/cyber/smartid/SmartIdService;

    invoke-static {v1}, Lee/cyber/smartid/SmartIdService;->z(Lee/cyber/smartid/SmartIdService;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 3
    monitor-exit v0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method public scheduleAlarmFor(Ljava/lang/String;JLjava/lang/String;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/SmartIdService$SmartIdServiceAlarmAccess;->a:Lee/cyber/smartid/SmartIdService;

    invoke-static {v0}, Lee/cyber/smartid/SmartIdService;->b(Lee/cyber/smartid/SmartIdService;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p1, p2, p3, p4}, Lee/cyber/smartid/SmartIdService;->a(Landroid/content/Context;Ljava/lang/String;JLjava/lang/String;)V

    return-void
.end method
