.class public interface abstract Lee/cyber/smartid/network/SmartIdAPI;
.super Ljava/lang/Object;
.source "SmartIdAPI.java"


# static fields
.field public static final METHOD_CANCEL_RP_REQUEST:Ljava/lang/String; = "cancelRpRequest"

.field public static final METHOD_CANCEL_TRANSACTION:Ljava/lang/String; = "cancelTransaction"

.field public static final METHOD_CONFIRM_RP_REQUEST:Ljava/lang/String; = "confirmRpRequest"

.field public static final METHOD_CREATE_TRANSACTION_FOR_RP_REQUEST:Ljava/lang/String; = "createTransactionForRpRequest"

.field public static final METHOD_DELETE_ACCOUNT:Ljava/lang/String; = "deleteAccount"

.field public static final METHOD_GET_ACCOUNT_STATUS:Ljava/lang/String; = "getAccountStatus"

.field public static final METHOD_GET_PENDING_OPERATION:Ljava/lang/String; = "getPendingOperation"

.field public static final METHOD_GET_REG_TOKEN_NONCE:Ljava/lang/String; = "getRegTokenNonce"

.field public static final METHOD_GET_RP_REQUEST:Ljava/lang/String; = "getRpRequest"

.field public static final METHOD_GET_TRANSACTION:Ljava/lang/String; = "getTransaction"

.field public static final METHOD_REGISTER_ACCOUNT:Ljava/lang/String; = "registerAccount"

.field public static final METHOD_REGISTER_DEVICE:Ljava/lang/String; = "registerDevice"

.field public static final METHOD_UPDATE_DEVICE:Ljava/lang/String; = "updateDevice"

.field public static final METHOD_UPGRADE_KEY_PAIR_REQUEST:Ljava/lang/String; = "upgradeKeyPair"


# virtual methods
.method public abstract cancelRPRequest(Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCRequest;)Lc/b;
    .param p1    # Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCRequest;
        .annotation runtime Lc/b/a;
        .end annotation
    .end param
    .annotation runtime Lc/b/k;
        a = {
            "Content-Type: application/json-rpc",
            "Accept-Charset: utf-8"
        }
    .end annotation

    .annotation runtime Lc/b/o;
        a = "v2/protected"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCRequest;",
            ")",
            "Lc/b<",
            "Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCResponse<",
            "Lee/cyber/smartid/dto/jsonrpc/result/CancelRPRequestResult;",
            ">;>;"
        }
    .end annotation
.end method

.method public abstract cancelTransaction(Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCRequest;)Lc/b;
    .param p1    # Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCRequest;
        .annotation runtime Lc/b/a;
        .end annotation
    .end param
    .annotation runtime Lc/b/k;
        a = {
            "Content-Type: application/json-rpc",
            "Accept-Charset: utf-8"
        }
    .end annotation

    .annotation runtime Lc/b/o;
        a = "v2/protected"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCRequest;",
            ")",
            "Lc/b<",
            "Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCResponse<",
            "Lee/cyber/smartid/dto/jsonrpc/result/CancelTransactionResult;",
            ">;>;"
        }
    .end annotation
.end method

.method public abstract confirmRPRequest(Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCRequest;)Lc/b;
    .param p1    # Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCRequest;
        .annotation runtime Lc/b/a;
        .end annotation
    .end param
    .annotation runtime Lc/b/k;
        a = {
            "Content-Type: application/json-rpc",
            "Accept-Charset: utf-8"
        }
    .end annotation

    .annotation runtime Lc/b/o;
        a = "v2/protected"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCRequest;",
            ")",
            "Lc/b<",
            "Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCResponse<",
            "Lee/cyber/smartid/dto/jsonrpc/result/ConfirmRPRequestResult;",
            ">;>;"
        }
    .end annotation
.end method

.method public abstract createTransactionForRpRequest(Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCRequest;)Lc/b;
    .param p1    # Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCRequest;
        .annotation runtime Lc/b/a;
        .end annotation
    .end param
    .annotation runtime Lc/b/k;
        a = {
            "Content-Type: application/json-rpc",
            "Accept-Charset: utf-8"
        }
    .end annotation

    .annotation runtime Lc/b/o;
        a = "v2/protected"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCRequest;",
            ")",
            "Lc/b<",
            "Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCResponse<",
            "Lee/cyber/smartid/dto/jsonrpc/result/TransactionContainerResult;",
            ">;>;"
        }
    .end annotation
.end method

.method public abstract deleteAccount(Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCRequest;)Lc/b;
    .param p1    # Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCRequest;
        .annotation runtime Lc/b/a;
        .end annotation
    .end param
    .annotation runtime Lc/b/k;
        a = {
            "Content-Type: application/json-rpc",
            "Accept-Charset: utf-8"
        }
    .end annotation

    .annotation runtime Lc/b/o;
        a = "v2/protected"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCRequest;",
            ")",
            "Lc/b<",
            "Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCResponse<",
            "Lee/cyber/smartid/dto/jsonrpc/result/DeleteAccountResult;",
            ">;>;"
        }
    .end annotation
.end method

.method public abstract getAccountStatus(Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCRequest;)Lc/b;
    .param p1    # Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCRequest;
        .annotation runtime Lc/b/a;
        .end annotation
    .end param
    .annotation runtime Lc/b/k;
        a = {
            "Content-Type: application/json-rpc",
            "Accept-Charset: utf-8"
        }
    .end annotation

    .annotation runtime Lc/b/o;
        a = "v2/protected"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCRequest;",
            ")",
            "Lc/b<",
            "Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCResponse<",
            "Lee/cyber/smartid/tse/dto/ResultWithRawJson<",
            "Lee/cyber/smartid/dto/jsonrpc/result/GetAccountStatusResult;",
            ">;>;>;"
        }
    .end annotation
.end method

.method public abstract getLastTransaction(Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCRequest;)Lc/b;
    .param p1    # Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCRequest;
        .annotation runtime Lc/b/a;
        .end annotation
    .end param
    .annotation runtime Lc/b/k;
        a = {
            "Content-Type: application/json-rpc",
            "Accept-Charset: utf-8"
        }
    .end annotation

    .annotation runtime Lc/b/o;
        a = "v2/protected"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCRequest;",
            ")",
            "Lc/b<",
            "Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCResponse<",
            "Lee/cyber/smartid/dto/jsonrpc/result/GetPendingOperationResult;",
            ">;>;"
        }
    .end annotation
.end method

.method public abstract getNewFreshnessToken(Ljava/lang/String;Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCRequest;)Lc/b;
    .param p1    # Ljava/lang/String;
        .annotation runtime Lc/b/i;
            a = "X-SplitKey-Trigger"
        .end annotation
    .end param
    .param p2    # Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCRequest;
        .annotation runtime Lc/b/a;
        .end annotation
    .end param
    .annotation runtime Lc/b/k;
        a = {
            "Content-Type: application/json-rpc",
            "Accept-Charset: utf-8"
        }
    .end annotation

    .annotation runtime Lc/b/o;
        a = "v2/protected"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCRequest;",
            ")",
            "Lc/b<",
            "Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCResponse<",
            "Lee/cyber/smartid/tse/dto/jsonrpc/result/NewFreshnessTokenResult;",
            ">;>;"
        }
    .end annotation
.end method

.method public abstract getRPRequest(Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCRequest;)Lc/b;
    .param p1    # Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCRequest;
        .annotation runtime Lc/b/a;
        .end annotation
    .end param
    .annotation runtime Lc/b/k;
        a = {
            "Content-Type: application/json-rpc",
            "Accept-Charset: utf-8"
        }
    .end annotation

    .annotation runtime Lc/b/o;
        a = "v2/protected"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCRequest;",
            ")",
            "Lc/b<",
            "Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCResponse<",
            "Lee/cyber/smartid/dto/jsonrpc/result/GetRPRequestResult;",
            ">;>;"
        }
    .end annotation
.end method

.method public abstract getRegTokenNonce(Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCRequest;)Lc/b;
    .param p1    # Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCRequest;
        .annotation runtime Lc/b/a;
        .end annotation
    .end param
    .annotation runtime Lc/b/k;
        a = {
            "Content-Type: application/json-rpc",
            "Accept-Charset: utf-8"
        }
    .end annotation

    .annotation runtime Lc/b/o;
        a = "v2/public"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCRequest;",
            ")",
            "Lc/b<",
            "Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCResponse<",
            "Lee/cyber/smartid/dto/jsonrpc/result/GetRegTokenNonceResult;",
            ">;>;"
        }
    .end annotation
.end method

.method public abstract getTransaction(Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCRequest;)Lc/b;
    .param p1    # Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCRequest;
        .annotation runtime Lc/b/a;
        .end annotation
    .end param
    .annotation runtime Lc/b/k;
        a = {
            "Content-Type: application/json-rpc",
            "Accept-Charset: utf-8"
        }
    .end annotation

    .annotation runtime Lc/b/o;
        a = "v2/protected"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCRequest;",
            ")",
            "Lc/b<",
            "Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCResponse<",
            "Lee/cyber/smartid/dto/jsonrpc/result/TransactionContainerResult;",
            ">;>;"
        }
    .end annotation
.end method

.method public abstract registerAccount(Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCRequest;)Lc/b;
    .param p1    # Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCRequest;
        .annotation runtime Lc/b/a;
        .end annotation
    .end param
    .annotation runtime Lc/b/k;
        a = {
            "Content-Type: application/json-rpc",
            "Accept-Charset: utf-8"
        }
    .end annotation

    .annotation runtime Lc/b/o;
        a = "v2/protected"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCRequest;",
            ")",
            "Lc/b<",
            "Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCResponse<",
            "Lee/cyber/smartid/dto/jsonrpc/result/RegisterAccountResult;",
            ">;>;"
        }
    .end annotation
.end method

.method public abstract registerDevice(Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCRequest;)Lc/b;
    .param p1    # Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCRequest;
        .annotation runtime Lc/b/a;
        .end annotation
    .end param
    .annotation runtime Lc/b/k;
        a = {
            "Content-Type: application/json-rpc",
            "Accept-Charset: utf-8"
        }
    .end annotation

    .annotation runtime Lc/b/o;
        a = "v2/public"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCRequest;",
            ")",
            "Lc/b<",
            "Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCResponse<",
            "Lee/cyber/smartid/dto/jsonrpc/result/RegisterDeviceResult;",
            ">;>;"
        }
    .end annotation
.end method

.method public abstract updateDevice(Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCRequest;)Lc/b;
    .param p1    # Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCRequest;
        .annotation runtime Lc/b/a;
        .end annotation
    .end param
    .annotation runtime Lc/b/k;
        a = {
            "Content-Type: application/json-rpc",
            "Accept-Charset: utf-8"
        }
    .end annotation

    .annotation runtime Lc/b/o;
        a = "v2/protected"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCRequest;",
            ")",
            "Lc/b<",
            "Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCResponse<",
            "Lee/cyber/smartid/dto/jsonrpc/result/UpdateDeviceResult;",
            ">;>;"
        }
    .end annotation
.end method

.method public abstract upgradeKeyPair(Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCRequest;)Lc/b;
    .param p1    # Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCRequest;
        .annotation runtime Lc/b/a;
        .end annotation
    .end param
    .annotation runtime Lc/b/k;
        a = {
            "Content-Type: application/json-rpc",
            "Accept-Charset: utf-8"
        }
    .end annotation

    .annotation runtime Lc/b/o;
        a = "v2/protected"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCRequest;",
            ")",
            "Lc/b<",
            "Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCResponse<",
            "Lee/cyber/smartid/dto/jsonrpc/result/UpgradeKeyPairResult;",
            ">;>;"
        }
    .end annotation
.end method
