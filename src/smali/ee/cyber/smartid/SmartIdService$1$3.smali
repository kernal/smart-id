.class Lee/cyber/smartid/SmartIdService$1$3;
.super Ljava/lang/Object;
.source "SmartIdService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lee/cyber/smartid/SmartIdService$1;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lee/cyber/smartid/dto/SmartIdError;

.field final synthetic b:Lee/cyber/smartid/dto/upgrade/InteractiveUpgradeState;

.field final synthetic c:[Lee/cyber/smartid/dto/SmartIdError;

.field final synthetic d:Lee/cyber/smartid/SmartIdService$1;


# direct methods
.method constructor <init>(Lee/cyber/smartid/SmartIdService$1;Lee/cyber/smartid/dto/SmartIdError;Lee/cyber/smartid/dto/upgrade/InteractiveUpgradeState;[Lee/cyber/smartid/dto/SmartIdError;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lee/cyber/smartid/SmartIdService$1$3;->d:Lee/cyber/smartid/SmartIdService$1;

    iput-object p2, p0, Lee/cyber/smartid/SmartIdService$1$3;->a:Lee/cyber/smartid/dto/SmartIdError;

    iput-object p3, p0, Lee/cyber/smartid/SmartIdService$1$3;->b:Lee/cyber/smartid/dto/upgrade/InteractiveUpgradeState;

    iput-object p4, p0, Lee/cyber/smartid/SmartIdService$1$3;->c:[Lee/cyber/smartid/dto/SmartIdError;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    const-string v0, "initService - wakelock released"

    .line 1
    :try_start_0
    iget-object v1, p0, Lee/cyber/smartid/SmartIdService$1$3;->d:Lee/cyber/smartid/SmartIdService$1;

    iget-object v1, v1, Lee/cyber/smartid/SmartIdService$1;->b:Lee/cyber/smartid/SmartIdService;

    iget-object v2, p0, Lee/cyber/smartid/SmartIdService$1$3;->d:Lee/cyber/smartid/SmartIdService$1;

    iget-object v2, v2, Lee/cyber/smartid/SmartIdService$1;->a:Ljava/lang/String;

    const/4 v3, 0x0

    const-class v4, Lee/cyber/smartid/inter/InitServiceListener;

    invoke-static {v1, v2, v3, v4}, Lee/cyber/smartid/SmartIdService;->a(Lee/cyber/smartid/SmartIdService;Ljava/lang/String;ZLjava/lang/Class;)Lee/cyber/smartid/inter/ServiceListener;

    move-result-object v1

    check-cast v1, Lee/cyber/smartid/inter/InitServiceListener;

    if-eqz v1, :cond_0

    .line 2
    iget-object v2, p0, Lee/cyber/smartid/SmartIdService$1$3;->a:Lee/cyber/smartid/dto/SmartIdError;

    if-eqz v2, :cond_0

    .line 3
    iget-object v2, p0, Lee/cyber/smartid/SmartIdService$1$3;->d:Lee/cyber/smartid/SmartIdService$1;

    iget-object v2, v2, Lee/cyber/smartid/SmartIdService$1;->b:Lee/cyber/smartid/SmartIdService;

    iget-object v3, p0, Lee/cyber/smartid/SmartIdService$1$3;->d:Lee/cyber/smartid/SmartIdService$1;

    iget-object v3, v3, Lee/cyber/smartid/SmartIdService$1;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lee/cyber/smartid/SmartIdService;->removeListener(Ljava/lang/String;)V

    .line 4
    iget-object v2, p0, Lee/cyber/smartid/SmartIdService$1$3;->d:Lee/cyber/smartid/SmartIdService$1;

    iget-object v2, v2, Lee/cyber/smartid/SmartIdService$1;->a:Ljava/lang/String;

    iget-object v3, p0, Lee/cyber/smartid/SmartIdService$1$3;->a:Lee/cyber/smartid/dto/SmartIdError;

    invoke-interface {v1, v2, v3}, Lee/cyber/smartid/inter/InitServiceListener;->onInitServiceFailed(Ljava/lang/String;Lee/cyber/smartid/dto/SmartIdError;)V

    goto :goto_0

    :cond_0
    if-eqz v1, :cond_1

    .line 5
    iget-object v2, p0, Lee/cyber/smartid/SmartIdService$1$3;->b:Lee/cyber/smartid/dto/upgrade/InteractiveUpgradeState;

    invoke-virtual {v2}, Lee/cyber/smartid/dto/upgrade/InteractiveUpgradeState;->isUpgradePending()Z

    move-result v2

    if-eqz v2, :cond_1

    goto :goto_0

    :cond_1
    if-eqz v1, :cond_2

    .line 6
    iget-object v2, p0, Lee/cyber/smartid/SmartIdService$1$3;->d:Lee/cyber/smartid/SmartIdService$1;

    iget-object v2, v2, Lee/cyber/smartid/SmartIdService$1;->b:Lee/cyber/smartid/SmartIdService;

    iget-object v3, p0, Lee/cyber/smartid/SmartIdService$1$3;->d:Lee/cyber/smartid/SmartIdService$1;

    iget-object v3, v3, Lee/cyber/smartid/SmartIdService$1;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lee/cyber/smartid/SmartIdService;->removeListener(Ljava/lang/String;)V

    .line 7
    iget-object v2, p0, Lee/cyber/smartid/SmartIdService$1$3;->d:Lee/cyber/smartid/SmartIdService$1;

    iget-object v2, v2, Lee/cyber/smartid/SmartIdService$1;->a:Ljava/lang/String;

    iget-object v3, p0, Lee/cyber/smartid/SmartIdService$1$3;->c:[Lee/cyber/smartid/dto/SmartIdError;

    invoke-interface {v1, v2, v3}, Lee/cyber/smartid/inter/InitServiceListener;->onInitServiceSuccess(Ljava/lang/String;[Lee/cyber/smartid/dto/SmartIdError;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 8
    :cond_2
    :goto_0
    invoke-static {}, Lee/cyber/smartid/SmartIdService;->a()Lee/cyber/smartid/util/Log;

    move-result-object v1

    invoke-virtual {v1, v0}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    .line 9
    iget-object v0, p0, Lee/cyber/smartid/SmartIdService$1$3;->d:Lee/cyber/smartid/SmartIdService$1;

    iget-object v0, v0, Lee/cyber/smartid/SmartIdService$1;->b:Lee/cyber/smartid/SmartIdService;

    invoke-static {v0}, Lee/cyber/smartid/SmartIdService;->p(Lee/cyber/smartid/SmartIdService;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    invoke-static {v0}, Lee/cyber/smartid/util/Util;->releaseWakeLock(Landroid/os/PowerManager$WakeLock;)V

    goto :goto_1

    :catchall_0
    move-exception v1

    goto :goto_2

    :catch_0
    move-exception v1

    .line 10
    :try_start_1
    invoke-static {}, Lee/cyber/smartid/SmartIdService;->a()Lee/cyber/smartid/util/Log;

    move-result-object v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    const-string v3, "initService notifyUI"

    :try_start_2
    invoke-virtual {v2, v3, v1}, Lee/cyber/smartid/util/Log;->e(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 11
    invoke-static {}, Lee/cyber/smartid/SmartIdService;->a()Lee/cyber/smartid/util/Log;

    move-result-object v1

    invoke-virtual {v1, v0}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    .line 12
    iget-object v0, p0, Lee/cyber/smartid/SmartIdService$1$3;->d:Lee/cyber/smartid/SmartIdService$1;

    iget-object v0, v0, Lee/cyber/smartid/SmartIdService$1;->b:Lee/cyber/smartid/SmartIdService;

    invoke-static {v0}, Lee/cyber/smartid/SmartIdService;->p(Lee/cyber/smartid/SmartIdService;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    invoke-static {v0}, Lee/cyber/smartid/util/Util;->releaseWakeLock(Landroid/os/PowerManager$WakeLock;)V

    :goto_1
    return-void

    .line 13
    :goto_2
    invoke-static {}, Lee/cyber/smartid/SmartIdService;->a()Lee/cyber/smartid/util/Log;

    move-result-object v2

    invoke-virtual {v2, v0}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    .line 14
    iget-object v0, p0, Lee/cyber/smartid/SmartIdService$1$3;->d:Lee/cyber/smartid/SmartIdService$1;

    iget-object v0, v0, Lee/cyber/smartid/SmartIdService$1;->b:Lee/cyber/smartid/SmartIdService;

    invoke-static {v0}, Lee/cyber/smartid/SmartIdService;->p(Lee/cyber/smartid/SmartIdService;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    invoke-static {v0}, Lee/cyber/smartid/util/Util;->releaseWakeLock(Landroid/os/PowerManager$WakeLock;)V

    throw v1
.end method
