.class Lee/cyber/smartid/SmartIdService$14;
.super Ljava/lang/Object;
.source "SmartIdService.java"

# interfaces
.implements Lee/cyber/smartid/inter/UpdateDeviceListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lee/cyber/smartid/SmartIdService;->a(Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/inter/StorePushNotificationDataListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:Ljava/lang/String;

.field final synthetic c:Lee/cyber/smartid/SmartIdService;


# direct methods
.method constructor <init>(Lee/cyber/smartid/SmartIdService;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lee/cyber/smartid/SmartIdService$14;->c:Lee/cyber/smartid/SmartIdService;

    iput-object p2, p0, Lee/cyber/smartid/SmartIdService$14;->a:Ljava/lang/String;

    iput-object p3, p0, Lee/cyber/smartid/SmartIdService$14;->b:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onUpdateDeviceFailed(Ljava/lang/String;Lee/cyber/smartid/dto/SmartIdError;)V
    .locals 2

    .line 1
    invoke-static {}, Lee/cyber/smartid/SmartIdService;->a()Lee/cyber/smartid/util/Log;

    move-result-object p1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "storePushNotificationData - onUpdateDeviceFailed: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lee/cyber/smartid/util/Log;->e(Ljava/lang/String;)V

    .line 2
    iget-object p1, p0, Lee/cyber/smartid/SmartIdService$14;->c:Lee/cyber/smartid/SmartIdService;

    new-instance v0, Lee/cyber/smartid/SmartIdService$14$1;

    invoke-direct {v0, p0, p2}, Lee/cyber/smartid/SmartIdService$14$1;-><init>(Lee/cyber/smartid/SmartIdService$14;Lee/cyber/smartid/dto/SmartIdError;)V

    invoke-static {p1, v0}, Lee/cyber/smartid/SmartIdService;->a(Lee/cyber/smartid/SmartIdService;Ljava/lang/Runnable;)V

    return-void
.end method

.method public onUpdateDeviceSuccess(Ljava/lang/String;Lee/cyber/smartid/dto/jsonrpc/resp/UpdateDeviceResp;)V
    .locals 2

    .line 1
    invoke-static {}, Lee/cyber/smartid/SmartIdService;->a()Lee/cyber/smartid/util/Log;

    move-result-object p1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "storePushNotificationData - onUpdateDeviceSuccess: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    .line 2
    iget-object p1, p0, Lee/cyber/smartid/SmartIdService$14;->c:Lee/cyber/smartid/SmartIdService;

    iget-object p2, p0, Lee/cyber/smartid/SmartIdService$14;->a:Ljava/lang/String;

    const-class v0, Lee/cyber/smartid/inter/StorePushNotificationDataListener;

    const/4 v1, 0x1

    invoke-static {p1, p2, v1, v0}, Lee/cyber/smartid/SmartIdService;->a(Lee/cyber/smartid/SmartIdService;Ljava/lang/String;ZLjava/lang/Class;)Lee/cyber/smartid/inter/ServiceListener;

    move-result-object p1

    check-cast p1, Lee/cyber/smartid/inter/StorePushNotificationDataListener;

    if-eqz p1, :cond_0

    .line 3
    iget-object p2, p0, Lee/cyber/smartid/SmartIdService$14;->a:Ljava/lang/String;

    new-instance v0, Lee/cyber/smartid/dto/jsonrpc/resp/StorePushNotificationResp;

    iget-object v1, p0, Lee/cyber/smartid/SmartIdService$14;->b:Ljava/lang/String;

    invoke-direct {v0, p2, v1}, Lee/cyber/smartid/dto/jsonrpc/resp/StorePushNotificationResp;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {p1, p2, v0}, Lee/cyber/smartid/inter/StorePushNotificationDataListener;->onStorePushNotificationDataSuccess(Ljava/lang/String;Lee/cyber/smartid/dto/jsonrpc/resp/StorePushNotificationResp;)V

    :cond_0
    return-void
.end method
