.class public Lee/cyber/smartid/SmartIdService$SmartIdServiceAlarmManager;
.super Landroid/content/BroadcastReceiver;
.source "SmartIdService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lee/cyber/smartid/SmartIdService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "SmartIdServiceAlarmManager"
.end annotation


# instance fields
.field private a:Landroid/os/PowerManager$WakeLock;

.field private final b:Ljava/lang/Object;

.field private final c:Lee/cyber/smartid/util/Log;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 2
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lee/cyber/smartid/SmartIdService$SmartIdServiceAlarmManager;->b:Ljava/lang/Object;

    .line 3
    invoke-static {p0}, Lee/cyber/smartid/util/Log;->getInstance(Ljava/lang/Object;)Lee/cyber/smartid/util/Log;

    move-result-object v0

    iput-object v0, p0, Lee/cyber/smartid/SmartIdService$SmartIdServiceAlarmManager;->c:Lee/cyber/smartid/util/Log;

    return-void
.end method

.method static synthetic a(Lee/cyber/smartid/SmartIdService$SmartIdServiceAlarmManager;Landroid/content/Context;)Landroid/os/PowerManager$WakeLock;
    .locals 0

    .line 3
    invoke-direct {p0, p1}, Lee/cyber/smartid/SmartIdService$SmartIdServiceAlarmManager;->b(Landroid/content/Context;)Landroid/os/PowerManager$WakeLock;

    move-result-object p0

    return-object p0
.end method

.method static synthetic a(Lee/cyber/smartid/SmartIdService$SmartIdServiceAlarmManager;)Lee/cyber/smartid/util/Log;
    .locals 0

    .line 2
    iget-object p0, p0, Lee/cyber/smartid/SmartIdService$SmartIdServiceAlarmManager;->c:Lee/cyber/smartid/util/Log;

    return-object p0
.end method

.method private a(Landroid/content/Context;)V
    .locals 5

    .line 1
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lee/cyber/smartid/SmartIdService;->getInstance(Landroid/content/Context;)Lee/cyber/smartid/SmartIdService;

    move-result-object v0

    .line 2
    invoke-virtual {v0}, Lee/cyber/smartid/SmartIdService;->isInitDone()Z

    move-result v1

    if-nez v1, :cond_0

    .line 3
    iget-object p1, p0, Lee/cyber/smartid/SmartIdService$SmartIdServiceAlarmManager;->c:Lee/cyber/smartid/util/Log;

    const-string v0, "handleRetryUpdateDeviceAlarm - initService not done, aborting .."

    invoke-virtual {p1, v0}, Lee/cyber/smartid/util/Log;->e(Ljava/lang/String;)V

    return-void

    .line 4
    :cond_0
    iget-object v1, p0, Lee/cyber/smartid/SmartIdService$SmartIdServiceAlarmManager;->c:Lee/cyber/smartid/util/Log;

    const-string v2, "handleRetryUpdateDeviceAlarm"

    invoke-virtual {v1, v2}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    .line 5
    invoke-direct {p0, p1}, Lee/cyber/smartid/SmartIdService$SmartIdServiceAlarmManager;->b(Landroid/content/Context;)Landroid/os/PowerManager$WakeLock;

    move-result-object v1

    const-wide/32 v3, 0x493e0

    invoke-static {v1, v3, v4}, Lee/cyber/smartid/util/Util;->acquireWakeLock(Landroid/os/PowerManager$WakeLock;J)V

    .line 6
    :try_start_0
    invoke-static {v0}, Lee/cyber/smartid/SmartIdService;->d(Lee/cyber/smartid/SmartIdService;)Lee/cyber/smartid/tse/SmartIdTSE;

    move-result-object v1

    invoke-virtual {v1}, Lee/cyber/smartid/tse/SmartIdTSE;->isDeviceRegistrationDone()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 7
    invoke-static {v0}, Lee/cyber/smartid/SmartIdService;->h(Lee/cyber/smartid/SmartIdService;)Lee/cyber/smartid/manager/inter/SafetyNetManager;

    move-result-object v1

    invoke-interface {v1}, Lee/cyber/smartid/manager/inter/SafetyNetManager;->shouldStartSafetyNetCheckPostInit()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-static {v0}, Lee/cyber/smartid/SmartIdService;->h(Lee/cyber/smartid/SmartIdService;)Lee/cyber/smartid/manager/inter/SafetyNetManager;

    move-result-object v1

    invoke-interface {v1}, Lee/cyber/smartid/manager/inter/SafetyNetManager;->getLastSafetyNetAttestation()Lee/cyber/smartid/dto/SafetyNetAttestation;

    move-result-object v1

    if-nez v1, :cond_1

    .line 8
    iget-object v1, p0, Lee/cyber/smartid/SmartIdService$SmartIdServiceAlarmManager;->c:Lee/cyber/smartid/util/Log;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const-string v3, "handleRetryUpdateDeviceAlarm: init done, calling SafetyNet and updateDevice .."

    :try_start_1
    invoke-virtual {v1, v3}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    .line 9
    invoke-static {v0}, Lee/cyber/smartid/SmartIdService;->h(Lee/cyber/smartid/SmartIdService;)Lee/cyber/smartid/manager/inter/SafetyNetManager;

    move-result-object v0

    invoke-interface {v0}, Lee/cyber/smartid/manager/inter/SafetyNetManager;->startSafetyNetCheckAsync()V

    goto :goto_0

    .line 10
    :cond_1
    iget-object v1, p0, Lee/cyber/smartid/SmartIdService$SmartIdServiceAlarmManager;->c:Lee/cyber/smartid/util/Log;
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    const-string v3, "handleRetryUpdateDeviceAlarm: init done, calling updateDevice .."

    :try_start_2
    invoke-virtual {v1, v3}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    .line 11
    invoke-static {v0}, Lee/cyber/smartid/SmartIdService;->o(Lee/cyber/smartid/SmartIdService;)V

    goto :goto_0

    .line 12
    :cond_2
    iget-object v0, p0, Lee/cyber/smartid/SmartIdService$SmartIdServiceAlarmManager;->c:Lee/cyber/smartid/util/Log;
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    const-string v1, "handleRetryUpdateDeviceAlarm - registration not done, skipping updateDeviceIfNeeded"

    :try_start_3
    invoke-virtual {v0, v1}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 13
    :goto_0
    invoke-direct {p0, p1}, Lee/cyber/smartid/SmartIdService$SmartIdServiceAlarmManager;->b(Landroid/content/Context;)Landroid/os/PowerManager$WakeLock;

    move-result-object p1

    invoke-static {p1}, Lee/cyber/smartid/util/Util;->releaseWakeLock(Landroid/os/PowerManager$WakeLock;)V

    goto :goto_1

    :catchall_0
    move-exception v0

    goto :goto_2

    :catch_0
    move-exception v0

    .line 14
    :try_start_4
    iget-object v1, p0, Lee/cyber/smartid/SmartIdService$SmartIdServiceAlarmManager;->c:Lee/cyber/smartid/util/Log;

    invoke-virtual {v1, v2, v0}, Lee/cyber/smartid/util/Log;->e(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 15
    invoke-direct {p0, p1}, Lee/cyber/smartid/SmartIdService$SmartIdServiceAlarmManager;->b(Landroid/content/Context;)Landroid/os/PowerManager$WakeLock;

    move-result-object p1

    invoke-static {p1}, Lee/cyber/smartid/util/Util;->releaseWakeLock(Landroid/os/PowerManager$WakeLock;)V

    :goto_1
    return-void

    :goto_2
    invoke-direct {p0, p1}, Lee/cyber/smartid/SmartIdService$SmartIdServiceAlarmManager;->b(Landroid/content/Context;)Landroid/os/PowerManager$WakeLock;

    move-result-object p1

    invoke-static {p1}, Lee/cyber/smartid/util/Util;->releaseWakeLock(Landroid/os/PowerManager$WakeLock;)V

    throw v0
.end method

.method private a(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5

    .line 5
    iget-object v0, p0, Lee/cyber/smartid/SmartIdService$SmartIdServiceAlarmManager;->b:Ljava/lang/Object;

    monitor-enter v0

    const/4 v1, 0x0

    if-eqz p2, :cond_0

    const-string v2, "android.intent.action.ACTION_SHUTDOWN"

    .line 6
    :try_start_0
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 7
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Lee/cyber/smartid/SmartIdService;->getInstance(Landroid/content/Context;)Lee/cyber/smartid/SmartIdService;

    move-result-object p1

    .line 8
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const-string v3, "ee.cyber.smartid.EXTRA_ALARM_TARGET_ID"

    :try_start_1
    invoke-virtual {p2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    invoke-static {p1, v2, p2, v1, v1}, Lee/cyber/smartid/SmartIdService;->a(Lee/cyber/smartid/SmartIdService;Ljava/lang/String;Ljava/lang/String;ZZ)V

    .line 9
    iget-object p1, p0, Lee/cyber/smartid/SmartIdService$SmartIdServiceAlarmManager;->c:Lee/cyber/smartid/util/Log;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    const-string p2, "handleOnReceive: we are done, no need for SmartIdService init"

    :try_start_2
    invoke-virtual {p1, p2}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    .line 10
    monitor-exit v0

    return-void

    .line 11
    :cond_0
    iget-object v2, p0, Lee/cyber/smartid/SmartIdService$SmartIdServiceAlarmManager;->c:Lee/cyber/smartid/util/Log;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    const-string v3, "handleOnReceive: we need SmartIdService init. Do we have it already?"

    :try_start_3
    invoke-virtual {v2, v3}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    .line 12
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lee/cyber/smartid/SmartIdService;->getInstance(Landroid/content/Context;)Lee/cyber/smartid/SmartIdService;

    move-result-object v2

    .line 13
    invoke-virtual {v2}, Lee/cyber/smartid/SmartIdService;->isInitDone()Z

    move-result v3

    if-nez v3, :cond_1

    .line 14
    invoke-direct {p0, p1}, Lee/cyber/smartid/SmartIdService$SmartIdServiceAlarmManager;->b(Landroid/content/Context;)Landroid/os/PowerManager$WakeLock;

    move-result-object v1

    const-wide/32 v3, 0x493e0

    invoke-static {v1, v3, v4}, Lee/cyber/smartid/util/Util;->acquireWakeLock(Landroid/os/PowerManager$WakeLock;J)V

    .line 15
    iget-object v1, p0, Lee/cyber/smartid/SmartIdService$SmartIdServiceAlarmManager;->c:Lee/cyber/smartid/util/Log;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    const-string v3, "handleOnReceive: init not yet done, calling init .."

    :try_start_4
    invoke-virtual {v1, v3}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    .line 16
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    const-string v3, "ee.cyber.smartid.TAG_INIT_SERVICE_FROM_ALARM_"

    :try_start_5
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    const-string v3, "_"

    :try_start_6
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    const-string v3, "ee.cyber.smartid.EXTRA_ALARM_TARGET_ID"

    :try_start_7
    invoke-virtual {p2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v3, Lee/cyber/smartid/SmartIdService$SmartIdServiceAlarmManager$2;

    invoke-direct {v3, p0, p1, p2}, Lee/cyber/smartid/SmartIdService$SmartIdServiceAlarmManager$2;-><init>(Lee/cyber/smartid/SmartIdService$SmartIdServiceAlarmManager;Landroid/content/Context;Landroid/content/Intent;)V

    invoke-virtual {v2, v1, v3}, Lee/cyber/smartid/SmartIdService;->initService(Ljava/lang/String;Lee/cyber/smartid/inter/InitServiceListener;)V

    goto :goto_0

    .line 17
    :cond_1
    iget-object v2, p0, Lee/cyber/smartid/SmartIdService$SmartIdServiceAlarmManager;->c:Lee/cyber/smartid/util/Log;
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    const-string v3, "handleOnReceive: init is already done"

    :try_start_8
    invoke-virtual {v2, v3}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    .line 18
    invoke-direct {p0, p1, p2, v1}, Lee/cyber/smartid/SmartIdService$SmartIdServiceAlarmManager;->a(Landroid/content/Context;Landroid/content/Intent;Z)V

    .line 19
    :goto_0
    monitor-exit v0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit v0
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    throw p1
.end method

.method private a(Landroid/content/Context;Landroid/content/Intent;Z)V
    .locals 3

    .line 20
    iget-object v0, p0, Lee/cyber/smartid/SmartIdService$SmartIdServiceAlarmManager;->c:Lee/cyber/smartid/util/Log;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "handleOnReceiveInitServiceConfirmed - wasInitServiceJustCompleted: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    if-eqz p2, :cond_1

    .line 21
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ee.cyber.smartid.ACTION_RETRY_UPDATE_DEVICE"

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    if-eqz p3, :cond_0

    .line 22
    iget-object v0, p0, Lee/cyber/smartid/SmartIdService$SmartIdServiceAlarmManager;->c:Lee/cyber/smartid/util/Log;

    const-string v1, "handleOnReceiveInitServiceConfirmed - no need for trigger ACTION_RETRY_UPDATE_DEVICE, initService was just completed .."

    invoke-virtual {v0, v1}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    goto :goto_0

    .line 23
    :cond_0
    iget-object v0, p0, Lee/cyber/smartid/SmartIdService$SmartIdServiceAlarmManager;->c:Lee/cyber/smartid/util/Log;

    const-string v1, "handleOnReceiveInitServiceConfirmed - triggering ACTION_RETRY_UPDATE_DEVICE .."

    invoke-virtual {v0, v1}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    .line 24
    invoke-direct {p0, p1}, Lee/cyber/smartid/SmartIdService$SmartIdServiceAlarmManager;->a(Landroid/content/Context;)V

    goto :goto_0

    :cond_1
    if-eqz p2, :cond_2

    .line 25
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ee.cyber.smartid.ACTION_REMOVE_EXPIRED_TRANSACTIONS"

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 26
    iget-object v0, p0, Lee/cyber/smartid/SmartIdService$SmartIdServiceAlarmManager;->c:Lee/cyber/smartid/util/Log;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "handleOnReceiveInitServiceConfirmed - triggering "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, " .."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    .line 27
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lee/cyber/smartid/SmartIdService;->getInstance(Landroid/content/Context;)Lee/cyber/smartid/SmartIdService;

    move-result-object v0

    .line 28
    invoke-static {v0}, Lee/cyber/smartid/SmartIdService;->i(Lee/cyber/smartid/SmartIdService;)Lee/cyber/smartid/manager/inter/TransactionCacheManager;

    move-result-object v1

    invoke-interface {v1}, Lee/cyber/smartid/manager/inter/TransactionCacheManager;->removeExpiredTransactionsFromCache()V

    .line 29
    invoke-static {v0}, Lee/cyber/smartid/SmartIdService;->i(Lee/cyber/smartid/SmartIdService;)Lee/cyber/smartid/manager/inter/TransactionCacheManager;

    move-result-object v0

    invoke-interface {v0}, Lee/cyber/smartid/manager/inter/TransactionCacheManager;->setNextRemoveExpiredTransactionsAlarm()V

    .line 30
    :cond_2
    :goto_0
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Lee/cyber/smartid/SmartIdService;->getInstance(Landroid/content/Context;)Lee/cyber/smartid/SmartIdService;

    move-result-object p1

    if-eqz p2, :cond_3

    .line 31
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ee.cyber.smartid.EXTRA_ALARM_TARGET_ID"

    invoke-virtual {p2, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    const/4 v1, 0x1

    invoke-static {p1, v0, p2, v1, p3}, Lee/cyber/smartid/SmartIdService;->a(Lee/cyber/smartid/SmartIdService;Ljava/lang/String;Ljava/lang/String;ZZ)V

    :cond_3
    return-void
.end method

.method static synthetic a(Lee/cyber/smartid/SmartIdService$SmartIdServiceAlarmManager;Landroid/content/Context;Landroid/content/Intent;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lee/cyber/smartid/SmartIdService$SmartIdServiceAlarmManager;->a(Landroid/content/Context;Landroid/content/Intent;)V

    return-void
.end method

.method static synthetic a(Lee/cyber/smartid/SmartIdService$SmartIdServiceAlarmManager;Landroid/content/Context;Landroid/content/Intent;Z)V
    .locals 0

    .line 4
    invoke-direct {p0, p1, p2, p3}, Lee/cyber/smartid/SmartIdService$SmartIdServiceAlarmManager;->a(Landroid/content/Context;Landroid/content/Intent;Z)V

    return-void
.end method

.method private b(Landroid/content/Context;)Landroid/os/PowerManager$WakeLock;
    .locals 2

    .line 32
    iget-object v0, p0, Lee/cyber/smartid/SmartIdService$SmartIdServiceAlarmManager;->a:Landroid/os/PowerManager$WakeLock;

    if-nez v0, :cond_1

    const-string v0, "power"

    .line 33
    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/os/PowerManager;

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    const-string v1, "ee.cyber.smartid:WAKELOCK_KEY_ALARM_BROADCAST_RECEIVER"

    .line 34
    invoke-virtual {p1, v0, v1}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    iput-object p1, p0, Lee/cyber/smartid/SmartIdService$SmartIdServiceAlarmManager;->a:Landroid/os/PowerManager$WakeLock;

    .line 35
    :cond_1
    iget-object p1, p0, Lee/cyber/smartid/SmartIdService$SmartIdServiceAlarmManager;->a:Landroid/os/PowerManager$WakeLock;

    return-object p1
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "UnsafeProtectedBroadcastReceiver"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/SmartIdService$SmartIdServiceAlarmManager;->c:Lee/cyber/smartid/util/Log;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onReceive: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    .line 2
    invoke-direct {p0, p1}, Lee/cyber/smartid/SmartIdService$SmartIdServiceAlarmManager;->b(Landroid/content/Context;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    const-wide/32 v1, 0x493e0

    invoke-static {v0, v1, v2}, Lee/cyber/smartid/util/Util;->acquireWakeLock(Landroid/os/PowerManager$WakeLock;J)V

    .line 3
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lee/cyber/smartid/SmartIdService$SmartIdServiceAlarmManager$1;

    invoke-direct {v1, p0, p1, p2}, Lee/cyber/smartid/SmartIdService$SmartIdServiceAlarmManager$1;-><init>(Lee/cyber/smartid/SmartIdService$SmartIdServiceAlarmManager;Landroid/content/Context;Landroid/content/Intent;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 4
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    return-void
.end method
