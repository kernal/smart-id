.class public interface abstract Lee/cyber/smartid/inter/ListenerAccess;
.super Ljava/lang/Object;
.source "ListenerAccess.java"


# virtual methods
.method public abstract getListener(Ljava/lang/String;ZLjava/lang/Class;)Lee/cyber/smartid/inter/ServiceListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lee/cyber/smartid/inter/ServiceListener;",
            ">(",
            "Ljava/lang/String;",
            "Z",
            "Ljava/lang/Class<",
            "TT;>;)TT;"
        }
    .end annotation
.end method

.method public abstract getListenersForType(ZLjava/lang/Class;)[Landroid/support/v4/e/j;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lee/cyber/smartid/inter/ServiceListener;",
            ">(Z",
            "Ljava/lang/Class<",
            "TT;>;)[",
            "Landroid/support/v4/e/j<",
            "Ljava/lang/String;",
            "TT;>;"
        }
    .end annotation
.end method

.method public abstract getTSEListenerToServiceListenerMapper()Lee/cyber/smartid/manager/inter/TSEListenerToServiceListenerMapper;
.end method

.method public abstract notifyAlarmHandlers(Ljava/lang/String;Ljava/lang/String;ZZ)V
.end method

.method public abstract notifyError(Lee/cyber/smartid/inter/ServiceListener;Ljava/lang/String;Lee/cyber/smartid/dto/SmartIdError;)V
.end method

.method public abstract notifySystemEventsListeners(Lee/cyber/smartid/dto/SmartIdError;)V
.end method

.method public abstract setListener(Ljava/lang/String;Lee/cyber/smartid/inter/ServiceListener;)V
.end method
