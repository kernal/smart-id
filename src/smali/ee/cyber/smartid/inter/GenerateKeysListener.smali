.class public interface abstract Lee/cyber/smartid/inter/GenerateKeysListener;
.super Ljava/lang/Object;
.source "GenerateKeysListener.java"

# interfaces
.implements Lee/cyber/smartid/inter/ServiceListener;


# virtual methods
.method public abstract onGenerateKeysFailed(Ljava/lang/String;Lee/cyber/smartid/dto/SmartIdError;)V
.end method

.method public abstract onGenerateKeysProgress(Ljava/lang/String;II)V
.end method

.method public abstract onGenerateKeysSuccess(Ljava/lang/String;Lee/cyber/smartid/tse/dto/jsonrpc/resp/GenerateKeysResp;)V
.end method
