.class public interface abstract Lee/cyber/smartid/inter/DeleteAccountListener;
.super Ljava/lang/Object;
.source "DeleteAccountListener.java"

# interfaces
.implements Lee/cyber/smartid/inter/ServiceListener;


# virtual methods
.method public abstract onDeleteAccountFailed(Ljava/lang/String;Lee/cyber/smartid/dto/SmartIdError;)V
.end method

.method public abstract onDeleteAccountSuccess(Ljava/lang/String;Lee/cyber/smartid/dto/jsonrpc/resp/DeleteAccountResp;)V
.end method
