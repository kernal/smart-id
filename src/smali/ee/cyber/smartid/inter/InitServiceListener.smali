.class public interface abstract Lee/cyber/smartid/inter/InitServiceListener;
.super Ljava/lang/Object;
.source "InitServiceListener.java"

# interfaces
.implements Lee/cyber/smartid/inter/ServiceListener;


# virtual methods
.method public abstract onInitServiceFailed(Ljava/lang/String;Lee/cyber/smartid/dto/SmartIdError;)V
.end method

.method public abstract onInitServiceInteractiveUpgradeFailed(Ljava/lang/String;ILee/cyber/smartid/dto/SmartIdError;)V
.end method

.method public abstract onInitServiceInteractiveUpgradeStarted(Ljava/lang/String;I)V
.end method

.method public abstract onInitServiceSuccess(Ljava/lang/String;[Lee/cyber/smartid/dto/SmartIdError;)V
.end method
