.class public interface abstract Lee/cyber/smartid/inter/ServiceAccess;
.super Ljava/lang/Object;
.source "ServiceAccess.java"

# interfaces
.implements Lee/cyber/smartid/tse/inter/ExternalResourceAccess;


# virtual methods
.method public abstract acquireWakeLockForService()V
.end method

.method public abstract getAccountsStorageId()Ljava/lang/String;
.end method

.method public abstract getApplicationContext()Landroid/content/Context;
.end method

.method public abstract getAutomaticRequestRetryDelay(I)J
.end method

.method public abstract getCurrentStorageVersion()I
.end method

.method public abstract getDeleteAccountManager()Lee/cyber/smartid/manager/inter/DeleteAccountManager;
.end method

.method public abstract getInteractiveUpgradeHelper()Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;
.end method

.method public abstract getInteractiveUpgradeStateStorageId()Ljava/lang/String;
.end method

.method public abstract getKeyId(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
.end method

.method public abstract getKnownServerKeys(Ljava/lang/String;)Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getLastSafetyNetAttestation()Lee/cyber/smartid/dto/SafetyNetAttestation;
.end method

.method public abstract getMDv2StorageVersion()I
.end method

.method public abstract getPostInitManager()Lee/cyber/smartid/manager/inter/PostInitManager;
.end method

.method public abstract getPropertiesManager()Lee/cyber/smartid/manager/inter/PropertiesManager;
.end method

.method public abstract getPropertySource()Lee/cyber/smartid/manager/inter/PropertiesSource;
.end method

.method public abstract getPushDataCurrentStorageId()Ljava/lang/String;
.end method

.method public abstract getPushDataSentStorageId()Ljava/lang/String;
.end method

.method public abstract getRemoveExpiredTransactionActionId()Ljava/lang/String;
.end method

.method public abstract getSafetyNetManager()Lee/cyber/smartid/manager/inter/SafetyNetManager;
.end method

.method public abstract getSafetyNetResultHashStorageId()Ljava/lang/String;
.end method

.method public abstract getStoredAccountManager()Lee/cyber/smartid/manager/inter/StoredAccountManager;
.end method

.method public abstract getTransactionCacheManager()Lee/cyber/smartid/manager/inter/TransactionCacheManager;
.end method

.method public abstract getTransactionCacheStorageId()Ljava/lang/String;
.end method

.method public abstract getTransactionFinder()Lee/cyber/smartid/util/TransactionFinder;
.end method

.method public abstract getTransactionRespMapper()Lee/cyber/smartid/manager/inter/TransactionRespMapper;
.end method

.method public abstract getTseErrorToServiceErrorMapper()Lee/cyber/smartid/manager/inter/TSEErrorToServiceErrorMapper;
.end method

.method public abstract getUpdateDeviceData()Lee/cyber/smartid/dto/jsonrpc/param/RegisterDeviceData;
.end method

.method public abstract getUpdateDeviceHashStorageId()Ljava/lang/String;
.end method

.method public abstract getVerificationCodeEvaluator()Lee/cyber/smartid/manager/inter/VerificationCodeEvaluator;
.end method

.method public abstract getVerificationCodeGenerator()Lee/cyber/smartid/manager/inter/VerificationCodeGenerator;
.end method

.method public abstract handleUpgrade()Lee/cyber/smartid/dto/upgrade/InteractiveUpgradeState;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lee/cyber/smartid/cryptolib/dto/StorageException;
        }
    .end annotation
.end method

.method public abstract notifyUI(Ljava/lang/Runnable;)V
.end method

.method public abstract onPostUpdateDevice(Lee/cyber/smartid/dto/jsonrpc/param/RegisterDeviceData;Ljava/lang/String;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lee/cyber/smartid/cryptolib/dto/StorageException;
        }
    .end annotation
.end method

.method public abstract releaseWakeLockForService()V
.end method

.method public abstract setInitServiceDone()V
.end method

.method public abstract updateDeviceForPostInitIfNeeded()V
.end method

.method public abstract updateDeviceForPostKeyGenerationIfNeeded()V
.end method
