.class public interface abstract Lee/cyber/smartid/inter/CancelTransactionListener;
.super Ljava/lang/Object;
.source "CancelTransactionListener.java"

# interfaces
.implements Lee/cyber/smartid/inter/ServiceListener;


# virtual methods
.method public abstract onCancelTransactionFailed(Ljava/lang/String;Lee/cyber/smartid/dto/SmartIdError;)V
.end method

.method public abstract onCancelTransactionSuccess(Ljava/lang/String;Lee/cyber/smartid/dto/jsonrpc/resp/CancelTransactionResp;)V
.end method
