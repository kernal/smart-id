.class Lee/cyber/smartid/SmartIdService$8$1;
.super Ljava/lang/Object;
.source "SmartIdService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lee/cyber/smartid/SmartIdService$8;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:I

.field final synthetic b:Lee/cyber/smartid/SmartIdService$8;


# direct methods
.method constructor <init>(Lee/cyber/smartid/SmartIdService$8;I)V
    .locals 0

    .line 1
    iput-object p1, p0, Lee/cyber/smartid/SmartIdService$8$1;->b:Lee/cyber/smartid/SmartIdService$8;

    iput p2, p0, Lee/cyber/smartid/SmartIdService$8$1;->a:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 10

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/SmartIdService$8$1;->b:Lee/cyber/smartid/SmartIdService$8;

    iget-object v1, v0, Lee/cyber/smartid/SmartIdService$8;->e:Lee/cyber/smartid/SmartIdService;

    iget-object v0, v0, Lee/cyber/smartid/SmartIdService$8;->c:Ljava/lang/String;

    const-class v2, Lee/cyber/smartid/inter/GetKeyPinLengthListener;

    const/4 v3, 0x1

    invoke-static {v1, v0, v3, v2}, Lee/cyber/smartid/SmartIdService;->a(Lee/cyber/smartid/SmartIdService;Ljava/lang/String;ZLjava/lang/Class;)Lee/cyber/smartid/inter/ServiceListener;

    move-result-object v0

    check-cast v0, Lee/cyber/smartid/inter/GetKeyPinLengthListener;

    if-eqz v0, :cond_0

    .line 2
    iget-object v1, p0, Lee/cyber/smartid/SmartIdService$8$1;->b:Lee/cyber/smartid/SmartIdService$8;

    iget-object v8, v1, Lee/cyber/smartid/SmartIdService$8;->c:Ljava/lang/String;

    new-instance v9, Lee/cyber/smartid/dto/jsonrpc/resp/GetKeyPinLengthResp;

    iget-object v4, v1, Lee/cyber/smartid/SmartIdService$8;->a:Ljava/lang/String;

    iget-object v5, v1, Lee/cyber/smartid/SmartIdService$8;->b:Ljava/lang/String;

    iget v6, p0, Lee/cyber/smartid/SmartIdService$8$1;->a:I

    iget-object v7, v1, Lee/cyber/smartid/SmartIdService$8;->d:Landroid/os/Bundle;

    move-object v2, v9

    move-object v3, v8

    invoke-direct/range {v2 .. v7}, Lee/cyber/smartid/dto/jsonrpc/resp/GetKeyPinLengthResp;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILandroid/os/Bundle;)V

    invoke-interface {v0, v8, v9}, Lee/cyber/smartid/inter/GetKeyPinLengthListener;->onGetKeyPinLengthSuccess(Ljava/lang/String;Lee/cyber/smartid/dto/jsonrpc/resp/GetKeyPinLengthResp;)V

    :cond_0
    return-void
.end method
