.class public final Lee/cyber/smartid/SmartIdService;
.super Ljava/lang/Object;
.source "SmartIdService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lee/cyber/smartid/SmartIdService$SmartIdServiceAlarmManager;,
        Lee/cyber/smartid/SmartIdService$SmartIdServiceWallClock;,
        Lee/cyber/smartid/SmartIdService$SmartIdServiceAlarmAccess;,
        Lee/cyber/smartid/SmartIdService$SmartIdListenerAccess;,
        Lee/cyber/smartid/SmartIdService$SmartIdServiceAccess;,
        Lee/cyber/smartid/SmartIdService$AccountDeleteReason;,
        Lee/cyber/smartid/SmartIdService$InteractiveUpgradeType;,
        Lee/cyber/smartid/SmartIdService$TransactionSource;,
        Lee/cyber/smartid/SmartIdService$AccountInitiationType;,
        Lee/cyber/smartid/SmartIdService$TransactionType;,
        Lee/cyber/smartid/SmartIdService$RPRequestType;,
        Lee/cyber/smartid/SmartIdService$RegistrationTokenAlgorithm;
    }
.end annotation


# static fields
.field public static final ACCOUNT_DELETE_REASON_UPGRADE_TO_MDV2_FAILED:Ljava/lang/String; = "UPGRADE_TO_MDV2_FAILED"

.field public static final ACCOUNT_DELETE_REASON_USER_INITIATED:Ljava/lang/String; = "USER_INITIATED"

.field public static final ACCOUNT_DELETE_REASON_USER_REFUSED_REKEY:Ljava/lang/String; = "USER_REFUSED_REKEY"

.field public static final ACCOUNT_KEY_STATUS_EXPIRED:Ljava/lang/String; = "EXPIRED"

.field public static final ACCOUNT_KEY_STATUS_IN_PREPARATION:Ljava/lang/String; = "IN_PREPARATION"

.field public static final ACCOUNT_KEY_STATUS_LOCKED:Ljava/lang/String; = "LOCKED"

.field public static final ACCOUNT_KEY_STATUS_OK:Ljava/lang/String; = "OK"

.field public static final ACCOUNT_KEY_STATUS_REVOKED:Ljava/lang/String; = "REVOKED"

.field public static final ACCOUNT_KEY_TYPES:[Ljava/lang/String;

.field public static final ACCOUNT_KEY_TYPE_AUTHENTICATION:Ljava/lang/String; = "AUTHENTICATION"

.field public static final ACCOUNT_KEY_TYPE_SIGNATURE:Ljava/lang/String; = "SIGNATURE"

.field public static final ACCOUNT_STATUS_DISABLED:Ljava/lang/String; = "DISABLED"

.field public static final ACCOUNT_STATUS_ENABLED:Ljava/lang/String; = "ENABLED"

.field public static final CERTIFICATE_TYPE_ADVANCED:Ljava/lang/String; = "ADVANCED"

.field public static final CERTIFICATE_TYPE_QUALIFIED:Ljava/lang/String; = "QUALIFIED"

.field public static final HASH_ALGORITHM_SHA1:Ljava/lang/String; = "SHA-1"

.field public static final INITIATION_TYPE_IDENTITY_TOKEN:Ljava/lang/String; = "IDENTITYTOKEN"

.field public static final INITIATION_TYPE_KEY_TOKEN:Ljava/lang/String; = "KEYTOKEN"

.field public static final INITIATION_TYPE_UNKNOWN:Ljava/lang/String; = ""

.field public static final INTERACTIVE_UPGRADE_TYPE_MDV2:I = 0x0

.field public static final IS_NOT_QSCD_KEYPAIR:I = 0x0

.field public static final IS_QSCD_KEYPAIR:I = 0x1

.field public static final PERSONAL_IDENTIFIER_SCHEME_ETSI_IDC:Ljava/lang/String; = "ETSI_ID"

.field public static final PERSONAL_IDENTIFIER_SCHEME_ETSI_PAS:Ljava/lang/String; = "ETSI_PAS"

.field public static final PERSONAL_IDENTIFIER_SCHEME_ETSI_PNO:Ljava/lang/String; = "ETSI_PNO"

.field public static final REGISTRATION_TOKEN_ALGORITHM_MODULI_WITH_NONCE_SHA256:Ljava/lang/String; = "MODULI-WITH-NONCE-SHA256"

.field public static final RP_REQUEST_TYPE_AUTHENTICATION:Ljava/lang/String; = "AUTHENTICATION"

.field public static final RP_REQUEST_TYPE_GET_SIGNING_CERTIFICATE:Ljava/lang/String; = "GET_SIGNING_CERTIFICATE"

.field public static final RP_REQUEST_TYPE_SIGNING:Ljava/lang/String; = "SIGNING"

.field public static final TRANSACTION_SOURCE_CSR:Ljava/lang/String; = "CSR"

.field public static final TRANSACTION_SOURCE_RELYING_PARTY:Ljava/lang/String; = "RELYING_PARTY"

.field public static final TRANSACTION_TYPES:[Ljava/lang/String;

.field public static final TRANSACTION_TYPE_AUTHENTICATION:Ljava/lang/String; = "AUTHENTICATION"

.field public static final TRANSACTION_TYPE_SIGNATURE:Ljava/lang/String; = "SIGNATURE"

.field private static final a:Ljava/math/BigInteger;

.field private static volatile b:Lee/cyber/smartid/SmartIdService;

.field private static c:Lee/cyber/smartid/util/Log;


# instance fields
.field private final A:Lee/cyber/smartid/cryptolib/CryptoLib;

.field private final B:Lee/cyber/smartid/manager/inter/TransactionCacheManager;

.field private final C:Lee/cyber/smartid/manager/inter/DeviceFingerprintManager;

.field private final D:Lee/cyber/smartid/manager/inter/DevicePropertiesManager;

.field private final E:Lee/cyber/smartid/manager/inter/PropertiesSource;

.field private final F:Ljava/lang/Object;

.field private G:Landroid/os/PowerManager$WakeLock;

.field private final H:Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;

.field private final d:Landroid/content/Context;

.field private final e:Lee/cyber/smartid/tse/SmartIdTSE;

.field private volatile f:Z

.field private g:Lee/cyber/smartid/network/SmartIdAPI;

.field private final h:Ljava/util/WeakHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/WeakHashMap<",
            "Ljava/lang/String;",
            "Lee/cyber/smartid/inter/ServiceListener;",
            ">;"
        }
    .end annotation
.end field

.field private final i:Lee/cyber/smartid/inter/ServiceAccess;

.field private j:Lee/cyber/smartid/inter/ListenerAccess;

.field private k:Lee/cyber/smartid/manager/inter/DeleteAccountManager;

.field private l:Lee/cyber/smartid/manager/inter/GetAccountStatusManager;

.field private m:Lee/cyber/smartid/manager/inter/AddAccountManager;

.field private n:Lee/cyber/smartid/manager/inter/StoredAccountManager;

.field private o:Lee/cyber/smartid/manager/inter/TransactionAndRPRequestManager;

.field private final p:Lee/cyber/smartid/manager/inter/PropertiesManager;

.field private final q:Lee/cyber/smartid/manager/inter/TSEListenerToServiceListenerMapper;

.field private final r:Lee/cyber/smartid/manager/impl/TSEErrorToServiceErrorMapperImpl;

.field private s:Lee/cyber/smartid/manager/inter/VerificationCodeEvaluator;

.field private t:Lee/cyber/smartid/manager/inter/VerificationCodeGenerator;

.field private u:Lee/cyber/smartid/manager/inter/TransactionRespMapper;

.field private v:Lee/cyber/smartid/manager/inter/SafetyNetManager;

.field private w:Lee/cyber/smartid/manager/inter/PostInitManager;

.field private final x:Lee/cyber/smartid/tse/inter/AlarmAccess;

.field private final y:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lee/cyber/smartid/tse/inter/AlarmAccess$AlarmHandler;",
            ">;"
        }
    .end annotation
.end field

.field private final z:Lee/cyber/smartid/tse/inter/WallClock;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const-string v0, "SIGNATURE"

    const-string v1, "AUTHENTICATION"

    .line 1
    filled-new-array {v1, v0}, [Ljava/lang/String;

    move-result-object v2

    sput-object v2, Lee/cyber/smartid/SmartIdService;->TRANSACTION_TYPES:[Ljava/lang/String;

    .line 2
    filled-new-array {v1, v0}, [Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lee/cyber/smartid/SmartIdService;->ACCOUNT_KEY_TYPES:[Ljava/lang/String;

    .line 3
    new-instance v0, Ljava/math/BigInteger;

    const-string v1, "65537"

    invoke-direct {v0, v1}, Ljava/math/BigInteger;-><init>(Ljava/lang/String;)V

    sput-object v0, Lee/cyber/smartid/SmartIdService;->a:Ljava/math/BigInteger;

    .line 4
    const-class v0, Lee/cyber/smartid/SmartIdService;

    invoke-static {v0}, Lee/cyber/smartid/util/Log;->getInstance(Ljava/lang/Class;)Lee/cyber/smartid/util/Log;

    move-result-object v0

    sput-object v0, Lee/cyber/smartid/SmartIdService;->c:Lee/cyber/smartid/util/Log;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 9

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    new-instance v0, Ljava/util/WeakHashMap;

    invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V

    iput-object v0, p0, Lee/cyber/smartid/SmartIdService;->h:Ljava/util/WeakHashMap;

    .line 3
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lee/cyber/smartid/SmartIdService;->y:Ljava/util/ArrayList;

    .line 4
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lee/cyber/smartid/SmartIdService;->F:Ljava/lang/Object;

    if-eqz p1, :cond_0

    .line 5
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    iput-object p1, p0, Lee/cyber/smartid/SmartIdService;->d:Landroid/content/Context;

    .line 6
    new-instance p1, Lee/cyber/smartid/SmartIdService$SmartIdServiceAccess;

    const/4 v0, 0x0

    invoke-direct {p1, p0, v0}, Lee/cyber/smartid/SmartIdService$SmartIdServiceAccess;-><init>(Lee/cyber/smartid/SmartIdService;Lee/cyber/smartid/SmartIdService$1;)V

    iput-object p1, p0, Lee/cyber/smartid/SmartIdService;->i:Lee/cyber/smartid/inter/ServiceAccess;

    .line 7
    new-instance p1, Lee/cyber/smartid/SmartIdService$SmartIdListenerAccess;

    invoke-direct {p1, p0, v0}, Lee/cyber/smartid/SmartIdService$SmartIdListenerAccess;-><init>(Lee/cyber/smartid/SmartIdService;Lee/cyber/smartid/SmartIdService$1;)V

    iput-object p1, p0, Lee/cyber/smartid/SmartIdService;->j:Lee/cyber/smartid/inter/ListenerAccess;

    .line 8
    new-instance p1, Lee/cyber/smartid/SmartIdService$SmartIdServiceAlarmAccess;

    invoke-direct {p1, p0, v0}, Lee/cyber/smartid/SmartIdService$SmartIdServiceAlarmAccess;-><init>(Lee/cyber/smartid/SmartIdService;Lee/cyber/smartid/SmartIdService$1;)V

    iput-object p1, p0, Lee/cyber/smartid/SmartIdService;->x:Lee/cyber/smartid/tse/inter/AlarmAccess;

    .line 9
    new-instance p1, Lee/cyber/smartid/SmartIdService$SmartIdServiceWallClock;

    invoke-direct {p1, p0, v0}, Lee/cyber/smartid/SmartIdService$SmartIdServiceWallClock;-><init>(Lee/cyber/smartid/SmartIdService;Lee/cyber/smartid/SmartIdService$1;)V

    iput-object p1, p0, Lee/cyber/smartid/SmartIdService;->z:Lee/cyber/smartid/tse/inter/WallClock;

    .line 10
    invoke-static {}, Lee/cyber/smartid/cryptolib/CryptoLib;->newInstance()Lee/cyber/smartid/cryptolib/CryptoLib;

    move-result-object p1

    iget-object v0, p0, Lee/cyber/smartid/SmartIdService;->d:Landroid/content/Context;

    invoke-virtual {p1, v0}, Lee/cyber/smartid/cryptolib/CryptoLib;->setAndroidDefaults(Landroid/content/Context;)Lee/cyber/smartid/cryptolib/CryptoLib;

    move-result-object p1

    iput-object p1, p0, Lee/cyber/smartid/SmartIdService;->A:Lee/cyber/smartid/cryptolib/CryptoLib;

    .line 11
    new-instance p1, Lee/cyber/smartid/manager/impl/TransactionCacheManagerImpl;

    iget-object v0, p0, Lee/cyber/smartid/SmartIdService;->A:Lee/cyber/smartid/cryptolib/CryptoLib;

    iget-object v1, p0, Lee/cyber/smartid/SmartIdService;->i:Lee/cyber/smartid/inter/ServiceAccess;

    iget-object v2, p0, Lee/cyber/smartid/SmartIdService;->z:Lee/cyber/smartid/tse/inter/WallClock;

    iget-object v3, p0, Lee/cyber/smartid/SmartIdService;->x:Lee/cyber/smartid/tse/inter/AlarmAccess;

    invoke-direct {p1, v0, v1, v2, v3}, Lee/cyber/smartid/manager/impl/TransactionCacheManagerImpl;-><init>(Lee/cyber/smartid/cryptolib/inter/StorageOp;Lee/cyber/smartid/inter/ServiceAccess;Lee/cyber/smartid/tse/inter/WallClock;Lee/cyber/smartid/tse/inter/AlarmAccess;)V

    iput-object p1, p0, Lee/cyber/smartid/SmartIdService;->B:Lee/cyber/smartid/manager/inter/TransactionCacheManager;

    .line 12
    iget-object p1, p0, Lee/cyber/smartid/SmartIdService;->d:Landroid/content/Context;

    iget-object v0, p0, Lee/cyber/smartid/SmartIdService;->i:Lee/cyber/smartid/inter/ServiceAccess;

    iget-object v1, p0, Lee/cyber/smartid/SmartIdService;->x:Lee/cyber/smartid/tse/inter/AlarmAccess;

    iget-object v2, p0, Lee/cyber/smartid/SmartIdService;->z:Lee/cyber/smartid/tse/inter/WallClock;

    const-class v3, Lee/cyber/smartid/tse/SmartIdTSE;

    invoke-static {v3}, Lee/cyber/smartid/util/Log;->getInstance(Ljava/lang/Class;)Lee/cyber/smartid/util/Log;

    move-result-object v3

    invoke-static {p1, v0, v1, v2, v3}, Lee/cyber/smartid/tse/SmartIdTSE;->getInstance(Landroid/content/Context;Lee/cyber/smartid/tse/inter/ExternalResourceAccess;Lee/cyber/smartid/tse/inter/AlarmAccess;Lee/cyber/smartid/tse/inter/WallClock;Lee/cyber/smartid/tse/inter/LogAccess;)Lee/cyber/smartid/tse/SmartIdTSE;

    move-result-object p1

    iput-object p1, p0, Lee/cyber/smartid/SmartIdService;->e:Lee/cyber/smartid/tse/SmartIdTSE;

    .line 13
    new-instance p1, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;

    iget-object v1, p0, Lee/cyber/smartid/SmartIdService;->z:Lee/cyber/smartid/tse/inter/WallClock;

    iget-object v2, p0, Lee/cyber/smartid/SmartIdService;->i:Lee/cyber/smartid/inter/ServiceAccess;

    iget-object v3, p0, Lee/cyber/smartid/SmartIdService;->j:Lee/cyber/smartid/inter/ListenerAccess;

    iget-object v8, p0, Lee/cyber/smartid/SmartIdService;->A:Lee/cyber/smartid/cryptolib/CryptoLib;

    move-object v0, p1

    move-object v4, v8

    move-object v5, v8

    move-object v6, v8

    move-object v7, v8

    invoke-direct/range {v0 .. v8}, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;-><init>(Lee/cyber/smartid/tse/inter/WallClock;Lee/cyber/smartid/inter/ServiceAccess;Lee/cyber/smartid/inter/ListenerAccess;Lee/cyber/smartid/cryptolib/inter/StorageOp;Lee/cyber/smartid/cryptolib/inter/EncodingOp;Lee/cyber/smartid/cryptolib/inter/RandomGenerationOp;Lee/cyber/smartid/cryptolib/inter/KeyGenerationOp;Lee/cyber/smartid/cryptolib/inter/CryptoOp;)V

    iput-object p1, p0, Lee/cyber/smartid/SmartIdService;->H:Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;

    .line 14
    new-instance p1, Lee/cyber/smartid/manager/impl/DeviceFingerprintManagerImpl;

    iget-object v0, p0, Lee/cyber/smartid/SmartIdService;->i:Lee/cyber/smartid/inter/ServiceAccess;

    iget-object v1, p0, Lee/cyber/smartid/SmartIdService;->j:Lee/cyber/smartid/inter/ListenerAccess;

    invoke-direct {p1, v0, v1}, Lee/cyber/smartid/manager/impl/DeviceFingerprintManagerImpl;-><init>(Lee/cyber/smartid/inter/ServiceAccess;Lee/cyber/smartid/inter/ListenerAccess;)V

    iput-object p1, p0, Lee/cyber/smartid/SmartIdService;->C:Lee/cyber/smartid/manager/inter/DeviceFingerprintManager;

    .line 15
    new-instance p1, Lee/cyber/smartid/manager/impl/DevicePropertiesManagerImpl;

    invoke-direct {p1}, Lee/cyber/smartid/manager/impl/DevicePropertiesManagerImpl;-><init>()V

    iput-object p1, p0, Lee/cyber/smartid/SmartIdService;->D:Lee/cyber/smartid/manager/inter/DevicePropertiesManager;

    .line 16
    new-instance p1, Lee/cyber/smartid/manager/impl/PropertiesSourceImpl;

    iget-object v0, p0, Lee/cyber/smartid/SmartIdService;->i:Lee/cyber/smartid/inter/ServiceAccess;

    invoke-direct {p1, v0}, Lee/cyber/smartid/manager/impl/PropertiesSourceImpl;-><init>(Lee/cyber/smartid/inter/ServiceAccess;)V

    iput-object p1, p0, Lee/cyber/smartid/SmartIdService;->E:Lee/cyber/smartid/manager/inter/PropertiesSource;

    .line 17
    iget-object p1, p0, Lee/cyber/smartid/SmartIdService;->i:Lee/cyber/smartid/inter/ServiceAccess;

    invoke-static {p1}, Lee/cyber/smartid/manager/impl/PropertiesManagerImpl;->getInstance(Lee/cyber/smartid/inter/ServiceAccess;)Lee/cyber/smartid/manager/impl/PropertiesManagerImpl;

    move-result-object p1

    iput-object p1, p0, Lee/cyber/smartid/SmartIdService;->p:Lee/cyber/smartid/manager/inter/PropertiesManager;

    .line 18
    new-instance p1, Lee/cyber/smartid/manager/impl/TSEListenerToServiceListenerMapperImpl;

    iget-object v0, p0, Lee/cyber/smartid/SmartIdService;->i:Lee/cyber/smartid/inter/ServiceAccess;

    iget-object v1, p0, Lee/cyber/smartid/SmartIdService;->j:Lee/cyber/smartid/inter/ListenerAccess;

    iget-object v2, p0, Lee/cyber/smartid/SmartIdService;->e:Lee/cyber/smartid/tse/SmartIdTSE;

    invoke-direct {p1, v0, v1, v2}, Lee/cyber/smartid/manager/impl/TSEListenerToServiceListenerMapperImpl;-><init>(Lee/cyber/smartid/inter/ServiceAccess;Lee/cyber/smartid/inter/ListenerAccess;Lee/cyber/smartid/tse/SmartIdTSE;)V

    iput-object p1, p0, Lee/cyber/smartid/SmartIdService;->q:Lee/cyber/smartid/manager/inter/TSEListenerToServiceListenerMapper;

    .line 19
    new-instance p1, Lee/cyber/smartid/manager/impl/TSEErrorToServiceErrorMapperImpl;

    iget-object v0, p0, Lee/cyber/smartid/SmartIdService;->i:Lee/cyber/smartid/inter/ServiceAccess;

    iget-object v1, p0, Lee/cyber/smartid/SmartIdService;->z:Lee/cyber/smartid/tse/inter/WallClock;

    invoke-direct {p1, v0, v1}, Lee/cyber/smartid/manager/impl/TSEErrorToServiceErrorMapperImpl;-><init>(Lee/cyber/smartid/inter/ServiceAccess;Lee/cyber/smartid/tse/inter/WallClock;)V

    iput-object p1, p0, Lee/cyber/smartid/SmartIdService;->r:Lee/cyber/smartid/manager/impl/TSEErrorToServiceErrorMapperImpl;

    return-void

    .line 20
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "Context can\'t be null!"

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method static synthetic A(Lee/cyber/smartid/SmartIdService;)Ljava/lang/Object;
    .locals 0

    .line 1
    iget-object p0, p0, Lee/cyber/smartid/SmartIdService;->F:Ljava/lang/Object;

    return-object p0
.end method

.method static synthetic B(Lee/cyber/smartid/SmartIdService;)Lee/cyber/smartid/inter/ListenerAccess;
    .locals 0

    .line 1
    iget-object p0, p0, Lee/cyber/smartid/SmartIdService;->j:Lee/cyber/smartid/inter/ListenerAccess;

    return-object p0
.end method

.method static synthetic C(Lee/cyber/smartid/SmartIdService;)Lee/cyber/smartid/cryptolib/CryptoLib;
    .locals 0

    .line 1
    iget-object p0, p0, Lee/cyber/smartid/SmartIdService;->A:Lee/cyber/smartid/cryptolib/CryptoLib;

    return-object p0
.end method

.method static synthetic D(Lee/cyber/smartid/SmartIdService;)Lee/cyber/smartid/network/SmartIdAPI;
    .locals 0

    .line 1
    iget-object p0, p0, Lee/cyber/smartid/SmartIdService;->g:Lee/cyber/smartid/network/SmartIdAPI;

    return-object p0
.end method

.method static synthetic E(Lee/cyber/smartid/SmartIdService;)Lee/cyber/smartid/tse/inter/WallClock;
    .locals 0

    .line 1
    iget-object p0, p0, Lee/cyber/smartid/SmartIdService;->z:Lee/cyber/smartid/tse/inter/WallClock;

    return-object p0
.end method

.method static synthetic F(Lee/cyber/smartid/SmartIdService;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lee/cyber/smartid/cryptolib/dto/StorageException;
        }
    .end annotation

    .line 1
    invoke-direct {p0}, Lee/cyber/smartid/SmartIdService;->g()V

    return-void
.end method

.method static synthetic G(Lee/cyber/smartid/SmartIdService;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lee/cyber/smartid/cryptolib/dto/StorageException;
        }
    .end annotation

    .line 1
    invoke-direct {p0}, Lee/cyber/smartid/SmartIdService;->n()V

    return-void
.end method

.method static synthetic a(Lee/cyber/smartid/SmartIdService;I)J
    .locals 0

    .line 29
    invoke-direct {p0, p1}, Lee/cyber/smartid/SmartIdService;->b(I)J

    move-result-wide p0

    return-wide p0
.end method

.method private a(Ljava/lang/String;)J
    .locals 2

    .line 2
    iget-object v0, p0, Lee/cyber/smartid/SmartIdService;->A:Lee/cyber/smartid/cryptolib/CryptoLib;

    new-instance v1, Lee/cyber/smartid/SmartIdService$6;

    invoke-direct {v1, p0}, Lee/cyber/smartid/SmartIdService$6;-><init>(Lee/cyber/smartid/SmartIdService;)V

    .line 3
    invoke-virtual {v1}, Lcom/google/gson/b/a;->getType()Ljava/lang/reflect/Type;

    move-result-object v1

    .line 4
    invoke-virtual {v0, p1, v1}, Lee/cyber/smartid/cryptolib/CryptoLib;->retrieveData(Ljava/lang/String;Ljava/lang/reflect/Type;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Long;

    if-eqz p1, :cond_0

    .line 5
    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    goto :goto_0

    :cond_0
    const-wide/16 v0, 0x0

    :goto_0
    return-wide v0
.end method

.method private a(I)Lee/cyber/smartid/dto/upgrade/InteractiveUpgradeState;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lee/cyber/smartid/cryptolib/dto/StorageException;
        }
    .end annotation

    .line 3
    sget-object v0, Lee/cyber/smartid/SmartIdService;->c:Lee/cyber/smartid/util/Log;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onUpgrade - versionTo: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 4
    iget-object p1, p0, Lee/cyber/smartid/SmartIdService;->A:Lee/cyber/smartid/cryptolib/CryptoLib;

    iget-object v0, p0, Lee/cyber/smartid/SmartIdService;->C:Lee/cyber/smartid/manager/inter/DeviceFingerprintManager;

    invoke-interface {v0}, Lee/cyber/smartid/manager/inter/DeviceFingerprintManager;->getDeviceFingerPrint()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lee/cyber/smartid/manager/impl/DataUpgradeHelper;->onUpgradeToV1(Lee/cyber/smartid/cryptolib/CryptoLib;Ljava/lang/String;)V

    .line 5
    invoke-static {}, Lee/cyber/smartid/dto/upgrade/InteractiveUpgradeState;->forAllNone()Lee/cyber/smartid/dto/upgrade/InteractiveUpgradeState;

    move-result-object p1

    return-object p1

    :cond_0
    const/4 v1, 0x2

    if-ne p1, v1, :cond_1

    .line 6
    iget-object p1, p0, Lee/cyber/smartid/SmartIdService;->d:Landroid/content/Context;

    iget-object v0, p0, Lee/cyber/smartid/SmartIdService;->A:Lee/cyber/smartid/cryptolib/CryptoLib;

    iget-object v1, p0, Lee/cyber/smartid/SmartIdService;->C:Lee/cyber/smartid/manager/inter/DeviceFingerprintManager;

    invoke-interface {v1}, Lee/cyber/smartid/manager/inter/DeviceFingerprintManager;->getDeviceFingerPrint()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, Lee/cyber/smartid/manager/impl/DataUpgradeHelper;->onUpgradeToV2(Landroid/content/Context;Lee/cyber/smartid/cryptolib/CryptoLib;Ljava/lang/String;)V

    .line 7
    invoke-static {}, Lee/cyber/smartid/dto/upgrade/InteractiveUpgradeState;->forAllNone()Lee/cyber/smartid/dto/upgrade/InteractiveUpgradeState;

    move-result-object p1

    return-object p1

    :cond_1
    const/4 v1, 0x3

    if-ne p1, v1, :cond_2

    .line 8
    iget-object p1, p0, Lee/cyber/smartid/SmartIdService;->A:Lee/cyber/smartid/cryptolib/CryptoLib;

    iget-object v0, p0, Lee/cyber/smartid/SmartIdService;->C:Lee/cyber/smartid/manager/inter/DeviceFingerprintManager;

    invoke-interface {v0}, Lee/cyber/smartid/manager/inter/DeviceFingerprintManager;->getDeviceFingerPrint()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lee/cyber/smartid/manager/impl/DataUpgradeHelper;->onUpgradeToV3(Lee/cyber/smartid/cryptolib/CryptoLib;Ljava/lang/String;)V

    .line 9
    invoke-static {}, Lee/cyber/smartid/dto/upgrade/InteractiveUpgradeState;->forAllNone()Lee/cyber/smartid/dto/upgrade/InteractiveUpgradeState;

    move-result-object p1

    return-object p1

    :cond_2
    const/4 v1, 0x4

    if-ne p1, v1, :cond_3

    .line 10
    invoke-static {}, Lee/cyber/smartid/dto/upgrade/InteractiveUpgradeState;->forAllNone()Lee/cyber/smartid/dto/upgrade/InteractiveUpgradeState;

    move-result-object p1

    return-object p1

    :cond_3
    const/4 v1, 0x5

    if-ne p1, v1, :cond_4

    .line 11
    iget-object p1, p0, Lee/cyber/smartid/SmartIdService;->H:Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;

    invoke-virtual {p1}, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->detectInteractiveUpgradeState()Lee/cyber/smartid/dto/upgrade/InteractiveUpgradeState;

    move-result-object p1

    return-object p1

    :cond_4
    const/4 v1, 0x6

    if-ne p1, v1, :cond_5

    .line 12
    iget-object p1, p0, Lee/cyber/smartid/SmartIdService;->A:Lee/cyber/smartid/cryptolib/CryptoLib;

    iget-object v0, p0, Lee/cyber/smartid/SmartIdService;->C:Lee/cyber/smartid/manager/inter/DeviceFingerprintManager;

    invoke-interface {v0}, Lee/cyber/smartid/manager/inter/DeviceFingerprintManager;->getDeviceFingerPrint()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lee/cyber/smartid/manager/impl/DataUpgradeHelper;->onUpgradeToV6(Lee/cyber/smartid/cryptolib/CryptoLib;Ljava/lang/String;)V

    .line 13
    iget-object p1, p0, Lee/cyber/smartid/SmartIdService;->H:Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;

    invoke-virtual {p1}, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->detectInteractiveUpgradeState()Lee/cyber/smartid/dto/upgrade/InteractiveUpgradeState;

    move-result-object p1

    return-object p1

    .line 14
    :cond_5
    new-instance v1, Lee/cyber/smartid/cryptolib/dto/StorageException;

    iget-object v2, p0, Lee/cyber/smartid/SmartIdService;->d:Landroid/content/Context;

    sget v3, Lee/cyber/smartid/R$string;->err_upgrade_to_x_not_supported:I

    new-array v4, v0, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object p1

    const/4 v5, 0x0

    aput-object p1, v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {v1, v0, p1}, Lee/cyber/smartid/cryptolib/dto/StorageException;-><init>(ILjava/lang/String;)V

    throw v1
.end method

.method private a(II)Lee/cyber/smartid/dto/upgrade/InteractiveUpgradeState;
    .locals 11
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lee/cyber/smartid/cryptolib/dto/StorageException;
        }
    .end annotation

    const-string v0, "ee.cyber.smartid.APP_STORAGE_VERSION"

    if-nez p1, :cond_0

    const/4 v1, 0x6

    if-ne p2, v1, :cond_0

    .line 37
    invoke-direct {p0}, Lee/cyber/smartid/SmartIdService;->e()Z

    move-result v1

    if-nez v1, :cond_0

    .line 38
    sget-object p1, Lee/cyber/smartid/SmartIdService;->c:Lee/cyber/smartid/util/Log;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "handleUpgrade: this is a clean install, no need to upgrade, we are at version "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    .line 39
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-direct {p0, v0, p1}, Lee/cyber/smartid/SmartIdService;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 40
    iget-object p1, p0, Lee/cyber/smartid/SmartIdService;->H:Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;

    invoke-virtual {p1, p2}, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->detectInteractiveUpgradeStateForCleanInstall(I)Lee/cyber/smartid/dto/upgrade/InteractiveUpgradeState;

    move-result-object p1

    return-object p1

    :cond_0
    const/4 v1, 0x0

    const/4 v2, 0x2

    const/4 v3, 0x1

    if-ge p1, p2, :cond_4

    .line 41
    sget-object v4, Lee/cyber/smartid/SmartIdService;->c:Lee/cyber/smartid/util/Log;

    new-array v5, v2, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v3

    const-string v6, "handleUpgrade: update loop from version %1$s to version %2$s"

    invoke-static {v6, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    .line 42
    iget-object v4, p0, Lee/cyber/smartid/SmartIdService;->H:Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;

    invoke-virtual {v4}, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->isInteractiveUpgradeStatePresentAndPending()Z

    move-result v4

    const-string v5, "handleUpgrade: stopping data upgrade, we need to do an interactive upgrade .."

    if-eqz v4, :cond_1

    .line 43
    sget-object p1, Lee/cyber/smartid/SmartIdService;->c:Lee/cyber/smartid/util/Log;

    invoke-virtual {p1, v5}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    .line 44
    iget-object p1, p0, Lee/cyber/smartid/SmartIdService;->H:Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;

    invoke-virtual {p1}, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->detectInteractiveUpgradeState()Lee/cyber/smartid/dto/upgrade/InteractiveUpgradeState;

    move-result-object p1

    return-object p1

    .line 45
    :cond_1
    invoke-static {}, Lee/cyber/smartid/dto/upgrade/InteractiveUpgradeState;->forAllSuccess()Lee/cyber/smartid/dto/upgrade/InteractiveUpgradeState;

    move-result-object v4

    move-object v6, v4

    const/4 v4, 0x1

    :goto_0
    sub-int v7, p2, p1

    if-gt v4, v7, :cond_3

    add-int v6, p1, v4

    .line 46
    sget-object v7, Lee/cyber/smartid/SmartIdService;->c:Lee/cyber/smartid/util/Log;

    new-array v8, v2, [Ljava/lang/Object;

    add-int/lit8 v9, v6, -0x1

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v8, v1

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v8, v3

    const-string v10, "handleUpgrade: starting sub-update from version %1$s to version %2$s"

    invoke-static {v10, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    .line 47
    invoke-direct {p0, v6}, Lee/cyber/smartid/SmartIdService;->a(I)Lee/cyber/smartid/dto/upgrade/InteractiveUpgradeState;

    move-result-object v7

    .line 48
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-direct {p0, v0, v8}, Lee/cyber/smartid/SmartIdService;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 49
    sget-object v8, Lee/cyber/smartid/SmartIdService;->c:Lee/cyber/smartid/util/Log;

    new-array v10, v2, [Ljava/lang/Object;

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v10, v1

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v10, v3

    const-string v6, "handleUpgrade: finished sub-update from version %1$s to version %2$s"

    invoke-static {v6, v10}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v8, v6}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    .line 50
    invoke-virtual {v7}, Lee/cyber/smartid/dto/upgrade/InteractiveUpgradeState;->isUpgradePending()Z

    move-result v6

    if-eqz v6, :cond_2

    .line 51
    sget-object p1, Lee/cyber/smartid/SmartIdService;->c:Lee/cyber/smartid/util/Log;

    invoke-virtual {p1, v5}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    move-object v6, v7

    goto :goto_1

    :cond_2
    add-int/lit8 v4, v4, 0x1

    move-object v6, v7

    goto :goto_0

    :cond_3
    :goto_1
    return-object v6

    :cond_4
    if-ne p1, p2, :cond_5

    .line 52
    sget-object p1, Lee/cyber/smartid/SmartIdService;->c:Lee/cyber/smartid/util/Log;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "handleUpgrade: fromVersion and toVersion are the same, no need for upgrade, we are at version "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    .line 53
    iget-object p1, p0, Lee/cyber/smartid/SmartIdService;->H:Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;

    invoke-virtual {p1}, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->detectInteractiveUpgradeState()Lee/cyber/smartid/dto/upgrade/InteractiveUpgradeState;

    move-result-object p1

    return-object p1

    .line 54
    :cond_5
    sget-object v0, Lee/cyber/smartid/SmartIdService;->c:Lee/cyber/smartid/util/Log;

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    aput-object p2, v2, v1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v2, v3

    const-string p1, "handleUpgrade: toVersion %1$s lower than fromVersion %2$s"

    invoke-static {p1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lee/cyber/smartid/util/Log;->w(Ljava/lang/String;)V

    .line 55
    new-instance p1, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;

    iget-object p2, p0, Lee/cyber/smartid/SmartIdService;->d:Landroid/content/Context;

    sget v0, Lee/cyber/smartid/R$string;->err_storage_version_downgrade_is_not_supported_use_a_clean_install:I

    invoke-virtual {p2, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p2

    const/16 v0, 0x84

    invoke-direct {p1, v0, p2}, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;-><init>(ILjava/lang/String;)V

    throw p1
.end method

.method static synthetic a(Lee/cyber/smartid/SmartIdService;Ljava/lang/String;ZLjava/lang/Class;)Lee/cyber/smartid/inter/ServiceListener;
    .locals 0

    .line 12
    invoke-direct {p0, p1, p2, p3}, Lee/cyber/smartid/SmartIdService;->a(Ljava/lang/String;ZLjava/lang/Class;)Lee/cyber/smartid/inter/ServiceListener;

    move-result-object p0

    return-object p0
.end method

.method private a(Ljava/lang/String;ZLjava/lang/Class;)Lee/cyber/smartid/inter/ServiceListener;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lee/cyber/smartid/inter/ServiceListener;",
            ">(",
            "Ljava/lang/String;",
            "Z",
            "Ljava/lang/Class<",
            "TT;>;)TT;"
        }
    .end annotation

    .line 31
    sget-object v0, Lee/cyber/smartid/SmartIdService;->c:Lee/cyber/smartid/util/Log;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getListener: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, ", forgetListener: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    .line 32
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_3

    if-nez p3, :cond_0

    goto :goto_1

    .line 33
    :cond_0
    iget-object v0, p0, Lee/cyber/smartid/SmartIdService;->h:Ljava/util/WeakHashMap;

    monitor-enter v0

    .line 34
    :try_start_0
    iget-object v2, p0, Lee/cyber/smartid/SmartIdService;->h:Ljava/util/WeakHashMap;

    invoke-virtual {v2, p1}, Ljava/util/WeakHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lee/cyber/smartid/SmartIdService;->h:Ljava/util/WeakHashMap;

    invoke-virtual {v2, p1}, Ljava/util/WeakHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {p3, v2}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result p3

    if-eqz p3, :cond_2

    if-eqz p2, :cond_1

    .line 35
    iget-object p2, p0, Lee/cyber/smartid/SmartIdService;->h:Ljava/util/WeakHashMap;

    invoke-virtual {p2, p1}, Ljava/util/WeakHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    goto :goto_0

    :cond_1
    iget-object p2, p0, Lee/cyber/smartid/SmartIdService;->h:Ljava/util/WeakHashMap;

    invoke-virtual {p2, p1}, Ljava/util/WeakHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    :goto_0
    check-cast p1, Lee/cyber/smartid/inter/ServiceListener;

    monitor-exit v0

    return-object p1

    .line 36
    :cond_2
    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception p1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1

    :cond_3
    :goto_1
    return-object v1
.end method

.method static synthetic a(Lee/cyber/smartid/SmartIdService;Lee/cyber/smartid/manager/inter/AddAccountManager;)Lee/cyber/smartid/manager/inter/AddAccountManager;
    .locals 0

    .line 10
    iput-object p1, p0, Lee/cyber/smartid/SmartIdService;->m:Lee/cyber/smartid/manager/inter/AddAccountManager;

    return-object p1
.end method

.method static synthetic a(Lee/cyber/smartid/SmartIdService;Lee/cyber/smartid/manager/inter/DeleteAccountManager;)Lee/cyber/smartid/manager/inter/DeleteAccountManager;
    .locals 0

    .line 8
    iput-object p1, p0, Lee/cyber/smartid/SmartIdService;->k:Lee/cyber/smartid/manager/inter/DeleteAccountManager;

    return-object p1
.end method

.method static synthetic a(Lee/cyber/smartid/SmartIdService;)Lee/cyber/smartid/manager/inter/DeviceFingerprintManager;
    .locals 0

    .line 1
    iget-object p0, p0, Lee/cyber/smartid/SmartIdService;->C:Lee/cyber/smartid/manager/inter/DeviceFingerprintManager;

    return-object p0
.end method

.method static synthetic a(Lee/cyber/smartid/SmartIdService;Lee/cyber/smartid/manager/inter/GetAccountStatusManager;)Lee/cyber/smartid/manager/inter/GetAccountStatusManager;
    .locals 0

    .line 22
    iput-object p1, p0, Lee/cyber/smartid/SmartIdService;->l:Lee/cyber/smartid/manager/inter/GetAccountStatusManager;

    return-object p1
.end method

.method static synthetic a(Lee/cyber/smartid/SmartIdService;Lee/cyber/smartid/manager/inter/PostInitManager;)Lee/cyber/smartid/manager/inter/PostInitManager;
    .locals 0

    .line 9
    iput-object p1, p0, Lee/cyber/smartid/SmartIdService;->w:Lee/cyber/smartid/manager/inter/PostInitManager;

    return-object p1
.end method

.method static synthetic a(Lee/cyber/smartid/SmartIdService;Lee/cyber/smartid/manager/inter/SafetyNetManager;)Lee/cyber/smartid/manager/inter/SafetyNetManager;
    .locals 0

    .line 3
    iput-object p1, p0, Lee/cyber/smartid/SmartIdService;->v:Lee/cyber/smartid/manager/inter/SafetyNetManager;

    return-object p1
.end method

.method static synthetic a(Lee/cyber/smartid/SmartIdService;Lee/cyber/smartid/manager/inter/StoredAccountManager;)Lee/cyber/smartid/manager/inter/StoredAccountManager;
    .locals 0

    .line 7
    iput-object p1, p0, Lee/cyber/smartid/SmartIdService;->n:Lee/cyber/smartid/manager/inter/StoredAccountManager;

    return-object p1
.end method

.method static synthetic a(Lee/cyber/smartid/SmartIdService;Lee/cyber/smartid/manager/inter/TransactionAndRPRequestManager;)Lee/cyber/smartid/manager/inter/TransactionAndRPRequestManager;
    .locals 0

    .line 23
    iput-object p1, p0, Lee/cyber/smartid/SmartIdService;->o:Lee/cyber/smartid/manager/inter/TransactionAndRPRequestManager;

    return-object p1
.end method

.method static synthetic a(Lee/cyber/smartid/SmartIdService;Lee/cyber/smartid/manager/inter/TransactionRespMapper;)Lee/cyber/smartid/manager/inter/TransactionRespMapper;
    .locals 0

    .line 4
    iput-object p1, p0, Lee/cyber/smartid/SmartIdService;->u:Lee/cyber/smartid/manager/inter/TransactionRespMapper;

    return-object p1
.end method

.method static synthetic a(Lee/cyber/smartid/SmartIdService;Lee/cyber/smartid/manager/inter/VerificationCodeEvaluator;)Lee/cyber/smartid/manager/inter/VerificationCodeEvaluator;
    .locals 0

    .line 5
    iput-object p1, p0, Lee/cyber/smartid/SmartIdService;->s:Lee/cyber/smartid/manager/inter/VerificationCodeEvaluator;

    return-object p1
.end method

.method static synthetic a(Lee/cyber/smartid/SmartIdService;Lee/cyber/smartid/manager/inter/VerificationCodeGenerator;)Lee/cyber/smartid/manager/inter/VerificationCodeGenerator;
    .locals 0

    .line 6
    iput-object p1, p0, Lee/cyber/smartid/SmartIdService;->t:Lee/cyber/smartid/manager/inter/VerificationCodeGenerator;

    return-object p1
.end method

.method static synthetic a(Lee/cyber/smartid/SmartIdService;Lee/cyber/smartid/network/SmartIdAPI;)Lee/cyber/smartid/network/SmartIdAPI;
    .locals 0

    .line 21
    iput-object p1, p0, Lee/cyber/smartid/SmartIdService;->g:Lee/cyber/smartid/network/SmartIdAPI;

    return-object p1
.end method

.method static synthetic a()Lee/cyber/smartid/util/Log;
    .locals 1

    .line 11
    sget-object v0, Lee/cyber/smartid/SmartIdService;->c:Lee/cyber/smartid/util/Log;

    return-object v0
.end method

.method static synthetic a(Lee/cyber/smartid/SmartIdService;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .line 2
    invoke-direct {p0, p1, p2}, Lee/cyber/smartid/SmartIdService;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method static synthetic a(Lee/cyber/smartid/SmartIdService;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .line 26
    invoke-direct {p0, p1, p2, p3}, Lee/cyber/smartid/SmartIdService;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    const/4 v0, 0x3

    .line 80
    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p2, v0, v1

    const/4 p2, 0x1

    aput-object p1, v0, p2

    iget-object p1, p0, Lee/cyber/smartid/SmartIdService;->C:Lee/cyber/smartid/manager/inter/DeviceFingerprintManager;

    invoke-interface {p1}, Lee/cyber/smartid/manager/inter/DeviceFingerprintManager;->getDeviceFingerPrint()Ljava/lang/String;

    move-result-object p1

    const/4 p2, 0x2

    aput-object p1, v0, p2

    const-string p1, "ee.cyber.smartid.APP_KEY_DATA_%1$s_%2$s_%3$s"

    invoke-static {p1, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .line 106
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "_"

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private a(J)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lee/cyber/smartid/cryptolib/dto/StorageException;
        }
    .end annotation

    const-string v0, "ee.cyber.smartid.TAG_UPDATE_DEVICE_CURRENT_RETRY_COUNT"

    .line 79
    invoke-direct {p0, v0, p1, p2}, Lee/cyber/smartid/SmartIdService;->a(Ljava/lang/String;J)V

    return-void
.end method

.method static synthetic a(Landroid/content/Context;Ljava/lang/String;JLjava/lang/String;)V
    .locals 0

    .line 17
    invoke-static {p0, p1, p2, p3, p4}, Lee/cyber/smartid/SmartIdService;->b(Landroid/content/Context;Ljava/lang/String;JLjava/lang/String;)V

    return-void
.end method

.method static synthetic a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 18
    invoke-static {p0, p1, p2}, Lee/cyber/smartid/SmartIdService;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic a(Lee/cyber/smartid/SmartIdService;Lee/cyber/smartid/dto/SmartIdError;)V
    .locals 0

    .line 15
    invoke-direct {p0, p1}, Lee/cyber/smartid/SmartIdService;->a(Lee/cyber/smartid/dto/SmartIdError;)V

    return-void
.end method

.method static synthetic a(Lee/cyber/smartid/SmartIdService;Lee/cyber/smartid/dto/jsonrpc/param/RegisterDeviceData;Ljava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lee/cyber/smartid/cryptolib/dto/StorageException;
        }
    .end annotation

    .line 2
    invoke-direct {p0, p1, p2}, Lee/cyber/smartid/SmartIdService;->a(Lee/cyber/smartid/dto/jsonrpc/param/RegisterDeviceData;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic a(Lee/cyber/smartid/SmartIdService;Lee/cyber/smartid/tse/dto/jsonrpc/resp/EncryptKeyResp;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 24
    invoke-direct {p0, p1, p2, p3, p4}, Lee/cyber/smartid/SmartIdService;->a(Lee/cyber/smartid/tse/dto/jsonrpc/resp/EncryptKeyResp;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic a(Lee/cyber/smartid/SmartIdService;Ljava/lang/Runnable;)V
    .locals 0

    .line 13
    invoke-direct {p0, p1}, Lee/cyber/smartid/SmartIdService;->a(Ljava/lang/Runnable;)V

    return-void
.end method

.method static synthetic a(Lee/cyber/smartid/SmartIdService;Ljava/lang/String;Lee/cyber/smartid/inter/ServiceListener;Lee/cyber/smartid/dto/SmartIdError;)V
    .locals 0

    .line 14
    invoke-direct {p0, p1, p2, p3}, Lee/cyber/smartid/SmartIdService;->a(Ljava/lang/String;Lee/cyber/smartid/inter/ServiceListener;Lee/cyber/smartid/dto/SmartIdError;)V

    return-void
.end method

.method static synthetic a(Lee/cyber/smartid/SmartIdService;Ljava/lang/String;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lee/cyber/smartid/cryptolib/dto/StorageException;
        }
    .end annotation

    .line 20
    invoke-direct {p0, p1, p2}, Lee/cyber/smartid/SmartIdService;->a(Ljava/lang/String;Ljava/lang/Object;)V

    return-void
.end method

.method static synthetic a(Lee/cyber/smartid/SmartIdService;Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/inter/ServiceListener;)V
    .locals 0

    .line 27
    invoke-direct {p0, p1, p2, p3}, Lee/cyber/smartid/SmartIdService;->a(Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/inter/ServiceListener;)V

    return-void
.end method

.method static synthetic a(Lee/cyber/smartid/SmartIdService;Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/inter/StorePushNotificationDataListener;)V
    .locals 0

    .line 25
    invoke-direct {p0, p1, p2, p3}, Lee/cyber/smartid/SmartIdService;->a(Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/inter/StorePushNotificationDataListener;)V

    return-void
.end method

.method static synthetic a(Lee/cyber/smartid/SmartIdService;Ljava/lang/String;Ljava/lang/String;ZZ)V
    .locals 0

    .line 16
    invoke-direct {p0, p1, p2, p3, p4}, Lee/cyber/smartid/SmartIdService;->a(Ljava/lang/String;Ljava/lang/String;ZZ)V

    return-void
.end method

.method static synthetic a(Lee/cyber/smartid/SmartIdService;Z)V
    .locals 0

    .line 19
    invoke-direct {p0, p1}, Lee/cyber/smartid/SmartIdService;->a(Z)V

    return-void
.end method

.method private a(Lee/cyber/smartid/dto/SmartIdError;)V
    .locals 1

    .line 237
    new-instance v0, Lee/cyber/smartid/SmartIdService$40;

    invoke-direct {v0, p0, p1}, Lee/cyber/smartid/SmartIdService$40;-><init>(Lee/cyber/smartid/SmartIdService;Lee/cyber/smartid/dto/SmartIdError;)V

    invoke-direct {p0, v0}, Lee/cyber/smartid/SmartIdService;->a(Ljava/lang/Runnable;)V

    return-void
.end method

.method private a(Lee/cyber/smartid/dto/jsonrpc/param/RegisterDeviceData;)V
    .locals 3

    .line 60
    new-instance v0, Lee/cyber/smartid/dto/jsonrpc/DeviceProperties;

    invoke-direct {v0}, Lee/cyber/smartid/dto/jsonrpc/DeviceProperties;-><init>()V

    .line 61
    iget-object v1, p0, Lee/cyber/smartid/SmartIdService;->D:Lee/cyber/smartid/manager/inter/DevicePropertiesManager;

    invoke-interface {v1}, Lee/cyber/smartid/manager/inter/DevicePropertiesManager;->getDeviceBuildVersion()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lee/cyber/smartid/dto/jsonrpc/DeviceProperties;->setPlatformVersion(Ljava/lang/String;)V

    .line 62
    iget-object v1, p0, Lee/cyber/smartid/SmartIdService;->C:Lee/cyber/smartid/manager/inter/DeviceFingerprintManager;

    invoke-interface {v1}, Lee/cyber/smartid/manager/inter/DeviceFingerprintManager;->getDeviceFingerPrint()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lee/cyber/smartid/dto/jsonrpc/DeviceProperties;->setDeviceFingerprint(Ljava/lang/String;)V

    .line 63
    iget-object v1, p0, Lee/cyber/smartid/SmartIdService;->D:Lee/cyber/smartid/manager/inter/DevicePropertiesManager;

    invoke-interface {v1}, Lee/cyber/smartid/manager/inter/DevicePropertiesManager;->getDeviceApiVersion()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lee/cyber/smartid/dto/jsonrpc/DeviceProperties;->setApiVersion(Ljava/lang/String;)V

    .line 64
    iget-object v1, p0, Lee/cyber/smartid/SmartIdService;->D:Lee/cyber/smartid/manager/inter/DevicePropertiesManager;

    invoke-interface {v1}, Lee/cyber/smartid/manager/inter/DevicePropertiesManager;->getDeviceModel()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lee/cyber/smartid/dto/jsonrpc/DeviceProperties;->setModelName(Ljava/lang/String;)V

    .line 65
    iget-object v1, p0, Lee/cyber/smartid/SmartIdService;->D:Lee/cyber/smartid/manager/inter/DevicePropertiesManager;

    invoke-interface {v1}, Lee/cyber/smartid/manager/inter/DevicePropertiesManager;->getDeviceManufacturer()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lee/cyber/smartid/dto/jsonrpc/DeviceProperties;->setManufacturer(Ljava/lang/String;)V

    .line 66
    iget-object v1, p0, Lee/cyber/smartid/SmartIdService;->D:Lee/cyber/smartid/manager/inter/DevicePropertiesManager;

    invoke-interface {v1}, Lee/cyber/smartid/manager/inter/DevicePropertiesManager;->getDeviceCodeName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lee/cyber/smartid/dto/jsonrpc/DeviceProperties;->setDeviceCodeName(Ljava/lang/String;)V

    .line 67
    iget-object v1, p0, Lee/cyber/smartid/SmartIdService;->D:Lee/cyber/smartid/manager/inter/DevicePropertiesManager;

    invoke-interface {v1}, Lee/cyber/smartid/manager/inter/DevicePropertiesManager;->getProductCodeName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lee/cyber/smartid/dto/jsonrpc/DeviceProperties;->setProductCodeName(Ljava/lang/String;)V

    .line 68
    iget-object v1, p0, Lee/cyber/smartid/SmartIdService;->D:Lee/cyber/smartid/manager/inter/DevicePropertiesManager;

    iget-object v2, p0, Lee/cyber/smartid/SmartIdService;->d:Landroid/content/Context;

    invoke-interface {v1, v2}, Lee/cyber/smartid/manager/inter/DevicePropertiesManager;->getDeviceScreenHeightInPx(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lee/cyber/smartid/dto/jsonrpc/DeviceProperties;->setScreenHeightPx(Ljava/lang/String;)V

    .line 69
    iget-object v1, p0, Lee/cyber/smartid/SmartIdService;->D:Lee/cyber/smartid/manager/inter/DevicePropertiesManager;

    iget-object v2, p0, Lee/cyber/smartid/SmartIdService;->d:Landroid/content/Context;

    invoke-interface {v1, v2}, Lee/cyber/smartid/manager/inter/DevicePropertiesManager;->getDeviceScreenWidthInPx(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lee/cyber/smartid/dto/jsonrpc/DeviceProperties;->setScreenWidthPx(Ljava/lang/String;)V

    .line 70
    iget-object v1, p0, Lee/cyber/smartid/SmartIdService;->D:Lee/cyber/smartid/manager/inter/DevicePropertiesManager;

    iget-object v2, p0, Lee/cyber/smartid/SmartIdService;->d:Landroid/content/Context;

    invoke-interface {v1, v2}, Lee/cyber/smartid/manager/inter/DevicePropertiesManager;->getDeviceScreenDensityInDpi(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lee/cyber/smartid/dto/jsonrpc/DeviceProperties;->setScreenDensity(Ljava/lang/String;)V

    .line 71
    iget-object v1, p0, Lee/cyber/smartid/SmartIdService;->D:Lee/cyber/smartid/manager/inter/DevicePropertiesManager;

    iget-object v2, p0, Lee/cyber/smartid/SmartIdService;->d:Landroid/content/Context;

    invoke-interface {v1, v2}, Lee/cyber/smartid/manager/inter/DevicePropertiesManager;->getDeviceCapabilityList(Landroid/content/Context;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v0, v1}, Lee/cyber/smartid/dto/jsonrpc/DeviceProperties;->setCapabilityList(Ljava/util/ArrayList;)V

    .line 72
    iget-object v1, p0, Lee/cyber/smartid/SmartIdService;->e:Lee/cyber/smartid/tse/SmartIdTSE;

    invoke-virtual {v1}, Lee/cyber/smartid/tse/SmartIdTSE;->getPRNGTestResult()[Lee/cyber/smartid/cryptolib/dto/TestResult;

    move-result-object v1

    invoke-virtual {v0, v1}, Lee/cyber/smartid/dto/jsonrpc/DeviceProperties;->setPRNGTestResult([Lee/cyber/smartid/cryptolib/dto/TestResult;)V

    .line 73
    iget-object v1, p0, Lee/cyber/smartid/SmartIdService;->v:Lee/cyber/smartid/manager/inter/SafetyNetManager;

    invoke-interface {v1}, Lee/cyber/smartid/manager/inter/SafetyNetManager;->getLastSafetyNetAttestation()Lee/cyber/smartid/dto/SafetyNetAttestation;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lee/cyber/smartid/SmartIdService;->v:Lee/cyber/smartid/manager/inter/SafetyNetManager;

    invoke-interface {v1}, Lee/cyber/smartid/manager/inter/SafetyNetManager;->getLastSafetyNetAttestation()Lee/cyber/smartid/dto/SafetyNetAttestation;

    move-result-object v1

    invoke-virtual {v1}, Lee/cyber/smartid/dto/SafetyNetAttestation;->getPayloadJWS()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {v0, v1}, Lee/cyber/smartid/dto/jsonrpc/DeviceProperties;->setSafetyNetAttestationResult(Ljava/lang/String;)V

    .line 74
    invoke-virtual {p1, v0}, Lee/cyber/smartid/dto/jsonrpc/param/RegisterDeviceData;->setProperties(Lee/cyber/smartid/dto/jsonrpc/DeviceProperties;)V

    .line 75
    iget-object v0, p0, Lee/cyber/smartid/SmartIdService;->i:Lee/cyber/smartid/inter/ServiceAccess;

    invoke-interface {v0}, Lee/cyber/smartid/tse/inter/ExternalResourceAccess;->getPlatform()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lee/cyber/smartid/dto/jsonrpc/param/RegisterDeviceData;->setPlatform(Ljava/lang/String;)V

    return-void
.end method

.method private a(Lee/cyber/smartid/dto/jsonrpc/param/RegisterDeviceData;Ljava/lang/String;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lee/cyber/smartid/cryptolib/dto/StorageException;
        }
    .end annotation

    .line 165
    iget-object v0, p0, Lee/cyber/smartid/SmartIdService;->v:Lee/cyber/smartid/manager/inter/SafetyNetManager;

    iget-object v1, p0, Lee/cyber/smartid/SmartIdService;->A:Lee/cyber/smartid/cryptolib/CryptoLib;

    invoke-virtual {p1}, Lee/cyber/smartid/dto/jsonrpc/param/RegisterDeviceData;->getProperties()Lee/cyber/smartid/dto/jsonrpc/DeviceProperties;

    move-result-object v2

    invoke-virtual {v2}, Lee/cyber/smartid/dto/jsonrpc/DeviceProperties;->getSafetyNetAttestationResult()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v1, v2, p2}, Lee/cyber/smartid/util/Util;->calculateSHA1FromSafetyNetJWSMeaning(Lee/cyber/smartid/cryptolib/inter/EncodingOp;Lee/cyber/smartid/cryptolib/inter/CryptoOp;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    invoke-interface {v0, p2}, Lee/cyber/smartid/manager/inter/SafetyNetManager;->setSafetyNetResultHash(Ljava/lang/String;)V

    .line 166
    invoke-direct {p0, p1}, Lee/cyber/smartid/SmartIdService;->b(Lee/cyber/smartid/dto/jsonrpc/param/RegisterDeviceData;)Ljava/lang/String;

    move-result-object p2

    invoke-direct {p0, p2}, Lee/cyber/smartid/SmartIdService;->f(Ljava/lang/String;)V

    .line 167
    invoke-virtual {p1}, Lee/cyber/smartid/dto/jsonrpc/param/RegisterDeviceData;->getPushNotificationToken()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p1}, Lee/cyber/smartid/SmartIdService;->e(Ljava/lang/String;)V

    return-void
.end method

.method private a(Lee/cyber/smartid/tse/dto/jsonrpc/resp/EncryptKeyResp;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 8

    .line 58
    sget-object v0, Lee/cyber/smartid/SmartIdService;->c:Lee/cyber/smartid/util/Log;

    const-string v1, "encryptKeySignInternal"

    invoke-virtual {v0, v1}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    .line 59
    iget-object v2, p0, Lee/cyber/smartid/SmartIdService;->e:Lee/cyber/smartid/tse/SmartIdTSE;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v0, p0, Lee/cyber/smartid/SmartIdService;->p:Lee/cyber/smartid/manager/inter/PropertiesManager;

    invoke-interface {v0}, Lee/cyber/smartid/manager/inter/PropertiesManager;->getPropAccountRegistrationSZId()Ljava/lang/String;

    move-result-object v6

    new-instance v7, Lee/cyber/smartid/SmartIdService$5;

    invoke-direct {v7, p0, p2, p1}, Lee/cyber/smartid/SmartIdService$5;-><init>(Lee/cyber/smartid/SmartIdService;Ljava/lang/String;Lee/cyber/smartid/tse/dto/jsonrpc/resp/EncryptKeyResp;)V

    move-object v4, p3

    move-object v5, p4

    invoke-virtual/range {v2 .. v7}, Lee/cyber/smartid/tse/SmartIdTSE;->encryptKey(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/tse/inter/EncryptKeyListener;)V

    return-void
.end method

.method private a(Ljava/lang/Runnable;)V
    .locals 2

    .line 81
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 82
    invoke-virtual {v0, p1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method private a(Ljava/lang/String;J)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lee/cyber/smartid/cryptolib/dto/StorageException;
        }
    .end annotation

    .line 78
    iget-object v0, p0, Lee/cyber/smartid/SmartIdService;->A:Lee/cyber/smartid/cryptolib/CryptoLib;

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p2

    invoke-virtual {v0, p1, p2}, Lee/cyber/smartid/cryptolib/CryptoLib;->storeData(Ljava/lang/String;Ljava/lang/Object;)V

    return-void
.end method

.method private a(Ljava/lang/String;Lee/cyber/smartid/inter/ServiceListener;Lee/cyber/smartid/dto/SmartIdError;)V
    .locals 11

    const-string v0, "notifyError"

    if-eqz p3, :cond_0

    goto :goto_0

    .line 170
    :cond_0
    iget-object p3, p0, Lee/cyber/smartid/SmartIdService;->z:Lee/cyber/smartid/tse/inter/WallClock;

    iget-object v1, p0, Lee/cyber/smartid/SmartIdService;->d:Landroid/content/Context;

    sget v2, Lee/cyber/smartid/R$string;->err_unknown:I

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-wide/16 v2, 0x0

    invoke-static {p3, v2, v3, v1}, Lee/cyber/smartid/dto/SmartIdError;->from(Lee/cyber/smartid/tse/inter/WallClock;JLjava/lang/String;)Lee/cyber/smartid/dto/SmartIdError;

    move-result-object p3

    .line 171
    :goto_0
    instance-of v1, p2, Lee/cyber/smartid/inter/ConfirmTransactionListener;

    if-eqz v1, :cond_1

    .line 172
    check-cast p2, Lee/cyber/smartid/inter/ConfirmTransactionListener;

    invoke-interface {p2, p1, p3}, Lee/cyber/smartid/inter/ConfirmTransactionListener;->onConfirmTransactionFailed(Ljava/lang/String;Lee/cyber/smartid/dto/SmartIdError;)V

    goto/16 :goto_4

    .line 173
    :cond_1
    instance-of v1, p2, Lee/cyber/smartid/inter/CancelTransactionListener;

    if-eqz v1, :cond_2

    .line 174
    check-cast p2, Lee/cyber/smartid/inter/CancelTransactionListener;

    invoke-interface {p2, p1, p3}, Lee/cyber/smartid/inter/CancelTransactionListener;->onCancelTransactionFailed(Ljava/lang/String;Lee/cyber/smartid/dto/SmartIdError;)V

    goto/16 :goto_4

    .line 175
    :cond_2
    instance-of v1, p2, Lee/cyber/smartid/inter/RefreshCloneDetectionListener;

    if-eqz v1, :cond_3

    .line 176
    check-cast p2, Lee/cyber/smartid/inter/RefreshCloneDetectionListener;

    invoke-interface {p2, p1, p3}, Lee/cyber/smartid/inter/RefreshCloneDetectionListener;->onRefreshCloneDetectionFailed(Ljava/lang/String;Lee/cyber/smartid/dto/SmartIdError;)V

    goto/16 :goto_4

    .line 177
    :cond_3
    instance-of v1, p2, Lee/cyber/smartid/inter/AddAccountListener;

    if-eqz v1, :cond_4

    .line 178
    check-cast p2, Lee/cyber/smartid/inter/AddAccountListener;

    invoke-interface {p2, p1, p3}, Lee/cyber/smartid/inter/AddAccountListener;->onAddAccountFailed(Ljava/lang/String;Lee/cyber/smartid/dto/SmartIdError;)V

    goto/16 :goto_4

    .line 179
    :cond_4
    instance-of v1, p2, Lee/cyber/smartid/inter/GenerateKeysListener;

    if-eqz v1, :cond_5

    .line 180
    check-cast p2, Lee/cyber/smartid/inter/GenerateKeysListener;

    invoke-interface {p2, p1, p3}, Lee/cyber/smartid/inter/GenerateKeysListener;->onGenerateKeysFailed(Ljava/lang/String;Lee/cyber/smartid/dto/SmartIdError;)V

    goto/16 :goto_4

    .line 181
    :cond_5
    instance-of v1, p2, Lee/cyber/smartid/inter/GetAccountsListener;

    if-eqz v1, :cond_6

    .line 182
    check-cast p2, Lee/cyber/smartid/inter/GetAccountsListener;

    invoke-interface {p2, p1, p3}, Lee/cyber/smartid/inter/GetAccountsListener;->onGetAccountsFailed(Ljava/lang/String;Lee/cyber/smartid/dto/SmartIdError;)V

    goto/16 :goto_4

    .line 183
    :cond_6
    instance-of v1, p2, Lee/cyber/smartid/inter/GetPendingOperationListener;

    if-eqz v1, :cond_7

    .line 184
    check-cast p2, Lee/cyber/smartid/inter/GetPendingOperationListener;

    invoke-interface {p2, p1, p3}, Lee/cyber/smartid/inter/GetPendingOperationListener;->onGetPendingOperationFailed(Ljava/lang/String;Lee/cyber/smartid/dto/SmartIdError;)V

    goto/16 :goto_4

    .line 185
    :cond_7
    instance-of v1, p2, Lee/cyber/smartid/inter/GetTransactionListener;

    if-eqz v1, :cond_8

    .line 186
    check-cast p2, Lee/cyber/smartid/inter/GetTransactionListener;

    invoke-interface {p2, p1, p3}, Lee/cyber/smartid/inter/GetTransactionListener;->onGetTransactionFailed(Ljava/lang/String;Lee/cyber/smartid/dto/SmartIdError;)V

    goto/16 :goto_4

    .line 187
    :cond_8
    instance-of v1, p2, Lee/cyber/smartid/inter/InitServiceListener;

    if-eqz v1, :cond_9

    .line 188
    check-cast p2, Lee/cyber/smartid/inter/InitServiceListener;

    invoke-interface {p2, p1, p3}, Lee/cyber/smartid/inter/InitServiceListener;->onInitServiceFailed(Ljava/lang/String;Lee/cyber/smartid/dto/SmartIdError;)V

    goto/16 :goto_4

    .line 189
    :cond_9
    instance-of v1, p2, Lee/cyber/smartid/inter/StorePushNotificationDataListener;

    if-eqz v1, :cond_a

    .line 190
    check-cast p2, Lee/cyber/smartid/inter/StorePushNotificationDataListener;

    invoke-interface {p2, p1, p3}, Lee/cyber/smartid/inter/StorePushNotificationDataListener;->onStorePushNotificationDataFailed(Ljava/lang/String;Lee/cyber/smartid/dto/SmartIdError;)V

    goto/16 :goto_4

    .line 191
    :cond_a
    instance-of v1, p2, Lee/cyber/smartid/inter/UpdateDeviceListener;

    if-eqz v1, :cond_b

    .line 192
    check-cast p2, Lee/cyber/smartid/inter/UpdateDeviceListener;

    invoke-interface {p2, p1, p3}, Lee/cyber/smartid/inter/UpdateDeviceListener;->onUpdateDeviceFailed(Ljava/lang/String;Lee/cyber/smartid/dto/SmartIdError;)V

    goto/16 :goto_4

    .line 193
    :cond_b
    instance-of v1, p2, Lee/cyber/smartid/inter/DeleteAccountListener;

    if-eqz v1, :cond_c

    .line 194
    check-cast p2, Lee/cyber/smartid/inter/DeleteAccountListener;

    invoke-interface {p2, p1, p3}, Lee/cyber/smartid/inter/DeleteAccountListener;->onDeleteAccountFailed(Ljava/lang/String;Lee/cyber/smartid/dto/SmartIdError;)V

    goto/16 :goto_4

    .line 195
    :cond_c
    instance-of v1, p2, Lee/cyber/smartid/inter/GetRPRequestListener;

    if-eqz v1, :cond_d

    .line 196
    check-cast p2, Lee/cyber/smartid/inter/GetRPRequestListener;

    invoke-interface {p2, p1, p3}, Lee/cyber/smartid/inter/GetRPRequestListener;->onGetRPRequestFailed(Ljava/lang/String;Lee/cyber/smartid/dto/SmartIdError;)V

    goto/16 :goto_4

    .line 197
    :cond_d
    instance-of v1, p2, Lee/cyber/smartid/inter/ConfirmRPRequestListener;

    if-eqz v1, :cond_e

    .line 198
    check-cast p2, Lee/cyber/smartid/inter/ConfirmRPRequestListener;

    invoke-interface {p2, p1, p3}, Lee/cyber/smartid/inter/ConfirmRPRequestListener;->onConfirmRPRequestFailed(Ljava/lang/String;Lee/cyber/smartid/dto/SmartIdError;)V

    goto/16 :goto_4

    .line 199
    :cond_e
    instance-of v1, p2, Lee/cyber/smartid/inter/GetKeyPinLengthListener;

    if-eqz v1, :cond_f

    .line 200
    check-cast p2, Lee/cyber/smartid/inter/GetKeyPinLengthListener;

    invoke-interface {p2, p1, p3}, Lee/cyber/smartid/inter/GetKeyPinLengthListener;->onGetKeyPinLengthFailed(Ljava/lang/String;Lee/cyber/smartid/dto/SmartIdError;)V

    goto/16 :goto_4

    .line 201
    :cond_f
    instance-of v1, p2, Lee/cyber/smartid/inter/GetAccountStatusListener;

    if-eqz v1, :cond_10

    .line 202
    check-cast p2, Lee/cyber/smartid/inter/GetAccountStatusListener;

    invoke-interface {p2, p1, p3}, Lee/cyber/smartid/inter/GetAccountStatusListener;->onGetAccountStatusFailed(Ljava/lang/String;Lee/cyber/smartid/dto/SmartIdError;)V

    goto/16 :goto_4

    .line 203
    :cond_10
    instance-of v1, p2, Lee/cyber/smartid/inter/GetRegistrationTokenListener;

    if-eqz v1, :cond_11

    .line 204
    check-cast p2, Lee/cyber/smartid/inter/GetRegistrationTokenListener;

    invoke-interface {p2, p1, p3}, Lee/cyber/smartid/inter/GetRegistrationTokenListener;->onGetRegistrationTokenFailed(Ljava/lang/String;Lee/cyber/smartid/dto/SmartIdError;)V

    goto/16 :goto_4

    .line 205
    :cond_11
    instance-of v1, p2, Lee/cyber/smartid/inter/EncryptKeysListener;

    if-eqz v1, :cond_12

    .line 206
    check-cast p2, Lee/cyber/smartid/inter/EncryptKeysListener;

    invoke-interface {p2, p1, p3}, Lee/cyber/smartid/inter/EncryptKeysListener;->onEncryptKeysFailed(Ljava/lang/String;Lee/cyber/smartid/dto/SmartIdError;)V

    goto/16 :goto_4

    .line 207
    :cond_12
    instance-of v1, p2, Lee/cyber/smartid/inter/GetPushNotificationDataListener;

    if-eqz v1, :cond_13

    .line 208
    check-cast p2, Lee/cyber/smartid/inter/GetPushNotificationDataListener;

    invoke-interface {p2, p1, p3}, Lee/cyber/smartid/inter/GetPushNotificationDataListener;->onGetPushNotificationDataFailed(Ljava/lang/String;Lee/cyber/smartid/dto/SmartIdError;)V

    goto/16 :goto_4

    .line 209
    :cond_13
    instance-of v1, p2, Lee/cyber/smartid/inter/CheckLocalPendingStateListener;

    if-eqz v1, :cond_14

    .line 210
    check-cast p2, Lee/cyber/smartid/inter/CheckLocalPendingStateListener;

    invoke-interface {p2, p1, p3}, Lee/cyber/smartid/inter/CheckLocalPendingStateListener;->onCheckLocalPendingStateFailed(Ljava/lang/String;Lee/cyber/smartid/dto/SmartIdError;)V

    goto/16 :goto_4

    .line 211
    :cond_14
    instance-of v1, p2, Lee/cyber/smartid/inter/CancelRPRequestListener;

    if-eqz v1, :cond_15

    .line 212
    check-cast p2, Lee/cyber/smartid/inter/CancelRPRequestListener;

    invoke-interface {p2, p1, p3}, Lee/cyber/smartid/inter/CancelRPRequestListener;->onCancelRPRequestFailed(Ljava/lang/String;Lee/cyber/smartid/dto/SmartIdError;)V

    goto/16 :goto_4

    .line 213
    :cond_15
    instance-of v1, p2, Lee/cyber/smartid/inter/CreateTransactionForRPRequestListener;

    if-eqz v1, :cond_16

    .line 214
    check-cast p2, Lee/cyber/smartid/inter/CreateTransactionForRPRequestListener;

    invoke-interface {p2, p1, p3}, Lee/cyber/smartid/inter/CreateTransactionForRPRequestListener;->onCreateTransactionForRPRequestFailed(Ljava/lang/String;Lee/cyber/smartid/dto/SmartIdError;)V

    goto/16 :goto_4

    .line 215
    :cond_16
    instance-of v1, p2, Lee/cyber/smartid/inter/GetSafetyNetAttestationListener;

    if-eqz v1, :cond_17

    .line 216
    check-cast p2, Lee/cyber/smartid/inter/GetSafetyNetAttestationListener;

    invoke-interface {p2, p1, p3}, Lee/cyber/smartid/inter/GetSafetyNetAttestationListener;->onGetSafetyNetAttestationFailed(Ljava/lang/String;Lee/cyber/smartid/dto/SmartIdError;)V

    goto/16 :goto_4

    .line 217
    :cond_17
    instance-of v1, p2, Lee/cyber/smartid/inter/CancelInteractiveUpgradeListener;

    if-eqz v1, :cond_18

    .line 218
    check-cast p2, Lee/cyber/smartid/inter/CancelInteractiveUpgradeListener;

    invoke-interface {p2, p1, p3}, Lee/cyber/smartid/inter/CancelInteractiveUpgradeListener;->onCancelInteractiveUpgradeFailed(Ljava/lang/String;Lee/cyber/smartid/dto/SmartIdError;)V

    goto/16 :goto_4

    .line 219
    :cond_18
    instance-of v1, p2, Lee/cyber/smartid/inter/VerifyTransactionVerificationCodeListener;

    if-eqz v1, :cond_19

    .line 220
    check-cast p2, Lee/cyber/smartid/inter/VerifyTransactionVerificationCodeListener;

    invoke-interface {p2, p1, p3}, Lee/cyber/smartid/inter/VerifyTransactionVerificationCodeListener;->onVerifyTransactionVerificationCodeFailed(Ljava/lang/String;Lee/cyber/smartid/dto/SmartIdError;)V

    goto/16 :goto_4

    .line 221
    :cond_19
    instance-of v1, p2, Lee/cyber/smartid/inter/GetDeviceFingerprintListener;

    if-eqz v1, :cond_1a

    .line 222
    check-cast p2, Lee/cyber/smartid/inter/GetDeviceFingerprintListener;

    invoke-interface {p2, p1, p3}, Lee/cyber/smartid/inter/GetDeviceFingerprintListener;->onGetDeviceFingerprintFailed(Ljava/lang/String;Lee/cyber/smartid/dto/SmartIdError;)V

    goto/16 :goto_4

    :cond_1a
    if-eqz p2, :cond_1d

    .line 223
    sget-object v1, Lee/cyber/smartid/SmartIdService;->c:Lee/cyber/smartid/util/Log;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "notifyError: Unknown listener class "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, " Please add implementation for this to the notifyError() method!"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lee/cyber/smartid/util/Log;->e(Ljava/lang/String;)V

    .line 224
    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getDeclaredMethods()[Ljava/lang/reflect/Method;

    move-result-object v1

    if-eqz v1, :cond_1d

    .line 225
    array-length v2, v1

    if-lez v2, :cond_1d

    .line 226
    array-length v2, v1

    const/4 v3, 0x0

    const/4 v4, 0x0

    :goto_1
    if-ge v4, v2, :cond_1d

    aget-object v5, v1, v4

    .line 227
    invoke-virtual {v5}, Ljava/lang/reflect/Method;->getParameterTypes()[Ljava/lang/Class;

    move-result-object v6

    if-eqz v6, :cond_1c

    .line 228
    array-length v7, v6

    const/4 v8, 0x2

    if-ne v7, v8, :cond_1c

    aget-object v7, v6, v3

    invoke-virtual {v7}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v7

    const-class v9, Ljava/lang/String;

    invoke-virtual {v9}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v9

    invoke-static {v7, v9}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_1c

    const/4 v7, 0x1

    aget-object v6, v6, v7

    invoke-virtual {v6}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v6

    const-class v9, Lee/cyber/smartid/dto/SmartIdError;

    invoke-virtual {v9}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v9

    invoke-static {v6, v9}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_1b

    goto :goto_2

    .line 229
    :cond_1b
    :try_start_0
    sget-object v6, Lee/cyber/smartid/SmartIdService;->c:Lee/cyber/smartid/util/Log;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V
    :try_end_0
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    const-string v10, "notifyError: Calling though reflection the method "

    :try_start_1
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/reflect/Method;->getName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v6, v9}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    .line 230
    new-array v6, v8, [Ljava/lang/Object;

    aput-object p1, v6, v3

    aput-object p3, v6, v7

    invoke-virtual {v5, p2, v6}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/lang/IllegalAccessException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_3

    :catch_0
    move-exception v5

    .line 231
    sget-object v6, Lee/cyber/smartid/SmartIdService;->c:Lee/cyber/smartid/util/Log;

    invoke-virtual {v6, v0, v5}, Lee/cyber/smartid/util/Log;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_3

    :catch_1
    move-exception v5

    .line 232
    sget-object v6, Lee/cyber/smartid/SmartIdService;->c:Lee/cyber/smartid/util/Log;

    invoke-virtual {v6, v0, v5}, Lee/cyber/smartid/util/Log;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_3

    :catch_2
    move-exception v5

    .line 233
    sget-object v6, Lee/cyber/smartid/SmartIdService;->c:Lee/cyber/smartid/util/Log;

    invoke-virtual {v6, v0, v5}, Lee/cyber/smartid/util/Log;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_3

    :catch_3
    move-exception v5

    .line 234
    sget-object v6, Lee/cyber/smartid/SmartIdService;->c:Lee/cyber/smartid/util/Log;

    invoke-virtual {v6, v0, v5}, Lee/cyber/smartid/util/Log;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_3

    .line 235
    :cond_1c
    :goto_2
    sget-object v6, Lee/cyber/smartid/SmartIdService;->c:Lee/cyber/smartid/util/Log;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "notifyError: Unusable method "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/reflect/Method;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v6, v5}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    :goto_3
    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_1

    :cond_1d
    :goto_4
    return-void
.end method

.method private a(Ljava/lang/String;Lee/cyber/smartid/inter/UpdateDeviceListener;)V
    .locals 8

    .line 156
    invoke-direct {p0}, Lee/cyber/smartid/SmartIdService;->b()V

    .line 157
    invoke-virtual {p0, p1, p2}, Lee/cyber/smartid/SmartIdService;->setListener(Ljava/lang/String;Lee/cyber/smartid/inter/ServiceListener;)V

    .line 158
    new-instance p2, Lee/cyber/smartid/dto/jsonrpc/param/UpdateDeviceParams;

    invoke-direct {p2}, Lee/cyber/smartid/dto/jsonrpc/param/UpdateDeviceParams;-><init>()V

    .line 159
    invoke-direct {p0}, Lee/cyber/smartid/SmartIdService;->m()Lee/cyber/smartid/dto/jsonrpc/param/RegisterDeviceData;

    move-result-object v4

    .line 160
    iget-object v0, p0, Lee/cyber/smartid/SmartIdService;->v:Lee/cyber/smartid/manager/inter/SafetyNetManager;

    invoke-interface {v0}, Lee/cyber/smartid/manager/inter/SafetyNetManager;->getLastSafetyNetAttestation()Lee/cyber/smartid/dto/SafetyNetAttestation;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lee/cyber/smartid/SmartIdService;->v:Lee/cyber/smartid/manager/inter/SafetyNetManager;

    invoke-interface {v0}, Lee/cyber/smartid/manager/inter/SafetyNetManager;->getLastSafetyNetAttestation()Lee/cyber/smartid/dto/SafetyNetAttestation;

    move-result-object v0

    iget-object v1, p0, Lee/cyber/smartid/SmartIdService;->d:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lee/cyber/smartid/dto/SafetyNetAttestation;->getResultTypeString(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    move-object v5, v0

    .line 161
    invoke-virtual {p2, v4}, Lee/cyber/smartid/dto/jsonrpc/param/UpdateDeviceParams;->setDeviceData(Lee/cyber/smartid/dto/jsonrpc/param/RegisterDeviceData;)V

    .line 162
    new-instance v2, Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCRequest;

    const-string v0, "updateDevice"

    invoke-direct {v2, v0, p2}, Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCRequest;-><init>(Ljava/lang/String;Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCParams;)V

    .line 163
    iget-object p2, p0, Lee/cyber/smartid/SmartIdService;->g:Lee/cyber/smartid/network/SmartIdAPI;

    invoke-interface {p2, v2}, Lee/cyber/smartid/network/SmartIdAPI;->updateDevice(Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCRequest;)Lc/b;

    move-result-object p2

    .line 164
    new-instance v7, Lee/cyber/smartid/SmartIdService$32;

    iget-object v3, p0, Lee/cyber/smartid/SmartIdService;->e:Lee/cyber/smartid/tse/SmartIdTSE;

    move-object v0, v7

    move-object v1, p0

    move-object v6, p1

    invoke-direct/range {v0 .. v6}, Lee/cyber/smartid/SmartIdService$32;-><init>(Lee/cyber/smartid/SmartIdService;Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCRequest;Lee/cyber/smartid/tse/SmartIdTSE;Lee/cyber/smartid/dto/jsonrpc/param/RegisterDeviceData;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {p2, v7}, Lc/b;->a(Lc/d;)V

    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lee/cyber/smartid/cryptolib/dto/StorageException;
        }
    .end annotation

    .line 76
    sget-object v0, Lee/cyber/smartid/SmartIdService;->c:Lee/cyber/smartid/util/Log;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "storeData: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    .line 77
    iget-object v0, p0, Lee/cyber/smartid/SmartIdService;->A:Lee/cyber/smartid/cryptolib/CryptoLib;

    invoke-virtual {v0, p1, p2}, Lee/cyber/smartid/cryptolib/CryptoLib;->storeData(Ljava/lang/String;Ljava/lang/Object;)V

    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/inter/ServiceListener;)V
    .locals 3

    .line 107
    sget-object v0, Lee/cyber/smartid/SmartIdService;->c:Lee/cyber/smartid/util/Log;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "retryLocalPendingStateForConfirmTransaction - referenceId: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    .line 108
    invoke-direct {p0, p1}, Lee/cyber/smartid/SmartIdService;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_1

    .line 109
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 110
    :cond_0
    iget-object v0, p0, Lee/cyber/smartid/SmartIdService;->e:Lee/cyber/smartid/tse/SmartIdTSE;

    iget-object v1, p0, Lee/cyber/smartid/SmartIdService;->q:Lee/cyber/smartid/manager/inter/TSEListenerToServiceListenerMapper;

    invoke-interface {v1, p3}, Lee/cyber/smartid/manager/inter/TSEListenerToServiceListenerMapper;->map(Lee/cyber/smartid/inter/ServiceListener;)Lee/cyber/smartid/tse/inter/TSEListener;

    move-result-object p3

    invoke-virtual {v0, p2, p1, p3}, Lee/cyber/smartid/tse/SmartIdTSE;->retryLocalPendingState(Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/tse/inter/TSEListener;)V

    return-void

    .line 111
    :cond_1
    :goto_0
    new-instance p1, Lee/cyber/smartid/SmartIdService$25;

    invoke-direct {p1, p0, p2}, Lee/cyber/smartid/SmartIdService$25;-><init>(Lee/cyber/smartid/SmartIdService;Ljava/lang/String;)V

    invoke-direct {p0, p1}, Lee/cyber/smartid/SmartIdService;->a(Ljava/lang/Runnable;)V

    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/inter/StorePushNotificationDataListener;)V
    .locals 4

    .line 83
    invoke-direct {p0, p2}, Lee/cyber/smartid/SmartIdService;->g(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 84
    sget-object v0, Lee/cyber/smartid/SmartIdService;->c:Lee/cyber/smartid/util/Log;

    const-string v1, "storePushNotificationDataInternal - Nothing has changed, ignoring .."

    invoke-virtual {v0, v1}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    .line 85
    new-instance v0, Lee/cyber/smartid/SmartIdService$10;

    invoke-direct {v0, p0, p3, p1, p2}, Lee/cyber/smartid/SmartIdService$10;-><init>(Lee/cyber/smartid/SmartIdService;Lee/cyber/smartid/inter/StorePushNotificationDataListener;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lee/cyber/smartid/SmartIdService;->a(Ljava/lang/Runnable;)V

    return-void

    .line 86
    :cond_0
    iget-object v0, p0, Lee/cyber/smartid/SmartIdService;->A:Lee/cyber/smartid/cryptolib/CryptoLib;

    iget-object v1, p0, Lee/cyber/smartid/SmartIdService;->i:Lee/cyber/smartid/inter/ServiceAccess;

    invoke-interface {v1}, Lee/cyber/smartid/inter/ServiceAccess;->getPushDataCurrentStorageId()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lee/cyber/smartid/SmartIdService$11;

    invoke-direct {v2, p0}, Lee/cyber/smartid/SmartIdService$11;-><init>(Lee/cyber/smartid/SmartIdService;)V

    .line 87
    invoke-virtual {v2}, Lcom/google/gson/b/a;->getType()Ljava/lang/reflect/Type;

    move-result-object v2

    .line 88
    invoke-virtual {v0, v1, v2}, Lee/cyber/smartid/cryptolib/CryptoLib;->retrieveData(Ljava/lang/String;Ljava/lang/reflect/Type;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lee/cyber/smartid/dto/PushData;

    if-eqz v0, :cond_1

    .line 89
    invoke-virtual {v0}, Lee/cyber/smartid/dto/PushData;->getPushToken()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    invoke-static {v0, p2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_2

    .line 90
    sget-object v1, Lee/cyber/smartid/SmartIdService;->c:Lee/cyber/smartid/util/Log;

    const-string v2, "storePushNotificationData - local unsent token changed, setting didCurrentUnsentTokenChange to true"

    invoke-virtual {v1, v2}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    goto :goto_1

    .line 91
    :cond_2
    sget-object v1, Lee/cyber/smartid/SmartIdService;->c:Lee/cyber/smartid/util/Log;

    const-string v2, "storePushNotificationData - local unsent token didn\'t change"

    invoke-virtual {v1, v2}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    .line 92
    :goto_1
    :try_start_0
    invoke-direct {p0, p2}, Lee/cyber/smartid/SmartIdService;->d(Ljava/lang/String;)V
    :try_end_0
    .catch Lee/cyber/smartid/cryptolib/dto/StorageException; {:try_start_0 .. :try_end_0} :catch_1

    const-wide/16 v1, 0x0

    .line 93
    :try_start_1
    invoke-direct {p0, v1, v2}, Lee/cyber/smartid/SmartIdService;->a(J)V
    :try_end_1
    .catch Lee/cyber/smartid/cryptolib/dto/StorageException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2

    :catch_0
    move-exception v1

    .line 94
    sget-object v2, Lee/cyber/smartid/SmartIdService;->c:Lee/cyber/smartid/util/Log;

    const-string v3, "storePushNotificationData"

    invoke-virtual {v2, v3, v1}, Lee/cyber/smartid/util/Log;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 95
    :goto_2
    invoke-virtual {p0}, Lee/cyber/smartid/SmartIdService;->isInitDone()Z

    move-result v1

    if-nez v1, :cond_3

    .line 96
    sget-object v0, Lee/cyber/smartid/SmartIdService;->c:Lee/cyber/smartid/util/Log;

    const-string v1, "storePushNotificationData - init not done yet, will send later"

    invoke-virtual {v0, v1}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    .line 97
    new-instance v0, Lee/cyber/smartid/SmartIdService$13;

    invoke-direct {v0, p0, p3, p1, p2}, Lee/cyber/smartid/SmartIdService$13;-><init>(Lee/cyber/smartid/SmartIdService;Lee/cyber/smartid/inter/StorePushNotificationDataListener;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lee/cyber/smartid/SmartIdService;->a(Ljava/lang/Runnable;)V

    return-void

    .line 98
    :cond_3
    invoke-direct {p0}, Lee/cyber/smartid/SmartIdService;->b()V

    .line 99
    invoke-virtual {p0, p1, p3}, Lee/cyber/smartid/SmartIdService;->setListener(Ljava/lang/String;Lee/cyber/smartid/inter/ServiceListener;)V

    .line 100
    iget-object p3, p0, Lee/cyber/smartid/SmartIdService;->e:Lee/cyber/smartid/tse/SmartIdTSE;

    invoke-virtual {p3}, Lee/cyber/smartid/tse/SmartIdTSE;->isDeviceRegistrationDone()Z

    move-result p3

    if-eqz p3, :cond_4

    .line 101
    new-instance p3, Lee/cyber/smartid/SmartIdService$14;

    invoke-direct {p3, p0, p1, p2}, Lee/cyber/smartid/SmartIdService$14;-><init>(Lee/cyber/smartid/SmartIdService;Ljava/lang/String;Ljava/lang/String;)V

    .line 102
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "CHAIN"

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 103
    invoke-direct {p0, p1, v0, p3}, Lee/cyber/smartid/SmartIdService;->a(Ljava/lang/String;ZLee/cyber/smartid/inter/UpdateDeviceListener;)V

    goto :goto_3

    .line 104
    :cond_4
    new-instance p3, Lee/cyber/smartid/SmartIdService$15;

    invoke-direct {p3, p0, p1, p2}, Lee/cyber/smartid/SmartIdService$15;-><init>(Lee/cyber/smartid/SmartIdService;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, p3}, Lee/cyber/smartid/SmartIdService;->a(Ljava/lang/Runnable;)V

    :goto_3
    return-void

    :catch_1
    move-exception p2

    .line 105
    new-instance p3, Lee/cyber/smartid/SmartIdService$12;

    invoke-direct {p3, p0, p1, p2}, Lee/cyber/smartid/SmartIdService$12;-><init>(Lee/cyber/smartid/SmartIdService;Ljava/lang/String;Lee/cyber/smartid/cryptolib/dto/StorageException;)V

    invoke-direct {p0, p3}, Lee/cyber/smartid/SmartIdService;->a(Ljava/lang/Runnable;)V

    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 8

    .line 56
    sget-object v0, Lee/cyber/smartid/SmartIdService;->c:Lee/cyber/smartid/util/Log;

    const-string v1, "encryptKeyAuthInternal"

    invoke-virtual {v0, v1}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    .line 57
    iget-object v2, p0, Lee/cyber/smartid/SmartIdService;->e:Lee/cyber/smartid/tse/SmartIdTSE;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v0, p0, Lee/cyber/smartid/SmartIdService;->p:Lee/cyber/smartid/manager/inter/PropertiesManager;

    invoke-interface {v0}, Lee/cyber/smartid/manager/inter/PropertiesManager;->getPropAccountRegistrationSZId()Ljava/lang/String;

    move-result-object v6

    new-instance v7, Lee/cyber/smartid/SmartIdService$4;

    invoke-direct {v7, p0, p1, p4, p5}, Lee/cyber/smartid/SmartIdService$4;-><init>(Lee/cyber/smartid/SmartIdService;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object v4, p2

    move-object v5, p3

    invoke-virtual/range {v2 .. v7}, Lee/cyber/smartid/tse/SmartIdTSE;->encryptKey(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/tse/inter/EncryptKeyListener;)V

    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLee/cyber/smartid/inter/DeleteAccountListener;)V
    .locals 6

    .line 168
    invoke-direct {p0}, Lee/cyber/smartid/SmartIdService;->b()V

    .line 169
    iget-object v0, p0, Lee/cyber/smartid/SmartIdService;->k:Lee/cyber/smartid/manager/inter/DeleteAccountManager;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move-object v5, p5

    invoke-interface/range {v0 .. v5}, Lee/cyber/smartid/manager/inter/DeleteAccountManager;->deleteAccount(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLee/cyber/smartid/inter/DeleteAccountListener;)V

    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;ZZ)V
    .locals 4

    .line 150
    iget-object v0, p0, Lee/cyber/smartid/SmartIdService;->y:Ljava/util/ArrayList;

    monitor-enter v0

    .line 151
    :try_start_0
    iget-object v1, p0, Lee/cyber/smartid/SmartIdService;->y:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 152
    iget-object v1, p0, Lee/cyber/smartid/SmartIdService;->y:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lee/cyber/smartid/tse/inter/AlarmAccess$AlarmHandler;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 153
    :try_start_1
    invoke-interface {v2, p1, p2, p3, p4}, Lee/cyber/smartid/tse/inter/AlarmAccess$AlarmHandler;->onHandleAlarm(Ljava/lang/String;Ljava/lang/String;ZZ)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 154
    :catch_0
    :try_start_2
    sget-object v2, Lee/cyber/smartid/SmartIdService;->c:Lee/cyber/smartid/util/Log;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    const-string v3, "notifyAlarmHandlers, e"

    :try_start_3
    invoke-virtual {v2, v3}, Lee/cyber/smartid/util/Log;->e(Ljava/lang/String;)V

    goto :goto_0

    .line 155
    :cond_0
    monitor-exit v0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw p1
.end method

.method private a(Ljava/lang/String;ZLee/cyber/smartid/inter/UpdateDeviceListener;)V
    .locals 9

    .line 120
    invoke-direct {p0}, Lee/cyber/smartid/SmartIdService;->b()V

    .line 121
    invoke-direct {p0}, Lee/cyber/smartid/SmartIdService;->j()Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    const-wide/32 v1, 0x1d4c0

    invoke-static {v0, v1, v2}, Lee/cyber/smartid/util/Util;->acquireWakeLock(Landroid/os/PowerManager$WakeLock;J)V

    .line 122
    invoke-direct {p0}, Lee/cyber/smartid/SmartIdService;->i()Z

    move-result v0

    .line 123
    invoke-direct {p0}, Lee/cyber/smartid/SmartIdService;->h()Ljava/lang/String;

    move-result-object v1

    .line 124
    invoke-direct {p0}, Lee/cyber/smartid/SmartIdService;->m()Lee/cyber/smartid/dto/jsonrpc/param/RegisterDeviceData;

    move-result-object v2

    invoke-direct {p0, v2}, Lee/cyber/smartid/SmartIdService;->b(Lee/cyber/smartid/dto/jsonrpc/param/RegisterDeviceData;)Ljava/lang/String;

    move-result-object v2

    .line 125
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    const-string v4, ")"

    const-string v5, ", current: "

    const/4 v6, 0x1

    if-nez v3, :cond_0

    invoke-static {v1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 126
    :cond_0
    sget-object v0, Lee/cyber/smartid/SmartIdService;->c:Lee/cyber/smartid/util/Log;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "updateDeviceIfNeeded - device data changed, update is needed (sent: "

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    const/4 v0, 0x1

    .line 127
    :cond_1
    iget-object v1, p0, Lee/cyber/smartid/SmartIdService;->v:Lee/cyber/smartid/manager/inter/SafetyNetManager;

    invoke-interface {v1}, Lee/cyber/smartid/manager/inter/SafetyNetManager;->getSafetyNetResultHash()Ljava/lang/String;

    move-result-object v1

    .line 128
    iget-object v2, p0, Lee/cyber/smartid/SmartIdService;->v:Lee/cyber/smartid/manager/inter/SafetyNetManager;

    invoke-interface {v2}, Lee/cyber/smartid/manager/inter/SafetyNetManager;->getLastSafetyNetAttestation()Lee/cyber/smartid/dto/SafetyNetAttestation;

    move-result-object v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lee/cyber/smartid/SmartIdService;->A:Lee/cyber/smartid/cryptolib/CryptoLib;

    iget-object v3, p0, Lee/cyber/smartid/SmartIdService;->v:Lee/cyber/smartid/manager/inter/SafetyNetManager;

    invoke-interface {v3}, Lee/cyber/smartid/manager/inter/SafetyNetManager;->getLastSafetyNetAttestation()Lee/cyber/smartid/dto/SafetyNetAttestation;

    move-result-object v3

    invoke-virtual {v3}, Lee/cyber/smartid/dto/SafetyNetAttestation;->getPayloadJWS()Ljava/lang/String;

    move-result-object v3

    iget-object v7, p0, Lee/cyber/smartid/SmartIdService;->v:Lee/cyber/smartid/manager/inter/SafetyNetManager;

    invoke-interface {v7}, Lee/cyber/smartid/manager/inter/SafetyNetManager;->getLastSafetyNetAttestation()Lee/cyber/smartid/dto/SafetyNetAttestation;

    move-result-object v7

    iget-object v8, p0, Lee/cyber/smartid/SmartIdService;->d:Landroid/content/Context;

    invoke-virtual {v7, v8}, Lee/cyber/smartid/dto/SafetyNetAttestation;->getResultTypeString(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v2, v2, v3, v7}, Lee/cyber/smartid/util/Util;->calculateSHA1FromSafetyNetJWSMeaning(Lee/cyber/smartid/cryptolib/inter/EncodingOp;Lee/cyber/smartid/cryptolib/inter/CryptoOp;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    :cond_2
    const/4 v2, 0x0

    .line 129
    :goto_0
    iget-object v3, p0, Lee/cyber/smartid/SmartIdService;->v:Lee/cyber/smartid/manager/inter/SafetyNetManager;

    invoke-interface {v3}, Lee/cyber/smartid/manager/inter/SafetyNetManager;->isSafetyNetCheckInProgress()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 130
    sget-object v2, Lee/cyber/smartid/SmartIdService;->c:Lee/cyber/smartid/util/Log;

    const-string v3, "updateDeviceIfNeeded - SafetyNet check still in progress and the current value is null, we will use the old hash value or comparison in the meanwhile .."

    invoke-virtual {v2, v3}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    move-object v2, v1

    .line 131
    :cond_3
    iget-object v3, p0, Lee/cyber/smartid/SmartIdService;->v:Lee/cyber/smartid/manager/inter/SafetyNetManager;

    invoke-interface {v3}, Lee/cyber/smartid/manager/inter/SafetyNetManager;->shouldStartSafetyNetCheckPostInit()Z

    move-result v3

    if-nez v3, :cond_4

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 132
    sget-object v1, Lee/cyber/smartid/SmartIdService;->c:Lee/cyber/smartid/util/Log;

    const-string v2, "updateDeviceIfNeeded - SafetyNet result is missing and automatic requests are turned off, ignoring SafetyNet as a component of updateDevice .."

    invoke-virtual {v1, v2}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    goto :goto_1

    .line 133
    :cond_4
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_5

    invoke-static {v1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_6

    .line 134
    :cond_5
    sget-object v0, Lee/cyber/smartid/SmartIdService;->c:Lee/cyber/smartid/util/Log;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "updateDeviceIfNeeded - SafetyNet result changed, update is needed (sent: "

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    const/4 v0, 0x1

    :cond_6
    :goto_1
    if-eqz p2, :cond_7

    .line 135
    sget-object p2, Lee/cyber/smartid/SmartIdService;->c:Lee/cyber/smartid/util/Log;

    const-string v0, "updateDeviceIfNeeded - forceUpdate is set to true"

    invoke-virtual {p2, v0}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    const/4 v0, 0x1

    :cond_7
    if-eqz v0, :cond_8

    .line 136
    sget-object p2, Lee/cyber/smartid/SmartIdService;->c:Lee/cyber/smartid/util/Log;

    const-string v0, "updateDeviceIfNeeded - triggering the update .."

    invoke-virtual {p2, v0}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    .line 137
    :try_start_0
    invoke-direct {p0, p1, p3}, Lee/cyber/smartid/SmartIdService;->a(Ljava/lang/String;Lee/cyber/smartid/inter/UpdateDeviceListener;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_3

    :catch_0
    move-exception p1

    .line 138
    sget-object p2, Lee/cyber/smartid/SmartIdService;->c:Lee/cyber/smartid/util/Log;

    const-string p3, "updateDeviceIfNeeded - update failed .."

    invoke-virtual {p2, p3, p1}, Lee/cyber/smartid/util/Log;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 139
    invoke-direct {p0}, Lee/cyber/smartid/SmartIdService;->j()Landroid/os/PowerManager$WakeLock;

    move-result-object p1

    invoke-static {p1}, Lee/cyber/smartid/util/Util;->releaseWakeLock(Landroid/os/PowerManager$WakeLock;)V

    goto :goto_3

    .line 140
    :cond_8
    sget-object p2, Lee/cyber/smartid/SmartIdService;->c:Lee/cyber/smartid/util/Log;

    const-string v0, "updateDeviceIfNeeded - update is not needed"

    invoke-virtual {p2, v0}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    .line 141
    :try_start_1
    invoke-direct {p0}, Lee/cyber/smartid/SmartIdService;->g()V
    :try_end_1
    .catch Lee/cyber/smartid/cryptolib/dto/StorageException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_2

    :catch_1
    move-exception p2

    .line 142
    sget-object v0, Lee/cyber/smartid/SmartIdService;->c:Lee/cyber/smartid/util/Log;

    const-string v1, "updateDeviceIfNeeded"

    invoke-virtual {v0, v1, p2}, Lee/cyber/smartid/util/Log;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 143
    :goto_2
    invoke-direct {p0}, Lee/cyber/smartid/SmartIdService;->j()Landroid/os/PowerManager$WakeLock;

    move-result-object p2

    invoke-static {p2}, Lee/cyber/smartid/util/Util;->releaseWakeLock(Landroid/os/PowerManager$WakeLock;)V

    .line 144
    new-instance p2, Lee/cyber/smartid/SmartIdService$27;

    invoke-direct {p2, p0, p3, p1}, Lee/cyber/smartid/SmartIdService$27;-><init>(Lee/cyber/smartid/SmartIdService;Lee/cyber/smartid/inter/UpdateDeviceListener;Ljava/lang/String;)V

    invoke-direct {p0, p2}, Lee/cyber/smartid/SmartIdService;->a(Ljava/lang/Runnable;)V

    :goto_3
    return-void
.end method

.method private a(Z)V
    .locals 0

    .line 30
    iput-boolean p1, p0, Lee/cyber/smartid/SmartIdService;->f:Z

    return-void
.end method

.method private a(Lee/cyber/smartid/dto/PushData;)Z
    .locals 3

    .line 145
    iget-object v0, p0, Lee/cyber/smartid/SmartIdService;->A:Lee/cyber/smartid/cryptolib/CryptoLib;

    iget-object v1, p0, Lee/cyber/smartid/SmartIdService;->i:Lee/cyber/smartid/inter/ServiceAccess;

    invoke-interface {v1}, Lee/cyber/smartid/inter/ServiceAccess;->getPushDataSentStorageId()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lee/cyber/smartid/SmartIdService$29;

    invoke-direct {v2, p0}, Lee/cyber/smartid/SmartIdService$29;-><init>(Lee/cyber/smartid/SmartIdService;)V

    .line 146
    invoke-virtual {v2}, Lcom/google/gson/b/a;->getType()Ljava/lang/reflect/Type;

    move-result-object v2

    .line 147
    invoke-virtual {v0, v1, v2}, Lee/cyber/smartid/cryptolib/CryptoLib;->retrieveData(Ljava/lang/String;Ljava/lang/reflect/Type;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lee/cyber/smartid/dto/PushData;

    if-nez v0, :cond_0

    if-nez p1, :cond_2

    :cond_0
    if-eqz v0, :cond_1

    if-eqz p1, :cond_2

    :cond_1
    if-eqz v0, :cond_3

    if-eqz p1, :cond_3

    .line 148
    invoke-virtual {v0}, Lee/cyber/smartid/dto/PushData;->getPushToken()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lee/cyber/smartid/dto/PushData;->getPushToken()Ljava/lang/String;

    move-result-object p1

    invoke-static {v0, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result p1

    if-nez p1, :cond_3

    .line 149
    :cond_2
    sget-object p1, Lee/cyber/smartid/SmartIdService;->c:Lee/cyber/smartid/util/Log;

    const-string v0, "updateDeviceIfNeeded - push token changed, update is needed"

    invoke-virtual {p1, v0}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    const/4 p1, 0x1

    return p1

    :cond_3
    const/4 p1, 0x0

    return p1
.end method

.method private b(I)J
    .locals 2
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "Range"
        }
    .end annotation

    .line 236
    iget-object v0, p0, Lee/cyber/smartid/SmartIdService;->e:Lee/cyber/smartid/tse/SmartIdTSE;

    invoke-virtual {v0, p1}, Lee/cyber/smartid/tse/SmartIdTSE;->getAutomaticRequestRetryDelay(I)J

    move-result-wide v0

    return-wide v0
.end method

.method static synthetic b(Lee/cyber/smartid/SmartIdService;)Landroid/content/Context;
    .locals 0

    .line 1
    iget-object p0, p0, Lee/cyber/smartid/SmartIdService;->d:Landroid/content/Context;

    return-object p0
.end method

.method private b(Lee/cyber/smartid/dto/jsonrpc/param/RegisterDeviceData;)Ljava/lang/String;
    .locals 3

    const/4 v0, 0x0

    .line 8
    :try_start_0
    new-instance v1, Lee/cyber/smartid/dto/jsonrpc/param/RegisterDeviceData;

    invoke-direct {v1, p1}, Lee/cyber/smartid/dto/jsonrpc/param/RegisterDeviceData;-><init>(Lee/cyber/smartid/dto/jsonrpc/param/RegisterDeviceData;)V

    .line 9
    invoke-virtual {v1}, Lee/cyber/smartid/dto/jsonrpc/param/RegisterDeviceData;->getProperties()Lee/cyber/smartid/dto/jsonrpc/DeviceProperties;

    move-result-object p1

    invoke-virtual {p1, v0}, Lee/cyber/smartid/dto/jsonrpc/DeviceProperties;->setSafetyNetAttestationResult(Ljava/lang/String;)V

    .line 10
    iget-object p1, p0, Lee/cyber/smartid/SmartIdService;->A:Lee/cyber/smartid/cryptolib/CryptoLib;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    const-string v2, "SHA-1"

    :try_start_1
    invoke-virtual {p1, v1, v2}, Lee/cyber/smartid/cryptolib/CryptoLib;->digestAndEncodeToBase64(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    return-object p1

    :catch_0
    move-exception p1

    .line 11
    sget-object v1, Lee/cyber/smartid/SmartIdService;->c:Lee/cyber/smartid/util/Log;

    const-string v2, "calculateSHA1ForUpdateDeviceData"

    invoke-virtual {v1, v2, p1}, Lee/cyber/smartid/util/Log;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    return-object v0
.end method

.method private b(Ljava/lang/String;)Ljava/lang/String;
    .locals 8

    .line 35
    iget-object v0, p0, Lee/cyber/smartid/SmartIdService;->i:Lee/cyber/smartid/inter/ServiceAccess;

    invoke-interface {v0}, Lee/cyber/smartid/tse/inter/ExternalResourceAccess;->getAccountUUIDs()Ljava/util/ArrayList;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_4

    .line 36
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-nez v2, :cond_0

    goto :goto_1

    .line 37
    :cond_0
    iget-object v2, p0, Lee/cyber/smartid/SmartIdService;->i:Lee/cyber/smartid/inter/ServiceAccess;

    invoke-interface {v2}, Lee/cyber/smartid/tse/inter/ExternalResourceAccess;->getKeyTypes()[Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_4

    .line 38
    array-length v3, v2

    if-nez v3, :cond_1

    goto :goto_1

    .line 39
    :cond_1
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 40
    array-length v4, v2

    const/4 v5, 0x0

    :goto_0
    if-ge v5, v4, :cond_2

    aget-object v6, v2, v5

    const-string v7, "confirm_transaction"

    .line 41
    invoke-direct {p0, v7, v3, v6}, Lee/cyber/smartid/SmartIdService;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 42
    invoke-direct {p0, v3, v6}, Lee/cyber/smartid/SmartIdService;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1

    :cond_3
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    :cond_4
    :goto_1
    return-object v1
.end method

.method private b()V
    .locals 2

    .line 2
    invoke-virtual {p0}, Lee/cyber/smartid/SmartIdService;->isInitDone()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 3
    :cond_0
    sget-object v0, Lee/cyber/smartid/SmartIdService;->c:Lee/cyber/smartid/util/Log;

    const-string v1, "throwIfNoInit: Init not yet done!"

    invoke-virtual {v0, v1}, Lee/cyber/smartid/util/Log;->e(Ljava/lang/String;)V

    .line 4
    new-instance v0, Lee/cyber/smartid/tse/dto/InitializationNotCompletedException;

    const-string v1, "Service not initialized. Did you call SmartIdService.initService() first?"

    invoke-direct {v0, v1}, Lee/cyber/smartid/tse/dto/InitializationNotCompletedException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private static b(Landroid/content/Context;Ljava/lang/String;JLjava/lang/String;)V
    .locals 3

    const-string v0, "alarm"

    .line 48
    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    if-nez v0, :cond_0

    .line 49
    const-class p0, Lee/cyber/smartid/SmartIdService;

    invoke-static {p0}, Lee/cyber/smartid/util/Log;->getInstance(Ljava/lang/Class;)Lee/cyber/smartid/util/Log;

    move-result-object p0

    const-string p1, "scheduleAlarmFor: Unable to access alarmManager"

    invoke-virtual {p0, p1}, Lee/cyber/smartid/util/Log;->e(Ljava/lang/String;)V

    return-void

    .line 50
    :cond_0
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v1

    add-long/2addr v1, p2

    .line 51
    invoke-static {p0, p1, p4}, Lee/cyber/smartid/SmartIdService;->c(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/app/PendingIntent;

    move-result-object p0

    const/4 p4, 0x2

    .line 52
    invoke-virtual {v0, p4, v1, v2, p0}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    .line 53
    const-class p0, Lee/cyber/smartid/SmartIdService;

    invoke-static {p0}, Lee/cyber/smartid/util/Log;->getInstance(Ljava/lang/Class;)Lee/cyber/smartid/util/Log;

    move-result-object p0

    new-instance p4, Ljava/lang/StringBuilder;

    invoke-direct {p4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "scheduleAlarmFor - new alarm set for "

    invoke-virtual {p4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p4, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string p2, " from now for "

    invoke-virtual {p4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    return-void
.end method

.method private static b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    const-string v0, "alarm"

    .line 54
    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    if-nez v0, :cond_0

    .line 55
    const-class p0, Lee/cyber/smartid/SmartIdService;

    invoke-static {p0}, Lee/cyber/smartid/util/Log;->getInstance(Ljava/lang/Class;)Lee/cyber/smartid/util/Log;

    move-result-object p0

    const-string p1, "clearAlarmFor: Unable to access alarmManager"

    invoke-virtual {p0, p1}, Lee/cyber/smartid/util/Log;->e(Ljava/lang/String;)V

    return-void

    .line 56
    :cond_0
    invoke-static {p0, p1, p2}, Lee/cyber/smartid/SmartIdService;->c(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/app/PendingIntent;

    move-result-object p0

    .line 57
    invoke-virtual {v0, p0}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    .line 58
    const-class p0, Lee/cyber/smartid/SmartIdService;

    invoke-static {p0}, Lee/cyber/smartid/util/Log;->getInstance(Ljava/lang/Class;)Lee/cyber/smartid/util/Log;

    move-result-object p0

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "clearAlarmFor - cleared an alarm for "

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic b(Lee/cyber/smartid/SmartIdService;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 28
    invoke-direct {p0, p1, p2}, Lee/cyber/smartid/SmartIdService;->b(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private b(Ljava/lang/String;Ljava/lang/String;)V
    .locals 10

    .line 18
    sget-object v0, Lee/cyber/smartid/SmartIdService;->c:Lee/cyber/smartid/util/Log;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "retryLocalPendingStateForSubmitClientSecondPart - tag: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, ", referenceId: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    .line 19
    iget-object v0, p0, Lee/cyber/smartid/SmartIdService;->n:Lee/cyber/smartid/manager/inter/StoredAccountManager;

    invoke-interface {v0}, Lee/cyber/smartid/manager/inter/StoredAccountManager;->getAccountStateUpdateLock()Ljava/lang/Object;

    move-result-object v0

    monitor-enter v0

    .line 20
    :try_start_0
    invoke-direct {p0, p1}, Lee/cyber/smartid/SmartIdService;->c(Ljava/lang/String;)Lee/cyber/smartid/dto/AccountState;

    move-result-object p1

    if-nez p1, :cond_0

    .line 21
    new-instance p1, Lee/cyber/smartid/SmartIdService$21;

    invoke-direct {p1, p0, p2}, Lee/cyber/smartid/SmartIdService$21;-><init>(Lee/cyber/smartid/SmartIdService;Ljava/lang/String;)V

    invoke-direct {p0, p1}, Lee/cyber/smartid/SmartIdService;->a(Ljava/lang/Runnable;)V

    .line 22
    monitor-exit v0

    return-void

    .line 23
    :cond_0
    invoke-virtual {p1}, Lee/cyber/smartid/dto/AccountState;->getAuthSubmitClientSecondPartState()Ljava/lang/String;

    move-result-object v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const-string v2, "STATE_SUBMIT_CLIENT_SECOND_PART_CONFIRMED"

    :try_start_1
    invoke-static {v1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p1}, Lee/cyber/smartid/dto/AccountState;->getSignSubmitClientSecondPartState()Ljava/lang/String;

    move-result-object v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    const-string v2, "STATE_SUBMIT_CLIENT_SECOND_PART_CONFIRMED"

    :try_start_2
    invoke-static {v1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 24
    new-instance v1, Lee/cyber/smartid/dto/jsonrpc/resp/AddAccountResp;

    invoke-virtual {p1}, Lee/cyber/smartid/dto/AccountState;->getAccountUUID()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1}, Lee/cyber/smartid/dto/AccountState;->getAuthCSRTransactionUUID()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1}, Lee/cyber/smartid/dto/AccountState;->getSignCSRTransactionUUID()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1}, Lee/cyber/smartid/dto/AccountState;->getIdentityDataFromRegistration()Lee/cyber/smartid/dto/jsonrpc/IdentityData;

    move-result-object v7

    iget-object v2, p0, Lee/cyber/smartid/SmartIdService;->n:Lee/cyber/smartid/manager/inter/StoredAccountManager;

    invoke-interface {v2}, Lee/cyber/smartid/manager/inter/StoredAccountManager;->loadAccountsFromStorage()Ljava/util/ArrayList;

    move-result-object v2

    invoke-static {v2}, Lee/cyber/smartid/dto/AccountWrapper;->toWrapperList(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v8

    invoke-virtual {p1}, Lee/cyber/smartid/dto/AccountState;->getInitiationType()Ljava/lang/String;

    move-result-object v9

    move-object v2, v1

    move-object v3, p2

    invoke-direct/range {v2 .. v9}, Lee/cyber/smartid/dto/jsonrpc/resp/AddAccountResp;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/dto/jsonrpc/IdentityData;Ljava/util/ArrayList;Ljava/lang/String;)V

    .line 25
    new-instance p1, Lee/cyber/smartid/SmartIdService$22;

    invoke-direct {p1, p0, p2, v1}, Lee/cyber/smartid/SmartIdService$22;-><init>(Lee/cyber/smartid/SmartIdService;Ljava/lang/String;Lee/cyber/smartid/dto/jsonrpc/resp/AddAccountResp;)V

    invoke-direct {p0, p1}, Lee/cyber/smartid/SmartIdService;->a(Ljava/lang/Runnable;)V

    .line 26
    monitor-exit v0

    return-void

    .line 27
    :cond_1
    invoke-virtual {p1, p2}, Lee/cyber/smartid/dto/AccountState;->addSubmitClientSecondPartListenerTag(Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 28
    :try_start_3
    iget-object v1, p0, Lee/cyber/smartid/SmartIdService;->n:Lee/cyber/smartid/manager/inter/StoredAccountManager;

    invoke-interface {v1, p1}, Lee/cyber/smartid/manager/inter/StoredAccountManager;->storeAccountToStorage(Lee/cyber/smartid/dto/AccountState;)V
    :try_end_3
    .catch Lee/cyber/smartid/cryptolib/dto/StorageException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v1

    .line 29
    :try_start_4
    sget-object v2, Lee/cyber/smartid/SmartIdService;->c:Lee/cyber/smartid/util/Log;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    const-string v3, "retryLocalPendingStateForSubmitClientSecondPart"

    :try_start_5
    invoke-virtual {v2, v3, v1}, Lee/cyber/smartid/util/Log;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 30
    :goto_0
    invoke-virtual {p1}, Lee/cyber/smartid/dto/AccountState;->getAuthSubmitClientSecondPartState()Ljava/lang/String;

    move-result-object v1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    const-string v2, "STATE_SUBMIT_CLIENT_SECOND_PART_CONFIRMED"

    :try_start_6
    invoke-static {v1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 31
    iget-object v1, p0, Lee/cyber/smartid/SmartIdService;->e:Lee/cyber/smartid/tse/SmartIdTSE;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lee/cyber/smartid/dto/AccountState;->getAuthKeyReference()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lee/cyber/smartid/dto/AccountState;->getAccountUUID()Ljava/lang/String;

    move-result-object v3
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    const-string v4, "AUTHENTICATION"

    :try_start_7
    invoke-direct {p0, v3, v4}, Lee/cyber/smartid/SmartIdService;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Lee/cyber/smartid/SmartIdService$23;

    invoke-direct {v4, p0}, Lee/cyber/smartid/SmartIdService$23;-><init>(Lee/cyber/smartid/SmartIdService;)V

    invoke-virtual {v1, v2, v3, v4}, Lee/cyber/smartid/tse/SmartIdTSE;->retryLocalPendingState(Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/tse/inter/TSEListener;)V

    .line 32
    :cond_2
    invoke-virtual {p1}, Lee/cyber/smartid/dto/AccountState;->getSignSubmitClientSecondPartState()Ljava/lang/String;

    move-result-object v1
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    const-string v2, "STATE_SUBMIT_CLIENT_SECOND_PART_CONFIRMED"

    :try_start_8
    invoke-static {v1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 33
    iget-object v1, p0, Lee/cyber/smartid/SmartIdService;->e:Lee/cyber/smartid/tse/SmartIdTSE;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lee/cyber/smartid/dto/AccountState;->getSignKeyReference()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1}, Lee/cyber/smartid/dto/AccountState;->getAccountUUID()Ljava/lang/String;

    move-result-object p1
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    const-string v2, "SIGNATURE"

    :try_start_9
    invoke-direct {p0, p1, v2}, Lee/cyber/smartid/SmartIdService;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    new-instance v2, Lee/cyber/smartid/SmartIdService$24;

    invoke-direct {v2, p0}, Lee/cyber/smartid/SmartIdService$24;-><init>(Lee/cyber/smartid/SmartIdService;)V

    invoke-virtual {v1, p2, p1, v2}, Lee/cyber/smartid/tse/SmartIdTSE;->retryLocalPendingState(Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/tse/inter/TSEListener;)V

    .line 34
    :cond_3
    monitor-exit v0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit v0
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    throw p1
.end method

.method static synthetic c(Lee/cyber/smartid/SmartIdService;)I
    .locals 0

    .line 1
    invoke-direct {p0}, Lee/cyber/smartid/SmartIdService;->d()I

    move-result p0

    return p0
.end method

.method private static c(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/app/PendingIntent;
    .locals 3

    .line 12
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lee/cyber/smartid/SmartIdService$SmartIdServiceAlarmManager;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 13
    invoke-virtual {v0, p1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string p1, "ee.cyber.smartid.EXTRA_ALARM_TARGET_ID"

    .line 14
    invoke-virtual {v0, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 15
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p0

    const/4 p1, 0x0

    const/high16 p2, 0x8000000

    invoke-static {p0, p1, v0, p2}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object p0

    return-object p0
.end method

.method private c(Ljava/lang/String;)Lee/cyber/smartid/dto/AccountState;
    .locals 7

    .line 112
    iget-object v0, p0, Lee/cyber/smartid/SmartIdService;->n:Lee/cyber/smartid/manager/inter/StoredAccountManager;

    invoke-interface {v0}, Lee/cyber/smartid/manager/inter/StoredAccountManager;->getAccountStateUpdateLock()Ljava/lang/Object;

    move-result-object v0

    monitor-enter v0

    .line 113
    :try_start_0
    iget-object v1, p0, Lee/cyber/smartid/SmartIdService;->n:Lee/cyber/smartid/manager/inter/StoredAccountManager;

    invoke-interface {v1}, Lee/cyber/smartid/manager/inter/StoredAccountManager;->loadAccountsFromStorage()Ljava/util/ArrayList;

    move-result-object v1

    .line 114
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v2

    const/4 v3, 0x0

    if-nez v2, :cond_0

    .line 115
    monitor-exit v0

    return-object v3

    .line 116
    :cond_0
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lee/cyber/smartid/dto/AccountState;

    .line 117
    invoke-virtual {v2}, Lee/cyber/smartid/dto/AccountState;->getAuthKeyReference()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1

    invoke-virtual {v2}, Lee/cyber/smartid/dto/AccountState;->getSignKeyReference()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v4, :cond_1

    const-string v4, "add_account"

    :try_start_1
    invoke-virtual {v2}, Lee/cyber/smartid/dto/AccountState;->getAuthKeyReference()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2}, Lee/cyber/smartid/dto/AccountState;->getSignKeyReference()Ljava/lang/String;

    move-result-object v6

    invoke-direct {p0, v4, v5, v6}, Lee/cyber/smartid/SmartIdService;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {p1, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 118
    monitor-exit v0

    return-object v2

    .line 119
    :cond_2
    monitor-exit v0

    return-object v3

    :catchall_0
    move-exception p1

    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw p1
.end method

.method private c()Lee/cyber/smartid/dto/upgrade/InteractiveUpgradeState;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lee/cyber/smartid/cryptolib/dto/StorageException;
        }
    .end annotation

    .line 2
    invoke-direct {p0}, Lee/cyber/smartid/SmartIdService;->d()I

    move-result v0

    const/4 v1, 0x6

    invoke-direct {p0, v0, v1}, Lee/cyber/smartid/SmartIdService;->a(II)Lee/cyber/smartid/dto/upgrade/InteractiveUpgradeState;

    move-result-object v0

    return-object v0
.end method

.method private c(Lee/cyber/smartid/dto/jsonrpc/param/RegisterDeviceData;)V
    .locals 3

    if-nez p1, :cond_0

    return-void

    .line 43
    :cond_0
    iget-object v0, p0, Lee/cyber/smartid/SmartIdService;->A:Lee/cyber/smartid/cryptolib/CryptoLib;

    iget-object v1, p0, Lee/cyber/smartid/SmartIdService;->i:Lee/cyber/smartid/inter/ServiceAccess;

    invoke-interface {v1}, Lee/cyber/smartid/inter/ServiceAccess;->getPushDataCurrentStorageId()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lee/cyber/smartid/SmartIdService$33;

    invoke-direct {v2, p0}, Lee/cyber/smartid/SmartIdService$33;-><init>(Lee/cyber/smartid/SmartIdService;)V

    .line 44
    invoke-virtual {v2}, Lcom/google/gson/b/a;->getType()Ljava/lang/reflect/Type;

    move-result-object v2

    .line 45
    invoke-virtual {v0, v1, v2}, Lee/cyber/smartid/cryptolib/CryptoLib;->retrieveData(Ljava/lang/String;Ljava/lang/reflect/Type;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lee/cyber/smartid/dto/PushData;

    const-string v1, "GCM"

    .line 46
    invoke-virtual {p1, v1}, Lee/cyber/smartid/dto/jsonrpc/param/RegisterDeviceData;->setPushNotificationType(Ljava/lang/String;)V

    if-eqz v0, :cond_1

    .line 47
    invoke-virtual {v0}, Lee/cyber/smartid/dto/PushData;->getPushToken()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {v0}, Lee/cyber/smartid/dto/PushData;->getPushToken()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1, v0}, Lee/cyber/smartid/dto/jsonrpc/param/RegisterDeviceData;->setPushNotificationToken(Ljava/lang/String;)V

    return-void
.end method

.method private d()I
    .locals 3

    .line 2
    iget-object v0, p0, Lee/cyber/smartid/SmartIdService;->A:Lee/cyber/smartid/cryptolib/CryptoLib;

    new-instance v1, Lee/cyber/smartid/SmartIdService$2;

    invoke-direct {v1, p0}, Lee/cyber/smartid/SmartIdService$2;-><init>(Lee/cyber/smartid/SmartIdService;)V

    .line 3
    invoke-virtual {v1}, Lcom/google/gson/b/a;->getType()Ljava/lang/reflect/Type;

    move-result-object v1

    const-string v2, "ee.cyber.smartid.APP_STORAGE_VERSION"

    .line 4
    invoke-virtual {v0, v2, v1}, Lee/cyber/smartid/cryptolib/CryptoLib;->retrieveData(Ljava/lang/String;Ljava/lang/reflect/Type;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 5
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 6
    :cond_0
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method static synthetic d(Lee/cyber/smartid/SmartIdService;)Lee/cyber/smartid/tse/SmartIdTSE;
    .locals 0

    .line 1
    iget-object p0, p0, Lee/cyber/smartid/SmartIdService;->e:Lee/cyber/smartid/tse/SmartIdTSE;

    return-object p0
.end method

.method private d(Ljava/lang/String;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lee/cyber/smartid/cryptolib/dto/StorageException;
        }
    .end annotation

    .line 2
    new-instance v0, Lee/cyber/smartid/dto/PushData;

    invoke-direct {v0, p1}, Lee/cyber/smartid/dto/PushData;-><init>(Ljava/lang/String;)V

    .line 3
    iget-object p1, p0, Lee/cyber/smartid/SmartIdService;->A:Lee/cyber/smartid/cryptolib/CryptoLib;

    iget-object v1, p0, Lee/cyber/smartid/SmartIdService;->i:Lee/cyber/smartid/inter/ServiceAccess;

    invoke-interface {v1}, Lee/cyber/smartid/inter/ServiceAccess;->getPushDataCurrentStorageId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1, v0}, Lee/cyber/smartid/cryptolib/CryptoLib;->storeData(Ljava/lang/String;Ljava/lang/Object;)V

    return-void
.end method

.method static synthetic e(Lee/cyber/smartid/SmartIdService;)Lee/cyber/smartid/dto/upgrade/InteractiveUpgradeState;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lee/cyber/smartid/cryptolib/dto/StorageException;
        }
    .end annotation

    .line 1
    invoke-direct {p0}, Lee/cyber/smartid/SmartIdService;->c()Lee/cyber/smartid/dto/upgrade/InteractiveUpgradeState;

    move-result-object p0

    return-object p0
.end method

.method private e(Ljava/lang/String;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lee/cyber/smartid/cryptolib/dto/StorageException;
        }
    .end annotation

    .line 2
    new-instance v0, Lee/cyber/smartid/dto/PushData;

    invoke-direct {v0, p1}, Lee/cyber/smartid/dto/PushData;-><init>(Ljava/lang/String;)V

    .line 3
    iget-object p1, p0, Lee/cyber/smartid/SmartIdService;->A:Lee/cyber/smartid/cryptolib/CryptoLib;

    iget-object v1, p0, Lee/cyber/smartid/SmartIdService;->i:Lee/cyber/smartid/inter/ServiceAccess;

    invoke-interface {v1}, Lee/cyber/smartid/inter/ServiceAccess;->getPushDataSentStorageId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1, v0}, Lee/cyber/smartid/cryptolib/CryptoLib;->storeData(Ljava/lang/String;Ljava/lang/Object;)V

    return-void
.end method

.method private e()Z
    .locals 3

    .line 2
    iget-object v0, p0, Lee/cyber/smartid/SmartIdService;->A:Lee/cyber/smartid/cryptolib/CryptoLib;

    new-instance v1, Lee/cyber/smartid/SmartIdService$3;

    invoke-direct {v1, p0}, Lee/cyber/smartid/SmartIdService$3;-><init>(Lee/cyber/smartid/SmartIdService;)V

    .line 3
    invoke-virtual {v1}, Lcom/google/gson/b/a;->getType()Ljava/lang/reflect/Type;

    move-result-object v1

    const-string v2, "ee.cyber.smartid.APP_STORAGE_VERSION"

    .line 4
    invoke-virtual {v0, v2, v1}, Lee/cyber/smartid/cryptolib/CryptoLib;->retrieveData(Ljava/lang/String;Ljava/lang/reflect/Type;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private f()J
    .locals 2

    const-string v0, "ee.cyber.smartid.TAG_UPDATE_DEVICE_CURRENT_RETRY_COUNT"

    .line 2
    invoke-direct {p0, v0}, Lee/cyber/smartid/SmartIdService;->a(Ljava/lang/String;)J

    move-result-wide v0

    return-wide v0
.end method

.method static synthetic f(Lee/cyber/smartid/SmartIdService;)Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;
    .locals 0

    .line 1
    iget-object p0, p0, Lee/cyber/smartid/SmartIdService;->H:Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;

    return-object p0
.end method

.method private f(Ljava/lang/String;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lee/cyber/smartid/cryptolib/dto/StorageException;
        }
    .end annotation

    .line 3
    iget-object v0, p0, Lee/cyber/smartid/SmartIdService;->A:Lee/cyber/smartid/cryptolib/CryptoLib;

    iget-object v1, p0, Lee/cyber/smartid/SmartIdService;->i:Lee/cyber/smartid/inter/ServiceAccess;

    invoke-interface {v1}, Lee/cyber/smartid/inter/ServiceAccess;->getUpdateDeviceHashStorageId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Lee/cyber/smartid/cryptolib/CryptoLib;->storeData(Ljava/lang/String;Ljava/lang/Object;)V

    return-void
.end method

.method static synthetic g(Lee/cyber/smartid/SmartIdService;)Lee/cyber/smartid/dto/jsonrpc/param/RegisterDeviceData;
    .locals 0

    .line 1
    invoke-direct {p0}, Lee/cyber/smartid/SmartIdService;->m()Lee/cyber/smartid/dto/jsonrpc/param/RegisterDeviceData;

    move-result-object p0

    return-object p0
.end method

.method private g()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lee/cyber/smartid/cryptolib/dto/StorageException;
        }
    .end annotation

    .line 15
    sget-object v0, Lee/cyber/smartid/SmartIdService;->c:Lee/cyber/smartid/util/Log;

    const-string v1, "clearRetryUpdateDeviceAlarm"

    invoke-virtual {v0, v1}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    const-wide/16 v0, 0x0

    .line 16
    invoke-direct {p0, v0, v1}, Lee/cyber/smartid/SmartIdService;->a(J)V

    .line 17
    iget-object v0, p0, Lee/cyber/smartid/SmartIdService;->d:Landroid/content/Context;

    const-string v1, "ee.cyber.smartid.ACTION_RETRY_UPDATE_DEVICE"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lee/cyber/smartid/SmartIdService;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private g(Ljava/lang/String;)Z
    .locals 1

    .line 7
    new-instance v0, Lee/cyber/smartid/dto/PushData;

    invoke-direct {v0, p1}, Lee/cyber/smartid/dto/PushData;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lee/cyber/smartid/SmartIdService;->a(Lee/cyber/smartid/dto/PushData;)Z

    move-result p1

    return p1
.end method

.method public static getInstance(Landroid/content/Context;)Lee/cyber/smartid/SmartIdService;
    .locals 2

    .line 1
    sget-object v0, Lee/cyber/smartid/SmartIdService;->b:Lee/cyber/smartid/SmartIdService;

    if-nez v0, :cond_1

    .line 2
    const-class v0, Lee/cyber/smartid/SmartIdService;

    monitor-enter v0

    .line 3
    :try_start_0
    sget-object v1, Lee/cyber/smartid/SmartIdService;->b:Lee/cyber/smartid/SmartIdService;

    if-nez v1, :cond_0

    .line 4
    new-instance v1, Lee/cyber/smartid/SmartIdService;

    invoke-direct {v1, p0}, Lee/cyber/smartid/SmartIdService;-><init>(Landroid/content/Context;)V

    sput-object v1, Lee/cyber/smartid/SmartIdService;->b:Lee/cyber/smartid/SmartIdService;

    .line 5
    :cond_0
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception p0

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p0

    .line 6
    :cond_1
    :goto_0
    sget-object p0, Lee/cyber/smartid/SmartIdService;->b:Lee/cyber/smartid/SmartIdService;

    return-object p0
.end method

.method static synthetic h(Lee/cyber/smartid/SmartIdService;)Lee/cyber/smartid/manager/inter/SafetyNetManager;
    .locals 0

    .line 1
    iget-object p0, p0, Lee/cyber/smartid/SmartIdService;->v:Lee/cyber/smartid/manager/inter/SafetyNetManager;

    return-object p0
.end method

.method private h()Ljava/lang/String;
    .locals 3

    .line 4
    iget-object v0, p0, Lee/cyber/smartid/SmartIdService;->A:Lee/cyber/smartid/cryptolib/CryptoLib;

    iget-object v1, p0, Lee/cyber/smartid/SmartIdService;->i:Lee/cyber/smartid/inter/ServiceAccess;

    invoke-interface {v1}, Lee/cyber/smartid/inter/ServiceAccess;->getUpdateDeviceHashStorageId()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lee/cyber/smartid/SmartIdService$26;

    invoke-direct {v2, p0}, Lee/cyber/smartid/SmartIdService$26;-><init>(Lee/cyber/smartid/SmartIdService;)V

    .line 5
    invoke-virtual {v2}, Lcom/google/gson/b/a;->getType()Ljava/lang/reflect/Type;

    move-result-object v2

    .line 6
    invoke-virtual {v0, v1, v2}, Lee/cyber/smartid/cryptolib/CryptoLib;->retrieveData(Ljava/lang/String;Ljava/lang/reflect/Type;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method static synthetic i(Lee/cyber/smartid/SmartIdService;)Lee/cyber/smartid/manager/inter/TransactionCacheManager;
    .locals 0

    .line 1
    iget-object p0, p0, Lee/cyber/smartid/SmartIdService;->B:Lee/cyber/smartid/manager/inter/TransactionCacheManager;

    return-object p0
.end method

.method private i()Z
    .locals 3

    .line 2
    iget-object v0, p0, Lee/cyber/smartid/SmartIdService;->A:Lee/cyber/smartid/cryptolib/CryptoLib;

    iget-object v1, p0, Lee/cyber/smartid/SmartIdService;->i:Lee/cyber/smartid/inter/ServiceAccess;

    invoke-interface {v1}, Lee/cyber/smartid/inter/ServiceAccess;->getPushDataCurrentStorageId()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lee/cyber/smartid/SmartIdService$28;

    invoke-direct {v2, p0}, Lee/cyber/smartid/SmartIdService$28;-><init>(Lee/cyber/smartid/SmartIdService;)V

    .line 3
    invoke-virtual {v2}, Lcom/google/gson/b/a;->getType()Ljava/lang/reflect/Type;

    move-result-object v2

    .line 4
    invoke-virtual {v0, v1, v2}, Lee/cyber/smartid/cryptolib/CryptoLib;->retrieveData(Ljava/lang/String;Ljava/lang/reflect/Type;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lee/cyber/smartid/dto/PushData;

    .line 5
    invoke-direct {p0, v0}, Lee/cyber/smartid/SmartIdService;->a(Lee/cyber/smartid/dto/PushData;)Z

    move-result v0

    return v0
.end method

.method private j()Landroid/os/PowerManager$WakeLock;
    .locals 3

    .line 4
    iget-object v0, p0, Lee/cyber/smartid/SmartIdService;->G:Landroid/os/PowerManager$WakeLock;

    if-nez v0, :cond_1

    .line 5
    iget-object v0, p0, Lee/cyber/smartid/SmartIdService;->d:Landroid/content/Context;

    const-string v1, "power"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    if-eqz v0, :cond_0

    const/4 v1, 0x1

    const-string v2, "ee.cyber.smartid:WAKELOCK_KEY_FOR_SMARTIDSERVICE"

    .line 6
    invoke-virtual {v0, v1, v2}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    iput-object v0, p0, Lee/cyber/smartid/SmartIdService;->G:Landroid/os/PowerManager$WakeLock;

    .line 7
    :cond_1
    iget-object v0, p0, Lee/cyber/smartid/SmartIdService;->G:Landroid/os/PowerManager$WakeLock;

    return-object v0
.end method

.method static synthetic j(Lee/cyber/smartid/SmartIdService;)Lee/cyber/smartid/manager/inter/TransactionRespMapper;
    .locals 0

    .line 1
    iget-object p0, p0, Lee/cyber/smartid/SmartIdService;->u:Lee/cyber/smartid/manager/inter/TransactionRespMapper;

    return-object p0
.end method

.method static synthetic k(Lee/cyber/smartid/SmartIdService;)Lee/cyber/smartid/manager/inter/PropertiesManager;
    .locals 0

    .line 1
    iget-object p0, p0, Lee/cyber/smartid/SmartIdService;->p:Lee/cyber/smartid/manager/inter/PropertiesManager;

    return-object p0
.end method

.method private k()V
    .locals 3

    .line 2
    iget-object v0, p0, Lee/cyber/smartid/SmartIdService;->e:Lee/cyber/smartid/tse/SmartIdTSE;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lee/cyber/smartid/tse/SmartIdTSE;->isDeviceRegistrationDone()Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    .line 3
    :cond_0
    new-instance v0, Lee/cyber/smartid/SmartIdService$30;

    invoke-direct {v0, p0}, Lee/cyber/smartid/SmartIdService$30;-><init>(Lee/cyber/smartid/SmartIdService;)V

    const/4 v1, 0x0

    const-string v2, "ee.cyber.smartid.TAG_UPDATE_DEVICE_POST_INIT"

    invoke-direct {p0, v2, v1, v0}, Lee/cyber/smartid/SmartIdService;->a(Ljava/lang/String;ZLee/cyber/smartid/inter/UpdateDeviceListener;)V

    return-void

    .line 4
    :cond_1
    :goto_0
    sget-object v0, Lee/cyber/smartid/SmartIdService;->c:Lee/cyber/smartid/util/Log;

    const-string v1, "updateDeviceForPostInitIfNeeded - device not registered, skipping .."

    invoke-virtual {v0, v1}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic l(Lee/cyber/smartid/SmartIdService;)Lee/cyber/smartid/manager/inter/VerificationCodeEvaluator;
    .locals 0

    .line 1
    iget-object p0, p0, Lee/cyber/smartid/SmartIdService;->s:Lee/cyber/smartid/manager/inter/VerificationCodeEvaluator;

    return-object p0
.end method

.method private l()V
    .locals 3

    .line 2
    new-instance v0, Lee/cyber/smartid/SmartIdService$31;

    invoke-direct {v0, p0}, Lee/cyber/smartid/SmartIdService$31;-><init>(Lee/cyber/smartid/SmartIdService;)V

    const-string v1, "ee.cyber.smartid.TAG_UPDATE_DEVICE_POST_GENERATE"

    const/4 v2, 0x0

    invoke-direct {p0, v1, v2, v0}, Lee/cyber/smartid/SmartIdService;->a(Ljava/lang/String;ZLee/cyber/smartid/inter/UpdateDeviceListener;)V

    return-void
.end method

.method private m()Lee/cyber/smartid/dto/jsonrpc/param/RegisterDeviceData;
    .locals 2

    .line 6
    new-instance v0, Lee/cyber/smartid/dto/jsonrpc/param/RegisterDeviceData;

    invoke-direct {v0}, Lee/cyber/smartid/dto/jsonrpc/param/RegisterDeviceData;-><init>()V

    .line 7
    invoke-direct {p0, v0}, Lee/cyber/smartid/SmartIdService;->c(Lee/cyber/smartid/dto/jsonrpc/param/RegisterDeviceData;)V

    .line 8
    invoke-direct {p0, v0}, Lee/cyber/smartid/SmartIdService;->a(Lee/cyber/smartid/dto/jsonrpc/param/RegisterDeviceData;)V

    .line 9
    iget-object v1, p0, Lee/cyber/smartid/SmartIdService;->d:Landroid/content/Context;

    invoke-static {v1}, Lee/cyber/smartid/util/Util;->getAppVersionName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lee/cyber/smartid/dto/jsonrpc/param/RegisterDeviceData;->setAppVersion(Ljava/lang/String;)V

    .line 10
    iget-object v1, p0, Lee/cyber/smartid/SmartIdService;->e:Lee/cyber/smartid/tse/SmartIdTSE;

    invoke-virtual {v1}, Lee/cyber/smartid/tse/SmartIdTSE;->getVersion()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lee/cyber/smartid/dto/jsonrpc/param/RegisterDeviceData;->setTseVersion(Ljava/lang/String;)V

    .line 11
    iget-object v1, p0, Lee/cyber/smartid/SmartIdService;->d:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lee/cyber/smartid/dto/jsonrpc/param/RegisterDeviceData;->setAppPackageName(Ljava/lang/String;)V

    return-object v0
.end method

.method static synthetic m(Lee/cyber/smartid/SmartIdService;)Lee/cyber/smartid/manager/inter/VerificationCodeGenerator;
    .locals 0

    .line 1
    iget-object p0, p0, Lee/cyber/smartid/SmartIdService;->t:Lee/cyber/smartid/manager/inter/VerificationCodeGenerator;

    return-object p0
.end method

.method static synthetic n(Lee/cyber/smartid/SmartIdService;)Lee/cyber/smartid/inter/ServiceAccess;
    .locals 0

    .line 1
    iget-object p0, p0, Lee/cyber/smartid/SmartIdService;->i:Lee/cyber/smartid/inter/ServiceAccess;

    return-object p0
.end method

.method private n()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lee/cyber/smartid/cryptolib/dto/StorageException;
        }
    .end annotation

    .line 2
    sget-object v0, Lee/cyber/smartid/SmartIdService;->c:Lee/cyber/smartid/util/Log;

    const-string v1, "setRetryUpdateDeviceAlarm"

    invoke-virtual {v0, v1}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    .line 3
    invoke-direct {p0}, Lee/cyber/smartid/SmartIdService;->f()J

    move-result-wide v0

    long-to-int v1, v0

    const/4 v0, 0x0

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    int-to-long v1, v0

    .line 4
    invoke-direct {p0, v1, v2}, Lee/cyber/smartid/SmartIdService;->a(J)V

    .line 5
    sget-object v1, Lee/cyber/smartid/SmartIdService;->c:Lee/cyber/smartid/util/Log;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setRetryUpdateDeviceAlarm - currentRetryCount: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    .line 6
    iget-object v1, p0, Lee/cyber/smartid/SmartIdService;->d:Landroid/content/Context;

    invoke-direct {p0, v0}, Lee/cyber/smartid/SmartIdService;->b(I)J

    move-result-wide v2

    const-string v0, "ee.cyber.smartid.ACTION_RETRY_UPDATE_DEVICE"

    const/4 v4, 0x0

    invoke-static {v1, v0, v2, v3, v4}, Lee/cyber/smartid/SmartIdService;->b(Landroid/content/Context;Ljava/lang/String;JLjava/lang/String;)V

    return-void
.end method

.method static synthetic o(Lee/cyber/smartid/SmartIdService;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lee/cyber/smartid/SmartIdService;->k()V

    return-void
.end method

.method static synthetic p(Lee/cyber/smartid/SmartIdService;)Landroid/os/PowerManager$WakeLock;
    .locals 0

    .line 1
    invoke-direct {p0}, Lee/cyber/smartid/SmartIdService;->j()Landroid/os/PowerManager$WakeLock;

    move-result-object p0

    return-object p0
.end method

.method static synthetic q(Lee/cyber/smartid/SmartIdService;)Lee/cyber/smartid/manager/inter/PropertiesSource;
    .locals 0

    .line 1
    iget-object p0, p0, Lee/cyber/smartid/SmartIdService;->E:Lee/cyber/smartid/manager/inter/PropertiesSource;

    return-object p0
.end method

.method static synthetic r(Lee/cyber/smartid/SmartIdService;)Lee/cyber/smartid/manager/inter/StoredAccountManager;
    .locals 0

    .line 1
    iget-object p0, p0, Lee/cyber/smartid/SmartIdService;->n:Lee/cyber/smartid/manager/inter/StoredAccountManager;

    return-object p0
.end method

.method static synthetic s(Lee/cyber/smartid/SmartIdService;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lee/cyber/smartid/SmartIdService;->l()V

    return-void
.end method

.method static synthetic t(Lee/cyber/smartid/SmartIdService;)Lee/cyber/smartid/manager/impl/TSEErrorToServiceErrorMapperImpl;
    .locals 0

    .line 1
    iget-object p0, p0, Lee/cyber/smartid/SmartIdService;->r:Lee/cyber/smartid/manager/impl/TSEErrorToServiceErrorMapperImpl;

    return-object p0
.end method

.method static synthetic u(Lee/cyber/smartid/SmartIdService;)Lee/cyber/smartid/manager/inter/DeleteAccountManager;
    .locals 0

    .line 1
    iget-object p0, p0, Lee/cyber/smartid/SmartIdService;->k:Lee/cyber/smartid/manager/inter/DeleteAccountManager;

    return-object p0
.end method

.method static synthetic v(Lee/cyber/smartid/SmartIdService;)Lee/cyber/smartid/manager/inter/PostInitManager;
    .locals 0

    .line 1
    iget-object p0, p0, Lee/cyber/smartid/SmartIdService;->w:Lee/cyber/smartid/manager/inter/PostInitManager;

    return-object p0
.end method

.method static synthetic w(Lee/cyber/smartid/SmartIdService;)Lee/cyber/smartid/manager/inter/AddAccountManager;
    .locals 0

    .line 1
    iget-object p0, p0, Lee/cyber/smartid/SmartIdService;->m:Lee/cyber/smartid/manager/inter/AddAccountManager;

    return-object p0
.end method

.method static synthetic x(Lee/cyber/smartid/SmartIdService;)Ljava/util/WeakHashMap;
    .locals 0

    .line 1
    iget-object p0, p0, Lee/cyber/smartid/SmartIdService;->h:Ljava/util/WeakHashMap;

    return-object p0
.end method

.method static synthetic y(Lee/cyber/smartid/SmartIdService;)Lee/cyber/smartid/manager/inter/TSEListenerToServiceListenerMapper;
    .locals 0

    .line 1
    iget-object p0, p0, Lee/cyber/smartid/SmartIdService;->q:Lee/cyber/smartid/manager/inter/TSEListenerToServiceListenerMapper;

    return-object p0
.end method

.method static synthetic z(Lee/cyber/smartid/SmartIdService;)Ljava/util/ArrayList;
    .locals 0

    .line 1
    iget-object p0, p0, Lee/cyber/smartid/SmartIdService;->y:Ljava/util/ArrayList;

    return-object p0
.end method


# virtual methods
.method public addAccount(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/inter/AddAccountListener;)V
    .locals 10

    .line 1
    invoke-direct {p0}, Lee/cyber/smartid/SmartIdService;->b()V

    move-object v0, p0

    .line 2
    iget-object v1, v0, Lee/cyber/smartid/SmartIdService;->m:Lee/cyber/smartid/manager/inter/AddAccountManager;

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    invoke-interface/range {v1 .. v9}, Lee/cyber/smartid/manager/inter/AddAccountManager;->addAccount(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/inter/AddAccountListener;)V

    return-void
.end method

.method public addSystemEventListener(Ljava/lang/String;Lee/cyber/smartid/inter/SystemEventListener;)V
    .locals 1

    .line 1
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2
    sget-object p1, Lee/cyber/smartid/SmartIdService;->c:Lee/cyber/smartid/util/Log;

    const-string p2, "addSystemEventListener - tag is null, aborting .."

    invoke-virtual {p1, p2}, Lee/cyber/smartid/util/Log;->e(Ljava/lang/String;)V

    return-void

    :cond_0
    if-nez p2, :cond_1

    .line 3
    invoke-virtual {p0, p1}, Lee/cyber/smartid/SmartIdService;->removeSystemEventListener(Ljava/lang/String;)V

    return-void

    .line 4
    :cond_1
    invoke-virtual {p0, p1, p2}, Lee/cyber/smartid/SmartIdService;->setListener(Ljava/lang/String;Lee/cyber/smartid/inter/ServiceListener;)V

    return-void
.end method

.method public cancelInteractiveUpgrade(Ljava/lang/String;Lee/cyber/smartid/inter/CancelInteractiveUpgradeListener;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/SmartIdService;->H:Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;

    invoke-virtual {v0, p1, p2}, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->cancelInteractiveUpgrade(Ljava/lang/String;Lee/cyber/smartid/inter/CancelInteractiveUpgradeListener;)V

    return-void
.end method

.method public cancelRPRequest(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/inter/CancelRPRequestListener;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Lee/cyber/smartid/SmartIdService;->b()V

    .line 2
    iget-object v0, p0, Lee/cyber/smartid/SmartIdService;->o:Lee/cyber/smartid/manager/inter/TransactionAndRPRequestManager;

    invoke-interface {v0, p1, p2, p3, p4}, Lee/cyber/smartid/manager/inter/TransactionAndRPRequestManager;->cancelRPRequest(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/inter/CancelRPRequestListener;)V

    return-void
.end method

.method public cancelTransaction(Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/inter/CancelTransactionListener;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Lee/cyber/smartid/SmartIdService;->b()V

    .line 2
    iget-object v0, p0, Lee/cyber/smartid/SmartIdService;->o:Lee/cyber/smartid/manager/inter/TransactionAndRPRequestManager;

    invoke-interface {v0, p1, p2, p3}, Lee/cyber/smartid/manager/inter/TransactionAndRPRequestManager;->cancelTransaction(Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/inter/CancelTransactionListener;)V

    return-void
.end method

.method public checkLocalPendingStateForAccountRegistration(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/inter/CheckLocalPendingStateListener;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Lee/cyber/smartid/SmartIdService;->b()V

    .line 2
    invoke-virtual {p0, p1, p4}, Lee/cyber/smartid/SmartIdService;->setListener(Ljava/lang/String;Lee/cyber/smartid/inter/ServiceListener;)V

    .line 3
    new-instance p4, Ljava/lang/Thread;

    new-instance v0, Lee/cyber/smartid/SmartIdService$17;

    invoke-direct {v0, p0, p2, p3, p1}, Lee/cyber/smartid/SmartIdService$17;-><init>(Lee/cyber/smartid/SmartIdService;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p4, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 4
    invoke-virtual {p4}, Ljava/lang/Thread;->start()V

    return-void
.end method

.method public checkLocalPendingStateForKey(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/inter/CheckLocalPendingStateListener;)V
    .locals 2

    .line 1
    invoke-direct {p0}, Lee/cyber/smartid/SmartIdService;->b()V

    .line 2
    sget-object v0, Lee/cyber/smartid/SmartIdService;->ACCOUNT_KEY_TYPES:[Ljava/lang/String;

    const/4 v1, 0x0

    invoke-static {v0, p3, v1}, Lee/cyber/smartid/util/Util;->contains([Ljava/lang/String;Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_0

    .line 3
    new-instance p2, Lee/cyber/smartid/SmartIdService$18;

    invoke-direct {p2, p0, p4, p1, p3}, Lee/cyber/smartid/SmartIdService$18;-><init>(Lee/cyber/smartid/SmartIdService;Lee/cyber/smartid/inter/CheckLocalPendingStateListener;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, p2}, Lee/cyber/smartid/SmartIdService;->a(Ljava/lang/Runnable;)V

    return-void

    .line 4
    :cond_0
    invoke-virtual {p0, p1, p4}, Lee/cyber/smartid/SmartIdService;->setListener(Ljava/lang/String;Lee/cyber/smartid/inter/ServiceListener;)V

    .line 5
    iget-object p4, p0, Lee/cyber/smartid/SmartIdService;->e:Lee/cyber/smartid/tse/SmartIdTSE;

    invoke-direct {p0, p2, p3}, Lee/cyber/smartid/SmartIdService;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lee/cyber/smartid/SmartIdService$19;

    invoke-direct {v1, p0, p2, p3}, Lee/cyber/smartid/SmartIdService$19;-><init>(Lee/cyber/smartid/SmartIdService;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p4, p1, v0, v1}, Lee/cyber/smartid/tse/SmartIdTSE;->checkLocalPendingState(Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/tse/inter/CheckLocalPendingStateListener;)V

    return-void
.end method

.method public confirmRPRequest(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/inter/ConfirmRPRequestListener;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Lee/cyber/smartid/SmartIdService;->b()V

    .line 2
    iget-object v0, p0, Lee/cyber/smartid/SmartIdService;->o:Lee/cyber/smartid/manager/inter/TransactionAndRPRequestManager;

    invoke-interface {v0, p1, p2, p3, p4}, Lee/cyber/smartid/manager/inter/TransactionAndRPRequestManager;->confirmRPRequest(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/inter/ConfirmRPRequestListener;)V

    return-void
.end method

.method public confirmTransaction(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/inter/ConfirmTransactionListener;)V
    .locals 8

    .line 1
    sget-object v0, Lee/cyber/smartid/SmartIdService;->c:Lee/cyber/smartid/util/Log;

    const-string v1, "confirmTransaction"

    invoke-virtual {v0, v1}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    .line 2
    invoke-direct {p0}, Lee/cyber/smartid/SmartIdService;->b()V

    .line 3
    iget-object v2, p0, Lee/cyber/smartid/SmartIdService;->o:Lee/cyber/smartid/manager/inter/TransactionAndRPRequestManager;

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    move-object v6, p4

    move-object v7, p5

    invoke-interface/range {v2 .. v7}, Lee/cyber/smartid/manager/inter/TransactionAndRPRequestManager;->confirmTransaction(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/inter/ConfirmTransactionListener;)V

    return-void
.end method

.method public createRequestClient()Lokhttp3/w$a;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    invoke-direct {p0}, Lee/cyber/smartid/SmartIdService;->b()V

    .line 2
    iget-object v0, p0, Lee/cyber/smartid/SmartIdService;->e:Lee/cyber/smartid/tse/SmartIdTSE;

    invoke-virtual {v0}, Lee/cyber/smartid/tse/SmartIdTSE;->createRequestClient()Lokhttp3/w$a;

    move-result-object v0

    return-object v0
.end method

.method public createTransactionForRPRequest(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/inter/CreateTransactionForRPRequestListener;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Lee/cyber/smartid/SmartIdService;->b()V

    .line 2
    iget-object v0, p0, Lee/cyber/smartid/SmartIdService;->o:Lee/cyber/smartid/manager/inter/TransactionAndRPRequestManager;

    invoke-interface {v0, p1, p2, p3, p4}, Lee/cyber/smartid/manager/inter/TransactionAndRPRequestManager;->createTransactionForRPRequest(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/inter/CreateTransactionForRPRequestListener;)V

    return-void
.end method

.method public deleteAccount(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/inter/DeleteAccountListener;)V
    .locals 6

    const/4 v4, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v5, p4

    .line 1
    invoke-direct/range {v0 .. v5}, Lee/cyber/smartid/SmartIdService;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLee/cyber/smartid/inter/DeleteAccountListener;)V

    return-void
.end method

.method public encryptKeys(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/inter/EncryptKeysListener;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Lee/cyber/smartid/SmartIdService;->b()V

    .line 2
    invoke-virtual {p0, p1, p6}, Lee/cyber/smartid/SmartIdService;->setListener(Ljava/lang/String;Lee/cyber/smartid/inter/ServiceListener;)V

    .line 3
    sget-object p6, Lee/cyber/smartid/SmartIdService;->c:Lee/cyber/smartid/util/Log;

    const-string v0, "encryptKeys"

    invoke-virtual {p6, v0}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    .line 4
    invoke-direct/range {p0 .. p5}, Lee/cyber/smartid/SmartIdService;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public generateKeys(Ljava/lang/String;ILee/cyber/smartid/inter/GenerateKeysListener;)V
    .locals 7

    .line 1
    invoke-direct {p0}, Lee/cyber/smartid/SmartIdService;->b()V

    .line 2
    invoke-virtual {p0, p1, p3}, Lee/cyber/smartid/SmartIdService;->setListener(Ljava/lang/String;Lee/cyber/smartid/inter/ServiceListener;)V

    .line 3
    iget-object v0, p0, Lee/cyber/smartid/SmartIdService;->e:Lee/cyber/smartid/tse/SmartIdTSE;

    iget-object v1, p0, Lee/cyber/smartid/SmartIdService;->p:Lee/cyber/smartid/manager/inter/PropertiesManager;

    invoke-interface {v1}, Lee/cyber/smartid/manager/inter/PropertiesManager;->getPropDefaultKeyLengthBits()Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v3

    sget-object v4, Lee/cyber/smartid/SmartIdService;->a:Ljava/math/BigInteger;

    iget-object v1, p0, Lee/cyber/smartid/SmartIdService;->p:Lee/cyber/smartid/manager/inter/PropertiesManager;

    invoke-interface {v1}, Lee/cyber/smartid/manager/inter/PropertiesManager;->getPropDefaultKeyLengthBits()Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    add-int/lit8 v1, v1, 0x7

    div-int/lit8 v5, v1, 0x8

    iget-object v1, p0, Lee/cyber/smartid/SmartIdService;->q:Lee/cyber/smartid/manager/inter/TSEListenerToServiceListenerMapper;

    invoke-interface {v1, p3}, Lee/cyber/smartid/manager/inter/TSEListenerToServiceListenerMapper;->map(Lee/cyber/smartid/inter/ServiceListener;)Lee/cyber/smartid/tse/inter/TSEListener;

    move-result-object p3

    move-object v6, p3

    check-cast v6, Lee/cyber/smartid/tse/inter/GenerateKeysListener;

    move-object v1, p1

    move v2, p2

    invoke-virtual/range {v0 .. v6}, Lee/cyber/smartid/tse/SmartIdTSE;->generateKeys(Ljava/lang/String;IILjava/math/BigInteger;ILee/cyber/smartid/tse/inter/GenerateKeysListener;)V

    return-void
.end method

.method public getAccountStatus(Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/inter/GetAccountStatusListener;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Lee/cyber/smartid/SmartIdService;->b()V

    .line 2
    iget-object v0, p0, Lee/cyber/smartid/SmartIdService;->l:Lee/cyber/smartid/manager/inter/GetAccountStatusManager;

    invoke-interface {v0, p1, p2, p3}, Lee/cyber/smartid/manager/inter/GetAccountStatusManager;->getAccountStatus(Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/inter/GetAccountStatusListener;)V

    return-void
.end method

.method public getAccounts(Ljava/lang/String;Lee/cyber/smartid/inter/GetAccountsListener;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Lee/cyber/smartid/SmartIdService;->b()V

    .line 2
    iget-object v0, p0, Lee/cyber/smartid/SmartIdService;->n:Lee/cyber/smartid/manager/inter/StoredAccountManager;

    invoke-interface {v0, p1, p2}, Lee/cyber/smartid/manager/inter/StoredAccountManager;->getAccounts(Ljava/lang/String;Lee/cyber/smartid/inter/GetAccountsListener;)V

    return-void
.end method

.method public getDeviceFingerprint(Ljava/lang/String;Lee/cyber/smartid/inter/GetDeviceFingerprintListener;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/SmartIdService;->C:Lee/cyber/smartid/manager/inter/DeviceFingerprintManager;

    invoke-interface {v0, p1, p2}, Lee/cyber/smartid/manager/inter/DeviceFingerprintManager;->getDeviceFingerprint(Ljava/lang/String;Lee/cyber/smartid/inter/GetDeviceFingerprintListener;)V

    return-void
.end method

.method public getKeyPinLength(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Lee/cyber/smartid/inter/GetKeyPinLengthListener;)V
    .locals 7

    .line 1
    invoke-direct {p0}, Lee/cyber/smartid/SmartIdService;->b()V

    .line 2
    invoke-virtual {p0, p1, p5}, Lee/cyber/smartid/SmartIdService;->setListener(Ljava/lang/String;Lee/cyber/smartid/inter/ServiceListener;)V

    .line 3
    new-instance p5, Ljava/lang/Thread;

    new-instance v6, Lee/cyber/smartid/SmartIdService$8;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p2

    move-object v3, p3

    move-object v4, p1

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lee/cyber/smartid/SmartIdService$8;-><init>(Lee/cyber/smartid/SmartIdService;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V

    invoke-direct {p5, v6}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 4
    invoke-virtual {p5}, Ljava/lang/Thread;->start()V

    return-void
.end method

.method public getPendingOperation(Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/inter/GetPendingOperationListener;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Lee/cyber/smartid/SmartIdService;->b()V

    .line 2
    iget-object v0, p0, Lee/cyber/smartid/SmartIdService;->o:Lee/cyber/smartid/manager/inter/TransactionAndRPRequestManager;

    invoke-interface {v0, p1, p2, p3}, Lee/cyber/smartid/manager/inter/TransactionAndRPRequestManager;->getPendingOperation(Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/inter/GetPendingOperationListener;)V

    return-void
.end method

.method public getPushNotificationData(Ljava/lang/String;Lee/cyber/smartid/inter/GetPushNotificationDataListener;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Lee/cyber/smartid/SmartIdService;->b()V

    .line 2
    invoke-virtual {p0, p1, p2}, Lee/cyber/smartid/SmartIdService;->setListener(Ljava/lang/String;Lee/cyber/smartid/inter/ServiceListener;)V

    .line 3
    new-instance p2, Ljava/lang/Thread;

    new-instance v0, Lee/cyber/smartid/SmartIdService$16;

    invoke-direct {v0, p0, p1}, Lee/cyber/smartid/SmartIdService$16;-><init>(Lee/cyber/smartid/SmartIdService;Ljava/lang/String;)V

    invoke-direct {p2, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 4
    invoke-virtual {p2}, Ljava/lang/Thread;->start()V

    return-void
.end method

.method public getRPRequest(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/inter/GetRPRequestListener;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Lee/cyber/smartid/SmartIdService;->b()V

    .line 2
    iget-object v0, p0, Lee/cyber/smartid/SmartIdService;->o:Lee/cyber/smartid/manager/inter/TransactionAndRPRequestManager;

    invoke-interface {v0, p1, p2, p3, p4}, Lee/cyber/smartid/manager/inter/TransactionAndRPRequestManager;->getRPRequest(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/inter/GetRPRequestListener;)V

    return-void
.end method

.method public getRegistrationToken(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/inter/GetRegistrationTokenListener;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lee/cyber/smartid/SmartIdService;->b()V

    .line 2
    invoke-virtual {p0, p1, p6}, Lee/cyber/smartid/SmartIdService;->setListener(Ljava/lang/String;Lee/cyber/smartid/inter/ServiceListener;)V

    const-string p6, "KEYTOKEN"

    .line 3
    invoke-static {p6, p2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result p6

    if-nez p6, :cond_0

    .line 4
    new-instance p2, Lee/cyber/smartid/SmartIdService$34;

    invoke-direct {p2, p0, p1}, Lee/cyber/smartid/SmartIdService$34;-><init>(Lee/cyber/smartid/SmartIdService;Ljava/lang/String;)V

    invoke-direct {p0, p2}, Lee/cyber/smartid/SmartIdService;->a(Ljava/lang/Runnable;)V

    return-void

    :cond_0
    const-string p6, "MODULI-WITH-NONCE-SHA256"

    .line 5
    invoke-static {p6, p3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result p6

    if-nez p6, :cond_1

    .line 6
    new-instance p2, Lee/cyber/smartid/SmartIdService$35;

    invoke-direct {p2, p0, p1}, Lee/cyber/smartid/SmartIdService$35;-><init>(Lee/cyber/smartid/SmartIdService;Ljava/lang/String;)V

    invoke-direct {p0, p2}, Lee/cyber/smartid/SmartIdService;->a(Ljava/lang/Runnable;)V

    return-void

    .line 7
    :cond_1
    new-instance p6, Lee/cyber/smartid/dto/jsonrpc/param/GetRegTokenNonceParams;

    invoke-direct {p6, p3, p2}, Lee/cyber/smartid/dto/jsonrpc/param/GetRegTokenNonceParams;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 8
    :try_start_0
    iget-object p2, p0, Lee/cyber/smartid/SmartIdService;->e:Lee/cyber/smartid/tse/SmartIdTSE;

    new-instance p3, Lee/cyber/smartid/SmartIdService$36;

    invoke-direct {p3, p0, p6}, Lee/cyber/smartid/SmartIdService$36;-><init>(Lee/cyber/smartid/SmartIdService;Lee/cyber/smartid/dto/jsonrpc/param/GetRegTokenNonceParams;)V

    invoke-virtual {p2, p4, p3}, Lee/cyber/smartid/tse/SmartIdTSE;->addKeyMaterialValuesForServer(Ljava/lang/String;Lee/cyber/smartid/tse/inter/KeyMaterialApplier;)V

    .line 9
    iget-object p2, p0, Lee/cyber/smartid/SmartIdService;->e:Lee/cyber/smartid/tse/SmartIdTSE;

    new-instance p3, Lee/cyber/smartid/SmartIdService$37;

    invoke-direct {p3, p0, p6}, Lee/cyber/smartid/SmartIdService$37;-><init>(Lee/cyber/smartid/SmartIdService;Lee/cyber/smartid/dto/jsonrpc/param/GetRegTokenNonceParams;)V

    invoke-virtual {p2, p5, p3}, Lee/cyber/smartid/tse/SmartIdTSE;->addKeyMaterialValuesForServer(Ljava/lang/String;Lee/cyber/smartid/tse/inter/KeyMaterialApplier;)V
    :try_end_0
    .catch Lee/cyber/smartid/tse/dto/NoSuchKeysException; {:try_start_0 .. :try_end_0} :catch_0

    .line 10
    new-instance p2, Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCRequest;

    const-string p3, "getRegTokenNonce"

    invoke-direct {p2, p3, p6}, Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCRequest;-><init>(Ljava/lang/String;Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCParams;)V

    .line 11
    iget-object p3, p0, Lee/cyber/smartid/SmartIdService;->g:Lee/cyber/smartid/network/SmartIdAPI;

    invoke-interface {p3, p2}, Lee/cyber/smartid/network/SmartIdAPI;->getRegTokenNonce(Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCRequest;)Lc/b;

    move-result-object p3

    .line 12
    new-instance p4, Lee/cyber/smartid/SmartIdService$39;

    iget-object p5, p0, Lee/cyber/smartid/SmartIdService;->e:Lee/cyber/smartid/tse/SmartIdTSE;

    invoke-direct {p4, p0, p2, p5, p1}, Lee/cyber/smartid/SmartIdService$39;-><init>(Lee/cyber/smartid/SmartIdService;Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCRequest;Lee/cyber/smartid/tse/SmartIdTSE;Ljava/lang/String;)V

    invoke-interface {p3, p4}, Lc/b;->a(Lc/d;)V

    return-void

    .line 13
    :catch_0
    new-instance p2, Lee/cyber/smartid/SmartIdService$38;

    invoke-direct {p2, p0, p1}, Lee/cyber/smartid/SmartIdService$38;-><init>(Lee/cyber/smartid/SmartIdService;Ljava/lang/String;)V

    invoke-direct {p0, p2}, Lee/cyber/smartid/SmartIdService;->a(Ljava/lang/Runnable;)V

    return-void
.end method

.method public getSafetyNetAttestation(Ljava/lang/String;ZLee/cyber/smartid/inter/GetSafetyNetAttestationListener;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Lee/cyber/smartid/SmartIdService;->b()V

    .line 2
    iget-object v0, p0, Lee/cyber/smartid/SmartIdService;->v:Lee/cyber/smartid/manager/inter/SafetyNetManager;

    invoke-interface {v0, p1, p2, p3}, Lee/cyber/smartid/manager/inter/SafetyNetManager;->getSafetyNetAttestation(Ljava/lang/String;ZLee/cyber/smartid/inter/GetSafetyNetAttestationListener;)V

    return-void
.end method

.method public getSecureRandom(Ljava/lang/String;Lee/cyber/smartid/inter/GetSecureRandomListener;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Lee/cyber/smartid/SmartIdService;->b()V

    .line 2
    invoke-virtual {p0, p1, p2}, Lee/cyber/smartid/SmartIdService;->setListener(Ljava/lang/String;Lee/cyber/smartid/inter/ServiceListener;)V

    .line 3
    new-instance p2, Lee/cyber/smartid/dto/jsonrpc/resp/GetSecureRandomResp;

    iget-object v0, p0, Lee/cyber/smartid/SmartIdService;->A:Lee/cyber/smartid/cryptolib/CryptoLib;

    invoke-virtual {v0}, Lee/cyber/smartid/cryptolib/CryptoLib;->getRandom()Ljava/security/SecureRandom;

    move-result-object v0

    invoke-direct {p2, p1, v0}, Lee/cyber/smartid/dto/jsonrpc/resp/GetSecureRandomResp;-><init>(Ljava/lang/String;Ljava/security/SecureRandom;)V

    .line 4
    new-instance v0, Lee/cyber/smartid/SmartIdService$41;

    invoke-direct {v0, p0, p1, p2}, Lee/cyber/smartid/SmartIdService$41;-><init>(Lee/cyber/smartid/SmartIdService;Ljava/lang/String;Lee/cyber/smartid/dto/jsonrpc/resp/GetSecureRandomResp;)V

    invoke-direct {p0, v0}, Lee/cyber/smartid/SmartIdService;->a(Ljava/lang/Runnable;)V

    return-void
.end method

.method public getTransaction(Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/inter/GetTransactionListener;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Lee/cyber/smartid/SmartIdService;->b()V

    .line 2
    iget-object v0, p0, Lee/cyber/smartid/SmartIdService;->o:Lee/cyber/smartid/manager/inter/TransactionAndRPRequestManager;

    invoke-interface {v0, p1, p2, p3}, Lee/cyber/smartid/manager/inter/TransactionAndRPRequestManager;->getTransaction(Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/inter/GetTransactionListener;)V

    return-void
.end method

.method public getVersion()Ljava/lang/String;
    .locals 1

    const-string v0, "12.1.117-sk-release"

    return-object v0
.end method

.method public getVersionCryptoLib()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/SmartIdService;->A:Lee/cyber/smartid/cryptolib/CryptoLib;

    invoke-virtual {v0}, Lee/cyber/smartid/cryptolib/CryptoLib;->getVersion()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getVersionSmartIdTSE()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/SmartIdService;->e:Lee/cyber/smartid/tse/SmartIdTSE;

    invoke-virtual {v0}, Lee/cyber/smartid/tse/SmartIdTSE;->getVersion()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public initService(Ljava/lang/String;Lee/cyber/smartid/inter/InitServiceListener;)V
    .locals 3

    .line 1
    sget-object v0, Lee/cyber/smartid/SmartIdService;->c:Lee/cyber/smartid/util/Log;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "initService - tag: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    .line 2
    invoke-direct {p0}, Lee/cyber/smartid/SmartIdService;->j()Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    const-wide/32 v1, 0x1d4c0

    invoke-static {v0, v1, v2}, Lee/cyber/smartid/util/Util;->acquireWakeLock(Landroid/os/PowerManager$WakeLock;J)V

    .line 3
    sget-object v0, Lee/cyber/smartid/SmartIdService;->c:Lee/cyber/smartid/util/Log;

    const-string v1, "initService - wakelock taken"

    invoke-virtual {v0, v1}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    .line 4
    invoke-virtual {p0, p1, p2}, Lee/cyber/smartid/SmartIdService;->setListener(Ljava/lang/String;Lee/cyber/smartid/inter/ServiceListener;)V

    .line 5
    new-instance p2, Ljava/lang/Thread;

    new-instance v0, Lee/cyber/smartid/SmartIdService$1;

    invoke-direct {v0, p0, p1}, Lee/cyber/smartid/SmartIdService$1;-><init>(Lee/cyber/smartid/SmartIdService;Ljava/lang/String;)V

    invoke-direct {p2, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 6
    invoke-virtual {p2}, Ljava/lang/Thread;->start()V

    return-void
.end method

.method public isInitDone()Z
    .locals 2

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/SmartIdService;->F:Ljava/lang/Object;

    monitor-enter v0

    .line 2
    :try_start_0
    iget-boolean v1, p0, Lee/cyber/smartid/SmartIdService;->f:Z

    monitor-exit v0

    return v1

    :catchall_0
    move-exception v1

    .line 3
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public removeListener(Ljava/lang/String;)V
    .locals 2

    .line 1
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 2
    :cond_0
    iget-object v0, p0, Lee/cyber/smartid/SmartIdService;->h:Ljava/util/WeakHashMap;

    monitor-enter v0

    .line 3
    :try_start_0
    iget-object v1, p0, Lee/cyber/smartid/SmartIdService;->h:Ljava/util/WeakHashMap;

    invoke-virtual {v1, p1}, Ljava/util/WeakHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 4
    monitor-exit v0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method public removeSystemEventListener(Ljava/lang/String;)V
    .locals 1

    .line 1
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2
    sget-object p1, Lee/cyber/smartid/SmartIdService;->c:Lee/cyber/smartid/util/Log;

    const-string v0, "removeSystemEventListener - tag is null, aborting .."

    invoke-virtual {p1, v0}, Lee/cyber/smartid/util/Log;->e(Ljava/lang/String;)V

    return-void

    .line 3
    :cond_0
    invoke-virtual {p0, p1}, Lee/cyber/smartid/SmartIdService;->removeListener(Ljava/lang/String;)V

    return-void
.end method

.method public retryLocalPendingState(Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/inter/ServiceListener;)V
    .locals 2

    .line 1
    invoke-direct {p0}, Lee/cyber/smartid/SmartIdService;->b()V

    .line 2
    invoke-virtual {p0, p1, p3}, Lee/cyber/smartid/SmartIdService;->setListener(Ljava/lang/String;Lee/cyber/smartid/inter/ServiceListener;)V

    .line 3
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lee/cyber/smartid/SmartIdService$20;

    invoke-direct {v1, p0, p2, p1, p3}, Lee/cyber/smartid/SmartIdService$20;-><init>(Lee/cyber/smartid/SmartIdService;Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/inter/ServiceListener;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 4
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    return-void
.end method

.method public setListener(Ljava/lang/String;Lee/cyber/smartid/inter/ServiceListener;)V
    .locals 3

    .line 1
    sget-object v0, Lee/cyber/smartid/SmartIdService;->c:Lee/cyber/smartid/util/Log;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setListener: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, ", listener: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    .line 2
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 3
    :cond_0
    iget-object v0, p0, Lee/cyber/smartid/SmartIdService;->h:Ljava/util/WeakHashMap;

    monitor-enter v0

    if-nez p2, :cond_1

    .line 4
    :try_start_0
    invoke-virtual {p0, p1}, Lee/cyber/smartid/SmartIdService;->removeListener(Ljava/lang/String;)V

    goto :goto_0

    .line 5
    :cond_1
    iget-object v1, p0, Lee/cyber/smartid/SmartIdService;->h:Ljava/util/WeakHashMap;

    invoke-virtual {v1, p1, p2}, Ljava/util/WeakHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 6
    :goto_0
    monitor-exit v0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method public storePushNotificationData(Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/inter/StorePushNotificationDataListener;)V
    .locals 3

    .line 1
    sget-object v0, Lee/cyber/smartid/SmartIdService;->c:Lee/cyber/smartid/util/Log;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "storePushNotificationData - token: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    .line 2
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lee/cyber/smartid/SmartIdService$9;

    invoke-direct {v1, p0, p1, p2, p3}, Lee/cyber/smartid/SmartIdService$9;-><init>(Lee/cyber/smartid/SmartIdService;Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/inter/StorePushNotificationDataListener;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 3
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    return-void
.end method

.method public validatePins(Ljava/lang/String;Ljava/util/ArrayList;Lee/cyber/smartid/inter/ValidatePinListener;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList<",
            "Lee/cyber/smartid/tse/dto/PinInfo;",
            ">;",
            "Lee/cyber/smartid/inter/ValidatePinListener;",
            ")V"
        }
    .end annotation

    .line 1
    invoke-direct {p0}, Lee/cyber/smartid/SmartIdService;->b()V

    .line 2
    sget-object v0, Lee/cyber/smartid/SmartIdService;->c:Lee/cyber/smartid/util/Log;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "validatePins - tag "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    .line 3
    invoke-virtual {p0, p1, p3}, Lee/cyber/smartid/SmartIdService;->setListener(Ljava/lang/String;Lee/cyber/smartid/inter/ServiceListener;)V

    .line 4
    iget-object v0, p0, Lee/cyber/smartid/SmartIdService;->e:Lee/cyber/smartid/tse/SmartIdTSE;

    iget-object v1, p0, Lee/cyber/smartid/SmartIdService;->q:Lee/cyber/smartid/manager/inter/TSEListenerToServiceListenerMapper;

    invoke-interface {v1, p3}, Lee/cyber/smartid/manager/inter/TSEListenerToServiceListenerMapper;->map(Lee/cyber/smartid/inter/ServiceListener;)Lee/cyber/smartid/tse/inter/TSEListener;

    move-result-object p3

    check-cast p3, Lee/cyber/smartid/tse/inter/ValidatePinListener;

    invoke-virtual {v0, p1, p2, p3}, Lee/cyber/smartid/tse/SmartIdTSE;->validatePins(Ljava/lang/String;Ljava/util/ArrayList;Lee/cyber/smartid/tse/inter/ValidatePinListener;)V

    return-void
.end method

.method public verifyTransactionVerificationCode(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/inter/VerifyTransactionVerificationCodeListener;)V
    .locals 2

    .line 1
    sget-object v0, Lee/cyber/smartid/SmartIdService;->c:Lee/cyber/smartid/util/Log;

    const-string v1, "verifyTransactionVerificationCode"

    invoke-virtual {v0, v1}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    .line 2
    invoke-direct {p0}, Lee/cyber/smartid/SmartIdService;->b()V

    .line 3
    iget-object v0, p0, Lee/cyber/smartid/SmartIdService;->o:Lee/cyber/smartid/manager/inter/TransactionAndRPRequestManager;

    invoke-interface {v0, p1, p2, p3, p4}, Lee/cyber/smartid/manager/inter/TransactionAndRPRequestManager;->verifyTransactionVerificationCode(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/inter/VerifyTransactionVerificationCodeListener;)V

    return-void
.end method
