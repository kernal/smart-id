.class Lee/cyber/smartid/SmartIdService$8;
.super Ljava/lang/Object;
.source "SmartIdService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lee/cyber/smartid/SmartIdService;->getKeyPinLength(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Lee/cyber/smartid/inter/GetKeyPinLengthListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:Ljava/lang/String;

.field final synthetic c:Ljava/lang/String;

.field final synthetic d:Landroid/os/Bundle;

.field final synthetic e:Lee/cyber/smartid/SmartIdService;


# direct methods
.method constructor <init>(Lee/cyber/smartid/SmartIdService;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lee/cyber/smartid/SmartIdService$8;->e:Lee/cyber/smartid/SmartIdService;

    iput-object p2, p0, Lee/cyber/smartid/SmartIdService$8;->a:Ljava/lang/String;

    iput-object p3, p0, Lee/cyber/smartid/SmartIdService$8;->b:Ljava/lang/String;

    iput-object p4, p0, Lee/cyber/smartid/SmartIdService$8;->c:Ljava/lang/String;

    iput-object p5, p0, Lee/cyber/smartid/SmartIdService$8;->d:Landroid/os/Bundle;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .line 1
    :try_start_0
    iget-object v0, p0, Lee/cyber/smartid/SmartIdService$8;->e:Lee/cyber/smartid/SmartIdService;

    invoke-static {v0}, Lee/cyber/smartid/SmartIdService;->d(Lee/cyber/smartid/SmartIdService;)Lee/cyber/smartid/tse/SmartIdTSE;

    move-result-object v0

    iget-object v1, p0, Lee/cyber/smartid/SmartIdService$8;->e:Lee/cyber/smartid/SmartIdService;

    iget-object v2, p0, Lee/cyber/smartid/SmartIdService$8;->a:Ljava/lang/String;

    iget-object v3, p0, Lee/cyber/smartid/SmartIdService$8;->b:Ljava/lang/String;

    invoke-static {v1, v2, v3}, Lee/cyber/smartid/SmartIdService;->a(Lee/cyber/smartid/SmartIdService;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lee/cyber/smartid/tse/SmartIdTSE;->getKeyPinLength(Ljava/lang/String;)I

    move-result v0

    .line 2
    iget-object v1, p0, Lee/cyber/smartid/SmartIdService$8;->e:Lee/cyber/smartid/SmartIdService;

    new-instance v2, Lee/cyber/smartid/SmartIdService$8$1;

    invoke-direct {v2, p0, v0}, Lee/cyber/smartid/SmartIdService$8$1;-><init>(Lee/cyber/smartid/SmartIdService$8;I)V

    invoke-static {v1, v2}, Lee/cyber/smartid/SmartIdService;->a(Lee/cyber/smartid/SmartIdService;Ljava/lang/Runnable;)V
    :try_end_0
    .catch Lee/cyber/smartid/tse/dto/NoSuchKeysException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 3
    invoke-static {}, Lee/cyber/smartid/SmartIdService;->a()Lee/cyber/smartid/util/Log;

    move-result-object v1

    const-string v2, "getKeyPinLength"

    invoke-virtual {v1, v2, v0}, Lee/cyber/smartid/util/Log;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 4
    iget-object v1, p0, Lee/cyber/smartid/SmartIdService$8;->e:Lee/cyber/smartid/SmartIdService;

    new-instance v2, Lee/cyber/smartid/SmartIdService$8$2;

    invoke-direct {v2, p0, v0}, Lee/cyber/smartid/SmartIdService$8$2;-><init>(Lee/cyber/smartid/SmartIdService$8;Lee/cyber/smartid/tse/dto/NoSuchKeysException;)V

    invoke-static {v1, v2}, Lee/cyber/smartid/SmartIdService;->a(Lee/cyber/smartid/SmartIdService;Ljava/lang/Runnable;)V

    :goto_0
    return-void
.end method
