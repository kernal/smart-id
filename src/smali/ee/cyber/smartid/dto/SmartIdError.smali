.class public Lee/cyber/smartid/dto/SmartIdError;
.super Lee/cyber/smartid/tse/dto/BaseError;
.source "SmartIdError.java"


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lee/cyber/smartid/dto/SmartIdError;",
            ">;"
        }
    .end annotation
.end field

.field public static final ERROR_CODE_ADDITIONAL_VERIFICATION_CODE_GENERATION_FAILED:J = 0xc353L

.field public static final ERROR_CODE_INSUFFICIENT_AMOUNT_OF_PINS_PROVIDED:J = 0x3fbL

.field public static final ERROR_CODE_INTERACTIVE_UPGRADE_IN_PROGRESS:J = 0x415L

.field public static final ERROR_CODE_INTERACTIVE_UPGRADE_NOT_FOUND:J = 0x416L

.field public static final ERROR_CODE_INTERACTIVE_UPGRADE_PROCESS_FAILED:J = 0x414L

.field public static final ERROR_CODE_NO_SUCH_TRANSACTION:J = 0x3eeL

.field public static final ERROR_CODE_SAFETY_NET_FAILED:J = 0x402L

.field public static final ERROR_CODE_SERVER_CANNOT_ISSUE_REGISTRATION_TOKEN:J = -0x792cL

.field public static final ERROR_CODE_SERVER_CLIENT_NOT_SUPPORTED:J = -0x7935L

.field public static final ERROR_CODE_SERVER_REGISTRATION_TOKEN_INVALID:J = -0x7918L

.field public static final ERROR_CODE_SERVER_RP_REQUEST_NOT_FOUND:J = -0x7922L

.field public static final ERROR_CODE_UNSUPPORTED_REGISTRATION_INIT_TYPE:J = 0x3f8L

.field public static final ERROR_CODE_UNSUPPORTED_TOKEN_GENERATION_ALGORITHM:J = 0x3fdL

.field public static final ERROR_CODE_UNSUPPORTED_TOKEN_GENERATION_INIT_TYPE:J = 0x3fcL

.field public static final ERROR_CODE_VERIFICATION_CODE_GENERATION_FAILED:J = 0xc351L

.field public static final ERROR_CODE_WRONG_TRANSACTION_TYPE:J = 0x3edL

.field public static final ERROR_CODE_WRONG_VERIFICATION_CODE:J = 0xc352L

.field public static final ERROR_SERVER_RESPONSE_VALIDATION_FAILED:J = 0x40cL

.field public static final EXTRA_SAFETY_NET_RESP:Ljava/lang/String; = "ee.cyber.smartid.EXTRA_SAFETY_NET_RESP"

.field public static final EXTRA_SERVICE_ACCOUNT_KEY_STATUS_INFO:Ljava/lang/String; = "ee.cyber.smartid.EXTRA_SERVICE_ACCOUNT_KEY_STATUS_INFO"


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1
    new-instance v0, Lee/cyber/smartid/dto/SmartIdError$1;

    invoke-direct {v0}, Lee/cyber/smartid/dto/SmartIdError$1;-><init>()V

    sput-object v0, Lee/cyber/smartid/dto/SmartIdError;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(JJLjava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 8

    move-object v0, p0

    move-wide v1, p3

    move-object v3, p5

    move-object v4, p7

    move-object v5, p6

    move-wide v6, p1

    .line 2
    invoke-direct/range {v0 .. v7}, Lee/cyber/smartid/tse/dto/BaseError;-><init>(JLjava/lang/String;Landroid/os/Bundle;Ljava/lang/String;J)V

    return-void
.end method

.method protected constructor <init>(Landroid/os/Parcel;)V
    .locals 0

    .line 3
    invoke-direct {p0, p1}, Lee/cyber/smartid/tse/dto/BaseError;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method private constructor <init>(Lee/cyber/smartid/tse/inter/WallClock;JLjava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 10

    if-eqz p1, :cond_0

    .line 1
    invoke-interface {p1}, Lee/cyber/smartid/tse/inter/WallClock;->currentTimeMillis()J

    move-result-wide v0

    goto :goto_0

    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    :goto_0
    move-wide v3, v0

    move-object v2, p0

    move-wide v5, p2

    move-object v7, p4

    move-object v8, p5

    move-object/from16 v9, p6

    invoke-direct/range {v2 .. v9}, Lee/cyber/smartid/dto/SmartIdError;-><init>(JJLjava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V

    return-void
.end method

.method private static filterExtras(Landroid/os/Bundle;)Landroid/os/Bundle;
    .locals 4

    if-nez p0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 1
    :cond_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2
    invoke-virtual {v0, p0}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    const-string p0, "ee.cyber.smartid.EXTRA_KEY_STATUS_INFO"

    .line 3
    invoke-virtual {v0, p0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 4
    :try_start_0
    invoke-virtual {v0, p0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v1

    check-cast v1, Lee/cyber/smartid/tse/dto/jsonrpc/AccountKeyStatus;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v1, :cond_1

    const-string v2, "ee.cyber.smartid.EXTRA_SERVICE_ACCOUNT_KEY_STATUS_INFO"

    .line 5
    :try_start_1
    invoke-static {v1}, Lee/cyber/smartid/dto/jsonrpc/ServiceAccountKeyStatus;->from(Lee/cyber/smartid/tse/dto/jsonrpc/AccountKeyStatus;)Lee/cyber/smartid/dto/jsonrpc/ServiceAccountKeyStatus;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    .line 6
    const-class v2, Lee/cyber/smartid/dto/SmartIdError;

    invoke-static {v2}, Lee/cyber/smartid/util/Log;->getInstance(Ljava/lang/Class;)Lee/cyber/smartid/util/Log;

    move-result-object v2

    const-string v3, "filterExtras"

    invoke-virtual {v2, v3, v1}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_1
    :goto_0
    const-string v1, "ee.cyber.smartid.EXTRA_KEY_ID"

    .line 7
    invoke-virtual {v0, v1}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    const-string v1, "ee.cyber.smartid.EXTRA_IS_NON_RETRIABLE"

    .line 8
    invoke-virtual {v0, v1}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    .line 9
    invoke-virtual {v0, p0}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    return-object v0
.end method

.method public static from(Landroid/content/Context;Lee/cyber/smartid/tse/inter/WallClock;Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCError;Ljava/lang/Throwable;)Lee/cyber/smartid/dto/SmartIdError;
    .locals 7

    if-eqz p2, :cond_0

    .line 1
    invoke-static {p1, p2}, Lee/cyber/smartid/dto/SmartIdError;->from(Lee/cyber/smartid/tse/inter/WallClock;Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCError;)Lee/cyber/smartid/dto/SmartIdError;

    move-result-object p0

    return-object p0

    :cond_0
    if-eqz p3, :cond_1

    .line 2
    invoke-static {p0, p1, p3}, Lee/cyber/smartid/dto/SmartIdError;->from(Landroid/content/Context;Lee/cyber/smartid/tse/inter/WallClock;Ljava/lang/Throwable;)Lee/cyber/smartid/dto/SmartIdError;

    move-result-object p0

    return-object p0

    .line 3
    :cond_1
    new-instance p0, Lee/cyber/smartid/dto/SmartIdError;

    const-wide/16 v2, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v6}, Lee/cyber/smartid/dto/SmartIdError;-><init>(Lee/cyber/smartid/tse/inter/WallClock;JLjava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V

    return-object p0
.end method

.method public static from(Landroid/content/Context;Lee/cyber/smartid/tse/inter/WallClock;Ljava/lang/Throwable;)Lee/cyber/smartid/dto/SmartIdError;
    .locals 8

    .line 4
    new-instance v7, Lee/cyber/smartid/dto/SmartIdError;

    const-wide/16 v2, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object v0, v7

    move-object v1, p1

    invoke-direct/range {v0 .. v6}, Lee/cyber/smartid/dto/SmartIdError;-><init>(Lee/cyber/smartid/tse/inter/WallClock;JLjava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 5
    instance-of p1, p2, Lee/cyber/smartid/tse/dto/AuthorizationRequiredException;

    if-eqz p1, :cond_0

    const-wide/16 v0, 0x3f7

    .line 6
    iput-wide v0, v7, Lee/cyber/smartid/tse/dto/BaseError;->code:J

    .line 7
    sget p1, Lee/cyber/smartid/R$string;->err_invalid_or_missing_authorization:I

    invoke-virtual {p0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p0

    iput-object p0, v7, Lee/cyber/smartid/tse/dto/BaseError;->message:Ljava/lang/String;

    goto/16 :goto_3

    .line 8
    :cond_0
    instance-of p1, p2, Lee/cyber/smartid/tse/dto/ClientTooOldException;

    if-eqz p1, :cond_1

    const-wide/16 v0, 0x44c

    .line 9
    iput-wide v0, v7, Lee/cyber/smartid/tse/dto/BaseError;->code:J

    .line 10
    sget p1, Lee/cyber/smartid/R$string;->err_client_too_old:I

    invoke-virtual {p0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p0

    iput-object p0, v7, Lee/cyber/smartid/tse/dto/BaseError;->message:Ljava/lang/String;

    goto/16 :goto_3

    .line 11
    :cond_1
    instance-of p1, p2, Lee/cyber/smartid/tse/dto/SystemUnderMaintenanceException;

    if-eqz p1, :cond_2

    const-wide/16 v0, 0x44d

    .line 12
    iput-wide v0, v7, Lee/cyber/smartid/tse/dto/BaseError;->code:J

    .line 13
    sget p1, Lee/cyber/smartid/R$string;->err_system_under_maintenance:I

    invoke-virtual {p0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p0

    iput-object p0, v7, Lee/cyber/smartid/tse/dto/BaseError;->message:Ljava/lang/String;

    goto/16 :goto_3

    .line 14
    :cond_2
    instance-of p1, p2, Ljavax/net/ssl/SSLException;

    if-nez p1, :cond_18

    instance-of p1, p2, Ljava/net/UnknownServiceException;

    if-eqz p1, :cond_3

    goto/16 :goto_2

    .line 15
    :cond_3
    instance-of p1, p2, Ljava/net/UnknownHostException;

    if-eqz p1, :cond_4

    const-wide/16 v0, 0x40e

    .line 16
    iput-wide v0, v7, Lee/cyber/smartid/tse/dto/BaseError;->code:J

    .line 17
    sget p1, Lee/cyber/smartid/tse/R$string;->err_failed_to_resolve_the_hostname_exception:I

    invoke-virtual {p0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p0

    iput-object p0, v7, Lee/cyber/smartid/tse/dto/BaseError;->message:Ljava/lang/String;

    goto/16 :goto_3

    .line 18
    :cond_4
    instance-of p1, p2, Ljavax/security/cert/CertificateException;

    if-eqz p1, :cond_5

    const-wide/16 v0, 0x40f

    .line 19
    iput-wide v0, v7, Lee/cyber/smartid/tse/dto/BaseError;->code:J

    .line 20
    sget p1, Lee/cyber/smartid/tse/R$string;->err_certificate_exception:I

    invoke-virtual {p0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p0

    iput-object p0, v7, Lee/cyber/smartid/tse/dto/BaseError;->message:Ljava/lang/String;

    goto/16 :goto_3

    .line 21
    :cond_5
    instance-of p1, p2, Ljava/net/ProtocolException;

    if-eqz p1, :cond_6

    const-wide/16 v0, 0x410

    .line 22
    iput-wide v0, v7, Lee/cyber/smartid/tse/dto/BaseError;->code:J

    .line 23
    sget p1, Lee/cyber/smartid/tse/R$string;->err_protocol_exception:I

    invoke-virtual {p0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p0

    iput-object p0, v7, Lee/cyber/smartid/tse/dto/BaseError;->message:Ljava/lang/String;

    goto/16 :goto_3

    .line 24
    :cond_6
    instance-of p1, p2, Ljava/net/MalformedURLException;

    if-eqz p1, :cond_7

    const-wide/16 v0, 0x411

    .line 25
    iput-wide v0, v7, Lee/cyber/smartid/tse/dto/BaseError;->code:J

    .line 26
    sget p1, Lee/cyber/smartid/tse/R$string;->err_malformed_url_exception:I

    invoke-virtual {p0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p0

    iput-object p0, v7, Lee/cyber/smartid/tse/dto/BaseError;->message:Ljava/lang/String;

    goto/16 :goto_3

    .line 27
    :cond_7
    instance-of p1, p2, Ljava/security/GeneralSecurityException;

    if-eqz p1, :cond_8

    const-wide/16 v0, 0x412

    .line 28
    iput-wide v0, v7, Lee/cyber/smartid/tse/dto/BaseError;->code:J

    .line 29
    sget p1, Lee/cyber/smartid/tse/R$string;->err_general_security_exception:I

    invoke-virtual {p0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p0

    iput-object p0, v7, Lee/cyber/smartid/tse/dto/BaseError;->message:Ljava/lang/String;

    goto/16 :goto_3

    .line 30
    :cond_8
    instance-of p1, p2, Lee/cyber/smartid/tse/dto/InvalidResponseIdException;

    if-eqz p1, :cond_9

    const-wide/16 v0, 0x3eb

    .line 31
    iput-wide v0, v7, Lee/cyber/smartid/tse/dto/BaseError;->code:J

    .line 32
    sget p1, Lee/cyber/smartid/R$string;->err_server_response_id_invalid:I

    invoke-virtual {p0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p0

    iput-object p0, v7, Lee/cyber/smartid/tse/dto/BaseError;->message:Ljava/lang/String;

    goto/16 :goto_3

    .line 33
    :cond_9
    instance-of p1, p2, Lee/cyber/smartid/tse/dto/InvalidResponseResultException;

    if-eqz p1, :cond_a

    const-wide/16 v0, 0x3ec

    .line 34
    iput-wide v0, v7, Lee/cyber/smartid/tse/dto/BaseError;->code:J

    .line 35
    sget p1, Lee/cyber/smartid/R$string;->err_server_response_invalid:I

    invoke-virtual {p0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p0

    iput-object p0, v7, Lee/cyber/smartid/tse/dto/BaseError;->message:Ljava/lang/String;

    goto/16 :goto_3

    .line 36
    :cond_a
    instance-of p1, p2, Ljava/net/SocketTimeoutException;

    if-eqz p1, :cond_b

    const-wide/16 v0, 0x3e9

    .line 37
    iput-wide v0, v7, Lee/cyber/smartid/tse/dto/BaseError;->code:J

    .line 38
    sget p1, Lee/cyber/smartid/R$string;->err_server_response_timeout:I

    invoke-virtual {p0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p0

    iput-object p0, v7, Lee/cyber/smartid/tse/dto/BaseError;->message:Ljava/lang/String;

    goto/16 :goto_3

    .line 39
    :cond_b
    instance-of p1, p2, Lee/cyber/smartid/tse/dto/HttpErrorCodeResponseException;

    const/4 v0, 0x1

    if-eqz p1, :cond_c

    .line 40
    move-object p1, p2

    check-cast p1, Lee/cyber/smartid/tse/dto/HttpErrorCodeResponseException;

    invoke-virtual {p1}, Lee/cyber/smartid/tse/dto/HttpErrorCodeResponseException;->getServerResponseCode()I

    move-result p1

    const-wide/16 v1, 0x40b

    .line 41
    iput-wide v1, v7, Lee/cyber/smartid/tse/dto/BaseError;->code:J

    .line 42
    sget v1, Lee/cyber/smartid/tse/R$string;->err_server_response_non_200_OK_X:I

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v0, v3

    invoke-virtual {p0, v1, v0}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    iput-object p0, v7, Lee/cyber/smartid/tse/dto/BaseError;->message:Ljava/lang/String;

    .line 43
    invoke-virtual {v7}, Lee/cyber/smartid/tse/dto/BaseError;->createOrGetExtras()Landroid/os/Bundle;

    move-result-object p0

    const-string v0, "ee.cyber.smartid.EXTRA_SERVER_HTTP_RESPONSE_CODE"

    invoke-virtual {p0, v0, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    goto/16 :goto_3

    .line 44
    :cond_c
    instance-of p1, p2, Lee/cyber/smartid/cryptolib/dto/StorageException;

    if-eqz p1, :cond_d

    move-object v1, p2

    check-cast v1, Lee/cyber/smartid/cryptolib/dto/StorageException;

    invoke-virtual {v1}, Lee/cyber/smartid/cryptolib/dto/StorageException;->getErrorCode()I

    move-result v1

    if-ne v1, v0, :cond_d

    const-wide/16 v0, 0x404

    .line 45
    iput-wide v0, v7, Lee/cyber/smartid/tse/dto/BaseError;->code:J

    .line 46
    sget p1, Lee/cyber/smartid/R$string;->err_failed_to_write_to_storage:I

    invoke-virtual {p0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p0

    iput-object p0, v7, Lee/cyber/smartid/tse/dto/BaseError;->message:Ljava/lang/String;

    goto/16 :goto_3

    :cond_d
    if-eqz p1, :cond_e

    .line 47
    move-object p1, p2

    check-cast p1, Lee/cyber/smartid/cryptolib/dto/StorageException;

    invoke-virtual {p1}, Lee/cyber/smartid/cryptolib/dto/StorageException;->getErrorCode()I

    move-result p1

    const/4 v0, 0x2

    if-ne p1, v0, :cond_e

    const-wide/16 v0, 0x405

    .line 48
    iput-wide v0, v7, Lee/cyber/smartid/tse/dto/BaseError;->code:J

    .line 49
    sget p1, Lee/cyber/smartid/R$string;->err_failed_to_read_from_storage:I

    invoke-virtual {p0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p0

    iput-object p0, v7, Lee/cyber/smartid/tse/dto/BaseError;->message:Ljava/lang/String;

    goto/16 :goto_3

    .line 50
    :cond_e
    instance-of p1, p2, Lee/cyber/smartid/tse/dto/NoSuchKeysException;

    if-eqz p1, :cond_f

    .line 51
    move-object p1, p2

    check-cast p1, Lee/cyber/smartid/tse/dto/NoSuchKeysException;

    invoke-virtual {p1}, Lee/cyber/smartid/tse/dto/NoSuchKeysException;->getErrorCode()J

    move-result-wide v0

    iput-wide v0, v7, Lee/cyber/smartid/tse/dto/BaseError;->code:J

    .line 52
    sget p1, Lee/cyber/smartid/R$string;->err_no_such_keys_found_regenerate_keys:I

    invoke-virtual {p0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p0

    iput-object p0, v7, Lee/cyber/smartid/tse/dto/BaseError;->message:Ljava/lang/String;

    goto/16 :goto_3

    .line 53
    :cond_f
    instance-of p1, p2, Lee/cyber/smartid/dto/NoSuchTransactionException;

    if-eqz p1, :cond_10

    .line 54
    move-object p0, p2

    check-cast p0, Lee/cyber/smartid/dto/NoSuchTransactionException;

    invoke-virtual {p0}, Lee/cyber/smartid/dto/NoSuchTransactionException;->getErrorCode()J

    move-result-wide p0

    iput-wide p0, v7, Lee/cyber/smartid/tse/dto/BaseError;->code:J

    .line 55
    invoke-virtual {p2}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object p0

    iput-object p0, v7, Lee/cyber/smartid/tse/dto/BaseError;->message:Ljava/lang/String;

    goto/16 :goto_3

    .line 56
    :cond_10
    instance-of p1, p2, Lee/cyber/smartid/dto/UnknownKeyTypeException;

    if-eqz p1, :cond_11

    .line 57
    move-object p0, p2

    check-cast p0, Lee/cyber/smartid/dto/UnknownKeyTypeException;

    invoke-virtual {p0}, Lee/cyber/smartid/dto/UnknownKeyTypeException;->getErrorCode()J

    move-result-wide p0

    iput-wide p0, v7, Lee/cyber/smartid/tse/dto/BaseError;->code:J

    .line 58
    invoke-virtual {p2}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object p0

    iput-object p0, v7, Lee/cyber/smartid/tse/dto/BaseError;->message:Ljava/lang/String;

    goto/16 :goto_3

    .line 59
    :cond_11
    instance-of p1, p2, Lee/cyber/smartid/dto/VerificationCodeException;

    if-eqz p1, :cond_12

    .line 60
    move-object p0, p2

    check-cast p0, Lee/cyber/smartid/dto/VerificationCodeException;

    invoke-virtual {p0}, Lee/cyber/smartid/dto/VerificationCodeException;->getErrorCode()J

    move-result-wide p0

    iput-wide p0, v7, Lee/cyber/smartid/tse/dto/BaseError;->code:J

    .line 61
    invoke-virtual {p2}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object p0

    iput-object p0, v7, Lee/cyber/smartid/tse/dto/BaseError;->message:Ljava/lang/String;

    goto :goto_3

    .line 62
    :cond_12
    instance-of p1, p2, Ljava/io/IOException;

    if-eqz p1, :cond_13

    const-wide/16 v0, 0x3e8

    .line 63
    iput-wide v0, v7, Lee/cyber/smartid/tse/dto/BaseError;->code:J

    .line 64
    sget p1, Lee/cyber/smartid/R$string;->err_network_connection:I

    invoke-virtual {p0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p0

    iput-object p0, v7, Lee/cyber/smartid/tse/dto/BaseError;->message:Ljava/lang/String;

    goto :goto_3

    .line 65
    :cond_13
    instance-of p1, p2, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;

    if-eqz p1, :cond_16

    .line 66
    move-object p0, p2

    check-cast p0, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;

    invoke-virtual {p0}, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;->getErrorCode()I

    move-result p1

    const/16 v0, 0x73

    if-ne p1, v0, :cond_14

    const-wide/16 p0, 0x40d

    .line 67
    iput-wide p0, v7, Lee/cyber/smartid/tse/dto/BaseError;->code:J

    goto :goto_0

    .line 68
    :cond_14
    invoke-virtual {p0}, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;->getErrorCode()I

    move-result p1

    const/16 v0, 0x68

    if-ne p1, v0, :cond_15

    const-wide/16 p0, 0x3fa

    .line 69
    iput-wide p0, v7, Lee/cyber/smartid/tse/dto/BaseError;->code:J

    goto :goto_0

    .line 70
    :cond_15
    invoke-virtual {p0}, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;->getErrorCode()I

    move-result p0

    int-to-long p0, p0

    iput-wide p0, v7, Lee/cyber/smartid/tse/dto/BaseError;->code:J

    .line 71
    :goto_0
    invoke-virtual {p2}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object p0

    iput-object p0, v7, Lee/cyber/smartid/tse/dto/BaseError;->message:Ljava/lang/String;

    goto :goto_3

    :cond_16
    const-wide/16 v0, 0x0

    .line 72
    iput-wide v0, v7, Lee/cyber/smartid/tse/dto/BaseError;->code:J

    if-eqz p2, :cond_17

    .line 73
    invoke-virtual {p2}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p1

    if-nez p1, :cond_17

    invoke-virtual {p2}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object p0

    goto :goto_1

    :cond_17
    sget p1, Lee/cyber/smartid/R$string;->err_unknown:I

    invoke-virtual {p0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p0

    :goto_1
    iput-object p0, v7, Lee/cyber/smartid/tse/dto/BaseError;->message:Ljava/lang/String;

    goto :goto_3

    :cond_18
    :goto_2
    const-wide/16 v0, 0x3f9

    .line 74
    iput-wide v0, v7, Lee/cyber/smartid/tse/dto/BaseError;->code:J

    .line 75
    sget p1, Lee/cyber/smartid/tse/R$string;->err_ssl_exception:I

    invoke-virtual {p0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p0

    iput-object p0, v7, Lee/cyber/smartid/tse/dto/BaseError;->message:Ljava/lang/String;

    .line 76
    :goto_3
    invoke-static {p2}, Lee/cyber/smartid/util/Util;->toString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object p0

    iput-object p0, v7, Lee/cyber/smartid/tse/dto/BaseError;->trace:Ljava/lang/String;

    return-object v7
.end method

.method public static from(Lee/cyber/smartid/tse/inter/WallClock;JLjava/lang/String;)Lee/cyber/smartid/dto/SmartIdError;
    .locals 8

    .line 77
    new-instance v7, Lee/cyber/smartid/dto/SmartIdError;

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object v0, v7

    move-object v1, p0

    move-wide v2, p1

    move-object v4, p3

    invoke-direct/range {v0 .. v6}, Lee/cyber/smartid/dto/SmartIdError;-><init>(Lee/cyber/smartid/tse/inter/WallClock;JLjava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V

    return-object v7
.end method

.method public static from(Lee/cyber/smartid/tse/inter/WallClock;JLjava/lang/String;Ljava/io/Serializable;Ljava/lang/String;)Lee/cyber/smartid/dto/SmartIdError;
    .locals 8

    .line 87
    new-instance v7, Lee/cyber/smartid/dto/SmartIdError;

    const-wide/16 v2, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object v0, v7

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Lee/cyber/smartid/dto/SmartIdError;-><init>(Lee/cyber/smartid/tse/inter/WallClock;JLjava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 88
    iput-wide p1, v7, Lee/cyber/smartid/tse/dto/BaseError;->code:J

    .line 89
    iput-object p3, v7, Lee/cyber/smartid/tse/dto/BaseError;->message:Ljava/lang/String;

    .line 90
    invoke-static {p5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p0

    if-nez p0, :cond_0

    if-eqz p4, :cond_0

    .line 91
    invoke-virtual {v7}, Lee/cyber/smartid/tse/dto/BaseError;->createOrGetExtras()Landroid/os/Bundle;

    move-result-object p0

    invoke-virtual {p0, p5, p4}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    :cond_0
    return-object v7
.end method

.method public static from(Lee/cyber/smartid/tse/inter/WallClock;JLjava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Lee/cyber/smartid/dto/SmartIdError;
    .locals 8

    .line 79
    new-instance v7, Lee/cyber/smartid/dto/SmartIdError;

    move-object v0, v7

    move-object v1, p0

    move-wide v2, p1

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lee/cyber/smartid/dto/SmartIdError;-><init>(Lee/cyber/smartid/tse/inter/WallClock;JLjava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V

    return-object v7
.end method

.method public static from(Lee/cyber/smartid/tse/inter/WallClock;JLjava/lang/String;Ljava/lang/Throwable;)Lee/cyber/smartid/dto/SmartIdError;
    .locals 8

    .line 78
    new-instance v7, Lee/cyber/smartid/dto/SmartIdError;

    invoke-static {p4}, Lee/cyber/smartid/util/Util;->toString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    move-object v0, v7

    move-object v1, p0

    move-wide v2, p1

    move-object v4, p3

    invoke-direct/range {v0 .. v6}, Lee/cyber/smartid/dto/SmartIdError;-><init>(Lee/cyber/smartid/tse/inter/WallClock;JLjava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V

    return-object v7
.end method

.method public static from(Lee/cyber/smartid/tse/inter/WallClock;Lee/cyber/smartid/tse/dto/BaseError;)Lee/cyber/smartid/dto/SmartIdError;
    .locals 16

    if-nez p1, :cond_0

    .line 92
    new-instance v7, Lee/cyber/smartid/dto/SmartIdError;

    const-wide/16 v2, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object v0, v7

    move-object/from16 v1, p0

    invoke-direct/range {v0 .. v6}, Lee/cyber/smartid/dto/SmartIdError;-><init>(Lee/cyber/smartid/tse/inter/WallClock;JLjava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V

    return-object v7

    .line 93
    :cond_0
    new-instance v0, Lee/cyber/smartid/dto/SmartIdError;

    invoke-virtual/range {p1 .. p1}, Lee/cyber/smartid/tse/dto/BaseError;->getCreatedAtTimestamp()J

    move-result-wide v9

    invoke-virtual/range {p1 .. p1}, Lee/cyber/smartid/tse/dto/BaseError;->getCode()J

    move-result-wide v11

    invoke-virtual/range {p1 .. p1}, Lee/cyber/smartid/tse/dto/BaseError;->getMessage()Ljava/lang/String;

    move-result-object v13

    invoke-virtual/range {p1 .. p1}, Lee/cyber/smartid/tse/dto/BaseError;->getTrace()Ljava/lang/String;

    move-result-object v14

    invoke-virtual/range {p1 .. p1}, Lee/cyber/smartid/tse/dto/BaseError;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    invoke-static {v1}, Lee/cyber/smartid/dto/SmartIdError;->filterExtras(Landroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object v15

    move-object v8, v0

    invoke-direct/range {v8 .. v15}, Lee/cyber/smartid/dto/SmartIdError;-><init>(JJLjava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V

    return-object v0
.end method

.method public static from(Lee/cyber/smartid/tse/inter/WallClock;Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCError;)Lee/cyber/smartid/dto/SmartIdError;
    .locals 8

    .line 80
    new-instance v7, Lee/cyber/smartid/dto/SmartIdError;

    const-wide/16 v2, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object v0, v7

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Lee/cyber/smartid/dto/SmartIdError;-><init>(Lee/cyber/smartid/tse/inter/WallClock;JLjava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 81
    invoke-virtual {p1}, Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCError;->getCode()J

    move-result-wide v0

    iput-wide v0, v7, Lee/cyber/smartid/tse/dto/BaseError;->code:J

    .line 82
    invoke-virtual {p1}, Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCError;->getMessage()Ljava/lang/String;

    move-result-object p0

    iput-object p0, v7, Lee/cyber/smartid/tse/dto/BaseError;->message:Ljava/lang/String;

    .line 83
    invoke-virtual {p1}, Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCError;->getData()Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCErrorData;

    move-result-object p0

    if-eqz p0, :cond_0

    invoke-virtual {p1}, Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCError;->getData()Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCErrorData;

    move-result-object p0

    invoke-virtual {p0}, Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCErrorData;->getKeyStatusInfo()Lee/cyber/smartid/tse/dto/jsonrpc/AccountKeyStatus;

    move-result-object p0

    if-eqz p0, :cond_0

    .line 84
    invoke-virtual {v7}, Lee/cyber/smartid/tse/dto/BaseError;->createOrGetExtras()Landroid/os/Bundle;

    move-result-object p0

    invoke-virtual {p1}, Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCError;->getData()Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCErrorData;

    move-result-object v0

    invoke-virtual {v0}, Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCErrorData;->getKeyStatusInfo()Lee/cyber/smartid/tse/dto/jsonrpc/AccountKeyStatus;

    move-result-object v0

    invoke-static {v0}, Lee/cyber/smartid/dto/jsonrpc/ServiceAccountKeyStatus;->from(Lee/cyber/smartid/tse/dto/jsonrpc/AccountKeyStatus;)Lee/cyber/smartid/dto/jsonrpc/ServiceAccountKeyStatus;

    move-result-object v0

    const-string v1, "ee.cyber.smartid.EXTRA_SERVICE_ACCOUNT_KEY_STATUS_INFO"

    invoke-virtual {p0, v1, v0}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 85
    :cond_0
    invoke-virtual {p1}, Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCError;->getData()Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCErrorData;

    move-result-object p0

    if-eqz p0, :cond_1

    invoke-virtual {p1}, Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCError;->getDataRaw()Ljava/lang/String;

    move-result-object p0

    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p0

    if-nez p0, :cond_1

    .line 86
    invoke-virtual {v7}, Lee/cyber/smartid/tse/dto/BaseError;->createOrGetExtras()Landroid/os/Bundle;

    move-result-object p0

    invoke-virtual {p1}, Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCError;->getDataRaw()Ljava/lang/String;

    move-result-object p1

    const-string v0, "ee.cyber.smartid.EXTRA_RAW_ERROR_DATA"

    invoke-virtual {p0, v0, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    return-object v7
.end method


# virtual methods
.method public getHumanReadableErrorCodeName()Ljava/lang/String;
    .locals 5

    .line 1
    iget-wide v0, p0, Lee/cyber/smartid/tse/dto/BaseError;->code:J

    const-wide/16 v2, 0x3ed

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    const-string v0, "ERROR_CODE_WRONG_TRANSACTION_TYPE"

    return-object v0

    :cond_0
    const-wide/16 v2, 0x3ee

    cmp-long v4, v0, v2

    if-nez v4, :cond_1

    const-string v0, "ERROR_CODE_NO_SUCH_TRANSACTION"

    return-object v0

    :cond_1
    const-wide/16 v2, 0x3f8

    cmp-long v4, v0, v2

    if-nez v4, :cond_2

    const-string v0, "ERROR_CODE_UNSUPPORTED_REGISTRATION_INIT_TYPE"

    return-object v0

    :cond_2
    const-wide/16 v2, 0x3fb

    cmp-long v4, v0, v2

    if-nez v4, :cond_3

    const-string v0, "ERROR_CODE_INSUFFICIENT_AMOUNT_OF_PINS_PROVIDED"

    return-object v0

    :cond_3
    const-wide/16 v2, 0x3fc

    cmp-long v4, v0, v2

    if-nez v4, :cond_4

    const-string v0, "ERROR_CODE_UNSUPPORTED_TOKEN_GENERATION_INIT_TYPE"

    return-object v0

    :cond_4
    const-wide/16 v2, 0x3fd

    cmp-long v4, v0, v2

    if-nez v4, :cond_5

    const-string v0, "ERROR_CODE_UNSUPPORTED_TOKEN_GENERATION_ALGORITHM"

    return-object v0

    :cond_5
    const-wide/16 v2, 0x402

    cmp-long v4, v0, v2

    if-nez v4, :cond_6

    const-string v0, "ERROR_CODE_SAFETY_NET_FAILED"

    return-object v0

    :cond_6
    const-wide/16 v2, -0x7918

    cmp-long v4, v0, v2

    if-nez v4, :cond_7

    const-string v0, "ERROR_CODE_SERVER_REGISTRATION_TOKEN_INVALID"

    return-object v0

    :cond_7
    const-wide/16 v2, -0x7922

    cmp-long v4, v0, v2

    if-nez v4, :cond_8

    const-string v0, "ERROR_CODE_SERVER_RP_REQUEST_NOT_FOUND"

    return-object v0

    :cond_8
    const-wide/16 v2, -0x792c

    cmp-long v4, v0, v2

    if-nez v4, :cond_9

    const-string v0, "ERROR_CODE_SERVER_CANNOT_ISSUE_REGISTRATION_TOKEN"

    return-object v0

    :cond_9
    const-wide/16 v2, 0x40c

    cmp-long v4, v0, v2

    if-nez v4, :cond_a

    const-string v0, "ERROR_SERVER_RESPONSE_VALIDATION_FAILED"

    return-object v0

    :cond_a
    const-wide/16 v2, 0x40d

    cmp-long v4, v0, v2

    if-nez v4, :cond_b

    const-string v0, "ERROR_SERVER_UNSUPPORTED_RESPONSE_ENCODING"

    return-object v0

    :cond_b
    const-wide/16 v2, -0x7930

    cmp-long v4, v0, v2

    if-nez v4, :cond_c

    const-string v0, "ERROR_CODE_SERVER_KEY_MATERIAL_UNSUITABLE"

    return-object v0

    :cond_c
    const-wide/16 v2, 0x414

    cmp-long v4, v0, v2

    if-nez v4, :cond_d

    const-string v0, "ERROR_CODE_INTERACTIVE_UPGRADE_PROCESS_FAILED"

    return-object v0

    :cond_d
    const-wide/16 v2, 0x415

    cmp-long v4, v0, v2

    if-nez v4, :cond_e

    const-string v0, "ERROR_CODE_INTERACTIVE_UPGRADE_IN_PROGRESS"

    return-object v0

    :cond_e
    const-wide/16 v2, 0x416

    cmp-long v4, v0, v2

    if-nez v4, :cond_f

    const-string v0, "ERROR_CODE_INTERACTIVE_UPGRADE_NOT_FOUND"

    return-object v0

    :cond_f
    const-wide/16 v2, -0x7935

    cmp-long v4, v0, v2

    if-nez v4, :cond_10

    const-string v0, "ERROR_CODE_SERVER_CLIENT_NOT_SUPPORTED"

    return-object v0

    :cond_10
    const-wide/32 v2, 0xc351

    cmp-long v4, v0, v2

    if-nez v4, :cond_11

    const-string v0, "ERROR_CODE_VERIFICATION_CODE_GENERATION_FAILED"

    return-object v0

    :cond_11
    const-wide/32 v2, 0xc352

    cmp-long v4, v0, v2

    if-nez v4, :cond_12

    const-string v0, "ERROR_CODE_WRONG_VERIFICATION_CODE"

    return-object v0

    :cond_12
    const-wide/32 v2, 0xc353

    cmp-long v4, v0, v2

    if-nez v4, :cond_13

    const-string v0, "ERROR_CODE_ADDITIONAL_VERIFICATION_CODE_GENERATION_FAILED"

    return-object v0

    .line 2
    :cond_13
    invoke-super {p0}, Lee/cyber/smartid/tse/dto/BaseError;->getHumanReadableErrorCodeName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getKeyStatus()Lee/cyber/smartid/tse/dto/jsonrpc/AccountKeyStatus;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    const/4 v0, 0x0

    return-object v0
.end method

.method public getSafetyNetResp()Lee/cyber/smartid/dto/jsonrpc/resp/SafetyNetAttestationSummary;
    .locals 2

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/tse/dto/BaseError;->extras:Landroid/os/Bundle;

    if-eqz v0, :cond_1

    const-string v1, "ee.cyber.smartid.EXTRA_SAFETY_NET_RESP"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    .line 2
    :cond_0
    iget-object v0, p0, Lee/cyber/smartid/tse/dto/BaseError;->extras:Landroid/os/Bundle;

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lee/cyber/smartid/dto/jsonrpc/resp/SafetyNetAttestationSummary;

    return-object v0

    :cond_1
    :goto_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public getServiceAccountKeyStatus()Lee/cyber/smartid/dto/jsonrpc/ServiceAccountKeyStatus;
    .locals 2

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/tse/dto/BaseError;->extras:Landroid/os/Bundle;

    if-eqz v0, :cond_1

    const-string v1, "ee.cyber.smartid.EXTRA_SERVICE_ACCOUNT_KEY_STATUS_INFO"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    .line 2
    :cond_0
    iget-object v0, p0, Lee/cyber/smartid/tse/dto/BaseError;->extras:Landroid/os/Bundle;

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lee/cyber/smartid/dto/jsonrpc/ServiceAccountKeyStatus;

    return-object v0

    :cond_1
    :goto_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "SmartIdError{} "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-super {p0}, Lee/cyber/smartid/tse/dto/BaseError;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
