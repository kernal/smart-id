.class Lee/cyber/smartid/dto/AccountState$1;
.super Ljava/lang/Object;
.source "AccountState.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lee/cyber/smartid/dto/AccountState;->notifyAndClearSubmitClientSecondPartListeners(Landroid/os/Handler;Lee/cyber/smartid/inter/ListenerAccess;Lee/cyber/smartid/dto/jsonrpc/resp/AddAccountResp;Lee/cyber/smartid/dto/SmartIdError;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lee/cyber/smartid/dto/AccountState;

.field final synthetic val$error:Lee/cyber/smartid/dto/SmartIdError;

.field final synthetic val$listener:Lee/cyber/smartid/inter/AddAccountListener;

.field final synthetic val$resp:Lee/cyber/smartid/dto/jsonrpc/resp/AddAccountResp;

.field final synthetic val$tag:Ljava/lang/String;


# direct methods
.method constructor <init>(Lee/cyber/smartid/dto/AccountState;Lee/cyber/smartid/dto/SmartIdError;Lee/cyber/smartid/inter/AddAccountListener;Ljava/lang/String;Lee/cyber/smartid/dto/jsonrpc/resp/AddAccountResp;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lee/cyber/smartid/dto/AccountState$1;->this$0:Lee/cyber/smartid/dto/AccountState;

    iput-object p2, p0, Lee/cyber/smartid/dto/AccountState$1;->val$error:Lee/cyber/smartid/dto/SmartIdError;

    iput-object p3, p0, Lee/cyber/smartid/dto/AccountState$1;->val$listener:Lee/cyber/smartid/inter/AddAccountListener;

    iput-object p4, p0, Lee/cyber/smartid/dto/AccountState$1;->val$tag:Ljava/lang/String;

    iput-object p5, p0, Lee/cyber/smartid/dto/AccountState$1;->val$resp:Lee/cyber/smartid/dto/jsonrpc/resp/AddAccountResp;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .line 1
    :try_start_0
    iget-object v0, p0, Lee/cyber/smartid/dto/AccountState$1;->val$error:Lee/cyber/smartid/dto/SmartIdError;

    if-eqz v0, :cond_0

    .line 2
    iget-object v0, p0, Lee/cyber/smartid/dto/AccountState$1;->val$listener:Lee/cyber/smartid/inter/AddAccountListener;

    iget-object v1, p0, Lee/cyber/smartid/dto/AccountState$1;->val$tag:Ljava/lang/String;

    iget-object v2, p0, Lee/cyber/smartid/dto/AccountState$1;->val$error:Lee/cyber/smartid/dto/SmartIdError;

    invoke-interface {v0, v1, v2}, Lee/cyber/smartid/inter/AddAccountListener;->onAddAccountFailed(Ljava/lang/String;Lee/cyber/smartid/dto/SmartIdError;)V

    goto :goto_0

    .line 3
    :cond_0
    new-instance v0, Lee/cyber/smartid/dto/jsonrpc/resp/AddAccountResp;

    iget-object v1, p0, Lee/cyber/smartid/dto/AccountState$1;->val$tag:Ljava/lang/String;

    iget-object v2, p0, Lee/cyber/smartid/dto/AccountState$1;->val$resp:Lee/cyber/smartid/dto/jsonrpc/resp/AddAccountResp;

    invoke-direct {v0, v1, v2}, Lee/cyber/smartid/dto/jsonrpc/resp/AddAccountResp;-><init>(Ljava/lang/String;Lee/cyber/smartid/dto/jsonrpc/resp/AddAccountResp;)V

    .line 4
    iget-object v1, p0, Lee/cyber/smartid/dto/AccountState$1;->val$listener:Lee/cyber/smartid/inter/AddAccountListener;

    iget-object v2, p0, Lee/cyber/smartid/dto/AccountState$1;->val$tag:Ljava/lang/String;

    invoke-interface {v1, v2, v0}, Lee/cyber/smartid/inter/AddAccountListener;->onAddAccountSuccess(Ljava/lang/String;Lee/cyber/smartid/dto/jsonrpc/resp/AddAccountResp;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 5
    invoke-static {p0}, Lee/cyber/smartid/util/Log;->getInstance(Ljava/lang/Object;)Lee/cyber/smartid/util/Log;

    move-result-object v1

    const-string v2, "notifyAndClearSubmitClientSecondPartListeners"

    invoke-virtual {v1, v2, v0}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;Ljava/lang/Throwable;)V

    :goto_0
    return-void
.end method
