.class public Lee/cyber/smartid/dto/SafetyNetAttestation;
.super Ljava/lang/Object;
.source "SafetyNetAttestation.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = -0xb657ca719f1e70eL


# instance fields
.field private final payloadJWS:Ljava/lang/String;

.field private final payloadParsed:Lee/cyber/smartid/dto/SafetyNetPayload;

.field private final payloadSummary:Lee/cyber/smartid/dto/jsonrpc/resp/SafetyNetAttestationSummary;


# direct methods
.method private constructor <init>(Lee/cyber/smartid/dto/jsonrpc/resp/SafetyNetAttestationSummary;Ljava/lang/String;Lee/cyber/smartid/dto/SafetyNetPayload;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Lee/cyber/smartid/dto/SafetyNetAttestation;->payloadSummary:Lee/cyber/smartid/dto/jsonrpc/resp/SafetyNetAttestationSummary;

    .line 3
    iput-object p2, p0, Lee/cyber/smartid/dto/SafetyNetAttestation;->payloadJWS:Ljava/lang/String;

    .line 4
    iput-object p3, p0, Lee/cyber/smartid/dto/SafetyNetAttestation;->payloadParsed:Lee/cyber/smartid/dto/SafetyNetPayload;

    return-void
.end method

.method public static create(Lee/cyber/smartid/dto/jsonrpc/resp/SafetyNetAttestationSummary;Ljava/lang/String;Lee/cyber/smartid/dto/SafetyNetPayload;)Lee/cyber/smartid/dto/SafetyNetAttestation;
    .locals 1

    .line 1
    new-instance v0, Lee/cyber/smartid/dto/SafetyNetAttestation;

    invoke-direct {v0, p0, p1, p2}, Lee/cyber/smartid/dto/SafetyNetAttestation;-><init>(Lee/cyber/smartid/dto/jsonrpc/resp/SafetyNetAttestationSummary;Ljava/lang/String;Lee/cyber/smartid/dto/SafetyNetPayload;)V

    return-object v0
.end method


# virtual methods
.method public getPayloadJWS()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/dto/SafetyNetAttestation;->payloadJWS:Ljava/lang/String;

    return-object v0
.end method

.method public getPayloadParsed()Lee/cyber/smartid/dto/SafetyNetPayload;
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/dto/SafetyNetAttestation;->payloadParsed:Lee/cyber/smartid/dto/SafetyNetPayload;

    return-object v0
.end method

.method public getPayloadSummary()Lee/cyber/smartid/dto/jsonrpc/resp/SafetyNetAttestationSummary;
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/dto/SafetyNetAttestation;->payloadSummary:Lee/cyber/smartid/dto/jsonrpc/resp/SafetyNetAttestationSummary;

    return-object v0
.end method

.method public getResultTypeString(Landroid/content/Context;)Ljava/lang/String;
    .locals 5

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/dto/SafetyNetAttestation;->payloadSummary:Lee/cyber/smartid/dto/jsonrpc/resp/SafetyNetAttestationSummary;

    if-nez v0, :cond_0

    .line 2
    sget v0, Lee/cyber/smartid/R$string;->text_na:I

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 3
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 4
    iget-object v1, p0, Lee/cyber/smartid/dto/SafetyNetAttestation;->payloadSummary:Lee/cyber/smartid/dto/jsonrpc/resp/SafetyNetAttestationSummary;

    invoke-virtual {v1}, Lee/cyber/smartid/dto/jsonrpc/resp/SafetyNetAttestationSummary;->isSuccess()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 5
    sget v1, Lee/cyber/smartid/R$string;->text_safetynet_validation_succesful:I

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 6
    :cond_1
    iget-object v1, p0, Lee/cyber/smartid/dto/SafetyNetAttestation;->payloadSummary:Lee/cyber/smartid/dto/jsonrpc/resp/SafetyNetAttestationSummary;

    invoke-virtual {v1}, Lee/cyber/smartid/dto/jsonrpc/resp/SafetyNetAttestationSummary;->isAttestationDataPresent()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 7
    sget v1, Lee/cyber/smartid/R$string;->text_safetynet_attestation_results_local_x_basic_y_cts_x:I

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, Lee/cyber/smartid/dto/SafetyNetAttestation;->payloadSummary:Lee/cyber/smartid/dto/jsonrpc/resp/SafetyNetAttestationSummary;

    invoke-virtual {v3}, Lee/cyber/smartid/dto/jsonrpc/resp/SafetyNetAttestationSummary;->isLocalValidationSuccess()Z

    move-result v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    aput-object v3, v2, v4

    iget-object v3, p0, Lee/cyber/smartid/dto/SafetyNetAttestation;->payloadSummary:Lee/cyber/smartid/dto/jsonrpc/resp/SafetyNetAttestationSummary;

    invoke-virtual {v3}, Lee/cyber/smartid/dto/jsonrpc/resp/SafetyNetAttestationSummary;->isBasicIntegrityVerified()Z

    move-result v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    aput-object v3, v2, v4

    iget-object v3, p0, Lee/cyber/smartid/dto/SafetyNetAttestation;->payloadSummary:Lee/cyber/smartid/dto/jsonrpc/resp/SafetyNetAttestationSummary;

    invoke-virtual {v3}, Lee/cyber/smartid/dto/jsonrpc/resp/SafetyNetAttestationSummary;->isCtsProfileMatch()Z

    move-result v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x2

    aput-object v3, v2, v4

    invoke-virtual {p1, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 8
    :cond_2
    sget v1, Lee/cyber/smartid/R$string;->text_safetynet_attestation_get_data_failed:I

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 9
    :goto_0
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public isSuccess()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/dto/SafetyNetAttestation;->payloadSummary:Lee/cyber/smartid/dto/jsonrpc/resp/SafetyNetAttestationSummary;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lee/cyber/smartid/dto/jsonrpc/resp/SafetyNetAttestationSummary;->isSuccess()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "SafetyNetAttestation{payloadSummary="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lee/cyber/smartid/dto/SafetyNetAttestation;->payloadSummary:Lee/cyber/smartid/dto/jsonrpc/resp/SafetyNetAttestationSummary;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", payloadJWS=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lee/cyber/smartid/dto/SafetyNetAttestation;->payloadJWS:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v1, ", payloadParsed="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lee/cyber/smartid/dto/SafetyNetAttestation;->payloadParsed:Lee/cyber/smartid/dto/SafetyNetPayload;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
