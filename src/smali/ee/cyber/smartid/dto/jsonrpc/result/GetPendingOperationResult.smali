.class public Lee/cyber/smartid/dto/jsonrpc/result/GetPendingOperationResult;
.super Lee/cyber/smartid/dto/jsonrpc/result/TransactionContainerResult;
.source "GetPendingOperationResult.java"

# interfaces
.implements Lee/cyber/smartid/tse/inter/Validatable;


# static fields
.field private static final serialVersionUID:J = 0x3a736ec1c6722dcL


# instance fields
.field private rpRequest:Lee/cyber/smartid/dto/jsonrpc/RPRequest;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Lee/cyber/smartid/dto/jsonrpc/result/TransactionContainerResult;-><init>()V

    return-void
.end method


# virtual methods
.method public getRpRequest()Lee/cyber/smartid/dto/jsonrpc/RPRequest;
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/dto/jsonrpc/result/GetPendingOperationResult;->rpRequest:Lee/cyber/smartid/dto/jsonrpc/RPRequest;

    return-object v0
.end method

.method public isValid()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/dto/jsonrpc/result/TransactionContainerResult;->freshnessToken:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lee/cyber/smartid/dto/jsonrpc/result/TransactionContainerResult;->transaction:Lee/cyber/smartid/dto/jsonrpc/Transaction;

    if-nez v0, :cond_1

    :cond_0
    iget-object v0, p0, Lee/cyber/smartid/dto/jsonrpc/result/GetPendingOperationResult;->rpRequest:Lee/cyber/smartid/dto/jsonrpc/RPRequest;

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public setFreshnessToken(Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lee/cyber/smartid/dto/jsonrpc/result/TransactionContainerResult;->freshnessToken:Ljava/lang/String;

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "GetPendingOperationResult{rpRequest="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lee/cyber/smartid/dto/jsonrpc/result/GetPendingOperationResult;->rpRequest:Lee/cyber/smartid/dto/jsonrpc/RPRequest;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", transaction="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lee/cyber/smartid/dto/jsonrpc/result/TransactionContainerResult;->transaction:Lee/cyber/smartid/dto/jsonrpc/Transaction;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", freshnessToken=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lee/cyber/smartid/dto/jsonrpc/result/TransactionContainerResult;->freshnessToken:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
