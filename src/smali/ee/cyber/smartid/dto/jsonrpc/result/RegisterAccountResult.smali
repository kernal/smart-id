.class public Lee/cyber/smartid/dto/jsonrpc/result/RegisterAccountResult;
.super Ljava/lang/Object;
.source "RegisterAccountResult.java"

# interfaces
.implements Lee/cyber/smartid/tse/inter/Validatable;
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = -0x1a04088252451574L


# instance fields
.field protected accountUUID:Ljava/lang/String;

.field protected authKey:Lee/cyber/smartid/tse/dto/jsonrpc/KeyData;

.field protected identityData:Lee/cyber/smartid/dto/jsonrpc/IdentityData;

.field protected signKey:Lee/cyber/smartid/tse/dto/jsonrpc/KeyData;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getAccountUUID()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/dto/jsonrpc/result/RegisterAccountResult;->accountUUID:Ljava/lang/String;

    return-object v0
.end method

.method public getAuthData()Lee/cyber/smartid/tse/dto/jsonrpc/KeyData;
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/dto/jsonrpc/result/RegisterAccountResult;->authKey:Lee/cyber/smartid/tse/dto/jsonrpc/KeyData;

    return-object v0
.end method

.method public getIdentityData()Lee/cyber/smartid/dto/jsonrpc/IdentityData;
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/dto/jsonrpc/result/RegisterAccountResult;->identityData:Lee/cyber/smartid/dto/jsonrpc/IdentityData;

    return-object v0
.end method

.method public getSignData()Lee/cyber/smartid/tse/dto/jsonrpc/KeyData;
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/dto/jsonrpc/result/RegisterAccountResult;->signKey:Lee/cyber/smartid/tse/dto/jsonrpc/KeyData;

    return-object v0
.end method

.method public isValid()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/dto/jsonrpc/result/RegisterAccountResult;->accountUUID:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lee/cyber/smartid/dto/jsonrpc/result/RegisterAccountResult;->identityData:Lee/cyber/smartid/dto/jsonrpc/IdentityData;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lee/cyber/smartid/dto/jsonrpc/IdentityData;->isValid()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lee/cyber/smartid/dto/jsonrpc/result/RegisterAccountResult;->authKey:Lee/cyber/smartid/tse/dto/jsonrpc/KeyData;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lee/cyber/smartid/dto/jsonrpc/result/RegisterAccountResult;->signKey:Lee/cyber/smartid/tse/dto/jsonrpc/KeyData;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "RegisterAccountResult{identityData=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lee/cyber/smartid/dto/jsonrpc/result/RegisterAccountResult;->identityData:Lee/cyber/smartid/dto/jsonrpc/IdentityData;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v2, ", accountUUID=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lee/cyber/smartid/dto/jsonrpc/result/RegisterAccountResult;->accountUUID:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v1, ", authKey="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lee/cyber/smartid/dto/jsonrpc/result/RegisterAccountResult;->authKey:Lee/cyber/smartid/tse/dto/jsonrpc/KeyData;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", signKey="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lee/cyber/smartid/dto/jsonrpc/result/RegisterAccountResult;->signKey:Lee/cyber/smartid/tse/dto/jsonrpc/KeyData;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
