.class public Lee/cyber/smartid/dto/jsonrpc/result/ConfirmRPRequestResult;
.super Ljava/lang/Object;
.source "ConfirmRPRequestResult.java"

# interfaces
.implements Lee/cyber/smartid/tse/inter/Validatable;
.implements Ljava/io/Serializable;


# static fields
.field public static final MESSAGE_OK:Ljava/lang/String; = "OK"

.field private static final serialVersionUID:J = 0x4d5c458458a44e94L


# instance fields
.field private message:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    .line 1
    invoke-direct {p0, v0}, Lee/cyber/smartid/dto/jsonrpc/result/ConfirmRPRequestResult;-><init>(Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 0

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3
    iput-object p1, p0, Lee/cyber/smartid/dto/jsonrpc/result/ConfirmRPRequestResult;->message:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public isValid()Z
    .locals 2

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/dto/jsonrpc/result/ConfirmRPRequestResult;->message:Ljava/lang/String;

    const-string v1, "OK"

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "CancelTransactionResult{message=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lee/cyber/smartid/dto/jsonrpc/result/ConfirmRPRequestResult;->message:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
