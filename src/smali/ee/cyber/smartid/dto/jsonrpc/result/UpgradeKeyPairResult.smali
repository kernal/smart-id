.class public Lee/cyber/smartid/dto/jsonrpc/result/UpgradeKeyPairResult;
.super Ljava/lang/Object;
.source "UpgradeKeyPairResult.java"

# interfaces
.implements Lee/cyber/smartid/tse/inter/Validatable;
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = -0x31e40ceac74e7fd5L


# instance fields
.field private keyData:Lee/cyber/smartid/tse/dto/jsonrpc/KeyData;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getKeyData()Lee/cyber/smartid/tse/dto/jsonrpc/KeyData;
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/dto/jsonrpc/result/UpgradeKeyPairResult;->keyData:Lee/cyber/smartid/tse/dto/jsonrpc/KeyData;

    return-object v0
.end method

.method public isValid()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/dto/jsonrpc/result/UpgradeKeyPairResult;->keyData:Lee/cyber/smartid/tse/dto/jsonrpc/KeyData;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method
