.class public Lee/cyber/smartid/dto/jsonrpc/result/GetAccountStatusResult;
.super Ljava/lang/Object;
.source "GetAccountStatusResult.java"

# interfaces
.implements Lee/cyber/smartid/tse/inter/Validatable;
.implements Ljava/io/Serializable;


# static fields
.field public static final ACCOUNT_STATUS_DISABLED:Ljava/lang/String; = "DISABLED"

.field public static final ACCOUNT_STATUS_ENABLED:Ljava/lang/String; = "ENABLED"

.field private static final serialVersionUID:J = -0x390fd830a2574df2L


# instance fields
.field private accountUUID:Ljava/lang/String;

.field private keys:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lee/cyber/smartid/dto/jsonrpc/ServiceAccountKeyStatus;",
            ">;"
        }
    .end annotation
.end field

.field private numberOfAccounts:J

.field private registration:Lee/cyber/smartid/dto/jsonrpc/GetAccountStatusRegistration;

.field private status:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getAccountUUID()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/dto/jsonrpc/result/GetAccountStatusResult;->accountUUID:Ljava/lang/String;

    return-object v0
.end method

.method public getKeys()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Lee/cyber/smartid/dto/jsonrpc/ServiceAccountKeyStatus;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/dto/jsonrpc/result/GetAccountStatusResult;->keys:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getNumberOfAccounts()J
    .locals 2

    .line 1
    iget-wide v0, p0, Lee/cyber/smartid/dto/jsonrpc/result/GetAccountStatusResult;->numberOfAccounts:J

    return-wide v0
.end method

.method public getRegistration()Lee/cyber/smartid/dto/jsonrpc/GetAccountStatusRegistration;
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/dto/jsonrpc/result/GetAccountStatusResult;->registration:Lee/cyber/smartid/dto/jsonrpc/GetAccountStatusRegistration;

    return-object v0
.end method

.method public getStatus()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/dto/jsonrpc/result/GetAccountStatusResult;->status:Ljava/lang/String;

    return-object v0
.end method

.method public isValid()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/dto/jsonrpc/result/GetAccountStatusResult;->accountUUID:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lee/cyber/smartid/dto/jsonrpc/result/GetAccountStatusResult;->status:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "GetAccountStatusResult{accountUUID=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lee/cyber/smartid/dto/jsonrpc/result/GetAccountStatusResult;->accountUUID:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v2, ", numberOfAccounts="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v2, p0, Lee/cyber/smartid/dto/jsonrpc/result/GetAccountStatusResult;->numberOfAccounts:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v2, ", status=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lee/cyber/smartid/dto/jsonrpc/result/GetAccountStatusResult;->status:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v1, ", registration="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lee/cyber/smartid/dto/jsonrpc/result/GetAccountStatusResult;->registration:Lee/cyber/smartid/dto/jsonrpc/GetAccountStatusRegistration;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", keys="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lee/cyber/smartid/dto/jsonrpc/result/GetAccountStatusResult;->keys:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
