.class public Lee/cyber/smartid/dto/jsonrpc/result/GetRPRequestResult;
.super Ljava/lang/Object;
.source "GetRPRequestResult.java"

# interfaces
.implements Lee/cyber/smartid/tse/inter/Validatable;
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = 0xcd6d5b953681cc2L


# instance fields
.field private rpRequest:Lee/cyber/smartid/dto/jsonrpc/RPRequest;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getRpRequest()Lee/cyber/smartid/dto/jsonrpc/RPRequest;
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/dto/jsonrpc/result/GetRPRequestResult;->rpRequest:Lee/cyber/smartid/dto/jsonrpc/RPRequest;

    return-object v0
.end method

.method public isValid()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/dto/jsonrpc/result/GetRPRequestResult;->rpRequest:Lee/cyber/smartid/dto/jsonrpc/RPRequest;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lee/cyber/smartid/dto/jsonrpc/RPRequest;->isValid()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public setRpRequest(Lee/cyber/smartid/dto/jsonrpc/RPRequest;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lee/cyber/smartid/dto/jsonrpc/result/GetRPRequestResult;->rpRequest:Lee/cyber/smartid/dto/jsonrpc/RPRequest;

    return-void
.end method
