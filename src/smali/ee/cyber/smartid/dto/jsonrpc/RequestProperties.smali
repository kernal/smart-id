.class public Lee/cyber/smartid/dto/jsonrpc/RequestProperties;
.super Ljava/lang/Object;
.source "RequestProperties.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = -0x42e3ea28ed59e8b7L


# instance fields
.field private vcChoice:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p0, p1, :cond_0

    return v0

    :cond_0
    const/4 v1, 0x0

    if-eqz p1, :cond_3

    .line 1
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    const-class v3, Lee/cyber/smartid/dto/jsonrpc/RequestProperties;

    if-eq v3, v2, :cond_1

    goto :goto_1

    .line 2
    :cond_1
    check-cast p1, Lee/cyber/smartid/dto/jsonrpc/RequestProperties;

    .line 3
    iget-boolean v2, p0, Lee/cyber/smartid/dto/jsonrpc/RequestProperties;->vcChoice:Z

    iget-boolean p1, p1, Lee/cyber/smartid/dto/jsonrpc/RequestProperties;->vcChoice:Z

    if-ne v2, p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_3
    :goto_1
    return v1
.end method

.method public hashCode()I
    .locals 1

    .line 1
    iget-boolean v0, p0, Lee/cyber/smartid/dto/jsonrpc/RequestProperties;->vcChoice:Z

    return v0
.end method

.method public isVcChoice()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lee/cyber/smartid/dto/jsonrpc/RequestProperties;->vcChoice:Z

    return v0
.end method

.method public setVcChoice(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lee/cyber/smartid/dto/jsonrpc/RequestProperties;->vcChoice:Z

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "RequestProperties{vcChoice="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lee/cyber/smartid/dto/jsonrpc/RequestProperties;->vcChoice:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
