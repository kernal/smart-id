.class public Lee/cyber/smartid/dto/jsonrpc/DeviceProperties;
.super Ljava/lang/Object;
.source "DeviceProperties.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = -0x5fa2350c10cf773fL


# instance fields
.field private apiVersion:Ljava/lang/String;

.field private capabilityList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private deviceCodeName:Ljava/lang/String;

.field private deviceFamily:Ljava/lang/String;

.field private deviceFingerprint:Ljava/lang/String;

.field private friendlyName:Ljava/lang/String;

.field private manufacturer:Ljava/lang/String;

.field private modelName:Ljava/lang/String;

.field private platformVersion:Ljava/lang/String;

.field private prngTestResult:[Lee/cyber/smartid/cryptolib/dto/TestResult;

.field private productCodeName:Ljava/lang/String;

.field private safetyNetAttestationResult:Ljava/lang/String;

.field private screenDensity:Ljava/lang/String;

.field private screenHeightPx:Ljava/lang/String;

.field private screenWidthPx:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public constructor <init>(Lee/cyber/smartid/dto/jsonrpc/DeviceProperties;)V
    .locals 2

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3
    iget-object v0, p1, Lee/cyber/smartid/dto/jsonrpc/DeviceProperties;->platformVersion:Ljava/lang/String;

    iput-object v0, p0, Lee/cyber/smartid/dto/jsonrpc/DeviceProperties;->platformVersion:Ljava/lang/String;

    .line 4
    iget-object v0, p1, Lee/cyber/smartid/dto/jsonrpc/DeviceProperties;->deviceFingerprint:Ljava/lang/String;

    iput-object v0, p0, Lee/cyber/smartid/dto/jsonrpc/DeviceProperties;->deviceFingerprint:Ljava/lang/String;

    .line 5
    iget-object v0, p1, Lee/cyber/smartid/dto/jsonrpc/DeviceProperties;->apiVersion:Ljava/lang/String;

    iput-object v0, p0, Lee/cyber/smartid/dto/jsonrpc/DeviceProperties;->apiVersion:Ljava/lang/String;

    .line 6
    iget-object v0, p1, Lee/cyber/smartid/dto/jsonrpc/DeviceProperties;->modelName:Ljava/lang/String;

    iput-object v0, p0, Lee/cyber/smartid/dto/jsonrpc/DeviceProperties;->modelName:Ljava/lang/String;

    .line 7
    iget-object v0, p1, Lee/cyber/smartid/dto/jsonrpc/DeviceProperties;->deviceFamily:Ljava/lang/String;

    iput-object v0, p0, Lee/cyber/smartid/dto/jsonrpc/DeviceProperties;->deviceFamily:Ljava/lang/String;

    .line 8
    iget-object v0, p1, Lee/cyber/smartid/dto/jsonrpc/DeviceProperties;->friendlyName:Ljava/lang/String;

    iput-object v0, p0, Lee/cyber/smartid/dto/jsonrpc/DeviceProperties;->friendlyName:Ljava/lang/String;

    .line 9
    iget-object v0, p1, Lee/cyber/smartid/dto/jsonrpc/DeviceProperties;->manufacturer:Ljava/lang/String;

    iput-object v0, p0, Lee/cyber/smartid/dto/jsonrpc/DeviceProperties;->manufacturer:Ljava/lang/String;

    .line 10
    iget-object v0, p1, Lee/cyber/smartid/dto/jsonrpc/DeviceProperties;->deviceCodeName:Ljava/lang/String;

    iput-object v0, p0, Lee/cyber/smartid/dto/jsonrpc/DeviceProperties;->deviceCodeName:Ljava/lang/String;

    .line 11
    iget-object v0, p1, Lee/cyber/smartid/dto/jsonrpc/DeviceProperties;->productCodeName:Ljava/lang/String;

    iput-object v0, p0, Lee/cyber/smartid/dto/jsonrpc/DeviceProperties;->productCodeName:Ljava/lang/String;

    .line 12
    iget-object v0, p1, Lee/cyber/smartid/dto/jsonrpc/DeviceProperties;->screenHeightPx:Ljava/lang/String;

    iput-object v0, p0, Lee/cyber/smartid/dto/jsonrpc/DeviceProperties;->screenHeightPx:Ljava/lang/String;

    .line 13
    iget-object v0, p1, Lee/cyber/smartid/dto/jsonrpc/DeviceProperties;->screenWidthPx:Ljava/lang/String;

    iput-object v0, p0, Lee/cyber/smartid/dto/jsonrpc/DeviceProperties;->screenWidthPx:Ljava/lang/String;

    .line 14
    iget-object v0, p1, Lee/cyber/smartid/dto/jsonrpc/DeviceProperties;->screenDensity:Ljava/lang/String;

    iput-object v0, p0, Lee/cyber/smartid/dto/jsonrpc/DeviceProperties;->screenDensity:Ljava/lang/String;

    .line 15
    iget-object v0, p1, Lee/cyber/smartid/dto/jsonrpc/DeviceProperties;->capabilityList:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 16
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lee/cyber/smartid/dto/jsonrpc/DeviceProperties;->capabilityList:Ljava/util/ArrayList;

    .line 17
    iget-object v0, p0, Lee/cyber/smartid/dto/jsonrpc/DeviceProperties;->capabilityList:Ljava/util/ArrayList;

    iget-object v1, p1, Lee/cyber/smartid/dto/jsonrpc/DeviceProperties;->capabilityList:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 18
    iput-object v0, p0, Lee/cyber/smartid/dto/jsonrpc/DeviceProperties;->capabilityList:Ljava/util/ArrayList;

    .line 19
    :goto_0
    iget-object v0, p1, Lee/cyber/smartid/dto/jsonrpc/DeviceProperties;->safetyNetAttestationResult:Ljava/lang/String;

    iput-object v0, p0, Lee/cyber/smartid/dto/jsonrpc/DeviceProperties;->safetyNetAttestationResult:Ljava/lang/String;

    .line 20
    iget-object p1, p1, Lee/cyber/smartid/dto/jsonrpc/DeviceProperties;->prngTestResult:[Lee/cyber/smartid/cryptolib/dto/TestResult;

    iput-object p1, p0, Lee/cyber/smartid/dto/jsonrpc/DeviceProperties;->prngTestResult:[Lee/cyber/smartid/cryptolib/dto/TestResult;

    return-void
.end method


# virtual methods
.method public getPRNGTestResult()[Lee/cyber/smartid/cryptolib/dto/TestResult;
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/dto/jsonrpc/DeviceProperties;->prngTestResult:[Lee/cyber/smartid/cryptolib/dto/TestResult;

    return-object v0
.end method

.method public getSafetyNetAttestationResult()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/dto/jsonrpc/DeviceProperties;->safetyNetAttestationResult:Ljava/lang/String;

    return-object v0
.end method

.method public setApiVersion(Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lee/cyber/smartid/dto/jsonrpc/DeviceProperties;->apiVersion:Ljava/lang/String;

    return-void
.end method

.method public setCapabilityList(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 1
    iput-object p1, p0, Lee/cyber/smartid/dto/jsonrpc/DeviceProperties;->capabilityList:Ljava/util/ArrayList;

    return-void
.end method

.method public setDeviceCodeName(Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lee/cyber/smartid/dto/jsonrpc/DeviceProperties;->deviceCodeName:Ljava/lang/String;

    return-void
.end method

.method public setDeviceFingerprint(Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lee/cyber/smartid/dto/jsonrpc/DeviceProperties;->deviceFingerprint:Ljava/lang/String;

    return-void
.end method

.method public setManufacturer(Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lee/cyber/smartid/dto/jsonrpc/DeviceProperties;->manufacturer:Ljava/lang/String;

    return-void
.end method

.method public setModelName(Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lee/cyber/smartid/dto/jsonrpc/DeviceProperties;->modelName:Ljava/lang/String;

    return-void
.end method

.method public setPRNGTestResult([Lee/cyber/smartid/cryptolib/dto/TestResult;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lee/cyber/smartid/dto/jsonrpc/DeviceProperties;->prngTestResult:[Lee/cyber/smartid/cryptolib/dto/TestResult;

    return-void
.end method

.method public setPlatformVersion(Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lee/cyber/smartid/dto/jsonrpc/DeviceProperties;->platformVersion:Ljava/lang/String;

    return-void
.end method

.method public setProductCodeName(Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lee/cyber/smartid/dto/jsonrpc/DeviceProperties;->productCodeName:Ljava/lang/String;

    return-void
.end method

.method public setSafetyNetAttestationResult(Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lee/cyber/smartid/dto/jsonrpc/DeviceProperties;->safetyNetAttestationResult:Ljava/lang/String;

    return-void
.end method

.method public setScreenDensity(Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lee/cyber/smartid/dto/jsonrpc/DeviceProperties;->screenDensity:Ljava/lang/String;

    return-void
.end method

.method public setScreenHeightPx(Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lee/cyber/smartid/dto/jsonrpc/DeviceProperties;->screenHeightPx:Ljava/lang/String;

    return-void
.end method

.method public setScreenWidthPx(Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lee/cyber/smartid/dto/jsonrpc/DeviceProperties;->screenWidthPx:Ljava/lang/String;

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "DeviceProperties{platformVersion=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lee/cyber/smartid/dto/jsonrpc/DeviceProperties;->platformVersion:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v2, ", deviceFingerprint=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lee/cyber/smartid/dto/jsonrpc/DeviceProperties;->deviceFingerprint:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v2, ", apiVersion=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lee/cyber/smartid/dto/jsonrpc/DeviceProperties;->apiVersion:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v2, ", modelName=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lee/cyber/smartid/dto/jsonrpc/DeviceProperties;->modelName:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v2, ", deviceFamily=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lee/cyber/smartid/dto/jsonrpc/DeviceProperties;->deviceFamily:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v2, ", friendlyName=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lee/cyber/smartid/dto/jsonrpc/DeviceProperties;->friendlyName:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v2, ", manufacturer=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lee/cyber/smartid/dto/jsonrpc/DeviceProperties;->manufacturer:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v2, ", deviceCodeName=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lee/cyber/smartid/dto/jsonrpc/DeviceProperties;->deviceCodeName:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v2, ", productCodeName=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lee/cyber/smartid/dto/jsonrpc/DeviceProperties;->productCodeName:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v2, ", screenHeightPx=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lee/cyber/smartid/dto/jsonrpc/DeviceProperties;->screenHeightPx:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v2, ", screenWidthPx=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lee/cyber/smartid/dto/jsonrpc/DeviceProperties;->screenWidthPx:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v2, ", screenDensity=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lee/cyber/smartid/dto/jsonrpc/DeviceProperties;->screenDensity:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v2, ", capabilityList="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lee/cyber/smartid/dto/jsonrpc/DeviceProperties;->capabilityList:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v2, ", safetyNetAttestationResult=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lee/cyber/smartid/dto/jsonrpc/DeviceProperties;->safetyNetAttestationResult:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v1, ", prngTestResult="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lee/cyber/smartid/dto/jsonrpc/DeviceProperties;->prngTestResult:[Lee/cyber/smartid/cryptolib/dto/TestResult;

    .line 2
    invoke-static {v1}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
