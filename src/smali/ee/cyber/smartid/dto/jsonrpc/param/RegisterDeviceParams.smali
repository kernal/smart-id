.class public Lee/cyber/smartid/dto/jsonrpc/param/RegisterDeviceParams;
.super Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCParams;
.source "RegisterDeviceParams.java"


# static fields
.field private static final serialVersionUID:J = 0xf9c93211b1916ccL


# instance fields
.field private accountRegistrationData:Lee/cyber/smartid/dto/jsonrpc/param/AccountRegistrationDataParams;

.field private deviceData:Lee/cyber/smartid/dto/jsonrpc/param/RegisterDeviceData;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCParams;-><init>()V

    return-void
.end method


# virtual methods
.method public setAccountRegistrationData(Lee/cyber/smartid/dto/jsonrpc/param/AccountRegistrationDataParams;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lee/cyber/smartid/dto/jsonrpc/param/RegisterDeviceParams;->accountRegistrationData:Lee/cyber/smartid/dto/jsonrpc/param/AccountRegistrationDataParams;

    return-void
.end method

.method public setDeviceData(Lee/cyber/smartid/dto/jsonrpc/param/RegisterDeviceData;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lee/cyber/smartid/dto/jsonrpc/param/RegisterDeviceParams;->deviceData:Lee/cyber/smartid/dto/jsonrpc/param/RegisterDeviceData;

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "RegisterDeviceParams{deviceData="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lee/cyber/smartid/dto/jsonrpc/param/RegisterDeviceParams;->deviceData:Lee/cyber/smartid/dto/jsonrpc/param/RegisterDeviceData;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", accountRegistrationData="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lee/cyber/smartid/dto/jsonrpc/param/RegisterDeviceParams;->accountRegistrationData:Lee/cyber/smartid/dto/jsonrpc/param/AccountRegistrationDataParams;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
