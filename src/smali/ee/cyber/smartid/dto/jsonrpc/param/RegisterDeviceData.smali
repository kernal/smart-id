.class public Lee/cyber/smartid/dto/jsonrpc/param/RegisterDeviceData;
.super Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCParams;
.source "RegisterDeviceData.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lee/cyber/smartid/dto/jsonrpc/param/RegisterDeviceData$PushNotificationType;,
        Lee/cyber/smartid/dto/jsonrpc/param/RegisterDeviceData$DevicePlatform;
    }
.end annotation


# static fields
.field public static final PLATFORM_ANDROID:Ljava/lang/String; = "Android"

.field public static final PUSH_NOTIFICATION_TYPE_GCM:Ljava/lang/String; = "GCM"

.field private static final serialVersionUID:J = 0x6df80c91906e0e7aL


# instance fields
.field private appPackageName:Ljava/lang/String;

.field private appVersion:Ljava/lang/String;

.field private platform:Ljava/lang/String;

.field private properties:Lee/cyber/smartid/dto/jsonrpc/DeviceProperties;

.field private pushNotificationToken:Ljava/lang/String;

.field private pushNotificationType:Ljava/lang/String;

.field private tseVersion:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCParams;-><init>()V

    return-void
.end method

.method public constructor <init>(Lee/cyber/smartid/dto/jsonrpc/param/RegisterDeviceData;)V
    .locals 1

    .line 2
    invoke-direct {p0}, Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCParams;-><init>()V

    .line 3
    iget-object v0, p1, Lee/cyber/smartid/dto/jsonrpc/param/RegisterDeviceData;->platform:Ljava/lang/String;

    iput-object v0, p0, Lee/cyber/smartid/dto/jsonrpc/param/RegisterDeviceData;->platform:Ljava/lang/String;

    .line 4
    iget-object v0, p1, Lee/cyber/smartid/dto/jsonrpc/param/RegisterDeviceData;->pushNotificationType:Ljava/lang/String;

    iput-object v0, p0, Lee/cyber/smartid/dto/jsonrpc/param/RegisterDeviceData;->pushNotificationType:Ljava/lang/String;

    .line 5
    iget-object v0, p1, Lee/cyber/smartid/dto/jsonrpc/param/RegisterDeviceData;->appVersion:Ljava/lang/String;

    iput-object v0, p0, Lee/cyber/smartid/dto/jsonrpc/param/RegisterDeviceData;->appVersion:Ljava/lang/String;

    .line 6
    iget-object v0, p1, Lee/cyber/smartid/dto/jsonrpc/param/RegisterDeviceData;->tseVersion:Ljava/lang/String;

    iput-object v0, p0, Lee/cyber/smartid/dto/jsonrpc/param/RegisterDeviceData;->tseVersion:Ljava/lang/String;

    .line 7
    iget-object v0, p1, Lee/cyber/smartid/dto/jsonrpc/param/RegisterDeviceData;->appPackageName:Ljava/lang/String;

    iput-object v0, p0, Lee/cyber/smartid/dto/jsonrpc/param/RegisterDeviceData;->appPackageName:Ljava/lang/String;

    .line 8
    iget-object v0, p1, Lee/cyber/smartid/dto/jsonrpc/param/RegisterDeviceData;->pushNotificationToken:Ljava/lang/String;

    iput-object v0, p0, Lee/cyber/smartid/dto/jsonrpc/param/RegisterDeviceData;->pushNotificationToken:Ljava/lang/String;

    .line 9
    iget-object p1, p1, Lee/cyber/smartid/dto/jsonrpc/param/RegisterDeviceData;->properties:Lee/cyber/smartid/dto/jsonrpc/DeviceProperties;

    if-eqz p1, :cond_0

    new-instance v0, Lee/cyber/smartid/dto/jsonrpc/DeviceProperties;

    invoke-direct {v0, p1}, Lee/cyber/smartid/dto/jsonrpc/DeviceProperties;-><init>(Lee/cyber/smartid/dto/jsonrpc/DeviceProperties;)V

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    iput-object v0, p0, Lee/cyber/smartid/dto/jsonrpc/param/RegisterDeviceData;->properties:Lee/cyber/smartid/dto/jsonrpc/DeviceProperties;

    return-void
.end method


# virtual methods
.method public getProperties()Lee/cyber/smartid/dto/jsonrpc/DeviceProperties;
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/dto/jsonrpc/param/RegisterDeviceData;->properties:Lee/cyber/smartid/dto/jsonrpc/DeviceProperties;

    return-object v0
.end method

.method public getPushNotificationToken()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/dto/jsonrpc/param/RegisterDeviceData;->pushNotificationToken:Ljava/lang/String;

    return-object v0
.end method

.method public setAppPackageName(Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lee/cyber/smartid/dto/jsonrpc/param/RegisterDeviceData;->appPackageName:Ljava/lang/String;

    return-void
.end method

.method public setAppVersion(Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lee/cyber/smartid/dto/jsonrpc/param/RegisterDeviceData;->appVersion:Ljava/lang/String;

    return-void
.end method

.method public setPlatform(Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lee/cyber/smartid/dto/jsonrpc/param/RegisterDeviceData;->platform:Ljava/lang/String;

    return-void
.end method

.method public setProperties(Lee/cyber/smartid/dto/jsonrpc/DeviceProperties;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lee/cyber/smartid/dto/jsonrpc/param/RegisterDeviceData;->properties:Lee/cyber/smartid/dto/jsonrpc/DeviceProperties;

    return-void
.end method

.method public setPushNotificationToken(Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lee/cyber/smartid/dto/jsonrpc/param/RegisterDeviceData;->pushNotificationToken:Ljava/lang/String;

    return-void
.end method

.method public setPushNotificationType(Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lee/cyber/smartid/dto/jsonrpc/param/RegisterDeviceData;->pushNotificationType:Ljava/lang/String;

    return-void
.end method

.method public setTseVersion(Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lee/cyber/smartid/dto/jsonrpc/param/RegisterDeviceData;->tseVersion:Ljava/lang/String;

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "RegisterDeviceData{platform=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lee/cyber/smartid/dto/jsonrpc/param/RegisterDeviceData;->platform:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v2, ", pushNotificationType=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lee/cyber/smartid/dto/jsonrpc/param/RegisterDeviceData;->pushNotificationType:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v2, ", appVersion=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lee/cyber/smartid/dto/jsonrpc/param/RegisterDeviceData;->appVersion:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v2, ", tseVersion=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lee/cyber/smartid/dto/jsonrpc/param/RegisterDeviceData;->tseVersion:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v2, ", appPackageName=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lee/cyber/smartid/dto/jsonrpc/param/RegisterDeviceData;->appPackageName:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v2, ", pushNotificationToken=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lee/cyber/smartid/dto/jsonrpc/param/RegisterDeviceData;->pushNotificationToken:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v1, ", properties="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lee/cyber/smartid/dto/jsonrpc/param/RegisterDeviceData;->properties:Lee/cyber/smartid/dto/jsonrpc/DeviceProperties;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, "} "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2
    invoke-super {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
