.class public Lee/cyber/smartid/dto/jsonrpc/param/GetTransactionParams;
.super Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCParams;
.source "GetTransactionParams.java"


# static fields
.field private static final serialVersionUID:J = -0x76379fb644f8c1aeL


# instance fields
.field private final transactionUUID:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCParams;-><init>()V

    .line 2
    iput-object p1, p0, Lee/cyber/smartid/dto/jsonrpc/param/GetTransactionParams;->transactionUUID:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 2

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "GetTransactionParams{transactionUUID=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lee/cyber/smartid/dto/jsonrpc/param/GetTransactionParams;->transactionUUID:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
