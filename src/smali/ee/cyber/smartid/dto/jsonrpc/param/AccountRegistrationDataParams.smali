.class public Lee/cyber/smartid/dto/jsonrpc/param/AccountRegistrationDataParams;
.super Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCParams;
.source "AccountRegistrationDataParams.java"


# static fields
.field public static final INITIATION_TYPE_IDENTITY_TOKEN:Ljava/lang/String; = "IDENTITYTOKEN"

.field public static final INITIATION_TYPE_KEY_TOKEN:Ljava/lang/String; = "KEYTOKEN"

.field private static final serialVersionUID:J = 0x2b527c5340408fb8L


# instance fields
.field private clientAuthDhPublicKey:Ljava/lang/String;

.field private clientAuthModulus:Ljava/lang/String;

.field private clientSignDhPublicKey:Ljava/lang/String;

.field private clientSignModulus:Ljava/lang/String;

.field private knownServerKeys:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private registrationTokenAlgorithm:Ljava/lang/String;

.field private registrationTokenFromBackend:Ljava/lang/String;

.field private registrationType:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCParams;-><init>()V

    return-void
.end method


# virtual methods
.method public getRegistrationType()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/dto/jsonrpc/param/AccountRegistrationDataParams;->registrationType:Ljava/lang/String;

    return-object v0
.end method

.method public setClientAuthDhPublicKey(Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lee/cyber/smartid/dto/jsonrpc/param/AccountRegistrationDataParams;->clientAuthDhPublicKey:Ljava/lang/String;

    return-void
.end method

.method public setClientAuthModulus(Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lee/cyber/smartid/dto/jsonrpc/param/AccountRegistrationDataParams;->clientAuthModulus:Ljava/lang/String;

    return-void
.end method

.method public setClientSignDhPublicKey(Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lee/cyber/smartid/dto/jsonrpc/param/AccountRegistrationDataParams;->clientSignDhPublicKey:Ljava/lang/String;

    return-void
.end method

.method public setClientSignModulus(Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lee/cyber/smartid/dto/jsonrpc/param/AccountRegistrationDataParams;->clientSignModulus:Ljava/lang/String;

    return-void
.end method

.method public setKnownServerKeys(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 1
    iput-object p1, p0, Lee/cyber/smartid/dto/jsonrpc/param/AccountRegistrationDataParams;->knownServerKeys:Ljava/util/ArrayList;

    return-void
.end method

.method public setRegistrationToken(Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lee/cyber/smartid/dto/jsonrpc/param/AccountRegistrationDataParams;->registrationTokenFromBackend:Ljava/lang/String;

    return-void
.end method

.method public setRegistrationTokenAlgorithm(Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lee/cyber/smartid/dto/jsonrpc/param/AccountRegistrationDataParams;->registrationTokenAlgorithm:Ljava/lang/String;

    return-void
.end method

.method public setRegistrationType(Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lee/cyber/smartid/dto/jsonrpc/param/AccountRegistrationDataParams;->registrationType:Ljava/lang/String;

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "AccountRegistrationDataParams{registrationType=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lee/cyber/smartid/dto/jsonrpc/param/AccountRegistrationDataParams;->registrationType:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v2, ", registrationTokenFromBackend=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lee/cyber/smartid/dto/jsonrpc/param/AccountRegistrationDataParams;->registrationTokenFromBackend:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v2, ", registrationTokenAlgorithm=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lee/cyber/smartid/dto/jsonrpc/param/AccountRegistrationDataParams;->registrationTokenAlgorithm:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v2, ", knownServerKeys="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lee/cyber/smartid/dto/jsonrpc/param/AccountRegistrationDataParams;->knownServerKeys:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v2, ", clientAuthModulus=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lee/cyber/smartid/dto/jsonrpc/param/AccountRegistrationDataParams;->clientAuthModulus:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v2, ", clientAuthDhPublicKey=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lee/cyber/smartid/dto/jsonrpc/param/AccountRegistrationDataParams;->clientAuthDhPublicKey:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v2, ", clientSignModulus=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lee/cyber/smartid/dto/jsonrpc/param/AccountRegistrationDataParams;->clientSignModulus:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v2, ", clientSignDhPublicKey=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lee/cyber/smartid/dto/jsonrpc/param/AccountRegistrationDataParams;->clientSignDhPublicKey:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v1, "} "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2
    invoke-super {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
