.class public Lee/cyber/smartid/dto/jsonrpc/ServiceAccountKeyLockInfo;
.super Ljava/lang/Object;
.source "ServiceAccountKeyLockInfo.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = 0xccd0090d30a1e8L


# instance fields
.field private lockDurationSec:I

.field private nextLockDurationSec:I

.field private pinAttemptsLeft:I

.field private pinAttemptsLeftInTotal:I

.field private wrongAttempts:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public constructor <init>(IIIII)V
    .locals 0

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3
    iput p1, p0, Lee/cyber/smartid/dto/jsonrpc/ServiceAccountKeyLockInfo;->lockDurationSec:I

    .line 4
    iput p2, p0, Lee/cyber/smartid/dto/jsonrpc/ServiceAccountKeyLockInfo;->pinAttemptsLeft:I

    .line 5
    iput p3, p0, Lee/cyber/smartid/dto/jsonrpc/ServiceAccountKeyLockInfo;->pinAttemptsLeftInTotal:I

    .line 6
    iput p4, p0, Lee/cyber/smartid/dto/jsonrpc/ServiceAccountKeyLockInfo;->nextLockDurationSec:I

    .line 7
    iput p5, p0, Lee/cyber/smartid/dto/jsonrpc/ServiceAccountKeyLockInfo;->wrongAttempts:I

    return-void
.end method

.method public static from(Lee/cyber/smartid/tse/dto/jsonrpc/AccountKeyLockInfo;)Lee/cyber/smartid/dto/jsonrpc/ServiceAccountKeyLockInfo;
    .locals 7

    .line 1
    new-instance v6, Lee/cyber/smartid/dto/jsonrpc/ServiceAccountKeyLockInfo;

    invoke-virtual {p0}, Lee/cyber/smartid/tse/dto/jsonrpc/AccountKeyLockInfo;->getLockDurationSec()I

    move-result v1

    invoke-virtual {p0}, Lee/cyber/smartid/tse/dto/jsonrpc/AccountKeyLockInfo;->getPinAttemptsLeft()I

    move-result v2

    invoke-virtual {p0}, Lee/cyber/smartid/tse/dto/jsonrpc/AccountKeyLockInfo;->getPinAttemptsLeftInTotal()I

    move-result v3

    invoke-virtual {p0}, Lee/cyber/smartid/tse/dto/jsonrpc/AccountKeyLockInfo;->getNextLockDurationSec()I

    move-result v4

    invoke-virtual {p0}, Lee/cyber/smartid/tse/dto/jsonrpc/AccountKeyLockInfo;->getWrongAttempts()I

    move-result v5

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lee/cyber/smartid/dto/jsonrpc/ServiceAccountKeyLockInfo;-><init>(IIIII)V

    return-object v6
.end method


# virtual methods
.method public getLockDurationSec()I
    .locals 1

    .line 1
    iget v0, p0, Lee/cyber/smartid/dto/jsonrpc/ServiceAccountKeyLockInfo;->lockDurationSec:I

    return v0
.end method

.method public getNextLockDurationSec()I
    .locals 1

    .line 1
    iget v0, p0, Lee/cyber/smartid/dto/jsonrpc/ServiceAccountKeyLockInfo;->nextLockDurationSec:I

    return v0
.end method

.method public getPinAttemptsLeft()I
    .locals 1

    .line 1
    iget v0, p0, Lee/cyber/smartid/dto/jsonrpc/ServiceAccountKeyLockInfo;->pinAttemptsLeft:I

    return v0
.end method

.method public getPinAttemptsLeftInTotal()I
    .locals 1

    .line 1
    iget v0, p0, Lee/cyber/smartid/dto/jsonrpc/ServiceAccountKeyLockInfo;->pinAttemptsLeftInTotal:I

    return v0
.end method

.method public getWrongAttempts()I
    .locals 1

    .line 1
    iget v0, p0, Lee/cyber/smartid/dto/jsonrpc/ServiceAccountKeyLockInfo;->wrongAttempts:I

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ServiceAccountKeyLockInfo{lockDurationSec="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lee/cyber/smartid/dto/jsonrpc/ServiceAccountKeyLockInfo;->lockDurationSec:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", pinAttemptsLeft="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lee/cyber/smartid/dto/jsonrpc/ServiceAccountKeyLockInfo;->pinAttemptsLeft:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", pinAttemptsLeftInTotal="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lee/cyber/smartid/dto/jsonrpc/ServiceAccountKeyLockInfo;->pinAttemptsLeftInTotal:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", nextLockDurationSec="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lee/cyber/smartid/dto/jsonrpc/ServiceAccountKeyLockInfo;->nextLockDurationSec:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", wrongAttempts="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lee/cyber/smartid/dto/jsonrpc/ServiceAccountKeyLockInfo;->wrongAttempts:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
