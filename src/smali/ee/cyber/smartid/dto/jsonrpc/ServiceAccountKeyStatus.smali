.class public Lee/cyber/smartid/dto/jsonrpc/ServiceAccountKeyStatus;
.super Ljava/lang/Object;
.source "ServiceAccountKeyStatus.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field public static final ACCOUNT_KEY_STATUS_EXPIRED:Ljava/lang/String; = "EXPIRED"

.field public static final ACCOUNT_KEY_STATUS_IN_PREPARATION:Ljava/lang/String; = "IN_PREPARATION"

.field public static final ACCOUNT_KEY_STATUS_LOCKED:Ljava/lang/String; = "LOCKED"

.field public static final ACCOUNT_KEY_STATUS_OK:Ljava/lang/String; = "OK"

.field public static final ACCOUNT_KEY_STATUS_REVOKED:Ljava/lang/String; = "REVOKED"

.field public static final ACCOUNT_KEY_TYPE_AUTHENTICATION:Ljava/lang/String; = "AUTHENTICATION"

.field public static final ACCOUNT_KEY_TYPE_SIGNATURE:Ljava/lang/String; = "SIGNATURE"

.field private static final serialVersionUID:J = 0x71acd6fc087c586fL


# instance fields
.field private certificate:Lee/cyber/smartid/dto/jsonrpc/ServiceAccountKeyCertificate;

.field private documentCreationTime:Ljava/lang/String;

.field private documentNumber:Ljava/lang/String;

.field private keyType:Ljava/lang/String;

.field private keyUUID:Ljava/lang/String;

.field private lockInfo:Lee/cyber/smartid/dto/jsonrpc/ServiceAccountKeyLockInfo;

.field private status:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/dto/jsonrpc/ServiceAccountKeyLockInfo;Lee/cyber/smartid/dto/jsonrpc/ServiceAccountKeyCertificate;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Lee/cyber/smartid/dto/jsonrpc/ServiceAccountKeyStatus;->keyUUID:Ljava/lang/String;

    .line 3
    iput-object p2, p0, Lee/cyber/smartid/dto/jsonrpc/ServiceAccountKeyStatus;->keyType:Ljava/lang/String;

    .line 4
    iput-object p3, p0, Lee/cyber/smartid/dto/jsonrpc/ServiceAccountKeyStatus;->status:Ljava/lang/String;

    .line 5
    iput-object p4, p0, Lee/cyber/smartid/dto/jsonrpc/ServiceAccountKeyStatus;->documentNumber:Ljava/lang/String;

    .line 6
    iput-object p5, p0, Lee/cyber/smartid/dto/jsonrpc/ServiceAccountKeyStatus;->documentCreationTime:Ljava/lang/String;

    .line 7
    iput-object p6, p0, Lee/cyber/smartid/dto/jsonrpc/ServiceAccountKeyStatus;->lockInfo:Lee/cyber/smartid/dto/jsonrpc/ServiceAccountKeyLockInfo;

    .line 8
    iput-object p7, p0, Lee/cyber/smartid/dto/jsonrpc/ServiceAccountKeyStatus;->certificate:Lee/cyber/smartid/dto/jsonrpc/ServiceAccountKeyCertificate;

    return-void
.end method

.method private static findKeyUUID(Lee/cyber/smartid/tse/dto/jsonrpc/AccountKeyStatus;)Ljava/lang/String;
    .locals 1

    const-string v0, "getKeyUUID"

    .line 1
    invoke-static {p0, v0}, Lee/cyber/smartid/util/ReflectionUtil;->callStringGetterByName(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    .line 2
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    return-object p0

    :cond_0
    const/4 p0, 0x0

    return-object p0
.end method

.method public static from(Lee/cyber/smartid/tse/dto/jsonrpc/AccountKeyStatus;)Lee/cyber/smartid/dto/jsonrpc/ServiceAccountKeyStatus;
    .locals 9

    .line 1
    new-instance v8, Lee/cyber/smartid/dto/jsonrpc/ServiceAccountKeyStatus;

    invoke-static {p0}, Lee/cyber/smartid/dto/jsonrpc/ServiceAccountKeyStatus;->findKeyUUID(Lee/cyber/smartid/tse/dto/jsonrpc/AccountKeyStatus;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lee/cyber/smartid/tse/dto/jsonrpc/AccountKeyStatus;->getKeyType()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lee/cyber/smartid/tse/dto/jsonrpc/AccountKeyStatus;->getStatus()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0}, Lee/cyber/smartid/tse/dto/jsonrpc/AccountKeyStatus;->getLockInfo()Lee/cyber/smartid/tse/dto/jsonrpc/AccountKeyLockInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lee/cyber/smartid/tse/dto/jsonrpc/AccountKeyStatus;->getLockInfo()Lee/cyber/smartid/tse/dto/jsonrpc/AccountKeyLockInfo;

    move-result-object p0

    invoke-static {p0}, Lee/cyber/smartid/dto/jsonrpc/ServiceAccountKeyLockInfo;->from(Lee/cyber/smartid/tse/dto/jsonrpc/AccountKeyLockInfo;)Lee/cyber/smartid/dto/jsonrpc/ServiceAccountKeyLockInfo;

    move-result-object p0

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    move-object v6, p0

    const/4 v7, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v0, v8

    invoke-direct/range {v0 .. v7}, Lee/cyber/smartid/dto/jsonrpc/ServiceAccountKeyStatus;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/dto/jsonrpc/ServiceAccountKeyLockInfo;Lee/cyber/smartid/dto/jsonrpc/ServiceAccountKeyCertificate;)V

    return-object v8
.end method


# virtual methods
.method public getCertificate()Lee/cyber/smartid/dto/jsonrpc/ServiceAccountKeyCertificate;
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/dto/jsonrpc/ServiceAccountKeyStatus;->certificate:Lee/cyber/smartid/dto/jsonrpc/ServiceAccountKeyCertificate;

    return-object v0
.end method

.method public getDocumentCreationTime()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/dto/jsonrpc/ServiceAccountKeyStatus;->documentCreationTime:Ljava/lang/String;

    return-object v0
.end method

.method public getDocumentNumber()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/dto/jsonrpc/ServiceAccountKeyStatus;->documentNumber:Ljava/lang/String;

    return-object v0
.end method

.method public getKeyType()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/dto/jsonrpc/ServiceAccountKeyStatus;->keyType:Ljava/lang/String;

    return-object v0
.end method

.method public getKeyUUID()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/dto/jsonrpc/ServiceAccountKeyStatus;->keyUUID:Ljava/lang/String;

    return-object v0
.end method

.method public getLockInfo()Lee/cyber/smartid/dto/jsonrpc/ServiceAccountKeyLockInfo;
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/dto/jsonrpc/ServiceAccountKeyStatus;->lockInfo:Lee/cyber/smartid/dto/jsonrpc/ServiceAccountKeyLockInfo;

    return-object v0
.end method

.method public getStatus()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/dto/jsonrpc/ServiceAccountKeyStatus;->status:Ljava/lang/String;

    return-object v0
.end method

.method public setKeyType(Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lee/cyber/smartid/dto/jsonrpc/ServiceAccountKeyStatus;->keyType:Ljava/lang/String;

    return-void
.end method

.method public setKeyUUID(Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lee/cyber/smartid/dto/jsonrpc/ServiceAccountKeyStatus;->keyUUID:Ljava/lang/String;

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ServiceAccountKeyStatus{keyUUID=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lee/cyber/smartid/dto/jsonrpc/ServiceAccountKeyStatus;->keyUUID:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v2, ", keyType=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lee/cyber/smartid/dto/jsonrpc/ServiceAccountKeyStatus;->keyType:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v2, ", status=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lee/cyber/smartid/dto/jsonrpc/ServiceAccountKeyStatus;->status:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v2, ", documentNumber=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lee/cyber/smartid/dto/jsonrpc/ServiceAccountKeyStatus;->documentNumber:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v2, ", documentCreationTime=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lee/cyber/smartid/dto/jsonrpc/ServiceAccountKeyStatus;->documentCreationTime:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v1, ", lockInfo="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lee/cyber/smartid/dto/jsonrpc/ServiceAccountKeyStatus;->lockInfo:Lee/cyber/smartid/dto/jsonrpc/ServiceAccountKeyLockInfo;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", certificate="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lee/cyber/smartid/dto/jsonrpc/ServiceAccountKeyStatus;->certificate:Lee/cyber/smartid/dto/jsonrpc/ServiceAccountKeyCertificate;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
