.class public Lee/cyber/smartid/dto/jsonrpc/resp/VerifyTransactionVerificationCodeResp;
.super Lee/cyber/smartid/tse/dto/jsonrpc/resp/BaseResp;
.source "VerifyTransactionVerificationCodeResp.java"


# static fields
.field private static final serialVersionUID:J = 0x32e4785eb11c8ec5L


# instance fields
.field private final isCorrectVerificationCode:Z

.field private final verificationCode:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lee/cyber/smartid/tse/dto/jsonrpc/resp/BaseResp;-><init>(Ljava/lang/String;)V

    .line 2
    iput-object p2, p0, Lee/cyber/smartid/dto/jsonrpc/resp/VerifyTransactionVerificationCodeResp;->verificationCode:Ljava/lang/String;

    .line 3
    iput-boolean p3, p0, Lee/cyber/smartid/dto/jsonrpc/resp/VerifyTransactionVerificationCodeResp;->isCorrectVerificationCode:Z

    return-void
.end method


# virtual methods
.method public getVerificationCode()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/dto/jsonrpc/resp/VerifyTransactionVerificationCodeResp;->verificationCode:Ljava/lang/String;

    return-object v0
.end method

.method public isCorrectVerificationCode()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lee/cyber/smartid/dto/jsonrpc/resp/VerifyTransactionVerificationCodeResp;->isCorrectVerificationCode:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "VerifyTransactionVerificationCodeResp{verificationCode=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lee/cyber/smartid/dto/jsonrpc/resp/VerifyTransactionVerificationCodeResp;->verificationCode:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v1, ", isCorrectVerificationCode="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lee/cyber/smartid/dto/jsonrpc/resp/VerifyTransactionVerificationCodeResp;->isCorrectVerificationCode:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
