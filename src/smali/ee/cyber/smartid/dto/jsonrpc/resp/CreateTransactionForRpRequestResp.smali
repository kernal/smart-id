.class public Lee/cyber/smartid/dto/jsonrpc/resp/CreateTransactionForRpRequestResp;
.super Lee/cyber/smartid/dto/jsonrpc/resp/GetTransactionResp;
.source "CreateTransactionForRpRequestResp.java"


# static fields
.field private static final serialVersionUID:J = 0x373b59a76a8a2feL


# instance fields
.field private rpRequestUUID:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lee/cyber/smartid/dto/jsonrpc/resp/GetTransactionResp;)V
    .locals 0

    .line 1
    invoke-direct {p0, p2}, Lee/cyber/smartid/dto/jsonrpc/resp/GetTransactionResp;-><init>(Lee/cyber/smartid/dto/jsonrpc/resp/GetTransactionResp;)V

    .line 2
    iput-object p1, p0, Lee/cyber/smartid/dto/jsonrpc/resp/CreateTransactionForRpRequestResp;->rpRequestUUID:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public getRpRequestUUID()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/dto/jsonrpc/resp/CreateTransactionForRpRequestResp;->rpRequestUUID:Ljava/lang/String;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "CreateTransactionForRpRequestResp{rpRequestUUID=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lee/cyber/smartid/dto/jsonrpc/resp/CreateTransactionForRpRequestResp;->rpRequestUUID:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v1, "} "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2
    invoke-super {p0}, Lee/cyber/smartid/dto/jsonrpc/resp/GetTransactionResp;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
