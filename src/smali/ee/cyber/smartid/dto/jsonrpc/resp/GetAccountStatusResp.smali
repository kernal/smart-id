.class public Lee/cyber/smartid/dto/jsonrpc/resp/GetAccountStatusResp;
.super Lee/cyber/smartid/dto/jsonrpc/resp/BaseRespWithRaw;
.source "GetAccountStatusResp.java"


# static fields
.field private static final serialVersionUID:J = 0x22a0d6f378981bb7L


# instance fields
.field private final accountUUID:Ljava/lang/String;

.field private final keys:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lee/cyber/smartid/dto/jsonrpc/ServiceAccountKeyStatus;",
            ">;"
        }
    .end annotation
.end field

.field private final numberOfAccounts:J

.field private final registration:Lee/cyber/smartid/dto/jsonrpc/GetAccountStatusRegistration;

.field private final status:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Lee/cyber/smartid/dto/jsonrpc/GetAccountStatusRegistration;Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "J",
            "Ljava/lang/String;",
            "Lee/cyber/smartid/dto/jsonrpc/GetAccountStatusRegistration;",
            "Ljava/util/ArrayList<",
            "Lee/cyber/smartid/dto/jsonrpc/ServiceAccountKeyStatus;",
            ">;)V"
        }
    .end annotation

    .line 1
    invoke-direct {p0, p1, p2}, Lee/cyber/smartid/dto/jsonrpc/resp/BaseRespWithRaw;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 2
    iput-object p3, p0, Lee/cyber/smartid/dto/jsonrpc/resp/GetAccountStatusResp;->accountUUID:Ljava/lang/String;

    .line 3
    iput-wide p4, p0, Lee/cyber/smartid/dto/jsonrpc/resp/GetAccountStatusResp;->numberOfAccounts:J

    .line 4
    iput-object p6, p0, Lee/cyber/smartid/dto/jsonrpc/resp/GetAccountStatusResp;->status:Ljava/lang/String;

    .line 5
    iput-object p7, p0, Lee/cyber/smartid/dto/jsonrpc/resp/GetAccountStatusResp;->registration:Lee/cyber/smartid/dto/jsonrpc/GetAccountStatusRegistration;

    .line 6
    iput-object p8, p0, Lee/cyber/smartid/dto/jsonrpc/resp/GetAccountStatusResp;->keys:Ljava/util/ArrayList;

    return-void
.end method


# virtual methods
.method public getAccountUUID()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/dto/jsonrpc/resp/GetAccountStatusResp;->accountUUID:Ljava/lang/String;

    return-object v0
.end method

.method public getDocumentNumberForKeyType(Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    .line 1
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    return-object v1

    .line 2
    :cond_0
    iget-object v0, p0, Lee/cyber/smartid/dto/jsonrpc/resp/GetAccountStatusResp;->keys:Ljava/util/ArrayList;

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v2, 0x1

    if-ge v0, v2, :cond_1

    goto :goto_0

    .line 3
    :cond_1
    iget-object v0, p0, Lee/cyber/smartid/dto/jsonrpc/resp/GetAccountStatusResp;->keys:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lee/cyber/smartid/dto/jsonrpc/ServiceAccountKeyStatus;

    .line 4
    invoke-virtual {v2}, Lee/cyber/smartid/dto/jsonrpc/ServiceAccountKeyStatus;->getKeyType()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 5
    invoke-virtual {v2}, Lee/cyber/smartid/dto/jsonrpc/ServiceAccountKeyStatus;->getDocumentNumber()Ljava/lang/String;

    move-result-object p1

    return-object p1

    :cond_3
    :goto_0
    return-object v1
.end method

.method public getKeys()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Lee/cyber/smartid/dto/jsonrpc/ServiceAccountKeyStatus;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/dto/jsonrpc/resp/GetAccountStatusResp;->keys:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getNumberOfAccounts()J
    .locals 2

    .line 1
    iget-wide v0, p0, Lee/cyber/smartid/dto/jsonrpc/resp/GetAccountStatusResp;->numberOfAccounts:J

    return-wide v0
.end method

.method public getRegistration()Lee/cyber/smartid/dto/jsonrpc/GetAccountStatusRegistration;
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/dto/jsonrpc/resp/GetAccountStatusResp;->registration:Lee/cyber/smartid/dto/jsonrpc/GetAccountStatusRegistration;

    return-object v0
.end method

.method public getStatus()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/dto/jsonrpc/resp/GetAccountStatusResp;->status:Ljava/lang/String;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "GetAccountStatusResp{accountUUID=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lee/cyber/smartid/dto/jsonrpc/resp/GetAccountStatusResp;->accountUUID:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v2, ", numberOfAccounts="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v2, p0, Lee/cyber/smartid/dto/jsonrpc/resp/GetAccountStatusResp;->numberOfAccounts:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v2, ", status=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lee/cyber/smartid/dto/jsonrpc/resp/GetAccountStatusResp;->status:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v1, ", registration="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lee/cyber/smartid/dto/jsonrpc/resp/GetAccountStatusResp;->registration:Lee/cyber/smartid/dto/jsonrpc/GetAccountStatusRegistration;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", keys="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lee/cyber/smartid/dto/jsonrpc/resp/GetAccountStatusResp;->keys:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, "} "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2
    invoke-super {p0}, Lee/cyber/smartid/dto/jsonrpc/resp/BaseRespWithRaw;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
