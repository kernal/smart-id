.class public Lee/cyber/smartid/dto/jsonrpc/resp/GetAccountsResp;
.super Lee/cyber/smartid/tse/dto/jsonrpc/resp/BaseResp;
.source "GetAccountsResp.java"


# static fields
.field private static final serialVersionUID:J = -0x5f3ea2e0db36fcc7L


# instance fields
.field private accounts:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lee/cyber/smartid/dto/AccountWrapper;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList<",
            "Lee/cyber/smartid/dto/AccountWrapper;",
            ">;)V"
        }
    .end annotation

    .line 1
    invoke-direct {p0, p1}, Lee/cyber/smartid/tse/dto/jsonrpc/resp/BaseResp;-><init>(Ljava/lang/String;)V

    .line 2
    iput-object p2, p0, Lee/cyber/smartid/dto/jsonrpc/resp/GetAccountsResp;->accounts:Ljava/util/ArrayList;

    return-void
.end method


# virtual methods
.method public getAccounts()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Lee/cyber/smartid/dto/AccountWrapper;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/dto/jsonrpc/resp/GetAccountsResp;->accounts:Ljava/util/ArrayList;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "GetAccountsResp{accounts="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lee/cyber/smartid/dto/jsonrpc/resp/GetAccountsResp;->accounts:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
