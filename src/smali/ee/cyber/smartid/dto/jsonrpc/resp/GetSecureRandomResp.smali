.class public Lee/cyber/smartid/dto/jsonrpc/resp/GetSecureRandomResp;
.super Lee/cyber/smartid/tse/dto/jsonrpc/resp/BaseResp;
.source "GetSecureRandomResp.java"


# static fields
.field private static final serialVersionUID:J = -0x5b066788a07494e3L


# instance fields
.field private secureRandom:Ljava/security/SecureRandom;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/security/SecureRandom;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lee/cyber/smartid/tse/dto/jsonrpc/resp/BaseResp;-><init>(Ljava/lang/String;)V

    .line 2
    iput-object p2, p0, Lee/cyber/smartid/dto/jsonrpc/resp/GetSecureRandomResp;->secureRandom:Ljava/security/SecureRandom;

    return-void
.end method


# virtual methods
.method public getSecureRandom()Ljava/security/SecureRandom;
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/dto/jsonrpc/resp/GetSecureRandomResp;->secureRandom:Ljava/security/SecureRandom;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "GetSecureRandomResp{secureRandom="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lee/cyber/smartid/dto/jsonrpc/resp/GetSecureRandomResp;->secureRandom:Ljava/security/SecureRandom;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, "} "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2
    invoke-super {p0}, Lee/cyber/smartid/tse/dto/jsonrpc/resp/BaseResp;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
