.class public Lee/cyber/smartid/dto/jsonrpc/resp/GetTransactionResp;
.super Lee/cyber/smartid/tse/dto/jsonrpc/resp/BaseResp;
.source "GetTransactionResp.java"


# static fields
.field private static final serialVersionUID:J = 0x2e450c24d58619eL


# instance fields
.field private accountUUID:Ljava/lang/String;

.field private displayText:Ljava/lang/String;

.field private resultCode:Ljava/lang/String;

.field private rpName:Ljava/lang/String;

.field private state:Ljava/lang/String;

.field private transactionSource:Ljava/lang/String;

.field private transactionType:Ljava/lang/String;

.field private transactionUUID:Ljava/lang/String;

.field private ttlSec:J

.field private verificationCodes:[Ljava/lang/String;


# direct methods
.method public constructor <init>(Lee/cyber/smartid/dto/jsonrpc/resp/GetTransactionResp;)V
    .locals 2

    .line 12
    iget-object v0, p1, Lee/cyber/smartid/tse/dto/jsonrpc/resp/BaseResp;->tag:Ljava/lang/String;

    invoke-direct {p0, v0}, Lee/cyber/smartid/tse/dto/jsonrpc/resp/BaseResp;-><init>(Ljava/lang/String;)V

    .line 13
    iget-object v0, p1, Lee/cyber/smartid/dto/jsonrpc/resp/GetTransactionResp;->transactionUUID:Ljava/lang/String;

    iput-object v0, p0, Lee/cyber/smartid/dto/jsonrpc/resp/GetTransactionResp;->transactionUUID:Ljava/lang/String;

    .line 14
    iget-object v0, p1, Lee/cyber/smartid/dto/jsonrpc/resp/GetTransactionResp;->accountUUID:Ljava/lang/String;

    iput-object v0, p0, Lee/cyber/smartid/dto/jsonrpc/resp/GetTransactionResp;->accountUUID:Ljava/lang/String;

    .line 15
    iget-object v0, p1, Lee/cyber/smartid/dto/jsonrpc/resp/GetTransactionResp;->rpName:Ljava/lang/String;

    iput-object v0, p0, Lee/cyber/smartid/dto/jsonrpc/resp/GetTransactionResp;->rpName:Ljava/lang/String;

    .line 16
    iget-object v0, p1, Lee/cyber/smartid/dto/jsonrpc/resp/GetTransactionResp;->transactionType:Ljava/lang/String;

    iput-object v0, p0, Lee/cyber/smartid/dto/jsonrpc/resp/GetTransactionResp;->transactionType:Ljava/lang/String;

    .line 17
    iget-object v0, p1, Lee/cyber/smartid/dto/jsonrpc/resp/GetTransactionResp;->transactionSource:Ljava/lang/String;

    iput-object v0, p0, Lee/cyber/smartid/dto/jsonrpc/resp/GetTransactionResp;->transactionSource:Ljava/lang/String;

    .line 18
    iget-object v0, p1, Lee/cyber/smartid/dto/jsonrpc/resp/GetTransactionResp;->state:Ljava/lang/String;

    iput-object v0, p0, Lee/cyber/smartid/dto/jsonrpc/resp/GetTransactionResp;->state:Ljava/lang/String;

    .line 19
    iget-object v0, p1, Lee/cyber/smartid/dto/jsonrpc/resp/GetTransactionResp;->resultCode:Ljava/lang/String;

    iput-object v0, p0, Lee/cyber/smartid/dto/jsonrpc/resp/GetTransactionResp;->resultCode:Ljava/lang/String;

    .line 20
    iget-object v0, p1, Lee/cyber/smartid/dto/jsonrpc/resp/GetTransactionResp;->displayText:Ljava/lang/String;

    iput-object v0, p0, Lee/cyber/smartid/dto/jsonrpc/resp/GetTransactionResp;->displayText:Ljava/lang/String;

    .line 21
    iget-object v0, p1, Lee/cyber/smartid/dto/jsonrpc/resp/GetTransactionResp;->verificationCodes:[Ljava/lang/String;

    iput-object v0, p0, Lee/cyber/smartid/dto/jsonrpc/resp/GetTransactionResp;->verificationCodes:[Ljava/lang/String;

    .line 22
    iget-wide v0, p1, Lee/cyber/smartid/dto/jsonrpc/resp/GetTransactionResp;->ttlSec:J

    iput-wide v0, p0, Lee/cyber/smartid/dto/jsonrpc/resp/GetTransactionResp;->ttlSec:J

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lee/cyber/smartid/dto/jsonrpc/Transaction;[Ljava/lang/String;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lee/cyber/smartid/tse/dto/jsonrpc/resp/BaseResp;-><init>(Ljava/lang/String;)V

    .line 2
    invoke-virtual {p2}, Lee/cyber/smartid/dto/jsonrpc/Transaction;->getTransactionUUID()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lee/cyber/smartid/dto/jsonrpc/resp/GetTransactionResp;->transactionUUID:Ljava/lang/String;

    .line 3
    invoke-virtual {p2}, Lee/cyber/smartid/dto/jsonrpc/Transaction;->getRpName()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lee/cyber/smartid/dto/jsonrpc/resp/GetTransactionResp;->rpName:Ljava/lang/String;

    .line 4
    invoke-virtual {p2}, Lee/cyber/smartid/dto/jsonrpc/Transaction;->getTransactionType()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lee/cyber/smartid/dto/jsonrpc/resp/GetTransactionResp;->transactionType:Ljava/lang/String;

    .line 5
    invoke-virtual {p2}, Lee/cyber/smartid/dto/jsonrpc/Transaction;->getTransactionSource()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lee/cyber/smartid/dto/jsonrpc/resp/GetTransactionResp;->transactionSource:Ljava/lang/String;

    .line 6
    invoke-virtual {p2}, Lee/cyber/smartid/dto/jsonrpc/Transaction;->getState()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lee/cyber/smartid/dto/jsonrpc/resp/GetTransactionResp;->state:Ljava/lang/String;

    .line 7
    invoke-virtual {p2}, Lee/cyber/smartid/dto/jsonrpc/Transaction;->getResultCode()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lee/cyber/smartid/dto/jsonrpc/resp/GetTransactionResp;->resultCode:Ljava/lang/String;

    .line 8
    invoke-virtual {p2}, Lee/cyber/smartid/dto/jsonrpc/Transaction;->getDisplayText()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lee/cyber/smartid/dto/jsonrpc/resp/GetTransactionResp;->displayText:Ljava/lang/String;

    .line 9
    iput-object p3, p0, Lee/cyber/smartid/dto/jsonrpc/resp/GetTransactionResp;->verificationCodes:[Ljava/lang/String;

    .line 10
    invoke-virtual {p2}, Lee/cyber/smartid/dto/jsonrpc/Transaction;->getAccountUUID()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lee/cyber/smartid/dto/jsonrpc/resp/GetTransactionResp;->accountUUID:Ljava/lang/String;

    .line 11
    invoke-virtual {p2}, Lee/cyber/smartid/dto/jsonrpc/Transaction;->getTtlSec()J

    move-result-wide p1

    iput-wide p1, p0, Lee/cyber/smartid/dto/jsonrpc/resp/GetTransactionResp;->ttlSec:J

    return-void
.end method


# virtual methods
.method public getAccountUUID()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/dto/jsonrpc/resp/GetTransactionResp;->accountUUID:Ljava/lang/String;

    return-object v0
.end method

.method public getDisplayText()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/dto/jsonrpc/resp/GetTransactionResp;->displayText:Ljava/lang/String;

    return-object v0
.end method

.method public getResultCode()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/dto/jsonrpc/resp/GetTransactionResp;->resultCode:Ljava/lang/String;

    return-object v0
.end method

.method public getRpName()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/dto/jsonrpc/resp/GetTransactionResp;->rpName:Ljava/lang/String;

    return-object v0
.end method

.method public getState()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/dto/jsonrpc/resp/GetTransactionResp;->state:Ljava/lang/String;

    return-object v0
.end method

.method public getTransactionSource()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/dto/jsonrpc/resp/GetTransactionResp;->transactionSource:Ljava/lang/String;

    return-object v0
.end method

.method public getTransactionType()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/dto/jsonrpc/resp/GetTransactionResp;->transactionType:Ljava/lang/String;

    return-object v0
.end method

.method public getTransactionUUID()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/dto/jsonrpc/resp/GetTransactionResp;->transactionUUID:Ljava/lang/String;

    return-object v0
.end method

.method public getTtlSec()J
    .locals 2

    .line 1
    iget-wide v0, p0, Lee/cyber/smartid/dto/jsonrpc/resp/GetTransactionResp;->ttlSec:J

    return-wide v0
.end method

.method public getVerificationCodes()[Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/dto/jsonrpc/resp/GetTransactionResp;->verificationCodes:[Ljava/lang/String;

    return-object v0
.end method

.method public isMultiCodeVerification()Z
    .locals 2

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/dto/jsonrpc/resp/GetTransactionResp;->verificationCodes:[Ljava/lang/String;

    array-length v0, v0

    const/4 v1, 0x1

    if-le v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return v1
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "GetTransactionResp{transactionUUID=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lee/cyber/smartid/dto/jsonrpc/resp/GetTransactionResp;->transactionUUID:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v2, ", accountUUID=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lee/cyber/smartid/dto/jsonrpc/resp/GetTransactionResp;->accountUUID:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v2, ", rpName=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lee/cyber/smartid/dto/jsonrpc/resp/GetTransactionResp;->rpName:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v2, ", transactionType=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lee/cyber/smartid/dto/jsonrpc/resp/GetTransactionResp;->transactionType:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v2, ", transactionSource=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lee/cyber/smartid/dto/jsonrpc/resp/GetTransactionResp;->transactionSource:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v2, ", displayText=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lee/cyber/smartid/dto/jsonrpc/resp/GetTransactionResp;->displayText:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v2, ", verificationCodes=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lee/cyber/smartid/dto/jsonrpc/resp/GetTransactionResp;->verificationCodes:[Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 2
    invoke-static {v2}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    :cond_0
    const-string v2, ""

    :goto_0
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v1, ", isMultiCodeVerification="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 3
    invoke-virtual {p0}, Lee/cyber/smartid/dto/jsonrpc/resp/GetTransactionResp;->isMultiCodeVerification()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", ttlSec="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lee/cyber/smartid/dto/jsonrpc/resp/GetTransactionResp;->ttlSec:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, "} "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 4
    invoke-super {p0}, Lee/cyber/smartid/tse/dto/jsonrpc/resp/BaseResp;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
