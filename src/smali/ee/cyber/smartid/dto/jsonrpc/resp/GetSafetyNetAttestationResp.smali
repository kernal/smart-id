.class public Lee/cyber/smartid/dto/jsonrpc/resp/GetSafetyNetAttestationResp;
.super Lee/cyber/smartid/tse/dto/jsonrpc/resp/BaseResp;
.source "GetSafetyNetAttestationResp.java"


# static fields
.field private static final serialVersionUID:J = 0x5994bb3554c2e405L


# instance fields
.field private final attestation:Lee/cyber/smartid/dto/SafetyNetAttestation;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lee/cyber/smartid/dto/SafetyNetAttestation;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lee/cyber/smartid/tse/dto/jsonrpc/resp/BaseResp;-><init>(Ljava/lang/String;)V

    .line 2
    iput-object p2, p0, Lee/cyber/smartid/dto/jsonrpc/resp/GetSafetyNetAttestationResp;->attestation:Lee/cyber/smartid/dto/SafetyNetAttestation;

    return-void
.end method


# virtual methods
.method public getAttestation()Lee/cyber/smartid/dto/SafetyNetAttestation;
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/dto/jsonrpc/resp/GetSafetyNetAttestationResp;->attestation:Lee/cyber/smartid/dto/SafetyNetAttestation;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "GetSafetyNetAttestationResp{attestation=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lee/cyber/smartid/dto/jsonrpc/resp/GetSafetyNetAttestationResp;->attestation:Lee/cyber/smartid/dto/SafetyNetAttestation;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v1, "} "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2
    invoke-super {p0}, Lee/cyber/smartid/tse/dto/jsonrpc/resp/BaseResp;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
