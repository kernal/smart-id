.class public Lee/cyber/smartid/dto/jsonrpc/resp/CheckLocalPendingStateResp;
.super Lee/cyber/smartid/tse/dto/jsonrpc/resp/BaseResp;
.source "CheckLocalPendingStateResp.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lee/cyber/smartid/dto/jsonrpc/resp/CheckLocalPendingStateResp$CheckLocalPendingStateType;
    }
.end annotation


# static fields
.field public static final LOCAL_PENDING_STATE_TYPE_CONFIRM_TRANSACTION:Ljava/lang/String; = "confirmTransaction"

.field public static final LOCAL_PENDING_STATE_TYPE_NONE:Ljava/lang/String; = "none"

.field public static final LOCAL_PENDING_STATE_TYPE_REGISTRATION:Ljava/lang/String; = "addAccount"

.field private static final serialVersionUID:J = -0x14658a391fc6a938L


# instance fields
.field private final accountUUID:Ljava/lang/String;

.field private final keyType:Ljava/lang/String;

.field private final localPendingStateReferenceId:Ljava/lang/String;

.field private final localPendingStateType:Ljava/lang/String;

.field private final transactionUUID:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 7
    invoke-direct {p0, p1}, Lee/cyber/smartid/tse/dto/jsonrpc/resp/BaseResp;-><init>(Ljava/lang/String;)V

    const/4 p1, 0x0

    .line 8
    iput-object p1, p0, Lee/cyber/smartid/dto/jsonrpc/resp/CheckLocalPendingStateResp;->transactionUUID:Ljava/lang/String;

    .line 9
    iput-object p1, p0, Lee/cyber/smartid/dto/jsonrpc/resp/CheckLocalPendingStateResp;->accountUUID:Ljava/lang/String;

    .line 10
    iput-object p1, p0, Lee/cyber/smartid/dto/jsonrpc/resp/CheckLocalPendingStateResp;->keyType:Ljava/lang/String;

    .line 11
    iput-object p2, p0, Lee/cyber/smartid/dto/jsonrpc/resp/CheckLocalPendingStateResp;->localPendingStateReferenceId:Ljava/lang/String;

    .line 12
    iput-object p3, p0, Lee/cyber/smartid/dto/jsonrpc/resp/CheckLocalPendingStateResp;->localPendingStateType:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lee/cyber/smartid/tse/dto/jsonrpc/resp/BaseResp;-><init>(Ljava/lang/String;)V

    .line 2
    iput-object p6, p0, Lee/cyber/smartid/dto/jsonrpc/resp/CheckLocalPendingStateResp;->transactionUUID:Ljava/lang/String;

    .line 3
    iput-object p2, p0, Lee/cyber/smartid/dto/jsonrpc/resp/CheckLocalPendingStateResp;->accountUUID:Ljava/lang/String;

    .line 4
    iput-object p3, p0, Lee/cyber/smartid/dto/jsonrpc/resp/CheckLocalPendingStateResp;->keyType:Ljava/lang/String;

    .line 5
    iput-object p4, p0, Lee/cyber/smartid/dto/jsonrpc/resp/CheckLocalPendingStateResp;->localPendingStateReferenceId:Ljava/lang/String;

    .line 6
    iput-object p5, p0, Lee/cyber/smartid/dto/jsonrpc/resp/CheckLocalPendingStateResp;->localPendingStateType:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public getAccountUUID()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/dto/jsonrpc/resp/CheckLocalPendingStateResp;->accountUUID:Ljava/lang/String;

    return-object v0
.end method

.method public getKeyType()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/dto/jsonrpc/resp/CheckLocalPendingStateResp;->keyType:Ljava/lang/String;

    return-object v0
.end method

.method public getLocalPendingStateReferenceId()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/dto/jsonrpc/resp/CheckLocalPendingStateResp;->localPendingStateReferenceId:Ljava/lang/String;

    return-object v0
.end method

.method public getLocalPendingStateType()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/dto/jsonrpc/resp/CheckLocalPendingStateResp;->localPendingStateType:Ljava/lang/String;

    return-object v0
.end method

.method public getTransactionUUID()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/dto/jsonrpc/resp/CheckLocalPendingStateResp;->transactionUUID:Ljava/lang/String;

    return-object v0
.end method

.method public hasLocalPendingState()Z
    .locals 2

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/dto/jsonrpc/resp/CheckLocalPendingStateResp;->localPendingStateReferenceId:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lee/cyber/smartid/dto/jsonrpc/resp/CheckLocalPendingStateResp;->localPendingStateType:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lee/cyber/smartid/dto/jsonrpc/resp/CheckLocalPendingStateResp;->localPendingStateType:Ljava/lang/String;

    const-string v1, "none"

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "CheckLocalPendingStateResp{localPendingStateReferenceId=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lee/cyber/smartid/dto/jsonrpc/resp/CheckLocalPendingStateResp;->localPendingStateReferenceId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v2, ", accountUUID=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lee/cyber/smartid/dto/jsonrpc/resp/CheckLocalPendingStateResp;->accountUUID:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v2, ", keyType=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lee/cyber/smartid/dto/jsonrpc/resp/CheckLocalPendingStateResp;->keyType:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v2, ", localPendingStateType=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lee/cyber/smartid/dto/jsonrpc/resp/CheckLocalPendingStateResp;->localPendingStateType:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v2, ", transactionUUID=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lee/cyber/smartid/dto/jsonrpc/resp/CheckLocalPendingStateResp;->transactionUUID:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v1, "} "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2
    invoke-super {p0}, Lee/cyber/smartid/tse/dto/jsonrpc/resp/BaseResp;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
