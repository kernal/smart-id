.class public Lee/cyber/smartid/dto/jsonrpc/resp/ConfirmRPRequestResp;
.super Lee/cyber/smartid/tse/dto/jsonrpc/resp/BaseResp;
.source "ConfirmRPRequestResp.java"


# static fields
.field private static final serialVersionUID:J = 0x2491137c895e0a10L


# instance fields
.field private final accountUUID:Ljava/lang/String;

.field private final rpRequestUUID:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lee/cyber/smartid/tse/dto/jsonrpc/resp/BaseResp;-><init>(Ljava/lang/String;)V

    .line 2
    iput-object p2, p0, Lee/cyber/smartid/dto/jsonrpc/resp/ConfirmRPRequestResp;->accountUUID:Ljava/lang/String;

    .line 3
    iput-object p3, p0, Lee/cyber/smartid/dto/jsonrpc/resp/ConfirmRPRequestResp;->rpRequestUUID:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public getAccountUUID()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/dto/jsonrpc/resp/ConfirmRPRequestResp;->accountUUID:Ljava/lang/String;

    return-object v0
.end method

.method public getRPRequestUUID()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/dto/jsonrpc/resp/ConfirmRPRequestResp;->rpRequestUUID:Ljava/lang/String;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ConfirmRPRequestResp{accountUUID=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lee/cyber/smartid/dto/jsonrpc/resp/ConfirmRPRequestResp;->accountUUID:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v2, ", rpRequestUUID=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lee/cyber/smartid/dto/jsonrpc/resp/ConfirmRPRequestResp;->rpRequestUUID:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
