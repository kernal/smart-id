.class public Lee/cyber/smartid/dto/jsonrpc/resp/GetPendingOperationResp;
.super Lee/cyber/smartid/tse/dto/jsonrpc/resp/BaseResp;
.source "GetPendingOperationResp.java"


# static fields
.field private static final serialVersionUID:J = 0x7ef392f473ce8da0L


# instance fields
.field private final rpRequestResp:Lee/cyber/smartid/dto/jsonrpc/resp/GetRPRequestResp;

.field private final transactionResp:Lee/cyber/smartid/dto/jsonrpc/resp/GetTransactionResp;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lee/cyber/smartid/dto/jsonrpc/resp/GetTransactionResp;Lee/cyber/smartid/dto/jsonrpc/resp/GetRPRequestResp;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lee/cyber/smartid/tse/dto/jsonrpc/resp/BaseResp;-><init>(Ljava/lang/String;)V

    .line 2
    iput-object p2, p0, Lee/cyber/smartid/dto/jsonrpc/resp/GetPendingOperationResp;->transactionResp:Lee/cyber/smartid/dto/jsonrpc/resp/GetTransactionResp;

    .line 3
    iput-object p3, p0, Lee/cyber/smartid/dto/jsonrpc/resp/GetPendingOperationResp;->rpRequestResp:Lee/cyber/smartid/dto/jsonrpc/resp/GetRPRequestResp;

    return-void
.end method


# virtual methods
.method public getRpRequestResp()Lee/cyber/smartid/dto/jsonrpc/resp/GetRPRequestResp;
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/dto/jsonrpc/resp/GetPendingOperationResp;->rpRequestResp:Lee/cyber/smartid/dto/jsonrpc/resp/GetRPRequestResp;

    return-object v0
.end method

.method public getTransactionResp()Lee/cyber/smartid/dto/jsonrpc/resp/GetTransactionResp;
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/dto/jsonrpc/resp/GetPendingOperationResp;->transactionResp:Lee/cyber/smartid/dto/jsonrpc/resp/GetTransactionResp;

    return-object v0
.end method

.method public isRPRequest()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/dto/jsonrpc/resp/GetPendingOperationResp;->rpRequestResp:Lee/cyber/smartid/dto/jsonrpc/resp/GetRPRequestResp;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isTransaction()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/dto/jsonrpc/resp/GetPendingOperationResp;->transactionResp:Lee/cyber/smartid/dto/jsonrpc/resp/GetTransactionResp;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "GetPendingOperationResp{transactionResp="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lee/cyber/smartid/dto/jsonrpc/resp/GetPendingOperationResp;->transactionResp:Lee/cyber/smartid/dto/jsonrpc/resp/GetTransactionResp;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", rpRequestResp="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lee/cyber/smartid/dto/jsonrpc/resp/GetPendingOperationResp;->rpRequestResp:Lee/cyber/smartid/dto/jsonrpc/resp/GetRPRequestResp;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
