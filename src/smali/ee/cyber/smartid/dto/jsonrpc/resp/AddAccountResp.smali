.class public Lee/cyber/smartid/dto/jsonrpc/resp/AddAccountResp;
.super Lee/cyber/smartid/tse/dto/jsonrpc/resp/BaseResp;
.source "AddAccountResp.java"


# static fields
.field private static final serialVersionUID:J = -0x699eddce2d2dd3fdL


# instance fields
.field private final accountInitiationType:Ljava/lang/String;

.field private final accountUUID:Ljava/lang/String;

.field private final accounts:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lee/cyber/smartid/dto/AccountWrapper;",
            ">;"
        }
    .end annotation
.end field

.field private final authCsrTransactionUUID:Ljava/lang/String;

.field private final identityData:Lee/cyber/smartid/dto/jsonrpc/IdentityData;

.field private final signCsrTransactionUUID:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lee/cyber/smartid/dto/jsonrpc/resp/AddAccountResp;)V
    .locals 0

    .line 8
    invoke-direct {p0, p1}, Lee/cyber/smartid/tse/dto/jsonrpc/resp/BaseResp;-><init>(Ljava/lang/String;)V

    .line 9
    iget-object p1, p2, Lee/cyber/smartid/dto/jsonrpc/resp/AddAccountResp;->accounts:Ljava/util/ArrayList;

    iput-object p1, p0, Lee/cyber/smartid/dto/jsonrpc/resp/AddAccountResp;->accounts:Ljava/util/ArrayList;

    .line 10
    iget-object p1, p2, Lee/cyber/smartid/dto/jsonrpc/resp/AddAccountResp;->accountUUID:Ljava/lang/String;

    iput-object p1, p0, Lee/cyber/smartid/dto/jsonrpc/resp/AddAccountResp;->accountUUID:Ljava/lang/String;

    .line 11
    iget-object p1, p2, Lee/cyber/smartid/dto/jsonrpc/resp/AddAccountResp;->authCsrTransactionUUID:Ljava/lang/String;

    iput-object p1, p0, Lee/cyber/smartid/dto/jsonrpc/resp/AddAccountResp;->authCsrTransactionUUID:Ljava/lang/String;

    .line 12
    iget-object p1, p2, Lee/cyber/smartid/dto/jsonrpc/resp/AddAccountResp;->signCsrTransactionUUID:Ljava/lang/String;

    iput-object p1, p0, Lee/cyber/smartid/dto/jsonrpc/resp/AddAccountResp;->signCsrTransactionUUID:Ljava/lang/String;

    .line 13
    iget-object p1, p2, Lee/cyber/smartid/dto/jsonrpc/resp/AddAccountResp;->accountInitiationType:Ljava/lang/String;

    iput-object p1, p0, Lee/cyber/smartid/dto/jsonrpc/resp/AddAccountResp;->accountInitiationType:Ljava/lang/String;

    .line 14
    iget-object p1, p2, Lee/cyber/smartid/dto/jsonrpc/resp/AddAccountResp;->identityData:Lee/cyber/smartid/dto/jsonrpc/IdentityData;

    iput-object p1, p0, Lee/cyber/smartid/dto/jsonrpc/resp/AddAccountResp;->identityData:Lee/cyber/smartid/dto/jsonrpc/IdentityData;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/dto/jsonrpc/IdentityData;Ljava/util/ArrayList;Ljava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lee/cyber/smartid/dto/jsonrpc/IdentityData;",
            "Ljava/util/ArrayList<",
            "Lee/cyber/smartid/dto/AccountWrapper;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 1
    invoke-direct {p0, p1}, Lee/cyber/smartid/tse/dto/jsonrpc/resp/BaseResp;-><init>(Ljava/lang/String;)V

    .line 2
    iput-object p2, p0, Lee/cyber/smartid/dto/jsonrpc/resp/AddAccountResp;->accountUUID:Ljava/lang/String;

    .line 3
    iput-object p3, p0, Lee/cyber/smartid/dto/jsonrpc/resp/AddAccountResp;->authCsrTransactionUUID:Ljava/lang/String;

    .line 4
    iput-object p4, p0, Lee/cyber/smartid/dto/jsonrpc/resp/AddAccountResp;->signCsrTransactionUUID:Ljava/lang/String;

    .line 5
    iput-object p5, p0, Lee/cyber/smartid/dto/jsonrpc/resp/AddAccountResp;->identityData:Lee/cyber/smartid/dto/jsonrpc/IdentityData;

    .line 6
    iput-object p6, p0, Lee/cyber/smartid/dto/jsonrpc/resp/AddAccountResp;->accounts:Ljava/util/ArrayList;

    .line 7
    iput-object p7, p0, Lee/cyber/smartid/dto/jsonrpc/resp/AddAccountResp;->accountInitiationType:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public getAccountInitiationType()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/dto/jsonrpc/resp/AddAccountResp;->accountInitiationType:Ljava/lang/String;

    return-object v0
.end method

.method public getAccountUUID()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/dto/jsonrpc/resp/AddAccountResp;->accountUUID:Ljava/lang/String;

    return-object v0
.end method

.method public getAccounts()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Lee/cyber/smartid/dto/AccountWrapper;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/dto/jsonrpc/resp/AddAccountResp;->accounts:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getAuthCsrTransactionUUID()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/dto/jsonrpc/resp/AddAccountResp;->authCsrTransactionUUID:Ljava/lang/String;

    return-object v0
.end method

.method public getIdentityData()Lee/cyber/smartid/dto/jsonrpc/IdentityData;
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/dto/jsonrpc/resp/AddAccountResp;->identityData:Lee/cyber/smartid/dto/jsonrpc/IdentityData;

    return-object v0
.end method

.method public getSignCsrTransactionUUID()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/dto/jsonrpc/resp/AddAccountResp;->signCsrTransactionUUID:Ljava/lang/String;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "AddAccountResp{accounts="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lee/cyber/smartid/dto/jsonrpc/resp/AddAccountResp;->accounts:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", accountUUID=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lee/cyber/smartid/dto/jsonrpc/resp/AddAccountResp;->accountUUID:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v2, ", authCsrTransactionUUID=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lee/cyber/smartid/dto/jsonrpc/resp/AddAccountResp;->authCsrTransactionUUID:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v2, ", signCsrTransactionUUID=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lee/cyber/smartid/dto/jsonrpc/resp/AddAccountResp;->signCsrTransactionUUID:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v2, ", accountInitiationType=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lee/cyber/smartid/dto/jsonrpc/resp/AddAccountResp;->accountInitiationType:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v2, ", identityData=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lee/cyber/smartid/dto/jsonrpc/resp/AddAccountResp;->identityData:Lee/cyber/smartid/dto/jsonrpc/IdentityData;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
