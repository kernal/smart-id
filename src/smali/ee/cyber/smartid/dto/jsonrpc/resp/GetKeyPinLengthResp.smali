.class public Lee/cyber/smartid/dto/jsonrpc/resp/GetKeyPinLengthResp;
.super Lee/cyber/smartid/tse/dto/jsonrpc/resp/BaseResp;
.source "GetKeyPinLengthResp.java"


# static fields
.field private static final serialVersionUID:J = 0x3fc0336635296461L


# instance fields
.field private accountId:Ljava/lang/String;

.field private extras:Landroid/os/Bundle;

.field private keyPinLength:I

.field private keyType:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILandroid/os/Bundle;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lee/cyber/smartid/tse/dto/jsonrpc/resp/BaseResp;-><init>(Ljava/lang/String;)V

    .line 2
    iput-object p2, p0, Lee/cyber/smartid/dto/jsonrpc/resp/GetKeyPinLengthResp;->accountId:Ljava/lang/String;

    .line 3
    iput-object p3, p0, Lee/cyber/smartid/dto/jsonrpc/resp/GetKeyPinLengthResp;->keyType:Ljava/lang/String;

    .line 4
    iput p4, p0, Lee/cyber/smartid/dto/jsonrpc/resp/GetKeyPinLengthResp;->keyPinLength:I

    .line 5
    iput-object p5, p0, Lee/cyber/smartid/dto/jsonrpc/resp/GetKeyPinLengthResp;->extras:Landroid/os/Bundle;

    return-void
.end method


# virtual methods
.method public getAccountId()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/dto/jsonrpc/resp/GetKeyPinLengthResp;->accountId:Ljava/lang/String;

    return-object v0
.end method

.method public getExtras()Landroid/os/Bundle;
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/dto/jsonrpc/resp/GetKeyPinLengthResp;->extras:Landroid/os/Bundle;

    return-object v0
.end method

.method public getKeyPinLength()I
    .locals 1

    .line 1
    iget v0, p0, Lee/cyber/smartid/dto/jsonrpc/resp/GetKeyPinLengthResp;->keyPinLength:I

    return v0
.end method

.method public getKeyType()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/dto/jsonrpc/resp/GetKeyPinLengthResp;->keyType:Ljava/lang/String;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "GetKeyPinLengthResp{accountId=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lee/cyber/smartid/dto/jsonrpc/resp/GetKeyPinLengthResp;->accountId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v2, ", keyType=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lee/cyber/smartid/dto/jsonrpc/resp/GetKeyPinLengthResp;->keyType:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v1, ", keyPinLength="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lee/cyber/smartid/dto/jsonrpc/resp/GetKeyPinLengthResp;->keyPinLength:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
