.class public Lee/cyber/smartid/dto/jsonrpc/resp/SafetyNetAttestationSummary;
.super Lee/cyber/smartid/tse/dto/jsonrpc/resp/BaseResp;
.source "SafetyNetAttestationSummary.java"


# static fields
.field private static final serialVersionUID:J = -0x47f94440856af7e0L


# instance fields
.field private attestationDataPresent:Z

.field private basicIntegrity:Z

.field private ctsProfileMatch:Z

.field private localBinaryValidation:Z


# direct methods
.method public constructor <init>(Lee/cyber/smartid/dto/jsonrpc/resp/SafetyNetAttestationSummary;)V
    .locals 1

    .line 7
    invoke-virtual {p1}, Lee/cyber/smartid/tse/dto/jsonrpc/resp/BaseResp;->getTag()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lee/cyber/smartid/tse/dto/jsonrpc/resp/BaseResp;-><init>(Ljava/lang/String;)V

    .line 8
    iget-boolean v0, p1, Lee/cyber/smartid/dto/jsonrpc/resp/SafetyNetAttestationSummary;->attestationDataPresent:Z

    iput-boolean v0, p0, Lee/cyber/smartid/dto/jsonrpc/resp/SafetyNetAttestationSummary;->attestationDataPresent:Z

    .line 9
    iget-boolean v0, p1, Lee/cyber/smartid/dto/jsonrpc/resp/SafetyNetAttestationSummary;->localBinaryValidation:Z

    iput-boolean v0, p0, Lee/cyber/smartid/dto/jsonrpc/resp/SafetyNetAttestationSummary;->localBinaryValidation:Z

    .line 10
    iget-boolean v0, p1, Lee/cyber/smartid/dto/jsonrpc/resp/SafetyNetAttestationSummary;->basicIntegrity:Z

    iput-boolean v0, p0, Lee/cyber/smartid/dto/jsonrpc/resp/SafetyNetAttestationSummary;->basicIntegrity:Z

    .line 11
    iget-boolean p1, p1, Lee/cyber/smartid/dto/jsonrpc/resp/SafetyNetAttestationSummary;->ctsProfileMatch:Z

    iput-boolean p1, p0, Lee/cyber/smartid/dto/jsonrpc/resp/SafetyNetAttestationSummary;->ctsProfileMatch:Z

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lee/cyber/smartid/tse/dto/jsonrpc/resp/BaseResp;-><init>(Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;ZZZZ)V
    .locals 0

    .line 2
    invoke-direct {p0, p1}, Lee/cyber/smartid/tse/dto/jsonrpc/resp/BaseResp;-><init>(Ljava/lang/String;)V

    .line 3
    iput-boolean p2, p0, Lee/cyber/smartid/dto/jsonrpc/resp/SafetyNetAttestationSummary;->attestationDataPresent:Z

    .line 4
    iput-boolean p3, p0, Lee/cyber/smartid/dto/jsonrpc/resp/SafetyNetAttestationSummary;->localBinaryValidation:Z

    .line 5
    iput-boolean p4, p0, Lee/cyber/smartid/dto/jsonrpc/resp/SafetyNetAttestationSummary;->basicIntegrity:Z

    .line 6
    iput-boolean p5, p0, Lee/cyber/smartid/dto/jsonrpc/resp/SafetyNetAttestationSummary;->ctsProfileMatch:Z

    return-void
.end method


# virtual methods
.method public isAttestationDataPresent()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lee/cyber/smartid/dto/jsonrpc/resp/SafetyNetAttestationSummary;->attestationDataPresent:Z

    return v0
.end method

.method public isBasicIntegrityVerified()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lee/cyber/smartid/dto/jsonrpc/resp/SafetyNetAttestationSummary;->basicIntegrity:Z

    return v0
.end method

.method public isCtsProfileMatch()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lee/cyber/smartid/dto/jsonrpc/resp/SafetyNetAttestationSummary;->ctsProfileMatch:Z

    return v0
.end method

.method public isLocalValidationSuccess()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lee/cyber/smartid/dto/jsonrpc/resp/SafetyNetAttestationSummary;->localBinaryValidation:Z

    return v0
.end method

.method public isSuccess()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lee/cyber/smartid/dto/jsonrpc/resp/SafetyNetAttestationSummary;->attestationDataPresent:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lee/cyber/smartid/dto/jsonrpc/resp/SafetyNetAttestationSummary;->localBinaryValidation:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lee/cyber/smartid/dto/jsonrpc/resp/SafetyNetAttestationSummary;->basicIntegrity:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lee/cyber/smartid/dto/jsonrpc/resp/SafetyNetAttestationSummary;->ctsProfileMatch:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "SafetyNetAttestationSummary{attestationDataPresent="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lee/cyber/smartid/dto/jsonrpc/resp/SafetyNetAttestationSummary;->attestationDataPresent:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", localBinaryValidation="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lee/cyber/smartid/dto/jsonrpc/resp/SafetyNetAttestationSummary;->localBinaryValidation:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", basicIntegrity="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lee/cyber/smartid/dto/jsonrpc/resp/SafetyNetAttestationSummary;->basicIntegrity:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", ctsProfileMatch="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lee/cyber/smartid/dto/jsonrpc/resp/SafetyNetAttestationSummary;->ctsProfileMatch:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, "} "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2
    invoke-super {p0}, Lee/cyber/smartid/tse/dto/jsonrpc/resp/BaseResp;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
