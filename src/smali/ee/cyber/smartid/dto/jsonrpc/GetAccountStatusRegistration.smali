.class public Lee/cyber/smartid/dto/jsonrpc/GetAccountStatusRegistration;
.super Ljava/lang/Object;
.source "GetAccountStatusRegistration.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field public static final RESULT_CODE_CA_REFUSED:Ljava/lang/String; = "CA_REFUSED"

.field public static final RESULT_CODE_KEY_LOCKED:Ljava/lang/String; = "KEY_LOCKED"

.field public static final RESULT_CODE_OK:Ljava/lang/String; = "OK"

.field public static final RESULT_CODE_POOR_KEY:Ljava/lang/String; = "POOR_KEY"

.field public static final RESULT_CODE_TIMEOUT:Ljava/lang/String; = "TIMEOUT"

.field public static final STATE_CERTIFICATION:Ljava/lang/String; = "CERTIFICATION"

.field public static final STATE_COMPLETE:Ljava/lang/String; = "COMPLETE"

.field public static final STATE_DOCUMENT_CREATED:Ljava/lang/String; = "DOCUMENT_CREATED"

.field public static final STATE_WAITING_APPROVAL:Ljava/lang/String; = "WAITING_APPROVAL"

.field public static final TIMEOUT_SOURCE_APPROVAL:Ljava/lang/String; = "APPROVAL"

.field public static final TIMEOUT_SOURCE_CA:Ljava/lang/String; = "CA"

.field public static final TIMEOUT_SOURCE_CSR:Ljava/lang/String; = "CSR"

.field private static final serialVersionUID:J = -0xab63c721d688990L


# instance fields
.field private documentNumber:Ljava/lang/String;

.field private missingApproval:Ljava/lang/String;

.field private resultCode:Ljava/lang/String;

.field private state:Ljava/lang/String;

.field private timeoutSource:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getDocumentNumber()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/dto/jsonrpc/GetAccountStatusRegistration;->documentNumber:Ljava/lang/String;

    return-object v0
.end method

.method public getMissingApproval()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/dto/jsonrpc/GetAccountStatusRegistration;->missingApproval:Ljava/lang/String;

    return-object v0
.end method

.method public getResultCode()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/dto/jsonrpc/GetAccountStatusRegistration;->resultCode:Ljava/lang/String;

    return-object v0
.end method

.method public getState()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/dto/jsonrpc/GetAccountStatusRegistration;->state:Ljava/lang/String;

    return-object v0
.end method

.method public getTimeoutSource()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/dto/jsonrpc/GetAccountStatusRegistration;->timeoutSource:Ljava/lang/String;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "GetAccountStatusRegistration{documentNumber=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lee/cyber/smartid/dto/jsonrpc/GetAccountStatusRegistration;->documentNumber:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v2, ", state=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lee/cyber/smartid/dto/jsonrpc/GetAccountStatusRegistration;->state:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v2, ", resultCode=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lee/cyber/smartid/dto/jsonrpc/GetAccountStatusRegistration;->resultCode:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v2, ", timeoutSource=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lee/cyber/smartid/dto/jsonrpc/GetAccountStatusRegistration;->timeoutSource:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v2, ", missingApproval=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lee/cyber/smartid/dto/jsonrpc/GetAccountStatusRegistration;->missingApproval:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
