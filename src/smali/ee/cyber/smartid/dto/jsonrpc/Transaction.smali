.class public Lee/cyber/smartid/dto/jsonrpc/Transaction;
.super Ljava/lang/Object;
.source "Transaction.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field public static final DEFAULT_TTL_SEC:J = 0x12cL

.field public static final TRANSACTION_RESULT_CODE_OK:Ljava/lang/String; = "OK"

.field public static final TRANSACTION_RESULT_CODE_SERVER_CANCELLED:Ljava/lang/String; = "SERVER_CANCELLED"

.field public static final TRANSACTION_RESULT_CODE_TIMEOUT:Ljava/lang/String; = "TIMEOUT"

.field public static final TRANSACTION_RESULT_CODE_UNKNOWN:Ljava/lang/String; = null

.field public static final TRANSACTION_RESULT_CODE_USER_REFUSED:Ljava/lang/String; = "USER_REFUSED"

.field public static final TRANSACTION_RESULT_CODE_WRONG_VC:Ljava/lang/String; = "WRONG_VC"

.field public static final TRANSACTION_SOURCE_CSR:Ljava/lang/String; = "CSR"

.field public static final TRANSACTION_SOURCE_RELYING_PARTY:Ljava/lang/String; = "RELYING_PARTY"

.field public static final TRANSACTION_STATE_COMPLETE:Ljava/lang/String; = "COMPLETE"

.field public static final TRANSACTION_STATE_RUNNING:Ljava/lang/String; = "RUNNING"

.field private static final serialVersionUID:J = 0x434beb6c2b5e332cL


# instance fields
.field private accountUUID:Ljava/lang/String;

.field private displayText:Ljava/lang/String;

.field private hash:Ljava/lang/String;

.field private hashType:Ljava/lang/String;

.field private requestProperties:Lee/cyber/smartid/dto/jsonrpc/RequestProperties;

.field private resultCode:Ljava/lang/String;

.field private rpName:Ljava/lang/String;

.field private state:Ljava/lang/String;

.field private transactionSource:Ljava/lang/String;

.field private transactionType:Ljava/lang/String;

.field private transactionUUID:Ljava/lang/String;

.field private ttlSec:J


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-wide/16 v0, 0x12c

    .line 2
    iput-wide v0, p0, Lee/cyber/smartid/dto/jsonrpc/Transaction;->ttlSec:J

    const-string v0, "RUNNING"

    .line 3
    iput-object v0, p0, Lee/cyber/smartid/dto/jsonrpc/Transaction;->state:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p0, p1, :cond_0

    return v0

    :cond_0
    const/4 v1, 0x0

    if-eqz p1, :cond_18

    .line 1
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    const-class v3, Lee/cyber/smartid/dto/jsonrpc/Transaction;

    if-eq v3, v2, :cond_1

    goto/16 :goto_b

    .line 2
    :cond_1
    check-cast p1, Lee/cyber/smartid/dto/jsonrpc/Transaction;

    .line 3
    iget-object v2, p0, Lee/cyber/smartid/dto/jsonrpc/Transaction;->accountUUID:Ljava/lang/String;

    if-eqz v2, :cond_2

    iget-object v3, p1, Lee/cyber/smartid/dto/jsonrpc/Transaction;->accountUUID:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    goto :goto_0

    :cond_2
    iget-object v2, p1, Lee/cyber/smartid/dto/jsonrpc/Transaction;->accountUUID:Ljava/lang/String;

    if-eqz v2, :cond_3

    :goto_0
    return v1

    .line 4
    :cond_3
    iget-object v2, p0, Lee/cyber/smartid/dto/jsonrpc/Transaction;->transactionUUID:Ljava/lang/String;

    if-eqz v2, :cond_4

    iget-object v3, p1, Lee/cyber/smartid/dto/jsonrpc/Transaction;->transactionUUID:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    goto :goto_1

    :cond_4
    iget-object v2, p1, Lee/cyber/smartid/dto/jsonrpc/Transaction;->transactionUUID:Ljava/lang/String;

    if-eqz v2, :cond_5

    :goto_1
    return v1

    .line 5
    :cond_5
    iget-object v2, p0, Lee/cyber/smartid/dto/jsonrpc/Transaction;->rpName:Ljava/lang/String;

    if-eqz v2, :cond_6

    iget-object v3, p1, Lee/cyber/smartid/dto/jsonrpc/Transaction;->rpName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    goto :goto_2

    :cond_6
    iget-object v2, p1, Lee/cyber/smartid/dto/jsonrpc/Transaction;->rpName:Ljava/lang/String;

    if-eqz v2, :cond_7

    :goto_2
    return v1

    .line 6
    :cond_7
    iget-object v2, p0, Lee/cyber/smartid/dto/jsonrpc/Transaction;->transactionType:Ljava/lang/String;

    if-eqz v2, :cond_8

    iget-object v3, p1, Lee/cyber/smartid/dto/jsonrpc/Transaction;->transactionType:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    goto :goto_3

    :cond_8
    iget-object v2, p1, Lee/cyber/smartid/dto/jsonrpc/Transaction;->transactionType:Ljava/lang/String;

    if-eqz v2, :cond_9

    :goto_3
    return v1

    .line 7
    :cond_9
    iget-object v2, p0, Lee/cyber/smartid/dto/jsonrpc/Transaction;->transactionSource:Ljava/lang/String;

    if-eqz v2, :cond_a

    iget-object v3, p1, Lee/cyber/smartid/dto/jsonrpc/Transaction;->transactionSource:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_b

    goto :goto_4

    :cond_a
    iget-object v2, p1, Lee/cyber/smartid/dto/jsonrpc/Transaction;->transactionSource:Ljava/lang/String;

    if-eqz v2, :cond_b

    :goto_4
    return v1

    .line 8
    :cond_b
    iget-object v2, p0, Lee/cyber/smartid/dto/jsonrpc/Transaction;->state:Ljava/lang/String;

    if-eqz v2, :cond_c

    iget-object v3, p1, Lee/cyber/smartid/dto/jsonrpc/Transaction;->state:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_d

    goto :goto_5

    :cond_c
    iget-object v2, p1, Lee/cyber/smartid/dto/jsonrpc/Transaction;->state:Ljava/lang/String;

    if-eqz v2, :cond_d

    :goto_5
    return v1

    .line 9
    :cond_d
    iget-object v2, p0, Lee/cyber/smartid/dto/jsonrpc/Transaction;->resultCode:Ljava/lang/String;

    if-eqz v2, :cond_e

    iget-object v3, p1, Lee/cyber/smartid/dto/jsonrpc/Transaction;->resultCode:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_f

    goto :goto_6

    :cond_e
    iget-object v2, p1, Lee/cyber/smartid/dto/jsonrpc/Transaction;->resultCode:Ljava/lang/String;

    if-eqz v2, :cond_f

    :goto_6
    return v1

    .line 10
    :cond_f
    iget-object v2, p0, Lee/cyber/smartid/dto/jsonrpc/Transaction;->hash:Ljava/lang/String;

    if-eqz v2, :cond_10

    iget-object v3, p1, Lee/cyber/smartid/dto/jsonrpc/Transaction;->hash:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_11

    goto :goto_7

    :cond_10
    iget-object v2, p1, Lee/cyber/smartid/dto/jsonrpc/Transaction;->hash:Ljava/lang/String;

    if-eqz v2, :cond_11

    :goto_7
    return v1

    .line 11
    :cond_11
    iget-object v2, p0, Lee/cyber/smartid/dto/jsonrpc/Transaction;->hashType:Ljava/lang/String;

    if-eqz v2, :cond_12

    iget-object v3, p1, Lee/cyber/smartid/dto/jsonrpc/Transaction;->hashType:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_13

    goto :goto_8

    :cond_12
    iget-object v2, p1, Lee/cyber/smartid/dto/jsonrpc/Transaction;->hashType:Ljava/lang/String;

    if-eqz v2, :cond_13

    :goto_8
    return v1

    .line 12
    :cond_13
    iget-object v2, p0, Lee/cyber/smartid/dto/jsonrpc/Transaction;->requestProperties:Lee/cyber/smartid/dto/jsonrpc/RequestProperties;

    if-eqz v2, :cond_14

    iget-object v3, p1, Lee/cyber/smartid/dto/jsonrpc/Transaction;->requestProperties:Lee/cyber/smartid/dto/jsonrpc/RequestProperties;

    invoke-virtual {v2, v3}, Lee/cyber/smartid/dto/jsonrpc/RequestProperties;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_15

    goto :goto_9

    :cond_14
    iget-object v2, p1, Lee/cyber/smartid/dto/jsonrpc/Transaction;->requestProperties:Lee/cyber/smartid/dto/jsonrpc/RequestProperties;

    if-eqz v2, :cond_15

    :goto_9
    return v1

    .line 13
    :cond_15
    iget-object v2, p0, Lee/cyber/smartid/dto/jsonrpc/Transaction;->displayText:Ljava/lang/String;

    if-eqz v2, :cond_16

    iget-object p1, p1, Lee/cyber/smartid/dto/jsonrpc/Transaction;->displayText:Ljava/lang/String;

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_a

    :cond_16
    iget-object p1, p1, Lee/cyber/smartid/dto/jsonrpc/Transaction;->displayText:Ljava/lang/String;

    if-nez p1, :cond_17

    goto :goto_a

    :cond_17
    const/4 v0, 0x0

    :goto_a
    return v0

    :cond_18
    :goto_b
    return v1
.end method

.method public getAccountUUID()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/dto/jsonrpc/Transaction;->accountUUID:Ljava/lang/String;

    return-object v0
.end method

.method public getDisplayText()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/dto/jsonrpc/Transaction;->displayText:Ljava/lang/String;

    return-object v0
.end method

.method public getHash()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/dto/jsonrpc/Transaction;->hash:Ljava/lang/String;

    return-object v0
.end method

.method public getHashType()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/dto/jsonrpc/Transaction;->hashType:Ljava/lang/String;

    return-object v0
.end method

.method public getRequestProperties()Lee/cyber/smartid/dto/jsonrpc/RequestProperties;
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/dto/jsonrpc/Transaction;->requestProperties:Lee/cyber/smartid/dto/jsonrpc/RequestProperties;

    return-object v0
.end method

.method public getResultCode()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/dto/jsonrpc/Transaction;->resultCode:Ljava/lang/String;

    return-object v0
.end method

.method public getRpName()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/dto/jsonrpc/Transaction;->rpName:Ljava/lang/String;

    return-object v0
.end method

.method public getState()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/dto/jsonrpc/Transaction;->state:Ljava/lang/String;

    return-object v0
.end method

.method public getTransactionSource()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/dto/jsonrpc/Transaction;->transactionSource:Ljava/lang/String;

    return-object v0
.end method

.method public getTransactionType()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/dto/jsonrpc/Transaction;->transactionType:Ljava/lang/String;

    return-object v0
.end method

.method public getTransactionUUID()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/dto/jsonrpc/Transaction;->transactionUUID:Ljava/lang/String;

    return-object v0
.end method

.method public getTtlSec()J
    .locals 2

    .line 1
    iget-wide v0, p0, Lee/cyber/smartid/dto/jsonrpc/Transaction;->ttlSec:J

    return-wide v0
.end method

.method public hashCode()I
    .locals 3

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/dto/jsonrpc/Transaction;->accountUUID:Ljava/lang/String;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    .line 2
    iget-object v2, p0, Lee/cyber/smartid/dto/jsonrpc/Transaction;->transactionUUID:Ljava/lang/String;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    .line 3
    iget-object v2, p0, Lee/cyber/smartid/dto/jsonrpc/Transaction;->rpName:Ljava/lang/String;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    .line 4
    iget-object v2, p0, Lee/cyber/smartid/dto/jsonrpc/Transaction;->transactionType:Ljava/lang/String;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_3

    :cond_3
    const/4 v2, 0x0

    :goto_3
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    .line 5
    iget-object v2, p0, Lee/cyber/smartid/dto/jsonrpc/Transaction;->transactionSource:Ljava/lang/String;

    if-eqz v2, :cond_4

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_4

    :cond_4
    const/4 v2, 0x0

    :goto_4
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    .line 6
    iget-object v2, p0, Lee/cyber/smartid/dto/jsonrpc/Transaction;->state:Ljava/lang/String;

    if-eqz v2, :cond_5

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_5

    :cond_5
    const/4 v2, 0x0

    :goto_5
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    .line 7
    iget-object v2, p0, Lee/cyber/smartid/dto/jsonrpc/Transaction;->resultCode:Ljava/lang/String;

    if-eqz v2, :cond_6

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_6

    :cond_6
    const/4 v2, 0x0

    :goto_6
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    .line 8
    iget-object v2, p0, Lee/cyber/smartid/dto/jsonrpc/Transaction;->hash:Ljava/lang/String;

    if-eqz v2, :cond_7

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_7

    :cond_7
    const/4 v2, 0x0

    :goto_7
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    .line 9
    iget-object v2, p0, Lee/cyber/smartid/dto/jsonrpc/Transaction;->hashType:Ljava/lang/String;

    if-eqz v2, :cond_8

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_8

    :cond_8
    const/4 v2, 0x0

    :goto_8
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    .line 10
    iget-object v2, p0, Lee/cyber/smartid/dto/jsonrpc/Transaction;->displayText:Ljava/lang/String;

    if-eqz v2, :cond_9

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_9

    :cond_9
    const/4 v2, 0x0

    :goto_9
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    .line 11
    iget-object v2, p0, Lee/cyber/smartid/dto/jsonrpc/Transaction;->displayText:Ljava/lang/String;

    if-eqz v2, :cond_a

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_a

    :cond_a
    const/4 v2, 0x0

    :goto_a
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    .line 12
    iget-object v2, p0, Lee/cyber/smartid/dto/jsonrpc/Transaction;->requestProperties:Lee/cyber/smartid/dto/jsonrpc/RequestProperties;

    if-eqz v2, :cond_b

    invoke-virtual {v2}, Lee/cyber/smartid/dto/jsonrpc/RequestProperties;->hashCode()I

    move-result v1

    :cond_b
    add-int/2addr v0, v1

    return v0
.end method

.method public isMultiCodeVerification()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/dto/jsonrpc/Transaction;->requestProperties:Lee/cyber/smartid/dto/jsonrpc/RequestProperties;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lee/cyber/smartid/dto/jsonrpc/RequestProperties;->isVcChoice()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isValid()Z
    .locals 1

    .line 1
    invoke-virtual {p0}, Lee/cyber/smartid/dto/jsonrpc/Transaction;->getTransactionUUID()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lee/cyber/smartid/dto/jsonrpc/Transaction;->getAccountUUID()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lee/cyber/smartid/dto/jsonrpc/Transaction;->getTransactionType()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lee/cyber/smartid/dto/jsonrpc/Transaction;->getTransactionSource()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lee/cyber/smartid/dto/jsonrpc/Transaction;->getState()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lee/cyber/smartid/dto/jsonrpc/Transaction;->getHash()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lee/cyber/smartid/dto/jsonrpc/Transaction;->getHashType()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Transaction{accountUUID=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lee/cyber/smartid/dto/jsonrpc/Transaction;->accountUUID:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v2, ", transactionUUID=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lee/cyber/smartid/dto/jsonrpc/Transaction;->transactionUUID:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v2, ", rpName=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lee/cyber/smartid/dto/jsonrpc/Transaction;->rpName:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v2, ", transactionType=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lee/cyber/smartid/dto/jsonrpc/Transaction;->transactionType:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v2, ", transactionSource=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lee/cyber/smartid/dto/jsonrpc/Transaction;->transactionSource:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v2, ", state=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lee/cyber/smartid/dto/jsonrpc/Transaction;->state:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v2, ", resultCode=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lee/cyber/smartid/dto/jsonrpc/Transaction;->resultCode:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v2, ", hash=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lee/cyber/smartid/dto/jsonrpc/Transaction;->hash:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v2, ", hashType=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lee/cyber/smartid/dto/jsonrpc/Transaction;->hashType:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v2, ", displayText=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lee/cyber/smartid/dto/jsonrpc/Transaction;->displayText:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v2, ", requestProperties=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lee/cyber/smartid/dto/jsonrpc/Transaction;->requestProperties:Lee/cyber/smartid/dto/jsonrpc/RequestProperties;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v1, ", ttlSec="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lee/cyber/smartid/dto/jsonrpc/Transaction;->ttlSec:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
