.class public Lee/cyber/smartid/dto/upgrade/v6/PreV6AccountState;
.super Ljava/lang/Object;
.source "PreV6AccountState.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = 0x3ac15d67de110925L


# instance fields
.field accountUUID:Ljava/lang/String;

.field authCSRTransactionUUID:Ljava/lang/String;

.field authKeyReference:Ljava/lang/String;

.field authSubmitClientSecondPartState:Ljava/lang/String;

.field identityDataFromRegistration:Lee/cyber/smartid/dto/upgrade/v6/PreV6IdentityData;

.field initiationType:Ljava/lang/String;

.field lastSubmitClientSecondPartError:Lee/cyber/smartid/tse/dto/TSEError;

.field registrationState:Ljava/lang/String;

.field signCSRTransactionUUID:Ljava/lang/String;

.field signKeyReference:Ljava/lang/String;

.field signSubmitClientSecondPartState:Ljava/lang/String;

.field final submitClientSecondPartListenerTags:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 3
    invoke-static {v0}, Ljava/util/Collections;->synchronizedList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lee/cyber/smartid/dto/upgrade/v6/PreV6AccountState;->submitClientSecondPartListenerTags:Ljava/util/List;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/dto/upgrade/v6/PreV6IdentityData;Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/tse/dto/TSEError;)V
    .locals 1

    .line 4
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 5
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 6
    invoke-static {v0}, Ljava/util/Collections;->synchronizedList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lee/cyber/smartid/dto/upgrade/v6/PreV6AccountState;->submitClientSecondPartListenerTags:Ljava/util/List;

    .line 7
    iput-object p1, p0, Lee/cyber/smartid/dto/upgrade/v6/PreV6AccountState;->accountUUID:Ljava/lang/String;

    .line 8
    iput-object p2, p0, Lee/cyber/smartid/dto/upgrade/v6/PreV6AccountState;->registrationState:Ljava/lang/String;

    .line 9
    iput-object p3, p0, Lee/cyber/smartid/dto/upgrade/v6/PreV6AccountState;->authCSRTransactionUUID:Ljava/lang/String;

    .line 10
    iput-object p4, p0, Lee/cyber/smartid/dto/upgrade/v6/PreV6AccountState;->signCSRTransactionUUID:Ljava/lang/String;

    .line 11
    iput-object p5, p0, Lee/cyber/smartid/dto/upgrade/v6/PreV6AccountState;->authKeyReference:Ljava/lang/String;

    .line 12
    iput-object p6, p0, Lee/cyber/smartid/dto/upgrade/v6/PreV6AccountState;->signKeyReference:Ljava/lang/String;

    .line 13
    iput-object p7, p0, Lee/cyber/smartid/dto/upgrade/v6/PreV6AccountState;->initiationType:Ljava/lang/String;

    .line 14
    iput-object p8, p0, Lee/cyber/smartid/dto/upgrade/v6/PreV6AccountState;->identityDataFromRegistration:Lee/cyber/smartid/dto/upgrade/v6/PreV6IdentityData;

    .line 15
    iput-object p9, p0, Lee/cyber/smartid/dto/upgrade/v6/PreV6AccountState;->authSubmitClientSecondPartState:Ljava/lang/String;

    .line 16
    iput-object p10, p0, Lee/cyber/smartid/dto/upgrade/v6/PreV6AccountState;->signSubmitClientSecondPartState:Ljava/lang/String;

    .line 17
    iput-object p11, p0, Lee/cyber/smartid/dto/upgrade/v6/PreV6AccountState;->lastSubmitClientSecondPartError:Lee/cyber/smartid/tse/dto/TSEError;

    return-void
.end method


# virtual methods
.method public addTag(Ljava/lang/String;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/dto/upgrade/v6/PreV6AccountState;->submitClientSecondPartListenerTags:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public getAccountUUID()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/dto/upgrade/v6/PreV6AccountState;->accountUUID:Ljava/lang/String;

    return-object v0
.end method
