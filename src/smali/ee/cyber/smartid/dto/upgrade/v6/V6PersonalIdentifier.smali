.class public Lee/cyber/smartid/dto/upgrade/v6/V6PersonalIdentifier;
.super Ljava/lang/Object;
.source "V6PersonalIdentifier.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field public static final PERSONAL_IDENTIFIER_SCHEME_ETSI_PNO:Ljava/lang/String; = "ETSI_PNO"

.field private static final serialVersionUID:J = -0x5d8dc07272bbbf60L


# instance fields
.field public issuer:Ljava/lang/String;

.field public personSemanticID:Ljava/lang/String;

.field public scheme:Ljava/lang/String;

.field public value:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static fromPreV6(Lee/cyber/smartid/dto/upgrade/v6/PreV6GovID;)Lee/cyber/smartid/dto/upgrade/v6/V6PersonalIdentifier;
    .locals 3

    .line 1
    new-instance v0, Lee/cyber/smartid/dto/upgrade/v6/V6PersonalIdentifier;

    invoke-direct {v0}, Lee/cyber/smartid/dto/upgrade/v6/V6PersonalIdentifier;-><init>()V

    .line 2
    iget-object v1, p0, Lee/cyber/smartid/dto/upgrade/v6/PreV6GovID;->country:Ljava/lang/String;

    iput-object v1, v0, Lee/cyber/smartid/dto/upgrade/v6/V6PersonalIdentifier;->issuer:Ljava/lang/String;

    .line 3
    iget-object v2, p0, Lee/cyber/smartid/dto/upgrade/v6/PreV6GovID;->code:Ljava/lang/String;

    iput-object v2, v0, Lee/cyber/smartid/dto/upgrade/v6/V6PersonalIdentifier;->value:Ljava/lang/String;

    const-string v2, "ETSI_PNO"

    .line 4
    iput-object v2, v0, Lee/cyber/smartid/dto/upgrade/v6/V6PersonalIdentifier;->scheme:Ljava/lang/String;

    .line 5
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lee/cyber/smartid/dto/upgrade/v6/PreV6GovID;->code:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 6
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "PNO"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lee/cyber/smartid/dto/upgrade/v6/PreV6GovID;->country:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "-"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p0, p0, Lee/cyber/smartid/dto/upgrade/v6/PreV6GovID;->code:Ljava/lang/String;

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    iput-object p0, v0, Lee/cyber/smartid/dto/upgrade/v6/V6PersonalIdentifier;->personSemanticID:Ljava/lang/String;

    :cond_0
    return-object v0
.end method
