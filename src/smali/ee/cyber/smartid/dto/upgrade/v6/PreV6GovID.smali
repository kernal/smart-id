.class public Lee/cyber/smartid/dto/upgrade/v6/PreV6GovID;
.super Ljava/lang/Object;
.source "PreV6GovID.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = 0x3c99a09be7b10eb8L


# instance fields
.field code:Ljava/lang/String;

.field country:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3
    iput-object p1, p0, Lee/cyber/smartid/dto/upgrade/v6/PreV6GovID;->country:Ljava/lang/String;

    .line 4
    iput-object p2, p0, Lee/cyber/smartid/dto/upgrade/v6/PreV6GovID;->code:Ljava/lang/String;

    return-void
.end method
