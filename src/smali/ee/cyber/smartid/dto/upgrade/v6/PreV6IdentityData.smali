.class public Lee/cyber/smartid/dto/upgrade/v6/PreV6IdentityData;
.super Ljava/lang/Object;
.source "PreV6IdentityData.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = 0x4060f5767751d8b8L


# instance fields
.field givenName:Ljava/lang/String;

.field govID:Lee/cyber/smartid/dto/upgrade/v6/PreV6GovID;

.field surname:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lee/cyber/smartid/dto/upgrade/v6/PreV6GovID;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Lee/cyber/smartid/dto/upgrade/v6/PreV6IdentityData;->govID:Lee/cyber/smartid/dto/upgrade/v6/PreV6GovID;

    .line 3
    iput-object p2, p0, Lee/cyber/smartid/dto/upgrade/v6/PreV6IdentityData;->surname:Ljava/lang/String;

    .line 4
    iput-object p3, p0, Lee/cyber/smartid/dto/upgrade/v6/PreV6IdentityData;->givenName:Ljava/lang/String;

    return-void
.end method
