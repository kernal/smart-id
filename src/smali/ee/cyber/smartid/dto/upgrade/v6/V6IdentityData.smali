.class public Lee/cyber/smartid/dto/upgrade/v6/V6IdentityData;
.super Ljava/lang/Object;
.source "V6IdentityData.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = 0x4060f5767751d8b8L


# instance fields
.field public givenName:Ljava/lang/String;

.field public id:Lee/cyber/smartid/dto/upgrade/v6/V6PersonalIdentifier;

.field public surname:Ljava/lang/String;


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static fromPreV6(Lee/cyber/smartid/dto/upgrade/v6/PreV6IdentityData;)Lee/cyber/smartid/dto/upgrade/v6/V6IdentityData;
    .locals 2

    .line 1
    new-instance v0, Lee/cyber/smartid/dto/upgrade/v6/V6IdentityData;

    invoke-direct {v0}, Lee/cyber/smartid/dto/upgrade/v6/V6IdentityData;-><init>()V

    .line 2
    iget-object v1, p0, Lee/cyber/smartid/dto/upgrade/v6/PreV6IdentityData;->govID:Lee/cyber/smartid/dto/upgrade/v6/PreV6GovID;

    if-eqz v1, :cond_0

    .line 3
    invoke-static {v1}, Lee/cyber/smartid/dto/upgrade/v6/V6PersonalIdentifier;->fromPreV6(Lee/cyber/smartid/dto/upgrade/v6/PreV6GovID;)Lee/cyber/smartid/dto/upgrade/v6/V6PersonalIdentifier;

    move-result-object v1

    iput-object v1, v0, Lee/cyber/smartid/dto/upgrade/v6/V6IdentityData;->id:Lee/cyber/smartid/dto/upgrade/v6/V6PersonalIdentifier;

    .line 4
    :cond_0
    iget-object v1, p0, Lee/cyber/smartid/dto/upgrade/v6/PreV6IdentityData;->surname:Ljava/lang/String;

    iput-object v1, v0, Lee/cyber/smartid/dto/upgrade/v6/V6IdentityData;->surname:Ljava/lang/String;

    .line 5
    iget-object p0, p0, Lee/cyber/smartid/dto/upgrade/v6/PreV6IdentityData;->givenName:Ljava/lang/String;

    iput-object p0, v0, Lee/cyber/smartid/dto/upgrade/v6/V6IdentityData;->givenName:Ljava/lang/String;

    return-object v0
.end method
