.class public Lee/cyber/smartid/dto/upgrade/v6/V6AccountState;
.super Ljava/lang/Object;
.source "V6AccountState.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = 0x3ac15d67de110925L


# instance fields
.field public accountUUID:Ljava/lang/String;

.field public authCSRTransactionUUID:Ljava/lang/String;

.field public authKeyReference:Ljava/lang/String;

.field public authSubmitClientSecondPartState:Ljava/lang/String;

.field public identityDataFromRegistration:Lee/cyber/smartid/dto/upgrade/v6/V6IdentityData;

.field public initiationType:Ljava/lang/String;

.field public lastSubmitClientSecondPartError:Lee/cyber/smartid/tse/dto/TSEError;

.field public registrationState:Ljava/lang/String;

.field public signCSRTransactionUUID:Ljava/lang/String;

.field public signKeyReference:Ljava/lang/String;

.field public signSubmitClientSecondPartState:Ljava/lang/String;

.field public submitClientSecondPartListenerTags:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 3
    invoke-static {v0}, Ljava/util/Collections;->synchronizedList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lee/cyber/smartid/dto/upgrade/v6/V6AccountState;->submitClientSecondPartListenerTags:Ljava/util/List;

    return-void
.end method

.method public static fromPreV6(Lee/cyber/smartid/dto/upgrade/v6/PreV6AccountState;)Lee/cyber/smartid/dto/upgrade/v6/V6AccountState;
    .locals 2

    .line 1
    new-instance v0, Lee/cyber/smartid/dto/upgrade/v6/V6AccountState;

    invoke-direct {v0}, Lee/cyber/smartid/dto/upgrade/v6/V6AccountState;-><init>()V

    .line 2
    iget-object v1, p0, Lee/cyber/smartid/dto/upgrade/v6/PreV6AccountState;->accountUUID:Ljava/lang/String;

    iput-object v1, v0, Lee/cyber/smartid/dto/upgrade/v6/V6AccountState;->accountUUID:Ljava/lang/String;

    .line 3
    iget-object v1, p0, Lee/cyber/smartid/dto/upgrade/v6/PreV6AccountState;->registrationState:Ljava/lang/String;

    iput-object v1, v0, Lee/cyber/smartid/dto/upgrade/v6/V6AccountState;->registrationState:Ljava/lang/String;

    .line 4
    iget-object v1, p0, Lee/cyber/smartid/dto/upgrade/v6/PreV6AccountState;->authCSRTransactionUUID:Ljava/lang/String;

    iput-object v1, v0, Lee/cyber/smartid/dto/upgrade/v6/V6AccountState;->authCSRTransactionUUID:Ljava/lang/String;

    .line 5
    iget-object v1, p0, Lee/cyber/smartid/dto/upgrade/v6/PreV6AccountState;->signCSRTransactionUUID:Ljava/lang/String;

    iput-object v1, v0, Lee/cyber/smartid/dto/upgrade/v6/V6AccountState;->signCSRTransactionUUID:Ljava/lang/String;

    .line 6
    iget-object v1, p0, Lee/cyber/smartid/dto/upgrade/v6/PreV6AccountState;->authKeyReference:Ljava/lang/String;

    iput-object v1, v0, Lee/cyber/smartid/dto/upgrade/v6/V6AccountState;->authKeyReference:Ljava/lang/String;

    .line 7
    iget-object v1, p0, Lee/cyber/smartid/dto/upgrade/v6/PreV6AccountState;->signKeyReference:Ljava/lang/String;

    iput-object v1, v0, Lee/cyber/smartid/dto/upgrade/v6/V6AccountState;->signKeyReference:Ljava/lang/String;

    .line 8
    iget-object v1, p0, Lee/cyber/smartid/dto/upgrade/v6/PreV6AccountState;->initiationType:Ljava/lang/String;

    iput-object v1, v0, Lee/cyber/smartid/dto/upgrade/v6/V6AccountState;->initiationType:Ljava/lang/String;

    .line 9
    iget-object v1, p0, Lee/cyber/smartid/dto/upgrade/v6/PreV6AccountState;->identityDataFromRegistration:Lee/cyber/smartid/dto/upgrade/v6/PreV6IdentityData;

    if-eqz v1, :cond_0

    .line 10
    invoke-static {v1}, Lee/cyber/smartid/dto/upgrade/v6/V6IdentityData;->fromPreV6(Lee/cyber/smartid/dto/upgrade/v6/PreV6IdentityData;)Lee/cyber/smartid/dto/upgrade/v6/V6IdentityData;

    move-result-object v1

    iput-object v1, v0, Lee/cyber/smartid/dto/upgrade/v6/V6AccountState;->identityDataFromRegistration:Lee/cyber/smartid/dto/upgrade/v6/V6IdentityData;

    .line 11
    :cond_0
    iget-object v1, p0, Lee/cyber/smartid/dto/upgrade/v6/PreV6AccountState;->authSubmitClientSecondPartState:Ljava/lang/String;

    iput-object v1, v0, Lee/cyber/smartid/dto/upgrade/v6/V6AccountState;->authSubmitClientSecondPartState:Ljava/lang/String;

    .line 12
    iget-object v1, p0, Lee/cyber/smartid/dto/upgrade/v6/PreV6AccountState;->signSubmitClientSecondPartState:Ljava/lang/String;

    iput-object v1, v0, Lee/cyber/smartid/dto/upgrade/v6/V6AccountState;->signSubmitClientSecondPartState:Ljava/lang/String;

    .line 13
    iget-object v1, p0, Lee/cyber/smartid/dto/upgrade/v6/PreV6AccountState;->submitClientSecondPartListenerTags:Ljava/util/List;

    iput-object v1, v0, Lee/cyber/smartid/dto/upgrade/v6/V6AccountState;->submitClientSecondPartListenerTags:Ljava/util/List;

    .line 14
    iget-object p0, p0, Lee/cyber/smartid/dto/upgrade/v6/PreV6AccountState;->lastSubmitClientSecondPartError:Lee/cyber/smartid/tse/dto/TSEError;

    iput-object p0, v0, Lee/cyber/smartid/dto/upgrade/v6/V6AccountState;->lastSubmitClientSecondPartError:Lee/cyber/smartid/tse/dto/TSEError;

    return-object v0
.end method
