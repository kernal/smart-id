.class public Lee/cyber/smartid/dto/upgrade/v3/V3TransactionCachedHolder;
.super Ljava/lang/Object;
.source "V3TransactionCachedHolder.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = -0x5a453934cf30b1a4L


# instance fields
.field private createdAt:J

.field private transaction:Lee/cyber/smartid/dto/upgrade/v3/PreV3AndV3Transaction;


# direct methods
.method public constructor <init>(Lee/cyber/smartid/dto/upgrade/v3/PreV3AndV3Transaction;)V
    .locals 2

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lee/cyber/smartid/dto/upgrade/v3/V3TransactionCachedHolder;->createdAt:J

    .line 3
    iput-object p1, p0, Lee/cyber/smartid/dto/upgrade/v3/V3TransactionCachedHolder;->transaction:Lee/cyber/smartid/dto/upgrade/v3/PreV3AndV3Transaction;

    return-void
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 3

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "V3TransactionCachedHolder{createdAt="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lee/cyber/smartid/dto/upgrade/v3/V3TransactionCachedHolder;->createdAt:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ", transaction="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lee/cyber/smartid/dto/upgrade/v3/V3TransactionCachedHolder;->transaction:Lee/cyber/smartid/dto/upgrade/v3/PreV3AndV3Transaction;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
