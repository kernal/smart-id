.class public Lee/cyber/smartid/dto/upgrade/v5/PreMDv2Key;
.super Ljava/lang/Object;
.source "PreMDv2Key.java"


# static fields
.field private static final CURRENT_FORMAT_VERSION:I = 0x3

.field private static final serialVersionUID:J = -0x4d870b227092d1a9L


# instance fields
.field private compositeModulusBits:I

.field private d1Prime:Lee/cyber/smartid/dto/upgrade/v5/PreMDv2ClientShare;

.field private formatVersion:I

.field private keyPinLength:I

.field private n1:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, -0x1

    .line 2
    iput v0, p0, Lee/cyber/smartid/dto/upgrade/v5/PreMDv2Key;->keyPinLength:I

    return-void
.end method


# virtual methods
.method public getCompositeModulusBits()I
    .locals 1

    .line 1
    iget v0, p0, Lee/cyber/smartid/dto/upgrade/v5/PreMDv2Key;->compositeModulusBits:I

    return v0
.end method

.method public getD1Prime()Lee/cyber/smartid/dto/upgrade/v5/PreMDv2ClientShare;
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/dto/upgrade/v5/PreMDv2Key;->d1Prime:Lee/cyber/smartid/dto/upgrade/v5/PreMDv2ClientShare;

    return-object v0
.end method

.method public getKeyPinLength()I
    .locals 1

    .line 1
    iget v0, p0, Lee/cyber/smartid/dto/upgrade/v5/PreMDv2Key;->keyPinLength:I

    return v0
.end method

.method public getN1()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/dto/upgrade/v5/PreMDv2Key;->n1:Ljava/lang/String;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "PreMDv2Key{d1Prime="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lee/cyber/smartid/dto/upgrade/v5/PreMDv2Key;->d1Prime:Lee/cyber/smartid/dto/upgrade/v5/PreMDv2ClientShare;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", n1=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lee/cyber/smartid/dto/upgrade/v5/PreMDv2Key;->n1:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v1, ", compositeModulusBits="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lee/cyber/smartid/dto/upgrade/v5/PreMDv2Key;->compositeModulusBits:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", keyPinLength="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lee/cyber/smartid/dto/upgrade/v5/PreMDv2Key;->keyPinLength:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", formatVersion="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lee/cyber/smartid/dto/upgrade/v5/PreMDv2Key;->formatVersion:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
