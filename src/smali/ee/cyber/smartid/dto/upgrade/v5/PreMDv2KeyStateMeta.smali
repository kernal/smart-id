.class public Lee/cyber/smartid/dto/upgrade/v5/PreMDv2KeyStateMeta;
.super Ljava/lang/Object;
.source "PreMDv2KeyStateMeta.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = 0x40394dcc8b82f8afL


# instance fields
.field private currentRetry:I

.field private formatVersion:I

.field private id:Ljava/lang/String;

.field private keyStatus:Lee/cyber/smartid/dto/upgrade/v5/PreMDv2AccountKeyStatus;

.field private keyStatusTimestamp:J

.field private lastErrorType:I

.field private timestamp:J


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static isKeyLocked(Lee/cyber/smartid/dto/upgrade/v5/PreMDv2AccountKeyStatus;JJ)Z
    .locals 7

    const/4 v0, 0x0

    if-nez p0, :cond_0

    return v0

    .line 2
    :cond_0
    invoke-static {p0}, Lee/cyber/smartid/dto/upgrade/v5/PreMDv2KeyStateMeta;->isKeyLockedPermanently(Lee/cyber/smartid/dto/upgrade/v5/PreMDv2AccountKeyStatus;)Z

    move-result v1

    const/4 v2, 0x1

    if-eqz v1, :cond_1

    return v2

    .line 3
    :cond_1
    invoke-virtual {p0}, Lee/cyber/smartid/dto/upgrade/v5/PreMDv2AccountKeyStatus;->getStatus()Ljava/lang/String;

    move-result-object v1

    const-string v3, "LOCKED"

    invoke-static {v1, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    const-wide/16 v3, 0x0

    cmp-long v1, p1, v3

    if-ltz v1, :cond_2

    invoke-virtual {p0}, Lee/cyber/smartid/dto/upgrade/v5/PreMDv2AccountKeyStatus;->getLockInfo()Lee/cyber/smartid/dto/upgrade/v5/PreMDv2AccountKeyLockInfo;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {p0}, Lee/cyber/smartid/dto/upgrade/v5/PreMDv2AccountKeyStatus;->getLockInfo()Lee/cyber/smartid/dto/upgrade/v5/PreMDv2AccountKeyLockInfo;

    move-result-object p0

    invoke-virtual {p0}, Lee/cyber/smartid/dto/upgrade/v5/PreMDv2AccountKeyLockInfo;->getLockDurationSec()I

    move-result p0

    int-to-long v3, p0

    const-wide/16 v5, 0x3e8

    mul-long v3, v3, v5

    add-long/2addr p1, v3

    cmp-long p0, p1, p3

    if-ltz p0, :cond_2

    return v2

    :cond_2
    return v0
.end method

.method private static isKeyLockedPermanently(Lee/cyber/smartid/dto/upgrade/v5/PreMDv2AccountKeyStatus;)Z
    .locals 4

    const/4 v0, 0x0

    if-nez p0, :cond_0

    return v0

    .line 2
    :cond_0
    invoke-virtual {p0}, Lee/cyber/smartid/dto/upgrade/v5/PreMDv2AccountKeyStatus;->getStatus()Ljava/lang/String;

    move-result-object v1

    const-string v2, "REVOKED"

    invoke-static {v1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    const/4 v2, 0x1

    if-nez v1, :cond_3

    invoke-virtual {p0}, Lee/cyber/smartid/dto/upgrade/v5/PreMDv2AccountKeyStatus;->getStatus()Ljava/lang/String;

    move-result-object v1

    const-string v3, "EXPIRED"

    invoke-static {v1, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    goto :goto_0

    .line 3
    :cond_1
    invoke-virtual {p0}, Lee/cyber/smartid/dto/upgrade/v5/PreMDv2AccountKeyStatus;->getStatus()Ljava/lang/String;

    move-result-object v1

    const-string v3, "LOCKED"

    invoke-static {v1, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {p0}, Lee/cyber/smartid/dto/upgrade/v5/PreMDv2AccountKeyStatus;->getLockInfo()Lee/cyber/smartid/dto/upgrade/v5/PreMDv2AccountKeyLockInfo;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {p0}, Lee/cyber/smartid/dto/upgrade/v5/PreMDv2AccountKeyStatus;->getLockInfo()Lee/cyber/smartid/dto/upgrade/v5/PreMDv2AccountKeyLockInfo;

    move-result-object p0

    invoke-virtual {p0}, Lee/cyber/smartid/dto/upgrade/v5/PreMDv2AccountKeyLockInfo;->getPinAttemptsLeftInTotal()I

    move-result p0

    if-gtz p0, :cond_2

    return v2

    :cond_2
    return v0

    :cond_3
    :goto_0
    return v2
.end method


# virtual methods
.method public isKeyLocked()Z
    .locals 5

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/dto/upgrade/v5/PreMDv2KeyStateMeta;->keyStatus:Lee/cyber/smartid/dto/upgrade/v5/PreMDv2AccountKeyStatus;

    if-eqz v0, :cond_0

    iget-wide v1, p0, Lee/cyber/smartid/dto/upgrade/v5/PreMDv2KeyStateMeta;->keyStatusTimestamp:J

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    invoke-static {v0, v1, v2, v3, v4}, Lee/cyber/smartid/dto/upgrade/v5/PreMDv2KeyStateMeta;->isKeyLocked(Lee/cyber/smartid/dto/upgrade/v5/PreMDv2AccountKeyStatus;JJ)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isKeyLockedPermanently()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/dto/upgrade/v5/PreMDv2KeyStateMeta;->keyStatus:Lee/cyber/smartid/dto/upgrade/v5/PreMDv2AccountKeyStatus;

    if-eqz v0, :cond_0

    invoke-static {v0}, Lee/cyber/smartid/dto/upgrade/v5/PreMDv2KeyStateMeta;->isKeyLockedPermanently(Lee/cyber/smartid/dto/upgrade/v5/PreMDv2AccountKeyStatus;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method
