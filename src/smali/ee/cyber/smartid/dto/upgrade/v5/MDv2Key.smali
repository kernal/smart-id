.class public Lee/cyber/smartid/dto/upgrade/v5/MDv2Key;
.super Ljava/lang/Object;
.source "MDv2Key.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final CURRENT_FORMAT_VERSION:I = 0x4

.field private static final serialVersionUID:J = -0x4d870b227092d1a9L


# instance fields
.field private compositeModulus:Ljava/lang/String;

.field private compositeModulusBits:I

.field private d1Prime:Lee/cyber/smartid/dto/upgrade/v5/PreMDv2AndMDv2ClientShare;

.field private dhDerivedKey:Ljava/lang/String;

.field private dhDerivedKeyId:Ljava/lang/String;

.field private formatVersion:I

.field private keyPinLength:I

.field private n1:Ljava/lang/String;

.field private final szId:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lee/cyber/smartid/dto/upgrade/v5/PreMDv2AndMDv2ClientShare;Ljava/lang/String;ILjava/lang/String;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Lee/cyber/smartid/dto/upgrade/v5/MDv2Key;->d1Prime:Lee/cyber/smartid/dto/upgrade/v5/PreMDv2AndMDv2ClientShare;

    .line 3
    iput-object p2, p0, Lee/cyber/smartid/dto/upgrade/v5/MDv2Key;->n1:Ljava/lang/String;

    .line 4
    iput p3, p0, Lee/cyber/smartid/dto/upgrade/v5/MDv2Key;->keyPinLength:I

    .line 5
    iput-object p4, p0, Lee/cyber/smartid/dto/upgrade/v5/MDv2Key;->szId:Ljava/lang/String;

    const/4 p1, 0x4

    .line 6
    iput p1, p0, Lee/cyber/smartid/dto/upgrade/v5/MDv2Key;->formatVersion:I

    return-void
.end method


# virtual methods
.method public getCompositeModulusBits()I
    .locals 1

    .line 1
    iget v0, p0, Lee/cyber/smartid/dto/upgrade/v5/MDv2Key;->compositeModulusBits:I

    return v0
.end method

.method public getD1Prime()Lee/cyber/smartid/dto/upgrade/v5/PreMDv2AndMDv2ClientShare;
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/dto/upgrade/v5/MDv2Key;->d1Prime:Lee/cyber/smartid/dto/upgrade/v5/PreMDv2AndMDv2ClientShare;

    return-object v0
.end method

.method public getDHDerivedKey()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/dto/upgrade/v5/MDv2Key;->dhDerivedKey:Ljava/lang/String;

    return-object v0
.end method

.method public getDHDerivedKeyBytes(Lee/cyber/smartid/cryptolib/inter/EncodingOp;)[B
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/dto/upgrade/v5/MDv2Key;->dhDerivedKey:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2
    iget-object v0, p0, Lee/cyber/smartid/dto/upgrade/v5/MDv2Key;->dhDerivedKey:Ljava/lang/String;

    invoke-interface {p1, v0}, Lee/cyber/smartid/cryptolib/inter/EncodingOp;->decodeBytesFromBase64(Ljava/lang/String;)[B

    move-result-object p1

    return-object p1

    :cond_0
    const/4 p1, 0x0

    return-object p1
.end method

.method public getDhDerivedKeyId()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/dto/upgrade/v5/MDv2Key;->dhDerivedKeyId:Ljava/lang/String;

    return-object v0
.end method

.method public getKeyPinLength()I
    .locals 1

    .line 1
    iget v0, p0, Lee/cyber/smartid/dto/upgrade/v5/MDv2Key;->keyPinLength:I

    return v0
.end method

.method public getKeySize()I
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/dto/upgrade/v5/MDv2Key;->d1Prime:Lee/cyber/smartid/dto/upgrade/v5/PreMDv2AndMDv2ClientShare;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return v0

    .line 2
    :cond_0
    invoke-virtual {v0}, Lee/cyber/smartid/dto/upgrade/v5/PreMDv2AndMDv2ClientShare;->getKeySize()I

    move-result v0

    return v0
.end method

.method public getN1()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/dto/upgrade/v5/MDv2Key;->n1:Ljava/lang/String;

    return-object v0
.end method

.method public getSZId()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/dto/upgrade/v5/MDv2Key;->szId:Ljava/lang/String;

    return-object v0
.end method

.method public setCompositeModulus(Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lee/cyber/smartid/dto/upgrade/v5/MDv2Key;->compositeModulus:Ljava/lang/String;

    return-void
.end method

.method public setCompositeModulusBits(I)V
    .locals 0

    .line 1
    iput p1, p0, Lee/cyber/smartid/dto/upgrade/v5/MDv2Key;->compositeModulusBits:I

    return-void
.end method

.method public setDHDerivedKey(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lee/cyber/smartid/dto/upgrade/v5/MDv2Key;->dhDerivedKeyId:Ljava/lang/String;

    .line 2
    iput-object p2, p0, Lee/cyber/smartid/dto/upgrade/v5/MDv2Key;->dhDerivedKey:Ljava/lang/String;

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Key{d1Prime="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lee/cyber/smartid/dto/upgrade/v5/MDv2Key;->d1Prime:Lee/cyber/smartid/dto/upgrade/v5/PreMDv2AndMDv2ClientShare;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", n1=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lee/cyber/smartid/dto/upgrade/v5/MDv2Key;->n1:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v1, ", compositeModulusBits="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lee/cyber/smartid/dto/upgrade/v5/MDv2Key;->compositeModulusBits:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", keyPinLength="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lee/cyber/smartid/dto/upgrade/v5/MDv2Key;->keyPinLength:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", szId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lee/cyber/smartid/dto/upgrade/v5/MDv2Key;->szId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", formatVersion="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lee/cyber/smartid/dto/upgrade/v5/MDv2Key;->formatVersion:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
