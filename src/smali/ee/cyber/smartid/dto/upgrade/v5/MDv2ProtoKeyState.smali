.class public Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;
.super Ljava/lang/Object;
.source "MDv2ProtoKeyState.java"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState$KeyOperationOrigin;,
        Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState$KeyOperationType;
    }
.end annotation


# static fields
.field private static final CURRENT_FORMAT_VERSION:I = 0x3

.field public static final FORMAT_VERSION_1:I = 0x1

.field public static final FORMAT_VERSION_2:I = 0x2

.field public static final FORMAT_VERSION_3:I = 0x3

.field protected static final OPERATION_ORIGIN_NEW:I = 0x1

.field protected static final OPERATION_ORIGIN_NONE:I = 0x0

.field protected static final OPERATION_ORIGIN_RETRY:I = 0x2

.field public static final TYPE_CLONE_DETECTION:I = 0x3

.field public static final TYPE_IDLE:I = 0x0

.field public static final TYPE_SIGN:I = 0x1

.field public static final TYPE_SUBMIT_CLIENT_SECOND_PART:I = 0x4

.field private static final serialVersionUID:J = 0x5430a8a2b923ff9cL


# instance fields
.field protected final accountUUID:Ljava/lang/String;

.field protected clientModulus:Ljava/lang/String;

.field protected clientShareSecondPart:Ljava/lang/String;

.field protected digest:Ljava/lang/String;

.field protected digestAlgorithm:Ljava/lang/String;

.field protected formatVersion:I

.field protected final id:Ljava/lang/String;

.field protected final keyId:Ljava/lang/String;

.field protected keyType:Ljava/lang/String;

.field protected nonce:Ljava/lang/String;

.field protected operationOrigin:I

.field protected signatureShare:Ljava/lang/String;

.field protected transactionUUID:Ljava/lang/String;

.field protected type:I


# direct methods
.method protected constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->id:Ljava/lang/String;

    .line 3
    iput-object p2, p0, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->keyId:Ljava/lang/String;

    .line 4
    iput-object p3, p0, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->accountUUID:Ljava/lang/String;

    return-void
.end method

.method public static forCloneDetection(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;
    .locals 1

    .line 1
    new-instance v0, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;

    invoke-direct {v0, p0, p1, p2}, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    const/4 p0, 0x3

    .line 2
    iput p0, v0, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->type:I

    const/4 p1, 0x1

    .line 3
    iput p1, v0, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->operationOrigin:I

    const/4 p1, 0x0

    .line 4
    iput-object p1, v0, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->transactionUUID:Ljava/lang/String;

    .line 5
    iput-object p4, v0, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->nonce:Ljava/lang/String;

    .line 6
    iput-object p1, v0, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->signatureShare:Ljava/lang/String;

    .line 7
    iput-object p1, v0, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->digest:Ljava/lang/String;

    .line 8
    iput-object p1, v0, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->digestAlgorithm:Ljava/lang/String;

    .line 9
    iput-object p3, v0, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->keyType:Ljava/lang/String;

    .line 10
    iput-object p1, v0, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->clientShareSecondPart:Ljava/lang/String;

    .line 11
    iput-object p1, v0, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->clientModulus:Ljava/lang/String;

    .line 12
    iput p0, v0, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->formatVersion:I

    return-object v0
.end method

.method public static forIdle(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;
    .locals 1

    .line 1
    new-instance v0, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;

    invoke-direct {v0, p0, p1, p2}, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    const/4 p0, 0x0

    .line 2
    iput p0, v0, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->type:I

    .line 3
    iput p0, v0, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->operationOrigin:I

    const/4 p0, 0x0

    .line 4
    iput-object p0, v0, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->transactionUUID:Ljava/lang/String;

    .line 5
    iput-object p0, v0, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->signatureShare:Ljava/lang/String;

    .line 6
    iput-object p0, v0, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->digest:Ljava/lang/String;

    .line 7
    iput-object p0, v0, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->digestAlgorithm:Ljava/lang/String;

    .line 8
    iput-object p3, v0, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->keyType:Ljava/lang/String;

    .line 9
    iput-object p0, v0, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->clientShareSecondPart:Ljava/lang/String;

    .line 10
    iput-object p0, v0, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->clientModulus:Ljava/lang/String;

    const/4 p0, 0x3

    .line 11
    iput p0, v0, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->formatVersion:I

    return-object v0
.end method

.method private static forRetry(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;
    .locals 1

    .line 1
    new-instance v0, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;

    invoke-direct {v0, p0, p1, p2}, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2
    iput p5, v0, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->type:I

    const/4 p0, 0x2

    .line 3
    iput p0, v0, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->operationOrigin:I

    .line 4
    iput-object p3, v0, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->transactionUUID:Ljava/lang/String;

    const/4 p0, 0x0

    .line 5
    iput-object p0, v0, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->nonce:Ljava/lang/String;

    .line 6
    iput-object p0, v0, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->signatureShare:Ljava/lang/String;

    .line 7
    iput-object p0, v0, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->digest:Ljava/lang/String;

    .line 8
    iput-object p0, v0, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->digestAlgorithm:Ljava/lang/String;

    .line 9
    iput-object p4, v0, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->keyType:Ljava/lang/String;

    .line 10
    iput-object p0, v0, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->clientShareSecondPart:Ljava/lang/String;

    .line 11
    iput-object p0, v0, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->clientModulus:Ljava/lang/String;

    const/4 p0, 0x3

    .line 12
    iput p0, v0, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->formatVersion:I

    return-object v0
.end method

.method public static forSign(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;
    .locals 1

    .line 1
    new-instance v0, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;

    invoke-direct {v0, p0, p1, p2}, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    const/4 p0, 0x1

    .line 2
    iput p0, v0, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->type:I

    .line 3
    iput p0, v0, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->operationOrigin:I

    .line 4
    iput-object p5, v0, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->transactionUUID:Ljava/lang/String;

    .line 5
    iput-object p4, v0, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->nonce:Ljava/lang/String;

    .line 6
    iput-object p6, v0, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->signatureShare:Ljava/lang/String;

    .line 7
    iput-object p7, v0, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->digest:Ljava/lang/String;

    .line 8
    iput-object p8, v0, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->digestAlgorithm:Ljava/lang/String;

    .line 9
    iput-object p3, v0, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->keyType:Ljava/lang/String;

    const/4 p0, 0x0

    .line 10
    iput-object p0, v0, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->clientShareSecondPart:Ljava/lang/String;

    .line 11
    iput-object p0, v0, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->clientModulus:Ljava/lang/String;

    const/4 p0, 0x3

    .line 12
    iput p0, v0, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->formatVersion:I

    return-object v0
.end method

.method public static forSubmitClientSecondPart(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;
    .locals 1

    .line 1
    new-instance v0, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;

    invoke-direct {v0, p0, p1, p2}, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    const/4 p0, 0x4

    .line 2
    iput p0, v0, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->type:I

    const/4 p0, 0x1

    .line 3
    iput p0, v0, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->operationOrigin:I

    const/4 p0, 0x0

    .line 4
    iput-object p0, v0, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->transactionUUID:Ljava/lang/String;

    .line 5
    iput-object p4, v0, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->nonce:Ljava/lang/String;

    .line 6
    iput-object p0, v0, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->signatureShare:Ljava/lang/String;

    .line 7
    iput-object p0, v0, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->digest:Ljava/lang/String;

    .line 8
    iput-object p0, v0, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->digestAlgorithm:Ljava/lang/String;

    .line 9
    iput-object p3, v0, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->keyType:Ljava/lang/String;

    .line 10
    iput-object p5, v0, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->clientShareSecondPart:Ljava/lang/String;

    .line 11
    iput-object p6, v0, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->clientModulus:Ljava/lang/String;

    const/4 p0, 0x3

    .line 12
    iput p0, v0, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->formatVersion:I

    return-object v0
.end method

.method public static retryFrom(Lee/cyber/smartid/dto/upgrade/v5/MDv2KeyState;)Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    if-eqz p0, :cond_3

    .line 1
    invoke-virtual {p0}, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->isActiveCloneDetection()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2
    iget-object v1, p0, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->id:Ljava/lang/String;

    iget-object v2, p0, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->keyId:Ljava/lang/String;

    iget-object v3, p0, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->accountUUID:Ljava/lang/String;

    invoke-virtual {p0}, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->getTransactionUUID()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->keyType:Ljava/lang/String;

    const/4 v6, 0x3

    invoke-static/range {v1 .. v6}, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->forRetry(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;

    move-result-object p0

    return-object p0

    .line 3
    :cond_0
    invoke-virtual {p0}, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->isActiveSubmitClientSecondPart()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 4
    iget-object v1, p0, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->id:Ljava/lang/String;

    iget-object v2, p0, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->keyId:Ljava/lang/String;

    iget-object v3, p0, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->accountUUID:Ljava/lang/String;

    invoke-virtual {p0}, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->getTransactionUUID()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->keyType:Ljava/lang/String;

    const/4 v6, 0x4

    invoke-static/range {v1 .. v6}, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->forRetry(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;

    move-result-object p0

    return-object p0

    .line 5
    :cond_1
    invoke-virtual {p0}, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->isActiveSign()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 6
    iget-object v1, p0, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->id:Ljava/lang/String;

    iget-object v2, p0, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->keyId:Ljava/lang/String;

    iget-object v3, p0, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->accountUUID:Ljava/lang/String;

    invoke-virtual {p0}, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->getTransactionUUID()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->keyType:Ljava/lang/String;

    const/4 v6, 0x1

    invoke-static/range {v1 .. v6}, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->forRetry(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;

    move-result-object p0

    return-object p0

    .line 7
    :cond_2
    new-instance p0, Ljava/lang/IllegalArgumentException;

    const-string v0, "Invalid keyState, unable to convert!"

    invoke-direct {p0, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p0

    .line 8
    :cond_3
    new-instance p0, Ljava/lang/IllegalArgumentException;

    const-string v0, "The keyState param can\'t be null!"

    invoke-direct {p0, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p0, p1, :cond_0

    return v0

    .line 1
    :cond_0
    instance-of v1, p1, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 2
    :cond_1
    check-cast p1, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;

    .line 3
    iget v1, p0, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->type:I

    iget v3, p1, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->type:I

    if-eq v1, v3, :cond_2

    return v2

    .line 4
    :cond_2
    iget v1, p0, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->operationOrigin:I

    iget v3, p1, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->operationOrigin:I

    if-eq v1, v3, :cond_3

    return v2

    .line 5
    :cond_3
    iget-object v1, p0, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->id:Ljava/lang/String;

    iget-object v3, p1, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->id:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    return v2

    .line 6
    :cond_4
    iget-object v1, p0, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->keyId:Ljava/lang/String;

    iget-object v3, p1, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->keyId:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    return v2

    .line 7
    :cond_5
    iget-object v1, p0, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->nonce:Ljava/lang/String;

    if-eqz v1, :cond_6

    iget-object v3, p1, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->nonce:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    goto :goto_0

    :cond_6
    iget-object v1, p1, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->nonce:Ljava/lang/String;

    if-eqz v1, :cond_7

    :goto_0
    return v2

    .line 8
    :cond_7
    iget-object v1, p0, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->accountUUID:Ljava/lang/String;

    if-eqz v1, :cond_8

    iget-object v3, p1, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->accountUUID:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_9

    goto :goto_1

    :cond_8
    iget-object v1, p1, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->accountUUID:Ljava/lang/String;

    if-eqz v1, :cond_9

    :goto_1
    return v2

    .line 9
    :cond_9
    iget-object v1, p0, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->transactionUUID:Ljava/lang/String;

    if-eqz v1, :cond_a

    iget-object v3, p1, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->transactionUUID:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_b

    goto :goto_2

    :cond_a
    iget-object v1, p1, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->transactionUUID:Ljava/lang/String;

    if-eqz v1, :cond_b

    :goto_2
    return v2

    .line 10
    :cond_b
    iget-object v1, p0, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->digest:Ljava/lang/String;

    if-eqz v1, :cond_c

    iget-object v3, p1, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->digest:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_d

    goto :goto_3

    :cond_c
    iget-object v1, p1, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->digest:Ljava/lang/String;

    if-eqz v1, :cond_d

    :goto_3
    return v2

    .line 11
    :cond_d
    iget-object v1, p0, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->digestAlgorithm:Ljava/lang/String;

    if-eqz v1, :cond_e

    iget-object v3, p1, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->digestAlgorithm:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_f

    goto :goto_4

    :cond_e
    iget-object v1, p1, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->digestAlgorithm:Ljava/lang/String;

    if-eqz v1, :cond_f

    :goto_4
    return v2

    .line 12
    :cond_f
    iget-object v1, p0, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->signatureShare:Ljava/lang/String;

    if-eqz v1, :cond_10

    iget-object v3, p1, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->signatureShare:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_11

    goto :goto_5

    :cond_10
    iget-object v1, p1, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->signatureShare:Ljava/lang/String;

    if-eqz v1, :cond_11

    :goto_5
    return v2

    .line 13
    :cond_11
    iget-object v1, p0, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->clientShareSecondPart:Ljava/lang/String;

    if-eqz v1, :cond_12

    iget-object v3, p1, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->clientShareSecondPart:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_13

    goto :goto_6

    :cond_12
    iget-object v1, p1, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->clientShareSecondPart:Ljava/lang/String;

    if-eqz v1, :cond_13

    :goto_6
    return v2

    .line 14
    :cond_13
    iget-object v1, p0, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->clientModulus:Ljava/lang/String;

    if-eqz v1, :cond_14

    iget-object v3, p1, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->clientModulus:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_15

    goto :goto_7

    :cond_14
    iget-object v1, p1, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->clientModulus:Ljava/lang/String;

    if-eqz v1, :cond_15

    :goto_7
    return v2

    .line 15
    :cond_15
    iget-object v1, p0, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->keyType:Ljava/lang/String;

    if-eqz v1, :cond_16

    iget-object p1, p1, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->keyType:Ljava/lang/String;

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_8

    :cond_16
    iget-object p1, p1, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->keyType:Ljava/lang/String;

    if-nez p1, :cond_17

    goto :goto_8

    :cond_17
    const/4 v0, 0x0

    :goto_8
    return v0
.end method

.method public getAccountUUID()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->accountUUID:Ljava/lang/String;

    return-object v0
.end method

.method public getClientModulus()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->clientModulus:Ljava/lang/String;

    return-object v0
.end method

.method public getClientShareSecondPart()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->clientShareSecondPart:Ljava/lang/String;

    return-object v0
.end method

.method public getDigest()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->digest:Ljava/lang/String;

    return-object v0
.end method

.method public getDigestAlgorithm()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->digestAlgorithm:Ljava/lang/String;

    return-object v0
.end method

.method public getFormatVersion()I
    .locals 1

    .line 1
    iget v0, p0, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->formatVersion:I

    return v0
.end method

.method public getHumanReadableNameForOperationOrigin()Ljava/lang/String;
    .locals 2

    .line 1
    iget v0, p0, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->operationOrigin:I

    if-eqz v0, :cond_2

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    const-string v0, "OPERATION_ORIGIN_UNKNOWN"

    return-object v0

    :cond_0
    const-string v0, "OPERATION_ORIGIN_RETRY"

    return-object v0

    :cond_1
    const-string v0, "OPERATION_ORIGIN_NEW"

    return-object v0

    :cond_2
    const-string v0, "OPERATION_ORIGIN_NONE"

    return-object v0
.end method

.method public getHumanReadableNameForType()Ljava/lang/String;
    .locals 2

    .line 1
    iget v0, p0, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->type:I

    if-eqz v0, :cond_3

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-eq v0, v1, :cond_1

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    const-string v0, "TYPE_UNKNOWN"

    return-object v0

    :cond_0
    const-string v0, "TYPE_SUBMIT_CLIENT_SECOND_PART"

    return-object v0

    :cond_1
    const-string v0, "TYPE_CLONE_DETECTION"

    return-object v0

    :cond_2
    const-string v0, "TYPE_SIGN"

    return-object v0

    :cond_3
    const-string v0, "TYPE_IDLE"

    return-object v0
.end method

.method public getId()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->id:Ljava/lang/String;

    return-object v0
.end method

.method public getKeyId()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->keyId:Ljava/lang/String;

    return-object v0
.end method

.method public getKeyType()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->keyType:Ljava/lang/String;

    return-object v0
.end method

.method public getNonce()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->nonce:Ljava/lang/String;

    return-object v0
.end method

.method public getSignatureShare()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->signatureShare:Ljava/lang/String;

    return-object v0
.end method

.method public getTransactionUUID()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->transactionUUID:Ljava/lang/String;

    return-object v0
.end method

.method public getType()I
    .locals 1

    .line 1
    iget v0, p0, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->type:I

    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->id:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x1f

    .line 2
    iget-object v1, p0, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->keyId:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    .line 3
    iget v1, p0, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->type:I

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    .line 4
    iget v1, p0, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->operationOrigin:I

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    .line 5
    iget-object v1, p0, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->nonce:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    .line 6
    iget-object v1, p0, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->accountUUID:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    .line 7
    iget-object v1, p0, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->transactionUUID:Ljava/lang/String;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    .line 8
    iget-object v1, p0, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->digest:Ljava/lang/String;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    .line 9
    iget-object v1, p0, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->digestAlgorithm:Ljava/lang/String;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    .line 10
    iget-object v1, p0, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->signatureShare:Ljava/lang/String;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    .line 11
    iget-object v1, p0, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->clientShareSecondPart:Ljava/lang/String;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_6

    :cond_6
    const/4 v1, 0x0

    :goto_6
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    .line 12
    iget-object v1, p0, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->clientModulus:Ljava/lang/String;

    if-eqz v1, :cond_7

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_7

    :cond_7
    const/4 v1, 0x0

    :goto_7
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    .line 13
    iget-object v1, p0, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->keyType:Ljava/lang/String;

    if-eqz v1, :cond_8

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    :cond_8
    add-int/2addr v0, v2

    return v0
.end method

.method public isActiveCloneDetection()Z
    .locals 2

    .line 1
    iget v0, p0, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->type:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isActiveSign()Z
    .locals 2

    .line 1
    iget v0, p0, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->type:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return v1
.end method

.method public isActiveSubmitClientSecondPart()Z
    .locals 2

    .line 1
    iget v0, p0, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->type:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isInActiveState()Z
    .locals 1

    .line 1
    invoke-virtual {p0}, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->isActiveSign()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->isActiveCloneDetection()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->isActiveSubmitClientSecondPart()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method public isRetry()Z
    .locals 2

    .line 1
    iget v0, p0, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->operationOrigin:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public setSignatureShare(Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->signatureShare:Ljava/lang/String;

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ProtoKeyState{id=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v2, ", keyId=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->keyId:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v2, ", type="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v2, p0, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->type:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v2, "("

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2
    invoke-virtual {p0}, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->getHumanReadableNameForType()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, "), operationOrigin="

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v3, p0, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->operationOrigin:I

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 3
    invoke-virtual {p0}, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->getHumanReadableNameForOperationOrigin()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "), accountUUID=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->accountUUID:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v2, ", transactionUUID=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->transactionUUID:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v2, ", keyType=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->keyType:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v1, ", formatVersion="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->formatVersion:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
