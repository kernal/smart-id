.class public Lee/cyber/smartid/dto/upgrade/v5/MDv2KeyState;
.super Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;
.source "MDv2KeyState.java"


# static fields
.field private static final CURRENT_FORMAT_VERSION:I = 0x3

.field public static final FORMAT_VERSION_1:I = 0x1

.field public static final FORMAT_VERSION_2:I = 0x2

.field public static final FORMAT_VERSION_3:I = 0x3

.field private static final serialVersionUID:J = -0x1121499e746e0ebL


# instance fields
.field private oneTimePassword:Ljava/lang/String;

.field private payload:Ljava/lang/String;

.field private payloadEncoding:Ljava/lang/String;


# direct methods
.method protected constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2, p3}, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2
    iput-object p4, p0, Lee/cyber/smartid/dto/upgrade/v5/MDv2KeyState;->oneTimePassword:Ljava/lang/String;

    return-void
.end method

.method public static forCloneDetection(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lee/cyber/smartid/dto/upgrade/v5/MDv2KeyState;
    .locals 1

    .line 1
    new-instance v0, Lee/cyber/smartid/dto/upgrade/v5/MDv2KeyState;

    invoke-direct {v0, p0, p1, p2, p6}, Lee/cyber/smartid/dto/upgrade/v5/MDv2KeyState;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    const/4 p0, 0x3

    .line 2
    iput p0, v0, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->type:I

    const/4 p1, 0x1

    .line 3
    iput p1, v0, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->operationOrigin:I

    const/4 p1, 0x0

    .line 4
    iput-object p1, v0, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->transactionUUID:Ljava/lang/String;

    .line 5
    iput-object p1, v0, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->nonce:Ljava/lang/String;

    .line 6
    iput-object p1, v0, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->signatureShare:Ljava/lang/String;

    .line 7
    iput-object p1, v0, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->digest:Ljava/lang/String;

    .line 8
    iput-object p1, v0, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->digestAlgorithm:Ljava/lang/String;

    .line 9
    iput-object p3, v0, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->keyType:Ljava/lang/String;

    .line 10
    iput-object p4, v0, Lee/cyber/smartid/dto/upgrade/v5/MDv2KeyState;->payload:Ljava/lang/String;

    .line 11
    iput-object p5, v0, Lee/cyber/smartid/dto/upgrade/v5/MDv2KeyState;->payloadEncoding:Ljava/lang/String;

    .line 12
    iput p0, v0, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->formatVersion:I

    return-object v0
.end method

.method public static forIdle(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lee/cyber/smartid/dto/upgrade/v5/MDv2KeyState;
    .locals 1

    .line 1
    new-instance v0, Lee/cyber/smartid/dto/upgrade/v5/MDv2KeyState;

    invoke-direct {v0, p0, p1, p2, p4}, Lee/cyber/smartid/dto/upgrade/v5/MDv2KeyState;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    const/4 p0, 0x0

    .line 2
    iput p0, v0, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->type:I

    .line 3
    iput p0, v0, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->operationOrigin:I

    const/4 p0, 0x0

    .line 4
    iput-object p0, v0, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->transactionUUID:Ljava/lang/String;

    .line 5
    iput-object p0, v0, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->signatureShare:Ljava/lang/String;

    .line 6
    iput-object p0, v0, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->digest:Ljava/lang/String;

    .line 7
    iput-object p0, v0, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->digestAlgorithm:Ljava/lang/String;

    .line 8
    iput-object p3, v0, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->keyType:Ljava/lang/String;

    .line 9
    iput-object p0, v0, Lee/cyber/smartid/dto/upgrade/v5/MDv2KeyState;->payload:Ljava/lang/String;

    .line 10
    iput-object p0, v0, Lee/cyber/smartid/dto/upgrade/v5/MDv2KeyState;->payloadEncoding:Ljava/lang/String;

    const/4 p0, 0x3

    .line 11
    iput p0, v0, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->formatVersion:I

    return-object v0
.end method

.method public static forSign(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lee/cyber/smartid/dto/upgrade/v5/MDv2KeyState;
    .locals 1

    .line 1
    new-instance v0, Lee/cyber/smartid/dto/upgrade/v5/MDv2KeyState;

    invoke-direct {v0, p0, p1, p3, p9}, Lee/cyber/smartid/dto/upgrade/v5/MDv2KeyState;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    const/4 p0, 0x1

    .line 2
    iput p0, v0, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->type:I

    .line 3
    iput p0, v0, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->operationOrigin:I

    .line 4
    iput-object p4, v0, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->transactionUUID:Ljava/lang/String;

    const/4 p0, 0x0

    .line 5
    iput-object p0, v0, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->nonce:Ljava/lang/String;

    .line 6
    iput-object p0, v0, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->signatureShare:Ljava/lang/String;

    .line 7
    iput-object p5, v0, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->digest:Ljava/lang/String;

    .line 8
    iput-object p6, v0, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->digestAlgorithm:Ljava/lang/String;

    .line 9
    iput-object p2, v0, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->keyType:Ljava/lang/String;

    .line 10
    iput-object p7, v0, Lee/cyber/smartid/dto/upgrade/v5/MDv2KeyState;->payload:Ljava/lang/String;

    .line 11
    iput-object p8, v0, Lee/cyber/smartid/dto/upgrade/v5/MDv2KeyState;->payloadEncoding:Ljava/lang/String;

    const/4 p0, 0x3

    .line 12
    iput p0, v0, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->formatVersion:I

    return-object v0
.end method

.method public static forSubmitClientSecondPart(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lee/cyber/smartid/dto/upgrade/v5/MDv2KeyState;
    .locals 1

    .line 1
    new-instance v0, Lee/cyber/smartid/dto/upgrade/v5/MDv2KeyState;

    invoke-direct {v0, p0, p1, p2, p6}, Lee/cyber/smartid/dto/upgrade/v5/MDv2KeyState;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    const/4 p0, 0x4

    .line 2
    iput p0, v0, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->type:I

    const/4 p0, 0x1

    .line 3
    iput p0, v0, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->operationOrigin:I

    const/4 p0, 0x0

    .line 4
    iput-object p0, v0, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->transactionUUID:Ljava/lang/String;

    .line 5
    iput-object p0, v0, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->nonce:Ljava/lang/String;

    .line 6
    iput-object p0, v0, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->signatureShare:Ljava/lang/String;

    .line 7
    iput-object p0, v0, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->digest:Ljava/lang/String;

    .line 8
    iput-object p0, v0, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->digestAlgorithm:Ljava/lang/String;

    .line 9
    iput-object p3, v0, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->keyType:Ljava/lang/String;

    .line 10
    iput-object p0, v0, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->clientShareSecondPart:Ljava/lang/String;

    .line 11
    iput-object p0, v0, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->clientModulus:Ljava/lang/String;

    .line 12
    iput-object p4, v0, Lee/cyber/smartid/dto/upgrade/v5/MDv2KeyState;->payload:Ljava/lang/String;

    .line 13
    iput-object p5, v0, Lee/cyber/smartid/dto/upgrade/v5/MDv2KeyState;->payloadEncoding:Ljava/lang/String;

    const/4 p0, 0x3

    .line 14
    iput p0, v0, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->formatVersion:I

    return-object v0
.end method


# virtual methods
.method public getOneTimePassword()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/dto/upgrade/v5/MDv2KeyState;->oneTimePassword:Ljava/lang/String;

    return-object v0
.end method

.method public getPayload()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/dto/upgrade/v5/MDv2KeyState;->payload:Ljava/lang/String;

    return-object v0
.end method

.method public getPayloadEncoding()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/dto/upgrade/v5/MDv2KeyState;->payloadEncoding:Ljava/lang/String;

    return-object v0
.end method

.method public isRetriableByProto(Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;)Z
    .locals 3

    const/4 v0, 0x0

    if-nez p1, :cond_0

    return v0

    .line 1
    :cond_0
    invoke-virtual {p1}, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->isRetry()Z

    move-result v1

    if-nez v1, :cond_1

    return v0

    .line 2
    :cond_1
    iget v1, p0, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->type:I

    iget v2, p1, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->type:I

    if-eq v1, v2, :cond_2

    return v0

    .line 3
    :cond_2
    iget-object v1, p0, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->id:Ljava/lang/String;

    iget-object v2, p1, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->id:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    return v0

    .line 4
    :cond_3
    iget-object v1, p0, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->accountUUID:Ljava/lang/String;

    if-eqz v1, :cond_4

    iget-object v2, p1, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->accountUUID:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    goto :goto_0

    :cond_4
    iget-object v1, p1, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->accountUUID:Ljava/lang/String;

    if-eqz v1, :cond_5

    :goto_0
    return v0

    .line 5
    :cond_5
    iget-object v1, p0, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->transactionUUID:Ljava/lang/String;

    if-eqz v1, :cond_6

    iget-object v2, p1, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->transactionUUID:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    goto :goto_1

    :cond_6
    iget-object v1, p1, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->transactionUUID:Ljava/lang/String;

    if-eqz v1, :cond_7

    :goto_1
    return v0

    .line 6
    :cond_7
    iget-object v1, p0, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->keyType:Ljava/lang/String;

    if-eqz v1, :cond_8

    iget-object p1, p1, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->keyType:Ljava/lang/String;

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_2

    :cond_8
    iget-object p1, p1, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->keyType:Ljava/lang/String;

    if-nez p1, :cond_9

    const/4 v0, 0x1

    :cond_9
    :goto_2
    return v0
.end method

.method public isRollbackAllowed()Z
    .locals 4

    .line 1
    iget v0, p0, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->type:I

    const/4 v1, 0x0

    if-eqz v0, :cond_2

    const/4 v2, 0x1

    if-eq v0, v2, :cond_1

    const/4 v3, 0x3

    if-eq v0, v3, :cond_1

    const/4 v3, 0x4

    if-eq v0, v3, :cond_0

    return v2

    :cond_0
    return v1

    :cond_1
    return v2

    :cond_2
    return v1
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "KeyState{id=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v1, ", payload exists: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lee/cyber/smartid/dto/upgrade/v5/MDv2KeyState;->payload:Ljava/lang/String;

    .line 2
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, "} "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 3
    invoke-super {p0}, Lee/cyber/smartid/dto/upgrade/v5/MDv2ProtoKeyState;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
