.class public Lee/cyber/smartid/dto/upgrade/v5/PreMDv2AccountKeyStatus;
.super Ljava/lang/Object;
.source "PreMDv2AccountKeyStatus.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field public static final ACCOUNT_KEY_STATUS_EXPIRED:Ljava/lang/String; = "EXPIRED"

.field public static final ACCOUNT_KEY_STATUS_IN_PREPARATION:Ljava/lang/String; = "IN_PREPARATION"

.field public static final ACCOUNT_KEY_STATUS_LOCKED:Ljava/lang/String; = "LOCKED"

.field public static final ACCOUNT_KEY_STATUS_OK:Ljava/lang/String; = "OK"

.field public static final ACCOUNT_KEY_STATUS_REVOKED:Ljava/lang/String; = "REVOKED"

.field public static final ACCOUNT_KEY_TYPE_AUTHENTICATION:Ljava/lang/String; = "AUTHENTICATION"

.field public static final ACCOUNT_KEY_TYPE_SIGNATURE:Ljava/lang/String; = "SIGNATURE"

.field private static final serialVersionUID:J = 0x1f6b531b08a566dL


# instance fields
.field private certificate:Lee/cyber/smartid/dto/upgrade/v5/PreMDv2AccountKeyCertificate;

.field private documentCreationTime:Ljava/lang/String;

.field private documentNumber:Ljava/lang/String;

.field private keyType:Ljava/lang/String;

.field private lockInfo:Lee/cyber/smartid/dto/upgrade/v5/PreMDv2AccountKeyLockInfo;

.field private status:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/dto/upgrade/v5/PreMDv2AccountKeyLockInfo;Lee/cyber/smartid/dto/upgrade/v5/PreMDv2AccountKeyCertificate;)V
    .locals 0

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3
    iput-object p1, p0, Lee/cyber/smartid/dto/upgrade/v5/PreMDv2AccountKeyStatus;->keyType:Ljava/lang/String;

    .line 4
    iput-object p2, p0, Lee/cyber/smartid/dto/upgrade/v5/PreMDv2AccountKeyStatus;->status:Ljava/lang/String;

    .line 5
    iput-object p3, p0, Lee/cyber/smartid/dto/upgrade/v5/PreMDv2AccountKeyStatus;->documentNumber:Ljava/lang/String;

    .line 6
    iput-object p4, p0, Lee/cyber/smartid/dto/upgrade/v5/PreMDv2AccountKeyStatus;->documentCreationTime:Ljava/lang/String;

    .line 7
    iput-object p5, p0, Lee/cyber/smartid/dto/upgrade/v5/PreMDv2AccountKeyStatus;->lockInfo:Lee/cyber/smartid/dto/upgrade/v5/PreMDv2AccountKeyLockInfo;

    .line 8
    iput-object p6, p0, Lee/cyber/smartid/dto/upgrade/v5/PreMDv2AccountKeyStatus;->certificate:Lee/cyber/smartid/dto/upgrade/v5/PreMDv2AccountKeyCertificate;

    return-void
.end method


# virtual methods
.method public getCertificate()Lee/cyber/smartid/dto/upgrade/v5/PreMDv2AccountKeyCertificate;
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/dto/upgrade/v5/PreMDv2AccountKeyStatus;->certificate:Lee/cyber/smartid/dto/upgrade/v5/PreMDv2AccountKeyCertificate;

    return-object v0
.end method

.method public getDocumentCreationTime()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/dto/upgrade/v5/PreMDv2AccountKeyStatus;->documentCreationTime:Ljava/lang/String;

    return-object v0
.end method

.method public getDocumentNumber()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/dto/upgrade/v5/PreMDv2AccountKeyStatus;->documentNumber:Ljava/lang/String;

    return-object v0
.end method

.method public getKeyType()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/dto/upgrade/v5/PreMDv2AccountKeyStatus;->keyType:Ljava/lang/String;

    return-object v0
.end method

.method public getLockInfo()Lee/cyber/smartid/dto/upgrade/v5/PreMDv2AccountKeyLockInfo;
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/dto/upgrade/v5/PreMDv2AccountKeyStatus;->lockInfo:Lee/cyber/smartid/dto/upgrade/v5/PreMDv2AccountKeyLockInfo;

    return-object v0
.end method

.method public getStatus()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/dto/upgrade/v5/PreMDv2AccountKeyStatus;->status:Ljava/lang/String;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "PreMDv2AccountKeyStatus{keyType=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lee/cyber/smartid/dto/upgrade/v5/PreMDv2AccountKeyStatus;->keyType:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v2, ", status=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lee/cyber/smartid/dto/upgrade/v5/PreMDv2AccountKeyStatus;->status:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v2, ", documentNumber=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lee/cyber/smartid/dto/upgrade/v5/PreMDv2AccountKeyStatus;->documentNumber:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v2, ", documentCreationTime=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lee/cyber/smartid/dto/upgrade/v5/PreMDv2AccountKeyStatus;->documentCreationTime:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v1, ", lockInfo="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lee/cyber/smartid/dto/upgrade/v5/PreMDv2AccountKeyStatus;->lockInfo:Lee/cyber/smartid/dto/upgrade/v5/PreMDv2AccountKeyLockInfo;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", certificate="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lee/cyber/smartid/dto/upgrade/v5/PreMDv2AccountKeyStatus;->certificate:Lee/cyber/smartid/dto/upgrade/v5/PreMDv2AccountKeyCertificate;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
