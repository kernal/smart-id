.class public Lee/cyber/smartid/dto/upgrade/v5/PreMDv2KeyState;
.super Ljava/lang/Object;
.source "PreMDv2KeyState.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = -0x1121499e746e0ebL


# instance fields
.field protected accountUUID:Ljava/lang/String;

.field protected formatVersion:I

.field protected id:Ljava/lang/String;

.field protected keyType:Ljava/lang/String;

.field protected nonce:Ljava/lang/String;

.field private oneTimePassword:Ljava/lang/String;

.field protected signatureShare:Ljava/lang/String;

.field protected signatureShareEncoding:Ljava/lang/String;

.field protected transaction:Lee/cyber/smartid/dto/jsonrpc/Transaction;

.field protected type:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getOneTimePassword()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/dto/upgrade/v5/PreMDv2KeyState;->oneTimePassword:Ljava/lang/String;

    return-object v0
.end method
