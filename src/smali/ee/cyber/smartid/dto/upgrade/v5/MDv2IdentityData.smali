.class public Lee/cyber/smartid/dto/upgrade/v5/MDv2IdentityData;
.super Ljava/lang/Object;
.source "MDv2IdentityData.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = 0x4060f5767751d8b8L


# instance fields
.field private givenName:Ljava/lang/String;

.field private govID:Lee/cyber/smartid/dto/upgrade/v5/MDv2GovID;

.field private surname:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getGivenName()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/dto/upgrade/v5/MDv2IdentityData;->givenName:Ljava/lang/String;

    return-object v0
.end method

.method public getGovID()Lee/cyber/smartid/dto/upgrade/v5/MDv2GovID;
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/dto/upgrade/v5/MDv2IdentityData;->govID:Lee/cyber/smartid/dto/upgrade/v5/MDv2GovID;

    return-object v0
.end method

.method public getSurname()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/dto/upgrade/v5/MDv2IdentityData;->surname:Ljava/lang/String;

    return-object v0
.end method

.method public setGivenName(Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lee/cyber/smartid/dto/upgrade/v5/MDv2IdentityData;->givenName:Ljava/lang/String;

    return-void
.end method

.method public setGovID(Lee/cyber/smartid/dto/upgrade/v5/MDv2GovID;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lee/cyber/smartid/dto/upgrade/v5/MDv2IdentityData;->govID:Lee/cyber/smartid/dto/upgrade/v5/MDv2GovID;

    return-void
.end method

.method public setSurname(Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lee/cyber/smartid/dto/upgrade/v5/MDv2IdentityData;->surname:Ljava/lang/String;

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "IdentityData{govID="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lee/cyber/smartid/dto/upgrade/v5/MDv2IdentityData;->govID:Lee/cyber/smartid/dto/upgrade/v5/MDv2GovID;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", surname=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lee/cyber/smartid/dto/upgrade/v5/MDv2IdentityData;->surname:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v2, ", givenName=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lee/cyber/smartid/dto/upgrade/v5/MDv2IdentityData;->givenName:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
