.class public Lee/cyber/smartid/dto/upgrade/v2/V2ProtoKeyState;
.super Ljava/lang/Object;
.source "V2ProtoKeyState.java"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lee/cyber/smartid/dto/upgrade/v2/V2ProtoKeyState$KeyOperationType;
    }
.end annotation


# static fields
.field private static final CURRENT_FORMAT_VERSION:I = 0x1

.field public static final TYPE_CLONE_DETECTION:I = 0x3

.field public static final TYPE_IDLE:I = 0x0

.field public static final TYPE_SIGN:I = 0x1

.field private static final serialVersionUID:J = 0x5430a8a2b923ff9cL


# instance fields
.field protected final accountUUID:Ljava/lang/String;

.field protected formatVersion:I

.field protected final id:Ljava/lang/String;

.field protected keyType:Ljava/lang/String;

.field protected nonce:Ljava/lang/String;

.field protected signatureShare:Ljava/lang/String;

.field protected signatureShareEncoding:Ljava/lang/String;

.field protected transaction:Lee/cyber/smartid/dto/jsonrpc/Transaction;

.field protected type:I


# direct methods
.method protected constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Lee/cyber/smartid/dto/upgrade/v2/V2ProtoKeyState;->id:Ljava/lang/String;

    .line 3
    iput-object p2, p0, Lee/cyber/smartid/dto/upgrade/v2/V2ProtoKeyState;->accountUUID:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public getAccountUUID()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/dto/upgrade/v2/V2ProtoKeyState;->accountUUID:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lee/cyber/smartid/dto/upgrade/v2/V2ProtoKeyState;->transaction:Lee/cyber/smartid/dto/jsonrpc/Transaction;

    if-eqz v0, :cond_0

    .line 2
    invoke-virtual {v0}, Lee/cyber/smartid/dto/jsonrpc/Transaction;->getAccountUUID()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 3
    :cond_0
    iget-object v0, p0, Lee/cyber/smartid/dto/upgrade/v2/V2ProtoKeyState;->accountUUID:Ljava/lang/String;

    return-object v0
.end method

.method public getFormatVersion()I
    .locals 1

    .line 1
    iget v0, p0, Lee/cyber/smartid/dto/upgrade/v2/V2ProtoKeyState;->formatVersion:I

    return v0
.end method

.method public getId()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/dto/upgrade/v2/V2ProtoKeyState;->id:Ljava/lang/String;

    return-object v0
.end method

.method public getKeyType()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/dto/upgrade/v2/V2ProtoKeyState;->keyType:Ljava/lang/String;

    return-object v0
.end method

.method public getNonce()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/dto/upgrade/v2/V2ProtoKeyState;->nonce:Ljava/lang/String;

    return-object v0
.end method

.method public getSignatureShare()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/dto/upgrade/v2/V2ProtoKeyState;->signatureShare:Ljava/lang/String;

    return-object v0
.end method

.method public getSignatureShareEncoding()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/dto/upgrade/v2/V2ProtoKeyState;->signatureShareEncoding:Ljava/lang/String;

    return-object v0
.end method

.method public getTransaction()Lee/cyber/smartid/dto/jsonrpc/Transaction;
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/dto/upgrade/v2/V2ProtoKeyState;->transaction:Lee/cyber/smartid/dto/jsonrpc/Transaction;

    return-object v0
.end method

.method public getType()I
    .locals 1

    .line 1
    iget v0, p0, Lee/cyber/smartid/dto/upgrade/v2/V2ProtoKeyState;->type:I

    return v0
.end method

.method public isActiveCloneDetection()Z
    .locals 2

    .line 1
    iget v0, p0, Lee/cyber/smartid/dto/upgrade/v2/V2ProtoKeyState;->type:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isActiveSign()Z
    .locals 2

    .line 1
    iget v0, p0, Lee/cyber/smartid/dto/upgrade/v2/V2ProtoKeyState;->type:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return v1
.end method

.method public isForSameTransaction(Lee/cyber/smartid/dto/jsonrpc/Transaction;)Z
    .locals 1

    if-eqz p1, :cond_0

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/dto/upgrade/v2/V2ProtoKeyState;->transaction:Lee/cyber/smartid/dto/jsonrpc/Transaction;

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lee/cyber/smartid/dto/jsonrpc/Transaction;->getTransactionUUID()Ljava/lang/String;

    move-result-object p1

    iget-object v0, p0, Lee/cyber/smartid/dto/upgrade/v2/V2ProtoKeyState;->transaction:Lee/cyber/smartid/dto/jsonrpc/Transaction;

    invoke-virtual {v0}, Lee/cyber/smartid/dto/jsonrpc/Transaction;->getTransactionUUID()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public isInActiveState()Z
    .locals 1

    .line 1
    invoke-virtual {p0}, Lee/cyber/smartid/dto/upgrade/v2/V2ProtoKeyState;->isActiveSign()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lee/cyber/smartid/dto/upgrade/v2/V2ProtoKeyState;->isActiveCloneDetection()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method public setSignatureShare(Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lee/cyber/smartid/dto/upgrade/v2/V2ProtoKeyState;->signatureShare:Ljava/lang/String;

    return-void
.end method

.method public setSignatureShareEncoding(Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lee/cyber/smartid/dto/upgrade/v2/V2ProtoKeyState;->signatureShareEncoding:Ljava/lang/String;

    return-void
.end method

.method public setTransaction(Lee/cyber/smartid/dto/jsonrpc/Transaction;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lee/cyber/smartid/dto/upgrade/v2/V2ProtoKeyState;->transaction:Lee/cyber/smartid/dto/jsonrpc/Transaction;

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "KeyState{id=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lee/cyber/smartid/dto/upgrade/v2/V2ProtoKeyState;->id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v2, ", type="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v2, p0, Lee/cyber/smartid/dto/upgrade/v2/V2ProtoKeyState;->type:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v2, ", nonce=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lee/cyber/smartid/dto/upgrade/v2/V2ProtoKeyState;->nonce:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v2, ", accountUUID=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lee/cyber/smartid/dto/upgrade/v2/V2ProtoKeyState;->accountUUID:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v2, ", transaction="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lee/cyber/smartid/dto/upgrade/v2/V2ProtoKeyState;->transaction:Lee/cyber/smartid/dto/jsonrpc/Transaction;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v2, ", signatureShare=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lee/cyber/smartid/dto/upgrade/v2/V2ProtoKeyState;->signatureShare:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v2, ", signatureShareEncoding=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lee/cyber/smartid/dto/upgrade/v2/V2ProtoKeyState;->signatureShareEncoding:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v2, ", keyType=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lee/cyber/smartid/dto/upgrade/v2/V2ProtoKeyState;->keyType:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v1, ", formatVersion="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lee/cyber/smartid/dto/upgrade/v2/V2ProtoKeyState;->formatVersion:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
