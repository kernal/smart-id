.class public Lee/cyber/smartid/dto/upgrade/v2/V2KeyState;
.super Lee/cyber/smartid/dto/upgrade/v2/V2ProtoKeyState;
.source "V2KeyState.java"


# static fields
.field private static final CURRENT_FORMAT_VERSION:I = 0x1

.field private static final serialVersionUID:J = -0x1121499e746e0ebL


# instance fields
.field private oneTimePassword:Ljava/lang/String;


# direct methods
.method protected constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lee/cyber/smartid/dto/upgrade/v2/V2ProtoKeyState;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public static forCloneDetection(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lee/cyber/smartid/dto/upgrade/v2/V2KeyState;
    .locals 1

    .line 1
    new-instance v0, Lee/cyber/smartid/dto/upgrade/v2/V2KeyState;

    invoke-direct {v0, p0, p1}, Lee/cyber/smartid/dto/upgrade/v2/V2KeyState;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const/4 p0, 0x3

    .line 2
    iput p0, v0, Lee/cyber/smartid/dto/upgrade/v2/V2ProtoKeyState;->type:I

    .line 3
    iput-object p3, v0, Lee/cyber/smartid/dto/upgrade/v2/V2ProtoKeyState;->nonce:Ljava/lang/String;

    const/4 p0, 0x0

    .line 4
    iput-object p0, v0, Lee/cyber/smartid/dto/upgrade/v2/V2ProtoKeyState;->transaction:Lee/cyber/smartid/dto/jsonrpc/Transaction;

    .line 5
    iput-object p0, v0, Lee/cyber/smartid/dto/upgrade/v2/V2ProtoKeyState;->signatureShare:Ljava/lang/String;

    .line 6
    iput-object p0, v0, Lee/cyber/smartid/dto/upgrade/v2/V2ProtoKeyState;->signatureShareEncoding:Ljava/lang/String;

    .line 7
    iput-object p2, v0, Lee/cyber/smartid/dto/upgrade/v2/V2ProtoKeyState;->keyType:Ljava/lang/String;

    .line 8
    iput-object p4, v0, Lee/cyber/smartid/dto/upgrade/v2/V2KeyState;->oneTimePassword:Ljava/lang/String;

    const/4 p0, 0x1

    .line 9
    iput p0, v0, Lee/cyber/smartid/dto/upgrade/v2/V2ProtoKeyState;->formatVersion:I

    return-object v0
.end method

.method public static forIdle(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lee/cyber/smartid/dto/upgrade/v2/V2KeyState;
    .locals 1

    .line 1
    new-instance v0, Lee/cyber/smartid/dto/upgrade/v2/V2KeyState;

    invoke-direct {v0, p0, p1}, Lee/cyber/smartid/dto/upgrade/v2/V2KeyState;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const/4 p0, 0x0

    .line 2
    iput p0, v0, Lee/cyber/smartid/dto/upgrade/v2/V2ProtoKeyState;->type:I

    const/4 p0, 0x0

    .line 3
    iput-object p0, v0, Lee/cyber/smartid/dto/upgrade/v2/V2ProtoKeyState;->transaction:Lee/cyber/smartid/dto/jsonrpc/Transaction;

    .line 4
    iput-object p0, v0, Lee/cyber/smartid/dto/upgrade/v2/V2ProtoKeyState;->signatureShare:Ljava/lang/String;

    .line 5
    iput-object p0, v0, Lee/cyber/smartid/dto/upgrade/v2/V2ProtoKeyState;->signatureShareEncoding:Ljava/lang/String;

    .line 6
    iput-object p2, v0, Lee/cyber/smartid/dto/upgrade/v2/V2ProtoKeyState;->keyType:Ljava/lang/String;

    .line 7
    iput-object p3, v0, Lee/cyber/smartid/dto/upgrade/v2/V2KeyState;->oneTimePassword:Ljava/lang/String;

    const/4 p0, 0x1

    .line 8
    iput p0, v0, Lee/cyber/smartid/dto/upgrade/v2/V2ProtoKeyState;->formatVersion:I

    return-object v0
.end method

.method public static forSign(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/dto/jsonrpc/Transaction;Ljava/lang/String;Ljava/lang/String;)Lee/cyber/smartid/dto/upgrade/v2/V2KeyState;
    .locals 2

    .line 1
    new-instance v0, Lee/cyber/smartid/dto/upgrade/v2/V2KeyState;

    invoke-virtual {p4}, Lee/cyber/smartid/dto/jsonrpc/Transaction;->getAccountUUID()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lee/cyber/smartid/dto/upgrade/v2/V2KeyState;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const/4 p0, 0x1

    .line 2
    iput p0, v0, Lee/cyber/smartid/dto/upgrade/v2/V2ProtoKeyState;->type:I

    .line 3
    iput-object p5, v0, Lee/cyber/smartid/dto/upgrade/v2/V2ProtoKeyState;->nonce:Ljava/lang/String;

    .line 4
    iput-object p4, v0, Lee/cyber/smartid/dto/upgrade/v2/V2ProtoKeyState;->transaction:Lee/cyber/smartid/dto/jsonrpc/Transaction;

    .line 5
    iput-object p2, v0, Lee/cyber/smartid/dto/upgrade/v2/V2ProtoKeyState;->signatureShare:Ljava/lang/String;

    .line 6
    iput-object p3, v0, Lee/cyber/smartid/dto/upgrade/v2/V2ProtoKeyState;->signatureShareEncoding:Ljava/lang/String;

    .line 7
    iput-object p1, v0, Lee/cyber/smartid/dto/upgrade/v2/V2ProtoKeyState;->keyType:Ljava/lang/String;

    .line 8
    iput-object p6, v0, Lee/cyber/smartid/dto/upgrade/v2/V2KeyState;->oneTimePassword:Ljava/lang/String;

    .line 9
    iput p0, v0, Lee/cyber/smartid/dto/upgrade/v2/V2ProtoKeyState;->formatVersion:I

    return-object v0
.end method


# virtual methods
.method public getOneTimePassword()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/dto/upgrade/v2/V2KeyState;->oneTimePassword:Ljava/lang/String;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "KeyState{oneTimePassword=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lee/cyber/smartid/dto/upgrade/v2/V2KeyState;->oneTimePassword:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v1, "} "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2
    invoke-super {p0}, Lee/cyber/smartid/dto/upgrade/v2/V2ProtoKeyState;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
