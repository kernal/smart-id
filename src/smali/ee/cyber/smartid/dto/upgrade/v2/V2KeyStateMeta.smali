.class public Lee/cyber/smartid/dto/upgrade/v2/V2KeyStateMeta;
.super Ljava/lang/Object;
.source "V2KeyStateMeta.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final CURRENT_FORMAT_VERSION:I = 0x2

.field public static final LAST_ERROR_TYPE_NETWORK:I = 0x1

.field public static final LAST_ERROR_TYPE_UNKNOWN:I = 0x0

.field private static final serialVersionUID:J = 0x40394dcc8b82f8afL


# instance fields
.field private currentRetry:I

.field private formatVersion:I

.field private final id:Ljava/lang/String;

.field private keyStatus:Lee/cyber/smartid/dto/upgrade/v2/V2AccountKeyStatus;

.field private keyStatusTimestamp:J

.field private lastErrorType:I

.field private final timestamp:J


# direct methods
.method private constructor <init>(Ljava/lang/String;)V
    .locals 2

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Lee/cyber/smartid/dto/upgrade/v2/V2KeyStateMeta;->id:Ljava/lang/String;

    .line 3
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lee/cyber/smartid/dto/upgrade/v2/V2KeyStateMeta;->timestamp:J

    return-void
.end method

.method public static forActive(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lee/cyber/smartid/dto/upgrade/v2/V2KeyStateMeta;
    .locals 0

    .line 1
    new-instance p1, Lee/cyber/smartid/dto/upgrade/v2/V2KeyStateMeta;

    invoke-direct {p1, p0}, Lee/cyber/smartid/dto/upgrade/v2/V2KeyStateMeta;-><init>(Ljava/lang/String;)V

    const/4 p0, 0x2

    .line 2
    iput p0, p1, Lee/cyber/smartid/dto/upgrade/v2/V2KeyStateMeta;->formatVersion:I

    const/4 p0, 0x0

    .line 3
    iput p0, p1, Lee/cyber/smartid/dto/upgrade/v2/V2KeyStateMeta;->currentRetry:I

    return-object p1
.end method

.method public static forIdle(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lee/cyber/smartid/dto/upgrade/v2/V2KeyStateMeta;
    .locals 0

    .line 1
    new-instance p1, Lee/cyber/smartid/dto/upgrade/v2/V2KeyStateMeta;

    invoke-direct {p1, p0}, Lee/cyber/smartid/dto/upgrade/v2/V2KeyStateMeta;-><init>(Ljava/lang/String;)V

    const/4 p0, 0x2

    .line 2
    iput p0, p1, Lee/cyber/smartid/dto/upgrade/v2/V2KeyStateMeta;->formatVersion:I

    const/4 p0, 0x0

    .line 3
    iput p0, p1, Lee/cyber/smartid/dto/upgrade/v2/V2KeyStateMeta;->currentRetry:I

    return-object p1
.end method


# virtual methods
.method public getCurrentRetry()I
    .locals 1

    .line 1
    iget v0, p0, Lee/cyber/smartid/dto/upgrade/v2/V2KeyStateMeta;->currentRetry:I

    return v0
.end method

.method public getFormatVersion()I
    .locals 1

    .line 1
    iget v0, p0, Lee/cyber/smartid/dto/upgrade/v2/V2KeyStateMeta;->formatVersion:I

    return v0
.end method

.method public getId()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/dto/upgrade/v2/V2KeyStateMeta;->id:Ljava/lang/String;

    return-object v0
.end method

.method public getLastErrorType()I
    .locals 1

    .line 1
    iget v0, p0, Lee/cyber/smartid/dto/upgrade/v2/V2KeyStateMeta;->lastErrorType:I

    return v0
.end method

.method public getTimestamp()J
    .locals 2

    .line 1
    iget-wide v0, p0, Lee/cyber/smartid/dto/upgrade/v2/V2KeyStateMeta;->timestamp:J

    return-wide v0
.end method

.method public isInLongTermRetryMode()Z
    .locals 1

    .line 1
    iget v0, p0, Lee/cyber/smartid/dto/upgrade/v2/V2KeyStateMeta;->currentRetry:I

    if-lez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public setCurrentRetry(I)V
    .locals 0

    .line 1
    iput p1, p0, Lee/cyber/smartid/dto/upgrade/v2/V2KeyStateMeta;->currentRetry:I

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "KeyStateMeta{id=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lee/cyber/smartid/dto/upgrade/v2/V2KeyStateMeta;->id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v1, ", timestamp="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lee/cyber/smartid/dto/upgrade/v2/V2KeyStateMeta;->timestamp:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ", currentRetry="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lee/cyber/smartid/dto/upgrade/v2/V2KeyStateMeta;->currentRetry:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", keyStatusTimestamp="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lee/cyber/smartid/dto/upgrade/v2/V2KeyStateMeta;->keyStatusTimestamp:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ", keyStatus="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lee/cyber/smartid/dto/upgrade/v2/V2KeyStateMeta;->keyStatus:Lee/cyber/smartid/dto/upgrade/v2/V2AccountKeyStatus;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", lastErrorType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lee/cyber/smartid/dto/upgrade/v2/V2KeyStateMeta;->lastErrorType:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", formatVersion="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lee/cyber/smartid/dto/upgrade/v2/V2KeyStateMeta;->formatVersion:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
