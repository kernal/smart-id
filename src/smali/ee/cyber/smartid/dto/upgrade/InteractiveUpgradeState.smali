.class public Lee/cyber/smartid/dto/upgrade/InteractiveUpgradeState;
.super Ljava/lang/Object;
.source "InteractiveUpgradeState.java"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lee/cyber/smartid/dto/upgrade/InteractiveUpgradeState$StateUpgradeToMDv2;
    }
.end annotation


# static fields
.field public static final STATE_UPGRADE_TO_MDV2_FAILED:I = 0x4

.field public static final STATE_UPGRADE_TO_MDV2_NONE:I = 0x0

.field public static final STATE_UPGRADE_TO_MDV2_PENDING:I = 0x1

.field public static final STATE_UPGRADE_TO_MDV2_SUCCESS:I = 0x3

.field private static final serialVersionUID:J = -0x11ac8cc56bb7ad45L


# instance fields
.field private mdv2UpgradeDHKeys:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Lee/cyber/smartid/cryptolib/dto/DiffieHellmanKeyPair;",
            ">;"
        }
    .end annotation
.end field

.field private mdv2UpgradeParams:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Lee/cyber/smartid/dto/jsonrpc/param/UpgradeKeyPairParams;",
            ">;"
        }
    .end annotation
.end field

.field private mdv2UpgradeResult:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Lee/cyber/smartid/dto/jsonrpc/result/UpgradeKeyPairResult;",
            ">;"
        }
    .end annotation
.end field

.field private stateUpgradeToMDv2:I


# direct methods
.method private constructor <init>(I)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput p1, p0, Lee/cyber/smartid/dto/upgrade/InteractiveUpgradeState;->stateUpgradeToMDv2:I

    return-void
.end method

.method private createKeyPairDataStorageId(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "_"

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public static forAllNone()Lee/cyber/smartid/dto/upgrade/InteractiveUpgradeState;
    .locals 2

    .line 1
    new-instance v0, Lee/cyber/smartid/dto/upgrade/InteractiveUpgradeState;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lee/cyber/smartid/dto/upgrade/InteractiveUpgradeState;-><init>(I)V

    return-object v0
.end method

.method public static forAllSuccess()Lee/cyber/smartid/dto/upgrade/InteractiveUpgradeState;
    .locals 2

    .line 1
    new-instance v0, Lee/cyber/smartid/dto/upgrade/InteractiveUpgradeState;

    const/4 v1, 0x3

    invoke-direct {v0, v1}, Lee/cyber/smartid/dto/upgrade/InteractiveUpgradeState;-><init>(I)V

    return-object v0
.end method

.method public static forMDv2Success()Lee/cyber/smartid/dto/upgrade/InteractiveUpgradeState;
    .locals 2

    .line 1
    new-instance v0, Lee/cyber/smartid/dto/upgrade/InteractiveUpgradeState;

    const/4 v1, 0x3

    invoke-direct {v0, v1}, Lee/cyber/smartid/dto/upgrade/InteractiveUpgradeState;-><init>(I)V

    return-object v0
.end method


# virtual methods
.method public getMDv2HDKeyPair(Ljava/lang/String;Ljava/lang/String;)Lee/cyber/smartid/cryptolib/dto/DiffieHellmanKeyPair;
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/dto/upgrade/InteractiveUpgradeState;->mdv2UpgradeDHKeys:Ljava/util/HashMap;

    if-eqz v0, :cond_0

    .line 2
    invoke-direct {p0, p1, p2}, Lee/cyber/smartid/dto/upgrade/InteractiveUpgradeState;->createKeyPairDataStorageId(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lee/cyber/smartid/cryptolib/dto/DiffieHellmanKeyPair;

    return-object p1

    :cond_0
    const/4 p1, 0x0

    return-object p1
.end method

.method public getMDv2UpgradeParams(Ljava/lang/String;Ljava/lang/String;)Lee/cyber/smartid/dto/jsonrpc/param/UpgradeKeyPairParams;
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/dto/upgrade/InteractiveUpgradeState;->mdv2UpgradeParams:Ljava/util/HashMap;

    if-eqz v0, :cond_0

    .line 2
    invoke-direct {p0, p1, p2}, Lee/cyber/smartid/dto/upgrade/InteractiveUpgradeState;->createKeyPairDataStorageId(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lee/cyber/smartid/dto/jsonrpc/param/UpgradeKeyPairParams;

    return-object p1

    :cond_0
    const/4 p1, 0x0

    return-object p1
.end method

.method public getMDv2UpgradeResult(Ljava/lang/String;Ljava/lang/String;)Lee/cyber/smartid/dto/jsonrpc/result/UpgradeKeyPairResult;
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/dto/upgrade/InteractiveUpgradeState;->mdv2UpgradeResult:Ljava/util/HashMap;

    if-eqz v0, :cond_0

    .line 2
    invoke-direct {p0, p1, p2}, Lee/cyber/smartid/dto/upgrade/InteractiveUpgradeState;->createKeyPairDataStorageId(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lee/cyber/smartid/dto/jsonrpc/result/UpgradeKeyPairResult;

    return-object p1

    :cond_0
    const/4 p1, 0x0

    return-object p1
.end method

.method public getStateUpgradeToMDv2()I
    .locals 1

    .line 1
    iget v0, p0, Lee/cyber/smartid/dto/upgrade/InteractiveUpgradeState;->stateUpgradeToMDv2:I

    return v0
.end method

.method public hasMDv2UpgradeDataFor(Ljava/lang/String;[Ljava/lang/String;)Z
    .locals 5

    const/4 v0, 0x0

    if-eqz p2, :cond_5

    .line 1
    array-length v1, p2

    if-nez v1, :cond_0

    goto :goto_1

    .line 2
    :cond_0
    array-length v1, p2

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_4

    aget-object v3, p2, v2

    .line 3
    invoke-virtual {p0, p1, v3}, Lee/cyber/smartid/dto/upgrade/InteractiveUpgradeState;->getMDv2UpgradeParams(Ljava/lang/String;Ljava/lang/String;)Lee/cyber/smartid/dto/jsonrpc/param/UpgradeKeyPairParams;

    move-result-object v4

    if-nez v4, :cond_1

    return v0

    .line 4
    :cond_1
    invoke-virtual {p0, p1, v3}, Lee/cyber/smartid/dto/upgrade/InteractiveUpgradeState;->getMDv2UpgradeResult(Ljava/lang/String;Ljava/lang/String;)Lee/cyber/smartid/dto/jsonrpc/result/UpgradeKeyPairResult;

    move-result-object v4

    if-nez v4, :cond_2

    return v0

    .line 5
    :cond_2
    invoke-virtual {p0, p1, v3}, Lee/cyber/smartid/dto/upgrade/InteractiveUpgradeState;->getMDv2HDKeyPair(Ljava/lang/String;Ljava/lang/String;)Lee/cyber/smartid/cryptolib/dto/DiffieHellmanKeyPair;

    move-result-object v3

    if-nez v3, :cond_3

    return v0

    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_4
    const/4 p1, 0x1

    return p1

    :cond_5
    :goto_1
    return v0
.end method

.method public isUpgradePending()Z
    .locals 2

    .line 1
    iget v0, p0, Lee/cyber/smartid/dto/upgrade/InteractiveUpgradeState;->stateUpgradeToMDv2:I

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public removeAllMDv2UpgradeData()V
    .locals 2

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/dto/upgrade/InteractiveUpgradeState;->mdv2UpgradeParams:Ljava/util/HashMap;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 2
    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 3
    iput-object v1, p0, Lee/cyber/smartid/dto/upgrade/InteractiveUpgradeState;->mdv2UpgradeParams:Ljava/util/HashMap;

    .line 4
    :cond_0
    iget-object v0, p0, Lee/cyber/smartid/dto/upgrade/InteractiveUpgradeState;->mdv2UpgradeResult:Ljava/util/HashMap;

    if-eqz v0, :cond_1

    .line 5
    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 6
    iput-object v1, p0, Lee/cyber/smartid/dto/upgrade/InteractiveUpgradeState;->mdv2UpgradeResult:Ljava/util/HashMap;

    .line 7
    :cond_1
    iget-object v0, p0, Lee/cyber/smartid/dto/upgrade/InteractiveUpgradeState;->mdv2UpgradeDHKeys:Ljava/util/HashMap;

    if-eqz v0, :cond_2

    .line 8
    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 9
    iput-object v1, p0, Lee/cyber/smartid/dto/upgrade/InteractiveUpgradeState;->mdv2UpgradeDHKeys:Ljava/util/HashMap;

    :cond_2
    return-void
.end method

.method public removeMDv2UpgradeData(Ljava/lang/String;[Ljava/lang/String;)V
    .locals 4

    if-eqz p2, :cond_1

    .line 1
    array-length v0, p2

    if-nez v0, :cond_0

    goto :goto_1

    .line 2
    :cond_0
    array-length v0, p2

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_1

    aget-object v2, p2, v1

    const/4 v3, 0x0

    .line 3
    invoke-virtual {p0, p1, v2, v3}, Lee/cyber/smartid/dto/upgrade/InteractiveUpgradeState;->setMDv2UpgradeParams(Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/dto/jsonrpc/param/UpgradeKeyPairParams;)V

    .line 4
    invoke-virtual {p0, p1, v2, v3}, Lee/cyber/smartid/dto/upgrade/InteractiveUpgradeState;->setMDv2UpgradeResult(Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/dto/jsonrpc/result/UpgradeKeyPairResult;)V

    .line 5
    invoke-virtual {p0, p1, v2, v3}, Lee/cyber/smartid/dto/upgrade/InteractiveUpgradeState;->setMDv2HDKeyPair(Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/cryptolib/dto/DiffieHellmanKeyPair;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    :goto_1
    return-void
.end method

.method public setMDv2HDKeyPair(Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/cryptolib/dto/DiffieHellmanKeyPair;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/dto/upgrade/InteractiveUpgradeState;->mdv2UpgradeDHKeys:Ljava/util/HashMap;

    if-nez v0, :cond_0

    .line 2
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lee/cyber/smartid/dto/upgrade/InteractiveUpgradeState;->mdv2UpgradeDHKeys:Ljava/util/HashMap;

    :cond_0
    if-eqz p3, :cond_1

    .line 3
    iget-object v0, p0, Lee/cyber/smartid/dto/upgrade/InteractiveUpgradeState;->mdv2UpgradeDHKeys:Ljava/util/HashMap;

    invoke-direct {p0, p1, p2}, Lee/cyber/smartid/dto/upgrade/InteractiveUpgradeState;->createKeyPairDataStorageId(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1, p3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 4
    :cond_1
    iget-object p3, p0, Lee/cyber/smartid/dto/upgrade/InteractiveUpgradeState;->mdv2UpgradeDHKeys:Ljava/util/HashMap;

    invoke-direct {p0, p1, p2}, Lee/cyber/smartid/dto/upgrade/InteractiveUpgradeState;->createKeyPairDataStorageId(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p3, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :goto_0
    return-void
.end method

.method public setMDv2UpgradeParams(Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/dto/jsonrpc/param/UpgradeKeyPairParams;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/dto/upgrade/InteractiveUpgradeState;->mdv2UpgradeParams:Ljava/util/HashMap;

    if-nez v0, :cond_0

    .line 2
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lee/cyber/smartid/dto/upgrade/InteractiveUpgradeState;->mdv2UpgradeParams:Ljava/util/HashMap;

    :cond_0
    if-eqz p3, :cond_1

    .line 3
    iget-object v0, p0, Lee/cyber/smartid/dto/upgrade/InteractiveUpgradeState;->mdv2UpgradeParams:Ljava/util/HashMap;

    invoke-direct {p0, p1, p2}, Lee/cyber/smartid/dto/upgrade/InteractiveUpgradeState;->createKeyPairDataStorageId(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1, p3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 4
    :cond_1
    iget-object p3, p0, Lee/cyber/smartid/dto/upgrade/InteractiveUpgradeState;->mdv2UpgradeParams:Ljava/util/HashMap;

    invoke-direct {p0, p1, p2}, Lee/cyber/smartid/dto/upgrade/InteractiveUpgradeState;->createKeyPairDataStorageId(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p3, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :goto_0
    return-void
.end method

.method public setMDv2UpgradeResult(Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/dto/jsonrpc/result/UpgradeKeyPairResult;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/dto/upgrade/InteractiveUpgradeState;->mdv2UpgradeResult:Ljava/util/HashMap;

    if-nez v0, :cond_0

    .line 2
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lee/cyber/smartid/dto/upgrade/InteractiveUpgradeState;->mdv2UpgradeResult:Ljava/util/HashMap;

    :cond_0
    if-eqz p3, :cond_1

    .line 3
    iget-object v0, p0, Lee/cyber/smartid/dto/upgrade/InteractiveUpgradeState;->mdv2UpgradeResult:Ljava/util/HashMap;

    invoke-direct {p0, p1, p2}, Lee/cyber/smartid/dto/upgrade/InteractiveUpgradeState;->createKeyPairDataStorageId(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1, p3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 4
    :cond_1
    iget-object p3, p0, Lee/cyber/smartid/dto/upgrade/InteractiveUpgradeState;->mdv2UpgradeResult:Ljava/util/HashMap;

    invoke-direct {p0, p1, p2}, Lee/cyber/smartid/dto/upgrade/InteractiveUpgradeState;->createKeyPairDataStorageId(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p3, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :goto_0
    return-void
.end method

.method public setStateUpgradeToMDv2(I)V
    .locals 0

    .line 1
    iput p1, p0, Lee/cyber/smartid/dto/upgrade/InteractiveUpgradeState;->stateUpgradeToMDv2:I

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "InteractiveUpgradeState{stateUpgradeToMDv2="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lee/cyber/smartid/dto/upgrade/InteractiveUpgradeState;->stateUpgradeToMDv2:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
