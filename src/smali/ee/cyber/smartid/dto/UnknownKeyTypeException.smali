.class public Lee/cyber/smartid/dto/UnknownKeyTypeException;
.super Ljava/io/IOException;
.source "UnknownKeyTypeException.java"


# static fields
.field private static final serialVersionUID:J = -0x6538626f4f26e881L


# instance fields
.field private final errorCode:J


# direct methods
.method public constructor <init>()V
    .locals 2

    const-string v0, "Unknown key type!"

    .line 1
    invoke-direct {p0, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    const-wide/16 v0, 0x409

    .line 2
    iput-wide v0, p0, Lee/cyber/smartid/dto/UnknownKeyTypeException;->errorCode:J

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 2

    .line 3
    invoke-direct {p0, p1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    const-wide/16 v0, 0x409

    .line 4
    iput-wide v0, p0, Lee/cyber/smartid/dto/UnknownKeyTypeException;->errorCode:J

    return-void
.end method


# virtual methods
.method public getErrorCode()J
    .locals 2

    .line 1
    iget-wide v0, p0, Lee/cyber/smartid/dto/UnknownKeyTypeException;->errorCode:J

    return-wide v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "UnknownKeyTypeException{errorCode="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lee/cyber/smartid/dto/UnknownKeyTypeException;->errorCode:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, "} "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2
    invoke-super {p0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
