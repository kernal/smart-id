.class public Lee/cyber/smartid/dto/TransactionCachedHolder;
.super Ljava/lang/Object;
.source "TransactionCachedHolder.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = -0x5a453934cf30b1a4L


# instance fields
.field private createdAt:J

.field private transaction:Lee/cyber/smartid/dto/jsonrpc/Transaction;


# direct methods
.method public constructor <init>(Lee/cyber/smartid/tse/inter/WallClock;Lee/cyber/smartid/dto/jsonrpc/Transaction;)V
    .locals 2

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    invoke-interface {p1}, Lee/cyber/smartid/tse/inter/WallClock;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lee/cyber/smartid/dto/TransactionCachedHolder;->createdAt:J

    .line 3
    iput-object p2, p0, Lee/cyber/smartid/dto/TransactionCachedHolder;->transaction:Lee/cyber/smartid/dto/jsonrpc/Transaction;

    return-void
.end method

.method protected static getExpirationDelay(Lee/cyber/smartid/dto/jsonrpc/Transaction;JJ)J
    .locals 6

    const-wide/16 v0, 0x0

    if-nez p0, :cond_0

    return-wide v0

    .line 2
    :cond_0
    invoke-virtual {p0}, Lee/cyber/smartid/dto/jsonrpc/Transaction;->getTtlSec()J

    move-result-wide v2

    cmp-long p0, v2, v0

    if-gtz p0, :cond_1

    const-wide/16 v2, 0x12c

    :cond_1
    const-wide/16 v4, 0x3e8

    mul-long v2, v2, v4

    add-long/2addr p1, v2

    sub-long p0, p1, p3

    cmp-long p2, p0, v0

    if-lez p2, :cond_2

    goto :goto_0

    :cond_2
    move-wide p0, v0

    :goto_0
    return-wide p0
.end method


# virtual methods
.method public getAccountUUID()Ljava/lang/String;
    .locals 1

    .line 1
    invoke-virtual {p0}, Lee/cyber/smartid/dto/TransactionCachedHolder;->getTransaction()Lee/cyber/smartid/dto/jsonrpc/Transaction;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lee/cyber/smartid/dto/TransactionCachedHolder;->getTransaction()Lee/cyber/smartid/dto/jsonrpc/Transaction;

    move-result-object v0

    invoke-virtual {v0}, Lee/cyber/smartid/dto/jsonrpc/Transaction;->getAccountUUID()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public getCreatedAt()J
    .locals 2

    .line 1
    iget-wide v0, p0, Lee/cyber/smartid/dto/TransactionCachedHolder;->createdAt:J

    return-wide v0
.end method

.method public getExpirationDelay(Lee/cyber/smartid/tse/inter/WallClock;)J
    .locals 5

    .line 1
    invoke-virtual {p0}, Lee/cyber/smartid/dto/TransactionCachedHolder;->getTransaction()Lee/cyber/smartid/dto/jsonrpc/Transaction;

    move-result-object v0

    invoke-virtual {p0}, Lee/cyber/smartid/dto/TransactionCachedHolder;->getCreatedAt()J

    move-result-wide v1

    invoke-interface {p1}, Lee/cyber/smartid/tse/inter/WallClock;->currentTimeMillis()J

    move-result-wide v3

    invoke-static {v0, v1, v2, v3, v4}, Lee/cyber/smartid/dto/TransactionCachedHolder;->getExpirationDelay(Lee/cyber/smartid/dto/jsonrpc/Transaction;JJ)J

    move-result-wide v0

    return-wide v0
.end method

.method public getTransaction()Lee/cyber/smartid/dto/jsonrpc/Transaction;
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/dto/TransactionCachedHolder;->transaction:Lee/cyber/smartid/dto/jsonrpc/Transaction;

    return-object v0
.end method

.method public getTransactionUUID()Ljava/lang/String;
    .locals 1

    .line 1
    invoke-virtual {p0}, Lee/cyber/smartid/dto/TransactionCachedHolder;->getTransaction()Lee/cyber/smartid/dto/jsonrpc/Transaction;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lee/cyber/smartid/dto/TransactionCachedHolder;->getTransaction()Lee/cyber/smartid/dto/jsonrpc/Transaction;

    move-result-object v0

    invoke-virtual {v0}, Lee/cyber/smartid/dto/jsonrpc/Transaction;->getTransactionUUID()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public getTtlSec()J
    .locals 2

    .line 1
    invoke-virtual {p0}, Lee/cyber/smartid/dto/TransactionCachedHolder;->getTransaction()Lee/cyber/smartid/dto/jsonrpc/Transaction;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lee/cyber/smartid/dto/TransactionCachedHolder;->getTransaction()Lee/cyber/smartid/dto/jsonrpc/Transaction;

    move-result-object v0

    invoke-virtual {v0}, Lee/cyber/smartid/dto/jsonrpc/Transaction;->getTtlSec()J

    move-result-wide v0

    goto :goto_0

    :cond_0
    const-wide/16 v0, 0x0

    :goto_0
    return-wide v0
.end method

.method public isExpired(Lee/cyber/smartid/tse/inter/WallClock;)Z
    .locals 4

    .line 1
    invoke-virtual {p0, p1}, Lee/cyber/smartid/dto/TransactionCachedHolder;->getExpirationDelay(Lee/cyber/smartid/tse/inter/WallClock;)J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long p1, v0, v2

    if-gtz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "TransactionCachedHolder{createdAt="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lee/cyber/smartid/dto/TransactionCachedHolder;->createdAt:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ", transaction="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lee/cyber/smartid/dto/TransactionCachedHolder;->transaction:Lee/cyber/smartid/dto/jsonrpc/Transaction;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
