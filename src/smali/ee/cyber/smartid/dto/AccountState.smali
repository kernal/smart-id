.class public Lee/cyber/smartid/dto/AccountState;
.super Ljava/lang/Object;
.source "AccountState.java"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lee/cyber/smartid/dto/AccountState$SubmitClientSecondPartState;,
        Lee/cyber/smartid/dto/AccountState$RegistrationState;
    }
.end annotation


# static fields
.field public static final STATE_REGISTERED:Ljava/lang/String; = "REGISTRATION_STATE_ACCOUNT_REGISTERED"

.field public static final STATE_SUBMIT_CLIENT_SECOND_PART_CONFIRMED:Ljava/lang/String; = "STATE_SUBMIT_CLIENT_SECOND_PART_CONFIRMED"

.field public static final STATE_SUBMIT_CLIENT_SECOND_PART_FAILED:Ljava/lang/String; = "STATE_SUBMIT_CLIENT_SECOND_PART_FAILED"

.field public static final STATE_SUBMIT_CLIENT_SECOND_PART_PENDING:Ljava/lang/String; = "STATE_SUBMIT_CLIENT_SECOND_PART_PENDING"

.field public static final STATE_WAITING_FOR_SUBMIT_CLIENT_2ND_PART:Ljava/lang/String; = "REGISTRATION_STATE_WAITING_FOR_SUBMIT_CLIENT_2ND_PART"

.field private static final serialVersionUID:J = 0x3ac15d67de110925L


# instance fields
.field private accountUUID:Ljava/lang/String;

.field private authCSRTransactionUUID:Ljava/lang/String;

.field private authKeyReference:Ljava/lang/String;

.field private authKeyUUID:Ljava/lang/String;

.field private authSubmitClientSecondPartState:Ljava/lang/String;

.field private identityDataFromRegistration:Lee/cyber/smartid/dto/jsonrpc/IdentityData;

.field initiationType:Ljava/lang/String;

.field private lastSubmitClientSecondPartError:Lee/cyber/smartid/tse/dto/TSEError;

.field private registrationState:Ljava/lang/String;

.field private signCSRTransactionUUID:Ljava/lang/String;

.field private signKeyReference:Ljava/lang/String;

.field private signKeyUUID:Ljava/lang/String;

.field private signSubmitClientSecondPartState:Ljava/lang/String;

.field private final submitClientSecondPartListenerTags:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 3
    invoke-static {v0}, Ljava/util/Collections;->synchronizedList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lee/cyber/smartid/dto/AccountState;->submitClientSecondPartListenerTags:Ljava/util/List;

    .line 4
    iput-object p1, p0, Lee/cyber/smartid/dto/AccountState;->accountUUID:Ljava/lang/String;

    .line 5
    iput-object p2, p0, Lee/cyber/smartid/dto/AccountState;->registrationState:Ljava/lang/String;

    .line 6
    iput-object p3, p0, Lee/cyber/smartid/dto/AccountState;->initiationType:Ljava/lang/String;

    const-string p1, "STATE_SUBMIT_CLIENT_SECOND_PART_PENDING"

    .line 7
    iput-object p1, p0, Lee/cyber/smartid/dto/AccountState;->authSubmitClientSecondPartState:Ljava/lang/String;

    .line 8
    iput-object p1, p0, Lee/cyber/smartid/dto/AccountState;->signSubmitClientSecondPartState:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public addSubmitClientSecondPartListenerTag(Ljava/lang/String;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/dto/AccountState;->submitClientSecondPartListenerTags:Ljava/util/List;

    monitor-enter v0

    .line 2
    :try_start_0
    iget-object v1, p0, Lee/cyber/smartid/dto/AccountState;->submitClientSecondPartListenerTags:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 3
    iget-object v1, p0, Lee/cyber/smartid/dto/AccountState;->submitClientSecondPartListenerTags:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 4
    :cond_0
    monitor-exit v0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method public getAccountUUID()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/dto/AccountState;->accountUUID:Ljava/lang/String;

    return-object v0
.end method

.method public getAuthCSRTransactionUUID()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/dto/AccountState;->authCSRTransactionUUID:Ljava/lang/String;

    return-object v0
.end method

.method public getAuthKeyReference()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/dto/AccountState;->authKeyReference:Ljava/lang/String;

    return-object v0
.end method

.method public getAuthKeyUUID()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/dto/AccountState;->authKeyUUID:Ljava/lang/String;

    return-object v0
.end method

.method public getAuthSubmitClientSecondPartState()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/dto/AccountState;->authSubmitClientSecondPartState:Ljava/lang/String;

    return-object v0
.end method

.method public getIdentityDataFromRegistration()Lee/cyber/smartid/dto/jsonrpc/IdentityData;
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/dto/AccountState;->identityDataFromRegistration:Lee/cyber/smartid/dto/jsonrpc/IdentityData;

    return-object v0
.end method

.method public getInitiationType()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/dto/AccountState;->initiationType:Ljava/lang/String;

    return-object v0
.end method

.method public getKeyUUIDByKeyType(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    const-string v0, "AUTHENTICATION"

    .line 1
    invoke-static {p1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2
    invoke-virtual {p0}, Lee/cyber/smartid/dto/AccountState;->getAuthKeyUUID()Ljava/lang/String;

    move-result-object p1

    return-object p1

    :cond_0
    const-string v0, "SIGNATURE"

    .line 3
    invoke-static {p1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result p1

    if-eqz p1, :cond_1

    .line 4
    invoke-virtual {p0}, Lee/cyber/smartid/dto/AccountState;->getSignKeyUUID()Ljava/lang/String;

    move-result-object p1

    return-object p1

    :cond_1
    const/4 p1, 0x0

    return-object p1
.end method

.method public getLastSubmitClientSecondPartError()Lee/cyber/smartid/tse/dto/TSEError;
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/dto/AccountState;->lastSubmitClientSecondPartError:Lee/cyber/smartid/tse/dto/TSEError;

    return-object v0
.end method

.method public getRegistrationState()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/dto/AccountState;->registrationState:Ljava/lang/String;

    return-object v0
.end method

.method public getSignCSRTransactionUUID()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/dto/AccountState;->signCSRTransactionUUID:Ljava/lang/String;

    return-object v0
.end method

.method public getSignKeyReference()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/dto/AccountState;->signKeyReference:Ljava/lang/String;

    return-object v0
.end method

.method public getSignKeyUUID()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/dto/AccountState;->signKeyUUID:Ljava/lang/String;

    return-object v0
.end method

.method public getSignSubmitClientSecondPartState()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/dto/AccountState;->signSubmitClientSecondPartState:Ljava/lang/String;

    return-object v0
.end method

.method public isSubmitClientSecondPartPending()Z
    .locals 2

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/dto/AccountState;->authSubmitClientSecondPartState:Ljava/lang/String;

    const-string v1, "STATE_SUBMIT_CLIENT_SECOND_PART_PENDING"

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lee/cyber/smartid/dto/AccountState;->signSubmitClientSecondPartState:Ljava/lang/String;

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method public notifyAndClearSubmitClientSecondPartListeners(Landroid/os/Handler;Lee/cyber/smartid/inter/ListenerAccess;Lee/cyber/smartid/dto/jsonrpc/resp/AddAccountResp;Lee/cyber/smartid/dto/SmartIdError;)V
    .locals 10

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/dto/AccountState;->submitClientSecondPartListenerTags:Ljava/util/List;

    monitor-enter v0

    .line 2
    :try_start_0
    iget-object v1, p0, Lee/cyber/smartid/dto/AccountState;->submitClientSecondPartListenerTags:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    const/4 v2, 0x1

    if-ge v1, v2, :cond_0

    .line 3
    monitor-exit v0

    return-void

    .line 4
    :cond_0
    invoke-static {p0}, Lee/cyber/smartid/util/Log;->getInstance(Ljava/lang/Object;)Lee/cyber/smartid/util/Log;

    move-result-object v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const-string v3, "notifyAndClearSubmitClientSecondPartListeners called"

    :try_start_1
    invoke-virtual {v1, v3}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    .line 5
    iget-object v1, p0, Lee/cyber/smartid/dto/AccountState;->submitClientSecondPartListenerTags:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 6
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 7
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    move-object v8, v3

    check-cast v8, Ljava/lang/String;

    .line 8
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    .line 9
    const-class v3, Lee/cyber/smartid/inter/AddAccountListener;

    invoke-interface {p2, v8, v2, v3}, Lee/cyber/smartid/inter/ListenerAccess;->getListener(Ljava/lang/String;ZLjava/lang/Class;)Lee/cyber/smartid/inter/ServiceListener;

    move-result-object v3

    move-object v7, v3

    check-cast v7, Lee/cyber/smartid/inter/AddAccountListener;

    if-eqz v7, :cond_1

    .line 10
    invoke-static {p0}, Lee/cyber/smartid/util/Log;->getInstance(Ljava/lang/Object;)Lee/cyber/smartid/util/Log;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    const-string v5, "notifyAndClearSubmitClientSecondPartListeners: Notifying listener tag "

    :try_start_2
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    .line 11
    new-instance v3, Lee/cyber/smartid/dto/AccountState$1;

    move-object v4, v3

    move-object v5, p0

    move-object v6, p4

    move-object v9, p3

    invoke-direct/range {v4 .. v9}, Lee/cyber/smartid/dto/AccountState$1;-><init>(Lee/cyber/smartid/dto/AccountState;Lee/cyber/smartid/dto/SmartIdError;Lee/cyber/smartid/inter/AddAccountListener;Ljava/lang/String;Lee/cyber/smartid/dto/jsonrpc/resp/AddAccountResp;)V

    invoke-virtual {p1, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    .line 12
    :cond_1
    invoke-static {p0}, Lee/cyber/smartid/util/Log;->getInstance(Ljava/lang/Object;)Lee/cyber/smartid/util/Log;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    const-string v5, "notifyAndClearSubmitClientSecondPartListeners: Listener-less tag "

    :try_start_3
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    goto :goto_0

    .line 13
    :cond_2
    monitor-exit v0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw p1
.end method

.method public resetFailedSubmitClientSecondPartStates()V
    .locals 3

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/dto/AccountState;->authSubmitClientSecondPartState:Ljava/lang/String;

    const-string v1, "STATE_SUBMIT_CLIENT_SECOND_PART_FAILED"

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    const-string v2, "STATE_SUBMIT_CLIENT_SECOND_PART_PENDING"

    if-eqz v0, :cond_0

    .line 2
    iput-object v2, p0, Lee/cyber/smartid/dto/AccountState;->authSubmitClientSecondPartState:Ljava/lang/String;

    .line 3
    :cond_0
    iget-object v0, p0, Lee/cyber/smartid/dto/AccountState;->signSubmitClientSecondPartState:Ljava/lang/String;

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 4
    iput-object v2, p0, Lee/cyber/smartid/dto/AccountState;->signSubmitClientSecondPartState:Ljava/lang/String;

    :cond_1
    return-void
.end method

.method public setAuthCSRTransactionUUID(Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lee/cyber/smartid/dto/AccountState;->authCSRTransactionUUID:Ljava/lang/String;

    return-void
.end method

.method public setAuthKeyReference(Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lee/cyber/smartid/dto/AccountState;->authKeyReference:Ljava/lang/String;

    return-void
.end method

.method public setAuthKeyUUID(Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lee/cyber/smartid/dto/AccountState;->authKeyUUID:Ljava/lang/String;

    return-void
.end method

.method public setAuthSubmitClientSecondPartState(Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lee/cyber/smartid/dto/AccountState;->authSubmitClientSecondPartState:Ljava/lang/String;

    return-void
.end method

.method public setCSRTransactionUUIDByKeyType(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    const-string v0, "AUTHENTICATION"

    .line 1
    invoke-static {p1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2
    invoke-virtual {p0, p2}, Lee/cyber/smartid/dto/AccountState;->setAuthCSRTransactionUUID(Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    const-string v0, "SIGNATURE"

    .line 3
    invoke-static {p1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result p1

    if-eqz p1, :cond_1

    .line 4
    invoke-virtual {p0, p2}, Lee/cyber/smartid/dto/AccountState;->setSignCSRTransactionUUID(Ljava/lang/String;)V

    :cond_1
    :goto_0
    return-void
.end method

.method public setIdentityDataFromRegistration(Lee/cyber/smartid/dto/jsonrpc/IdentityData;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lee/cyber/smartid/dto/AccountState;->identityDataFromRegistration:Lee/cyber/smartid/dto/jsonrpc/IdentityData;

    return-void
.end method

.method public setInitiationType(Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lee/cyber/smartid/dto/AccountState;->initiationType:Ljava/lang/String;

    return-void
.end method

.method public setKeyUUIDByKeyType(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    const-string v0, "AUTHENTICATION"

    .line 1
    invoke-static {p1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2
    invoke-virtual {p0, p2}, Lee/cyber/smartid/dto/AccountState;->setAuthKeyUUID(Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    const-string v0, "SIGNATURE"

    .line 3
    invoke-static {p1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result p1

    if-eqz p1, :cond_1

    .line 4
    invoke-virtual {p0, p2}, Lee/cyber/smartid/dto/AccountState;->setSignKeyUUID(Ljava/lang/String;)V

    :cond_1
    :goto_0
    return-void
.end method

.method public setLastSubmitClientSecondPartError(Lee/cyber/smartid/tse/dto/TSEError;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lee/cyber/smartid/dto/AccountState;->lastSubmitClientSecondPartError:Lee/cyber/smartid/tse/dto/TSEError;

    return-void
.end method

.method public setRegistrationState(Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lee/cyber/smartid/dto/AccountState;->registrationState:Ljava/lang/String;

    return-void
.end method

.method public setSignCSRTransactionUUID(Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lee/cyber/smartid/dto/AccountState;->signCSRTransactionUUID:Ljava/lang/String;

    return-void
.end method

.method public setSignKeyReference(Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lee/cyber/smartid/dto/AccountState;->signKeyReference:Ljava/lang/String;

    return-void
.end method

.method public setSignKeyUUID(Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lee/cyber/smartid/dto/AccountState;->signKeyUUID:Ljava/lang/String;

    return-void
.end method

.method public setSignSubmitClientSecondPartState(Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lee/cyber/smartid/dto/AccountState;->signSubmitClientSecondPartState:Ljava/lang/String;

    return-void
.end method

.method public setSubmitClientSecondPartStateByKeyType(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    const-string v0, "AUTHENTICATION"

    .line 1
    invoke-static {p1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2
    invoke-virtual {p0, p2}, Lee/cyber/smartid/dto/AccountState;->setAuthSubmitClientSecondPartState(Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    const-string v0, "SIGNATURE"

    .line 3
    invoke-static {p1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result p1

    if-eqz p1, :cond_1

    .line 4
    invoke-virtual {p0, p2}, Lee/cyber/smartid/dto/AccountState;->setSignSubmitClientSecondPartState(Ljava/lang/String;)V

    :cond_1
    :goto_0
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "AccountState{accountUUID=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lee/cyber/smartid/dto/AccountState;->accountUUID:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v2, ", registrationState=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lee/cyber/smartid/dto/AccountState;->registrationState:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v2, ", authCSRTransactionUUID=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lee/cyber/smartid/dto/AccountState;->authCSRTransactionUUID:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v2, ", signCSRTransactionUUID=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lee/cyber/smartid/dto/AccountState;->signCSRTransactionUUID:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v2, ", authKeyReference=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lee/cyber/smartid/dto/AccountState;->authKeyReference:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v2, ", signKeyReference=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lee/cyber/smartid/dto/AccountState;->signKeyReference:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v2, ", initiationType=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lee/cyber/smartid/dto/AccountState;->initiationType:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v2, ", identityDataFromRegistration="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lee/cyber/smartid/dto/AccountState;->identityDataFromRegistration:Lee/cyber/smartid/dto/jsonrpc/IdentityData;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v2, ", authSubmitClientSecondPartState=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lee/cyber/smartid/dto/AccountState;->authSubmitClientSecondPartState:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v2, ", signSubmitClientSecondPartState=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lee/cyber/smartid/dto/AccountState;->signSubmitClientSecondPartState:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
