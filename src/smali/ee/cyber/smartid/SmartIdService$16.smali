.class Lee/cyber/smartid/SmartIdService$16;
.super Ljava/lang/Object;
.source "SmartIdService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lee/cyber/smartid/SmartIdService;->getPushNotificationData(Ljava/lang/String;Lee/cyber/smartid/inter/GetPushNotificationDataListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:Lee/cyber/smartid/SmartIdService;


# direct methods
.method constructor <init>(Lee/cyber/smartid/SmartIdService;Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lee/cyber/smartid/SmartIdService$16;->b:Lee/cyber/smartid/SmartIdService;

    iput-object p2, p0, Lee/cyber/smartid/SmartIdService$16;->a:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/SmartIdService$16;->b:Lee/cyber/smartid/SmartIdService;

    invoke-static {v0}, Lee/cyber/smartid/SmartIdService;->C(Lee/cyber/smartid/SmartIdService;)Lee/cyber/smartid/cryptolib/CryptoLib;

    move-result-object v0

    iget-object v1, p0, Lee/cyber/smartid/SmartIdService$16;->b:Lee/cyber/smartid/SmartIdService;

    invoke-static {v1}, Lee/cyber/smartid/SmartIdService;->n(Lee/cyber/smartid/SmartIdService;)Lee/cyber/smartid/inter/ServiceAccess;

    move-result-object v1

    invoke-interface {v1}, Lee/cyber/smartid/inter/ServiceAccess;->getPushDataSentStorageId()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lee/cyber/smartid/SmartIdService$16$1;

    invoke-direct {v2, p0}, Lee/cyber/smartid/SmartIdService$16$1;-><init>(Lee/cyber/smartid/SmartIdService$16;)V

    .line 2
    invoke-virtual {v2}, Lcom/google/gson/b/a;->getType()Ljava/lang/reflect/Type;

    move-result-object v2

    .line 3
    invoke-virtual {v0, v1, v2}, Lee/cyber/smartid/cryptolib/CryptoLib;->retrieveData(Ljava/lang/String;Ljava/lang/reflect/Type;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lee/cyber/smartid/dto/PushData;

    .line 4
    new-instance v1, Lee/cyber/smartid/dto/jsonrpc/resp/GetPushNotificationResp;

    iget-object v2, p0, Lee/cyber/smartid/SmartIdService$16;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lee/cyber/smartid/dto/PushData;->getPushToken()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    invoke-direct {v1, v2, v0}, Lee/cyber/smartid/dto/jsonrpc/resp/GetPushNotificationResp;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 5
    iget-object v0, p0, Lee/cyber/smartid/SmartIdService$16;->b:Lee/cyber/smartid/SmartIdService;

    new-instance v2, Lee/cyber/smartid/SmartIdService$16$2;

    invoke-direct {v2, p0, v1}, Lee/cyber/smartid/SmartIdService$16$2;-><init>(Lee/cyber/smartid/SmartIdService$16;Lee/cyber/smartid/dto/jsonrpc/resp/GetPushNotificationResp;)V

    invoke-static {v0, v2}, Lee/cyber/smartid/SmartIdService;->a(Lee/cyber/smartid/SmartIdService;Ljava/lang/Runnable;)V

    return-void
.end method
