.class Lee/cyber/smartid/SmartIdService$39$1;
.super Ljava/lang/Object;
.source "SmartIdService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lee/cyber/smartid/SmartIdService$39;->onFailure(Lc/b;Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCError;Ljava/lang/Throwable;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCError;

.field final synthetic b:Ljava/lang/Throwable;

.field final synthetic c:Lee/cyber/smartid/SmartIdService$39;


# direct methods
.method constructor <init>(Lee/cyber/smartid/SmartIdService$39;Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCError;Ljava/lang/Throwable;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lee/cyber/smartid/SmartIdService$39$1;->c:Lee/cyber/smartid/SmartIdService$39;

    iput-object p2, p0, Lee/cyber/smartid/SmartIdService$39$1;->a:Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCError;

    iput-object p3, p0, Lee/cyber/smartid/SmartIdService$39$1;->b:Ljava/lang/Throwable;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/SmartIdService$39$1;->c:Lee/cyber/smartid/SmartIdService$39;

    iget-object v1, v0, Lee/cyber/smartid/SmartIdService$39;->b:Lee/cyber/smartid/SmartIdService;

    iget-object v0, v0, Lee/cyber/smartid/SmartIdService$39;->a:Ljava/lang/String;

    const-class v2, Lee/cyber/smartid/inter/GetRegistrationTokenListener;

    const/4 v3, 0x1

    invoke-static {v1, v0, v3, v2}, Lee/cyber/smartid/SmartIdService;->a(Lee/cyber/smartid/SmartIdService;Ljava/lang/String;ZLjava/lang/Class;)Lee/cyber/smartid/inter/ServiceListener;

    move-result-object v2

    iget-object v3, p0, Lee/cyber/smartid/SmartIdService$39$1;->c:Lee/cyber/smartid/SmartIdService$39;

    iget-object v3, v3, Lee/cyber/smartid/SmartIdService$39;->b:Lee/cyber/smartid/SmartIdService;

    invoke-static {v3}, Lee/cyber/smartid/SmartIdService;->b(Lee/cyber/smartid/SmartIdService;)Landroid/content/Context;

    move-result-object v3

    iget-object v4, p0, Lee/cyber/smartid/SmartIdService$39$1;->c:Lee/cyber/smartid/SmartIdService$39;

    iget-object v4, v4, Lee/cyber/smartid/SmartIdService$39;->b:Lee/cyber/smartid/SmartIdService;

    invoke-static {v4}, Lee/cyber/smartid/SmartIdService;->E(Lee/cyber/smartid/SmartIdService;)Lee/cyber/smartid/tse/inter/WallClock;

    move-result-object v4

    iget-object v5, p0, Lee/cyber/smartid/SmartIdService$39$1;->a:Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCError;

    iget-object v6, p0, Lee/cyber/smartid/SmartIdService$39$1;->b:Ljava/lang/Throwable;

    invoke-static {v3, v4, v5, v6}, Lee/cyber/smartid/dto/SmartIdError;->from(Landroid/content/Context;Lee/cyber/smartid/tse/inter/WallClock;Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCError;Ljava/lang/Throwable;)Lee/cyber/smartid/dto/SmartIdError;

    move-result-object v3

    invoke-static {v1, v0, v2, v3}, Lee/cyber/smartid/SmartIdService;->a(Lee/cyber/smartid/SmartIdService;Ljava/lang/String;Lee/cyber/smartid/inter/ServiceListener;Lee/cyber/smartid/dto/SmartIdError;)V

    return-void
.end method
