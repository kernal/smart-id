.class Lee/cyber/smartid/SmartIdService$SmartIdServiceAccess;
.super Ljava/lang/Object;
.source "SmartIdService.java"

# interfaces
.implements Lee/cyber/smartid/inter/ServiceAccess;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lee/cyber/smartid/SmartIdService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SmartIdServiceAccess"
.end annotation


# instance fields
.field final synthetic a:Lee/cyber/smartid/SmartIdService;

.field private final b:Ljava/lang/String;

.field private final c:Ljava/lang/String;

.field private final d:Ljava/lang/String;

.field private final e:Ljava/lang/String;

.field private final f:Ljava/lang/String;

.field private final g:Ljava/lang/String;

.field private final h:Ljava/lang/String;

.field private final i:Ljava/lang/String;


# direct methods
.method private constructor <init>(Lee/cyber/smartid/SmartIdService;)V
    .locals 0

    .line 2
    iput-object p1, p0, Lee/cyber/smartid/SmartIdService$SmartIdServiceAccess;->a:Lee/cyber/smartid/SmartIdService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string p1, "ee.cyber.smartid.APP_ACCOUNTS_DATA_%1$s"

    .line 3
    iput-object p1, p0, Lee/cyber/smartid/SmartIdService$SmartIdServiceAccess;->b:Ljava/lang/String;

    const-string p1, "ee.cyber.smartid.APP_ACCOUNT_STATES_ID_%1$s"

    .line 4
    iput-object p1, p0, Lee/cyber/smartid/SmartIdService$SmartIdServiceAccess;->c:Ljava/lang/String;

    const-string p1, "ee.cyber.smartid.APP_TRANSACTION_CACHE_STORAGE_ID_%1$s"

    .line 5
    iput-object p1, p0, Lee/cyber/smartid/SmartIdService$SmartIdServiceAccess;->d:Ljava/lang/String;

    const-string p1, "APP_PUSH_DATA_%1$s"

    .line 6
    iput-object p1, p0, Lee/cyber/smartid/SmartIdService$SmartIdServiceAccess;->e:Ljava/lang/String;

    const-string p1, "APP_PUSH_DATA_SENT_%1$s"

    .line 7
    iput-object p1, p0, Lee/cyber/smartid/SmartIdService$SmartIdServiceAccess;->f:Ljava/lang/String;

    const-string p1, "APP_UPDATE_DEVICE_HASH_%1$s"

    .line 8
    iput-object p1, p0, Lee/cyber/smartid/SmartIdService$SmartIdServiceAccess;->g:Ljava/lang/String;

    const-string p1, "APP_SAFETY_NET_RESULT_HASH_%1$s"

    .line 9
    iput-object p1, p0, Lee/cyber/smartid/SmartIdService$SmartIdServiceAccess;->h:Ljava/lang/String;

    const-string p1, "APP_INTERACTIVE_UPGRADE_STATE_STORAGE_ID_%1$s"

    .line 10
    iput-object p1, p0, Lee/cyber/smartid/SmartIdService$SmartIdServiceAccess;->i:Ljava/lang/String;

    return-void
.end method

.method synthetic constructor <init>(Lee/cyber/smartid/SmartIdService;Lee/cyber/smartid/SmartIdService$1;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lee/cyber/smartid/SmartIdService$SmartIdServiceAccess;-><init>(Lee/cyber/smartid/SmartIdService;)V

    return-void
.end method


# virtual methods
.method public acquireWakeLockForService()V
    .locals 3

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/SmartIdService$SmartIdServiceAccess;->a:Lee/cyber/smartid/SmartIdService;

    invoke-static {v0}, Lee/cyber/smartid/SmartIdService;->p(Lee/cyber/smartid/SmartIdService;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    const-wide/32 v1, 0x1d4c0

    invoke-static {v0, v1, v2}, Lee/cyber/smartid/util/Util;->acquireWakeLock(Landroid/os/PowerManager$WakeLock;J)V

    return-void
.end method

.method public getAccountUUIDs()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/SmartIdService$SmartIdServiceAccess;->a:Lee/cyber/smartid/SmartIdService;

    invoke-static {v0}, Lee/cyber/smartid/SmartIdService;->r(Lee/cyber/smartid/SmartIdService;)Lee/cyber/smartid/manager/inter/StoredAccountManager;

    move-result-object v0

    invoke-interface {v0}, Lee/cyber/smartid/manager/inter/StoredAccountManager;->getAccountUUIDs()Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public getAccountsStorageId()Ljava/lang/String;
    .locals 3

    const/4 v0, 0x1

    .line 1
    new-array v0, v0, [Ljava/lang/Object;

    invoke-virtual {p0}, Lee/cyber/smartid/SmartIdService$SmartIdServiceAccess;->getDeviceFingerPrint()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    const-string v1, "ee.cyber.smartid.APP_ACCOUNT_STATES_ID_%1$s"

    invoke-static {v1, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getAppStorageVersion()I
    .locals 1

    .line 1
    invoke-virtual {p0}, Lee/cyber/smartid/SmartIdService$SmartIdServiceAccess;->getCurrentStorageVersion()I

    move-result v0

    return v0
.end method

.method public getAppVersion()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/SmartIdService$SmartIdServiceAccess;->a:Lee/cyber/smartid/SmartIdService;

    invoke-static {v0}, Lee/cyber/smartid/SmartIdService;->b(Lee/cyber/smartid/SmartIdService;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lee/cyber/smartid/util/Util;->getAppVersionName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getApplicationContext()Landroid/content/Context;
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/SmartIdService$SmartIdServiceAccess;->a:Lee/cyber/smartid/SmartIdService;

    invoke-static {v0}, Lee/cyber/smartid/SmartIdService;->b(Lee/cyber/smartid/SmartIdService;)Landroid/content/Context;

    move-result-object v0

    return-object v0
.end method

.method public getAutomaticRequestRetryDelay(I)J
    .locals 2

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/SmartIdService$SmartIdServiceAccess;->a:Lee/cyber/smartid/SmartIdService;

    invoke-static {v0, p1}, Lee/cyber/smartid/SmartIdService;->a(Lee/cyber/smartid/SmartIdService;I)J

    move-result-wide v0

    return-wide v0
.end method

.method public getCurrentStorageVersion()I
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/SmartIdService$SmartIdServiceAccess;->a:Lee/cyber/smartid/SmartIdService;

    invoke-static {v0}, Lee/cyber/smartid/SmartIdService;->c(Lee/cyber/smartid/SmartIdService;)I

    move-result v0

    return v0
.end method

.method public getDeleteAccountManager()Lee/cyber/smartid/manager/inter/DeleteAccountManager;
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/SmartIdService$SmartIdServiceAccess;->a:Lee/cyber/smartid/SmartIdService;

    invoke-static {v0}, Lee/cyber/smartid/SmartIdService;->u(Lee/cyber/smartid/SmartIdService;)Lee/cyber/smartid/manager/inter/DeleteAccountManager;

    move-result-object v0

    return-object v0
.end method

.method public getDeviceFingerPrint()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/SmartIdService$SmartIdServiceAccess;->a:Lee/cyber/smartid/SmartIdService;

    invoke-static {v0}, Lee/cyber/smartid/SmartIdService;->a(Lee/cyber/smartid/SmartIdService;)Lee/cyber/smartid/manager/inter/DeviceFingerprintManager;

    move-result-object v0

    invoke-interface {v0}, Lee/cyber/smartid/manager/inter/DeviceFingerprintManager;->getDeviceFingerPrint()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getInteractiveUpgradeHelper()Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/SmartIdService$SmartIdServiceAccess;->a:Lee/cyber/smartid/SmartIdService;

    invoke-static {v0}, Lee/cyber/smartid/SmartIdService;->f(Lee/cyber/smartid/SmartIdService;)Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;

    move-result-object v0

    return-object v0
.end method

.method public getInteractiveUpgradeStateStorageId()Ljava/lang/String;
    .locals 3

    const/4 v0, 0x1

    .line 1
    new-array v0, v0, [Ljava/lang/Object;

    invoke-virtual {p0}, Lee/cyber/smartid/SmartIdService$SmartIdServiceAccess;->getDeviceFingerPrint()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    const-string v1, "APP_INTERACTIVE_UPGRADE_STATE_STORAGE_ID_%1$s"

    invoke-static {v1, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getKeyId(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/SmartIdService$SmartIdServiceAccess;->a:Lee/cyber/smartid/SmartIdService;

    invoke-static {v0, p1, p2}, Lee/cyber/smartid/SmartIdService;->a(Lee/cyber/smartid/SmartIdService;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public getKeyIds()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/SmartIdService$SmartIdServiceAccess;->a:Lee/cyber/smartid/SmartIdService;

    invoke-static {v0}, Lee/cyber/smartid/SmartIdService;->r(Lee/cyber/smartid/SmartIdService;)Lee/cyber/smartid/manager/inter/StoredAccountManager;

    move-result-object v0

    invoke-interface {v0}, Lee/cyber/smartid/manager/inter/StoredAccountManager;->getAllUsableKeyIds()Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public getKeyIdsWithAccountUUIDAndKeyType()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Lee/cyber/smartid/tse/dto/Triple<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/SmartIdService$SmartIdServiceAccess;->a:Lee/cyber/smartid/SmartIdService;

    invoke-static {v0}, Lee/cyber/smartid/SmartIdService;->r(Lee/cyber/smartid/SmartIdService;)Lee/cyber/smartid/manager/inter/StoredAccountManager;

    move-result-object v0

    invoke-interface {v0}, Lee/cyber/smartid/manager/inter/StoredAccountManager;->getAllUsableKeyIdsWithAccountUUIDAndKeyType()Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public getKeyTypes()[Ljava/lang/String;
    .locals 1

    .line 1
    sget-object v0, Lee/cyber/smartid/SmartIdService;->ACCOUNT_KEY_TYPES:[Ljava/lang/String;

    return-object v0
.end method

.method public getKnownServerKeys(Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/SmartIdService$SmartIdServiceAccess;->a:Lee/cyber/smartid/SmartIdService;

    invoke-static {v0}, Lee/cyber/smartid/SmartIdService;->d(Lee/cyber/smartid/SmartIdService;)Lee/cyber/smartid/tse/SmartIdTSE;

    move-result-object v0

    invoke-virtual {v0, p1}, Lee/cyber/smartid/tse/SmartIdTSE;->getKnownServerKeys(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object p1

    return-object p1
.end method

.method public getLastSafetyNetAttestation()Lee/cyber/smartid/dto/SafetyNetAttestation;
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/SmartIdService$SmartIdServiceAccess;->a:Lee/cyber/smartid/SmartIdService;

    invoke-static {v0}, Lee/cyber/smartid/SmartIdService;->h(Lee/cyber/smartid/SmartIdService;)Lee/cyber/smartid/manager/inter/SafetyNetManager;

    move-result-object v0

    invoke-interface {v0}, Lee/cyber/smartid/manager/inter/SafetyNetManager;->getLastSafetyNetAttestation()Lee/cyber/smartid/dto/SafetyNetAttestation;

    move-result-object v0

    return-object v0
.end method

.method public getLibraryVersion()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/SmartIdService$SmartIdServiceAccess;->a:Lee/cyber/smartid/SmartIdService;

    invoke-virtual {v0}, Lee/cyber/smartid/SmartIdService;->getVersion()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getMDv2StorageVersion()I
    .locals 1

    const/4 v0, 0x5

    return v0
.end method

.method public getPlatform()Ljava/lang/String;
    .locals 1

    const-string v0, "Android"

    return-object v0
.end method

.method public getPostInitManager()Lee/cyber/smartid/manager/inter/PostInitManager;
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/SmartIdService$SmartIdServiceAccess;->a:Lee/cyber/smartid/SmartIdService;

    invoke-static {v0}, Lee/cyber/smartid/SmartIdService;->v(Lee/cyber/smartid/SmartIdService;)Lee/cyber/smartid/manager/inter/PostInitManager;

    move-result-object v0

    return-object v0
.end method

.method public getPropertiesManager()Lee/cyber/smartid/manager/inter/PropertiesManager;
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/SmartIdService$SmartIdServiceAccess;->a:Lee/cyber/smartid/SmartIdService;

    invoke-static {v0}, Lee/cyber/smartid/SmartIdService;->k(Lee/cyber/smartid/SmartIdService;)Lee/cyber/smartid/manager/inter/PropertiesManager;

    move-result-object v0

    return-object v0
.end method

.method public getPropertySource()Lee/cyber/smartid/manager/inter/PropertiesSource;
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/SmartIdService$SmartIdServiceAccess;->a:Lee/cyber/smartid/SmartIdService;

    invoke-static {v0}, Lee/cyber/smartid/SmartIdService;->q(Lee/cyber/smartid/SmartIdService;)Lee/cyber/smartid/manager/inter/PropertiesSource;

    move-result-object v0

    return-object v0
.end method

.method public getPushDataCurrentStorageId()Ljava/lang/String;
    .locals 3

    const/4 v0, 0x1

    .line 1
    new-array v0, v0, [Ljava/lang/Object;

    invoke-virtual {p0}, Lee/cyber/smartid/SmartIdService$SmartIdServiceAccess;->getDeviceFingerPrint()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    const-string v1, "APP_PUSH_DATA_%1$s"

    invoke-static {v1, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getPushDataSentStorageId()Ljava/lang/String;
    .locals 3

    const/4 v0, 0x1

    .line 1
    new-array v0, v0, [Ljava/lang/Object;

    invoke-virtual {p0}, Lee/cyber/smartid/SmartIdService$SmartIdServiceAccess;->getDeviceFingerPrint()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    const-string v1, "APP_PUSH_DATA_SENT_%1$s"

    invoke-static {v1, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getRemoveExpiredTransactionActionId()Ljava/lang/String;
    .locals 1

    const-string v0, "ee.cyber.smartid.ACTION_REMOVE_EXPIRED_TRANSACTIONS"

    return-object v0
.end method

.method public getSafetyNetManager()Lee/cyber/smartid/manager/inter/SafetyNetManager;
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/SmartIdService$SmartIdServiceAccess;->a:Lee/cyber/smartid/SmartIdService;

    invoke-static {v0}, Lee/cyber/smartid/SmartIdService;->h(Lee/cyber/smartid/SmartIdService;)Lee/cyber/smartid/manager/inter/SafetyNetManager;

    move-result-object v0

    return-object v0
.end method

.method public getSafetyNetResultHashStorageId()Ljava/lang/String;
    .locals 3

    const/4 v0, 0x1

    .line 1
    new-array v0, v0, [Ljava/lang/Object;

    invoke-virtual {p0}, Lee/cyber/smartid/SmartIdService$SmartIdServiceAccess;->getDeviceFingerPrint()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    const-string v1, "APP_SAFETY_NET_RESULT_HASH_%1$s"

    invoke-static {v1, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getStoredAccountManager()Lee/cyber/smartid/manager/inter/StoredAccountManager;
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/SmartIdService$SmartIdServiceAccess;->a:Lee/cyber/smartid/SmartIdService;

    invoke-static {v0}, Lee/cyber/smartid/SmartIdService;->r(Lee/cyber/smartid/SmartIdService;)Lee/cyber/smartid/manager/inter/StoredAccountManager;

    move-result-object v0

    return-object v0
.end method

.method public getTransactionCacheManager()Lee/cyber/smartid/manager/inter/TransactionCacheManager;
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/SmartIdService$SmartIdServiceAccess;->a:Lee/cyber/smartid/SmartIdService;

    invoke-static {v0}, Lee/cyber/smartid/SmartIdService;->i(Lee/cyber/smartid/SmartIdService;)Lee/cyber/smartid/manager/inter/TransactionCacheManager;

    move-result-object v0

    return-object v0
.end method

.method public getTransactionCacheStorageId()Ljava/lang/String;
    .locals 3

    const/4 v0, 0x1

    .line 1
    new-array v0, v0, [Ljava/lang/Object;

    invoke-virtual {p0}, Lee/cyber/smartid/SmartIdService$SmartIdServiceAccess;->getDeviceFingerPrint()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    const-string v1, "ee.cyber.smartid.APP_TRANSACTION_CACHE_STORAGE_ID_%1$s"

    invoke-static {v1, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getTransactionFinder()Lee/cyber/smartid/util/TransactionFinder;
    .locals 2

    .line 1
    new-instance v0, Lee/cyber/smartid/util/TransactionFinder;

    iget-object v1, p0, Lee/cyber/smartid/SmartIdService$SmartIdServiceAccess;->a:Lee/cyber/smartid/SmartIdService;

    invoke-static {v1}, Lee/cyber/smartid/SmartIdService;->n(Lee/cyber/smartid/SmartIdService;)Lee/cyber/smartid/inter/ServiceAccess;

    move-result-object v1

    invoke-direct {v0, v1}, Lee/cyber/smartid/util/TransactionFinder;-><init>(Lee/cyber/smartid/inter/ServiceAccess;)V

    return-object v0
.end method

.method public getTransactionRespMapper()Lee/cyber/smartid/manager/inter/TransactionRespMapper;
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/SmartIdService$SmartIdServiceAccess;->a:Lee/cyber/smartid/SmartIdService;

    invoke-static {v0}, Lee/cyber/smartid/SmartIdService;->j(Lee/cyber/smartid/SmartIdService;)Lee/cyber/smartid/manager/inter/TransactionRespMapper;

    move-result-object v0

    return-object v0
.end method

.method public getTseErrorToServiceErrorMapper()Lee/cyber/smartid/manager/inter/TSEErrorToServiceErrorMapper;
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/SmartIdService$SmartIdServiceAccess;->a:Lee/cyber/smartid/SmartIdService;

    invoke-static {v0}, Lee/cyber/smartid/SmartIdService;->t(Lee/cyber/smartid/SmartIdService;)Lee/cyber/smartid/manager/impl/TSEErrorToServiceErrorMapperImpl;

    move-result-object v0

    return-object v0
.end method

.method public getUpdateDeviceData()Lee/cyber/smartid/dto/jsonrpc/param/RegisterDeviceData;
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/SmartIdService$SmartIdServiceAccess;->a:Lee/cyber/smartid/SmartIdService;

    invoke-static {v0}, Lee/cyber/smartid/SmartIdService;->g(Lee/cyber/smartid/SmartIdService;)Lee/cyber/smartid/dto/jsonrpc/param/RegisterDeviceData;

    move-result-object v0

    return-object v0
.end method

.method public getUpdateDeviceHashStorageId()Ljava/lang/String;
    .locals 3

    const/4 v0, 0x1

    .line 1
    new-array v0, v0, [Ljava/lang/Object;

    invoke-virtual {p0}, Lee/cyber/smartid/SmartIdService$SmartIdServiceAccess;->getDeviceFingerPrint()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    const-string v1, "APP_UPDATE_DEVICE_HASH_%1$s"

    invoke-static {v1, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getVerificationCodeEvaluator()Lee/cyber/smartid/manager/inter/VerificationCodeEvaluator;
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/SmartIdService$SmartIdServiceAccess;->a:Lee/cyber/smartid/SmartIdService;

    invoke-static {v0}, Lee/cyber/smartid/SmartIdService;->l(Lee/cyber/smartid/SmartIdService;)Lee/cyber/smartid/manager/inter/VerificationCodeEvaluator;

    move-result-object v0

    return-object v0
.end method

.method public getVerificationCodeGenerator()Lee/cyber/smartid/manager/inter/VerificationCodeGenerator;
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/SmartIdService$SmartIdServiceAccess;->a:Lee/cyber/smartid/SmartIdService;

    invoke-static {v0}, Lee/cyber/smartid/SmartIdService;->m(Lee/cyber/smartid/SmartIdService;)Lee/cyber/smartid/manager/inter/VerificationCodeGenerator;

    move-result-object v0

    return-object v0
.end method

.method public handleUpgrade()Lee/cyber/smartid/dto/upgrade/InteractiveUpgradeState;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lee/cyber/smartid/cryptolib/dto/StorageException;
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/SmartIdService$SmartIdServiceAccess;->a:Lee/cyber/smartid/SmartIdService;

    invoke-static {v0}, Lee/cyber/smartid/SmartIdService;->e(Lee/cyber/smartid/SmartIdService;)Lee/cyber/smartid/dto/upgrade/InteractiveUpgradeState;

    move-result-object v0

    return-object v0
.end method

.method public notifyUI(Ljava/lang/Runnable;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/SmartIdService$SmartIdServiceAccess;->a:Lee/cyber/smartid/SmartIdService;

    invoke-static {v0, p1}, Lee/cyber/smartid/SmartIdService;->a(Lee/cyber/smartid/SmartIdService;Ljava/lang/Runnable;)V

    return-void
.end method

.method public onPostUpdateDevice(Lee/cyber/smartid/dto/jsonrpc/param/RegisterDeviceData;Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lee/cyber/smartid/cryptolib/dto/StorageException;
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/SmartIdService$SmartIdServiceAccess;->a:Lee/cyber/smartid/SmartIdService;

    invoke-static {v0, p1, p2}, Lee/cyber/smartid/SmartIdService;->a(Lee/cyber/smartid/SmartIdService;Lee/cyber/smartid/dto/jsonrpc/param/RegisterDeviceData;Ljava/lang/String;)V

    return-void
.end method

.method public onSubmitClientSecondPartFailed(Ljava/lang/String;Lee/cyber/smartid/tse/dto/TSEError;)V
    .locals 7

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/SmartIdService$SmartIdServiceAccess;->a:Lee/cyber/smartid/SmartIdService;

    invoke-static {v0}, Lee/cyber/smartid/SmartIdService;->r(Lee/cyber/smartid/SmartIdService;)Lee/cyber/smartid/manager/inter/StoredAccountManager;

    move-result-object v0

    invoke-interface {v0}, Lee/cyber/smartid/manager/inter/StoredAccountManager;->getAccountStateUpdateLock()Ljava/lang/Object;

    move-result-object v0

    monitor-enter v0

    .line 2
    :try_start_0
    invoke-virtual {p2}, Lee/cyber/smartid/tse/dto/TSEError;->createOrGetExtras()Landroid/os/Bundle;

    move-result-object v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const-string v2, "ee.cyber.smartid.EXTRA_KEY_ID"

    :try_start_1
    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_4

    .line 3
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    goto :goto_1

    .line 4
    :cond_0
    invoke-virtual {p2}, Lee/cyber/smartid/tse/dto/TSEError;->getCode()J

    move-result-wide v2

    const-wide/16 v4, 0x406

    cmp-long v6, v2, v4

    if-nez v6, :cond_3

    .line 5
    iget-object v2, p0, Lee/cyber/smartid/SmartIdService$SmartIdServiceAccess;->a:Lee/cyber/smartid/SmartIdService;

    invoke-static {v2}, Lee/cyber/smartid/SmartIdService;->r(Lee/cyber/smartid/SmartIdService;)Lee/cyber/smartid/manager/inter/StoredAccountManager;

    move-result-object v2

    invoke-interface {v2, v1}, Lee/cyber/smartid/manager/inter/StoredAccountManager;->loadAccountFromStorageByKeyId(Ljava/lang/String;)Lee/cyber/smartid/dto/AccountState;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 6
    invoke-virtual {v1}, Lee/cyber/smartid/dto/AccountState;->isSubmitClientSecondPartPending()Z

    move-result v2

    if-nez v2, :cond_1

    .line 7
    iget-object p1, p0, Lee/cyber/smartid/SmartIdService$SmartIdServiceAccess;->a:Lee/cyber/smartid/SmartIdService;

    invoke-static {p1}, Lee/cyber/smartid/SmartIdService;->w(Lee/cyber/smartid/SmartIdService;)Lee/cyber/smartid/manager/inter/AddAccountManager;

    move-result-object p1

    invoke-interface {p1, v1}, Lee/cyber/smartid/manager/inter/AddAccountManager;->handleSCSPStateCheckForAddAccount(Lee/cyber/smartid/dto/AccountState;)V

    goto :goto_0

    :cond_1
    if-eqz v1, :cond_2

    goto :goto_0

    .line 8
    :cond_2
    new-instance v1, Lee/cyber/smartid/SmartIdService$SmartIdServiceAccess$1;

    invoke-direct {v1, p0, p1, p2}, Lee/cyber/smartid/SmartIdService$SmartIdServiceAccess$1;-><init>(Lee/cyber/smartid/SmartIdService$SmartIdServiceAccess;Ljava/lang/String;Lee/cyber/smartid/tse/dto/TSEError;)V

    invoke-virtual {p0, v1}, Lee/cyber/smartid/SmartIdService$SmartIdServiceAccess;->notifyUI(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 9
    :cond_3
    iget-object v2, p0, Lee/cyber/smartid/SmartIdService$SmartIdServiceAccess;->a:Lee/cyber/smartid/SmartIdService;

    invoke-static {v2}, Lee/cyber/smartid/SmartIdService;->w(Lee/cyber/smartid/SmartIdService;)Lee/cyber/smartid/manager/inter/AddAccountManager;

    move-result-object v2

    const/4 v3, 0x0

    invoke-interface {v2, p1, v1, v3, p2}, Lee/cyber/smartid/manager/inter/AddAccountManager;->handleSCSPResultForAddAccount(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/tse/dto/TSEError;)V

    .line 10
    :goto_0
    monitor-exit v0

    return-void

    .line 11
    :cond_4
    :goto_1
    invoke-static {}, Lee/cyber/smartid/SmartIdService;->a()Lee/cyber/smartid/util/Log;

    move-result-object p1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    const-string p2, "onSubmitClientSecondPartFailed: KeyId should not be null here, aborting!"

    :try_start_2
    invoke-virtual {p1, p2}, Lee/cyber/smartid/util/Log;->wtf(Ljava/lang/String;)V

    .line 12
    monitor-exit v0

    return-void

    :catchall_0
    move-exception p1

    .line 13
    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw p1
.end method

.method public onSubmitClientSecondPartSuccess(Ljava/lang/String;Lee/cyber/smartid/tse/dto/jsonrpc/resp/SubmitClientSecondPartResp;)V
    .locals 3

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/SmartIdService$SmartIdServiceAccess;->a:Lee/cyber/smartid/SmartIdService;

    invoke-static {v0}, Lee/cyber/smartid/SmartIdService;->w(Lee/cyber/smartid/SmartIdService;)Lee/cyber/smartid/manager/inter/AddAccountManager;

    move-result-object v0

    invoke-virtual {p2}, Lee/cyber/smartid/tse/dto/jsonrpc/resp/SubmitClientSecondPartResp;->getKeyId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2}, Lee/cyber/smartid/tse/dto/jsonrpc/resp/SubmitClientSecondPartResp;->getCsrTransactionUUID()Ljava/lang/String;

    move-result-object p2

    const/4 v2, 0x0

    invoke-interface {v0, p1, v1, p2, v2}, Lee/cyber/smartid/manager/inter/AddAccountManager;->handleSCSPResultForAddAccount(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/tse/dto/TSEError;)V

    return-void
.end method

.method public releaseWakeLockForService()V
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/SmartIdService$SmartIdServiceAccess;->a:Lee/cyber/smartid/SmartIdService;

    invoke-static {v0}, Lee/cyber/smartid/SmartIdService;->p(Lee/cyber/smartid/SmartIdService;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    invoke-static {v0}, Lee/cyber/smartid/util/Util;->releaseWakeLock(Landroid/os/PowerManager$WakeLock;)V

    return-void
.end method

.method public setInitServiceDone()V
    .locals 2

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/SmartIdService$SmartIdServiceAccess;->a:Lee/cyber/smartid/SmartIdService;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lee/cyber/smartid/SmartIdService;->a(Lee/cyber/smartid/SmartIdService;Z)V

    return-void
.end method

.method public updateDeviceForPostInitIfNeeded()V
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/SmartIdService$SmartIdServiceAccess;->a:Lee/cyber/smartid/SmartIdService;

    invoke-static {v0}, Lee/cyber/smartid/SmartIdService;->o(Lee/cyber/smartid/SmartIdService;)V

    return-void
.end method

.method public updateDeviceForPostKeyGenerationIfNeeded()V
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/SmartIdService$SmartIdServiceAccess;->a:Lee/cyber/smartid/SmartIdService;

    invoke-static {v0}, Lee/cyber/smartid/SmartIdService;->s(Lee/cyber/smartid/SmartIdService;)V

    return-void
.end method
