.class public Lee/cyber/smartid/util/AccountFinder;
.super Ljava/lang/Object;
.source "AccountFinder.java"


# instance fields
.field private a:Lee/cyber/smartid/dto/AccountState;

.field private b:Ljava/lang/String;

.field private c:Lee/cyber/smartid/util/Log;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    invoke-static {p0}, Lee/cyber/smartid/util/Log;->getInstance(Ljava/lang/Object;)Lee/cyber/smartid/util/Log;

    move-result-object v0

    iput-object v0, p0, Lee/cyber/smartid/util/AccountFinder;->c:Lee/cyber/smartid/util/Log;

    return-void
.end method


# virtual methods
.method public findOrThrow(Lee/cyber/smartid/inter/ServiceAccess;Ljava/lang/String;)Lee/cyber/smartid/util/AccountFinder;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lee/cyber/smartid/cryptolib/dto/StorageException;
        }
    .end annotation

    .line 1
    invoke-interface {p1}, Lee/cyber/smartid/inter/ServiceAccess;->getStoredAccountManager()Lee/cyber/smartid/manager/inter/StoredAccountManager;

    move-result-object v0

    invoke-interface {v0}, Lee/cyber/smartid/manager/inter/StoredAccountManager;->loadAccountsFromStorage()Ljava/util/ArrayList;

    move-result-object v0

    .line 2
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    const/4 v2, 0x2

    if-eqz v1, :cond_4

    const/4 v1, 0x0

    .line 3
    iput-object v1, p0, Lee/cyber/smartid/util/AccountFinder;->a:Lee/cyber/smartid/dto/AccountState;

    .line 4
    iput-object v1, p0, Lee/cyber/smartid/util/AccountFinder;->b:Ljava/lang/String;

    .line 5
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lee/cyber/smartid/dto/AccountState;

    .line 6
    invoke-virtual {v1}, Lee/cyber/smartid/dto/AccountState;->getAccountUUID()Ljava/lang/String;

    move-result-object v3

    const-string v4, "AUTHENTICATION"

    invoke-interface {p1, v3, v4}, Lee/cyber/smartid/inter/ServiceAccess;->getKeyId(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {p2, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 7
    iput-object v1, p0, Lee/cyber/smartid/util/AccountFinder;->a:Lee/cyber/smartid/dto/AccountState;

    .line 8
    iput-object v4, p0, Lee/cyber/smartid/util/AccountFinder;->b:Ljava/lang/String;

    goto :goto_0

    .line 9
    :cond_1
    invoke-virtual {v1}, Lee/cyber/smartid/dto/AccountState;->getAccountUUID()Ljava/lang/String;

    move-result-object v3

    const-string v4, "SIGNATURE"

    invoke-interface {p1, v3, v4}, Lee/cyber/smartid/inter/ServiceAccess;->getKeyId(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {p2, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 10
    iput-object v1, p0, Lee/cyber/smartid/util/AccountFinder;->a:Lee/cyber/smartid/dto/AccountState;

    .line 11
    iput-object v4, p0, Lee/cyber/smartid/util/AccountFinder;->b:Ljava/lang/String;

    .line 12
    :cond_2
    :goto_0
    iget-object v0, p0, Lee/cyber/smartid/util/AccountFinder;->a:Lee/cyber/smartid/dto/AccountState;

    if-eqz v0, :cond_3

    return-object p0

    .line 13
    :cond_3
    iget-object v0, p0, Lee/cyber/smartid/util/AccountFinder;->c:Lee/cyber/smartid/util/Log;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "AccountFinder - The keyId "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p2, " does not belong to any of the known accounts, aborting .."

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v0, p2}, Lee/cyber/smartid/util/Log;->wtf(Ljava/lang/String;)V

    .line 14
    new-instance p2, Lee/cyber/smartid/cryptolib/dto/StorageException;

    invoke-interface {p1}, Lee/cyber/smartid/inter/ServiceAccess;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    sget v0, Lee/cyber/smartid/R$string;->err_account_not_found:I

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, v2, p1}, Lee/cyber/smartid/cryptolib/dto/StorageException;-><init>(ILjava/lang/String;)V

    throw p2

    .line 15
    :cond_4
    iget-object p2, p0, Lee/cyber/smartid/util/AccountFinder;->c:Lee/cyber/smartid/util/Log;

    const-string v0, "AccountFinder - No accounts found, aborting .."

    invoke-virtual {p2, v0}, Lee/cyber/smartid/util/Log;->wtf(Ljava/lang/String;)V

    .line 16
    new-instance p2, Lee/cyber/smartid/cryptolib/dto/StorageException;

    invoke-interface {p1}, Lee/cyber/smartid/inter/ServiceAccess;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    sget v0, Lee/cyber/smartid/R$string;->err_no_accounts_found:I

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, v2, p1}, Lee/cyber/smartid/cryptolib/dto/StorageException;-><init>(ILjava/lang/String;)V

    throw p2
.end method

.method public getAccount()Lee/cyber/smartid/dto/AccountState;
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/util/AccountFinder;->a:Lee/cyber/smartid/dto/AccountState;

    return-object v0
.end method

.method public getKeyType()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/util/AccountFinder;->b:Ljava/lang/String;

    return-object v0
.end method
