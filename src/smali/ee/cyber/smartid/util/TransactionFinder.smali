.class public Lee/cyber/smartid/util/TransactionFinder;
.super Ljava/lang/Object;
.source "TransactionFinder.java"


# instance fields
.field private final a:Lee/cyber/smartid/inter/ServiceAccess;

.field private b:Lee/cyber/smartid/dto/jsonrpc/Transaction;


# direct methods
.method public constructor <init>(Lee/cyber/smartid/inter/ServiceAccess;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Lee/cyber/smartid/util/TransactionFinder;->a:Lee/cyber/smartid/inter/ServiceAccess;

    return-void
.end method


# virtual methods
.method public findOrThrow(Ljava/lang/String;)Lee/cyber/smartid/util/TransactionFinder;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lee/cyber/smartid/dto/NoSuchTransactionException;
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/util/TransactionFinder;->a:Lee/cyber/smartid/inter/ServiceAccess;

    invoke-interface {v0}, Lee/cyber/smartid/inter/ServiceAccess;->getTransactionCacheManager()Lee/cyber/smartid/manager/inter/TransactionCacheManager;

    move-result-object v0

    invoke-interface {v0, p1}, Lee/cyber/smartid/manager/inter/TransactionCacheManager;->getCachedTransaction(Ljava/lang/String;)Lee/cyber/smartid/dto/jsonrpc/Transaction;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2
    invoke-virtual {v0}, Lee/cyber/smartid/dto/jsonrpc/Transaction;->getTransactionUUID()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result p1

    if-eqz p1, :cond_1

    .line 3
    sget-object p1, Lee/cyber/smartid/SmartIdService;->TRANSACTION_TYPES:[Ljava/lang/String;

    invoke-virtual {v0}, Lee/cyber/smartid/dto/jsonrpc/Transaction;->getTransactionType()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {p1, v1, v2}, Lee/cyber/smartid/util/Util;->contains([Ljava/lang/String;Ljava/lang/String;Z)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 4
    iput-object v0, p0, Lee/cyber/smartid/util/TransactionFinder;->b:Lee/cyber/smartid/dto/jsonrpc/Transaction;

    return-object p0

    .line 5
    :cond_0
    new-instance p1, Lee/cyber/smartid/dto/NoSuchTransactionException;

    iget-object v0, p0, Lee/cyber/smartid/util/TransactionFinder;->a:Lee/cyber/smartid/inter/ServiceAccess;

    invoke-interface {v0}, Lee/cyber/smartid/inter/ServiceAccess;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    sget v1, Lee/cyber/smartid/R$string;->err_wrong_transaction_type:I

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-wide/16 v1, 0x3ed

    invoke-direct {p1, v1, v2, v0}, Lee/cyber/smartid/dto/NoSuchTransactionException;-><init>(JLjava/lang/String;)V

    throw p1

    .line 6
    :cond_1
    new-instance p1, Lee/cyber/smartid/dto/NoSuchTransactionException;

    iget-object v0, p0, Lee/cyber/smartid/util/TransactionFinder;->a:Lee/cyber/smartid/inter/ServiceAccess;

    invoke-interface {v0}, Lee/cyber/smartid/inter/ServiceAccess;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    sget v1, Lee/cyber/smartid/R$string;->err_no_such_transaction:I

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-wide/16 v1, 0x3ee

    invoke-direct {p1, v1, v2, v0}, Lee/cyber/smartid/dto/NoSuchTransactionException;-><init>(JLjava/lang/String;)V

    throw p1
.end method

.method public getTransaction()Lee/cyber/smartid/dto/jsonrpc/Transaction;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lee/cyber/smartid/dto/NoSuchTransactionException;
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/util/TransactionFinder;->b:Lee/cyber/smartid/dto/jsonrpc/Transaction;

    if-eqz v0, :cond_0

    return-object v0

    .line 2
    :cond_0
    new-instance v0, Lee/cyber/smartid/dto/NoSuchTransactionException;

    iget-object v1, p0, Lee/cyber/smartid/util/TransactionFinder;->a:Lee/cyber/smartid/inter/ServiceAccess;

    invoke-interface {v1}, Lee/cyber/smartid/inter/ServiceAccess;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    sget v2, Lee/cyber/smartid/R$string;->err_no_such_transaction:I

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-wide/16 v2, 0x3ee

    invoke-direct {v0, v2, v3, v1}, Lee/cyber/smartid/dto/NoSuchTransactionException;-><init>(JLjava/lang/String;)V

    throw v0
.end method
