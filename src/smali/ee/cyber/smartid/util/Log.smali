.class public Lee/cyber/smartid/util/Log;
.super Ljava/lang/Object;
.source "Log.java"

# interfaces
.implements Lee/cyber/smartid/tse/inter/LogAccess;


# static fields
.field public static LOG_LEVEL_DEBUG:Ljava/lang/String; = "debug"

.field public static LOG_LEVEL_RELEASE_ERRORS_ONLY:Ljava/lang/String; = "release_errors"

.field public static LOG_LEVEL_RELEASE_SILENT:Ljava/lang/String; = "release_silent"

.field private static a:Ljava/lang/String;


# instance fields
.field private b:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1
    sget-object v0, Lee/cyber/smartid/util/Log;->LOG_LEVEL_RELEASE_SILENT:Ljava/lang/String;

    sput-object v0, Lee/cyber/smartid/util/Log;->a:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    sget-object v0, Lee/cyber/smartid/util/Log;->a:Ljava/lang/String;

    invoke-static {v0}, Lee/cyber/smartid/util/Log;->setLogLevel(Ljava/lang/String;)V

    .line 3
    iput-object p1, p0, Lee/cyber/smartid/util/Log;->b:Ljava/lang/String;

    return-void
.end method

.method public static getInstance(Ljava/lang/Class;)Lee/cyber/smartid/util/Log;
    .locals 0

    .line 2
    invoke-virtual {p0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object p0

    invoke-static {p0}, Lee/cyber/smartid/util/Log;->getInstance(Ljava/lang/String;)Lee/cyber/smartid/util/Log;

    move-result-object p0

    return-object p0
.end method

.method public static getInstance(Ljava/lang/Object;)Lee/cyber/smartid/util/Log;
    .locals 0

    .line 1
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p0

    invoke-static {p0}, Lee/cyber/smartid/util/Log;->getInstance(Ljava/lang/Class;)Lee/cyber/smartid/util/Log;

    move-result-object p0

    return-object p0
.end method

.method public static getInstance(Ljava/lang/String;)Lee/cyber/smartid/util/Log;
    .locals 1

    .line 3
    new-instance v0, Lee/cyber/smartid/util/Log;

    invoke-direct {v0, p0}, Lee/cyber/smartid/util/Log;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static setLogLevel(Ljava/lang/String;)V
    .locals 1

    .line 1
    sget-object v0, Lee/cyber/smartid/util/Log;->a:Ljava/lang/String;

    invoke-static {v0, p0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2
    invoke-static {}, Ld/a/a;->a()V

    .line 3
    sget-object v0, Lee/cyber/smartid/util/Log;->LOG_LEVEL_DEBUG:Ljava/lang/String;

    invoke-virtual {v0, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 4
    new-instance v0, Ld/a/a$a;

    invoke-direct {v0}, Ld/a/a$a;-><init>()V

    invoke-static {v0}, Ld/a/a;->a(Ld/a/a$b;)V

    goto :goto_0

    .line 5
    :cond_0
    sget-object v0, Lee/cyber/smartid/util/Log;->LOG_LEVEL_RELEASE_ERRORS_ONLY:Ljava/lang/String;

    invoke-virtual {v0, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 6
    new-instance v0, Lee/cyber/smartid/util/ErrorTree;

    invoke-direct {v0}, Lee/cyber/smartid/util/ErrorTree;-><init>()V

    invoke-static {v0}, Ld/a/a;->a(Ld/a/a$b;)V

    .line 7
    :cond_1
    :goto_0
    sput-object p0, Lee/cyber/smartid/util/Log;->a:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public d(Ljava/lang/String;)V
    .locals 1

    const/4 v0, 0x0

    .line 1
    invoke-virtual {p0, p1, v0}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;Ljava/lang/Throwable;)V

    return-void
.end method

.method public d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 0

    .line 3
    invoke-static {p1}, Ld/a/a;->a(Ljava/lang/String;)Ld/a/a$b;

    const/4 p1, 0x0

    if-nez p3, :cond_0

    .line 4
    new-array p1, p1, [Ljava/lang/Object;

    invoke-static {p2, p1}, Ld/a/a;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 5
    :cond_0
    new-array p1, p1, [Ljava/lang/Object;

    invoke-static {p3, p2, p1}, Ld/a/a;->a(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    :goto_0
    return-void
.end method

.method public d(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 1

    .line 2
    iget-object v0, p0, Lee/cyber/smartid/util/Log;->b:Ljava/lang/String;

    invoke-virtual {p0, v0, p1, p2}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    return-void
.end method

.method public e(Ljava/lang/String;)V
    .locals 1

    const/4 v0, 0x0

    .line 1
    invoke-virtual {p0, p1, v0}, Lee/cyber/smartid/util/Log;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    return-void
.end method

.method public e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 0

    .line 3
    invoke-static {p1}, Ld/a/a;->a(Ljava/lang/String;)Ld/a/a$b;

    const/4 p1, 0x0

    if-nez p3, :cond_0

    .line 4
    new-array p1, p1, [Ljava/lang/Object;

    invoke-static {p2, p1}, Ld/a/a;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 5
    :cond_0
    new-array p1, p1, [Ljava/lang/Object;

    invoke-static {p3, p2, p1}, Ld/a/a;->d(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    :goto_0
    return-void
.end method

.method public e(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 1

    .line 2
    iget-object v0, p0, Lee/cyber/smartid/util/Log;->b:Ljava/lang/String;

    invoke-virtual {p0, v0, p1, p2}, Lee/cyber/smartid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    return-void
.end method

.method public i(Ljava/lang/String;)V
    .locals 1

    const/4 v0, 0x0

    .line 1
    invoke-virtual {p0, p1, v0}, Lee/cyber/smartid/util/Log;->i(Ljava/lang/String;Ljava/lang/Throwable;)V

    return-void
.end method

.method public i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 0

    .line 3
    invoke-static {p1}, Ld/a/a;->a(Ljava/lang/String;)Ld/a/a$b;

    const/4 p1, 0x0

    if-nez p3, :cond_0

    .line 4
    new-array p1, p1, [Ljava/lang/Object;

    invoke-static {p2, p1}, Ld/a/a;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 5
    :cond_0
    new-array p1, p1, [Ljava/lang/Object;

    invoke-static {p3, p2, p1}, Ld/a/a;->b(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    :goto_0
    return-void
.end method

.method public i(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 1

    .line 2
    iget-object v0, p0, Lee/cyber/smartid/util/Log;->b:Ljava/lang/String;

    invoke-virtual {p0, v0, p1, p2}, Lee/cyber/smartid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    return-void
.end method

.method public w(Ljava/lang/String;)V
    .locals 1

    const/4 v0, 0x0

    .line 1
    invoke-virtual {p0, p1, v0}, Lee/cyber/smartid/util/Log;->w(Ljava/lang/String;Ljava/lang/Throwable;)V

    return-void
.end method

.method public w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 0

    .line 3
    invoke-static {p1}, Ld/a/a;->a(Ljava/lang/String;)Ld/a/a$b;

    const/4 p1, 0x0

    if-nez p3, :cond_0

    .line 4
    new-array p1, p1, [Ljava/lang/Object;

    invoke-static {p2, p1}, Ld/a/a;->c(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 5
    :cond_0
    new-array p1, p1, [Ljava/lang/Object;

    invoke-static {p3, p2, p1}, Ld/a/a;->c(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    :goto_0
    return-void
.end method

.method public w(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 1

    .line 2
    iget-object v0, p0, Lee/cyber/smartid/util/Log;->b:Ljava/lang/String;

    invoke-virtual {p0, v0, p1, p2}, Lee/cyber/smartid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    return-void
.end method

.method public wtf(Ljava/lang/String;)V
    .locals 1

    const/4 v0, 0x0

    .line 1
    invoke-virtual {p0, p1, v0}, Lee/cyber/smartid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/Throwable;)V

    return-void
.end method

.method public wtf(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 0

    .line 3
    invoke-static {p1}, Ld/a/a;->a(Ljava/lang/String;)Ld/a/a$b;

    const/4 p1, 0x0

    if-nez p3, :cond_0

    .line 4
    new-array p1, p1, [Ljava/lang/Object;

    invoke-static {p2, p1}, Ld/a/a;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 5
    :cond_0
    new-array p1, p1, [Ljava/lang/Object;

    invoke-static {p3, p2, p1}, Ld/a/a;->e(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    :goto_0
    return-void
.end method

.method public wtf(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 1

    .line 2
    iget-object v0, p0, Lee/cyber/smartid/util/Log;->b:Ljava/lang/String;

    invoke-virtual {p0, v0, p1, p2}, Lee/cyber/smartid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    return-void
.end method
