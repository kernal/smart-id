.class public interface abstract Lee/cyber/smartid/manager/inter/StoredAccountManager;
.super Ljava/lang/Object;
.source "StoredAccountManager.java"


# virtual methods
.method public abstract addAndStoreKeyUUIDsToAccountStateIfNeeded(Ljava/lang/String;Ljava/util/ArrayList;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList<",
            "Lee/cyber/smartid/dto/jsonrpc/ServiceAccountKeyStatus;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract getAccountStateUpdateLock()Ljava/lang/Object;
.end method

.method public abstract getAccountUUIDs()Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getAccounts(Ljava/lang/String;Lee/cyber/smartid/inter/GetAccountsListener;)V
.end method

.method public abstract getAllUsableAccountUUIDs()Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getAllUsableKeyIds()Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getAllUsableKeyIdsWithAccountUUIDAndKeyType()Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Lee/cyber/smartid/tse/dto/Triple<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end method

.method public abstract getKeyTypeForKeyUUID(Ljava/lang/String;)Ljava/lang/String;
.end method

.method public abstract initializeAccountState(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/dto/jsonrpc/result/RegisterAccountResult;Lee/cyber/smartid/tse/dto/jsonrpc/resp/BaseResp;Lee/cyber/smartid/tse/dto/jsonrpc/resp/BaseResp;Ljava/lang/String;)Lee/cyber/smartid/dto/AccountState;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lee/cyber/smartid/cryptolib/dto/StorageException;
        }
    .end annotation
.end method

.method public abstract loadAccountFromStorage(Ljava/lang/String;)Lee/cyber/smartid/dto/AccountState;
.end method

.method public abstract loadAccountFromStorageByKeyId(Ljava/lang/String;)Lee/cyber/smartid/dto/AccountState;
.end method

.method public abstract loadAccountsFromStorage()Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Lee/cyber/smartid/dto/AccountState;",
            ">;"
        }
    .end annotation
.end method

.method public abstract loadAllUsableAccountsFromStorage()Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Lee/cyber/smartid/dto/AccountState;",
            ">;"
        }
    .end annotation
.end method

.method public abstract storeAccountToStorage(Lee/cyber/smartid/dto/AccountState;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lee/cyber/smartid/cryptolib/dto/StorageException;
        }
    .end annotation
.end method

.method public abstract storeAccountsToStorage(Ljava/util/ArrayList;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Lee/cyber/smartid/dto/AccountState;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lee/cyber/smartid/cryptolib/dto/StorageException;
        }
    .end annotation
.end method
