.class public interface abstract Lee/cyber/smartid/manager/inter/DevicePropertiesManager;
.super Ljava/lang/Object;
.source "DevicePropertiesManager.java"


# static fields
.field public static final VALUE_LTR:Ljava/lang/String; = "LTR"

.field public static final VALUE_NFC_SUPPORTED:Ljava/lang/String; = "NFC_supported"

.field public static final VALUE_NFC_UNSUPPORTED:Ljava/lang/String; = "NFC_unsupported"

.field public static final VALUE_RTL:Ljava/lang/String; = "RTL"


# virtual methods
.method public abstract getDeviceApiVersion()Ljava/lang/String;
.end method

.method public abstract getDeviceBuildVersion()Ljava/lang/String;
.end method

.method public abstract getDeviceCapabilityList(Landroid/content/Context;)Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getDeviceCodeName()Ljava/lang/String;
.end method

.method public abstract getDeviceManufacturer()Ljava/lang/String;
.end method

.method public abstract getDeviceModel()Ljava/lang/String;
.end method

.method public abstract getDeviceScreenDensityInDpi(Landroid/content/Context;)Ljava/lang/String;
.end method

.method public abstract getDeviceScreenHeightInPx(Landroid/content/Context;)Ljava/lang/String;
.end method

.method public abstract getDeviceScreenWidthInPx(Landroid/content/Context;)Ljava/lang/String;
.end method

.method public abstract getProductCodeName()Ljava/lang/String;
.end method
