.class public interface abstract Lee/cyber/smartid/manager/inter/SafetyNetManager;
.super Ljava/lang/Object;
.source "SafetyNetManager.java"


# virtual methods
.method public abstract getLastSafetyNetAttestation()Lee/cyber/smartid/dto/SafetyNetAttestation;
.end method

.method public abstract getSafetyNetAttestation(Ljava/lang/String;ZLee/cyber/smartid/inter/GetSafetyNetAttestationListener;)V
.end method

.method public abstract getSafetyNetResultHash()Ljava/lang/String;
.end method

.method public abstract isSafetyNetCheckInProgress()Z
.end method

.method public abstract setSafetyNetResultHash(Ljava/lang/String;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lee/cyber/smartid/cryptolib/dto/StorageException;
        }
    .end annotation
.end method

.method public abstract shouldStartSafetyNetCheckPostInit()Z
.end method

.method public abstract startPostInitSafetyNetCheckIfNeeded()Z
.end method

.method public abstract startSafetyNetCheckAsync()V
.end method
