.class public interface abstract Lee/cyber/smartid/manager/inter/VerificationCodeEvaluator;
.super Ljava/lang/Object;
.source "VerificationCodeEvaluator.java"


# virtual methods
.method public abstract isValidVerificationCodeCandidate(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;I)Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;I)Z"
        }
    .end annotation
.end method

.method public abstract isValidVerificationCodePairBasedOnLevenshteinDistance(ILjava/lang/String;Ljava/lang/String;)Z
.end method
