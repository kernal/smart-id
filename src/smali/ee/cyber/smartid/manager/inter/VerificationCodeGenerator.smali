.class public interface abstract Lee/cyber/smartid/manager/inter/VerificationCodeGenerator;
.super Ljava/lang/Object;
.source "VerificationCodeGenerator.java"


# virtual methods
.method public abstract generateRealAndFakeVerificationCodes(Ljava/lang/String;II)Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "II)",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lee/cyber/smartid/dto/VerificationCodeException;
        }
    .end annotation
.end method

.method public abstract isValidVerificationCode(Ljava/lang/String;Ljava/lang/String;)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lee/cyber/smartid/dto/VerificationCodeException;
        }
    .end annotation
.end method
