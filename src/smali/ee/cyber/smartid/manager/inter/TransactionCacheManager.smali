.class public interface abstract Lee/cyber/smartid/manager/inter/TransactionCacheManager;
.super Ljava/lang/Object;
.source "TransactionCacheManager.java"


# virtual methods
.method public abstract cacheTransaction(Lee/cyber/smartid/dto/jsonrpc/Transaction;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lee/cyber/smartid/cryptolib/dto/StorageException;
        }
    .end annotation
.end method

.method public abstract getCachedTransaction(Ljava/lang/String;)Lee/cyber/smartid/dto/jsonrpc/Transaction;
.end method

.method public abstract removeCachedTransactionByAccountUUID(Ljava/lang/String;)V
.end method

.method public abstract removeExpiredTransactionsFromCache()V
.end method

.method public abstract setNextRemoveExpiredTransactionsAlarm()V
.end method
