.class public interface abstract Lee/cyber/smartid/manager/inter/TransactionAndRPRequestManager;
.super Ljava/lang/Object;
.source "TransactionAndRPRequestManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lee/cyber/smartid/manager/inter/TransactionAndRPRequestManager$CancelTransactionReason;
    }
.end annotation


# static fields
.field public static final CANCEL_TRANSACTION_REASON_USER_REFUSED:Ljava/lang/String; = "USER_REFUSED"

.field public static final CANCEL_TRANSACTION_REASON_WRONG_VERIFICATION_CODE:Ljava/lang/String; = "WRONG_VC"


# virtual methods
.method public abstract cancelRPRequest(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/inter/CancelRPRequestListener;)V
.end method

.method public abstract cancelTransaction(Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/inter/CancelTransactionListener;)V
.end method

.method public abstract confirmRPRequest(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/inter/ConfirmRPRequestListener;)V
.end method

.method public abstract confirmTransaction(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/inter/ConfirmTransactionListener;)V
.end method

.method public abstract createTransactionForRPRequest(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/inter/CreateTransactionForRPRequestListener;)V
.end method

.method public abstract getPendingOperation(Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/inter/GetPendingOperationListener;)V
.end method

.method public abstract getRPRequest(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/inter/GetRPRequestListener;)V
.end method

.method public abstract getTransaction(Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/inter/GetTransactionListener;)V
.end method

.method public abstract verifyTransactionVerificationCode(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/inter/VerifyTransactionVerificationCodeListener;)V
.end method
