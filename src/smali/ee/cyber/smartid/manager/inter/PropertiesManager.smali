.class public interface abstract Lee/cyber/smartid/manager/inter/PropertiesManager;
.super Ljava/lang/Object;
.source "PropertiesManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lee/cyber/smartid/manager/inter/PropertiesManager$PropSafetyNetAutomaticRequestMode;
    }
.end annotation


# static fields
.field public static final DEFAULT_MULTI_CODE_VERIFICATION_FAKE_CODE_COUNT:Ljava/lang/Integer;

.field public static final DEFAULT_MULTI_CODE_VERIFICATION_FAKE_CODE_MIN_LEVENSHTEIN_DISTANCE:Ljava/lang/Integer;

.field public static final MAXIMUM_MULTI_CODE_VERIFICATION_FAKE_CODE_COUNT:Ljava/lang/Integer;

.field public static final MAXIMUM_MULTI_CODE_VERIFICATION_FAKE_CODE_MIN_LEVENSHTEIN_DISTANCE:Ljava/lang/Integer;

.field public static final MINIMUM_MULTI_CODE_VERIFICATION_FAKE_CODE_COUNT:Ljava/lang/Integer;

.field public static final MINIMUM_MULTI_CODE_VERIFICATION_FAKE_CODE_MIN_LEVENSHTEIN_DISTANCE:Ljava/lang/Integer;

.field public static final PROP_KEY_ACCOUNT_REGISTRATION_SZ_ID:Ljava/lang/String; = "accountRegistrationSZId"

.field public static final PROP_KEY_ACCOUNT_UPGRADE_SZ_ID:Ljava/lang/String; = "accountUpgradeSZId"

.field public static final PROP_KEY_DEFAULT_KEY_LENGTH_BITS:Ljava/lang/String; = "defaultKeyLengthBits"

.field public static final PROP_KEY_MULTI_CODE_VERIFICATION_FAKE_CODE_COUNT:Ljava/lang/String; = "multiCodeVerificationFakeCodeCount"

.field public static final PROP_KEY_MULTI_CODE_VERIFICATION_FAKE_CODE_MIN_LEVENSHTEIN_DISTANCE:Ljava/lang/String; = "multiCodeVerificationFakeCodeMinLevenshteinDistance"

.field public static final PROP_KEY_SAFETY_NET_API_KEY:Ljava/lang/String; = "safetyNetAPIKey"

.field public static final PROP_KEY_SAFETY_NET_AUTOMATIC_REQUEST_MODE:Ljava/lang/String; = "safetyNetAutomaticRequestMode"

.field public static final PROP_VALUE_SAFETY_NET_AUTOMATIC_REQUEST_MODE_DISABLE:Ljava/lang/String; = "disable"

.field public static final PROP_VALUE_SAFETY_NET_AUTOMATIC_REQUEST_MODE_INIT_ONLY:Ljava/lang/String; = "initonly"


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v0, 0x1

    .line 1
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lee/cyber/smartid/manager/inter/PropertiesManager;->MINIMUM_MULTI_CODE_VERIFICATION_FAKE_CODE_COUNT:Ljava/lang/Integer;

    const/4 v1, 0x2

    .line 2
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sput-object v1, Lee/cyber/smartid/manager/inter/PropertiesManager;->DEFAULT_MULTI_CODE_VERIFICATION_FAKE_CODE_COUNT:Ljava/lang/Integer;

    const/4 v2, 0x5

    .line 3
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    sput-object v2, Lee/cyber/smartid/manager/inter/PropertiesManager;->MAXIMUM_MULTI_CODE_VERIFICATION_FAKE_CODE_COUNT:Ljava/lang/Integer;

    .line 4
    sput-object v1, Lee/cyber/smartid/manager/inter/PropertiesManager;->DEFAULT_MULTI_CODE_VERIFICATION_FAKE_CODE_MIN_LEVENSHTEIN_DISTANCE:Ljava/lang/Integer;

    .line 5
    sput-object v0, Lee/cyber/smartid/manager/inter/PropertiesManager;->MINIMUM_MULTI_CODE_VERIFICATION_FAKE_CODE_MIN_LEVENSHTEIN_DISTANCE:Ljava/lang/Integer;

    const/4 v0, 0x4

    .line 6
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lee/cyber/smartid/manager/inter/PropertiesManager;->MAXIMUM_MULTI_CODE_VERIFICATION_FAKE_CODE_MIN_LEVENSHTEIN_DISTANCE:Ljava/lang/Integer;

    return-void
.end method


# virtual methods
.method public abstract getPropAccountRegistrationSZId()Ljava/lang/String;
.end method

.method public abstract getPropAccountUpgradeSZId()Ljava/lang/String;
.end method

.method public abstract getPropDefaultKeyLengthBits()Ljava/lang/Integer;
.end method

.method public abstract getPropMultiCodeVerificationFakeCodeCount()Ljava/lang/Integer;
.end method

.method public abstract getPropMultiCodeVerificationFakeCodeMinLevenshteinDistance()Ljava/lang/Integer;
.end method

.method public abstract getPropSafetyNetAPIKey()Ljava/lang/String;
.end method

.method public abstract getPropSafetyNetAutomaticRequestMode()Ljava/lang/String;
.end method

.method public abstract parseProperties()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method
