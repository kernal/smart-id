.class Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$2$1;
.super Ljava/lang/Object;
.source "AddAccountManagerImpl.java"

# interfaces
.implements Lee/cyber/smartid/tse/inter/GenerateDiffieHellmanKeyPairListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$2;->onGenerateDiffieHellmanKeyPairSuccess(Ljava/lang/String;Lee/cyber/smartid/tse/dto/jsonrpc/resp/GenerateDiffieHellmanKeyPairResp;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lee/cyber/smartid/tse/dto/jsonrpc/resp/GenerateDiffieHellmanKeyPairResp;

.field final synthetic b:Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$2;


# direct methods
.method constructor <init>(Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$2;Lee/cyber/smartid/tse/dto/jsonrpc/resp/GenerateDiffieHellmanKeyPairResp;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$2$1;->b:Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$2;

    iput-object p2, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$2$1;->a:Lee/cyber/smartid/tse/dto/jsonrpc/resp/GenerateDiffieHellmanKeyPairResp;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onGenerateDiffieHellmanKeyPairFailed(Ljava/lang/String;Lee/cyber/smartid/tse/dto/TSEError;)V
    .locals 1

    .line 1
    iget-object p1, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$2$1;->b:Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$2;

    iget-object p1, p1, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$2;->h:Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;

    invoke-static {p1}, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;->d(Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;)Lee/cyber/smartid/util/Log;

    move-result-object p1

    const-string v0, "generateDiffieHellmanKeyPairsForAddAccount - fail for sign"

    invoke-virtual {p1, v0}, Lee/cyber/smartid/util/Log;->e(Ljava/lang/String;)V

    .line 2
    iget-object p1, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$2$1;->b:Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$2;

    iget-object p1, p1, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$2;->h:Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;

    invoke-static {p1}, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;->c(Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;)Lee/cyber/smartid/inter/ServiceAccess;

    move-result-object p1

    new-instance v0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$2$1$1;

    invoke-direct {v0, p0, p2}, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$2$1$1;-><init>(Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$2$1;Lee/cyber/smartid/tse/dto/TSEError;)V

    invoke-interface {p1, v0}, Lee/cyber/smartid/inter/ServiceAccess;->notifyUI(Ljava/lang/Runnable;)V

    return-void
.end method

.method public onGenerateDiffieHellmanKeyPairSuccess(Ljava/lang/String;Lee/cyber/smartid/tse/dto/jsonrpc/resp/GenerateDiffieHellmanKeyPairResp;)V
    .locals 10

    .line 1
    iget-object p1, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$2$1;->b:Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$2;

    iget-object p1, p1, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$2;->h:Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;

    invoke-static {p1}, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;->d(Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;)Lee/cyber/smartid/util/Log;

    move-result-object p1

    const-string v0, "generateDiffieHellmanKeyPairsForAddAccount - success for sign"

    invoke-virtual {p1, v0}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    .line 2
    iget-object p1, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$2$1;->b:Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$2;

    iget-object v0, p1, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$2;->h:Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;

    iget-object v1, p1, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$2;->b:Ljava/lang/String;

    iget-object v2, p1, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$2;->c:Ljava/lang/String;

    iget-object v3, p1, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$2;->d:Ljava/lang/String;

    iget-object v4, p1, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$2;->e:Ljava/lang/String;

    iget-object v5, p1, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$2;->f:Ljava/lang/String;

    iget-object v6, p1, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$2;->g:Ljava/lang/String;

    iget-object v7, p1, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$2;->a:Ljava/lang/String;

    iget-object p1, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$2$1;->a:Lee/cyber/smartid/tse/dto/jsonrpc/resp/GenerateDiffieHellmanKeyPairResp;

    invoke-virtual {p1}, Lee/cyber/smartid/tse/dto/jsonrpc/resp/GenerateDiffieHellmanKeyPairResp;->getDiffieHellmanPublicKeyEncoded()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p2}, Lee/cyber/smartid/tse/dto/jsonrpc/resp/GenerateDiffieHellmanKeyPairResp;->getDiffieHellmanPublicKeyEncoded()Ljava/lang/String;

    move-result-object v9

    invoke-static/range {v0 .. v9}, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;->a(Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method
