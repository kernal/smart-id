.class Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$12;
.super Ljava/lang/Object;
.source "AddAccountManagerImpl.java"

# interfaces
.implements Lee/cyber/smartid/inter/DeleteAccountListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;->a(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;


# direct methods
.method constructor <init>(Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$12;->a:Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDeleteAccountFailed(Ljava/lang/String;Lee/cyber/smartid/dto/SmartIdError;)V
    .locals 2

    .line 1
    iget-object p1, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$12;->a:Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;

    invoke-static {p1}, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;->d(Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;)Lee/cyber/smartid/util/Log;

    move-result-object p1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "deleteAccountForAddAccount - onDeleteAccountFailed: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Lee/cyber/smartid/util/Log;->e(Ljava/lang/String;)V

    return-void
.end method

.method public onDeleteAccountSuccess(Ljava/lang/String;Lee/cyber/smartid/dto/jsonrpc/resp/DeleteAccountResp;)V
    .locals 2

    .line 1
    iget-object p1, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$12;->a:Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;

    invoke-static {p1}, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;->d(Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;)Lee/cyber/smartid/util/Log;

    move-result-object p1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "deleteAccountForAddAccount - onDeleteAccountSuccess: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    return-void
.end method
