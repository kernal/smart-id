.class Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$2;
.super Ljava/lang/Object;
.source "TransactionAndRPRequestManagerImpl.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;->a(Lee/cyber/smartid/dto/jsonrpc/result/GetPendingOperationResult;Lee/cyber/smartid/dto/jsonrpc/resp/GetTransactionResp;Ljava/lang/String;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:Lee/cyber/smartid/dto/jsonrpc/result/GetPendingOperationResult;

.field final synthetic c:Lee/cyber/smartid/dto/jsonrpc/resp/GetTransactionResp;

.field final synthetic d:Ljava/lang/String;

.field final synthetic e:Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;


# direct methods
.method constructor <init>(Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;Ljava/lang/String;Lee/cyber/smartid/dto/jsonrpc/result/GetPendingOperationResult;Lee/cyber/smartid/dto/jsonrpc/resp/GetTransactionResp;Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$2;->e:Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;

    iput-object p2, p0, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$2;->a:Ljava/lang/String;

    iput-object p3, p0, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$2;->b:Lee/cyber/smartid/dto/jsonrpc/result/GetPendingOperationResult;

    iput-object p4, p0, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$2;->c:Lee/cyber/smartid/dto/jsonrpc/resp/GetTransactionResp;

    iput-object p5, p0, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$2;->d:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 11

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$2;->e:Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;

    invoke-static {v0}, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;->c(Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;)Lee/cyber/smartid/inter/ListenerAccess;

    move-result-object v0

    iget-object v1, p0, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$2;->a:Ljava/lang/String;

    const-class v2, Lee/cyber/smartid/inter/GetPendingOperationListener;

    const/4 v3, 0x1

    invoke-interface {v0, v1, v3, v2}, Lee/cyber/smartid/inter/ListenerAccess;->getListener(Ljava/lang/String;ZLjava/lang/Class;)Lee/cyber/smartid/inter/ServiceListener;

    move-result-object v0

    check-cast v0, Lee/cyber/smartid/inter/GetPendingOperationListener;

    .line 2
    iget-object v1, p0, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$2;->e:Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;

    invoke-static {v1}, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;->b(Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;)Lee/cyber/smartid/util/Log;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getPendingOperation - onResponse, listener: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    if-nez v0, :cond_0

    return-void

    .line 3
    :cond_0
    iget-object v1, p0, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$2;->b:Lee/cyber/smartid/dto/jsonrpc/result/GetPendingOperationResult;

    invoke-virtual {v1}, Lee/cyber/smartid/dto/jsonrpc/result/TransactionContainerResult;->getTransaction()Lee/cyber/smartid/dto/jsonrpc/Transaction;

    move-result-object v1

    const/4 v2, 0x0

    if-eqz v1, :cond_1

    .line 4
    iget-object v1, p0, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$2;->a:Ljava/lang/String;

    new-instance v3, Lee/cyber/smartid/dto/jsonrpc/resp/GetPendingOperationResp;

    iget-object v4, p0, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$2;->c:Lee/cyber/smartid/dto/jsonrpc/resp/GetTransactionResp;

    invoke-direct {v3, v1, v4, v2}, Lee/cyber/smartid/dto/jsonrpc/resp/GetPendingOperationResp;-><init>(Ljava/lang/String;Lee/cyber/smartid/dto/jsonrpc/resp/GetTransactionResp;Lee/cyber/smartid/dto/jsonrpc/resp/GetRPRequestResp;)V

    invoke-interface {v0, v1, v3}, Lee/cyber/smartid/inter/GetPendingOperationListener;->onGetPendingOperationSuccess(Ljava/lang/String;Lee/cyber/smartid/dto/jsonrpc/resp/GetPendingOperationResp;)V

    goto :goto_0

    .line 5
    :cond_1
    iget-object v1, p0, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$2;->b:Lee/cyber/smartid/dto/jsonrpc/result/GetPendingOperationResult;

    invoke-virtual {v1}, Lee/cyber/smartid/dto/jsonrpc/result/GetPendingOperationResult;->getRpRequest()Lee/cyber/smartid/dto/jsonrpc/RPRequest;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 6
    new-instance v1, Lee/cyber/smartid/dto/jsonrpc/resp/GetRPRequestResp;

    iget-object v4, p0, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$2;->a:Ljava/lang/String;

    iget-object v5, p0, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$2;->d:Ljava/lang/String;

    iget-object v3, p0, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$2;->b:Lee/cyber/smartid/dto/jsonrpc/result/GetPendingOperationResult;

    invoke-virtual {v3}, Lee/cyber/smartid/dto/jsonrpc/result/GetPendingOperationResult;->getRpRequest()Lee/cyber/smartid/dto/jsonrpc/RPRequest;

    move-result-object v3

    invoke-virtual {v3}, Lee/cyber/smartid/dto/jsonrpc/RPRequest;->getRpRequestUUID()Ljava/lang/String;

    move-result-object v6

    iget-object v3, p0, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$2;->b:Lee/cyber/smartid/dto/jsonrpc/result/GetPendingOperationResult;

    invoke-virtual {v3}, Lee/cyber/smartid/dto/jsonrpc/result/GetPendingOperationResult;->getRpRequest()Lee/cyber/smartid/dto/jsonrpc/RPRequest;

    move-result-object v3

    invoke-virtual {v3}, Lee/cyber/smartid/dto/jsonrpc/RPRequest;->getRpName()Ljava/lang/String;

    move-result-object v7

    iget-object v3, p0, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$2;->b:Lee/cyber/smartid/dto/jsonrpc/result/GetPendingOperationResult;

    invoke-virtual {v3}, Lee/cyber/smartid/dto/jsonrpc/result/GetPendingOperationResult;->getRpRequest()Lee/cyber/smartid/dto/jsonrpc/RPRequest;

    move-result-object v3

    invoke-virtual {v3}, Lee/cyber/smartid/dto/jsonrpc/RPRequest;->getRequestType()Ljava/lang/String;

    move-result-object v8

    iget-object v3, p0, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$2;->b:Lee/cyber/smartid/dto/jsonrpc/result/GetPendingOperationResult;

    invoke-virtual {v3}, Lee/cyber/smartid/dto/jsonrpc/result/GetPendingOperationResult;->getRpRequest()Lee/cyber/smartid/dto/jsonrpc/RPRequest;

    move-result-object v3

    invoke-virtual {v3}, Lee/cyber/smartid/dto/jsonrpc/RPRequest;->getTtlSec()J

    move-result-wide v9

    move-object v3, v1

    invoke-direct/range {v3 .. v10}, Lee/cyber/smartid/dto/jsonrpc/resp/GetRPRequestResp;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;J)V

    .line 7
    iget-object v3, p0, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$2;->a:Ljava/lang/String;

    new-instance v4, Lee/cyber/smartid/dto/jsonrpc/resp/GetPendingOperationResp;

    invoke-direct {v4, v3, v2, v1}, Lee/cyber/smartid/dto/jsonrpc/resp/GetPendingOperationResp;-><init>(Ljava/lang/String;Lee/cyber/smartid/dto/jsonrpc/resp/GetTransactionResp;Lee/cyber/smartid/dto/jsonrpc/resp/GetRPRequestResp;)V

    invoke-interface {v0, v3, v4}, Lee/cyber/smartid/inter/GetPendingOperationListener;->onGetPendingOperationSuccess(Ljava/lang/String;Lee/cyber/smartid/dto/jsonrpc/resp/GetPendingOperationResp;)V

    :cond_2
    :goto_0
    return-void
.end method
