.class public Lee/cyber/smartid/manager/impl/TSEErrorToServiceErrorMapperImpl;
.super Ljava/lang/Object;
.source "TSEErrorToServiceErrorMapperImpl.java"

# interfaces
.implements Lee/cyber/smartid/manager/inter/TSEErrorToServiceErrorMapper;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lee/cyber/smartid/manager/impl/TSEErrorToServiceErrorMapperImpl$ServiceAccountKeyStatusWrapper;
    }
.end annotation


# instance fields
.field private final a:Lee/cyber/smartid/inter/ServiceAccess;

.field private final b:Lee/cyber/smartid/tse/inter/WallClock;

.field private c:Lee/cyber/smartid/util/Log;


# direct methods
.method public constructor <init>(Lee/cyber/smartid/inter/ServiceAccess;Lee/cyber/smartid/tse/inter/WallClock;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    invoke-static {p0}, Lee/cyber/smartid/util/Log;->getInstance(Ljava/lang/Object;)Lee/cyber/smartid/util/Log;

    move-result-object v0

    iput-object v0, p0, Lee/cyber/smartid/manager/impl/TSEErrorToServiceErrorMapperImpl;->c:Lee/cyber/smartid/util/Log;

    .line 3
    iput-object p1, p0, Lee/cyber/smartid/manager/impl/TSEErrorToServiceErrorMapperImpl;->a:Lee/cyber/smartid/inter/ServiceAccess;

    .line 4
    iput-object p2, p0, Lee/cyber/smartid/manager/impl/TSEErrorToServiceErrorMapperImpl;->b:Lee/cyber/smartid/tse/inter/WallClock;

    return-void
.end method


# virtual methods
.method a(Lee/cyber/smartid/dto/SmartIdError;)V
    .locals 4

    .line 1
    invoke-virtual {p1}, Lee/cyber/smartid/dto/SmartIdError;->getServiceAccountKeyStatus()Lee/cyber/smartid/dto/jsonrpc/ServiceAccountKeyStatus;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lee/cyber/smartid/dto/SmartIdError;->getServiceAccountKeyStatus()Lee/cyber/smartid/dto/jsonrpc/ServiceAccountKeyStatus;

    move-result-object v0

    invoke-virtual {v0}, Lee/cyber/smartid/dto/jsonrpc/ServiceAccountKeyStatus;->getKeyUUID()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lee/cyber/smartid/tse/dto/BaseError;->getRawErrorData()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 2
    :cond_0
    new-instance v0, Lcom/google/gson/g;

    invoke-direct {v0}, Lcom/google/gson/g;-><init>()V

    invoke-virtual {v0}, Lcom/google/gson/g;->a()Lcom/google/gson/f;

    move-result-object v0

    invoke-virtual {p1}, Lee/cyber/smartid/tse/dto/BaseError;->getRawErrorData()Ljava/lang/String;

    move-result-object v1

    const-class v2, Lee/cyber/smartid/manager/impl/TSEErrorToServiceErrorMapperImpl$ServiceAccountKeyStatusWrapper;

    invoke-virtual {v0, v1, v2}, Lcom/google/gson/f;->a(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lee/cyber/smartid/manager/impl/TSEErrorToServiceErrorMapperImpl$ServiceAccountKeyStatusWrapper;

    .line 3
    iget-object v1, v0, Lee/cyber/smartid/manager/impl/TSEErrorToServiceErrorMapperImpl$ServiceAccountKeyStatusWrapper;->a:Lee/cyber/smartid/dto/jsonrpc/ServiceAccountKeyStatus;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lee/cyber/smartid/dto/jsonrpc/ServiceAccountKeyStatus;->getKeyUUID()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    goto :goto_0

    .line 4
    :cond_1
    iget-object v0, v0, Lee/cyber/smartid/manager/impl/TSEErrorToServiceErrorMapperImpl$ServiceAccountKeyStatusWrapper;->a:Lee/cyber/smartid/dto/jsonrpc/ServiceAccountKeyStatus;

    invoke-virtual {v0}, Lee/cyber/smartid/dto/jsonrpc/ServiceAccountKeyStatus;->getKeyUUID()Ljava/lang/String;

    move-result-object v0

    .line 5
    invoke-virtual {p1}, Lee/cyber/smartid/dto/SmartIdError;->getServiceAccountKeyStatus()Lee/cyber/smartid/dto/jsonrpc/ServiceAccountKeyStatus;

    move-result-object v1

    .line 6
    invoke-virtual {v1, v0}, Lee/cyber/smartid/dto/jsonrpc/ServiceAccountKeyStatus;->setKeyUUID(Ljava/lang/String;)V

    .line 7
    invoke-virtual {p1}, Lee/cyber/smartid/tse/dto/BaseError;->createOrGetExtras()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "ee.cyber.smartid.EXTRA_SERVICE_ACCOUNT_KEY_STATUS_INFO"

    invoke-virtual {v2, v3, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 8
    iget-object v1, p0, Lee/cyber/smartid/manager/impl/TSEErrorToServiceErrorMapperImpl;->c:Lee/cyber/smartid/util/Log;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "addKeyUUIDToServiceAccountKeyStatusIfNeeded: Added keyUUID "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, " to error "

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lee/cyber/smartid/dto/SmartIdError;->getHumanReadableErrorCodeName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    :cond_2
    :goto_0
    return-void
.end method

.method b(Lee/cyber/smartid/dto/SmartIdError;)V
    .locals 5

    .line 1
    invoke-virtual {p1}, Lee/cyber/smartid/dto/SmartIdError;->getServiceAccountKeyStatus()Lee/cyber/smartid/dto/jsonrpc/ServiceAccountKeyStatus;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lee/cyber/smartid/dto/SmartIdError;->getServiceAccountKeyStatus()Lee/cyber/smartid/dto/jsonrpc/ServiceAccountKeyStatus;

    move-result-object v0

    invoke-virtual {v0}, Lee/cyber/smartid/dto/jsonrpc/ServiceAccountKeyStatus;->getKeyType()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lee/cyber/smartid/dto/SmartIdError;->getServiceAccountKeyStatus()Lee/cyber/smartid/dto/jsonrpc/ServiceAccountKeyStatus;

    move-result-object v0

    invoke-virtual {v0}, Lee/cyber/smartid/dto/jsonrpc/ServiceAccountKeyStatus;->getKeyUUID()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 2
    :cond_0
    invoke-virtual {p1}, Lee/cyber/smartid/dto/SmartIdError;->getServiceAccountKeyStatus()Lee/cyber/smartid/dto/jsonrpc/ServiceAccountKeyStatus;

    move-result-object v0

    invoke-virtual {v0}, Lee/cyber/smartid/dto/jsonrpc/ServiceAccountKeyStatus;->getKeyUUID()Ljava/lang/String;

    move-result-object v0

    .line 3
    iget-object v1, p0, Lee/cyber/smartid/manager/impl/TSEErrorToServiceErrorMapperImpl;->a:Lee/cyber/smartid/inter/ServiceAccess;

    invoke-interface {v1}, Lee/cyber/smartid/inter/ServiceAccess;->getStoredAccountManager()Lee/cyber/smartid/manager/inter/StoredAccountManager;

    move-result-object v1

    invoke-interface {v1, v0}, Lee/cyber/smartid/manager/inter/StoredAccountManager;->getKeyTypeForKeyUUID(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 4
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 5
    invoke-virtual {p1}, Lee/cyber/smartid/dto/SmartIdError;->getServiceAccountKeyStatus()Lee/cyber/smartid/dto/jsonrpc/ServiceAccountKeyStatus;

    move-result-object v2

    .line 6
    invoke-virtual {v2, v1}, Lee/cyber/smartid/dto/jsonrpc/ServiceAccountKeyStatus;->setKeyType(Ljava/lang/String;)V

    .line 7
    invoke-virtual {p1}, Lee/cyber/smartid/tse/dto/BaseError;->createOrGetExtras()Landroid/os/Bundle;

    move-result-object v3

    const-string v4, "ee.cyber.smartid.EXTRA_SERVICE_ACCOUNT_KEY_STATUS_INFO"

    invoke-virtual {v3, v4, v2}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 8
    iget-object v2, p0, Lee/cyber/smartid/manager/impl/TSEErrorToServiceErrorMapperImpl;->c:Lee/cyber/smartid/util/Log;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "addKeyTypeToServiceAccountKeyStatusIfNeeded: Added keyType "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " to error "

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lee/cyber/smartid/dto/SmartIdError;->getHumanReadableErrorCodeName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, " for key with UUID "

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v2, p1}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    :cond_1
    :goto_0
    return-void
.end method

.method public map(Lee/cyber/smartid/tse/dto/BaseError;)Lee/cyber/smartid/dto/SmartIdError;
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/manager/impl/TSEErrorToServiceErrorMapperImpl;->b:Lee/cyber/smartid/tse/inter/WallClock;

    invoke-static {v0, p1}, Lee/cyber/smartid/dto/SmartIdError;->from(Lee/cyber/smartid/tse/inter/WallClock;Lee/cyber/smartid/tse/dto/BaseError;)Lee/cyber/smartid/dto/SmartIdError;

    move-result-object p1

    .line 2
    invoke-virtual {p0, p1}, Lee/cyber/smartid/manager/impl/TSEErrorToServiceErrorMapperImpl;->a(Lee/cyber/smartid/dto/SmartIdError;)V

    .line 3
    invoke-virtual {p0, p1}, Lee/cyber/smartid/manager/impl/TSEErrorToServiceErrorMapperImpl;->b(Lee/cyber/smartid/dto/SmartIdError;)V

    return-object p1
.end method
