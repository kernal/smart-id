.class Lee/cyber/smartid/manager/impl/DeleteAccountManagerImpl$2;
.super Ljava/lang/Object;
.source "DeleteAccountManagerImpl.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lee/cyber/smartid/manager/impl/DeleteAccountManagerImpl;->a(Ljava/lang/String;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:Lee/cyber/smartid/cryptolib/dto/StorageException;

.field final synthetic c:Lee/cyber/smartid/manager/impl/DeleteAccountManagerImpl;


# direct methods
.method constructor <init>(Lee/cyber/smartid/manager/impl/DeleteAccountManagerImpl;Ljava/lang/String;Lee/cyber/smartid/cryptolib/dto/StorageException;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lee/cyber/smartid/manager/impl/DeleteAccountManagerImpl$2;->c:Lee/cyber/smartid/manager/impl/DeleteAccountManagerImpl;

    iput-object p2, p0, Lee/cyber/smartid/manager/impl/DeleteAccountManagerImpl$2;->a:Ljava/lang/String;

    iput-object p3, p0, Lee/cyber/smartid/manager/impl/DeleteAccountManagerImpl$2;->b:Lee/cyber/smartid/cryptolib/dto/StorageException;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/manager/impl/DeleteAccountManagerImpl$2;->c:Lee/cyber/smartid/manager/impl/DeleteAccountManagerImpl;

    invoke-static {v0}, Lee/cyber/smartid/manager/impl/DeleteAccountManagerImpl;->b(Lee/cyber/smartid/manager/impl/DeleteAccountManagerImpl;)Lee/cyber/smartid/inter/ListenerAccess;

    move-result-object v0

    iget-object v1, p0, Lee/cyber/smartid/manager/impl/DeleteAccountManagerImpl$2;->c:Lee/cyber/smartid/manager/impl/DeleteAccountManagerImpl;

    invoke-static {v1}, Lee/cyber/smartid/manager/impl/DeleteAccountManagerImpl;->b(Lee/cyber/smartid/manager/impl/DeleteAccountManagerImpl;)Lee/cyber/smartid/inter/ListenerAccess;

    move-result-object v1

    iget-object v2, p0, Lee/cyber/smartid/manager/impl/DeleteAccountManagerImpl$2;->a:Ljava/lang/String;

    const-class v3, Lee/cyber/smartid/inter/DeleteAccountListener;

    const/4 v4, 0x1

    invoke-interface {v1, v2, v4, v3}, Lee/cyber/smartid/inter/ListenerAccess;->getListener(Ljava/lang/String;ZLjava/lang/Class;)Lee/cyber/smartid/inter/ServiceListener;

    move-result-object v1

    iget-object v2, p0, Lee/cyber/smartid/manager/impl/DeleteAccountManagerImpl$2;->a:Ljava/lang/String;

    iget-object v3, p0, Lee/cyber/smartid/manager/impl/DeleteAccountManagerImpl$2;->c:Lee/cyber/smartid/manager/impl/DeleteAccountManagerImpl;

    invoke-static {v3}, Lee/cyber/smartid/manager/impl/DeleteAccountManagerImpl;->c(Lee/cyber/smartid/manager/impl/DeleteAccountManagerImpl;)Lee/cyber/smartid/inter/ServiceAccess;

    move-result-object v3

    invoke-interface {v3}, Lee/cyber/smartid/inter/ServiceAccess;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    iget-object v4, p0, Lee/cyber/smartid/manager/impl/DeleteAccountManagerImpl$2;->c:Lee/cyber/smartid/manager/impl/DeleteAccountManagerImpl;

    invoke-static {v4}, Lee/cyber/smartid/manager/impl/DeleteAccountManagerImpl;->d(Lee/cyber/smartid/manager/impl/DeleteAccountManagerImpl;)Lee/cyber/smartid/tse/inter/WallClock;

    move-result-object v4

    iget-object v5, p0, Lee/cyber/smartid/manager/impl/DeleteAccountManagerImpl$2;->b:Lee/cyber/smartid/cryptolib/dto/StorageException;

    invoke-static {v3, v4, v5}, Lee/cyber/smartid/dto/SmartIdError;->from(Landroid/content/Context;Lee/cyber/smartid/tse/inter/WallClock;Ljava/lang/Throwable;)Lee/cyber/smartid/dto/SmartIdError;

    move-result-object v3

    invoke-interface {v0, v1, v2, v3}, Lee/cyber/smartid/inter/ListenerAccess;->notifyError(Lee/cyber/smartid/inter/ServiceListener;Ljava/lang/String;Lee/cyber/smartid/dto/SmartIdError;)V

    return-void
.end method
