.class Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$5;
.super Lee/cyber/smartid/tse/network/RPCCallbackNonUIThread;
.source "TransactionAndRPRequestManagerImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;->createTransactionForRPRequest(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/inter/CreateTransactionForRPRequestListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lee/cyber/smartid/tse/network/RPCCallbackNonUIThread<",
        "Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCResponse<",
        "Lee/cyber/smartid/dto/jsonrpc/result/TransactionContainerResult;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:Ljava/lang/String;

.field final synthetic c:Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;


# direct methods
.method constructor <init>(Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCRequest;Lee/cyber/smartid/tse/SmartIdTSE;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$5;->c:Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;

    iput-object p4, p0, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$5;->a:Ljava/lang/String;

    iput-object p5, p0, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$5;->b:Ljava/lang/String;

    invoke-direct {p0, p2, p3}, Lee/cyber/smartid/tse/network/RPCCallbackNonUIThread;-><init>(Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCRequest;Lee/cyber/smartid/tse/SmartIdTSE;)V

    return-void
.end method


# virtual methods
.method public onFailure(Lc/b;Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCError;Ljava/lang/Throwable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lc/b<",
            "Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCResponse<",
            "Lee/cyber/smartid/dto/jsonrpc/result/TransactionContainerResult;",
            ">;>;",
            "Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCError;",
            "Ljava/lang/Throwable;",
            ")V"
        }
    .end annotation

    .line 1
    iget-object p1, p0, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$5;->c:Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;

    invoke-static {p1}, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;->a(Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;)Lee/cyber/smartid/inter/ServiceAccess;

    move-result-object p1

    new-instance v0, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$5$3;

    invoke-direct {v0, p0, p2, p3}, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$5$3;-><init>(Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$5;Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCError;Ljava/lang/Throwable;)V

    invoke-interface {p1, v0}, Lee/cyber/smartid/inter/ServiceAccess;->notifyUI(Ljava/lang/Runnable;)V

    return-void
.end method

.method public onSuccess(Lc/b;Lc/r;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lc/b<",
            "Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCResponse<",
            "Lee/cyber/smartid/dto/jsonrpc/result/TransactionContainerResult;",
            ">;>;",
            "Lc/r<",
            "Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCResponse<",
            "Lee/cyber/smartid/dto/jsonrpc/result/TransactionContainerResult;",
            ">;>;)V"
        }
    .end annotation

    const-string p1, "createTransactionForRPRequest"

    .line 1
    invoke-virtual {p2}, Lc/r;->e()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCResponse;

    invoke-virtual {p2}, Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCResponse;->getResult()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lee/cyber/smartid/dto/jsonrpc/result/TransactionContainerResult;

    .line 2
    :try_start_0
    iget-object v0, p0, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$5;->c:Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;

    invoke-static {v0}, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;->a(Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;)Lee/cyber/smartid/inter/ServiceAccess;

    move-result-object v0

    invoke-interface {v0}, Lee/cyber/smartid/inter/ServiceAccess;->getTransactionRespMapper()Lee/cyber/smartid/manager/inter/TransactionRespMapper;

    move-result-object v0

    iget-object v1, p0, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$5;->a:Ljava/lang/String;

    invoke-virtual {p2}, Lee/cyber/smartid/dto/jsonrpc/result/TransactionContainerResult;->getTransaction()Lee/cyber/smartid/dto/jsonrpc/Transaction;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lee/cyber/smartid/manager/inter/TransactionRespMapper;->map(Ljava/lang/String;Lee/cyber/smartid/dto/jsonrpc/Transaction;)Lee/cyber/smartid/dto/jsonrpc/resp/GetTransactionResp;

    move-result-object v0
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 3
    :try_start_1
    iget-object v1, p0, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$5;->c:Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;

    invoke-static {v1, p2}, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;->a(Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;Lee/cyber/smartid/dto/jsonrpc/result/TransactionContainerResult;)V
    :try_end_1
    .catch Lee/cyber/smartid/cryptolib/dto/StorageException; {:try_start_1 .. :try_end_1} :catch_0

    .line 4
    iget-object p1, p0, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$5;->c:Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;

    iget-object p2, p0, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$5;->b:Ljava/lang/String;

    iget-object v1, p0, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$5;->a:Ljava/lang/String;

    invoke-static {p1, v0, p2, v1}, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;->a(Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;Lee/cyber/smartid/dto/jsonrpc/resp/GetTransactionResp;Ljava/lang/String;Ljava/lang/String;)V

    return-void

    :catch_0
    move-exception p2

    .line 5
    iget-object v0, p0, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$5;->c:Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;

    invoke-static {v0}, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;->b(Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;)Lee/cyber/smartid/util/Log;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lee/cyber/smartid/util/Log;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 6
    iget-object p1, p0, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$5;->c:Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;

    invoke-static {p1}, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;->a(Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;)Lee/cyber/smartid/inter/ServiceAccess;

    move-result-object p1

    new-instance v0, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$5$2;

    invoke-direct {v0, p0, p2}, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$5$2;-><init>(Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$5;Lee/cyber/smartid/cryptolib/dto/StorageException;)V

    invoke-interface {p1, v0}, Lee/cyber/smartid/inter/ServiceAccess;->notifyUI(Ljava/lang/Runnable;)V

    return-void

    :catch_1
    move-exception p2

    .line 7
    iget-object v0, p0, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$5;->c:Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;

    invoke-static {v0}, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;->b(Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;)Lee/cyber/smartid/util/Log;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lee/cyber/smartid/util/Log;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 8
    iget-object p1, p0, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$5;->c:Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;

    invoke-static {p1}, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;->a(Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;)Lee/cyber/smartid/inter/ServiceAccess;

    move-result-object p1

    new-instance v0, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$5$1;

    invoke-direct {v0, p0, p2}, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$5$1;-><init>(Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$5;Ljava/io/IOException;)V

    invoke-interface {p1, v0}, Lee/cyber/smartid/inter/ServiceAccess;->notifyUI(Ljava/lang/Runnable;)V

    return-void
.end method

.method public onValidate(Lc/b;Lc/r;)Z
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lc/b<",
            "Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCResponse<",
            "Lee/cyber/smartid/dto/jsonrpc/result/TransactionContainerResult;",
            ">;>;",
            "Lc/r<",
            "Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCResponse<",
            "Lee/cyber/smartid/dto/jsonrpc/result/TransactionContainerResult;",
            ">;>;)Z"
        }
    .end annotation

    .line 1
    invoke-static {p2}, Lee/cyber/smartid/util/Util;->validateResponse(Lc/r;)Z

    move-result p1

    return p1
.end method
