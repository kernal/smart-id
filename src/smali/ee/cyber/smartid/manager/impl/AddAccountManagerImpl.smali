.class public Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;
.super Ljava/lang/Object;
.source "AddAccountManagerImpl.java"

# interfaces
.implements Lee/cyber/smartid/manager/inter/AddAccountManager;


# static fields
.field private static volatile a:Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;


# instance fields
.field private final b:Lee/cyber/smartid/network/SmartIdAPI;

.field private final c:Lee/cyber/smartid/tse/SmartIdTSE;

.field private final d:Lee/cyber/smartid/inter/ServiceAccess;

.field private final e:Lee/cyber/smartid/inter/ListenerAccess;

.field private final f:Lee/cyber/smartid/tse/inter/WallClock;

.field private g:Lee/cyber/smartid/util/Log;


# direct methods
.method private constructor <init>(Lee/cyber/smartid/network/SmartIdAPI;Lee/cyber/smartid/tse/SmartIdTSE;Lee/cyber/smartid/inter/ServiceAccess;Lee/cyber/smartid/inter/ListenerAccess;Lee/cyber/smartid/tse/inter/WallClock;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    invoke-static {p0}, Lee/cyber/smartid/util/Log;->getInstance(Ljava/lang/Object;)Lee/cyber/smartid/util/Log;

    move-result-object v0

    iput-object v0, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;->g:Lee/cyber/smartid/util/Log;

    .line 3
    iput-object p1, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;->b:Lee/cyber/smartid/network/SmartIdAPI;

    .line 4
    iput-object p2, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;->c:Lee/cyber/smartid/tse/SmartIdTSE;

    .line 5
    iput-object p3, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;->d:Lee/cyber/smartid/inter/ServiceAccess;

    .line 6
    iput-object p4, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;->e:Lee/cyber/smartid/inter/ListenerAccess;

    .line 7
    iput-object p5, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;->f:Lee/cyber/smartid/tse/inter/WallClock;

    return-void
.end method

.method static synthetic a(Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;)Lee/cyber/smartid/inter/ListenerAccess;
    .locals 0

    .line 1
    iget-object p0, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;->e:Lee/cyber/smartid/inter/ListenerAccess;

    return-object p0
.end method

.method private a(Lee/cyber/smartid/dto/AccountState;)V
    .locals 11

    if-nez p1, :cond_0

    .line 26
    iget-object p1, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;->g:Lee/cyber/smartid/util/Log;

    const-string v0, "handleSuccessForAddAccount - account can\'t be null!"

    invoke-virtual {p1, v0}, Lee/cyber/smartid/util/Log;->wtf(Ljava/lang/String;)V

    return-void

    .line 27
    :cond_0
    iget-object v0, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;->g:Lee/cyber/smartid/util/Log;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "handleSuccessForAddAccount - account: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    .line 28
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iget-object v1, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;->e:Lee/cyber/smartid/inter/ListenerAccess;

    new-instance v10, Lee/cyber/smartid/dto/jsonrpc/resp/AddAccountResp;

    invoke-virtual {p1}, Lee/cyber/smartid/dto/AccountState;->getAccountUUID()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1}, Lee/cyber/smartid/dto/AccountState;->getAuthCSRTransactionUUID()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1}, Lee/cyber/smartid/dto/AccountState;->getSignCSRTransactionUUID()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1}, Lee/cyber/smartid/dto/AccountState;->getIdentityDataFromRegistration()Lee/cyber/smartid/dto/jsonrpc/IdentityData;

    move-result-object v7

    iget-object v2, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;->d:Lee/cyber/smartid/inter/ServiceAccess;

    invoke-interface {v2}, Lee/cyber/smartid/inter/ServiceAccess;->getStoredAccountManager()Lee/cyber/smartid/manager/inter/StoredAccountManager;

    move-result-object v2

    invoke-interface {v2}, Lee/cyber/smartid/manager/inter/StoredAccountManager;->loadAccountsFromStorage()Ljava/util/ArrayList;

    move-result-object v2

    invoke-static {v2}, Lee/cyber/smartid/dto/AccountWrapper;->toWrapperList(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v8

    invoke-virtual {p1}, Lee/cyber/smartid/dto/AccountState;->getInitiationType()Ljava/lang/String;

    move-result-object v9

    const/4 v3, 0x0

    move-object v2, v10

    invoke-direct/range {v2 .. v9}, Lee/cyber/smartid/dto/jsonrpc/resp/AddAccountResp;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/dto/jsonrpc/IdentityData;Ljava/util/ArrayList;Ljava/lang/String;)V

    const/4 v2, 0x0

    invoke-virtual {p1, v0, v1, v10, v2}, Lee/cyber/smartid/dto/AccountState;->notifyAndClearSubmitClientSecondPartListeners(Landroid/os/Handler;Lee/cyber/smartid/inter/ListenerAccess;Lee/cyber/smartid/dto/jsonrpc/resp/AddAccountResp;Lee/cyber/smartid/dto/SmartIdError;)V

    .line 29
    :try_start_0
    iget-object v0, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;->d:Lee/cyber/smartid/inter/ServiceAccess;

    invoke-interface {v0}, Lee/cyber/smartid/inter/ServiceAccess;->getStoredAccountManager()Lee/cyber/smartid/manager/inter/StoredAccountManager;

    move-result-object v0

    invoke-interface {v0, p1}, Lee/cyber/smartid/manager/inter/StoredAccountManager;->storeAccountToStorage(Lee/cyber/smartid/dto/AccountState;)V
    :try_end_0
    .catch Lee/cyber/smartid/cryptolib/dto/StorageException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 30
    iget-object v0, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;->g:Lee/cyber/smartid/util/Log;

    const-string v1, "handleSuccessForAddAccount"

    invoke-virtual {v0, v1, p1}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;Ljava/lang/Throwable;)V

    :goto_0
    return-void
.end method

.method static synthetic a(Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;Ljava/lang/String;Lee/cyber/smartid/dto/SmartIdError;Ljava/lang/String;Z)V
    .locals 0

    .line 7
    invoke-direct {p0, p1, p2, p3, p4}, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;->a(Ljava/lang/String;Lee/cyber/smartid/dto/SmartIdError;Ljava/lang/String;Z)V

    return-void
.end method

.method static synthetic a(Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/dto/jsonrpc/param/AccountRegistrationDataParams;Ljava/lang/String;)V
    .locals 0

    .line 5
    invoke-direct/range {p0 .. p5}, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/dto/jsonrpc/param/AccountRegistrationDataParams;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic a(Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/dto/jsonrpc/result/RegisterAccountResult;Lee/cyber/smartid/dto/jsonrpc/param/RegisterDeviceData;Ljava/lang/String;)V
    .locals 0

    .line 6
    invoke-direct/range {p0 .. p6}, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/dto/jsonrpc/result/RegisterAccountResult;Lee/cyber/smartid/dto/jsonrpc/param/RegisterDeviceData;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic a(Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/dto/jsonrpc/result/RegisterAccountResult;Ljava/lang/String;)V
    .locals 0

    .line 2
    invoke-direct/range {p0 .. p5}, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/dto/jsonrpc/result/RegisterAccountResult;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic a(Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/dto/jsonrpc/result/RegisterAccountResult;Ljava/lang/String;Lee/cyber/smartid/tse/dto/jsonrpc/resp/BaseResp;Lee/cyber/smartid/tse/dto/jsonrpc/resp/BaseResp;)V
    .locals 0

    .line 3
    invoke-direct/range {p0 .. p7}, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/dto/jsonrpc/result/RegisterAccountResult;Ljava/lang/String;Lee/cyber/smartid/tse/dto/jsonrpc/resp/BaseResp;Lee/cyber/smartid/tse/dto/jsonrpc/resp/BaseResp;)V

    return-void
.end method

.method static synthetic a(Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 4
    invoke-direct/range {p0 .. p9}, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 7

    .line 45
    iget-object v0, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;->g:Lee/cyber/smartid/util/Log;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "deleteAccountForAddAccount - removing the account "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    .line 46
    iget-object v0, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;->d:Lee/cyber/smartid/inter/ServiceAccess;

    invoke-interface {v0}, Lee/cyber/smartid/inter/ServiceAccess;->getDeleteAccountManager()Lee/cyber/smartid/manager/inter/DeleteAccountManager;

    move-result-object v1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ee.cyber.smartid.TAG_ADD_ACCOUNT_FAILED_DELETE_ACCOUNT_"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v6, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$12;

    invoke-direct {v6, p0}, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$12;-><init>(Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;)V

    const/4 v4, 0x0

    const/4 v5, 0x1

    move-object v3, p1

    invoke-interface/range {v1 .. v6}, Lee/cyber/smartid/manager/inter/DeleteAccountManager;->deleteAccount(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLee/cyber/smartid/inter/DeleteAccountListener;)V

    return-void
.end method

.method private a(Ljava/lang/String;Lee/cyber/smartid/dto/SmartIdError;Ljava/lang/String;Z)V
    .locals 4

    .line 31
    iget-object v0, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;->g:Lee/cyber/smartid/util/Log;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "handleFailureForAddAccount - "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    .line 32
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 33
    iget-object v0, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;->d:Lee/cyber/smartid/inter/ServiceAccess;

    invoke-interface {v0}, Lee/cyber/smartid/inter/ServiceAccess;->getStoredAccountManager()Lee/cyber/smartid/manager/inter/StoredAccountManager;

    move-result-object v0

    invoke-interface {v0}, Lee/cyber/smartid/manager/inter/StoredAccountManager;->getAccountStateUpdateLock()Ljava/lang/Object;

    move-result-object v0

    monitor-enter v0

    .line 34
    :try_start_0
    iget-object v1, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;->d:Lee/cyber/smartid/inter/ServiceAccess;

    invoke-interface {v1}, Lee/cyber/smartid/inter/ServiceAccess;->getStoredAccountManager()Lee/cyber/smartid/manager/inter/StoredAccountManager;

    move-result-object v1

    invoke-interface {v1, p3}, Lee/cyber/smartid/manager/inter/StoredAccountManager;->loadAccountFromStorage(Ljava/lang/String;)Lee/cyber/smartid/dto/AccountState;

    move-result-object v1

    if-nez v1, :cond_0

    .line 35
    iget-object p3, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;->d:Lee/cyber/smartid/inter/ServiceAccess;

    new-instance p4, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$10;

    invoke-direct {p4, p0, p1, p2}, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$10;-><init>(Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;Ljava/lang/String;Lee/cyber/smartid/dto/SmartIdError;)V

    invoke-interface {p3, p4}, Lee/cyber/smartid/inter/ServiceAccess;->notifyUI(Ljava/lang/Runnable;)V

    .line 36
    monitor-exit v0

    return-void

    .line 37
    :cond_0
    new-instance p1, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {p1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iget-object v2, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;->e:Lee/cyber/smartid/inter/ListenerAccess;

    const/4 v3, 0x0

    invoke-virtual {v1, p1, v2, v3, p2}, Lee/cyber/smartid/dto/AccountState;->notifyAndClearSubmitClientSecondPartListeners(Landroid/os/Handler;Lee/cyber/smartid/inter/ListenerAccess;Lee/cyber/smartid/dto/jsonrpc/resp/AddAccountResp;Lee/cyber/smartid/dto/SmartIdError;)V

    .line 38
    invoke-virtual {v1}, Lee/cyber/smartid/dto/AccountState;->resetFailedSubmitClientSecondPartStates()V

    .line 39
    invoke-virtual {v1, v3}, Lee/cyber/smartid/dto/AccountState;->setLastSubmitClientSecondPartError(Lee/cyber/smartid/tse/dto/TSEError;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 40
    :try_start_1
    iget-object p1, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;->d:Lee/cyber/smartid/inter/ServiceAccess;

    invoke-interface {p1}, Lee/cyber/smartid/inter/ServiceAccess;->getStoredAccountManager()Lee/cyber/smartid/manager/inter/StoredAccountManager;

    move-result-object p1

    invoke-interface {p1, v1}, Lee/cyber/smartid/manager/inter/StoredAccountManager;->storeAccountToStorage(Lee/cyber/smartid/dto/AccountState;)V
    :try_end_1
    .catch Lee/cyber/smartid/cryptolib/dto/StorageException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 41
    :try_start_2
    iget-object p2, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;->g:Lee/cyber/smartid/util/Log;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    const-string v1, "handleFailureForAddAccount"

    :try_start_3
    invoke-virtual {p2, v1, p1}, Lee/cyber/smartid/util/Log;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    :goto_0
    if-eqz p4, :cond_1

    .line 42
    invoke-direct {p0, p3}, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;->a(Ljava/lang/String;)V

    .line 43
    :cond_1
    monitor-exit v0

    goto :goto_1

    :catchall_0
    move-exception p1

    monitor-exit v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw p1

    .line 44
    :cond_2
    iget-object p3, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;->d:Lee/cyber/smartid/inter/ServiceAccess;

    new-instance p4, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$11;

    invoke-direct {p4, p0, p1, p2}, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$11;-><init>(Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;Ljava/lang/String;Lee/cyber/smartid/dto/SmartIdError;)V

    invoke-interface {p3, p4}, Lee/cyber/smartid/inter/ServiceAccess;->notifyUI(Ljava/lang/Runnable;)V

    :goto_1
    return-void
.end method

.method private a(Ljava/lang/String;Lee/cyber/smartid/dto/jsonrpc/param/AccountRegistrationDataParams;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 12

    move-object v9, p0

    .line 3
    iget-object v0, v9, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;->g:Lee/cyber/smartid/util/Log;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "requestNewDeviceForAddAccount - "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-object v4, p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    .line 4
    new-instance v0, Lee/cyber/smartid/dto/jsonrpc/param/RegisterDeviceParams;

    invoke-direct {v0}, Lee/cyber/smartid/dto/jsonrpc/param/RegisterDeviceParams;-><init>()V

    .line 5
    iget-object v1, v9, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;->d:Lee/cyber/smartid/inter/ServiceAccess;

    invoke-interface {v1}, Lee/cyber/smartid/inter/ServiceAccess;->getUpdateDeviceData()Lee/cyber/smartid/dto/jsonrpc/param/RegisterDeviceData;

    move-result-object v7

    .line 6
    invoke-virtual {v0, v7}, Lee/cyber/smartid/dto/jsonrpc/param/RegisterDeviceParams;->setDeviceData(Lee/cyber/smartid/dto/jsonrpc/param/RegisterDeviceData;)V

    move-object v1, p2

    .line 7
    invoke-virtual {v0, p2}, Lee/cyber/smartid/dto/jsonrpc/param/RegisterDeviceParams;->setAccountRegistrationData(Lee/cyber/smartid/dto/jsonrpc/param/AccountRegistrationDataParams;)V

    .line 8
    new-instance v2, Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCRequest;

    const-string v1, "registerDevice"

    invoke-direct {v2, v1, v0}, Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCRequest;-><init>(Ljava/lang/String;Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCParams;)V

    .line 9
    iget-object v0, v9, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;->b:Lee/cyber/smartid/network/SmartIdAPI;

    invoke-interface {v0, v2}, Lee/cyber/smartid/network/SmartIdAPI;->registerDevice(Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCRequest;)Lc/b;

    move-result-object v10

    .line 10
    new-instance v11, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$4;

    iget-object v3, v9, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;->c:Lee/cyber/smartid/tse/SmartIdTSE;

    move-object v0, v11

    move-object v1, p0

    move-object v5, p3

    move-object/from16 v6, p4

    move-object/from16 v8, p5

    invoke-direct/range {v0 .. v8}, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$4;-><init>(Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCRequest;Lee/cyber/smartid/tse/SmartIdTSE;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/dto/jsonrpc/param/RegisterDeviceData;Ljava/lang/String;)V

    invoke-interface {v10, v11}, Lc/b;->a(Lc/d;)V

    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/dto/jsonrpc/param/AccountRegistrationDataParams;Ljava/lang/String;)V
    .locals 6

    .line 13
    iget-object v0, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;->c:Lee/cyber/smartid/tse/SmartIdTSE;

    invoke-virtual {v0}, Lee/cyber/smartid/tse/SmartIdTSE;->isDeviceRegistrationDone()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 14
    iget-object v0, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;->g:Lee/cyber/smartid/util/Log;

    const-string v1, "requestAddAccount - Adding an account, device already registered"

    invoke-virtual {v0, v1}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    move-object v0, p0

    move-object v1, p1

    move-object v2, p4

    move-object v3, p2

    move-object v4, p3

    move-object v5, p5

    .line 15
    invoke-direct/range {v0 .. v5}, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;->b(Ljava/lang/String;Lee/cyber/smartid/dto/jsonrpc/param/AccountRegistrationDataParams;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 16
    :cond_0
    iget-object v0, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;->g:Lee/cyber/smartid/util/Log;

    const-string v1, "requestAddAccount - Registering the device along with the new account"

    invoke-virtual {v0, v1}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    move-object v0, p0

    move-object v1, p1

    move-object v2, p4

    move-object v3, p2

    move-object v4, p3

    move-object v5, p5

    .line 17
    invoke-direct/range {v0 .. v5}, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;->a(Ljava/lang/String;Lee/cyber/smartid/dto/jsonrpc/param/AccountRegistrationDataParams;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/dto/jsonrpc/result/RegisterAccountResult;Lee/cyber/smartid/dto/jsonrpc/param/RegisterDeviceData;Ljava/lang/String;)V
    .locals 14

    move-object v8, p0

    move-object/from16 v9, p2

    .line 11
    iget-object v0, v8, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;->g:Lee/cyber/smartid/util/Log;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "verifyResponseForAddAccount - "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-object v2, p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, ", authKeyReference: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, ", signKeyReference: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-object/from16 v4, p3

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    .line 12
    iget-object v10, v8, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;->c:Lee/cyber/smartid/tse/SmartIdTSE;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ee.cyber.smartid.TAG_VERIFY_REGISTRATION_ACCOUNT_RESPONSE_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual/range {p4 .. p4}, Lee/cyber/smartid/dto/jsonrpc/result/RegisterAccountResult;->getAuthData()Lee/cyber/smartid/tse/dto/jsonrpc/KeyData;

    move-result-object v12

    new-instance v13, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$5;

    move-object v0, v13

    move-object v1, p0

    move-object/from16 v3, p4

    move-object/from16 v5, p2

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    invoke-direct/range {v0 .. v7}, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$5;-><init>(Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;Ljava/lang/String;Lee/cyber/smartid/dto/jsonrpc/result/RegisterAccountResult;Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/dto/jsonrpc/param/RegisterDeviceData;Ljava/lang/String;)V

    invoke-virtual {v10, v11, v9, v12, v13}, Lee/cyber/smartid/tse/SmartIdTSE;->validateKeyCreationResponse(Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/tse/dto/jsonrpc/KeyData;Lee/cyber/smartid/tse/inter/ValidateKeyCreationResponseListener;)V

    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/dto/jsonrpc/result/RegisterAccountResult;Ljava/lang/String;)V
    .locals 16

    move-object/from16 v7, p0

    move-object/from16 v11, p2

    .line 21
    iget-object v0, v7, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;->g:Lee/cyber/smartid/util/Log;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "initializeKeysAndAccountForAddAccount - "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-object/from16 v2, p1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, ", authKeyReference: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, ", signKeyReference: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-object/from16 v4, p3

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    .line 22
    iget-object v8, v7, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;->c:Lee/cyber/smartid/tse/SmartIdTSE;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ee.cyber.smartid.TAG_INITIALIZE_KEY_AND_STATES_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual/range {p4 .. p4}, Lee/cyber/smartid/dto/jsonrpc/result/RegisterAccountResult;->getAccountUUID()Ljava/lang/String;

    move-result-object v10

    iget-object v0, v7, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;->d:Lee/cyber/smartid/inter/ServiceAccess;

    invoke-virtual/range {p4 .. p4}, Lee/cyber/smartid/dto/jsonrpc/result/RegisterAccountResult;->getAccountUUID()Ljava/lang/String;

    move-result-object v1

    const-string v3, "AUTHENTICATION"

    invoke-interface {v0, v1, v3}, Lee/cyber/smartid/inter/ServiceAccess;->getKeyId(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual/range {p4 .. p4}, Lee/cyber/smartid/dto/jsonrpc/result/RegisterAccountResult;->getAuthData()Lee/cyber/smartid/tse/dto/jsonrpc/KeyData;

    move-result-object v14

    new-instance v15, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$7;

    move-object v0, v15

    move-object/from16 v1, p0

    move-object/from16 v3, p4

    move-object/from16 v5, p2

    move-object/from16 v6, p5

    invoke-direct/range {v0 .. v6}, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$7;-><init>(Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;Ljava/lang/String;Lee/cyber/smartid/dto/jsonrpc/result/RegisterAccountResult;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    const-string v13, "AUTHENTICATION"

    invoke-virtual/range {v8 .. v15}, Lee/cyber/smartid/tse/SmartIdTSE;->initializeKeyAndKeyStates(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/tse/dto/jsonrpc/KeyData;Lee/cyber/smartid/tse/inter/InitializeKeyAndKeyStatesListener;)V

    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/dto/jsonrpc/result/RegisterAccountResult;Ljava/lang/String;Lee/cyber/smartid/tse/dto/jsonrpc/resp/BaseResp;Lee/cyber/smartid/tse/dto/jsonrpc/resp/BaseResp;)V
    .locals 12

    move-object v9, p0

    .line 23
    iget-object v0, v9, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;->g:Lee/cyber/smartid/util/Log;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "createSCSPStatesForAddAccount - "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-object v8, p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, ", authKeyReference: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-object v4, p2

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, ", signKeyReference: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-object v5, p3

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    .line 24
    new-instance v10, Ljava/lang/Thread;

    new-instance v11, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$8;

    move-object v0, v11

    move-object v1, p0

    move-object/from16 v2, p4

    move-object/from16 v3, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    invoke-direct/range {v0 .. v8}, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$8;-><init>(Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;Lee/cyber/smartid/dto/jsonrpc/result/RegisterAccountResult;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/tse/dto/jsonrpc/resp/BaseResp;Lee/cyber/smartid/tse/dto/jsonrpc/resp/BaseResp;Ljava/lang/String;)V

    invoke-direct {v10, v11}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 25
    invoke-virtual {v10}, Ljava/lang/Thread;->start()V

    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 14

    move-object v9, p0

    move-object/from16 v10, p6

    .line 8
    iget-object v0, v9, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;->g:Lee/cyber/smartid/util/Log;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "generateDiffieHellmanKeyPairsForAddAccount - "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-object v3, p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, ", authKeyReference: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, ", signKeyReference: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-object/from16 v2, p7

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    .line 9
    iget-object v11, v9, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;->c:Lee/cyber/smartid/tse/SmartIdTSE;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ee.cyber.smartid.TAG_GENERATE_DH_KEY_PAIR_FOR_REGISTRATION_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    new-instance v13, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$2;

    move-object v0, v13

    move-object v1, p0

    move-object/from16 v4, p2

    move-object/from16 v5, p3

    move-object/from16 v6, p4

    move-object/from16 v7, p5

    move-object/from16 v8, p6

    invoke-direct/range {v0 .. v8}, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$2;-><init>(Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v11, v12, v10, v13}, Lee/cyber/smartid/tse/SmartIdTSE;->generateDiffieHellmanKeyPairForTSEKey(Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/tse/inter/GenerateDiffieHellmanKeyPairListener;)V

    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 14

    move-object v11, p0

    .line 10
    iget-object v0, v11, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;->g:Lee/cyber/smartid/util/Log;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "prepareArgumentsForAddAccount - "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-object v10, p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, ", authKeyReference: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-object/from16 v8, p6

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, ", signKeyReference: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-object/from16 v9, p7

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    .line 11
    new-instance v12, Ljava/lang/Thread;

    new-instance v13, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$3;

    move-object v0, v13

    move-object v1, p0

    move-object/from16 v2, p8

    move-object/from16 v3, p9

    move-object/from16 v4, p2

    move-object/from16 v5, p5

    move-object/from16 v6, p4

    move-object/from16 v7, p3

    invoke-direct/range {v0 .. v10}, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$3;-><init>(Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v12, v13}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 12
    invoke-virtual {v12}, Ljava/lang/Thread;->start()V

    return-void
.end method

.method static synthetic b(Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;)Lee/cyber/smartid/tse/inter/WallClock;
    .locals 0

    .line 1
    iget-object p0, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;->f:Lee/cyber/smartid/tse/inter/WallClock;

    return-object p0
.end method

.method static synthetic b(Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/dto/jsonrpc/result/RegisterAccountResult;Lee/cyber/smartid/dto/jsonrpc/param/RegisterDeviceData;Ljava/lang/String;)V
    .locals 0

    .line 2
    invoke-direct/range {p0 .. p6}, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/dto/jsonrpc/result/RegisterAccountResult;Lee/cyber/smartid/dto/jsonrpc/param/RegisterDeviceData;Ljava/lang/String;)V

    return-void
.end method

.method private b(Ljava/lang/String;Lee/cyber/smartid/dto/jsonrpc/param/AccountRegistrationDataParams;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 9

    .line 47
    iget-object v0, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;->g:Lee/cyber/smartid/util/Log;

    const-string v1, "requestNewAccountForAddAccount called"

    invoke-virtual {v0, v1}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    .line 48
    new-instance v0, Lee/cyber/smartid/dto/jsonrpc/param/RegisterDeviceParams;

    invoke-direct {v0}, Lee/cyber/smartid/dto/jsonrpc/param/RegisterDeviceParams;-><init>()V

    .line 49
    invoke-virtual {v0, p2}, Lee/cyber/smartid/dto/jsonrpc/param/RegisterDeviceParams;->setAccountRegistrationData(Lee/cyber/smartid/dto/jsonrpc/param/AccountRegistrationDataParams;)V

    .line 50
    new-instance v3, Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCRequest;

    const-string p2, "registerAccount"

    invoke-direct {v3, p2, v0}, Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCRequest;-><init>(Ljava/lang/String;Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCParams;)V

    .line 51
    iget-object p2, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;->b:Lee/cyber/smartid/network/SmartIdAPI;

    invoke-interface {p2, v3}, Lee/cyber/smartid/network/SmartIdAPI;->registerAccount(Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCRequest;)Lc/b;

    move-result-object p2

    .line 52
    new-instance v0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$13;

    iget-object v4, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;->c:Lee/cyber/smartid/tse/SmartIdTSE;

    move-object v1, v0

    move-object v2, p0

    move-object v5, p1

    move-object v6, p3

    move-object v7, p4

    move-object v8, p5

    invoke-direct/range {v1 .. v8}, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$13;-><init>(Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCRequest;Lee/cyber/smartid/tse/SmartIdTSE;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {p2, v0}, Lc/b;->a(Lc/d;)V

    return-void
.end method

.method private b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/dto/jsonrpc/result/RegisterAccountResult;Lee/cyber/smartid/dto/jsonrpc/param/RegisterDeviceData;Ljava/lang/String;)V
    .locals 11

    move-object v8, p0

    .line 18
    iget-object v0, v8, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;->g:Lee/cyber/smartid/util/Log;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "handleStepOneSuccessForAddAccount - "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-object v3, p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, ", authKeyReference: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-object v5, p2

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, ", signKeyReference: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-object v6, p3

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    .line 19
    new-instance v9, Ljava/lang/Thread;

    new-instance v10, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$6;

    move-object v0, v10

    move-object v1, p0

    move-object v2, p4

    move-object/from16 v4, p5

    move-object/from16 v7, p6

    invoke-direct/range {v0 .. v7}, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$6;-><init>(Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;Lee/cyber/smartid/dto/jsonrpc/result/RegisterAccountResult;Ljava/lang/String;Lee/cyber/smartid/dto/jsonrpc/param/RegisterDeviceData;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v9, v10}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 20
    invoke-virtual {v9}, Ljava/lang/Thread;->start()V

    return-void
.end method

.method static synthetic c(Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;)Lee/cyber/smartid/inter/ServiceAccess;
    .locals 0

    .line 1
    iget-object p0, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;->d:Lee/cyber/smartid/inter/ServiceAccess;

    return-object p0
.end method

.method static synthetic d(Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;)Lee/cyber/smartid/util/Log;
    .locals 0

    .line 1
    iget-object p0, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;->g:Lee/cyber/smartid/util/Log;

    return-object p0
.end method

.method static synthetic e(Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;)Lee/cyber/smartid/tse/SmartIdTSE;
    .locals 0

    .line 1
    iget-object p0, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;->c:Lee/cyber/smartid/tse/SmartIdTSE;

    return-object p0
.end method

.method public static getInstance(Lee/cyber/smartid/network/SmartIdAPI;Lee/cyber/smartid/tse/SmartIdTSE;Lee/cyber/smartid/inter/ServiceAccess;Lee/cyber/smartid/inter/ListenerAccess;Lee/cyber/smartid/tse/inter/WallClock;)Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;
    .locals 8

    .line 1
    sget-object v0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;->a:Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;

    if-nez v0, :cond_0

    .line 2
    const-class v0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;

    monitor-enter v0

    .line 3
    :try_start_0
    new-instance v7, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;

    move-object v1, v7

    move-object v2, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    move-object v6, p4

    invoke-direct/range {v1 .. v6}, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;-><init>(Lee/cyber/smartid/network/SmartIdAPI;Lee/cyber/smartid/tse/SmartIdTSE;Lee/cyber/smartid/inter/ServiceAccess;Lee/cyber/smartid/inter/ListenerAccess;Lee/cyber/smartid/tse/inter/WallClock;)V

    sput-object v7, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;->a:Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;

    .line 4
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception p0

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p0

    .line 5
    :cond_0
    :goto_0
    sget-object p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;->a:Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;

    return-object p0
.end method


# virtual methods
.method public addAccount(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/inter/AddAccountListener;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;->e:Lee/cyber/smartid/inter/ListenerAccess;

    invoke-interface {v0, p1, p8}, Lee/cyber/smartid/inter/ListenerAccess;->setListener(Ljava/lang/String;Lee/cyber/smartid/inter/ServiceListener;)V

    const-string p8, "KEYTOKEN"

    .line 2
    invoke-static {p8, p2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result p8

    if-nez p8, :cond_0

    const-string p8, "IDENTITYTOKEN"

    invoke-static {p8, p2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result p8

    if-nez p8, :cond_0

    .line 3
    iget-object p3, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;->d:Lee/cyber/smartid/inter/ServiceAccess;

    new-instance p4, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$1;

    invoke-direct {p4, p0, p1, p2}, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$1;-><init>(Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {p3, p4}, Lee/cyber/smartid/inter/ServiceAccess;->notifyUI(Ljava/lang/Runnable;)V

    return-void

    .line 4
    :cond_0
    invoke-direct/range {p0 .. p7}, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public handleSCSPResultForAddAccount(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/tse/dto/TSEError;)V
    .locals 8

    .line 1
    new-instance v0, Ljava/lang/Thread;

    new-instance v7, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$9;

    move-object v1, v7

    move-object v2, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    move-object v6, p4

    invoke-direct/range {v1 .. v6}, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$9;-><init>(Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/tse/dto/TSEError;)V

    invoke-direct {v0, v7}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 2
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    return-void
.end method

.method public handleSCSPStateCheckForAddAccount(Lee/cyber/smartid/dto/AccountState;)V
    .locals 3

    if-nez p1, :cond_0

    .line 1
    iget-object p1, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;->g:Lee/cyber/smartid/util/Log;

    const-string v0, "handleSCSPStateCheckForAddAccount: account can not be null!"

    invoke-virtual {p1, v0}, Lee/cyber/smartid/util/Log;->wtf(Ljava/lang/String;)V

    return-void

    .line 2
    :cond_0
    invoke-virtual {p1}, Lee/cyber/smartid/dto/AccountState;->isSubmitClientSecondPartPending()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 3
    iget-object v0, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;->g:Lee/cyber/smartid/util/Log;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "handleSCSPartResult - The account "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lee/cyber/smartid/dto/AccountState;->getAccountUUID()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, " is still missing one submitClientSecondPart result, waiting .."

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    return-void

    .line 4
    :cond_1
    invoke-virtual {p1}, Lee/cyber/smartid/dto/AccountState;->getRegistrationState()Ljava/lang/String;

    move-result-object v0

    const-string v1, "REGISTRATION_STATE_ACCOUNT_REGISTERED"

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    const-string v1, "handleSCSPartResult - The submitClientSecondPart for "

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lee/cyber/smartid/dto/AccountState;->getAuthSubmitClientSecondPartState()Ljava/lang/String;

    move-result-object v0

    const-string v2, "STATE_SUBMIT_CLIENT_SECOND_PART_CONFIRMED"

    invoke-static {v0, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lee/cyber/smartid/dto/AccountState;->getSignSubmitClientSecondPartState()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 5
    iget-object v0, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;->g:Lee/cyber/smartid/util/Log;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, "\nhas finalized"

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    .line 6
    invoke-direct {p0, p1}, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;->a(Lee/cyber/smartid/dto/AccountState;)V

    goto :goto_0

    .line 7
    :cond_2
    iget-object v0, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;->g:Lee/cyber/smartid/util/Log;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, "\nhas failed"

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lee/cyber/smartid/util/Log;->e(Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 8
    invoke-virtual {p1}, Lee/cyber/smartid/dto/AccountState;->getLastSubmitClientSecondPartError()Lee/cyber/smartid/tse/dto/TSEError;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 9
    invoke-virtual {p1}, Lee/cyber/smartid/dto/AccountState;->getLastSubmitClientSecondPartError()Lee/cyber/smartid/tse/dto/TSEError;

    move-result-object v0

    invoke-virtual {v0}, Lee/cyber/smartid/tse/dto/TSEError;->isNonRetriable()Z

    move-result v0

    .line 10
    :cond_3
    iget-object v1, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;->d:Lee/cyber/smartid/inter/ServiceAccess;

    invoke-interface {v1}, Lee/cyber/smartid/inter/ServiceAccess;->getTseErrorToServiceErrorMapper()Lee/cyber/smartid/manager/inter/TSEErrorToServiceErrorMapper;

    move-result-object v1

    invoke-virtual {p1}, Lee/cyber/smartid/dto/AccountState;->getLastSubmitClientSecondPartError()Lee/cyber/smartid/tse/dto/TSEError;

    move-result-object v2

    invoke-interface {v1, v2}, Lee/cyber/smartid/manager/inter/TSEErrorToServiceErrorMapper;->map(Lee/cyber/smartid/tse/dto/BaseError;)Lee/cyber/smartid/dto/SmartIdError;

    move-result-object v1

    invoke-virtual {p1}, Lee/cyber/smartid/dto/AccountState;->getAccountUUID()Ljava/lang/String;

    move-result-object p1

    const/4 v2, 0x0

    invoke-direct {p0, v2, v1, p1, v0}, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;->a(Ljava/lang/String;Lee/cyber/smartid/dto/SmartIdError;Ljava/lang/String;Z)V

    :goto_0
    return-void
.end method
