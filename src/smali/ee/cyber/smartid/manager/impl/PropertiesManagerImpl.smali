.class public Lee/cyber/smartid/manager/impl/PropertiesManagerImpl;
.super Ljava/lang/Object;
.source "PropertiesManagerImpl.java"

# interfaces
.implements Lee/cyber/smartid/manager/inter/PropertiesManager;


# static fields
.field private static volatile a:Lee/cyber/smartid/manager/impl/PropertiesManagerImpl;


# instance fields
.field private final b:Lee/cyber/smartid/inter/ServiceAccess;

.field private c:Lee/cyber/smartid/util/Log;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;

.field private g:Ljava/lang/String;

.field private h:Ljava/lang/Integer;

.field private i:Ljava/lang/Integer;

.field private j:Ljava/lang/Integer;


# direct methods
.method private constructor <init>(Lee/cyber/smartid/inter/ServiceAccess;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    invoke-static {p0}, Lee/cyber/smartid/util/Log;->getInstance(Ljava/lang/Object;)Lee/cyber/smartid/util/Log;

    move-result-object v0

    iput-object v0, p0, Lee/cyber/smartid/manager/impl/PropertiesManagerImpl;->c:Lee/cyber/smartid/util/Log;

    .line 3
    iput-object p1, p0, Lee/cyber/smartid/manager/impl/PropertiesManagerImpl;->b:Lee/cyber/smartid/inter/ServiceAccess;

    return-void
.end method

.method public static getInstance(Lee/cyber/smartid/inter/ServiceAccess;)Lee/cyber/smartid/manager/impl/PropertiesManagerImpl;
    .locals 2

    .line 1
    sget-object v0, Lee/cyber/smartid/manager/impl/PropertiesManagerImpl;->a:Lee/cyber/smartid/manager/impl/PropertiesManagerImpl;

    if-nez v0, :cond_0

    .line 2
    const-class v0, Lee/cyber/smartid/manager/impl/PropertiesManagerImpl;

    monitor-enter v0

    .line 3
    :try_start_0
    new-instance v1, Lee/cyber/smartid/manager/impl/PropertiesManagerImpl;

    invoke-direct {v1, p0}, Lee/cyber/smartid/manager/impl/PropertiesManagerImpl;-><init>(Lee/cyber/smartid/inter/ServiceAccess;)V

    sput-object v1, Lee/cyber/smartid/manager/impl/PropertiesManagerImpl;->a:Lee/cyber/smartid/manager/impl/PropertiesManagerImpl;

    .line 4
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception p0

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p0

    .line 5
    :cond_0
    :goto_0
    sget-object p0, Lee/cyber/smartid/manager/impl/PropertiesManagerImpl;->a:Lee/cyber/smartid/manager/impl/PropertiesManagerImpl;

    return-object p0
.end method


# virtual methods
.method a(Ljava/util/Properties;)Ljava/lang/String;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const-string v0, "safetyNetAPIKey"

    .line 1
    invoke-virtual {p1, v0}, Ljava/util/Properties;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 2
    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p1

    .line 3
    :cond_0
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    return-object p1

    .line 4
    :cond_1
    new-instance p1, Ljava/io/IOException;

    iget-object v1, p0, Lee/cyber/smartid/manager/impl/PropertiesManagerImpl;->b:Lee/cyber/smartid/inter/ServiceAccess;

    invoke-interface {v1}, Lee/cyber/smartid/inter/ServiceAccess;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    sget v2, Lee/cyber/smartid/R$string;->err_no_safety_net_api_url:I

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    iget-object v0, p0, Lee/cyber/smartid/manager/impl/PropertiesManagerImpl;->b:Lee/cyber/smartid/inter/ServiceAccess;

    invoke-interface {v0}, Lee/cyber/smartid/inter/ServiceAccess;->getPropertySource()Lee/cyber/smartid/manager/inter/PropertiesSource;

    move-result-object v0

    invoke-interface {v0}, Lee/cyber/smartid/manager/inter/PropertiesSource;->getPropertiesSourceName()Ljava/lang/String;

    move-result-object v0

    const/4 v4, 0x1

    aput-object v0, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method b(Ljava/util/Properties;)Ljava/lang/String;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const-string v0, "initonly"

    const-string v1, "disable"

    const-string v2, "safetyNetAutomaticRequestMode"

    .line 1
    :try_start_0
    invoke-virtual {p1, v2}, Ljava/util/Properties;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 2
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 3
    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p1

    .line 4
    invoke-static {p1, v1}, Lee/cyber/smartid/util/Util;->equalsIgnoreCase(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    return-object v1

    .line 5
    :cond_0
    invoke-static {p1, v0}, Lee/cyber/smartid/util/Util;->equalsIgnoreCase(Ljava/lang/String;Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_1

    return-object v0

    .line 6
    :cond_1
    new-instance p1, Ljava/io/IOException;

    invoke-direct {p1}, Ljava/io/IOException;-><init>()V

    throw p1

    .line 7
    :cond_2
    new-instance p1, Ljava/io/IOException;

    invoke-direct {p1}, Ljava/io/IOException;-><init>()V

    throw p1
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 8
    :catch_0
    new-instance p1, Ljava/io/IOException;

    iget-object v0, p0, Lee/cyber/smartid/manager/impl/PropertiesManagerImpl;->b:Lee/cyber/smartid/inter/ServiceAccess;

    invoke-interface {v0}, Lee/cyber/smartid/inter/ServiceAccess;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    sget v1, Lee/cyber/smartid/R$string;->err_no_safety_net_automatic_request_mode_setting:I

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, Lee/cyber/smartid/manager/impl/PropertiesManagerImpl;->b:Lee/cyber/smartid/inter/ServiceAccess;

    invoke-interface {v3}, Lee/cyber/smartid/inter/ServiceAccess;->getPropertySource()Lee/cyber/smartid/manager/inter/PropertiesSource;

    move-result-object v3

    invoke-interface {v3}, Lee/cyber/smartid/manager/inter/PropertiesSource;->getPropertiesSourceName()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method c(Ljava/util/Properties;)Ljava/lang/String;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const-string v0, "accountRegistrationSZId"

    .line 1
    invoke-virtual {p1, v0}, Ljava/util/Properties;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 2
    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p1

    .line 3
    :cond_0
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    return-object p1

    .line 4
    :cond_1
    new-instance p1, Ljava/io/IOException;

    iget-object v1, p0, Lee/cyber/smartid/manager/impl/PropertiesManagerImpl;->b:Lee/cyber/smartid/inter/ServiceAccess;

    invoke-interface {v1}, Lee/cyber/smartid/inter/ServiceAccess;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    sget v2, Lee/cyber/smartid/R$string;->err_no_account_registration_sz_id:I

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    iget-object v0, p0, Lee/cyber/smartid/manager/impl/PropertiesManagerImpl;->b:Lee/cyber/smartid/inter/ServiceAccess;

    invoke-interface {v0}, Lee/cyber/smartid/inter/ServiceAccess;->getPropertySource()Lee/cyber/smartid/manager/inter/PropertiesSource;

    move-result-object v0

    invoke-interface {v0}, Lee/cyber/smartid/manager/inter/PropertiesSource;->getPropertiesSourceName()Ljava/lang/String;

    move-result-object v0

    const/4 v4, 0x1

    aput-object v0, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method d(Ljava/util/Properties;)Ljava/lang/String;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const-string v0, "accountUpgradeSZId"

    .line 1
    invoke-virtual {p1, v0}, Ljava/util/Properties;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 2
    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p1

    .line 3
    :cond_0
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    return-object p1

    .line 4
    :cond_1
    new-instance p1, Ljava/io/IOException;

    iget-object v1, p0, Lee/cyber/smartid/manager/impl/PropertiesManagerImpl;->b:Lee/cyber/smartid/inter/ServiceAccess;

    invoke-interface {v1}, Lee/cyber/smartid/inter/ServiceAccess;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    sget v2, Lee/cyber/smartid/R$string;->err_no_account_upgrade_sz_id:I

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    iget-object v0, p0, Lee/cyber/smartid/manager/impl/PropertiesManagerImpl;->b:Lee/cyber/smartid/inter/ServiceAccess;

    invoke-interface {v0}, Lee/cyber/smartid/inter/ServiceAccess;->getPropertySource()Lee/cyber/smartid/manager/inter/PropertiesSource;

    move-result-object v0

    invoke-interface {v0}, Lee/cyber/smartid/manager/inter/PropertiesSource;->getPropertiesSourceName()Ljava/lang/String;

    move-result-object v0

    const/4 v4, 0x1

    aput-object v0, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method e(Ljava/util/Properties;)Ljava/lang/Integer;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const-string v0, "defaultKeyLengthBits"

    .line 1
    invoke-virtual {p1, v0}, Ljava/util/Properties;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 2
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-nez v0, :cond_1

    .line 3
    :try_start_0
    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result p1

    if-lez p1, :cond_0

    .line 4
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    return-object p1

    .line 5
    :cond_0
    new-instance p1, Ljava/lang/NumberFormatException;

    invoke-direct {p1}, Ljava/lang/NumberFormatException;-><init>()V

    throw p1
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 6
    :catch_0
    new-instance p1, Ljava/io/IOException;

    iget-object v0, p0, Lee/cyber/smartid/manager/impl/PropertiesManagerImpl;->b:Lee/cyber/smartid/inter/ServiceAccess;

    invoke-interface {v0}, Lee/cyber/smartid/inter/ServiceAccess;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    sget v3, Lee/cyber/smartid/R$string;->err_default_key_length_not_a_positive_number:I

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v4, p0, Lee/cyber/smartid/manager/impl/PropertiesManagerImpl;->b:Lee/cyber/smartid/inter/ServiceAccess;

    invoke-interface {v4}, Lee/cyber/smartid/inter/ServiceAccess;->getPropertySource()Lee/cyber/smartid/manager/inter/PropertiesSource;

    move-result-object v4

    invoke-interface {v4}, Lee/cyber/smartid/manager/inter/PropertiesSource;->getPropertiesSourceName()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v1

    invoke-virtual {v0, v3, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 7
    :cond_1
    new-instance p1, Ljava/io/IOException;

    iget-object v0, p0, Lee/cyber/smartid/manager/impl/PropertiesManagerImpl;->b:Lee/cyber/smartid/inter/ServiceAccess;

    invoke-interface {v0}, Lee/cyber/smartid/inter/ServiceAccess;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    sget v3, Lee/cyber/smartid/R$string;->err_no_default_key_length:I

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v4, p0, Lee/cyber/smartid/manager/impl/PropertiesManagerImpl;->b:Lee/cyber/smartid/inter/ServiceAccess;

    invoke-interface {v4}, Lee/cyber/smartid/inter/ServiceAccess;->getPropertySource()Lee/cyber/smartid/manager/inter/PropertiesSource;

    move-result-object v4

    invoke-interface {v4}, Lee/cyber/smartid/manager/inter/PropertiesSource;->getPropertiesSourceName()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v1

    invoke-virtual {v0, v3, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method f(Ljava/util/Properties;)Ljava/lang/Integer;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const-string v0, "multiCodeVerificationFakeCodeCount"

    .line 1
    :try_start_0
    invoke-virtual {p1, v0}, Ljava/util/Properties;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    if-nez p1, :cond_0

    .line 2
    :try_start_1
    sget-object p1, Lee/cyber/smartid/manager/inter/PropertiesManager;->DEFAULT_MULTI_CODE_VERIFICATION_FAKE_CODE_COUNT:Ljava/lang/Integer;

    return-object p1

    .line 3
    :cond_0
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 4
    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 5
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v2

    sget-object v3, Lee/cyber/smartid/manager/inter/PropertiesManager;->MINIMUM_MULTI_CODE_VERIFICATION_FAKE_CODE_COUNT:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    if-lt v2, v3, :cond_2

    .line 6
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v2

    sget-object v3, Lee/cyber/smartid/manager/inter/PropertiesManager;->MAXIMUM_MULTI_CODE_VERIFICATION_FAKE_CODE_COUNT:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    if-gt v2, v3, :cond_1

    return-object v1

    .line 7
    :cond_1
    new-instance v1, Ljava/lang/NumberFormatException;

    invoke-direct {v1}, Ljava/lang/NumberFormatException;-><init>()V

    throw v1

    .line 8
    :cond_2
    new-instance v1, Ljava/lang/NumberFormatException;

    invoke-direct {v1}, Ljava/lang/NumberFormatException;-><init>()V

    throw v1

    .line 9
    :cond_3
    new-instance v1, Ljava/lang/NumberFormatException;

    invoke-direct {v1}, Ljava/lang/NumberFormatException;-><init>()V

    throw v1
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1

    :catch_0
    const-string p1, ""

    .line 10
    :catch_1
    new-instance v1, Ljava/io/IOException;

    iget-object v2, p0, Lee/cyber/smartid/manager/impl/PropertiesManagerImpl;->b:Lee/cyber/smartid/inter/ServiceAccess;

    invoke-interface {v2}, Lee/cyber/smartid/inter/ServiceAccess;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    sget v3, Lee/cyber/smartid/R$string;->err_multi_code_verification_fake_code_count_not_a_valid_value:I

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    const/4 p1, 0x1

    aput-object v0, v4, p1

    iget-object p1, p0, Lee/cyber/smartid/manager/impl/PropertiesManagerImpl;->b:Lee/cyber/smartid/inter/ServiceAccess;

    invoke-interface {p1}, Lee/cyber/smartid/inter/ServiceAccess;->getPropertySource()Lee/cyber/smartid/manager/inter/PropertiesSource;

    move-result-object p1

    invoke-interface {p1}, Lee/cyber/smartid/manager/inter/PropertiesSource;->getPropertiesSourceName()Ljava/lang/String;

    move-result-object p1

    const/4 v0, 0x2

    aput-object p1, v4, v0

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {v1, p1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method g(Ljava/util/Properties;)Ljava/lang/Integer;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const-string v0, "multiCodeVerificationFakeCodeMinLevenshteinDistance"

    .line 1
    :try_start_0
    invoke-virtual {p1, v0}, Ljava/util/Properties;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    if-nez p1, :cond_0

    .line 2
    :try_start_1
    sget-object p1, Lee/cyber/smartid/manager/inter/PropertiesManager;->DEFAULT_MULTI_CODE_VERIFICATION_FAKE_CODE_MIN_LEVENSHTEIN_DISTANCE:Ljava/lang/Integer;

    return-object p1

    .line 3
    :cond_0
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 4
    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 5
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v2

    sget-object v3, Lee/cyber/smartid/manager/inter/PropertiesManager;->MINIMUM_MULTI_CODE_VERIFICATION_FAKE_CODE_MIN_LEVENSHTEIN_DISTANCE:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    if-lt v2, v3, :cond_2

    .line 6
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v2

    sget-object v3, Lee/cyber/smartid/manager/inter/PropertiesManager;->MAXIMUM_MULTI_CODE_VERIFICATION_FAKE_CODE_MIN_LEVENSHTEIN_DISTANCE:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    if-gt v2, v3, :cond_1

    return-object v1

    .line 7
    :cond_1
    new-instance v1, Ljava/lang/NumberFormatException;

    invoke-direct {v1}, Ljava/lang/NumberFormatException;-><init>()V

    throw v1

    .line 8
    :cond_2
    new-instance v1, Ljava/lang/NumberFormatException;

    invoke-direct {v1}, Ljava/lang/NumberFormatException;-><init>()V

    throw v1

    .line 9
    :cond_3
    new-instance v1, Ljava/io/IOException;

    invoke-direct {v1}, Ljava/io/IOException;-><init>()V

    throw v1
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1

    :catch_0
    const-string p1, ""

    .line 10
    :catch_1
    new-instance v1, Ljava/io/IOException;

    iget-object v2, p0, Lee/cyber/smartid/manager/impl/PropertiesManagerImpl;->b:Lee/cyber/smartid/inter/ServiceAccess;

    invoke-interface {v2}, Lee/cyber/smartid/inter/ServiceAccess;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    sget v3, Lee/cyber/smartid/R$string;->err_multi_code_verification_fake_code_min_levenshtein_distance_not_a_valid_value:I

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    const/4 p1, 0x1

    aput-object v0, v4, p1

    iget-object p1, p0, Lee/cyber/smartid/manager/impl/PropertiesManagerImpl;->b:Lee/cyber/smartid/inter/ServiceAccess;

    invoke-interface {p1}, Lee/cyber/smartid/inter/ServiceAccess;->getPropertySource()Lee/cyber/smartid/manager/inter/PropertiesSource;

    move-result-object p1

    invoke-interface {p1}, Lee/cyber/smartid/manager/inter/PropertiesSource;->getPropertiesSourceName()Ljava/lang/String;

    move-result-object p1

    const/4 v0, 0x2

    aput-object p1, v4, v0

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {v1, p1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public getPropAccountRegistrationSZId()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/manager/impl/PropertiesManagerImpl;->f:Ljava/lang/String;

    return-object v0
.end method

.method public getPropAccountUpgradeSZId()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/manager/impl/PropertiesManagerImpl;->g:Ljava/lang/String;

    return-object v0
.end method

.method public getPropDefaultKeyLengthBits()Ljava/lang/Integer;
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/manager/impl/PropertiesManagerImpl;->h:Ljava/lang/Integer;

    return-object v0
.end method

.method public getPropMultiCodeVerificationFakeCodeCount()Ljava/lang/Integer;
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/manager/impl/PropertiesManagerImpl;->i:Ljava/lang/Integer;

    return-object v0
.end method

.method public getPropMultiCodeVerificationFakeCodeMinLevenshteinDistance()Ljava/lang/Integer;
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/manager/impl/PropertiesManagerImpl;->j:Ljava/lang/Integer;

    return-object v0
.end method

.method public getPropSafetyNetAPIKey()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/manager/impl/PropertiesManagerImpl;->d:Ljava/lang/String;

    return-object v0
.end method

.method public getPropSafetyNetAutomaticRequestMode()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/manager/impl/PropertiesManagerImpl;->e:Ljava/lang/String;

    return-object v0
.end method

.method public parseProperties()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/manager/impl/PropertiesManagerImpl;->b:Lee/cyber/smartid/inter/ServiceAccess;

    invoke-interface {v0}, Lee/cyber/smartid/inter/ServiceAccess;->getPropertySource()Lee/cyber/smartid/manager/inter/PropertiesSource;

    move-result-object v0

    invoke-interface {v0}, Lee/cyber/smartid/manager/inter/PropertiesSource;->loadProperties()Ljava/util/Properties;

    move-result-object v0

    .line 2
    invoke-virtual {p0, v0}, Lee/cyber/smartid/manager/impl/PropertiesManagerImpl;->a(Ljava/util/Properties;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lee/cyber/smartid/manager/impl/PropertiesManagerImpl;->d:Ljava/lang/String;

    .line 3
    iget-object v1, p0, Lee/cyber/smartid/manager/impl/PropertiesManagerImpl;->c:Lee/cyber/smartid/util/Log;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "parseProperties - propSafetyNetAPIKey: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lee/cyber/smartid/manager/impl/PropertiesManagerImpl;->d:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    .line 4
    invoke-virtual {p0, v0}, Lee/cyber/smartid/manager/impl/PropertiesManagerImpl;->b(Ljava/util/Properties;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lee/cyber/smartid/manager/impl/PropertiesManagerImpl;->e:Ljava/lang/String;

    .line 5
    iget-object v1, p0, Lee/cyber/smartid/manager/impl/PropertiesManagerImpl;->c:Lee/cyber/smartid/util/Log;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "parseProperties - propSafetyNetAutomaticRequestMode: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lee/cyber/smartid/manager/impl/PropertiesManagerImpl;->e:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    .line 6
    invoke-virtual {p0, v0}, Lee/cyber/smartid/manager/impl/PropertiesManagerImpl;->c(Ljava/util/Properties;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lee/cyber/smartid/manager/impl/PropertiesManagerImpl;->f:Ljava/lang/String;

    .line 7
    iget-object v1, p0, Lee/cyber/smartid/manager/impl/PropertiesManagerImpl;->c:Lee/cyber/smartid/util/Log;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "parseProperties - propAccountRegistrationSZId: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lee/cyber/smartid/manager/impl/PropertiesManagerImpl;->f:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    .line 8
    invoke-virtual {p0, v0}, Lee/cyber/smartid/manager/impl/PropertiesManagerImpl;->d(Ljava/util/Properties;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lee/cyber/smartid/manager/impl/PropertiesManagerImpl;->g:Ljava/lang/String;

    .line 9
    iget-object v1, p0, Lee/cyber/smartid/manager/impl/PropertiesManagerImpl;->c:Lee/cyber/smartid/util/Log;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "parseProperties - propAccountUpgradeSZId: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lee/cyber/smartid/manager/impl/PropertiesManagerImpl;->g:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    .line 10
    invoke-virtual {p0, v0}, Lee/cyber/smartid/manager/impl/PropertiesManagerImpl;->e(Ljava/util/Properties;)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p0, Lee/cyber/smartid/manager/impl/PropertiesManagerImpl;->h:Ljava/lang/Integer;

    .line 11
    iget-object v1, p0, Lee/cyber/smartid/manager/impl/PropertiesManagerImpl;->c:Lee/cyber/smartid/util/Log;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "parseProperties - propDefaultKeyLengthBits: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lee/cyber/smartid/manager/impl/PropertiesManagerImpl;->h:Ljava/lang/Integer;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    .line 12
    invoke-virtual {p0, v0}, Lee/cyber/smartid/manager/impl/PropertiesManagerImpl;->f(Ljava/util/Properties;)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p0, Lee/cyber/smartid/manager/impl/PropertiesManagerImpl;->i:Ljava/lang/Integer;

    .line 13
    iget-object v1, p0, Lee/cyber/smartid/manager/impl/PropertiesManagerImpl;->c:Lee/cyber/smartid/util/Log;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "parseProperties - propMultiCodeVerificationFakeCodeCounts: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lee/cyber/smartid/manager/impl/PropertiesManagerImpl;->i:Ljava/lang/Integer;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    .line 14
    invoke-virtual {p0, v0}, Lee/cyber/smartid/manager/impl/PropertiesManagerImpl;->g(Ljava/util/Properties;)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lee/cyber/smartid/manager/impl/PropertiesManagerImpl;->j:Ljava/lang/Integer;

    .line 15
    iget-object v0, p0, Lee/cyber/smartid/manager/impl/PropertiesManagerImpl;->c:Lee/cyber/smartid/util/Log;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "parseProperties - propMultiCodeVerificationFakeCodeMinLevenshteinDistance: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lee/cyber/smartid/manager/impl/PropertiesManagerImpl;->j:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    return-void
.end method
