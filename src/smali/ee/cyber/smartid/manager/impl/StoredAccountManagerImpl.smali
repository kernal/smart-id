.class public Lee/cyber/smartid/manager/impl/StoredAccountManagerImpl;
.super Ljava/lang/Object;
.source "StoredAccountManagerImpl.java"

# interfaces
.implements Lee/cyber/smartid/manager/inter/StoredAccountManager;


# static fields
.field private static volatile a:Lee/cyber/smartid/manager/impl/StoredAccountManagerImpl;


# instance fields
.field private final b:Lee/cyber/smartid/inter/ServiceAccess;

.field private final c:Lee/cyber/smartid/inter/ListenerAccess;

.field private final d:Lee/cyber/smartid/cryptolib/inter/StorageOp;

.field private final e:Ljava/lang/Object;

.field private f:Lee/cyber/smartid/util/Log;


# direct methods
.method private constructor <init>(Lee/cyber/smartid/inter/ServiceAccess;Lee/cyber/smartid/inter/ListenerAccess;Lee/cyber/smartid/cryptolib/inter/StorageOp;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lee/cyber/smartid/manager/impl/StoredAccountManagerImpl;->e:Ljava/lang/Object;

    .line 3
    invoke-static {p0}, Lee/cyber/smartid/util/Log;->getInstance(Ljava/lang/Object;)Lee/cyber/smartid/util/Log;

    move-result-object v0

    iput-object v0, p0, Lee/cyber/smartid/manager/impl/StoredAccountManagerImpl;->f:Lee/cyber/smartid/util/Log;

    .line 4
    iput-object p1, p0, Lee/cyber/smartid/manager/impl/StoredAccountManagerImpl;->b:Lee/cyber/smartid/inter/ServiceAccess;

    .line 5
    iput-object p2, p0, Lee/cyber/smartid/manager/impl/StoredAccountManagerImpl;->c:Lee/cyber/smartid/inter/ListenerAccess;

    .line 6
    iput-object p3, p0, Lee/cyber/smartid/manager/impl/StoredAccountManagerImpl;->d:Lee/cyber/smartid/cryptolib/inter/StorageOp;

    return-void
.end method

.method static synthetic a(Lee/cyber/smartid/manager/impl/StoredAccountManagerImpl;)Lee/cyber/smartid/inter/ListenerAccess;
    .locals 0

    .line 1
    iget-object p0, p0, Lee/cyber/smartid/manager/impl/StoredAccountManagerImpl;->c:Lee/cyber/smartid/inter/ListenerAccess;

    return-object p0
.end method

.method static synthetic b(Lee/cyber/smartid/manager/impl/StoredAccountManagerImpl;)Lee/cyber/smartid/inter/ServiceAccess;
    .locals 0

    .line 1
    iget-object p0, p0, Lee/cyber/smartid/manager/impl/StoredAccountManagerImpl;->b:Lee/cyber/smartid/inter/ServiceAccess;

    return-object p0
.end method

.method private b(Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Lee/cyber/smartid/dto/AccountState;",
            ">;)",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 2
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 3
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 4
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lee/cyber/smartid/dto/AccountState;

    .line 5
    invoke-virtual {v1}, Lee/cyber/smartid/dto/AccountState;->getAccountUUID()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    return-object v0
.end method

.method public static getInstance(Lee/cyber/smartid/inter/ServiceAccess;Lee/cyber/smartid/inter/ListenerAccess;Lee/cyber/smartid/cryptolib/inter/StorageOp;)Lee/cyber/smartid/manager/impl/StoredAccountManagerImpl;
    .locals 2

    .line 1
    sget-object v0, Lee/cyber/smartid/manager/impl/StoredAccountManagerImpl;->a:Lee/cyber/smartid/manager/impl/StoredAccountManagerImpl;

    if-nez v0, :cond_0

    .line 2
    const-class v0, Lee/cyber/smartid/manager/impl/StoredAccountManagerImpl;

    monitor-enter v0

    .line 3
    :try_start_0
    new-instance v1, Lee/cyber/smartid/manager/impl/StoredAccountManagerImpl;

    invoke-direct {v1, p0, p1, p2}, Lee/cyber/smartid/manager/impl/StoredAccountManagerImpl;-><init>(Lee/cyber/smartid/inter/ServiceAccess;Lee/cyber/smartid/inter/ListenerAccess;Lee/cyber/smartid/cryptolib/inter/StorageOp;)V

    sput-object v1, Lee/cyber/smartid/manager/impl/StoredAccountManagerImpl;->a:Lee/cyber/smartid/manager/impl/StoredAccountManagerImpl;

    .line 4
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception p0

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p0

    .line 5
    :cond_0
    :goto_0
    sget-object p0, Lee/cyber/smartid/manager/impl/StoredAccountManagerImpl;->a:Lee/cyber/smartid/manager/impl/StoredAccountManagerImpl;

    return-object p0
.end method


# virtual methods
.method a(Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Lee/cyber/smartid/dto/AccountState;",
            ">;)",
            "Ljava/util/ArrayList<",
            "Lee/cyber/smartid/dto/AccountState;",
            ">;"
        }
    .end annotation

    .line 2
    iget-object v0, p0, Lee/cyber/smartid/manager/impl/StoredAccountManagerImpl;->b:Lee/cyber/smartid/inter/ServiceAccess;

    invoke-interface {v0}, Lee/cyber/smartid/inter/ServiceAccess;->getInteractiveUpgradeHelper()Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;

    move-result-object v0

    invoke-virtual {v0}, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->detectInteractiveUpgradeState()Lee/cyber/smartid/dto/upgrade/InteractiveUpgradeState;

    move-result-object v0

    invoke-virtual {v0}, Lee/cyber/smartid/dto/upgrade/InteractiveUpgradeState;->isUpgradePending()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3
    iget-object p1, p0, Lee/cyber/smartid/manager/impl/StoredAccountManagerImpl;->f:Lee/cyber/smartid/util/Log;

    const-string v0, "filterUnUsableAccounts - interactive upgrade in progress, all accounts are hidden"

    invoke-virtual {p1, v0}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    .line 4
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    :cond_0
    return-object p1
.end method

.method a(Lee/cyber/smartid/dto/AccountState;Lee/cyber/smartid/tse/dto/jsonrpc/resp/BaseResp;)V
    .locals 3

    if-nez p2, :cond_0

    .line 5
    iget-object p1, p0, Lee/cyber/smartid/manager/impl/StoredAccountManagerImpl;->f:Lee/cyber/smartid/util/Log;

    const-string p2, "setKeyUUIDToAccountState - skipping, no resp object .."

    invoke-virtual {p1, p2}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    return-void

    :cond_0
    const-string v0, "getAccountUUID"

    .line 6
    invoke-static {p2, v0}, Lee/cyber/smartid/util/ReflectionUtil;->callStringGetterByName(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "getKeyUUID"

    .line 7
    invoke-static {p2, v1}, Lee/cyber/smartid/util/ReflectionUtil;->callStringGetterByName(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "getKeyType"

    .line 8
    invoke-static {p2, v2}, Lee/cyber/smartid/util/ReflectionUtil;->callStringGetterByName(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    .line 9
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    invoke-virtual {p1}, Lee/cyber/smartid/dto/AccountState;->getAccountUUID()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    goto :goto_0

    .line 10
    :cond_1
    invoke-virtual {p1, p2, v1}, Lee/cyber/smartid/dto/AccountState;->setKeyUUIDByKeyType(Ljava/lang/String;Ljava/lang/String;)V

    .line 11
    iget-object p1, p0, Lee/cyber/smartid/manager/impl/StoredAccountManagerImpl;->f:Lee/cyber/smartid/util/Log;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setKeyUUIDToAccountState - Set the keyUUID for "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p2, " - "

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    return-void

    .line 12
    :cond_2
    :goto_0
    iget-object p1, p0, Lee/cyber/smartid/manager/impl/StoredAccountManagerImpl;->f:Lee/cyber/smartid/util/Log;

    const-string p2, "setKeyUUIDToAccountState - skipping, failed to read the respInitKey object, one or more values were null .."

    invoke-virtual {p1, p2}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    return-void
.end method

.method public addAndStoreKeyUUIDsToAccountStateIfNeeded(Ljava/lang/String;Ljava/util/ArrayList;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList<",
            "Lee/cyber/smartid/dto/jsonrpc/ServiceAccountKeyStatus;",
            ">;)V"
        }
    .end annotation

    .line 1
    invoke-virtual {p0}, Lee/cyber/smartid/manager/impl/StoredAccountManagerImpl;->getAccountStateUpdateLock()Ljava/lang/Object;

    move-result-object v0

    monitor-enter v0

    .line 2
    :try_start_0
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_5

    if-eqz p2, :cond_5

    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v1

    const/4 v2, 0x1

    if-ge v1, v2, :cond_0

    goto/16 :goto_2

    .line 3
    :cond_0
    invoke-virtual {p0, p1}, Lee/cyber/smartid/manager/impl/StoredAccountManagerImpl;->loadAccountFromStorage(Ljava/lang/String;)Lee/cyber/smartid/dto/AccountState;

    move-result-object v1

    if-nez v1, :cond_1

    .line 4
    iget-object p2, p0, Lee/cyber/smartid/manager/impl/StoredAccountManagerImpl;->f:Lee/cyber/smartid/util/Log;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const-string v2, "addAndStoreKeyUUIDsToAccountStateIfNeeded - Account "

    :try_start_1
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    const-string p1, " was not found, weird, aborting .."

    :try_start_2
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2, p1}, Lee/cyber/smartid/util/Log;->w(Ljava/lang/String;)V

    .line 5
    monitor-exit v0

    return-void

    :cond_1
    const/4 p1, 0x0

    .line 6
    invoke-virtual {p2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lee/cyber/smartid/dto/jsonrpc/ServiceAccountKeyStatus;

    .line 7
    invoke-virtual {v3}, Lee/cyber/smartid/dto/jsonrpc/ServiceAccountKeyStatus;->getKeyType()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lee/cyber/smartid/dto/AccountState;->getKeyUUIDByKeyType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_2

    goto :goto_0

    .line 8
    :cond_2
    invoke-virtual {v3}, Lee/cyber/smartid/dto/jsonrpc/ServiceAccountKeyStatus;->getKeyType()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v3}, Lee/cyber/smartid/dto/jsonrpc/ServiceAccountKeyStatus;->getKeyUUID()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, p1, v4}, Lee/cyber/smartid/dto/AccountState;->setKeyUUIDByKeyType(Ljava/lang/String;Ljava/lang/String;)V

    .line 9
    iget-object p1, p0, Lee/cyber/smartid/manager/impl/StoredAccountManagerImpl;->f:Lee/cyber/smartid/util/Log;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    const-string v5, "addAndStoreKeyUUIDsToAccountStateIfNeeded - Set the keyUUID for "

    :try_start_3
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Lee/cyber/smartid/dto/jsonrpc/ServiceAccountKeyStatus;->getKeyType()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    const-string v5, " - "

    :try_start_4
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Lee/cyber/smartid/dto/jsonrpc/ServiceAccountKeyStatus;->getKeyUUID()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    const/4 p1, 0x1

    goto :goto_0

    :cond_3
    if-nez p1, :cond_4

    .line 10
    monitor-exit v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    return-void

    .line 11
    :cond_4
    :try_start_5
    invoke-virtual {p0, v1}, Lee/cyber/smartid/manager/impl/StoredAccountManagerImpl;->storeAccountToStorage(Lee/cyber/smartid/dto/AccountState;)V
    :try_end_5
    .catch Lee/cyber/smartid/cryptolib/dto/StorageException; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_1

    :catch_0
    move-exception p1

    .line 12
    :try_start_6
    iget-object p2, p0, Lee/cyber/smartid/manager/impl/StoredAccountManagerImpl;->f:Lee/cyber/smartid/util/Log;
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    const-string v1, "addAndStoreKeyUUIDsToAccountStateIfNeeded - Failed to store the updated AccountState"

    :try_start_7
    invoke-virtual {p2, v1, p1}, Lee/cyber/smartid/util/Log;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 13
    :goto_1
    monitor-exit v0

    return-void

    .line 14
    :cond_5
    :goto_2
    monitor-exit v0

    return-void

    :catchall_0
    move-exception p1

    .line 15
    monitor-exit v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    throw p1
.end method

.method public getAccountStateUpdateLock()Ljava/lang/Object;
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/manager/impl/StoredAccountManagerImpl;->e:Ljava/lang/Object;

    return-object v0
.end method

.method public getAccountUUIDs()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 1
    invoke-virtual {p0}, Lee/cyber/smartid/manager/impl/StoredAccountManagerImpl;->loadAccountsFromStorage()Ljava/util/ArrayList;

    move-result-object v0

    invoke-direct {p0, v0}, Lee/cyber/smartid/manager/impl/StoredAccountManagerImpl;->b(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public getAccounts(Ljava/lang/String;Lee/cyber/smartid/inter/GetAccountsListener;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/manager/impl/StoredAccountManagerImpl;->c:Lee/cyber/smartid/inter/ListenerAccess;

    invoke-interface {v0, p1, p2}, Lee/cyber/smartid/inter/ListenerAccess;->setListener(Ljava/lang/String;Lee/cyber/smartid/inter/ServiceListener;)V

    .line 2
    new-instance p2, Ljava/lang/Thread;

    new-instance v0, Lee/cyber/smartid/manager/impl/StoredAccountManagerImpl$1;

    invoke-direct {v0, p0, p1}, Lee/cyber/smartid/manager/impl/StoredAccountManagerImpl$1;-><init>(Lee/cyber/smartid/manager/impl/StoredAccountManagerImpl;Ljava/lang/String;)V

    invoke-direct {p2, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 3
    invoke-virtual {p2}, Ljava/lang/Thread;->start()V

    return-void
.end method

.method public getAllUsableAccountUUIDs()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 1
    invoke-virtual {p0}, Lee/cyber/smartid/manager/impl/StoredAccountManagerImpl;->loadAllUsableAccountsFromStorage()Ljava/util/ArrayList;

    move-result-object v0

    invoke-direct {p0, v0}, Lee/cyber/smartid/manager/impl/StoredAccountManagerImpl;->b(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public getAllUsableKeyIds()Ljava/util/ArrayList;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 1
    invoke-virtual {p0}, Lee/cyber/smartid/manager/impl/StoredAccountManagerImpl;->getAllUsableAccountUUIDs()Ljava/util/ArrayList;

    move-result-object v0

    .line 2
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    const/4 v2, 0x0

    if-nez v1, :cond_0

    return-object v2

    .line 3
    :cond_0
    iget-object v1, p0, Lee/cyber/smartid/manager/impl/StoredAccountManagerImpl;->b:Lee/cyber/smartid/inter/ServiceAccess;

    invoke-interface {v1}, Lee/cyber/smartid/tse/inter/ExternalResourceAccess;->getKeyTypes()[Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 4
    array-length v3, v1

    if-nez v3, :cond_1

    goto :goto_1

    .line 5
    :cond_1
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 6
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 7
    array-length v4, v1

    const/4 v5, 0x0

    :goto_0
    if-ge v5, v4, :cond_2

    aget-object v6, v1, v5

    .line 8
    iget-object v7, p0, Lee/cyber/smartid/manager/impl/StoredAccountManagerImpl;->b:Lee/cyber/smartid/inter/ServiceAccess;

    invoke-interface {v7, v3, v6}, Lee/cyber/smartid/inter/ServiceAccess;->getKeyId(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    :cond_3
    :goto_1
    return-object v2
.end method

.method public getAllUsableKeyIdsWithAccountUUIDAndKeyType()Ljava/util/ArrayList;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Lee/cyber/smartid/tse/dto/Triple<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .line 1
    invoke-virtual {p0}, Lee/cyber/smartid/manager/impl/StoredAccountManagerImpl;->getAllUsableAccountUUIDs()Ljava/util/ArrayList;

    move-result-object v0

    .line 2
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    const/4 v2, 0x0

    if-nez v1, :cond_0

    return-object v2

    .line 3
    :cond_0
    iget-object v1, p0, Lee/cyber/smartid/manager/impl/StoredAccountManagerImpl;->b:Lee/cyber/smartid/inter/ServiceAccess;

    invoke-interface {v1}, Lee/cyber/smartid/tse/inter/ExternalResourceAccess;->getKeyTypes()[Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 4
    array-length v3, v1

    if-nez v3, :cond_1

    goto :goto_1

    .line 5
    :cond_1
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 6
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 7
    array-length v4, v1

    const/4 v5, 0x0

    :goto_0
    if-ge v5, v4, :cond_2

    aget-object v6, v1, v5

    .line 8
    new-instance v7, Lee/cyber/smartid/tse/dto/Triple;

    iget-object v8, p0, Lee/cyber/smartid/manager/impl/StoredAccountManagerImpl;->b:Lee/cyber/smartid/inter/ServiceAccess;

    invoke-interface {v8, v3, v6}, Lee/cyber/smartid/inter/ServiceAccess;->getKeyId(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8, v3, v6}, Lee/cyber/smartid/tse/dto/Triple;-><init>(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v2, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    :cond_3
    :goto_1
    return-object v2
.end method

.method public getKeyTypeForKeyUUID(Ljava/lang/String;)Ljava/lang/String;
    .locals 5

    .line 1
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    return-object v1

    .line 2
    :cond_0
    invoke-virtual {p0}, Lee/cyber/smartid/manager/impl/StoredAccountManagerImpl;->getAccountStateUpdateLock()Ljava/lang/Object;

    move-result-object v0

    monitor-enter v0

    .line 3
    :try_start_0
    invoke-virtual {p0}, Lee/cyber/smartid/manager/impl/StoredAccountManagerImpl;->loadAccountsFromStorage()Ljava/util/ArrayList;

    move-result-object v2

    .line 4
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lez v3, :cond_3

    .line 5
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lee/cyber/smartid/dto/AccountState;

    .line 6
    invoke-virtual {v3}, Lee/cyber/smartid/dto/AccountState;->getAuthKeyUUID()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 7
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const-string p1, "AUTHENTICATION"

    return-object p1

    .line 8
    :cond_2
    :try_start_1
    invoke-virtual {v3}, Lee/cyber/smartid/dto/AccountState;->getSignKeyUUID()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 9
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    const-string p1, "SIGNATURE"

    return-object p1

    .line 10
    :cond_3
    :try_start_2
    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception p1

    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw p1
.end method

.method public initializeAccountState(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/dto/jsonrpc/result/RegisterAccountResult;Lee/cyber/smartid/tse/dto/jsonrpc/resp/BaseResp;Lee/cyber/smartid/tse/dto/jsonrpc/resp/BaseResp;Ljava/lang/String;)Lee/cyber/smartid/dto/AccountState;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lee/cyber/smartid/cryptolib/dto/StorageException;
        }
    .end annotation

    .line 1
    invoke-virtual {p0}, Lee/cyber/smartid/manager/impl/StoredAccountManagerImpl;->getAccountStateUpdateLock()Ljava/lang/Object;

    move-result-object v0

    monitor-enter v0

    .line 2
    :try_start_0
    new-instance v1, Lee/cyber/smartid/dto/AccountState;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const-string v2, "REGISTRATION_STATE_WAITING_FOR_SUBMIT_CLIENT_2ND_PART"

    :try_start_1
    invoke-direct {v1, p1, v2, p2}, Lee/cyber/smartid/dto/AccountState;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 3
    invoke-virtual {v1, p3}, Lee/cyber/smartid/dto/AccountState;->setAuthKeyReference(Ljava/lang/String;)V

    .line 4
    invoke-virtual {v1, p4}, Lee/cyber/smartid/dto/AccountState;->setSignKeyReference(Ljava/lang/String;)V

    .line 5
    invoke-virtual {p5}, Lee/cyber/smartid/dto/jsonrpc/result/RegisterAccountResult;->getIdentityData()Lee/cyber/smartid/dto/jsonrpc/IdentityData;

    move-result-object p1

    invoke-virtual {v1, p1}, Lee/cyber/smartid/dto/AccountState;->setIdentityDataFromRegistration(Lee/cyber/smartid/dto/jsonrpc/IdentityData;)V

    .line 6
    invoke-virtual {p0, v1, p6}, Lee/cyber/smartid/manager/impl/StoredAccountManagerImpl;->a(Lee/cyber/smartid/dto/AccountState;Lee/cyber/smartid/tse/dto/jsonrpc/resp/BaseResp;)V

    .line 7
    invoke-virtual {p0, v1, p7}, Lee/cyber/smartid/manager/impl/StoredAccountManagerImpl;->a(Lee/cyber/smartid/dto/AccountState;Lee/cyber/smartid/tse/dto/jsonrpc/resp/BaseResp;)V

    .line 8
    invoke-virtual {v1, p8}, Lee/cyber/smartid/dto/AccountState;->addSubmitClientSecondPartListenerTag(Ljava/lang/String;)V

    .line 9
    invoke-virtual {p0, v1}, Lee/cyber/smartid/manager/impl/StoredAccountManagerImpl;->storeAccountToStorage(Lee/cyber/smartid/dto/AccountState;)V

    .line 10
    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception p1

    .line 11
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw p1
.end method

.method public loadAccountFromStorage(Ljava/lang/String;)Lee/cyber/smartid/dto/AccountState;
    .locals 5

    .line 1
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    return-object v1

    .line 2
    :cond_0
    invoke-virtual {p0}, Lee/cyber/smartid/manager/impl/StoredAccountManagerImpl;->getAccountStateUpdateLock()Ljava/lang/Object;

    move-result-object v0

    monitor-enter v0

    .line 3
    :try_start_0
    invoke-virtual {p0}, Lee/cyber/smartid/manager/impl/StoredAccountManagerImpl;->loadAccountsFromStorage()Ljava/util/ArrayList;

    move-result-object v2

    .line 4
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lee/cyber/smartid/dto/AccountState;

    .line 5
    invoke-virtual {v3}, Lee/cyber/smartid/dto/AccountState;->getAccountUUID()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 6
    monitor-exit v0

    return-object v3

    .line 7
    :cond_2
    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception p1

    .line 8
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method public loadAccountFromStorageByKeyId(Ljava/lang/String;)Lee/cyber/smartid/dto/AccountState;
    .locals 7

    .line 1
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    return-object v1

    .line 2
    :cond_0
    invoke-virtual {p0}, Lee/cyber/smartid/manager/impl/StoredAccountManagerImpl;->getAccountStateUpdateLock()Ljava/lang/Object;

    move-result-object v0

    monitor-enter v0

    .line 3
    :try_start_0
    invoke-virtual {p0}, Lee/cyber/smartid/manager/impl/StoredAccountManagerImpl;->loadAccountsFromStorage()Ljava/util/ArrayList;

    move-result-object v2

    .line 4
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lee/cyber/smartid/dto/AccountState;

    .line 5
    iget-object v4, p0, Lee/cyber/smartid/manager/impl/StoredAccountManagerImpl;->b:Lee/cyber/smartid/inter/ServiceAccess;

    invoke-virtual {v3}, Lee/cyber/smartid/dto/AccountState;->getAccountUUID()Ljava/lang/String;

    move-result-object v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const-string v6, "AUTHENTICATION"

    :try_start_1
    invoke-interface {v4, v5, v6}, Lee/cyber/smartid/inter/ServiceAccess;->getKeyId(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {p1, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 6
    monitor-exit v0

    return-object v3

    .line 7
    :cond_2
    iget-object v4, p0, Lee/cyber/smartid/manager/impl/StoredAccountManagerImpl;->b:Lee/cyber/smartid/inter/ServiceAccess;

    invoke-virtual {v3}, Lee/cyber/smartid/dto/AccountState;->getAccountUUID()Ljava/lang/String;

    move-result-object v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    const-string v6, "SIGNATURE"

    :try_start_2
    invoke-interface {v4, v5, v6}, Lee/cyber/smartid/inter/ServiceAccess;->getKeyId(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {p1, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 8
    monitor-exit v0

    return-object v3

    .line 9
    :cond_3
    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception p1

    .line 10
    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw p1
.end method

.method public loadAccountsFromStorage()Ljava/util/ArrayList;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Lee/cyber/smartid/dto/AccountState;",
            ">;"
        }
    .end annotation

    .line 1
    invoke-virtual {p0}, Lee/cyber/smartid/manager/impl/StoredAccountManagerImpl;->getAccountStateUpdateLock()Ljava/lang/Object;

    move-result-object v0

    monitor-enter v0

    .line 2
    :try_start_0
    iget-object v1, p0, Lee/cyber/smartid/manager/impl/StoredAccountManagerImpl;->d:Lee/cyber/smartid/cryptolib/inter/StorageOp;

    iget-object v2, p0, Lee/cyber/smartid/manager/impl/StoredAccountManagerImpl;->b:Lee/cyber/smartid/inter/ServiceAccess;

    invoke-interface {v2}, Lee/cyber/smartid/inter/ServiceAccess;->getAccountsStorageId()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lee/cyber/smartid/manager/impl/StoredAccountManagerImpl$2;

    invoke-direct {v3, p0}, Lee/cyber/smartid/manager/impl/StoredAccountManagerImpl$2;-><init>(Lee/cyber/smartid/manager/impl/StoredAccountManagerImpl;)V

    .line 3
    invoke-virtual {v3}, Lcom/google/gson/b/a;->getType()Ljava/lang/reflect/Type;

    move-result-object v3

    .line 4
    invoke-interface {v1, v2, v3}, Lee/cyber/smartid/cryptolib/inter/StorageOp;->retrieveData(Ljava/lang/String;Ljava/lang/reflect/Type;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/ArrayList;

    if-nez v1, :cond_0

    .line 5
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 6
    :cond_0
    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception v1

    .line 7
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public loadAllUsableAccountsFromStorage()Ljava/util/ArrayList;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Lee/cyber/smartid/dto/AccountState;",
            ">;"
        }
    .end annotation

    .line 1
    invoke-virtual {p0}, Lee/cyber/smartid/manager/impl/StoredAccountManagerImpl;->getAccountStateUpdateLock()Ljava/lang/Object;

    move-result-object v0

    monitor-enter v0

    .line 2
    :try_start_0
    invoke-virtual {p0}, Lee/cyber/smartid/manager/impl/StoredAccountManagerImpl;->loadAccountsFromStorage()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {p0, v1}, Lee/cyber/smartid/manager/impl/StoredAccountManagerImpl;->a(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v1

    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception v1

    .line 3
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public storeAccountToStorage(Lee/cyber/smartid/dto/AccountState;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lee/cyber/smartid/cryptolib/dto/StorageException;
        }
    .end annotation

    if-nez p1, :cond_0

    return-void

    .line 1
    :cond_0
    invoke-virtual {p0}, Lee/cyber/smartid/manager/impl/StoredAccountManagerImpl;->getAccountStateUpdateLock()Ljava/lang/Object;

    move-result-object v0

    monitor-enter v0

    .line 2
    :try_start_0
    invoke-virtual {p0}, Lee/cyber/smartid/manager/impl/StoredAccountManagerImpl;->loadAccountsFromStorage()Ljava/util/ArrayList;

    move-result-object v1

    .line 3
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_2

    .line 4
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 5
    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 6
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lee/cyber/smartid/dto/AccountState;

    .line 7
    invoke-virtual {p1}, Lee/cyber/smartid/dto/AccountState;->getAccountUUID()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3}, Lee/cyber/smartid/dto/AccountState;->getAccountUUID()Ljava/lang/String;

    move-result-object v3

    invoke-static {v4, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 8
    invoke-interface {v2}, Ljava/util/Iterator;->remove()V

    .line 9
    :cond_2
    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 10
    invoke-virtual {p0, v1}, Lee/cyber/smartid/manager/impl/StoredAccountManagerImpl;->storeAccountsToStorage(Ljava/util/ArrayList;)V

    .line 11
    monitor-exit v0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method public storeAccountsToStorage(Ljava/util/ArrayList;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Lee/cyber/smartid/dto/AccountState;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lee/cyber/smartid/cryptolib/dto/StorageException;
        }
    .end annotation

    .line 1
    invoke-virtual {p0}, Lee/cyber/smartid/manager/impl/StoredAccountManagerImpl;->getAccountStateUpdateLock()Ljava/lang/Object;

    move-result-object v0

    monitor-enter v0

    .line 2
    :try_start_0
    iget-object v1, p0, Lee/cyber/smartid/manager/impl/StoredAccountManagerImpl;->d:Lee/cyber/smartid/cryptolib/inter/StorageOp;

    iget-object v2, p0, Lee/cyber/smartid/manager/impl/StoredAccountManagerImpl;->b:Lee/cyber/smartid/inter/ServiceAccess;

    invoke-interface {v2}, Lee/cyber/smartid/inter/ServiceAccess;->getAccountsStorageId()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2, p1}, Lee/cyber/smartid/cryptolib/inter/StorageOp;->storeData(Ljava/lang/String;Ljava/lang/Object;)V

    .line 3
    monitor-exit v0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method
