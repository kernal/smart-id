.class Lee/cyber/smartid/manager/impl/TSEListenerToServiceListenerMapperImpl$3;
.super Ljava/lang/Object;
.source "TSEListenerToServiceListenerMapperImpl.java"

# interfaces
.implements Lee/cyber/smartid/tse/inter/ConfirmTransactionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lee/cyber/smartid/manager/impl/TSEListenerToServiceListenerMapperImpl;->c()Lee/cyber/smartid/tse/inter/TSEListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lee/cyber/smartid/manager/impl/TSEListenerToServiceListenerMapperImpl;


# direct methods
.method constructor <init>(Lee/cyber/smartid/manager/impl/TSEListenerToServiceListenerMapperImpl;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lee/cyber/smartid/manager/impl/TSEListenerToServiceListenerMapperImpl$3;->a:Lee/cyber/smartid/manager/impl/TSEListenerToServiceListenerMapperImpl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onConfirmTransactionFailed(Ljava/lang/String;Lee/cyber/smartid/tse/dto/TSEError;)V
    .locals 3

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/manager/impl/TSEListenerToServiceListenerMapperImpl$3;->a:Lee/cyber/smartid/manager/impl/TSEListenerToServiceListenerMapperImpl;

    invoke-static {v0}, Lee/cyber/smartid/manager/impl/TSEListenerToServiceListenerMapperImpl;->a(Lee/cyber/smartid/manager/impl/TSEListenerToServiceListenerMapperImpl;)Lee/cyber/smartid/inter/ListenerAccess;

    move-result-object v0

    const-class v1, Lee/cyber/smartid/inter/ConfirmTransactionListener;

    const/4 v2, 0x1

    invoke-interface {v0, p1, v2, v1}, Lee/cyber/smartid/inter/ListenerAccess;->getListener(Ljava/lang/String;ZLjava/lang/Class;)Lee/cyber/smartid/inter/ServiceListener;

    move-result-object v0

    check-cast v0, Lee/cyber/smartid/inter/ConfirmTransactionListener;

    if-eqz v0, :cond_0

    .line 2
    iget-object v1, p0, Lee/cyber/smartid/manager/impl/TSEListenerToServiceListenerMapperImpl$3;->a:Lee/cyber/smartid/manager/impl/TSEListenerToServiceListenerMapperImpl;

    invoke-static {v1}, Lee/cyber/smartid/manager/impl/TSEListenerToServiceListenerMapperImpl;->b(Lee/cyber/smartid/manager/impl/TSEListenerToServiceListenerMapperImpl;)Lee/cyber/smartid/inter/ServiceAccess;

    move-result-object v1

    invoke-interface {v1}, Lee/cyber/smartid/inter/ServiceAccess;->getTseErrorToServiceErrorMapper()Lee/cyber/smartid/manager/inter/TSEErrorToServiceErrorMapper;

    move-result-object v1

    invoke-interface {v1, p2}, Lee/cyber/smartid/manager/inter/TSEErrorToServiceErrorMapper;->map(Lee/cyber/smartid/tse/dto/BaseError;)Lee/cyber/smartid/dto/SmartIdError;

    move-result-object p2

    invoke-interface {v0, p1, p2}, Lee/cyber/smartid/inter/ConfirmTransactionListener;->onConfirmTransactionFailed(Ljava/lang/String;Lee/cyber/smartid/dto/SmartIdError;)V

    :cond_0
    return-void
.end method

.method public onConfirmTransactionSuccess(Ljava/lang/String;Lee/cyber/smartid/tse/dto/jsonrpc/resp/ConfirmTransactionResp;)V
    .locals 3

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/manager/impl/TSEListenerToServiceListenerMapperImpl$3;->a:Lee/cyber/smartid/manager/impl/TSEListenerToServiceListenerMapperImpl;

    invoke-static {v0}, Lee/cyber/smartid/manager/impl/TSEListenerToServiceListenerMapperImpl;->a(Lee/cyber/smartid/manager/impl/TSEListenerToServiceListenerMapperImpl;)Lee/cyber/smartid/inter/ListenerAccess;

    move-result-object v0

    const-class v1, Lee/cyber/smartid/inter/ConfirmTransactionListener;

    const/4 v2, 0x1

    invoke-interface {v0, p1, v2, v1}, Lee/cyber/smartid/inter/ListenerAccess;->getListener(Ljava/lang/String;ZLjava/lang/Class;)Lee/cyber/smartid/inter/ServiceListener;

    move-result-object v0

    check-cast v0, Lee/cyber/smartid/inter/ConfirmTransactionListener;

    if-eqz v0, :cond_0

    .line 2
    invoke-interface {v0, p1, p2}, Lee/cyber/smartid/inter/ConfirmTransactionListener;->onConfirmTransactionSuccess(Ljava/lang/String;Lee/cyber/smartid/tse/dto/jsonrpc/resp/ConfirmTransactionResp;)V

    :cond_0
    return-void
.end method
