.class Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$1;
.super Ljava/lang/Object;
.source "AddAccountManagerImpl.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;->addAccount(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/inter/AddAccountListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:Ljava/lang/String;

.field final synthetic c:Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;


# direct methods
.method constructor <init>(Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$1;->c:Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;

    iput-object p2, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$1;->a:Ljava/lang/String;

    iput-object p3, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$1;->b:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 9

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$1;->c:Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;

    invoke-static {v0}, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;->a(Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;)Lee/cyber/smartid/inter/ListenerAccess;

    move-result-object v0

    iget-object v1, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$1;->c:Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;

    invoke-static {v1}, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;->a(Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;)Lee/cyber/smartid/inter/ListenerAccess;

    move-result-object v1

    iget-object v2, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$1;->a:Ljava/lang/String;

    const-class v3, Lee/cyber/smartid/inter/AddAccountListener;

    const/4 v4, 0x1

    invoke-interface {v1, v2, v4, v3}, Lee/cyber/smartid/inter/ListenerAccess;->getListener(Ljava/lang/String;ZLjava/lang/Class;)Lee/cyber/smartid/inter/ServiceListener;

    move-result-object v1

    iget-object v2, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$1;->a:Ljava/lang/String;

    iget-object v3, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$1;->c:Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;

    invoke-static {v3}, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;->b(Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;)Lee/cyber/smartid/tse/inter/WallClock;

    move-result-object v3

    iget-object v5, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$1;->c:Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;

    invoke-static {v5}, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;->c(Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;)Lee/cyber/smartid/inter/ServiceAccess;

    move-result-object v5

    invoke-interface {v5}, Lee/cyber/smartid/inter/ServiceAccess;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    sget v6, Lee/cyber/smartid/R$string;->err_registration_init_type_not_supported:I

    new-array v4, v4, [Ljava/lang/Object;

    iget-object v7, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$1;->b:Ljava/lang/String;

    const/4 v8, 0x0

    aput-object v7, v4, v8

    invoke-virtual {v5, v6, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    const-wide/16 v5, 0x3f8

    invoke-static {v3, v5, v6, v4}, Lee/cyber/smartid/dto/SmartIdError;->from(Lee/cyber/smartid/tse/inter/WallClock;JLjava/lang/String;)Lee/cyber/smartid/dto/SmartIdError;

    move-result-object v3

    invoke-interface {v0, v1, v2, v3}, Lee/cyber/smartid/inter/ListenerAccess;->notifyError(Lee/cyber/smartid/inter/ServiceListener;Ljava/lang/String;Lee/cyber/smartid/dto/SmartIdError;)V

    return-void
.end method
