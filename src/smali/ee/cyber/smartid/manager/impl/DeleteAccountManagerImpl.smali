.class public Lee/cyber/smartid/manager/impl/DeleteAccountManagerImpl;
.super Ljava/lang/Object;
.source "DeleteAccountManagerImpl.java"

# interfaces
.implements Lee/cyber/smartid/manager/inter/DeleteAccountManager;


# static fields
.field private static volatile a:Lee/cyber/smartid/manager/inter/DeleteAccountManager;


# instance fields
.field private final b:Lee/cyber/smartid/network/SmartIdAPI;

.field private final c:Lee/cyber/smartid/tse/SmartIdTSE;

.field private final d:Lee/cyber/smartid/inter/ServiceAccess;

.field private final e:Lee/cyber/smartid/inter/ListenerAccess;

.field private final f:Lee/cyber/smartid/tse/inter/WallClock;

.field private g:Lee/cyber/smartid/util/Log;


# direct methods
.method private constructor <init>(Lee/cyber/smartid/network/SmartIdAPI;Lee/cyber/smartid/tse/SmartIdTSE;Lee/cyber/smartid/inter/ServiceAccess;Lee/cyber/smartid/inter/ListenerAccess;Lee/cyber/smartid/tse/inter/WallClock;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    invoke-static {p0}, Lee/cyber/smartid/util/Log;->getInstance(Ljava/lang/Object;)Lee/cyber/smartid/util/Log;

    move-result-object v0

    iput-object v0, p0, Lee/cyber/smartid/manager/impl/DeleteAccountManagerImpl;->g:Lee/cyber/smartid/util/Log;

    .line 3
    iput-object p1, p0, Lee/cyber/smartid/manager/impl/DeleteAccountManagerImpl;->b:Lee/cyber/smartid/network/SmartIdAPI;

    .line 4
    iput-object p2, p0, Lee/cyber/smartid/manager/impl/DeleteAccountManagerImpl;->c:Lee/cyber/smartid/tse/SmartIdTSE;

    .line 5
    iput-object p3, p0, Lee/cyber/smartid/manager/impl/DeleteAccountManagerImpl;->d:Lee/cyber/smartid/inter/ServiceAccess;

    .line 6
    iput-object p4, p0, Lee/cyber/smartid/manager/impl/DeleteAccountManagerImpl;->e:Lee/cyber/smartid/inter/ListenerAccess;

    .line 7
    iput-object p5, p0, Lee/cyber/smartid/manager/impl/DeleteAccountManagerImpl;->f:Lee/cyber/smartid/tse/inter/WallClock;

    return-void
.end method

.method static synthetic a(Lee/cyber/smartid/manager/impl/DeleteAccountManagerImpl;)Lee/cyber/smartid/util/Log;
    .locals 0

    .line 2
    iget-object p0, p0, Lee/cyber/smartid/manager/impl/DeleteAccountManagerImpl;->g:Lee/cyber/smartid/util/Log;

    return-object p0
.end method

.method private a(Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList<",
            "Lee/cyber/smartid/dto/AccountWrapper;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lee/cyber/smartid/cryptolib/dto/StorageException;
        }
    .end annotation

    .line 12
    iget-object v0, p0, Lee/cyber/smartid/manager/impl/DeleteAccountManagerImpl;->d:Lee/cyber/smartid/inter/ServiceAccess;

    invoke-interface {v0}, Lee/cyber/smartid/inter/ServiceAccess;->getStoredAccountManager()Lee/cyber/smartid/manager/inter/StoredAccountManager;

    move-result-object v0

    invoke-interface {v0}, Lee/cyber/smartid/manager/inter/StoredAccountManager;->getAccountStateUpdateLock()Ljava/lang/Object;

    move-result-object v0

    monitor-enter v0

    .line 13
    :try_start_0
    iget-object v1, p0, Lee/cyber/smartid/manager/impl/DeleteAccountManagerImpl;->g:Lee/cyber/smartid/util/Log;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const-string v3, "deleteLocalAccountAndData: "

    :try_start_1
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    .line 14
    iget-object v1, p0, Lee/cyber/smartid/manager/impl/DeleteAccountManagerImpl;->d:Lee/cyber/smartid/inter/ServiceAccess;

    invoke-interface {v1}, Lee/cyber/smartid/inter/ServiceAccess;->getStoredAccountManager()Lee/cyber/smartid/manager/inter/StoredAccountManager;

    move-result-object v1

    invoke-interface {v1}, Lee/cyber/smartid/manager/inter/StoredAccountManager;->loadAccountsFromStorage()Ljava/util/ArrayList;

    move-result-object v1

    .line 15
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 16
    iget-object p1, p0, Lee/cyber/smartid/manager/impl/DeleteAccountManagerImpl;->g:Lee/cyber/smartid/util/Log;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    const-string v2, "deleteLocalAccountAndData: Invalid accountUUID, aborting .."

    :try_start_2
    invoke-virtual {p1, v2}, Lee/cyber/smartid/util/Log;->e(Ljava/lang/String;)V

    .line 17
    invoke-static {v1}, Lee/cyber/smartid/dto/AccountWrapper;->toWrapperList(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object p1

    monitor-exit v0

    return-object p1

    .line 18
    :cond_0
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-nez v2, :cond_1

    .line 19
    iget-object p1, p0, Lee/cyber/smartid/manager/impl/DeleteAccountManagerImpl;->g:Lee/cyber/smartid/util/Log;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    const-string v2, "deleteLocalAccountAndData: No accounts found, aborting .."

    :try_start_3
    invoke-virtual {p1, v2}, Lee/cyber/smartid/util/Log;->e(Ljava/lang/String;)V

    .line 20
    invoke-static {v1}, Lee/cyber/smartid/dto/AccountWrapper;->toWrapperList(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object p1

    monitor-exit v0

    return-object p1

    :cond_1
    const/4 v2, 0x0

    .line 21
    :goto_0
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v3

    const/4 v4, -0x1

    if-ge v2, v3, :cond_3

    .line 22
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lee/cyber/smartid/dto/AccountState;

    invoke-virtual {v3}, Lee/cyber/smartid/dto/AccountState;->getAccountUUID()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_1

    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_3
    const/4 v2, -0x1

    :goto_1
    if-ne v2, v4, :cond_4

    .line 23
    iget-object p1, p0, Lee/cyber/smartid/manager/impl/DeleteAccountManagerImpl;->g:Lee/cyber/smartid/util/Log;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    const-string v2, "deleteLocalAccountAndData: Target account not found, aborting .."

    :try_start_4
    invoke-virtual {p1, v2}, Lee/cyber/smartid/util/Log;->w(Ljava/lang/String;)V

    .line 24
    invoke-static {v1}, Lee/cyber/smartid/dto/AccountWrapper;->toWrapperList(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object p1

    monitor-exit v0

    return-object p1

    .line 25
    :cond_4
    iget-object v3, p0, Lee/cyber/smartid/manager/impl/DeleteAccountManagerImpl;->g:Lee/cyber/smartid/util/Log;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    const-string v5, "deleteLocalAccountAndData: Removing Keys and KeyStates for "

    :try_start_5
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    .line 26
    iget-object v3, p0, Lee/cyber/smartid/manager/impl/DeleteAccountManagerImpl;->c:Lee/cyber/smartid/tse/SmartIdTSE;

    iget-object v4, p0, Lee/cyber/smartid/manager/impl/DeleteAccountManagerImpl;->d:Lee/cyber/smartid/inter/ServiceAccess;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    const-string v5, "AUTHENTICATION"

    :try_start_6
    invoke-interface {v4, p1, v5}, Lee/cyber/smartid/inter/ServiceAccess;->getKeyId(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lee/cyber/smartid/manager/impl/DeleteAccountManagerImpl;->d:Lee/cyber/smartid/inter/ServiceAccess;
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    const-string v6, "SIGNATURE"

    :try_start_7
    invoke-interface {v5, p1, v6}, Lee/cyber/smartid/inter/ServiceAccess;->getKeyId(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lee/cyber/smartid/tse/SmartIdTSE;->deleteKeysAndRelatedObjects(Ljava/lang/String;Ljava/lang/String;)V

    .line 27
    iget-object v3, p0, Lee/cyber/smartid/manager/impl/DeleteAccountManagerImpl;->g:Lee/cyber/smartid/util/Log;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    const-string v5, "deleteLocalAccountAndData: Removing the account for  "

    :try_start_8
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    .line 28
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 29
    iget-object v2, p0, Lee/cyber/smartid/manager/impl/DeleteAccountManagerImpl;->d:Lee/cyber/smartid/inter/ServiceAccess;

    invoke-interface {v2}, Lee/cyber/smartid/inter/ServiceAccess;->getStoredAccountManager()Lee/cyber/smartid/manager/inter/StoredAccountManager;

    move-result-object v2

    invoke-interface {v2, v1}, Lee/cyber/smartid/manager/inter/StoredAccountManager;->storeAccountsToStorage(Ljava/util/ArrayList;)V

    .line 30
    iget-object v2, p0, Lee/cyber/smartid/manager/impl/DeleteAccountManagerImpl;->g:Lee/cyber/smartid/util/Log;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    const-string v4, "deleteLocalAccountAndData: Clearing transaction cache for "

    :try_start_9
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    .line 31
    iget-object v2, p0, Lee/cyber/smartid/manager/impl/DeleteAccountManagerImpl;->d:Lee/cyber/smartid/inter/ServiceAccess;

    invoke-interface {v2}, Lee/cyber/smartid/inter/ServiceAccess;->getTransactionCacheManager()Lee/cyber/smartid/manager/inter/TransactionCacheManager;

    move-result-object v2

    invoke-interface {v2, p1}, Lee/cyber/smartid/manager/inter/TransactionCacheManager;->removeCachedTransactionByAccountUUID(Ljava/lang/String;)V

    .line 32
    invoke-static {v1}, Lee/cyber/smartid/dto/AccountWrapper;->toWrapperList(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object p1

    monitor-exit v0

    return-object p1

    :catchall_0
    move-exception p1

    .line 33
    monitor-exit v0
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    throw p1
.end method

.method static synthetic a(Lee/cyber/smartid/manager/impl/DeleteAccountManagerImpl;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lee/cyber/smartid/manager/impl/DeleteAccountManagerImpl;->a(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .line 3
    iget-object v0, p0, Lee/cyber/smartid/manager/impl/DeleteAccountManagerImpl;->g:Lee/cyber/smartid/util/Log;

    const-string v1, "handleServerAccountDeleted"

    invoke-virtual {v0, v1}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    .line 4
    iget-object v0, p0, Lee/cyber/smartid/manager/impl/DeleteAccountManagerImpl;->d:Lee/cyber/smartid/inter/ServiceAccess;

    invoke-interface {v0}, Lee/cyber/smartid/inter/ServiceAccess;->getStoredAccountManager()Lee/cyber/smartid/manager/inter/StoredAccountManager;

    move-result-object v0

    invoke-interface {v0}, Lee/cyber/smartid/manager/inter/StoredAccountManager;->getAccountStateUpdateLock()Ljava/lang/Object;

    move-result-object v0

    monitor-enter v0

    .line 5
    :try_start_0
    invoke-direct {p0, p2}, Lee/cyber/smartid/manager/impl/DeleteAccountManagerImpl;->a(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1
    :try_end_0
    .catch Lee/cyber/smartid/cryptolib/dto/StorageException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 6
    :try_start_1
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 7
    iget-object v1, p0, Lee/cyber/smartid/manager/impl/DeleteAccountManagerImpl;->d:Lee/cyber/smartid/inter/ServiceAccess;

    new-instance v3, Lee/cyber/smartid/manager/impl/DeleteAccountManagerImpl$3;

    invoke-direct {v3, p0, p1, p2, v2}, Lee/cyber/smartid/manager/impl/DeleteAccountManagerImpl$3;-><init>(Lee/cyber/smartid/manager/impl/DeleteAccountManagerImpl;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;)V

    invoke-interface {v1, v3}, Lee/cyber/smartid/inter/ServiceAccess;->notifyUI(Ljava/lang/Runnable;)V

    .line 8
    monitor-exit v0

    return-void

    :catchall_0
    move-exception p1

    goto :goto_0

    :catch_0
    move-exception p2

    .line 9
    iget-object v1, p0, Lee/cyber/smartid/manager/impl/DeleteAccountManagerImpl;->d:Lee/cyber/smartid/inter/ServiceAccess;

    new-instance v2, Lee/cyber/smartid/manager/impl/DeleteAccountManagerImpl$2;

    invoke-direct {v2, p0, p1, p2}, Lee/cyber/smartid/manager/impl/DeleteAccountManagerImpl$2;-><init>(Lee/cyber/smartid/manager/impl/DeleteAccountManagerImpl;Ljava/lang/String;Lee/cyber/smartid/cryptolib/dto/StorageException;)V

    invoke-interface {v1, v2}, Lee/cyber/smartid/inter/ServiceAccess;->notifyUI(Ljava/lang/Runnable;)V

    .line 10
    monitor-exit v0

    return-void

    .line 11
    :goto_0
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw p1
.end method

.method static synthetic b(Lee/cyber/smartid/manager/impl/DeleteAccountManagerImpl;)Lee/cyber/smartid/inter/ListenerAccess;
    .locals 0

    .line 1
    iget-object p0, p0, Lee/cyber/smartid/manager/impl/DeleteAccountManagerImpl;->e:Lee/cyber/smartid/inter/ListenerAccess;

    return-object p0
.end method

.method static synthetic c(Lee/cyber/smartid/manager/impl/DeleteAccountManagerImpl;)Lee/cyber/smartid/inter/ServiceAccess;
    .locals 0

    .line 1
    iget-object p0, p0, Lee/cyber/smartid/manager/impl/DeleteAccountManagerImpl;->d:Lee/cyber/smartid/inter/ServiceAccess;

    return-object p0
.end method

.method static synthetic d(Lee/cyber/smartid/manager/impl/DeleteAccountManagerImpl;)Lee/cyber/smartid/tse/inter/WallClock;
    .locals 0

    .line 1
    iget-object p0, p0, Lee/cyber/smartid/manager/impl/DeleteAccountManagerImpl;->f:Lee/cyber/smartid/tse/inter/WallClock;

    return-object p0
.end method

.method public static getInstance(Lee/cyber/smartid/network/SmartIdAPI;Lee/cyber/smartid/tse/SmartIdTSE;Lee/cyber/smartid/inter/ServiceAccess;Lee/cyber/smartid/inter/ListenerAccess;Lee/cyber/smartid/tse/inter/WallClock;)Lee/cyber/smartid/manager/inter/DeleteAccountManager;
    .locals 8

    .line 1
    sget-object v0, Lee/cyber/smartid/manager/impl/DeleteAccountManagerImpl;->a:Lee/cyber/smartid/manager/inter/DeleteAccountManager;

    if-nez v0, :cond_0

    .line 2
    const-class v0, Lee/cyber/smartid/manager/impl/DeleteAccountManagerImpl;

    monitor-enter v0

    .line 3
    :try_start_0
    new-instance v7, Lee/cyber/smartid/manager/impl/DeleteAccountManagerImpl;

    move-object v1, v7

    move-object v2, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    move-object v6, p4

    invoke-direct/range {v1 .. v6}, Lee/cyber/smartid/manager/impl/DeleteAccountManagerImpl;-><init>(Lee/cyber/smartid/network/SmartIdAPI;Lee/cyber/smartid/tse/SmartIdTSE;Lee/cyber/smartid/inter/ServiceAccess;Lee/cyber/smartid/inter/ListenerAccess;Lee/cyber/smartid/tse/inter/WallClock;)V

    sput-object v7, Lee/cyber/smartid/manager/impl/DeleteAccountManagerImpl;->a:Lee/cyber/smartid/manager/inter/DeleteAccountManager;

    .line 4
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception p0

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p0

    .line 5
    :cond_0
    :goto_0
    sget-object p0, Lee/cyber/smartid/manager/impl/DeleteAccountManagerImpl;->a:Lee/cyber/smartid/manager/inter/DeleteAccountManager;

    return-object p0
.end method


# virtual methods
.method public deleteAccount(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLee/cyber/smartid/inter/DeleteAccountListener;)V
    .locals 7

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/manager/impl/DeleteAccountManagerImpl;->g:Lee/cyber/smartid/util/Log;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "deleteAccount - accountUUID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, ", reason: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    .line 2
    iget-object v0, p0, Lee/cyber/smartid/manager/impl/DeleteAccountManagerImpl;->e:Lee/cyber/smartid/inter/ListenerAccess;

    invoke-interface {v0, p1, p5}, Lee/cyber/smartid/inter/ListenerAccess;->setListener(Ljava/lang/String;Lee/cyber/smartid/inter/ServiceListener;)V

    .line 3
    new-instance p5, Lee/cyber/smartid/dto/jsonrpc/param/DeleteAccountParams;

    invoke-direct {p5, p2, p3}, Lee/cyber/smartid/dto/jsonrpc/param/DeleteAccountParams;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 4
    new-instance v2, Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCRequest;

    const-string p3, "deleteAccount"

    invoke-direct {v2, p3, p5}, Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCRequest;-><init>(Ljava/lang/String;Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCParams;)V

    .line 5
    iget-object p3, p0, Lee/cyber/smartid/manager/impl/DeleteAccountManagerImpl;->b:Lee/cyber/smartid/network/SmartIdAPI;

    invoke-interface {p3, v2}, Lee/cyber/smartid/network/SmartIdAPI;->deleteAccount(Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCRequest;)Lc/b;

    move-result-object p3

    .line 6
    new-instance p5, Lee/cyber/smartid/manager/impl/DeleteAccountManagerImpl$1;

    iget-object v3, p0, Lee/cyber/smartid/manager/impl/DeleteAccountManagerImpl;->c:Lee/cyber/smartid/tse/SmartIdTSE;

    move-object v0, p5

    move-object v1, p0

    move-object v4, p1

    move-object v5, p2

    move v6, p4

    invoke-direct/range {v0 .. v6}, Lee/cyber/smartid/manager/impl/DeleteAccountManagerImpl$1;-><init>(Lee/cyber/smartid/manager/impl/DeleteAccountManagerImpl;Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCRequest;Lee/cyber/smartid/tse/SmartIdTSE;Ljava/lang/String;Ljava/lang/String;Z)V

    invoke-interface {p3, p5}, Lc/b;->a(Lc/d;)V

    return-void
.end method
