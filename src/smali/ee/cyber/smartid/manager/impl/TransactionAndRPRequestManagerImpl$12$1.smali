.class Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$12$1;
.super Ljava/lang/Object;
.source "TransactionAndRPRequestManagerImpl.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$12;->onVerifyTransactionVerificationCodeSuccess(Ljava/lang/String;Lee/cyber/smartid/dto/jsonrpc/resp/VerifyTransactionVerificationCodeResp;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$12;


# direct methods
.method constructor <init>(Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$12;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$12$1;->a:Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$12;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 9

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$12$1;->a:Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$12;

    iget-object v0, v0, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$12;->g:Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;

    invoke-static {v0}, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;->c(Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;)Lee/cyber/smartid/inter/ListenerAccess;

    move-result-object v0

    iget-object v1, p0, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$12$1;->a:Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$12;

    iget-object v1, v1, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$12;->g:Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;

    invoke-static {v1}, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;->c(Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;)Lee/cyber/smartid/inter/ListenerAccess;

    move-result-object v1

    iget-object v2, p0, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$12$1;->a:Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$12;

    iget-object v3, v2, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$12;->b:Ljava/lang/String;

    iget-object v2, v2, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$12;->f:Ljava/lang/Class;

    const/4 v4, 0x1

    invoke-interface {v1, v3, v4, v2}, Lee/cyber/smartid/inter/ListenerAccess;->getListener(Ljava/lang/String;ZLjava/lang/Class;)Lee/cyber/smartid/inter/ServiceListener;

    move-result-object v1

    iget-object v2, p0, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$12$1;->a:Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$12;

    iget-object v3, v2, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$12;->b:Ljava/lang/String;

    iget-object v2, v2, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$12;->g:Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;

    invoke-static {v2}, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;->d(Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;)Lee/cyber/smartid/tse/inter/WallClock;

    move-result-object v2

    iget-object v5, p0, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$12$1;->a:Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$12;

    iget-object v5, v5, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$12;->g:Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;

    invoke-static {v5}, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;->a(Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;)Lee/cyber/smartid/inter/ServiceAccess;

    move-result-object v5

    invoke-interface {v5}, Lee/cyber/smartid/inter/ServiceAccess;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    sget v6, Lee/cyber/smartid/R$string;->err_wrong_verification_code:I

    new-array v4, v4, [Ljava/lang/Object;

    iget-object v7, p0, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$12$1;->a:Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$12;

    iget-object v7, v7, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$12;->a:Ljava/lang/String;

    const/4 v8, 0x0

    aput-object v7, v4, v8

    invoke-virtual {v5, v6, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    const-wide/32 v5, 0xc352

    invoke-static {v2, v5, v6, v4}, Lee/cyber/smartid/dto/SmartIdError;->from(Lee/cyber/smartid/tse/inter/WallClock;JLjava/lang/String;)Lee/cyber/smartid/dto/SmartIdError;

    move-result-object v2

    invoke-interface {v0, v1, v3, v2}, Lee/cyber/smartid/inter/ListenerAccess;->notifyError(Lee/cyber/smartid/inter/ServiceListener;Ljava/lang/String;Lee/cyber/smartid/dto/SmartIdError;)V

    return-void
.end method
