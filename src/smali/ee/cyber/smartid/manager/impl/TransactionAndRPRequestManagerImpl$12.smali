.class Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$12;
.super Ljava/lang/Object;
.source "TransactionAndRPRequestManagerImpl.java"

# interfaces
.implements Lee/cyber/smartid/inter/VerifyTransactionVerificationCodeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/inter/ConfirmTransactionListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:Ljava/lang/String;

.field final synthetic c:Lee/cyber/smartid/dto/jsonrpc/Transaction;

.field final synthetic d:Ljava/lang/String;

.field final synthetic e:Lee/cyber/smartid/inter/ConfirmTransactionListener;

.field final synthetic f:Ljava/lang/Class;

.field final synthetic g:Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;


# direct methods
.method constructor <init>(Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/dto/jsonrpc/Transaction;Ljava/lang/String;Lee/cyber/smartid/inter/ConfirmTransactionListener;Ljava/lang/Class;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$12;->g:Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;

    iput-object p2, p0, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$12;->a:Ljava/lang/String;

    iput-object p3, p0, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$12;->b:Ljava/lang/String;

    iput-object p4, p0, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$12;->c:Lee/cyber/smartid/dto/jsonrpc/Transaction;

    iput-object p5, p0, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$12;->d:Ljava/lang/String;

    iput-object p6, p0, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$12;->e:Lee/cyber/smartid/inter/ConfirmTransactionListener;

    iput-object p7, p0, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$12;->f:Ljava/lang/Class;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onVerifyTransactionVerificationCodeFailed(Ljava/lang/String;Lee/cyber/smartid/dto/SmartIdError;)V
    .locals 1

    .line 1
    iget-object p1, p0, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$12;->g:Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;

    invoke-static {p1}, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;->b(Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;)Lee/cyber/smartid/util/Log;

    move-result-object p1

    const-string v0, "checkConfirmTransactionPrerequisites - unable to verify the verification code"

    invoke-virtual {p1, v0}, Lee/cyber/smartid/util/Log;->e(Ljava/lang/String;)V

    .line 2
    iget-object p1, p0, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$12;->g:Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;

    invoke-static {p1}, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;->a(Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;)Lee/cyber/smartid/inter/ServiceAccess;

    move-result-object p1

    new-instance v0, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$12$2;

    invoke-direct {v0, p0, p2}, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$12$2;-><init>(Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$12;Lee/cyber/smartid/dto/SmartIdError;)V

    invoke-interface {p1, v0}, Lee/cyber/smartid/inter/ServiceAccess;->notifyUI(Ljava/lang/Runnable;)V

    return-void
.end method

.method public onVerifyTransactionVerificationCodeSuccess(Ljava/lang/String;Lee/cyber/smartid/dto/jsonrpc/resp/VerifyTransactionVerificationCodeResp;)V
    .locals 3

    .line 1
    invoke-virtual {p2}, Lee/cyber/smartid/dto/jsonrpc/resp/VerifyTransactionVerificationCodeResp;->isCorrectVerificationCode()Z

    move-result p1

    if-eqz p1, :cond_0

    invoke-virtual {p2}, Lee/cyber/smartid/dto/jsonrpc/resp/VerifyTransactionVerificationCodeResp;->getVerificationCode()Ljava/lang/String;

    move-result-object p1

    iget-object p2, p0, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$12;->a:Ljava/lang/String;

    invoke-static {p1, p2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 2
    iget-object p1, p0, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$12;->g:Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;

    iget-object p2, p0, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$12;->b:Ljava/lang/String;

    iget-object v0, p0, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$12;->c:Lee/cyber/smartid/dto/jsonrpc/Transaction;

    iget-object v1, p0, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$12;->d:Ljava/lang/String;

    iget-object v2, p0, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$12;->e:Lee/cyber/smartid/inter/ConfirmTransactionListener;

    invoke-static {p1, p2, v0, v1, v2}, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;->a(Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;Ljava/lang/String;Lee/cyber/smartid/dto/jsonrpc/Transaction;Ljava/lang/String;Lee/cyber/smartid/inter/ConfirmTransactionListener;)V

    goto :goto_0

    .line 3
    :cond_0
    iget-object p1, p0, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$12;->g:Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;

    invoke-static {p1}, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;->b(Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;)Lee/cyber/smartid/util/Log;

    move-result-object p1

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "checkConfirmTransactionPrerequisites - verification code "

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$12;->a:Ljava/lang/String;

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, " is not valid"

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    .line 4
    iget-object p1, p0, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$12;->g:Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;

    invoke-static {p1}, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;->a(Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;)Lee/cyber/smartid/inter/ServiceAccess;

    move-result-object p1

    new-instance p2, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$12$1;

    invoke-direct {p2, p0}, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$12$1;-><init>(Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$12;)V

    invoke-interface {p1, p2}, Lee/cyber/smartid/inter/ServiceAccess;->notifyUI(Ljava/lang/Runnable;)V

    :goto_0
    return-void
.end method
