.class Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$9;
.super Ljava/lang/Object;
.source "AddAccountManagerImpl.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;->handleSCSPResultForAddAccount(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/tse/dto/TSEError;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:Ljava/lang/String;

.field final synthetic c:Ljava/lang/String;

.field final synthetic d:Lee/cyber/smartid/tse/dto/TSEError;

.field final synthetic e:Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;


# direct methods
.method constructor <init>(Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/tse/dto/TSEError;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$9;->e:Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;

    iput-object p2, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$9;->a:Ljava/lang/String;

    iput-object p3, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$9;->b:Ljava/lang/String;

    iput-object p4, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$9;->c:Ljava/lang/String;

    iput-object p5, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$9;->d:Lee/cyber/smartid/tse/dto/TSEError;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$9;->e:Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;

    invoke-static {v0}, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;->d(Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;)Lee/cyber/smartid/util/Log;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "handleSCSPartResult - tagInternal: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$9;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, ", keyId: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$9;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, ", csrTransactionUUID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$9;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, ", error: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$9;->d:Lee/cyber/smartid/tse/dto/TSEError;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    .line 2
    iget-object v0, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$9;->e:Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;

    invoke-static {v0}, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;->c(Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;)Lee/cyber/smartid/inter/ServiceAccess;

    move-result-object v0

    invoke-interface {v0}, Lee/cyber/smartid/inter/ServiceAccess;->getStoredAccountManager()Lee/cyber/smartid/manager/inter/StoredAccountManager;

    move-result-object v0

    invoke-interface {v0}, Lee/cyber/smartid/manager/inter/StoredAccountManager;->getAccountStateUpdateLock()Ljava/lang/Object;

    move-result-object v0

    monitor-enter v0

    .line 3
    :try_start_0
    new-instance v1, Lee/cyber/smartid/util/AccountFinder;

    invoke-direct {v1}, Lee/cyber/smartid/util/AccountFinder;-><init>()V

    iget-object v2, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$9;->e:Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;

    invoke-static {v2}, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;->c(Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;)Lee/cyber/smartid/inter/ServiceAccess;

    move-result-object v2

    iget-object v3, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$9;->b:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lee/cyber/smartid/util/AccountFinder;->findOrThrow(Lee/cyber/smartid/inter/ServiceAccess;Ljava/lang/String;)Lee/cyber/smartid/util/AccountFinder;

    move-result-object v1
    :try_end_0
    .catch Lee/cyber/smartid/cryptolib/dto/StorageException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 4
    :try_start_1
    invoke-virtual {v1}, Lee/cyber/smartid/util/AccountFinder;->getAccount()Lee/cyber/smartid/dto/AccountState;

    move-result-object v2

    .line 5
    invoke-virtual {v1}, Lee/cyber/smartid/util/AccountFinder;->getKeyType()Ljava/lang/String;

    move-result-object v1

    .line 6
    iget-object v3, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$9;->d:Lee/cyber/smartid/tse/dto/TSEError;

    const/4 v4, 0x0

    if-eqz v3, :cond_0

    .line 7
    iget-object v3, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$9;->e:Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;

    invoke-static {v3}, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;->d(Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;)Lee/cyber/smartid/util/Log;

    move-result-object v3

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    const-string v6, "handleSCSPartResult - we have an error for "

    :try_start_2
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    const-string v3, "STATE_SUBMIT_CLIENT_SECOND_PART_FAILED"

    .line 8
    :try_start_3
    invoke-virtual {v2, v1, v3}, Lee/cyber/smartid/dto/AccountState;->setSubmitClientSecondPartStateByKeyType(Ljava/lang/String;Ljava/lang/String;)V

    .line 9
    iget-object v1, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$9;->d:Lee/cyber/smartid/tse/dto/TSEError;

    invoke-virtual {v2, v1}, Lee/cyber/smartid/dto/AccountState;->setLastSubmitClientSecondPartError(Lee/cyber/smartid/tse/dto/TSEError;)V

    goto :goto_0

    .line 10
    :cond_0
    iget-object v3, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$9;->e:Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;

    invoke-static {v3}, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;->d(Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;)Lee/cyber/smartid/util/Log;

    move-result-object v3

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    const-string v6, "handleSCSPartResult - we have a success case for "

    :try_start_4
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    const-string v3, "STATE_SUBMIT_CLIENT_SECOND_PART_CONFIRMED"

    .line 11
    :try_start_5
    invoke-virtual {v2, v1, v3}, Lee/cyber/smartid/dto/AccountState;->setSubmitClientSecondPartStateByKeyType(Ljava/lang/String;Ljava/lang/String;)V

    .line 12
    iget-object v3, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$9;->c:Ljava/lang/String;

    invoke-virtual {v2, v1, v3}, Lee/cyber/smartid/dto/AccountState;->setCSRTransactionUUIDByKeyType(Ljava/lang/String;Ljava/lang/String;)V

    .line 13
    invoke-virtual {v2}, Lee/cyber/smartid/dto/AccountState;->getAuthSubmitClientSecondPartState()Ljava/lang/String;

    move-result-object v1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    const-string v3, "STATE_SUBMIT_CLIENT_SECOND_PART_CONFIRMED"

    :try_start_6
    invoke-static {v1, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {v2}, Lee/cyber/smartid/dto/AccountState;->getSignSubmitClientSecondPartState()Ljava/lang/String;

    move-result-object v1
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    const-string v3, "STATE_SUBMIT_CLIENT_SECOND_PART_CONFIRMED"

    :try_start_7
    invoke-static {v1, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    if-eqz v1, :cond_1

    const-string v1, "REGISTRATION_STATE_ACCOUNT_REGISTERED"

    .line 14
    :try_start_8
    invoke-virtual {v2, v1}, Lee/cyber/smartid/dto/AccountState;->setRegistrationState(Ljava/lang/String;)V

    .line 15
    invoke-virtual {v2, v4}, Lee/cyber/smartid/dto/AccountState;->setLastSubmitClientSecondPartError(Lee/cyber/smartid/tse/dto/TSEError;)V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 16
    :cond_1
    :goto_0
    :try_start_9
    iget-object v1, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$9;->e:Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;

    invoke-static {v1}, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;->c(Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;)Lee/cyber/smartid/inter/ServiceAccess;

    move-result-object v1

    invoke-interface {v1}, Lee/cyber/smartid/inter/ServiceAccess;->getStoredAccountManager()Lee/cyber/smartid/manager/inter/StoredAccountManager;

    move-result-object v1

    invoke-interface {v1, v2}, Lee/cyber/smartid/manager/inter/StoredAccountManager;->storeAccountToStorage(Lee/cyber/smartid/dto/AccountState;)V
    :try_end_9
    .catch Lee/cyber/smartid/cryptolib/dto/StorageException; {:try_start_9 .. :try_end_9} :catch_0
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    .line 17
    :try_start_a
    iget-object v1, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$9;->e:Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;

    invoke-virtual {v1, v2}, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;->handleSCSPStateCheckForAddAccount(Lee/cyber/smartid/dto/AccountState;)V

    .line 18
    monitor-exit v0

    return-void

    :catch_0
    move-exception v1

    .line 19
    iget-object v3, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$9;->e:Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;

    iget-object v5, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$9;->e:Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;

    invoke-static {v5}, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;->c(Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;)Lee/cyber/smartid/inter/ServiceAccess;

    move-result-object v5

    invoke-interface {v5}, Lee/cyber/smartid/inter/ServiceAccess;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    iget-object v6, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$9;->e:Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;

    invoke-static {v6}, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;->b(Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;)Lee/cyber/smartid/tse/inter/WallClock;

    move-result-object v6

    invoke-static {v5, v6, v1}, Lee/cyber/smartid/dto/SmartIdError;->from(Landroid/content/Context;Lee/cyber/smartid/tse/inter/WallClock;Ljava/lang/Throwable;)Lee/cyber/smartid/dto/SmartIdError;

    move-result-object v1

    invoke-virtual {v2}, Lee/cyber/smartid/dto/AccountState;->getAccountUUID()Ljava/lang/String;

    move-result-object v2

    const/4 v5, 0x0

    invoke-static {v3, v4, v1, v2, v5}, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;->a(Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;Ljava/lang/String;Lee/cyber/smartid/dto/SmartIdError;Ljava/lang/String;Z)V

    .line 20
    monitor-exit v0

    return-void

    :catchall_0
    move-exception v1

    goto :goto_1

    :catch_1
    move-exception v1

    .line 21
    iget-object v2, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$9;->e:Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;

    invoke-static {v2}, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;->c(Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;)Lee/cyber/smartid/inter/ServiceAccess;

    move-result-object v2

    new-instance v3, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$9$1;

    invoke-direct {v3, p0, v1}, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$9$1;-><init>(Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$9;Lee/cyber/smartid/cryptolib/dto/StorageException;)V

    invoke-interface {v2, v3}, Lee/cyber/smartid/inter/ServiceAccess;->notifyUI(Ljava/lang/Runnable;)V

    .line 22
    monitor-exit v0

    return-void

    .line 23
    :goto_1
    monitor-exit v0
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    throw v1
.end method
