.class Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$2$2;
.super Ljava/lang/Object;
.source "AddAccountManagerImpl.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$2;->onGenerateDiffieHellmanKeyPairFailed(Ljava/lang/String;Lee/cyber/smartid/tse/dto/TSEError;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lee/cyber/smartid/tse/dto/TSEError;

.field final synthetic b:Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$2;


# direct methods
.method constructor <init>(Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$2;Lee/cyber/smartid/tse/dto/TSEError;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$2$2;->b:Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$2;

    iput-object p2, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$2$2;->a:Lee/cyber/smartid/tse/dto/TSEError;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$2$2;->b:Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$2;

    iget-object v0, v0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$2;->h:Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;

    invoke-static {v0}, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;->a(Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;)Lee/cyber/smartid/inter/ListenerAccess;

    move-result-object v0

    iget-object v1, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$2$2;->b:Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$2;

    iget-object v1, v1, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$2;->h:Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;

    invoke-static {v1}, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;->a(Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;)Lee/cyber/smartid/inter/ListenerAccess;

    move-result-object v1

    iget-object v2, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$2$2;->b:Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$2;

    iget-object v2, v2, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$2;->b:Ljava/lang/String;

    const-class v3, Lee/cyber/smartid/inter/AddAccountListener;

    const/4 v4, 0x1

    invoke-interface {v1, v2, v4, v3}, Lee/cyber/smartid/inter/ListenerAccess;->getListener(Ljava/lang/String;ZLjava/lang/Class;)Lee/cyber/smartid/inter/ServiceListener;

    move-result-object v1

    iget-object v2, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$2$2;->b:Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$2;

    iget-object v3, v2, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$2;->b:Ljava/lang/String;

    iget-object v2, v2, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$2;->h:Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;

    invoke-static {v2}, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;->c(Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;)Lee/cyber/smartid/inter/ServiceAccess;

    move-result-object v2

    invoke-interface {v2}, Lee/cyber/smartid/inter/ServiceAccess;->getTseErrorToServiceErrorMapper()Lee/cyber/smartid/manager/inter/TSEErrorToServiceErrorMapper;

    move-result-object v2

    iget-object v4, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$2$2;->a:Lee/cyber/smartid/tse/dto/TSEError;

    invoke-interface {v2, v4}, Lee/cyber/smartid/manager/inter/TSEErrorToServiceErrorMapper;->map(Lee/cyber/smartid/tse/dto/BaseError;)Lee/cyber/smartid/dto/SmartIdError;

    move-result-object v2

    invoke-interface {v0, v1, v3, v2}, Lee/cyber/smartid/inter/ListenerAccess;->notifyError(Lee/cyber/smartid/inter/ServiceListener;Ljava/lang/String;Lee/cyber/smartid/dto/SmartIdError;)V

    return-void
.end method
