.class public Lee/cyber/smartid/manager/impl/VerificationCodeGeneratorImpl;
.super Ljava/lang/Object;
.source "VerificationCodeGeneratorImpl.java"

# interfaces
.implements Lee/cyber/smartid/manager/inter/VerificationCodeGenerator;


# static fields
.field private static volatile a:Lee/cyber/smartid/manager/impl/VerificationCodeGeneratorImpl;


# instance fields
.field private final b:Lee/cyber/smartid/inter/ServiceAccess;

.field private final c:Lee/cyber/smartid/tse/SmartIdTSE;

.field private final d:Lee/cyber/smartid/cryptolib/CryptoLib;

.field private e:Lee/cyber/smartid/util/Log;


# direct methods
.method private constructor <init>(Lee/cyber/smartid/inter/ServiceAccess;Lee/cyber/smartid/tse/SmartIdTSE;Lee/cyber/smartid/cryptolib/CryptoLib;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    invoke-static {p0}, Lee/cyber/smartid/util/Log;->getInstance(Ljava/lang/Object;)Lee/cyber/smartid/util/Log;

    move-result-object v0

    iput-object v0, p0, Lee/cyber/smartid/manager/impl/VerificationCodeGeneratorImpl;->e:Lee/cyber/smartid/util/Log;

    .line 3
    iput-object p1, p0, Lee/cyber/smartid/manager/impl/VerificationCodeGeneratorImpl;->b:Lee/cyber/smartid/inter/ServiceAccess;

    .line 4
    iput-object p2, p0, Lee/cyber/smartid/manager/impl/VerificationCodeGeneratorImpl;->c:Lee/cyber/smartid/tse/SmartIdTSE;

    .line 5
    iput-object p3, p0, Lee/cyber/smartid/manager/impl/VerificationCodeGeneratorImpl;->d:Lee/cyber/smartid/cryptolib/CryptoLib;

    return-void
.end method

.method private a(Ljava/lang/String;II)Ljava/util/ArrayList;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "II)",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lee/cyber/smartid/dto/VerificationCodeException;
        }
    .end annotation

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    if-nez p2, :cond_0

    return-object v0

    :cond_0
    const/4 v1, 0x0

    .line 2
    :cond_1
    :goto_0
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v2, p2, :cond_2

    const v2, 0x186a0

    if-ge v1, v2, :cond_2

    add-int/lit8 v1, v1, 0x1

    .line 3
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {p0, v2}, Lee/cyber/smartid/manager/impl/VerificationCodeGeneratorImpl;->a(I)Ljava/lang/String;

    move-result-object v2

    .line 4
    iget-object v3, p0, Lee/cyber/smartid/manager/impl/VerificationCodeGeneratorImpl;->b:Lee/cyber/smartid/inter/ServiceAccess;

    invoke-interface {v3}, Lee/cyber/smartid/inter/ServiceAccess;->getVerificationCodeEvaluator()Lee/cyber/smartid/manager/inter/VerificationCodeEvaluator;

    move-result-object v3

    invoke-interface {v3, v2, p1, v0, p3}, Lee/cyber/smartid/manager/inter/VerificationCodeEvaluator;->isValidVerificationCodeCandidate(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;I)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 5
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 6
    :cond_2
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result p1

    if-lt p1, p2, :cond_3

    .line 7
    iget-object p1, p0, Lee/cyber/smartid/manager/impl/VerificationCodeGeneratorImpl;->e:Lee/cyber/smartid/util/Log;

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string p3, "generateFakeVerificationCodes: Generation finished with "

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p3, " loops."

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    return-object v0

    .line 8
    :cond_3
    iget-object p1, p0, Lee/cyber/smartid/manager/impl/VerificationCodeGeneratorImpl;->e:Lee/cyber/smartid/util/Log;

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string p3, "generateFakeVerificationCodes: Generation failed as max loop count "

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p3, " was reached, aborting"

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Lee/cyber/smartid/util/Log;->e(Ljava/lang/String;)V

    .line 9
    new-instance p1, Lee/cyber/smartid/dto/VerificationCodeException;

    iget-object p2, p0, Lee/cyber/smartid/manager/impl/VerificationCodeGeneratorImpl;->b:Lee/cyber/smartid/inter/ServiceAccess;

    invoke-interface {p2}, Lee/cyber/smartid/inter/ServiceAccess;->getApplicationContext()Landroid/content/Context;

    move-result-object p2

    sget p3, Lee/cyber/smartid/R$string;->err_additional_verification_code_generation_failed:I

    invoke-virtual {p2, p3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p2

    const-wide/32 v0, 0xc353

    invoke-direct {p1, v0, v1, p2}, Lee/cyber/smartid/dto/VerificationCodeException;-><init>(JLjava/lang/String;)V

    throw p1
.end method

.method public static getInstance(Lee/cyber/smartid/inter/ServiceAccess;Lee/cyber/smartid/tse/SmartIdTSE;Lee/cyber/smartid/cryptolib/CryptoLib;)Lee/cyber/smartid/manager/impl/VerificationCodeGeneratorImpl;
    .locals 2

    .line 1
    sget-object v0, Lee/cyber/smartid/manager/impl/VerificationCodeGeneratorImpl;->a:Lee/cyber/smartid/manager/impl/VerificationCodeGeneratorImpl;

    if-nez v0, :cond_0

    .line 2
    const-class v0, Lee/cyber/smartid/manager/impl/VerificationCodeGeneratorImpl;

    monitor-enter v0

    .line 3
    :try_start_0
    new-instance v1, Lee/cyber/smartid/manager/impl/VerificationCodeGeneratorImpl;

    invoke-direct {v1, p0, p1, p2}, Lee/cyber/smartid/manager/impl/VerificationCodeGeneratorImpl;-><init>(Lee/cyber/smartid/inter/ServiceAccess;Lee/cyber/smartid/tse/SmartIdTSE;Lee/cyber/smartid/cryptolib/CryptoLib;)V

    sput-object v1, Lee/cyber/smartid/manager/impl/VerificationCodeGeneratorImpl;->a:Lee/cyber/smartid/manager/impl/VerificationCodeGeneratorImpl;

    .line 4
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception p0

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p0

    .line 5
    :cond_0
    :goto_0
    sget-object p0, Lee/cyber/smartid/manager/impl/VerificationCodeGeneratorImpl;->a:Lee/cyber/smartid/manager/impl/VerificationCodeGeneratorImpl;

    return-object p0
.end method


# virtual methods
.method a(I)Ljava/lang/String;
    .locals 6

    .line 10
    iget-object v0, p0, Lee/cyber/smartid/manager/impl/VerificationCodeGeneratorImpl;->e:Lee/cyber/smartid/util/Log;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "generateFakeVerificationCodeCandidate - length: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    add-int/lit8 v0, p1, 0x1

    int-to-double v0, v0

    const-wide/high16 v2, 0x4024000000000000L    # 10.0

    .line 11
    invoke-static {v2, v3, v0, v1}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v0

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    sub-double/2addr v0, v2

    const-wide v2, 0x41dfffffffc00000L    # 2.147483647E9

    cmpl-double v4, v0, v2

    if-gez v4, :cond_1

    .line 12
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v3, 0x0

    const/4 v4, 0x0

    :goto_0
    add-int/lit8 v5, p1, -0x1

    if-ge v4, v5, :cond_0

    const-string v5, "0"

    .line 13
    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 14
    :cond_0
    iget-object v4, p0, Lee/cyber/smartid/manager/impl/VerificationCodeGeneratorImpl;->d:Lee/cyber/smartid/cryptolib/CryptoLib;

    invoke-virtual {v4}, Lee/cyber/smartid/cryptolib/CryptoLib;->getRandom()Ljava/security/SecureRandom;

    move-result-object v4

    double-to-int v0, v0

    invoke-static {v4, v3, v0}, Lee/cyber/smartid/util/Util;->generateRandomIntegerFromRange(Ljava/util/Random;II)I

    move-result v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 15
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    sub-int/2addr v0, p1

    invoke-static {v3, v0}, Ljava/lang/Math;->max(II)I

    move-result p1

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    invoke-virtual {v2, p1, v0}, Ljava/lang/StringBuilder;->substring(II)Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 16
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "generateFakeVerificationCodeCandidate: Length of "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, " is too large and not supported."

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method a(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lee/cyber/smartid/dto/VerificationCodeException;
        }
    .end annotation

    .line 17
    :try_start_0
    iget-object v0, p0, Lee/cyber/smartid/manager/impl/VerificationCodeGeneratorImpl;->c:Lee/cyber/smartid/tse/SmartIdTSE;

    invoke-virtual {v0, p1}, Lee/cyber/smartid/tse/SmartIdTSE;->generateVerificationCode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    .line 18
    :catch_0
    new-instance p1, Lee/cyber/smartid/dto/VerificationCodeException;

    iget-object v0, p0, Lee/cyber/smartid/manager/impl/VerificationCodeGeneratorImpl;->b:Lee/cyber/smartid/inter/ServiceAccess;

    invoke-interface {v0}, Lee/cyber/smartid/inter/ServiceAccess;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    sget v1, Lee/cyber/smartid/R$string;->err_verification_code_generation_failed:I

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-wide/32 v1, 0xc351

    invoke-direct {p1, v1, v2, v0}, Lee/cyber/smartid/dto/VerificationCodeException;-><init>(JLjava/lang/String;)V

    throw p1
.end method

.method public generateRealAndFakeVerificationCodes(Ljava/lang/String;II)Ljava/util/ArrayList;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "II)",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lee/cyber/smartid/dto/VerificationCodeException;
        }
    .end annotation

    .line 1
    invoke-virtual {p0, p1}, Lee/cyber/smartid/manager/impl/VerificationCodeGeneratorImpl;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 2
    invoke-direct {p0, p1, p2, p3}, Lee/cyber/smartid/manager/impl/VerificationCodeGeneratorImpl;->a(Ljava/lang/String;II)Ljava/util/ArrayList;

    move-result-object p2

    .line 3
    new-instance p3, Ljava/util/ArrayList;

    invoke-direct {p3}, Ljava/util/ArrayList;-><init>()V

    .line 4
    invoke-virtual {p3, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 5
    invoke-virtual {p3, p2}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 6
    iget-object p1, p0, Lee/cyber/smartid/manager/impl/VerificationCodeGeneratorImpl;->d:Lee/cyber/smartid/cryptolib/CryptoLib;

    invoke-virtual {p1}, Lee/cyber/smartid/cryptolib/CryptoLib;->getRandom()Ljava/security/SecureRandom;

    move-result-object p1

    invoke-static {p3, p1}, Ljava/util/Collections;->shuffle(Ljava/util/List;Ljava/util/Random;)V

    return-object p3
.end method

.method public isValidVerificationCode(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lee/cyber/smartid/dto/VerificationCodeException;
        }
    .end annotation

    .line 1
    invoke-virtual {p0, p2}, Lee/cyber/smartid/manager/impl/VerificationCodeGeneratorImpl;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    invoke-static {p2, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result p1

    return p1
.end method
