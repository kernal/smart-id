.class Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$5$1;
.super Ljava/lang/Object;
.source "AddAccountManagerImpl.java"

# interfaces
.implements Lee/cyber/smartid/tse/inter/ValidateKeyCreationResponseListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$5;->onValidateKeyCreationResponseSuccess(Ljava/lang/String;Lee/cyber/smartid/tse/dto/jsonrpc/resp/ValidateKeyCreationResp;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$5;


# direct methods
.method constructor <init>(Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$5;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$5$1;->a:Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$5;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onValidateKeyCreationResponseFailed(Ljava/lang/String;Lee/cyber/smartid/tse/dto/TSEError;)V
    .locals 3

    .line 1
    iget-object p1, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$5$1;->a:Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$5;

    iget-object p1, p1, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$5;->g:Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;

    invoke-static {p1}, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;->d(Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;)Lee/cyber/smartid/util/Log;

    move-result-object p1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "verifyResponseForAddAccount - sign onValidateKeyCreationResponseFailed: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lee/cyber/smartid/util/Log;->e(Ljava/lang/String;)V

    .line 2
    iget-object p1, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$5$1;->a:Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$5;

    iget-object v0, p1, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$5;->g:Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;

    iget-object p1, p1, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$5;->a:Ljava/lang/String;

    invoke-static {v0}, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;->c(Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;)Lee/cyber/smartid/inter/ServiceAccess;

    move-result-object v1

    invoke-interface {v1}, Lee/cyber/smartid/inter/ServiceAccess;->getTseErrorToServiceErrorMapper()Lee/cyber/smartid/manager/inter/TSEErrorToServiceErrorMapper;

    move-result-object v1

    invoke-interface {v1, p2}, Lee/cyber/smartid/manager/inter/TSEErrorToServiceErrorMapper;->map(Lee/cyber/smartid/tse/dto/BaseError;)Lee/cyber/smartid/dto/SmartIdError;

    move-result-object p2

    iget-object v1, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$5$1;->a:Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$5;

    iget-object v1, v1, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$5;->b:Lee/cyber/smartid/dto/jsonrpc/result/RegisterAccountResult;

    invoke-virtual {v1}, Lee/cyber/smartid/dto/jsonrpc/result/RegisterAccountResult;->getAccountUUID()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {v0, p1, p2, v1, v2}, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;->a(Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;Ljava/lang/String;Lee/cyber/smartid/dto/SmartIdError;Ljava/lang/String;Z)V

    return-void
.end method

.method public onValidateKeyCreationResponseSuccess(Ljava/lang/String;Lee/cyber/smartid/tse/dto/jsonrpc/resp/ValidateKeyCreationResp;)V
    .locals 7

    .line 1
    invoke-virtual {p2}, Lee/cyber/smartid/tse/dto/jsonrpc/resp/ValidateKeyCreationResp;->isValid()Z

    move-result p1

    if-nez p1, :cond_0

    .line 2
    iget-object p1, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$5$1;->a:Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$5;

    iget-object p1, p1, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$5;->g:Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;

    invoke-static {p1}, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;->d(Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;)Lee/cyber/smartid/util/Log;

    move-result-object p1

    const-string p2, "verifyResponseForAddAccount - sign is not valid!"

    invoke-virtual {p1, p2}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    .line 3
    iget-object p1, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$5$1;->a:Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$5;

    iget-object p2, p1, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$5;->g:Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;

    iget-object p1, p1, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$5;->a:Ljava/lang/String;

    invoke-static {p2}, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;->b(Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;)Lee/cyber/smartid/tse/inter/WallClock;

    move-result-object v0

    iget-object v1, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$5$1;->a:Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$5;

    iget-object v1, v1, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$5;->g:Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;

    invoke-static {v1}, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;->c(Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;)Lee/cyber/smartid/inter/ServiceAccess;

    move-result-object v1

    invoke-interface {v1}, Lee/cyber/smartid/inter/ServiceAccess;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    sget v2, Lee/cyber/smartid/R$string;->err_failed_to_cryptographically_validate_servers_response:I

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-wide/16 v2, 0x40c

    invoke-static {v0, v2, v3, v1}, Lee/cyber/smartid/dto/SmartIdError;->from(Lee/cyber/smartid/tse/inter/WallClock;JLjava/lang/String;)Lee/cyber/smartid/dto/SmartIdError;

    move-result-object v0

    iget-object v1, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$5$1;->a:Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$5;

    iget-object v1, v1, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$5;->b:Lee/cyber/smartid/dto/jsonrpc/result/RegisterAccountResult;

    invoke-virtual {v1}, Lee/cyber/smartid/dto/jsonrpc/result/RegisterAccountResult;->getAccountUUID()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {p2, p1, v0, v1, v2}, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;->a(Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;Ljava/lang/String;Lee/cyber/smartid/dto/SmartIdError;Ljava/lang/String;Z)V

    .line 4
    :cond_0
    iget-object p1, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$5$1;->a:Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$5;

    iget-object p1, p1, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$5;->g:Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;

    invoke-static {p1}, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;->d(Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;)Lee/cyber/smartid/util/Log;

    move-result-object p1

    const-string p2, "verifyResponseForAddAccount - sign is valid"

    invoke-virtual {p1, p2}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    .line 5
    iget-object p1, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$5$1;->a:Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$5;

    iget-object v0, p1, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$5;->g:Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;

    iget-object v1, p1, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$5;->a:Ljava/lang/String;

    iget-object v2, p1, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$5;->d:Ljava/lang/String;

    iget-object v3, p1, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$5;->c:Ljava/lang/String;

    iget-object v4, p1, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$5;->b:Lee/cyber/smartid/dto/jsonrpc/result/RegisterAccountResult;

    iget-object v5, p1, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$5;->e:Lee/cyber/smartid/dto/jsonrpc/param/RegisterDeviceData;

    iget-object v6, p1, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$5;->f:Ljava/lang/String;

    invoke-static/range {v0 .. v6}, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;->b(Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/dto/jsonrpc/result/RegisterAccountResult;Lee/cyber/smartid/dto/jsonrpc/param/RegisterDeviceData;Ljava/lang/String;)V

    return-void
.end method
