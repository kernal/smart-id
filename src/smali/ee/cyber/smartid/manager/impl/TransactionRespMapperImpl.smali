.class public Lee/cyber/smartid/manager/impl/TransactionRespMapperImpl;
.super Ljava/lang/Object;
.source "TransactionRespMapperImpl.java"

# interfaces
.implements Lee/cyber/smartid/manager/inter/TransactionRespMapper;


# static fields
.field private static volatile a:Lee/cyber/smartid/manager/impl/TransactionRespMapperImpl;


# instance fields
.field private final b:Lee/cyber/smartid/inter/ServiceAccess;

.field private c:Lee/cyber/smartid/util/Log;


# direct methods
.method private constructor <init>(Lee/cyber/smartid/inter/ServiceAccess;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    invoke-static {p0}, Lee/cyber/smartid/util/Log;->getInstance(Ljava/lang/Object;)Lee/cyber/smartid/util/Log;

    move-result-object v0

    iput-object v0, p0, Lee/cyber/smartid/manager/impl/TransactionRespMapperImpl;->c:Lee/cyber/smartid/util/Log;

    .line 3
    iput-object p1, p0, Lee/cyber/smartid/manager/impl/TransactionRespMapperImpl;->b:Lee/cyber/smartid/inter/ServiceAccess;

    return-void
.end method

.method public static getInstance(Lee/cyber/smartid/inter/ServiceAccess;)Lee/cyber/smartid/manager/impl/TransactionRespMapperImpl;
    .locals 2

    .line 1
    sget-object v0, Lee/cyber/smartid/manager/impl/TransactionRespMapperImpl;->a:Lee/cyber/smartid/manager/impl/TransactionRespMapperImpl;

    if-nez v0, :cond_0

    .line 2
    const-class v0, Lee/cyber/smartid/manager/impl/TransactionRespMapperImpl;

    monitor-enter v0

    .line 3
    :try_start_0
    new-instance v1, Lee/cyber/smartid/manager/impl/TransactionRespMapperImpl;

    invoke-direct {v1, p0}, Lee/cyber/smartid/manager/impl/TransactionRespMapperImpl;-><init>(Lee/cyber/smartid/inter/ServiceAccess;)V

    sput-object v1, Lee/cyber/smartid/manager/impl/TransactionRespMapperImpl;->a:Lee/cyber/smartid/manager/impl/TransactionRespMapperImpl;

    .line 4
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception p0

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p0

    .line 5
    :cond_0
    :goto_0
    sget-object p0, Lee/cyber/smartid/manager/impl/TransactionRespMapperImpl;->a:Lee/cyber/smartid/manager/impl/TransactionRespMapperImpl;

    return-object p0
.end method


# virtual methods
.method public isValidVerificationCodeMapping(Ljava/lang/String;Lee/cyber/smartid/dto/jsonrpc/Transaction;)Z
    .locals 1

    .line 1
    :try_start_0
    iget-object v0, p0, Lee/cyber/smartid/manager/impl/TransactionRespMapperImpl;->b:Lee/cyber/smartid/inter/ServiceAccess;

    invoke-interface {v0}, Lee/cyber/smartid/inter/ServiceAccess;->getVerificationCodeGenerator()Lee/cyber/smartid/manager/inter/VerificationCodeGenerator;

    move-result-object v0

    invoke-virtual {p2}, Lee/cyber/smartid/dto/jsonrpc/Transaction;->getHash()Ljava/lang/String;

    move-result-object p2

    invoke-interface {v0, p1, p2}, Lee/cyber/smartid/manager/inter/VerificationCodeGenerator;->isValidVerificationCode(Ljava/lang/String;Ljava/lang/String;)Z

    move-result p1
    :try_end_0
    .catch Lee/cyber/smartid/dto/VerificationCodeException; {:try_start_0 .. :try_end_0} :catch_0

    return p1

    :catch_0
    move-exception p1

    .line 2
    iget-object p2, p0, Lee/cyber/smartid/manager/impl/TransactionRespMapperImpl;->c:Lee/cyber/smartid/util/Log;

    const-string v0, "isValidVerificationCodeMapping"

    invoke-virtual {p2, v0, p1}, Lee/cyber/smartid/util/Log;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    const/4 p1, 0x0

    return p1
.end method

.method public map(Ljava/lang/String;Lee/cyber/smartid/dto/jsonrpc/Transaction;)Lee/cyber/smartid/dto/jsonrpc/resp/GetTransactionResp;
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    const-class v0, Lee/cyber/smartid/manager/impl/TransactionRespMapperImpl;

    monitor-enter v0

    .line 2
    :try_start_0
    new-instance v1, Lee/cyber/smartid/dto/jsonrpc/resp/GetTransactionResp;

    iget-object v2, p0, Lee/cyber/smartid/manager/impl/TransactionRespMapperImpl;->b:Lee/cyber/smartid/inter/ServiceAccess;

    invoke-interface {v2}, Lee/cyber/smartid/inter/ServiceAccess;->getVerificationCodeGenerator()Lee/cyber/smartid/manager/inter/VerificationCodeGenerator;

    move-result-object v2

    invoke-virtual {p2}, Lee/cyber/smartid/dto/jsonrpc/Transaction;->getHash()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p2}, Lee/cyber/smartid/dto/jsonrpc/Transaction;->isMultiCodeVerification()Z

    move-result v4

    const/4 v5, 0x0

    if-eqz v4, :cond_0

    iget-object v4, p0, Lee/cyber/smartid/manager/impl/TransactionRespMapperImpl;->b:Lee/cyber/smartid/inter/ServiceAccess;

    invoke-interface {v4}, Lee/cyber/smartid/inter/ServiceAccess;->getPropertiesManager()Lee/cyber/smartid/manager/inter/PropertiesManager;

    move-result-object v4

    invoke-interface {v4}, Lee/cyber/smartid/manager/inter/PropertiesManager;->getPropMultiCodeVerificationFakeCodeCount()Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    goto :goto_0

    :cond_0
    const/4 v4, 0x0

    :goto_0
    iget-object v6, p0, Lee/cyber/smartid/manager/impl/TransactionRespMapperImpl;->b:Lee/cyber/smartid/inter/ServiceAccess;

    invoke-interface {v6}, Lee/cyber/smartid/inter/ServiceAccess;->getPropertiesManager()Lee/cyber/smartid/manager/inter/PropertiesManager;

    move-result-object v6

    invoke-interface {v6}, Lee/cyber/smartid/manager/inter/PropertiesManager;->getPropMultiCodeVerificationFakeCodeMinLevenshteinDistance()Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    invoke-interface {v2, v3, v4, v6}, Lee/cyber/smartid/manager/inter/VerificationCodeGenerator;->generateRealAndFakeVerificationCodes(Ljava/lang/String;II)Ljava/util/ArrayList;

    move-result-object v2

    new-array v3, v5, [Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Ljava/lang/String;

    invoke-direct {v1, p1, p2, v2}, Lee/cyber/smartid/dto/jsonrpc/resp/GetTransactionResp;-><init>(Ljava/lang/String;Lee/cyber/smartid/dto/jsonrpc/Transaction;[Ljava/lang/String;)V

    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception p1

    .line 3
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method
