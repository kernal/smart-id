.class Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$7;
.super Ljava/lang/Object;
.source "AddAccountManagerImpl.java"

# interfaces
.implements Lee/cyber/smartid/tse/inter/InitializeKeyAndKeyStatesListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/dto/jsonrpc/result/RegisterAccountResult;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:Lee/cyber/smartid/dto/jsonrpc/result/RegisterAccountResult;

.field final synthetic c:Ljava/lang/String;

.field final synthetic d:Ljava/lang/String;

.field final synthetic e:Ljava/lang/String;

.field final synthetic f:Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;


# direct methods
.method constructor <init>(Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;Ljava/lang/String;Lee/cyber/smartid/dto/jsonrpc/result/RegisterAccountResult;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$7;->f:Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;

    iput-object p2, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$7;->a:Ljava/lang/String;

    iput-object p3, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$7;->b:Lee/cyber/smartid/dto/jsonrpc/result/RegisterAccountResult;

    iput-object p4, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$7;->c:Ljava/lang/String;

    iput-object p5, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$7;->d:Ljava/lang/String;

    iput-object p6, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$7;->e:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private b(Lee/cyber/smartid/tse/dto/jsonrpc/resp/BaseResp;Lee/cyber/smartid/tse/dto/jsonrpc/resp/BaseResp;)V
    .locals 10

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$7;->f:Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;

    invoke-static {v0}, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;->d(Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;)Lee/cyber/smartid/util/Log;

    move-result-object v0

    const-string v1, "handleInitializeKeyAndKeyStatesSuccessForSign AUTH and SIGN success."

    invoke-virtual {v0, v1}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    .line 2
    iget-object v2, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$7;->f:Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;

    iget-object v3, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$7;->a:Ljava/lang/String;

    iget-object v4, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$7;->d:Ljava/lang/String;

    iget-object v5, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$7;->c:Ljava/lang/String;

    iget-object v6, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$7;->b:Lee/cyber/smartid/dto/jsonrpc/result/RegisterAccountResult;

    iget-object v7, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$7;->e:Ljava/lang/String;

    move-object v8, p1

    move-object v9, p2

    invoke-static/range {v2 .. v9}, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;->a(Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/dto/jsonrpc/result/RegisterAccountResult;Ljava/lang/String;Lee/cyber/smartid/tse/dto/jsonrpc/resp/BaseResp;Lee/cyber/smartid/tse/dto/jsonrpc/resp/BaseResp;)V

    return-void
.end method


# virtual methods
.method a(Lee/cyber/smartid/tse/dto/jsonrpc/resp/BaseResp;)V
    .locals 9

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$7;->f:Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;

    invoke-static {v0}, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;->e(Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;)Lee/cyber/smartid/tse/SmartIdTSE;

    move-result-object v1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ee.cyber.smartid.TAG_INITIALIZE_KEY_AND_STATES_"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$7;->c:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$7;->b:Lee/cyber/smartid/dto/jsonrpc/result/RegisterAccountResult;

    invoke-virtual {v0}, Lee/cyber/smartid/dto/jsonrpc/result/RegisterAccountResult;->getAccountUUID()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$7;->c:Ljava/lang/String;

    iget-object v0, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$7;->f:Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;

    invoke-static {v0}, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;->c(Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;)Lee/cyber/smartid/inter/ServiceAccess;

    move-result-object v0

    iget-object v5, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$7;->b:Lee/cyber/smartid/dto/jsonrpc/result/RegisterAccountResult;

    invoke-virtual {v5}, Lee/cyber/smartid/dto/jsonrpc/result/RegisterAccountResult;->getAccountUUID()Ljava/lang/String;

    move-result-object v5

    const-string v6, "SIGNATURE"

    invoke-interface {v0, v5, v6}, Lee/cyber/smartid/inter/ServiceAccess;->getKeyId(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iget-object v0, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$7;->b:Lee/cyber/smartid/dto/jsonrpc/result/RegisterAccountResult;

    invoke-virtual {v0}, Lee/cyber/smartid/dto/jsonrpc/result/RegisterAccountResult;->getSignData()Lee/cyber/smartid/tse/dto/jsonrpc/KeyData;

    move-result-object v7

    new-instance v8, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$7$1;

    invoke-direct {v8, p0, p1}, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$7$1;-><init>(Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$7;Lee/cyber/smartid/tse/dto/jsonrpc/resp/BaseResp;)V

    const-string v6, "SIGNATURE"

    invoke-virtual/range {v1 .. v8}, Lee/cyber/smartid/tse/SmartIdTSE;->initializeKeyAndKeyStates(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/tse/dto/jsonrpc/KeyData;Lee/cyber/smartid/tse/inter/InitializeKeyAndKeyStatesListener;)V

    return-void
.end method

.method a(Lee/cyber/smartid/tse/dto/jsonrpc/resp/BaseResp;Lee/cyber/smartid/tse/dto/jsonrpc/resp/BaseResp;)V
    .locals 0

    .line 2
    invoke-direct {p0, p1, p2}, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$7;->b(Lee/cyber/smartid/tse/dto/jsonrpc/resp/BaseResp;Lee/cyber/smartid/tse/dto/jsonrpc/resp/BaseResp;)V

    return-void
.end method

.method public onInitializeKeyAndKeyStatesFailed(Ljava/lang/String;Lee/cyber/smartid/tse/dto/TSEError;)V
    .locals 3

    .line 1
    iget-object p1, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$7;->f:Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;

    invoke-static {p1}, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;->d(Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;)Lee/cyber/smartid/util/Log;

    move-result-object p1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "initializeKeysAndAccountForAddAccount auth onInitializeKeyAndKeyStatesFailed: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lee/cyber/smartid/util/Log;->e(Ljava/lang/String;)V

    .line 2
    iget-object p1, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$7;->f:Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;

    iget-object v0, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$7;->a:Ljava/lang/String;

    invoke-static {p1}, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;->c(Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;)Lee/cyber/smartid/inter/ServiceAccess;

    move-result-object v1

    invoke-interface {v1}, Lee/cyber/smartid/inter/ServiceAccess;->getTseErrorToServiceErrorMapper()Lee/cyber/smartid/manager/inter/TSEErrorToServiceErrorMapper;

    move-result-object v1

    invoke-interface {v1, p2}, Lee/cyber/smartid/manager/inter/TSEErrorToServiceErrorMapper;->map(Lee/cyber/smartid/tse/dto/BaseError;)Lee/cyber/smartid/dto/SmartIdError;

    move-result-object p2

    iget-object v1, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$7;->b:Lee/cyber/smartid/dto/jsonrpc/result/RegisterAccountResult;

    invoke-virtual {v1}, Lee/cyber/smartid/dto/jsonrpc/result/RegisterAccountResult;->getAccountUUID()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {p1, v0, p2, v1, v2}, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;->a(Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;Ljava/lang/String;Lee/cyber/smartid/dto/SmartIdError;Ljava/lang/String;Z)V

    return-void
.end method

.method public onInitializeKeyAndKeyStatesSuccess(Ljava/lang/String;)V
    .locals 1

    .line 1
    iget-object p1, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$7;->f:Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;

    invoke-static {p1}, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;->d(Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;)Lee/cyber/smartid/util/Log;

    move-result-object p1

    const-string v0, "onInitializeKeyAndKeyStatesSuccess old callback called for AUTH"

    invoke-virtual {p1, v0}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    const/4 p1, 0x0

    .line 2
    invoke-virtual {p0, p1}, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$7;->a(Lee/cyber/smartid/tse/dto/jsonrpc/resp/BaseResp;)V

    return-void
.end method

.method public onInitializeKeyAndKeyStatesSuccess(Ljava/lang/String;Lee/cyber/smartid/tse/dto/jsonrpc/resp/BaseResp;)V
    .locals 1

    .line 3
    iget-object p1, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$7;->f:Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;

    invoke-static {p1}, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;->d(Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;)Lee/cyber/smartid/util/Log;

    move-result-object p1

    const-string v0, "onInitializeKeyAndKeyStatesSuccess new callback called for AUTH"

    invoke-virtual {p1, v0}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    .line 4
    invoke-virtual {p0, p2}, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$7;->a(Lee/cyber/smartid/tse/dto/jsonrpc/resp/BaseResp;)V

    return-void
.end method
