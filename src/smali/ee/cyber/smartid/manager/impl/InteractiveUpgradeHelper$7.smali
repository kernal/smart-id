.class Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper$7;
.super Ljava/lang/Object;
.source "InteractiveUpgradeHelper.java"

# interfaces
.implements Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper$ValidateResponseCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->a(Lee/cyber/smartid/dto/upgrade/InteractiveUpgradeState;Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/dto/jsonrpc/param/UpgradeKeyPairParams;)Lee/cyber/smartid/dto/SmartIdError;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;


# direct methods
.method constructor <init>(Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper$7;->a:Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public isValidResponse(Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCResponse;)Z
    .locals 1

    .line 1
    invoke-virtual {p1}, Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCResponse;->getResult()Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Lee/cyber/smartid/dto/jsonrpc/result/UpgradeKeyPairResult;

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCResponse;->getResult()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lee/cyber/smartid/dto/jsonrpc/result/UpgradeKeyPairResult;

    invoke-virtual {p1}, Lee/cyber/smartid/dto/jsonrpc/result/UpgradeKeyPairResult;->isValid()Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method
