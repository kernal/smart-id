.class Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl$1;
.super Ljava/lang/Object;
.source "SafetyNetManagerImpl.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl;->startSafetyNetCheckAsync()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl;


# direct methods
.method constructor <init>(Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl$1;->a:Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 10

    const-string v0, "startSafetyNetCheckBlocking"

    const/4 v1, 0x0

    .line 1
    :try_start_0
    iget-object v2, p0, Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl$1;->a:Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl;

    invoke-static {v2}, Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl;->a(Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl;)Lee/cyber/smartid/util/Log;

    move-result-object v2

    invoke-virtual {v2, v0}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    .line 2
    iget-object v2, p0, Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl$1;->a:Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl;

    const/4 v3, 0x1

    invoke-static {v2, v3}, Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl;->a(Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl;Z)Z

    .line 3
    iget-object v4, p0, Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl$1;->a:Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl;

    iget-object v2, p0, Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl$1;->a:Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl;

    invoke-static {v2}, Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl;->b(Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl;)Lee/cyber/smartid/inter/ServiceAccess;

    move-result-object v2

    invoke-interface {v2}, Lee/cyber/smartid/inter/ServiceAccess;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    iget-object v2, p0, Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl$1;->a:Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl;

    invoke-static {v2}, Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl;->c(Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl;)Lee/cyber/smartid/cryptolib/CryptoLib;

    move-result-object v6

    iget-object v2, p0, Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl$1;->a:Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl;

    invoke-static {v2}, Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl;->b(Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl;)Lee/cyber/smartid/inter/ServiceAccess;

    move-result-object v2

    invoke-interface {v2}, Lee/cyber/smartid/tse/inter/ExternalResourceAccess;->getDeviceFingerPrint()Ljava/lang/String;

    move-result-object v7

    iget-object v2, p0, Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl$1;->a:Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl;

    invoke-static {v2}, Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl;->d(Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl;)Lee/cyber/smartid/tse/inter/WallClock;

    move-result-object v8

    iget-object v2, p0, Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl$1;->a:Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl;

    invoke-static {v2}, Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl;->b(Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl;)Lee/cyber/smartid/inter/ServiceAccess;

    move-result-object v2

    invoke-interface {v2}, Lee/cyber/smartid/inter/ServiceAccess;->getPropertiesManager()Lee/cyber/smartid/manager/inter/PropertiesManager;

    move-result-object v2

    invoke-interface {v2}, Lee/cyber/smartid/manager/inter/PropertiesManager;->getPropSafetyNetAPIKey()Ljava/lang/String;

    move-result-object v9

    invoke-static/range {v4 .. v9}, Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl;->a(Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl;Landroid/content/Context;Lee/cyber/smartid/cryptolib/CryptoLib;Ljava/lang/String;Lee/cyber/smartid/tse/inter/WallClock;Ljava/lang/String;)Lee/cyber/smartid/dto/SafetyNetAttestation;

    move-result-object v2

    .line 4
    iget-object v3, p0, Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl$1;->a:Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl;

    invoke-static {v3, v2}, Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl;->a(Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl;Lee/cyber/smartid/dto/SafetyNetAttestation;)Lee/cyber/smartid/dto/SafetyNetAttestation;
    :try_end_0
    .catch Ljava/lang/Error; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 5
    :try_start_1
    iget-object v3, p0, Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl$1;->a:Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl;

    invoke-static {v3}, Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl;->e(Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl;)Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    .line 6
    iget-object v3, p0, Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl$1;->a:Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl;

    invoke-static {v3}, Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl;->a(Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl;)Lee/cyber/smartid/util/Log;

    move-result-object v3
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    const-string v4, "startSafetyNetCheckBlocking - notifying UI listeners"

    :try_start_2
    invoke-virtual {v3, v4}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    .line 7
    iget-object v3, p0, Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl$1;->a:Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl;

    invoke-static {v3}, Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl;->e(Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl;)Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .line 8
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 9
    iget-object v4, p0, Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl$1;->a:Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl;

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-static {v4, v5, v2}, Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl;->a(Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl;Ljava/lang/String;Lee/cyber/smartid/dto/SafetyNetAttestation;)V

    .line 10
    invoke-interface {v3}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 11
    :cond_0
    iget-object v2, p0, Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl$1;->a:Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl;

    invoke-static {v2}, Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl;->a(Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl;)Lee/cyber/smartid/util/Log;

    move-result-object v2
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    const-string v3, "startSafetyNetCheckBlocking - no UI listeners found"

    :try_start_3
    invoke-virtual {v2, v3}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    :catch_0
    move-exception v2

    .line 12
    :try_start_4
    iget-object v3, p0, Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl$1;->a:Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl;

    invoke-static {v3}, Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl;->a(Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl;)Lee/cyber/smartid/util/Log;

    move-result-object v3

    invoke-virtual {v3, v0, v2}, Lee/cyber/smartid/util/Log;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 13
    :cond_1
    :goto_1
    iget-object v2, p0, Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl$1;->a:Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl;

    invoke-static {v2}, Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl;->a(Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl;)Lee/cyber/smartid/util/Log;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V
    :try_end_4
    .catch Ljava/lang/Error; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    const-string v4, "startSafetyNetCheckBlocking - SafetyNet result: "

    :try_start_5
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v4, p0, Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl$1;->a:Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl;

    invoke-static {v4}, Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl;->f(Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl;)Lee/cyber/smartid/dto/SafetyNetAttestation;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    .line 14
    iget-object v2, p0, Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl$1;->a:Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl;

    invoke-static {v2}, Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl;->a(Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl;)Lee/cyber/smartid/util/Log;

    move-result-object v2
    :try_end_5
    .catch Ljava/lang/Error; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    const-string v3, "startSafetyNetCheckBlocking - updateDeviceForPostInitIfNeeded .."

    :try_start_6
    invoke-virtual {v2, v3}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    .line 15
    iget-object v2, p0, Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl$1;->a:Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl;

    invoke-static {v2}, Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl;->b(Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl;)Lee/cyber/smartid/inter/ServiceAccess;

    move-result-object v2

    invoke-interface {v2}, Lee/cyber/smartid/inter/ServiceAccess;->updateDeviceForPostInitIfNeeded()V
    :try_end_6
    .catch Ljava/lang/Error; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 16
    iget-object v0, p0, Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl$1;->a:Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl;

    invoke-static {v0}, Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl;->b(Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl;)Lee/cyber/smartid/inter/ServiceAccess;

    move-result-object v0

    invoke-interface {v0}, Lee/cyber/smartid/inter/ServiceAccess;->releaseWakeLockForService()V

    .line 17
    iget-object v0, p0, Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl$1;->a:Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl;

    invoke-static {v0, v1}, Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl;->a(Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl;Z)Z

    goto :goto_2

    :catchall_0
    move-exception v0

    goto :goto_3

    :catch_1
    move-exception v2

    .line 18
    :try_start_7
    iget-object v3, p0, Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl$1;->a:Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl;

    invoke-static {v3}, Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl;->a(Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl;)Lee/cyber/smartid/util/Log;

    move-result-object v3

    invoke-virtual {v3, v0, v2}, Lee/cyber/smartid/util/Log;->e(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 19
    iget-object v0, p0, Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl$1;->a:Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl;

    invoke-static {v0}, Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl;->b(Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl;)Lee/cyber/smartid/inter/ServiceAccess;

    move-result-object v0

    invoke-interface {v0}, Lee/cyber/smartid/inter/ServiceAccess;->releaseWakeLockForService()V

    .line 20
    iget-object v0, p0, Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl$1;->a:Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl;

    invoke-static {v0, v1}, Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl;->a(Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl;Z)Z

    :goto_2
    return-void

    .line 21
    :goto_3
    iget-object v2, p0, Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl$1;->a:Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl;

    invoke-static {v2}, Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl;->b(Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl;)Lee/cyber/smartid/inter/ServiceAccess;

    move-result-object v2

    invoke-interface {v2}, Lee/cyber/smartid/inter/ServiceAccess;->releaseWakeLockForService()V

    .line 22
    iget-object v2, p0, Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl$1;->a:Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl;

    invoke-static {v2, v1}, Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl;->a(Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl;Z)Z

    throw v0
.end method
