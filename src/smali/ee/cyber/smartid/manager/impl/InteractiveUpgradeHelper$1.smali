.class Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper$1;
.super Ljava/lang/Object;
.source "InteractiveUpgradeHelper.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->cancelInteractiveUpgrade(Ljava/lang/String;Lee/cyber/smartid/inter/CancelInteractiveUpgradeListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;


# direct methods
.method constructor <init>(Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper$1;->b:Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;

    iput-object p2, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper$1;->a:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper$1;->b:Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;

    invoke-static {v0}, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->a(Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2
    iget-object v0, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper$1;->b:Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;

    invoke-static {v0}, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->d(Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;)Lee/cyber/smartid/inter/ServiceAccess;

    move-result-object v0

    new-instance v1, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper$1$1;

    invoke-direct {v1, p0}, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper$1$1;-><init>(Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper$1;)V

    invoke-interface {v0, v1}, Lee/cyber/smartid/inter/ServiceAccess;->notifyUI(Ljava/lang/Runnable;)V

    return-void

    .line 3
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 4
    iget-object v1, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper$1;->b:Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;

    invoke-static {v1}, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->e(Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;)Lee/cyber/smartid/dto/upgrade/InteractiveUpgradeState;

    move-result-object v1

    .line 5
    invoke-virtual {v1}, Lee/cyber/smartid/dto/upgrade/InteractiveUpgradeState;->getStateUpgradeToMDv2()I

    move-result v2

    const/4 v3, 0x3

    if-eq v2, v3, :cond_1

    .line 6
    :try_start_0
    iget-object v2, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper$1;->b:Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;

    invoke-static {v2}, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->f(Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;)V

    .line 7
    invoke-virtual {v1, v3}, Lee/cyber/smartid/dto/upgrade/InteractiveUpgradeState;->setStateUpgradeToMDv2(I)V

    .line 8
    iget-object v2, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper$1;->b:Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;

    invoke-static {v2, v1}, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->a(Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;Lee/cyber/smartid/dto/upgrade/InteractiveUpgradeState;)V

    const/4 v1, 0x0

    .line 9
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lee/cyber/smartid/cryptolib/dto/StorageException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 10
    iget-object v1, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper$1;->b:Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;

    invoke-static {v1}, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->g(Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;)Lee/cyber/smartid/util/Log;

    move-result-object v1

    const-string v2, "cancelInteractiveUpgrade"

    invoke-virtual {v1, v2, v0}, Lee/cyber/smartid/util/Log;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 11
    iget-object v1, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper$1;->b:Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;

    invoke-static {v1}, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->d(Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;)Lee/cyber/smartid/inter/ServiceAccess;

    move-result-object v1

    new-instance v2, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper$1$2;

    invoke-direct {v2, p0, v0}, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper$1$2;-><init>(Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper$1;Lee/cyber/smartid/cryptolib/dto/StorageException;)V

    invoke-interface {v1, v2}, Lee/cyber/smartid/inter/ServiceAccess;->notifyUI(Ljava/lang/Runnable;)V

    return-void

    .line 12
    :cond_1
    :goto_0
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_2

    .line 13
    iget-object v1, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper$1;->b:Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;

    invoke-static {v1}, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->d(Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;)Lee/cyber/smartid/inter/ServiceAccess;

    move-result-object v1

    new-instance v2, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper$1$3;

    invoke-direct {v2, p0, v0}, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper$1$3;-><init>(Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper$1;Ljava/util/ArrayList;)V

    invoke-interface {v1, v2}, Lee/cyber/smartid/inter/ServiceAccess;->notifyUI(Ljava/lang/Runnable;)V

    goto :goto_1

    .line 14
    :cond_2
    iget-object v0, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper$1;->b:Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;

    invoke-static {v0}, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->d(Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;)Lee/cyber/smartid/inter/ServiceAccess;

    move-result-object v0

    new-instance v1, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper$1$4;

    invoke-direct {v1, p0}, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper$1$4;-><init>(Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper$1;)V

    invoke-interface {v0, v1}, Lee/cyber/smartid/inter/ServiceAccess;->notifyUI(Ljava/lang/Runnable;)V

    :goto_1
    return-void
.end method
