.class Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$10;
.super Ljava/lang/Object;
.source "TransactionAndRPRequestManagerImpl.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:Ljava/lang/Class;

.field final synthetic c:Lee/cyber/smartid/dto/jsonrpc/resp/VerifyTransactionVerificationCodeResp;

.field final synthetic d:Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;


# direct methods
.method constructor <init>(Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;Ljava/lang/String;Ljava/lang/Class;Lee/cyber/smartid/dto/jsonrpc/resp/VerifyTransactionVerificationCodeResp;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$10;->d:Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;

    iput-object p2, p0, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$10;->a:Ljava/lang/String;

    iput-object p3, p0, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$10;->b:Ljava/lang/Class;

    iput-object p4, p0, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$10;->c:Lee/cyber/smartid/dto/jsonrpc/resp/VerifyTransactionVerificationCodeResp;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$10;->d:Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;

    invoke-static {v0}, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;->c(Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;)Lee/cyber/smartid/inter/ListenerAccess;

    move-result-object v0

    iget-object v1, p0, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$10;->a:Ljava/lang/String;

    iget-object v2, p0, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$10;->b:Ljava/lang/Class;

    const/4 v3, 0x1

    invoke-interface {v0, v1, v3, v2}, Lee/cyber/smartid/inter/ListenerAccess;->getListener(Ljava/lang/String;ZLjava/lang/Class;)Lee/cyber/smartid/inter/ServiceListener;

    move-result-object v0

    check-cast v0, Lee/cyber/smartid/inter/VerifyTransactionVerificationCodeListener;

    if-eqz v0, :cond_0

    .line 2
    iget-object v1, p0, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$10;->a:Ljava/lang/String;

    iget-object v2, p0, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$10;->c:Lee/cyber/smartid/dto/jsonrpc/resp/VerifyTransactionVerificationCodeResp;

    invoke-interface {v0, v1, v2}, Lee/cyber/smartid/inter/VerifyTransactionVerificationCodeListener;->onVerifyTransactionVerificationCodeSuccess(Ljava/lang/String;Lee/cyber/smartid/dto/jsonrpc/resp/VerifyTransactionVerificationCodeResp;)V

    :cond_0
    return-void
.end method
