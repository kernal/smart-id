.class Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$17;
.super Lee/cyber/smartid/tse/network/RPCCallback;
.source "TransactionAndRPRequestManagerImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;->getRPRequest(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/inter/GetRPRequestListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lee/cyber/smartid/tse/network/RPCCallback<",
        "Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCResponse<",
        "Lee/cyber/smartid/dto/jsonrpc/result/GetRPRequestResult;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:Ljava/lang/String;

.field final synthetic c:Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;


# direct methods
.method constructor <init>(Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCRequest;Lee/cyber/smartid/tse/SmartIdTSE;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$17;->c:Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;

    iput-object p4, p0, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$17;->a:Ljava/lang/String;

    iput-object p5, p0, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$17;->b:Ljava/lang/String;

    invoke-direct {p0, p2, p3}, Lee/cyber/smartid/tse/network/RPCCallback;-><init>(Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCRequest;Lee/cyber/smartid/tse/SmartIdTSE;)V

    return-void
.end method


# virtual methods
.method public onFailure(Lc/b;Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCError;Ljava/lang/Throwable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lc/b<",
            "Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCResponse<",
            "Lee/cyber/smartid/dto/jsonrpc/result/GetRPRequestResult;",
            ">;>;",
            "Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCError;",
            "Ljava/lang/Throwable;",
            ")V"
        }
    .end annotation

    .line 1
    iget-object p1, p0, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$17;->c:Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;

    invoke-static {p1}, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;->a(Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;)Lee/cyber/smartid/inter/ServiceAccess;

    move-result-object p1

    new-instance v0, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$17$1;

    invoke-direct {v0, p0, p2, p3}, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$17$1;-><init>(Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$17;Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCError;Ljava/lang/Throwable;)V

    invoke-interface {p1, v0}, Lee/cyber/smartid/inter/ServiceAccess;->notifyUI(Ljava/lang/Runnable;)V

    return-void
.end method

.method public onSuccess(Lc/b;Lc/r;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lc/b<",
            "Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCResponse<",
            "Lee/cyber/smartid/dto/jsonrpc/result/GetRPRequestResult;",
            ">;>;",
            "Lc/r<",
            "Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCResponse<",
            "Lee/cyber/smartid/dto/jsonrpc/result/GetRPRequestResult;",
            ">;>;)V"
        }
    .end annotation

    .line 1
    iget-object p1, p0, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$17;->c:Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;

    invoke-static {p1}, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;->c(Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;)Lee/cyber/smartid/inter/ListenerAccess;

    move-result-object p1

    iget-object v0, p0, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$17;->a:Ljava/lang/String;

    const-class v1, Lee/cyber/smartid/inter/GetRPRequestListener;

    const/4 v2, 0x1

    invoke-interface {p1, v0, v2, v1}, Lee/cyber/smartid/inter/ListenerAccess;->getListener(Ljava/lang/String;ZLjava/lang/Class;)Lee/cyber/smartid/inter/ServiceListener;

    move-result-object p1

    check-cast p1, Lee/cyber/smartid/inter/GetRPRequestListener;

    .line 2
    iget-object v0, p0, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$17;->c:Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;

    invoke-static {v0}, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;->b(Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;)Lee/cyber/smartid/util/Log;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getRPRequest - onResponse, listener "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    if-nez p1, :cond_0

    return-void

    .line 3
    :cond_0
    invoke-virtual {p2}, Lc/r;->e()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCResponse;

    invoke-virtual {p2}, Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCResponse;->getResult()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lee/cyber/smartid/dto/jsonrpc/result/GetRPRequestResult;

    .line 4
    iget-object v8, p0, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$17;->a:Ljava/lang/String;

    new-instance v9, Lee/cyber/smartid/dto/jsonrpc/resp/GetRPRequestResp;

    iget-object v2, p0, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$17;->b:Ljava/lang/String;

    invoke-virtual {p2}, Lee/cyber/smartid/dto/jsonrpc/result/GetRPRequestResult;->getRpRequest()Lee/cyber/smartid/dto/jsonrpc/RPRequest;

    move-result-object v0

    invoke-virtual {v0}, Lee/cyber/smartid/dto/jsonrpc/RPRequest;->getRpRequestUUID()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p2}, Lee/cyber/smartid/dto/jsonrpc/result/GetRPRequestResult;->getRpRequest()Lee/cyber/smartid/dto/jsonrpc/RPRequest;

    move-result-object v0

    invoke-virtual {v0}, Lee/cyber/smartid/dto/jsonrpc/RPRequest;->getRpName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p2}, Lee/cyber/smartid/dto/jsonrpc/result/GetRPRequestResult;->getRpRequest()Lee/cyber/smartid/dto/jsonrpc/RPRequest;

    move-result-object v0

    invoke-virtual {v0}, Lee/cyber/smartid/dto/jsonrpc/RPRequest;->getRequestType()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p2}, Lee/cyber/smartid/dto/jsonrpc/result/GetRPRequestResult;->getRpRequest()Lee/cyber/smartid/dto/jsonrpc/RPRequest;

    move-result-object p2

    invoke-virtual {p2}, Lee/cyber/smartid/dto/jsonrpc/RPRequest;->getTtlSec()J

    move-result-wide v6

    move-object v0, v9

    move-object v1, v8

    invoke-direct/range {v0 .. v7}, Lee/cyber/smartid/dto/jsonrpc/resp/GetRPRequestResp;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;J)V

    invoke-interface {p1, v8, v9}, Lee/cyber/smartid/inter/GetRPRequestListener;->onGetRPRequestSuccess(Ljava/lang/String;Lee/cyber/smartid/dto/jsonrpc/resp/GetRPRequestResp;)V

    return-void
.end method

.method public onValidate(Lc/b;Lc/r;)Z
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lc/b<",
            "Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCResponse<",
            "Lee/cyber/smartid/dto/jsonrpc/result/GetRPRequestResult;",
            ">;>;",
            "Lc/r<",
            "Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCResponse<",
            "Lee/cyber/smartid/dto/jsonrpc/result/GetRPRequestResult;",
            ">;>;)Z"
        }
    .end annotation

    .line 1
    invoke-static {p2}, Lee/cyber/smartid/util/Util;->validateResponse(Lc/r;)Z

    move-result p1

    return p1
.end method
