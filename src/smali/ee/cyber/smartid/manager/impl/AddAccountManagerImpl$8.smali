.class Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$8;
.super Ljava/lang/Object;
.source "AddAccountManagerImpl.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/dto/jsonrpc/result/RegisterAccountResult;Ljava/lang/String;Lee/cyber/smartid/tse/dto/jsonrpc/resp/BaseResp;Lee/cyber/smartid/tse/dto/jsonrpc/resp/BaseResp;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lee/cyber/smartid/dto/jsonrpc/result/RegisterAccountResult;

.field final synthetic b:Ljava/lang/String;

.field final synthetic c:Ljava/lang/String;

.field final synthetic d:Ljava/lang/String;

.field final synthetic e:Lee/cyber/smartid/tse/dto/jsonrpc/resp/BaseResp;

.field final synthetic f:Lee/cyber/smartid/tse/dto/jsonrpc/resp/BaseResp;

.field final synthetic g:Ljava/lang/String;

.field final synthetic h:Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;


# direct methods
.method constructor <init>(Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;Lee/cyber/smartid/dto/jsonrpc/result/RegisterAccountResult;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/tse/dto/jsonrpc/resp/BaseResp;Lee/cyber/smartid/tse/dto/jsonrpc/resp/BaseResp;Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$8;->h:Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;

    iput-object p2, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$8;->a:Lee/cyber/smartid/dto/jsonrpc/result/RegisterAccountResult;

    iput-object p3, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$8;->b:Ljava/lang/String;

    iput-object p4, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$8;->c:Ljava/lang/String;

    iput-object p5, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$8;->d:Ljava/lang/String;

    iput-object p6, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$8;->e:Lee/cyber/smartid/tse/dto/jsonrpc/resp/BaseResp;

    iput-object p7, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$8;->f:Lee/cyber/smartid/tse/dto/jsonrpc/resp/BaseResp;

    iput-object p8, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$8;->g:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 11

    .line 1
    :try_start_0
    iget-object v0, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$8;->h:Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;

    invoke-static {v0}, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;->e(Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;)Lee/cyber/smartid/tse/SmartIdTSE;

    move-result-object v0

    iget-object v1, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$8;->h:Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;

    invoke-static {v1}, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;->c(Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;)Lee/cyber/smartid/inter/ServiceAccess;

    move-result-object v1

    iget-object v2, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$8;->a:Lee/cyber/smartid/dto/jsonrpc/result/RegisterAccountResult;

    invoke-virtual {v2}, Lee/cyber/smartid/dto/jsonrpc/result/RegisterAccountResult;->getAccountUUID()Ljava/lang/String;

    move-result-object v2
    :try_end_0
    .catch Lee/cyber/smartid/cryptolib/dto/StorageException; {:try_start_0 .. :try_end_0} :catch_0

    const-string v3, "AUTHENTICATION"

    :try_start_1
    invoke-interface {v1, v2, v3}, Lee/cyber/smartid/inter/ServiceAccess;->getKeyId(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$8;->a:Lee/cyber/smartid/dto/jsonrpc/result/RegisterAccountResult;

    invoke-virtual {v2}, Lee/cyber/smartid/dto/jsonrpc/result/RegisterAccountResult;->getAuthData()Lee/cyber/smartid/tse/dto/jsonrpc/KeyData;

    move-result-object v2

    invoke-virtual {v2}, Lee/cyber/smartid/tse/dto/jsonrpc/KeyData;->getFreshnessToken()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lee/cyber/smartid/tse/SmartIdTSE;->updateFreshnessToken(Ljava/lang/String;Ljava/lang/String;)V

    .line 2
    iget-object v0, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$8;->h:Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;

    invoke-static {v0}, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;->e(Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;)Lee/cyber/smartid/tse/SmartIdTSE;

    move-result-object v0

    iget-object v1, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$8;->h:Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;

    invoke-static {v1}, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;->c(Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;)Lee/cyber/smartid/inter/ServiceAccess;

    move-result-object v1

    iget-object v2, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$8;->a:Lee/cyber/smartid/dto/jsonrpc/result/RegisterAccountResult;

    invoke-virtual {v2}, Lee/cyber/smartid/dto/jsonrpc/result/RegisterAccountResult;->getAccountUUID()Ljava/lang/String;

    move-result-object v2
    :try_end_1
    .catch Lee/cyber/smartid/cryptolib/dto/StorageException; {:try_start_1 .. :try_end_1} :catch_0

    const-string v3, "SIGNATURE"

    :try_start_2
    invoke-interface {v1, v2, v3}, Lee/cyber/smartid/inter/ServiceAccess;->getKeyId(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$8;->a:Lee/cyber/smartid/dto/jsonrpc/result/RegisterAccountResult;

    invoke-virtual {v2}, Lee/cyber/smartid/dto/jsonrpc/result/RegisterAccountResult;->getSignData()Lee/cyber/smartid/tse/dto/jsonrpc/KeyData;

    move-result-object v2

    invoke-virtual {v2}, Lee/cyber/smartid/tse/dto/jsonrpc/KeyData;->getFreshnessToken()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lee/cyber/smartid/tse/SmartIdTSE;->updateFreshnessToken(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Lee/cyber/smartid/cryptolib/dto/StorageException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    .line 3
    :catch_0
    iget-object v0, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$8;->h:Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;

    invoke-static {v0}, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;->d(Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;)Lee/cyber/smartid/util/Log;

    move-result-object v0

    const-string v1, "createSCSPStatesForAddAccount - failed to update freshnessTokens!"

    invoke-virtual {v0, v1}, Lee/cyber/smartid/util/Log;->e(Ljava/lang/String;)V

    .line 4
    :goto_0
    iget-object v0, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$8;->h:Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;

    invoke-static {v0}, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;->c(Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;)Lee/cyber/smartid/inter/ServiceAccess;

    move-result-object v0

    invoke-interface {v0}, Lee/cyber/smartid/inter/ServiceAccess;->getStoredAccountManager()Lee/cyber/smartid/manager/inter/StoredAccountManager;

    move-result-object v0

    invoke-interface {v0}, Lee/cyber/smartid/manager/inter/StoredAccountManager;->getAccountStateUpdateLock()Ljava/lang/Object;

    move-result-object v0

    monitor-enter v0

    .line 5
    :try_start_3
    iget-object v1, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$8;->a:Lee/cyber/smartid/dto/jsonrpc/result/RegisterAccountResult;

    invoke-virtual {v1}, Lee/cyber/smartid/dto/jsonrpc/result/RegisterAccountResult;->getAccountUUID()Ljava/lang/String;

    move-result-object v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 6
    :try_start_4
    iget-object v2, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$8;->h:Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;

    invoke-static {v2}, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;->c(Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;)Lee/cyber/smartid/inter/ServiceAccess;

    move-result-object v2

    invoke-interface {v2}, Lee/cyber/smartid/inter/ServiceAccess;->getStoredAccountManager()Lee/cyber/smartid/manager/inter/StoredAccountManager;

    move-result-object v2

    iget-object v4, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$8;->b:Ljava/lang/String;

    iget-object v5, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$8;->c:Ljava/lang/String;

    iget-object v6, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$8;->d:Ljava/lang/String;

    iget-object v7, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$8;->a:Lee/cyber/smartid/dto/jsonrpc/result/RegisterAccountResult;

    iget-object v8, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$8;->e:Lee/cyber/smartid/tse/dto/jsonrpc/resp/BaseResp;

    iget-object v9, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$8;->f:Lee/cyber/smartid/tse/dto/jsonrpc/resp/BaseResp;

    iget-object v10, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$8;->g:Ljava/lang/String;

    move-object v3, v1

    invoke-interface/range {v2 .. v10}, Lee/cyber/smartid/manager/inter/StoredAccountManager;->initializeAccountState(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/dto/jsonrpc/result/RegisterAccountResult;Lee/cyber/smartid/tse/dto/jsonrpc/resp/BaseResp;Lee/cyber/smartid/tse/dto/jsonrpc/resp/BaseResp;Ljava/lang/String;)Lee/cyber/smartid/dto/AccountState;
    :try_end_4
    .catch Lee/cyber/smartid/cryptolib/dto/StorageException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 7
    :try_start_5
    iget-object v1, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$8;->h:Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;

    invoke-static {v1}, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;->c(Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;)Lee/cyber/smartid/inter/ServiceAccess;

    move-result-object v1

    iget-object v2, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$8;->a:Lee/cyber/smartid/dto/jsonrpc/result/RegisterAccountResult;

    invoke-virtual {v2}, Lee/cyber/smartid/dto/jsonrpc/result/RegisterAccountResult;->getAccountUUID()Ljava/lang/String;

    move-result-object v2
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    const-string v3, "AUTHENTICATION"

    :try_start_6
    invoke-interface {v1, v2, v3}, Lee/cyber/smartid/inter/ServiceAccess;->getKeyId(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 8
    iget-object v1, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$8;->h:Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;

    invoke-static {v1}, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;->e(Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;)Lee/cyber/smartid/tse/SmartIdTSE;

    move-result-object v4

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    const-string v2, "ee.cyber.smartid.TAG_SUBMIT_CLIENT_SECOND_PART_"

    :try_start_7
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    iget-object v7, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$8;->c:Ljava/lang/String;
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    const-string v8, "AUTHENTICATION"

    :try_start_8
    iget-object v1, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$8;->a:Lee/cyber/smartid/dto/jsonrpc/result/RegisterAccountResult;

    invoke-virtual {v1}, Lee/cyber/smartid/dto/jsonrpc/result/RegisterAccountResult;->getAccountUUID()Ljava/lang/String;

    move-result-object v9

    const/4 v10, 0x0

    invoke-virtual/range {v4 .. v10}, Lee/cyber/smartid/tse/SmartIdTSE;->submitClientSecondPart(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/tse/inter/SubmitClientSecondPartListener;)V

    .line 9
    iget-object v1, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$8;->h:Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;

    invoke-static {v1}, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;->c(Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;)Lee/cyber/smartid/inter/ServiceAccess;

    move-result-object v1

    iget-object v2, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$8;->a:Lee/cyber/smartid/dto/jsonrpc/result/RegisterAccountResult;

    invoke-virtual {v2}, Lee/cyber/smartid/dto/jsonrpc/result/RegisterAccountResult;->getAccountUUID()Ljava/lang/String;

    move-result-object v2
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    const-string v3, "SIGNATURE"

    :try_start_9
    invoke-interface {v1, v2, v3}, Lee/cyber/smartid/inter/ServiceAccess;->getKeyId(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 10
    iget-object v1, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$8;->h:Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;

    invoke-static {v1}, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;->e(Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;)Lee/cyber/smartid/tse/SmartIdTSE;

    move-result-object v4

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    const-string v2, "ee.cyber.smartid.TAG_SUBMIT_CLIENT_SECOND_PART_"

    :try_start_a
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    iget-object v7, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$8;->d:Ljava/lang/String;
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    const-string v8, "SIGNATURE"

    :try_start_b
    iget-object v1, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$8;->a:Lee/cyber/smartid/dto/jsonrpc/result/RegisterAccountResult;

    invoke-virtual {v1}, Lee/cyber/smartid/dto/jsonrpc/result/RegisterAccountResult;->getAccountUUID()Ljava/lang/String;

    move-result-object v9

    const/4 v10, 0x0

    invoke-virtual/range {v4 .. v10}, Lee/cyber/smartid/tse/SmartIdTSE;->submitClientSecondPart(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/tse/inter/SubmitClientSecondPartListener;)V

    .line 11
    monitor-exit v0

    return-void

    :catch_1
    move-exception v2

    .line 12
    iget-object v3, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$8;->h:Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;

    invoke-static {v3}, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;->d(Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;)Lee/cyber/smartid/util/Log;

    move-result-object v3
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    const-string v4, "createSCSPStatesForAddAccount"

    :try_start_c
    invoke-virtual {v3, v4, v2}, Lee/cyber/smartid/util/Log;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 13
    iget-object v3, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$8;->h:Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;

    iget-object v4, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$8;->g:Ljava/lang/String;

    iget-object v5, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$8;->h:Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;

    invoke-static {v5}, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;->c(Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;)Lee/cyber/smartid/inter/ServiceAccess;

    move-result-object v5

    invoke-interface {v5}, Lee/cyber/smartid/inter/ServiceAccess;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    iget-object v6, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$8;->h:Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;

    invoke-static {v6}, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;->b(Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;)Lee/cyber/smartid/tse/inter/WallClock;

    move-result-object v6

    invoke-static {v5, v6, v2}, Lee/cyber/smartid/dto/SmartIdError;->from(Landroid/content/Context;Lee/cyber/smartid/tse/inter/WallClock;Ljava/lang/Throwable;)Lee/cyber/smartid/dto/SmartIdError;

    move-result-object v2

    const/4 v5, 0x1

    invoke-static {v3, v4, v2, v1, v5}, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;->a(Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;Ljava/lang/String;Lee/cyber/smartid/dto/SmartIdError;Ljava/lang/String;Z)V

    .line 14
    monitor-exit v0

    return-void

    :catchall_0
    move-exception v1

    .line 15
    monitor-exit v0
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_0

    throw v1
.end method
