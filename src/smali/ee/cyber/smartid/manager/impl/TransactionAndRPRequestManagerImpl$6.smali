.class Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$6;
.super Ljava/lang/Object;
.source "TransactionAndRPRequestManagerImpl.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;->a(Lee/cyber/smartid/dto/jsonrpc/resp/GetTransactionResp;Ljava/lang/String;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:Ljava/lang/String;

.field final synthetic c:Lee/cyber/smartid/dto/jsonrpc/resp/GetTransactionResp;

.field final synthetic d:Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;


# direct methods
.method constructor <init>(Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/dto/jsonrpc/resp/GetTransactionResp;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$6;->d:Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;

    iput-object p2, p0, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$6;->a:Ljava/lang/String;

    iput-object p3, p0, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$6;->b:Ljava/lang/String;

    iput-object p4, p0, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$6;->c:Lee/cyber/smartid/dto/jsonrpc/resp/GetTransactionResp;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$6;->d:Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;

    invoke-static {v0}, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;->c(Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;)Lee/cyber/smartid/inter/ListenerAccess;

    move-result-object v0

    iget-object v1, p0, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$6;->a:Ljava/lang/String;

    const-class v2, Lee/cyber/smartid/inter/CreateTransactionForRPRequestListener;

    const/4 v3, 0x1

    invoke-interface {v0, v1, v3, v2}, Lee/cyber/smartid/inter/ListenerAccess;->getListener(Ljava/lang/String;ZLjava/lang/Class;)Lee/cyber/smartid/inter/ServiceListener;

    move-result-object v0

    check-cast v0, Lee/cyber/smartid/inter/CreateTransactionForRPRequestListener;

    .line 2
    iget-object v1, p0, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$6;->d:Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;

    invoke-static {v1}, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;->b(Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;)Lee/cyber/smartid/util/Log;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "createTransactionForRPRequest - onResponse, listener: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    if-nez v0, :cond_0

    return-void

    .line 3
    :cond_0
    iget-object v1, p0, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$6;->a:Ljava/lang/String;

    new-instance v2, Lee/cyber/smartid/dto/jsonrpc/resp/CreateTransactionForRpRequestResp;

    iget-object v3, p0, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$6;->b:Ljava/lang/String;

    iget-object v4, p0, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$6;->c:Lee/cyber/smartid/dto/jsonrpc/resp/GetTransactionResp;

    invoke-direct {v2, v3, v4}, Lee/cyber/smartid/dto/jsonrpc/resp/CreateTransactionForRpRequestResp;-><init>(Ljava/lang/String;Lee/cyber/smartid/dto/jsonrpc/resp/GetTransactionResp;)V

    invoke-interface {v0, v1, v2}, Lee/cyber/smartid/inter/CreateTransactionForRPRequestListener;->onCreateTransactionForRPRequestSuccess(Ljava/lang/String;Lee/cyber/smartid/dto/jsonrpc/resp/CreateTransactionForRpRequestResp;)V

    return-void
.end method
