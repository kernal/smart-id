.class Lee/cyber/smartid/manager/impl/TSEListenerToServiceListenerMapperImpl$1;
.super Ljava/lang/Object;
.source "TSEListenerToServiceListenerMapperImpl.java"

# interfaces
.implements Lee/cyber/smartid/tse/inter/GenerateKeysListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lee/cyber/smartid/manager/impl/TSEListenerToServiceListenerMapperImpl;->a()Lee/cyber/smartid/tse/inter/TSEListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lee/cyber/smartid/manager/impl/TSEListenerToServiceListenerMapperImpl;


# direct methods
.method constructor <init>(Lee/cyber/smartid/manager/impl/TSEListenerToServiceListenerMapperImpl;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lee/cyber/smartid/manager/impl/TSEListenerToServiceListenerMapperImpl$1;->a:Lee/cyber/smartid/manager/impl/TSEListenerToServiceListenerMapperImpl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onGenerateKeysFailed(Ljava/lang/String;Lee/cyber/smartid/tse/dto/TSEError;)V
    .locals 3

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/manager/impl/TSEListenerToServiceListenerMapperImpl$1;->a:Lee/cyber/smartid/manager/impl/TSEListenerToServiceListenerMapperImpl;

    invoke-static {v0}, Lee/cyber/smartid/manager/impl/TSEListenerToServiceListenerMapperImpl;->a(Lee/cyber/smartid/manager/impl/TSEListenerToServiceListenerMapperImpl;)Lee/cyber/smartid/inter/ListenerAccess;

    move-result-object v0

    const-class v1, Lee/cyber/smartid/inter/GenerateKeysListener;

    const/4 v2, 0x1

    invoke-interface {v0, p1, v2, v1}, Lee/cyber/smartid/inter/ListenerAccess;->getListener(Ljava/lang/String;ZLjava/lang/Class;)Lee/cyber/smartid/inter/ServiceListener;

    move-result-object v0

    check-cast v0, Lee/cyber/smartid/inter/GenerateKeysListener;

    if-eqz v0, :cond_0

    .line 2
    iget-object v1, p0, Lee/cyber/smartid/manager/impl/TSEListenerToServiceListenerMapperImpl$1;->a:Lee/cyber/smartid/manager/impl/TSEListenerToServiceListenerMapperImpl;

    invoke-static {v1}, Lee/cyber/smartid/manager/impl/TSEListenerToServiceListenerMapperImpl;->b(Lee/cyber/smartid/manager/impl/TSEListenerToServiceListenerMapperImpl;)Lee/cyber/smartid/inter/ServiceAccess;

    move-result-object v1

    invoke-interface {v1}, Lee/cyber/smartid/inter/ServiceAccess;->getTseErrorToServiceErrorMapper()Lee/cyber/smartid/manager/inter/TSEErrorToServiceErrorMapper;

    move-result-object v1

    invoke-interface {v1, p2}, Lee/cyber/smartid/manager/inter/TSEErrorToServiceErrorMapper;->map(Lee/cyber/smartid/tse/dto/BaseError;)Lee/cyber/smartid/dto/SmartIdError;

    move-result-object p2

    invoke-interface {v0, p1, p2}, Lee/cyber/smartid/inter/GenerateKeysListener;->onGenerateKeysFailed(Ljava/lang/String;Lee/cyber/smartid/dto/SmartIdError;)V

    :cond_0
    return-void
.end method

.method public onGenerateKeysPRNGTestUpdate(Ljava/lang/String;[Lee/cyber/smartid/cryptolib/dto/TestResult;)V
    .locals 0

    .line 1
    iget-object p1, p0, Lee/cyber/smartid/manager/impl/TSEListenerToServiceListenerMapperImpl$1;->a:Lee/cyber/smartid/manager/impl/TSEListenerToServiceListenerMapperImpl;

    invoke-static {p1}, Lee/cyber/smartid/manager/impl/TSEListenerToServiceListenerMapperImpl;->c(Lee/cyber/smartid/manager/impl/TSEListenerToServiceListenerMapperImpl;)Lee/cyber/smartid/tse/SmartIdTSE;

    move-result-object p1

    invoke-virtual {p1}, Lee/cyber/smartid/tse/SmartIdTSE;->isDeviceRegistrationDone()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 2
    iget-object p1, p0, Lee/cyber/smartid/manager/impl/TSEListenerToServiceListenerMapperImpl$1;->a:Lee/cyber/smartid/manager/impl/TSEListenerToServiceListenerMapperImpl;

    invoke-static {p1}, Lee/cyber/smartid/manager/impl/TSEListenerToServiceListenerMapperImpl;->d(Lee/cyber/smartid/manager/impl/TSEListenerToServiceListenerMapperImpl;)Lee/cyber/smartid/util/Log;

    move-result-object p1

    const-string p2, "generateKeys - calling updateDevice .."

    invoke-virtual {p1, p2}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    .line 3
    iget-object p1, p0, Lee/cyber/smartid/manager/impl/TSEListenerToServiceListenerMapperImpl$1;->a:Lee/cyber/smartid/manager/impl/TSEListenerToServiceListenerMapperImpl;

    invoke-static {p1}, Lee/cyber/smartid/manager/impl/TSEListenerToServiceListenerMapperImpl;->b(Lee/cyber/smartid/manager/impl/TSEListenerToServiceListenerMapperImpl;)Lee/cyber/smartid/inter/ServiceAccess;

    move-result-object p1

    invoke-interface {p1}, Lee/cyber/smartid/inter/ServiceAccess;->updateDeviceForPostKeyGenerationIfNeeded()V

    goto :goto_0

    .line 4
    :cond_0
    iget-object p1, p0, Lee/cyber/smartid/manager/impl/TSEListenerToServiceListenerMapperImpl$1;->a:Lee/cyber/smartid/manager/impl/TSEListenerToServiceListenerMapperImpl;

    invoke-static {p1}, Lee/cyber/smartid/manager/impl/TSEListenerToServiceListenerMapperImpl;->d(Lee/cyber/smartid/manager/impl/TSEListenerToServiceListenerMapperImpl;)Lee/cyber/smartid/util/Log;

    move-result-object p1

    const-string p2, "generateKeys - skipping updateDevice as no registration done yet .."

    invoke-virtual {p1, p2}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    :goto_0
    return-void
.end method

.method public onGenerateKeysProgress(Ljava/lang/String;II)V
    .locals 3

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/manager/impl/TSEListenerToServiceListenerMapperImpl$1;->a:Lee/cyber/smartid/manager/impl/TSEListenerToServiceListenerMapperImpl;

    invoke-static {v0}, Lee/cyber/smartid/manager/impl/TSEListenerToServiceListenerMapperImpl;->a(Lee/cyber/smartid/manager/impl/TSEListenerToServiceListenerMapperImpl;)Lee/cyber/smartid/inter/ListenerAccess;

    move-result-object v0

    const-class v1, Lee/cyber/smartid/inter/GenerateKeysListener;

    const/4 v2, 0x0

    invoke-interface {v0, p1, v2, v1}, Lee/cyber/smartid/inter/ListenerAccess;->getListener(Ljava/lang/String;ZLjava/lang/Class;)Lee/cyber/smartid/inter/ServiceListener;

    move-result-object v0

    check-cast v0, Lee/cyber/smartid/inter/GenerateKeysListener;

    if-eqz v0, :cond_0

    .line 2
    invoke-interface {v0, p1, p2, p3}, Lee/cyber/smartid/inter/GenerateKeysListener;->onGenerateKeysProgress(Ljava/lang/String;II)V

    :cond_0
    return-void
.end method

.method public onGenerateKeysSuccess(Ljava/lang/String;Lee/cyber/smartid/tse/dto/jsonrpc/resp/GenerateKeysResp;)V
    .locals 3

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/manager/impl/TSEListenerToServiceListenerMapperImpl$1;->a:Lee/cyber/smartid/manager/impl/TSEListenerToServiceListenerMapperImpl;

    invoke-static {v0}, Lee/cyber/smartid/manager/impl/TSEListenerToServiceListenerMapperImpl;->a(Lee/cyber/smartid/manager/impl/TSEListenerToServiceListenerMapperImpl;)Lee/cyber/smartid/inter/ListenerAccess;

    move-result-object v0

    const-class v1, Lee/cyber/smartid/inter/GenerateKeysListener;

    const/4 v2, 0x1

    invoke-interface {v0, p1, v2, v1}, Lee/cyber/smartid/inter/ListenerAccess;->getListener(Ljava/lang/String;ZLjava/lang/Class;)Lee/cyber/smartid/inter/ServiceListener;

    move-result-object v0

    check-cast v0, Lee/cyber/smartid/inter/GenerateKeysListener;

    if-eqz v0, :cond_0

    .line 2
    invoke-interface {v0, p1, p2}, Lee/cyber/smartid/inter/GenerateKeysListener;->onGenerateKeysSuccess(Ljava/lang/String;Lee/cyber/smartid/tse/dto/jsonrpc/resp/GenerateKeysResp;)V

    :cond_0
    return-void
.end method
