.class Lee/cyber/smartid/manager/impl/DeleteAccountManagerImpl$1;
.super Lee/cyber/smartid/tse/network/RPCCallbackNonUIThread;
.source "DeleteAccountManagerImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lee/cyber/smartid/manager/impl/DeleteAccountManagerImpl;->deleteAccount(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLee/cyber/smartid/inter/DeleteAccountListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lee/cyber/smartid/tse/network/RPCCallbackNonUIThread<",
        "Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCResponse<",
        "Lee/cyber/smartid/dto/jsonrpc/result/DeleteAccountResult;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:Ljava/lang/String;

.field final synthetic c:Z

.field final synthetic d:Lee/cyber/smartid/manager/impl/DeleteAccountManagerImpl;


# direct methods
.method constructor <init>(Lee/cyber/smartid/manager/impl/DeleteAccountManagerImpl;Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCRequest;Lee/cyber/smartid/tse/SmartIdTSE;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 0

    .line 1
    iput-object p1, p0, Lee/cyber/smartid/manager/impl/DeleteAccountManagerImpl$1;->d:Lee/cyber/smartid/manager/impl/DeleteAccountManagerImpl;

    iput-object p4, p0, Lee/cyber/smartid/manager/impl/DeleteAccountManagerImpl$1;->a:Ljava/lang/String;

    iput-object p5, p0, Lee/cyber/smartid/manager/impl/DeleteAccountManagerImpl$1;->b:Ljava/lang/String;

    iput-boolean p6, p0, Lee/cyber/smartid/manager/impl/DeleteAccountManagerImpl$1;->c:Z

    invoke-direct {p0, p2, p3}, Lee/cyber/smartid/tse/network/RPCCallbackNonUIThread;-><init>(Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCRequest;Lee/cyber/smartid/tse/SmartIdTSE;)V

    return-void
.end method


# virtual methods
.method public onFailure(Lc/b;Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCError;Ljava/lang/Throwable;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lc/b<",
            "Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCResponse<",
            "Lee/cyber/smartid/dto/jsonrpc/result/DeleteAccountResult;",
            ">;>;",
            "Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCError;",
            "Ljava/lang/Throwable;",
            ")V"
        }
    .end annotation

    if-eqz p2, :cond_1

    .line 1
    invoke-virtual {p2}, Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCError;->getCode()J

    move-result-wide v0

    const-wide/16 v2, -0x791a

    cmp-long p1, v0, v2

    if-eqz p1, :cond_0

    invoke-virtual {p2}, Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCError;->getCode()J

    move-result-wide v0

    const-wide/16 v2, -0x7921

    cmp-long p1, v0, v2

    if-nez p1, :cond_1

    .line 2
    :cond_0
    iget-object p1, p0, Lee/cyber/smartid/manager/impl/DeleteAccountManagerImpl$1;->d:Lee/cyber/smartid/manager/impl/DeleteAccountManagerImpl;

    invoke-static {p1}, Lee/cyber/smartid/manager/impl/DeleteAccountManagerImpl;->a(Lee/cyber/smartid/manager/impl/DeleteAccountManagerImpl;)Lee/cyber/smartid/util/Log;

    move-result-object p1

    const-string p2, "deleteAccount - account is inactive on the server side, removing it from local storage also .."

    invoke-virtual {p1, p2}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    .line 3
    iget-object p1, p0, Lee/cyber/smartid/manager/impl/DeleteAccountManagerImpl$1;->d:Lee/cyber/smartid/manager/impl/DeleteAccountManagerImpl;

    iget-object p2, p0, Lee/cyber/smartid/manager/impl/DeleteAccountManagerImpl$1;->a:Ljava/lang/String;

    iget-object p3, p0, Lee/cyber/smartid/manager/impl/DeleteAccountManagerImpl$1;->b:Ljava/lang/String;

    invoke-static {p1, p2, p3}, Lee/cyber/smartid/manager/impl/DeleteAccountManagerImpl;->a(Lee/cyber/smartid/manager/impl/DeleteAccountManagerImpl;Ljava/lang/String;Ljava/lang/String;)V

    return-void

    .line 4
    :cond_1
    iget-boolean p1, p0, Lee/cyber/smartid/manager/impl/DeleteAccountManagerImpl$1;->c:Z

    if-eqz p1, :cond_2

    .line 5
    iget-object p1, p0, Lee/cyber/smartid/manager/impl/DeleteAccountManagerImpl$1;->d:Lee/cyber/smartid/manager/impl/DeleteAccountManagerImpl;

    invoke-static {p1}, Lee/cyber/smartid/manager/impl/DeleteAccountManagerImpl;->a(Lee/cyber/smartid/manager/impl/DeleteAccountManagerImpl;)Lee/cyber/smartid/util/Log;

    move-result-object p1

    const-string p2, "deleteAccount - Delete failed on the server side, but forceLocalDelete = true, removing the account locally .."

    invoke-virtual {p1, p2}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    .line 6
    iget-object p1, p0, Lee/cyber/smartid/manager/impl/DeleteAccountManagerImpl$1;->d:Lee/cyber/smartid/manager/impl/DeleteAccountManagerImpl;

    iget-object p2, p0, Lee/cyber/smartid/manager/impl/DeleteAccountManagerImpl$1;->a:Ljava/lang/String;

    iget-object p3, p0, Lee/cyber/smartid/manager/impl/DeleteAccountManagerImpl$1;->b:Ljava/lang/String;

    invoke-static {p1, p2, p3}, Lee/cyber/smartid/manager/impl/DeleteAccountManagerImpl;->a(Lee/cyber/smartid/manager/impl/DeleteAccountManagerImpl;Ljava/lang/String;Ljava/lang/String;)V

    return-void

    .line 7
    :cond_2
    iget-object p1, p0, Lee/cyber/smartid/manager/impl/DeleteAccountManagerImpl$1;->d:Lee/cyber/smartid/manager/impl/DeleteAccountManagerImpl;

    invoke-static {p1}, Lee/cyber/smartid/manager/impl/DeleteAccountManagerImpl;->c(Lee/cyber/smartid/manager/impl/DeleteAccountManagerImpl;)Lee/cyber/smartid/inter/ServiceAccess;

    move-result-object p1

    new-instance v0, Lee/cyber/smartid/manager/impl/DeleteAccountManagerImpl$1$1;

    invoke-direct {v0, p0, p2, p3}, Lee/cyber/smartid/manager/impl/DeleteAccountManagerImpl$1$1;-><init>(Lee/cyber/smartid/manager/impl/DeleteAccountManagerImpl$1;Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCError;Ljava/lang/Throwable;)V

    invoke-interface {p1, v0}, Lee/cyber/smartid/inter/ServiceAccess;->notifyUI(Ljava/lang/Runnable;)V

    return-void
.end method

.method public onSuccess(Lc/b;Lc/r;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lc/b<",
            "Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCResponse<",
            "Lee/cyber/smartid/dto/jsonrpc/result/DeleteAccountResult;",
            ">;>;",
            "Lc/r<",
            "Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCResponse<",
            "Lee/cyber/smartid/dto/jsonrpc/result/DeleteAccountResult;",
            ">;>;)V"
        }
    .end annotation

    .line 1
    iget-object p1, p0, Lee/cyber/smartid/manager/impl/DeleteAccountManagerImpl$1;->d:Lee/cyber/smartid/manager/impl/DeleteAccountManagerImpl;

    iget-object p2, p0, Lee/cyber/smartid/manager/impl/DeleteAccountManagerImpl$1;->a:Ljava/lang/String;

    iget-object v0, p0, Lee/cyber/smartid/manager/impl/DeleteAccountManagerImpl$1;->b:Ljava/lang/String;

    invoke-static {p1, p2, v0}, Lee/cyber/smartid/manager/impl/DeleteAccountManagerImpl;->a(Lee/cyber/smartid/manager/impl/DeleteAccountManagerImpl;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public onValidate(Lc/b;Lc/r;)Z
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lc/b<",
            "Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCResponse<",
            "Lee/cyber/smartid/dto/jsonrpc/result/DeleteAccountResult;",
            ">;>;",
            "Lc/r<",
            "Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCResponse<",
            "Lee/cyber/smartid/dto/jsonrpc/result/DeleteAccountResult;",
            ">;>;)Z"
        }
    .end annotation

    .line 1
    invoke-static {p2}, Lee/cyber/smartid/util/Util;->validateResponse(Lc/r;)Z

    move-result p1

    return p1
.end method
