.class public Lee/cyber/smartid/manager/impl/PropertiesSourceImpl;
.super Ljava/lang/Object;
.source "PropertiesSourceImpl.java"

# interfaces
.implements Lee/cyber/smartid/manager/inter/PropertiesSource;


# instance fields
.field private a:Ljava/lang/String;

.field private final b:Lee/cyber/smartid/inter/ServiceAccess;


# direct methods
.method public constructor <init>(Lee/cyber/smartid/inter/ServiceAccess;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "service.properties"

    .line 2
    iput-object v0, p0, Lee/cyber/smartid/manager/impl/PropertiesSourceImpl;->a:Ljava/lang/String;

    .line 3
    iput-object p1, p0, Lee/cyber/smartid/manager/impl/PropertiesSourceImpl;->b:Lee/cyber/smartid/inter/ServiceAccess;

    return-void
.end method


# virtual methods
.method public getPropertiesSourceName()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/manager/impl/PropertiesSourceImpl;->a:Ljava/lang/String;

    return-object v0
.end method

.method public loadProperties()Ljava/util/Properties;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1
    new-instance v0, Ljava/util/Properties;

    invoke-direct {v0}, Ljava/util/Properties;-><init>()V

    .line 2
    :try_start_0
    iget-object v1, p0, Lee/cyber/smartid/manager/impl/PropertiesSourceImpl;->b:Lee/cyber/smartid/inter/ServiceAccess;

    invoke-interface {v1}, Lee/cyber/smartid/inter/ServiceAccess;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v1

    iget-object v2, p0, Lee/cyber/smartid/manager/impl/PropertiesSourceImpl;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v1

    .line 3
    invoke-virtual {v0, v1}, Ljava/util/Properties;->load(Ljava/io/InputStream;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 4
    invoke-static {v1}, Lee/cyber/smartid/tse/util/Util;->closeClosable(Ljava/io/Closeable;)V

    return-object v0

    .line 5
    :catch_0
    new-instance v0, Ljava/io/IOException;

    iget-object v1, p0, Lee/cyber/smartid/manager/impl/PropertiesSourceImpl;->b:Lee/cyber/smartid/inter/ServiceAccess;

    invoke-interface {v1}, Lee/cyber/smartid/inter/ServiceAccess;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    sget v2, Lee/cyber/smartid/R$string;->err_failed_to_load_properties_from_x:I

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, p0, Lee/cyber/smartid/manager/impl/PropertiesSourceImpl;->a:Ljava/lang/String;

    const/4 v5, 0x0

    aput-object v4, v3, v5

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
