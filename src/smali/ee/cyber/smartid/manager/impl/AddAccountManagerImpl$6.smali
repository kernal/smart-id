.class Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$6;
.super Ljava/lang/Object;
.source "AddAccountManagerImpl.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/dto/jsonrpc/result/RegisterAccountResult;Lee/cyber/smartid/dto/jsonrpc/param/RegisterDeviceData;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lee/cyber/smartid/dto/jsonrpc/result/RegisterAccountResult;

.field final synthetic b:Ljava/lang/String;

.field final synthetic c:Lee/cyber/smartid/dto/jsonrpc/param/RegisterDeviceData;

.field final synthetic d:Ljava/lang/String;

.field final synthetic e:Ljava/lang/String;

.field final synthetic f:Ljava/lang/String;

.field final synthetic g:Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;


# direct methods
.method constructor <init>(Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;Lee/cyber/smartid/dto/jsonrpc/result/RegisterAccountResult;Ljava/lang/String;Lee/cyber/smartid/dto/jsonrpc/param/RegisterDeviceData;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$6;->g:Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;

    iput-object p2, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$6;->a:Lee/cyber/smartid/dto/jsonrpc/result/RegisterAccountResult;

    iput-object p3, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$6;->b:Ljava/lang/String;

    iput-object p4, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$6;->c:Lee/cyber/smartid/dto/jsonrpc/param/RegisterDeviceData;

    iput-object p5, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$6;->d:Ljava/lang/String;

    iput-object p6, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$6;->e:Ljava/lang/String;

    iput-object p7, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$6;->f:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 9

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$6;->a:Lee/cyber/smartid/dto/jsonrpc/result/RegisterAccountResult;

    instance-of v0, v0, Lee/cyber/smartid/dto/jsonrpc/result/RegisterDeviceResult;

    if-eqz v0, :cond_0

    .line 2
    :try_start_0
    iget-object v0, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$6;->g:Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;

    invoke-static {v0}, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;->e(Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;)Lee/cyber/smartid/tse/SmartIdTSE;

    move-result-object v0

    iget-object v1, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$6;->a:Lee/cyber/smartid/dto/jsonrpc/result/RegisterAccountResult;

    check-cast v1, Lee/cyber/smartid/dto/jsonrpc/result/RegisterDeviceResult;

    invoke-virtual {v1}, Lee/cyber/smartid/dto/jsonrpc/result/RegisterDeviceResult;->getAppInstanceUUID()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$6;->a:Lee/cyber/smartid/dto/jsonrpc/result/RegisterAccountResult;

    check-cast v2, Lee/cyber/smartid/dto/jsonrpc/result/RegisterDeviceResult;

    invoke-virtual {v2}, Lee/cyber/smartid/dto/jsonrpc/result/RegisterDeviceResult;->getPassword()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lee/cyber/smartid/tse/SmartIdTSE;->setDeviceCredentials(Ljava/lang/String;Ljava/lang/String;)V

    .line 3
    iget-object v0, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$6;->g:Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;

    invoke-static {v0}, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;->d(Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;)Lee/cyber/smartid/util/Log;

    move-result-object v0
    :try_end_0
    .catch Lee/cyber/smartid/cryptolib/dto/StorageException; {:try_start_0 .. :try_end_0} :catch_0

    const-string v1, "handleStepOneSuccessForAddAccount - stored device Basic Auth"

    :try_start_1
    invoke-virtual {v0, v1}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V
    :try_end_1
    .catch Lee/cyber/smartid/cryptolib/dto/StorageException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 4
    iget-object v1, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$6;->g:Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;

    iget-object v2, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$6;->b:Ljava/lang/String;

    invoke-static {v1}, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;->c(Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;)Lee/cyber/smartid/inter/ServiceAccess;

    move-result-object v3

    invoke-interface {v3}, Lee/cyber/smartid/inter/ServiceAccess;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    iget-object v4, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$6;->g:Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;

    invoke-static {v4}, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;->b(Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;)Lee/cyber/smartid/tse/inter/WallClock;

    move-result-object v4

    invoke-static {v3, v4, v0}, Lee/cyber/smartid/dto/SmartIdError;->from(Landroid/content/Context;Lee/cyber/smartid/tse/inter/WallClock;Ljava/lang/Throwable;)Lee/cyber/smartid/dto/SmartIdError;

    move-result-object v0

    iget-object v3, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$6;->a:Lee/cyber/smartid/dto/jsonrpc/result/RegisterAccountResult;

    invoke-virtual {v3}, Lee/cyber/smartid/dto/jsonrpc/result/RegisterAccountResult;->getAccountUUID()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    invoke-static {v1, v2, v0, v3, v4}, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;->a(Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;Ljava/lang/String;Lee/cyber/smartid/dto/SmartIdError;Ljava/lang/String;Z)V

    return-void

    .line 5
    :cond_0
    :goto_0
    iget-object v0, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$6;->c:Lee/cyber/smartid/dto/jsonrpc/param/RegisterDeviceData;

    if-eqz v0, :cond_2

    .line 6
    iget-object v0, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$6;->g:Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;

    invoke-static {v0}, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;->c(Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;)Lee/cyber/smartid/inter/ServiceAccess;

    move-result-object v0

    invoke-interface {v0}, Lee/cyber/smartid/inter/ServiceAccess;->getLastSafetyNetAttestation()Lee/cyber/smartid/dto/SafetyNetAttestation;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$6;->g:Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;

    invoke-static {v0}, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;->c(Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;)Lee/cyber/smartid/inter/ServiceAccess;

    move-result-object v0

    invoke-interface {v0}, Lee/cyber/smartid/inter/ServiceAccess;->getLastSafetyNetAttestation()Lee/cyber/smartid/dto/SafetyNetAttestation;

    move-result-object v0

    iget-object v1, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$6;->g:Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;

    invoke-static {v1}, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;->c(Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;)Lee/cyber/smartid/inter/ServiceAccess;

    move-result-object v1

    invoke-interface {v1}, Lee/cyber/smartid/inter/ServiceAccess;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lee/cyber/smartid/dto/SafetyNetAttestation;->getResultTypeString(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_1
    const/4 v0, 0x0

    .line 7
    :goto_1
    :try_start_2
    iget-object v1, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$6;->g:Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;

    invoke-static {v1}, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;->c(Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;)Lee/cyber/smartid/inter/ServiceAccess;

    move-result-object v1

    iget-object v2, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$6;->c:Lee/cyber/smartid/dto/jsonrpc/param/RegisterDeviceData;

    invoke-interface {v1, v2, v0}, Lee/cyber/smartid/inter/ServiceAccess;->onPostUpdateDevice(Lee/cyber/smartid/dto/jsonrpc/param/RegisterDeviceData;Ljava/lang/String;)V
    :try_end_2
    .catch Lee/cyber/smartid/cryptolib/dto/StorageException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_2

    :catch_1
    move-exception v0

    .line 8
    iget-object v1, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$6;->g:Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;

    invoke-static {v1}, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;->d(Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;)Lee/cyber/smartid/util/Log;

    move-result-object v1

    const-string v2, "handleStepOneSuccessForAddAccount"

    invoke-virtual {v1, v2, v0}, Lee/cyber/smartid/util/Log;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 9
    :cond_2
    :goto_2
    iget-object v3, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$6;->g:Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;

    iget-object v4, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$6;->b:Ljava/lang/String;

    iget-object v5, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$6;->d:Ljava/lang/String;

    iget-object v6, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$6;->e:Ljava/lang/String;

    iget-object v7, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$6;->a:Lee/cyber/smartid/dto/jsonrpc/result/RegisterAccountResult;

    iget-object v8, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$6;->f:Ljava/lang/String;

    invoke-static/range {v3 .. v8}, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;->a(Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/dto/jsonrpc/result/RegisterAccountResult;Ljava/lang/String;)V

    return-void
.end method
