.class public Lee/cyber/smartid/manager/impl/TransactionCacheManagerImpl;
.super Ljava/lang/Object;
.source "TransactionCacheManagerImpl.java"

# interfaces
.implements Lee/cyber/smartid/manager/inter/TransactionCacheManager;


# instance fields
.field private final a:Ljava/lang/Object;

.field private final b:Lee/cyber/smartid/cryptolib/inter/StorageOp;

.field private final c:Lee/cyber/smartid/inter/ServiceAccess;

.field private final d:Lee/cyber/smartid/tse/inter/AlarmAccess;

.field private final e:Lee/cyber/smartid/tse/inter/WallClock;

.field private f:Lee/cyber/smartid/util/Log;


# direct methods
.method public constructor <init>(Lee/cyber/smartid/cryptolib/inter/StorageOp;Lee/cyber/smartid/inter/ServiceAccess;Lee/cyber/smartid/tse/inter/WallClock;Lee/cyber/smartid/tse/inter/AlarmAccess;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lee/cyber/smartid/manager/impl/TransactionCacheManagerImpl;->a:Ljava/lang/Object;

    .line 3
    const-class v0, Lee/cyber/smartid/manager/impl/TransactionCacheManagerImpl;

    invoke-static {v0}, Lee/cyber/smartid/util/Log;->getInstance(Ljava/lang/Class;)Lee/cyber/smartid/util/Log;

    move-result-object v0

    iput-object v0, p0, Lee/cyber/smartid/manager/impl/TransactionCacheManagerImpl;->f:Lee/cyber/smartid/util/Log;

    .line 4
    iput-object p1, p0, Lee/cyber/smartid/manager/impl/TransactionCacheManagerImpl;->b:Lee/cyber/smartid/cryptolib/inter/StorageOp;

    .line 5
    iput-object p2, p0, Lee/cyber/smartid/manager/impl/TransactionCacheManagerImpl;->c:Lee/cyber/smartid/inter/ServiceAccess;

    .line 6
    iput-object p3, p0, Lee/cyber/smartid/manager/impl/TransactionCacheManagerImpl;->e:Lee/cyber/smartid/tse/inter/WallClock;

    .line 7
    iput-object p4, p0, Lee/cyber/smartid/manager/impl/TransactionCacheManagerImpl;->d:Lee/cyber/smartid/tse/inter/AlarmAccess;

    return-void
.end method

.method private a()Ljava/util/HashMap;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Lee/cyber/smartid/dto/TransactionCachedHolder;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/manager/impl/TransactionCacheManagerImpl;->a:Ljava/lang/Object;

    monitor-enter v0

    .line 2
    :try_start_0
    iget-object v1, p0, Lee/cyber/smartid/manager/impl/TransactionCacheManagerImpl;->b:Lee/cyber/smartid/cryptolib/inter/StorageOp;

    iget-object v2, p0, Lee/cyber/smartid/manager/impl/TransactionCacheManagerImpl;->c:Lee/cyber/smartid/inter/ServiceAccess;

    invoke-interface {v2}, Lee/cyber/smartid/inter/ServiceAccess;->getTransactionCacheStorageId()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lee/cyber/smartid/manager/impl/TransactionCacheManagerImpl$1;

    invoke-direct {v3, p0}, Lee/cyber/smartid/manager/impl/TransactionCacheManagerImpl$1;-><init>(Lee/cyber/smartid/manager/impl/TransactionCacheManagerImpl;)V

    .line 3
    invoke-virtual {v3}, Lcom/google/gson/b/a;->getType()Ljava/lang/reflect/Type;

    move-result-object v3

    .line 4
    invoke-interface {v1, v2, v3}, Lee/cyber/smartid/cryptolib/inter/StorageOp;->retrieveData(Ljava/lang/String;Ljava/lang/reflect/Type;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/HashMap;

    if-nez v1, :cond_0

    .line 5
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 6
    :cond_0
    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception v1

    .line 7
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private a(Ljava/util/HashMap;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Lee/cyber/smartid/dto/TransactionCachedHolder;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lee/cyber/smartid/cryptolib/dto/StorageException;
        }
    .end annotation

    .line 8
    iget-object v0, p0, Lee/cyber/smartid/manager/impl/TransactionCacheManagerImpl;->a:Ljava/lang/Object;

    monitor-enter v0

    if-nez p1, :cond_0

    .line 9
    :try_start_0
    iget-object p1, p0, Lee/cyber/smartid/manager/impl/TransactionCacheManagerImpl;->b:Lee/cyber/smartid/cryptolib/inter/StorageOp;

    iget-object v1, p0, Lee/cyber/smartid/manager/impl/TransactionCacheManagerImpl;->c:Lee/cyber/smartid/inter/ServiceAccess;

    invoke-interface {v1}, Lee/cyber/smartid/inter/ServiceAccess;->getTransactionCacheStorageId()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v1}, Lee/cyber/smartid/cryptolib/inter/StorageOp;->removeData(Ljava/lang/String;)V

    goto :goto_0

    .line 10
    :cond_0
    iget-object v1, p0, Lee/cyber/smartid/manager/impl/TransactionCacheManagerImpl;->b:Lee/cyber/smartid/cryptolib/inter/StorageOp;

    iget-object v2, p0, Lee/cyber/smartid/manager/impl/TransactionCacheManagerImpl;->c:Lee/cyber/smartid/inter/ServiceAccess;

    invoke-interface {v2}, Lee/cyber/smartid/inter/ServiceAccess;->getTransactionCacheStorageId()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2, p1}, Lee/cyber/smartid/cryptolib/inter/StorageOp;->storeData(Ljava/lang/String;Ljava/lang/Object;)V

    .line 11
    :goto_0
    monitor-exit v0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method


# virtual methods
.method public cacheTransaction(Lee/cyber/smartid/dto/jsonrpc/Transaction;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lee/cyber/smartid/cryptolib/dto/StorageException;
        }
    .end annotation

    if-nez p1, :cond_0

    return-void

    .line 1
    :cond_0
    iget-object v0, p0, Lee/cyber/smartid/manager/impl/TransactionCacheManagerImpl;->a:Ljava/lang/Object;

    monitor-enter v0

    .line 2
    :try_start_0
    iget-object v1, p0, Lee/cyber/smartid/manager/impl/TransactionCacheManagerImpl;->f:Lee/cyber/smartid/util/Log;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const-string v3, "cacheTransaction - transactionUUID: "

    :try_start_1
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lee/cyber/smartid/dto/jsonrpc/Transaction;->getTransactionUUID()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    const-string v3, ", TTL: "

    :try_start_2
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lee/cyber/smartid/dto/jsonrpc/Transaction;->getTtlSec()J

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    const-string v3, " sec"

    :try_start_3
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    .line 3
    invoke-direct {p0}, Lee/cyber/smartid/manager/impl/TransactionCacheManagerImpl;->a()Ljava/util/HashMap;

    move-result-object v1

    .line 4
    invoke-virtual {p1}, Lee/cyber/smartid/dto/jsonrpc/Transaction;->getTransactionUUID()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lee/cyber/smartid/dto/TransactionCachedHolder;

    iget-object v4, p0, Lee/cyber/smartid/manager/impl/TransactionCacheManagerImpl;->e:Lee/cyber/smartid/tse/inter/WallClock;

    invoke-direct {v3, v4, p1}, Lee/cyber/smartid/dto/TransactionCachedHolder;-><init>(Lee/cyber/smartid/tse/inter/WallClock;Lee/cyber/smartid/dto/jsonrpc/Transaction;)V

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 5
    invoke-direct {p0, v1}, Lee/cyber/smartid/manager/impl/TransactionCacheManagerImpl;->a(Ljava/util/HashMap;)V

    .line 6
    monitor-exit v0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw p1
.end method

.method public getCachedTransaction(Ljava/lang/String;)Lee/cyber/smartid/dto/jsonrpc/Transaction;
    .locals 4

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/manager/impl/TransactionCacheManagerImpl;->a:Ljava/lang/Object;

    monitor-enter v0

    .line 2
    :try_start_0
    iget-object v1, p0, Lee/cyber/smartid/manager/impl/TransactionCacheManagerImpl;->f:Lee/cyber/smartid/util/Log;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const-string v3, "getCachedTransaction - transactionUUID: "

    :try_start_1
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    .line 3
    invoke-direct {p0}, Lee/cyber/smartid/manager/impl/TransactionCacheManagerImpl;->a()Ljava/util/HashMap;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lee/cyber/smartid/dto/TransactionCachedHolder;

    if-eqz p1, :cond_0

    .line 4
    invoke-virtual {p1}, Lee/cyber/smartid/dto/TransactionCachedHolder;->getTransaction()Lee/cyber/smartid/dto/jsonrpc/Transaction;

    move-result-object p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    monitor-exit v0

    return-object p1

    :catchall_0
    move-exception p1

    .line 5
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw p1
.end method

.method public removeCachedTransactionByAccountUUID(Ljava/lang/String;)V
    .locals 5

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/manager/impl/TransactionCacheManagerImpl;->a:Ljava/lang/Object;

    monitor-enter v0

    .line 2
    :try_start_0
    invoke-direct {p0}, Lee/cyber/smartid/manager/impl/TransactionCacheManagerImpl;->a()Ljava/util/HashMap;

    move-result-object v1

    .line 3
    invoke-virtual {v1}, Ljava/util/HashMap;->size()I

    move-result v2

    const/4 v3, 0x1

    if-ge v2, v3, :cond_0

    .line 4
    iget-object v1, p0, Lee/cyber/smartid/manager/impl/TransactionCacheManagerImpl;->f:Lee/cyber/smartid/util/Log;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const-string v3, "removeCachedTransactionByAccountUUID - accountUUID: "

    :try_start_1
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    const-string p1, " - no need, none found"

    :try_start_2
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    .line 5
    monitor-exit v0

    return-void

    .line 6
    :cond_0
    invoke-virtual {v1}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    const/4 v2, 0x0

    .line 7
    :cond_1
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 8
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Map$Entry;

    .line 9
    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lee/cyber/smartid/dto/TransactionCachedHolder;

    invoke-virtual {v3}, Lee/cyber/smartid/dto/TransactionCachedHolder;->getAccountUUID()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 10
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 11
    :cond_2
    iget-object v1, p0, Lee/cyber/smartid/manager/impl/TransactionCacheManagerImpl;->f:Lee/cyber/smartid/util/Log;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    const-string v4, "removeCachedTransactionByAccountUUID - accountUUID: "

    :try_start_3
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    const-string p1, " "

    :try_start_4
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    const-string p1, " removed"

    :try_start_5
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    .line 12
    monitor-exit v0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    throw p1
.end method

.method public removeCachedTransactionByTransactionUUID(Ljava/lang/String;)Lee/cyber/smartid/dto/jsonrpc/Transaction;
    .locals 4

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/manager/impl/TransactionCacheManagerImpl;->a:Ljava/lang/Object;

    monitor-enter v0

    .line 2
    :try_start_0
    iget-object v1, p0, Lee/cyber/smartid/manager/impl/TransactionCacheManagerImpl;->f:Lee/cyber/smartid/util/Log;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const-string v3, "removeCachedTransactionByTransactionUUID - transactionUUID: "

    :try_start_1
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    .line 3
    invoke-direct {p0}, Lee/cyber/smartid/manager/impl/TransactionCacheManagerImpl;->a()Ljava/util/HashMap;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lee/cyber/smartid/dto/TransactionCachedHolder;

    if-eqz p1, :cond_0

    .line 4
    invoke-virtual {p1}, Lee/cyber/smartid/dto/TransactionCachedHolder;->getTransaction()Lee/cyber/smartid/dto/jsonrpc/Transaction;

    move-result-object p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    monitor-exit v0

    return-object p1

    :catchall_0
    move-exception p1

    .line 5
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw p1
.end method

.method public removeExpiredTransactionsFromCache()V
    .locals 7

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/manager/impl/TransactionCacheManagerImpl;->a:Ljava/lang/Object;

    monitor-enter v0

    .line 2
    :try_start_0
    invoke-direct {p0}, Lee/cyber/smartid/manager/impl/TransactionCacheManagerImpl;->a()Ljava/util/HashMap;

    move-result-object v1

    .line 3
    invoke-virtual {v1}, Ljava/util/HashMap;->size()I

    move-result v2

    const/4 v3, 0x1

    if-ge v2, v3, :cond_0

    .line 4
    monitor-exit v0

    return-void

    .line 5
    :cond_0
    invoke-virtual {v1}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 6
    :cond_1
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 7
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Map$Entry;

    .line 8
    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lee/cyber/smartid/dto/TransactionCachedHolder;

    iget-object v5, p0, Lee/cyber/smartid/manager/impl/TransactionCacheManagerImpl;->e:Lee/cyber/smartid/tse/inter/WallClock;

    invoke-virtual {v4, v5}, Lee/cyber/smartid/dto/TransactionCachedHolder;->isExpired(Lee/cyber/smartid/tse/inter/WallClock;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 9
    iget-object v4, p0, Lee/cyber/smartid/manager/impl/TransactionCacheManagerImpl;->f:Lee/cyber/smartid/util/Log;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const-string v6, "removeExpiredTransactionsFromCache - removing "

    :try_start_1
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lee/cyber/smartid/dto/TransactionCachedHolder;

    invoke-virtual {v3}, Lee/cyber/smartid/dto/TransactionCachedHolder;->getTransactionUUID()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    .line 10
    invoke-interface {v2}, Ljava/util/Iterator;->remove()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 11
    :cond_2
    :try_start_2
    invoke-direct {p0, v1}, Lee/cyber/smartid/manager/impl/TransactionCacheManagerImpl;->a(Ljava/util/HashMap;)V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    :catch_0
    move-exception v1

    .line 12
    :try_start_3
    iget-object v2, p0, Lee/cyber/smartid/manager/impl/TransactionCacheManagerImpl;->f:Lee/cyber/smartid/util/Log;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    const-string v3, "removeExpiredTransactionsFromCache"

    :try_start_4
    invoke-virtual {v2, v3, v1}, Lee/cyber/smartid/util/Log;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 13
    :goto_1
    monitor-exit v0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    throw v1
.end method

.method public setNextRemoveExpiredTransactionsAlarm()V
    .locals 11

    .line 1
    invoke-direct {p0}, Lee/cyber/smartid/manager/impl/TransactionCacheManagerImpl;->a()Ljava/util/HashMap;

    move-result-object v0

    .line 2
    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    move-result v1

    const/4 v2, 0x1

    if-ge v1, v2, :cond_0

    .line 3
    iget-object v0, p0, Lee/cyber/smartid/manager/impl/TransactionCacheManagerImpl;->f:Lee/cyber/smartid/util/Log;

    const-string v1, "setNextRemoveExpiredTransactionsAlarm - no alarm needed, no transactions!"

    invoke-virtual {v0, v1}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    return-void

    .line 4
    :cond_0
    invoke-virtual {v0}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    const/4 v1, 0x0

    const-wide v2, 0x7fffffffffffffffL

    move-object v6, v1

    move-wide v4, v2

    .line 5
    :cond_1
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_2

    .line 6
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/util/Map$Entry;

    .line 7
    invoke-interface {v7}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lee/cyber/smartid/dto/TransactionCachedHolder;

    iget-object v9, p0, Lee/cyber/smartid/manager/impl/TransactionCacheManagerImpl;->e:Lee/cyber/smartid/tse/inter/WallClock;

    invoke-virtual {v8, v9}, Lee/cyber/smartid/dto/TransactionCachedHolder;->getExpirationDelay(Lee/cyber/smartid/tse/inter/WallClock;)J

    move-result-wide v8

    cmp-long v10, v8, v4

    if-gez v10, :cond_1

    .line 8
    invoke-interface {v7}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lee/cyber/smartid/dto/TransactionCachedHolder;

    iget-object v5, p0, Lee/cyber/smartid/manager/impl/TransactionCacheManagerImpl;->e:Lee/cyber/smartid/tse/inter/WallClock;

    invoke-virtual {v4, v5}, Lee/cyber/smartid/dto/TransactionCachedHolder;->getExpirationDelay(Lee/cyber/smartid/tse/inter/WallClock;)J

    move-result-wide v4

    .line 9
    invoke-interface {v7}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lee/cyber/smartid/dto/TransactionCachedHolder;

    goto :goto_0

    :cond_2
    cmp-long v0, v4, v2

    if-eqz v0, :cond_4

    if-nez v6, :cond_3

    goto :goto_1

    .line 10
    :cond_3
    iget-object v0, p0, Lee/cyber/smartid/manager/impl/TransactionCacheManagerImpl;->d:Lee/cyber/smartid/tse/inter/AlarmAccess;

    iget-object v2, p0, Lee/cyber/smartid/manager/impl/TransactionCacheManagerImpl;->c:Lee/cyber/smartid/inter/ServiceAccess;

    invoke-interface {v2}, Lee/cyber/smartid/inter/ServiceAccess;->getRemoveExpiredTransactionActionId()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lee/cyber/smartid/manager/impl/TransactionCacheManagerImpl;->e:Lee/cyber/smartid/tse/inter/WallClock;

    invoke-virtual {v6, v3}, Lee/cyber/smartid/dto/TransactionCachedHolder;->getExpirationDelay(Lee/cyber/smartid/tse/inter/WallClock;)J

    move-result-wide v3

    const-wide/32 v5, 0x493e0

    add-long/2addr v3, v5

    invoke-interface {v0, v2, v3, v4, v1}, Lee/cyber/smartid/tse/inter/AlarmAccess;->scheduleAlarmFor(Ljava/lang/String;JLjava/lang/String;)V

    return-void

    .line 11
    :cond_4
    :goto_1
    iget-object v0, p0, Lee/cyber/smartid/manager/impl/TransactionCacheManagerImpl;->f:Lee/cyber/smartid/util/Log;

    const-string v1, "setNextRemoveExpiredTransactionsAlarm - failed to find the next Transaction that expires!"

    invoke-virtual {v0, v1}, Lee/cyber/smartid/util/Log;->w(Ljava/lang/String;)V

    return-void
.end method
