.class public Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl;
.super Ljava/lang/Object;
.source "SafetyNetManagerImpl.java"

# interfaces
.implements Lee/cyber/smartid/manager/inter/SafetyNetManager;


# static fields
.field public static final TAG_SAFETY_NET_AUTOMATIC_CHECK:Ljava/lang/String; = "ee.cyber.smartid.TAG_SAFETY_NET_AUTOMATIC_CHECK"

.field private static volatile a:Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl;


# instance fields
.field private volatile b:Z

.field private volatile c:Lee/cyber/smartid/dto/SafetyNetAttestation;

.field private final d:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Lee/cyber/smartid/inter/ServiceAccess;

.field private final f:Lee/cyber/smartid/tse/SmartIdTSE;

.field private final g:Lee/cyber/smartid/inter/ListenerAccess;

.field private final h:Lee/cyber/smartid/tse/inter/WallClock;

.field private final i:Lee/cyber/smartid/cryptolib/CryptoLib;

.field private j:Lee/cyber/smartid/util/Log;


# direct methods
.method private constructor <init>(Lee/cyber/smartid/inter/ServiceAccess;Lee/cyber/smartid/tse/SmartIdTSE;Lee/cyber/smartid/inter/ListenerAccess;Lee/cyber/smartid/tse/inter/WallClock;Lee/cyber/smartid/cryptolib/CryptoLib;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->newSetFromMap(Ljava/util/Map;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl;->d:Ljava/util/Set;

    .line 3
    invoke-static {p0}, Lee/cyber/smartid/util/Log;->getInstance(Ljava/lang/Object;)Lee/cyber/smartid/util/Log;

    move-result-object v0

    iput-object v0, p0, Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl;->j:Lee/cyber/smartid/util/Log;

    .line 4
    iput-object p1, p0, Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl;->e:Lee/cyber/smartid/inter/ServiceAccess;

    .line 5
    iput-object p2, p0, Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl;->f:Lee/cyber/smartid/tse/SmartIdTSE;

    .line 6
    iput-object p3, p0, Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl;->g:Lee/cyber/smartid/inter/ListenerAccess;

    .line 7
    iput-object p4, p0, Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl;->h:Lee/cyber/smartid/tse/inter/WallClock;

    .line 8
    iput-object p5, p0, Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl;->i:Lee/cyber/smartid/cryptolib/CryptoLib;

    return-void
.end method

.method private a(Landroid/content/Context;Lee/cyber/smartid/cryptolib/CryptoLib;Ljava/lang/String;Lee/cyber/smartid/tse/inter/WallClock;Ljava/lang/String;)Lee/cyber/smartid/dto/SafetyNetAttestation;
    .locals 5

    .line 8
    const-class v0, Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl;

    monitor-enter v0

    .line 9
    :try_start_0
    invoke-interface {p4}, Lee/cyber/smartid/tse/inter/WallClock;->currentTimeMillis()J

    move-result-wide v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    const/4 v3, 0x0

    if-nez p1, :cond_0

    .line 10
    :try_start_1
    iget-object p1, p0, Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl;->j:Lee/cyber/smartid/util/Log;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    const-string p2, "attestAndValidateLocally: appContext == null, aborting .."

    :try_start_2
    invoke-virtual {p1, p2}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    .line 11
    new-instance p1, Lee/cyber/smartid/dto/jsonrpc/resp/SafetyNetAttestationSummary;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    const-string p2, "ee.cyber.smartid.TAG_SAFETY_NET_AUTOMATIC_CHECK"

    :try_start_3
    invoke-direct {p1, p2}, Lee/cyber/smartid/dto/jsonrpc/resp/SafetyNetAttestationSummary;-><init>(Ljava/lang/String;)V

    invoke-static {p1, v3, v3}, Lee/cyber/smartid/dto/SafetyNetAttestation;->create(Lee/cyber/smartid/dto/jsonrpc/resp/SafetyNetAttestationSummary;Ljava/lang/String;Lee/cyber/smartid/dto/SafetyNetPayload;)Lee/cyber/smartid/dto/SafetyNetAttestation;

    move-result-object p1
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 12
    :try_start_4
    iget-object p2, p0, Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl;->j:Lee/cyber/smartid/util/Log;

    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    const-string p5, "attestAndValidateLocally took "

    :try_start_5
    invoke-virtual {p3, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-interface {p4}, Lee/cyber/smartid/tse/inter/WallClock;->currentTimeMillis()J

    move-result-wide p4

    sub-long/2addr p4, v1

    invoke-virtual {p3, p4, p5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    const-string p4, " ms"

    :try_start_6
    invoke-virtual {p3, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p2, p3}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    monitor-exit v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    return-object p1

    :catchall_0
    move-exception p1

    goto/16 :goto_1

    :catch_0
    move-exception p1

    goto/16 :goto_0

    :cond_0
    if-nez p2, :cond_1

    .line 13
    :try_start_7
    iget-object p1, p0, Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl;->j:Lee/cyber/smartid/util/Log;
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_0
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    const-string p2, "attestAndValidateLocally: cryptoLib == null, aborting .."

    :try_start_8
    invoke-virtual {p1, p2}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    .line 14
    new-instance p1, Lee/cyber/smartid/dto/jsonrpc/resp/SafetyNetAttestationSummary;
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_0
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    const-string p2, "ee.cyber.smartid.TAG_SAFETY_NET_AUTOMATIC_CHECK"

    :try_start_9
    invoke-direct {p1, p2}, Lee/cyber/smartid/dto/jsonrpc/resp/SafetyNetAttestationSummary;-><init>(Ljava/lang/String;)V

    invoke-static {p1, v3, v3}, Lee/cyber/smartid/dto/SafetyNetAttestation;->create(Lee/cyber/smartid/dto/jsonrpc/resp/SafetyNetAttestationSummary;Ljava/lang/String;Lee/cyber/smartid/dto/SafetyNetPayload;)Lee/cyber/smartid/dto/SafetyNetAttestation;

    move-result-object p1
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_0
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    .line 15
    :try_start_a
    iget-object p2, p0, Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl;->j:Lee/cyber/smartid/util/Log;

    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    const-string p5, "attestAndValidateLocally took "

    :try_start_b
    invoke-virtual {p3, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-interface {p4}, Lee/cyber/smartid/tse/inter/WallClock;->currentTimeMillis()J

    move-result-wide p4

    sub-long/2addr p4, v1

    invoke-virtual {p3, p4, p5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_1

    const-string p4, " ms"

    :try_start_c
    invoke-virtual {p3, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p2, p3}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    monitor-exit v0
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_1

    return-object p1

    .line 16
    :cond_1
    :try_start_d
    invoke-static {}, Lcom/google/android/gms/common/e;->a()Lcom/google/android/gms/common/e;

    move-result-object v4

    invoke-virtual {v4, p1}, Lcom/google/android/gms/common/e;->a(Landroid/content/Context;)I

    move-result v4

    if-eqz v4, :cond_2

    .line 17
    iget-object p1, p0, Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl;->j:Lee/cyber/smartid/util/Log;
    :try_end_d
    .catch Ljava/lang/Exception; {:try_start_d .. :try_end_d} :catch_0
    .catchall {:try_start_d .. :try_end_d} :catchall_0

    const-string p2, "attestAndValidateLocally: Play Services API not available, aborting .."

    :try_start_e
    invoke-virtual {p1, p2}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    .line 18
    new-instance p1, Lee/cyber/smartid/dto/jsonrpc/resp/SafetyNetAttestationSummary;
    :try_end_e
    .catch Ljava/lang/Exception; {:try_start_e .. :try_end_e} :catch_0
    .catchall {:try_start_e .. :try_end_e} :catchall_0

    const-string p2, "ee.cyber.smartid.TAG_SAFETY_NET_AUTOMATIC_CHECK"

    :try_start_f
    invoke-direct {p1, p2}, Lee/cyber/smartid/dto/jsonrpc/resp/SafetyNetAttestationSummary;-><init>(Ljava/lang/String;)V

    invoke-static {p1, v3, v3}, Lee/cyber/smartid/dto/SafetyNetAttestation;->create(Lee/cyber/smartid/dto/jsonrpc/resp/SafetyNetAttestationSummary;Ljava/lang/String;Lee/cyber/smartid/dto/SafetyNetPayload;)Lee/cyber/smartid/dto/SafetyNetAttestation;

    move-result-object p1
    :try_end_f
    .catch Ljava/lang/Exception; {:try_start_f .. :try_end_f} :catch_0
    .catchall {:try_start_f .. :try_end_f} :catchall_0

    .line 19
    :try_start_10
    iget-object p2, p0, Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl;->j:Lee/cyber/smartid/util/Log;

    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V
    :try_end_10
    .catchall {:try_start_10 .. :try_end_10} :catchall_1

    const-string p5, "attestAndValidateLocally took "

    :try_start_11
    invoke-virtual {p3, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-interface {p4}, Lee/cyber/smartid/tse/inter/WallClock;->currentTimeMillis()J

    move-result-wide p4

    sub-long/2addr p4, v1

    invoke-virtual {p3, p4, p5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
    :try_end_11
    .catchall {:try_start_11 .. :try_end_11} :catchall_1

    const-string p4, " ms"

    :try_start_12
    invoke-virtual {p3, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p2, p3}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    monitor-exit v0
    :try_end_12
    .catchall {:try_start_12 .. :try_end_12} :catchall_1

    return-object p1

    .line 20
    :cond_2
    :try_start_13
    invoke-direct {p0, p2, p3}, Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl;->a(Lee/cyber/smartid/cryptolib/CryptoLib;Ljava/lang/String;)[B

    move-result-object p3
    :try_end_13
    .catch Ljava/lang/Exception; {:try_start_13 .. :try_end_13} :catch_0
    .catchall {:try_start_13 .. :try_end_13} :catchall_0

    .line 21
    :try_start_14
    invoke-static {p1}, Lcom/google/android/gms/e/c;->a(Landroid/content/Context;)Lcom/google/android/gms/e/e;

    move-result-object v4

    invoke-virtual {v4, p3, p5}, Lcom/google/android/gms/e/e;->a([BLjava/lang/String;)Lcom/google/android/gms/h/h;

    move-result-object p5
    :try_end_14
    .catch Ljava/lang/Throwable; {:try_start_14 .. :try_end_14} :catch_2
    .catchall {:try_start_14 .. :try_end_14} :catchall_0

    .line 22
    :try_start_15
    invoke-static {p5}, Lcom/google/android/gms/h/k;->a(Lcom/google/android/gms/h/h;)Ljava/lang/Object;

    move-result-object p5

    check-cast p5, Lcom/google/android/gms/e/d$a;
    :try_end_15
    .catch Ljava/lang/Throwable; {:try_start_15 .. :try_end_15} :catch_1
    .catchall {:try_start_15 .. :try_end_15} :catchall_0

    .line 23
    :try_start_16
    invoke-virtual {p5}, Lcom/google/android/gms/e/d$a;->b()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 24
    new-instance p1, Lee/cyber/smartid/dto/jsonrpc/resp/SafetyNetAttestationSummary;
    :try_end_16
    .catch Ljava/lang/Exception; {:try_start_16 .. :try_end_16} :catch_0
    .catchall {:try_start_16 .. :try_end_16} :catchall_0

    const-string p2, "ee.cyber.smartid.TAG_SAFETY_NET_AUTOMATIC_CHECK"

    :try_start_17
    invoke-direct {p1, p2}, Lee/cyber/smartid/dto/jsonrpc/resp/SafetyNetAttestationSummary;-><init>(Ljava/lang/String;)V

    invoke-static {p1, v3, v3}, Lee/cyber/smartid/dto/SafetyNetAttestation;->create(Lee/cyber/smartid/dto/jsonrpc/resp/SafetyNetAttestationSummary;Ljava/lang/String;Lee/cyber/smartid/dto/SafetyNetPayload;)Lee/cyber/smartid/dto/SafetyNetAttestation;

    move-result-object p1
    :try_end_17
    .catch Ljava/lang/Exception; {:try_start_17 .. :try_end_17} :catch_0
    .catchall {:try_start_17 .. :try_end_17} :catchall_0

    .line 25
    :try_start_18
    iget-object p2, p0, Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl;->j:Lee/cyber/smartid/util/Log;

    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V
    :try_end_18
    .catchall {:try_start_18 .. :try_end_18} :catchall_1

    const-string p5, "attestAndValidateLocally took "

    :try_start_19
    invoke-virtual {p3, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-interface {p4}, Lee/cyber/smartid/tse/inter/WallClock;->currentTimeMillis()J

    move-result-wide p4

    sub-long/2addr p4, v1

    invoke-virtual {p3, p4, p5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
    :try_end_19
    .catchall {:try_start_19 .. :try_end_19} :catchall_1

    const-string p4, " ms"

    :try_start_1a
    invoke-virtual {p3, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p2, p3}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    monitor-exit v0
    :try_end_1a
    .catchall {:try_start_1a .. :try_end_1a} :catchall_1

    return-object p1

    .line 26
    :cond_3
    :try_start_1b
    invoke-virtual {p5}, Lcom/google/android/gms/e/d$a;->b()Ljava/lang/String;

    move-result-object p5

    invoke-direct {p0, p1, p2, p5, p3}, Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl;->a(Landroid/content/Context;Lee/cyber/smartid/cryptolib/CryptoLib;Ljava/lang/String;[B)Lee/cyber/smartid/dto/SafetyNetAttestation;

    move-result-object p1
    :try_end_1b
    .catch Ljava/lang/Exception; {:try_start_1b .. :try_end_1b} :catch_0
    .catchall {:try_start_1b .. :try_end_1b} :catchall_0

    .line 27
    :try_start_1c
    iget-object p2, p0, Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl;->j:Lee/cyber/smartid/util/Log;

    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V
    :try_end_1c
    .catchall {:try_start_1c .. :try_end_1c} :catchall_1

    const-string p5, "attestAndValidateLocally took "

    :try_start_1d
    invoke-virtual {p3, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-interface {p4}, Lee/cyber/smartid/tse/inter/WallClock;->currentTimeMillis()J

    move-result-wide p4

    sub-long/2addr p4, v1

    invoke-virtual {p3, p4, p5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
    :try_end_1d
    .catchall {:try_start_1d .. :try_end_1d} :catchall_1

    const-string p4, " ms"

    :try_start_1e
    invoke-virtual {p3, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p2, p3}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    monitor-exit v0
    :try_end_1e
    .catchall {:try_start_1e .. :try_end_1e} :catchall_1

    return-object p1

    :catch_1
    move-exception p1

    .line 28
    :try_start_1f
    iget-object p2, p0, Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl;->j:Lee/cyber/smartid/util/Log;
    :try_end_1f
    .catch Ljava/lang/Exception; {:try_start_1f .. :try_end_1f} :catch_0
    .catchall {:try_start_1f .. :try_end_1f} :catchall_0

    const-string p3, "attestAndValidateLocally: attest failed, aborting .."

    :try_start_20
    invoke-virtual {p2, p3, p1}, Lee/cyber/smartid/util/Log;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 29
    new-instance p1, Lee/cyber/smartid/dto/jsonrpc/resp/SafetyNetAttestationSummary;
    :try_end_20
    .catch Ljava/lang/Exception; {:try_start_20 .. :try_end_20} :catch_0
    .catchall {:try_start_20 .. :try_end_20} :catchall_0

    const-string p2, "ee.cyber.smartid.TAG_SAFETY_NET_AUTOMATIC_CHECK"

    :try_start_21
    invoke-direct {p1, p2}, Lee/cyber/smartid/dto/jsonrpc/resp/SafetyNetAttestationSummary;-><init>(Ljava/lang/String;)V

    invoke-static {p1, v3, v3}, Lee/cyber/smartid/dto/SafetyNetAttestation;->create(Lee/cyber/smartid/dto/jsonrpc/resp/SafetyNetAttestationSummary;Ljava/lang/String;Lee/cyber/smartid/dto/SafetyNetPayload;)Lee/cyber/smartid/dto/SafetyNetAttestation;

    move-result-object p1
    :try_end_21
    .catch Ljava/lang/Exception; {:try_start_21 .. :try_end_21} :catch_0
    .catchall {:try_start_21 .. :try_end_21} :catchall_0

    .line 30
    :try_start_22
    iget-object p2, p0, Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl;->j:Lee/cyber/smartid/util/Log;

    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V
    :try_end_22
    .catchall {:try_start_22 .. :try_end_22} :catchall_1

    const-string p5, "attestAndValidateLocally took "

    :try_start_23
    invoke-virtual {p3, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-interface {p4}, Lee/cyber/smartid/tse/inter/WallClock;->currentTimeMillis()J

    move-result-wide p4

    sub-long/2addr p4, v1

    invoke-virtual {p3, p4, p5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
    :try_end_23
    .catchall {:try_start_23 .. :try_end_23} :catchall_1

    const-string p4, " ms"

    :try_start_24
    invoke-virtual {p3, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p2, p3}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    monitor-exit v0
    :try_end_24
    .catchall {:try_start_24 .. :try_end_24} :catchall_1

    return-object p1

    :catch_2
    move-exception p1

    .line 31
    :try_start_25
    iget-object p2, p0, Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl;->j:Lee/cyber/smartid/util/Log;
    :try_end_25
    .catch Ljava/lang/Exception; {:try_start_25 .. :try_end_25} :catch_0
    .catchall {:try_start_25 .. :try_end_25} :catchall_0

    const-string p3, "attestAndValidateLocally: attest failed, aborting .."

    :try_start_26
    invoke-virtual {p2, p3, p1}, Lee/cyber/smartid/util/Log;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 32
    new-instance p1, Lee/cyber/smartid/dto/jsonrpc/resp/SafetyNetAttestationSummary;
    :try_end_26
    .catch Ljava/lang/Exception; {:try_start_26 .. :try_end_26} :catch_0
    .catchall {:try_start_26 .. :try_end_26} :catchall_0

    const-string p2, "ee.cyber.smartid.TAG_SAFETY_NET_AUTOMATIC_CHECK"

    :try_start_27
    invoke-direct {p1, p2}, Lee/cyber/smartid/dto/jsonrpc/resp/SafetyNetAttestationSummary;-><init>(Ljava/lang/String;)V

    invoke-static {p1, v3, v3}, Lee/cyber/smartid/dto/SafetyNetAttestation;->create(Lee/cyber/smartid/dto/jsonrpc/resp/SafetyNetAttestationSummary;Ljava/lang/String;Lee/cyber/smartid/dto/SafetyNetPayload;)Lee/cyber/smartid/dto/SafetyNetAttestation;

    move-result-object p1
    :try_end_27
    .catch Ljava/lang/Exception; {:try_start_27 .. :try_end_27} :catch_0
    .catchall {:try_start_27 .. :try_end_27} :catchall_0

    .line 33
    :try_start_28
    iget-object p2, p0, Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl;->j:Lee/cyber/smartid/util/Log;

    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V
    :try_end_28
    .catchall {:try_start_28 .. :try_end_28} :catchall_1

    const-string p5, "attestAndValidateLocally took "

    :try_start_29
    invoke-virtual {p3, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-interface {p4}, Lee/cyber/smartid/tse/inter/WallClock;->currentTimeMillis()J

    move-result-wide p4

    sub-long/2addr p4, v1

    invoke-virtual {p3, p4, p5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
    :try_end_29
    .catchall {:try_start_29 .. :try_end_29} :catchall_1

    const-string p4, " ms"

    :try_start_2a
    invoke-virtual {p3, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p2, p3}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    monitor-exit v0
    :try_end_2a
    .catchall {:try_start_2a .. :try_end_2a} :catchall_1

    return-object p1

    .line 34
    :goto_0
    :try_start_2b
    iget-object p2, p0, Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl;->j:Lee/cyber/smartid/util/Log;
    :try_end_2b
    .catchall {:try_start_2b .. :try_end_2b} :catchall_0

    const-string p3, "attestAndValidateLocally"

    :try_start_2c
    invoke-virtual {p2, p3, p1}, Lee/cyber/smartid/util/Log;->e(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2c
    .catchall {:try_start_2c .. :try_end_2c} :catchall_0

    .line 35
    :try_start_2d
    iget-object p1, p0, Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl;->j:Lee/cyber/smartid/util/Log;

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V
    :try_end_2d
    .catchall {:try_start_2d .. :try_end_2d} :catchall_1

    const-string p3, "attestAndValidateLocally took "

    :try_start_2e
    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-interface {p4}, Lee/cyber/smartid/tse/inter/WallClock;->currentTimeMillis()J

    move-result-wide p3

    sub-long/2addr p3, v1

    invoke-virtual {p2, p3, p4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
    :try_end_2e
    .catchall {:try_start_2e .. :try_end_2e} :catchall_1

    const-string p3, " ms"

    :try_start_2f
    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    .line 36
    new-instance p1, Lee/cyber/smartid/dto/jsonrpc/resp/SafetyNetAttestationSummary;
    :try_end_2f
    .catchall {:try_start_2f .. :try_end_2f} :catchall_1

    const-string p2, "ee.cyber.smartid.TAG_SAFETY_NET_AUTOMATIC_CHECK"

    :try_start_30
    invoke-direct {p1, p2}, Lee/cyber/smartid/dto/jsonrpc/resp/SafetyNetAttestationSummary;-><init>(Ljava/lang/String;)V

    invoke-static {p1, v3, v3}, Lee/cyber/smartid/dto/SafetyNetAttestation;->create(Lee/cyber/smartid/dto/jsonrpc/resp/SafetyNetAttestationSummary;Ljava/lang/String;Lee/cyber/smartid/dto/SafetyNetPayload;)Lee/cyber/smartid/dto/SafetyNetAttestation;

    move-result-object p1

    monitor-exit v0

    return-object p1

    .line 37
    :goto_1
    iget-object p2, p0, Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl;->j:Lee/cyber/smartid/util/Log;

    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V
    :try_end_30
    .catchall {:try_start_30 .. :try_end_30} :catchall_1

    const-string p5, "attestAndValidateLocally took "

    :try_start_31
    invoke-virtual {p3, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-interface {p4}, Lee/cyber/smartid/tse/inter/WallClock;->currentTimeMillis()J

    move-result-wide p4

    sub-long/2addr p4, v1

    invoke-virtual {p3, p4, p5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
    :try_end_31
    .catchall {:try_start_31 .. :try_end_31} :catchall_1

    const-string p4, " ms"

    :try_start_32
    invoke-virtual {p3, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p2, p3}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    throw p1

    :catchall_1
    move-exception p1

    .line 38
    monitor-exit v0
    :try_end_32
    .catchall {:try_start_32 .. :try_end_32} :catchall_1

    throw p1
.end method

.method private a(Landroid/content/Context;Lee/cyber/smartid/cryptolib/CryptoLib;Ljava/lang/String;[B)Lee/cyber/smartid/dto/SafetyNetAttestation;
    .locals 10

    const-string v0, "validateAttestationResponse"

    const-string v1, "ee.cyber.smartid.TAG_SAFETY_NET_AUTOMATIC_CHECK"

    const/4 v2, 0x0

    if-eqz p1, :cond_8

    if-eqz p2, :cond_8

    .line 39
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    goto/16 :goto_a

    .line 40
    :cond_0
    :try_start_0
    invoke-virtual {p2, p3}, Lee/cyber/smartid/cryptolib/CryptoLib;->parseAndExtractPayloadFromJWS(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 41
    new-instance v4, Lcom/google/gson/g;

    invoke-direct {v4}, Lcom/google/gson/g;-><init>()V

    invoke-virtual {v4}, Lcom/google/gson/g;->a()Lcom/google/gson/f;

    move-result-object v4

    const-class v5, Lee/cyber/smartid/dto/SafetyNetPayload;

    invoke-virtual {v4, v3, v5}, Lcom/google/gson/f;->a(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lee/cyber/smartid/dto/SafetyNetPayload;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_6

    const/4 v6, 0x1

    const/4 v1, 0x1

    const/4 v4, 0x0

    if-eqz p4, :cond_1

    .line 42
    :try_start_1
    invoke-virtual {p2, p4}, Lee/cyber/smartid/cryptolib/CryptoLib;->encodeBytesToBase64([B)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    :catch_0
    move-exception p1

    const/4 p4, 0x1

    goto/16 :goto_3

    .line 43
    :cond_1
    :goto_0
    invoke-virtual {v3}, Lee/cyber/smartid/dto/SafetyNetPayload;->getNonce()Ljava/lang/String;

    move-result-object p4

    invoke-static {v2, p4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result p4

    if-nez p4, :cond_2

    .line 44
    iget-object p4, p0, Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl;->j:Lee/cyber/smartid/util/Log;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    const-string v2, "validateAttestationResponse - nonce doesn\'t match!"

    :try_start_2
    invoke-virtual {p4, v2}, Lee/cyber/smartid/util/Log;->w(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    const/4 p4, 0x0

    goto :goto_1

    :cond_2
    const/4 p4, 0x1

    .line 45
    :goto_1
    :try_start_3
    invoke-virtual {v3}, Lee/cyber/smartid/dto/SafetyNetPayload;->getApkPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3}, Lee/cyber/smartid/dto/SafetyNetPayload;->getApkPackageName()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v5}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 46
    iget-object v2, p0, Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl;->j:Lee/cyber/smartid/util/Log;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    const-string v5, "validateAttestationResponse - apkPackageName doesn\'t match!"

    :try_start_4
    invoke-virtual {v2, v5}, Lee/cyber/smartid/util/Log;->w(Ljava/lang/String;)V

    const/4 p4, 0x0

    goto :goto_2

    :catch_1
    move-exception p1

    goto :goto_3

    .line 47
    :cond_3
    :goto_2
    invoke-virtual {v3}, Lee/cyber/smartid/dto/SafetyNetPayload;->getApkCertificateDigestSha256()[Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_4

    invoke-virtual {v3}, Lee/cyber/smartid/dto/SafetyNetPayload;->getApkCertificateDigestSha256()[Ljava/lang/String;

    move-result-object v2

    array-length v2, v2

    if-lez v2, :cond_4

    invoke-static {p1, p2}, Lee/cyber/smartid/util/Util;->getApkCertificateDigestSha256Base64(Landroid/content/Context;Lee/cyber/smartid/cryptolib/CryptoLib;)[Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3}, Lee/cyber/smartid/dto/SafetyNetPayload;->getApkCertificateDigestSha256()[Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v5}, Lee/cyber/smartid/util/Util;->arrayEqualsIgnoreOrder([Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 48
    iget-object v2, p0, Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl;->j:Lee/cyber/smartid/util/Log;
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1

    const-string v5, "validateAttestationResponse - apkCertificateDigestSha256 doesn\'t match!"

    :try_start_5
    invoke-virtual {v2, v5}, Lee/cyber/smartid/util/Log;->w(Ljava/lang/String;)V

    const/4 p4, 0x0

    .line 49
    :cond_4
    invoke-virtual {v3}, Lee/cyber/smartid/dto/SafetyNetPayload;->getApkDigestSha256()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_5

    invoke-static {p1, p2}, Lee/cyber/smartid/util/Util;->getApkDigestSha256(Landroid/content/Context;Lee/cyber/smartid/cryptolib/CryptoLib;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v3}, Lee/cyber/smartid/dto/SafetyNetPayload;->getApkDigestSha256()Ljava/lang/String;

    move-result-object p2

    invoke-static {p1, p2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result p1

    if-nez p1, :cond_5

    .line 50
    iget-object p1, p0, Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl;->j:Lee/cyber/smartid/util/Log;
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_1

    const-string p2, "validateAttestationResponse - apkDigestSha256 doesn\'t match!"

    :try_start_6
    invoke-virtual {p1, p2}, Lee/cyber/smartid/util/Log;->w(Ljava/lang/String;)V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_1

    const/4 v7, 0x0

    goto :goto_5

    :goto_3
    move v7, p4

    const/4 v8, 0x1

    :goto_4
    const/4 v9, 0x1

    goto :goto_9

    :cond_5
    move v7, p4

    .line 51
    :goto_5
    :try_start_7
    invoke-virtual {v3}, Lee/cyber/smartid/dto/SafetyNetPayload;->isBasicIntegrity()Z

    move-result p1
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_5

    if-eqz p1, :cond_6

    const/4 v8, 0x1

    goto :goto_6

    .line 52
    :cond_6
    :try_start_8
    iget-object p1, p0, Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl;->j:Lee/cyber/smartid/util/Log;
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_4

    const-string p2, "validateAttestationResponse - basicIntegrity is false!"

    :try_start_9
    invoke-virtual {p1, p2}, Lee/cyber/smartid/util/Log;->w(Ljava/lang/String;)V
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_4

    const/4 v8, 0x0

    .line 53
    :goto_6
    :try_start_a
    invoke-virtual {v3}, Lee/cyber/smartid/dto/SafetyNetPayload;->isCtsProfileMatch()Z

    move-result p1
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_3

    if-eqz p1, :cond_7

    const/4 v9, 0x1

    goto :goto_7

    .line 54
    :cond_7
    :try_start_b
    iget-object p1, p0, Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl;->j:Lee/cyber/smartid/util/Log;
    :try_end_b
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_2

    const-string p2, "validateAttestationResponse - ctsProfileMatch is false!"

    :try_start_c
    invoke-virtual {p1, p2}, Lee/cyber/smartid/util/Log;->w(Ljava/lang/String;)V
    :try_end_c
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_c} :catch_2

    const/4 v9, 0x0

    .line 55
    :goto_7
    new-instance p1, Lee/cyber/smartid/dto/jsonrpc/resp/SafetyNetAttestationSummary;

    const-string v5, "ee.cyber.smartid.TAG_SAFETY_NET_AUTOMATIC_CHECK"

    move-object v4, p1

    invoke-direct/range {v4 .. v9}, Lee/cyber/smartid/dto/jsonrpc/resp/SafetyNetAttestationSummary;-><init>(Ljava/lang/String;ZZZZ)V

    invoke-static {p1, p3, v3}, Lee/cyber/smartid/dto/SafetyNetAttestation;->create(Lee/cyber/smartid/dto/jsonrpc/resp/SafetyNetAttestationSummary;Ljava/lang/String;Lee/cyber/smartid/dto/SafetyNetPayload;)Lee/cyber/smartid/dto/SafetyNetAttestation;

    move-result-object p1

    return-object p1

    :catch_2
    move-exception p1

    const/4 v9, 0x0

    goto :goto_9

    :catch_3
    move-exception p1

    move v4, v8

    goto :goto_8

    :catch_4
    move-exception p1

    :goto_8
    move v8, v4

    goto :goto_4

    :catch_5
    move-exception p1

    move p4, v7

    goto :goto_3

    .line 56
    :goto_9
    iget-object p2, p0, Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl;->j:Lee/cyber/smartid/util/Log;

    invoke-virtual {p2, v0, p1}, Lee/cyber/smartid/util/Log;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 57
    new-instance p1, Lee/cyber/smartid/dto/jsonrpc/resp/SafetyNetAttestationSummary;

    const/4 v6, 0x1

    const-string v5, "ee.cyber.smartid.TAG_SAFETY_NET_AUTOMATIC_CHECK"

    move-object v4, p1

    invoke-direct/range {v4 .. v9}, Lee/cyber/smartid/dto/jsonrpc/resp/SafetyNetAttestationSummary;-><init>(Ljava/lang/String;ZZZZ)V

    invoke-static {p1, p3, v3}, Lee/cyber/smartid/dto/SafetyNetAttestation;->create(Lee/cyber/smartid/dto/jsonrpc/resp/SafetyNetAttestationSummary;Ljava/lang/String;Lee/cyber/smartid/dto/SafetyNetPayload;)Lee/cyber/smartid/dto/SafetyNetAttestation;

    move-result-object p1

    return-object p1

    :catch_6
    move-exception p1

    .line 58
    iget-object p2, p0, Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl;->j:Lee/cyber/smartid/util/Log;

    invoke-virtual {p2, v0, p1}, Lee/cyber/smartid/util/Log;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 59
    new-instance p1, Lee/cyber/smartid/dto/jsonrpc/resp/SafetyNetAttestationSummary;

    invoke-direct {p1, v1}, Lee/cyber/smartid/dto/jsonrpc/resp/SafetyNetAttestationSummary;-><init>(Ljava/lang/String;)V

    invoke-static {p1, p3, v2}, Lee/cyber/smartid/dto/SafetyNetAttestation;->create(Lee/cyber/smartid/dto/jsonrpc/resp/SafetyNetAttestationSummary;Ljava/lang/String;Lee/cyber/smartid/dto/SafetyNetPayload;)Lee/cyber/smartid/dto/SafetyNetAttestation;

    move-result-object p1

    return-object p1

    .line 60
    :cond_8
    :goto_a
    new-instance p1, Lee/cyber/smartid/dto/jsonrpc/resp/SafetyNetAttestationSummary;

    invoke-direct {p1, v1}, Lee/cyber/smartid/dto/jsonrpc/resp/SafetyNetAttestationSummary;-><init>(Ljava/lang/String;)V

    invoke-static {p1, p3, v2}, Lee/cyber/smartid/dto/SafetyNetAttestation;->create(Lee/cyber/smartid/dto/jsonrpc/resp/SafetyNetAttestationSummary;Ljava/lang/String;Lee/cyber/smartid/dto/SafetyNetPayload;)Lee/cyber/smartid/dto/SafetyNetAttestation;

    move-result-object p1

    return-object p1
.end method

.method static synthetic a(Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl;Landroid/content/Context;Lee/cyber/smartid/cryptolib/CryptoLib;Ljava/lang/String;Lee/cyber/smartid/tse/inter/WallClock;Ljava/lang/String;)Lee/cyber/smartid/dto/SafetyNetAttestation;
    .locals 0

    .line 3
    invoke-direct/range {p0 .. p5}, Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl;->a(Landroid/content/Context;Lee/cyber/smartid/cryptolib/CryptoLib;Ljava/lang/String;Lee/cyber/smartid/tse/inter/WallClock;Ljava/lang/String;)Lee/cyber/smartid/dto/SafetyNetAttestation;

    move-result-object p0

    return-object p0
.end method

.method static synthetic a(Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl;Lee/cyber/smartid/dto/SafetyNetAttestation;)Lee/cyber/smartid/dto/SafetyNetAttestation;
    .locals 0

    .line 4
    iput-object p1, p0, Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl;->c:Lee/cyber/smartid/dto/SafetyNetAttestation;

    return-object p1
.end method

.method static synthetic a(Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl;)Lee/cyber/smartid/util/Log;
    .locals 0

    .line 1
    iget-object p0, p0, Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl;->j:Lee/cyber/smartid/util/Log;

    return-object p0
.end method

.method static synthetic a(Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl;Ljava/lang/String;Lee/cyber/smartid/dto/SafetyNetAttestation;)V
    .locals 0

    .line 5
    invoke-direct {p0, p1, p2}, Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl;->a(Ljava/lang/String;Lee/cyber/smartid/dto/SafetyNetAttestation;)V

    return-void
.end method

.method private a(Ljava/lang/String;Lee/cyber/smartid/dto/SafetyNetAttestation;)V
    .locals 3

    .line 6
    new-instance v0, Lee/cyber/smartid/dto/jsonrpc/resp/GetSafetyNetAttestationResp;

    invoke-direct {v0, p1, p2}, Lee/cyber/smartid/dto/jsonrpc/resp/GetSafetyNetAttestationResp;-><init>(Ljava/lang/String;Lee/cyber/smartid/dto/SafetyNetAttestation;)V

    .line 7
    iget-object v1, p0, Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl;->e:Lee/cyber/smartid/inter/ServiceAccess;

    new-instance v2, Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl$2;

    invoke-direct {v2, p0, p1, p2, v0}, Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl$2;-><init>(Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl;Ljava/lang/String;Lee/cyber/smartid/dto/SafetyNetAttestation;Lee/cyber/smartid/dto/jsonrpc/resp/GetSafetyNetAttestationResp;)V

    invoke-interface {v1, v2}, Lee/cyber/smartid/inter/ServiceAccess;->notifyUI(Ljava/lang/Runnable;)V

    return-void
.end method

.method static synthetic a(Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl;Z)Z
    .locals 0

    .line 2
    iput-boolean p1, p0, Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl;->b:Z

    return p1
.end method

.method private a(Lee/cyber/smartid/cryptolib/CryptoLib;Ljava/lang/String;)[B
    .locals 2

    const/16 v0, 0x18

    .line 61
    new-array v0, v0, [B

    .line 62
    invoke-virtual {p1}, Lee/cyber/smartid/cryptolib/CryptoLib;->getRandom()Ljava/security/SecureRandom;

    move-result-object p1

    invoke-virtual {p1, v0}, Ljava/security/SecureRandom;->nextBytes([B)V

    .line 63
    new-instance p1, Ljava/io/ByteArrayOutputStream;

    invoke-direct {p1}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 64
    :try_start_0
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v1, :cond_0

    const-string v1, "utf-8"

    .line 65
    :try_start_1
    invoke-virtual {p2, v1}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object p2

    invoke-virtual {p1, p2}, Ljava/io/ByteArrayOutputStream;->write([B)V

    .line 66
    :cond_0
    invoke-virtual {p1, v0}, Ljava/io/ByteArrayOutputStream;->write([B)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 67
    invoke-virtual {p1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object p1

    return-object p1

    :catch_0
    move-exception p1

    .line 68
    iget-object p2, p0, Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl;->j:Lee/cyber/smartid/util/Log;

    const-string v1, "createNonce"

    invoke-virtual {p2, v1, p1}, Lee/cyber/smartid/util/Log;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    return-object v0
.end method

.method static synthetic b(Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl;)Lee/cyber/smartid/inter/ServiceAccess;
    .locals 0

    .line 1
    iget-object p0, p0, Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl;->e:Lee/cyber/smartid/inter/ServiceAccess;

    return-object p0
.end method

.method static synthetic c(Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl;)Lee/cyber/smartid/cryptolib/CryptoLib;
    .locals 0

    .line 1
    iget-object p0, p0, Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl;->i:Lee/cyber/smartid/cryptolib/CryptoLib;

    return-object p0
.end method

.method static synthetic d(Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl;)Lee/cyber/smartid/tse/inter/WallClock;
    .locals 0

    .line 1
    iget-object p0, p0, Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl;->h:Lee/cyber/smartid/tse/inter/WallClock;

    return-object p0
.end method

.method static synthetic e(Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl;)Ljava/util/Set;
    .locals 0

    .line 1
    iget-object p0, p0, Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl;->d:Ljava/util/Set;

    return-object p0
.end method

.method static synthetic f(Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl;)Lee/cyber/smartid/dto/SafetyNetAttestation;
    .locals 0

    .line 1
    iget-object p0, p0, Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl;->c:Lee/cyber/smartid/dto/SafetyNetAttestation;

    return-object p0
.end method

.method static synthetic g(Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl;)Lee/cyber/smartid/inter/ListenerAccess;
    .locals 0

    .line 1
    iget-object p0, p0, Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl;->g:Lee/cyber/smartid/inter/ListenerAccess;

    return-object p0
.end method

.method public static getInstance(Lee/cyber/smartid/inter/ServiceAccess;Lee/cyber/smartid/tse/SmartIdTSE;Lee/cyber/smartid/inter/ListenerAccess;Lee/cyber/smartid/tse/inter/WallClock;Lee/cyber/smartid/cryptolib/CryptoLib;)Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl;
    .locals 8

    .line 1
    sget-object v0, Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl;->a:Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl;

    if-nez v0, :cond_0

    .line 2
    const-class v0, Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl;

    monitor-enter v0

    .line 3
    :try_start_0
    new-instance v7, Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl;

    move-object v1, v7

    move-object v2, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    move-object v6, p4

    invoke-direct/range {v1 .. v6}, Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl;-><init>(Lee/cyber/smartid/inter/ServiceAccess;Lee/cyber/smartid/tse/SmartIdTSE;Lee/cyber/smartid/inter/ListenerAccess;Lee/cyber/smartid/tse/inter/WallClock;Lee/cyber/smartid/cryptolib/CryptoLib;)V

    sput-object v7, Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl;->a:Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl;

    .line 4
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception p0

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p0

    .line 5
    :cond_0
    :goto_0
    sget-object p0, Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl;->a:Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl;

    return-object p0
.end method


# virtual methods
.method public getLastSafetyNetAttestation()Lee/cyber/smartid/dto/SafetyNetAttestation;
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl;->c:Lee/cyber/smartid/dto/SafetyNetAttestation;

    return-object v0
.end method

.method public getSafetyNetAttestation(Ljava/lang/String;ZLee/cyber/smartid/inter/GetSafetyNetAttestationListener;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl;->g:Lee/cyber/smartid/inter/ListenerAccess;

    invoke-interface {v0, p1, p3}, Lee/cyber/smartid/inter/ListenerAccess;->setListener(Ljava/lang/String;Lee/cyber/smartid/inter/ServiceListener;)V

    .line 2
    iget-object p3, p0, Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl;->j:Lee/cyber/smartid/util/Log;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "getSafetyNetAttestation - forceNewAttestation: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    .line 3
    iget-object p3, p0, Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl;->c:Lee/cyber/smartid/dto/SafetyNetAttestation;

    if-nez p2, :cond_0

    if-eqz p3, :cond_0

    .line 4
    invoke-virtual {p3}, Lee/cyber/smartid/dto/SafetyNetAttestation;->getPayloadSummary()Lee/cyber/smartid/dto/jsonrpc/resp/SafetyNetAttestationSummary;

    move-result-object p2

    if-eqz p2, :cond_0

    invoke-virtual {p3}, Lee/cyber/smartid/dto/SafetyNetAttestation;->getPayloadSummary()Lee/cyber/smartid/dto/jsonrpc/resp/SafetyNetAttestationSummary;

    move-result-object p2

    invoke-virtual {p2}, Lee/cyber/smartid/dto/jsonrpc/resp/SafetyNetAttestationSummary;->isAttestationDataPresent()Z

    move-result p2

    if-eqz p2, :cond_0

    .line 5
    iget-object p2, p0, Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl;->j:Lee/cyber/smartid/util/Log;

    const-string v0, "getSafetyNetAttestation - check is already done, we have the result"

    invoke-virtual {p2, v0}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    .line 6
    invoke-direct {p0, p1, p3}, Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl;->a(Ljava/lang/String;Lee/cyber/smartid/dto/SafetyNetAttestation;)V

    return-void

    .line 7
    :cond_0
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p2

    if-nez p2, :cond_1

    .line 8
    iget-object p2, p0, Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl;->d:Ljava/util/Set;

    invoke-interface {p2, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 9
    :cond_1
    iget-boolean p1, p0, Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl;->b:Z

    if-eqz p1, :cond_2

    .line 10
    iget-object p1, p0, Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl;->j:Lee/cyber/smartid/util/Log;

    const-string p2, "getSafetyNetAttestation - check is in progress, lets wait for it"

    invoke-virtual {p1, p2}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    goto :goto_0

    .line 11
    :cond_2
    iget-object p1, p0, Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl;->j:Lee/cyber/smartid/util/Log;

    const-string p2, "getSafetyNetAttestation - check is not started yet, starting it"

    invoke-virtual {p1, p2}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    .line 12
    invoke-virtual {p0}, Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl;->startSafetyNetCheckAsync()V

    :goto_0
    return-void
.end method

.method public getSafetyNetResultHash()Ljava/lang/String;
    .locals 3

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl;->i:Lee/cyber/smartid/cryptolib/CryptoLib;

    iget-object v1, p0, Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl;->e:Lee/cyber/smartid/inter/ServiceAccess;

    invoke-interface {v1}, Lee/cyber/smartid/inter/ServiceAccess;->getSafetyNetResultHashStorageId()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl$3;

    invoke-direct {v2, p0}, Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl$3;-><init>(Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl;)V

    .line 2
    invoke-virtual {v2}, Lcom/google/gson/b/a;->getType()Ljava/lang/reflect/Type;

    move-result-object v2

    .line 3
    invoke-virtual {v0, v1, v2}, Lee/cyber/smartid/cryptolib/CryptoLib;->retrieveData(Ljava/lang/String;Ljava/lang/reflect/Type;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public isSafetyNetCheckInProgress()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl;->b:Z

    return v0
.end method

.method public setSafetyNetResultHash(Ljava/lang/String;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lee/cyber/smartid/cryptolib/dto/StorageException;
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl;->i:Lee/cyber/smartid/cryptolib/CryptoLib;

    iget-object v1, p0, Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl;->e:Lee/cyber/smartid/inter/ServiceAccess;

    invoke-interface {v1}, Lee/cyber/smartid/inter/ServiceAccess;->getSafetyNetResultHashStorageId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Lee/cyber/smartid/cryptolib/CryptoLib;->storeData(Ljava/lang/String;Ljava/lang/Object;)V

    return-void
.end method

.method public shouldStartSafetyNetCheckPostInit()Z
    .locals 2

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl;->e:Lee/cyber/smartid/inter/ServiceAccess;

    invoke-interface {v0}, Lee/cyber/smartid/inter/ServiceAccess;->getPropertiesManager()Lee/cyber/smartid/manager/inter/PropertiesManager;

    move-result-object v0

    invoke-interface {v0}, Lee/cyber/smartid/manager/inter/PropertiesManager;->getPropSafetyNetAutomaticRequestMode()Ljava/lang/String;

    move-result-object v0

    const-string v1, "initonly"

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    return v0
.end method

.method public startPostInitSafetyNetCheckIfNeeded()Z
    .locals 1

    .line 1
    invoke-virtual {p0}, Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl;->shouldStartSafetyNetCheckPostInit()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2
    invoke-virtual {p0}, Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl;->startSafetyNetCheckAsync()V

    const/4 v0, 0x1

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public startSafetyNetCheckAsync()V
    .locals 3

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl;->j:Lee/cyber/smartid/util/Log;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "startSafetyNetCheckAsync - Starting the automatic SafetyNet check, automatic request mode is set to \""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl;->e:Lee/cyber/smartid/inter/ServiceAccess;

    invoke-interface {v2}, Lee/cyber/smartid/inter/ServiceAccess;->getPropertiesManager()Lee/cyber/smartid/manager/inter/PropertiesManager;

    move-result-object v2

    invoke-interface {v2}, Lee/cyber/smartid/manager/inter/PropertiesManager;->getPropSafetyNetAutomaticRequestMode()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "\""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    .line 2
    iget-object v0, p0, Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl;->e:Lee/cyber/smartid/inter/ServiceAccess;

    invoke-interface {v0}, Lee/cyber/smartid/inter/ServiceAccess;->acquireWakeLockForService()V

    .line 3
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl$1;

    invoke-direct {v1, p0}, Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl$1;-><init>(Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 4
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    return-void
.end method
