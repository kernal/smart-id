.class Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$2;
.super Ljava/lang/Object;
.source "AddAccountManagerImpl.java"

# interfaces
.implements Lee/cyber/smartid/tse/inter/GenerateDiffieHellmanKeyPairListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:Ljava/lang/String;

.field final synthetic c:Ljava/lang/String;

.field final synthetic d:Ljava/lang/String;

.field final synthetic e:Ljava/lang/String;

.field final synthetic f:Ljava/lang/String;

.field final synthetic g:Ljava/lang/String;

.field final synthetic h:Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;


# direct methods
.method constructor <init>(Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$2;->h:Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;

    iput-object p2, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$2;->a:Ljava/lang/String;

    iput-object p3, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$2;->b:Ljava/lang/String;

    iput-object p4, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$2;->c:Ljava/lang/String;

    iput-object p5, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$2;->d:Ljava/lang/String;

    iput-object p6, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$2;->e:Ljava/lang/String;

    iput-object p7, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$2;->f:Ljava/lang/String;

    iput-object p8, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$2;->g:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onGenerateDiffieHellmanKeyPairFailed(Ljava/lang/String;Lee/cyber/smartid/tse/dto/TSEError;)V
    .locals 1

    .line 1
    iget-object p1, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$2;->h:Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;

    invoke-static {p1}, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;->d(Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;)Lee/cyber/smartid/util/Log;

    move-result-object p1

    const-string v0, "generateDiffieHellmanKeyPairsForAddAccount - fail for auth"

    invoke-virtual {p1, v0}, Lee/cyber/smartid/util/Log;->e(Ljava/lang/String;)V

    .line 2
    iget-object p1, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$2;->h:Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;

    invoke-static {p1}, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;->c(Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;)Lee/cyber/smartid/inter/ServiceAccess;

    move-result-object p1

    new-instance v0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$2$2;

    invoke-direct {v0, p0, p2}, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$2$2;-><init>(Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$2;Lee/cyber/smartid/tse/dto/TSEError;)V

    invoke-interface {p1, v0}, Lee/cyber/smartid/inter/ServiceAccess;->notifyUI(Ljava/lang/Runnable;)V

    return-void
.end method

.method public onGenerateDiffieHellmanKeyPairSuccess(Ljava/lang/String;Lee/cyber/smartid/tse/dto/jsonrpc/resp/GenerateDiffieHellmanKeyPairResp;)V
    .locals 3

    .line 1
    iget-object p1, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$2;->h:Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;

    invoke-static {p1}, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;->d(Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;)Lee/cyber/smartid/util/Log;

    move-result-object p1

    const-string v0, "generateDiffieHellmanKeyPairsForAddAccount - success for auth"

    invoke-virtual {p1, v0}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    .line 2
    iget-object p1, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$2;->h:Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;

    invoke-static {p1}, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;->e(Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;)Lee/cyber/smartid/tse/SmartIdTSE;

    move-result-object p1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ee.cyber.smartid.TAG_GENERATE_DH_KEY_PAIR_FOR_REGISTRATION_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$2;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$2;->a:Ljava/lang/String;

    new-instance v2, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$2$1;

    invoke-direct {v2, p0, p2}, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$2$1;-><init>(Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$2;Lee/cyber/smartid/tse/dto/jsonrpc/resp/GenerateDiffieHellmanKeyPairResp;)V

    invoke-virtual {p1, v0, v1, v2}, Lee/cyber/smartid/tse/SmartIdTSE;->generateDiffieHellmanKeyPairForTSEKey(Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/tse/inter/GenerateDiffieHellmanKeyPairListener;)V

    return-void
.end method
