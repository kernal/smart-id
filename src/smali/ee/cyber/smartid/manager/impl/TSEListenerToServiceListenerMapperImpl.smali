.class public Lee/cyber/smartid/manager/impl/TSEListenerToServiceListenerMapperImpl;
.super Ljava/lang/Object;
.source "TSEListenerToServiceListenerMapperImpl.java"

# interfaces
.implements Lee/cyber/smartid/manager/inter/TSEListenerToServiceListenerMapper;


# instance fields
.field private final a:Lee/cyber/smartid/inter/ServiceAccess;

.field private final b:Lee/cyber/smartid/inter/ListenerAccess;

.field private final c:Lee/cyber/smartid/tse/SmartIdTSE;

.field private d:Lee/cyber/smartid/util/Log;


# direct methods
.method public constructor <init>(Lee/cyber/smartid/inter/ServiceAccess;Lee/cyber/smartid/inter/ListenerAccess;Lee/cyber/smartid/tse/SmartIdTSE;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    invoke-static {p0}, Lee/cyber/smartid/util/Log;->getInstance(Ljava/lang/Object;)Lee/cyber/smartid/util/Log;

    move-result-object v0

    iput-object v0, p0, Lee/cyber/smartid/manager/impl/TSEListenerToServiceListenerMapperImpl;->d:Lee/cyber/smartid/util/Log;

    .line 3
    iput-object p1, p0, Lee/cyber/smartid/manager/impl/TSEListenerToServiceListenerMapperImpl;->a:Lee/cyber/smartid/inter/ServiceAccess;

    .line 4
    iput-object p2, p0, Lee/cyber/smartid/manager/impl/TSEListenerToServiceListenerMapperImpl;->b:Lee/cyber/smartid/inter/ListenerAccess;

    .line 5
    iput-object p3, p0, Lee/cyber/smartid/manager/impl/TSEListenerToServiceListenerMapperImpl;->c:Lee/cyber/smartid/tse/SmartIdTSE;

    return-void
.end method

.method static synthetic a(Lee/cyber/smartid/manager/impl/TSEListenerToServiceListenerMapperImpl;)Lee/cyber/smartid/inter/ListenerAccess;
    .locals 0

    .line 1
    iget-object p0, p0, Lee/cyber/smartid/manager/impl/TSEListenerToServiceListenerMapperImpl;->b:Lee/cyber/smartid/inter/ListenerAccess;

    return-object p0
.end method

.method private a()Lee/cyber/smartid/tse/inter/TSEListener;
    .locals 1

    .line 2
    new-instance v0, Lee/cyber/smartid/manager/impl/TSEListenerToServiceListenerMapperImpl$1;

    invoke-direct {v0, p0}, Lee/cyber/smartid/manager/impl/TSEListenerToServiceListenerMapperImpl$1;-><init>(Lee/cyber/smartid/manager/impl/TSEListenerToServiceListenerMapperImpl;)V

    return-object v0
.end method

.method static synthetic b(Lee/cyber/smartid/manager/impl/TSEListenerToServiceListenerMapperImpl;)Lee/cyber/smartid/inter/ServiceAccess;
    .locals 0

    .line 1
    iget-object p0, p0, Lee/cyber/smartid/manager/impl/TSEListenerToServiceListenerMapperImpl;->a:Lee/cyber/smartid/inter/ServiceAccess;

    return-object p0
.end method

.method private b()Lee/cyber/smartid/tse/inter/TSEListener;
    .locals 1

    .line 2
    new-instance v0, Lee/cyber/smartid/manager/impl/TSEListenerToServiceListenerMapperImpl$2;

    invoke-direct {v0, p0}, Lee/cyber/smartid/manager/impl/TSEListenerToServiceListenerMapperImpl$2;-><init>(Lee/cyber/smartid/manager/impl/TSEListenerToServiceListenerMapperImpl;)V

    return-object v0
.end method

.method static synthetic c(Lee/cyber/smartid/manager/impl/TSEListenerToServiceListenerMapperImpl;)Lee/cyber/smartid/tse/SmartIdTSE;
    .locals 0

    .line 1
    iget-object p0, p0, Lee/cyber/smartid/manager/impl/TSEListenerToServiceListenerMapperImpl;->c:Lee/cyber/smartid/tse/SmartIdTSE;

    return-object p0
.end method

.method private c()Lee/cyber/smartid/tse/inter/TSEListener;
    .locals 1

    .line 2
    new-instance v0, Lee/cyber/smartid/manager/impl/TSEListenerToServiceListenerMapperImpl$3;

    invoke-direct {v0, p0}, Lee/cyber/smartid/manager/impl/TSEListenerToServiceListenerMapperImpl$3;-><init>(Lee/cyber/smartid/manager/impl/TSEListenerToServiceListenerMapperImpl;)V

    return-object v0
.end method

.method static synthetic d(Lee/cyber/smartid/manager/impl/TSEListenerToServiceListenerMapperImpl;)Lee/cyber/smartid/util/Log;
    .locals 0

    .line 1
    iget-object p0, p0, Lee/cyber/smartid/manager/impl/TSEListenerToServiceListenerMapperImpl;->d:Lee/cyber/smartid/util/Log;

    return-object p0
.end method


# virtual methods
.method public map(Lee/cyber/smartid/inter/ServiceListener;)Lee/cyber/smartid/tse/inter/TSEListener;
    .locals 1

    .line 1
    instance-of v0, p1, Lee/cyber/smartid/inter/ConfirmTransactionListener;

    if-eqz v0, :cond_0

    .line 2
    invoke-direct {p0}, Lee/cyber/smartid/manager/impl/TSEListenerToServiceListenerMapperImpl;->c()Lee/cyber/smartid/tse/inter/TSEListener;

    move-result-object p1

    return-object p1

    .line 3
    :cond_0
    instance-of v0, p1, Lee/cyber/smartid/inter/GenerateKeysListener;

    if-eqz v0, :cond_1

    .line 4
    invoke-direct {p0}, Lee/cyber/smartid/manager/impl/TSEListenerToServiceListenerMapperImpl;->a()Lee/cyber/smartid/tse/inter/TSEListener;

    move-result-object p1

    return-object p1

    .line 5
    :cond_1
    instance-of p1, p1, Lee/cyber/smartid/inter/ValidatePinListener;

    if-eqz p1, :cond_2

    .line 6
    invoke-direct {p0}, Lee/cyber/smartid/manager/impl/TSEListenerToServiceListenerMapperImpl;->b()Lee/cyber/smartid/tse/inter/TSEListener;

    move-result-object p1

    return-object p1

    :cond_2
    const/4 p1, 0x0

    return-object p1
.end method
