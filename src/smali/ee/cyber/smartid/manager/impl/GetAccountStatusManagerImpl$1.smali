.class Lee/cyber/smartid/manager/impl/GetAccountStatusManagerImpl$1;
.super Lee/cyber/smartid/tse/network/RPCCallback;
.source "GetAccountStatusManagerImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lee/cyber/smartid/manager/impl/GetAccountStatusManagerImpl;->getAccountStatus(Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/inter/GetAccountStatusListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lee/cyber/smartid/tse/network/RPCCallback<",
        "Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCResponse<",
        "Lee/cyber/smartid/tse/dto/ResultWithRawJson<",
        "Lee/cyber/smartid/dto/jsonrpc/result/GetAccountStatusResult;",
        ">;>;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:Lee/cyber/smartid/manager/impl/GetAccountStatusManagerImpl;


# direct methods
.method constructor <init>(Lee/cyber/smartid/manager/impl/GetAccountStatusManagerImpl;Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCRequest;Lee/cyber/smartid/tse/SmartIdTSE;Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lee/cyber/smartid/manager/impl/GetAccountStatusManagerImpl$1;->b:Lee/cyber/smartid/manager/impl/GetAccountStatusManagerImpl;

    iput-object p4, p0, Lee/cyber/smartid/manager/impl/GetAccountStatusManagerImpl$1;->a:Ljava/lang/String;

    invoke-direct {p0, p2, p3}, Lee/cyber/smartid/tse/network/RPCCallback;-><init>(Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCRequest;Lee/cyber/smartid/tse/SmartIdTSE;)V

    return-void
.end method


# virtual methods
.method public onFailure(Lc/b;Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCError;Ljava/lang/Throwable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lc/b<",
            "Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCResponse<",
            "Lee/cyber/smartid/tse/dto/ResultWithRawJson<",
            "Lee/cyber/smartid/dto/jsonrpc/result/GetAccountStatusResult;",
            ">;>;>;",
            "Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCError;",
            "Ljava/lang/Throwable;",
            ")V"
        }
    .end annotation

    .line 1
    iget-object p1, p0, Lee/cyber/smartid/manager/impl/GetAccountStatusManagerImpl$1;->b:Lee/cyber/smartid/manager/impl/GetAccountStatusManagerImpl;

    invoke-static {p1}, Lee/cyber/smartid/manager/impl/GetAccountStatusManagerImpl;->b(Lee/cyber/smartid/manager/impl/GetAccountStatusManagerImpl;)Lee/cyber/smartid/inter/ServiceAccess;

    move-result-object p1

    new-instance v0, Lee/cyber/smartid/manager/impl/GetAccountStatusManagerImpl$1$1;

    invoke-direct {v0, p0, p2, p3}, Lee/cyber/smartid/manager/impl/GetAccountStatusManagerImpl$1$1;-><init>(Lee/cyber/smartid/manager/impl/GetAccountStatusManagerImpl$1;Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCError;Ljava/lang/Throwable;)V

    invoke-interface {p1, v0}, Lee/cyber/smartid/inter/ServiceAccess;->notifyUI(Ljava/lang/Runnable;)V

    return-void
.end method

.method public onSuccess(Lc/b;Lc/r;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lc/b<",
            "Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCResponse<",
            "Lee/cyber/smartid/tse/dto/ResultWithRawJson<",
            "Lee/cyber/smartid/dto/jsonrpc/result/GetAccountStatusResult;",
            ">;>;>;",
            "Lc/r<",
            "Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCResponse<",
            "Lee/cyber/smartid/tse/dto/ResultWithRawJson<",
            "Lee/cyber/smartid/dto/jsonrpc/result/GetAccountStatusResult;",
            ">;>;>;)V"
        }
    .end annotation

    .line 1
    iget-object p1, p0, Lee/cyber/smartid/manager/impl/GetAccountStatusManagerImpl$1;->b:Lee/cyber/smartid/manager/impl/GetAccountStatusManagerImpl;

    invoke-static {p1}, Lee/cyber/smartid/manager/impl/GetAccountStatusManagerImpl;->a(Lee/cyber/smartid/manager/impl/GetAccountStatusManagerImpl;)Lee/cyber/smartid/util/Log;

    move-result-object p1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "getAccountStatus - onSuccess: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Lc/r;->e()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    .line 2
    invoke-virtual {p2}, Lc/r;->e()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCResponse;

    invoke-virtual {p1}, Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCResponse;->getResult()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lee/cyber/smartid/tse/dto/ResultWithRawJson;

    invoke-virtual {p1}, Lee/cyber/smartid/tse/dto/ResultWithRawJson;->getDeserialized()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lee/cyber/smartid/dto/jsonrpc/result/GetAccountStatusResult;

    invoke-virtual {p1}, Lee/cyber/smartid/dto/jsonrpc/result/GetAccountStatusResult;->getAccountUUID()Ljava/lang/String;

    move-result-object v3

    .line 3
    invoke-virtual {p2}, Lc/r;->e()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCResponse;

    invoke-virtual {p1}, Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCResponse;->getResult()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lee/cyber/smartid/tse/dto/ResultWithRawJson;

    invoke-virtual {p1}, Lee/cyber/smartid/tse/dto/ResultWithRawJson;->getDeserialized()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lee/cyber/smartid/dto/jsonrpc/result/GetAccountStatusResult;

    invoke-virtual {p1}, Lee/cyber/smartid/dto/jsonrpc/result/GetAccountStatusResult;->getKeys()Ljava/util/ArrayList;

    move-result-object v8

    .line 4
    iget-object p1, p0, Lee/cyber/smartid/manager/impl/GetAccountStatusManagerImpl$1;->b:Lee/cyber/smartid/manager/impl/GetAccountStatusManagerImpl;

    invoke-static {p1}, Lee/cyber/smartid/manager/impl/GetAccountStatusManagerImpl;->b(Lee/cyber/smartid/manager/impl/GetAccountStatusManagerImpl;)Lee/cyber/smartid/inter/ServiceAccess;

    move-result-object p1

    invoke-interface {p1}, Lee/cyber/smartid/inter/ServiceAccess;->getStoredAccountManager()Lee/cyber/smartid/manager/inter/StoredAccountManager;

    move-result-object p1

    invoke-interface {p1, v3, v8}, Lee/cyber/smartid/manager/inter/StoredAccountManager;->addAndStoreKeyUUIDsToAccountStateIfNeeded(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 5
    iget-object p1, p0, Lee/cyber/smartid/manager/impl/GetAccountStatusManagerImpl$1;->b:Lee/cyber/smartid/manager/impl/GetAccountStatusManagerImpl;

    invoke-static {p1}, Lee/cyber/smartid/manager/impl/GetAccountStatusManagerImpl;->c(Lee/cyber/smartid/manager/impl/GetAccountStatusManagerImpl;)Lee/cyber/smartid/inter/ListenerAccess;

    move-result-object p1

    iget-object v0, p0, Lee/cyber/smartid/manager/impl/GetAccountStatusManagerImpl$1;->a:Ljava/lang/String;

    const-class v1, Lee/cyber/smartid/inter/GetAccountStatusListener;

    const/4 v2, 0x1

    invoke-interface {p1, v0, v2, v1}, Lee/cyber/smartid/inter/ListenerAccess;->getListener(Ljava/lang/String;ZLjava/lang/Class;)Lee/cyber/smartid/inter/ServiceListener;

    move-result-object p1

    check-cast p1, Lee/cyber/smartid/inter/GetAccountStatusListener;

    if-nez p1, :cond_0

    return-void

    .line 6
    :cond_0
    iget-object v9, p0, Lee/cyber/smartid/manager/impl/GetAccountStatusManagerImpl$1;->a:Ljava/lang/String;

    new-instance v10, Lee/cyber/smartid/dto/jsonrpc/resp/GetAccountStatusResp;

    invoke-virtual {p2}, Lc/r;->e()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCResponse;

    invoke-virtual {v0}, Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCResponse;->getResult()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lee/cyber/smartid/tse/dto/ResultWithRawJson;

    invoke-virtual {v0}, Lee/cyber/smartid/tse/dto/ResultWithRawJson;->getSerialized()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2}, Lc/r;->e()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCResponse;

    invoke-virtual {v0}, Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCResponse;->getResult()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lee/cyber/smartid/tse/dto/ResultWithRawJson;

    invoke-virtual {v0}, Lee/cyber/smartid/tse/dto/ResultWithRawJson;->getDeserialized()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lee/cyber/smartid/dto/jsonrpc/result/GetAccountStatusResult;

    invoke-virtual {v0}, Lee/cyber/smartid/dto/jsonrpc/result/GetAccountStatusResult;->getNumberOfAccounts()J

    move-result-wide v4

    invoke-virtual {p2}, Lc/r;->e()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCResponse;

    invoke-virtual {v0}, Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCResponse;->getResult()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lee/cyber/smartid/tse/dto/ResultWithRawJson;

    invoke-virtual {v0}, Lee/cyber/smartid/tse/dto/ResultWithRawJson;->getDeserialized()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lee/cyber/smartid/dto/jsonrpc/result/GetAccountStatusResult;

    invoke-virtual {v0}, Lee/cyber/smartid/dto/jsonrpc/result/GetAccountStatusResult;->getStatus()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p2}, Lc/r;->e()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCResponse;

    invoke-virtual {p2}, Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCResponse;->getResult()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lee/cyber/smartid/tse/dto/ResultWithRawJson;

    invoke-virtual {p2}, Lee/cyber/smartid/tse/dto/ResultWithRawJson;->getDeserialized()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lee/cyber/smartid/dto/jsonrpc/result/GetAccountStatusResult;

    invoke-virtual {p2}, Lee/cyber/smartid/dto/jsonrpc/result/GetAccountStatusResult;->getRegistration()Lee/cyber/smartid/dto/jsonrpc/GetAccountStatusRegistration;

    move-result-object v7

    move-object v0, v10

    move-object v1, v9

    invoke-direct/range {v0 .. v8}, Lee/cyber/smartid/dto/jsonrpc/resp/GetAccountStatusResp;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Lee/cyber/smartid/dto/jsonrpc/GetAccountStatusRegistration;Ljava/util/ArrayList;)V

    invoke-interface {p1, v9, v10}, Lee/cyber/smartid/inter/GetAccountStatusListener;->onGetAccountStatusSuccess(Ljava/lang/String;Lee/cyber/smartid/dto/jsonrpc/resp/GetAccountStatusResp;)V

    return-void
.end method

.method public onValidate(Lc/b;Lc/r;)Z
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lc/b<",
            "Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCResponse<",
            "Lee/cyber/smartid/tse/dto/ResultWithRawJson<",
            "Lee/cyber/smartid/dto/jsonrpc/result/GetAccountStatusResult;",
            ">;>;>;",
            "Lc/r<",
            "Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCResponse<",
            "Lee/cyber/smartid/tse/dto/ResultWithRawJson<",
            "Lee/cyber/smartid/dto/jsonrpc/result/GetAccountStatusResult;",
            ">;>;>;)Z"
        }
    .end annotation

    if-eqz p2, :cond_0

    .line 1
    invoke-virtual {p2}, Lc/r;->e()Ljava/lang/Object;

    move-result-object p1

    if-eqz p1, :cond_0

    invoke-virtual {p2}, Lc/r;->e()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCResponse;

    invoke-virtual {p1}, Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCResponse;->getResult()Ljava/lang/Object;

    move-result-object p1

    if-eqz p1, :cond_0

    invoke-virtual {p2}, Lc/r;->e()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCResponse;

    invoke-virtual {p1}, Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCResponse;->getResult()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lee/cyber/smartid/tse/dto/ResultWithRawJson;

    invoke-virtual {p1}, Lee/cyber/smartid/tse/dto/ResultWithRawJson;->isValid()Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method
