.class public Lee/cyber/smartid/manager/impl/VerificationCodeEvaluatorImpl;
.super Ljava/lang/Object;
.source "VerificationCodeEvaluatorImpl.java"

# interfaces
.implements Lee/cyber/smartid/manager/inter/VerificationCodeEvaluator;


# static fields
.field private static volatile a:Lee/cyber/smartid/manager/impl/VerificationCodeEvaluatorImpl;


# instance fields
.field private final b:Lorg/d/a;


# direct methods
.method private constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    invoke-static {}, Lorg/d/a/c;->a()Lorg/d/a;

    move-result-object v0

    iput-object v0, p0, Lee/cyber/smartid/manager/impl/VerificationCodeEvaluatorImpl;->b:Lorg/d/a;

    return-void
.end method

.method private a(ILjava/lang/String;Ljava/lang/String;)Z
    .locals 0

    .line 7
    invoke-virtual {p0, p1, p3, p2}, Lee/cyber/smartid/manager/impl/VerificationCodeEvaluatorImpl;->isValidVerificationCodePairBasedOnLevenshteinDistance(ILjava/lang/String;Ljava/lang/String;)Z

    move-result p1

    xor-int/lit8 p1, p1, 0x1

    return p1
.end method

.method public static getInstance()Lee/cyber/smartid/manager/impl/VerificationCodeEvaluatorImpl;
    .locals 2

    .line 1
    sget-object v0, Lee/cyber/smartid/manager/impl/VerificationCodeEvaluatorImpl;->a:Lee/cyber/smartid/manager/impl/VerificationCodeEvaluatorImpl;

    if-nez v0, :cond_0

    .line 2
    const-class v0, Lee/cyber/smartid/manager/impl/VerificationCodeEvaluatorImpl;

    monitor-enter v0

    .line 3
    :try_start_0
    new-instance v1, Lee/cyber/smartid/manager/impl/VerificationCodeEvaluatorImpl;

    invoke-direct {v1}, Lee/cyber/smartid/manager/impl/VerificationCodeEvaluatorImpl;-><init>()V

    sput-object v1, Lee/cyber/smartid/manager/impl/VerificationCodeEvaluatorImpl;->a:Lee/cyber/smartid/manager/impl/VerificationCodeEvaluatorImpl;

    .line 4
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 5
    :cond_0
    :goto_0
    sget-object v0, Lee/cyber/smartid/manager/impl/VerificationCodeEvaluatorImpl;->a:Lee/cyber/smartid/manager/impl/VerificationCodeEvaluatorImpl;

    return-object v0
.end method

.method public static getTestInstance()Lee/cyber/smartid/manager/impl/VerificationCodeEvaluatorImpl;
    .locals 1

    .line 1
    new-instance v0, Lee/cyber/smartid/manager/impl/VerificationCodeEvaluatorImpl;

    invoke-direct {v0}, Lee/cyber/smartid/manager/impl/VerificationCodeEvaluatorImpl;-><init>()V

    return-object v0
.end method


# virtual methods
.method a(Ljava/lang/String;Ljava/lang/String;)I
    .locals 1

    .line 1
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 p1, 0x0

    return p1

    .line 2
    :cond_0
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 3
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result p1

    return p1

    .line 4
    :cond_1
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 5
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result p1

    return p1

    .line 6
    :cond_2
    iget-object v0, p0, Lee/cyber/smartid/manager/impl/VerificationCodeEvaluatorImpl;->b:Lorg/d/a;

    invoke-interface {v0, p1, p2}, Lorg/d/a;->a(Ljava/lang/String;Ljava/lang/String;)F

    move-result p1

    float-to-int p1, p1

    return p1
.end method

.method public isValidVerificationCodeCandidate(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;I)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;I)Z"
        }
    .end annotation

    .line 1
    invoke-direct {p0, p4, p1, p2}, Lee/cyber/smartid/manager/impl/VerificationCodeEvaluatorImpl;->a(ILjava/lang/String;Ljava/lang/String;)Z

    move-result p2

    const/4 v0, 0x0

    if-eqz p2, :cond_0

    return v0

    .line 2
    :cond_0
    invoke-virtual {p3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :cond_1
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result p3

    if-eqz p3, :cond_2

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Ljava/lang/String;

    .line 3
    invoke-direct {p0, p4, p1, p3}, Lee/cyber/smartid/manager/impl/VerificationCodeEvaluatorImpl;->a(ILjava/lang/String;Ljava/lang/String;)Z

    move-result p3

    if-eqz p3, :cond_1

    return v0

    :cond_2
    const/4 p1, 0x1

    return p1
.end method

.method public isValidVerificationCodePairBasedOnLevenshteinDistance(ILjava/lang/String;Ljava/lang/String;)Z
    .locals 0

    .line 1
    invoke-virtual {p0, p2, p3}, Lee/cyber/smartid/manager/impl/VerificationCodeEvaluatorImpl;->a(Ljava/lang/String;Ljava/lang/String;)I

    move-result p2

    if-lt p2, p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method
