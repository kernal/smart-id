.class Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$19;
.super Lee/cyber/smartid/tse/network/RPCCallback;
.source "TransactionAndRPRequestManagerImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;->confirmRPRequest(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/inter/ConfirmRPRequestListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lee/cyber/smartid/tse/network/RPCCallback<",
        "Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCResponse<",
        "Lee/cyber/smartid/dto/jsonrpc/result/ConfirmRPRequestResult;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:Ljava/lang/String;

.field final synthetic c:Ljava/lang/String;

.field final synthetic d:Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;


# direct methods
.method constructor <init>(Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCRequest;Lee/cyber/smartid/tse/SmartIdTSE;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$19;->d:Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;

    iput-object p4, p0, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$19;->a:Ljava/lang/String;

    iput-object p5, p0, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$19;->b:Ljava/lang/String;

    iput-object p6, p0, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$19;->c:Ljava/lang/String;

    invoke-direct {p0, p2, p3}, Lee/cyber/smartid/tse/network/RPCCallback;-><init>(Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCRequest;Lee/cyber/smartid/tse/SmartIdTSE;)V

    return-void
.end method


# virtual methods
.method public onFailure(Lc/b;Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCError;Ljava/lang/Throwable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lc/b<",
            "Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCResponse<",
            "Lee/cyber/smartid/dto/jsonrpc/result/ConfirmRPRequestResult;",
            ">;>;",
            "Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCError;",
            "Ljava/lang/Throwable;",
            ")V"
        }
    .end annotation

    .line 1
    iget-object p1, p0, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$19;->d:Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;

    invoke-static {p1}, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;->a(Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;)Lee/cyber/smartid/inter/ServiceAccess;

    move-result-object p1

    new-instance v0, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$19$1;

    invoke-direct {v0, p0, p2, p3}, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$19$1;-><init>(Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$19;Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCError;Ljava/lang/Throwable;)V

    invoke-interface {p1, v0}, Lee/cyber/smartid/inter/ServiceAccess;->notifyUI(Ljava/lang/Runnable;)V

    return-void
.end method

.method public onSuccess(Lc/b;Lc/r;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lc/b<",
            "Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCResponse<",
            "Lee/cyber/smartid/dto/jsonrpc/result/ConfirmRPRequestResult;",
            ">;>;",
            "Lc/r<",
            "Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCResponse<",
            "Lee/cyber/smartid/dto/jsonrpc/result/ConfirmRPRequestResult;",
            ">;>;)V"
        }
    .end annotation

    .line 1
    iget-object p1, p0, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$19;->d:Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;

    invoke-static {p1}, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;->c(Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;)Lee/cyber/smartid/inter/ListenerAccess;

    move-result-object p1

    iget-object p2, p0, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$19;->a:Ljava/lang/String;

    const-class v0, Lee/cyber/smartid/inter/ConfirmRPRequestListener;

    const/4 v1, 0x1

    invoke-interface {p1, p2, v1, v0}, Lee/cyber/smartid/inter/ListenerAccess;->getListener(Ljava/lang/String;ZLjava/lang/Class;)Lee/cyber/smartid/inter/ServiceListener;

    move-result-object p1

    check-cast p1, Lee/cyber/smartid/inter/ConfirmRPRequestListener;

    .line 2
    iget-object p2, p0, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$19;->d:Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;

    invoke-static {p2}, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;->b(Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;)Lee/cyber/smartid/util/Log;

    move-result-object p2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "confirmRPRequest - onResponse, listener "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    if-nez p1, :cond_0

    return-void

    .line 3
    :cond_0
    iget-object p2, p0, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$19;->a:Ljava/lang/String;

    new-instance v0, Lee/cyber/smartid/dto/jsonrpc/resp/ConfirmRPRequestResp;

    iget-object v1, p0, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$19;->b:Ljava/lang/String;

    iget-object v2, p0, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$19;->c:Ljava/lang/String;

    invoke-direct {v0, p2, v1, v2}, Lee/cyber/smartid/dto/jsonrpc/resp/ConfirmRPRequestResp;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {p1, p2, v0}, Lee/cyber/smartid/inter/ConfirmRPRequestListener;->onConfirmRPRequestSuccess(Ljava/lang/String;Lee/cyber/smartid/dto/jsonrpc/resp/ConfirmRPRequestResp;)V

    return-void
.end method

.method public onValidate(Lc/b;Lc/r;)Z
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lc/b<",
            "Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCResponse<",
            "Lee/cyber/smartid/dto/jsonrpc/result/ConfirmRPRequestResult;",
            ">;>;",
            "Lc/r<",
            "Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCResponse<",
            "Lee/cyber/smartid/dto/jsonrpc/result/ConfirmRPRequestResult;",
            ">;>;)Z"
        }
    .end annotation

    .line 1
    invoke-static {p2}, Lee/cyber/smartid/util/Util;->validateResponse(Lc/r;)Z

    move-result p1

    return p1
.end method
