.class Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$1$2;
.super Ljava/lang/Object;
.source "TransactionAndRPRequestManagerImpl.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$1;->onSuccess(Lc/b;Lc/r;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lee/cyber/smartid/cryptolib/dto/StorageException;

.field final synthetic b:Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$1;


# direct methods
.method constructor <init>(Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$1;Lee/cyber/smartid/cryptolib/dto/StorageException;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$1$2;->b:Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$1;

    iput-object p2, p0, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$1$2;->a:Lee/cyber/smartid/cryptolib/dto/StorageException;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$1$2;->b:Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$1;

    iget-object v0, v0, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$1;->c:Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;

    invoke-static {v0}, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;->c(Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;)Lee/cyber/smartid/inter/ListenerAccess;

    move-result-object v0

    iget-object v1, p0, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$1$2;->b:Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$1;

    iget-object v1, v1, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$1;->c:Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;

    invoke-static {v1}, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;->c(Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;)Lee/cyber/smartid/inter/ListenerAccess;

    move-result-object v1

    iget-object v2, p0, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$1$2;->b:Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$1;

    iget-object v2, v2, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$1;->a:Ljava/lang/String;

    const-class v3, Lee/cyber/smartid/inter/GetPendingOperationListener;

    const/4 v4, 0x1

    invoke-interface {v1, v2, v4, v3}, Lee/cyber/smartid/inter/ListenerAccess;->getListener(Ljava/lang/String;ZLjava/lang/Class;)Lee/cyber/smartid/inter/ServiceListener;

    move-result-object v1

    iget-object v2, p0, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$1$2;->b:Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$1;

    iget-object v3, v2, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$1;->a:Ljava/lang/String;

    iget-object v2, v2, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$1;->c:Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;

    invoke-static {v2}, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;->a(Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;)Lee/cyber/smartid/inter/ServiceAccess;

    move-result-object v2

    invoke-interface {v2}, Lee/cyber/smartid/inter/ServiceAccess;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    iget-object v4, p0, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$1$2;->b:Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$1;

    iget-object v4, v4, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$1;->c:Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;

    invoke-static {v4}, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;->d(Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;)Lee/cyber/smartid/tse/inter/WallClock;

    move-result-object v4

    iget-object v5, p0, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$1$2;->a:Lee/cyber/smartid/cryptolib/dto/StorageException;

    invoke-static {v2, v4, v5}, Lee/cyber/smartid/dto/SmartIdError;->from(Landroid/content/Context;Lee/cyber/smartid/tse/inter/WallClock;Ljava/lang/Throwable;)Lee/cyber/smartid/dto/SmartIdError;

    move-result-object v2

    invoke-interface {v0, v1, v3, v2}, Lee/cyber/smartid/inter/ListenerAccess;->notifyError(Lee/cyber/smartid/inter/ServiceListener;Ljava/lang/String;Lee/cyber/smartid/dto/SmartIdError;)V

    return-void
.end method
