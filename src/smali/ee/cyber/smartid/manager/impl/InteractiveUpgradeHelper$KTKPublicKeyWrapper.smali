.class Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper$KTKPublicKeyWrapper;
.super Ljava/lang/Object;
.source "InteractiveUpgradeHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "KTKPublicKeyWrapper"
.end annotation


# instance fields
.field a:Ljava/lang/String;

.field b:Ljava/lang/String;

.field c:Ljava/lang/String;

.field d:Ljava/lang/String;

.field e:Ljava/lang/String;

.field final synthetic f:Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;


# direct methods
.method constructor <init>(Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper$KTKPublicKeyWrapper;->f:Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p2, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper$KTKPublicKeyWrapper;->a:Ljava/lang/String;

    .line 3
    iput-object p3, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper$KTKPublicKeyWrapper;->b:Ljava/lang/String;

    .line 4
    iput-object p4, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper$KTKPublicKeyWrapper;->c:Ljava/lang/String;

    return-void
.end method

.method constructor <init>(Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 5
    iput-object p1, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper$KTKPublicKeyWrapper;->f:Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 6
    iput-object p2, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper$KTKPublicKeyWrapper;->a:Ljava/lang/String;

    .line 7
    iput-object p3, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper$KTKPublicKeyWrapper;->b:Ljava/lang/String;

    .line 8
    iput-object p4, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper$KTKPublicKeyWrapper;->d:Ljava/lang/String;

    .line 9
    iput-object p5, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper$KTKPublicKeyWrapper;->e:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method a()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper$KTKPublicKeyWrapper;->e:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper$KTKPublicKeyWrapper;->d:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method b()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper$KTKPublicKeyWrapper;->c:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method
