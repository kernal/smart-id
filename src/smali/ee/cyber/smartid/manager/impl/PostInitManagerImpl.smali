.class public Lee/cyber/smartid/manager/impl/PostInitManagerImpl;
.super Ljava/lang/Object;
.source "PostInitManagerImpl.java"

# interfaces
.implements Lee/cyber/smartid/manager/inter/PostInitManager;


# static fields
.field private static volatile a:Lee/cyber/smartid/manager/impl/PostInitManagerImpl;


# instance fields
.field private final b:Lee/cyber/smartid/inter/ServiceAccess;

.field private c:Lee/cyber/smartid/util/Log;


# direct methods
.method private constructor <init>(Lee/cyber/smartid/inter/ServiceAccess;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    invoke-static {p0}, Lee/cyber/smartid/util/Log;->getInstance(Ljava/lang/Object;)Lee/cyber/smartid/util/Log;

    move-result-object v0

    iput-object v0, p0, Lee/cyber/smartid/manager/impl/PostInitManagerImpl;->c:Lee/cyber/smartid/util/Log;

    .line 3
    iput-object p1, p0, Lee/cyber/smartid/manager/impl/PostInitManagerImpl;->b:Lee/cyber/smartid/inter/ServiceAccess;

    return-void
.end method

.method public static getInstance(Lee/cyber/smartid/inter/ServiceAccess;)Lee/cyber/smartid/manager/impl/PostInitManagerImpl;
    .locals 2

    .line 1
    sget-object v0, Lee/cyber/smartid/manager/impl/PostInitManagerImpl;->a:Lee/cyber/smartid/manager/impl/PostInitManagerImpl;

    if-nez v0, :cond_0

    .line 2
    const-class v0, Lee/cyber/smartid/manager/impl/PostInitManagerImpl;

    monitor-enter v0

    .line 3
    :try_start_0
    new-instance v1, Lee/cyber/smartid/manager/impl/PostInitManagerImpl;

    invoke-direct {v1, p0}, Lee/cyber/smartid/manager/impl/PostInitManagerImpl;-><init>(Lee/cyber/smartid/inter/ServiceAccess;)V

    sput-object v1, Lee/cyber/smartid/manager/impl/PostInitManagerImpl;->a:Lee/cyber/smartid/manager/impl/PostInitManagerImpl;

    .line 4
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception p0

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p0

    .line 5
    :cond_0
    :goto_0
    sget-object p0, Lee/cyber/smartid/manager/impl/PostInitManagerImpl;->a:Lee/cyber/smartid/manager/impl/PostInitManagerImpl;

    return-object p0
.end method


# virtual methods
.method public executePostInitServiceTasks()V
    .locals 3

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/manager/impl/PostInitManagerImpl;->b:Lee/cyber/smartid/inter/ServiceAccess;

    invoke-interface {v0}, Lee/cyber/smartid/inter/ServiceAccess;->acquireWakeLockForService()V

    .line 2
    :try_start_0
    iget-object v0, p0, Lee/cyber/smartid/manager/impl/PostInitManagerImpl;->c:Lee/cyber/smartid/util/Log;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const-string v1, "executePostInitServiceTasks called"

    :try_start_1
    invoke-virtual {v0, v1}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    .line 3
    iget-object v0, p0, Lee/cyber/smartid/manager/impl/PostInitManagerImpl;->b:Lee/cyber/smartid/inter/ServiceAccess;

    invoke-interface {v0}, Lee/cyber/smartid/inter/ServiceAccess;->getSafetyNetManager()Lee/cyber/smartid/manager/inter/SafetyNetManager;

    move-result-object v0

    invoke-interface {v0}, Lee/cyber/smartid/manager/inter/SafetyNetManager;->startPostInitSafetyNetCheckIfNeeded()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 4
    iget-object v0, p0, Lee/cyber/smartid/manager/impl/PostInitManagerImpl;->c:Lee/cyber/smartid/util/Log;
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    const-string v1, "executePostInitServiceTasks - Starting SafetyNet. The startUpdateDeviceAfterPostInitIfNeeded will follow after that."

    :try_start_2
    invoke-virtual {v0, v1}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    goto :goto_0

    .line 5
    :cond_0
    iget-object v0, p0, Lee/cyber/smartid/manager/impl/PostInitManagerImpl;->c:Lee/cyber/smartid/util/Log;
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    const-string v1, "executePostInitServiceTasks - Calling startUpdateDeviceAfterPostInitIfNeeded directly .."

    :try_start_3
    invoke-virtual {v0, v1}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    .line 6
    iget-object v0, p0, Lee/cyber/smartid/manager/impl/PostInitManagerImpl;->b:Lee/cyber/smartid/inter/ServiceAccess;

    invoke-interface {v0}, Lee/cyber/smartid/inter/ServiceAccess;->updateDeviceForPostInitIfNeeded()V

    .line 7
    :goto_0
    iget-object v0, p0, Lee/cyber/smartid/manager/impl/PostInitManagerImpl;->b:Lee/cyber/smartid/inter/ServiceAccess;

    invoke-interface {v0}, Lee/cyber/smartid/inter/ServiceAccess;->getTransactionCacheManager()Lee/cyber/smartid/manager/inter/TransactionCacheManager;

    move-result-object v0

    invoke-interface {v0}, Lee/cyber/smartid/manager/inter/TransactionCacheManager;->removeExpiredTransactionsFromCache()V

    .line 8
    iget-object v0, p0, Lee/cyber/smartid/manager/impl/PostInitManagerImpl;->b:Lee/cyber/smartid/inter/ServiceAccess;

    invoke-interface {v0}, Lee/cyber/smartid/inter/ServiceAccess;->getTransactionCacheManager()Lee/cyber/smartid/manager/inter/TransactionCacheManager;

    move-result-object v0

    invoke-interface {v0}, Lee/cyber/smartid/manager/inter/TransactionCacheManager;->setNextRemoveExpiredTransactionsAlarm()V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    goto :goto_2

    :catch_0
    move-exception v0

    .line 9
    :try_start_4
    iget-object v1, p0, Lee/cyber/smartid/manager/impl/PostInitManagerImpl;->c:Lee/cyber/smartid/util/Log;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    const-string v2, "executePostInitServiceTasks"

    :try_start_5
    invoke-virtual {v1, v2, v0}, Lee/cyber/smartid/util/Log;->e(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 10
    :goto_1
    iget-object v0, p0, Lee/cyber/smartid/manager/impl/PostInitManagerImpl;->b:Lee/cyber/smartid/inter/ServiceAccess;

    invoke-interface {v0}, Lee/cyber/smartid/inter/ServiceAccess;->releaseWakeLockForService()V

    return-void

    :goto_2
    iget-object v1, p0, Lee/cyber/smartid/manager/impl/PostInitManagerImpl;->b:Lee/cyber/smartid/inter/ServiceAccess;

    invoke-interface {v1}, Lee/cyber/smartid/inter/ServiceAccess;->releaseWakeLockForService()V

    throw v0
.end method
