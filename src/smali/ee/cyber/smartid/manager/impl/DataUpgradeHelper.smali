.class public Lee/cyber/smartid/manager/impl/DataUpgradeHelper;
.super Ljava/lang/Object;
.source "DataUpgradeHelper.java"


# static fields
.field private static final a:Lee/cyber/smartid/util/Log;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1
    const-class v0, Lee/cyber/smartid/manager/impl/DataUpgradeHelper;

    invoke-static {v0}, Lee/cyber/smartid/util/Log;->getInstance(Ljava/lang/Class;)Lee/cyber/smartid/util/Log;

    move-result-object v0

    sput-object v0, Lee/cyber/smartid/manager/impl/DataUpgradeHelper;->a:Lee/cyber/smartid/util/Log;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static a(Lee/cyber/smartid/dto/upgrade/v6/PreV6AccountState;)Lee/cyber/smartid/dto/upgrade/v6/V6AccountState;
    .locals 0

    .line 25
    invoke-static {p0}, Lee/cyber/smartid/dto/upgrade/v6/V6AccountState;->fromPreV6(Lee/cyber/smartid/dto/upgrade/v6/PreV6AccountState;)Lee/cyber/smartid/dto/upgrade/v6/V6AccountState;

    move-result-object p0

    return-object p0
.end method

.method private static a(Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Lee/cyber/smartid/dto/upgrade/v6/PreV6AccountState;",
            ">;)",
            "Ljava/util/ArrayList<",
            "Lee/cyber/smartid/dto/upgrade/v6/V6AccountState;",
            ">;"
        }
    .end annotation

    .line 21
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 22
    invoke-virtual {p0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lee/cyber/smartid/dto/upgrade/v6/PreV6AccountState;

    if-nez v1, :cond_0

    goto :goto_0

    .line 23
    :cond_0
    sget-object v2, Lee/cyber/smartid/manager/impl/DataUpgradeHelper;->a:Lee/cyber/smartid/util/Log;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onUpgradeToV6: upgrading account "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Lee/cyber/smartid/dto/upgrade/v6/PreV6AccountState;->getAccountUUID()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    .line 24
    invoke-static {v1}, Lee/cyber/smartid/manager/impl/DataUpgradeHelper;->a(Lee/cyber/smartid/dto/upgrade/v6/PreV6AccountState;)Lee/cyber/smartid/dto/upgrade/v6/V6AccountState;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    return-object v0
.end method

.method private static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    const-string v0, "alarm"

    .line 16
    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    if-nez v0, :cond_0

    .line 17
    const-class p0, Lee/cyber/smartid/SmartIdService;

    invoke-static {p0}, Lee/cyber/smartid/util/Log;->getInstance(Ljava/lang/Class;)Lee/cyber/smartid/util/Log;

    move-result-object p0

    const-string p1, "clearAlarmFor: Unable to access alarmManager"

    invoke-virtual {p0, p1}, Lee/cyber/smartid/util/Log;->e(Ljava/lang/String;)V

    return-void

    .line 18
    :cond_0
    invoke-static {p0, p1, p2}, Lee/cyber/smartid/manager/impl/DataUpgradeHelper;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/app/PendingIntent;

    move-result-object p0

    .line 19
    invoke-virtual {v0, p0}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    .line 20
    const-class p0, Lee/cyber/smartid/SmartIdService;

    invoke-static {p0}, Lee/cyber/smartid/util/Log;->getInstance(Ljava/lang/Class;)Lee/cyber/smartid/util/Log;

    move-result-object p0

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "clearAlarmFor - cleared an alarm for "

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    return-void
.end method

.method private static a(Lee/cyber/smartid/cryptolib/CryptoLib;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/dto/jsonrpc/Transaction;Ljava/lang/String;)V
    .locals 13
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lee/cyber/smartid/cryptolib/dto/StorageException;
        }
    .end annotation

    move-object v0, p0

    move v1, p2

    move-object/from16 v2, p3

    move-object/from16 v10, p4

    const/4 v3, 0x3

    .line 5
    new-array v4, v3, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v2, v4, v5

    const/4 v6, 0x1

    aput-object p1, v4, v6

    const/4 v7, 0x2

    aput-object v10, v4, v7

    const-string v8, "ee.cyber.smartid.APP_KEY_STATE_%1$s_%2$s_%3$s"

    invoke-static {v8, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    .line 6
    new-array v4, v3, [Ljava/lang/Object;

    aput-object v2, v4, v5

    aput-object p1, v4, v6

    aput-object v10, v4, v7

    const-string v5, "ee.cyber.smartid.APP_KEY_STATE_META_%1$s_%2$s_%3$s"

    invoke-static {v5, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    .line 7
    new-instance v4, Lee/cyber/smartid/manager/impl/DataUpgradeHelper$5;

    invoke-direct {v4}, Lee/cyber/smartid/manager/impl/DataUpgradeHelper$5;-><init>()V

    .line 8
    invoke-virtual {v4}, Lcom/google/gson/b/a;->getType()Ljava/lang/reflect/Type;

    move-result-object v4

    .line 9
    invoke-virtual {p0, v11, v4}, Lee/cyber/smartid/cryptolib/CryptoLib;->retrieveData(Ljava/lang/String;Ljava/lang/reflect/Type;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lee/cyber/smartid/dto/upgrade/v2/V2KeyState;

    const/4 v5, 0x0

    if-ne v1, v6, :cond_0

    .line 10
    invoke-virtual {v4}, Lee/cyber/smartid/dto/upgrade/v2/V2KeyState;->getOneTimePassword()Ljava/lang/String;

    move-result-object v9

    move-object v3, v11

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    invoke-static/range {v3 .. v9}, Lee/cyber/smartid/dto/upgrade/v2/V2KeyState;->forSign(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/dto/jsonrpc/Transaction;Ljava/lang/String;Ljava/lang/String;)Lee/cyber/smartid/dto/upgrade/v2/V2KeyState;

    move-result-object v5

    .line 11
    invoke-static {v12, v10, v2}, Lee/cyber/smartid/dto/upgrade/v2/V2KeyStateMeta;->forActive(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lee/cyber/smartid/dto/upgrade/v2/V2KeyStateMeta;

    move-result-object v1

    goto :goto_0

    :cond_0
    if-ne v1, v3, :cond_1

    .line 12
    invoke-virtual {v4}, Lee/cyber/smartid/dto/upgrade/v2/V2KeyState;->getOneTimePassword()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v3, p8

    invoke-static {v11, v2, v10, v3, v1}, Lee/cyber/smartid/dto/upgrade/v2/V2KeyState;->forCloneDetection(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lee/cyber/smartid/dto/upgrade/v2/V2KeyState;

    move-result-object v5

    .line 13
    invoke-static {v12, v10, v2}, Lee/cyber/smartid/dto/upgrade/v2/V2KeyStateMeta;->forActive(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lee/cyber/smartid/dto/upgrade/v2/V2KeyStateMeta;

    move-result-object v1

    goto :goto_0

    :cond_1
    move-object v1, v5

    .line 14
    :goto_0
    invoke-virtual {p0, v11, v5}, Lee/cyber/smartid/cryptolib/CryptoLib;->storeData(Ljava/lang/String;Ljava/lang/Object;)V

    .line 15
    invoke-virtual {p0, v12, v1}, Lee/cyber/smartid/cryptolib/CryptoLib;->storeData(Ljava/lang/String;Ljava/lang/Object;)V

    return-void
.end method

.method private static a(Lee/cyber/smartid/cryptolib/CryptoLib;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lee/cyber/smartid/cryptolib/dto/StorageException;
        }
    .end annotation

    const/4 v0, 0x3

    .line 1
    new-array v1, v0, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p2, v1, v2

    const/4 v3, 0x1

    aput-object p1, v1, v3

    const/4 v4, 0x2

    aput-object p3, v1, v4

    const-string v5, "ee.cyber.smartid.APP_KEY_STATE_%1$s_%2$s_%3$s"

    invoke-static {v5, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 2
    new-array v0, v0, [Ljava/lang/Object;

    aput-object p2, v0, v2

    aput-object p1, v0, v3

    aput-object p3, v0, v4

    const-string p1, "ee.cyber.smartid.APP_KEY_STATE_META_%1$s_%2$s_%3$s"

    invoke-static {p1, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    .line 3
    invoke-static {v1, p2, p3, p4}, Lee/cyber/smartid/dto/upgrade/v2/V2KeyState;->forIdle(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lee/cyber/smartid/dto/upgrade/v2/V2KeyState;

    move-result-object p4

    invoke-virtual {p0, v1, p4}, Lee/cyber/smartid/cryptolib/CryptoLib;->storeData(Ljava/lang/String;Ljava/lang/Object;)V

    .line 4
    invoke-static {p1, p2, p3}, Lee/cyber/smartid/dto/upgrade/v2/V2KeyStateMeta;->forIdle(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lee/cyber/smartid/dto/upgrade/v2/V2KeyStateMeta;

    move-result-object p2

    invoke-virtual {p0, p1, p2}, Lee/cyber/smartid/cryptolib/CryptoLib;->storeData(Ljava/lang/String;Ljava/lang/Object;)V

    return-void
.end method

.method private static b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/app/PendingIntent;
    .locals 3

    .line 1
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lee/cyber/smartid/SmartIdService$SmartIdServiceAlarmManager;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2
    invoke-virtual {v0, p1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string p1, "ee.cyber.smartid.EXTRA_ALARM_TARGET_ID"

    .line 3
    invoke-virtual {v0, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 4
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p0

    const/4 p1, 0x0

    const/high16 p2, 0x8000000

    invoke-static {p0, p1, v0, p2}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object p0

    return-object p0
.end method

.method public static onUpgradeToV1(Lee/cyber/smartid/cryptolib/CryptoLib;Ljava/lang/String;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lee/cyber/smartid/cryptolib/dto/StorageException;
        }
    .end annotation

    const/4 v0, 0x1

    .line 1
    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const-string p1, "ee.cyber.smartid.APP_ACCOUNTS_DATA_%1$s"

    invoke-static {p1, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    .line 2
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 3
    new-instance v1, Lee/cyber/smartid/manager/impl/DataUpgradeHelper$1;

    invoke-direct {v1}, Lee/cyber/smartid/manager/impl/DataUpgradeHelper$1;-><init>()V

    .line 4
    invoke-virtual {v1}, Lcom/google/gson/b/a;->getType()Ljava/lang/reflect/Type;

    move-result-object v1

    .line 5
    invoke-virtual {p0, p1, v1}, Lee/cyber/smartid/cryptolib/CryptoLib;->retrieveData(Ljava/lang/String;Ljava/lang/reflect/Type;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/ArrayList;

    if-eqz v1, :cond_1

    .line 6
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_1

    .line 7
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lee/cyber/smartid/dto/upgrade/v1/PreV1Account;

    .line 8
    new-instance v3, Lee/cyber/smartid/dto/upgrade/v1/V1AccountWrapper;

    invoke-virtual {v2}, Lee/cyber/smartid/dto/upgrade/v1/PreV1Account;->getAccountUUID()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v3, v2}, Lee/cyber/smartid/dto/upgrade/v1/V1AccountWrapper;-><init>(Ljava/lang/String;)V

    .line 9
    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 10
    :cond_0
    :try_start_0
    invoke-virtual {p0, p1, v0}, Lee/cyber/smartid/cryptolib/CryptoLib;->storeData(Ljava/lang/String;Ljava/lang/Object;)V
    :try_end_0
    .catch Lee/cyber/smartid/cryptolib/dto/StorageException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception p0

    .line 11
    sget-object p1, Lee/cyber/smartid/manager/impl/DataUpgradeHelper;->a:Lee/cyber/smartid/util/Log;

    const-string v0, "onUpgradeToV1"

    invoke-virtual {p1, v0, p0}, Lee/cyber/smartid/util/Log;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_1
    :goto_1
    return-void
.end method

.method public static onUpgradeToV2(Landroid/content/Context;Lee/cyber/smartid/cryptolib/CryptoLib;Ljava/lang/String;)V
    .locals 23
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lee/cyber/smartid/cryptolib/dto/StorageException;
        }
    .end annotation

    move-object/from16 v1, p0

    move-object/from16 v11, p1

    move-object/from16 v12, p2

    const/4 v13, 0x1

    .line 1
    new-array v0, v13, [Ljava/lang/Object;

    const/4 v14, 0x0

    aput-object v12, v0, v14

    const-string v2, "ee.cyber.smartid.APP_ACCOUNTS_DATA_%1$s"

    invoke-static {v2, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2
    new-instance v2, Lee/cyber/smartid/manager/impl/DataUpgradeHelper$2;

    invoke-direct {v2}, Lee/cyber/smartid/manager/impl/DataUpgradeHelper$2;-><init>()V

    .line 3
    invoke-virtual {v2}, Lcom/google/gson/b/a;->getType()Ljava/lang/reflect/Type;

    move-result-object v2

    .line 4
    invoke-virtual {v11, v0, v2}, Lee/cyber/smartid/cryptolib/CryptoLib;->retrieveData(Ljava/lang/String;Ljava/lang/reflect/Type;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    if-eqz v0, :cond_a

    .line 5
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-nez v2, :cond_0

    goto/16 :goto_7

    .line 6
    :cond_0
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v15

    :cond_1
    invoke-interface {v15}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    const-string v10, "onUpgradeToV2"

    if-eqz v0, :cond_9

    invoke-interface {v15}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object/from16 v16, v0

    check-cast v16, Lee/cyber/smartid/dto/upgrade/v2/PreV2AndV2AccountWrapper;

    .line 7
    sget-object v9, Lee/cyber/smartid/SmartIdService;->ACCOUNT_KEY_TYPES:[Ljava/lang/String;

    array-length v8, v9

    const/4 v7, 0x0

    :goto_0
    if-ge v7, v8, :cond_1

    aget-object v6, v9, v7

    const/4 v0, 0x3

    .line 8
    new-array v2, v0, [Ljava/lang/Object;

    aput-object v6, v2, v14

    invoke-virtual/range {v16 .. v16}, Lee/cyber/smartid/dto/upgrade/v2/PreV2AndV2AccountWrapper;->getAccountUUID()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v13

    const/4 v3, 0x2

    aput-object v12, v2, v3

    const-string v4, "ee.cyber.smartid.APP_KEY_DATA_%1$s_%2$s_%3$s"

    invoke-static {v4, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 9
    new-instance v4, Lee/cyber/smartid/manager/impl/DataUpgradeHelper$3;

    invoke-direct {v4}, Lee/cyber/smartid/manager/impl/DataUpgradeHelper$3;-><init>()V

    .line 10
    invoke-virtual {v4}, Lcom/google/gson/b/a;->getType()Ljava/lang/reflect/Type;

    move-result-object v4

    .line 11
    invoke-virtual {v11, v2, v4}, Lee/cyber/smartid/cryptolib/CryptoLib;->retrieveData(Ljava/lang/String;Ljava/lang/reflect/Type;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lee/cyber/smartid/dto/upgrade/v2/PreV2AndV2Key;

    if-nez v2, :cond_2

    .line 12
    sget-object v0, Lee/cyber/smartid/manager/impl/DataUpgradeHelper;->a:Lee/cyber/smartid/util/Log;

    const-string v2, "onUpgradeToV2 - Key was null .."

    invoke-virtual {v0, v2}, Lee/cyber/smartid/util/Log;->w(Ljava/lang/String;)V

    :goto_1
    move/from16 v22, v7

    move/from16 v18, v8

    move-object/from16 v19, v9

    move-object/from16 v20, v15

    move-object v15, v10

    goto/16 :goto_5

    .line 13
    :cond_2
    new-array v2, v0, [Ljava/lang/Object;

    invoke-virtual/range {v16 .. v16}, Lee/cyber/smartid/dto/upgrade/v2/PreV2AndV2AccountWrapper;->getAccountUUID()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v14

    aput-object v12, v2, v13

    aput-object v6, v2, v3

    const-string v3, "ee.cyber.smartid.APP_KEY_STATE_DATA_%1$s_%2$s_%3$s"

    invoke-static {v3, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 14
    new-instance v2, Lee/cyber/smartid/manager/impl/DataUpgradeHelper$4;

    invoke-direct {v2}, Lee/cyber/smartid/manager/impl/DataUpgradeHelper$4;-><init>()V

    .line 15
    invoke-virtual {v2}, Lcom/google/gson/b/a;->getType()Ljava/lang/reflect/Type;

    move-result-object v2

    .line 16
    invoke-virtual {v11, v5, v2}, Lee/cyber/smartid/cryptolib/CryptoLib;->retrieveData(Ljava/lang/String;Ljava/lang/reflect/Type;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lee/cyber/smartid/dto/upgrade/v2/PreV2KeyState;

    if-nez v2, :cond_3

    .line 17
    sget-object v0, Lee/cyber/smartid/manager/impl/DataUpgradeHelper;->a:Lee/cyber/smartid/util/Log;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onUpgradeToV2 - keyState for "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, " was null .."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    goto :goto_1

    .line 18
    :cond_3
    invoke-virtual {v2}, Lee/cyber/smartid/dto/upgrade/v2/PreV2KeyState;->isInActiveState()Z

    move-result v3

    if-nez v3, :cond_5

    .line 19
    invoke-virtual/range {v16 .. v16}, Lee/cyber/smartid/dto/upgrade/v2/PreV2AndV2AccountWrapper;->getAccountUUID()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2}, Lee/cyber/smartid/dto/upgrade/v2/PreV2KeyState;->getOneTimePassword()Ljava/lang/String;

    move-result-object v2

    invoke-static {v11, v12, v0, v6, v2}, Lee/cyber/smartid/manager/impl/DataUpgradeHelper;->a(Lee/cyber/smartid/cryptolib/CryptoLib;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 20
    sget-object v0, Lee/cyber/smartid/manager/impl/DataUpgradeHelper;->a:Lee/cyber/smartid/util/Log;

    const-string v2, "onUpgradeToV2 - converted an idle KeyState"

    invoke-virtual {v0, v2}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    :cond_4
    :goto_2
    move-object v13, v5

    move/from16 v22, v7

    move/from16 v18, v8

    move-object/from16 v19, v9

    move-object/from16 v20, v15

    move-object v15, v10

    goto/16 :goto_4

    .line 21
    :cond_5
    invoke-virtual {v2}, Lee/cyber/smartid/dto/upgrade/v2/PreV2KeyState;->isInActiveState()Z

    move-result v3

    const-string v4, "ee.cyber.smartid.ACTION_SOLVE_KEY_STATE_"

    if-eqz v3, :cond_6

    invoke-virtual {v2}, Lee/cyber/smartid/dto/upgrade/v2/PreV2KeyState;->isAutoResolvable()Z

    move-result v3

    if-nez v3, :cond_6

    .line 22
    invoke-virtual/range {v16 .. v16}, Lee/cyber/smartid/dto/upgrade/v2/PreV2AndV2AccountWrapper;->getAccountUUID()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2}, Lee/cyber/smartid/dto/upgrade/v2/PreV2KeyState;->getOneTimePassword()Ljava/lang/String;

    move-result-object v2

    invoke-static {v11, v12, v0, v6, v2}, Lee/cyber/smartid/manager/impl/DataUpgradeHelper;->a(Lee/cyber/smartid/cryptolib/CryptoLib;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 23
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0, v5}, Lee/cyber/smartid/manager/impl/DataUpgradeHelper;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 24
    sget-object v0, Lee/cyber/smartid/manager/impl/DataUpgradeHelper;->a:Lee/cyber/smartid/util/Log;

    const-string v2, "onUpgradeToV2 - converted an non-autoresolvable KeyState"

    invoke-virtual {v0, v2}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    goto :goto_2

    .line 25
    :cond_6
    invoke-virtual {v2}, Lee/cyber/smartid/dto/upgrade/v2/PreV2KeyState;->isInActiveState()Z

    move-result v3

    if-eqz v3, :cond_7

    invoke-virtual {v2}, Lee/cyber/smartid/dto/upgrade/v2/PreV2KeyState;->isActiveCancel()Z

    move-result v3

    if-eqz v3, :cond_7

    .line 26
    invoke-virtual/range {v16 .. v16}, Lee/cyber/smartid/dto/upgrade/v2/PreV2AndV2AccountWrapper;->getAccountUUID()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2}, Lee/cyber/smartid/dto/upgrade/v2/PreV2KeyState;->getOneTimePassword()Ljava/lang/String;

    move-result-object v2

    invoke-static {v11, v12, v0, v6, v2}, Lee/cyber/smartid/manager/impl/DataUpgradeHelper;->a(Lee/cyber/smartid/cryptolib/CryptoLib;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 27
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0, v5}, Lee/cyber/smartid/manager/impl/DataUpgradeHelper;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 28
    sget-object v0, Lee/cyber/smartid/manager/impl/DataUpgradeHelper;->a:Lee/cyber/smartid/util/Log;

    const-string v2, "onUpgradeToV2 - converted an active cancel KeyState"

    invoke-virtual {v0, v2}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    goto :goto_2

    .line 29
    :cond_7
    invoke-virtual {v2}, Lee/cyber/smartid/dto/upgrade/v2/PreV2KeyState;->isInActiveState()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 30
    invoke-virtual/range {v16 .. v16}, Lee/cyber/smartid/dto/upgrade/v2/PreV2AndV2AccountWrapper;->getAccountUUID()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2}, Lee/cyber/smartid/dto/upgrade/v2/PreV2KeyState;->getOneTimePassword()Ljava/lang/String;

    move-result-object v0

    invoke-static {v11, v12, v3, v6, v0}, Lee/cyber/smartid/manager/impl/DataUpgradeHelper;->a(Lee/cyber/smartid/cryptolib/CryptoLib;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 31
    invoke-virtual {v2}, Lee/cyber/smartid/dto/upgrade/v2/PreV2KeyState;->getType()I

    move-result v0

    if-ne v0, v13, :cond_8

    const/16 v17, 0x1

    goto :goto_3

    :cond_8
    const/16 v17, 0x3

    :goto_3
    invoke-virtual/range {v16 .. v16}, Lee/cyber/smartid/dto/upgrade/v2/PreV2AndV2AccountWrapper;->getAccountUUID()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2}, Lee/cyber/smartid/dto/upgrade/v2/PreV2KeyState;->getSignatureShare()Ljava/lang/String;

    move-result-object v18

    invoke-virtual {v2}, Lee/cyber/smartid/dto/upgrade/v2/PreV2KeyState;->getSignatureShareEncoding()Ljava/lang/String;

    move-result-object v19

    invoke-virtual {v2}, Lee/cyber/smartid/dto/upgrade/v2/PreV2KeyState;->getTransaction()Lee/cyber/smartid/dto/jsonrpc/Transaction;

    move-result-object v20

    invoke-virtual {v2}, Lee/cyber/smartid/dto/upgrade/v2/PreV2KeyState;->getRetransmitNonce()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object v14, v4

    move/from16 v4, v17

    move-object v13, v5

    move-object v5, v0

    move/from16 v22, v7

    move-object/from16 v7, v18

    move/from16 v18, v8

    move-object/from16 v8, v19

    move-object/from16 v19, v9

    move-object/from16 v9, v20

    move-object/from16 v20, v15

    move-object v15, v10

    move-object/from16 v10, v21

    invoke-static/range {v2 .. v10}, Lee/cyber/smartid/manager/impl/DataUpgradeHelper;->a(Lee/cyber/smartid/cryptolib/CryptoLib;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/dto/jsonrpc/Transaction;Ljava/lang/String;)V

    .line 32
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0, v13}, Lee/cyber/smartid/manager/impl/DataUpgradeHelper;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 33
    sget-object v0, Lee/cyber/smartid/manager/impl/DataUpgradeHelper;->a:Lee/cyber/smartid/util/Log;

    const-string v2, "onUpgradeToV2 - converted an active sign / cloneDetection KeyState"

    invoke-virtual {v0, v2}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    .line 34
    :goto_4
    :try_start_0
    invoke-virtual {v11, v13}, Lee/cyber/smartid/cryptolib/CryptoLib;->removeData(Ljava/lang/String;)V
    :try_end_0
    .catch Lee/cyber/smartid/cryptolib/dto/StorageException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_5

    :catch_0
    move-exception v0

    move-object v2, v0

    .line 35
    sget-object v0, Lee/cyber/smartid/manager/impl/DataUpgradeHelper;->a:Lee/cyber/smartid/util/Log;

    invoke-virtual {v0, v15, v2}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;Ljava/lang/Throwable;)V

    :goto_5
    add-int/lit8 v7, v22, 0x1

    move-object v10, v15

    move/from16 v8, v18

    move-object/from16 v9, v19

    move-object/from16 v15, v20

    const/4 v13, 0x1

    const/4 v14, 0x0

    goto/16 :goto_0

    :cond_9
    move-object v15, v10

    const-string v0, "ee.cyber.smartid.PREF_IS_POST_BOOT_KEY_STATE_CHECK_DONE_DIGEST_%1$s"

    const/4 v1, 0x1

    .line 36
    :try_start_1
    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object v12, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v11, v0, v1}, Lee/cyber/smartid/cryptolib/CryptoLib;->storeData(Ljava/lang/String;Ljava/lang/Object;)V
    :try_end_1
    .catch Lee/cyber/smartid/cryptolib/dto/StorageException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_6

    :catch_1
    move-exception v0

    .line 37
    sget-object v1, Lee/cyber/smartid/manager/impl/DataUpgradeHelper;->a:Lee/cyber/smartid/util/Log;

    invoke-virtual {v1, v15, v0}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;Ljava/lang/Throwable;)V

    :goto_6
    return-void

    .line 38
    :cond_a
    :goto_7
    sget-object v0, Lee/cyber/smartid/manager/impl/DataUpgradeHelper;->a:Lee/cyber/smartid/util/Log;

    const-string v1, "onUpgradeToV2 - no need, no accounts found .."

    invoke-virtual {v0, v1}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    return-void
.end method

.method public static onUpgradeToV3(Lee/cyber/smartid/cryptolib/CryptoLib;Ljava/lang/String;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lee/cyber/smartid/cryptolib/dto/StorageException;
        }
    .end annotation

    const/4 v0, 0x1

    .line 1
    new-array v1, v0, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    const-string v3, "ee.cyber.smartid.APP_TRANSACTION_CACHE_STORAGE_ID_%1$s"

    invoke-static {v3, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 2
    new-array v0, v0, [Ljava/lang/Object;

    aput-object p1, v0, v2

    const-string p1, "ee.cyber.smartid.APP_ACTIVE_TRANSACTION_DATA_%1$s"

    invoke-static {p1, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    .line 3
    new-instance v0, Lee/cyber/smartid/manager/impl/DataUpgradeHelper$6;

    invoke-direct {v0}, Lee/cyber/smartid/manager/impl/DataUpgradeHelper$6;-><init>()V

    .line 4
    invoke-virtual {v0}, Lcom/google/gson/b/a;->getType()Ljava/lang/reflect/Type;

    move-result-object v0

    .line 5
    invoke-virtual {p0, p1, v0}, Lee/cyber/smartid/cryptolib/CryptoLib;->retrieveData(Ljava/lang/String;Ljava/lang/reflect/Type;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lee/cyber/smartid/dto/upgrade/v3/PreV3AndV3Transaction;

    if-eqz v0, :cond_1

    .line 6
    new-instance v2, Lee/cyber/smartid/manager/impl/DataUpgradeHelper$7;

    invoke-direct {v2}, Lee/cyber/smartid/manager/impl/DataUpgradeHelper$7;-><init>()V

    .line 7
    invoke-virtual {v2}, Lcom/google/gson/b/a;->getType()Ljava/lang/reflect/Type;

    move-result-object v2

    .line 8
    invoke-virtual {p0, v1, v2}, Lee/cyber/smartid/cryptolib/CryptoLib;->retrieveData(Ljava/lang/String;Ljava/lang/reflect/Type;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/HashMap;

    if-nez v2, :cond_0

    .line 9
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 10
    :cond_0
    invoke-virtual {v0}, Lee/cyber/smartid/dto/upgrade/v3/PreV3AndV3Transaction;->getTransactionUUID()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Lee/cyber/smartid/dto/upgrade/v3/V3TransactionCachedHolder;

    invoke-direct {v4, v0}, Lee/cyber/smartid/dto/upgrade/v3/V3TransactionCachedHolder;-><init>(Lee/cyber/smartid/dto/upgrade/v3/PreV3AndV3Transaction;)V

    invoke-virtual {v2, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 11
    :try_start_0
    invoke-virtual {p0, v1, v2}, Lee/cyber/smartid/cryptolib/CryptoLib;->storeData(Ljava/lang/String;Ljava/lang/Object;)V

    .line 12
    invoke-virtual {p0, p1}, Lee/cyber/smartid/cryptolib/CryptoLib;->removeData(Ljava/lang/String;)V
    :try_end_0
    .catch Lee/cyber/smartid/cryptolib/dto/StorageException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p0

    .line 13
    sget-object p1, Lee/cyber/smartid/manager/impl/DataUpgradeHelper;->a:Lee/cyber/smartid/util/Log;

    const-string v0, "onUpgradeToV3"

    invoke-virtual {p1, v0, p0}, Lee/cyber/smartid/util/Log;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_1
    :goto_0
    return-void
.end method

.method public static onUpgradeToV6(Lee/cyber/smartid/cryptolib/CryptoLib;Ljava/lang/String;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lee/cyber/smartid/cryptolib/dto/StorageException;
        }
    .end annotation

    .line 1
    sget-object v0, Lee/cyber/smartid/manager/impl/DataUpgradeHelper;->a:Lee/cyber/smartid/util/Log;

    const-string v1, "onUpgradeToV6 started"

    invoke-virtual {v0, v1}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    const/4 v0, 0x1

    .line 2
    new-array v1, v0, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    const-string p1, "ee.cyber.smartid.APP_ACCOUNT_STATES_ID_%1$s"

    invoke-static {p1, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    .line 3
    new-instance v1, Lee/cyber/smartid/manager/impl/DataUpgradeHelper$8;

    invoke-direct {v1}, Lee/cyber/smartid/manager/impl/DataUpgradeHelper$8;-><init>()V

    .line 4
    invoke-virtual {v1}, Lcom/google/gson/b/a;->getType()Ljava/lang/reflect/Type;

    move-result-object v1

    .line 5
    invoke-virtual {p0, p1, v1}, Lee/cyber/smartid/cryptolib/CryptoLib;->retrieveData(Ljava/lang/String;Ljava/lang/reflect/Type;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/ArrayList;

    if-eqz v1, :cond_1

    .line 6
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v2, v0, :cond_0

    goto :goto_0

    .line 7
    :cond_0
    sget-object v0, Lee/cyber/smartid/manager/impl/DataUpgradeHelper;->a:Lee/cyber/smartid/util/Log;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onUpgradeToV6: We have "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v3, " accounts to upgrade"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    .line 8
    invoke-static {v1}, Lee/cyber/smartid/manager/impl/DataUpgradeHelper;->a(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v0

    .line 9
    invoke-virtual {p0, p1, v0}, Lee/cyber/smartid/cryptolib/CryptoLib;->storeData(Ljava/lang/String;Ljava/lang/Object;)V

    .line 10
    sget-object p0, Lee/cyber/smartid/manager/impl/DataUpgradeHelper;->a:Lee/cyber/smartid/util/Log;

    const-string p1, "onUpgradeToV6 completed"

    invoke-virtual {p0, p1}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    return-void

    .line 11
    :cond_1
    :goto_0
    sget-object p0, Lee/cyber/smartid/manager/impl/DataUpgradeHelper;->a:Lee/cyber/smartid/util/Log;

    const-string p1, "onUpgradeToV6 completed, nothing to upgrade"

    invoke-virtual {p0, p1}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    return-void
.end method
