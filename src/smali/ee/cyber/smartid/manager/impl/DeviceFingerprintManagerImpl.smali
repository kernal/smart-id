.class public Lee/cyber/smartid/manager/impl/DeviceFingerprintManagerImpl;
.super Ljava/lang/Object;
.source "DeviceFingerprintManagerImpl.java"

# interfaces
.implements Lee/cyber/smartid/manager/inter/DeviceFingerprintManager;


# instance fields
.field private final a:Lee/cyber/smartid/inter/ServiceAccess;

.field private final b:Lee/cyber/smartid/inter/ListenerAccess;


# direct methods
.method public constructor <init>(Lee/cyber/smartid/inter/ServiceAccess;Lee/cyber/smartid/inter/ListenerAccess;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Lee/cyber/smartid/manager/impl/DeviceFingerprintManagerImpl;->a:Lee/cyber/smartid/inter/ServiceAccess;

    .line 3
    iput-object p2, p0, Lee/cyber/smartid/manager/impl/DeviceFingerprintManagerImpl;->b:Lee/cyber/smartid/inter/ListenerAccess;

    return-void
.end method

.method static synthetic a(Lee/cyber/smartid/manager/impl/DeviceFingerprintManagerImpl;)Lee/cyber/smartid/inter/ListenerAccess;
    .locals 0

    .line 1
    iget-object p0, p0, Lee/cyber/smartid/manager/impl/DeviceFingerprintManagerImpl;->b:Lee/cyber/smartid/inter/ListenerAccess;

    return-object p0
.end method


# virtual methods
.method public getDeviceFingerPrint()Ljava/lang/String;
    .locals 2
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "HardwareIds"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/manager/impl/DeviceFingerprintManagerImpl;->a:Lee/cyber/smartid/inter/ServiceAccess;

    invoke-interface {v0}, Lee/cyber/smartid/inter/ServiceAccess;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "android_id"

    invoke-static {v0, v1}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getDeviceFingerprint(Ljava/lang/String;Lee/cyber/smartid/inter/GetDeviceFingerprintListener;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/manager/impl/DeviceFingerprintManagerImpl;->b:Lee/cyber/smartid/inter/ListenerAccess;

    invoke-interface {v0, p1, p2}, Lee/cyber/smartid/inter/ListenerAccess;->setListener(Ljava/lang/String;Lee/cyber/smartid/inter/ServiceListener;)V

    .line 2
    new-instance p2, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-direct {p2, v0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 3
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lee/cyber/smartid/manager/impl/DeviceFingerprintManagerImpl$1;

    invoke-direct {v1, p0, p2, p1}, Lee/cyber/smartid/manager/impl/DeviceFingerprintManagerImpl$1;-><init>(Lee/cyber/smartid/manager/impl/DeviceFingerprintManagerImpl;Landroid/os/Handler;Ljava/lang/String;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 4
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    return-void
.end method
