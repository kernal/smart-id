.class Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper$2;
.super Ljava/lang/Object;
.source "InteractiveUpgradeHelper.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->a()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;


# direct methods
.method constructor <init>(Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper$2;->a:Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .line 1
    :try_start_0
    iget-object v0, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper$2;->a:Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;

    invoke-static {v0}, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->h(Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    const-wide/32 v1, 0x1d4c0

    invoke-static {v0, v1, v2}, Lee/cyber/smartid/util/Util;->acquireWakeLock(Landroid/os/PowerManager$WakeLock;J)V

    .line 2
    iget-object v0, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper$2;->a:Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;

    invoke-static {v0}, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->i(Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    goto :goto_1

    :catch_0
    move-exception v0

    .line 3
    :try_start_1
    iget-object v1, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper$2;->a:Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;

    invoke-static {v1}, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->g(Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;)Lee/cyber/smartid/util/Log;

    move-result-object v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    const-string v2, "startMDv2UpgradeAsync"

    :try_start_2
    invoke-virtual {v1, v2, v0}, Lee/cyber/smartid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 4
    :goto_0
    iget-object v0, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper$2;->a:Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;

    invoke-static {v0}, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->h(Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    invoke-static {v0}, Lee/cyber/smartid/util/Util;->releaseWakeLock(Landroid/os/PowerManager$WakeLock;)V

    return-void

    :goto_1
    iget-object v1, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper$2;->a:Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;

    invoke-static {v1}, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->h(Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;)Landroid/os/PowerManager$WakeLock;

    move-result-object v1

    invoke-static {v1}, Lee/cyber/smartid/util/Util;->releaseWakeLock(Landroid/os/PowerManager$WakeLock;)V

    throw v0
.end method
