.class Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper$1$3;
.super Ljava/lang/Object;
.source "InteractiveUpgradeHelper.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper$1;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Ljava/util/ArrayList;

.field final synthetic b:Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper$1;


# direct methods
.method constructor <init>(Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper$1;Ljava/util/ArrayList;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper$1$3;->b:Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper$1;

    iput-object p2, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper$1$3;->a:Ljava/util/ArrayList;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper$1$3;->b:Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper$1;

    iget-object v0, v0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper$1;->b:Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;

    invoke-static {v0}, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->b(Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;)Lee/cyber/smartid/inter/ListenerAccess;

    move-result-object v0

    iget-object v1, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper$1$3;->b:Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper$1;

    iget-object v1, v1, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper$1;->a:Ljava/lang/String;

    const-class v2, Lee/cyber/smartid/inter/CancelInteractiveUpgradeListener;

    const/4 v3, 0x1

    invoke-interface {v0, v1, v3, v2}, Lee/cyber/smartid/inter/ListenerAccess;->getListener(Ljava/lang/String;ZLjava/lang/Class;)Lee/cyber/smartid/inter/ServiceListener;

    move-result-object v0

    check-cast v0, Lee/cyber/smartid/inter/CancelInteractiveUpgradeListener;

    if-eqz v0, :cond_0

    .line 2
    iget-object v1, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper$1$3;->b:Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper$1;

    iget-object v1, v1, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper$1;->a:Ljava/lang/String;

    new-instance v2, Lee/cyber/smartid/dto/jsonrpc/resp/CancelInteractiveUpgradeResp;

    iget-object v3, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper$1$3;->a:Ljava/util/ArrayList;

    invoke-direct {v2, v1, v3}, Lee/cyber/smartid/dto/jsonrpc/resp/CancelInteractiveUpgradeResp;-><init>(Ljava/lang/String;Ljava/util/ArrayList;)V

    invoke-interface {v0, v1, v2}, Lee/cyber/smartid/inter/CancelInteractiveUpgradeListener;->onCancelInteractiveUpgradeSuccess(Ljava/lang/String;Lee/cyber/smartid/dto/jsonrpc/resp/CancelInteractiveUpgradeResp;)V

    :cond_0
    return-void
.end method
