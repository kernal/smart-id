.class Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$13;
.super Lee/cyber/smartid/tse/network/RPCCallbackNonUIThread;
.source "AddAccountManagerImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;->b(Ljava/lang/String;Lee/cyber/smartid/dto/jsonrpc/param/AccountRegistrationDataParams;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lee/cyber/smartid/tse/network/RPCCallbackNonUIThread<",
        "Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCResponse<",
        "Lee/cyber/smartid/dto/jsonrpc/result/RegisterAccountResult;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:Ljava/lang/String;

.field final synthetic c:Ljava/lang/String;

.field final synthetic d:Ljava/lang/String;

.field final synthetic e:Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;


# direct methods
.method constructor <init>(Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCRequest;Lee/cyber/smartid/tse/SmartIdTSE;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$13;->e:Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;

    iput-object p4, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$13;->a:Ljava/lang/String;

    iput-object p5, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$13;->b:Ljava/lang/String;

    iput-object p6, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$13;->c:Ljava/lang/String;

    iput-object p7, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$13;->d:Ljava/lang/String;

    invoke-direct {p0, p2, p3}, Lee/cyber/smartid/tse/network/RPCCallbackNonUIThread;-><init>(Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCRequest;Lee/cyber/smartid/tse/SmartIdTSE;)V

    return-void
.end method


# virtual methods
.method public onFailure(Lc/b;Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCError;Ljava/lang/Throwable;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lc/b<",
            "Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCResponse<",
            "Lee/cyber/smartid/dto/jsonrpc/result/RegisterAccountResult;",
            ">;>;",
            "Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCError;",
            "Ljava/lang/Throwable;",
            ")V"
        }
    .end annotation

    .line 1
    iget-object p1, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$13;->e:Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;

    iget-object v0, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$13;->a:Ljava/lang/String;

    invoke-static {p1}, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;->c(Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;)Lee/cyber/smartid/inter/ServiceAccess;

    move-result-object v1

    invoke-interface {v1}, Lee/cyber/smartid/inter/ServiceAccess;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$13;->e:Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;

    invoke-static {v2}, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;->b(Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;)Lee/cyber/smartid/tse/inter/WallClock;

    move-result-object v2

    invoke-static {v1, v2, p2, p3}, Lee/cyber/smartid/dto/SmartIdError;->from(Landroid/content/Context;Lee/cyber/smartid/tse/inter/WallClock;Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCError;Ljava/lang/Throwable;)Lee/cyber/smartid/dto/SmartIdError;

    move-result-object p2

    const/4 p3, 0x0

    const/4 v1, 0x0

    invoke-static {p1, v0, p2, p3, v1}, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;->a(Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;Ljava/lang/String;Lee/cyber/smartid/dto/SmartIdError;Ljava/lang/String;Z)V

    return-void
.end method

.method public onSuccess(Lc/b;Lc/r;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lc/b<",
            "Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCResponse<",
            "Lee/cyber/smartid/dto/jsonrpc/result/RegisterAccountResult;",
            ">;>;",
            "Lc/r<",
            "Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCResponse<",
            "Lee/cyber/smartid/dto/jsonrpc/result/RegisterAccountResult;",
            ">;>;)V"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$13;->e:Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;

    iget-object v1, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$13;->a:Ljava/lang/String;

    iget-object v2, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$13;->b:Ljava/lang/String;

    iget-object v3, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$13;->c:Ljava/lang/String;

    invoke-virtual {p2}, Lc/r;->e()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCResponse;

    invoke-virtual {p1}, Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCResponse;->getResult()Ljava/lang/Object;

    move-result-object p1

    move-object v4, p1

    check-cast v4, Lee/cyber/smartid/dto/jsonrpc/result/RegisterAccountResult;

    iget-object v6, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$13;->d:Ljava/lang/String;

    const/4 v5, 0x0

    invoke-static/range {v0 .. v6}, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;->a(Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/dto/jsonrpc/result/RegisterAccountResult;Lee/cyber/smartid/dto/jsonrpc/param/RegisterDeviceData;Ljava/lang/String;)V

    return-void
.end method

.method public onValidate(Lc/b;Lc/r;)Z
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lc/b<",
            "Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCResponse<",
            "Lee/cyber/smartid/dto/jsonrpc/result/RegisterAccountResult;",
            ">;>;",
            "Lc/r<",
            "Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCResponse<",
            "Lee/cyber/smartid/dto/jsonrpc/result/RegisterAccountResult;",
            ">;>;)Z"
        }
    .end annotation

    .line 1
    invoke-static {p2}, Lee/cyber/smartid/util/Util;->validateResponse(Lc/r;)Z

    move-result p1

    return p1
.end method
