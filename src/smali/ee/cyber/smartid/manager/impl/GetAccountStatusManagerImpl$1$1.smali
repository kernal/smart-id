.class Lee/cyber/smartid/manager/impl/GetAccountStatusManagerImpl$1$1;
.super Ljava/lang/Object;
.source "GetAccountStatusManagerImpl.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lee/cyber/smartid/manager/impl/GetAccountStatusManagerImpl$1;->onFailure(Lc/b;Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCError;Ljava/lang/Throwable;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCError;

.field final synthetic b:Ljava/lang/Throwable;

.field final synthetic c:Lee/cyber/smartid/manager/impl/GetAccountStatusManagerImpl$1;


# direct methods
.method constructor <init>(Lee/cyber/smartid/manager/impl/GetAccountStatusManagerImpl$1;Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCError;Ljava/lang/Throwable;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lee/cyber/smartid/manager/impl/GetAccountStatusManagerImpl$1$1;->c:Lee/cyber/smartid/manager/impl/GetAccountStatusManagerImpl$1;

    iput-object p2, p0, Lee/cyber/smartid/manager/impl/GetAccountStatusManagerImpl$1$1;->a:Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCError;

    iput-object p3, p0, Lee/cyber/smartid/manager/impl/GetAccountStatusManagerImpl$1$1;->b:Ljava/lang/Throwable;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/manager/impl/GetAccountStatusManagerImpl$1$1;->c:Lee/cyber/smartid/manager/impl/GetAccountStatusManagerImpl$1;

    iget-object v0, v0, Lee/cyber/smartid/manager/impl/GetAccountStatusManagerImpl$1;->b:Lee/cyber/smartid/manager/impl/GetAccountStatusManagerImpl;

    invoke-static {v0}, Lee/cyber/smartid/manager/impl/GetAccountStatusManagerImpl;->c(Lee/cyber/smartid/manager/impl/GetAccountStatusManagerImpl;)Lee/cyber/smartid/inter/ListenerAccess;

    move-result-object v0

    iget-object v1, p0, Lee/cyber/smartid/manager/impl/GetAccountStatusManagerImpl$1$1;->c:Lee/cyber/smartid/manager/impl/GetAccountStatusManagerImpl$1;

    iget-object v1, v1, Lee/cyber/smartid/manager/impl/GetAccountStatusManagerImpl$1;->b:Lee/cyber/smartid/manager/impl/GetAccountStatusManagerImpl;

    invoke-static {v1}, Lee/cyber/smartid/manager/impl/GetAccountStatusManagerImpl;->c(Lee/cyber/smartid/manager/impl/GetAccountStatusManagerImpl;)Lee/cyber/smartid/inter/ListenerAccess;

    move-result-object v1

    iget-object v2, p0, Lee/cyber/smartid/manager/impl/GetAccountStatusManagerImpl$1$1;->c:Lee/cyber/smartid/manager/impl/GetAccountStatusManagerImpl$1;

    iget-object v2, v2, Lee/cyber/smartid/manager/impl/GetAccountStatusManagerImpl$1;->a:Ljava/lang/String;

    const-class v3, Lee/cyber/smartid/inter/GetAccountStatusListener;

    const/4 v4, 0x1

    invoke-interface {v1, v2, v4, v3}, Lee/cyber/smartid/inter/ListenerAccess;->getListener(Ljava/lang/String;ZLjava/lang/Class;)Lee/cyber/smartid/inter/ServiceListener;

    move-result-object v1

    iget-object v2, p0, Lee/cyber/smartid/manager/impl/GetAccountStatusManagerImpl$1$1;->c:Lee/cyber/smartid/manager/impl/GetAccountStatusManagerImpl$1;

    iget-object v3, v2, Lee/cyber/smartid/manager/impl/GetAccountStatusManagerImpl$1;->a:Ljava/lang/String;

    iget-object v2, v2, Lee/cyber/smartid/manager/impl/GetAccountStatusManagerImpl$1;->b:Lee/cyber/smartid/manager/impl/GetAccountStatusManagerImpl;

    invoke-static {v2}, Lee/cyber/smartid/manager/impl/GetAccountStatusManagerImpl;->b(Lee/cyber/smartid/manager/impl/GetAccountStatusManagerImpl;)Lee/cyber/smartid/inter/ServiceAccess;

    move-result-object v2

    invoke-interface {v2}, Lee/cyber/smartid/inter/ServiceAccess;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    iget-object v4, p0, Lee/cyber/smartid/manager/impl/GetAccountStatusManagerImpl$1$1;->c:Lee/cyber/smartid/manager/impl/GetAccountStatusManagerImpl$1;

    iget-object v4, v4, Lee/cyber/smartid/manager/impl/GetAccountStatusManagerImpl$1;->b:Lee/cyber/smartid/manager/impl/GetAccountStatusManagerImpl;

    invoke-static {v4}, Lee/cyber/smartid/manager/impl/GetAccountStatusManagerImpl;->d(Lee/cyber/smartid/manager/impl/GetAccountStatusManagerImpl;)Lee/cyber/smartid/tse/inter/WallClock;

    move-result-object v4

    iget-object v5, p0, Lee/cyber/smartid/manager/impl/GetAccountStatusManagerImpl$1$1;->a:Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCError;

    iget-object v6, p0, Lee/cyber/smartid/manager/impl/GetAccountStatusManagerImpl$1$1;->b:Ljava/lang/Throwable;

    invoke-static {v2, v4, v5, v6}, Lee/cyber/smartid/dto/SmartIdError;->from(Landroid/content/Context;Lee/cyber/smartid/tse/inter/WallClock;Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCError;Ljava/lang/Throwable;)Lee/cyber/smartid/dto/SmartIdError;

    move-result-object v2

    invoke-interface {v0, v1, v3, v2}, Lee/cyber/smartid/inter/ListenerAccess;->notifyError(Lee/cyber/smartid/inter/ServiceListener;Ljava/lang/String;Lee/cyber/smartid/dto/SmartIdError;)V

    return-void
.end method
