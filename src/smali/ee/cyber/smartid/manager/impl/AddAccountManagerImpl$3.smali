.class Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$3;
.super Ljava/lang/Object;
.source "AddAccountManagerImpl.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:Ljava/lang/String;

.field final synthetic c:Ljava/lang/String;

.field final synthetic d:Ljava/lang/String;

.field final synthetic e:Ljava/lang/String;

.field final synthetic f:Ljava/lang/String;

.field final synthetic g:Ljava/lang/String;

.field final synthetic h:Ljava/lang/String;

.field final synthetic i:Ljava/lang/String;

.field final synthetic j:Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;


# direct methods
.method constructor <init>(Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$3;->j:Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;

    iput-object p2, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$3;->a:Ljava/lang/String;

    iput-object p3, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$3;->b:Ljava/lang/String;

    iput-object p4, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$3;->c:Ljava/lang/String;

    iput-object p5, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$3;->d:Ljava/lang/String;

    iput-object p6, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$3;->e:Ljava/lang/String;

    iput-object p7, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$3;->f:Ljava/lang/String;

    iput-object p8, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$3;->g:Ljava/lang/String;

    iput-object p9, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$3;->h:Ljava/lang/String;

    iput-object p10, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$3;->i:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .line 1
    new-instance v4, Lee/cyber/smartid/dto/jsonrpc/param/AccountRegistrationDataParams;

    invoke-direct {v4}, Lee/cyber/smartid/dto/jsonrpc/param/AccountRegistrationDataParams;-><init>()V

    .line 2
    iget-object v0, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$3;->j:Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;

    invoke-static {v0}, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;->e(Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;)Lee/cyber/smartid/tse/SmartIdTSE;

    move-result-object v0

    iget-object v1, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$3;->j:Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;

    invoke-static {v1}, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;->c(Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;)Lee/cyber/smartid/inter/ServiceAccess;

    move-result-object v1

    invoke-interface {v1}, Lee/cyber/smartid/inter/ServiceAccess;->getPropertiesManager()Lee/cyber/smartid/manager/inter/PropertiesManager;

    move-result-object v1

    invoke-interface {v1}, Lee/cyber/smartid/manager/inter/PropertiesManager;->getPropAccountRegistrationSZId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lee/cyber/smartid/tse/SmartIdTSE;->getKnownServerKeys(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v4, v0}, Lee/cyber/smartid/dto/jsonrpc/param/AccountRegistrationDataParams;->setKnownServerKeys(Ljava/util/ArrayList;)V

    .line 3
    iget-object v0, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$3;->a:Ljava/lang/String;

    invoke-virtual {v4, v0}, Lee/cyber/smartid/dto/jsonrpc/param/AccountRegistrationDataParams;->setClientAuthDhPublicKey(Ljava/lang/String;)V

    .line 4
    iget-object v0, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$3;->b:Ljava/lang/String;

    invoke-virtual {v4, v0}, Lee/cyber/smartid/dto/jsonrpc/param/AccountRegistrationDataParams;->setClientSignDhPublicKey(Ljava/lang/String;)V

    .line 5
    iget-object v0, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$3;->c:Ljava/lang/String;

    invoke-virtual {v4, v0}, Lee/cyber/smartid/dto/jsonrpc/param/AccountRegistrationDataParams;->setRegistrationType(Ljava/lang/String;)V

    .line 6
    iget-object v0, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$3;->c:Ljava/lang/String;

    const-string v1, "IDENTITYTOKEN"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x0

    const-string v2, "KEYTOKEN"

    if-eqz v0, :cond_0

    .line 7
    iget-object v0, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$3;->d:Ljava/lang/String;

    invoke-virtual {v4, v0}, Lee/cyber/smartid/dto/jsonrpc/param/AccountRegistrationDataParams;->setRegistrationToken(Ljava/lang/String;)V

    goto :goto_0

    .line 8
    :cond_0
    iget-object v0, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$3;->c:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 9
    iget-object v0, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$3;->e:Ljava/lang/String;

    invoke-virtual {v4, v0}, Lee/cyber/smartid/dto/jsonrpc/param/AccountRegistrationDataParams;->setRegistrationToken(Ljava/lang/String;)V

    goto :goto_0

    .line 10
    :cond_1
    invoke-virtual {v4, v1}, Lee/cyber/smartid/dto/jsonrpc/param/AccountRegistrationDataParams;->setRegistrationToken(Ljava/lang/String;)V

    .line 11
    :goto_0
    iget-object v0, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$3;->c:Ljava/lang/String;

    invoke-static {v2, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v1, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$3;->f:Ljava/lang/String;

    :cond_2
    invoke-virtual {v4, v1}, Lee/cyber/smartid/dto/jsonrpc/param/AccountRegistrationDataParams;->setRegistrationTokenAlgorithm(Ljava/lang/String;)V

    .line 12
    :try_start_0
    iget-object v0, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$3;->j:Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;

    invoke-static {v0}, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;->e(Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;)Lee/cyber/smartid/tse/SmartIdTSE;

    move-result-object v0

    iget-object v1, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$3;->g:Ljava/lang/String;

    new-instance v2, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$3$1;

    invoke-direct {v2, p0, v4}, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$3$1;-><init>(Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$3;Lee/cyber/smartid/dto/jsonrpc/param/AccountRegistrationDataParams;)V

    invoke-virtual {v0, v1, v2}, Lee/cyber/smartid/tse/SmartIdTSE;->addKeyMaterialValuesForServer(Ljava/lang/String;Lee/cyber/smartid/tse/inter/KeyMaterialApplier;)V

    .line 13
    iget-object v0, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$3;->j:Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;

    invoke-static {v0}, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;->e(Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;)Lee/cyber/smartid/tse/SmartIdTSE;

    move-result-object v0

    iget-object v1, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$3;->h:Ljava/lang/String;

    new-instance v2, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$3$2;

    invoke-direct {v2, p0, v4}, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$3$2;-><init>(Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$3;Lee/cyber/smartid/dto/jsonrpc/param/AccountRegistrationDataParams;)V

    invoke-virtual {v0, v1, v2}, Lee/cyber/smartid/tse/SmartIdTSE;->addKeyMaterialValuesForServer(Ljava/lang/String;Lee/cyber/smartid/tse/inter/KeyMaterialApplier;)V
    :try_end_0
    .catch Lee/cyber/smartid/tse/dto/NoSuchKeysException; {:try_start_0 .. :try_end_0} :catch_0

    .line 14
    iget-object v0, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$3;->j:Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;

    iget-object v1, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$3;->i:Ljava/lang/String;

    iget-object v2, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$3;->g:Ljava/lang/String;

    iget-object v3, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$3;->h:Ljava/lang/String;

    iget-object v5, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$3;->c:Ljava/lang/String;

    invoke-static/range {v0 .. v5}, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;->a(Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/dto/jsonrpc/param/AccountRegistrationDataParams;Ljava/lang/String;)V

    return-void

    .line 15
    :catch_0
    iget-object v0, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$3;->j:Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;

    invoke-static {v0}, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;->c(Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;)Lee/cyber/smartid/inter/ServiceAccess;

    move-result-object v0

    new-instance v1, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$3$3;

    invoke-direct {v1, p0}, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$3$3;-><init>(Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$3;)V

    invoke-interface {v0, v1}, Lee/cyber/smartid/inter/ServiceAccess;->notifyUI(Ljava/lang/Runnable;)V

    return-void
.end method
