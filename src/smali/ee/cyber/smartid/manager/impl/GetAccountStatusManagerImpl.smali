.class public Lee/cyber/smartid/manager/impl/GetAccountStatusManagerImpl;
.super Ljava/lang/Object;
.source "GetAccountStatusManagerImpl.java"

# interfaces
.implements Lee/cyber/smartid/manager/inter/GetAccountStatusManager;


# static fields
.field private static volatile a:Lee/cyber/smartid/manager/inter/GetAccountStatusManager;


# instance fields
.field private final b:Lee/cyber/smartid/network/SmartIdAPI;

.field private final c:Lee/cyber/smartid/tse/SmartIdTSE;

.field private final d:Lee/cyber/smartid/inter/ServiceAccess;

.field private final e:Lee/cyber/smartid/inter/ListenerAccess;

.field private final f:Lee/cyber/smartid/tse/inter/WallClock;

.field private g:Lee/cyber/smartid/util/Log;


# direct methods
.method private constructor <init>(Lee/cyber/smartid/network/SmartIdAPI;Lee/cyber/smartid/tse/SmartIdTSE;Lee/cyber/smartid/inter/ServiceAccess;Lee/cyber/smartid/inter/ListenerAccess;Lee/cyber/smartid/tse/inter/WallClock;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    invoke-static {p0}, Lee/cyber/smartid/util/Log;->getInstance(Ljava/lang/Object;)Lee/cyber/smartid/util/Log;

    move-result-object v0

    iput-object v0, p0, Lee/cyber/smartid/manager/impl/GetAccountStatusManagerImpl;->g:Lee/cyber/smartid/util/Log;

    .line 3
    iput-object p1, p0, Lee/cyber/smartid/manager/impl/GetAccountStatusManagerImpl;->b:Lee/cyber/smartid/network/SmartIdAPI;

    .line 4
    iput-object p2, p0, Lee/cyber/smartid/manager/impl/GetAccountStatusManagerImpl;->c:Lee/cyber/smartid/tse/SmartIdTSE;

    .line 5
    iput-object p3, p0, Lee/cyber/smartid/manager/impl/GetAccountStatusManagerImpl;->d:Lee/cyber/smartid/inter/ServiceAccess;

    .line 6
    iput-object p4, p0, Lee/cyber/smartid/manager/impl/GetAccountStatusManagerImpl;->e:Lee/cyber/smartid/inter/ListenerAccess;

    .line 7
    iput-object p5, p0, Lee/cyber/smartid/manager/impl/GetAccountStatusManagerImpl;->f:Lee/cyber/smartid/tse/inter/WallClock;

    return-void
.end method

.method static synthetic a(Lee/cyber/smartid/manager/impl/GetAccountStatusManagerImpl;)Lee/cyber/smartid/util/Log;
    .locals 0

    .line 1
    iget-object p0, p0, Lee/cyber/smartid/manager/impl/GetAccountStatusManagerImpl;->g:Lee/cyber/smartid/util/Log;

    return-object p0
.end method

.method static synthetic b(Lee/cyber/smartid/manager/impl/GetAccountStatusManagerImpl;)Lee/cyber/smartid/inter/ServiceAccess;
    .locals 0

    .line 1
    iget-object p0, p0, Lee/cyber/smartid/manager/impl/GetAccountStatusManagerImpl;->d:Lee/cyber/smartid/inter/ServiceAccess;

    return-object p0
.end method

.method static synthetic c(Lee/cyber/smartid/manager/impl/GetAccountStatusManagerImpl;)Lee/cyber/smartid/inter/ListenerAccess;
    .locals 0

    .line 1
    iget-object p0, p0, Lee/cyber/smartid/manager/impl/GetAccountStatusManagerImpl;->e:Lee/cyber/smartid/inter/ListenerAccess;

    return-object p0
.end method

.method static synthetic d(Lee/cyber/smartid/manager/impl/GetAccountStatusManagerImpl;)Lee/cyber/smartid/tse/inter/WallClock;
    .locals 0

    .line 1
    iget-object p0, p0, Lee/cyber/smartid/manager/impl/GetAccountStatusManagerImpl;->f:Lee/cyber/smartid/tse/inter/WallClock;

    return-object p0
.end method

.method public static getInstance(Lee/cyber/smartid/network/SmartIdAPI;Lee/cyber/smartid/tse/SmartIdTSE;Lee/cyber/smartid/inter/ServiceAccess;Lee/cyber/smartid/inter/ListenerAccess;Lee/cyber/smartid/tse/inter/WallClock;)Lee/cyber/smartid/manager/inter/GetAccountStatusManager;
    .locals 8

    .line 1
    sget-object v0, Lee/cyber/smartid/manager/impl/GetAccountStatusManagerImpl;->a:Lee/cyber/smartid/manager/inter/GetAccountStatusManager;

    if-nez v0, :cond_0

    .line 2
    const-class v0, Lee/cyber/smartid/manager/impl/DeleteAccountManagerImpl;

    monitor-enter v0

    .line 3
    :try_start_0
    new-instance v7, Lee/cyber/smartid/manager/impl/GetAccountStatusManagerImpl;

    move-object v1, v7

    move-object v2, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    move-object v6, p4

    invoke-direct/range {v1 .. v6}, Lee/cyber/smartid/manager/impl/GetAccountStatusManagerImpl;-><init>(Lee/cyber/smartid/network/SmartIdAPI;Lee/cyber/smartid/tse/SmartIdTSE;Lee/cyber/smartid/inter/ServiceAccess;Lee/cyber/smartid/inter/ListenerAccess;Lee/cyber/smartid/tse/inter/WallClock;)V

    sput-object v7, Lee/cyber/smartid/manager/impl/GetAccountStatusManagerImpl;->a:Lee/cyber/smartid/manager/inter/GetAccountStatusManager;

    .line 4
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception p0

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p0

    .line 5
    :cond_0
    :goto_0
    sget-object p0, Lee/cyber/smartid/manager/impl/GetAccountStatusManagerImpl;->a:Lee/cyber/smartid/manager/inter/GetAccountStatusManager;

    return-object p0
.end method


# virtual methods
.method public getAccountStatus(Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/inter/GetAccountStatusListener;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/manager/impl/GetAccountStatusManagerImpl;->e:Lee/cyber/smartid/inter/ListenerAccess;

    invoke-interface {v0, p1, p3}, Lee/cyber/smartid/inter/ListenerAccess;->setListener(Ljava/lang/String;Lee/cyber/smartid/inter/ServiceListener;)V

    .line 2
    new-instance p3, Lee/cyber/smartid/dto/jsonrpc/param/GetAccountStatusParams;

    invoke-direct {p3, p2}, Lee/cyber/smartid/dto/jsonrpc/param/GetAccountStatusParams;-><init>(Ljava/lang/String;)V

    .line 3
    new-instance p2, Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCRequest;

    const-string v0, "getAccountStatus"

    invoke-direct {p2, v0, p3}, Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCRequest;-><init>(Ljava/lang/String;Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCParams;)V

    .line 4
    iget-object p3, p0, Lee/cyber/smartid/manager/impl/GetAccountStatusManagerImpl;->b:Lee/cyber/smartid/network/SmartIdAPI;

    invoke-interface {p3, p2}, Lee/cyber/smartid/network/SmartIdAPI;->getAccountStatus(Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCRequest;)Lc/b;

    move-result-object p3

    .line 5
    new-instance v0, Lee/cyber/smartid/manager/impl/GetAccountStatusManagerImpl$1;

    iget-object v1, p0, Lee/cyber/smartid/manager/impl/GetAccountStatusManagerImpl;->c:Lee/cyber/smartid/tse/SmartIdTSE;

    invoke-direct {v0, p0, p2, v1, p1}, Lee/cyber/smartid/manager/impl/GetAccountStatusManagerImpl$1;-><init>(Lee/cyber/smartid/manager/impl/GetAccountStatusManagerImpl;Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCRequest;Lee/cyber/smartid/tse/SmartIdTSE;Ljava/lang/String;)V

    invoke-interface {p3, v0}, Lc/b;->a(Lc/d;)V

    return-void
.end method
