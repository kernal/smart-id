.class Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$16$1;
.super Ljava/lang/Object;
.source "TransactionAndRPRequestManagerImpl.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$16;->onSuccess(Lc/b;Lc/r;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$16;


# direct methods
.method constructor <init>(Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$16;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$16$1;->a:Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$16;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$16$1;->a:Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$16;

    iget-object v0, v0, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$16;->d:Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;

    invoke-static {v0}, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;->c(Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;)Lee/cyber/smartid/inter/ListenerAccess;

    move-result-object v0

    iget-object v1, p0, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$16$1;->a:Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$16;

    iget-object v2, v1, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$16;->a:Ljava/lang/String;

    iget-object v1, v1, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$16;->b:Ljava/lang/Class;

    const/4 v3, 0x1

    invoke-interface {v0, v2, v3, v1}, Lee/cyber/smartid/inter/ListenerAccess;->getListener(Ljava/lang/String;ZLjava/lang/Class;)Lee/cyber/smartid/inter/ServiceListener;

    move-result-object v0

    check-cast v0, Lee/cyber/smartid/inter/CancelTransactionListener;

    if-eqz v0, :cond_0

    .line 2
    new-instance v1, Lee/cyber/smartid/dto/jsonrpc/resp/CancelTransactionResp;

    iget-object v2, p0, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$16$1;->a:Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$16;

    iget-object v3, v2, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$16;->a:Ljava/lang/String;

    iget-object v2, v2, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$16;->c:Ljava/lang/String;

    invoke-direct {v1, v3, v2}, Lee/cyber/smartid/dto/jsonrpc/resp/CancelTransactionResp;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 3
    iget-object v2, p0, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$16$1;->a:Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$16;

    iget-object v2, v2, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$16;->a:Ljava/lang/String;

    invoke-interface {v0, v2, v1}, Lee/cyber/smartid/inter/CancelTransactionListener;->onCancelTransactionSuccess(Ljava/lang/String;Lee/cyber/smartid/dto/jsonrpc/resp/CancelTransactionResp;)V

    :cond_0
    return-void
.end method
