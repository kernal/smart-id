.class Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper$1$1;
.super Ljava/lang/Object;
.source "InteractiveUpgradeHelper.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper$1;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper$1;


# direct methods
.method constructor <init>(Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper$1;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper$1$1;->a:Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper$1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper$1$1;->a:Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper$1;

    iget-object v0, v0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper$1;->b:Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;

    invoke-static {v0}, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->b(Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;)Lee/cyber/smartid/inter/ListenerAccess;

    move-result-object v0

    iget-object v1, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper$1$1;->a:Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper$1;

    iget-object v1, v1, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper$1;->b:Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;

    invoke-static {v1}, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->b(Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;)Lee/cyber/smartid/inter/ListenerAccess;

    move-result-object v1

    iget-object v2, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper$1$1;->a:Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper$1;

    iget-object v2, v2, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper$1;->a:Ljava/lang/String;

    const-class v3, Lee/cyber/smartid/inter/CancelInteractiveUpgradeListener;

    const/4 v4, 0x1

    invoke-interface {v1, v2, v4, v3}, Lee/cyber/smartid/inter/ListenerAccess;->getListener(Ljava/lang/String;ZLjava/lang/Class;)Lee/cyber/smartid/inter/ServiceListener;

    move-result-object v1

    iget-object v2, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper$1$1;->a:Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper$1;

    iget-object v3, v2, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper$1;->a:Ljava/lang/String;

    iget-object v2, v2, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper$1;->b:Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;

    invoke-static {v2}, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->c(Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;)Lee/cyber/smartid/tse/inter/WallClock;

    move-result-object v2

    iget-object v4, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper$1$1;->a:Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper$1;

    iget-object v4, v4, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper$1;->b:Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;

    invoke-static {v4}, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->d(Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;)Lee/cyber/smartid/inter/ServiceAccess;

    move-result-object v4

    invoke-interface {v4}, Lee/cyber/smartid/inter/ServiceAccess;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    sget v5, Lee/cyber/smartid/R$string;->err_interactive_upgrade_is_in_progress:I

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    const-wide/16 v5, 0x415

    invoke-static {v2, v5, v6, v4}, Lee/cyber/smartid/dto/SmartIdError;->from(Lee/cyber/smartid/tse/inter/WallClock;JLjava/lang/String;)Lee/cyber/smartid/dto/SmartIdError;

    move-result-object v2

    invoke-interface {v0, v1, v3, v2}, Lee/cyber/smartid/inter/ListenerAccess;->notifyError(Lee/cyber/smartid/inter/ServiceListener;Ljava/lang/String;Lee/cyber/smartid/dto/SmartIdError;)V

    return-void
.end method
