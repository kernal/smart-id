.class public Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;
.super Ljava/lang/Object;
.source "TransactionAndRPRequestManagerImpl.java"

# interfaces
.implements Lee/cyber/smartid/manager/inter/TransactionAndRPRequestManager;


# static fields
.field private static volatile a:Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;


# instance fields
.field private final b:Lee/cyber/smartid/network/SmartIdAPI;

.field private final c:Lee/cyber/smartid/tse/SmartIdTSE;

.field private final d:Lee/cyber/smartid/inter/ServiceAccess;

.field private final e:Lee/cyber/smartid/inter/ListenerAccess;

.field private final f:Lee/cyber/smartid/tse/inter/WallClock;

.field private g:Lee/cyber/smartid/util/Log;


# direct methods
.method private constructor <init>(Lee/cyber/smartid/network/SmartIdAPI;Lee/cyber/smartid/tse/SmartIdTSE;Lee/cyber/smartid/inter/ServiceAccess;Lee/cyber/smartid/inter/ListenerAccess;Lee/cyber/smartid/tse/inter/WallClock;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    invoke-static {p0}, Lee/cyber/smartid/util/Log;->getInstance(Ljava/lang/Object;)Lee/cyber/smartid/util/Log;

    move-result-object v0

    iput-object v0, p0, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;->g:Lee/cyber/smartid/util/Log;

    .line 3
    iput-object p1, p0, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;->b:Lee/cyber/smartid/network/SmartIdAPI;

    .line 4
    iput-object p2, p0, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;->c:Lee/cyber/smartid/tse/SmartIdTSE;

    .line 5
    iput-object p3, p0, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;->d:Lee/cyber/smartid/inter/ServiceAccess;

    .line 6
    iput-object p4, p0, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;->e:Lee/cyber/smartid/inter/ListenerAccess;

    .line 7
    iput-object p5, p0, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;->f:Lee/cyber/smartid/tse/inter/WallClock;

    return-void
.end method

.method static synthetic a(Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;)Lee/cyber/smartid/inter/ServiceAccess;
    .locals 0

    .line 1
    iget-object p0, p0, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;->d:Lee/cyber/smartid/inter/ServiceAccess;

    return-object p0
.end method

.method private a(Lee/cyber/smartid/dto/jsonrpc/Transaction;)Ljava/lang/String;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lee/cyber/smartid/dto/UnknownKeyTypeException;
        }
    .end annotation

    .line 42
    invoke-virtual {p1}, Lee/cyber/smartid/dto/jsonrpc/Transaction;->getTransactionType()Ljava/lang/String;

    move-result-object v0

    const-string v1, "SIGNATURE"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-object v1

    .line 43
    :cond_0
    invoke-virtual {p1}, Lee/cyber/smartid/dto/jsonrpc/Transaction;->getTransactionType()Ljava/lang/String;

    move-result-object v0

    const-string v1, "AUTHENTICATION"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    return-object v1

    .line 44
    :cond_1
    new-instance v0, Lee/cyber/smartid/dto/UnknownKeyTypeException;

    iget-object v1, p0, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;->d:Lee/cyber/smartid/inter/ServiceAccess;

    invoke-interface {v1}, Lee/cyber/smartid/inter/ServiceAccess;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    sget v2, Lee/cyber/smartid/R$string;->err_unknown_transaction_type:I

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {p1}, Lee/cyber/smartid/dto/jsonrpc/Transaction;->getTransactionType()Ljava/lang/String;

    move-result-object p1

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Lee/cyber/smartid/dto/UnknownKeyTypeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private a(Lee/cyber/smartid/dto/jsonrpc/resp/GetTransactionResp;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .line 9
    iget-object v0, p0, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;->d:Lee/cyber/smartid/inter/ServiceAccess;

    new-instance v1, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$6;

    invoke-direct {v1, p0, p3, p2, p1}, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$6;-><init>(Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/dto/jsonrpc/resp/GetTransactionResp;)V

    invoke-interface {v0, v1}, Lee/cyber/smartid/inter/ServiceAccess;->notifyUI(Ljava/lang/Runnable;)V

    return-void
.end method

.method private a(Lee/cyber/smartid/dto/jsonrpc/result/GetPendingOperationResult;Lee/cyber/smartid/dto/jsonrpc/resp/GetTransactionResp;Ljava/lang/String;Ljava/lang/String;)V
    .locals 8

    .line 7
    iget-object v0, p0, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;->d:Lee/cyber/smartid/inter/ServiceAccess;

    new-instance v7, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$2;

    move-object v1, v7

    move-object v2, p0

    move-object v3, p3

    move-object v4, p1

    move-object v5, p2

    move-object v6, p4

    invoke-direct/range {v1 .. v6}, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$2;-><init>(Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;Ljava/lang/String;Lee/cyber/smartid/dto/jsonrpc/result/GetPendingOperationResult;Lee/cyber/smartid/dto/jsonrpc/resp/GetTransactionResp;Ljava/lang/String;)V

    invoke-interface {v0, v7}, Lee/cyber/smartid/inter/ServiceAccess;->notifyUI(Ljava/lang/Runnable;)V

    return-void
.end method

.method private a(Lee/cyber/smartid/dto/jsonrpc/result/TransactionContainerResult;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lee/cyber/smartid/cryptolib/dto/StorageException;
        }
    .end annotation

    if-nez p1, :cond_0

    return-void

    .line 38
    :cond_0
    iget-object v0, p0, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;->d:Lee/cyber/smartid/inter/ServiceAccess;

    invoke-interface {v0}, Lee/cyber/smartid/inter/ServiceAccess;->getTransactionCacheManager()Lee/cyber/smartid/manager/inter/TransactionCacheManager;

    move-result-object v0

    invoke-interface {v0}, Lee/cyber/smartid/manager/inter/TransactionCacheManager;->removeExpiredTransactionsFromCache()V

    .line 39
    iget-object v0, p0, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;->d:Lee/cyber/smartid/inter/ServiceAccess;

    invoke-interface {v0}, Lee/cyber/smartid/inter/ServiceAccess;->getTransactionCacheManager()Lee/cyber/smartid/manager/inter/TransactionCacheManager;

    move-result-object v0

    invoke-virtual {p1}, Lee/cyber/smartid/dto/jsonrpc/result/TransactionContainerResult;->getTransaction()Lee/cyber/smartid/dto/jsonrpc/Transaction;

    move-result-object v1

    invoke-interface {v0, v1}, Lee/cyber/smartid/manager/inter/TransactionCacheManager;->cacheTransaction(Lee/cyber/smartid/dto/jsonrpc/Transaction;)V

    .line 40
    iget-object v0, p0, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;->d:Lee/cyber/smartid/inter/ServiceAccess;

    invoke-interface {v0}, Lee/cyber/smartid/inter/ServiceAccess;->getTransactionCacheManager()Lee/cyber/smartid/manager/inter/TransactionCacheManager;

    move-result-object v0

    invoke-interface {v0}, Lee/cyber/smartid/manager/inter/TransactionCacheManager;->setNextRemoveExpiredTransactionsAlarm()V

    .line 41
    invoke-direct {p0, p1}, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;->b(Lee/cyber/smartid/dto/jsonrpc/result/TransactionContainerResult;)V

    return-void
.end method

.method static synthetic a(Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;Lee/cyber/smartid/dto/jsonrpc/resp/GetTransactionResp;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 5
    invoke-direct {p0, p1, p2, p3}, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;->a(Lee/cyber/smartid/dto/jsonrpc/resp/GetTransactionResp;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic a(Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;Lee/cyber/smartid/dto/jsonrpc/result/GetPendingOperationResult;Lee/cyber/smartid/dto/jsonrpc/resp/GetTransactionResp;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 3
    invoke-direct {p0, p1, p2, p3, p4}, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;->a(Lee/cyber/smartid/dto/jsonrpc/result/GetPendingOperationResult;Lee/cyber/smartid/dto/jsonrpc/resp/GetTransactionResp;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic a(Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;Lee/cyber/smartid/dto/jsonrpc/result/TransactionContainerResult;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lee/cyber/smartid/cryptolib/dto/StorageException;
        }
    .end annotation

    .line 2
    invoke-direct {p0, p1}, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;->a(Lee/cyber/smartid/dto/jsonrpc/result/TransactionContainerResult;)V

    return-void
.end method

.method static synthetic a(Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;Ljava/lang/String;Lee/cyber/smartid/dto/jsonrpc/Transaction;Ljava/lang/String;Lee/cyber/smartid/inter/ConfirmTransactionListener;)V
    .locals 0

    .line 6
    invoke-direct {p0, p1, p2, p3, p4}, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;->a(Ljava/lang/String;Lee/cyber/smartid/dto/jsonrpc/Transaction;Ljava/lang/String;Lee/cyber/smartid/inter/ConfirmTransactionListener;)V

    return-void
.end method

.method static synthetic a(Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;Ljava/lang/String;Lee/cyber/smartid/dto/jsonrpc/resp/GetTransactionResp;)V
    .locals 0

    .line 4
    invoke-direct {p0, p1, p2}, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;->a(Ljava/lang/String;Lee/cyber/smartid/dto/jsonrpc/resp/GetTransactionResp;)V

    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 3

    .line 27
    iget-object v0, p0, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;->g:Lee/cyber/smartid/util/Log;

    const-string v1, "cancelTransactionForWrongVerificationCode: Cancelling the transaction with reason WRONG_VC"

    invoke-virtual {v0, v1}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    .line 28
    new-instance v0, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$14;

    invoke-direct {v0, p0}, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$14;-><init>(Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;)V

    const-string v1, "ee.cyber.smartid.manager.impl.TAG_CANCEL_TRANSACTION_FOR_WRONG_VERIFICATION_CODE"

    const-string v2, "WRONG_VC"

    invoke-direct {p0, v1, p1, v2, v0}, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/inter/CancelTransactionListener;)V

    return-void
.end method

.method private a(Ljava/lang/String;Lee/cyber/smartid/dto/jsonrpc/Transaction;Ljava/lang/String;Lee/cyber/smartid/inter/ConfirmTransactionListener;)V
    .locals 10

    .line 24
    :try_start_0
    invoke-direct {p0, p2}, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;->a(Lee/cyber/smartid/dto/jsonrpc/Transaction;)Ljava/lang/String;

    move-result-object v3
    :try_end_0
    .catch Lee/cyber/smartid/dto/UnknownKeyTypeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 25
    iget-object v0, p0, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;->c:Lee/cyber/smartid/tse/SmartIdTSE;

    iget-object v1, p0, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;->d:Lee/cyber/smartid/inter/ServiceAccess;

    invoke-virtual {p2}, Lee/cyber/smartid/dto/jsonrpc/Transaction;->getAccountUUID()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2, v3}, Lee/cyber/smartid/inter/ServiceAccess;->getKeyId(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2}, Lee/cyber/smartid/dto/jsonrpc/Transaction;->getAccountUUID()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p2}, Lee/cyber/smartid/dto/jsonrpc/Transaction;->getTransactionUUID()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p2}, Lee/cyber/smartid/dto/jsonrpc/Transaction;->getHash()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p2}, Lee/cyber/smartid/dto/jsonrpc/Transaction;->getHashType()Ljava/lang/String;

    move-result-object v7

    iget-object p2, p0, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;->e:Lee/cyber/smartid/inter/ListenerAccess;

    invoke-interface {p2}, Lee/cyber/smartid/inter/ListenerAccess;->getTSEListenerToServiceListenerMapper()Lee/cyber/smartid/manager/inter/TSEListenerToServiceListenerMapper;

    move-result-object p2

    invoke-interface {p2, p4}, Lee/cyber/smartid/manager/inter/TSEListenerToServiceListenerMapper;->map(Lee/cyber/smartid/inter/ServiceListener;)Lee/cyber/smartid/tse/inter/TSEListener;

    move-result-object p2

    move-object v9, p2

    check-cast v9, Lee/cyber/smartid/tse/inter/ConfirmTransactionListener;

    move-object v1, p1

    move-object v8, p3

    invoke-virtual/range {v0 .. v9}, Lee/cyber/smartid/tse/SmartIdTSE;->confirmTransaction(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/tse/inter/ConfirmTransactionListener;)V

    return-void

    :catch_0
    move-exception p2

    .line 26
    iget-object p3, p0, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;->d:Lee/cyber/smartid/inter/ServiceAccess;

    new-instance p4, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$13;

    invoke-direct {p4, p0, p1, p2}, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$13;-><init>(Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;Ljava/lang/String;Lee/cyber/smartid/dto/UnknownKeyTypeException;)V

    invoke-interface {p3, p4}, Lee/cyber/smartid/inter/ServiceAccess;->notifyUI(Ljava/lang/Runnable;)V

    return-void
.end method

.method private a(Ljava/lang/String;Lee/cyber/smartid/dto/jsonrpc/resp/GetTransactionResp;)V
    .locals 2

    .line 8
    iget-object v0, p0, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;->d:Lee/cyber/smartid/inter/ServiceAccess;

    new-instance v1, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$4;

    invoke-direct {v1, p0, p1, p2}, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$4;-><init>(Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;Ljava/lang/String;Lee/cyber/smartid/dto/jsonrpc/resp/GetTransactionResp;)V

    invoke-interface {v0, v1}, Lee/cyber/smartid/inter/ServiceAccess;->notifyUI(Ljava/lang/Runnable;)V

    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/inter/CancelTransactionListener;)V
    .locals 8

    .line 29
    iget-object v0, p0, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;->g:Lee/cyber/smartid/util/Log;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "cancelTransaction - transactionUUID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, ", reason: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    .line 30
    iget-object v0, p0, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;->e:Lee/cyber/smartid/inter/ListenerAccess;

    invoke-interface {v0, p1, p4}, Lee/cyber/smartid/inter/ListenerAccess;->setListener(Ljava/lang/String;Lee/cyber/smartid/inter/ServiceListener;)V

    .line 31
    const-class v6, Lee/cyber/smartid/inter/CancelTransactionListener;

    .line 32
    :try_start_0
    iget-object p4, p0, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;->d:Lee/cyber/smartid/inter/ServiceAccess;

    invoke-interface {p4}, Lee/cyber/smartid/inter/ServiceAccess;->getTransactionFinder()Lee/cyber/smartid/util/TransactionFinder;

    move-result-object p4

    invoke-virtual {p4, p2}, Lee/cyber/smartid/util/TransactionFinder;->findOrThrow(Ljava/lang/String;)Lee/cyber/smartid/util/TransactionFinder;

    move-result-object p4

    invoke-virtual {p4}, Lee/cyber/smartid/util/TransactionFinder;->getTransaction()Lee/cyber/smartid/dto/jsonrpc/Transaction;

    move-result-object p4
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 33
    new-instance v0, Lee/cyber/smartid/dto/jsonrpc/param/CancelTransactionParams;

    invoke-virtual {p4}, Lee/cyber/smartid/dto/jsonrpc/Transaction;->getAccountUUID()Ljava/lang/String;

    move-result-object p4

    invoke-direct {v0, p2, p4, p3}, Lee/cyber/smartid/dto/jsonrpc/param/CancelTransactionParams;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 34
    new-instance v3, Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCRequest;

    const-string p3, "cancelTransaction"

    invoke-direct {v3, p3, v0}, Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCRequest;-><init>(Ljava/lang/String;Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCParams;)V

    .line 35
    iget-object p3, p0, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;->b:Lee/cyber/smartid/network/SmartIdAPI;

    invoke-interface {p3, v3}, Lee/cyber/smartid/network/SmartIdAPI;->cancelTransaction(Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCRequest;)Lc/b;

    move-result-object p3

    .line 36
    new-instance p4, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$16;

    iget-object v4, p0, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;->c:Lee/cyber/smartid/tse/SmartIdTSE;

    move-object v1, p4

    move-object v2, p0

    move-object v5, p1

    move-object v7, p2

    invoke-direct/range {v1 .. v7}, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$16;-><init>(Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCRequest;Lee/cyber/smartid/tse/SmartIdTSE;Ljava/lang/String;Ljava/lang/Class;Ljava/lang/String;)V

    invoke-interface {p3, p4}, Lc/b;->a(Lc/d;)V

    return-void

    :catch_0
    move-exception p2

    .line 37
    iget-object p3, p0, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;->d:Lee/cyber/smartid/inter/ServiceAccess;

    new-instance p4, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$15;

    invoke-direct {p4, p0, p1, v6, p2}, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$15;-><init>(Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Throwable;)V

    invoke-interface {p3, p4}, Lee/cyber/smartid/inter/ServiceAccess;->notifyUI(Ljava/lang/Runnable;)V

    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/inter/ConfirmTransactionListener;)V
    .locals 10

    .line 20
    const-class v7, Lee/cyber/smartid/inter/ConfirmTransactionListener;

    .line 21
    :try_start_0
    iget-object v0, p0, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;->d:Lee/cyber/smartid/inter/ServiceAccess;

    invoke-interface {v0}, Lee/cyber/smartid/inter/ServiceAccess;->getTransactionFinder()Lee/cyber/smartid/util/TransactionFinder;

    move-result-object v0

    invoke-virtual {v0, p2}, Lee/cyber/smartid/util/TransactionFinder;->findOrThrow(Ljava/lang/String;)Lee/cyber/smartid/util/TransactionFinder;

    move-result-object v0

    invoke-virtual {v0}, Lee/cyber/smartid/util/TransactionFinder;->getTransaction()Lee/cyber/smartid/dto/jsonrpc/Transaction;

    move-result-object v4
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 22
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ee.cyber.smartid.manager.impl.TAG_VERIFY_TRANSACTION_VERIFICATION_CODE_FOR_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    new-instance v9, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$12;

    move-object v0, v9

    move-object v1, p0

    move-object v2, p4

    move-object v3, p1

    move-object v5, p3

    move-object v6, p5

    invoke-direct/range {v0 .. v7}, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$12;-><init>(Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/dto/jsonrpc/Transaction;Ljava/lang/String;Lee/cyber/smartid/inter/ConfirmTransactionListener;Ljava/lang/Class;)V

    invoke-virtual {p0, v8, p2, p4, v9}, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;->verifyTransactionVerificationCode(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/inter/VerifyTransactionVerificationCodeListener;)V

    return-void

    :catch_0
    move-exception p2

    .line 23
    iget-object p3, p0, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;->d:Lee/cyber/smartid/inter/ServiceAccess;

    new-instance p4, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$11;

    invoke-direct {p4, p0, p1, v7, p2}, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$11;-><init>(Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Throwable;)V

    invoke-interface {p3, p4}, Lee/cyber/smartid/inter/ServiceAccess;->notifyUI(Ljava/lang/Runnable;)V

    return-void
.end method

.method static synthetic b(Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;)Lee/cyber/smartid/util/Log;
    .locals 0

    .line 1
    iget-object p0, p0, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;->g:Lee/cyber/smartid/util/Log;

    return-object p0
.end method

.method private b(Lee/cyber/smartid/dto/jsonrpc/result/TransactionContainerResult;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lee/cyber/smartid/cryptolib/dto/StorageException;
        }
    .end annotation

    if-eqz p1, :cond_1

    .line 2
    invoke-virtual {p1}, Lee/cyber/smartid/dto/jsonrpc/result/TransactionContainerResult;->getTransaction()Lee/cyber/smartid/dto/jsonrpc/Transaction;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lee/cyber/smartid/dto/jsonrpc/result/TransactionContainerResult;->getFreshnessToken()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 3
    :cond_0
    :try_start_0
    invoke-virtual {p1}, Lee/cyber/smartid/dto/jsonrpc/result/TransactionContainerResult;->getTransaction()Lee/cyber/smartid/dto/jsonrpc/Transaction;

    move-result-object v0

    invoke-direct {p0, v0}, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;->a(Lee/cyber/smartid/dto/jsonrpc/Transaction;)Ljava/lang/String;

    move-result-object v0
    :try_end_0
    .catch Lee/cyber/smartid/dto/UnknownKeyTypeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 4
    iget-object v1, p0, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;->c:Lee/cyber/smartid/tse/SmartIdTSE;

    iget-object v2, p0, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;->d:Lee/cyber/smartid/inter/ServiceAccess;

    invoke-virtual {p1}, Lee/cyber/smartid/dto/jsonrpc/result/TransactionContainerResult;->getTransaction()Lee/cyber/smartid/dto/jsonrpc/Transaction;

    move-result-object v3

    invoke-virtual {v3}, Lee/cyber/smartid/dto/jsonrpc/Transaction;->getAccountUUID()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3, v0}, Lee/cyber/smartid/inter/ServiceAccess;->getKeyId(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lee/cyber/smartid/dto/jsonrpc/result/TransactionContainerResult;->getFreshnessToken()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, v0, p1}, Lee/cyber/smartid/tse/SmartIdTSE;->updateFreshnessToken(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    :catch_0
    move-exception p1

    .line 5
    iget-object v0, p0, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;->g:Lee/cyber/smartid/util/Log;

    const-string v1, "setFreshnessTokenFromTransaction"

    invoke-virtual {v0, v1, p1}, Lee/cyber/smartid/util/Log;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_1
    :goto_0
    return-void
.end method

.method static synthetic c(Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;)Lee/cyber/smartid/inter/ListenerAccess;
    .locals 0

    .line 1
    iget-object p0, p0, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;->e:Lee/cyber/smartid/inter/ListenerAccess;

    return-object p0
.end method

.method static synthetic d(Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;)Lee/cyber/smartid/tse/inter/WallClock;
    .locals 0

    .line 1
    iget-object p0, p0, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;->f:Lee/cyber/smartid/tse/inter/WallClock;

    return-object p0
.end method

.method public static getInstance(Lee/cyber/smartid/network/SmartIdAPI;Lee/cyber/smartid/tse/SmartIdTSE;Lee/cyber/smartid/inter/ServiceAccess;Lee/cyber/smartid/inter/ListenerAccess;Lee/cyber/smartid/tse/inter/WallClock;)Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;
    .locals 8

    .line 1
    sget-object v0, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;->a:Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;

    if-nez v0, :cond_0

    .line 2
    const-class v0, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;

    monitor-enter v0

    .line 3
    :try_start_0
    new-instance v7, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;

    move-object v1, v7

    move-object v2, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    move-object v6, p4

    invoke-direct/range {v1 .. v6}, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;-><init>(Lee/cyber/smartid/network/SmartIdAPI;Lee/cyber/smartid/tse/SmartIdTSE;Lee/cyber/smartid/inter/ServiceAccess;Lee/cyber/smartid/inter/ListenerAccess;Lee/cyber/smartid/tse/inter/WallClock;)V

    sput-object v7, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;->a:Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;

    .line 4
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception p0

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p0

    .line 5
    :cond_0
    :goto_0
    sget-object p0, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;->a:Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;

    return-object p0
.end method


# virtual methods
.method a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 5

    .line 10
    const-class v0, Lee/cyber/smartid/inter/VerifyTransactionVerificationCodeListener;

    .line 11
    :try_start_0
    iget-object v1, p0, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;->d:Lee/cyber/smartid/inter/ServiceAccess;

    invoke-interface {v1}, Lee/cyber/smartid/inter/ServiceAccess;->getTransactionFinder()Lee/cyber/smartid/util/TransactionFinder;

    move-result-object v1

    invoke-virtual {v1, p2}, Lee/cyber/smartid/util/TransactionFinder;->findOrThrow(Ljava/lang/String;)Lee/cyber/smartid/util/TransactionFinder;

    move-result-object v1

    invoke-virtual {v1}, Lee/cyber/smartid/util/TransactionFinder;->getTransaction()Lee/cyber/smartid/dto/jsonrpc/Transaction;

    move-result-object v1
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1

    .line 12
    :try_start_1
    iget-object v2, p0, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;->d:Lee/cyber/smartid/inter/ServiceAccess;

    invoke-interface {v2}, Lee/cyber/smartid/inter/ServiceAccess;->getTransactionRespMapper()Lee/cyber/smartid/manager/inter/TransactionRespMapper;

    move-result-object v2

    invoke-interface {v2, p3, v1}, Lee/cyber/smartid/manager/inter/TransactionRespMapper;->isValidVerificationCodeMapping(Ljava/lang/String;Lee/cyber/smartid/dto/jsonrpc/Transaction;)Z

    move-result v1
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    .line 13
    new-instance v2, Lee/cyber/smartid/dto/jsonrpc/resp/VerifyTransactionVerificationCodeResp;

    invoke-direct {v2, p1, p3, v1}, Lee/cyber/smartid/dto/jsonrpc/resp/VerifyTransactionVerificationCodeResp;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 14
    invoke-virtual {v2}, Lee/cyber/smartid/dto/jsonrpc/resp/VerifyTransactionVerificationCodeResp;->isCorrectVerificationCode()Z

    move-result v1

    if-nez v1, :cond_0

    .line 15
    iget-object v1, p0, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;->g:Lee/cyber/smartid/util/Log;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "verifyTransactionVerificationCode - verification code "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p3, " is not valid, canceling the Transaction .."

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p3

    invoke-virtual {v1, p3}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    .line 16
    invoke-direct {p0, p2}, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;->a(Ljava/lang/String;)V

    .line 17
    :cond_0
    iget-object p2, p0, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;->d:Lee/cyber/smartid/inter/ServiceAccess;

    new-instance p3, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$10;

    invoke-direct {p3, p0, p1, v0, v2}, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$10;-><init>(Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;Ljava/lang/String;Ljava/lang/Class;Lee/cyber/smartid/dto/jsonrpc/resp/VerifyTransactionVerificationCodeResp;)V

    invoke-interface {p2, p3}, Lee/cyber/smartid/inter/ServiceAccess;->notifyUI(Ljava/lang/Runnable;)V

    return-void

    :catch_0
    move-exception p2

    .line 18
    iget-object p3, p0, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;->d:Lee/cyber/smartid/inter/ServiceAccess;

    new-instance v1, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$9;

    invoke-direct {v1, p0, p1, v0, p2}, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$9;-><init>(Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Throwable;)V

    invoke-interface {p3, v1}, Lee/cyber/smartid/inter/ServiceAccess;->notifyUI(Ljava/lang/Runnable;)V

    return-void

    :catch_1
    move-exception p2

    .line 19
    iget-object p3, p0, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;->d:Lee/cyber/smartid/inter/ServiceAccess;

    new-instance v1, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$8;

    invoke-direct {v1, p0, p1, v0, p2}, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$8;-><init>(Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Throwable;)V

    invoke-interface {p3, v1}, Lee/cyber/smartid/inter/ServiceAccess;->notifyUI(Ljava/lang/Runnable;)V

    return-void
.end method

.method public cancelRPRequest(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/inter/CancelRPRequestListener;)V
    .locals 8

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;->e:Lee/cyber/smartid/inter/ListenerAccess;

    invoke-interface {v0, p1, p4}, Lee/cyber/smartid/inter/ListenerAccess;->setListener(Ljava/lang/String;Lee/cyber/smartid/inter/ServiceListener;)V

    .line 2
    new-instance p4, Lee/cyber/smartid/dto/jsonrpc/param/CancelRPRequestParams;

    invoke-direct {p4, p3, p2}, Lee/cyber/smartid/dto/jsonrpc/param/CancelRPRequestParams;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 3
    new-instance v2, Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCRequest;

    const-string v0, "cancelRpRequest"

    invoke-direct {v2, v0, p4}, Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCRequest;-><init>(Ljava/lang/String;Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCParams;)V

    .line 4
    iget-object p4, p0, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;->b:Lee/cyber/smartid/network/SmartIdAPI;

    invoke-interface {p4, v2}, Lee/cyber/smartid/network/SmartIdAPI;->cancelRPRequest(Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCRequest;)Lc/b;

    move-result-object p4

    .line 5
    new-instance v7, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$18;

    iget-object v3, p0, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;->c:Lee/cyber/smartid/tse/SmartIdTSE;

    move-object v0, v7

    move-object v1, p0

    move-object v4, p1

    move-object v5, p3

    move-object v6, p2

    invoke-direct/range {v0 .. v6}, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$18;-><init>(Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCRequest;Lee/cyber/smartid/tse/SmartIdTSE;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {p4, v7}, Lc/b;->a(Lc/d;)V

    return-void
.end method

.method public cancelTransaction(Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/inter/CancelTransactionListener;)V
    .locals 1

    const-string v0, "USER_REFUSED"

    .line 1
    invoke-direct {p0, p1, p2, v0, p3}, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/inter/CancelTransactionListener;)V

    return-void
.end method

.method public confirmRPRequest(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/inter/ConfirmRPRequestListener;)V
    .locals 8

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;->e:Lee/cyber/smartid/inter/ListenerAccess;

    invoke-interface {v0, p1, p4}, Lee/cyber/smartid/inter/ListenerAccess;->setListener(Ljava/lang/String;Lee/cyber/smartid/inter/ServiceListener;)V

    .line 2
    new-instance p4, Lee/cyber/smartid/dto/jsonrpc/param/ConfirmRPRequestParams;

    invoke-direct {p4, p2, p3}, Lee/cyber/smartid/dto/jsonrpc/param/ConfirmRPRequestParams;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 3
    new-instance v2, Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCRequest;

    const-string v0, "confirmRpRequest"

    invoke-direct {v2, v0, p4}, Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCRequest;-><init>(Ljava/lang/String;Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCParams;)V

    .line 4
    iget-object p4, p0, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;->b:Lee/cyber/smartid/network/SmartIdAPI;

    invoke-interface {p4, v2}, Lee/cyber/smartid/network/SmartIdAPI;->confirmRPRequest(Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCRequest;)Lc/b;

    move-result-object p4

    .line 5
    new-instance v7, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$19;

    iget-object v3, p0, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;->c:Lee/cyber/smartid/tse/SmartIdTSE;

    move-object v0, v7

    move-object v1, p0

    move-object v4, p1

    move-object v5, p2

    move-object v6, p3

    invoke-direct/range {v0 .. v6}, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$19;-><init>(Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCRequest;Lee/cyber/smartid/tse/SmartIdTSE;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {p4, v7}, Lc/b;->a(Lc/d;)V

    return-void
.end method

.method public confirmTransaction(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/inter/ConfirmTransactionListener;)V
    .locals 6

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;->g:Lee/cyber/smartid/util/Log;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "confirmTransaction - transactionUUID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    .line 2
    iget-object v0, p0, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;->e:Lee/cyber/smartid/inter/ListenerAccess;

    invoke-interface {v0, p1, p5}, Lee/cyber/smartid/inter/ListenerAccess;->setListener(Ljava/lang/String;Lee/cyber/smartid/inter/ServiceListener;)V

    move-object v0, p0

    move-object v1, p1

    move-object v2, p3

    move-object v3, p2

    move-object v4, p4

    move-object v5, p5

    .line 3
    invoke-direct/range {v0 .. v5}, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/inter/ConfirmTransactionListener;)V

    return-void
.end method

.method public createTransactionForRPRequest(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/inter/CreateTransactionForRPRequestListener;)V
    .locals 6

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;->e:Lee/cyber/smartid/inter/ListenerAccess;

    invoke-interface {v0, p1, p4}, Lee/cyber/smartid/inter/ListenerAccess;->setListener(Ljava/lang/String;Lee/cyber/smartid/inter/ServiceListener;)V

    .line 2
    new-instance p4, Lee/cyber/smartid/dto/jsonrpc/param/CreateTransactionForRPRequestParams;

    invoke-direct {p4, p2, p3}, Lee/cyber/smartid/dto/jsonrpc/param/CreateTransactionForRPRequestParams;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 3
    new-instance v2, Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCRequest;

    const-string p3, "createTransactionForRpRequest"

    invoke-direct {v2, p3, p4}, Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCRequest;-><init>(Ljava/lang/String;Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCParams;)V

    .line 4
    iget-object p3, p0, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;->b:Lee/cyber/smartid/network/SmartIdAPI;

    invoke-interface {p3, v2}, Lee/cyber/smartid/network/SmartIdAPI;->createTransactionForRpRequest(Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCRequest;)Lc/b;

    move-result-object p3

    .line 5
    new-instance p4, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$5;

    iget-object v3, p0, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;->c:Lee/cyber/smartid/tse/SmartIdTSE;

    move-object v0, p4

    move-object v1, p0

    move-object v4, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$5;-><init>(Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCRequest;Lee/cyber/smartid/tse/SmartIdTSE;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {p3, p4}, Lc/b;->a(Lc/d;)V

    return-void
.end method

.method public getPendingOperation(Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/inter/GetPendingOperationListener;)V
    .locals 7

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;->g:Lee/cyber/smartid/util/Log;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getPendingOperation - tag "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    .line 2
    iget-object v0, p0, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;->e:Lee/cyber/smartid/inter/ListenerAccess;

    invoke-interface {v0, p1, p3}, Lee/cyber/smartid/inter/ListenerAccess;->setListener(Ljava/lang/String;Lee/cyber/smartid/inter/ServiceListener;)V

    .line 3
    new-instance p3, Lee/cyber/smartid/dto/jsonrpc/param/GetPendingOperationParams;

    invoke-direct {p3, p2}, Lee/cyber/smartid/dto/jsonrpc/param/GetPendingOperationParams;-><init>(Ljava/lang/String;)V

    .line 4
    new-instance v2, Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCRequest;

    const-string v0, "getPendingOperation"

    invoke-direct {v2, v0, p3}, Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCRequest;-><init>(Ljava/lang/String;Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCParams;)V

    .line 5
    iget-object p3, p0, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;->b:Lee/cyber/smartid/network/SmartIdAPI;

    invoke-interface {p3, v2}, Lee/cyber/smartid/network/SmartIdAPI;->getLastTransaction(Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCRequest;)Lc/b;

    move-result-object p3

    .line 6
    new-instance v6, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$1;

    iget-object v3, p0, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;->c:Lee/cyber/smartid/tse/SmartIdTSE;

    move-object v0, v6

    move-object v1, p0

    move-object v4, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$1;-><init>(Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCRequest;Lee/cyber/smartid/tse/SmartIdTSE;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {p3, v6}, Lc/b;->a(Lc/d;)V

    return-void
.end method

.method public getRPRequest(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/inter/GetRPRequestListener;)V
    .locals 6

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;->e:Lee/cyber/smartid/inter/ListenerAccess;

    invoke-interface {v0, p1, p4}, Lee/cyber/smartid/inter/ListenerAccess;->setListener(Ljava/lang/String;Lee/cyber/smartid/inter/ServiceListener;)V

    .line 2
    new-instance p4, Lee/cyber/smartid/dto/jsonrpc/param/GetRPRequestParams;

    invoke-direct {p4, p2, p3}, Lee/cyber/smartid/dto/jsonrpc/param/GetRPRequestParams;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 3
    new-instance v2, Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCRequest;

    const-string p2, "getRpRequest"

    invoke-direct {v2, p2, p4}, Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCRequest;-><init>(Ljava/lang/String;Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCParams;)V

    .line 4
    iget-object p2, p0, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;->b:Lee/cyber/smartid/network/SmartIdAPI;

    invoke-interface {p2, v2}, Lee/cyber/smartid/network/SmartIdAPI;->getRPRequest(Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCRequest;)Lc/b;

    move-result-object p2

    .line 5
    new-instance p4, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$17;

    iget-object v3, p0, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;->c:Lee/cyber/smartid/tse/SmartIdTSE;

    move-object v0, p4

    move-object v1, p0

    move-object v4, p1

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$17;-><init>(Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCRequest;Lee/cyber/smartid/tse/SmartIdTSE;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {p2, p4}, Lc/b;->a(Lc/d;)V

    return-void
.end method

.method public getTransaction(Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/inter/GetTransactionListener;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;->e:Lee/cyber/smartid/inter/ListenerAccess;

    invoke-interface {v0, p1, p3}, Lee/cyber/smartid/inter/ListenerAccess;->setListener(Ljava/lang/String;Lee/cyber/smartid/inter/ServiceListener;)V

    .line 2
    new-instance p3, Lee/cyber/smartid/dto/jsonrpc/param/GetTransactionParams;

    invoke-direct {p3, p2}, Lee/cyber/smartid/dto/jsonrpc/param/GetTransactionParams;-><init>(Ljava/lang/String;)V

    .line 3
    new-instance p2, Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCRequest;

    const-string v0, "getTransaction"

    invoke-direct {p2, v0, p3}, Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCRequest;-><init>(Ljava/lang/String;Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCParams;)V

    .line 4
    iget-object p3, p0, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;->b:Lee/cyber/smartid/network/SmartIdAPI;

    invoke-interface {p3, p2}, Lee/cyber/smartid/network/SmartIdAPI;->getTransaction(Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCRequest;)Lc/b;

    move-result-object p3

    .line 5
    new-instance v0, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$3;

    iget-object v1, p0, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;->c:Lee/cyber/smartid/tse/SmartIdTSE;

    invoke-direct {v0, p0, p2, v1, p1}, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$3;-><init>(Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCRequest;Lee/cyber/smartid/tse/SmartIdTSE;Ljava/lang/String;)V

    invoke-interface {p3, v0}, Lc/b;->a(Lc/d;)V

    return-void
.end method

.method public verifyTransactionVerificationCode(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/inter/VerifyTransactionVerificationCodeListener;)V
    .locals 3

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;->g:Lee/cyber/smartid/util/Log;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "verifyTransactionVerificationCode - transactionUUID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, ", code: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    .line 2
    iget-object v0, p0, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;->e:Lee/cyber/smartid/inter/ListenerAccess;

    invoke-interface {v0, p1, p4}, Lee/cyber/smartid/inter/ListenerAccess;->setListener(Ljava/lang/String;Lee/cyber/smartid/inter/ServiceListener;)V

    .line 3
    new-instance p4, Ljava/lang/Thread;

    new-instance v0, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$7;

    invoke-direct {v0, p0, p1, p2, p3}, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$7;-><init>(Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p4, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 4
    invoke-virtual {p4}, Ljava/lang/Thread;->start()V

    return-void
.end method
