.class Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$16;
.super Lee/cyber/smartid/tse/network/RPCCallbackNonUIThread;
.source "TransactionAndRPRequestManagerImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/inter/CancelTransactionListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lee/cyber/smartid/tse/network/RPCCallbackNonUIThread<",
        "Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCResponse<",
        "Lee/cyber/smartid/dto/jsonrpc/result/CancelTransactionResult;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:Ljava/lang/Class;

.field final synthetic c:Ljava/lang/String;

.field final synthetic d:Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;


# direct methods
.method constructor <init>(Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCRequest;Lee/cyber/smartid/tse/SmartIdTSE;Ljava/lang/String;Ljava/lang/Class;Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$16;->d:Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;

    iput-object p4, p0, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$16;->a:Ljava/lang/String;

    iput-object p5, p0, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$16;->b:Ljava/lang/Class;

    iput-object p6, p0, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$16;->c:Ljava/lang/String;

    invoke-direct {p0, p2, p3}, Lee/cyber/smartid/tse/network/RPCCallbackNonUIThread;-><init>(Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCRequest;Lee/cyber/smartid/tse/SmartIdTSE;)V

    return-void
.end method


# virtual methods
.method public onFailure(Lc/b;Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCError;Ljava/lang/Throwable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lc/b<",
            "Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCResponse<",
            "Lee/cyber/smartid/dto/jsonrpc/result/CancelTransactionResult;",
            ">;>;",
            "Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCError;",
            "Ljava/lang/Throwable;",
            ")V"
        }
    .end annotation

    .line 1
    iget-object p1, p0, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$16;->d:Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;

    invoke-static {p1}, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;->a(Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;)Lee/cyber/smartid/inter/ServiceAccess;

    move-result-object p1

    invoke-interface {p1}, Lee/cyber/smartid/inter/ServiceAccess;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    iget-object v0, p0, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$16;->d:Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;

    invoke-static {v0}, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;->d(Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;)Lee/cyber/smartid/tse/inter/WallClock;

    move-result-object v0

    invoke-static {p1, v0, p2, p3}, Lee/cyber/smartid/dto/SmartIdError;->from(Landroid/content/Context;Lee/cyber/smartid/tse/inter/WallClock;Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCError;Ljava/lang/Throwable;)Lee/cyber/smartid/dto/SmartIdError;

    move-result-object p1

    .line 2
    iget-object p2, p0, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$16;->d:Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;

    invoke-static {p2}, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;->a(Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;)Lee/cyber/smartid/inter/ServiceAccess;

    move-result-object p2

    new-instance p3, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$16$2;

    invoke-direct {p3, p0, p1}, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$16$2;-><init>(Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$16;Lee/cyber/smartid/dto/SmartIdError;)V

    invoke-interface {p2, p3}, Lee/cyber/smartid/inter/ServiceAccess;->notifyUI(Ljava/lang/Runnable;)V

    return-void
.end method

.method public onSuccess(Lc/b;Lc/r;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lc/b<",
            "Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCResponse<",
            "Lee/cyber/smartid/dto/jsonrpc/result/CancelTransactionResult;",
            ">;>;",
            "Lc/r<",
            "Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCResponse<",
            "Lee/cyber/smartid/dto/jsonrpc/result/CancelTransactionResult;",
            ">;>;)V"
        }
    .end annotation

    .line 1
    iget-object p1, p0, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$16;->d:Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;

    invoke-static {p1}, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;->a(Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl;)Lee/cyber/smartid/inter/ServiceAccess;

    move-result-object p1

    new-instance p2, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$16$1;

    invoke-direct {p2, p0}, Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$16$1;-><init>(Lee/cyber/smartid/manager/impl/TransactionAndRPRequestManagerImpl$16;)V

    invoke-interface {p1, p2}, Lee/cyber/smartid/inter/ServiceAccess;->notifyUI(Ljava/lang/Runnable;)V

    return-void
.end method

.method public onValidate(Lc/b;Lc/r;)Z
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lc/b<",
            "Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCResponse<",
            "Lee/cyber/smartid/dto/jsonrpc/result/CancelTransactionResult;",
            ">;>;",
            "Lc/r<",
            "Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCResponse<",
            "Lee/cyber/smartid/dto/jsonrpc/result/CancelTransactionResult;",
            ">;>;)Z"
        }
    .end annotation

    .line 1
    invoke-static {p2}, Lee/cyber/smartid/util/Util;->validateResponse(Lc/r;)Z

    move-result p1

    return p1
.end method
