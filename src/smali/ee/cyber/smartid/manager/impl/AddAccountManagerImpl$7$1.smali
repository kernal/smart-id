.class Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$7$1;
.super Ljava/lang/Object;
.source "AddAccountManagerImpl.java"

# interfaces
.implements Lee/cyber/smartid/tse/inter/InitializeKeyAndKeyStatesListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$7;->a(Lee/cyber/smartid/tse/dto/jsonrpc/resp/BaseResp;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lee/cyber/smartid/tse/dto/jsonrpc/resp/BaseResp;

.field final synthetic b:Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$7;


# direct methods
.method constructor <init>(Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$7;Lee/cyber/smartid/tse/dto/jsonrpc/resp/BaseResp;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$7$1;->b:Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$7;

    iput-object p2, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$7$1;->a:Lee/cyber/smartid/tse/dto/jsonrpc/resp/BaseResp;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onInitializeKeyAndKeyStatesFailed(Ljava/lang/String;Lee/cyber/smartid/tse/dto/TSEError;)V
    .locals 3

    .line 1
    iget-object p1, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$7$1;->b:Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$7;

    iget-object p1, p1, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$7;->f:Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;

    invoke-static {p1}, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;->d(Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;)Lee/cyber/smartid/util/Log;

    move-result-object p1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "initializeKeysAndAccountForAddAccount sign onInitializeKeyAndKeyStatesFailed: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lee/cyber/smartid/util/Log;->e(Ljava/lang/String;)V

    .line 2
    iget-object p1, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$7$1;->b:Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$7;

    iget-object v0, p1, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$7;->f:Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;

    iget-object p1, p1, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$7;->a:Ljava/lang/String;

    invoke-static {v0}, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;->c(Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;)Lee/cyber/smartid/inter/ServiceAccess;

    move-result-object v1

    invoke-interface {v1}, Lee/cyber/smartid/inter/ServiceAccess;->getTseErrorToServiceErrorMapper()Lee/cyber/smartid/manager/inter/TSEErrorToServiceErrorMapper;

    move-result-object v1

    invoke-interface {v1, p2}, Lee/cyber/smartid/manager/inter/TSEErrorToServiceErrorMapper;->map(Lee/cyber/smartid/tse/dto/BaseError;)Lee/cyber/smartid/dto/SmartIdError;

    move-result-object p2

    iget-object v1, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$7$1;->b:Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$7;

    iget-object v1, v1, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$7;->b:Lee/cyber/smartid/dto/jsonrpc/result/RegisterAccountResult;

    invoke-virtual {v1}, Lee/cyber/smartid/dto/jsonrpc/result/RegisterAccountResult;->getAccountUUID()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {v0, p1, p2, v1, v2}, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;->a(Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;Ljava/lang/String;Lee/cyber/smartid/dto/SmartIdError;Ljava/lang/String;Z)V

    return-void
.end method

.method public onInitializeKeyAndKeyStatesSuccess(Ljava/lang/String;)V
    .locals 2

    .line 1
    iget-object p1, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$7$1;->b:Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$7;

    iget-object p1, p1, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$7;->f:Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;

    invoke-static {p1}, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;->d(Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;)Lee/cyber/smartid/util/Log;

    move-result-object p1

    const-string v0, "onInitializeKeyAndKeyStatesSuccess old callback called for SIGN"

    invoke-virtual {p1, v0}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    .line 2
    iget-object p1, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$7$1;->b:Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$7;

    iget-object v0, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$7$1;->a:Lee/cyber/smartid/tse/dto/jsonrpc/resp/BaseResp;

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$7;->a(Lee/cyber/smartid/tse/dto/jsonrpc/resp/BaseResp;Lee/cyber/smartid/tse/dto/jsonrpc/resp/BaseResp;)V

    return-void
.end method

.method public onInitializeKeyAndKeyStatesSuccess(Ljava/lang/String;Lee/cyber/smartid/tse/dto/jsonrpc/resp/BaseResp;)V
    .locals 1

    .line 3
    iget-object p1, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$7$1;->b:Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$7;

    iget-object p1, p1, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$7;->f:Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;

    invoke-static {p1}, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;->d(Lee/cyber/smartid/manager/impl/AddAccountManagerImpl;)Lee/cyber/smartid/util/Log;

    move-result-object p1

    const-string v0, "onInitializeKeyAndKeyStatesSuccess new callback called for SIGN"

    invoke-virtual {p1, v0}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    .line 4
    iget-object p1, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$7$1;->b:Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$7;

    iget-object v0, p0, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$7$1;->a:Lee/cyber/smartid/tse/dto/jsonrpc/resp/BaseResp;

    invoke-virtual {p1, v0, p2}, Lee/cyber/smartid/manager/impl/AddAccountManagerImpl$7;->a(Lee/cyber/smartid/tse/dto/jsonrpc/resp/BaseResp;Lee/cyber/smartid/tse/dto/jsonrpc/resp/BaseResp;)V

    return-void
.end method
