.class Lee/cyber/smartid/manager/impl/DeleteAccountManagerImpl$3;
.super Ljava/lang/Object;
.source "DeleteAccountManagerImpl.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lee/cyber/smartid/manager/impl/DeleteAccountManagerImpl;->a(Ljava/lang/String;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:Ljava/lang/String;

.field final synthetic c:Ljava/util/ArrayList;

.field final synthetic d:Lee/cyber/smartid/manager/impl/DeleteAccountManagerImpl;


# direct methods
.method constructor <init>(Lee/cyber/smartid/manager/impl/DeleteAccountManagerImpl;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lee/cyber/smartid/manager/impl/DeleteAccountManagerImpl$3;->d:Lee/cyber/smartid/manager/impl/DeleteAccountManagerImpl;

    iput-object p2, p0, Lee/cyber/smartid/manager/impl/DeleteAccountManagerImpl$3;->a:Ljava/lang/String;

    iput-object p3, p0, Lee/cyber/smartid/manager/impl/DeleteAccountManagerImpl$3;->b:Ljava/lang/String;

    iput-object p4, p0, Lee/cyber/smartid/manager/impl/DeleteAccountManagerImpl$3;->c:Ljava/util/ArrayList;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/manager/impl/DeleteAccountManagerImpl$3;->d:Lee/cyber/smartid/manager/impl/DeleteAccountManagerImpl;

    invoke-static {v0}, Lee/cyber/smartid/manager/impl/DeleteAccountManagerImpl;->b(Lee/cyber/smartid/manager/impl/DeleteAccountManagerImpl;)Lee/cyber/smartid/inter/ListenerAccess;

    move-result-object v0

    iget-object v1, p0, Lee/cyber/smartid/manager/impl/DeleteAccountManagerImpl$3;->a:Ljava/lang/String;

    const-class v2, Lee/cyber/smartid/inter/DeleteAccountListener;

    const/4 v3, 0x1

    invoke-interface {v0, v1, v3, v2}, Lee/cyber/smartid/inter/ListenerAccess;->getListener(Ljava/lang/String;ZLjava/lang/Class;)Lee/cyber/smartid/inter/ServiceListener;

    move-result-object v0

    check-cast v0, Lee/cyber/smartid/inter/DeleteAccountListener;

    if-nez v0, :cond_0

    return-void

    .line 2
    :cond_0
    iget-object v1, p0, Lee/cyber/smartid/manager/impl/DeleteAccountManagerImpl$3;->a:Ljava/lang/String;

    new-instance v2, Lee/cyber/smartid/dto/jsonrpc/resp/DeleteAccountResp;

    iget-object v3, p0, Lee/cyber/smartid/manager/impl/DeleteAccountManagerImpl$3;->b:Ljava/lang/String;

    iget-object v4, p0, Lee/cyber/smartid/manager/impl/DeleteAccountManagerImpl$3;->c:Ljava/util/ArrayList;

    invoke-direct {v2, v1, v3, v4}, Lee/cyber/smartid/dto/jsonrpc/resp/DeleteAccountResp;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;)V

    invoke-interface {v0, v1, v2}, Lee/cyber/smartid/inter/DeleteAccountListener;->onDeleteAccountSuccess(Ljava/lang/String;Lee/cyber/smartid/dto/jsonrpc/resp/DeleteAccountResp;)V

    return-void
.end method
