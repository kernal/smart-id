.class public Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;
.super Ljava/lang/Object;
.source "InteractiveUpgradeHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper$ValidateResponseCallback;,
        Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper$KTKPublicKeyWrapper;
    }
.end annotation


# instance fields
.field private a:Landroid/os/PowerManager$WakeLock;

.field private final b:Lee/cyber/smartid/tse/inter/WallClock;

.field private final c:Lee/cyber/smartid/inter/ListenerAccess;

.field private final d:Lee/cyber/smartid/cryptolib/inter/EncodingOp;

.field private final e:Lee/cyber/smartid/inter/ServiceAccess;

.field private final f:Lee/cyber/smartid/cryptolib/inter/StorageOp;

.field private final g:Lee/cyber/smartid/cryptolib/inter/RandomGenerationOp;

.field private final h:Lee/cyber/smartid/cryptolib/inter/KeyGenerationOp;

.field private final i:Lee/cyber/smartid/cryptolib/inter/CryptoOp;

.field private j:Lee/cyber/smartid/network/SmartIdAPI;

.field private volatile k:Z

.field private final l:Ljava/lang/Object;

.field private final m:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private n:Lee/cyber/smartid/util/Log;


# direct methods
.method public constructor <init>(Lee/cyber/smartid/tse/inter/WallClock;Lee/cyber/smartid/inter/ServiceAccess;Lee/cyber/smartid/inter/ListenerAccess;Lee/cyber/smartid/cryptolib/inter/StorageOp;Lee/cyber/smartid/cryptolib/inter/EncodingOp;Lee/cyber/smartid/cryptolib/inter/RandomGenerationOp;Lee/cyber/smartid/cryptolib/inter/KeyGenerationOp;Lee/cyber/smartid/cryptolib/inter/CryptoOp;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->l:Ljava/lang/Object;

    .line 3
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->m:Ljava/util/HashSet;

    .line 4
    const-class v0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;

    invoke-static {v0}, Lee/cyber/smartid/util/Log;->getInstance(Ljava/lang/Class;)Lee/cyber/smartid/util/Log;

    move-result-object v0

    iput-object v0, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->n:Lee/cyber/smartid/util/Log;

    .line 5
    iput-object p1, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->b:Lee/cyber/smartid/tse/inter/WallClock;

    .line 6
    iput-object p2, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->e:Lee/cyber/smartid/inter/ServiceAccess;

    .line 7
    iput-object p3, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->c:Lee/cyber/smartid/inter/ListenerAccess;

    .line 8
    iput-object p4, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->f:Lee/cyber/smartid/cryptolib/inter/StorageOp;

    .line 9
    iput-object p5, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->d:Lee/cyber/smartid/cryptolib/inter/EncodingOp;

    .line 10
    iput-object p6, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->g:Lee/cyber/smartid/cryptolib/inter/RandomGenerationOp;

    .line 11
    iput-object p7, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->h:Lee/cyber/smartid/cryptolib/inter/KeyGenerationOp;

    .line 12
    iput-object p8, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->i:Lee/cyber/smartid/cryptolib/inter/CryptoOp;

    return-void
.end method

.method private a(Ljava/util/concurrent/atomic/AtomicInteger;Ljava/lang/String;Ljava/lang/String;)Landroid/support/v4/e/j;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/atomic/AtomicInteger;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Landroid/support/v4/e/j<",
            "Ljava/lang/String;",
            "Lee/cyber/smartid/dto/SmartIdError;",
            ">;"
        }
    .end annotation

    const-string v0, "updateFreshnessToken"

    .line 155
    iget-object v1, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->n:Lee/cyber/smartid/util/Log;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "updateFreshnessToken: key type "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, ", account: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, ", startRetryCount: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    .line 156
    new-instance v1, Lee/cyber/smartid/tse/dto/jsonrpc/param/NewFreshnessTokenParams;

    invoke-direct {v1}, Lee/cyber/smartid/tse/dto/jsonrpc/param/NewFreshnessTokenParams;-><init>()V

    .line 157
    invoke-virtual {v1, p2}, Lee/cyber/smartid/tse/dto/jsonrpc/param/NewFreshnessTokenParams;->setAccountUUID(Ljava/lang/String;)V

    .line 158
    invoke-virtual {v1, p3}, Lee/cyber/smartid/tse/dto/jsonrpc/param/NewFreshnessTokenParams;->setKeyType(Ljava/lang/String;)V

    .line 159
    new-instance p2, Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCRequest;

    const-string p3, "getFreshnessToken"

    invoke-direct {p2, p3, v1}, Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCRequest;-><init>(Ljava/lang/String;Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCParams;)V

    :cond_0
    const/4 p3, 0x3

    const/4 v1, 0x0

    .line 160
    :try_start_0
    invoke-virtual {p1}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v2

    if-lez v2, :cond_1

    .line 161
    iget-object v2, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->n:Lee/cyber/smartid/util/Log;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    const-string v4, "updateFreshnessToken - overall retries at "

    :try_start_1
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0

    const-string v4, "/"

    :try_start_2
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    .line 162
    iget-object v2, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->e:Lee/cyber/smartid/inter/ServiceAccess;

    invoke-virtual {p1}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v3

    invoke-interface {v2, v3}, Lee/cyber/smartid/inter/ServiceAccess;->getAutomaticRequestRetryDelay(I)J

    move-result-wide v2

    invoke-static {v2, v3}, Landroid/os/SystemClock;->sleep(J)V

    .line 163
    :cond_1
    iget-object v2, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->j:Lee/cyber/smartid/network/SmartIdAPI;
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_0

    const-string v3, "internal-setup"

    :try_start_3
    invoke-interface {v2, v3, p2}, Lee/cyber/smartid/network/SmartIdAPI;->getNewFreshnessToken(Ljava/lang/String;Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCRequest;)Lc/b;

    move-result-object v2

    invoke-interface {v2}, Lc/b;->a()Lc/r;

    move-result-object v2
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/lang/RuntimeException; {:try_start_3 .. :try_end_3} :catch_0

    move-object v3, v1

    goto :goto_1

    :catch_0
    move-exception v2

    .line 164
    iget-object v3, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->n:Lee/cyber/smartid/util/Log;

    invoke-virtual {v3, v0, v2}, Lee/cyber/smartid/util/Log;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    :catch_1
    move-exception v2

    .line 165
    iget-object v3, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->n:Lee/cyber/smartid/util/Log;

    invoke-virtual {v3, v0, v2}, Lee/cyber/smartid/util/Log;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    :goto_0
    move-object v3, v2

    move-object v2, v1

    .line 166
    :goto_1
    new-instance v4, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper$8;

    invoke-direct {v4, p0}, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper$8;-><init>(Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;)V

    invoke-direct {p0, p2, v2, v3, v4}, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->a(Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCRequest;Lc/r;Ljava/lang/Throwable;Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper$ValidateResponseCallback;)Lee/cyber/smartid/tse/dto/BaseError;

    move-result-object v3

    if-eqz v3, :cond_2

    .line 167
    invoke-direct {p0, v3}, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->b(Lee/cyber/smartid/tse/dto/BaseError;)Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-virtual {p1}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    move-result v4

    if-le v4, p3, :cond_0

    :cond_2
    if-eqz v3, :cond_3

    .line 168
    iget-object p1, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->n:Lee/cyber/smartid/util/Log;

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string p3, "updateFreshnessToken failed - "

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Lee/cyber/smartid/util/Log;->e(Ljava/lang/String;)V

    .line 169
    new-instance p1, Landroid/support/v4/e/j;

    iget-object p2, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->e:Lee/cyber/smartid/inter/ServiceAccess;

    invoke-interface {p2}, Lee/cyber/smartid/inter/ServiceAccess;->getTseErrorToServiceErrorMapper()Lee/cyber/smartid/manager/inter/TSEErrorToServiceErrorMapper;

    move-result-object p2

    invoke-interface {p2, v3}, Lee/cyber/smartid/manager/inter/TSEErrorToServiceErrorMapper;->map(Lee/cyber/smartid/tse/dto/BaseError;)Lee/cyber/smartid/dto/SmartIdError;

    move-result-object p2

    invoke-direct {p1, v1, p2}, Landroid/support/v4/e/j;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object p1

    .line 170
    :cond_3
    iget-object p1, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->n:Lee/cyber/smartid/util/Log;

    const-string p2, "updateFreshnessToken success"

    invoke-virtual {p1, p2}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    .line 171
    new-instance p1, Landroid/support/v4/e/j;

    invoke-virtual {v2}, Lc/r;->e()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCResponse;

    invoke-virtual {p2}, Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCResponse;->getResult()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lee/cyber/smartid/tse/dto/jsonrpc/result/NewFreshnessTokenResult;

    invoke-virtual {p2}, Lee/cyber/smartid/tse/dto/jsonrpc/result/NewFreshnessTokenResult;->getFreshnessToken()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2, v1}, Landroid/support/v4/e/j;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object p1
.end method

.method private a(Lee/cyber/smartid/dto/upgrade/InteractiveUpgradeState;)Lee/cyber/smartid/dto/SmartIdError;
    .locals 13

    const-string v0, " of account "

    .line 2
    iget-object v1, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->e:Lee/cyber/smartid/inter/ServiceAccess;

    invoke-interface {v1}, Lee/cyber/smartid/inter/ServiceAccess;->getPropertiesManager()Lee/cyber/smartid/manager/inter/PropertiesManager;

    move-result-object v2

    invoke-interface {v2}, Lee/cyber/smartid/manager/inter/PropertiesManager;->getPropAccountUpgradeSZId()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lee/cyber/smartid/inter/ServiceAccess;->getKnownServerKeys(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    .line 3
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    const/4 v2, 0x1

    if-ge v1, v2, :cond_0

    .line 4
    iget-object p1, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->b:Lee/cyber/smartid/tse/inter/WallClock;

    iget-object v0, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->e:Lee/cyber/smartid/inter/ServiceAccess;

    invoke-interface {v0}, Lee/cyber/smartid/inter/ServiceAccess;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    sget v1, Lee/cyber/smartid/R$string;->err_no_such_ktk_found:I

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-wide/16 v1, 0x413

    invoke-static {p1, v1, v2, v0}, Lee/cyber/smartid/dto/SmartIdError;->from(Lee/cyber/smartid/tse/inter/WallClock;JLjava/lang/String;)Lee/cyber/smartid/dto/SmartIdError;

    move-result-object p1

    return-object p1

    .line 5
    :cond_0
    invoke-direct {p0}, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->i()Ljava/util/ArrayList;

    move-result-object v1

    .line 6
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v3

    const/4 v4, 0x0

    if-nez v3, :cond_1

    .line 7
    iget-object p1, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->n:Lee/cyber/smartid/util/Log;

    const-string v0, "upgradeAccountsToMDv2 - nothing to upgrade, we are done"

    invoke-virtual {p1, v0}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    return-object v4

    :cond_1
    const-string v3, "AUTHENTICATION"

    const-string v5, "SIGNATURE"

    .line 8
    filled-new-array {v3, v5}, [Ljava/lang/String;

    move-result-object v3

    .line 9
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    const/4 v7, 0x0

    if-eqz v6, :cond_4

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lee/cyber/smartid/dto/AccountWrapper;

    .line 10
    array-length v8, v3

    :goto_0
    if-ge v7, v8, :cond_2

    aget-object v9, v3, v7

    .line 11
    :try_start_0
    invoke-virtual {v6}, Lee/cyber/smartid/dto/AccountWrapper;->getAccountUUID()Ljava/lang/String;

    move-result-object v10

    invoke-direct {p0, p1, v10, v9}, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->a(Lee/cyber/smartid/dto/upgrade/InteractiveUpgradeState;Ljava/lang/String;Ljava/lang/String;)Lee/cyber/smartid/dto/jsonrpc/param/UpgradeKeyPairParams;

    move-result-object v10

    if-eqz v10, :cond_3

    .line 12
    iget-object v10, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->n:Lee/cyber/smartid/util/Log;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V
    :try_end_0
    .catch Lee/cyber/smartid/cryptolib/dto/StorageException; {:try_start_0 .. :try_end_0} :catch_0

    const-string v12, "upgradeAccountsToMDv2 upgrade params created and stored for key type "

    :try_start_1
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v11, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Lee/cyber/smartid/dto/AccountWrapper;->getAccountUUID()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    goto :goto_1

    .line 13
    :cond_3
    iget-object v10, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->n:Lee/cyber/smartid/util/Log;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V
    :try_end_1
    .catch Lee/cyber/smartid/cryptolib/dto/StorageException; {:try_start_1 .. :try_end_1} :catch_0

    const-string v12, "upgradeAccountsToMDv2 failed to create upgrade params for key type "

    :try_start_2
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v11, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Lee/cyber/smartid/dto/AccountWrapper;->getAccountUUID()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_2
    .catch Lee/cyber/smartid/cryptolib/dto/StorageException; {:try_start_2 .. :try_end_2} :catch_0

    const-string v12, ", ignoring, this account will be deleted later."

    :try_start_3
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Lee/cyber/smartid/util/Log;->w(Ljava/lang/String;)V
    :try_end_3
    .catch Lee/cyber/smartid/cryptolib/dto/StorageException; {:try_start_3 .. :try_end_3} :catch_0

    :goto_1
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    :catch_0
    move-exception p1

    .line 14
    iget-object v1, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->n:Lee/cyber/smartid/util/Log;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "upgradeAccountsToMDv2 failed to store upgrade params for key type "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Lee/cyber/smartid/dto/AccountWrapper;->getAccountUUID()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, ", aborting upgrade"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0, p1}, Lee/cyber/smartid/util/Log;->w(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 15
    iget-object v0, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->e:Lee/cyber/smartid/inter/ServiceAccess;

    invoke-interface {v0}, Lee/cyber/smartid/inter/ServiceAccess;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->b:Lee/cyber/smartid/tse/inter/WallClock;

    invoke-static {v0, v1, p1}, Lee/cyber/smartid/dto/SmartIdError;->from(Landroid/content/Context;Lee/cyber/smartid/tse/inter/WallClock;Ljava/lang/Throwable;)Lee/cyber/smartid/dto/SmartIdError;

    move-result-object p1

    return-object p1

    .line 16
    :cond_4
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    move-object v1, v4

    :goto_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_a

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lee/cyber/smartid/dto/AccountWrapper;

    .line 17
    array-length v6, v3

    move-object v8, v1

    const/4 v1, 0x0

    :goto_3
    if-ge v1, v6, :cond_7

    aget-object v9, v3, v1

    .line 18
    invoke-virtual {v5}, Lee/cyber/smartid/dto/AccountWrapper;->getAccountUUID()Ljava/lang/String;

    move-result-object v10

    invoke-direct {p0, p1, v10, v9}, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->b(Lee/cyber/smartid/dto/upgrade/InteractiveUpgradeState;Ljava/lang/String;Ljava/lang/String;)Lee/cyber/smartid/dto/jsonrpc/param/UpgradeKeyPairParams;

    move-result-object v10

    if-nez v10, :cond_5

    :goto_4
    const/4 v1, 0x1

    goto :goto_5

    .line 19
    :cond_5
    invoke-virtual {v5}, Lee/cyber/smartid/dto/AccountWrapper;->getAccountUUID()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {p1, v11, v9}, Lee/cyber/smartid/dto/upgrade/InteractiveUpgradeState;->getMDv2UpgradeResult(Ljava/lang/String;Ljava/lang/String;)Lee/cyber/smartid/dto/jsonrpc/result/UpgradeKeyPairResult;

    move-result-object v11

    if-nez v11, :cond_6

    .line 20
    invoke-virtual {v5}, Lee/cyber/smartid/dto/AccountWrapper;->getAccountUUID()Ljava/lang/String;

    move-result-object v8

    invoke-direct {p0, p1, v8, v9, v10}, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->a(Lee/cyber/smartid/dto/upgrade/InteractiveUpgradeState;Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/dto/jsonrpc/param/UpgradeKeyPairParams;)Lee/cyber/smartid/dto/SmartIdError;

    move-result-object v8

    if-eqz v8, :cond_6

    .line 21
    invoke-direct {p0, v8}, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->a(Lee/cyber/smartid/tse/dto/BaseError;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 22
    iget-object v1, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->n:Lee/cyber/smartid/util/Log;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "upgradeAccountsToMDv2 - Account-lost-event reported for accountUUID: "

    invoke-virtual {v6, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Lee/cyber/smartid/dto/AccountWrapper;->getAccountUUID()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v6, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v10, ", key Type: "

    invoke-virtual {v6, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v9, ", marking it for deletion. Error was: "

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    move-object v8, v4

    goto :goto_4

    :cond_6
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    :cond_7
    const/4 v1, 0x0

    :goto_5
    if-eqz v1, :cond_8

    .line 23
    iget-object v1, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->n:Lee/cyber/smartid/util/Log;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "upgradeAccountsToMDv2: Unable to upgrade account "

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Lee/cyber/smartid/dto/AccountWrapper;->getAccountUUID()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v9, ", deleting it .."

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    .line 24
    :try_start_4
    invoke-virtual {v5}, Lee/cyber/smartid/dto/AccountWrapper;->getAccountUUID()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1, v4}, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->a(Ljava/lang/String;Ljava/util/HashSet;)V

    .line 25
    invoke-virtual {v5}, Lee/cyber/smartid/dto/AccountWrapper;->getAccountUUID()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1, v3}, Lee/cyber/smartid/dto/upgrade/InteractiveUpgradeState;->removeMDv2UpgradeData(Ljava/lang/String;[Ljava/lang/String;)V

    .line 26
    invoke-direct {p0, p1}, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->c(Lee/cyber/smartid/dto/upgrade/InteractiveUpgradeState;)V
    :try_end_4
    .catch Lee/cyber/smartid/cryptolib/dto/StorageException; {:try_start_4 .. :try_end_4} :catch_1

    goto :goto_6

    :catch_1
    move-exception p1

    .line 27
    iget-object v0, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->e:Lee/cyber/smartid/inter/ServiceAccess;

    invoke-interface {v0}, Lee/cyber/smartid/inter/ServiceAccess;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->b:Lee/cyber/smartid/tse/inter/WallClock;

    invoke-static {v0, v1, p1}, Lee/cyber/smartid/dto/SmartIdError;->from(Landroid/content/Context;Lee/cyber/smartid/tse/inter/WallClock;Ljava/lang/Throwable;)Lee/cyber/smartid/dto/SmartIdError;

    move-result-object p1

    return-object p1

    :cond_8
    if-eqz v8, :cond_9

    return-object v8

    :cond_9
    :goto_6
    move-object v1, v8

    goto/16 :goto_2

    .line 28
    :cond_a
    invoke-direct {p0, p1}, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->b(Lee/cyber/smartid/dto/upgrade/InteractiveUpgradeState;)Lee/cyber/smartid/dto/SmartIdError;

    move-result-object v0

    if-eqz v0, :cond_b

    return-object v0

    .line 29
    :cond_b
    :try_start_5
    invoke-direct {p0}, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->m()V

    .line 30
    invoke-virtual {p1}, Lee/cyber/smartid/dto/upgrade/InteractiveUpgradeState;->removeAllMDv2UpgradeData()V

    .line 31
    invoke-direct {p0, p1}, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->c(Lee/cyber/smartid/dto/upgrade/InteractiveUpgradeState;)V
    :try_end_5
    .catch Lee/cyber/smartid/cryptolib/dto/StorageException; {:try_start_5 .. :try_end_5} :catch_2

    return-object v4

    :catch_2
    move-exception p1

    .line 32
    iget-object v0, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->n:Lee/cyber/smartid/util/Log;

    const-string v1, "upgradeAccountsToMDv2: Failed to clear the old accounts post-upgrade"

    invoke-virtual {v0, v1, p1}, Lee/cyber/smartid/util/Log;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 33
    iget-object v0, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->e:Lee/cyber/smartid/inter/ServiceAccess;

    invoke-interface {v0}, Lee/cyber/smartid/inter/ServiceAccess;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->b:Lee/cyber/smartid/tse/inter/WallClock;

    invoke-static {v0, v1, p1}, Lee/cyber/smartid/dto/SmartIdError;->from(Landroid/content/Context;Lee/cyber/smartid/tse/inter/WallClock;Ljava/lang/Throwable;)Lee/cyber/smartid/dto/SmartIdError;

    move-result-object p1

    return-object p1
.end method

.method private a(Lee/cyber/smartid/dto/upgrade/InteractiveUpgradeState;Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/dto/jsonrpc/param/UpgradeKeyPairParams;)Lee/cyber/smartid/dto/SmartIdError;
    .locals 11

    const-string v0, "requestMDv2UpgradeResult"

    .line 120
    iget-object v1, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->n:Lee/cyber/smartid/util/Log;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "requestMDv2UpgradeResult: key type "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, ", account "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    .line 121
    new-instance v1, Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    .line 122
    invoke-virtual {p4}, Lee/cyber/smartid/dto/jsonrpc/param/UpgradeKeyPairParams;->getFreshnessToken()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    .line 123
    :goto_0
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    const-string v5, ", account: "

    if-eqz v4, :cond_0

    .line 124
    iget-object v2, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->n:Lee/cyber/smartid/util/Log;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "requestMDv2UpgradeResult - we are missing a freshnessToken (key type "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v6, "), lets get one  .."

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    .line 125
    invoke-direct {p0, v1, p2, p3}, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->a(Ljava/util/concurrent/atomic/AtomicInteger;Ljava/lang/String;Ljava/lang/String;)Landroid/support/v4/e/j;

    move-result-object v2

    .line 126
    iget-object v4, v2, Landroid/support/v4/e/j;->a:Ljava/lang/Object;

    check-cast v4, Ljava/lang/String;

    .line 127
    iget-object v2, v2, Landroid/support/v4/e/j;->b:Ljava/lang/Object;

    check-cast v2, Lee/cyber/smartid/dto/SmartIdError;

    move-object v10, v4

    move-object v4, v2

    move-object v2, v10

    goto :goto_1

    .line 128
    :cond_0
    iget-object v4, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->n:Lee/cyber/smartid/util/Log;

    const-string v6, "requestMDv2UpgradeResult - we already have the freshnessToken, using the exiting one  .."

    invoke-virtual {v4, v6}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    move-object v4, v3

    .line 129
    :goto_1
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 130
    iget-object v0, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->n:Lee/cyber/smartid/util/Log;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "requestMDv2UpgradeResult - unable to get a freshnessToken (key type "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "), aborting .."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    if-nez v4, :cond_1

    .line 131
    iget-object v0, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->b:Lee/cyber/smartid/tse/inter/WallClock;

    iget-object v1, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->e:Lee/cyber/smartid/inter/ServiceAccess;

    invoke-interface {v1}, Lee/cyber/smartid/inter/ServiceAccess;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    sget v2, Lee/cyber/smartid/R$string;->err_unable_to_acquire_freshness_token:I

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-wide/16 v4, 0x40a

    invoke-static {v0, v4, v5, v1}, Lee/cyber/smartid/dto/SmartIdError;->from(Lee/cyber/smartid/tse/inter/WallClock;JLjava/lang/String;)Lee/cyber/smartid/dto/SmartIdError;

    move-result-object v4

    :cond_1
    move-object v5, v3

    goto/16 :goto_6

    .line 132
    :cond_2
    invoke-virtual {p4, v2}, Lee/cyber/smartid/dto/jsonrpc/param/UpgradeKeyPairParams;->setFreshnessToken(Ljava/lang/String;)V

    .line 133
    new-instance v2, Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCRequest;

    const-string v5, "upgradeKeyPair"

    invoke-direct {v2, v5, p4}, Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCRequest;-><init>(Ljava/lang/String;Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCParams;)V

    .line 134
    iget-object v5, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->j:Lee/cyber/smartid/network/SmartIdAPI;

    invoke-interface {v5, v2}, Lee/cyber/smartid/network/SmartIdAPI;->upgradeKeyPair(Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCRequest;)Lc/b;

    move-result-object v5

    const/4 v6, 0x3

    .line 135
    :try_start_0
    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v7

    if-lez v7, :cond_3

    .line 136
    iget-object v7, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->n:Lee/cyber/smartid/util/Log;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    const-string v9, "requestMDv2UpgradeResult - overall retries at "

    :try_start_1
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0

    const-string v9, "/"

    :try_start_2
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    .line 137
    :cond_3
    invoke-interface {v5}, Lc/b;->a()Lc/r;

    move-result-object v5
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_0

    move-object v7, v3

    goto :goto_3

    :catch_0
    move-exception v5

    .line 138
    iget-object v7, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->n:Lee/cyber/smartid/util/Log;

    invoke-virtual {v7, v0, v5}, Lee/cyber/smartid/util/Log;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_2

    :catch_1
    move-exception v5

    .line 139
    iget-object v7, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->n:Lee/cyber/smartid/util/Log;

    invoke-virtual {v7, v0, v5}, Lee/cyber/smartid/util/Log;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    :goto_2
    move-object v7, v5

    move-object v5, v3

    .line 140
    :goto_3
    new-instance v8, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper$7;

    invoke-direct {v8, p0}, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper$7;-><init>(Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;)V

    invoke-direct {p0, v2, v5, v7, v8}, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->a(Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCRequest;Lc/r;Ljava/lang/Throwable;Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper$ValidateResponseCallback;)Lee/cyber/smartid/tse/dto/BaseError;

    move-result-object v2

    .line 141
    instance-of v7, v2, Lee/cyber/smartid/dto/SmartIdError;

    if-eqz v7, :cond_4

    .line 142
    check-cast v2, Lee/cyber/smartid/dto/SmartIdError;

    :goto_4
    move-object v4, v2

    goto :goto_5

    :cond_4
    if-eqz v2, :cond_5

    .line 143
    iget-object v4, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->e:Lee/cyber/smartid/inter/ServiceAccess;

    invoke-interface {v4}, Lee/cyber/smartid/inter/ServiceAccess;->getTseErrorToServiceErrorMapper()Lee/cyber/smartid/manager/inter/TSEErrorToServiceErrorMapper;

    move-result-object v4

    invoke-interface {v4, v2}, Lee/cyber/smartid/manager/inter/TSEErrorToServiceErrorMapper;->map(Lee/cyber/smartid/tse/dto/BaseError;)Lee/cyber/smartid/dto/SmartIdError;

    move-result-object v2

    goto :goto_4

    :cond_5
    :goto_5
    if-eqz v4, :cond_7

    .line 144
    invoke-direct {p0, v4}, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->b(Lee/cyber/smartid/tse/dto/BaseError;)Z

    move-result v2

    if-eqz v2, :cond_7

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    move-result v2

    if-le v2, v6, :cond_6

    goto :goto_6

    :cond_6
    move-object v2, v3

    goto/16 :goto_0

    .line 145
    :cond_7
    :goto_6
    iget-object v0, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->n:Lee/cyber/smartid/util/Log;

    const-string v1, "requestMDv2UpgradeResult - clearing the freshnessToken .."

    invoke-virtual {v0, v1}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    .line 146
    invoke-virtual {p4, v3}, Lee/cyber/smartid/dto/jsonrpc/param/UpgradeKeyPairParams;->setFreshnessToken(Ljava/lang/String;)V

    .line 147
    invoke-virtual {p1, p2, p3, p4}, Lee/cyber/smartid/dto/upgrade/InteractiveUpgradeState;->setMDv2UpgradeParams(Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/dto/jsonrpc/param/UpgradeKeyPairParams;)V

    .line 148
    :try_start_3
    invoke-direct {p0, p1}, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->c(Lee/cyber/smartid/dto/upgrade/InteractiveUpgradeState;)V
    :try_end_3
    .catch Lee/cyber/smartid/cryptolib/dto/StorageException; {:try_start_3 .. :try_end_3} :catch_3

    if-eqz v4, :cond_8

    return-object v4

    .line 149
    :cond_8
    invoke-virtual {v5}, Lc/r;->e()Ljava/lang/Object;

    move-result-object p4

    check-cast p4, Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCResponse;

    invoke-virtual {p4}, Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCResponse;->getResult()Ljava/lang/Object;

    move-result-object p4

    check-cast p4, Lee/cyber/smartid/dto/jsonrpc/result/UpgradeKeyPairResult;

    .line 150
    :try_start_4
    invoke-virtual {p1, p2, p3, p4}, Lee/cyber/smartid/dto/upgrade/InteractiveUpgradeState;->setMDv2UpgradeResult(Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/dto/jsonrpc/result/UpgradeKeyPairResult;)V

    .line 151
    invoke-direct {p0, p1}, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->c(Lee/cyber/smartid/dto/upgrade/InteractiveUpgradeState;)V
    :try_end_4
    .catch Lee/cyber/smartid/cryptolib/dto/StorageException; {:try_start_4 .. :try_end_4} :catch_2

    return-object v3

    :catch_2
    move-exception p4

    .line 152
    invoke-virtual {p1, p2, p3, v3}, Lee/cyber/smartid/dto/upgrade/InteractiveUpgradeState;->setMDv2UpgradeResult(Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/dto/jsonrpc/result/UpgradeKeyPairResult;)V

    .line 153
    iget-object p1, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->e:Lee/cyber/smartid/inter/ServiceAccess;

    invoke-interface {p1}, Lee/cyber/smartid/inter/ServiceAccess;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    iget-object p2, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->b:Lee/cyber/smartid/tse/inter/WallClock;

    invoke-static {p1, p2, p4}, Lee/cyber/smartid/dto/SmartIdError;->from(Landroid/content/Context;Lee/cyber/smartid/tse/inter/WallClock;Ljava/lang/Throwable;)Lee/cyber/smartid/dto/SmartIdError;

    move-result-object p1

    return-object p1

    :catch_3
    move-exception p1

    .line 154
    iget-object p2, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->e:Lee/cyber/smartid/inter/ServiceAccess;

    invoke-interface {p2}, Lee/cyber/smartid/inter/ServiceAccess;->getApplicationContext()Landroid/content/Context;

    move-result-object p2

    iget-object p3, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->b:Lee/cyber/smartid/tse/inter/WallClock;

    invoke-static {p2, p3, p1}, Lee/cyber/smartid/dto/SmartIdError;->from(Landroid/content/Context;Lee/cyber/smartid/tse/inter/WallClock;Ljava/lang/Throwable;)Lee/cyber/smartid/dto/SmartIdError;

    move-result-object p1

    return-object p1
.end method

.method private a(Lee/cyber/smartid/dto/upgrade/InteractiveUpgradeState;Ljava/lang/String;Ljava/lang/String;)Lee/cyber/smartid/dto/jsonrpc/param/UpgradeKeyPairParams;
    .locals 12
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lee/cyber/smartid/cryptolib/dto/StorageException;
        }
    .end annotation

    const-string v0, "createOrLoadMDv2UpgradeParams"

    const/4 v1, 0x0

    .line 99
    :try_start_0
    invoke-direct {p0, p1, p2, p3}, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->b(Lee/cyber/smartid/dto/upgrade/InteractiveUpgradeState;Ljava/lang/String;Ljava/lang/String;)Lee/cyber/smartid/dto/jsonrpc/param/UpgradeKeyPairParams;

    move-result-object v2
    :try_end_0
    .catch Lee/cyber/smartid/cryptolib/dto/StorageException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    const-string v3, ", keyType: "

    if-eqz v2, :cond_0

    .line 100
    :try_start_1
    iget-object v4, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->n:Lee/cyber/smartid/util/Log;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V
    :try_end_1
    .catch Lee/cyber/smartid/cryptolib/dto/StorageException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    const-string v6, "createOrLoadMDv2UpgradeParams - found existing params for accountUUID: "

    :try_start_2
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    return-object v2

    .line 101
    :cond_0
    iget-object v2, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->n:Lee/cyber/smartid/util/Log;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V
    :try_end_2
    .catch Lee/cyber/smartid/cryptolib/dto/StorageException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0

    const-string v5, "createOrLoadMDv2UpgradeParams - creating new params params for accountUUID: "

    :try_start_3
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    .line 102
    iget-object v2, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->f:Lee/cyber/smartid/cryptolib/inter/StorageOp;

    invoke-direct {p0, p2, p3}, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper$5;

    invoke-direct {v4, p0}, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper$5;-><init>(Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;)V

    .line 103
    invoke-virtual {v4}, Lcom/google/gson/b/a;->getType()Ljava/lang/reflect/Type;

    move-result-object v4

    .line 104
    invoke-interface {v2, v3, v4}, Lee/cyber/smartid/cryptolib/inter/StorageOp;->retrieveData(Ljava/lang/String;Ljava/lang/reflect/Type;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lee/cyber/smartid/dto/upgrade/v5/PreMDv2KeyState;

    if-nez v2, :cond_1

    return-object v1

    .line 105
    :cond_1
    iget-object v3, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->f:Lee/cyber/smartid/cryptolib/inter/StorageOp;

    invoke-direct {p0, p2, p3}, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->d(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    new-instance v5, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper$6;

    invoke-direct {v5, p0}, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper$6;-><init>(Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;)V

    .line 106
    invoke-virtual {v5}, Lcom/google/gson/b/a;->getType()Ljava/lang/reflect/Type;

    move-result-object v5

    .line 107
    invoke-interface {v3, v4, v5}, Lee/cyber/smartid/cryptolib/inter/StorageOp;->retrieveData(Ljava/lang/String;Ljava/lang/reflect/Type;)Ljava/lang/Object;

    move-result-object v3

    move-object v9, v3

    check-cast v9, Ljava/lang/String;

    .line 108
    invoke-virtual {p1, p2, p3}, Lee/cyber/smartid/dto/upgrade/InteractiveUpgradeState;->getMDv2HDKeyPair(Ljava/lang/String;Ljava/lang/String;)Lee/cyber/smartid/cryptolib/dto/DiffieHellmanKeyPair;

    move-result-object v3

    if-nez v3, :cond_2

    .line 109
    iget-object v3, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->h:Lee/cyber/smartid/cryptolib/inter/KeyGenerationOp;

    const-wide/16 v4, 0xe

    invoke-static {v4, v5}, Lee/cyber/smartid/cryptolib/dto/DiffieHellmanGroup;->createGroup(J)Lee/cyber/smartid/cryptolib/dto/DiffieHellmanGroup;

    move-result-object v4

    invoke-interface {v3, v4}, Lee/cyber/smartid/cryptolib/inter/KeyGenerationOp;->generateDiffieHellmanKeyPair(Lee/cyber/smartid/cryptolib/dto/DiffieHellmanGroup;)Lee/cyber/smartid/cryptolib/dto/DiffieHellmanKeyPair;

    move-result-object v3

    .line 110
    invoke-virtual {p1, p2, p3, v3}, Lee/cyber/smartid/dto/upgrade/InteractiveUpgradeState;->setMDv2HDKeyPair(Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/cryptolib/dto/DiffieHellmanKeyPair;)V

    .line 111
    :cond_2
    iget-object v4, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->d:Lee/cyber/smartid/cryptolib/inter/EncodingOp;

    invoke-virtual {v3}, Lee/cyber/smartid/cryptolib/dto/DiffieHellmanKeyPair;->getPublicKey()Ljava/math/BigInteger;

    move-result-object v3

    invoke-interface {v4, v3}, Lee/cyber/smartid/cryptolib/inter/EncodingOp;->encodeDecimalToBase64(Ljava/math/BigInteger;)Ljava/lang/String;

    move-result-object v11

    .line 112
    iget-object v3, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->d:Lee/cyber/smartid/cryptolib/inter/EncodingOp;

    new-instance v4, Ljava/math/BigInteger;

    iget-object v5, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->g:Lee/cyber/smartid/cryptolib/inter/RandomGenerationOp;

    invoke-interface {v5}, Lee/cyber/smartid/cryptolib/inter/RandomGenerationOp;->generateRandomNonce()[B

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/math/BigInteger;-><init>([B)V

    invoke-interface {v3, v4}, Lee/cyber/smartid/cryptolib/inter/EncodingOp;->encodeDecimalToBase64(Ljava/math/BigInteger;)Ljava/lang/String;

    move-result-object v8

    .line 113
    new-instance v3, Lee/cyber/smartid/dto/jsonrpc/param/UpgradeKeyPairParams;

    invoke-virtual {v2}, Lee/cyber/smartid/dto/upgrade/v5/PreMDv2KeyState;->getOneTimePassword()Ljava/lang/String;

    move-result-object v7

    iget-object v2, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->e:Lee/cyber/smartid/inter/ServiceAccess;

    iget-object v4, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->e:Lee/cyber/smartid/inter/ServiceAccess;

    invoke-interface {v4}, Lee/cyber/smartid/inter/ServiceAccess;->getPropertiesManager()Lee/cyber/smartid/manager/inter/PropertiesManager;

    move-result-object v4

    invoke-interface {v4}, Lee/cyber/smartid/manager/inter/PropertiesManager;->getPropAccountUpgradeSZId()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v4}, Lee/cyber/smartid/inter/ServiceAccess;->getKnownServerKeys(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v10

    move-object v4, v3

    move-object v5, p2

    move-object v6, p3

    invoke-direct/range {v4 .. v11}, Lee/cyber/smartid/dto/jsonrpc/param/UpgradeKeyPairParams;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Ljava/lang/String;)V

    .line 114
    invoke-virtual {p1, p2, p3, v3}, Lee/cyber/smartid/dto/upgrade/InteractiveUpgradeState;->setMDv2UpgradeParams(Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/dto/jsonrpc/param/UpgradeKeyPairParams;)V

    .line 115
    invoke-direct {p0, p1}, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->c(Lee/cyber/smartid/dto/upgrade/InteractiveUpgradeState;)V
    :try_end_3
    .catch Lee/cyber/smartid/cryptolib/dto/StorageException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_0

    return-object v3

    :catch_0
    move-exception p1

    .line 116
    iget-object p2, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->n:Lee/cyber/smartid/util/Log;

    invoke-virtual {p2, v0, p1}, Lee/cyber/smartid/util/Log;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    return-object v1

    :catch_1
    move-exception v2

    .line 117
    invoke-virtual {p1, p2, p3, v1}, Lee/cyber/smartid/dto/upgrade/InteractiveUpgradeState;->setMDv2UpgradeParams(Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/dto/jsonrpc/param/UpgradeKeyPairParams;)V

    .line 118
    iget-object p1, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->n:Lee/cyber/smartid/util/Log;

    invoke-virtual {p1, v0, v2}, Lee/cyber/smartid/util/Log;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 119
    throw v2
.end method

.method private a(Ljava/lang/String;)Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper$KTKPublicKeyWrapper;
    .locals 13

    .line 7
    new-instance v1, Ljava/util/Properties;

    invoke-direct {v1}, Ljava/util/Properties;-><init>()V

    const/4 v2, 0x0

    .line 8
    :try_start_0
    iget-object v3, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->e:Lee/cyber/smartid/inter/ServiceAccess;

    invoke-interface {v3}, Lee/cyber/smartid/inter/ServiceAccess;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v3
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const-string v4, "tse.properties"

    :try_start_1
    invoke-virtual {v3, v4}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v3
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 9
    :try_start_2
    invoke-virtual {v1, v3}, Ljava/util/Properties;->load(Ljava/io/InputStream;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 10
    invoke-static {v3}, Lee/cyber/smartid/tse/util/Util;->closeClosable(Ljava/io/Closeable;)V

    const-string v3, "szKTKSpec"

    .line 11
    invoke-virtual {v1, v3}, Ljava/util/Properties;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "szKTKPublicKeys"

    .line 12
    invoke-virtual {v1, v4}, Ljava/util/Properties;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 13
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    const-string v5, ","

    const-string v6, ";"

    const/4 v7, 0x3

    const/4 v8, 0x2

    const/4 v9, 0x1

    const/4 v10, 0x0

    if-nez v4, :cond_2

    invoke-direct {p0}, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->d()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 14
    iget-object v1, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->n:Lee/cyber/smartid/util/Log;

    const-string v4, "getKTKPublicKey - We have the newer KTK format - szKTKSpec"

    invoke-virtual {v1, v4}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    .line 15
    invoke-virtual {v3, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 16
    array-length v3, v1

    const/4 v4, 0x0

    :goto_0
    if-ge v4, v3, :cond_5

    aget-object v6, v1, v4

    .line 17
    invoke-virtual {v6, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    .line 18
    array-length v11, v6

    const/4 v12, 0x4

    if-ne v11, v12, :cond_1

    aget-object v11, v6, v10

    invoke-static {v11}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v11

    if-nez v11, :cond_1

    aget-object v11, v6, v9

    invoke-static {v11}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v11

    if-nez v11, :cond_1

    aget-object v11, v6, v8

    invoke-static {v11}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v11

    if-nez v11, :cond_1

    aget-object v11, v6, v7

    invoke-static {v11}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v11

    if-eqz v11, :cond_0

    goto :goto_1

    .line 19
    :cond_0
    aget-object v11, v6, v10

    invoke-virtual {v11}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v11

    invoke-static {v11, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v11

    if-eqz v11, :cond_1

    .line 20
    new-instance v0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper$KTKPublicKeyWrapper;

    aget-object v1, v6, v10

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    aget-object v1, v6, v9

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v4

    aget-object v1, v6, v8

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v5

    aget-object v1, v6, v7

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v6

    move-object v1, v0

    move-object v2, p0

    invoke-direct/range {v1 .. v6}, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper$KTKPublicKeyWrapper;-><init>(Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0

    :cond_1
    :goto_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 21
    :cond_2
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_6

    invoke-direct {p0}, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->f()Z

    move-result v3

    if-eqz v3, :cond_6

    .line 22
    iget-object v3, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->n:Lee/cyber/smartid/util/Log;

    const-string v4, "getKTKPublicKey - We have the older KTK format - szKTKPublicKeys"

    invoke-virtual {v3, v4}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    .line 23
    invoke-virtual {v1, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 24
    array-length v3, v1

    const/4 v4, 0x0

    :goto_2
    if-ge v4, v3, :cond_5

    aget-object v6, v1, v4

    .line 25
    invoke-virtual {v6, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    .line 26
    array-length v11, v6

    if-ne v11, v7, :cond_4

    aget-object v11, v6, v10

    invoke-static {v11}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v11

    if-nez v11, :cond_4

    aget-object v11, v6, v9

    invoke-static {v11}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v11

    if-nez v11, :cond_4

    aget-object v11, v6, v8

    invoke-static {v11}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v11

    if-eqz v11, :cond_3

    goto :goto_3

    .line 27
    :cond_3
    aget-object v11, v6, v10

    invoke-virtual {v11}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v11

    invoke-static {v11, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v11

    if-eqz v11, :cond_4

    .line 28
    new-instance v0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper$KTKPublicKeyWrapper;

    aget-object v1, v6, v10

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    aget-object v2, v6, v9

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    aget-object v3, v6, v8

    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, p0, v1, v2, v3}, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper$KTKPublicKeyWrapper;-><init>(Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0

    :cond_4
    :goto_3
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    :cond_5
    return-object v2

    .line 29
    :cond_6
    iget-object v0, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->n:Lee/cyber/smartid/util/Log;

    const-string v1, "getKTKPublicKey - Could not find any KTK keys!"

    invoke-virtual {v0, v1}, Lee/cyber/smartid/util/Log;->wtf(Ljava/lang/String;)V

    return-object v2

    :catch_0
    move-exception v0

    goto :goto_4

    :catchall_0
    move-exception v0

    move-object v3, v2

    goto :goto_5

    :catch_1
    move-exception v0

    move-object v3, v2

    .line 30
    :goto_4
    :try_start_3
    iget-object v1, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->n:Lee/cyber/smartid/util/Log;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    const-string v4, "getKTKPublicKey"

    :try_start_4
    invoke-virtual {v1, v4, v0}, Lee/cyber/smartid/util/Log;->e(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 31
    invoke-static {v3}, Lee/cyber/smartid/tse/util/Util;->closeClosable(Ljava/io/Closeable;)V

    return-object v2

    :catchall_1
    move-exception v0

    :goto_5
    invoke-static {v3}, Lee/cyber/smartid/tse/util/Util;->closeClosable(Ljava/io/Closeable;)V

    throw v0
.end method

.method private a(Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCRequest;Lc/r;Ljava/lang/Throwable;Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper$ValidateResponseCallback;)Lee/cyber/smartid/tse/dto/BaseError;
    .locals 2

    if-eqz p2, :cond_0

    .line 183
    invoke-virtual {p2}, Lc/r;->b()I

    move-result v0

    const/16 v1, 0x191

    if-ne v0, v1, :cond_0

    .line 184
    iget-object p1, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->e:Lee/cyber/smartid/inter/ServiceAccess;

    invoke-interface {p1}, Lee/cyber/smartid/inter/ServiceAccess;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    iget-object p2, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->b:Lee/cyber/smartid/tse/inter/WallClock;

    new-instance p3, Lee/cyber/smartid/tse/dto/AuthorizationRequiredException;

    invoke-direct {p3}, Lee/cyber/smartid/tse/dto/AuthorizationRequiredException;-><init>()V

    invoke-static {p1, p2, p3}, Lee/cyber/smartid/tse/dto/TSEError;->from(Landroid/content/Context;Lee/cyber/smartid/tse/inter/WallClock;Ljava/lang/Throwable;)Lee/cyber/smartid/tse/dto/TSEError;

    move-result-object p1

    return-object p1

    :cond_0
    if-eqz p2, :cond_1

    .line 185
    invoke-virtual {p2}, Lc/r;->b()I

    move-result v0

    const/16 v1, 0x1e0

    if-ne v0, v1, :cond_1

    .line 186
    iget-object p1, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->e:Lee/cyber/smartid/inter/ServiceAccess;

    invoke-interface {p1}, Lee/cyber/smartid/inter/ServiceAccess;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    iget-object p2, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->b:Lee/cyber/smartid/tse/inter/WallClock;

    new-instance p3, Lee/cyber/smartid/tse/dto/ClientTooOldException;

    invoke-direct {p3}, Lee/cyber/smartid/tse/dto/ClientTooOldException;-><init>()V

    invoke-static {p1, p2, p3}, Lee/cyber/smartid/tse/dto/TSEError;->from(Landroid/content/Context;Lee/cyber/smartid/tse/inter/WallClock;Ljava/lang/Throwable;)Lee/cyber/smartid/tse/dto/TSEError;

    move-result-object p1

    return-object p1

    :cond_1
    if-eqz p2, :cond_2

    .line 187
    invoke-virtual {p2}, Lc/r;->b()I

    move-result v0

    const/16 v1, 0x244

    if-ne v0, v1, :cond_2

    .line 188
    iget-object p1, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->e:Lee/cyber/smartid/inter/ServiceAccess;

    invoke-interface {p1}, Lee/cyber/smartid/inter/ServiceAccess;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    iget-object p2, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->b:Lee/cyber/smartid/tse/inter/WallClock;

    new-instance p3, Lee/cyber/smartid/tse/dto/SystemUnderMaintenanceException;

    invoke-direct {p3}, Lee/cyber/smartid/tse/dto/SystemUnderMaintenanceException;-><init>()V

    invoke-static {p1, p2, p3}, Lee/cyber/smartid/tse/dto/TSEError;->from(Landroid/content/Context;Lee/cyber/smartid/tse/inter/WallClock;Ljava/lang/Throwable;)Lee/cyber/smartid/tse/dto/TSEError;

    move-result-object p1

    return-object p1

    :cond_2
    if-eqz p3, :cond_3

    .line 189
    iget-object p1, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->e:Lee/cyber/smartid/inter/ServiceAccess;

    invoke-interface {p1}, Lee/cyber/smartid/inter/ServiceAccess;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    iget-object p2, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->b:Lee/cyber/smartid/tse/inter/WallClock;

    invoke-static {p1, p2, p3}, Lee/cyber/smartid/tse/dto/TSEError;->from(Landroid/content/Context;Lee/cyber/smartid/tse/inter/WallClock;Ljava/lang/Throwable;)Lee/cyber/smartid/tse/dto/TSEError;

    move-result-object p1

    return-object p1

    :cond_3
    if-nez p2, :cond_4

    .line 190
    iget-object p1, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->e:Lee/cyber/smartid/inter/ServiceAccess;

    invoke-interface {p1}, Lee/cyber/smartid/inter/ServiceAccess;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    iget-object p2, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->b:Lee/cyber/smartid/tse/inter/WallClock;

    new-instance p3, Lee/cyber/smartid/tse/dto/InvalidResponseResultException;

    invoke-direct {p3}, Lee/cyber/smartid/tse/dto/InvalidResponseResultException;-><init>()V

    invoke-static {p1, p2, p3}, Lee/cyber/smartid/tse/dto/TSEError;->from(Landroid/content/Context;Lee/cyber/smartid/tse/inter/WallClock;Ljava/lang/Throwable;)Lee/cyber/smartid/tse/dto/TSEError;

    move-result-object p1

    return-object p1

    .line 191
    :cond_4
    invoke-static {p2}, Lee/cyber/smartid/tse/network/RPCCallback;->getBody(Lc/r;)Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCResponse;

    move-result-object p3

    .line 192
    invoke-virtual {p1}, Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCRequest;->getId()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2}, Lc/r;->b()I

    move-result v0

    invoke-static {p1, v0, p3}, Lee/cyber/smartid/tse/network/RPCCallback;->isForId(Ljava/lang/String;ILee/cyber/smartid/tse/dto/jsonrpc/base/RPCResponse;)Z

    move-result p1

    if-nez p1, :cond_5

    .line 193
    iget-object p1, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->e:Lee/cyber/smartid/inter/ServiceAccess;

    invoke-interface {p1}, Lee/cyber/smartid/inter/ServiceAccess;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    iget-object p2, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->b:Lee/cyber/smartid/tse/inter/WallClock;

    new-instance p3, Lee/cyber/smartid/tse/dto/InvalidResponseIdException;

    invoke-direct {p3}, Lee/cyber/smartid/tse/dto/InvalidResponseIdException;-><init>()V

    invoke-static {p1, p2, p3}, Lee/cyber/smartid/tse/dto/TSEError;->from(Landroid/content/Context;Lee/cyber/smartid/tse/inter/WallClock;Ljava/lang/Throwable;)Lee/cyber/smartid/tse/dto/TSEError;

    move-result-object p1

    return-object p1

    :cond_5
    if-nez p3, :cond_6

    .line 194
    invoke-virtual {p2}, Lc/r;->b()I

    move-result p1

    const/16 v0, 0xc8

    if-eq p1, v0, :cond_6

    .line 195
    iget-object p1, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->e:Lee/cyber/smartid/inter/ServiceAccess;

    invoke-interface {p1}, Lee/cyber/smartid/inter/ServiceAccess;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    iget-object p3, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->b:Lee/cyber/smartid/tse/inter/WallClock;

    new-instance p4, Lee/cyber/smartid/tse/dto/HttpErrorCodeResponseException;

    invoke-virtual {p2}, Lc/r;->b()I

    move-result p2

    invoke-direct {p4, p2}, Lee/cyber/smartid/tse/dto/HttpErrorCodeResponseException;-><init>(I)V

    invoke-static {p1, p3, p4}, Lee/cyber/smartid/tse/dto/TSEError;->from(Landroid/content/Context;Lee/cyber/smartid/tse/inter/WallClock;Ljava/lang/Throwable;)Lee/cyber/smartid/tse/dto/TSEError;

    move-result-object p1

    return-object p1

    :cond_6
    const/4 p1, 0x0

    if-eqz p3, :cond_7

    .line 196
    invoke-virtual {p3}, Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCResponse;->isError()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 197
    iget-object p2, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->e:Lee/cyber/smartid/inter/ServiceAccess;

    invoke-interface {p2}, Lee/cyber/smartid/inter/ServiceAccess;->getApplicationContext()Landroid/content/Context;

    move-result-object p2

    iget-object p4, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->b:Lee/cyber/smartid/tse/inter/WallClock;

    invoke-virtual {p3}, Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCResponse;->getError()Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCError;

    move-result-object p3

    invoke-static {p2, p4, p3, p1}, Lee/cyber/smartid/tse/dto/TSEError;->from(Landroid/content/Context;Lee/cyber/smartid/tse/inter/WallClock;Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCError;Ljava/lang/Throwable;)Lee/cyber/smartid/tse/dto/TSEError;

    move-result-object p1

    return-object p1

    :cond_7
    if-eqz p4, :cond_8

    .line 198
    invoke-virtual {p2}, Lc/r;->e()Ljava/lang/Object;

    move-result-object p3

    if-eqz p3, :cond_8

    invoke-virtual {p2}, Lc/r;->e()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCResponse;

    invoke-interface {p4, p2}, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper$ValidateResponseCallback;->isValidResponse(Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCResponse;)Z

    move-result p2

    if-nez p2, :cond_8

    .line 199
    iget-object p1, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->e:Lee/cyber/smartid/inter/ServiceAccess;

    invoke-interface {p1}, Lee/cyber/smartid/inter/ServiceAccess;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    iget-object p2, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->b:Lee/cyber/smartid/tse/inter/WallClock;

    new-instance p3, Lee/cyber/smartid/tse/dto/InvalidResponseResultException;

    invoke-direct {p3}, Lee/cyber/smartid/tse/dto/InvalidResponseResultException;-><init>()V

    invoke-static {p1, p2, p3}, Lee/cyber/smartid/tse/dto/TSEError;->from(Landroid/content/Context;Lee/cyber/smartid/tse/inter/WallClock;Ljava/lang/Throwable;)Lee/cyber/smartid/tse/dto/TSEError;

    move-result-object p1

    :cond_8
    return-object p1
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    const/4 v0, 0x3

    .line 10
    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p2, v0, v1

    const/4 p2, 0x1

    aput-object p1, v0, p2

    iget-object p1, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->e:Lee/cyber/smartid/inter/ServiceAccess;

    invoke-interface {p1}, Lee/cyber/smartid/tse/inter/ExternalResourceAccess;->getDeviceFingerPrint()Ljava/lang/String;

    move-result-object p1

    const/4 p2, 0x2

    aput-object p1, v0, p2

    const-string p1, "ee.cyber.smartid.APP_KEY_DATA_%1$s_%2$s_%3$s"

    invoke-static {p1, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private a()V
    .locals 2

    .line 1
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper$2;

    invoke-direct {v1, p0}, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper$2;-><init>(Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 2
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    return-void
.end method

.method private a(Lee/cyber/smartid/dto/SmartIdError;)V
    .locals 6

    .line 200
    iget-object v0, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->c:Lee/cyber/smartid/inter/ListenerAccess;

    const-class v1, Lee/cyber/smartid/inter/InitServiceListener;

    const/4 v2, 0x1

    invoke-interface {v0, v2, v1}, Lee/cyber/smartid/inter/ListenerAccess;->getListenersForType(ZLjava/lang/Class;)[Landroid/support/v4/e/j;

    move-result-object v0

    .line 201
    array-length v1, v0

    if-nez v1, :cond_0

    return-void

    .line 202
    :cond_0
    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_1

    aget-object v3, v0, v2

    .line 203
    :try_start_0
    iget-object v4, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->e:Lee/cyber/smartid/inter/ServiceAccess;

    new-instance v5, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper$10;

    invoke-direct {v5, p0, v3, p1}, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper$10;-><init>(Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;Landroid/support/v4/e/j;Lee/cyber/smartid/dto/SmartIdError;)V

    invoke-interface {v4, v5}, Lee/cyber/smartid/inter/ServiceAccess;->notifyUI(Ljava/lang/Runnable;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v3

    .line 204
    iget-object v4, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->n:Lee/cyber/smartid/util/Log;

    const-string v5, "notifyUpgradeStart"

    invoke-virtual {v4, v5, v3}, Lee/cyber/smartid/util/Log;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method private a(Lee/cyber/smartid/dto/upgrade/InteractiveUpgradeState;Lee/cyber/smartid/dto/SmartIdError;)V
    .locals 2

    const/4 v0, 0x4

    .line 3
    :try_start_0
    invoke-virtual {p1, v0}, Lee/cyber/smartid/dto/upgrade/InteractiveUpgradeState;->setStateUpgradeToMDv2(I)V

    .line 4
    invoke-direct {p0, p1}, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->c(Lee/cyber/smartid/dto/upgrade/InteractiveUpgradeState;)V
    :try_end_0
    .catch Lee/cyber/smartid/cryptolib/dto/StorageException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 5
    iget-object v0, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->n:Lee/cyber/smartid/util/Log;

    const-string v1, "storeAndNotifyUpgradeFailed - failed to store the upgrade failure"

    invoke-virtual {v0, v1, p1}, Lee/cyber/smartid/util/Log;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 6
    :goto_0
    invoke-direct {p0, p2}, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->a(Lee/cyber/smartid/dto/SmartIdError;)V

    return-void
.end method

.method static synthetic a(Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;Lee/cyber/smartid/dto/upgrade/InteractiveUpgradeState;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lee/cyber/smartid/cryptolib/dto/StorageException;
        }
    .end annotation

    .line 2
    invoke-direct {p0, p1}, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->c(Lee/cyber/smartid/dto/upgrade/InteractiveUpgradeState;)V

    return-void
.end method

.method private a(Ljava/lang/String;Ljava/util/HashSet;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/HashSet<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lee/cyber/smartid/cryptolib/dto/StorageException;
        }
    .end annotation

    .line 237
    invoke-direct {p0}, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->j()Ljava/util/ArrayList;

    move-result-object v0

    .line 238
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 239
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 240
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lee/cyber/smartid/dto/AccountWrapper;

    .line 241
    invoke-virtual {v2}, Lee/cyber/smartid/dto/AccountWrapper;->getAccountUUID()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 242
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    .line 243
    :cond_1
    iget-object v1, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->f:Lee/cyber/smartid/cryptolib/inter/StorageOp;

    invoke-direct {p0}, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->k()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Lee/cyber/smartid/cryptolib/inter/StorageOp;->storeData(Ljava/lang/String;Ljava/lang/Object;)V

    const-string v0, "AUTHENTICATION"

    const-string v1, "SIGNATURE"

    .line 244
    filled-new-array {v0, v1}, [Ljava/lang/String;

    move-result-object v0

    .line 245
    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_4

    aget-object v3, v0, v2

    if-eqz p2, :cond_3

    .line 246
    invoke-virtual {p2, p1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    goto :goto_1

    .line 247
    :cond_2
    iget-object v4, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->n:Lee/cyber/smartid/util/Log;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "deletePreMDv2AccountAndStates - skipping Key for "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v6, ", this is already the new MDv2 key."

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    goto :goto_2

    .line 248
    :cond_3
    :goto_1
    iget-object v4, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->f:Lee/cyber/smartid/cryptolib/inter/StorageOp;

    invoke-direct {p0, p1, v3}, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Lee/cyber/smartid/cryptolib/inter/StorageOp;->removeData(Ljava/lang/String;)V

    .line 249
    :goto_2
    iget-object v4, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->f:Lee/cyber/smartid/cryptolib/inter/StorageOp;

    invoke-direct {p0, p1, v3}, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Lee/cyber/smartid/cryptolib/inter/StorageOp;->removeData(Ljava/lang/String;)V

    .line 250
    iget-object v4, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->f:Lee/cyber/smartid/cryptolib/inter/StorageOp;

    invoke-direct {p0, p1, v3}, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->c(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Lee/cyber/smartid/cryptolib/inter/StorageOp;->removeData(Ljava/lang/String;)V

    .line 251
    iget-object v4, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->f:Lee/cyber/smartid/cryptolib/inter/StorageOp;

    invoke-direct {p0, p1, v3}, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->d(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v4, v3}, Lee/cyber/smartid/cryptolib/inter/StorageOp;->removeData(Ljava/lang/String;)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_4
    return-void
.end method

.method private a(Lcom/a/a/n;Ljava/lang/String;Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper$KTKPublicKeyWrapper;[Lee/cyber/smartid/cryptolib/dto/ValuePair;)Z
    .locals 17
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/a/a/n;",
            "Ljava/lang/String;",
            "Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper$KTKPublicKeyWrapper;",
            "[",
            "Lee/cyber/smartid/cryptolib/dto/ValuePair<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    move-object/from16 v1, p0

    move-object/from16 v0, p3

    const/4 v2, 0x0

    .line 86
    :try_start_0
    invoke-virtual/range {p3 .. p3}, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper$KTKPublicKeyWrapper;->a()Z

    move-result v3
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_2

    const-string v4, "Method \"verifyKTKSignedJWSAndCheckForRequiredContentValues\" invoked successfully, isValid: "

    const/4 v5, 0x6

    const-string v6, "DH"

    const/4 v7, 0x5

    const-string v8, "CLIENT"

    const/4 v9, 0x4

    const/4 v10, 0x3

    const/4 v11, 0x2

    const/4 v12, 0x1

    const/4 v13, 0x7

    if-eqz v3, :cond_0

    .line 87
    :try_start_1
    iget-object v3, v1, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->n:Lee/cyber/smartid/util/Log;
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_2

    const-string v14, "verifyKTKSignedJWSAndCheckForRequiredContentValues - trying to invoke the newer \"verifyKTKSignedJWSAndCheckForRequiredContentValues\" method .."

    :try_start_2
    invoke-virtual {v3, v14}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    .line 88
    invoke-direct/range {p0 .. p0}, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->c()Ljava/lang/reflect/Method;

    move-result-object v3

    .line 89
    iget-object v14, v1, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->i:Lee/cyber/smartid/cryptolib/inter/CryptoOp;

    const/16 v15, 0x8

    new-array v15, v15, [Ljava/lang/Object;

    invoke-virtual/range {p1 .. p1}, Lcom/a/a/n;->a()Lcom/a/a/v;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Lcom/a/a/v;->toString()Ljava/lang/String;

    move-result-object v16

    aput-object v16, v15, v2

    iget-object v2, v0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper$KTKPublicKeyWrapper;->d:Ljava/lang/String;

    aput-object v2, v15, v12

    iget-object v2, v0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper$KTKPublicKeyWrapper;->e:Ljava/lang/String;

    aput-object v2, v15, v11

    iget-object v0, v0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper$KTKPublicKeyWrapper;->b:Ljava/lang/String;

    aput-object v0, v15, v10

    aput-object p2, v15, v9

    aput-object v8, v15, v7

    aput-object v6, v15, v5

    aput-object p4, v15, v13

    invoke-virtual {v3, v14, v15}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 90
    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_2

    .line 91
    :try_start_3
    iget-object v0, v1, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->n:Lee/cyber/smartid/util/Log;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_0

    goto :goto_0

    .line 92
    :cond_0
    :try_start_4
    invoke-virtual/range {p3 .. p3}, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper$KTKPublicKeyWrapper;->b()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 93
    iget-object v2, v1, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->n:Lee/cyber/smartid/util/Log;
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_2

    const-string v3, "verifyKTKSignedJWSAndCheckForRequiredContentValues - trying to invoke the older \"verifyKTKSignedJWSAndCheckForRequiredContentValues\" method .."

    :try_start_5
    invoke-virtual {v2, v3}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    .line 94
    invoke-direct/range {p0 .. p0}, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->e()Ljava/lang/reflect/Method;

    move-result-object v2

    .line 95
    iget-object v3, v1, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->i:Lee/cyber/smartid/cryptolib/inter/CryptoOp;

    new-array v13, v13, [Ljava/lang/Object;

    invoke-virtual/range {p1 .. p1}, Lcom/a/a/n;->a()Lcom/a/a/v;

    move-result-object v14

    invoke-virtual {v14}, Lcom/a/a/v;->toString()Ljava/lang/String;

    move-result-object v14
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_2

    const/4 v15, 0x0

    :try_start_6
    aput-object v14, v13, v15

    iget-object v14, v0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper$KTKPublicKeyWrapper;->c:Ljava/lang/String;

    aput-object v14, v13, v12

    iget-object v0, v0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper$KTKPublicKeyWrapper;->b:Ljava/lang/String;

    aput-object v0, v13, v11

    aput-object p2, v13, v10

    aput-object v8, v13, v9

    aput-object v6, v13, v7

    aput-object p4, v13, v5

    invoke-virtual {v2, v3, v13}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 96
    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2
    :try_end_6
    .catch Ljava/lang/Throwable; {:try_start_6 .. :try_end_6} :catch_1

    .line 97
    :try_start_7
    iget-object v0, v1, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->n:Lee/cyber/smartid/util/Log;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V
    :try_end_7
    .catch Ljava/lang/Throwable; {:try_start_7 .. :try_end_7} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    move v15, v2

    goto :goto_1

    :catch_1
    move-exception v0

    goto :goto_1

    :cond_1
    const/4 v15, 0x0

    const/4 v2, 0x0

    :goto_0
    return v2

    :catch_2
    move-exception v0

    const/4 v15, 0x0

    .line 98
    :goto_1
    iget-object v2, v1, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->n:Lee/cyber/smartid/util/Log;

    const-string v3, "verifyKTKSignedJWSAndCheckForRequiredContentValues"

    invoke-virtual {v2, v3, v0}, Lee/cyber/smartid/util/Log;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    return v15
.end method

.method private a(Lee/cyber/smartid/dto/AccountWrapper;)Z
    .locals 8

    .line 205
    invoke-virtual {p1}, Lee/cyber/smartid/dto/AccountWrapper;->getAccountUUID()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    return v1

    .line 206
    :cond_0
    invoke-virtual {p1}, Lee/cyber/smartid/dto/AccountWrapper;->getAccountUUID()Ljava/lang/String;

    move-result-object p1

    const-string v0, "AUTHENTICATION"

    const-string v2, "SIGNATURE"

    .line 207
    filled-new-array {v0, v2}, [Ljava/lang/String;

    move-result-object v0

    .line 208
    array-length v2, v0

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v2, :cond_7

    aget-object v4, v0, v3

    .line 209
    iget-object v5, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->f:Lee/cyber/smartid/cryptolib/inter/StorageOp;

    invoke-direct {p0, p1, v4}, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    new-instance v7, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper$12;

    invoke-direct {v7, p0}, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper$12;-><init>(Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;)V

    .line 210
    invoke-virtual {v7}, Lcom/google/gson/b/a;->getType()Ljava/lang/reflect/Type;

    move-result-object v7

    .line 211
    invoke-interface {v5, v6, v7}, Lee/cyber/smartid/cryptolib/inter/StorageOp;->retrieveData(Ljava/lang/String;Ljava/lang/reflect/Type;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lee/cyber/smartid/dto/upgrade/v5/PreMDv2Key;

    if-nez v5, :cond_1

    .line 212
    iget-object p1, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->n:Lee/cyber/smartid/util/Log;

    const-string v0, "isAccountMDv2Upgradable - No, Key object missing!"

    invoke-virtual {p1, v0}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    return v1

    .line 213
    :cond_1
    invoke-virtual {v5}, Lee/cyber/smartid/dto/upgrade/v5/PreMDv2Key;->getD1Prime()Lee/cyber/smartid/dto/upgrade/v5/PreMDv2ClientShare;

    move-result-object v5

    if-nez v5, :cond_2

    .line 214
    iget-object p1, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->n:Lee/cyber/smartid/util/Log;

    const-string v0, "isAccountMDv2Upgradable - No, Key\'s d1Prime object missing!"

    invoke-virtual {p1, v0}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    return v1

    .line 215
    :cond_2
    iget-object v5, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->f:Lee/cyber/smartid/cryptolib/inter/StorageOp;

    invoke-direct {p0, p1, v4}, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    new-instance v7, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper$13;

    invoke-direct {v7, p0}, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper$13;-><init>(Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;)V

    .line 216
    invoke-virtual {v7}, Lcom/google/gson/b/a;->getType()Ljava/lang/reflect/Type;

    move-result-object v7

    .line 217
    invoke-interface {v5, v6, v7}, Lee/cyber/smartid/cryptolib/inter/StorageOp;->retrieveData(Ljava/lang/String;Ljava/lang/reflect/Type;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lee/cyber/smartid/dto/upgrade/v5/PreMDv2KeyState;

    if-eqz v5, :cond_6

    .line 218
    invoke-virtual {v5}, Lee/cyber/smartid/dto/upgrade/v5/PreMDv2KeyState;->getOneTimePassword()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_3

    goto :goto_2

    .line 219
    :cond_3
    iget-object v5, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->f:Lee/cyber/smartid/cryptolib/inter/StorageOp;

    invoke-direct {p0, p1, v4}, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->c(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    new-instance v6, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper$14;

    invoke-direct {v6, p0}, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper$14;-><init>(Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;)V

    .line 220
    invoke-virtual {v6}, Lcom/google/gson/b/a;->getType()Ljava/lang/reflect/Type;

    move-result-object v6

    .line 221
    invoke-interface {v5, v4, v6}, Lee/cyber/smartid/cryptolib/inter/StorageOp;->retrieveData(Ljava/lang/String;Ljava/lang/reflect/Type;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lee/cyber/smartid/dto/upgrade/v5/PreMDv2KeyStateMeta;

    if-eqz v4, :cond_5

    .line 222
    invoke-virtual {v4}, Lee/cyber/smartid/dto/upgrade/v5/PreMDv2KeyStateMeta;->isKeyLockedPermanently()Z

    move-result v4

    if-eqz v4, :cond_4

    goto :goto_1

    :cond_4
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 223
    :cond_5
    :goto_1
    iget-object p1, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->n:Lee/cyber/smartid/util/Log;

    const-string v0, "isAccountMDv2Upgradable - No, KeyStateMeta says key is permalocked"

    invoke-virtual {p1, v0}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    return v1

    .line 224
    :cond_6
    :goto_2
    iget-object p1, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->n:Lee/cyber/smartid/util/Log;

    const-string v0, "isAccountMDv2Upgradable - No, KeyState object or OTP missing!"

    invoke-virtual {p1, v0}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    return v1

    :cond_7
    const/4 p1, 0x1

    return p1
.end method

.method static synthetic a(Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;)Z
    .locals 0

    .line 1
    iget-boolean p0, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->k:Z

    return p0
.end method

.method private a(Lee/cyber/smartid/tse/dto/BaseError;)Z
    .locals 8

    const/4 v0, 0x0

    if-nez p1, :cond_0

    goto/16 :goto_1

    .line 172
    :cond_0
    invoke-virtual {p1}, Lee/cyber/smartid/tse/dto/BaseError;->getCode()J

    move-result-wide v1

    const-wide/16 v3, -0x792b

    cmp-long v5, v1, v3

    if-nez v5, :cond_1

    goto/16 :goto_1

    .line 173
    :cond_1
    invoke-virtual {p1}, Lee/cyber/smartid/tse/dto/BaseError;->getCode()J

    move-result-wide v1

    const-wide/16 v3, -0x7fbc

    cmp-long v5, v1, v3

    if-nez v5, :cond_2

    goto/16 :goto_0

    .line 174
    :cond_2
    invoke-virtual {p1}, Lee/cyber/smartid/tse/dto/BaseError;->getCode()J

    move-result-wide v1

    const-wide/16 v3, -0x7f58

    cmp-long v5, v1, v3

    if-nez v5, :cond_3

    goto :goto_0

    .line 175
    :cond_3
    invoke-virtual {p1}, Lee/cyber/smartid/tse/dto/BaseError;->getCode()J

    move-result-wide v1

    const-wide/16 v3, -0x7f5a

    cmp-long v5, v1, v3

    if-nez v5, :cond_4

    goto :goto_0

    .line 176
    :cond_4
    invoke-virtual {p1}, Lee/cyber/smartid/tse/dto/BaseError;->getCode()J

    move-result-wide v1

    const-wide/16 v3, -0x791d

    cmp-long v5, v1, v3

    if-nez v5, :cond_5

    goto :goto_0

    .line 177
    :cond_5
    invoke-virtual {p1}, Lee/cyber/smartid/tse/dto/BaseError;->getCode()J

    move-result-wide v1

    const-wide/16 v5, -0x792f

    cmp-long v7, v1, v5

    if-nez v7, :cond_6

    goto :goto_0

    .line 178
    :cond_6
    invoke-virtual {p1}, Lee/cyber/smartid/tse/dto/BaseError;->getCode()J

    move-result-wide v1

    const-wide/16 v5, -0x7930

    cmp-long v7, v1, v5

    if-nez v7, :cond_7

    goto :goto_0

    .line 179
    :cond_7
    invoke-virtual {p1}, Lee/cyber/smartid/tse/dto/BaseError;->getCode()J

    move-result-wide v1

    const-wide/16 v5, -0x791c

    cmp-long v7, v1, v5

    if-nez v7, :cond_8

    goto :goto_0

    .line 180
    :cond_8
    invoke-virtual {p1}, Lee/cyber/smartid/tse/dto/BaseError;->getCode()J

    move-result-wide v1

    cmp-long v5, v1, v3

    if-nez v5, :cond_9

    goto :goto_0

    .line 181
    :cond_9
    invoke-virtual {p1}, Lee/cyber/smartid/tse/dto/BaseError;->getCode()J

    move-result-wide v1

    const-wide/16 v3, -0x791a

    cmp-long v5, v1, v3

    if-nez v5, :cond_a

    goto :goto_0

    .line 182
    :cond_a
    invoke-virtual {p1}, Lee/cyber/smartid/tse/dto/BaseError;->getKeyStatus()Lee/cyber/smartid/tse/dto/jsonrpc/AccountKeyStatus;

    move-result-object v1

    if-eqz v1, :cond_b

    invoke-virtual {p1}, Lee/cyber/smartid/tse/dto/BaseError;->getKeyStatus()Lee/cyber/smartid/tse/dto/jsonrpc/AccountKeyStatus;

    move-result-object p1

    invoke-static {p1}, Lee/cyber/smartid/tse/util/Util;->isKeyLockedPermanently(Lee/cyber/smartid/tse/dto/jsonrpc/AccountKeyStatus;)Z

    move-result p1

    if-eqz p1, :cond_b

    :goto_0
    const/4 v0, 0x1

    :cond_b
    :goto_1
    return v0
.end method

.method private b(Lee/cyber/smartid/dto/upgrade/InteractiveUpgradeState;)Lee/cyber/smartid/dto/SmartIdError;
    .locals 27

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    const-string v3, ", marking this account for deletion"

    .line 32
    invoke-direct/range {p0 .. p0}, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->i()Ljava/util/ArrayList;

    move-result-object v0

    .line 33
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    const/4 v5, 0x0

    if-nez v4, :cond_0

    return-object v5

    :cond_0
    const-string v4, "AUTHENTICATION"

    const-string v6, "SIGNATURE"

    .line 34
    filled-new-array {v4, v6}, [Ljava/lang/String;

    move-result-object v4

    .line 35
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 36
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lee/cyber/smartid/dto/AccountWrapper;

    .line 37
    invoke-virtual {v8}, Lee/cyber/smartid/dto/AccountWrapper;->getAccountUUID()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0, v4}, Lee/cyber/smartid/dto/upgrade/InteractiveUpgradeState;->hasMDv2UpgradeDataFor(Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 38
    iget-object v0, v1, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->n:Lee/cyber/smartid/util/Log;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "createMDv2Accounts: Missing upgrade data for account "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v8}, Lee/cyber/smartid/dto/AccountWrapper;->getAccountUUID()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0, v8}, Lee/cyber/smartid/util/Log;->wtf(Ljava/lang/String;)V

    goto :goto_0

    .line 39
    :cond_1
    array-length v9, v4

    const/4 v11, 0x0

    :goto_1
    if-ge v11, v9, :cond_4

    aget-object v13, v4, v11

    .line 40
    invoke-virtual {v8}, Lee/cyber/smartid/dto/AccountWrapper;->getAccountUUID()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0, v13}, Lee/cyber/smartid/dto/upgrade/InteractiveUpgradeState;->getMDv2UpgradeResult(Ljava/lang/String;Ljava/lang/String;)Lee/cyber/smartid/dto/jsonrpc/result/UpgradeKeyPairResult;

    move-result-object v0

    invoke-virtual {v0}, Lee/cyber/smartid/dto/jsonrpc/result/UpgradeKeyPairResult;->getKeyData()Lee/cyber/smartid/tse/dto/jsonrpc/KeyData;

    move-result-object v14

    .line 41
    :try_start_0
    iget-object v0, v1, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->h:Lee/cyber/smartid/cryptolib/inter/KeyGenerationOp;

    invoke-virtual {v8}, Lee/cyber/smartid/dto/AccountWrapper;->getAccountUUID()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v2, v15, v13}, Lee/cyber/smartid/dto/upgrade/InteractiveUpgradeState;->getMDv2HDKeyPair(Ljava/lang/String;Ljava/lang/String;)Lee/cyber/smartid/cryptolib/dto/DiffieHellmanKeyPair;

    move-result-object v15

    iget-object v5, v1, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->d:Lee/cyber/smartid/cryptolib/inter/EncodingOp;

    invoke-virtual {v14}, Lee/cyber/smartid/tse/dto/jsonrpc/KeyData;->getServerDhPublicKey()Ljava/lang/String;

    move-result-object v12

    invoke-interface {v5, v12}, Lee/cyber/smartid/cryptolib/inter/EncodingOp;->decodeDecimalFromBase64(Ljava/lang/String;)Ljava/math/BigInteger;

    move-result-object v5

    invoke-interface {v0, v15, v5}, Lee/cyber/smartid/cryptolib/inter/KeyGenerationOp;->calculateConcatKDFWithSHA256(Lee/cyber/smartid/cryptolib/dto/DiffieHellmanKeyPair;Ljava/math/BigInteger;)[B

    move-result-object v5
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 42
    :try_start_1
    iget-object v0, v1, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->i:Lee/cyber/smartid/cryptolib/inter/CryptoOp;

    invoke-virtual {v14}, Lee/cyber/smartid/tse/dto/jsonrpc/KeyData;->getServerDhMessage()Ljava/lang/String;

    move-result-object v17
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_c

    const/16 v19, 0x0

    const/16 v20, 0x0

    const-string v21, "CLIENT"

    move-object/from16 v16, v0

    move-object/from16 v18, v5

    :try_start_2
    invoke-interface/range {v16 .. v21}, Lee/cyber/smartid/cryptolib/inter/CryptoOp;->decryptFromTEKEncryptedJWE(Ljava/lang/String;[BLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/a/a/n;

    move-result-object v0

    .line 43
    invoke-virtual {v0}, Lcom/a/a/n;->c()Lcom/a/a/m;

    move-result-object v12

    invoke-virtual {v12}, Lcom/a/a/m;->a()Ljava/lang/String;

    move-result-object v12
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_c

    .line 44
    iget-object v15, v1, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->e:Lee/cyber/smartid/inter/ServiceAccess;

    invoke-interface {v15}, Lee/cyber/smartid/inter/ServiceAccess;->getPropertiesManager()Lee/cyber/smartid/manager/inter/PropertiesManager;

    move-result-object v15

    invoke-interface {v15}, Lee/cyber/smartid/manager/inter/PropertiesManager;->getPropAccountUpgradeSZId()Ljava/lang/String;

    move-result-object v15

    invoke-direct {v1, v15}, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->a(Ljava/lang/String;)Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper$KTKPublicKeyWrapper;

    move-result-object v15

    if-nez v15, :cond_2

    .line 45
    iget-object v0, v1, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->n:Lee/cyber/smartid/util/Log;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "createMDv2Accounts: Data verification failed for "

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v8}, Lee/cyber/smartid/dto/AccountWrapper;->getAccountUUID()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v9, ", unable to find the KTK, marking this account for deletion"

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Lee/cyber/smartid/util/Log;->e(Ljava/lang/String;)V

    :catch_0
    move-object/from16 v26, v4

    move-object/from16 v25, v6

    move-object/from16 v23, v7

    const/16 v22, 0x1

    goto/16 :goto_7

    :cond_2
    const/4 v10, 0x2

    .line 46
    :try_start_3
    new-array v10, v10, [Lee/cyber/smartid/cryptolib/dto/ValuePair;
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_6

    move-object/from16 v23, v7

    .line 47
    :try_start_4
    new-instance v7, Lee/cyber/smartid/cryptolib/dto/ValuePair;
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_5

    move/from16 v24, v9

    const-string v9, "clientDhPublicKey"

    move-object/from16 v25, v6

    :try_start_5
    iget-object v6, v1, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->d:Lee/cyber/smartid/cryptolib/inter/EncodingOp;
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_4

    move-object/from16 v26, v4

    :try_start_6
    invoke-virtual {v8}, Lee/cyber/smartid/dto/AccountWrapper;->getAccountUUID()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4, v13}, Lee/cyber/smartid/dto/upgrade/InteractiveUpgradeState;->getMDv2HDKeyPair(Ljava/lang/String;Ljava/lang/String;)Lee/cyber/smartid/cryptolib/dto/DiffieHellmanKeyPair;

    move-result-object v4

    invoke-virtual {v4}, Lee/cyber/smartid/cryptolib/dto/DiffieHellmanKeyPair;->getPublicKey()Ljava/math/BigInteger;

    move-result-object v4

    invoke-interface {v6, v4}, Lee/cyber/smartid/cryptolib/inter/EncodingOp;->encodeDecimalToBase64(Ljava/math/BigInteger;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v7, v9, v4}, Lee/cyber/smartid/cryptolib/dto/ValuePair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_6
    .catch Ljava/lang/Throwable; {:try_start_6 .. :try_end_6} :catch_3

    const/4 v4, 0x0

    :try_start_7
    aput-object v7, v10, v4

    .line 48
    new-instance v6, Lee/cyber/smartid/cryptolib/dto/ValuePair;
    :try_end_7
    .catch Ljava/lang/Throwable; {:try_start_7 .. :try_end_7} :catch_2

    const-string v7, "serverDhPublicKey"

    :try_start_8
    invoke-virtual {v14}, Lee/cyber/smartid/tse/dto/jsonrpc/KeyData;->getServerDhPublicKey()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v6, v7, v9}, Lee/cyber/smartid/cryptolib/dto/ValuePair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_8
    .catch Ljava/lang/Throwable; {:try_start_8 .. :try_end_8} :catch_2

    const/16 v22, 0x1

    :try_start_9
    aput-object v6, v10, v22

    .line 49
    invoke-direct {v1, v0, v12, v15, v10}, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->a(Lcom/a/a/n;Ljava/lang/String;Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper$KTKPublicKeyWrapper;[Lee/cyber/smartid/cryptolib/dto/ValuePair;)Z

    move-result v10
    :try_end_9
    .catch Ljava/lang/Throwable; {:try_start_9 .. :try_end_9} :catch_1

    goto :goto_6

    :catch_1
    move-exception v0

    goto :goto_5

    :catch_2
    move-exception v0

    goto :goto_4

    :catch_3
    move-exception v0

    goto :goto_3

    :catch_4
    move-exception v0

    move-object/from16 v26, v4

    goto :goto_3

    :catch_5
    move-exception v0

    move-object/from16 v26, v4

    move-object/from16 v25, v6

    goto :goto_2

    :catch_6
    move-exception v0

    move-object/from16 v26, v4

    move-object/from16 v25, v6

    move-object/from16 v23, v7

    :goto_2
    move/from16 v24, v9

    :goto_3
    const/4 v4, 0x0

    :goto_4
    const/16 v22, 0x1

    .line 50
    :goto_5
    iget-object v6, v1, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->n:Lee/cyber/smartid/util/Log;

    const-string v7, "createMDv2Accounts - server response validation failed"

    invoke-virtual {v6, v7, v0}, Lee/cyber/smartid/util/Log;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    const/4 v10, 0x0

    :goto_6
    if-nez v10, :cond_3

    goto/16 :goto_7

    .line 51
    :cond_3
    :try_start_a
    iget-object v0, v1, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->i:Lee/cyber/smartid/cryptolib/inter/CryptoOp;

    invoke-virtual {v14}, Lee/cyber/smartid/tse/dto/jsonrpc/KeyData;->getResponseData()Ljava/lang/String;

    move-result-object v17
    :try_end_a
    .catch Ljava/lang/Throwable; {:try_start_a .. :try_end_a} :catch_b

    const-string v21, "CLIENT"

    move-object/from16 v16, v0

    move-object/from16 v18, v5

    move-object/from16 v19, v12

    move-object/from16 v20, v12

    :try_start_b
    invoke-interface/range {v16 .. v21}, Lee/cyber/smartid/cryptolib/inter/CryptoOp;->decryptFromTEKEncryptedJWE(Ljava/lang/String;[BLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/a/a/n;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/a/n;->a()Lcom/a/a/v;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/a/v;->toString()Ljava/lang/String;

    move-result-object v0

    .line 52
    invoke-static {}, Lee/cyber/smartid/tse/network/TSEAPI;->createGSONBuilderForAPIResponses()Lcom/google/gson/g;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/gson/g;->a()Lcom/google/gson/f;

    move-result-object v6

    const-class v7, Lee/cyber/smartid/tse/dto/jsonrpc/payload/OneTimePasswordSZPayload;

    invoke-virtual {v6, v0, v7}, Lcom/google/gson/f;->a(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lee/cyber/smartid/tse/dto/jsonrpc/payload/OneTimePasswordSZPayload;

    invoke-virtual {v0}, Lee/cyber/smartid/tse/dto/jsonrpc/payload/OneTimePasswordSZPayload;->getOneTimePassword()Ljava/lang/String;

    move-result-object v0
    :try_end_b
    .catch Ljava/lang/Throwable; {:try_start_b .. :try_end_b} :catch_b

    .line 53
    iget-object v6, v1, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->f:Lee/cyber/smartid/cryptolib/inter/StorageOp;

    invoke-virtual {v8}, Lee/cyber/smartid/dto/AccountWrapper;->getAccountUUID()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v1, v7, v13}, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    new-instance v9, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper$3;

    invoke-direct {v9, v1}, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper$3;-><init>(Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;)V

    .line 54
    invoke-virtual {v9}, Lcom/google/gson/b/a;->getType()Ljava/lang/reflect/Type;

    move-result-object v9

    .line 55
    invoke-interface {v6, v7, v9}, Lee/cyber/smartid/cryptolib/inter/StorageOp;->retrieveData(Ljava/lang/String;Ljava/lang/reflect/Type;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lee/cyber/smartid/dto/upgrade/v5/PreMDv2Key;

    .line 56
    new-instance v7, Lee/cyber/smartid/dto/upgrade/v5/PreMDv2AndMDv2ClientShare;

    invoke-virtual {v6}, Lee/cyber/smartid/dto/upgrade/v5/PreMDv2Key;->getD1Prime()Lee/cyber/smartid/dto/upgrade/v5/PreMDv2ClientShare;

    move-result-object v9

    invoke-virtual {v9}, Lee/cyber/smartid/dto/upgrade/v5/PreMDv2ClientShare;->getKeyShare()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v6}, Lee/cyber/smartid/dto/upgrade/v5/PreMDv2Key;->getD1Prime()Lee/cyber/smartid/dto/upgrade/v5/PreMDv2ClientShare;

    move-result-object v10

    invoke-virtual {v10}, Lee/cyber/smartid/dto/upgrade/v5/PreMDv2ClientShare;->getKeySize()I

    move-result v10

    invoke-direct {v7, v9, v10}, Lee/cyber/smartid/dto/upgrade/v5/PreMDv2AndMDv2ClientShare;-><init>(Ljava/lang/String;I)V

    .line 57
    invoke-virtual {v8}, Lee/cyber/smartid/dto/AccountWrapper;->getAccountUUID()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v1, v9, v13}, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 58
    new-instance v10, Lee/cyber/smartid/dto/upgrade/v5/MDv2Key;

    invoke-virtual {v6}, Lee/cyber/smartid/dto/upgrade/v5/PreMDv2Key;->getN1()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v6}, Lee/cyber/smartid/dto/upgrade/v5/PreMDv2Key;->getKeyPinLength()I

    move-result v6

    iget-object v4, v1, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->e:Lee/cyber/smartid/inter/ServiceAccess;

    invoke-interface {v4}, Lee/cyber/smartid/inter/ServiceAccess;->getPropertiesManager()Lee/cyber/smartid/manager/inter/PropertiesManager;

    move-result-object v4

    invoke-interface {v4}, Lee/cyber/smartid/manager/inter/PropertiesManager;->getPropAccountUpgradeSZId()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v10, v7, v15, v6, v4}, Lee/cyber/smartid/dto/upgrade/v5/MDv2Key;-><init>(Lee/cyber/smartid/dto/upgrade/v5/PreMDv2AndMDv2ClientShare;Ljava/lang/String;ILjava/lang/String;)V

    .line 59
    invoke-virtual {v14}, Lee/cyber/smartid/tse/dto/jsonrpc/KeyData;->getCompositeModulusBits()I

    move-result v4

    invoke-virtual {v10, v4}, Lee/cyber/smartid/dto/upgrade/v5/MDv2Key;->setCompositeModulusBits(I)V

    .line 60
    invoke-virtual {v14}, Lee/cyber/smartid/tse/dto/jsonrpc/KeyData;->getCompositeModulus()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v10, v4}, Lee/cyber/smartid/dto/upgrade/v5/MDv2Key;->setCompositeModulus(Ljava/lang/String;)V

    .line 61
    iget-object v4, v1, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->d:Lee/cyber/smartid/cryptolib/inter/EncodingOp;

    invoke-interface {v4, v5}, Lee/cyber/smartid/cryptolib/inter/EncodingOp;->encodeBytesToBase64([B)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v10, v12, v4}, Lee/cyber/smartid/dto/upgrade/v5/MDv2Key;->setDHDerivedKey(Ljava/lang/String;Ljava/lang/String;)V

    .line 62
    :try_start_c
    iget-object v4, v1, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->f:Lee/cyber/smartid/cryptolib/inter/StorageOp;

    invoke-interface {v4, v9, v10}, Lee/cyber/smartid/cryptolib/inter/StorageOp;->storeData(Ljava/lang/String;Ljava/lang/Object;)V
    :try_end_c
    .catch Lee/cyber/smartid/cryptolib/dto/StorageException; {:try_start_c .. :try_end_c} :catch_a

    .line 63
    invoke-virtual {v8}, Lee/cyber/smartid/dto/AccountWrapper;->getAccountUUID()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v4, v13}, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->e(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 64
    invoke-virtual {v8}, Lee/cyber/smartid/dto/AccountWrapper;->getAccountUUID()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v9, v5, v13, v0}, Lee/cyber/smartid/dto/upgrade/v5/MDv2KeyState;->forIdle(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lee/cyber/smartid/dto/upgrade/v5/MDv2KeyState;

    move-result-object v0

    .line 65
    :try_start_d
    iget-object v5, v1, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->f:Lee/cyber/smartid/cryptolib/inter/StorageOp;

    invoke-interface {v5, v4, v0}, Lee/cyber/smartid/cryptolib/inter/StorageOp;->storeData(Ljava/lang/String;Ljava/lang/Object;)V
    :try_end_d
    .catch Lee/cyber/smartid/cryptolib/dto/StorageException; {:try_start_d .. :try_end_d} :catch_9

    .line 66
    invoke-virtual {v8}, Lee/cyber/smartid/dto/AccountWrapper;->getAccountUUID()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0, v13}, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->f(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 67
    new-instance v4, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper$4;

    invoke-direct {v4, v1}, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper$4;-><init>(Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;)V

    invoke-static {v4, v0}, Lee/cyber/smartid/dto/upgrade/v5/MDv2KeyStateMeta;->forIdle(Lee/cyber/smartid/tse/inter/WallClock;Ljava/lang/String;)Lee/cyber/smartid/dto/upgrade/v5/MDv2KeyStateMeta;

    move-result-object v4

    .line 68
    :try_start_e
    iget-object v5, v1, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->f:Lee/cyber/smartid/cryptolib/inter/StorageOp;

    invoke-interface {v5, v0, v4}, Lee/cyber/smartid/cryptolib/inter/StorageOp;->storeData(Ljava/lang/String;Ljava/lang/Object;)V
    :try_end_e
    .catch Lee/cyber/smartid/cryptolib/dto/StorageException; {:try_start_e .. :try_end_e} :catch_8

    .line 69
    :try_start_f
    iget-object v0, v1, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->f:Lee/cyber/smartid/cryptolib/inter/StorageOp;

    invoke-virtual {v8}, Lee/cyber/smartid/dto/AccountWrapper;->getAccountUUID()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v4, v13}, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->g(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v14}, Lee/cyber/smartid/tse/dto/jsonrpc/KeyData;->getFreshnessToken()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v0, v4, v5}, Lee/cyber/smartid/cryptolib/inter/StorageOp;->storeData(Ljava/lang/String;Ljava/lang/Object;)V
    :try_end_f
    .catch Lee/cyber/smartid/cryptolib/dto/StorageException; {:try_start_f .. :try_end_f} :catch_7

    add-int/lit8 v11, v11, 0x1

    move-object/from16 v7, v23

    move/from16 v9, v24

    move-object/from16 v6, v25

    move-object/from16 v4, v26

    const/4 v5, 0x0

    goto/16 :goto_1

    :catch_7
    move-exception v0

    .line 70
    iget-object v2, v1, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->e:Lee/cyber/smartid/inter/ServiceAccess;

    invoke-interface {v2}, Lee/cyber/smartid/inter/ServiceAccess;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, v1, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->b:Lee/cyber/smartid/tse/inter/WallClock;

    invoke-static {v2, v3, v0}, Lee/cyber/smartid/dto/SmartIdError;->from(Landroid/content/Context;Lee/cyber/smartid/tse/inter/WallClock;Ljava/lang/Throwable;)Lee/cyber/smartid/dto/SmartIdError;

    move-result-object v0

    return-object v0

    :catch_8
    move-exception v0

    .line 71
    iget-object v2, v1, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->e:Lee/cyber/smartid/inter/ServiceAccess;

    invoke-interface {v2}, Lee/cyber/smartid/inter/ServiceAccess;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, v1, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->b:Lee/cyber/smartid/tse/inter/WallClock;

    invoke-static {v2, v3, v0}, Lee/cyber/smartid/dto/SmartIdError;->from(Landroid/content/Context;Lee/cyber/smartid/tse/inter/WallClock;Ljava/lang/Throwable;)Lee/cyber/smartid/dto/SmartIdError;

    move-result-object v0

    return-object v0

    :catch_9
    move-exception v0

    .line 72
    iget-object v2, v1, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->e:Lee/cyber/smartid/inter/ServiceAccess;

    invoke-interface {v2}, Lee/cyber/smartid/inter/ServiceAccess;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, v1, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->b:Lee/cyber/smartid/tse/inter/WallClock;

    invoke-static {v2, v3, v0}, Lee/cyber/smartid/dto/SmartIdError;->from(Landroid/content/Context;Lee/cyber/smartid/tse/inter/WallClock;Ljava/lang/Throwable;)Lee/cyber/smartid/dto/SmartIdError;

    move-result-object v0

    return-object v0

    :catch_a
    move-exception v0

    .line 73
    iget-object v2, v1, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->e:Lee/cyber/smartid/inter/ServiceAccess;

    invoke-interface {v2}, Lee/cyber/smartid/inter/ServiceAccess;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, v1, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->b:Lee/cyber/smartid/tse/inter/WallClock;

    invoke-static {v2, v3, v0}, Lee/cyber/smartid/dto/SmartIdError;->from(Landroid/content/Context;Lee/cyber/smartid/tse/inter/WallClock;Ljava/lang/Throwable;)Lee/cyber/smartid/dto/SmartIdError;

    move-result-object v0

    return-object v0

    .line 74
    :catch_b
    iget-object v0, v1, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->n:Lee/cyber/smartid/util/Log;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "createMDv2Accounts: Failed to read the OTP for "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v8}, Lee/cyber/smartid/dto/AccountWrapper;->getAccountUUID()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lee/cyber/smartid/util/Log;->wtf(Ljava/lang/String;)V

    goto :goto_7

    :catch_c
    move-object/from16 v26, v4

    move-object/from16 v25, v6

    move-object/from16 v23, v7

    const/16 v22, 0x1

    .line 75
    iget-object v0, v1, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->n:Lee/cyber/smartid/util/Log;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "createMDv2Accounts: Data decryption failed for "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v8}, Lee/cyber/smartid/dto/AccountWrapper;->getAccountUUID()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lee/cyber/smartid/util/Log;->wtf(Ljava/lang/String;)V

    goto :goto_7

    :cond_4
    move-object/from16 v26, v4

    move-object/from16 v25, v6

    move-object/from16 v23, v7

    const/16 v22, 0x0

    :goto_7
    if-eqz v22, :cond_5

    .line 76
    :try_start_10
    invoke-virtual {v8}, Lee/cyber/smartid/dto/AccountWrapper;->getAccountUUID()Ljava/lang/String;

    move-result-object v0

    const/4 v4, 0x0

    invoke-direct {v1, v0, v4}, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->a(Ljava/lang/String;Ljava/util/HashSet;)V

    .line 77
    invoke-virtual {v8}, Lee/cyber/smartid/dto/AccountWrapper;->getAccountUUID()Ljava/lang/String;

    move-result-object v0

    move-object/from16 v4, v26

    invoke-virtual {v2, v0, v4}, Lee/cyber/smartid/dto/upgrade/InteractiveUpgradeState;->removeMDv2UpgradeData(Ljava/lang/String;[Ljava/lang/String;)V

    .line 78
    invoke-direct/range {p0 .. p1}, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->c(Lee/cyber/smartid/dto/upgrade/InteractiveUpgradeState;)V
    :try_end_10
    .catch Lee/cyber/smartid/cryptolib/dto/StorageException; {:try_start_10 .. :try_end_10} :catch_d

    move-object/from16 v7, v23

    move-object/from16 v6, v25

    goto :goto_8

    :catch_d
    move-exception v0

    .line 79
    iget-object v2, v1, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->e:Lee/cyber/smartid/inter/ServiceAccess;

    invoke-interface {v2}, Lee/cyber/smartid/inter/ServiceAccess;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, v1, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->b:Lee/cyber/smartid/tse/inter/WallClock;

    invoke-static {v2, v3, v0}, Lee/cyber/smartid/dto/SmartIdError;->from(Landroid/content/Context;Lee/cyber/smartid/tse/inter/WallClock;Ljava/lang/Throwable;)Lee/cyber/smartid/dto/SmartIdError;

    move-result-object v0

    return-object v0

    :cond_5
    move-object/from16 v4, v26

    .line 80
    new-instance v0, Lee/cyber/smartid/dto/upgrade/v5/MDv2AccountState;

    invoke-virtual {v8}, Lee/cyber/smartid/dto/AccountWrapper;->getAccountUUID()Ljava/lang/String;

    move-result-object v5

    const-string v6, "REGISTRATION_STATE_ACCOUNT_REGISTERED"

    const-string v7, ""

    invoke-direct {v0, v5, v6, v7}, Lee/cyber/smartid/dto/upgrade/v5/MDv2AccountState;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    const-string v5, "STATE_SUBMIT_CLIENT_SECOND_PART_CONFIRMED"

    .line 81
    invoke-virtual {v0, v5}, Lee/cyber/smartid/dto/upgrade/v5/MDv2AccountState;->setAuthSubmitClientSecondPartState(Ljava/lang/String;)V

    .line 82
    invoke-virtual {v0, v5}, Lee/cyber/smartid/dto/upgrade/v5/MDv2AccountState;->setSignSubmitClientSecondPartState(Ljava/lang/String;)V

    move-object/from16 v5, v25

    .line 83
    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-object v6, v5

    move-object/from16 v7, v23

    :goto_8
    const/4 v5, 0x0

    goto/16 :goto_0

    :cond_6
    move-object v5, v6

    .line 84
    :try_start_11
    iget-object v0, v1, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->f:Lee/cyber/smartid/cryptolib/inter/StorageOp;

    invoke-direct/range {p0 .. p0}, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->l()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2, v5}, Lee/cyber/smartid/cryptolib/inter/StorageOp;->storeData(Ljava/lang/String;Ljava/lang/Object;)V
    :try_end_11
    .catch Lee/cyber/smartid/cryptolib/dto/StorageException; {:try_start_11 .. :try_end_11} :catch_e

    const/4 v2, 0x0

    return-object v2

    :catch_e
    move-exception v0

    .line 85
    iget-object v2, v1, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->e:Lee/cyber/smartid/inter/ServiceAccess;

    invoke-interface {v2}, Lee/cyber/smartid/inter/ServiceAccess;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, v1, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->b:Lee/cyber/smartid/tse/inter/WallClock;

    invoke-static {v2, v3, v0}, Lee/cyber/smartid/dto/SmartIdError;->from(Landroid/content/Context;Lee/cyber/smartid/tse/inter/WallClock;Ljava/lang/Throwable;)Lee/cyber/smartid/dto/SmartIdError;

    move-result-object v0

    return-object v0
.end method

.method private b(Lee/cyber/smartid/dto/upgrade/InteractiveUpgradeState;Ljava/lang/String;Ljava/lang/String;)Lee/cyber/smartid/dto/jsonrpc/param/UpgradeKeyPairParams;
    .locals 0

    .line 2
    invoke-virtual {p1, p2, p3}, Lee/cyber/smartid/dto/upgrade/InteractiveUpgradeState;->getMDv2UpgradeParams(Ljava/lang/String;Ljava/lang/String;)Lee/cyber/smartid/dto/jsonrpc/param/UpgradeKeyPairParams;

    move-result-object p1

    return-object p1
.end method

.method static synthetic b(Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;)Lee/cyber/smartid/inter/ListenerAccess;
    .locals 0

    .line 1
    iget-object p0, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->c:Lee/cyber/smartid/inter/ListenerAccess;

    return-object p0
.end method

.method private b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    const/4 v0, 0x3

    .line 3
    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    iget-object p1, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->e:Lee/cyber/smartid/inter/ServiceAccess;

    invoke-interface {p1}, Lee/cyber/smartid/tse/inter/ExternalResourceAccess;->getDeviceFingerPrint()Ljava/lang/String;

    move-result-object p1

    const/4 v1, 0x1

    aput-object p1, v0, v1

    const/4 p1, 0x2

    aput-object p2, v0, p1

    const-string p1, "ee.cyber.smartid.APP_KEY_STATE_%1$s_%2$s_%3$s"

    invoke-static {p1, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private b()V
    .locals 9

    const-string v0, "startMDv2Upgrade"

    .line 1
    iget-boolean v1, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->k:Z

    if-eqz v1, :cond_0

    .line 2
    iget-object v0, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->n:Lee/cyber/smartid/util/Log;

    const-string v1, "startMDv2Upgrade - upgrade already in progress, letting the caller know and waiting for the previous result .."

    invoke-virtual {v0, v1}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    .line 3
    invoke-direct {p0}, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->g()V

    return-void

    .line 4
    :cond_0
    iget-object v1, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->m:Ljava/util/HashSet;

    invoke-virtual {v1}, Ljava/util/HashSet;->clear()V

    const/4 v1, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 5
    :try_start_0
    iput-boolean v2, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->k:Z

    .line 6
    invoke-direct {p0}, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->n()Lee/cyber/smartid/dto/upgrade/InteractiveUpgradeState;

    move-result-object v4
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 7
    :try_start_1
    invoke-virtual {v4}, Lee/cyber/smartid/dto/upgrade/InteractiveUpgradeState;->getStateUpgradeToMDv2()I

    move-result v5

    const/4 v6, 0x3

    if-ne v5, v6, :cond_1

    .line 8
    invoke-direct {p0}, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->h()V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 9
    iput-boolean v3, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->k:Z

    return-void

    .line 10
    :cond_1
    :try_start_2
    invoke-direct {p0}, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->g()V

    .line 11
    invoke-virtual {v4, v2}, Lee/cyber/smartid/dto/upgrade/InteractiveUpgradeState;->setStateUpgradeToMDv2(I)V

    .line 12
    invoke-direct {p0, v4}, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->c(Lee/cyber/smartid/dto/upgrade/InteractiveUpgradeState;)V

    .line 13
    iget-object v5, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->n:Lee/cyber/smartid/util/Log;
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    const-string v7, "startMDv2Upgrade - upgrading to MDv2 .."

    :try_start_3
    invoke-virtual {v5, v7}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    .line 14
    invoke-direct {p0, v4}, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->a(Lee/cyber/smartid/dto/upgrade/InteractiveUpgradeState;)Lee/cyber/smartid/dto/SmartIdError;

    move-result-object v5

    .line 15
    iget-object v7, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->n:Lee/cyber/smartid/util/Log;
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    const-string v8, "startMDv2Upgrade - upgrade done"

    :try_start_4
    invoke-virtual {v7, v8}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    if-nez v5, :cond_3

    .line 16
    invoke-virtual {v4, v6}, Lee/cyber/smartid/dto/upgrade/InteractiveUpgradeState;->setStateUpgradeToMDv2(I)V

    .line 17
    invoke-virtual {v4}, Lee/cyber/smartid/dto/upgrade/InteractiveUpgradeState;->removeAllMDv2UpgradeData()V

    .line 18
    invoke-direct {p0, v4}, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->c(Lee/cyber/smartid/dto/upgrade/InteractiveUpgradeState;)V
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_2
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 19
    :try_start_5
    iget-object v5, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->e:Lee/cyber/smartid/inter/ServiceAccess;

    invoke-interface {v5}, Lee/cyber/smartid/inter/ServiceAccess;->handleUpgrade()Lee/cyber/smartid/dto/upgrade/InteractiveUpgradeState;

    move-result-object v5

    .line 20
    invoke-virtual {v5}, Lee/cyber/smartid/dto/upgrade/InteractiveUpgradeState;->isUpgradePending()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 21
    invoke-direct {p0}, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->a()V

    .line 22
    iget-object v1, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->n:Lee/cyber/smartid/util/Log;
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    const-string v2, "startMDv2Upgrade - storage upgrade finished, but we new need another interactive upgrade .."

    :try_start_6
    invoke-virtual {v1, v2}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V
    :try_end_6
    .catch Ljava/lang/Throwable; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 23
    iput-boolean v3, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->k:Z

    return-void

    .line 24
    :cond_2
    :try_start_7
    iget-object v5, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->n:Lee/cyber/smartid/util/Log;
    :try_end_7
    .catch Ljava/lang/Throwable; {:try_start_7 .. :try_end_7} :catch_1
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    const-string v6, "startMDv2Upgrade - storage upgrade completed, no need for the next interactive upgrade, we are done"

    :try_start_8
    invoke-virtual {v5, v6}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V
    :try_end_8
    .catch Ljava/lang/Throwable; {:try_start_8 .. :try_end_8} :catch_1
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 25
    :try_start_9
    iget-object v5, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->e:Lee/cyber/smartid/inter/ServiceAccess;

    invoke-interface {v5}, Lee/cyber/smartid/inter/ServiceAccess;->setInitServiceDone()V

    .line 26
    iget-object v5, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->e:Lee/cyber/smartid/inter/ServiceAccess;

    invoke-interface {v5}, Lee/cyber/smartid/inter/ServiceAccess;->getPostInitManager()Lee/cyber/smartid/manager/inter/PostInitManager;

    move-result-object v5

    invoke-interface {v5}, Lee/cyber/smartid/manager/inter/PostInitManager;->executePostInitServiceTasks()V

    .line 27
    invoke-direct {p0}, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->h()V
    :try_end_9
    .catch Ljava/lang/Throwable; {:try_start_9 .. :try_end_9} :catch_2
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    .line 28
    :try_start_a
    iget-object v5, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->c:Lee/cyber/smartid/inter/ListenerAccess;
    :try_end_a
    .catch Ljava/lang/Throwable; {:try_start_a .. :try_end_a} :catch_0
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    const-string v6, "ee.cyber.smartid.ACTION_REFRESH_CLONE_DETECTION"

    :try_start_b
    invoke-interface {v5, v6, v1, v2, v3}, Lee/cyber/smartid/inter/ListenerAccess;->notifyAlarmHandlers(Ljava/lang/String;Ljava/lang/String;ZZ)V
    :try_end_b
    .catch Ljava/lang/Throwable; {:try_start_b .. :try_end_b} :catch_0
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v1

    .line 29
    :try_start_c
    iget-object v2, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->n:Lee/cyber/smartid/util/Log;

    invoke-virtual {v2, v0, v1}, Lee/cyber/smartid/util/Log;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    :catch_1
    move-exception v1

    .line 30
    iget-object v2, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->n:Lee/cyber/smartid/util/Log;
    :try_end_c
    .catch Ljava/lang/Throwable; {:try_start_c .. :try_end_c} :catch_2
    .catchall {:try_start_c .. :try_end_c} :catchall_0

    const-string v5, "Post-interactive upgrade data upgrade failed!"

    :try_start_d
    invoke-virtual {v2, v5, v1}, Lee/cyber/smartid/util/Log;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 31
    iget-object v2, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->e:Lee/cyber/smartid/inter/ServiceAccess;

    invoke-interface {v2}, Lee/cyber/smartid/inter/ServiceAccess;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    iget-object v5, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->b:Lee/cyber/smartid/tse/inter/WallClock;

    invoke-static {v2, v5, v1}, Lee/cyber/smartid/dto/SmartIdError;->from(Landroid/content/Context;Lee/cyber/smartid/tse/inter/WallClock;Ljava/lang/Throwable;)Lee/cyber/smartid/dto/SmartIdError;

    move-result-object v1

    invoke-direct {p0, v1}, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->a(Lee/cyber/smartid/dto/SmartIdError;)V
    :try_end_d
    .catch Ljava/lang/Throwable; {:try_start_d .. :try_end_d} :catch_2
    .catchall {:try_start_d .. :try_end_d} :catchall_0

    .line 32
    iput-boolean v3, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->k:Z

    return-void

    .line 33
    :cond_3
    :try_start_e
    invoke-direct {p0, v4, v5}, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->a(Lee/cyber/smartid/dto/upgrade/InteractiveUpgradeState;Lee/cyber/smartid/dto/SmartIdError;)V
    :try_end_e
    .catch Ljava/lang/Throwable; {:try_start_e .. :try_end_e} :catch_2
    .catchall {:try_start_e .. :try_end_e} :catchall_0

    .line 34
    :goto_0
    iput-boolean v3, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->k:Z

    goto :goto_2

    :catch_2
    move-exception v1

    move-object v2, v1

    move-object v1, v4

    goto :goto_1

    :catchall_0
    move-exception v0

    goto :goto_3

    :catch_3
    move-exception v2

    .line 35
    :goto_1
    :try_start_f
    iget-object v4, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->n:Lee/cyber/smartid/util/Log;

    invoke-virtual {v4, v0, v2}, Lee/cyber/smartid/util/Log;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    if-nez v1, :cond_4

    .line 36
    invoke-direct {p0}, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->n()Lee/cyber/smartid/dto/upgrade/InteractiveUpgradeState;

    move-result-object v1

    .line 37
    :cond_4
    iget-object v0, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->e:Lee/cyber/smartid/inter/ServiceAccess;

    invoke-interface {v0}, Lee/cyber/smartid/inter/ServiceAccess;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iget-object v4, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->b:Lee/cyber/smartid/tse/inter/WallClock;

    invoke-static {v0, v4, v2}, Lee/cyber/smartid/dto/SmartIdError;->from(Landroid/content/Context;Lee/cyber/smartid/tse/inter/WallClock;Ljava/lang/Throwable;)Lee/cyber/smartid/dto/SmartIdError;

    move-result-object v0

    invoke-direct {p0, v1, v0}, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->a(Lee/cyber/smartid/dto/upgrade/InteractiveUpgradeState;Lee/cyber/smartid/dto/SmartIdError;)V
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_0

    .line 38
    iput-boolean v3, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->k:Z

    :goto_2
    return-void

    :goto_3
    iput-boolean v3, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->k:Z

    throw v0
.end method

.method private b(Lee/cyber/smartid/tse/dto/BaseError;)Z
    .locals 6

    const/4 v0, 0x1

    if-nez p1, :cond_0

    goto :goto_1

    .line 34
    :cond_0
    invoke-direct {p0, p1}, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->a(Lee/cyber/smartid/tse/dto/BaseError;)Z

    move-result v1

    if-eqz v1, :cond_1

    goto :goto_0

    .line 35
    :cond_1
    invoke-virtual {p1}, Lee/cyber/smartid/tse/dto/BaseError;->getCode()J

    move-result-wide v1

    const-wide/16 v3, 0x44c

    cmp-long v5, v1, v3

    if-nez v5, :cond_2

    goto :goto_0

    .line 36
    :cond_2
    invoke-direct {p0, p1}, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->c(Lee/cyber/smartid/tse/dto/BaseError;)Z

    move-result v1

    if-eqz v1, :cond_3

    goto :goto_0

    .line 37
    :cond_3
    invoke-virtual {p1}, Lee/cyber/smartid/tse/dto/BaseError;->getCode()J

    move-result-wide v1

    const-wide/16 v3, -0x792b

    cmp-long p1, v1, v3

    if-nez p1, :cond_4

    :goto_0
    const/4 v0, 0x0

    :cond_4
    :goto_1
    return v0
.end method

.method static synthetic c(Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;)Lee/cyber/smartid/tse/inter/WallClock;
    .locals 0

    .line 1
    iget-object p0, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->b:Lee/cyber/smartid/tse/inter/WallClock;

    return-object p0
.end method

.method private c(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    const/4 v0, 0x3

    .line 3
    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    iget-object p1, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->e:Lee/cyber/smartid/inter/ServiceAccess;

    invoke-interface {p1}, Lee/cyber/smartid/tse/inter/ExternalResourceAccess;->getDeviceFingerPrint()Ljava/lang/String;

    move-result-object p1

    const/4 v1, 0x1

    aput-object p1, v0, v1

    const/4 p1, 0x2

    aput-object p2, v0, p1

    const-string p1, "ee.cyber.smartid.APP_KEY_STATE_META_%1$s_%2$s_%3$s"

    invoke-static {p1, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private c()Ljava/lang/reflect/Method;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/NoSuchMethodException;
        }
    .end annotation

    .line 2
    iget-object v0, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->i:Lee/cyber/smartid/cryptolib/inter/CryptoOp;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const/16 v1, 0x8

    new-array v1, v1, [Ljava/lang/Class;

    const-class v2, Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const-class v2, Ljava/lang/String;

    const/4 v3, 0x1

    aput-object v2, v1, v3

    const-class v2, Ljava/lang/String;

    const/4 v3, 0x2

    aput-object v2, v1, v3

    const-class v2, Ljava/lang/String;

    const/4 v3, 0x3

    aput-object v2, v1, v3

    const-class v2, Ljava/lang/String;

    const/4 v3, 0x4

    aput-object v2, v1, v3

    const-class v2, Ljava/lang/String;

    const/4 v3, 0x5

    aput-object v2, v1, v3

    const-class v2, Ljava/lang/String;

    const/4 v3, 0x6

    aput-object v2, v1, v3

    const-class v2, [Lee/cyber/smartid/cryptolib/dto/ValuePair;

    const/4 v3, 0x7

    aput-object v2, v1, v3

    const-string v2, "verifyKTKSignedJWSAndCheckForRequiredContentValues"

    invoke-virtual {v0, v2, v1}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    return-object v0
.end method

.method private c(Lee/cyber/smartid/dto/upgrade/InteractiveUpgradeState;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lee/cyber/smartid/cryptolib/dto/StorageException;
        }
    .end annotation

    .line 15
    iget-object v0, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->l:Ljava/lang/Object;

    monitor-enter v0

    .line 16
    :try_start_0
    iget-object v1, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->f:Lee/cyber/smartid/cryptolib/inter/StorageOp;

    iget-object v2, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->e:Lee/cyber/smartid/inter/ServiceAccess;

    invoke-interface {v2}, Lee/cyber/smartid/inter/ServiceAccess;->getInteractiveUpgradeStateStorageId()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2, p1}, Lee/cyber/smartid/cryptolib/inter/StorageOp;->storeData(Ljava/lang/String;Ljava/lang/Object;)V

    .line 17
    monitor-exit v0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method private c(Lee/cyber/smartid/tse/dto/BaseError;)Z
    .locals 5

    if-eqz p1, :cond_1

    .line 3
    invoke-virtual {p1}, Lee/cyber/smartid/tse/dto/BaseError;->getKeyStatus()Lee/cyber/smartid/tse/dto/jsonrpc/AccountKeyStatus;

    move-result-object v0

    if-nez v0, :cond_0

    goto :goto_0

    .line 4
    :cond_0
    invoke-virtual {p1}, Lee/cyber/smartid/tse/dto/BaseError;->getKeyStatus()Lee/cyber/smartid/tse/dto/jsonrpc/AccountKeyStatus;

    move-result-object v0

    invoke-virtual {p1}, Lee/cyber/smartid/tse/dto/BaseError;->getCreatedAtTimestamp()J

    move-result-wide v1

    iget-object p1, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->b:Lee/cyber/smartid/tse/inter/WallClock;

    invoke-interface {p1}, Lee/cyber/smartid/tse/inter/WallClock;->currentTimeMillis()J

    move-result-wide v3

    invoke-static {v0, v1, v2, v3, v4}, Lee/cyber/smartid/tse/util/Util;->isKeyLocked(Lee/cyber/smartid/tse/dto/jsonrpc/AccountKeyStatus;JJ)Z

    move-result p1

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x0

    return p1
.end method

.method static synthetic d(Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;)Lee/cyber/smartid/inter/ServiceAccess;
    .locals 0

    .line 1
    iget-object p0, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->e:Lee/cyber/smartid/inter/ServiceAccess;

    return-object p0
.end method

.method private d(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    const/4 v0, 0x3

    .line 3
    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    iget-object p1, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->e:Lee/cyber/smartid/inter/ServiceAccess;

    invoke-interface {p1}, Lee/cyber/smartid/tse/inter/ExternalResourceAccess;->getDeviceFingerPrint()Ljava/lang/String;

    move-result-object p1

    const/4 v1, 0x1

    aput-object p1, v0, v1

    const/4 p1, 0x2

    aput-object p2, v0, p1

    const-string p1, "ee.cyber.smartid.APP_FRESHNESS_TOKEN_%1$s_%2$s_%3$s"

    invoke-static {p1, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private d()Z
    .locals 1

    .line 1
    :try_start_0
    invoke-direct {p0}, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->c()Ljava/lang/reflect/Method;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v0, 0x1

    return v0

    :catch_0
    const/4 v0, 0x0

    return v0
.end method

.method static synthetic e(Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;)Lee/cyber/smartid/dto/upgrade/InteractiveUpgradeState;
    .locals 0

    .line 1
    invoke-direct {p0}, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->n()Lee/cyber/smartid/dto/upgrade/InteractiveUpgradeState;

    move-result-object p0

    return-object p0
.end method

.method private e(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    const/4 v0, 0x1

    .line 39
    new-array v0, v0, [Ljava/lang/Object;

    invoke-direct {p0, p1, p2}, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    const/4 p2, 0x0

    aput-object p1, v0, p2

    const-string p1, "ee.cyber.smartid.APP_KEY_STATE_FOR_KEY_ID_%1$s"

    invoke-static {p1, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private e()Ljava/lang/reflect/Method;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/NoSuchMethodException;
        }
    .end annotation

    .line 2
    iget-object v0, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->i:Lee/cyber/smartid/cryptolib/inter/CryptoOp;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const/4 v1, 0x7

    new-array v1, v1, [Ljava/lang/Class;

    const-class v2, Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const-class v2, Ljava/lang/String;

    const/4 v3, 0x1

    aput-object v2, v1, v3

    const-class v2, Ljava/lang/String;

    const/4 v3, 0x2

    aput-object v2, v1, v3

    const-class v2, Ljava/lang/String;

    const/4 v3, 0x3

    aput-object v2, v1, v3

    const-class v2, Ljava/lang/String;

    const/4 v3, 0x4

    aput-object v2, v1, v3

    const-class v2, Ljava/lang/String;

    const/4 v3, 0x5

    aput-object v2, v1, v3

    const-class v2, [Lee/cyber/smartid/cryptolib/dto/ValuePair;

    const/4 v3, 0x6

    aput-object v2, v1, v3

    const-string v2, "verifyKTKSignedJWSAndCheckForRequiredContentValues"

    invoke-virtual {v0, v2, v1}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    return-object v0
.end method

.method private f(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    const/4 v0, 0x1

    .line 5
    new-array v0, v0, [Ljava/lang/Object;

    invoke-direct {p0, p1, p2}, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->e(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    const/4 p2, 0x0

    aput-object p1, v0, p2

    const-string p1, "ee.cyber.smartid.APP_KEY_STATE_META_FOR_KEY_STATE_ID_%1$s"

    invoke-static {p1, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method static synthetic f(Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lee/cyber/smartid/cryptolib/dto/StorageException;
        }
    .end annotation

    .line 1
    invoke-direct {p0}, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->m()V

    return-void
.end method

.method private f()Z
    .locals 1

    .line 1
    :try_start_0
    invoke-direct {p0}, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->e()Ljava/lang/reflect/Method;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v0, 0x1

    return v0

    :catch_0
    const/4 v0, 0x0

    return v0
.end method

.method static synthetic g(Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;)Lee/cyber/smartid/util/Log;
    .locals 0

    .line 1
    iget-object p0, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->n:Lee/cyber/smartid/util/Log;

    return-object p0
.end method

.method private g(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    const/4 v0, 0x1

    .line 225
    new-array v0, v0, [Ljava/lang/Object;

    invoke-direct {p0, p1, p2}, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->e(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    const/4 p2, 0x0

    aput-object p1, v0, p2

    const-string p1, "ee.cyber.smartid.APP_FRESHNESS_TOKEN_KEY_STATE_ID_%1$s"

    invoke-static {p1, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private g()V
    .locals 7

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->c:Lee/cyber/smartid/inter/ListenerAccess;

    const-class v1, Lee/cyber/smartid/inter/InitServiceListener;

    const/4 v2, 0x0

    invoke-interface {v0, v2, v1}, Lee/cyber/smartid/inter/ListenerAccess;->getListenersForType(ZLjava/lang/Class;)[Landroid/support/v4/e/j;

    move-result-object v0

    .line 2
    array-length v1, v0

    if-nez v1, :cond_0

    return-void

    .line 3
    :cond_0
    array-length v1, v0

    :goto_0
    if-ge v2, v1, :cond_3

    aget-object v3, v0, v2

    if-eqz v3, :cond_1

    .line 4
    iget-object v4, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->m:Ljava/util/HashSet;

    iget-object v5, v3, Landroid/support/v4/e/j;->a:Ljava/lang/Object;

    invoke-virtual {v4, v5}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 5
    iget-object v4, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->n:Lee/cyber/smartid/util/Log;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "notifyUpgradeStart - notifying "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v6, v3, Landroid/support/v4/e/j;->a:Ljava/lang/Object;

    check-cast v6, Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    .line 6
    iget-object v4, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->m:Ljava/util/HashSet;

    iget-object v5, v3, Landroid/support/v4/e/j;->a:Ljava/lang/Object;

    invoke-virtual {v4, v5}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 7
    :try_start_0
    iget-object v4, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->e:Lee/cyber/smartid/inter/ServiceAccess;

    new-instance v5, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper$9;

    invoke-direct {v5, p0, v3}, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper$9;-><init>(Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;Landroid/support/v4/e/j;)V

    invoke-interface {v4, v5}, Lee/cyber/smartid/inter/ServiceAccess;->notifyUI(Ljava/lang/Runnable;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v3

    .line 8
    iget-object v4, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->n:Lee/cyber/smartid/util/Log;

    const-string v5, "notifyUpgradeStart"

    invoke-virtual {v4, v5, v3}, Lee/cyber/smartid/util/Log;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    :cond_1
    if-eqz v3, :cond_2

    .line 9
    iget-object v4, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->n:Lee/cyber/smartid/util/Log;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "notifyUpgradeStart - skipping "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, v3, Landroid/support/v4/e/j;->a:Ljava/lang/Object;

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, ", already sent"

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    :cond_2
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_3
    return-void
.end method

.method static synthetic h(Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;)Landroid/os/PowerManager$WakeLock;
    .locals 0

    .line 1
    invoke-direct {p0}, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->o()Landroid/os/PowerManager$WakeLock;

    move-result-object p0

    return-object p0
.end method

.method private h()V
    .locals 6

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->c:Lee/cyber/smartid/inter/ListenerAccess;

    const-class v1, Lee/cyber/smartid/inter/InitServiceListener;

    const/4 v2, 0x1

    invoke-interface {v0, v2, v1}, Lee/cyber/smartid/inter/ListenerAccess;->getListenersForType(ZLjava/lang/Class;)[Landroid/support/v4/e/j;

    move-result-object v0

    .line 2
    array-length v1, v0

    if-nez v1, :cond_0

    return-void

    .line 3
    :cond_0
    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_1

    aget-object v3, v0, v2

    .line 4
    :try_start_0
    iget-object v4, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->e:Lee/cyber/smartid/inter/ServiceAccess;

    new-instance v5, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper$11;

    invoke-direct {v5, p0, v3}, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper$11;-><init>(Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;Landroid/support/v4/e/j;)V

    invoke-interface {v4, v5}, Lee/cyber/smartid/inter/ServiceAccess;->notifyUI(Ljava/lang/Runnable;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v3

    .line 5
    iget-object v4, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->n:Lee/cyber/smartid/util/Log;

    const-string v5, "notifyUpgradeStart"

    invoke-virtual {v4, v5, v3}, Lee/cyber/smartid/util/Log;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method private i()Ljava/util/ArrayList;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Lee/cyber/smartid/dto/AccountWrapper;",
            ">;"
        }
    .end annotation

    .line 2
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 3
    invoke-direct {p0}, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->j()Ljava/util/ArrayList;

    move-result-object v1

    .line 4
    iget-object v2, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->n:Lee/cyber/smartid/util/Log;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getMDv2UpgradableAccounts - found "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v5, " pre-MDv2 accounts"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    .line 5
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_1

    .line 6
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lee/cyber/smartid/dto/AccountWrapper;

    .line 7
    invoke-direct {p0, v2}, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->a(Lee/cyber/smartid/dto/AccountWrapper;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 8
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 9
    :cond_1
    iget-object v1, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->n:Lee/cyber/smartid/util/Log;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v3, " upgradable pre-MDv2 accounts"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    return-object v0
.end method

.method static synthetic i(Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->b()V

    return-void
.end method

.method private j()Ljava/util/ArrayList;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Lee/cyber/smartid/dto/AccountWrapper;",
            ">;"
        }
    .end annotation

    .line 2
    iget-object v0, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->f:Lee/cyber/smartid/cryptolib/inter/StorageOp;

    invoke-direct {p0}, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->k()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper$15;

    invoke-direct {v2, p0}, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper$15;-><init>(Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;)V

    .line 3
    invoke-virtual {v2}, Lcom/google/gson/b/a;->getType()Ljava/lang/reflect/Type;

    move-result-object v2

    .line 4
    invoke-interface {v0, v1, v2}, Lee/cyber/smartid/cryptolib/inter/StorageOp;->retrieveData(Ljava/lang/String;Ljava/lang/reflect/Type;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 5
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    :cond_0
    return-object v0
.end method

.method private k()Ljava/lang/String;
    .locals 3

    const/4 v0, 0x1

    .line 2
    new-array v0, v0, [Ljava/lang/Object;

    iget-object v1, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->e:Lee/cyber/smartid/inter/ServiceAccess;

    invoke-interface {v1}, Lee/cyber/smartid/tse/inter/ExternalResourceAccess;->getDeviceFingerPrint()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    const-string v1, "ee.cyber.smartid.APP_ACCOUNTS_DATA_%1$s"

    invoke-static {v1, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private l()Ljava/lang/String;
    .locals 3

    const/4 v0, 0x1

    .line 38
    new-array v0, v0, [Ljava/lang/Object;

    iget-object v1, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->e:Lee/cyber/smartid/inter/ServiceAccess;

    invoke-interface {v1}, Lee/cyber/smartid/tse/inter/ExternalResourceAccess;->getDeviceFingerPrint()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    const-string v1, "ee.cyber.smartid.APP_ACCOUNT_STATES_ID_%1$s"

    invoke-static {v1, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private m()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lee/cyber/smartid/cryptolib/dto/StorageException;
        }
    .end annotation

    .line 226
    invoke-direct {p0}, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->j()Ljava/util/ArrayList;

    move-result-object v0

    .line 227
    iget-object v1, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->f:Lee/cyber/smartid/cryptolib/inter/StorageOp;

    invoke-direct {p0}, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->l()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper$16;

    invoke-direct {v3, p0}, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper$16;-><init>(Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;)V

    .line 228
    invoke-virtual {v3}, Lcom/google/gson/b/a;->getType()Ljava/lang/reflect/Type;

    move-result-object v3

    .line 229
    invoke-interface {v1, v2, v3}, Lee/cyber/smartid/cryptolib/inter/StorageOp;->retrieveData(Ljava/lang/String;Ljava/lang/reflect/Type;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/ArrayList;

    if-eqz v1, :cond_0

    .line 230
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_0

    .line 231
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    .line 232
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lee/cyber/smartid/dto/upgrade/v5/MDv2AccountState;

    .line 233
    invoke-virtual {v3}, Lee/cyber/smartid/dto/upgrade/v5/MDv2AccountState;->getAccountUUID()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    .line 234
    :cond_1
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lee/cyber/smartid/dto/AccountWrapper;

    .line 235
    invoke-virtual {v1}, Lee/cyber/smartid/dto/AccountWrapper;->getAccountUUID()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1, v2}, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->a(Ljava/lang/String;Ljava/util/HashSet;)V

    goto :goto_1

    .line 236
    :cond_2
    iget-object v0, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->f:Lee/cyber/smartid/cryptolib/inter/StorageOp;

    invoke-direct {p0}, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->k()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lee/cyber/smartid/cryptolib/inter/StorageOp;->removeData(Ljava/lang/String;)V

    return-void
.end method

.method private n()Lee/cyber/smartid/dto/upgrade/InteractiveUpgradeState;
    .locals 5

    .line 6
    iget-object v0, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->l:Ljava/lang/Object;

    monitor-enter v0

    .line 7
    :try_start_0
    iget-object v1, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->f:Lee/cyber/smartid/cryptolib/inter/StorageOp;

    iget-object v2, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->e:Lee/cyber/smartid/inter/ServiceAccess;

    invoke-interface {v2}, Lee/cyber/smartid/inter/ServiceAccess;->getInteractiveUpgradeStateStorageId()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper$17;

    invoke-direct {v3, p0}, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper$17;-><init>(Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;)V

    .line 8
    invoke-virtual {v3}, Lcom/google/gson/b/a;->getType()Ljava/lang/reflect/Type;

    move-result-object v3

    .line 9
    invoke-interface {v1, v2, v3}, Lee/cyber/smartid/cryptolib/inter/StorageOp;->retrieveData(Ljava/lang/String;Ljava/lang/reflect/Type;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lee/cyber/smartid/dto/upgrade/InteractiveUpgradeState;

    if-nez v1, :cond_0

    .line 10
    invoke-static {}, Lee/cyber/smartid/dto/upgrade/InteractiveUpgradeState;->forAllNone()Lee/cyber/smartid/dto/upgrade/InteractiveUpgradeState;

    move-result-object v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 11
    :try_start_1
    invoke-direct {p0, v1}, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->c(Lee/cyber/smartid/dto/upgrade/InteractiveUpgradeState;)V
    :try_end_1
    .catch Lee/cyber/smartid/cryptolib/dto/StorageException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v2

    .line 12
    :try_start_2
    iget-object v3, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->n:Lee/cyber/smartid/util/Log;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    const-string v4, "getInteractiveUpgradeState - failed to initialize the state"

    :try_start_3
    invoke-virtual {v3, v4, v2}, Lee/cyber/smartid/util/Log;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 13
    :cond_0
    :goto_0
    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception v1

    .line 14
    monitor-exit v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v1
.end method

.method private o()Landroid/os/PowerManager$WakeLock;
    .locals 3

    .line 2
    iget-object v0, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->a:Landroid/os/PowerManager$WakeLock;

    if-nez v0, :cond_1

    .line 3
    iget-object v0, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->e:Lee/cyber/smartid/inter/ServiceAccess;

    invoke-interface {v0}, Lee/cyber/smartid/inter/ServiceAccess;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "power"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    if-eqz v0, :cond_0

    const/4 v1, 0x1

    const-string v2, "ee.cyber.smartid:WAKELOCK_KEY_FOR_SMARTIDSERVICE_INTERACTIVE_UPGRADE"

    .line 4
    invoke-virtual {v0, v1, v2}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    iput-object v0, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->a:Landroid/os/PowerManager$WakeLock;

    .line 5
    :cond_1
    iget-object v0, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->a:Landroid/os/PowerManager$WakeLock;

    return-object v0
.end method


# virtual methods
.method public cancelInteractiveUpgrade(Ljava/lang/String;Lee/cyber/smartid/inter/CancelInteractiveUpgradeListener;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->c:Lee/cyber/smartid/inter/ListenerAccess;

    invoke-interface {v0, p1, p2}, Lee/cyber/smartid/inter/ListenerAccess;->setListener(Ljava/lang/String;Lee/cyber/smartid/inter/ServiceListener;)V

    .line 2
    new-instance p2, Ljava/lang/Thread;

    new-instance v0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper$1;

    invoke-direct {v0, p0, p1}, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper$1;-><init>(Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;Ljava/lang/String;)V

    invoke-direct {p2, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 3
    invoke-virtual {p2}, Ljava/lang/Thread;->start()V

    return-void
.end method

.method public detectInteractiveUpgradeState()Lee/cyber/smartid/dto/upgrade/InteractiveUpgradeState;
    .locals 7

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->e:Lee/cyber/smartid/inter/ServiceAccess;

    invoke-interface {v0}, Lee/cyber/smartid/inter/ServiceAccess;->getCurrentStorageVersion()I

    move-result v0

    .line 2
    invoke-direct {p0}, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->n()Lee/cyber/smartid/dto/upgrade/InteractiveUpgradeState;

    move-result-object v1

    .line 3
    iget-object v2, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->e:Lee/cyber/smartid/inter/ServiceAccess;

    invoke-interface {v2}, Lee/cyber/smartid/inter/ServiceAccess;->getMDv2StorageVersion()I

    move-result v2

    const-string v3, "detectInteractiveUpgradeState"

    const/4 v4, 0x3

    if-le v0, v2, :cond_0

    .line 4
    iget-object v2, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->n:Lee/cyber/smartid/util/Log;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "detectInteractiveUpgradeState - no need, we are already post-first-MDv2-version (we are at "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v0, ", MDv2 happened at "

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->e:Lee/cyber/smartid/inter/ServiceAccess;

    invoke-interface {v0}, Lee/cyber/smartid/inter/ServiceAccess;->getMDv2StorageVersion()I

    move-result v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v0, ")"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    .line 5
    :try_start_0
    invoke-virtual {v1, v4}, Lee/cyber/smartid/dto/upgrade/InteractiveUpgradeState;->setStateUpgradeToMDv2(I)V

    .line 6
    invoke-direct {p0, v1}, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->c(Lee/cyber/smartid/dto/upgrade/InteractiveUpgradeState;)V
    :try_end_0
    .catch Lee/cyber/smartid/cryptolib/dto/StorageException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 7
    iget-object v2, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->n:Lee/cyber/smartid/util/Log;

    invoke-virtual {v2, v3, v0}, Lee/cyber/smartid/util/Log;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    :goto_0
    return-object v1

    .line 8
    :cond_0
    invoke-virtual {v1}, Lee/cyber/smartid/dto/upgrade/InteractiveUpgradeState;->getStateUpgradeToMDv2()I

    move-result v0

    if-ne v0, v4, :cond_1

    .line 9
    iget-object v0, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->n:Lee/cyber/smartid/util/Log;

    const-string v2, "detectInteractiveUpgradeState - no need, we have already completed MDv2 upgrade."

    invoke-virtual {v0, v2}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    return-object v1

    .line 10
    :cond_1
    invoke-virtual {v1}, Lee/cyber/smartid/dto/upgrade/InteractiveUpgradeState;->getStateUpgradeToMDv2()I

    move-result v0

    const/4 v2, 0x1

    if-ne v0, v2, :cond_2

    .line 11
    iget-object v0, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->n:Lee/cyber/smartid/util/Log;

    const-string v2, "detectInteractiveUpgradeState - MDv2 upgrade in in progress"

    invoke-virtual {v0, v2}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    return-object v1

    .line 12
    :cond_2
    invoke-virtual {v1}, Lee/cyber/smartid/dto/upgrade/InteractiveUpgradeState;->getStateUpgradeToMDv2()I

    move-result v0

    const/4 v5, 0x4

    if-ne v0, v5, :cond_3

    .line 13
    iget-object v0, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->n:Lee/cyber/smartid/util/Log;

    const-string v2, "detectInteractiveUpgradeState - MDv2 upgrade has failed once, we still need it"

    invoke-virtual {v0, v2}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    return-object v1

    .line 14
    :cond_3
    iget-object v0, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->n:Lee/cyber/smartid/util/Log;

    const-string v5, "We have no info on the MDv2 upgrade, upgrade is possibly needed, we have to check the accounts"

    invoke-virtual {v0, v5}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    .line 15
    invoke-direct {p0}, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->i()Ljava/util/ArrayList;

    move-result-object v0

    .line 16
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v0, v2, :cond_4

    .line 17
    :try_start_1
    invoke-virtual {v1, v4}, Lee/cyber/smartid/dto/upgrade/InteractiveUpgradeState;->setStateUpgradeToMDv2(I)V

    .line 18
    invoke-direct {p0, v1}, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->c(Lee/cyber/smartid/dto/upgrade/InteractiveUpgradeState;)V

    .line 19
    invoke-direct {p0}, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->m()V
    :try_end_1
    .catch Lee/cyber/smartid/cryptolib/dto/StorageException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    :catch_1
    move-exception v0

    .line 20
    iget-object v4, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->n:Lee/cyber/smartid/util/Log;

    invoke-virtual {v4, v3, v0}, Lee/cyber/smartid/util/Log;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 21
    invoke-virtual {v1, v2}, Lee/cyber/smartid/dto/upgrade/InteractiveUpgradeState;->setStateUpgradeToMDv2(I)V

    .line 22
    :goto_1
    iget-object v0, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->n:Lee/cyber/smartid/util/Log;

    const-string v2, "We have no upgradable pre-MDv2 accounts, we are done here"

    invoke-virtual {v0, v2}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    return-object v1

    .line 23
    :cond_4
    :try_start_2
    invoke-virtual {v1, v2}, Lee/cyber/smartid/dto/upgrade/InteractiveUpgradeState;->setStateUpgradeToMDv2(I)V

    .line 24
    invoke-direct {p0, v1}, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->c(Lee/cyber/smartid/dto/upgrade/InteractiveUpgradeState;)V
    :try_end_2
    .catch Lee/cyber/smartid/cryptolib/dto/StorageException; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_2

    :catch_2
    move-exception v0

    .line 25
    iget-object v2, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->n:Lee/cyber/smartid/util/Log;

    invoke-virtual {v2, v3, v0}, Lee/cyber/smartid/util/Log;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    :goto_2
    return-object v1
.end method

.method public detectInteractiveUpgradeStateForCleanInstall(I)Lee/cyber/smartid/dto/upgrade/InteractiveUpgradeState;
    .locals 3

    .line 1
    invoke-static {}, Lee/cyber/smartid/dto/upgrade/InteractiveUpgradeState;->forAllNone()Lee/cyber/smartid/dto/upgrade/InteractiveUpgradeState;

    move-result-object v0

    .line 2
    iget-object v1, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->e:Lee/cyber/smartid/inter/ServiceAccess;

    invoke-interface {v1}, Lee/cyber/smartid/inter/ServiceAccess;->getMDv2StorageVersion()I

    move-result v1

    if-ne p1, v1, :cond_0

    .line 3
    iget-object p1, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->n:Lee/cyber/smartid/util/Log;

    const-string v1, "detectInteractiveUpgradeStateForCleanInstall - marking MDv2 interactive upgrade as done"

    invoke-virtual {p1, v1}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    const/4 p1, 0x3

    .line 4
    :try_start_0
    invoke-virtual {v0, p1}, Lee/cyber/smartid/dto/upgrade/InteractiveUpgradeState;->setStateUpgradeToMDv2(I)V

    .line 5
    invoke-direct {p0, v0}, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->c(Lee/cyber/smartid/dto/upgrade/InteractiveUpgradeState;)V
    :try_end_0
    .catch Lee/cyber/smartid/cryptolib/dto/StorageException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 6
    iget-object v1, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->n:Lee/cyber/smartid/util/Log;

    const-string v2, "detectInteractiveUpgradeStateForCleanInstall"

    invoke-virtual {v1, v2, p1}, Lee/cyber/smartid/util/Log;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_0
    :goto_0
    return-object v0
.end method

.method public isInteractiveUpgradeStatePresentAndPending()Z
    .locals 1

    .line 1
    invoke-direct {p0}, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->n()Lee/cyber/smartid/dto/upgrade/InteractiveUpgradeState;

    move-result-object v0

    .line 2
    invoke-virtual {v0}, Lee/cyber/smartid/dto/upgrade/InteractiveUpgradeState;->isUpgradePending()Z

    move-result v0

    return v0
.end method

.method public startInteractiveUpgrade(Lee/cyber/smartid/network/SmartIdAPI;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->j:Lee/cyber/smartid/network/SmartIdAPI;

    .line 2
    invoke-direct {p0}, Lee/cyber/smartid/manager/impl/InteractiveUpgradeHelper;->a()V

    return-void
.end method
