.class Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl$2;
.super Ljava/lang/Object;
.source "SafetyNetManagerImpl.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl;->a(Ljava/lang/String;Lee/cyber/smartid/dto/SafetyNetAttestation;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:Lee/cyber/smartid/dto/SafetyNetAttestation;

.field final synthetic c:Lee/cyber/smartid/dto/jsonrpc/resp/GetSafetyNetAttestationResp;

.field final synthetic d:Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl;


# direct methods
.method constructor <init>(Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl;Ljava/lang/String;Lee/cyber/smartid/dto/SafetyNetAttestation;Lee/cyber/smartid/dto/jsonrpc/resp/GetSafetyNetAttestationResp;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl$2;->d:Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl;

    iput-object p2, p0, Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl$2;->a:Ljava/lang/String;

    iput-object p3, p0, Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl$2;->b:Lee/cyber/smartid/dto/SafetyNetAttestation;

    iput-object p4, p0, Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl$2;->c:Lee/cyber/smartid/dto/jsonrpc/resp/GetSafetyNetAttestationResp;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl$2;->d:Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl;

    invoke-static {v0}, Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl;->g(Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl;)Lee/cyber/smartid/inter/ListenerAccess;

    move-result-object v0

    iget-object v1, p0, Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl$2;->a:Ljava/lang/String;

    const-class v2, Lee/cyber/smartid/inter/GetSafetyNetAttestationListener;

    const/4 v3, 0x1

    invoke-interface {v0, v1, v3, v2}, Lee/cyber/smartid/inter/ListenerAccess;->getListener(Ljava/lang/String;ZLjava/lang/Class;)Lee/cyber/smartid/inter/ServiceListener;

    move-result-object v0

    check-cast v0, Lee/cyber/smartid/inter/GetSafetyNetAttestationListener;

    if-eqz v0, :cond_0

    .line 2
    iget-object v1, p0, Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl$2;->b:Lee/cyber/smartid/dto/SafetyNetAttestation;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lee/cyber/smartid/dto/SafetyNetAttestation;->getPayloadSummary()Lee/cyber/smartid/dto/jsonrpc/resp/SafetyNetAttestationSummary;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl$2;->b:Lee/cyber/smartid/dto/SafetyNetAttestation;

    invoke-virtual {v1}, Lee/cyber/smartid/dto/SafetyNetAttestation;->getPayloadSummary()Lee/cyber/smartid/dto/jsonrpc/resp/SafetyNetAttestationSummary;

    move-result-object v1

    invoke-virtual {v1}, Lee/cyber/smartid/dto/jsonrpc/resp/SafetyNetAttestationSummary;->isAttestationDataPresent()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 3
    iget-object v1, p0, Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl$2;->a:Ljava/lang/String;

    iget-object v2, p0, Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl$2;->c:Lee/cyber/smartid/dto/jsonrpc/resp/GetSafetyNetAttestationResp;

    invoke-interface {v0, v1, v2}, Lee/cyber/smartid/inter/GetSafetyNetAttestationListener;->onGetSafetyNetAttestationSuccess(Ljava/lang/String;Lee/cyber/smartid/dto/jsonrpc/resp/GetSafetyNetAttestationResp;)V

    goto :goto_0

    :cond_0
    if-eqz v0, :cond_1

    .line 4
    iget-object v1, p0, Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl$2;->a:Ljava/lang/String;

    iget-object v2, p0, Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl$2;->d:Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl;

    invoke-static {v2}, Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl;->d(Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl;)Lee/cyber/smartid/tse/inter/WallClock;

    move-result-object v2

    iget-object v3, p0, Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl$2;->d:Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl;

    invoke-static {v3}, Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl;->b(Lee/cyber/smartid/manager/impl/SafetyNetManagerImpl;)Lee/cyber/smartid/inter/ServiceAccess;

    move-result-object v3

    invoke-interface {v3}, Lee/cyber/smartid/inter/ServiceAccess;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    sget v4, Lee/cyber/smartid/R$string;->text_safetynet_attestation_get_data_failed:I

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    const-wide/16 v4, 0x402

    invoke-static {v2, v4, v5, v3}, Lee/cyber/smartid/dto/SmartIdError;->from(Lee/cyber/smartid/tse/inter/WallClock;JLjava/lang/String;)Lee/cyber/smartid/dto/SmartIdError;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lee/cyber/smartid/inter/GetSafetyNetAttestationListener;->onGetSafetyNetAttestationFailed(Ljava/lang/String;Lee/cyber/smartid/dto/SmartIdError;)V

    :cond_1
    :goto_0
    return-void
.end method
