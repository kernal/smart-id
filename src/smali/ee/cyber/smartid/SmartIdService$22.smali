.class Lee/cyber/smartid/SmartIdService$22;
.super Ljava/lang/Object;
.source "SmartIdService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lee/cyber/smartid/SmartIdService;->b(Ljava/lang/String;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:Lee/cyber/smartid/dto/jsonrpc/resp/AddAccountResp;

.field final synthetic c:Lee/cyber/smartid/SmartIdService;


# direct methods
.method constructor <init>(Lee/cyber/smartid/SmartIdService;Ljava/lang/String;Lee/cyber/smartid/dto/jsonrpc/resp/AddAccountResp;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lee/cyber/smartid/SmartIdService$22;->c:Lee/cyber/smartid/SmartIdService;

    iput-object p2, p0, Lee/cyber/smartid/SmartIdService$22;->a:Ljava/lang/String;

    iput-object p3, p0, Lee/cyber/smartid/SmartIdService$22;->b:Lee/cyber/smartid/dto/jsonrpc/resp/AddAccountResp;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/SmartIdService$22;->c:Lee/cyber/smartid/SmartIdService;

    iget-object v1, p0, Lee/cyber/smartid/SmartIdService$22;->a:Ljava/lang/String;

    const-class v2, Lee/cyber/smartid/inter/AddAccountListener;

    const/4 v3, 0x1

    invoke-static {v0, v1, v3, v2}, Lee/cyber/smartid/SmartIdService;->a(Lee/cyber/smartid/SmartIdService;Ljava/lang/String;ZLjava/lang/Class;)Lee/cyber/smartid/inter/ServiceListener;

    move-result-object v0

    check-cast v0, Lee/cyber/smartid/inter/AddAccountListener;

    if-eqz v0, :cond_0

    .line 2
    iget-object v1, p0, Lee/cyber/smartid/SmartIdService$22;->a:Ljava/lang/String;

    iget-object v2, p0, Lee/cyber/smartid/SmartIdService$22;->b:Lee/cyber/smartid/dto/jsonrpc/resp/AddAccountResp;

    invoke-interface {v0, v1, v2}, Lee/cyber/smartid/inter/AddAccountListener;->onAddAccountSuccess(Ljava/lang/String;Lee/cyber/smartid/dto/jsonrpc/resp/AddAccountResp;)V

    :cond_0
    return-void
.end method
