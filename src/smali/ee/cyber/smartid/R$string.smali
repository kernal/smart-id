.class public final Lee/cyber/smartid/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lee/cyber/smartid/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final app_name:I = 0x7f0e0028

.field public static final common_google_play_services_enable_button:I = 0x7f0e002e

.field public static final common_google_play_services_enable_text:I = 0x7f0e002f

.field public static final common_google_play_services_enable_title:I = 0x7f0e0030

.field public static final common_google_play_services_install_button:I = 0x7f0e0031

.field public static final common_google_play_services_install_text:I = 0x7f0e0032

.field public static final common_google_play_services_install_title:I = 0x7f0e0033

.field public static final common_google_play_services_notification_channel_name:I = 0x7f0e0034

.field public static final common_google_play_services_notification_ticker:I = 0x7f0e0035

.field public static final common_google_play_services_unknown_issue:I = 0x7f0e0036

.field public static final common_google_play_services_unsupported_text:I = 0x7f0e0037

.field public static final common_google_play_services_update_button:I = 0x7f0e0038

.field public static final common_google_play_services_update_text:I = 0x7f0e0039

.field public static final common_google_play_services_update_title:I = 0x7f0e003a

.field public static final common_google_play_services_updating_text:I = 0x7f0e003b

.field public static final common_google_play_services_wear_update_text:I = 0x7f0e003c

.field public static final common_open_on_phone:I = 0x7f0e003d

.field public static final common_signin_button_text:I = 0x7f0e003e

.field public static final common_signin_button_text_long:I = 0x7f0e003f

.field public static final err_account_not_found:I = 0x7f0e0047

.field public static final err_additional_verification_code_generation_failed:I = 0x7f0e0048

.field public static final err_certificate_exception:I = 0x7f0e0049

.field public static final err_client_too_old:I = 0x7f0e004a

.field public static final err_default_key_length_not_a_positive_number:I = 0x7f0e004b

.field public static final err_dh_key_id_missing:I = 0x7f0e004c

.field public static final err_encryption_failed_invalid_parameters:I = 0x7f0e004d

.field public static final err_failed_to_cryptographically_validate_servers_response:I = 0x7f0e004e

.field public static final err_failed_to_decrypt_server_response:I = 0x7f0e004f

.field public static final err_failed_to_encrypt_the_server_part_of_the_private_key:I = 0x7f0e0050

.field public static final err_failed_to_encrypt_the_signature:I = 0x7f0e0051

.field public static final err_failed_to_load_properties_from_x:I = 0x7f0e0052

.field public static final err_failed_to_read_from_storage:I = 0x7f0e0053

.field public static final err_failed_to_resolve_the_hostname_exception:I = 0x7f0e0054

.field public static final err_failed_to_verify_the_server_response_signature:I = 0x7f0e0055

.field public static final err_failed_to_write_to_storage:I = 0x7f0e0056

.field public static final err_forbidden_pin_pattern:I = 0x7f0e0057

.field public static final err_general_security_exception:I = 0x7f0e0058

.field public static final err_interactive_upgrade_is_in_progress:I = 0x7f0e0059

.field public static final err_interactive_upgrade_not_found:I = 0x7f0e005a

.field public static final err_invalid_automatic_backoff_value:I = 0x7f0e005b

.field public static final err_invalid_base_url_1:I = 0x7f0e005c

.field public static final err_invalid_base_url_2:I = 0x7f0e005d

.field public static final err_invalid_clone_detection_interval:I = 0x7f0e005e

.field public static final err_invalid_interactive_delay:I = 0x7f0e005f

.field public static final err_invalid_jose_jwe_key_value:I = 0x7f0e0060

.field public static final err_invalid_key_data:I = 0x7f0e0061

.field public static final err_invalid_or_missing_authorization:I = 0x7f0e0062

.field public static final err_invalid_pins:I = 0x7f0e0063

.field public static final err_invalid_pins_value:I = 0x7f0e0064

.field public static final err_invalid_prng_suppress_failure_boolean:I = 0x7f0e0065

.field public static final err_invalid_prng_test_loop_count:I = 0x7f0e0066

.field public static final err_invalid_signature_hash_type:I = 0x7f0e0067

.field public static final err_invalid_sz_ktk_key_data_value:I = 0x7f0e0068

.field public static final err_invalid_sz_ktk_keys_value:I = 0x7f0e0069

.field public static final err_invalid_transaction_state:I = 0x7f0e006a

.field public static final err_invalid_transaction_state_2:I = 0x7f0e006b

.field public static final err_invalid_transaction_state_previous_didnt_resolve:I = 0x7f0e006c

.field public static final err_invalid_type_of_listener_supplied:I = 0x7f0e006d

.field public static final err_key_unusable:I = 0x7f0e006e

.field public static final err_local_pending_state_not_found:I = 0x7f0e006f

.field public static final err_malformed_url_exception:I = 0x7f0e0070

.field public static final err_multi_code_verification_fake_code_count_not_a_valid_value:I = 0x7f0e0071

.field public static final err_multi_code_verification_fake_code_min_levenshtein_distance_not_a_valid_value:I = 0x7f0e0072

.field public static final err_network_connection:I = 0x7f0e0073

.field public static final err_no_account_registration_sz_id:I = 0x7f0e0074

.field public static final err_no_account_upgrade_sz_id:I = 0x7f0e0075

.field public static final err_no_accounts_found:I = 0x7f0e0076

.field public static final err_no_base_url:I = 0x7f0e0077

.field public static final err_no_default_key_length:I = 0x7f0e0078

.field public static final err_no_safety_net_api_url:I = 0x7f0e0079

.field public static final err_no_safety_net_automatic_request_mode_setting:I = 0x7f0e007a

.field public static final err_no_such_key_found:I = 0x7f0e007b

.field public static final err_no_such_keys_found_regenerate_keys:I = 0x7f0e007c

.field public static final err_no_such_keys_found_regenerate_keys_2:I = 0x7f0e007d

.field public static final err_no_such_keys_found_regenerate_keys_3:I = 0x7f0e007e

.field public static final err_no_such_ktk_found:I = 0x7f0e007f

.field public static final err_no_such_transaction:I = 0x7f0e0080

.field public static final err_prng_tests_failed:I = 0x7f0e0081

.field public static final err_protocol_exception:I = 0x7f0e0082

.field public static final err_registration_init_type_not_supported:I = 0x7f0e0083

.field public static final err_server_response_id_invalid:I = 0x7f0e0084

.field public static final err_server_response_invalid:I = 0x7f0e0085

.field public static final err_server_response_non_200_OK_X:I = 0x7f0e0086

.field public static final err_server_response_timeout:I = 0x7f0e0087

.field public static final err_signing_failure:I = 0x7f0e0088

.field public static final err_ssl_exception:I = 0x7f0e0089

.field public static final err_storage_version_downgrade_is_not_supported_use_a_clean_install:I = 0x7f0e008a

.field public static final err_system_under_maintenance:I = 0x7f0e008b

.field public static final err_text_failed_to_generate_the_retransmit_nonce:I = 0x7f0e008c

.field public static final err_token_generation_algorithm_not_supported:I = 0x7f0e008d

.field public static final err_token_generation_init_type_not_supported:I = 0x7f0e008e

.field public static final err_unable_to_acquire_freshness_token:I = 0x7f0e008f

.field public static final err_unable_to_decrypt_illegal_arguments_provided:I = 0x7f0e0090

.field public static final err_unable_to_decrypt_illegal_key_provided:I = 0x7f0e0091

.field public static final err_unknown:I = 0x7f0e0092

.field public static final err_unknown_key_type:I = 0x7f0e0093

.field public static final err_unknown_storage_upgrade_to_x:I = 0x7f0e0094

.field public static final err_unknown_storage_version_x:I = 0x7f0e0095

.field public static final err_unknown_transaction_type:I = 0x7f0e0096

.field public static final err_unsupported_response_encoding_x:I = 0x7f0e0097

.field public static final err_upgrade_to_mdv2_not_supported:I = 0x7f0e0098

.field public static final err_upgrade_to_x_not_supported:I = 0x7f0e0099

.field public static final err_verification_code_generation_failed:I = 0x7f0e009a

.field public static final err_wrong_pin:I = 0x7f0e009b

.field public static final err_wrong_transaction_type:I = 0x7f0e009c

.field public static final err_wrong_verification_code:I = 0x7f0e009d

.field public static final fcm_fallback_notification_channel_label:I = 0x7f0e00bf

.field public static final status_bar_notification_info_overflow:I = 0x7f0e0118

.field public static final text_na:I = 0x7f0e011a

.field public static final text_safetynet_attestation_get_data_failed:I = 0x7f0e011b

.field public static final text_safetynet_attestation_results_local_x_basic_y_cts_x:I = 0x7f0e011c

.field public static final text_safetynet_validation_succesful:I = 0x7f0e011d


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 192
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
