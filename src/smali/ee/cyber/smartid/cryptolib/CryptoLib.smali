.class public final Lee/cyber/smartid/cryptolib/CryptoLib;
.super Ljava/lang/Object;

# interfaces
.implements Lee/cyber/smartid/cryptolib/inter/CryptoOp;
.implements Lee/cyber/smartid/cryptolib/inter/EncodingOp;
.implements Lee/cyber/smartid/cryptolib/inter/KeyGenerationOp;
.implements Lee/cyber/smartid/cryptolib/inter/RandomGenerationOp;
.implements Lee/cyber/smartid/cryptolib/inter/SigningOp;
.implements Lee/cyber/smartid/cryptolib/inter/StorageOp;


# static fields
.field public static final CSPRNG_QUALITY_TEST_LONG_RUNS:Ljava/lang/String; = "FIPS 140-1 Long Runs test"

.field public static final CSPRNG_QUALITY_TEST_MONOBIT:Ljava/lang/String; = "FIPS 140-1 Monobit test"

.field public static final CSPRNG_QUALITY_TEST_POKER:Ljava/lang/String; = "FIPS 140-1 Poker test"

.field public static final CSPRNG_QUALITY_TEST_RUNS:Ljava/lang/String; = "FIPS 140-1 Runs test"

.field public static final DEFAULT_ENCODING:Ljava/nio/charset/Charset;

.field public static final JWE_ALGORITHM_NAME_RSA_OAEP:Ljava/lang/String; = "RSA-OAEP"

.field public static final JWE_AUDIENCE_VALUE_CLIENT:Ljava/lang/String; = "CLIENT"

.field public static final JWE_AUDIENCE_VALUE_SERVER:Ljava/lang/String; = "SERVER"

.field public static final JWE_ENCODING_NAME_A128CBC_HS256:Ljava/lang/String; = "A128CBC-HS256"

.field public static final JWE_I2OSP_X_LEN_256:I = 0x100

.field public static final JWE_I2OSP_X_LEN_384:I = 0x180

.field public static final JWE_PURPOSE_VALUE_CLIENT_2ND_PART:Ljava/lang/String; = "CLIENT2NDPART"

.field public static final JWE_PURPOSE_VALUE_CLIENT_SIGNATURE_SHARE:Ljava/lang/String; = "CLIENTSIGNATURESHARE"

.field public static final JWE_PURPOSE_VALUE_DH:Ljava/lang/String; = "DH"

.field public static final N_LEN_2048:I = 0x800

.field public static final N_LEN_3072:I = 0xc00


# instance fields
.field private a:Lee/cyber/smartid/cryptolib/inter/StorageOp;

.field private b:Lee/cyber/smartid/cryptolib/inter/SigningOp;

.field private c:Lee/cyber/smartid/cryptolib/inter/EncodingOp;

.field private d:Lee/cyber/smartid/cryptolib/inter/KeyGenerationOp;

.field private e:Lee/cyber/smartid/cryptolib/inter/RandomGenerationOp;

.field private f:Lee/cyber/smartid/cryptolib/inter/CryptoOp;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "UTF-8"

    .line 52
    invoke-static {v0}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v0

    sput-object v0, Lee/cyber/smartid/cryptolib/CryptoLib;->DEFAULT_ENCODING:Ljava/nio/charset/Charset;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 154
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static newInstance()Lee/cyber/smartid/cryptolib/CryptoLib;
    .locals 1

    .line 128
    new-instance v0, Lee/cyber/smartid/cryptolib/CryptoLib;

    invoke-direct {v0}, Lee/cyber/smartid/cryptolib/CryptoLib;-><init>()V

    return-object v0
.end method


# virtual methods
.method public calculateConcatKDFWithSHA256(Lee/cyber/smartid/cryptolib/dto/DiffieHellmanKeyPair;Ljava/math/BigInteger;)[B
    .locals 1

    .line 489
    iget-object v0, p0, Lee/cyber/smartid/cryptolib/CryptoLib;->d:Lee/cyber/smartid/cryptolib/inter/KeyGenerationOp;

    if-eqz v0, :cond_0

    .line 492
    invoke-interface {v0, p1, p2}, Lee/cyber/smartid/cryptolib/inter/KeyGenerationOp;->calculateConcatKDFWithSHA256(Lee/cyber/smartid/cryptolib/dto/DiffieHellmanKeyPair;Ljava/math/BigInteger;)[B

    move-result-object p1

    return-object p1

    .line 490
    :cond_0
    new-instance p1, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;

    const/16 p2, 0x65

    const-string v0, "keyGenerationOpImpl == null"

    invoke-direct {p1, p2, v0}, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;-><init>(ILjava/lang/String;)V

    throw p1
.end method

.method protected final clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .line 166
    new-instance v0, Ljava/lang/CloneNotSupportedException;

    invoke-direct {v0}, Ljava/lang/CloneNotSupportedException;-><init>()V

    throw v0
.end method

.method public decodeBytesFromBase64(Ljava/lang/String;)[B
    .locals 2

    .line 233
    iget-object v0, p0, Lee/cyber/smartid/cryptolib/CryptoLib;->c:Lee/cyber/smartid/cryptolib/inter/EncodingOp;

    if-eqz v0, :cond_0

    .line 236
    invoke-interface {v0, p1}, Lee/cyber/smartid/cryptolib/inter/EncodingOp;->decodeBytesFromBase64(Ljava/lang/String;)[B

    move-result-object p1

    return-object p1

    .line 234
    :cond_0
    new-instance p1, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;

    const/16 v0, 0x65

    const-string v1, "encodingOpImpl == null"

    invoke-direct {p1, v0, v1}, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;-><init>(ILjava/lang/String;)V

    throw p1
.end method

.method public decodeDecimalFromBase64(Ljava/lang/String;)Ljava/math/BigInteger;
    .locals 2

    .line 215
    iget-object v0, p0, Lee/cyber/smartid/cryptolib/CryptoLib;->c:Lee/cyber/smartid/cryptolib/inter/EncodingOp;

    if-eqz v0, :cond_0

    .line 218
    invoke-interface {v0, p1}, Lee/cyber/smartid/cryptolib/inter/EncodingOp;->decodeDecimalFromBase64(Ljava/lang/String;)Ljava/math/BigInteger;

    move-result-object p1

    return-object p1

    .line 216
    :cond_0
    new-instance p1, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;

    const/16 v0, 0x65

    const-string v1, "encodingOpImpl == null"

    invoke-direct {p1, v0, v1}, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;-><init>(ILjava/lang/String;)V

    throw p1
.end method

.method public decryptFromTEKEncryptedJWE(Ljava/lang/String;[BLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/a/a/n;
    .locals 6

    .line 383
    iget-object v0, p0, Lee/cyber/smartid/cryptolib/CryptoLib;->f:Lee/cyber/smartid/cryptolib/inter/CryptoOp;

    if-eqz v0, :cond_0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    .line 386
    invoke-interface/range {v0 .. v5}, Lee/cyber/smartid/cryptolib/inter/CryptoOp;->decryptFromTEKEncryptedJWE(Ljava/lang/String;[BLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/a/a/n;

    move-result-object p1

    return-object p1

    .line 384
    :cond_0
    new-instance p1, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;

    const/16 p2, 0x65

    const-string p3, "cryptoOpImpl == null"

    invoke-direct {p1, p2, p3}, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;-><init>(ILjava/lang/String;)V

    throw p1
.end method

.method public decryptKey(Lee/cyber/smartid/cryptolib/dto/ClientShare;Ljava/lang/String;)Ljava/math/BigInteger;
    .locals 1

    .line 366
    iget-object v0, p0, Lee/cyber/smartid/cryptolib/CryptoLib;->f:Lee/cyber/smartid/cryptolib/inter/CryptoOp;

    if-eqz v0, :cond_0

    .line 369
    invoke-interface {v0, p1, p2}, Lee/cyber/smartid/cryptolib/inter/CryptoOp;->decryptKey(Lee/cyber/smartid/cryptolib/dto/ClientShare;Ljava/lang/String;)Ljava/math/BigInteger;

    move-result-object p1

    return-object p1

    .line 367
    :cond_0
    new-instance p1, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;

    const/16 p2, 0x65

    const-string v0, "cryptoOpImpl == null"

    invoke-direct {p1, p2, v0}, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;-><init>(ILjava/lang/String;)V

    throw p1
.end method

.method public digestAndEncodeToBase64(Ljava/io/InputStream;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .line 278
    iget-object v0, p0, Lee/cyber/smartid/cryptolib/CryptoLib;->c:Lee/cyber/smartid/cryptolib/inter/EncodingOp;

    if-eqz v0, :cond_0

    .line 281
    invoke-interface {v0, p1, p2}, Lee/cyber/smartid/cryptolib/inter/EncodingOp;->digestAndEncodeToBase64(Ljava/io/InputStream;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 279
    :cond_0
    new-instance p1, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;

    const/16 p2, 0x65

    const-string v0, "encodingOpImpl == null"

    invoke-direct {p1, p2, v0}, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;-><init>(ILjava/lang/String;)V

    throw p1
.end method

.method public digestAndEncodeToBase64(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .line 287
    iget-object v0, p0, Lee/cyber/smartid/cryptolib/CryptoLib;->c:Lee/cyber/smartid/cryptolib/inter/EncodingOp;

    if-eqz v0, :cond_0

    .line 290
    invoke-interface {v0, p1, p2}, Lee/cyber/smartid/cryptolib/inter/EncodingOp;->digestAndEncodeToBase64(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 288
    :cond_0
    new-instance p1, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;

    const/16 p2, 0x65

    const-string v0, "encodingOpImpl == null"

    invoke-direct {p1, p2, v0}, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;-><init>(ILjava/lang/String;)V

    throw p1
.end method

.method public digestAndEncodeToBase64([BLjava/lang/String;)Ljava/lang/String;
    .locals 1

    .line 269
    iget-object v0, p0, Lee/cyber/smartid/cryptolib/CryptoLib;->c:Lee/cyber/smartid/cryptolib/inter/EncodingOp;

    if-eqz v0, :cond_0

    .line 272
    invoke-interface {v0, p1, p2}, Lee/cyber/smartid/cryptolib/inter/EncodingOp;->digestAndEncodeToBase64([BLjava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 270
    :cond_0
    new-instance p1, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;

    const/16 p2, 0x65

    const-string v0, "encodingOpImpl == null"

    invoke-direct {p1, p2, v0}, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;-><init>(ILjava/lang/String;)V

    throw p1
.end method

.method public encodeBytesToBase64([B)Ljava/lang/String;
    .locals 2

    .line 251
    iget-object v0, p0, Lee/cyber/smartid/cryptolib/CryptoLib;->c:Lee/cyber/smartid/cryptolib/inter/EncodingOp;

    if-eqz v0, :cond_0

    .line 254
    invoke-interface {v0, p1}, Lee/cyber/smartid/cryptolib/inter/EncodingOp;->encodeBytesToBase64([B)Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 252
    :cond_0
    new-instance p1, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;

    const/16 v0, 0x65

    const-string v1, "encodingOpImpl == null"

    invoke-direct {p1, v0, v1}, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;-><init>(ILjava/lang/String;)V

    throw p1
.end method

.method public encodeDecimalToBase64(Ljava/math/BigInteger;)Ljava/lang/String;
    .locals 2

    .line 205
    iget-object v0, p0, Lee/cyber/smartid/cryptolib/CryptoLib;->c:Lee/cyber/smartid/cryptolib/inter/EncodingOp;

    if-eqz v0, :cond_0

    .line 208
    invoke-interface {v0, p1}, Lee/cyber/smartid/cryptolib/inter/EncodingOp;->encodeDecimalToBase64(Ljava/math/BigInteger;)Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 206
    :cond_0
    new-instance p1, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;

    const/16 v0, 0x65

    const-string v1, "encodingOpImpl == null"

    invoke-direct {p1, v0, v1}, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;-><init>(ILjava/lang/String;)V

    throw p1
.end method

.method public encodeI2OSP(Ljava/math/BigInteger;I)[B
    .locals 1

    .line 260
    iget-object v0, p0, Lee/cyber/smartid/cryptolib/CryptoLib;->c:Lee/cyber/smartid/cryptolib/inter/EncodingOp;

    if-eqz v0, :cond_0

    .line 263
    invoke-interface {v0, p1, p2}, Lee/cyber/smartid/cryptolib/inter/EncodingOp;->encodeI2OSP(Ljava/math/BigInteger;I)[B

    move-result-object p1

    return-object p1

    .line 261
    :cond_0
    new-instance p1, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;

    const/16 p2, 0x65

    const-string v0, "encodingOpImpl == null"

    invoke-direct {p1, p2, v0}, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;-><init>(ILjava/lang/String;)V

    throw p1
.end method

.method public encodePositiveBigIntegerAsBytes(Ljava/math/BigInteger;)[B
    .locals 2

    .line 224
    iget-object v0, p0, Lee/cyber/smartid/cryptolib/CryptoLib;->c:Lee/cyber/smartid/cryptolib/inter/EncodingOp;

    if-eqz v0, :cond_0

    .line 227
    invoke-interface {v0, p1}, Lee/cyber/smartid/cryptolib/inter/EncodingOp;->encodePositiveBigIntegerAsBytes(Ljava/math/BigInteger;)[B

    move-result-object p1

    return-object p1

    .line 225
    :cond_0
    new-instance p1, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;

    const/16 v0, 0x65

    const-string v1, "encodingOpImpl == null"

    invoke-direct {p1, v0, v1}, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;-><init>(ILjava/lang/String;)V

    throw p1
.end method

.method public encryptKey(Ljava/math/BigInteger;Ljava/lang/String;I)Lee/cyber/smartid/cryptolib/dto/ClientShare;
    .locals 1

    .line 357
    iget-object v0, p0, Lee/cyber/smartid/cryptolib/CryptoLib;->f:Lee/cyber/smartid/cryptolib/inter/CryptoOp;

    if-eqz v0, :cond_0

    .line 360
    invoke-interface {v0, p1, p2, p3}, Lee/cyber/smartid/cryptolib/inter/CryptoOp;->encryptKey(Ljava/math/BigInteger;Ljava/lang/String;I)Lee/cyber/smartid/cryptolib/dto/ClientShare;

    move-result-object p1

    return-object p1

    .line 358
    :cond_0
    new-instance p1, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;

    const/16 p2, 0x65

    const-string p3, "cryptoOpImpl == null"

    invoke-direct {p1, p2, p3}, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;-><init>(ILjava/lang/String;)V

    throw p1
.end method

.method public encryptToKTKEncryptedJWE(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;
    .locals 10

    move-object v0, p0

    .line 374
    iget-object v1, v0, Lee/cyber/smartid/cryptolib/CryptoLib;->f:Lee/cyber/smartid/cryptolib/inter/CryptoOp;

    if-eqz v1, :cond_0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move/from16 v9, p8

    .line 377
    invoke-interface/range {v1 .. v9}, Lee/cyber/smartid/cryptolib/inter/CryptoOp;->encryptToKTKEncryptedJWE(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v1

    return-object v1

    .line 375
    :cond_0
    new-instance v1, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;

    const/16 v2, 0x65

    const-string v3, "cryptoOpImpl == null"

    invoke-direct {v1, v2, v3}, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;-><init>(ILjava/lang/String;)V

    throw v1
.end method

.method public encryptToTEKEncryptedJWE(Ljava/lang/String;[BLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 6

    .line 392
    iget-object v0, p0, Lee/cyber/smartid/cryptolib/CryptoLib;->f:Lee/cyber/smartid/cryptolib/inter/CryptoOp;

    if-eqz v0, :cond_0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    .line 395
    invoke-interface/range {v0 .. v5}, Lee/cyber/smartid/cryptolib/inter/CryptoOp;->encryptToTEKEncryptedJWE(Ljava/lang/String;[BLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 393
    :cond_0
    new-instance p1, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;

    const/16 p2, 0x65

    const-string p3, "cryptoOpImpl == null"

    invoke-direct {p1, p2, p3}, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;-><init>(ILjava/lang/String;)V

    throw p1
.end method

.method public generateDiffieHellmanKeyPair(Lee/cyber/smartid/cryptolib/dto/DiffieHellmanGroup;)Lee/cyber/smartid/cryptolib/dto/DiffieHellmanKeyPair;
    .locals 2

    .line 480
    iget-object v0, p0, Lee/cyber/smartid/cryptolib/CryptoLib;->d:Lee/cyber/smartid/cryptolib/inter/KeyGenerationOp;

    if-eqz v0, :cond_0

    .line 483
    invoke-interface {v0, p1}, Lee/cyber/smartid/cryptolib/inter/KeyGenerationOp;->generateDiffieHellmanKeyPair(Lee/cyber/smartid/cryptolib/dto/DiffieHellmanGroup;)Lee/cyber/smartid/cryptolib/dto/DiffieHellmanKeyPair;

    move-result-object p1

    return-object p1

    .line 481
    :cond_0
    new-instance p1, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;

    const/16 v0, 0x65

    const-string v1, "keyGenerationOpImpl == null"

    invoke-direct {p1, v0, v1}, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;-><init>(ILjava/lang/String;)V

    throw p1
.end method

.method public generatePQAndCreateKeyShare(ILjava/math/BigInteger;)[Ljava/lang/String;
    .locals 1

    .line 471
    iget-object v0, p0, Lee/cyber/smartid/cryptolib/CryptoLib;->d:Lee/cyber/smartid/cryptolib/inter/KeyGenerationOp;

    if-eqz v0, :cond_0

    .line 474
    invoke-interface {v0, p1, p2}, Lee/cyber/smartid/cryptolib/inter/KeyGenerationOp;->generatePQAndCreateKeyShare(ILjava/math/BigInteger;)[Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 472
    :cond_0
    new-instance p1, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;

    const/16 p2, 0x65

    const-string v0, "keyGenerationOpImpl == null"

    invoke-direct {p1, p2, v0}, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;-><init>(ILjava/lang/String;)V

    throw p1
.end method

.method public generateRandomNonce()[B
    .locals 3

    .line 516
    iget-object v0, p0, Lee/cyber/smartid/cryptolib/CryptoLib;->e:Lee/cyber/smartid/cryptolib/inter/RandomGenerationOp;

    if-eqz v0, :cond_0

    .line 519
    invoke-interface {v0}, Lee/cyber/smartid/cryptolib/inter/RandomGenerationOp;->generateRandomNonce()[B

    move-result-object v0

    return-object v0

    .line 517
    :cond_0
    new-instance v0, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;

    const/16 v1, 0x65

    const-string v2, "randomGenerationOpImpl == null"

    invoke-direct {v0, v1, v2}, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;-><init>(ILjava/lang/String;)V

    throw v0
.end method

.method public generateVerificationCode(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .line 443
    iget-object v0, p0, Lee/cyber/smartid/cryptolib/CryptoLib;->b:Lee/cyber/smartid/cryptolib/inter/SigningOp;

    if-eqz v0, :cond_0

    .line 446
    invoke-interface {v0, p1}, Lee/cyber/smartid/cryptolib/inter/SigningOp;->generateVerificationCode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 444
    :cond_0
    new-instance p1, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;

    const/16 v0, 0x65

    const-string v1, "signingOpImpl == null"

    invoke-direct {p1, v0, v1}, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;-><init>(ILjava/lang/String;)V

    throw p1
.end method

.method public getRandom()Ljava/security/SecureRandom;
    .locals 3

    .line 507
    iget-object v0, p0, Lee/cyber/smartid/cryptolib/CryptoLib;->e:Lee/cyber/smartid/cryptolib/inter/RandomGenerationOp;

    if-eqz v0, :cond_0

    .line 510
    invoke-interface {v0}, Lee/cyber/smartid/cryptolib/inter/RandomGenerationOp;->getRandom()Ljava/security/SecureRandom;

    move-result-object v0

    return-object v0

    .line 508
    :cond_0
    new-instance v0, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;

    const/16 v1, 0x65

    const-string v2, "randomGenerationOpImpl == null"

    invoke-direct {v0, v1, v2}, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;-><init>(ILjava/lang/String;)V

    throw v0
.end method

.method public getVersion()Ljava/lang/String;
    .locals 1

    const-string v0, "10.3.3_RELEASE"

    return-object v0
.end method

.method public isSignedJWS(Ljava/lang/String;)Z
    .locals 2

    .line 425
    iget-object v0, p0, Lee/cyber/smartid/cryptolib/CryptoLib;->f:Lee/cyber/smartid/cryptolib/inter/CryptoOp;

    if-eqz v0, :cond_0

    .line 428
    invoke-interface {v0, p1}, Lee/cyber/smartid/cryptolib/inter/CryptoOp;->isSignedJWS(Ljava/lang/String;)Z

    move-result p1

    return p1

    .line 426
    :cond_0
    new-instance p1, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;

    const/16 v0, 0x65

    const-string v1, "cryptoOpImpl == null"

    invoke-direct {p1, v0, v1}, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;-><init>(ILjava/lang/String;)V

    throw p1
.end method

.method public padToSize([BI)[B
    .locals 1

    .line 242
    iget-object v0, p0, Lee/cyber/smartid/cryptolib/CryptoLib;->c:Lee/cyber/smartid/cryptolib/inter/EncodingOp;

    if-eqz v0, :cond_0

    .line 245
    invoke-interface {v0, p1, p2}, Lee/cyber/smartid/cryptolib/inter/EncodingOp;->padToSize([BI)[B

    move-result-object p1

    return-object p1

    .line 243
    :cond_0
    new-instance p1, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;

    const/16 p2, 0x65

    const-string v0, "encodingOpImpl == null"

    invoke-direct {p1, p2, v0}, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;-><init>(ILjava/lang/String;)V

    throw p1
.end method

.method public parseAndExtractHeaderFromJWS(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .line 305
    iget-object v0, p0, Lee/cyber/smartid/cryptolib/CryptoLib;->c:Lee/cyber/smartid/cryptolib/inter/EncodingOp;

    if-eqz v0, :cond_0

    .line 308
    invoke-interface {v0, p1}, Lee/cyber/smartid/cryptolib/inter/EncodingOp;->parseAndExtractHeaderFromJWS(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 306
    :cond_0
    new-instance p1, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;

    const/16 v0, 0x65

    const-string v1, "encodingOpImpl == null"

    invoke-direct {p1, v0, v1}, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;-><init>(ILjava/lang/String;)V

    throw p1
.end method

.method public parseAndExtractPayloadFromJWS(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .line 296
    iget-object v0, p0, Lee/cyber/smartid/cryptolib/CryptoLib;->c:Lee/cyber/smartid/cryptolib/inter/EncodingOp;

    if-eqz v0, :cond_0

    .line 299
    invoke-interface {v0, p1}, Lee/cyber/smartid/cryptolib/inter/EncodingOp;->parseAndExtractPayloadFromJWS(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 297
    :cond_0
    new-instance p1, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;

    const/16 v0, 0x65

    const-string v1, "encodingOpImpl == null"

    invoke-direct {p1, v0, v1}, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;-><init>(ILjava/lang/String;)V

    throw p1
.end method

.method public readKeyIdFromJOSEJWE(Lcom/a/a/n;)Ljava/lang/String;
    .locals 2

    .line 401
    iget-object v0, p0, Lee/cyber/smartid/cryptolib/CryptoLib;->f:Lee/cyber/smartid/cryptolib/inter/CryptoOp;

    if-eqz v0, :cond_0

    .line 404
    invoke-interface {v0, p1}, Lee/cyber/smartid/cryptolib/inter/CryptoOp;->readKeyIdFromJOSEJWE(Lcom/a/a/n;)Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 402
    :cond_0
    new-instance p1, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;

    const/16 v0, 0x65

    const-string v1, "cryptoOpImpl == null"

    invoke-direct {p1, v0, v1}, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;-><init>(ILjava/lang/String;)V

    throw p1
.end method

.method public removeData(Ljava/lang/String;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lee/cyber/smartid/cryptolib/dto/StorageException;
        }
    .end annotation

    .line 340
    iget-object v0, p0, Lee/cyber/smartid/cryptolib/CryptoLib;->a:Lee/cyber/smartid/cryptolib/inter/StorageOp;

    if-eqz v0, :cond_0

    .line 343
    invoke-interface {v0, p1}, Lee/cyber/smartid/cryptolib/inter/StorageOp;->removeData(Ljava/lang/String;)V

    return-void

    .line 341
    :cond_0
    new-instance p1, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;

    const/16 v0, 0x65

    const-string v1, "storeImpl == null"

    invoke-direct {p1, v0, v1}, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;-><init>(ILjava/lang/String;)V

    throw p1
.end method

.method public retrieveData(Ljava/lang/String;Ljava/lang/reflect/Type;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/String;",
            "Ljava/lang/reflect/Type;",
            ")TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;
        }
    .end annotation

    .line 331
    iget-object v0, p0, Lee/cyber/smartid/cryptolib/CryptoLib;->a:Lee/cyber/smartid/cryptolib/inter/StorageOp;

    if-eqz v0, :cond_0

    .line 334
    invoke-interface {v0, p1, p2}, Lee/cyber/smartid/cryptolib/inter/StorageOp;->retrieveData(Ljava/lang/String;Ljava/lang/reflect/Type;)Ljava/lang/Object;

    move-result-object p1

    return-object p1

    .line 332
    :cond_0
    new-instance p1, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;

    const/16 p2, 0x65

    const-string v0, "storeImpl == null"

    invoke-direct {p1, p2, v0}, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;-><init>(ILjava/lang/String;)V

    throw p1
.end method

.method public setAndroidDefaults(Landroid/content/Context;)Lee/cyber/smartid/cryptolib/CryptoLib;
    .locals 5

    .line 138
    new-instance v0, Lee/cyber/smartid/cryptolib/impl/EncodingOpImpl;

    invoke-direct {v0}, Lee/cyber/smartid/cryptolib/impl/EncodingOpImpl;-><init>()V

    .line 139
    new-instance v1, Lee/cyber/smartid/cryptolib/impl/RandomGenerationOpImpl;

    invoke-direct {v1}, Lee/cyber/smartid/cryptolib/impl/RandomGenerationOpImpl;-><init>()V

    .line 140
    new-instance v2, Lee/cyber/smartid/cryptolib/impl/CryptoOpImpl;

    invoke-direct {v2, v0}, Lee/cyber/smartid/cryptolib/impl/CryptoOpImpl;-><init>(Lee/cyber/smartid/cryptolib/inter/EncodingOp;)V

    .line 141
    new-instance v3, Lee/cyber/smartid/cryptolib/impl/StorageOpImpl;

    invoke-direct {v3, p1}, Lee/cyber/smartid/cryptolib/impl/StorageOpImpl;-><init>(Landroid/content/Context;)V

    .line 142
    new-instance p1, Lee/cyber/smartid/cryptolib/impl/SigningOpImpl;

    invoke-direct {p1, v0, v2}, Lee/cyber/smartid/cryptolib/impl/SigningOpImpl;-><init>(Lee/cyber/smartid/cryptolib/inter/EncodingOp;Lee/cyber/smartid/cryptolib/inter/CryptoOp;)V

    .line 143
    new-instance v4, Lee/cyber/smartid/cryptolib/impl/KeyGenerationOpImpl;

    invoke-direct {v4, v0, v1}, Lee/cyber/smartid/cryptolib/impl/KeyGenerationOpImpl;-><init>(Lee/cyber/smartid/cryptolib/inter/EncodingOp;Lee/cyber/smartid/cryptolib/inter/RandomGenerationOp;)V

    .line 144
    invoke-virtual {p0, v1}, Lee/cyber/smartid/cryptolib/CryptoLib;->setRandomGenerationOpImpl(Lee/cyber/smartid/cryptolib/inter/RandomGenerationOp;)V

    .line 145
    invoke-virtual {p0, v0}, Lee/cyber/smartid/cryptolib/CryptoLib;->setEncodingOpImpl(Lee/cyber/smartid/cryptolib/inter/EncodingOp;)V

    .line 146
    invoke-virtual {p0, p1}, Lee/cyber/smartid/cryptolib/CryptoLib;->setSigningOpImpl(Lee/cyber/smartid/cryptolib/inter/SigningOp;)V

    .line 147
    invoke-virtual {p0, v3}, Lee/cyber/smartid/cryptolib/CryptoLib;->setStorageOpImpl(Lee/cyber/smartid/cryptolib/inter/StorageOp;)V

    .line 148
    invoke-virtual {p0, v4}, Lee/cyber/smartid/cryptolib/CryptoLib;->setKeyGenerationOpImpl(Lee/cyber/smartid/cryptolib/inter/KeyGenerationOp;)V

    .line 149
    invoke-virtual {p0, v2}, Lee/cyber/smartid/cryptolib/CryptoLib;->setCryptoOpImpl(Lee/cyber/smartid/cryptolib/inter/CryptoOp;)V

    return-object p0
.end method

.method public setCryptoOpImpl(Lee/cyber/smartid/cryptolib/inter/CryptoOp;)V
    .locals 0

    .line 352
    iput-object p1, p0, Lee/cyber/smartid/cryptolib/CryptoLib;->f:Lee/cyber/smartid/cryptolib/inter/CryptoOp;

    return-void
.end method

.method public setEncodingOpImpl(Lee/cyber/smartid/cryptolib/inter/EncodingOp;)V
    .locals 0

    .line 198
    iput-object p1, p0, Lee/cyber/smartid/cryptolib/CryptoLib;->c:Lee/cyber/smartid/cryptolib/inter/EncodingOp;

    return-void
.end method

.method public setKeyGenerationOpImpl(Lee/cyber/smartid/cryptolib/inter/KeyGenerationOp;)V
    .locals 0

    .line 464
    iput-object p1, p0, Lee/cyber/smartid/cryptolib/CryptoLib;->d:Lee/cyber/smartid/cryptolib/inter/KeyGenerationOp;

    return-void
.end method

.method public setRandomGenerationOpImpl(Lee/cyber/smartid/cryptolib/inter/RandomGenerationOp;)V
    .locals 0

    .line 501
    iput-object p1, p0, Lee/cyber/smartid/cryptolib/CryptoLib;->e:Lee/cyber/smartid/cryptolib/inter/RandomGenerationOp;

    return-void
.end method

.method public setSigningOpImpl(Lee/cyber/smartid/cryptolib/inter/SigningOp;)V
    .locals 0

    .line 437
    iput-object p1, p0, Lee/cyber/smartid/cryptolib/CryptoLib;->b:Lee/cyber/smartid/cryptolib/inter/SigningOp;

    return-void
.end method

.method public setStorageOpImpl(Lee/cyber/smartid/cryptolib/inter/StorageOp;)V
    .locals 0

    .line 317
    iput-object p1, p0, Lee/cyber/smartid/cryptolib/CryptoLib;->a:Lee/cyber/smartid/cryptolib/inter/StorageOp;

    return-void
.end method

.method public sign(Lee/cyber/smartid/cryptolib/dto/ClientShare;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 7

    .line 452
    iget-object v0, p0, Lee/cyber/smartid/cryptolib/CryptoLib;->b:Lee/cyber/smartid/cryptolib/inter/SigningOp;

    if-eqz v0, :cond_0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    .line 455
    invoke-interface/range {v0 .. v6}, Lee/cyber/smartid/cryptolib/inter/SigningOp;->sign(Lee/cyber/smartid/cryptolib/dto/ClientShare;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 453
    :cond_0
    new-instance p1, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;

    const/16 p2, 0x65

    const-string p3, "signingOpImpl == null"

    invoke-direct {p1, p2, p3}, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;-><init>(ILjava/lang/String;)V

    throw p1
.end method

.method public storeData(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lee/cyber/smartid/cryptolib/dto/StorageException;
        }
    .end annotation

    .line 323
    iget-object v0, p0, Lee/cyber/smartid/cryptolib/CryptoLib;->a:Lee/cyber/smartid/cryptolib/inter/StorageOp;

    if-eqz v0, :cond_0

    .line 326
    invoke-interface {v0, p1, p2}, Lee/cyber/smartid/cryptolib/inter/StorageOp;->storeData(Ljava/lang/String;Ljava/lang/Object;)V

    return-void

    .line 324
    :cond_0
    new-instance p1, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;

    const/16 p2, 0x65

    const-string v0, "storeImpl == null"

    invoke-direct {p1, p2, v0}, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;-><init>(ILjava/lang/String;)V

    throw p1
.end method

.method public testCSPRNGQuality(I)[Lee/cyber/smartid/cryptolib/dto/TestResult;
    .locals 2

    .line 525
    iget-object v0, p0, Lee/cyber/smartid/cryptolib/CryptoLib;->e:Lee/cyber/smartid/cryptolib/inter/RandomGenerationOp;

    if-eqz v0, :cond_0

    .line 528
    invoke-interface {v0, p1}, Lee/cyber/smartid/cryptolib/inter/RandomGenerationOp;->testCSPRNGQuality(I)[Lee/cyber/smartid/cryptolib/dto/TestResult;

    move-result-object p1

    return-object p1

    .line 526
    :cond_0
    new-instance p1, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;

    const/16 v0, 0x65

    const-string v1, "randomGenerationOpImpl == null"

    invoke-direct {p1, v0, v1}, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;-><init>(ILjava/lang/String;)V

    throw p1
.end method

.method public verifyKTKSignedJWS(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 7

    .line 409
    iget-object v0, p0, Lee/cyber/smartid/cryptolib/CryptoLib;->f:Lee/cyber/smartid/cryptolib/inter/CryptoOp;

    if-eqz v0, :cond_0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    .line 412
    invoke-interface/range {v0 .. v6}, Lee/cyber/smartid/cryptolib/inter/CryptoOp;->verifyKTKSignedJWS(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result p1

    return p1

    .line 410
    :cond_0
    new-instance p1, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;

    const/16 p2, 0x65

    const-string p3, "cryptoOpImpl == null"

    invoke-direct {p1, p2, p3}, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;-><init>(ILjava/lang/String;)V

    throw p1
.end method

.method public verifyKTKSignedJWSAndCheckForRequiredContentValues(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Lee/cyber/smartid/cryptolib/dto/ValuePair;)Z
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "[",
            "Lee/cyber/smartid/cryptolib/dto/ValuePair<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .line 417
    iget-object v0, p0, Lee/cyber/smartid/cryptolib/CryptoLib;->f:Lee/cyber/smartid/cryptolib/inter/CryptoOp;

    if-eqz v0, :cond_0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    move-object v7, p7

    .line 420
    invoke-interface/range {v0 .. v7}, Lee/cyber/smartid/cryptolib/inter/CryptoOp;->verifyKTKSignedJWSAndCheckForRequiredContentValues(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Lee/cyber/smartid/cryptolib/dto/ValuePair;)Z

    move-result p1

    return p1

    .line 418
    :cond_0
    new-instance p1, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;

    const/16 p2, 0x65

    const-string p3, "cryptoOpImpl == null"

    invoke-direct {p1, p2, p3}, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;-><init>(ILjava/lang/String;)V

    throw p1
.end method
