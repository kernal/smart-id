.class public Lee/cyber/smartid/cryptolib/impl/KeyGenerationOpImpl$GenerationParams;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lee/cyber/smartid/cryptolib/impl/KeyGenerationOpImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "GenerationParams"
.end annotation


# instance fields
.field public e:Ljava/math/BigInteger;

.field public mrIterations:I

.field public nlen:I

.field public securityStrength:I

.field public xP1PrimLen:I

.field public xP1PrimPrimLen:I

.field public xP2Len:I


# direct methods
.method public constructor <init>(ILjava/math/BigInteger;)V
    .locals 3

    .line 144
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/16 v0, 0x800

    if-eq p1, v0, :cond_1

    const/16 v1, 0xc00

    if-ne p1, v1, :cond_0

    goto :goto_0

    .line 146
    :cond_0
    new-instance p1, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;

    const/16 p2, 0x6b

    const-string v0, "Only 2048 and 3072 are supported!"

    invoke-direct {p1, p2, v0}, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;-><init>(ILjava/lang/String;)V

    throw p1

    .line 148
    :cond_1
    :goto_0
    iput p1, p0, Lee/cyber/smartid/cryptolib/impl/KeyGenerationOpImpl$GenerationParams;->nlen:I

    .line 149
    iput-object p2, p0, Lee/cyber/smartid/cryptolib/impl/KeyGenerationOpImpl$GenerationParams;->e:Ljava/math/BigInteger;

    .line 150
    invoke-static {p1}, Lee/cyber/smartid/cryptolib/impl/KeyGenerationOpImpl$GenerationParams;->a(I)I

    move-result p2

    iput p2, p0, Lee/cyber/smartid/cryptolib/impl/KeyGenerationOpImpl$GenerationParams;->securityStrength:I

    const/16 p2, 0x1a0

    const/16 v1, 0x280

    if-ne p1, v0, :cond_2

    const/16 v2, 0x1a0

    goto :goto_1

    :cond_2
    const/16 v2, 0x280

    .line 151
    :goto_1
    iput v2, p0, Lee/cyber/smartid/cryptolib/impl/KeyGenerationOpImpl$GenerationParams;->xP1PrimLen:I

    if-ne p1, v0, :cond_3

    const/16 v2, 0xa0

    goto :goto_2

    :cond_3
    const/16 v2, 0xc0

    .line 152
    :goto_2
    iput v2, p0, Lee/cyber/smartid/cryptolib/impl/KeyGenerationOpImpl$GenerationParams;->xP1PrimPrimLen:I

    if-ne p1, v0, :cond_4

    goto :goto_3

    :cond_4
    const/16 p2, 0x280

    .line 153
    :goto_3
    iput p2, p0, Lee/cyber/smartid/cryptolib/impl/KeyGenerationOpImpl$GenerationParams;->xP2Len:I

    if-ne p1, v0, :cond_5

    const/16 p1, 0x38

    goto :goto_4

    :cond_5
    const/16 p1, 0x40

    .line 154
    :goto_4
    iput p1, p0, Lee/cyber/smartid/cryptolib/impl/KeyGenerationOpImpl$GenerationParams;->mrIterations:I

    return-void
.end method

.method protected static a(I)I
    .locals 1

    const/16 v0, 0x800

    if-ne p0, v0, :cond_0

    const/16 p0, 0x70

    goto :goto_0

    :cond_0
    const/16 p0, 0x80

    :goto_0
    return p0
.end method


# virtual methods
.method public clear()V
    .locals 2

    const/4 v0, 0x0

    .line 173
    iput v0, p0, Lee/cyber/smartid/cryptolib/impl/KeyGenerationOpImpl$GenerationParams;->nlen:I

    const/4 v1, 0x0

    .line 174
    iput-object v1, p0, Lee/cyber/smartid/cryptolib/impl/KeyGenerationOpImpl$GenerationParams;->e:Ljava/math/BigInteger;

    .line 175
    iput v0, p0, Lee/cyber/smartid/cryptolib/impl/KeyGenerationOpImpl$GenerationParams;->securityStrength:I

    .line 176
    iput v0, p0, Lee/cyber/smartid/cryptolib/impl/KeyGenerationOpImpl$GenerationParams;->xP1PrimLen:I

    .line 177
    iput v0, p0, Lee/cyber/smartid/cryptolib/impl/KeyGenerationOpImpl$GenerationParams;->xP1PrimPrimLen:I

    .line 178
    iput v0, p0, Lee/cyber/smartid/cryptolib/impl/KeyGenerationOpImpl$GenerationParams;->xP2Len:I

    .line 179
    iput v0, p0, Lee/cyber/smartid/cryptolib/impl/KeyGenerationOpImpl$GenerationParams;->mrIterations:I

    return-void
.end method
