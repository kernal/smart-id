.class public final Lee/cyber/smartid/cryptolib/impl/CryptoOpImpl;
.super Ljava/lang/Object;

# interfaces
.implements Lee/cyber/smartid/cryptolib/inter/CryptoOp;


# static fields
.field static final a:Lcom/a/a/i;

.field static final b:Lcom/a/a/d;


# instance fields
.field private final c:[B

.field private final d:I

.field private final e:I

.field private f:Lee/cyber/smartid/cryptolib/inter/EncodingOp;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 67
    sget-object v0, Lcom/a/a/i;->h:Lcom/a/a/i;

    sput-object v0, Lee/cyber/smartid/cryptolib/impl/CryptoOpImpl;->a:Lcom/a/a/i;

    .line 68
    sget-object v0, Lcom/a/a/d;->b:Lcom/a/a/d;

    sput-object v0, Lee/cyber/smartid/cryptolib/impl/CryptoOpImpl;->b:Lcom/a/a/d;

    return-void
.end method

.method public constructor <init>(Lee/cyber/smartid/cryptolib/inter/EncodingOp;)V
    .locals 3

    .line 79
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    .line 69
    new-array v1, v0, [B

    const/4 v2, 0x0

    aput-byte v2, v1, v2

    iput-object v1, p0, Lee/cyber/smartid/cryptolib/impl/CryptoOpImpl;->c:[B

    const/16 v1, 0x80

    .line 70
    iput v1, p0, Lee/cyber/smartid/cryptolib/impl/CryptoOpImpl;->d:I

    .line 71
    iput v0, p0, Lee/cyber/smartid/cryptolib/impl/CryptoOpImpl;->e:I

    .line 80
    iput-object p1, p0, Lee/cyber/smartid/cryptolib/impl/CryptoOpImpl;->f:Lee/cyber/smartid/cryptolib/inter/EncodingOp;

    return-void
.end method

.method private a(ILjavax/crypto/spec/SecretKeySpec;Ljavax/crypto/spec/IvParameterSpec;[B)[B
    .locals 1

    :try_start_0
    const-string v0, "AES/CBC/NoPadding"

    .line 192
    invoke-static {v0}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;)Ljavax/crypto/Cipher;

    move-result-object v0

    .line 193
    invoke-virtual {v0, p1, p2, p3}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;Ljava/security/spec/AlgorithmParameterSpec;)V

    .line 194
    invoke-virtual {v0, p4}, Ljavax/crypto/Cipher;->doFinal([B)[B

    move-result-object p1
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_5
    .catch Ljavax/crypto/NoSuchPaddingException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ljava/security/InvalidAlgorithmParameterException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/security/InvalidKeyException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljavax/crypto/BadPaddingException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljavax/crypto/IllegalBlockSizeException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    move-exception p1

    goto :goto_0

    :catch_1
    move-exception p1

    goto :goto_0

    :catch_2
    move-exception p1

    goto :goto_0

    :catch_3
    move-exception p1

    goto :goto_0

    :catch_4
    move-exception p1

    goto :goto_0

    :catch_5
    move-exception p1

    .line 196
    :goto_0
    new-instance p2, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;

    const/16 p3, 0x68

    invoke-virtual {p1}, Ljava/security/GeneralSecurityException;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p3, p1}, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;-><init>(ILjava/lang/String;)V

    throw p2
.end method

.method private a([B[BII[BII)[B
    .locals 1

    .line 163
    new-instance v0, Lorg/a/b/d/a;

    invoke-direct {v0}, Lorg/a/b/d/a;-><init>()V

    .line 165
    invoke-virtual {v0, p1, p2, p3}, Lorg/a/b/d/a;->a([B[BI)V

    .line 166
    invoke-virtual {v0, p4, p4}, Lorg/a/b/d/a;->a(II)Lorg/a/b/b;

    move-result-object p1

    check-cast p1, Lorg/a/b/g/d;

    .line 167
    invoke-virtual {p1}, Lorg/a/b/g/d;->b()Lorg/a/b/b;

    move-result-object p2

    check-cast p2, Lorg/a/b/g/c;

    .line 169
    invoke-virtual {p2}, Lorg/a/b/g/c;->a()[B

    move-result-object p2

    .line 170
    invoke-virtual {p1}, Lorg/a/b/g/d;->a()[B

    move-result-object p1

    .line 171
    new-instance p3, Ljavax/crypto/spec/SecretKeySpec;

    const-string p4, "AES"

    invoke-direct {p3, p2, p4}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V

    .line 172
    new-instance p2, Ljavax/crypto/spec/IvParameterSpec;

    invoke-direct {p2, p1}, Ljavax/crypto/spec/IvParameterSpec;-><init>([B)V

    const/4 p1, 0x1

    if-ne p6, p1, :cond_0

    .line 175
    iget-object p1, p0, Lee/cyber/smartid/cryptolib/impl/CryptoOpImpl;->f:Lee/cyber/smartid/cryptolib/inter/EncodingOp;

    new-instance p4, Ljava/math/BigInteger;

    invoke-direct {p4, p5}, Ljava/math/BigInteger;-><init>([B)V

    invoke-interface {p1, p4}, Lee/cyber/smartid/cryptolib/inter/EncodingOp;->encodePositiveBigIntegerAsBytes(Ljava/math/BigInteger;)[B

    move-result-object p1

    .line 176
    iget-object p4, p0, Lee/cyber/smartid/cryptolib/impl/CryptoOpImpl;->f:Lee/cyber/smartid/cryptolib/inter/EncodingOp;

    invoke-interface {p4, p1, p7}, Lee/cyber/smartid/cryptolib/inter/EncodingOp;->padToSize([BI)[B

    move-result-object p1

    invoke-direct {p0, p6, p3, p2, p1}, Lee/cyber/smartid/cryptolib/impl/CryptoOpImpl;->a(ILjavax/crypto/spec/SecretKeySpec;Ljavax/crypto/spec/IvParameterSpec;[B)[B

    move-result-object p1

    return-object p1

    :cond_0
    const/4 p1, 0x2

    if-ne p6, p1, :cond_1

    .line 178
    invoke-direct {p0, p6, p3, p2, p5}, Lee/cyber/smartid/cryptolib/impl/CryptoOpImpl;->a(ILjavax/crypto/spec/SecretKeySpec;Ljavax/crypto/spec/IvParameterSpec;[B)[B

    move-result-object p1

    return-object p1

    .line 180
    :cond_1
    new-instance p1, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;

    const/16 p2, 0x69

    const-string p3, "Illegal Cipher mode!"

    invoke-direct {p1, p2, p3}, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;-><init>(ILjava/lang/String;)V

    throw p1
.end method

.method private b(Ljava/lang/String;)Lcom/a/a/r;
    .locals 2

    const-string v0, "Unable to parse the \"jwsString\"."

    const-string v1, "The \"jwsString\" parameter can\'t be empty or null!"

    .line 421
    invoke-static {p1, v1}, Lee/cyber/smartid/cryptolib/util/Util;->throwIfEmpty(Ljava/lang/String;Ljava/lang/String;)V

    const/16 v1, 0x78

    .line 425
    :try_start_0
    invoke-static {p1}, Lcom/a/a/r;->b(Ljava/lang/String;)Lcom/a/a/r;

    move-result-object p1
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    .line 429
    :catch_0
    new-instance p1, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;

    invoke-direct {p1, v1, v0}, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;-><init>(ILjava/lang/String;)V

    throw p1

    .line 427
    :catch_1
    new-instance p1, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;

    invoke-direct {p1, v1, v0}, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;-><init>(ILjava/lang/String;)V

    throw p1
.end method


# virtual methods
.method a(Ljava/lang/String;)Ljava/security/interfaces/RSAPublicKey;
    .locals 2

    const-string v0, "base64EncodedKey parameter can\'t be null!"

    .line 248
    invoke-static {p1, v0}, Lee/cyber/smartid/cryptolib/util/Util;->throwIfEmpty(Ljava/lang/String;Ljava/lang/String;)V

    .line 251
    :try_start_0
    iget-object v0, p0, Lee/cyber/smartid/cryptolib/impl/CryptoOpImpl;->f:Lee/cyber/smartid/cryptolib/inter/EncodingOp;

    invoke-interface {v0, p1}, Lee/cyber/smartid/cryptolib/inter/EncodingOp;->decodeBytesFromBase64(Ljava/lang/String;)[B

    move-result-object p1

    .line 252
    invoke-static {p1}, Ljavax/security/cert/X509Certificate;->getInstance([B)Ljavax/security/cert/X509Certificate;

    move-result-object p1

    .line 253
    invoke-virtual {p1}, Ljavax/security/cert/X509Certificate;->getPublicKey()Ljava/security/PublicKey;

    move-result-object p1

    check-cast p1, Ljava/security/interfaces/RSAPublicKey;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    move-exception p1

    .line 255
    new-instance v0, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;

    const/16 v1, 0x74

    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, v1, p1}, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;-><init>(ILjava/lang/String;)V

    throw v0
.end method

.method protected final clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .line 91
    new-instance v0, Ljava/lang/CloneNotSupportedException;

    invoke-direct {v0}, Ljava/lang/CloneNotSupportedException;-><init>()V

    throw v0
.end method

.method public decryptFromTEKEncryptedJWE(Ljava/lang/String;[BLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/a/a/n;
    .locals 6

    const-string v0, "The \"encryptedJWEString\" parameter can\'t be empty or null!"

    .line 262
    invoke-static {p1, v0}, Lee/cyber/smartid/cryptolib/util/Util;->throwIfEmpty(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "The \"keyMaterial\" parameter can\'t be null!"

    .line 263
    invoke-static {p2, v0}, Lee/cyber/smartid/cryptolib/util/Util;->throwIfEmpty([BLjava/lang/String;)V

    const-string v0, "The \"audience\" parameter can\'t be null!"

    .line 264
    invoke-static {p5, v0}, Lee/cyber/smartid/cryptolib/util/Util;->throwIfEmpty(Ljava/lang/String;Ljava/lang/String;)V

    const/16 v0, 0x78

    .line 271
    :try_start_0
    invoke-static {p1}, Lcom/a/a/n;->b(Ljava/lang/String;)Lcom/a/a/n;

    move-result-object p1
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_2

    .line 277
    invoke-virtual {p1}, Lcom/a/a/n;->c()Lcom/a/a/m;

    move-result-object v1

    if-eqz v1, :cond_d

    .line 282
    invoke-virtual {v1}, Lcom/a/a/m;->g()Lcom/a/a/i;

    move-result-object v0

    const-string v2, "\" not supported!"

    const-string v3, "NULL"

    if-eqz v0, :cond_b

    invoke-virtual {v1}, Lcom/a/a/m;->g()Lcom/a/a/i;

    move-result-object v0

    sget-object v4, Lee/cyber/smartid/cryptolib/impl/CryptoOpImpl;->a:Lcom/a/a/i;

    invoke-virtual {v0, v4}, Lcom/a/a/i;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    goto/16 :goto_4

    .line 285
    :cond_0
    invoke-virtual {v1}, Lcom/a/a/m;->h()Lcom/a/a/d;

    move-result-object v0

    if-eqz v0, :cond_9

    invoke-virtual {v1}, Lcom/a/a/m;->h()Lcom/a/a/d;

    move-result-object v0

    sget-object v4, Lee/cyber/smartid/cryptolib/impl/CryptoOpImpl;->b:Lcom/a/a/d;

    invoke-virtual {v0, v4}, Lcom/a/a/d;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    goto/16 :goto_3

    .line 288
    :cond_1
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    const-string v2, "\""

    const-string v4, "\" does not match the required value \""

    const/16 v5, 0x81

    if-nez v0, :cond_3

    invoke-virtual {p0, p1}, Lee/cyber/smartid/cryptolib/impl/CryptoOpImpl;->readKeyIdFromJOSEJWE(Lcom/a/a/n;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    goto :goto_0

    .line 290
    :cond_2
    new-instance p1, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string p4, "Encryption key id \""

    invoke-virtual {p2, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Lcom/a/a/m;->a()Ljava/lang/String;

    move-result-object p4

    invoke-virtual {p2, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, v5, p2}, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;-><init>(ILjava/lang/String;)V

    throw p1

    .line 291
    :cond_3
    :goto_0
    invoke-static {p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p3

    if-nez p3, :cond_5

    const-string p3, "keyUUID"

    invoke-virtual {v1, p3}, Lcom/a/a/m;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Ljava/lang/String;

    if-eqz v0, :cond_4

    invoke-virtual {v1, p3}, Lcom/a/a/m;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0, p4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_4

    goto :goto_1

    .line 293
    :cond_4
    new-instance p1, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string p5, "Encryption key UUID \""

    invoke-virtual {p2, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p3}, Lcom/a/a/m;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p3

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, v5, p2}, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;-><init>(ILjava/lang/String;)V

    throw p1

    :cond_5
    :goto_1
    const-string p3, "aud"

    .line 294
    invoke-virtual {v1, p3}, Lcom/a/a/m;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p4

    instance-of p4, p4, Ljava/lang/String;

    if-eqz p4, :cond_7

    invoke-virtual {v1, p3}, Lcom/a/a/m;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p4

    check-cast p4, Ljava/lang/String;

    invoke-static {p4, p5}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result p4

    if-nez p4, :cond_6

    goto :goto_2

    :cond_6
    const/16 p3, 0x68

    .line 301
    :try_start_1
    new-instance p4, Lcom/a/a/a/a;

    invoke-direct {p4, p2}, Lcom/a/a/a/a;-><init>([B)V

    invoke-virtual {p1, p4}, Lcom/a/a/n;->a(Lcom/a/a/k;)V
    :try_end_1
    .catch Lcom/a/a/f; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    return-object p1

    :catch_0
    move-exception p1

    .line 305
    new-instance p2, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;

    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p3, p1}, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;-><init>(ILjava/lang/String;)V

    throw p2

    :catch_1
    move-exception p1

    .line 303
    new-instance p2, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;

    invoke-virtual {p1}, Lcom/a/a/f;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p3, p1}, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;-><init>(ILjava/lang/String;)V

    throw p2

    .line 296
    :cond_7
    :goto_2
    new-instance p1, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;

    const/16 p2, 0x82

    new-instance p4, Ljava/lang/StringBuilder;

    invoke-direct {p4}, Ljava/lang/StringBuilder;-><init>()V

    const-string p5, "Encryption audience \""

    invoke-virtual {p4, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p3}, Lcom/a/a/m;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p5

    if-eqz p5, :cond_8

    invoke-virtual {v1, p3}, Lcom/a/a/m;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    :cond_8
    invoke-virtual {p4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string p3, "\" not allowed!"

    invoke-virtual {p4, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p3

    invoke-direct {p1, p2, p3}, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;-><init>(ILjava/lang/String;)V

    throw p1

    .line 287
    :cond_9
    :goto_3
    new-instance p1, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;

    const/16 p2, 0x7f

    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    const-string p4, "Encryption method \""

    invoke-virtual {p3, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Lcom/a/a/m;->h()Lcom/a/a/d;

    move-result-object p4

    if-eqz p4, :cond_a

    invoke-virtual {v1}, Lcom/a/a/m;->h()Lcom/a/a/d;

    move-result-object p4

    invoke-virtual {p4}, Lcom/a/a/d;->a()Ljava/lang/String;

    move-result-object v3

    :cond_a
    invoke-virtual {p3, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p3

    invoke-direct {p1, p2, p3}, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;-><init>(ILjava/lang/String;)V

    throw p1

    .line 284
    :cond_b
    :goto_4
    new-instance p1, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;

    const/16 p2, 0x80

    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    const-string p4, "Encryption algorithm \""

    invoke-virtual {p3, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Lcom/a/a/m;->g()Lcom/a/a/i;

    move-result-object p4

    if-eqz p4, :cond_c

    invoke-virtual {v1}, Lcom/a/a/m;->g()Lcom/a/a/i;

    move-result-object p4

    invoke-virtual {p4}, Lcom/a/a/i;->a()Ljava/lang/String;

    move-result-object v3

    :cond_c
    invoke-virtual {p3, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p3

    invoke-direct {p1, p2, p3}, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;-><init>(ILjava/lang/String;)V

    throw p1

    .line 281
    :cond_d
    new-instance p1, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;

    const-string p2, "Illegal \"encryptedJWEString\" value supplied, the header is missing!"

    invoke-direct {p1, v0, p2}, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;-><init>(ILjava/lang/String;)V

    throw p1

    :catch_2
    move-exception p1

    .line 275
    new-instance p2, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;

    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, v0, p1}, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;-><init>(ILjava/lang/String;)V

    throw p2

    :catch_3
    move-exception p1

    .line 273
    new-instance p2, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;

    invoke-virtual {p1}, Ljava/text/ParseException;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, v0, p1}, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;-><init>(ILjava/lang/String;)V

    throw p2
.end method

.method public decryptKey(Lee/cyber/smartid/cryptolib/dto/ClientShare;Ljava/lang/String;)Ljava/math/BigInteger;
    .locals 9

    const-string v0, "ClientShare can\'t be null!"

    .line 136
    invoke-static {p1, v0}, Lee/cyber/smartid/cryptolib/util/Util;->throwIfNull(Ljava/io/Serializable;Ljava/lang/String;)V

    const-string v0, "Pin can\'t be null!"

    .line 137
    invoke-static {p2, v0}, Lee/cyber/smartid/cryptolib/util/Util;->throwIfNull(Ljava/io/Serializable;Ljava/lang/String;)V

    .line 139
    invoke-virtual {p1}, Lee/cyber/smartid/cryptolib/dto/ClientShare;->getKeyShare()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lorg/a/f/a/a;->a(Ljava/lang/String;)[B

    move-result-object v6

    :try_start_0
    const-string v0, "UTF-8"

    .line 142
    invoke-virtual {p2, v0}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v2
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    .line 147
    new-instance p2, Ljava/math/BigInteger;

    const/4 v0, 0x1

    iget-object v3, p0, Lee/cyber/smartid/cryptolib/impl/CryptoOpImpl;->c:[B

    const/4 v4, 0x1

    const/16 v5, 0x80

    const/4 v7, 0x2

    invoke-virtual {p1}, Lee/cyber/smartid/cryptolib/dto/ClientShare;->getKeySize()I

    move-result v8

    move-object v1, p0

    invoke-direct/range {v1 .. v8}, Lee/cyber/smartid/cryptolib/impl/CryptoOpImpl;->a([B[BII[BII)[B

    move-result-object p1

    invoke-direct {p2, v0, p1}, Ljava/math/BigInteger;-><init>(I[B)V

    return-object p2

    .line 144
    :catch_0
    new-instance p1, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;

    const/16 p2, 0x73

    const-string v0, "Unsupported encoding"

    invoke-direct {p1, p2, v0}, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;-><init>(ILjava/lang/String;)V

    throw p1
.end method

.method public encryptKey(Ljava/math/BigInteger;Ljava/lang/String;I)Lee/cyber/smartid/cryptolib/dto/ClientShare;
    .locals 9

    const-string v0, "Pin parameter can\'t be null!"

    .line 119
    invoke-static {p2, v0}, Lee/cyber/smartid/cryptolib/util/Util;->throwIfNull(Ljava/io/Serializable;Ljava/lang/String;)V

    const-string v0, "Key parameter can\'t be null!"

    .line 120
    invoke-static {p1, v0}, Lee/cyber/smartid/cryptolib/util/Util;->throwIfNull(Ljava/math/BigInteger;Ljava/lang/String;)V

    :try_start_0
    const-string v0, "UTF-8"

    .line 123
    invoke-virtual {p2, v0}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v2
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    .line 128
    new-instance p2, Lee/cyber/smartid/cryptolib/dto/ClientShare;

    iget-object v3, p0, Lee/cyber/smartid/cryptolib/impl/CryptoOpImpl;->c:[B

    const/4 v4, 0x1

    const/16 v5, 0x80

    invoke-virtual {p1}, Ljava/math/BigInteger;->toByteArray()[B

    move-result-object v6

    const/4 v7, 0x1

    move-object v1, p0

    move v8, p3

    invoke-direct/range {v1 .. v8}, Lee/cyber/smartid/cryptolib/impl/CryptoOpImpl;->a([B[BII[BII)[B

    move-result-object p1

    invoke-static {p1}, Lorg/a/f/a/a;->a([B)Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1, p3}, Lee/cyber/smartid/cryptolib/dto/ClientShare;-><init>(Ljava/lang/String;I)V

    return-object p2

    .line 125
    :catch_0
    new-instance p1, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;

    const/16 p2, 0x73

    const-string p3, "Unsupported encoding"

    invoke-direct {p1, p2, p3}, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;-><init>(ILjava/lang/String;)V

    throw p1
.end method

.method public encryptToKTKEncryptedJWE(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;
    .locals 2

    const-string v0, "The payload parameter can\'t be null!"

    .line 202
    invoke-static {p1, v0}, Lee/cyber/smartid/cryptolib/util/Util;->throwIfNull(Ljava/io/Serializable;Ljava/lang/String;)V

    const-string v0, "The base64EncodedCert parameter can\'t be empty or null!"

    .line 203
    invoke-static {p2, v0}, Lee/cyber/smartid/cryptolib/util/Util;->throwIfEmpty(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "The algorithmName parameter can\'t be empty or null!"

    .line 204
    invoke-static {p4, v0}, Lee/cyber/smartid/cryptolib/util/Util;->throwIfEmpty(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "The encodingName parameter can\'t be empty or null!"

    .line 205
    invoke-static {p5, v0}, Lee/cyber/smartid/cryptolib/util/Util;->throwIfEmpty(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "The keyId parameter can\'t be empty or null!"

    .line 206
    invoke-static {p3, v0}, Lee/cyber/smartid/cryptolib/util/Util;->throwIfEmpty(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "The audience parameter can\'t be empty or null!"

    .line 207
    invoke-static {p6, v0}, Lee/cyber/smartid/cryptolib/util/Util;->throwIfEmpty(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "The purpose parameter can\'t be empty or null!"

    .line 208
    invoke-static {p7, v0}, Lee/cyber/smartid/cryptolib/util/Util;->throwIfEmpty(Ljava/lang/String;Ljava/lang/String;)V

    .line 214
    :try_start_0
    invoke-virtual {p0, p2}, Lee/cyber/smartid/cryptolib/impl/CryptoOpImpl;->a(Ljava/lang/String;)Ljava/security/interfaces/RSAPublicKey;

    move-result-object p2
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_3

    const/16 v0, 0x68

    .line 224
    :try_start_1
    invoke-static {p4}, Lcom/a/a/i;->a(Ljava/lang/String;)Lcom/a/a/i;

    move-result-object p4

    .line 225
    invoke-static {p5}, Lcom/a/a/d;->a(Ljava/lang/String;)Lcom/a/a/d;

    move-result-object p5

    .line 226
    new-instance v1, Lcom/a/a/m$a;

    invoke-direct {v1, p4, p5}, Lcom/a/a/m$a;-><init>(Lcom/a/a/i;Lcom/a/a/d;)V

    invoke-virtual {v1, p3}, Lcom/a/a/m$a;->b(Ljava/lang/String;)Lcom/a/a/m$a;

    move-result-object p3

    const-string p4, "aud"

    invoke-virtual {p3, p4, p6}, Lcom/a/a/m$a;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/a/a/m$a;

    move-result-object p3

    const-string p4, "purpose"

    invoke-virtual {p3, p4, p7}, Lcom/a/a/m$a;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/a/a/m$a;

    move-result-object p3

    invoke-virtual {p3}, Lcom/a/a/m$a;->a()Lcom/a/a/m;

    move-result-object p3

    .line 227
    new-instance p4, Lcom/a/a/v;

    iget-object p5, p0, Lee/cyber/smartid/cryptolib/impl/CryptoOpImpl;->f:Lee/cyber/smartid/cryptolib/inter/EncodingOp;

    iget-object p6, p0, Lee/cyber/smartid/cryptolib/impl/CryptoOpImpl;->f:Lee/cyber/smartid/cryptolib/inter/EncodingOp;

    invoke-interface {p6, p1}, Lee/cyber/smartid/cryptolib/inter/EncodingOp;->decodeDecimalFromBase64(Ljava/lang/String;)Ljava/math/BigInteger;

    move-result-object p1

    invoke-interface {p5, p1, p8}, Lee/cyber/smartid/cryptolib/inter/EncodingOp;->encodeI2OSP(Ljava/math/BigInteger;I)[B

    move-result-object p1

    invoke-direct {p4, p1}, Lcom/a/a/v;-><init>([B)V

    .line 228
    new-instance p1, Lcom/a/a/n;

    invoke-direct {p1, p3, p4}, Lcom/a/a/n;-><init>(Lcom/a/a/m;Lcom/a/a/v;)V

    .line 229
    new-instance p3, Lcom/a/a/a/c;

    invoke-direct {p3, p2}, Lcom/a/a/a/c;-><init>(Ljava/security/interfaces/RSAPublicKey;)V

    invoke-virtual {p1, p3}, Lcom/a/a/n;->a(Lcom/a/a/l;)V
    :try_end_1
    .catch Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Lcom/a/a/f; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 237
    invoke-virtual {p1}, Lcom/a/a/n;->h()Ljava/lang/String;

    move-result-object p1

    return-object p1

    :catch_0
    move-exception p1

    .line 235
    new-instance p2, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;

    invoke-virtual {p1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, v0, p1}, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;-><init>(ILjava/lang/String;)V

    throw p2

    :catch_1
    move-exception p1

    .line 233
    new-instance p2, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;

    invoke-virtual {p1}, Lcom/a/a/f;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, v0, p1}, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;-><init>(ILjava/lang/String;)V

    throw p2

    :catch_2
    move-exception p1

    .line 231
    throw p1

    :catch_3
    move-exception p1

    .line 216
    new-instance p2, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;

    const/16 p3, 0x74

    invoke-virtual {p1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p3, p1}, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;-><init>(ILjava/lang/String;)V

    throw p2
.end method

.method public encryptToTEKEncryptedJWE(Ljava/lang/String;[BLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    const-string v0, "The \"contentString\" parameter can\'t be empty or null!"

    .line 313
    invoke-static {p1, v0}, Lee/cyber/smartid/cryptolib/util/Util;->throwIfEmpty(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "The \"keyMaterial\" parameter can\'t be null!"

    .line 314
    invoke-static {p2, v0}, Lee/cyber/smartid/cryptolib/util/Util;->throwIfEmpty([BLjava/lang/String;)V

    const-string v0, "The \"keyId\" parameter can\'t be null!"

    .line 315
    invoke-static {p3, v0}, Lee/cyber/smartid/cryptolib/util/Util;->throwIfEmpty(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "The \"keyUUID\" parameter can\'t be null!"

    .line 316
    invoke-static {p4, v0}, Lee/cyber/smartid/cryptolib/util/Util;->throwIfEmpty(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "The \"audience\" parameter can\'t be null!"

    .line 317
    invoke-static {p5, v0}, Lee/cyber/smartid/cryptolib/util/Util;->throwIfEmpty(Ljava/lang/String;Ljava/lang/String;)V

    .line 319
    new-instance v0, Lcom/a/a/m$a;

    sget-object v1, Lee/cyber/smartid/cryptolib/impl/CryptoOpImpl;->a:Lcom/a/a/i;

    sget-object v2, Lee/cyber/smartid/cryptolib/impl/CryptoOpImpl;->b:Lcom/a/a/d;

    invoke-direct {v0, v1, v2}, Lcom/a/a/m$a;-><init>(Lcom/a/a/i;Lcom/a/a/d;)V

    .line 320
    invoke-virtual {v0, p3}, Lcom/a/a/m$a;->b(Ljava/lang/String;)Lcom/a/a/m$a;

    const-string p3, "keyUUID"

    .line 321
    invoke-virtual {v0, p3, p4}, Lcom/a/a/m$a;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/a/a/m$a;

    const-string p3, "aud"

    .line 322
    invoke-virtual {v0, p3, p5}, Lcom/a/a/m$a;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/a/a/m$a;

    .line 324
    new-instance p3, Lcom/a/a/v;

    invoke-direct {p3, p1}, Lcom/a/a/v;-><init>(Ljava/lang/String;)V

    const/16 p1, 0x78

    .line 329
    :try_start_0
    new-instance p4, Lcom/a/a/n;

    invoke-virtual {v0}, Lcom/a/a/m$a;->a()Lcom/a/a/m;

    move-result-object p5

    invoke-direct {p4, p5, p3}, Lcom/a/a/n;-><init>(Lcom/a/a/m;Lcom/a/a/v;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_3

    const/16 p1, 0x68

    .line 338
    :try_start_1
    new-instance p3, Lcom/a/a/a/b;

    invoke-direct {p3, p2}, Lcom/a/a/a/b;-><init>([B)V

    invoke-virtual {p4, p3}, Lcom/a/a/n;->a(Lcom/a/a/l;)V
    :try_end_1
    .catch Lcom/a/a/u; {:try_start_1 .. :try_end_1} :catch_2
    .catch Lcom/a/a/f; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    .line 347
    invoke-virtual {p4}, Lcom/a/a/n;->h()Ljava/lang/String;

    move-result-object p1

    return-object p1

    :catch_0
    move-exception p2

    .line 344
    new-instance p3, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;

    invoke-virtual {p2}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p3, p1, p2}, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;-><init>(ILjava/lang/String;)V

    throw p3

    :catch_1
    move-exception p2

    .line 342
    new-instance p3, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;

    invoke-virtual {p2}, Lcom/a/a/f;->getMessage()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p3, p1, p2}, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;-><init>(ILjava/lang/String;)V

    throw p3

    :catch_2
    move-exception p2

    .line 340
    new-instance p3, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;

    invoke-virtual {p2}, Lcom/a/a/u;->getMessage()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p3, p1, p2}, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;-><init>(ILjava/lang/String;)V

    throw p3

    :catch_3
    move-exception p2

    .line 333
    new-instance p3, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;

    invoke-virtual {p2}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p3, p1, p2}, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;-><init>(ILjava/lang/String;)V

    throw p3

    :catch_4
    move-exception p2

    .line 331
    new-instance p3, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;

    invoke-virtual {p2}, Ljava/lang/IllegalArgumentException;->getMessage()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p3, p1, p2}, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;-><init>(ILjava/lang/String;)V

    throw p3
.end method

.method public isSignedJWS(Ljava/lang/String;)Z
    .locals 2

    const-string v0, "The parameter jws can\'t be empty!"

    .line 475
    invoke-static {p1, v0}, Lee/cyber/smartid/cryptolib/util/Util;->throwIfEmpty(Ljava/lang/String;Ljava/lang/String;)V

    .line 478
    :try_start_0
    invoke-static {p1}, Lcom/a/a/r;->b(Ljava/lang/String;)Lcom/a/a/r;

    move-result-object p1

    .line 479
    invoke-virtual {p1}, Lcom/a/a/r;->f()Lcom/a/a/r$a;

    move-result-object v0

    sget-object v1, Lcom/a/a/r$a;->b:Lcom/a/a/r$a;

    if-eq v0, v1, :cond_0

    invoke-virtual {p1}, Lcom/a/a/r;->f()Lcom/a/a/r$a;

    move-result-object v0

    sget-object v1, Lcom/a/a/r$a;->c:Lcom/a/a/r$a;

    if-ne v0, v1, :cond_1

    :cond_0
    invoke-virtual {p1}, Lcom/a/a/r;->e()Lcom/a/a/d/c;

    move-result-object p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz p1, :cond_1

    const/4 p1, 0x1

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    :goto_0
    return p1

    :catch_0
    move-exception p1

    .line 481
    new-instance v0, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;

    const/16 v1, 0x7a

    invoke-virtual {p1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, v1, p1}, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;-><init>(ILjava/lang/String;)V

    throw v0
.end method

.method public readKeyIdFromJOSEJWE(Lcom/a/a/n;)Ljava/lang/String;
    .locals 1

    const-string v0, "The \"jweObject\" parameter can\'t be null!"

    .line 353
    invoke-static {p1, v0}, Lee/cyber/smartid/cryptolib/util/Util;->throwIfNull(Ljava/io/Serializable;Ljava/lang/String;)V

    .line 354
    invoke-virtual {p1}, Lcom/a/a/n;->c()Lcom/a/a/m;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/a/a/n;->c()Lcom/a/a/m;

    move-result-object p1

    invoke-virtual {p1}, Lcom/a/a/m;->a()Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return-object p1
.end method

.method public verifyKTKSignedJWS(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 5

    const-string v0, "The \"jwsString\" parameter can\'t be empty or null!"

    .line 359
    invoke-static {p1, v0}, Lee/cyber/smartid/cryptolib/util/Util;->throwIfEmpty(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "The \"base64EncodedCert\" parameter can\'t be empty or null!"

    .line 360
    invoke-static {p2, v0}, Lee/cyber/smartid/cryptolib/util/Util;->throwIfEmpty(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "The \"keyId\" parameter can\'t be empty or null!"

    .line 361
    invoke-static {p3, v0}, Lee/cyber/smartid/cryptolib/util/Util;->throwIfEmpty(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "The \"keyUUID\" parameter can\'t be empty or null!"

    .line 362
    invoke-static {p4, v0}, Lee/cyber/smartid/cryptolib/util/Util;->throwIfEmpty(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "The \"audience\" parameter can\'t be empty or null!"

    .line 363
    invoke-static {p5, v0}, Lee/cyber/smartid/cryptolib/util/Util;->throwIfEmpty(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "The \"purpose\" parameter can\'t be empty or null!"

    .line 364
    invoke-static {p6, v0}, Lee/cyber/smartid/cryptolib/util/Util;->throwIfEmpty(Ljava/lang/String;Ljava/lang/String;)V

    .line 367
    invoke-direct {p0, p1}, Lee/cyber/smartid/cryptolib/impl/CryptoOpImpl;->b(Ljava/lang/String;)Lcom/a/a/r;

    move-result-object p1

    .line 370
    invoke-virtual {p1}, Lcom/a/a/r;->c()Lcom/a/a/q;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 373
    invoke-virtual {v0}, Lcom/a/a/q;->g()Lcom/a/a/p;

    move-result-object v1

    if-eqz v1, :cond_5

    invoke-virtual {v0}, Lcom/a/a/q;->g()Lcom/a/a/p;

    move-result-object v1

    sget-object v2, Lcom/a/a/p;->e:Lcom/a/a/p;

    invoke-virtual {v1, v2}, Lcom/a/a/p;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Lcom/a/a/q;->g()Lcom/a/a/p;

    move-result-object v1

    sget-object v2, Lcom/a/a/p;->f:Lcom/a/a/p;

    invoke-virtual {v1, v2}, Lcom/a/a/p;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Lcom/a/a/q;->g()Lcom/a/a/p;

    move-result-object v1

    sget-object v2, Lcom/a/a/p;->g:Lcom/a/a/p;

    invoke-virtual {v1, v2}, Lcom/a/a/p;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    goto/16 :goto_0

    .line 376
    :cond_0
    invoke-virtual {v0}, Lcom/a/a/q;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, p3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    const/16 v2, 0x81

    const-string v3, "\""

    const-string v4, "\" does not match the required value \""

    if-eqz v1, :cond_4

    const-string p3, "keyUUID"

    .line 379
    invoke-virtual {v0, p3}, Lcom/a/a/q;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    instance-of v1, v1, Ljava/lang/String;

    if-eqz v1, :cond_3

    invoke-virtual {v0, p3}, Lcom/a/a/q;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {v1, p4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_3

    const-string p3, "aud"

    .line 382
    invoke-virtual {v0, p3}, Lcom/a/a/q;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p4

    instance-of p4, p4, Ljava/lang/String;

    if-eqz p4, :cond_2

    invoke-virtual {v0, p3}, Lcom/a/a/q;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p4

    check-cast p4, Ljava/lang/String;

    invoke-static {p4, p5}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result p4

    if-eqz p4, :cond_2

    const-string p3, "purpose"

    .line 385
    invoke-virtual {v0, p3}, Lcom/a/a/q;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p4

    instance-of p4, p4, Ljava/lang/String;

    if-eqz p4, :cond_1

    invoke-virtual {v0, p3}, Lcom/a/a/q;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p4

    check-cast p4, Ljava/lang/String;

    invoke-static {p4, p6}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result p4

    if-eqz p4, :cond_1

    .line 394
    :try_start_0
    invoke-virtual {p0, p2}, Lee/cyber/smartid/cryptolib/impl/CryptoOpImpl;->a(Ljava/lang/String;)Ljava/security/interfaces/RSAPublicKey;

    move-result-object p2
    :try_end_0
    .catch Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException; {:try_start_0 .. :try_end_0} :catch_2

    const/16 p3, 0x68

    .line 403
    :try_start_1
    new-instance p4, Lcom/a/a/a/d;

    invoke-direct {p4, p2}, Lcom/a/a/a/d;-><init>(Ljava/security/interfaces/RSAPublicKey;)V

    invoke-virtual {p1, p4}, Lcom/a/a/r;->a(Lcom/a/a/s;)Z

    move-result p1
    :try_end_1
    .catch Lcom/a/a/f; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    return p1

    :catch_0
    move-exception p1

    .line 407
    new-instance p2, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;

    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p3, p1}, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;-><init>(ILjava/lang/String;)V

    throw p2

    :catch_1
    move-exception p1

    .line 405
    new-instance p2, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;

    invoke-virtual {p1}, Lcom/a/a/f;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p3, p1}, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;-><init>(ILjava/lang/String;)V

    throw p2

    :catch_2
    move-exception p1

    .line 396
    new-instance p2, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;

    const/16 p3, 0x74

    invoke-virtual {p1}, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p3, p1}, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;-><init>(ILjava/lang/String;)V

    throw p2

    .line 387
    :cond_1
    new-instance p1, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;

    const/16 p2, 0x85

    new-instance p4, Ljava/lang/StringBuilder;

    invoke-direct {p4}, Ljava/lang/StringBuilder;-><init>()V

    const-string p5, "Signature purpose header value \""

    invoke-virtual {p4, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p3}, Lcom/a/a/q;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p3

    invoke-virtual {p4, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p4, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p4, p6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p3

    invoke-direct {p1, p2, p3}, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;-><init>(ILjava/lang/String;)V

    throw p1

    .line 384
    :cond_2
    new-instance p1, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;

    const/16 p2, 0x82

    new-instance p4, Ljava/lang/StringBuilder;

    invoke-direct {p4}, Ljava/lang/StringBuilder;-><init>()V

    const-string p6, "Signature audience header value \""

    invoke-virtual {p4, p6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p3}, Lcom/a/a/q;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p3

    invoke-virtual {p4, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p4, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p4, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p3

    invoke-direct {p1, p2, p3}, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;-><init>(ILjava/lang/String;)V

    throw p1

    .line 381
    :cond_3
    new-instance p1, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string p5, "Signature keyUUID header value \""

    invoke-virtual {p2, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p3}, Lcom/a/a/q;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p3

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, v2, p2}, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;-><init>(ILjava/lang/String;)V

    throw p1

    .line 378
    :cond_4
    new-instance p1, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string p4, "Signature keyId header value \""

    invoke-virtual {p2, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Lcom/a/a/q;->a()Ljava/lang/String;

    move-result-object p4

    invoke-virtual {p2, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, v2, p2}, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;-><init>(ILjava/lang/String;)V

    throw p1

    .line 375
    :cond_5
    :goto_0
    new-instance p1, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;

    const/16 p2, 0x7e

    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    const-string p4, "Signature algorithm \""

    invoke-virtual {p3, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Lcom/a/a/q;->g()Lcom/a/a/p;

    move-result-object p4

    if-eqz p4, :cond_6

    invoke-virtual {v0}, Lcom/a/a/q;->g()Lcom/a/a/p;

    move-result-object p4

    invoke-virtual {p4}, Lcom/a/a/p;->a()Ljava/lang/String;

    move-result-object p4

    goto :goto_1

    :cond_6
    const-string p4, "NULL"

    :goto_1
    invoke-virtual {p3, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p4, "\" not allowed!"

    invoke-virtual {p3, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p3

    invoke-direct {p1, p2, p3}, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;-><init>(ILjava/lang/String;)V

    throw p1

    .line 372
    :cond_7
    new-instance p1, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;

    const/16 p2, 0x78

    const-string p3, "JWS header is missing!"

    invoke-direct {p1, p2, p3}, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;-><init>(ILjava/lang/String;)V

    throw p1
.end method

.method public verifyKTKSignedJWSAndCheckForRequiredContentValues(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Lee/cyber/smartid/cryptolib/dto/ValuePair;)Z
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "[",
            "Lee/cyber/smartid/cryptolib/dto/ValuePair<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    const-string v0, "Unable to parse the JWS payload to a JSON object"

    const-string v1, "The \"jwsString\" parameter can\'t be empty or null!"

    .line 436
    invoke-static {p1, v1}, Lee/cyber/smartid/cryptolib/util/Util;->throwIfEmpty(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "The \"\" parameter can\'t be empty or null!"

    .line 437
    invoke-static {p2, v1}, Lee/cyber/smartid/cryptolib/util/Util;->throwIfEmpty(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "The \"keyId\" parameter can\'t be empty or null!"

    .line 438
    invoke-static {p3, v1}, Lee/cyber/smartid/cryptolib/util/Util;->throwIfEmpty(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "The \"keyUUID\" parameter can\'t be empty or null!"

    .line 439
    invoke-static {p4, v1}, Lee/cyber/smartid/cryptolib/util/Util;->throwIfEmpty(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "The \"audience\" parameter can\'t be empty or null!"

    .line 440
    invoke-static {p5, v1}, Lee/cyber/smartid/cryptolib/util/Util;->throwIfEmpty(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "The \"purpose\" parameter can\'t be empty or null!"

    .line 441
    invoke-static {p6, v1}, Lee/cyber/smartid/cryptolib/util/Util;->throwIfEmpty(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "The \"requiredContent\" parameter can\'t be empty or null!"

    .line 442
    invoke-static {p7, v1}, Lee/cyber/smartid/cryptolib/util/Util;->throwIfEmpty([Lee/cyber/smartid/cryptolib/dto/ValuePair;Ljava/lang/String;)V

    .line 444
    invoke-virtual/range {p0 .. p6}, Lee/cyber/smartid/cryptolib/impl/CryptoOpImpl;->verifyKTKSignedJWS(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result p2

    const/4 p3, 0x0

    if-nez p2, :cond_0

    return p3

    .line 451
    :cond_0
    invoke-direct {p0, p1}, Lee/cyber/smartid/cryptolib/impl/CryptoOpImpl;->b(Ljava/lang/String;)Lcom/a/a/r;

    move-result-object p1

    const/16 p2, 0x78

    .line 456
    :try_start_0
    new-instance p4, Lorg/json/JSONObject;

    invoke-virtual {p1}, Lcom/a/a/r;->a()Lcom/a/a/v;

    move-result-object p1

    invoke-virtual {p1}, Lcom/a/a/v;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p4, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 464
    array-length p1, p7

    const/4 p5, 0x1

    const/4 p6, 0x0

    const/4 v0, 0x1

    :goto_0
    if-ge p6, p1, :cond_3

    aget-object v1, p7, p6

    if-eqz v1, :cond_2

    .line 465
    iget-object v2, v1, Lee/cyber/smartid/cryptolib/dto/ValuePair;->first:Ljava/lang/Object;

    check-cast v2, Ljava/lang/CharSequence;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 468
    iget-object v2, v1, Lee/cyber/smartid/cryptolib/dto/ValuePair;->first:Ljava/lang/Object;

    check-cast v2, Ljava/lang/String;

    const/4 v3, 0x0

    invoke-virtual {p4, v2, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iget-object v1, v1, Lee/cyber/smartid/cryptolib/dto/ValuePair;->second:Ljava/lang/Object;

    check-cast v1, Ljava/lang/CharSequence;

    invoke-static {v2, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    goto :goto_1

    :cond_1
    const/4 v0, 0x0

    :goto_1
    add-int/lit8 p6, p6, 0x1

    goto :goto_0

    .line 466
    :cond_2
    new-instance p1, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;

    const-string p3, "Invalid key and/or value inside the \"requiredContent\" array! Please check that all keys and values are non-NULL."

    invoke-direct {p1, p2, p3}, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;-><init>(ILjava/lang/String;)V

    throw p1

    :cond_3
    return v0

    .line 460
    :catch_0
    new-instance p1, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;

    invoke-direct {p1, p2, v0}, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;-><init>(ILjava/lang/String;)V

    throw p1

    .line 458
    :catch_1
    new-instance p1, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;

    invoke-direct {p1, p2, v0}, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;-><init>(ILjava/lang/String;)V

    throw p1
.end method
