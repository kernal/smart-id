.class public final Lee/cyber/smartid/cryptolib/impl/RandomGenerationOpImpl;
.super Ljava/lang/Object;

# interfaces
.implements Lee/cyber/smartid/cryptolib/inter/RandomGenerationOp;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(Ljava/util/HashMap;ZJ)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Lee/cyber/smartid/cryptolib/dto/Run;",
            ">;ZJ)V"
        }
    .end annotation

    .line 463
    invoke-static {p2, p3, p4}, Lee/cyber/smartid/cryptolib/dto/Run;->getId(ZJ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lee/cyber/smartid/cryptolib/dto/Run;

    if-eqz v0, :cond_0

    .line 465
    invoke-virtual {v0}, Lee/cyber/smartid/cryptolib/dto/Run;->incrementCount()V

    goto :goto_0

    .line 467
    :cond_0
    new-instance v0, Lee/cyber/smartid/cryptolib/dto/Run;

    const/4 v1, 0x1

    invoke-direct {v0, p2, p3, p4, v1}, Lee/cyber/smartid/cryptolib/dto/Run;-><init>(ZJI)V

    .line 468
    invoke-virtual {v0}, Lee/cyber/smartid/cryptolib/dto/Run;->getId()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :goto_0
    return-void
.end method


# virtual methods
.method a([ZI)D
    .locals 12

    if-eqz p1, :cond_7

    const/16 v0, 0x76

    const/4 v1, 0x1

    if-lt p2, v1, :cond_6

    .line 223
    array-length v2, p1

    if-lt v2, p2, :cond_5

    .line 226
    array-length v0, p1

    div-int/2addr v0, p2

    int-to-double v2, v0

    invoke-static {v2, v3}, Ljava/lang/Math;->floor(D)D

    move-result-wide v2

    double-to-int v0, v2

    int-to-double v2, v0

    const-wide/high16 v4, 0x4014000000000000L    # 5.0

    int-to-double v6, p2

    const-wide/high16 v8, 0x4000000000000000L    # 2.0

    .line 227
    invoke-static {v8, v9, v6, v7}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v10

    mul-double v10, v10, v4

    cmpl-double v0, v2, v10

    if-ltz v0, :cond_4

    .line 233
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    const/4 v4, 0x0

    .line 234
    :goto_0
    array-length v5, p1

    if-ge v4, v5, :cond_2

    .line 235
    invoke-static {p1, v4, p2, v1}, Lee/cyber/smartid/cryptolib/util/Util;->createStringSequenceFromBooleanArray([ZIIZ)Ljava/lang/String;

    move-result-object v5

    .line 236
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_0

    goto :goto_2

    .line 239
    :cond_0
    invoke-virtual {v0, v5}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    .line 241
    invoke-virtual {v0, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/Integer;

    invoke-virtual {v10}, Ljava/lang/Integer;->intValue()I

    move-result v10

    add-int/2addr v10, v1

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v0, v5, v10}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 244
    :cond_1
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v0, v5, v10}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :goto_1
    add-int/2addr v4, p2

    goto :goto_0

    .line 249
    :cond_2
    :goto_2
    invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p1

    const-wide/16 v4, 0x0

    .line 252
    :goto_3
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p2

    if-eqz p2, :cond_3

    .line 253
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Ljava/lang/String;

    .line 254
    invoke-virtual {v0, p2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result p2

    int-to-double v10, p2

    .line 255
    invoke-static {v10, v11, v8, v9}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v10

    add-double/2addr v4, v10

    goto :goto_3

    .line 258
    :cond_3
    invoke-static {v8, v9, v6, v7}, Ljava/lang/Math;->pow(DD)D

    move-result-wide p1

    div-double/2addr p1, v2

    mul-double p1, p1, v4

    sub-double/2addr p1, v2

    return-wide p1

    .line 229
    :cond_4
    new-instance p1, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;

    const/16 p2, 0x78

    const-string v0, "We are no fulfilling the requirement of k >= 5 * (2^m)"

    invoke-direct {p1, p2, v0}, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;-><init>(ILjava/lang/String;)V

    throw p1

    .line 224
    :cond_5
    new-instance v1, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Input bits size ("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    array-length p1, p1

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, ") has to be bigger than m ("

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, ")!"

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v1, v0, p1}, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;-><init>(ILjava/lang/String;)V

    throw v1

    .line 222
    :cond_6
    new-instance p1, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "m ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p2, ") can\'t be smaller than 1!"

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, v0, p2}, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;-><init>(ILjava/lang/String;)V

    throw p1

    .line 220
    :cond_7
    new-instance p1, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;

    const/16 p2, 0x64

    const-string v0, "The parameter bits can\'t be null!"

    invoke-direct {p1, p2, v0}, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;-><init>(ILjava/lang/String;)V

    throw p1
.end method

.method a([Z)I
    .locals 3

    if-eqz p1, :cond_2

    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 200
    :goto_0
    array-length v2, p1

    if-ge v0, v2, :cond_1

    .line 201
    aget-boolean v2, p1, v0

    if-eqz v2, :cond_0

    add-int/lit8 v1, v1, 0x1

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return v1

    .line 195
    :cond_2
    new-instance p1, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;

    const/16 v0, 0x64

    const-string v1, "The parameter bits can\'t be null!"

    invoke-direct {p1, v0, v1}, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;-><init>(ILjava/lang/String;)V

    throw p1
.end method

.method a(Lee/cyber/smartid/cryptolib/dto/Run;II)Z
    .locals 1

    if-eqz p1, :cond_0

    .line 358
    invoke-virtual {p1}, Lee/cyber/smartid/cryptolib/dto/Run;->getCount()I

    move-result v0

    if-le v0, p2, :cond_0

    invoke-virtual {p1}, Lee/cyber/smartid/cryptolib/dto/Run;->getCount()I

    move-result p1

    if-ge p1, p3, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method a(Ljava/util/HashMap;)Z
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Lee/cyber/smartid/cryptolib/dto/Run;",
            ">;)Z"
        }
    .end annotation

    if-eqz p1, :cond_b

    .line 303
    invoke-virtual {p1}, Ljava/util/HashMap;->size()I

    move-result v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-ge v0, v1, :cond_0

    return v2

    :cond_0
    const-wide/16 v3, 0x1

    .line 308
    invoke-static {v2, v3, v4}, Lee/cyber/smartid/cryptolib/dto/Run;->getId(IJ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lee/cyber/smartid/cryptolib/dto/Run;

    const/16 v5, 0xaad

    const/16 v6, 0x8db

    invoke-virtual {p0, v0, v6, v5}, Lee/cyber/smartid/cryptolib/impl/RandomGenerationOpImpl;->a(Lee/cyber/smartid/cryptolib/dto/Run;II)Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-static {v1, v3, v4}, Lee/cyber/smartid/cryptolib/dto/Run;->getId(IJ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lee/cyber/smartid/cryptolib/dto/Run;

    invoke-virtual {p0, v0, v6, v5}, Lee/cyber/smartid/cryptolib/impl/RandomGenerationOpImpl;->a(Lee/cyber/smartid/cryptolib/dto/Run;II)Z

    move-result v0

    if-nez v0, :cond_1

    goto/16 :goto_1

    :cond_1
    const-wide/16 v3, 0x2

    .line 312
    invoke-static {v2, v3, v4}, Lee/cyber/smartid/cryptolib/dto/Run;->getId(IJ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lee/cyber/smartid/cryptolib/dto/Run;

    const/16 v5, 0x58d

    const/16 v6, 0x437

    invoke-virtual {p0, v0, v6, v5}, Lee/cyber/smartid/cryptolib/impl/RandomGenerationOpImpl;->a(Lee/cyber/smartid/cryptolib/dto/Run;II)Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-static {v1, v3, v4}, Lee/cyber/smartid/cryptolib/dto/Run;->getId(IJ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lee/cyber/smartid/cryptolib/dto/Run;

    invoke-virtual {p0, v0, v6, v5}, Lee/cyber/smartid/cryptolib/impl/RandomGenerationOpImpl;->a(Lee/cyber/smartid/cryptolib/dto/Run;II)Z

    move-result v0

    if-nez v0, :cond_2

    goto/16 :goto_1

    :cond_2
    const-wide/16 v3, 0x3

    .line 316
    invoke-static {v2, v3, v4}, Lee/cyber/smartid/cryptolib/dto/Run;->getId(IJ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lee/cyber/smartid/cryptolib/dto/Run;

    const/16 v5, 0x2ec

    const/16 v6, 0x1f6

    invoke-virtual {p0, v0, v6, v5}, Lee/cyber/smartid/cryptolib/impl/RandomGenerationOpImpl;->a(Lee/cyber/smartid/cryptolib/dto/Run;II)Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-static {v1, v3, v4}, Lee/cyber/smartid/cryptolib/dto/Run;->getId(IJ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lee/cyber/smartid/cryptolib/dto/Run;

    invoke-virtual {p0, v0, v6, v5}, Lee/cyber/smartid/cryptolib/impl/RandomGenerationOpImpl;->a(Lee/cyber/smartid/cryptolib/dto/Run;II)Z

    move-result v0

    if-nez v0, :cond_3

    goto/16 :goto_1

    :cond_3
    const-wide/16 v3, 0x4

    .line 320
    invoke-static {v2, v3, v4}, Lee/cyber/smartid/cryptolib/dto/Run;->getId(IJ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lee/cyber/smartid/cryptolib/dto/Run;

    const/16 v5, 0x192

    const/16 v6, 0xdf

    invoke-virtual {p0, v0, v6, v5}, Lee/cyber/smartid/cryptolib/impl/RandomGenerationOpImpl;->a(Lee/cyber/smartid/cryptolib/dto/Run;II)Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-static {v1, v3, v4}, Lee/cyber/smartid/cryptolib/dto/Run;->getId(IJ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lee/cyber/smartid/cryptolib/dto/Run;

    invoke-virtual {p0, v0, v6, v5}, Lee/cyber/smartid/cryptolib/impl/RandomGenerationOpImpl;->a(Lee/cyber/smartid/cryptolib/dto/Run;II)Z

    move-result v0

    if-nez v0, :cond_4

    goto/16 :goto_1

    :cond_4
    const-wide/16 v3, 0x5

    .line 324
    invoke-static {v2, v3, v4}, Lee/cyber/smartid/cryptolib/dto/Run;->getId(IJ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lee/cyber/smartid/cryptolib/dto/Run;

    const/16 v5, 0x5a

    invoke-virtual {p0, v0, v5, v6}, Lee/cyber/smartid/cryptolib/impl/RandomGenerationOpImpl;->a(Lee/cyber/smartid/cryptolib/dto/Run;II)Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-static {v1, v3, v4}, Lee/cyber/smartid/cryptolib/dto/Run;->getId(IJ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lee/cyber/smartid/cryptolib/dto/Run;

    const/16 v3, 0x5a

    invoke-virtual {p0, v0, v3, v6}, Lee/cyber/smartid/cryptolib/impl/RandomGenerationOpImpl;->a(Lee/cyber/smartid/cryptolib/dto/Run;II)Z

    move-result v0

    if-nez v0, :cond_5

    goto :goto_1

    :cond_5
    const-wide/16 v3, 0x0

    const-wide/16 v5, 0x0

    .line 332
    invoke-virtual {p1}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_6
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lee/cyber/smartid/cryptolib/dto/Run;

    .line 333
    invoke-virtual {v0}, Lee/cyber/smartid/cryptolib/dto/Run;->getLength()J

    move-result-wide v7

    const-wide/16 v9, 0x6

    cmp-long v11, v7, v9

    if-ltz v11, :cond_7

    invoke-virtual {v0}, Lee/cyber/smartid/cryptolib/dto/Run;->getType()I

    move-result v7

    if-nez v7, :cond_7

    .line 334
    invoke-virtual {v0}, Lee/cyber/smartid/cryptolib/dto/Run;->getCount()I

    move-result v0

    int-to-long v7, v0

    add-long/2addr v3, v7

    goto :goto_0

    .line 335
    :cond_7
    invoke-virtual {v0}, Lee/cyber/smartid/cryptolib/dto/Run;->getLength()J

    move-result-wide v7

    const-wide/16 v9, 0x6

    cmp-long v11, v7, v9

    if-ltz v11, :cond_6

    invoke-virtual {v0}, Lee/cyber/smartid/cryptolib/dto/Run;->getType()I

    move-result v7

    if-ne v7, v1, :cond_6

    .line 336
    invoke-virtual {v0}, Lee/cyber/smartid/cryptolib/dto/Run;->getCount()I

    move-result v0

    int-to-long v7, v0

    add-long/2addr v5, v7

    goto :goto_0

    :cond_8
    const-wide/16 v7, 0x5a

    cmp-long p1, v3, v7

    if-lez p1, :cond_a

    const-wide/16 v7, 0x5a

    cmp-long p1, v5, v7

    if-lez p1, :cond_a

    const-wide/16 v7, 0xdf

    cmp-long p1, v3, v7

    if-gez p1, :cond_a

    const-wide/16 v3, 0xe9

    cmp-long p1, v5, v3

    if-ltz p1, :cond_9

    goto :goto_1

    :cond_9
    return v1

    :cond_a
    :goto_1
    return v2

    .line 302
    :cond_b
    new-instance p1, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;

    const/16 v0, 0x64

    const-string v1, "The parameter runs can\'t be null!"

    invoke-direct {p1, v0, v1}, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;-><init>(ILjava/lang/String;)V

    throw p1
.end method

.method a(Ljava/security/SecureRandom;I)[Z
    .locals 9

    if-eqz p1, :cond_5

    const/4 v0, 0x1

    if-lt p2, v0, :cond_4

    int-to-double v1, p2

    const-wide/high16 v3, 0x4020000000000000L    # 8.0

    div-double/2addr v1, v3

    .line 404
    invoke-static {v1, v2}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v1

    double-to-int v1, v1

    new-array v1, v1, [B

    .line 405
    invoke-virtual {p1, v1}, Ljava/security/SecureRandom;->nextBytes([B)V

    .line 408
    new-array p1, p2, [Z

    .line 410
    array-length v2, v1

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    :goto_0
    if-ge v4, v2, :cond_3

    aget-byte v6, v1, v4

    move v7, v5

    const/4 v5, 0x0

    :goto_1
    const/16 v8, 0x8

    if-ge v5, v8, :cond_2

    shl-int v8, v0, v5

    and-int/2addr v8, v6

    if-eqz v8, :cond_0

    const/4 v8, 0x1

    goto :goto_2

    :cond_0
    const/4 v8, 0x0

    .line 413
    :goto_2
    aput-boolean v8, p1, v7

    add-int/lit8 v7, v7, 0x1

    if-lt v7, p2, :cond_1

    return-object p1

    :cond_1
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    :cond_2
    add-int/lit8 v4, v4, 0x1

    move v5, v7

    goto :goto_0

    :cond_3
    return-object p1

    .line 401
    :cond_4
    new-instance p1, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;

    const/16 p2, 0x76

    const-string v0, "The parameter bitCount can\'t be smaller than 1!"

    invoke-direct {p1, p2, v0}, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;-><init>(ILjava/lang/String;)V

    throw p1

    .line 399
    :cond_5
    new-instance p1, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;

    const/16 p2, 0x64

    const-string v0, "The parameter SecureRandom can\'t be null!"

    invoke-direct {p1, p2, v0}, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;-><init>(ILjava/lang/String;)V

    throw p1
.end method

.method b([Z)Ljava/util/HashMap;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([Z)",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Lee/cyber/smartid/cryptolib/dto/Run;",
            ">;"
        }
    .end annotation

    if-eqz p1, :cond_5

    .line 437
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 438
    array-length v1, p1

    const/4 v2, 0x1

    if-ge v1, v2, :cond_0

    return-object v0

    .line 443
    :cond_0
    array-length v1, p1

    const/4 v2, 0x0

    const-wide/16 v3, 0x0

    move-wide v5, v3

    const/4 v7, 0x0

    :goto_0
    if-ge v2, v1, :cond_3

    aget-boolean v8, p1, v2

    const-wide/16 v9, 0x1

    if-eq v8, v7, :cond_2

    cmp-long v11, v5, v3

    if-lez v11, :cond_1

    .line 446
    invoke-direct {p0, v0, v7, v5, v6}, Lee/cyber/smartid/cryptolib/impl/RandomGenerationOpImpl;->a(Ljava/util/HashMap;ZJ)V

    :cond_1
    move v7, v8

    move-wide v5, v9

    goto :goto_1

    :cond_2
    add-long/2addr v5, v9

    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_3
    cmp-long p1, v5, v3

    if-lez p1, :cond_4

    .line 456
    invoke-direct {p0, v0, v7, v5, v6}, Lee/cyber/smartid/cryptolib/impl/RandomGenerationOpImpl;->a(Ljava/util/HashMap;ZJ)V

    :cond_4
    return-object v0

    .line 435
    :cond_5
    new-instance p1, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;

    const/16 v0, 0x64

    const-string v1, "The parameter bits can\'t be null!"

    invoke-direct {p1, v0, v1}, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;-><init>(ILjava/lang/String;)V

    throw p1
.end method

.method b(Ljava/util/HashMap;)Z
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Lee/cyber/smartid/cryptolib/dto/Run;",
            ">;)Z"
        }
    .end annotation

    if-eqz p1, :cond_3

    .line 373
    invoke-virtual {p1}, Ljava/util/HashMap;->size()I

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-ge v0, v2, :cond_0

    return v1

    .line 378
    :cond_0
    invoke-virtual {p1}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lee/cyber/smartid/cryptolib/dto/Run;

    .line 379
    invoke-virtual {v0}, Lee/cyber/smartid/cryptolib/dto/Run;->getLength()J

    move-result-wide v3

    const-wide/16 v5, 0x22

    cmp-long v0, v3, v5

    if-ltz v0, :cond_1

    return v1

    :cond_2
    return v2

    .line 372
    :cond_3
    new-instance p1, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;

    const/16 v0, 0x64

    const-string v1, "The parameter runs can\'t be null!"

    invoke-direct {p1, v0, v1}, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;-><init>(ILjava/lang/String;)V

    throw p1
.end method

.method protected final clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .line 49
    new-instance v0, Ljava/lang/CloneNotSupportedException;

    invoke-direct {v0}, Ljava/lang/CloneNotSupportedException;-><init>()V

    throw v0
.end method

.method public generateRandomNonce()[B
    .locals 2

    .line 106
    invoke-virtual {p0}, Lee/cyber/smartid/cryptolib/impl/RandomGenerationOpImpl;->getRandom()Ljava/security/SecureRandom;

    move-result-object v0

    const/16 v1, 0x8

    .line 107
    new-array v1, v1, [B

    .line 108
    invoke-virtual {v0, v1}, Ljava/security/SecureRandom;->nextBytes([B)V

    return-object v1
.end method

.method public getRandom()Ljava/security/SecureRandom;
    .locals 4
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "TrulyRandom"
        }
    .end annotation

    const/16 v0, 0x6c

    :try_start_0
    const-string v1, "SHA1PRNG"

    .line 84
    invoke-static {v1}, Ljava/security/SecureRandom;->getInstance(Ljava/lang/String;)Ljava/security/SecureRandom;

    move-result-object v1
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v1, :cond_0

    return-object v1

    .line 97
    :cond_0
    new-instance v1, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;

    const-string v2, "Failed to create a random number generator for SHA1PRNG"

    invoke-direct {v1, v0, v2}, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;-><init>(ILjava/lang/String;)V

    throw v1

    :catch_0
    move-exception v1

    .line 90
    new-instance v2, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v0, v3}, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;-><init>(ILjava/lang/String;)V

    .line 91
    invoke-virtual {v1}, Ljava/lang/Exception;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v0

    invoke-virtual {v2, v0}, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;->setStackTrace([Ljava/lang/StackTraceElement;)V

    .line 92
    throw v2

    :catch_1
    move-exception v1

    .line 86
    new-instance v2, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;

    invoke-virtual {v1}, Ljava/security/NoSuchAlgorithmException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v0, v3}, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;-><init>(ILjava/lang/String;)V

    .line 87
    invoke-virtual {v1}, Ljava/security/NoSuchAlgorithmException;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v0

    invoke-virtual {v2, v0}, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;->setStackTrace([Ljava/lang/StackTraceElement;)V

    .line 88
    throw v2
.end method

.method public testCSPRNGQuality(I)[Lee/cyber/smartid/cryptolib/dto/TestResult;
    .locals 14

    const/4 v0, 0x1

    if-lt p1, v0, :cond_8

    const/4 v1, 0x4

    .line 121
    new-array v2, v1, [Lee/cyber/smartid/cryptolib/dto/TestResult;

    .line 122
    new-instance v3, Lee/cyber/smartid/cryptolib/dto/TestResult;

    const-string v4, "FIPS 140-1 Monobit test"

    invoke-direct {v3, v4}, Lee/cyber/smartid/cryptolib/dto/TestResult;-><init>(Ljava/lang/String;)V

    const/4 v4, 0x0

    aput-object v3, v2, v4

    .line 123
    new-instance v3, Lee/cyber/smartid/cryptolib/dto/TestResult;

    const-string v5, "FIPS 140-1 Poker test"

    invoke-direct {v3, v5}, Lee/cyber/smartid/cryptolib/dto/TestResult;-><init>(Ljava/lang/String;)V

    aput-object v3, v2, v0

    .line 124
    new-instance v3, Lee/cyber/smartid/cryptolib/dto/TestResult;

    const-string v5, "FIPS 140-1 Runs test"

    invoke-direct {v3, v5}, Lee/cyber/smartid/cryptolib/dto/TestResult;-><init>(Ljava/lang/String;)V

    const/4 v5, 0x2

    aput-object v3, v2, v5

    .line 125
    new-instance v3, Lee/cyber/smartid/cryptolib/dto/TestResult;

    const-string v6, "FIPS 140-1 Long Runs test"

    invoke-direct {v3, v6}, Lee/cyber/smartid/cryptolib/dto/TestResult;-><init>(Ljava/lang/String;)V

    const/4 v6, 0x3

    aput-object v3, v2, v6

    .line 128
    invoke-virtual {p0}, Lee/cyber/smartid/cryptolib/impl/RandomGenerationOpImpl;->getRandom()Ljava/security/SecureRandom;

    move-result-object v3

    const/4 v7, 0x0

    :goto_0
    if-ge v7, p1, :cond_7

    .line 132
    invoke-static {}, Ljava/lang/Thread;->interrupted()Z

    move-result v8

    if-nez v8, :cond_6

    const/16 v8, 0x4e20

    .line 137
    invoke-virtual {p0, v3, v8}, Lee/cyber/smartid/cryptolib/impl/RandomGenerationOpImpl;->a(Ljava/security/SecureRandom;I)[Z

    move-result-object v8

    .line 140
    invoke-virtual {p0, v8}, Lee/cyber/smartid/cryptolib/impl/RandomGenerationOpImpl;->a([Z)I

    move-result v9

    const/16 v10, 0x25b6

    if-ge v10, v9, :cond_1

    const/16 v10, 0x286a

    if-lt v9, v10, :cond_0

    goto :goto_1

    .line 145
    :cond_0
    aget-object v9, v2, v4

    invoke-virtual {v9}, Lee/cyber/smartid/cryptolib/dto/TestResult;->incrementSuccessCount()V

    goto :goto_2

    .line 143
    :cond_1
    :goto_1
    aget-object v9, v2, v4

    invoke-virtual {v9}, Lee/cyber/smartid/cryptolib/dto/TestResult;->incrementFailCount()V

    .line 150
    :goto_2
    invoke-virtual {p0, v8, v1}, Lee/cyber/smartid/cryptolib/impl/RandomGenerationOpImpl;->a([ZI)D

    move-result-wide v9

    const-wide v11, 0x3ff07ae147ae147bL    # 1.03

    cmpg-double v13, v11, v9

    if-gez v13, :cond_3

    const-wide v11, 0x404cb33333333333L    # 57.4

    cmpg-double v13, v9, v11

    if-ltz v13, :cond_2

    goto :goto_3

    .line 155
    :cond_2
    aget-object v9, v2, v0

    invoke-virtual {v9}, Lee/cyber/smartid/cryptolib/dto/TestResult;->incrementSuccessCount()V

    goto :goto_4

    .line 153
    :cond_3
    :goto_3
    aget-object v9, v2, v0

    invoke-virtual {v9}, Lee/cyber/smartid/cryptolib/dto/TestResult;->incrementFailCount()V

    .line 159
    :goto_4
    invoke-virtual {p0, v8}, Lee/cyber/smartid/cryptolib/impl/RandomGenerationOpImpl;->b([Z)Ljava/util/HashMap;

    move-result-object v8

    .line 162
    invoke-virtual {p0, v8}, Lee/cyber/smartid/cryptolib/impl/RandomGenerationOpImpl;->a(Ljava/util/HashMap;)Z

    move-result v9

    if-nez v9, :cond_4

    .line 165
    aget-object v9, v2, v5

    invoke-virtual {v9}, Lee/cyber/smartid/cryptolib/dto/TestResult;->incrementFailCount()V

    goto :goto_5

    .line 167
    :cond_4
    aget-object v9, v2, v5

    invoke-virtual {v9}, Lee/cyber/smartid/cryptolib/dto/TestResult;->incrementSuccessCount()V

    .line 172
    :goto_5
    invoke-virtual {p0, v8}, Lee/cyber/smartid/cryptolib/impl/RandomGenerationOpImpl;->b(Ljava/util/HashMap;)Z

    move-result v8

    if-nez v8, :cond_5

    .line 175
    aget-object v8, v2, v6

    invoke-virtual {v8}, Lee/cyber/smartid/cryptolib/dto/TestResult;->incrementFailCount()V

    goto :goto_6

    .line 177
    :cond_5
    aget-object v8, v2, v6

    invoke-virtual {v8}, Lee/cyber/smartid/cryptolib/dto/TestResult;->incrementSuccessCount()V

    :goto_6
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 133
    :cond_6
    new-instance p1, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;

    const/16 v0, 0x79

    const-string v1, "Execution was interrupted"

    invoke-direct {p1, v0, v1}, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;-><init>(ILjava/lang/String;)V

    throw p1

    :cond_7
    return-object v2

    .line 117
    :cond_8
    new-instance p1, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;

    const/16 v0, 0x76

    const-string v1, "Parameter loopCount can\'t be smaller than 1!"

    invoke-direct {p1, v0, v1}, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;-><init>(ILjava/lang/String;)V

    throw p1
.end method
