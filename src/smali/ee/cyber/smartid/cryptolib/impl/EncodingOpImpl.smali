.class public final Lee/cyber/smartid/cryptolib/impl/EncodingOpImpl;
.super Ljava/lang/Object;

# interfaces
.implements Lee/cyber/smartid/cryptolib/inter/EncodingOp;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method protected final clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .line 44
    new-instance v0, Ljava/lang/CloneNotSupportedException;

    invoke-direct {v0}, Ljava/lang/CloneNotSupportedException;-><init>()V

    throw v0
.end method

.method public decodeBytesFromBase64(Ljava/lang/String;)[B
    .locals 1

    const-string v0, "Input can\'t be null!"

    .line 99
    invoke-static {p1, v0}, Lee/cyber/smartid/cryptolib/util/Util;->throwIfEmpty(Ljava/lang/String;Ljava/lang/String;)V

    .line 100
    invoke-static {p1}, Lorg/a/f/a/a;->a(Ljava/lang/String;)[B

    move-result-object p1

    return-object p1
.end method

.method public decodeDecimalFromBase64(Ljava/lang/String;)Ljava/math/BigInteger;
    .locals 2

    const-string v0, "Input can\'t be null!"

    .line 92
    invoke-static {p1, v0}, Lee/cyber/smartid/cryptolib/util/Util;->throwIfEmpty(Ljava/lang/String;Ljava/lang/String;)V

    .line 93
    new-instance v0, Ljava/math/BigInteger;

    invoke-static {p1}, Lorg/a/f/a/a;->a(Ljava/lang/String;)[B

    move-result-object p1

    const/4 v1, 0x1

    invoke-direct {v0, v1, p1}, Ljava/math/BigInteger;-><init>(I[B)V

    return-object v0
.end method

.method public digestAndEncodeToBase64(Ljava/io/InputStream;Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    const-string v0, "Parameter inputStream can\'t be null!"

    .line 200
    invoke-static {p1, v0}, Lee/cyber/smartid/cryptolib/util/Util;->throwIfNull(Ljava/io/Closeable;Ljava/lang/String;)V

    const-string v0, "Parameter algorithmName can\'t be null or empty!"

    .line 201
    invoke-static {p2, v0}, Lee/cyber/smartid/cryptolib/util/Util;->throwIfEmpty(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 207
    :try_start_0
    invoke-static {p2}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object p2

    .line 208
    new-instance v1, Ljava/security/DigestInputStream;

    invoke-direct {v1, p1, p2}, Ljava/security/DigestInputStream;-><init>(Ljava/io/InputStream;Ljava/security/MessageDigest;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    const/16 v0, 0x800

    .line 209
    :try_start_1
    new-array v0, v0, [B

    .line 211
    :goto_0
    invoke-virtual {v1, v0}, Ljava/security/DigestInputStream;->read([B)I

    move-result v2
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    const/4 v3, -0x1

    if-eq v2, v3, :cond_0

    goto :goto_0

    .line 216
    :cond_0
    invoke-static {v1}, Lee/cyber/smartid/cryptolib/util/Util;->closeClosable(Ljava/io/Closeable;)V

    .line 217
    invoke-static {p1}, Lee/cyber/smartid/cryptolib/util/Util;->closeClosable(Ljava/io/Closeable;)V

    .line 219
    invoke-virtual {p2}, Ljava/security/MessageDigest;->digest()[B

    move-result-object p1

    invoke-virtual {p0, p1}, Lee/cyber/smartid/cryptolib/impl/EncodingOpImpl;->encodeBytesToBase64([B)Ljava/lang/String;

    move-result-object p1

    return-object p1

    :catchall_0
    move-exception p2

    move-object v0, v1

    goto :goto_2

    :catch_0
    move-exception p2

    move-object v0, v1

    goto :goto_1

    :catchall_1
    move-exception p2

    goto :goto_2

    :catch_1
    move-exception p2

    .line 214
    :goto_1
    :try_start_2
    new-instance v1, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;

    const/16 v2, 0x7b

    invoke-virtual {p2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object p2

    invoke-direct {v1, v2, p2}, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;-><init>(ILjava/lang/String;)V

    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 216
    :goto_2
    invoke-static {v0}, Lee/cyber/smartid/cryptolib/util/Util;->closeClosable(Ljava/io/Closeable;)V

    .line 217
    invoke-static {p1}, Lee/cyber/smartid/cryptolib/util/Util;->closeClosable(Ljava/io/Closeable;)V

    .line 218
    throw p2
.end method

.method public digestAndEncodeToBase64(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    if-eqz p1, :cond_0

    const-string v0, "Parameter algorithmName can\'t be empty or null!"

    .line 161
    invoke-static {p2, v0}, Lee/cyber/smartid/cryptolib/util/Util;->throwIfEmpty(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 166
    :try_start_0
    new-instance v1, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v1}, Ljava/io/ByteArrayOutputStream;-><init>()V
    :try_end_0
    .catch Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException; {:try_start_0 .. :try_end_0} :catch_5
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_4
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    .line 167
    :try_start_1
    new-instance v2, Ljava/io/ObjectOutputStream;

    invoke-direct {v2, v1}, Ljava/io/ObjectOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_1
    .catch Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 168
    :try_start_2
    invoke-virtual {v2, p1}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    .line 169
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object p1

    invoke-virtual {p0, p1, p2}, Lee/cyber/smartid/cryptolib/impl/EncodingOpImpl;->digestAndEncodeToBase64([BLjava/lang/String;)Ljava/lang/String;

    move-result-object p1
    :try_end_2
    .catch Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 175
    invoke-static {v1}, Lee/cyber/smartid/cryptolib/util/Util;->closeClosable(Ljava/io/Closeable;)V

    .line 176
    invoke-static {v2}, Lee/cyber/smartid/cryptolib/util/Util;->closeClosable(Ljava/io/Closeable;)V

    return-object p1

    :catchall_0
    move-exception p1

    goto :goto_0

    :catch_0
    move-exception p1

    goto :goto_1

    :catch_1
    move-exception p1

    goto :goto_2

    :catchall_1
    move-exception p1

    move-object v2, v0

    :goto_0
    move-object v0, v1

    goto :goto_5

    :catch_2
    move-exception p1

    move-object v2, v0

    :goto_1
    move-object v0, v1

    goto :goto_3

    :catch_3
    move-exception p1

    move-object v2, v0

    :goto_2
    move-object v0, v1

    goto :goto_4

    :catchall_2
    move-exception p1

    move-object v2, v0

    goto :goto_5

    :catch_4
    move-exception p1

    move-object v2, v0

    .line 173
    :goto_3
    :try_start_3
    new-instance p2, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;

    const/16 v1, 0x7b

    invoke-virtual {p1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, v1, p1}, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;-><init>(ILjava/lang/String;)V

    throw p2

    :catch_5
    move-exception p1

    move-object v2, v0

    .line 171
    :goto_4
    throw p1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    :catchall_3
    move-exception p1

    .line 175
    :goto_5
    invoke-static {v0}, Lee/cyber/smartid/cryptolib/util/Util;->closeClosable(Ljava/io/Closeable;)V

    .line 176
    invoke-static {v2}, Lee/cyber/smartid/cryptolib/util/Util;->closeClosable(Ljava/io/Closeable;)V

    .line 177
    throw p1

    .line 159
    :cond_0
    new-instance p1, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;

    const/16 p2, 0x64

    const-string v0, "Object o can\'t be null!"

    invoke-direct {p1, p2, v0}, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;-><init>(ILjava/lang/String;)V

    throw p1
.end method

.method public digestAndEncodeToBase64([BLjava/lang/String;)Ljava/lang/String;
    .locals 1

    const-string v0, "Parameter data can\'t be null!"

    .line 183
    invoke-static {p1, v0}, Lee/cyber/smartid/cryptolib/util/Util;->throwIfNull(Ljava/io/Serializable;Ljava/lang/String;)V

    const-string v0, "Parameter algorithmName can\'t be null or empty!"

    .line 184
    invoke-static {p2, v0}, Lee/cyber/smartid/cryptolib/util/Util;->throwIfEmpty(Ljava/lang/String;Ljava/lang/String;)V

    .line 189
    :try_start_0
    invoke-static {p2}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object p2

    .line 190
    invoke-virtual {p2, p1}, Ljava/security/MessageDigest;->update([B)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 194
    invoke-virtual {p2}, Ljava/security/MessageDigest;->digest()[B

    move-result-object p1

    invoke-virtual {p0, p1}, Lee/cyber/smartid/cryptolib/impl/EncodingOpImpl;->encodeBytesToBase64([B)Ljava/lang/String;

    move-result-object p1

    return-object p1

    :catch_0
    move-exception p1

    .line 192
    new-instance p2, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;

    const/16 v0, 0x7b

    invoke-virtual {p1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, v0, p1}, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;-><init>(ILjava/lang/String;)V

    throw p2
.end method

.method public encodeBytesToBase64([B)Ljava/lang/String;
    .locals 2

    const-string v0, "The value can\'t be null!"

    .line 128
    invoke-static {p1, v0}, Lee/cyber/smartid/cryptolib/util/Util;->throwIfNull(Ljava/io/Serializable;Ljava/lang/String;)V

    .line 129
    new-instance v0, Ljava/lang/String;

    invoke-static {p1}, Lorg/a/f/a/a;->b([B)[B

    move-result-object p1

    sget-object v1, Lee/cyber/smartid/cryptolib/CryptoLib;->DEFAULT_ENCODING:Ljava/nio/charset/Charset;

    invoke-direct {v0, p1, v1}, Ljava/lang/String;-><init>([BLjava/nio/charset/Charset;)V

    return-object v0
.end method

.method public encodeDecimalToBase64(Ljava/math/BigInteger;)Ljava/lang/String;
    .locals 2

    const-string v0, "Input can\'t be null!"

    .line 73
    invoke-static {p1, v0}, Lee/cyber/smartid/cryptolib/util/Util;->throwIfNull(Ljava/math/BigInteger;Ljava/lang/String;)V

    .line 74
    new-instance v0, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lee/cyber/smartid/cryptolib/impl/EncodingOpImpl;->encodePositiveBigIntegerAsBytes(Ljava/math/BigInteger;)[B

    move-result-object p1

    invoke-static {p1}, Lorg/a/f/a/a;->b([B)[B

    move-result-object p1

    sget-object v1, Lee/cyber/smartid/cryptolib/CryptoLib;->DEFAULT_ENCODING:Ljava/nio/charset/Charset;

    invoke-direct {v0, p1, v1}, Ljava/lang/String;-><init>([BLjava/nio/charset/Charset;)V

    return-object v0
.end method

.method public encodeI2OSP(Ljava/math/BigInteger;I)[B
    .locals 4

    const-string v0, "The integer x can\'t be null!"

    .line 136
    invoke-static {p1, v0}, Lee/cyber/smartid/cryptolib/util/Util;->throwIfNull(Ljava/math/BigInteger;Ljava/lang/String;)V

    .line 137
    sget-object v0, Ljava/math/BigInteger;->ZERO:Ljava/math/BigInteger;

    invoke-virtual {p1, v0}, Ljava/math/BigInteger;->compareTo(Ljava/math/BigInteger;)I

    move-result v0

    const/16 v1, 0x76

    const/4 v2, -0x1

    if-eq v0, v2, :cond_2

    if-ltz p2, :cond_1

    .line 143
    new-instance v0, Ljava/math/BigInteger;

    const-string v1, "256"

    invoke-direct {v0, v1}, Ljava/math/BigInteger;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/math/BigInteger;->pow(I)Ljava/math/BigInteger;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/math/BigInteger;->compareTo(Ljava/math/BigInteger;)I

    move-result v0

    if-gez v0, :cond_0

    .line 150
    invoke-virtual {p0, p1}, Lee/cyber/smartid/cryptolib/impl/EncodingOpImpl;->encodePositiveBigIntegerAsBytes(Ljava/math/BigInteger;)[B

    move-result-object p1

    invoke-virtual {p0, p1, p2}, Lee/cyber/smartid/cryptolib/impl/EncodingOpImpl;->padToSize([BI)[B

    move-result-object p1

    return-object p1

    .line 145
    :cond_0
    new-instance v0, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;

    const/16 v1, 0x75

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Integer too large - "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/math/BigInteger;->bitLength()I

    move-result p1

    div-int/lit8 p1, p1, 0x8

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, " bytes > "

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, " bytes"

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, v1, p1}, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 142
    :cond_1
    new-instance p1, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "The length xLen of the resulting octet string can\'t be negative - "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, v1, p2}, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;-><init>(ILjava/lang/String;)V

    throw p1

    .line 139
    :cond_2
    new-instance p2, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "The integer x needs to be non-negative - "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, v1, p1}, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;-><init>(ILjava/lang/String;)V

    throw p2
.end method

.method public encodePositiveBigIntegerAsBytes(Ljava/math/BigInteger;)[B
    .locals 2

    const-string v0, "Candidate can\'t be null!"

    .line 80
    invoke-static {p1, v0}, Lee/cyber/smartid/cryptolib/util/Util;->throwIfNull(Ljava/math/BigInteger;Ljava/lang/String;)V

    .line 81
    invoke-virtual {p1}, Ljava/math/BigInteger;->toByteArray()[B

    move-result-object p1

    if-eqz p1, :cond_0

    .line 82
    array-length v0, p1

    const/4 v1, 0x1

    if-le v0, v1, :cond_0

    const/4 v0, 0x0

    aget-byte v0, p1, v0

    if-nez v0, :cond_0

    .line 83
    array-length v0, p1

    invoke-static {p1, v1, v0}, Ljava/util/Arrays;->copyOfRange([BII)[B

    move-result-object p1

    :cond_0
    return-object p1
.end method

.method public padToSize([BI)[B
    .locals 4

    const-string v0, "The Input array can\'t be null!"

    .line 106
    invoke-static {p1, v0}, Lee/cyber/smartid/cryptolib/util/Util;->throwIfNull(Ljava/io/Serializable;Ljava/lang/String;)V

    if-ltz p2, :cond_3

    .line 110
    array-length v0, p1

    if-ge v0, p2, :cond_1

    .line 111
    array-length v0, p1

    sub-int/2addr p2, v0

    new-array p2, p2, [B

    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 112
    :goto_0
    array-length v2, p2

    if-ge v1, v2, :cond_0

    .line 113
    aput-byte v0, p2, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 115
    :cond_0
    array-length v1, p2

    array-length v2, p1

    add-int/2addr v1, v2

    new-array v1, v1, [B

    .line 116
    array-length v2, p2

    invoke-static {p2, v0, v1, v0, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 117
    array-length p2, p2

    array-length v2, p1

    invoke-static {p1, v0, v1, p2, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v1

    .line 119
    :cond_1
    array-length v0, p1

    if-gt v0, p2, :cond_2

    return-object p1

    .line 120
    :cond_2
    new-instance v0, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;

    const/16 v1, 0x75

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Input size "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    array-length p1, p1

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, " is larger than expected size "

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, v1, p1}, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 108
    :cond_3
    new-instance p1, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;

    const/16 v0, 0x76

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "The expectedSize value "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p2, " can\'t be negative!"

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, v0, p2}, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;-><init>(ILjava/lang/String;)V

    throw p1
.end method

.method public parseAndExtractHeaderFromJWS(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    const-string v0, "The parameter jws can\'t be empty!"

    .line 243
    invoke-static {p1, v0}, Lee/cyber/smartid/cryptolib/util/Util;->throwIfEmpty(Ljava/lang/String;Ljava/lang/String;)V

    const/16 v0, 0x7a

    .line 246
    :try_start_0
    invoke-static {p1}, Lcom/a/a/r;->b(Ljava/lang/String;)Lcom/a/a/r;

    move-result-object p1

    .line 247
    invoke-virtual {p1}, Lcom/a/a/r;->c()Lcom/a/a/q;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 251
    invoke-virtual {p1}, Lcom/a/a/e;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 249
    :cond_0
    new-instance p1, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;

    const-string v1, "Payload is missing!"

    invoke-direct {p1, v0, v1}, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;-><init>(ILjava/lang/String;)V

    throw p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception p1

    .line 253
    new-instance v1, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;

    invoke-virtual {p1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v1, v0, p1}, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;-><init>(ILjava/lang/String;)V

    throw v1
.end method

.method public parseAndExtractPayloadFromJWS(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    const-string v0, "The parameter jws can\'t be empty!"

    .line 226
    invoke-static {p1, v0}, Lee/cyber/smartid/cryptolib/util/Util;->throwIfEmpty(Ljava/lang/String;Ljava/lang/String;)V

    const/16 v0, 0x7a

    .line 229
    :try_start_0
    invoke-static {p1}, Lcom/a/a/r;->b(Ljava/lang/String;)Lcom/a/a/r;

    move-result-object p1

    .line 230
    invoke-virtual {p1}, Lcom/a/a/r;->a()Lcom/a/a/v;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 234
    invoke-virtual {p1}, Lcom/a/a/v;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 232
    :cond_0
    new-instance p1, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;

    const-string v1, "Payload is missing!"

    invoke-direct {p1, v0, v1}, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;-><init>(ILjava/lang/String;)V

    throw p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception p1

    .line 236
    new-instance v1, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;

    invoke-virtual {p1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v1, v0, p1}, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;-><init>(ILjava/lang/String;)V

    throw v1
.end method
