.class public final Lee/cyber/smartid/cryptolib/impl/SigningOpImpl;
.super Ljava/lang/Object;

# interfaces
.implements Lee/cyber/smartid/cryptolib/inter/SigningOp;


# instance fields
.field private a:Lee/cyber/smartid/cryptolib/inter/EncodingOp;

.field private b:Lee/cyber/smartid/cryptolib/inter/CryptoOp;


# direct methods
.method public constructor <init>(Lee/cyber/smartid/cryptolib/inter/EncodingOp;Lee/cyber/smartid/cryptolib/inter/CryptoOp;)V
    .locals 0

    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    iput-object p1, p0, Lee/cyber/smartid/cryptolib/impl/SigningOpImpl;->a:Lee/cyber/smartid/cryptolib/inter/EncodingOp;

    .line 50
    iput-object p2, p0, Lee/cyber/smartid/cryptolib/impl/SigningOpImpl;->b:Lee/cyber/smartid/cryptolib/inter/CryptoOp;

    return-void
.end method


# virtual methods
.method a(Ljava/math/BigInteger;I)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;
        }
    .end annotation

    if-eqz p1, :cond_0

    .line 124
    sget-object v0, Ljava/math/BigInteger;->ZERO:Ljava/math/BigInteger;

    invoke-virtual {p1, v0}, Ljava/math/BigInteger;->compareTo(Ljava/math/BigInteger;)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    new-instance v0, Ljava/math/BigInteger;

    const-string v1, "2"

    invoke-direct {v0, v1}, Ljava/math/BigInteger;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/math/BigInteger;->pow(I)Ljava/math/BigInteger;

    move-result-object p2

    invoke-virtual {p1, p2}, Ljava/math/BigInteger;->compareTo(Ljava/math/BigInteger;)I

    move-result p1

    const/4 p2, -0x1

    if-ne p1, p2, :cond_0

    return-void

    .line 131
    :cond_0
    new-instance p1, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;

    const/16 p2, 0x72

    const-string v0, "Message representative out of range"

    invoke-direct {p1, p2, v0}, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;-><init>(ILjava/lang/String;)V

    throw p1
.end method

.method public calculateSignatureShare(Ljava/lang/String;Ljava/math/BigInteger;ILjava/lang/String;Ljava/math/BigInteger;)Ljava/math/BigInteger;
    .locals 1

    const-string v0, "m can\'t be empty"

    .line 100
    invoke-static {p1, v0}, Lee/cyber/smartid/cryptolib/util/Util;->throwIfEmpty(Ljava/lang/String;Ljava/lang/String;)V

    .line 101
    invoke-static {p4, v0}, Lee/cyber/smartid/cryptolib/util/Util;->throwIfEmpty(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "dPrime can\'t be empty"

    .line 102
    invoke-static {p5, v0}, Lee/cyber/smartid/cryptolib/util/Util;->throwIfNull(Ljava/math/BigInteger;Ljava/lang/String;)V

    const-string v0, "n1 can\'t be empty"

    .line 103
    invoke-static {p2, v0}, Lee/cyber/smartid/cryptolib/util/Util;->throwIfNull(Ljava/math/BigInteger;Ljava/lang/String;)V

    .line 106
    new-instance v0, Ljava/math/BigInteger;

    invoke-virtual {p0, p1, p3, p4}, Lee/cyber/smartid/cryptolib/impl/SigningOpImpl;->createPaddedMessage(Ljava/lang/String;ILjava/lang/String;)[B

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/math/BigInteger;-><init>([B)V

    .line 108
    invoke-virtual {p0, v0, p3}, Lee/cyber/smartid/cryptolib/impl/SigningOpImpl;->a(Ljava/math/BigInteger;I)V

    .line 111
    invoke-virtual {v0, p5, p2}, Ljava/math/BigInteger;->modPow(Ljava/math/BigInteger;Ljava/math/BigInteger;)Ljava/math/BigInteger;

    move-result-object p1

    return-object p1
.end method

.method protected final clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .line 61
    new-instance v0, Ljava/lang/CloneNotSupportedException;

    invoke-direct {v0}, Ljava/lang/CloneNotSupportedException;-><init>()V

    throw v0
.end method

.method public createPaddedMessage(Ljava/lang/String;ILjava/lang/String;)[B
    .locals 5

    const/16 v0, 0x8

    add-int/2addr p2, v0

    const/4 v1, 0x1

    sub-int/2addr p2, v1

    .line 148
    :try_start_0
    div-int/2addr p2, v0

    const/4 v2, 0x0

    .line 149
    invoke-static {p1, v2}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object p1

    .line 150
    invoke-static {p3}, Lorg/a/c/a/a/a;->a(Ljava/lang/String;)Lorg/a/b/f;

    move-result-object v3

    invoke-interface {v3}, Lorg/a/b/f;->b()I

    move-result v3

    .line 151
    array-length v4, p1

    if-ne v4, v3, :cond_2

    .line 155
    new-instance v3, Lorg/a/a/k/b;

    new-instance v4, Lorg/a/e/a;

    invoke-direct {v4}, Lorg/a/e/a;-><init>()V

    invoke-virtual {v4, p3}, Lorg/a/e/a;->a(Ljava/lang/String;)Lorg/a/a/k/a;

    move-result-object p3

    invoke-direct {v3, p3, p1}, Lorg/a/a/k/b;-><init>(Lorg/a/a/k/a;[B)V

    invoke-virtual {v3}, Lorg/a/a/k/b;->b()[B

    move-result-object p1

    .line 156
    array-length p3, p1

    add-int/lit8 v3, p3, 0xb

    if-lt p2, v3, :cond_1

    sub-int/2addr p2, p3

    add-int/lit8 p2, p2, -0x3

    if-lt p2, v0, :cond_0

    .line 164
    new-array p2, p2, [B

    const/4 p3, -0x1

    .line 165
    invoke-static {p2, p3}, Ljava/util/Arrays;->fill([BB)V

    .line 166
    new-instance p3, Ljava/io/ByteArrayOutputStream;

    invoke-direct {p3}, Ljava/io/ByteArrayOutputStream;-><init>()V

    const/4 v0, 0x2

    .line 167
    new-array v0, v0, [B

    aput-byte v2, v0, v2

    aput-byte v1, v0, v1

    .line 168
    new-array v1, v1, [B

    aput-byte v2, v1, v2

    .line 169
    invoke-virtual {p3, v0}, Ljava/io/ByteArrayOutputStream;->write([B)V

    .line 170
    invoke-virtual {p3, p2}, Ljava/io/ByteArrayOutputStream;->write([B)V

    .line 171
    invoke-virtual {p3, v1}, Ljava/io/ByteArrayOutputStream;->write([B)V

    .line 172
    invoke-virtual {p3, p1}, Ljava/io/ByteArrayOutputStream;->write([B)V

    .line 173
    invoke-virtual {p3}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object p1

    return-object p1

    .line 162
    :cond_0
    new-instance p1, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;

    const/16 p2, 0x6f

    const-string p3, "Padding too short"

    invoke-direct {p1, p2, p3}, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;-><init>(ILjava/lang/String;)V

    throw p1

    .line 158
    :cond_1
    new-instance p1, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;

    const/16 p2, 0x6e

    const-string p3, "Intended encoded message length too short"

    invoke-direct {p1, p2, p3}, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;-><init>(ILjava/lang/String;)V

    throw p1

    .line 152
    :cond_2
    new-instance p1, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;

    const/16 p2, 0x71

    const-string v0, "Incorrect hash length for %1s algorithm"

    new-array v1, v1, [Ljava/lang/Object;

    aput-object p3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p3

    invoke-direct {p1, p2, p3}, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;-><init>(ILjava/lang/String;)V

    throw p1
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception p1

    .line 175
    new-instance p2, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;

    const/16 p3, 0x70

    invoke-virtual {p1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p3, p1}, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;-><init>(ILjava/lang/String;)V

    throw p2
.end method

.method public generateVerificationCode(Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    const-string v0, "Input hash can\'t be empty"

    .line 182
    invoke-static {p1, v0}, Lee/cyber/smartid/cryptolib/util/Util;->throwIfEmpty(Ljava/lang/String;Ljava/lang/String;)V

    .line 185
    :try_start_0
    iget-object v0, p0, Lee/cyber/smartid/cryptolib/impl/SigningOpImpl;->a:Lee/cyber/smartid/cryptolib/inter/EncodingOp;

    invoke-interface {v0, p1}, Lee/cyber/smartid/cryptolib/inter/EncodingOp;->decodeBytesFromBase64(Ljava/lang/String;)[B

    move-result-object p1

    const-string v0, "SHA256"

    .line 187
    invoke-static {v0}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v0

    .line 188
    invoke-virtual {v0, p1}, Ljava/security/MessageDigest;->digest([B)[B

    move-result-object p1

    const/4 v0, 0x2

    .line 190
    new-array v1, v0, [B

    .line 191
    array-length v2, p1

    sub-int/2addr v2, v0

    const/4 v0, 0x0

    array-length v3, v1

    invoke-static {p1, v2, v1, v0, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 193
    invoke-static {v1}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object p1

    .line 194
    sget-object v0, Ljava/nio/ByteOrder;->BIG_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 195
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->getShort()S

    move-result p1

    const v0, 0xffff

    and-int/2addr p1, v0

    .line 197
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "0000"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 199
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x4

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p1
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    .line 201
    :catch_0
    new-instance p1, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;

    const/16 v0, 0x64

    const-string v1, "NoSuchAlgorithmException thrown for SHA256"

    invoke-direct {p1, v0, v1}, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;-><init>(ILjava/lang/String;)V

    throw p1
.end method

.method public sign(Lee/cyber/smartid/cryptolib/dto/ClientShare;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 7

    .line 208
    iget-object v0, p0, Lee/cyber/smartid/cryptolib/impl/SigningOpImpl;->b:Lee/cyber/smartid/cryptolib/inter/CryptoOp;

    invoke-interface {v0, p1, p4}, Lee/cyber/smartid/cryptolib/inter/CryptoOp;->decryptKey(Lee/cyber/smartid/cryptolib/dto/ClientShare;Ljava/lang/String;)Ljava/math/BigInteger;

    move-result-object v6

    .line 209
    iget-object p1, p0, Lee/cyber/smartid/cryptolib/impl/SigningOpImpl;->a:Lee/cyber/smartid/cryptolib/inter/EncodingOp;

    invoke-interface {p1, p2}, Lee/cyber/smartid/cryptolib/inter/EncodingOp;->decodeDecimalFromBase64(Ljava/lang/String;)Ljava/math/BigInteger;

    move-result-object v3

    move-object v1, p0

    move-object v2, p5

    move v4, p3

    move-object v5, p6

    .line 210
    invoke-virtual/range {v1 .. v6}, Lee/cyber/smartid/cryptolib/impl/SigningOpImpl;->calculateSignatureShare(Ljava/lang/String;Ljava/math/BigInteger;ILjava/lang/String;Ljava/math/BigInteger;)Ljava/math/BigInteger;

    move-result-object p1

    .line 211
    iget-object p2, p0, Lee/cyber/smartid/cryptolib/impl/SigningOpImpl;->a:Lee/cyber/smartid/cryptolib/inter/EncodingOp;

    invoke-interface {p2, p1}, Lee/cyber/smartid/cryptolib/inter/EncodingOp;->encodeDecimalToBase64(Ljava/math/BigInteger;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method
