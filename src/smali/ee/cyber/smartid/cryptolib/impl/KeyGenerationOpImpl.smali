.class public final Lee/cyber/smartid/cryptolib/impl/KeyGenerationOpImpl;
.super Ljava/lang/Object;

# interfaces
.implements Lee/cyber/smartid/cryptolib/inter/KeyGenerationOp;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lee/cyber/smartid/cryptolib/impl/KeyGenerationOpImpl$GenerationParams;
    }
.end annotation


# instance fields
.field private final a:Lee/cyber/smartid/cryptolib/inter/RandomGenerationOp;

.field private final b:Lee/cyber/smartid/cryptolib/inter/EncodingOp;


# direct methods
.method public constructor <init>(Lee/cyber/smartid/cryptolib/inter/EncodingOp;Lee/cyber/smartid/cryptolib/inter/RandomGenerationOp;)V
    .locals 1

    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/16 v0, 0x65

    if-eqz p1, :cond_1

    if-eqz p2, :cond_0

    .line 60
    iput-object p1, p0, Lee/cyber/smartid/cryptolib/impl/KeyGenerationOpImpl;->b:Lee/cyber/smartid/cryptolib/inter/EncodingOp;

    .line 61
    iput-object p2, p0, Lee/cyber/smartid/cryptolib/impl/KeyGenerationOpImpl;->a:Lee/cyber/smartid/cryptolib/inter/RandomGenerationOp;

    return-void

    .line 58
    :cond_0
    new-instance p1, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;

    const-string p2, "RandomGenerationOp is required!"

    invoke-direct {p1, v0, p2}, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;-><init>(ILjava/lang/String;)V

    throw p1

    .line 56
    :cond_1
    new-instance p1, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;

    const-string p2, "EncodingOpImpl is required!"

    invoke-direct {p1, v0, p2}, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;-><init>(ILjava/lang/String;)V

    throw p1
.end method

.method private a([BLjava/math/BigInteger;Ljava/math/BigInteger;)Ljava/math/BigInteger;
    .locals 3

    .line 483
    iget-object v0, p0, Lee/cyber/smartid/cryptolib/impl/KeyGenerationOpImpl;->a:Lee/cyber/smartid/cryptolib/inter/RandomGenerationOp;

    invoke-interface {v0}, Lee/cyber/smartid/cryptolib/inter/RandomGenerationOp;->getRandom()Ljava/security/SecureRandom;

    move-result-object v0

    const/4 v1, 0x0

    :goto_0
    if-eqz v1, :cond_1

    .line 489
    sget-object v2, Ljava/math/BigInteger;->ZERO:Ljava/math/BigInteger;

    invoke-virtual {v2, v1}, Ljava/math/BigInteger;->compareTo(Ljava/math/BigInteger;)I

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v1, p2}, Ljava/math/BigInteger;->compareTo(Ljava/math/BigInteger;)I

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v1, p3}, Ljava/math/BigInteger;->compareTo(Ljava/math/BigInteger;)I

    move-result v2

    if-nez v2, :cond_0

    goto :goto_1

    :cond_0
    return-object v1

    .line 490
    :cond_1
    :goto_1
    invoke-virtual {v0, p1}, Ljava/security/SecureRandom;->nextBytes([B)V

    .line 491
    new-instance v1, Ljava/math/BigInteger;

    const/4 v2, 0x1

    invoke-direct {v1, v2, p1}, Ljava/math/BigInteger;-><init>(I[B)V

    goto :goto_0
.end method

.method private a(Ljava/math/BigInteger;I)Z
    .locals 1

    .line 345
    invoke-static {p1}, Lorg/a/d/a;->a(Ljava/math/BigInteger;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lee/cyber/smartid/cryptolib/impl/KeyGenerationOpImpl;->a:Lee/cyber/smartid/cryptolib/inter/RandomGenerationOp;

    invoke-interface {v0}, Lee/cyber/smartid/cryptolib/inter/RandomGenerationOp;->getRandom()Ljava/security/SecureRandom;

    move-result-object v0

    invoke-static {p1, v0, p2}, Lorg/a/d/a;->a(Ljava/math/BigInteger;Ljava/security/SecureRandom;I)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method private a(Ljava/math/BigInteger;Ljava/math/BigInteger;ILjava/math/BigInteger;I)[Ljava/math/BigInteger;
    .locals 18

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    .line 359
    new-instance v3, Ljava/math/BigInteger;

    const-string v4, "2"

    invoke-direct {v3, v4}, Ljava/math/BigInteger;-><init>(Ljava/lang/String;)V

    .line 361
    new-instance v4, Ljava/math/BigInteger;

    const-string v5, "1414213562373095048801688724209698078569671875376948073176679737990732478462107038850387534327641572735013846230912297024924836055850737212644121497099935831413222665927505592755799950501152782060571470109559971605970274534596862014728517418640889198609552329230484308714321450839762603627995251407989687253396546331808829640620615258352395054745750287759961729835575220337531857011354374603408498847160386899970699004815030544027790316454247823068492936918621580578463111596668713013015618568987237235288509264861249497715421833420428568606014682472077143585487415565706967765372022648544701585880162075847492265722600208558446652145839889394437092659180031138824646815708263010059485870400318648034219489727829064104507263688131373985525611732204024509122770022694112757362728049573810896750401836986836845072579936472906076299694138047565482372899718032680247442062926912485905218100445984215059112024944134172853147810580360337107730918286931471017111168391658172688941975871658215212822951848847208969463386289156288276595263514054226765323969461751129160240871"

    invoke-direct {v4, v5}, Ljava/math/BigInteger;-><init>(Ljava/lang/String;)V

    .line 363
    new-instance v5, Ljava/math/BigInteger;

    const-string v6, "10"

    invoke-direct {v5, v6}, Ljava/math/BigInteger;-><init>(Ljava/lang/String;)V

    const/16 v6, 0x429

    invoke-virtual {v5, v6}, Ljava/math/BigInteger;->pow(I)Ljava/math/BigInteger;

    move-result-object v5

    .line 368
    invoke-virtual/range {p1 .. p1}, Ljava/math/BigInteger;->bitLength()I

    move-result v6

    invoke-virtual/range {p2 .. p2}, Ljava/math/BigInteger;->bitLength()I

    move-result v7

    add-int/2addr v6, v7

    .line 375
    div-int/lit8 v7, p3, 0x2

    add-int/lit8 v8, v7, -0x1

    invoke-static {v8}, Ljava/lang/Integer;->numberOfLeadingZeros(I)I

    move-result v9

    rsub-int/lit8 v9, v9, 0x20

    sub-int v9, v7, v9

    add-int/lit8 v9, v9, -0x6

    const/4 v10, 0x0

    if-le v6, v9, :cond_0

    return-object v10

    .line 379
    :cond_0
    invoke-virtual {v1, v3}, Ljava/math/BigInteger;->multiply(Ljava/math/BigInteger;)Ljava/math/BigInteger;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/math/BigInteger;->gcd(Ljava/math/BigInteger;)Ljava/math/BigInteger;

    move-result-object v6

    sget-object v9, Ljava/math/BigInteger;->ONE:Ljava/math/BigInteger;

    invoke-virtual {v6, v9}, Ljava/math/BigInteger;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_1

    return-object v10

    .line 385
    :cond_1
    invoke-virtual {v1, v3}, Ljava/math/BigInteger;->multiply(Ljava/math/BigInteger;)Ljava/math/BigInteger;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/math/BigInteger;->modInverse(Ljava/math/BigInteger;)Ljava/math/BigInteger;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/math/BigInteger;->multiply(Ljava/math/BigInteger;)Ljava/math/BigInteger;

    move-result-object v6

    .line 386
    invoke-virtual {v1, v3}, Ljava/math/BigInteger;->multiply(Ljava/math/BigInteger;)Ljava/math/BigInteger;

    move-result-object v9

    invoke-virtual {v9, v2}, Ljava/math/BigInteger;->modInverse(Ljava/math/BigInteger;)Ljava/math/BigInteger;

    move-result-object v9

    invoke-virtual {v1, v3}, Ljava/math/BigInteger;->multiply(Ljava/math/BigInteger;)Ljava/math/BigInteger;

    move-result-object v11

    invoke-virtual {v9, v11}, Ljava/math/BigInteger;->multiply(Ljava/math/BigInteger;)Ljava/math/BigInteger;

    move-result-object v9

    .line 387
    invoke-virtual {v6, v9}, Ljava/math/BigInteger;->subtract(Ljava/math/BigInteger;)Ljava/math/BigInteger;

    move-result-object v6

    .line 390
    invoke-virtual {v1, v3}, Ljava/math/BigInteger;->multiply(Ljava/math/BigInteger;)Ljava/math/BigInteger;

    move-result-object v9

    invoke-virtual {v6, v9}, Ljava/math/BigInteger;->mod(Ljava/math/BigInteger;)Ljava/math/BigInteger;

    move-result-object v9

    sget-object v11, Ljava/math/BigInteger;->ONE:Ljava/math/BigInteger;

    invoke-virtual {v9, v11}, Ljava/math/BigInteger;->compareTo(Ljava/math/BigInteger;)I

    move-result v9

    if-nez v9, :cond_9

    sget-object v9, Ljava/math/BigInteger;->ONE:Ljava/math/BigInteger;

    invoke-virtual {v6, v9}, Ljava/math/BigInteger;->add(Ljava/math/BigInteger;)Ljava/math/BigInteger;

    move-result-object v9

    invoke-virtual {v9, v2}, Ljava/math/BigInteger;->mod(Ljava/math/BigInteger;)Ljava/math/BigInteger;

    move-result-object v9

    sget-object v11, Ljava/math/BigInteger;->ZERO:Ljava/math/BigInteger;

    invoke-virtual {v9, v11}, Ljava/math/BigInteger;->compareTo(Ljava/math/BigInteger;)I

    move-result v9

    if-eqz v9, :cond_2

    goto/16 :goto_2

    .line 403
    :cond_2
    :goto_0
    invoke-virtual {v3, v8}, Ljava/math/BigInteger;->pow(I)Ljava/math/BigInteger;

    move-result-object v9

    invoke-virtual {v9, v4}, Ljava/math/BigInteger;->multiply(Ljava/math/BigInteger;)Ljava/math/BigInteger;

    move-result-object v9

    invoke-virtual {v9, v5}, Ljava/math/BigInteger;->divide(Ljava/math/BigInteger;)Ljava/math/BigInteger;

    move-result-object v9

    .line 404
    invoke-virtual {v3, v7}, Ljava/math/BigInteger;->pow(I)Ljava/math/BigInteger;

    move-result-object v11

    sget-object v12, Ljava/math/BigInteger;->ONE:Ljava/math/BigInteger;

    invoke-virtual {v11, v12}, Ljava/math/BigInteger;->subtract(Ljava/math/BigInteger;)Ljava/math/BigInteger;

    move-result-object v11

    .line 405
    iget-object v12, v0, Lee/cyber/smartid/cryptolib/impl/KeyGenerationOpImpl;->a:Lee/cyber/smartid/cryptolib/inter/RandomGenerationOp;

    invoke-interface {v12}, Lee/cyber/smartid/cryptolib/inter/RandomGenerationOp;->getRandom()Ljava/security/SecureRandom;

    move-result-object v12

    invoke-static {v9, v11, v12}, Lorg/a/f/b;->a(Ljava/math/BigInteger;Ljava/math/BigInteger;Ljava/security/SecureRandom;)Ljava/math/BigInteger;

    move-result-object v9

    .line 408
    invoke-virtual {v6, v9}, Ljava/math/BigInteger;->subtract(Ljava/math/BigInteger;)Ljava/math/BigInteger;

    move-result-object v11

    invoke-virtual {v3, v1}, Ljava/math/BigInteger;->multiply(Ljava/math/BigInteger;)Ljava/math/BigInteger;

    move-result-object v12

    invoke-virtual {v12, v2}, Ljava/math/BigInteger;->multiply(Ljava/math/BigInteger;)Ljava/math/BigInteger;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/math/BigInteger;->mod(Ljava/math/BigInteger;)Ljava/math/BigInteger;

    move-result-object v11

    invoke-virtual {v9, v11}, Ljava/math/BigInteger;->add(Ljava/math/BigInteger;)Ljava/math/BigInteger;

    move-result-object v11

    .line 411
    sget-object v12, Ljava/math/BigInteger;->ONE:Ljava/math/BigInteger;

    invoke-virtual {v11, v12}, Ljava/math/BigInteger;->subtract(Ljava/math/BigInteger;)Ljava/math/BigInteger;

    move-result-object v12

    invoke-virtual {v12, v1}, Ljava/math/BigInteger;->mod(Ljava/math/BigInteger;)Ljava/math/BigInteger;

    move-result-object v12

    sget-object v13, Ljava/math/BigInteger;->ZERO:Ljava/math/BigInteger;

    invoke-virtual {v12, v13}, Ljava/math/BigInteger;->compareTo(Ljava/math/BigInteger;)I

    move-result v12

    if-nez v12, :cond_8

    sget-object v12, Ljava/math/BigInteger;->ONE:Ljava/math/BigInteger;

    invoke-virtual {v11, v12}, Ljava/math/BigInteger;->add(Ljava/math/BigInteger;)Ljava/math/BigInteger;

    move-result-object v12

    invoke-virtual {v12, v2}, Ljava/math/BigInteger;->mod(Ljava/math/BigInteger;)Ljava/math/BigInteger;

    move-result-object v12

    sget-object v13, Ljava/math/BigInteger;->ZERO:Ljava/math/BigInteger;

    invoke-virtual {v12, v13}, Ljava/math/BigInteger;->compareTo(Ljava/math/BigInteger;)I

    move-result v12

    if-eqz v12, :cond_3

    goto :goto_0

    :cond_3
    const/4 v12, 0x0

    const/4 v13, 0x0

    .line 419
    :goto_1
    invoke-virtual {v3, v7}, Ljava/math/BigInteger;->pow(I)Ljava/math/BigInteger;

    move-result-object v14

    invoke-virtual {v11, v14}, Ljava/math/BigInteger;->compareTo(Ljava/math/BigInteger;)I

    move-result v14

    const/4 v15, -0x1

    if-eq v14, v15, :cond_4

    goto :goto_0

    .line 425
    :cond_4
    sget-object v14, Ljava/math/BigInteger;->ONE:Ljava/math/BigInteger;

    invoke-virtual {v11, v14}, Ljava/math/BigInteger;->subtract(Ljava/math/BigInteger;)Ljava/math/BigInteger;

    move-result-object v14

    move-object/from16 v15, p4

    invoke-virtual {v15, v14}, Ljava/math/BigInteger;->gcd(Ljava/math/BigInteger;)Ljava/math/BigInteger;

    move-result-object v14

    sget-object v10, Ljava/math/BigInteger;->ONE:Ljava/math/BigInteger;

    invoke-virtual {v14, v10}, Ljava/math/BigInteger;->compareTo(Ljava/math/BigInteger;)I

    move-result v10

    const/4 v14, 0x2

    if-nez v10, :cond_5

    move/from16 v10, p5

    invoke-direct {v0, v11, v10}, Lee/cyber/smartid/cryptolib/impl/KeyGenerationOpImpl;->a(Ljava/math/BigInteger;I)Z

    move-result v17

    if-eqz v17, :cond_6

    .line 427
    new-array v1, v14, [Ljava/math/BigInteger;

    aput-object v11, v1, v12

    const/16 v16, 0x1

    aput-object v9, v1, v16

    return-object v1

    :cond_5
    move/from16 v10, p5

    :cond_6
    const/16 v16, 0x1

    add-int/lit8 v13, v13, 0x1

    mul-int/lit8 v16, p3, 0x5

    .line 434
    div-int/lit8 v14, v16, 0x2

    if-lt v13, v14, :cond_7

    const/4 v14, 0x0

    return-object v14

    :cond_7
    const/4 v14, 0x0

    .line 440
    invoke-virtual {v3, v1}, Ljava/math/BigInteger;->multiply(Ljava/math/BigInteger;)Ljava/math/BigInteger;

    move-result-object v12

    invoke-virtual {v12, v2}, Ljava/math/BigInteger;->multiply(Ljava/math/BigInteger;)Ljava/math/BigInteger;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/math/BigInteger;->add(Ljava/math/BigInteger;)Ljava/math/BigInteger;

    move-result-object v11

    move-object v10, v14

    const/4 v12, 0x0

    goto :goto_1

    :cond_8
    move-object/from16 v15, p4

    move-object v14, v10

    move/from16 v10, p5

    move-object v10, v14

    goto/16 :goto_0

    :cond_9
    :goto_2
    move-object v14, v10

    return-object v14
.end method


# virtual methods
.method a(II)Ljava/math/BigInteger;
    .locals 2

    .line 324
    new-instance v0, Ljava/math/BigInteger;

    iget-object v1, p0, Lee/cyber/smartid/cryptolib/impl/KeyGenerationOpImpl;->a:Lee/cyber/smartid/cryptolib/inter/RandomGenerationOp;

    invoke-interface {v1}, Lee/cyber/smartid/cryptolib/inter/RandomGenerationOp;->getRandom()Ljava/security/SecureRandom;

    move-result-object v1

    invoke-direct {v0, p1, v1}, Ljava/math/BigInteger;-><init>(ILjava/util/Random;)V

    const/4 p1, 0x0

    .line 326
    invoke-virtual {v0, p1}, Ljava/math/BigInteger;->setBit(I)Ljava/math/BigInteger;

    move-result-object p1

    .line 329
    new-instance v0, Ljava/math/BigInteger;

    const-string v1, "2"

    invoke-direct {v0, v1}, Ljava/math/BigInteger;-><init>(Ljava/lang/String;)V

    .line 330
    :goto_0
    invoke-direct {p0, p1, p2}, Lee/cyber/smartid/cryptolib/impl/KeyGenerationOpImpl;->a(Ljava/math/BigInteger;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 332
    invoke-virtual {p1, v0}, Ljava/math/BigInteger;->add(Ljava/math/BigInteger;)Ljava/math/BigInteger;

    move-result-object p1

    goto :goto_0

    :cond_0
    return-object p1
.end method

.method a(Lee/cyber/smartid/cryptolib/dto/DiffieHellmanGroup;)Ljava/math/BigInteger;
    .locals 3

    const-string v0, "Parameter group can\'t be null!"

    .line 570
    invoke-static {p1, v0}, Lee/cyber/smartid/cryptolib/util/Util;->throwIfNull(Ljava/io/Serializable;Ljava/lang/String;)V

    .line 571
    sget-object v0, Ljava/math/BigInteger;->ZERO:Ljava/math/BigInteger;

    .line 573
    :goto_0
    sget-object v1, Ljava/math/BigInteger;->ZERO:Ljava/math/BigInteger;

    invoke-virtual {v0, v1}, Ljava/math/BigInteger;->compareTo(Ljava/math/BigInteger;)I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    invoke-virtual {p1}, Lee/cyber/smartid/cryptolib/dto/DiffieHellmanGroup;->getPrime()Ljava/math/BigInteger;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/math/BigInteger;->compareTo(Ljava/math/BigInteger;)I

    move-result v1

    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    goto :goto_1

    :cond_0
    return-object v0

    .line 574
    :cond_1
    :goto_1
    new-instance v0, Ljava/math/BigInteger;

    invoke-virtual {p1}, Lee/cyber/smartid/cryptolib/dto/DiffieHellmanGroup;->getPrime()Ljava/math/BigInteger;

    move-result-object v1

    invoke-virtual {v1}, Ljava/math/BigInteger;->bitLength()I

    move-result v1

    iget-object v2, p0, Lee/cyber/smartid/cryptolib/impl/KeyGenerationOpImpl;->a:Lee/cyber/smartid/cryptolib/inter/RandomGenerationOp;

    invoke-interface {v2}, Lee/cyber/smartid/cryptolib/inter/RandomGenerationOp;->getRandom()Ljava/security/SecureRandom;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/math/BigInteger;-><init>(ILjava/util/Random;)V

    goto :goto_0
.end method

.method a(Lee/cyber/smartid/cryptolib/dto/DiffieHellmanGroup;Ljava/math/BigInteger;)Ljava/math/BigInteger;
    .locals 4

    const-string v0, "Parameter \"group\" can\'t be null!"

    .line 595
    invoke-static {p1, v0}, Lee/cyber/smartid/cryptolib/util/Util;->throwIfNull(Ljava/io/Serializable;Ljava/lang/String;)V

    const-string v0, "Parameter \"privateKey\" can\'t be null!"

    .line 596
    invoke-static {p2, v0}, Lee/cyber/smartid/cryptolib/util/Util;->throwIfNull(Ljava/math/BigInteger;Ljava/lang/String;)V

    .line 598
    sget-object v0, Ljava/math/BigInteger;->ZERO:Ljava/math/BigInteger;

    invoke-virtual {p2, v0}, Ljava/math/BigInteger;->compareTo(Ljava/math/BigInteger;)I

    move-result v0

    const-string v1, "Invalid private key, must be in range [1; p)"

    const/16 v2, 0x7d

    const/4 v3, 0x1

    if-ne v0, v3, :cond_1

    .line 601
    invoke-virtual {p1}, Lee/cyber/smartid/cryptolib/dto/DiffieHellmanGroup;->getPrime()Ljava/math/BigInteger;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/math/BigInteger;->compareTo(Ljava/math/BigInteger;)I

    move-result v0

    const/4 v3, -0x1

    if-ne v0, v3, :cond_0

    .line 606
    invoke-virtual {p1}, Lee/cyber/smartid/cryptolib/dto/DiffieHellmanGroup;->getGenerator()Ljava/math/BigInteger;

    move-result-object v0

    invoke-virtual {p1}, Lee/cyber/smartid/cryptolib/dto/DiffieHellmanGroup;->getPrime()Ljava/math/BigInteger;

    move-result-object p1

    invoke-virtual {v0, p2, p1}, Ljava/math/BigInteger;->modPow(Ljava/math/BigInteger;Ljava/math/BigInteger;)Ljava/math/BigInteger;

    move-result-object p1

    return-object p1

    .line 603
    :cond_0
    new-instance p1, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;

    invoke-direct {p1, v2, v1}, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;-><init>(ILjava/lang/String;)V

    throw p1

    .line 600
    :cond_1
    new-instance p1, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;

    invoke-direct {p1, v2, v1}, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;-><init>(ILjava/lang/String;)V

    throw p1
.end method

.method a(Lee/cyber/smartid/cryptolib/dto/DiffieHellmanKeyPair;Ljava/math/BigInteger;)Ljava/math/BigInteger;
    .locals 2

    const-string v0, "Parameter \"clientKeyPair\" can\'t be null!"

    .line 651
    invoke-static {p1, v0}, Lee/cyber/smartid/cryptolib/util/Util;->throwIfNull(Ljava/io/Serializable;Ljava/lang/String;)V

    .line 652
    invoke-virtual {p1}, Lee/cyber/smartid/cryptolib/dto/DiffieHellmanKeyPair;->getPrivateKey()Ljava/math/BigInteger;

    move-result-object v1

    invoke-static {v1, v0}, Lee/cyber/smartid/cryptolib/util/Util;->throwIfNull(Ljava/math/BigInteger;Ljava/lang/String;)V

    .line 653
    invoke-virtual {p1}, Lee/cyber/smartid/cryptolib/dto/DiffieHellmanKeyPair;->getGroup()Lee/cyber/smartid/cryptolib/dto/DiffieHellmanGroup;

    move-result-object v0

    const-string v1, "Parameter \"clientKeyPair group\" can\'t be null!"

    invoke-static {v0, v1}, Lee/cyber/smartid/cryptolib/util/Util;->throwIfNull(Ljava/io/Serializable;Ljava/lang/String;)V

    .line 654
    invoke-virtual {p1}, Lee/cyber/smartid/cryptolib/dto/DiffieHellmanKeyPair;->getGroup()Lee/cyber/smartid/cryptolib/dto/DiffieHellmanGroup;

    move-result-object v0

    invoke-virtual {v0}, Lee/cyber/smartid/cryptolib/dto/DiffieHellmanGroup;->getPrime()Ljava/math/BigInteger;

    move-result-object v0

    const-string v1, "Parameter \"clientKeyPair group prime\" can\'t be null!"

    invoke-static {v0, v1}, Lee/cyber/smartid/cryptolib/util/Util;->throwIfNull(Ljava/math/BigInteger;Ljava/lang/String;)V

    const-string v0, "Parameter \"serverPublicKey\" can\'t be null!"

    .line 655
    invoke-static {p2, v0}, Lee/cyber/smartid/cryptolib/util/Util;->throwIfNull(Ljava/math/BigInteger;Ljava/lang/String;)V

    .line 657
    invoke-virtual {p1}, Lee/cyber/smartid/cryptolib/dto/DiffieHellmanKeyPair;->getPrivateKey()Ljava/math/BigInteger;

    move-result-object v0

    invoke-virtual {p1}, Lee/cyber/smartid/cryptolib/dto/DiffieHellmanKeyPair;->getGroup()Lee/cyber/smartid/cryptolib/dto/DiffieHellmanGroup;

    move-result-object p1

    invoke-virtual {p1}, Lee/cyber/smartid/cryptolib/dto/DiffieHellmanGroup;->getPrime()Ljava/math/BigInteger;

    move-result-object p1

    invoke-virtual {p2, v0, p1}, Ljava/math/BigInteger;->modPow(Ljava/math/BigInteger;Ljava/math/BigInteger;)Ljava/math/BigInteger;

    move-result-object p1

    return-object p1
.end method

.method a(Ljava/math/BigInteger;Ljava/math/BigInteger;Ljava/math/BigInteger;)[Ljava/lang/String;
    .locals 2

    .line 460
    invoke-virtual {p1}, Ljava/math/BigInteger;->bitLength()I

    move-result v0

    div-int/lit8 v0, v0, 0x8

    new-array v0, v0, [B

    invoke-direct {p0, v0, p2, p3}, Lee/cyber/smartid/cryptolib/impl/KeyGenerationOpImpl;->a([BLjava/math/BigInteger;Ljava/math/BigInteger;)Ljava/math/BigInteger;

    move-result-object v0

    .line 461
    invoke-virtual {p2, v0}, Ljava/math/BigInteger;->subtract(Ljava/math/BigInteger;)Ljava/math/BigInteger;

    move-result-object p2

    invoke-virtual {p2, p3}, Ljava/math/BigInteger;->mod(Ljava/math/BigInteger;)Ljava/math/BigInteger;

    move-result-object p2

    const/4 p3, 0x3

    .line 465
    new-array p3, p3, [Ljava/lang/String;

    .line 466
    iget-object v1, p0, Lee/cyber/smartid/cryptolib/impl/KeyGenerationOpImpl;->b:Lee/cyber/smartid/cryptolib/inter/EncodingOp;

    invoke-interface {v1, v0}, Lee/cyber/smartid/cryptolib/inter/EncodingOp;->encodeDecimalToBase64(Ljava/math/BigInteger;)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    aput-object v0, p3, v1

    .line 467
    iget-object v0, p0, Lee/cyber/smartid/cryptolib/impl/KeyGenerationOpImpl;->b:Lee/cyber/smartid/cryptolib/inter/EncodingOp;

    invoke-interface {v0, p2}, Lee/cyber/smartid/cryptolib/inter/EncodingOp;->encodeDecimalToBase64(Ljava/math/BigInteger;)Ljava/lang/String;

    move-result-object p2

    const/4 v0, 0x1

    aput-object p2, p3, v0

    .line 468
    iget-object p2, p0, Lee/cyber/smartid/cryptolib/impl/KeyGenerationOpImpl;->b:Lee/cyber/smartid/cryptolib/inter/EncodingOp;

    invoke-interface {p2, p1}, Lee/cyber/smartid/cryptolib/inter/EncodingOp;->encodeDecimalToBase64(Ljava/math/BigInteger;)Ljava/lang/String;

    move-result-object p1

    const/4 p2, 0x2

    aput-object p1, p3, p2

    return-object p3
.end method

.method a(ILjava/math/BigInteger;)[Ljava/math/BigInteger;
    .locals 11

    const-string v0, "The public verification exponent e can\'t be null!"

    .line 195
    invoke-static {p2, v0}, Lee/cyber/smartid/cryptolib/util/Util;->throwIfNull(Ljava/math/BigInteger;Ljava/lang/String;)V

    const/16 v0, 0x800

    if-eq p1, v0, :cond_1

    const/16 v0, 0xc00

    if-ne p1, v0, :cond_0

    goto :goto_0

    .line 199
    :cond_0
    new-instance p1, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;

    const/16 p2, 0x6b

    const-string v0, "Only 2048 and 3072 are supported!"

    invoke-direct {p1, p2, v0}, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;-><init>(ILjava/lang/String;)V

    throw p1

    .line 200
    :cond_1
    :goto_0
    new-instance v0, Ljava/math/BigInteger;

    const-string v1, "2"

    invoke-direct {v0, v1}, Ljava/math/BigInteger;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2, v0}, Ljava/math/BigInteger;->mod(Ljava/math/BigInteger;)Ljava/math/BigInteger;

    move-result-object v0

    sget-object v2, Ljava/math/BigInteger;->ZERO:Ljava/math/BigInteger;

    invoke-virtual {v0, v2}, Ljava/math/BigInteger;->equals(Ljava/lang/Object;)Z

    move-result v0

    const/16 v2, 0x6d

    if-nez v0, :cond_8

    .line 203
    new-instance v0, Ljava/math/BigInteger;

    invoke-direct {v0, v1}, Ljava/math/BigInteger;-><init>(Ljava/lang/String;)V

    const/16 v3, 0x10

    invoke-virtual {v0, v3}, Ljava/math/BigInteger;->pow(I)Ljava/math/BigInteger;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/math/BigInteger;->compareTo(Ljava/math/BigInteger;)I

    move-result v0

    const/4 v3, 0x1

    if-ne v0, v3, :cond_7

    .line 206
    new-instance v0, Ljava/math/BigInteger;

    invoke-direct {v0, v1}, Ljava/math/BigInteger;-><init>(Ljava/lang/String;)V

    const/16 v4, 0x100

    invoke-virtual {v0, v4}, Ljava/math/BigInteger;->pow(I)Ljava/math/BigInteger;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/math/BigInteger;->compareTo(Ljava/math/BigInteger;)I

    move-result v0

    const/4 v4, -0x1

    if-ne v0, v4, :cond_6

    .line 211
    new-instance v0, Lee/cyber/smartid/cryptolib/impl/KeyGenerationOpImpl$GenerationParams;

    invoke-direct {v0, p1, p2}, Lee/cyber/smartid/cryptolib/impl/KeyGenerationOpImpl$GenerationParams;-><init>(ILjava/math/BigInteger;)V

    .line 219
    :cond_2
    invoke-virtual {p0, v0}, Lee/cyber/smartid/cryptolib/impl/KeyGenerationOpImpl;->generateProbablePrimeFromAuxiliaryPrimes(Lee/cyber/smartid/cryptolib/impl/KeyGenerationOpImpl$GenerationParams;)[Ljava/math/BigInteger;

    move-result-object p2

    if-eqz p2, :cond_2

    const/4 v2, 0x0

    .line 224
    aget-object v4, p2, v2

    .line 225
    aget-object v5, p2, v3

    const/4 v6, 0x0

    .line 227
    aput-object v6, p2, v2

    .line 228
    aput-object v6, p2, v3

    .line 245
    :cond_3
    invoke-virtual {p0, v0}, Lee/cyber/smartid/cryptolib/impl/KeyGenerationOpImpl;->generateProbablePrimeFromAuxiliaryPrimes(Lee/cyber/smartid/cryptolib/impl/KeyGenerationOpImpl$GenerationParams;)[Ljava/math/BigInteger;

    move-result-object p2

    if-eqz p2, :cond_3

    .line 250
    aget-object v7, p2, v2

    .line 251
    aget-object v8, p2, v3

    .line 253
    aput-object v6, p2, v2

    .line 254
    aput-object v6, p2, v3

    .line 259
    new-instance p2, Ljava/math/BigInteger;

    invoke-direct {p2, v1}, Ljava/math/BigInteger;-><init>(Ljava/lang/String;)V

    const/4 v9, 0x2

    div-int/lit8 v10, p1, 0x2

    add-int/lit8 v10, v10, -0x64

    invoke-virtual {p2, v10}, Ljava/math/BigInteger;->pow(I)Ljava/math/BigInteger;

    move-result-object p2

    .line 262
    invoke-virtual {v5, v8}, Ljava/math/BigInteger;->subtract(Ljava/math/BigInteger;)Ljava/math/BigInteger;

    move-result-object v8

    invoke-virtual {v8}, Ljava/math/BigInteger;->abs()Ljava/math/BigInteger;

    move-result-object v8

    invoke-virtual {v8, p2}, Ljava/math/BigInteger;->compareTo(Ljava/math/BigInteger;)I

    move-result v8

    if-ne v8, v3, :cond_4

    const/4 v8, 0x1

    goto :goto_1

    :cond_4
    const/4 v8, 0x0

    .line 264
    :goto_1
    invoke-virtual {v4, v7}, Ljava/math/BigInteger;->subtract(Ljava/math/BigInteger;)Ljava/math/BigInteger;

    move-result-object v10

    invoke-virtual {v10}, Ljava/math/BigInteger;->abs()Ljava/math/BigInteger;

    move-result-object v10

    invoke-virtual {v10, p2}, Ljava/math/BigInteger;->compareTo(Ljava/math/BigInteger;)I

    move-result p2

    if-ne p2, v3, :cond_5

    const/4 p2, 0x1

    goto :goto_2

    :cond_5
    const/4 p2, 0x0

    :goto_2
    if-eqz v8, :cond_3

    if-eqz p2, :cond_3

    .line 273
    invoke-virtual {v0}, Lee/cyber/smartid/cryptolib/impl/KeyGenerationOpImpl$GenerationParams;->clear()V

    .line 282
    new-array p1, v9, [Ljava/math/BigInteger;

    aput-object v4, p1, v2

    aput-object v7, p1, v3

    return-object p1

    .line 208
    :cond_6
    new-instance p1, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;

    const-string p2, "e can\'t be \u22652^256!"

    invoke-direct {p1, v2, p2}, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;-><init>(ILjava/lang/String;)V

    throw p1

    .line 205
    :cond_7
    new-instance p1, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;

    const-string p2, "e can\'t be \u2264 2^16!"

    invoke-direct {p1, v2, p2}, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;-><init>(ILjava/lang/String;)V

    throw p1

    .line 202
    :cond_8
    new-instance p1, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;

    const-string p2, "e has to be odd!"

    invoke-direct {p1, v2, p2}, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;-><init>(ILjava/lang/String;)V

    throw p1
.end method

.method b(ILjava/math/BigInteger;)[Ljava/math/BigInteger;
    .locals 8

    .line 512
    :cond_0
    invoke-virtual {p0, p1, p2}, Lee/cyber/smartid/cryptolib/impl/KeyGenerationOpImpl;->a(ILjava/math/BigInteger;)[Ljava/math/BigInteger;

    move-result-object v0

    const/4 v1, 0x0

    .line 515
    aget-object v2, v0, v1

    sget-object v3, Ljava/math/BigInteger;->ONE:Ljava/math/BigInteger;

    invoke-virtual {v2, v3}, Ljava/math/BigInteger;->subtract(Ljava/math/BigInteger;)Ljava/math/BigInteger;

    move-result-object v2

    const/4 v3, 0x1

    aget-object v4, v0, v3

    sget-object v5, Ljava/math/BigInteger;->ONE:Ljava/math/BigInteger;

    invoke-virtual {v4, v5}, Ljava/math/BigInteger;->subtract(Ljava/math/BigInteger;)Ljava/math/BigInteger;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/math/BigInteger;->multiply(Ljava/math/BigInteger;)Ljava/math/BigInteger;

    move-result-object v2

    .line 516
    invoke-virtual {p2, v2}, Ljava/math/BigInteger;->modInverse(Ljava/math/BigInteger;)Ljava/math/BigInteger;

    move-result-object v4

    .line 518
    invoke-virtual {v4}, Ljava/math/BigInteger;->bitLength()I

    move-result v5

    const/4 v6, 0x2

    div-int/lit8 v7, p1, 0x2

    if-le v5, v7, :cond_0

    .line 524
    aget-object p1, v0, v1

    aget-object p2, v0, v3

    invoke-virtual {p1, p2}, Ljava/math/BigInteger;->multiply(Ljava/math/BigInteger;)Ljava/math/BigInteger;

    move-result-object p1

    const/4 p2, 0x3

    .line 528
    new-array p2, p2, [Ljava/math/BigInteger;

    aput-object p1, p2, v1

    aput-object v4, p2, v3

    aput-object v2, p2, v6

    return-object p2
.end method

.method public calculateConcatKDFWithSHA256(Lee/cyber/smartid/cryptolib/dto/DiffieHellmanKeyPair;Ljava/math/BigInteger;)[B
    .locals 3

    const-string v0, "Parameter \"clientKeyPair\" can\'t be null!"

    .line 612
    invoke-static {p1, v0}, Lee/cyber/smartid/cryptolib/util/Util;->throwIfNull(Ljava/io/Serializable;Ljava/lang/String;)V

    .line 613
    invoke-virtual {p1}, Lee/cyber/smartid/cryptolib/dto/DiffieHellmanKeyPair;->getPrivateKey()Ljava/math/BigInteger;

    move-result-object v1

    invoke-static {v1, v0}, Lee/cyber/smartid/cryptolib/util/Util;->throwIfNull(Ljava/math/BigInteger;Ljava/lang/String;)V

    .line 614
    invoke-virtual {p1}, Lee/cyber/smartid/cryptolib/dto/DiffieHellmanKeyPair;->getGroup()Lee/cyber/smartid/cryptolib/dto/DiffieHellmanGroup;

    move-result-object v0

    const-string v1, "Parameter \"clientKeyPair group\" can\'t be null!"

    invoke-static {v0, v1}, Lee/cyber/smartid/cryptolib/util/Util;->throwIfNull(Ljava/io/Serializable;Ljava/lang/String;)V

    .line 615
    invoke-virtual {p1}, Lee/cyber/smartid/cryptolib/dto/DiffieHellmanKeyPair;->getGroup()Lee/cyber/smartid/cryptolib/dto/DiffieHellmanGroup;

    move-result-object v0

    invoke-virtual {v0}, Lee/cyber/smartid/cryptolib/dto/DiffieHellmanGroup;->getPrime()Ljava/math/BigInteger;

    move-result-object v0

    const-string v1, "Parameter \"clientKeyPair group prime\" can\'t be null!"

    invoke-static {v0, v1}, Lee/cyber/smartid/cryptolib/util/Util;->throwIfNull(Ljava/math/BigInteger;Ljava/lang/String;)V

    const-string v0, "Parameter \"serverPublicKey\" can\'t be null!"

    .line 616
    invoke-static {p2, v0}, Lee/cyber/smartid/cryptolib/util/Util;->throwIfNull(Ljava/math/BigInteger;Ljava/lang/String;)V

    .line 619
    invoke-virtual {p0, p1, p2}, Lee/cyber/smartid/cryptolib/impl/KeyGenerationOpImpl;->a(Lee/cyber/smartid/cryptolib/dto/DiffieHellmanKeyPair;Ljava/math/BigInteger;)Ljava/math/BigInteger;

    move-result-object p1

    .line 621
    new-instance p2, Lorg/a/b/a/a/a;

    new-instance v0, Lorg/a/b/b/g;

    invoke-direct {v0}, Lorg/a/b/b/g;-><init>()V

    invoke-direct {p2, v0}, Lorg/a/b/a/a/a;-><init>(Lorg/a/b/f;)V

    .line 622
    new-instance v0, Lorg/a/b/g/b;

    iget-object v1, p0, Lee/cyber/smartid/cryptolib/impl/KeyGenerationOpImpl;->b:Lee/cyber/smartid/cryptolib/inter/EncodingOp;

    invoke-interface {v1, p1}, Lee/cyber/smartid/cryptolib/inter/EncodingOp;->encodePositiveBigIntegerAsBytes(Ljava/math/BigInteger;)[B

    move-result-object p1

    sget-object v1, Lee/cyber/smartid/cryptolib/CryptoLib;->DEFAULT_ENCODING:Ljava/nio/charset/Charset;

    const-string v2, "A128CBC-HS256CLIENTSERVER"

    invoke-virtual {v2, v1}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v1

    invoke-direct {v0, p1, v1}, Lorg/a/b/g/b;-><init>([B[B)V

    const/16 p1, 0x78

    .line 624
    :try_start_0
    invoke-virtual {p2, v0}, Lorg/a/b/a/a/a;->a(Lorg/a/b/e;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_3

    const/16 v0, 0x20

    .line 628
    new-array v0, v0, [B

    const/4 v1, 0x0

    .line 630
    :try_start_1
    array-length v2, v0

    invoke-virtual {p2, v0, v1, v2}, Lorg/a/b/a/a/a;->a([BII)I
    :try_end_1
    .catch Lorg/a/b/d; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    return-object v0

    :catch_0
    move-exception p2

    .line 636
    new-instance v0, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;

    invoke-virtual {p2}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object p2

    invoke-direct {v0, p1, p2}, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;-><init>(ILjava/lang/String;)V

    throw v0

    :catch_1
    move-exception p2

    .line 634
    new-instance v0, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;

    invoke-virtual {p2}, Ljava/lang/IllegalArgumentException;->getMessage()Ljava/lang/String;

    move-result-object p2

    invoke-direct {v0, p1, p2}, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;-><init>(ILjava/lang/String;)V

    throw v0

    :catch_2
    move-exception p2

    .line 632
    new-instance v0, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;

    invoke-virtual {p2}, Lorg/a/b/d;->getMessage()Ljava/lang/String;

    move-result-object p2

    invoke-direct {v0, p1, p2}, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;-><init>(ILjava/lang/String;)V

    throw v0

    :catch_3
    move-exception p2

    .line 626
    new-instance v0, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;

    invoke-virtual {p2}, Ljava/lang/IllegalArgumentException;->getMessage()Ljava/lang/String;

    move-result-object p2

    invoke-direct {v0, p1, p2}, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;-><init>(ILjava/lang/String;)V

    throw v0
.end method

.method protected final clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .line 72
    new-instance v0, Ljava/lang/CloneNotSupportedException;

    invoke-direct {v0}, Ljava/lang/CloneNotSupportedException;-><init>()V

    throw v0
.end method

.method public generateDiffieHellmanKeyPair(Lee/cyber/smartid/cryptolib/dto/DiffieHellmanGroup;)Lee/cyber/smartid/cryptolib/dto/DiffieHellmanKeyPair;
    .locals 2

    const-string v0, "Parameter group can\'t be null!"

    .line 548
    invoke-static {p1, v0}, Lee/cyber/smartid/cryptolib/util/Util;->throwIfNull(Ljava/io/Serializable;Ljava/lang/String;)V

    .line 550
    invoke-virtual {p0, p1}, Lee/cyber/smartid/cryptolib/impl/KeyGenerationOpImpl;->a(Lee/cyber/smartid/cryptolib/dto/DiffieHellmanGroup;)Ljava/math/BigInteger;

    move-result-object v0

    .line 552
    invoke-virtual {p0, p1, v0}, Lee/cyber/smartid/cryptolib/impl/KeyGenerationOpImpl;->a(Lee/cyber/smartid/cryptolib/dto/DiffieHellmanGroup;Ljava/math/BigInteger;)Ljava/math/BigInteger;

    move-result-object v1

    invoke-static {v0, v1, p1}, Lee/cyber/smartid/cryptolib/dto/DiffieHellmanKeyPair;->createKeyPair(Ljava/math/BigInteger;Ljava/math/BigInteger;Lee/cyber/smartid/cryptolib/dto/DiffieHellmanGroup;)Lee/cyber/smartid/cryptolib/dto/DiffieHellmanKeyPair;

    move-result-object p1

    return-object p1
.end method

.method public generatePQAndCreateKeyShare(ILjava/math/BigInteger;)[Ljava/lang/String;
    .locals 2

    const-string v0, "Parameter e can\'t be null!"

    .line 538
    invoke-static {p2, v0}, Lee/cyber/smartid/cryptolib/util/Util;->throwIfNull(Ljava/math/BigInteger;Ljava/lang/String;)V

    .line 540
    invoke-virtual {p0, p1, p2}, Lee/cyber/smartid/cryptolib/impl/KeyGenerationOpImpl;->b(ILjava/math/BigInteger;)[Ljava/math/BigInteger;

    move-result-object p1

    const/4 p2, 0x0

    .line 542
    aget-object p2, p1, p2

    const/4 v0, 0x1

    aget-object v0, p1, v0

    const/4 v1, 0x2

    aget-object p1, p1, v1

    invoke-virtual {p0, p2, v0, p1}, Lee/cyber/smartid/cryptolib/impl/KeyGenerationOpImpl;->a(Ljava/math/BigInteger;Ljava/math/BigInteger;Ljava/math/BigInteger;)[Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public generateProbablePrimeFromAuxiliaryPrimes(Lee/cyber/smartid/cryptolib/impl/KeyGenerationOpImpl$GenerationParams;)[Ljava/math/BigInteger;
    .locals 10

    .line 295
    iget v0, p1, Lee/cyber/smartid/cryptolib/impl/KeyGenerationOpImpl$GenerationParams;->xP1PrimLen:I

    iget v1, p1, Lee/cyber/smartid/cryptolib/impl/KeyGenerationOpImpl$GenerationParams;->mrIterations:I

    invoke-virtual {p0, v0, v1}, Lee/cyber/smartid/cryptolib/impl/KeyGenerationOpImpl;->a(II)Ljava/math/BigInteger;

    move-result-object v0

    .line 296
    iget v1, p1, Lee/cyber/smartid/cryptolib/impl/KeyGenerationOpImpl$GenerationParams;->xP1PrimPrimLen:I

    iget v2, p1, Lee/cyber/smartid/cryptolib/impl/KeyGenerationOpImpl$GenerationParams;->mrIterations:I

    invoke-virtual {p0, v1, v2}, Lee/cyber/smartid/cryptolib/impl/KeyGenerationOpImpl;->a(II)Ljava/math/BigInteger;

    move-result-object v1

    .line 297
    iget v2, p1, Lee/cyber/smartid/cryptolib/impl/KeyGenerationOpImpl$GenerationParams;->xP2Len:I

    iget v3, p1, Lee/cyber/smartid/cryptolib/impl/KeyGenerationOpImpl$GenerationParams;->mrIterations:I

    invoke-virtual {p0, v2, v3}, Lee/cyber/smartid/cryptolib/impl/KeyGenerationOpImpl;->a(II)Ljava/math/BigInteger;

    move-result-object v6

    .line 300
    invoke-virtual {v0, v1}, Ljava/math/BigInteger;->multiply(Ljava/math/BigInteger;)Ljava/math/BigInteger;

    move-result-object v5

    .line 308
    iget v7, p1, Lee/cyber/smartid/cryptolib/impl/KeyGenerationOpImpl$GenerationParams;->nlen:I

    iget-object v8, p1, Lee/cyber/smartid/cryptolib/impl/KeyGenerationOpImpl$GenerationParams;->e:Ljava/math/BigInteger;

    iget v9, p1, Lee/cyber/smartid/cryptolib/impl/KeyGenerationOpImpl$GenerationParams;->mrIterations:I

    move-object v4, p0

    invoke-direct/range {v4 .. v9}, Lee/cyber/smartid/cryptolib/impl/KeyGenerationOpImpl;->a(Ljava/math/BigInteger;Ljava/math/BigInteger;ILjava/math/BigInteger;I)[Ljava/math/BigInteger;

    move-result-object p1

    return-object p1
.end method
