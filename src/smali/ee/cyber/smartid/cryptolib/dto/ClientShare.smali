.class public final Lee/cyber/smartid/cryptolib/dto/ClientShare;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = -0x2b6e8f7e76ac366bL


# instance fields
.field private keyShare:Ljava/lang/String;

.field private keySize:I


# direct methods
.method public constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-object p1, p0, Lee/cyber/smartid/cryptolib/dto/ClientShare;->keyShare:Ljava/lang/String;

    .line 27
    iput p2, p0, Lee/cyber/smartid/cryptolib/dto/ClientShare;->keySize:I

    return-void
.end method


# virtual methods
.method public getKeyShare()Ljava/lang/String;
    .locals 1

    .line 36
    iget-object v0, p0, Lee/cyber/smartid/cryptolib/dto/ClientShare;->keyShare:Ljava/lang/String;

    return-object v0
.end method

.method public getKeySize()I
    .locals 1

    .line 54
    iget v0, p0, Lee/cyber/smartid/cryptolib/dto/ClientShare;->keySize:I

    return v0
.end method

.method public setKeyShare(Ljava/lang/String;)V
    .locals 0

    .line 45
    iput-object p1, p0, Lee/cyber/smartid/cryptolib/dto/ClientShare;->keyShare:Ljava/lang/String;

    return-void
.end method

.method public setKeySize(I)V
    .locals 0

    .line 63
    iput p1, p0, Lee/cyber/smartid/cryptolib/dto/ClientShare;->keySize:I

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 73
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ClientShare{keyShare=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lee/cyber/smartid/cryptolib/dto/ClientShare;->keyShare:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v1, ", keySize="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lee/cyber/smartid/cryptolib/dto/ClientShare;->keySize:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
