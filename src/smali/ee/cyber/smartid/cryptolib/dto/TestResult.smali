.class public Lee/cyber/smartid/cryptolib/dto/TestResult;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field public static final CSPRNG_QUALITY_TEST_LONG_RUNS:Ljava/lang/String; = "FIPS 140-1 Long Runs test"

.field public static final CSPRNG_QUALITY_TEST_MONOBIT:Ljava/lang/String; = "FIPS 140-1 Monobit test"

.field public static final CSPRNG_QUALITY_TEST_POKER:Ljava/lang/String; = "FIPS 140-1 Poker test"

.field public static final CSPRNG_QUALITY_TEST_RUNS:Ljava/lang/String; = "FIPS 140-1 Runs test"

.field private static final serialVersionUID:J = 0x56271c99ca83df1aL


# instance fields
.field private failCount:I

.field private final name:Ljava/lang/String;

.field private successCount:I


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 0

    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    iput-object p1, p0, Lee/cyber/smartid/cryptolib/dto/TestResult;->name:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;II)V
    .locals 0

    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    iput-object p1, p0, Lee/cyber/smartid/cryptolib/dto/TestResult;->name:Ljava/lang/String;

    .line 58
    iput p2, p0, Lee/cyber/smartid/cryptolib/dto/TestResult;->successCount:I

    .line 59
    iput p3, p0, Lee/cyber/smartid/cryptolib/dto/TestResult;->failCount:I

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p0, p1, :cond_0

    return v0

    :cond_0
    const/4 v1, 0x0

    if-eqz p1, :cond_6

    .line 141
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_1

    goto :goto_1

    .line 145
    :cond_1
    check-cast p1, Lee/cyber/smartid/cryptolib/dto/TestResult;

    .line 146
    iget v2, p0, Lee/cyber/smartid/cryptolib/dto/TestResult;->successCount:I

    iget v3, p1, Lee/cyber/smartid/cryptolib/dto/TestResult;->successCount:I

    if-eq v2, v3, :cond_2

    return v1

    .line 149
    :cond_2
    iget v2, p0, Lee/cyber/smartid/cryptolib/dto/TestResult;->failCount:I

    iget v3, p1, Lee/cyber/smartid/cryptolib/dto/TestResult;->failCount:I

    if-eq v2, v3, :cond_3

    return v1

    .line 152
    :cond_3
    iget-object v2, p0, Lee/cyber/smartid/cryptolib/dto/TestResult;->name:Ljava/lang/String;

    if-eqz v2, :cond_4

    iget-object p1, p1, Lee/cyber/smartid/cryptolib/dto/TestResult;->name:Ljava/lang/String;

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0

    :cond_4
    iget-object p1, p1, Lee/cyber/smartid/cryptolib/dto/TestResult;->name:Ljava/lang/String;

    if-nez p1, :cond_5

    goto :goto_0

    :cond_5
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_6
    :goto_1
    return v1
.end method

.method public getExecutionCount()I
    .locals 2

    .line 95
    iget v0, p0, Lee/cyber/smartid/cryptolib/dto/TestResult;->successCount:I

    iget v1, p0, Lee/cyber/smartid/cryptolib/dto/TestResult;->failCount:I

    add-int/2addr v0, v1

    return v0
.end method

.method public getFailCount()I
    .locals 1

    .line 120
    iget v0, p0, Lee/cyber/smartid/cryptolib/dto/TestResult;->failCount:I

    return v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .line 86
    iget-object v0, p0, Lee/cyber/smartid/cryptolib/dto/TestResult;->name:Ljava/lang/String;

    return-object v0
.end method

.method public getSuccessCount()I
    .locals 1

    .line 111
    iget v0, p0, Lee/cyber/smartid/cryptolib/dto/TestResult;->successCount:I

    return v0
.end method

.method public hashCode()I
    .locals 2

    .line 162
    iget-object v0, p0, Lee/cyber/smartid/cryptolib/dto/TestResult;->name:Ljava/lang/String;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    .line 163
    iget v1, p0, Lee/cyber/smartid/cryptolib/dto/TestResult;->successCount:I

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    .line 164
    iget v1, p0, Lee/cyber/smartid/cryptolib/dto/TestResult;->failCount:I

    add-int/2addr v0, v1

    return v0
.end method

.method public incrementFailCount()V
    .locals 1

    .line 127
    iget v0, p0, Lee/cyber/smartid/cryptolib/dto/TestResult;->failCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lee/cyber/smartid/cryptolib/dto/TestResult;->failCount:I

    return-void
.end method

.method public incrementSuccessCount()V
    .locals 1

    .line 102
    iget v0, p0, Lee/cyber/smartid/cryptolib/dto/TestResult;->successCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lee/cyber/smartid/cryptolib/dto/TestResult;->successCount:I

    return-void
.end method

.method public isExecuted()Z
    .locals 1

    .line 77
    iget v0, p0, Lee/cyber/smartid/cryptolib/dto/TestResult;->successCount:I

    if-gtz v0, :cond_1

    iget v0, p0, Lee/cyber/smartid/cryptolib/dto/TestResult;->failCount:I

    if-lez v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method public isPassed()Z
    .locals 1

    .line 68
    invoke-virtual {p0}, Lee/cyber/smartid/cryptolib/dto/TestResult;->isExecuted()Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lee/cyber/smartid/cryptolib/dto/TestResult;->failCount:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 175
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "TestResult{name=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lee/cyber/smartid/cryptolib/dto/TestResult;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v1, ", successCount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lee/cyber/smartid/cryptolib/dto/TestResult;->successCount:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", failCount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lee/cyber/smartid/cryptolib/dto/TestResult;->failCount:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", isExecuted="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 179
    invoke-virtual {p0}, Lee/cyber/smartid/cryptolib/dto/TestResult;->isExecuted()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", isPassed="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 180
    invoke-virtual {p0}, Lee/cyber/smartid/cryptolib/dto/TestResult;->isPassed()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
