.class public final Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;
.super Ljava/lang/RuntimeException;


# static fields
.field public static final ERROR_CODE_ARGUMENT_IS_NULL_OR_EMPTY:I = 0x64

.field public static final ERROR_CODE_BYTE_BUFFER_IO:I = 0x70

.field public static final ERROR_CODE_DECODE_FAILED:I = 0x7a

.field public static final ERROR_CODE_ENCODE_FAILED:I = 0x7b

.field public static final ERROR_CODE_ENCRYPTION_DECRYPTION_FAILED:I = 0x68

.field public static final ERROR_CODE_EXECUTION_INTERRUPTED:I = 0x79

.field public static final ERROR_CODE_GROUP_NOT_SUPPORTED:I = 0x7c

.field public static final ERROR_CODE_ILLEGAL_ARGUMENTS:I = 0x78

.field public static final ERROR_CODE_ILLEGAL_AUDIENCE_USED:I = 0x82

.field public static final ERROR_CODE_ILLEGAL_CIPHER_MODE:I = 0x69

.field public static final ERROR_CODE_ILLEGAL_ENCRYPTION_ALGORITHM_USED:I = 0x80

.field public static final ERROR_CODE_ILLEGAL_ENCRYPTION_METHOD_USED:I = 0x7f

.field public static final ERROR_CODE_ILLEGAL_KEY_ID:I = 0x81

.field public static final ERROR_CODE_ILLEGAL_MODULUS_LENGTH:I = 0x6b

.field public static final ERROR_CODE_ILLEGAL_PAYLOAD:I = 0x83

.field public static final ERROR_CODE_ILLEGAL_PRIVATE_KEY:I = 0x7d

.field public static final ERROR_CODE_ILLEGAL_PUBLIC_EXPONENT_E:I = 0x6d

.field public static final ERROR_CODE_ILLEGAL_PURPOSE_USED:I = 0x85

.field public static final ERROR_CODE_ILLEGAL_SIGNATURE_ALGORITHM_USED:I = 0x7e

.field public static final ERROR_CODE_IMPL_IS_NULL:I = 0x65

.field public static final ERROR_CODE_INCORRECT_HASH_LENGTH:I = 0x71

.field public static final ERROR_CODE_INPUT_TOO_LARGE:I = 0x75

.field public static final ERROR_CODE_INPUT_TOO_SMALL:I = 0x76

.field public static final ERROR_CODE_INTENDED_ENCODED_MESSAGE_TOO_SHORT:I = 0x6e

.field public static final ERROR_CODE_INVALID_INPUTS:I = 0x78
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final ERROR_CODE_INVALID_RSA_KEY:I = 0x74

.field public static final ERROR_CODE_MESSAGE_REPRESENTATIVE_OUT_OF_RANGE:I = 0x72

.field public static final ERROR_CODE_NO_SUCH_ALGORITHM:I = 0x6c

.field public static final ERROR_CODE_PADDING_TOO_SHORT:I = 0x6f

.field public static final ERROR_CODE_STORAGE_TRANSFORMATION_UNSUPPORTED:I = 0x84

.field public static final ERROR_CODE_STORAGE_VERSION_UNKNOWN:I = 0x77

.field public static final ERROR_CODE_UNABLE_TO_ACCESS_SYSTEM_RESOURCES:I = 0x66

.field public static final ERROR_CODE_UNDEFINED_HASH_TYPE:I = 0x67

.field public static final ERROR_CODE_UNSUPPORTED_ENCODING:I = 0x73

.field private static final serialVersionUID:J = 0x35c723253fdcae29L


# instance fields
.field private final errorCode:I


# direct methods
.method public constructor <init>(ILjava/lang/String;)V
    .locals 0

    .line 163
    invoke-direct {p0, p2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    .line 164
    iput p1, p0, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;->errorCode:I

    return-void
.end method


# virtual methods
.method public getErrorCode()I
    .locals 1

    .line 173
    iget v0, p0, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;->errorCode:I

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 183
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "CryptoRuntimeException{errorCode="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;->errorCode:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", errorMessage="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 185
    invoke-virtual {p0}, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
