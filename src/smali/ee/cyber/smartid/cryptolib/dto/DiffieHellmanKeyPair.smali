.class public Lee/cyber/smartid/cryptolib/dto/DiffieHellmanKeyPair;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = 0x741f6835b0efb1b7L


# instance fields
.field private group:Lee/cyber/smartid/cryptolib/dto/DiffieHellmanGroup;

.field private privateKey:Ljava/math/BigInteger;

.field private publicKey:Ljava/math/BigInteger;


# direct methods
.method private constructor <init>(Ljava/math/BigInteger;Ljava/math/BigInteger;Lee/cyber/smartid/cryptolib/dto/DiffieHellmanGroup;)V
    .locals 0

    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    iput-object p1, p0, Lee/cyber/smartid/cryptolib/dto/DiffieHellmanKeyPair;->privateKey:Ljava/math/BigInteger;

    .line 52
    iput-object p2, p0, Lee/cyber/smartid/cryptolib/dto/DiffieHellmanKeyPair;->publicKey:Ljava/math/BigInteger;

    .line 53
    iput-object p3, p0, Lee/cyber/smartid/cryptolib/dto/DiffieHellmanKeyPair;->group:Lee/cyber/smartid/cryptolib/dto/DiffieHellmanGroup;

    return-void
.end method

.method public static createKeyPair(Ljava/math/BigInteger;Ljava/math/BigInteger;Lee/cyber/smartid/cryptolib/dto/DiffieHellmanGroup;)Lee/cyber/smartid/cryptolib/dto/DiffieHellmanKeyPair;
    .locals 1

    const-string v0, "The parameter \"privateKey\" can\'t be null!"

    .line 37
    invoke-static {p0, v0}, Lee/cyber/smartid/cryptolib/util/Util;->throwIfNull(Ljava/math/BigInteger;Ljava/lang/String;)V

    const-string v0, "The parameter \"publicKey\" can\'t be null!"

    .line 38
    invoke-static {p1, v0}, Lee/cyber/smartid/cryptolib/util/Util;->throwIfNull(Ljava/math/BigInteger;Ljava/lang/String;)V

    const-string v0, "The parameter \"group\" can\'t be null!"

    .line 39
    invoke-static {p2, v0}, Lee/cyber/smartid/cryptolib/util/Util;->throwIfNull(Ljava/io/Serializable;Ljava/lang/String;)V

    .line 40
    new-instance v0, Lee/cyber/smartid/cryptolib/dto/DiffieHellmanKeyPair;

    invoke-direct {v0, p0, p1, p2}, Lee/cyber/smartid/cryptolib/dto/DiffieHellmanKeyPair;-><init>(Ljava/math/BigInteger;Ljava/math/BigInteger;Lee/cyber/smartid/cryptolib/dto/DiffieHellmanGroup;)V

    return-object v0
.end method


# virtual methods
.method public getGroup()Lee/cyber/smartid/cryptolib/dto/DiffieHellmanGroup;
    .locals 1

    .line 83
    iget-object v0, p0, Lee/cyber/smartid/cryptolib/dto/DiffieHellmanKeyPair;->group:Lee/cyber/smartid/cryptolib/dto/DiffieHellmanGroup;

    return-object v0
.end method

.method public getPrivateKey()Ljava/math/BigInteger;
    .locals 1

    .line 63
    iget-object v0, p0, Lee/cyber/smartid/cryptolib/dto/DiffieHellmanKeyPair;->privateKey:Ljava/math/BigInteger;

    return-object v0
.end method

.method public getPublicKey()Ljava/math/BigInteger;
    .locals 1

    .line 73
    iget-object v0, p0, Lee/cyber/smartid/cryptolib/dto/DiffieHellmanKeyPair;->publicKey:Ljava/math/BigInteger;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 88
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "DiffieHellmanKeyPair{privateKey="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lee/cyber/smartid/cryptolib/dto/DiffieHellmanKeyPair;->privateKey:Ljava/math/BigInteger;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", publicKey="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lee/cyber/smartid/cryptolib/dto/DiffieHellmanKeyPair;->publicKey:Ljava/math/BigInteger;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", group="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lee/cyber/smartid/cryptolib/dto/DiffieHellmanKeyPair;->group:Lee/cyber/smartid/cryptolib/dto/DiffieHellmanGroup;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
