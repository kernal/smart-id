.class public Lee/cyber/smartid/cryptolib/dto/DiffieHellmanGroup;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lee/cyber/smartid/cryptolib/dto/DiffieHellmanGroup$DiffieHellmanGroupId;
    }
.end annotation


# static fields
.field public static final DIFFIE_HELLMAN_GROUP_ID_14:J = 0xeL

.field private static final serialVersionUID:J = -0x336abf2651d5b4a9L


# instance fields
.field private generator:Ljava/math/BigInteger;

.field private id:J

.field private prime:Ljava/math/BigInteger;


# direct methods
.method private constructor <init>(JLjava/math/BigInteger;Ljava/math/BigInteger;)V
    .locals 0

    .line 78
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 79
    iput-wide p1, p0, Lee/cyber/smartid/cryptolib/dto/DiffieHellmanGroup;->id:J

    .line 80
    iput-object p3, p0, Lee/cyber/smartid/cryptolib/dto/DiffieHellmanGroup;->prime:Ljava/math/BigInteger;

    .line 81
    iput-object p4, p0, Lee/cyber/smartid/cryptolib/dto/DiffieHellmanGroup;->generator:Ljava/math/BigInteger;

    return-void
.end method

.method public constructor <init>(Lee/cyber/smartid/cryptolib/dto/DiffieHellmanGroup;)V
    .locals 2

    .line 89
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 90
    iget-wide v0, p1, Lee/cyber/smartid/cryptolib/dto/DiffieHellmanGroup;->id:J

    iput-wide v0, p0, Lee/cyber/smartid/cryptolib/dto/DiffieHellmanGroup;->id:J

    .line 91
    iget-object v0, p1, Lee/cyber/smartid/cryptolib/dto/DiffieHellmanGroup;->prime:Ljava/math/BigInteger;

    iput-object v0, p0, Lee/cyber/smartid/cryptolib/dto/DiffieHellmanGroup;->prime:Ljava/math/BigInteger;

    .line 92
    iget-object p1, p1, Lee/cyber/smartid/cryptolib/dto/DiffieHellmanGroup;->generator:Ljava/math/BigInteger;

    iput-object p1, p0, Lee/cyber/smartid/cryptolib/dto/DiffieHellmanGroup;->generator:Ljava/math/BigInteger;

    return-void
.end method

.method public static createGroup(J)Lee/cyber/smartid/cryptolib/dto/DiffieHellmanGroup;
    .locals 4

    const-wide/16 v0, 0xe

    cmp-long v2, p0, v0

    if-nez v2, :cond_0

    .line 68
    new-instance p0, Lee/cyber/smartid/cryptolib/dto/DiffieHellmanGroup;

    new-instance p1, Ljava/math/BigInteger;

    const/16 v2, 0x10

    const-string v3, "FFFFFFFFFFFFFFFFC90FDAA22168C234C4C6628B80DC1CD129024E088A67CC74020BBEA63B139B22514A08798E3404DDEF9519B3CD3A431B302B0A6DF25F14374FE1356D6D51C245E485B576625E7EC6F44C42E9A637ED6B0BFF5CB6F406B7EDEE386BFB5A899FA5AE9F24117C4B1FE649286651ECE45B3DC2007CB8A163BF0598DA48361C55D39A69163FA8FD24CF5F83655D23DCA3AD961C62F356208552BB9ED529077096966D670C354E4ABC9804F1746C08CA18217C32905E462E36CE3BE39E772C180E86039B2783A2EC07A28FB5C55DF06F4C52C9DE2BCBF6955817183995497CEA956AE515D2261898FA051015728E5A8AACAA68FFFFFFFFFFFFFFFF"

    invoke-direct {p1, v3, v2}, Ljava/math/BigInteger;-><init>(Ljava/lang/String;I)V

    const-wide/16 v2, 0x2

    invoke-static {v2, v3}, Ljava/math/BigInteger;->valueOf(J)Ljava/math/BigInteger;

    move-result-object v2

    invoke-direct {p0, v0, v1, p1, v2}, Lee/cyber/smartid/cryptolib/dto/DiffieHellmanGroup;-><init>(JLjava/math/BigInteger;Ljava/math/BigInteger;)V

    return-object p0

    .line 66
    :cond_0
    new-instance v0, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;

    const/16 v1, 0x7c

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Group with id "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string p0, " no supported!"

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v0, v1, p0}, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;-><init>(ILjava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public getGenerator()Ljava/math/BigInteger;
    .locals 1

    .line 121
    iget-object v0, p0, Lee/cyber/smartid/cryptolib/dto/DiffieHellmanGroup;->generator:Ljava/math/BigInteger;

    return-object v0
.end method

.method public getId()J
    .locals 2

    .line 101
    iget-wide v0, p0, Lee/cyber/smartid/cryptolib/dto/DiffieHellmanGroup;->id:J

    return-wide v0
.end method

.method public getPrime()Ljava/math/BigInteger;
    .locals 1

    .line 111
    iget-object v0, p0, Lee/cyber/smartid/cryptolib/dto/DiffieHellmanGroup;->prime:Ljava/math/BigInteger;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .line 126
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "DiffieHellmanGroup{id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lee/cyber/smartid/cryptolib/dto/DiffieHellmanGroup;->id:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ", prime="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lee/cyber/smartid/cryptolib/dto/DiffieHellmanGroup;->prime:Ljava/math/BigInteger;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", generator="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lee/cyber/smartid/cryptolib/dto/DiffieHellmanGroup;->generator:Ljava/math/BigInteger;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
