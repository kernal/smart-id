.class public Lee/cyber/smartid/cryptolib/dto/Run;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lee/cyber/smartid/cryptolib/dto/Run$Type;
    }
.end annotation


# static fields
.field public static final TYPE_BLOCK:I = 0x1

.field public static final TYPE_GAP:I = 0x0

.field private static final serialVersionUID:J = -0x3ac83847cc572384L


# instance fields
.field private count:I

.field private length:J

.field private type:I


# direct methods
.method public constructor <init>(IJI)V
    .locals 0

    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64
    iput p1, p0, Lee/cyber/smartid/cryptolib/dto/Run;->type:I

    .line 65
    iput-wide p2, p0, Lee/cyber/smartid/cryptolib/dto/Run;->length:J

    .line 66
    iput p4, p0, Lee/cyber/smartid/cryptolib/dto/Run;->count:I

    return-void
.end method

.method public constructor <init>(ZJI)V
    .locals 0

    .line 53
    invoke-direct {p0, p1, p2, p3, p4}, Lee/cyber/smartid/cryptolib/dto/Run;-><init>(IJI)V

    return-void
.end method

.method public static getId(IJ)Ljava/lang/String;
    .locals 1

    .line 122
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    if-nez p0, :cond_0

    const-string p0, "gap"

    goto :goto_0

    :cond_0
    const-string p0, "block"

    :goto_0
    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p0, "_"

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static getId(ZJ)Ljava/lang/String;
    .locals 0

    .line 134
    invoke-static {p0, p1, p2}, Lee/cyber/smartid/cryptolib/dto/Run;->getId(IJ)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 1

    .line 146
    instance-of v0, p1, Lee/cyber/smartid/cryptolib/dto/Run;

    if-eqz v0, :cond_0

    .line 147
    invoke-virtual {p0}, Lee/cyber/smartid/cryptolib/dto/Run;->getId()Ljava/lang/String;

    move-result-object v0

    check-cast p1, Lee/cyber/smartid/cryptolib/dto/Run;

    invoke-virtual {p1}, Lee/cyber/smartid/cryptolib/dto/Run;->getId()Ljava/lang/String;

    move-result-object p1

    invoke-static {v0, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result p1

    return p1

    .line 149
    :cond_0
    invoke-super {p0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method public getCount()I
    .locals 1

    .line 93
    iget v0, p0, Lee/cyber/smartid/cryptolib/dto/Run;->count:I

    return v0
.end method

.method public getId()Ljava/lang/String;
    .locals 3

    .line 109
    iget v0, p0, Lee/cyber/smartid/cryptolib/dto/Run;->type:I

    iget-wide v1, p0, Lee/cyber/smartid/cryptolib/dto/Run;->length:J

    invoke-static {v0, v1, v2}, Lee/cyber/smartid/cryptolib/dto/Run;->getId(IJ)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getLength()J
    .locals 2

    .line 84
    iget-wide v0, p0, Lee/cyber/smartid/cryptolib/dto/Run;->length:J

    return-wide v0
.end method

.method public getType()I
    .locals 1

    .line 75
    iget v0, p0, Lee/cyber/smartid/cryptolib/dto/Run;->type:I

    return v0
.end method

.method public incrementCount()V
    .locals 1

    .line 100
    iget v0, p0, Lee/cyber/smartid/cryptolib/dto/Run;->count:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lee/cyber/smartid/cryptolib/dto/Run;->count:I

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .line 159
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Run{type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lee/cyber/smartid/cryptolib/dto/Run;->type:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", length="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lee/cyber/smartid/cryptolib/dto/Run;->length:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ", count="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lee/cyber/smartid/cryptolib/dto/Run;->count:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
