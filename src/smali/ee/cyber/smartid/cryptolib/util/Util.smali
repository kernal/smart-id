.class public Lee/cyber/smartid/cryptolib/util/Util;
.super Ljava/lang/Object;


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static closeClosable(Ljava/io/Closeable;)V
    .locals 0

    if-nez p0, :cond_0

    return-void

    .line 169
    :cond_0
    :try_start_0
    invoke-interface {p0}, Ljava/io/Closeable;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    return-void
.end method

.method public static createStringSequenceFromBooleanArray([ZIIZ)Ljava/lang/String;
    .locals 2

    const/4 v0, 0x0

    if-nez p0, :cond_0

    return-object v0

    .line 138
    :cond_0
    array-length v1, p0

    if-nez v1, :cond_1

    return-object v0

    :cond_1
    add-int/2addr p2, p1

    .line 143
    array-length v1, p0

    if-lt p1, v1, :cond_2

    return-object v0

    :cond_2
    if-ne p2, p1, :cond_3

    return-object v0

    :cond_3
    if-eqz p3, :cond_4

    .line 147
    array-length p3, p0

    if-le p2, p3, :cond_4

    return-object v0

    .line 152
    :cond_4
    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    :goto_0
    if-ge p1, p2, :cond_6

    .line 153
    array-length v0, p0

    if-ge p1, v0, :cond_6

    .line 154
    aget-boolean v0, p0, p1

    if-eqz v0, :cond_5

    const-string v0, "1"

    goto :goto_1

    :cond_5
    const-string v0, "0"

    :goto_1
    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 p1, p1, 0x1

    goto :goto_0

    .line 156
    :cond_6
    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static throwIfEmpty(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;
        }
    .end annotation

    .line 93
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p0

    if-nez p0, :cond_0

    return-void

    .line 94
    :cond_0
    new-instance p0, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;

    const/16 v0, 0x64

    invoke-direct {p0, v0, p1}, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;-><init>(ILjava/lang/String;)V

    throw p0
.end method

.method public static throwIfEmpty([BLjava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;
        }
    .end annotation

    if-eqz p0, :cond_0

    .line 106
    array-length p0, p0

    if-eqz p0, :cond_0

    return-void

    .line 107
    :cond_0
    new-instance p0, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;

    const/16 v0, 0x64

    invoke-direct {p0, v0, p1}, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;-><init>(ILjava/lang/String;)V

    throw p0
.end method

.method public static throwIfEmpty([Lee/cyber/smartid/cryptolib/dto/ValuePair;Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;
        }
    .end annotation

    if-eqz p0, :cond_0

    .line 119
    array-length p0, p0

    if-eqz p0, :cond_0

    return-void

    .line 120
    :cond_0
    new-instance p0, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;

    const/16 v0, 0x64

    invoke-direct {p0, v0, p1}, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;-><init>(ILjava/lang/String;)V

    throw p0
.end method

.method public static throwIfNull(Ljava/io/Closeable;Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;
        }
    .end annotation

    if-eqz p0, :cond_0

    return-void

    .line 81
    :cond_0
    new-instance p0, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;

    const/16 v0, 0x64

    invoke-direct {p0, v0, p1}, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;-><init>(ILjava/lang/String;)V

    throw p0
.end method

.method public static throwIfNull(Ljava/io/Serializable;Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;
        }
    .end annotation

    if-eqz p0, :cond_0

    return-void

    .line 68
    :cond_0
    new-instance p0, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;

    const/16 v0, 0x64

    invoke-direct {p0, v0, p1}, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;-><init>(ILjava/lang/String;)V

    throw p0
.end method

.method public static throwIfNull(Ljava/lang/reflect/Type;Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;
        }
    .end annotation

    if-eqz p0, :cond_0

    return-void

    .line 55
    :cond_0
    new-instance p0, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;

    const/16 v0, 0x64

    invoke-direct {p0, v0, p1}, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;-><init>(ILjava/lang/String;)V

    throw p0
.end method

.method public static throwIfNull(Ljava/math/BigInteger;Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;
        }
    .end annotation

    if-eqz p0, :cond_0

    return-void

    .line 42
    :cond_0
    new-instance p0, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;

    const/16 v0, 0x64

    invoke-direct {p0, v0, p1}, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;-><init>(ILjava/lang/String;)V

    throw p0
.end method
