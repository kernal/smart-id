.class public Lee/cyber/smartid/cryptolib/util/BundleTypeAdapterFactory;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/gson/x;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public create(Lcom/google/gson/f;Lcom/google/gson/b/a;)Lcom/google/gson/w;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/gson/f;",
            "Lcom/google/gson/b/a<",
            "TT;>;)",
            "Lcom/google/gson/w<",
            "TT;>;"
        }
    .end annotation

    .line 53
    const-class v0, Landroid/os/Bundle;

    invoke-virtual {p2}, Lcom/google/gson/b/a;->getRawType()Ljava/lang/Class;

    move-result-object p2

    invoke-virtual {v0, p2}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result p2

    if-nez p2, :cond_0

    const/4 p1, 0x0

    return-object p1

    .line 56
    :cond_0
    new-instance p2, Lee/cyber/smartid/cryptolib/util/BundleTypeAdapterFactory$1;

    invoke-direct {p2, p0, p1}, Lee/cyber/smartid/cryptolib/util/BundleTypeAdapterFactory$1;-><init>(Lee/cyber/smartid/cryptolib/util/BundleTypeAdapterFactory;Lcom/google/gson/f;)V

    return-object p2
.end method
