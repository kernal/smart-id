.class public interface abstract Lee/cyber/smartid/cryptolib/inter/CryptoOp;
.super Ljava/lang/Object;


# static fields
.field public static final JWE_AUDIENCE_VALUE_CLIENT:Ljava/lang/String; = "CLIENT"

.field public static final JWE_AUDIENCE_VALUE_SERVER:Ljava/lang/String; = "SERVER"

.field public static final JWE_PURPOSE_VALUE_CLIENT_2ND_PART:Ljava/lang/String; = "CLIENT2NDPART"

.field public static final JWE_PURPOSE_VALUE_CLIENT_SIGNATURE_SHARE:Ljava/lang/String; = "CLIENTSIGNATURESHARE"

.field public static final JWE_PURPOSE_VALUE_DH:Ljava/lang/String; = "DH"

.field public static final KEY_CLIENT_DIFFIE_HELLMAN_PUBLIC_KEY:Ljava/lang/String; = "clientDhPublicKey"

.field public static final KEY_SERVER_DIFFIE_HELLMAN_PUBLIC_KEY:Ljava/lang/String; = "serverDhPublicKey"


# virtual methods
.method public abstract decryptFromTEKEncryptedJWE(Ljava/lang/String;[BLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/a/a/n;
.end method

.method public abstract decryptKey(Lee/cyber/smartid/cryptolib/dto/ClientShare;Ljava/lang/String;)Ljava/math/BigInteger;
.end method

.method public abstract encryptKey(Ljava/math/BigInteger;Ljava/lang/String;I)Lee/cyber/smartid/cryptolib/dto/ClientShare;
.end method

.method public abstract encryptToKTKEncryptedJWE(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;
.end method

.method public abstract encryptToTEKEncryptedJWE(Ljava/lang/String;[BLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
.end method

.method public abstract isSignedJWS(Ljava/lang/String;)Z
.end method

.method public abstract readKeyIdFromJOSEJWE(Lcom/a/a/n;)Ljava/lang/String;
.end method

.method public abstract verifyKTKSignedJWS(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
.end method

.method public abstract verifyKTKSignedJWSAndCheckForRequiredContentValues(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Lee/cyber/smartid/cryptolib/dto/ValuePair;)Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "[",
            "Lee/cyber/smartid/cryptolib/dto/ValuePair<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation
.end method
