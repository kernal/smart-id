.class public interface abstract Lee/cyber/smartid/cryptolib/inter/EncodingOp;
.super Ljava/lang/Object;


# virtual methods
.method public abstract decodeBytesFromBase64(Ljava/lang/String;)[B
.end method

.method public abstract decodeDecimalFromBase64(Ljava/lang/String;)Ljava/math/BigInteger;
.end method

.method public abstract digestAndEncodeToBase64(Ljava/io/InputStream;Ljava/lang/String;)Ljava/lang/String;
.end method

.method public abstract digestAndEncodeToBase64(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/String;
.end method

.method public abstract digestAndEncodeToBase64([BLjava/lang/String;)Ljava/lang/String;
.end method

.method public abstract encodeBytesToBase64([B)Ljava/lang/String;
.end method

.method public abstract encodeDecimalToBase64(Ljava/math/BigInteger;)Ljava/lang/String;
.end method

.method public abstract encodeI2OSP(Ljava/math/BigInteger;I)[B
.end method

.method public abstract encodePositiveBigIntegerAsBytes(Ljava/math/BigInteger;)[B
.end method

.method public abstract padToSize([BI)[B
.end method

.method public abstract parseAndExtractHeaderFromJWS(Ljava/lang/String;)Ljava/lang/String;
.end method

.method public abstract parseAndExtractPayloadFromJWS(Ljava/lang/String;)Ljava/lang/String;
.end method
