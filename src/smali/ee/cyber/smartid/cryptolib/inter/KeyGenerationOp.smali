.class public interface abstract Lee/cyber/smartid/cryptolib/inter/KeyGenerationOp;
.super Ljava/lang/Object;


# virtual methods
.method public abstract calculateConcatKDFWithSHA256(Lee/cyber/smartid/cryptolib/dto/DiffieHellmanKeyPair;Ljava/math/BigInteger;)[B
.end method

.method public abstract generateDiffieHellmanKeyPair(Lee/cyber/smartid/cryptolib/dto/DiffieHellmanGroup;)Lee/cyber/smartid/cryptolib/dto/DiffieHellmanKeyPair;
.end method

.method public abstract generatePQAndCreateKeyShare(ILjava/math/BigInteger;)[Ljava/lang/String;
.end method
