.class Lee/cyber/smartid/SmartIdService$5;
.super Ljava/lang/Object;
.source "SmartIdService.java"

# interfaces
.implements Lee/cyber/smartid/tse/inter/EncryptKeyListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lee/cyber/smartid/SmartIdService;->a(Lee/cyber/smartid/tse/dto/jsonrpc/resp/EncryptKeyResp;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:Lee/cyber/smartid/tse/dto/jsonrpc/resp/EncryptKeyResp;

.field final synthetic c:Lee/cyber/smartid/SmartIdService;


# direct methods
.method constructor <init>(Lee/cyber/smartid/SmartIdService;Ljava/lang/String;Lee/cyber/smartid/tse/dto/jsonrpc/resp/EncryptKeyResp;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lee/cyber/smartid/SmartIdService$5;->c:Lee/cyber/smartid/SmartIdService;

    iput-object p2, p0, Lee/cyber/smartid/SmartIdService$5;->a:Ljava/lang/String;

    iput-object p3, p0, Lee/cyber/smartid/SmartIdService$5;->b:Lee/cyber/smartid/tse/dto/jsonrpc/resp/EncryptKeyResp;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onEncryptKeyFailed(Ljava/lang/String;Lee/cyber/smartid/tse/dto/TSEError;)V
    .locals 3

    .line 1
    invoke-static {}, Lee/cyber/smartid/SmartIdService;->a()Lee/cyber/smartid/util/Log;

    move-result-object p1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "encryptKeySignInternal - onEncryptKeyFailed:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lee/cyber/smartid/util/Log;->e(Ljava/lang/String;)V

    .line 2
    iget-object p1, p0, Lee/cyber/smartid/SmartIdService$5;->c:Lee/cyber/smartid/SmartIdService;

    iget-object v0, p0, Lee/cyber/smartid/SmartIdService$5;->a:Ljava/lang/String;

    const-class v1, Lee/cyber/smartid/inter/EncryptKeysListener;

    const/4 v2, 0x1

    invoke-static {p1, v0, v2, v1}, Lee/cyber/smartid/SmartIdService;->a(Lee/cyber/smartid/SmartIdService;Ljava/lang/String;ZLjava/lang/Class;)Lee/cyber/smartid/inter/ServiceListener;

    move-result-object p1

    check-cast p1, Lee/cyber/smartid/inter/EncryptKeysListener;

    if-eqz p1, :cond_0

    .line 3
    iget-object v0, p0, Lee/cyber/smartid/SmartIdService$5;->a:Ljava/lang/String;

    iget-object v1, p0, Lee/cyber/smartid/SmartIdService$5;->c:Lee/cyber/smartid/SmartIdService;

    invoke-static {v1}, Lee/cyber/smartid/SmartIdService;->t(Lee/cyber/smartid/SmartIdService;)Lee/cyber/smartid/manager/impl/TSEErrorToServiceErrorMapperImpl;

    move-result-object v1

    invoke-virtual {v1, p2}, Lee/cyber/smartid/manager/impl/TSEErrorToServiceErrorMapperImpl;->map(Lee/cyber/smartid/tse/dto/BaseError;)Lee/cyber/smartid/dto/SmartIdError;

    move-result-object p2

    invoke-interface {p1, v0, p2}, Lee/cyber/smartid/inter/EncryptKeysListener;->onEncryptKeysFailed(Ljava/lang/String;Lee/cyber/smartid/dto/SmartIdError;)V

    :cond_0
    return-void
.end method

.method public onEncryptKeySuccess(Ljava/lang/String;Lee/cyber/smartid/tse/dto/jsonrpc/resp/EncryptKeyResp;)V
    .locals 3

    .line 1
    invoke-static {}, Lee/cyber/smartid/SmartIdService;->a()Lee/cyber/smartid/util/Log;

    move-result-object p1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "encryptKeySignInternal - onEncryptKeySuccess - "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Lee/cyber/smartid/tse/dto/jsonrpc/resp/EncryptKeyResp;->getKeyReference()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    .line 2
    iget-object p1, p0, Lee/cyber/smartid/SmartIdService$5;->c:Lee/cyber/smartid/SmartIdService;

    iget-object v0, p0, Lee/cyber/smartid/SmartIdService$5;->a:Ljava/lang/String;

    const-class v1, Lee/cyber/smartid/inter/EncryptKeysListener;

    const/4 v2, 0x1

    invoke-static {p1, v0, v2, v1}, Lee/cyber/smartid/SmartIdService;->a(Lee/cyber/smartid/SmartIdService;Ljava/lang/String;ZLjava/lang/Class;)Lee/cyber/smartid/inter/ServiceListener;

    move-result-object p1

    check-cast p1, Lee/cyber/smartid/inter/EncryptKeysListener;

    if-eqz p1, :cond_0

    .line 3
    iget-object v0, p0, Lee/cyber/smartid/SmartIdService$5;->a:Ljava/lang/String;

    new-instance v1, Lee/cyber/smartid/dto/jsonrpc/resp/EncryptKeysResp;

    iget-object v2, p0, Lee/cyber/smartid/SmartIdService$5;->b:Lee/cyber/smartid/tse/dto/jsonrpc/resp/EncryptKeyResp;

    invoke-virtual {v2}, Lee/cyber/smartid/tse/dto/jsonrpc/resp/EncryptKeyResp;->getKeyReference()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2}, Lee/cyber/smartid/tse/dto/jsonrpc/resp/EncryptKeyResp;->getKeyReference()Ljava/lang/String;

    move-result-object p2

    invoke-direct {v1, v0, v2, p2}, Lee/cyber/smartid/dto/jsonrpc/resp/EncryptKeysResp;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {p1, v0, v1}, Lee/cyber/smartid/inter/EncryptKeysListener;->onEncryptKeysSuccess(Ljava/lang/String;Lee/cyber/smartid/dto/jsonrpc/resp/EncryptKeysResp;)V

    :cond_0
    return-void
.end method
