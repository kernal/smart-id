.class Lee/cyber/smartid/SmartIdService$32;
.super Lee/cyber/smartid/tse/network/RPCCallback;
.source "SmartIdService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lee/cyber/smartid/SmartIdService;->a(Ljava/lang/String;Lee/cyber/smartid/inter/UpdateDeviceListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lee/cyber/smartid/tse/network/RPCCallback<",
        "Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCResponse<",
        "Lee/cyber/smartid/dto/jsonrpc/result/UpdateDeviceResult;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lee/cyber/smartid/dto/jsonrpc/param/RegisterDeviceData;

.field final synthetic b:Ljava/lang/String;

.field final synthetic c:Ljava/lang/String;

.field final synthetic d:Lee/cyber/smartid/SmartIdService;


# direct methods
.method constructor <init>(Lee/cyber/smartid/SmartIdService;Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCRequest;Lee/cyber/smartid/tse/SmartIdTSE;Lee/cyber/smartid/dto/jsonrpc/param/RegisterDeviceData;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lee/cyber/smartid/SmartIdService$32;->d:Lee/cyber/smartid/SmartIdService;

    iput-object p4, p0, Lee/cyber/smartid/SmartIdService$32;->a:Lee/cyber/smartid/dto/jsonrpc/param/RegisterDeviceData;

    iput-object p5, p0, Lee/cyber/smartid/SmartIdService$32;->b:Ljava/lang/String;

    iput-object p6, p0, Lee/cyber/smartid/SmartIdService$32;->c:Ljava/lang/String;

    invoke-direct {p0, p2, p3}, Lee/cyber/smartid/tse/network/RPCCallback;-><init>(Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCRequest;Lee/cyber/smartid/tse/SmartIdTSE;)V

    return-void
.end method


# virtual methods
.method public onFailure(Lc/b;Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCError;Ljava/lang/Throwable;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lc/b<",
            "Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCResponse<",
            "Lee/cyber/smartid/dto/jsonrpc/result/UpdateDeviceResult;",
            ">;>;",
            "Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCError;",
            "Ljava/lang/Throwable;",
            ")V"
        }
    .end annotation

    .line 1
    :try_start_0
    iget-object p1, p0, Lee/cyber/smartid/SmartIdService$32;->d:Lee/cyber/smartid/SmartIdService;

    invoke-static {p1}, Lee/cyber/smartid/SmartIdService;->G(Lee/cyber/smartid/SmartIdService;)V
    :try_end_0
    .catch Lee/cyber/smartid/cryptolib/dto/StorageException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 2
    invoke-static {}, Lee/cyber/smartid/SmartIdService;->a()Lee/cyber/smartid/util/Log;

    move-result-object v0

    const-string v1, "updateDevice - onFailure"

    invoke-virtual {v0, v1, p1}, Lee/cyber/smartid/util/Log;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 3
    :goto_0
    iget-object p1, p0, Lee/cyber/smartid/SmartIdService$32;->d:Lee/cyber/smartid/SmartIdService;

    new-instance v0, Lee/cyber/smartid/SmartIdService$32$2;

    invoke-direct {v0, p0, p2, p3}, Lee/cyber/smartid/SmartIdService$32$2;-><init>(Lee/cyber/smartid/SmartIdService$32;Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCError;Ljava/lang/Throwable;)V

    invoke-static {p1, v0}, Lee/cyber/smartid/SmartIdService;->a(Lee/cyber/smartid/SmartIdService;Ljava/lang/Runnable;)V

    return-void
.end method

.method public onSuccess(Lc/b;Lc/r;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lc/b<",
            "Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCResponse<",
            "Lee/cyber/smartid/dto/jsonrpc/result/UpdateDeviceResult;",
            ">;>;",
            "Lc/r<",
            "Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCResponse<",
            "Lee/cyber/smartid/dto/jsonrpc/result/UpdateDeviceResult;",
            ">;>;)V"
        }
    .end annotation

    .line 1
    :try_start_0
    iget-object p1, p0, Lee/cyber/smartid/SmartIdService$32;->d:Lee/cyber/smartid/SmartIdService;

    iget-object v0, p0, Lee/cyber/smartid/SmartIdService$32;->a:Lee/cyber/smartid/dto/jsonrpc/param/RegisterDeviceData;

    iget-object v1, p0, Lee/cyber/smartid/SmartIdService$32;->b:Ljava/lang/String;

    invoke-static {p1, v0, v1}, Lee/cyber/smartid/SmartIdService;->a(Lee/cyber/smartid/SmartIdService;Lee/cyber/smartid/dto/jsonrpc/param/RegisterDeviceData;Ljava/lang/String;)V

    .line 2
    iget-object p1, p0, Lee/cyber/smartid/SmartIdService$32;->d:Lee/cyber/smartid/SmartIdService;

    invoke-static {p1}, Lee/cyber/smartid/SmartIdService;->F(Lee/cyber/smartid/SmartIdService;)V
    :try_end_0
    .catch Lee/cyber/smartid/cryptolib/dto/StorageException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 3
    invoke-static {}, Lee/cyber/smartid/SmartIdService;->a()Lee/cyber/smartid/util/Log;

    move-result-object v0

    const-string v1, "updateDevice"

    invoke-virtual {v0, v1, p1}, Lee/cyber/smartid/util/Log;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 4
    :goto_0
    iget-object p1, p0, Lee/cyber/smartid/SmartIdService$32;->d:Lee/cyber/smartid/SmartIdService;

    new-instance v0, Lee/cyber/smartid/SmartIdService$32$1;

    invoke-direct {v0, p0, p2}, Lee/cyber/smartid/SmartIdService$32$1;-><init>(Lee/cyber/smartid/SmartIdService$32;Lc/r;)V

    invoke-static {p1, v0}, Lee/cyber/smartid/SmartIdService;->a(Lee/cyber/smartid/SmartIdService;Ljava/lang/Runnable;)V

    return-void
.end method

.method public onValidate(Lc/b;Lc/r;)Z
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lc/b<",
            "Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCResponse<",
            "Lee/cyber/smartid/dto/jsonrpc/result/UpdateDeviceResult;",
            ">;>;",
            "Lc/r<",
            "Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCResponse<",
            "Lee/cyber/smartid/dto/jsonrpc/result/UpdateDeviceResult;",
            ">;>;)Z"
        }
    .end annotation

    .line 1
    invoke-static {p2}, Lee/cyber/smartid/util/Util;->validateResponse(Lc/r;)Z

    move-result p1

    return p1
.end method
