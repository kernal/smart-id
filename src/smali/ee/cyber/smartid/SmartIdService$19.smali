.class Lee/cyber/smartid/SmartIdService$19;
.super Ljava/lang/Object;
.source "SmartIdService.java"

# interfaces
.implements Lee/cyber/smartid/tse/inter/CheckLocalPendingStateListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lee/cyber/smartid/SmartIdService;->checkLocalPendingStateForKey(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/inter/CheckLocalPendingStateListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:Ljava/lang/String;

.field final synthetic c:Lee/cyber/smartid/SmartIdService;


# direct methods
.method constructor <init>(Lee/cyber/smartid/SmartIdService;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lee/cyber/smartid/SmartIdService$19;->c:Lee/cyber/smartid/SmartIdService;

    iput-object p2, p0, Lee/cyber/smartid/SmartIdService$19;->a:Ljava/lang/String;

    iput-object p3, p0, Lee/cyber/smartid/SmartIdService$19;->b:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCheckLocalPendingStateFailed(Ljava/lang/String;Lee/cyber/smartid/tse/dto/TSEError;)V
    .locals 3

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/SmartIdService$19;->c:Lee/cyber/smartid/SmartIdService;

    const-class v1, Lee/cyber/smartid/inter/CheckLocalPendingStateListener;

    const/4 v2, 0x1

    invoke-static {v0, p1, v2, v1}, Lee/cyber/smartid/SmartIdService;->a(Lee/cyber/smartid/SmartIdService;Ljava/lang/String;ZLjava/lang/Class;)Lee/cyber/smartid/inter/ServiceListener;

    move-result-object v0

    check-cast v0, Lee/cyber/smartid/inter/CheckLocalPendingStateListener;

    if-eqz v0, :cond_0

    .line 2
    iget-object v1, p0, Lee/cyber/smartid/SmartIdService$19;->c:Lee/cyber/smartid/SmartIdService;

    invoke-static {v1}, Lee/cyber/smartid/SmartIdService;->t(Lee/cyber/smartid/SmartIdService;)Lee/cyber/smartid/manager/impl/TSEErrorToServiceErrorMapperImpl;

    move-result-object v1

    invoke-virtual {v1, p2}, Lee/cyber/smartid/manager/impl/TSEErrorToServiceErrorMapperImpl;->map(Lee/cyber/smartid/tse/dto/BaseError;)Lee/cyber/smartid/dto/SmartIdError;

    move-result-object p2

    invoke-interface {v0, p1, p2}, Lee/cyber/smartid/inter/CheckLocalPendingStateListener;->onCheckLocalPendingStateFailed(Ljava/lang/String;Lee/cyber/smartid/dto/SmartIdError;)V

    :cond_0
    return-void
.end method

.method public onCheckLocalPendingStateSuccess(Ljava/lang/String;Lee/cyber/smartid/tse/dto/jsonrpc/resp/CheckLocalPendingStateResp;)V
    .locals 9

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/SmartIdService$19;->c:Lee/cyber/smartid/SmartIdService;

    const-class v1, Lee/cyber/smartid/inter/CheckLocalPendingStateListener;

    const/4 v2, 0x1

    invoke-static {v0, p1, v2, v1}, Lee/cyber/smartid/SmartIdService;->a(Lee/cyber/smartid/SmartIdService;Ljava/lang/String;ZLjava/lang/Class;)Lee/cyber/smartid/inter/ServiceListener;

    move-result-object v0

    check-cast v0, Lee/cyber/smartid/inter/CheckLocalPendingStateListener;

    if-eqz v0, :cond_1

    .line 2
    invoke-virtual {p2}, Lee/cyber/smartid/tse/dto/jsonrpc/resp/CheckLocalPendingStateResp;->hasLocalPendingState()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p2}, Lee/cyber/smartid/tse/dto/jsonrpc/resp/CheckLocalPendingStateResp;->getLocalPendingStateType()Ljava/lang/String;

    move-result-object v1

    const-string v2, "confirmTransaction"

    invoke-static {v1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lee/cyber/smartid/SmartIdService$19;->c:Lee/cyber/smartid/SmartIdService;

    iget-object v2, p0, Lee/cyber/smartid/SmartIdService$19;->a:Ljava/lang/String;

    iget-object v3, p0, Lee/cyber/smartid/SmartIdService$19;->b:Ljava/lang/String;

    const-string v4, "confirm_transaction"

    invoke-static {v1, v4, v2, v3}, Lee/cyber/smartid/SmartIdService;->a(Lee/cyber/smartid/SmartIdService;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    move-object v6, v1

    .line 3
    new-instance v1, Lee/cyber/smartid/dto/jsonrpc/resp/CheckLocalPendingStateResp;

    invoke-virtual {p2}, Lee/cyber/smartid/tse/dto/jsonrpc/resp/CheckLocalPendingStateResp;->getTag()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lee/cyber/smartid/SmartIdService$19;->a:Ljava/lang/String;

    iget-object v5, p0, Lee/cyber/smartid/SmartIdService$19;->b:Ljava/lang/String;

    invoke-virtual {p2}, Lee/cyber/smartid/tse/dto/jsonrpc/resp/CheckLocalPendingStateResp;->getLocalPendingStateType()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p2}, Lee/cyber/smartid/tse/dto/jsonrpc/resp/CheckLocalPendingStateResp;->getTransactionUUID()Ljava/lang/String;

    move-result-object v8

    move-object v2, v1

    invoke-direct/range {v2 .. v8}, Lee/cyber/smartid/dto/jsonrpc/resp/CheckLocalPendingStateResp;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, p1, v1}, Lee/cyber/smartid/inter/CheckLocalPendingStateListener;->onCheckLocalPendingStateSuccess(Ljava/lang/String;Lee/cyber/smartid/dto/jsonrpc/resp/CheckLocalPendingStateResp;)V

    :cond_1
    return-void
.end method
