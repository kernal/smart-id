.class Lee/cyber/smartid/SmartIdService$20;
.super Ljava/lang/Object;
.source "SmartIdService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lee/cyber/smartid/SmartIdService;->retryLocalPendingState(Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/inter/ServiceListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:Ljava/lang/String;

.field final synthetic c:Lee/cyber/smartid/inter/ServiceListener;

.field final synthetic d:Lee/cyber/smartid/SmartIdService;


# direct methods
.method constructor <init>(Lee/cyber/smartid/SmartIdService;Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/inter/ServiceListener;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lee/cyber/smartid/SmartIdService$20;->d:Lee/cyber/smartid/SmartIdService;

    iput-object p2, p0, Lee/cyber/smartid/SmartIdService$20;->a:Ljava/lang/String;

    iput-object p3, p0, Lee/cyber/smartid/SmartIdService$20;->b:Ljava/lang/String;

    iput-object p4, p0, Lee/cyber/smartid/SmartIdService$20;->c:Lee/cyber/smartid/inter/ServiceListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/SmartIdService$20;->a:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lee/cyber/smartid/SmartIdService$20;->a:Ljava/lang/String;

    const-string v1, "confirm_transaction"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2
    iget-object v0, p0, Lee/cyber/smartid/SmartIdService$20;->d:Lee/cyber/smartid/SmartIdService;

    iget-object v1, p0, Lee/cyber/smartid/SmartIdService$20;->a:Ljava/lang/String;

    iget-object v2, p0, Lee/cyber/smartid/SmartIdService$20;->b:Ljava/lang/String;

    iget-object v3, p0, Lee/cyber/smartid/SmartIdService$20;->c:Lee/cyber/smartid/inter/ServiceListener;

    invoke-static {v0, v1, v2, v3}, Lee/cyber/smartid/SmartIdService;->a(Lee/cyber/smartid/SmartIdService;Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/inter/ServiceListener;)V

    goto :goto_0

    .line 3
    :cond_0
    iget-object v0, p0, Lee/cyber/smartid/SmartIdService$20;->a:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lee/cyber/smartid/SmartIdService$20;->a:Ljava/lang/String;

    const-string v1, "add_account"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 4
    iget-object v0, p0, Lee/cyber/smartid/SmartIdService$20;->d:Lee/cyber/smartid/SmartIdService;

    iget-object v1, p0, Lee/cyber/smartid/SmartIdService$20;->a:Ljava/lang/String;

    iget-object v2, p0, Lee/cyber/smartid/SmartIdService$20;->b:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lee/cyber/smartid/SmartIdService;->b(Lee/cyber/smartid/SmartIdService;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 5
    :cond_1
    iget-object v0, p0, Lee/cyber/smartid/SmartIdService$20;->d:Lee/cyber/smartid/SmartIdService;

    new-instance v1, Lee/cyber/smartid/SmartIdService$20$1;

    invoke-direct {v1, p0}, Lee/cyber/smartid/SmartIdService$20$1;-><init>(Lee/cyber/smartid/SmartIdService$20;)V

    invoke-static {v0, v1}, Lee/cyber/smartid/SmartIdService;->a(Lee/cyber/smartid/SmartIdService;Ljava/lang/Runnable;)V

    :goto_0
    return-void
.end method
