.class final Lee/cyber/smartid/tse/KeyStateManager;
.super Ljava/lang/Object;
.source "KeyStateManager.java"


# static fields
.field private static volatile a:Lee/cyber/smartid/tse/KeyStateManager;


# instance fields
.field private final b:Ljava/util/concurrent/Executor;

.field private final c:Lee/cyber/smartid/tse/inter/ResourceAccess;

.field private final d:Lee/cyber/smartid/tse/inter/KeyManagerAccess;

.field private final e:Lee/cyber/smartid/tse/inter/ListenerAccess;

.field private final f:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final g:Landroid/os/PowerManager$WakeLock;

.field private final h:Lee/cyber/smartid/tse/network/TSEAPI$TSEAPIService;

.field private final i:Ljava/util/concurrent/ConcurrentLinkedQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentLinkedQueue<",
            "Lee/cyber/smartid/tse/dto/KeyStateQueue;",
            ">;"
        }
    .end annotation
.end field

.field private final j:Ljava/lang/Object;

.field private final k:Ljava/lang/Object;

.field private final l:Lee/cyber/smartid/tse/SmartIdTSE$TSEAlarmAccess;

.field private final m:Lee/cyber/smartid/cryptolib/inter/SigningOp;

.field private final n:Lee/cyber/smartid/cryptolib/inter/EncodingOp;

.field private final o:Lee/cyber/smartid/cryptolib/inter/RandomGenerationOp;

.field private final p:Lee/cyber/smartid/cryptolib/inter/CryptoOp;

.field private final q:Lee/cyber/smartid/tse/inter/ExternalResourceAccess;

.field private final r:Lee/cyber/smartid/tse/inter/WallClock;

.field private s:Lee/cyber/smartid/tse/inter/StateWorkerJobListener;

.field private final t:Lee/cyber/smartid/tse/inter/LogAccess;


# direct methods
.method private constructor <init>(Ljava/util/concurrent/Executor;Lee/cyber/smartid/tse/inter/ResourceAccess;Lee/cyber/smartid/tse/network/TSEAPI$TSEAPIService;Lee/cyber/smartid/tse/inter/ListenerAccess;Lee/cyber/smartid/tse/inter/KeyManagerAccess;Lee/cyber/smartid/tse/SmartIdTSE$TSEAlarmAccess;Lee/cyber/smartid/cryptolib/inter/SigningOp;Lee/cyber/smartid/cryptolib/inter/EncodingOp;Lee/cyber/smartid/cryptolib/inter/RandomGenerationOp;Lee/cyber/smartid/cryptolib/inter/CryptoOp;Lee/cyber/smartid/tse/inter/ExternalResourceAccess;Lee/cyber/smartid/tse/inter/WallClock;)V
    .locals 1

    .line 243
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 118
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lee/cyber/smartid/tse/KeyStateManager;->f:Ljava/util/HashSet;

    .line 130
    new-instance v0, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    iput-object v0, p0, Lee/cyber/smartid/tse/KeyStateManager;->i:Ljava/util/concurrent/ConcurrentLinkedQueue;

    .line 134
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lee/cyber/smartid/tse/KeyStateManager;->j:Ljava/lang/Object;

    .line 138
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lee/cyber/smartid/tse/KeyStateManager;->k:Ljava/lang/Object;

    .line 175
    invoke-static {p0}, Lee/cyber/smartid/tse/util/Log;->getInstance(Ljava/lang/Object;)Lee/cyber/smartid/tse/util/Log;

    move-result-object v0

    iput-object v0, p0, Lee/cyber/smartid/tse/KeyStateManager;->t:Lee/cyber/smartid/tse/inter/LogAccess;

    .line 244
    iput-object p2, p0, Lee/cyber/smartid/tse/KeyStateManager;->c:Lee/cyber/smartid/tse/inter/ResourceAccess;

    .line 245
    iput-object p3, p0, Lee/cyber/smartid/tse/KeyStateManager;->h:Lee/cyber/smartid/tse/network/TSEAPI$TSEAPIService;

    .line 246
    iput-object p1, p0, Lee/cyber/smartid/tse/KeyStateManager;->b:Ljava/util/concurrent/Executor;

    .line 247
    iput-object p4, p0, Lee/cyber/smartid/tse/KeyStateManager;->e:Lee/cyber/smartid/tse/inter/ListenerAccess;

    .line 248
    iput-object p5, p0, Lee/cyber/smartid/tse/KeyStateManager;->d:Lee/cyber/smartid/tse/inter/KeyManagerAccess;

    .line 249
    iput-object p6, p0, Lee/cyber/smartid/tse/KeyStateManager;->l:Lee/cyber/smartid/tse/SmartIdTSE$TSEAlarmAccess;

    .line 250
    iput-object p7, p0, Lee/cyber/smartid/tse/KeyStateManager;->m:Lee/cyber/smartid/cryptolib/inter/SigningOp;

    .line 251
    iput-object p8, p0, Lee/cyber/smartid/tse/KeyStateManager;->n:Lee/cyber/smartid/cryptolib/inter/EncodingOp;

    .line 252
    iput-object p9, p0, Lee/cyber/smartid/tse/KeyStateManager;->o:Lee/cyber/smartid/cryptolib/inter/RandomGenerationOp;

    .line 253
    iput-object p10, p0, Lee/cyber/smartid/tse/KeyStateManager;->p:Lee/cyber/smartid/cryptolib/inter/CryptoOp;

    .line 254
    iput-object p11, p0, Lee/cyber/smartid/tse/KeyStateManager;->q:Lee/cyber/smartid/tse/inter/ExternalResourceAccess;

    .line 255
    iput-object p12, p0, Lee/cyber/smartid/tse/KeyStateManager;->r:Lee/cyber/smartid/tse/inter/WallClock;

    .line 256
    invoke-interface {p2}, Lee/cyber/smartid/tse/inter/ResourceAccess;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    const-string p2, "power"

    invoke-virtual {p1, p2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/os/PowerManager;

    if-eqz p1, :cond_0

    const/4 p2, 0x1

    const-string p3, "ee.cyber.smartid.WAKELOCK_KEY_STATE_WORKER"

    .line 257
    invoke-virtual {p1, p2, p3}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    iput-object p1, p0, Lee/cyber/smartid/tse/KeyStateManager;->g:Landroid/os/PowerManager$WakeLock;

    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lee/cyber/smartid/tse/dto/ProtoKeyState;
    .locals 1

    .line 1024
    :try_start_0
    invoke-direct {p0}, Lee/cyber/smartid/tse/KeyStateManager;->b()Ljava/lang/String;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1029
    invoke-static {p1, p2, p3, p4, v0}, Lee/cyber/smartid/tse/dto/ProtoKeyState;->forCloneDetection(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lee/cyber/smartid/tse/dto/ProtoKeyState;

    move-result-object p1

    return-object p1

    :catch_0
    const/4 p1, 0x0

    return-object p1
.end method

.method static synthetic a(Lee/cyber/smartid/tse/KeyStateManager;)Lee/cyber/smartid/tse/inter/StateWorkerJobListener;
    .locals 0

    .line 68
    iget-object p0, p0, Lee/cyber/smartid/tse/KeyStateManager;->s:Lee/cyber/smartid/tse/inter/StateWorkerJobListener;

    return-object p0
.end method

.method private a(Ljava/lang/String;Lee/cyber/smartid/tse/dto/ProtoKeyState;)Ljava/lang/String;
    .locals 1

    .line 1146
    invoke-virtual {p2}, Lee/cyber/smartid/tse/dto/ProtoKeyState;->isActiveSubmitClientSecondPart()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1147
    const-class v0, Lee/cyber/smartid/tse/inter/SubmitClientSecondPartListener;

    goto :goto_0

    .line 1148
    :cond_0
    invoke-virtual {p2}, Lee/cyber/smartid/tse/dto/ProtoKeyState;->isActiveCloneDetection()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1149
    const-class v0, Lee/cyber/smartid/tse/inter/RefreshCloneDetectionListener;

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    .line 1151
    :goto_0
    invoke-virtual {p2}, Lee/cyber/smartid/tse/dto/ProtoKeyState;->getKeyId()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p0, p1, p2, v0}, Lee/cyber/smartid/tse/KeyStateManager;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private a()V
    .locals 8

    .line 448
    iget-object v0, p0, Lee/cyber/smartid/tse/KeyStateManager;->j:Ljava/lang/Object;

    monitor-enter v0

    .line 450
    :try_start_0
    invoke-virtual {p0}, Lee/cyber/smartid/tse/KeyStateManager;->getKeyStateQueueSize()I

    move-result v1

    if-nez v1, :cond_0

    .line 452
    iget-object v1, p0, Lee/cyber/smartid/tse/KeyStateManager;->t:Lee/cyber/smartid/tse/inter/LogAccess;

    const-string v2, "handleQueuedKeyState: queue is empty, we are done"

    invoke-interface {v1, v2}, Lee/cyber/smartid/tse/inter/LogAccess;->d(Ljava/lang/String;)V

    .line 453
    monitor-exit v0

    return-void

    :cond_0
    const/4 v1, 0x0

    .line 459
    iget-object v2, p0, Lee/cyber/smartid/tse/KeyStateManager;->i:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v2}, Ljava/util/concurrent/ConcurrentLinkedQueue;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lee/cyber/smartid/tse/dto/KeyStateQueue;

    .line 460
    invoke-virtual {v3}, Lee/cyber/smartid/tse/dto/KeyStateQueue;->getState()Lee/cyber/smartid/tse/dto/ProtoKeyState;

    move-result-object v4

    invoke-virtual {v4}, Lee/cyber/smartid/tse/dto/ProtoKeyState;->getId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Lee/cyber/smartid/tse/KeyStateManager;->isKeyStateInProgress(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 461
    iget-object v4, p0, Lee/cyber/smartid/tse/KeyStateManager;->t:Lee/cyber/smartid/tse/inter/LogAccess;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "handleQueuedKeyState: State "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v3, " still in progress"

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v4, v3}, Lee/cyber/smartid/tse/inter/LogAccess;->d(Ljava/lang/String;)V

    goto :goto_0

    .line 463
    :cond_1
    iget-object v1, p0, Lee/cyber/smartid/tse/KeyStateManager;->t:Lee/cyber/smartid/tse/inter/LogAccess;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "handleQueuedKeyState: State "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v4, " can be processed now, continuing .."

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lee/cyber/smartid/tse/inter/LogAccess;->d(Ljava/lang/String;)V

    move-object v1, v3

    :cond_2
    if-nez v1, :cond_3

    .line 470
    iget-object v1, p0, Lee/cyber/smartid/tse/KeyStateManager;->t:Lee/cyber/smartid/tse/inter/LogAccess;

    const-string v2, "handleQueuedKeyState - Didn\'t find any states to start, aborting .."

    invoke-interface {v1, v2}, Lee/cyber/smartid/tse/inter/LogAccess;->d(Ljava/lang/String;)V

    .line 471
    monitor-exit v0

    return-void

    .line 475
    :cond_3
    iget-object v2, p0, Lee/cyber/smartid/tse/KeyStateManager;->i:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v2, v1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->remove(Ljava/lang/Object;)Z

    .line 478
    invoke-virtual {v1}, Lee/cyber/smartid/tse/dto/KeyStateQueue;->getState()Lee/cyber/smartid/tse/dto/ProtoKeyState;

    move-result-object v2

    .line 479
    invoke-virtual {v1}, Lee/cyber/smartid/tse/dto/KeyStateQueue;->getListener()Lee/cyber/smartid/tse/inter/TSEListener;

    move-result-object v3

    .line 480
    invoke-virtual {v1}, Lee/cyber/smartid/tse/dto/KeyStateQueue;->getTag()Ljava/lang/String;

    move-result-object v4

    .line 481
    invoke-virtual {v1}, Lee/cyber/smartid/tse/dto/KeyStateQueue;->getExtras()Landroid/os/Bundle;

    move-result-object v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 485
    :try_start_1
    iget-object v5, p0, Lee/cyber/smartid/tse/KeyStateManager;->s:Lee/cyber/smartid/tse/inter/StateWorkerJobListener;

    if-eqz v5, :cond_4

    .line 486
    iget-object v5, p0, Lee/cyber/smartid/tse/KeyStateManager;->s:Lee/cyber/smartid/tse/inter/StateWorkerJobListener;

    invoke-virtual {v2}, Lee/cyber/smartid/tse/dto/ProtoKeyState;->getId()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v5, v6}, Lee/cyber/smartid/tse/inter/StateWorkerJobListener;->onJobDequeued(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :catch_0
    move-exception v5

    .line 489
    :try_start_2
    iget-object v6, p0, Lee/cyber/smartid/tse/KeyStateManager;->t:Lee/cyber/smartid/tse/inter/LogAccess;

    const-string v7, "handleQueuedKeyState"

    invoke-interface {v6, v7, v5}, Lee/cyber/smartid/tse/inter/LogAccess;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 491
    :cond_4
    :goto_1
    iget-object v5, p0, Lee/cyber/smartid/tse/KeyStateManager;->t:Lee/cyber/smartid/tse/inter/LogAccess;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "handleQueuedKeyState starting "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v7, " (remaining in queue: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v7, p0, Lee/cyber/smartid/tse/KeyStateManager;->i:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v7}, Ljava/util/concurrent/ConcurrentLinkedQueue;->size()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v7, ")"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v5, v6}, Lee/cyber/smartid/tse/inter/LogAccess;->d(Ljava/lang/String;)V

    .line 492
    invoke-direct {p0, v4, v2, v1, v3}, Lee/cyber/smartid/tse/KeyStateManager;->c(Ljava/lang/String;Lee/cyber/smartid/tse/dto/ProtoKeyState;Landroid/os/Bundle;Lee/cyber/smartid/tse/inter/TSEListener;)V

    .line 493
    monitor-exit v0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1
.end method

.method static synthetic a(Lee/cyber/smartid/tse/KeyStateManager;Lee/cyber/smartid/tse/inter/TSEListener;Ljava/lang/String;Lee/cyber/smartid/tse/dto/TSEError;)V
    .locals 0

    .line 68
    invoke-direct {p0, p1, p2, p3}, Lee/cyber/smartid/tse/KeyStateManager;->a(Lee/cyber/smartid/tse/inter/TSEListener;Ljava/lang/String;Lee/cyber/smartid/tse/dto/TSEError;)V

    return-void
.end method

.method static synthetic a(Lee/cyber/smartid/tse/KeyStateManager;Ljava/lang/Runnable;)V
    .locals 0

    .line 68
    invoke-direct {p0, p1}, Lee/cyber/smartid/tse/KeyStateManager;->a(Ljava/lang/Runnable;)V

    return-void
.end method

.method static synthetic a(Lee/cyber/smartid/tse/KeyStateManager;Ljava/lang/String;)V
    .locals 0

    .line 68
    invoke-direct {p0, p1}, Lee/cyber/smartid/tse/KeyStateManager;->a(Ljava/lang/String;)V

    return-void
.end method

.method private a(Lee/cyber/smartid/tse/dto/ProtoKeyState;)V
    .locals 5

    .line 338
    iget-object v0, p0, Lee/cyber/smartid/tse/KeyStateManager;->j:Ljava/lang/Object;

    monitor-enter v0

    .line 339
    :try_start_0
    iget-object v1, p0, Lee/cyber/smartid/tse/KeyStateManager;->i:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->size()I

    move-result v1

    if-nez v1, :cond_0

    .line 340
    monitor-exit v0

    return-void

    .line 342
    :cond_0
    iget-object v1, p0, Lee/cyber/smartid/tse/KeyStateManager;->i:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lee/cyber/smartid/tse/dto/KeyStateQueue;

    if-eqz v2, :cond_1

    .line 343
    invoke-virtual {v2}, Lee/cyber/smartid/tse/dto/KeyStateQueue;->getState()Lee/cyber/smartid/tse/dto/ProtoKeyState;

    move-result-object v3

    invoke-virtual {p1, v3}, Lee/cyber/smartid/tse/dto/ProtoKeyState;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 344
    iget-object v3, p0, Lee/cyber/smartid/tse/KeyStateManager;->t:Lee/cyber/smartid/tse/inter/LogAccess;

    const-string v4, "dequeueKeyStateIfNeeded - same job is also queued, clearing the queue!"

    invoke-interface {v3, v4}, Lee/cyber/smartid/tse/inter/LogAccess;->w(Ljava/lang/String;)V

    .line 345
    iget-object v3, p0, Lee/cyber/smartid/tse/KeyStateManager;->s:Lee/cyber/smartid/tse/inter/StateWorkerJobListener;

    if-eqz v3, :cond_2

    .line 346
    iget-object v3, p0, Lee/cyber/smartid/tse/KeyStateManager;->s:Lee/cyber/smartid/tse/inter/StateWorkerJobListener;

    invoke-virtual {v2}, Lee/cyber/smartid/tse/dto/KeyStateQueue;->getState()Lee/cyber/smartid/tse/dto/ProtoKeyState;

    move-result-object v4

    invoke-virtual {v4}, Lee/cyber/smartid/tse/dto/ProtoKeyState;->getId()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Lee/cyber/smartid/tse/inter/StateWorkerJobListener;->onJobDequeued(Ljava/lang/String;)V

    .line 348
    :cond_2
    iget-object v3, p0, Lee/cyber/smartid/tse/KeyStateManager;->i:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v3, v2}, Ljava/util/concurrent/ConcurrentLinkedQueue;->remove(Ljava/lang/Object;)Z

    goto :goto_0

    .line 351
    :cond_3
    monitor-exit v0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method private a(Lee/cyber/smartid/tse/inter/TSEListener;Ljava/lang/String;Lee/cyber/smartid/tse/dto/TSEError;)V
    .locals 2

    if-eqz p1, :cond_1

    if-nez p3, :cond_0

    goto :goto_0

    .line 540
    :cond_0
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    new-instance v1, Lee/cyber/smartid/tse/KeyStateManager$2;

    invoke-direct {v1, p0, p2, p1, p3}, Lee/cyber/smartid/tse/KeyStateManager$2;-><init>(Lee/cyber/smartid/tse/KeyStateManager;Ljava/lang/String;Lee/cyber/smartid/tse/inter/TSEListener;Lee/cyber/smartid/tse/dto/TSEError;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_1
    :goto_0
    return-void
.end method

.method private a(Ljava/lang/Runnable;)V
    .locals 1

    .line 532
    iget-object v0, p0, Lee/cyber/smartid/tse/KeyStateManager;->e:Lee/cyber/smartid/tse/inter/ListenerAccess;

    invoke-interface {v0, p1}, Lee/cyber/smartid/tse/inter/ListenerAccess;->notifyUI(Ljava/lang/Runnable;)V

    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 2

    .line 551
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 554
    :cond_0
    iget-object v0, p0, Lee/cyber/smartid/tse/KeyStateManager;->j:Ljava/lang/Object;

    monitor-enter v0

    .line 555
    :try_start_0
    iget-object v1, p0, Lee/cyber/smartid/tse/KeyStateManager;->f:Ljava/util/HashSet;

    invoke-virtual {v1, p1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 556
    monitor-exit v0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method private a(Ljava/lang/String;J)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lee/cyber/smartid/cryptolib/dto/StorageException;
        }
    .end annotation

    .line 1006
    iget-object v0, p0, Lee/cyber/smartid/tse/KeyStateManager;->t:Lee/cyber/smartid/tse/inter/LogAccess;

    const-string v1, "setRefreshCloneDetectionSingleKeyAlarm"

    invoke-interface {v0, v1}, Lee/cyber/smartid/tse/inter/LogAccess;->d(Ljava/lang/String;)V

    .line 1007
    iget-object v0, p0, Lee/cyber/smartid/tse/KeyStateManager;->l:Lee/cyber/smartid/tse/SmartIdTSE$TSEAlarmAccess;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ee.cyber.smartid.ACTION_REFRESH_CLONE_DETECTION_SINGLE_KEY"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p2, p3, p1}, Lee/cyber/smartid/tse/SmartIdTSE$TSEAlarmAccess;->scheduleAlarmFor(Ljava/lang/String;JLjava/lang/String;)V

    return-void
.end method

.method private a(Ljava/lang/String;Lee/cyber/smartid/tse/dto/ProtoKeyState;Landroid/os/Bundle;Lee/cyber/smartid/tse/inter/TSEListener;)V
    .locals 16

    move-object/from16 v1, p0

    move-object/from16 v0, p2

    .line 269
    iget-object v14, v1, Lee/cyber/smartid/tse/KeyStateManager;->j:Ljava/lang/Object;

    monitor-enter v14

    .line 270
    :try_start_0
    iget-object v2, v1, Lee/cyber/smartid/tse/KeyStateManager;->g:Landroid/os/PowerManager$WakeLock;

    const-wide/32 v3, 0x493e0

    invoke-static {v2, v3, v4}, Lee/cyber/smartid/tse/util/Util;->acquireWakeLock(Landroid/os/PowerManager$WakeLock;J)V

    .line 272
    invoke-virtual/range {p2 .. p2}, Lee/cyber/smartid/tse/dto/ProtoKeyState;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lee/cyber/smartid/tse/KeyStateManager;->isKeyStateInProgress(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 273
    iget-object v2, v1, Lee/cyber/smartid/tse/KeyStateManager;->t:Lee/cyber/smartid/tse/inter/LogAccess;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "startKeyState - rejecting a new startKeyState call for a job already in progress: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2, v0}, Lee/cyber/smartid/tse/inter/LogAccess;->e(Ljava/lang/String;)V

    .line 274
    monitor-exit v14

    return-void

    .line 276
    :cond_0
    iget-object v2, v1, Lee/cyber/smartid/tse/KeyStateManager;->t:Lee/cyber/smartid/tse/inter/LogAccess;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "startKeyState - accepted a new job: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lee/cyber/smartid/tse/inter/LogAccess;->d(Ljava/lang/String;)V

    .line 278
    invoke-virtual/range {p2 .. p2}, Lee/cyber/smartid/tse/dto/ProtoKeyState;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lee/cyber/smartid/tse/KeyStateManager;->b(Ljava/lang/String;)V

    .line 279
    invoke-static/range {p1 .. p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 281
    iget-object v2, v1, Lee/cyber/smartid/tse/KeyStateManager;->e:Lee/cyber/smartid/tse/inter/ListenerAccess;

    move-object/from16 v4, p1

    move-object/from16 v3, p4

    invoke-interface {v2, v4, v3}, Lee/cyber/smartid/tse/inter/ListenerAccess;->setListener(Ljava/lang/String;Lee/cyber/smartid/tse/inter/TSEListener;)V

    goto :goto_0

    :cond_1
    move-object/from16 v4, p1

    .line 284
    :goto_0
    invoke-direct {v1, v0}, Lee/cyber/smartid/tse/KeyStateManager;->a(Lee/cyber/smartid/tse/dto/ProtoKeyState;)V

    .line 286
    iget-object v15, v1, Lee/cyber/smartid/tse/KeyStateManager;->b:Ljava/util/concurrent/Executor;

    new-instance v13, Lee/cyber/smartid/tse/util/KeyStateRunnable;

    iget-object v5, v1, Lee/cyber/smartid/tse/KeyStateManager;->c:Lee/cyber/smartid/tse/inter/ResourceAccess;

    iget-object v6, v1, Lee/cyber/smartid/tse/KeyStateManager;->q:Lee/cyber/smartid/tse/inter/ExternalResourceAccess;

    iget-object v7, v1, Lee/cyber/smartid/tse/KeyStateManager;->h:Lee/cyber/smartid/tse/network/TSEAPI$TSEAPIService;

    new-instance v8, Lee/cyber/smartid/tse/KeyStateManager$1;

    invoke-direct {v8, v1}, Lee/cyber/smartid/tse/KeyStateManager$1;-><init>(Lee/cyber/smartid/tse/KeyStateManager;)V

    iget-object v9, v1, Lee/cyber/smartid/tse/KeyStateManager;->e:Lee/cyber/smartid/tse/inter/ListenerAccess;

    iget-object v10, v1, Lee/cyber/smartid/tse/KeyStateManager;->d:Lee/cyber/smartid/tse/inter/KeyManagerAccess;

    iget-object v11, v1, Lee/cyber/smartid/tse/KeyStateManager;->l:Lee/cyber/smartid/tse/SmartIdTSE$TSEAlarmAccess;

    iget-object v12, v1, Lee/cyber/smartid/tse/KeyStateManager;->r:Lee/cyber/smartid/tse/inter/WallClock;

    move-object v2, v13

    move-object/from16 v3, p2

    move-object/from16 v4, p1

    move-object v0, v13

    move-object/from16 v13, p3

    invoke-direct/range {v2 .. v13}, Lee/cyber/smartid/tse/util/KeyStateRunnable;-><init>(Lee/cyber/smartid/tse/dto/ProtoKeyState;Ljava/lang/String;Lee/cyber/smartid/tse/inter/ResourceAccess;Lee/cyber/smartid/tse/inter/ExternalResourceAccess;Lee/cyber/smartid/tse/network/TSEAPI$TSEAPIService;Lee/cyber/smartid/tse/inter/StateWorkerJobListener;Lee/cyber/smartid/tse/inter/ListenerAccess;Lee/cyber/smartid/tse/inter/KeyStorageAccess;Lee/cyber/smartid/tse/SmartIdTSE$TSEAlarmAccess;Lee/cyber/smartid/tse/inter/WallClock;Landroid/os/Bundle;)V

    invoke-interface {v15, v0}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 329
    monitor-exit v14

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v14
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private a(Ljava/util/ArrayList;Lee/cyber/smartid/tse/dto/KeyState;Lee/cyber/smartid/tse/dto/KeyStateMeta;)Z
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Lee/cyber/smartid/tse/dto/ProtoKeyState;",
            ">;",
            "Lee/cyber/smartid/tse/dto/KeyState;",
            "Lee/cyber/smartid/tse/dto/KeyStateMeta;",
            ")Z"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lee/cyber/smartid/cryptolib/dto/StorageException;
        }
    .end annotation

    .line 963
    iget-object v0, p0, Lee/cyber/smartid/tse/KeyStateManager;->k:Ljava/lang/Object;

    monitor-enter v0

    .line 964
    :try_start_0
    iget-object v1, p0, Lee/cyber/smartid/tse/KeyStateManager;->t:Lee/cyber/smartid/tse/inter/LogAccess;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "updateCloneDetection called for: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lee/cyber/smartid/tse/inter/LogAccess;->d(Ljava/lang/String;)V

    const/4 v1, 0x0

    if-eqz p2, :cond_2

    .line 966
    invoke-virtual {p2}, Lee/cyber/smartid/tse/dto/KeyState;->isInActiveState()Z

    move-result v2

    if-nez v2, :cond_2

    if-eqz p3, :cond_2

    iget-object v2, p0, Lee/cyber/smartid/tse/KeyStateManager;->r:Lee/cyber/smartid/tse/inter/WallClock;

    invoke-virtual {p3, v2}, Lee/cyber/smartid/tse/dto/KeyStateMeta;->isKeyLocked(Lee/cyber/smartid/tse/inter/WallClock;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 967
    iget-object v2, p0, Lee/cyber/smartid/tse/KeyStateManager;->r:Lee/cyber/smartid/tse/inter/WallClock;

    invoke-interface {v2}, Lee/cyber/smartid/tse/inter/WallClock;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {p3}, Lee/cyber/smartid/tse/dto/KeyStateMeta;->getTimestamp()J

    move-result-wide v4

    sub-long/2addr v2, v4

    iget-object p3, p0, Lee/cyber/smartid/tse/KeyStateManager;->c:Lee/cyber/smartid/tse/inter/ResourceAccess;

    invoke-interface {p3}, Lee/cyber/smartid/tse/inter/ResourceAccess;->getRefreshCloneDetectionInterval()J

    move-result-wide v4

    iget-object p3, p0, Lee/cyber/smartid/tse/KeyStateManager;->c:Lee/cyber/smartid/tse/inter/ResourceAccess;

    invoke-interface {p3}, Lee/cyber/smartid/tse/inter/ResourceAccess;->getRefreshCloneDetectionInterval()J

    move-result-wide v6

    long-to-double v6, v6

    const-wide/high16 v8, 0x4024000000000000L    # 10.0

    div-double/2addr v6, v8

    double-to-int p3, v6

    int-to-long v6, p3

    sub-long/2addr v4, v6

    cmp-long p3, v2, v4

    if-ltz p3, :cond_0

    const/4 p3, 0x1

    const/4 v1, 0x1

    .line 968
    :cond_0
    iget-object p3, p0, Lee/cyber/smartid/tse/KeyStateManager;->t:Lee/cyber/smartid/tse/inter/LogAccess;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "updateCloneDetection - shouldRefreshCloneDetection: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p3, v2}, Lee/cyber/smartid/tse/inter/LogAccess;->d(Ljava/lang/String;)V

    .line 970
    invoke-virtual {p2}, Lee/cyber/smartid/tse/dto/KeyState;->getId()Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p2}, Lee/cyber/smartid/tse/dto/KeyState;->getKeyId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2}, Lee/cyber/smartid/tse/dto/KeyState;->getAccountUUID()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p2}, Lee/cyber/smartid/tse/dto/KeyState;->getKeyType()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p0, p3, v2, v3, p2}, Lee/cyber/smartid/tse/KeyStateManager;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lee/cyber/smartid/tse/dto/ProtoKeyState;

    move-result-object p2

    if-eqz p2, :cond_1

    .line 972
    iget-object p3, p0, Lee/cyber/smartid/tse/KeyStateManager;->t:Lee/cyber/smartid/tse/inter/LogAccess;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "updateCloneDetection - created a refreshCloneDetection state for "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p3, v2}, Lee/cyber/smartid/tse/inter/LogAccess;->d(Ljava/lang/String;)V

    .line 973
    invoke-virtual {p1, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 975
    :cond_1
    iget-object p1, p0, Lee/cyber/smartid/tse/KeyStateManager;->t:Lee/cyber/smartid/tse/inter/LogAccess;

    const-string p2, "updateCloneDetection - failed to create a KeyStateOld for clone detection!"

    invoke-interface {p1, p2}, Lee/cyber/smartid/tse/inter/LogAccess;->e(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_2
    if-eqz p2, :cond_5

    .line 977
    invoke-virtual {p2}, Lee/cyber/smartid/tse/dto/KeyState;->isInActiveState()Z

    move-result p1

    if-nez p1, :cond_5

    if-eqz p3, :cond_5

    iget-object p1, p0, Lee/cyber/smartid/tse/KeyStateManager;->r:Lee/cyber/smartid/tse/inter/WallClock;

    invoke-virtual {p3, p1}, Lee/cyber/smartid/tse/dto/KeyStateMeta;->isKeyLocked(Lee/cyber/smartid/tse/inter/WallClock;)Z

    move-result p1

    if-eqz p1, :cond_5

    .line 979
    invoke-virtual {p3}, Lee/cyber/smartid/tse/dto/KeyStateMeta;->isKeyLockedPermanently()Z

    move-result p1

    if-eqz p1, :cond_3

    .line 980
    iget-object p1, p0, Lee/cyber/smartid/tse/KeyStateManager;->t:Lee/cyber/smartid/tse/inter/LogAccess;

    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "updateCloneDetection - key "

    invoke-virtual {p3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Lee/cyber/smartid/tse/dto/KeyState;->getKeyType()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, " for "

    invoke-virtual {p3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Lee/cyber/smartid/tse/dto/KeyState;->getAccountUUID()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p2, " is perma locked or CSR signing has not been completed, skipping .."

    invoke-virtual {p3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-interface {p1, p2}, Lee/cyber/smartid/tse/inter/LogAccess;->d(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 983
    :cond_3
    iget-object p1, p0, Lee/cyber/smartid/tse/KeyStateManager;->r:Lee/cyber/smartid/tse/inter/WallClock;

    invoke-virtual {p3, p1}, Lee/cyber/smartid/tse/dto/KeyStateMeta;->getKeyLockRemainingDuration(Lee/cyber/smartid/tse/inter/WallClock;)J

    move-result-wide v2

    const-wide/16 v4, 0x1388

    add-long/2addr v4, v2

    .line 986
    iget-object p1, p0, Lee/cyber/smartid/tse/KeyStateManager;->c:Lee/cyber/smartid/tse/inter/ResourceAccess;

    invoke-interface {p1}, Lee/cyber/smartid/tse/inter/ResourceAccess;->getRefreshCloneDetectionInterval()J

    move-result-wide v6

    cmp-long p1, v4, v6

    if-gez p1, :cond_4

    .line 988
    invoke-virtual {p2}, Lee/cyber/smartid/tse/dto/KeyState;->getId()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p1, v4, v5}, Lee/cyber/smartid/tse/KeyStateManager;->a(Ljava/lang/String;J)V

    .line 989
    iget-object p1, p0, Lee/cyber/smartid/tse/KeyStateManager;->t:Lee/cyber/smartid/tse/inter/LogAccess;

    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "updateCloneDetection - key "

    invoke-virtual {p3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Lee/cyber/smartid/tse/dto/KeyState;->getKeyType()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, " for "

    invoke-virtual {p3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Lee/cyber/smartid/tse/dto/KeyState;->getAccountUUID()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p2, " is temp locked for "

    invoke-virtual {p3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string p2, "ms, scheduling as a retry .."

    invoke-virtual {p3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-interface {p1, p2}, Lee/cyber/smartid/tse/inter/LogAccess;->d(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 991
    :cond_4
    iget-object p1, p0, Lee/cyber/smartid/tse/KeyStateManager;->t:Lee/cyber/smartid/tse/inter/LogAccess;

    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "updateCloneDetection - key "

    invoke-virtual {p3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Lee/cyber/smartid/tse/dto/KeyState;->getKeyType()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, " for "

    invoke-virtual {p3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Lee/cyber/smartid/tse/dto/KeyState;->getAccountUUID()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p2, " is temp locked for "

    invoke-virtual {p3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string p2, "ms, skipping .."

    invoke-virtual {p3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-interface {p1, p2}, Lee/cyber/smartid/tse/inter/LogAccess;->d(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_5
    if-eqz p2, :cond_6

    .line 994
    invoke-virtual {p2}, Lee/cyber/smartid/tse/dto/KeyState;->isInActiveState()Z

    move-result p1

    if-eqz p1, :cond_6

    if-eqz p3, :cond_6

    iget-object p1, p0, Lee/cyber/smartid/tse/KeyStateManager;->r:Lee/cyber/smartid/tse/inter/WallClock;

    invoke-virtual {p3, p1}, Lee/cyber/smartid/tse/dto/KeyStateMeta;->isKeyLocked(Lee/cyber/smartid/tse/inter/WallClock;)Z

    move-result p1

    if-nez p1, :cond_6

    .line 995
    iget-object p1, p0, Lee/cyber/smartid/tse/KeyStateManager;->t:Lee/cyber/smartid/tse/inter/LogAccess;

    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "updateCloneDetection - key "

    invoke-virtual {p3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Lee/cyber/smartid/tse/dto/KeyState;->getKeyType()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, " for "

    invoke-virtual {p3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Lee/cyber/smartid/tse/dto/KeyState;->getAccountUUID()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, " is in active state for "

    invoke-virtual {p3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Lee/cyber/smartid/tse/dto/KeyState;->getHumanReadableNameForType()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p2, ", skipping .."

    invoke-virtual {p3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-interface {p1, p2}, Lee/cyber/smartid/tse/inter/LogAccess;->d(Ljava/lang/String;)V

    goto :goto_0

    :cond_6
    if-eqz p2, :cond_7

    .line 996
    invoke-virtual {p2}, Lee/cyber/smartid/tse/dto/KeyState;->isInActiveState()Z

    move-result p1

    if-eqz p1, :cond_7

    if-eqz p3, :cond_7

    iget-object p1, p0, Lee/cyber/smartid/tse/KeyStateManager;->r:Lee/cyber/smartid/tse/inter/WallClock;

    invoke-virtual {p3, p1}, Lee/cyber/smartid/tse/dto/KeyStateMeta;->isKeyLocked(Lee/cyber/smartid/tse/inter/WallClock;)Z

    move-result p1

    if-eqz p1, :cond_7

    .line 997
    iget-object p1, p0, Lee/cyber/smartid/tse/KeyStateManager;->t:Lee/cyber/smartid/tse/inter/LogAccess;

    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "updateCloneDetection - key "

    invoke-virtual {p3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Lee/cyber/smartid/tse/dto/KeyState;->getKeyType()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, " for "

    invoke-virtual {p3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Lee/cyber/smartid/tse/dto/KeyState;->getAccountUUID()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, " is in active state for "

    invoke-virtual {p3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Lee/cyber/smartid/tse/dto/KeyState;->getHumanReadableNameForType()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p2, " and locked, skipping .."

    invoke-virtual {p3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-interface {p1, p2}, Lee/cyber/smartid/tse/inter/LogAccess;->d(Ljava/lang/String;)V

    goto :goto_0

    .line 999
    :cond_7
    iget-object p1, p0, Lee/cyber/smartid/tse/KeyStateManager;->t:Lee/cyber/smartid/tse/inter/LogAccess;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "updateCloneDetection - Something is very wrong, we missed the state "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string p2, ", meta: "

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-interface {p1, p2}, Lee/cyber/smartid/tse/inter/LogAccess;->wtf(Ljava/lang/String;)V

    .line 1001
    :goto_0
    monitor-exit v0

    return v1

    :catchall_0
    move-exception p1

    .line 1002
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method static synthetic b(Lee/cyber/smartid/tse/KeyStateManager;)Lee/cyber/smartid/tse/inter/LogAccess;
    .locals 0

    .line 68
    iget-object p0, p0, Lee/cyber/smartid/tse/KeyStateManager;->t:Lee/cyber/smartid/tse/inter/LogAccess;

    return-object p0
.end method

.method private b(Lee/cyber/smartid/tse/dto/ProtoKeyState;)Lee/cyber/smartid/tse/inter/TSEListener;
    .locals 3

    .line 1060
    invoke-virtual {p1}, Lee/cyber/smartid/tse/dto/ProtoKeyState;->isActiveSubmitClientSecondPart()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1061
    iget-object v0, p0, Lee/cyber/smartid/tse/KeyStateManager;->t:Lee/cyber/smartid/tse/inter/LogAccess;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "createSystemListenerForAlarmListener: Created a SubmitClientSecondPartListener for KeyState "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lee/cyber/smartid/tse/dto/ProtoKeyState;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lee/cyber/smartid/tse/inter/LogAccess;->d(Ljava/lang/String;)V

    .line 1062
    new-instance v0, Lee/cyber/smartid/tse/KeyStateManager$11;

    invoke-direct {v0, p0, p1}, Lee/cyber/smartid/tse/KeyStateManager$11;-><init>(Lee/cyber/smartid/tse/KeyStateManager;Lee/cyber/smartid/tse/dto/ProtoKeyState;)V

    return-object v0

    .line 1073
    :cond_0
    invoke-virtual {p1}, Lee/cyber/smartid/tse/dto/ProtoKeyState;->isActiveCloneDetection()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1074
    iget-object v0, p0, Lee/cyber/smartid/tse/KeyStateManager;->t:Lee/cyber/smartid/tse/inter/LogAccess;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "createSystemListenerForAlarmListener: Created a RefreshCloneDetectionListener for KeyState "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lee/cyber/smartid/tse/dto/ProtoKeyState;->getId()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, p1}, Lee/cyber/smartid/tse/inter/LogAccess;->d(Ljava/lang/String;)V

    .line 1075
    new-instance p1, Lee/cyber/smartid/tse/KeyStateManager$12;

    invoke-direct {p1, p0}, Lee/cyber/smartid/tse/KeyStateManager$12;-><init>(Lee/cyber/smartid/tse/KeyStateManager;)V

    return-object p1

    :cond_1
    const/4 p1, 0x0

    return-object p1
.end method

.method private b()Ljava/lang/String;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 899
    iget-object v0, p0, Lee/cyber/smartid/tse/KeyStateManager;->n:Lee/cyber/smartid/cryptolib/inter/EncodingOp;

    new-instance v1, Ljava/math/BigInteger;

    iget-object v2, p0, Lee/cyber/smartid/tse/KeyStateManager;->o:Lee/cyber/smartid/cryptolib/inter/RandomGenerationOp;

    invoke-interface {v2}, Lee/cyber/smartid/cryptolib/inter/RandomGenerationOp;->generateRandomNonce()[B

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/math/BigInteger;-><init>([B)V

    invoke-interface {v0, v1}, Lee/cyber/smartid/cryptolib/inter/EncodingOp;->encodeDecimalToBase64(Ljava/math/BigInteger;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private b(Ljava/lang/String;)V
    .locals 2

    .line 560
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 563
    :cond_0
    iget-object v0, p0, Lee/cyber/smartid/tse/KeyStateManager;->j:Ljava/lang/Object;

    monitor-enter v0

    .line 564
    :try_start_0
    iget-object v1, p0, Lee/cyber/smartid/tse/KeyStateManager;->f:Ljava/util/HashSet;

    invoke-virtual {v1, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 565
    monitor-exit v0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method private b(Ljava/lang/String;Lee/cyber/smartid/tse/dto/ProtoKeyState;Landroid/os/Bundle;Lee/cyber/smartid/tse/inter/TSEListener;)V
    .locals 4

    if-nez p2, :cond_0

    .line 367
    iget-object p1, p0, Lee/cyber/smartid/tse/KeyStateManager;->t:Lee/cyber/smartid/tse/inter/LogAccess;

    const-string p2, "queueKeyState: failed, state should not be null!"

    invoke-interface {p1, p2}, Lee/cyber/smartid/tse/inter/LogAccess;->e(Ljava/lang/String;)V

    return-void

    .line 371
    :cond_0
    iget-object v0, p0, Lee/cyber/smartid/tse/KeyStateManager;->j:Ljava/lang/Object;

    monitor-enter v0

    .line 372
    :try_start_0
    invoke-virtual {p2}, Lee/cyber/smartid/tse/dto/ProtoKeyState;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lee/cyber/smartid/tse/KeyStateManager;->isKeyStateInProgress(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 374
    iget-object v1, p0, Lee/cyber/smartid/tse/KeyStateManager;->t:Lee/cyber/smartid/tse/inter/LogAccess;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "queueKeyState: We can already start with "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lee/cyber/smartid/tse/inter/LogAccess;->d(Ljava/lang/String;)V

    .line 375
    invoke-direct {p0, p1, p2, p3, p4}, Lee/cyber/smartid/tse/KeyStateManager;->c(Ljava/lang/String;Lee/cyber/smartid/tse/dto/ProtoKeyState;Landroid/os/Bundle;Lee/cyber/smartid/tse/inter/TSEListener;)V

    .line 376
    monitor-exit v0

    return-void

    .line 381
    :cond_1
    new-instance v1, Lee/cyber/smartid/tse/dto/KeyStateQueue;

    invoke-direct {v1, p2, p1, p3, p4}, Lee/cyber/smartid/tse/dto/KeyStateQueue;-><init>(Lee/cyber/smartid/tse/dto/ProtoKeyState;Ljava/lang/String;Landroid/os/Bundle;Lee/cyber/smartid/tse/inter/TSEListener;)V

    .line 382
    iget-object p1, p0, Lee/cyber/smartid/tse/KeyStateManager;->i:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {p1, v1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->add(Ljava/lang/Object;)Z

    .line 383
    iget-object p1, p0, Lee/cyber/smartid/tse/KeyStateManager;->t:Lee/cyber/smartid/tse/inter/LogAccess;

    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    const-string p4, "queueKeyState: Queued ("

    invoke-virtual {p3, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p4, p0, Lee/cyber/smartid/tse/KeyStateManager;->i:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {p4}, Ljava/util/concurrent/ConcurrentLinkedQueue;->size()I

    move-result p4

    invoke-virtual {p3, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p4, ") "

    invoke-virtual {p3, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p3

    invoke-interface {p1, p3}, Lee/cyber/smartid/tse/inter/LogAccess;->d(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 385
    :try_start_1
    iget-object p1, p0, Lee/cyber/smartid/tse/KeyStateManager;->s:Lee/cyber/smartid/tse/inter/StateWorkerJobListener;

    if-eqz p1, :cond_2

    .line 386
    iget-object p1, p0, Lee/cyber/smartid/tse/KeyStateManager;->s:Lee/cyber/smartid/tse/inter/StateWorkerJobListener;

    invoke-virtual {p2}, Lee/cyber/smartid/tse/dto/ProtoKeyState;->getId()Ljava/lang/String;

    move-result-object p2

    invoke-interface {p1, p2}, Lee/cyber/smartid/tse/inter/StateWorkerJobListener;->onJobQueued(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 389
    :try_start_2
    iget-object p2, p0, Lee/cyber/smartid/tse/KeyStateManager;->t:Lee/cyber/smartid/tse/inter/LogAccess;

    const-string p3, "queueKeyState"

    invoke-interface {p2, p3, p1}, Lee/cyber/smartid/tse/inter/LogAccess;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 391
    :cond_2
    :goto_0
    monitor-exit v0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw p1
.end method

.method static synthetic c(Lee/cyber/smartid/tse/KeyStateManager;)Ljava/lang/Object;
    .locals 0

    .line 68
    iget-object p0, p0, Lee/cyber/smartid/tse/KeyStateManager;->j:Ljava/lang/Object;

    return-object p0
.end method

.method private c(Ljava/lang/String;Lee/cyber/smartid/tse/dto/ProtoKeyState;Landroid/os/Bundle;Lee/cyber/smartid/tse/inter/TSEListener;)V
    .locals 5

    .line 505
    iget-object v0, p0, Lee/cyber/smartid/tse/KeyStateManager;->j:Ljava/lang/Object;

    monitor-enter v0

    .line 508
    :try_start_0
    iget-object v1, p0, Lee/cyber/smartid/tse/KeyStateManager;->d:Lee/cyber/smartid/tse/inter/KeyManagerAccess;

    invoke-virtual {p2}, Lee/cyber/smartid/tse/dto/ProtoKeyState;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lee/cyber/smartid/tse/inter/KeyManagerAccess;->getKeyStateById(Ljava/lang/String;)Lee/cyber/smartid/tse/dto/KeyState;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 509
    invoke-virtual {v1}, Lee/cyber/smartid/tse/dto/KeyState;->isInActiveState()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 510
    iget-object p3, p0, Lee/cyber/smartid/tse/KeyStateManager;->d:Lee/cyber/smartid/tse/inter/KeyManagerAccess;

    invoke-virtual {p2}, Lee/cyber/smartid/tse/dto/ProtoKeyState;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p3, v2}, Lee/cyber/smartid/tse/inter/KeyManagerAccess;->getKeyStateMetaByKeyStateId(Ljava/lang/String;)Lee/cyber/smartid/tse/dto/KeyStateMeta;

    move-result-object p3

    .line 514
    iget-object v2, p0, Lee/cyber/smartid/tse/KeyStateManager;->t:Lee/cyber/smartid/tse/inter/LogAccess;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "resolveQueuedKeyState: State "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, " didn\'t resolve, we can\'t do anything with the new one, aborting "

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-interface {v2, p2}, Lee/cyber/smartid/tse/inter/LogAccess;->d(Ljava/lang/String;)V

    .line 515
    iget-object p2, p0, Lee/cyber/smartid/tse/KeyStateManager;->r:Lee/cyber/smartid/tse/inter/WallClock;

    const-wide/16 v1, 0x3f4    # 5.0E-321

    iget-object v3, p0, Lee/cyber/smartid/tse/KeyStateManager;->c:Lee/cyber/smartid/tse/inter/ResourceAccess;

    invoke-interface {v3}, Lee/cyber/smartid/tse/inter/ResourceAccess;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    sget v4, Lee/cyber/smartid/tse/R$string;->err_invalid_transaction_state_previous_didnt_resolve:I

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {p2, v1, v2, v3}, Lee/cyber/smartid/tse/dto/TSEError;->from(Lee/cyber/smartid/tse/inter/WallClock;JLjava/lang/String;)Lee/cyber/smartid/tse/dto/TSEError;

    move-result-object p2

    .line 516
    invoke-virtual {p2}, Lee/cyber/smartid/tse/dto/TSEError;->createOrGetExtras()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "ee.cyber.smartid.EXTRA_INVALID_TRANSACTION_STATE_SOURCE"

    const/4 v3, 0x1

    if-eqz p3, :cond_0

    invoke-virtual {p3}, Lee/cyber/smartid/tse/dto/KeyStateMeta;->getLastErrorType()I

    move-result p3

    if-ne p3, v3, :cond_0

    goto :goto_0

    :cond_0
    const/4 v3, 0x0

    :goto_0
    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 517
    invoke-direct {p0, p4, p1, p2}, Lee/cyber/smartid/tse/KeyStateManager;->a(Lee/cyber/smartid/tse/inter/TSEListener;Ljava/lang/String;Lee/cyber/smartid/tse/dto/TSEError;)V

    .line 519
    invoke-direct {p0}, Lee/cyber/smartid/tse/KeyStateManager;->a()V

    .line 520
    monitor-exit v0

    return-void

    .line 526
    :cond_1
    iget-object v1, p0, Lee/cyber/smartid/tse/KeyStateManager;->t:Lee/cyber/smartid/tse/inter/LogAccess;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "resolveQueuedKeyState: We can start with "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lee/cyber/smartid/tse/inter/LogAccess;->d(Ljava/lang/String;)V

    .line 527
    invoke-direct {p0, p1, p2, p3, p4}, Lee/cyber/smartid/tse/KeyStateManager;->a(Ljava/lang/String;Lee/cyber/smartid/tse/dto/ProtoKeyState;Landroid/os/Bundle;Lee/cyber/smartid/tse/inter/TSEListener;)V

    .line 528
    monitor-exit v0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method public static createNewInstanceForUnitTest(Ljava/util/concurrent/Executor;Lee/cyber/smartid/tse/inter/ResourceAccess;Lee/cyber/smartid/tse/network/TSEAPI$TSEAPIService;Lee/cyber/smartid/tse/inter/ListenerAccess;Lee/cyber/smartid/tse/inter/KeyManagerAccess;Lee/cyber/smartid/tse/SmartIdTSE$TSEAlarmAccess;Lee/cyber/smartid/cryptolib/inter/SigningOp;Lee/cyber/smartid/cryptolib/inter/EncodingOp;Lee/cyber/smartid/cryptolib/inter/RandomGenerationOp;Lee/cyber/smartid/cryptolib/inter/CryptoOp;Lee/cyber/smartid/tse/inter/ExternalResourceAccess;Lee/cyber/smartid/tse/inter/WallClock;)Lee/cyber/smartid/tse/KeyStateManager;
    .locals 14

    .line 224
    new-instance v13, Lee/cyber/smartid/tse/KeyStateManager;

    move-object v0, v13

    move-object v1, p0

    move-object v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    invoke-direct/range {v0 .. v12}, Lee/cyber/smartid/tse/KeyStateManager;-><init>(Ljava/util/concurrent/Executor;Lee/cyber/smartid/tse/inter/ResourceAccess;Lee/cyber/smartid/tse/network/TSEAPI$TSEAPIService;Lee/cyber/smartid/tse/inter/ListenerAccess;Lee/cyber/smartid/tse/inter/KeyManagerAccess;Lee/cyber/smartid/tse/SmartIdTSE$TSEAlarmAccess;Lee/cyber/smartid/cryptolib/inter/SigningOp;Lee/cyber/smartid/cryptolib/inter/EncodingOp;Lee/cyber/smartid/cryptolib/inter/RandomGenerationOp;Lee/cyber/smartid/cryptolib/inter/CryptoOp;Lee/cyber/smartid/tse/inter/ExternalResourceAccess;Lee/cyber/smartid/tse/inter/WallClock;)V

    return-object v13
.end method

.method static synthetic d(Lee/cyber/smartid/tse/KeyStateManager;)V
    .locals 0

    .line 68
    invoke-direct {p0}, Lee/cyber/smartid/tse/KeyStateManager;->a()V

    return-void
.end method

.method static synthetic e(Lee/cyber/smartid/tse/KeyStateManager;)Landroid/os/PowerManager$WakeLock;
    .locals 0

    .line 68
    iget-object p0, p0, Lee/cyber/smartid/tse/KeyStateManager;->g:Landroid/os/PowerManager$WakeLock;

    return-object p0
.end method

.method static synthetic f(Lee/cyber/smartid/tse/KeyStateManager;)Lee/cyber/smartid/tse/inter/ListenerAccess;
    .locals 0

    .line 68
    iget-object p0, p0, Lee/cyber/smartid/tse/KeyStateManager;->e:Lee/cyber/smartid/tse/inter/ListenerAccess;

    return-object p0
.end method

.method static synthetic g(Lee/cyber/smartid/tse/KeyStateManager;)Ljava/lang/Object;
    .locals 0

    .line 68
    iget-object p0, p0, Lee/cyber/smartid/tse/KeyStateManager;->k:Ljava/lang/Object;

    return-object p0
.end method

.method public static getInstance(Lee/cyber/smartid/tse/inter/ResourceAccess;Lee/cyber/smartid/tse/network/TSEAPI$TSEAPIService;Lee/cyber/smartid/tse/inter/ListenerAccess;Lee/cyber/smartid/tse/inter/KeyManagerAccess;Lee/cyber/smartid/tse/SmartIdTSE$TSEAlarmAccess;Lee/cyber/smartid/cryptolib/inter/SigningOp;Lee/cyber/smartid/cryptolib/inter/EncodingOp;Lee/cyber/smartid/cryptolib/inter/RandomGenerationOp;Lee/cyber/smartid/cryptolib/inter/CryptoOp;Lee/cyber/smartid/tse/inter/ExternalResourceAccess;Lee/cyber/smartid/tse/inter/WallClock;)Lee/cyber/smartid/tse/KeyStateManager;
    .locals 15

    .line 194
    sget-object v0, Lee/cyber/smartid/tse/KeyStateManager;->a:Lee/cyber/smartid/tse/KeyStateManager;

    if-nez v0, :cond_1

    .line 195
    const-class v1, Lee/cyber/smartid/tse/KeyStateManager;

    monitor-enter v1

    .line 196
    :try_start_0
    sget-object v0, Lee/cyber/smartid/tse/KeyStateManager;->a:Lee/cyber/smartid/tse/KeyStateManager;

    if-nez v0, :cond_0

    .line 197
    new-instance v0, Lee/cyber/smartid/tse/KeyStateManager;

    const/4 v2, 0x2

    invoke-static {v2}, Ljava/util/concurrent/Executors;->newFixedThreadPool(I)Ljava/util/concurrent/ExecutorService;

    move-result-object v3

    move-object v2, v0

    move-object v4, p0

    move-object/from16 v5, p1

    move-object/from16 v6, p2

    move-object/from16 v7, p3

    move-object/from16 v8, p4

    move-object/from16 v9, p5

    move-object/from16 v10, p6

    move-object/from16 v11, p7

    move-object/from16 v12, p8

    move-object/from16 v13, p9

    move-object/from16 v14, p10

    invoke-direct/range {v2 .. v14}, Lee/cyber/smartid/tse/KeyStateManager;-><init>(Ljava/util/concurrent/Executor;Lee/cyber/smartid/tse/inter/ResourceAccess;Lee/cyber/smartid/tse/network/TSEAPI$TSEAPIService;Lee/cyber/smartid/tse/inter/ListenerAccess;Lee/cyber/smartid/tse/inter/KeyManagerAccess;Lee/cyber/smartid/tse/SmartIdTSE$TSEAlarmAccess;Lee/cyber/smartid/cryptolib/inter/SigningOp;Lee/cyber/smartid/cryptolib/inter/EncodingOp;Lee/cyber/smartid/cryptolib/inter/RandomGenerationOp;Lee/cyber/smartid/cryptolib/inter/CryptoOp;Lee/cyber/smartid/tse/inter/ExternalResourceAccess;Lee/cyber/smartid/tse/inter/WallClock;)V

    sput-object v0, Lee/cyber/smartid/tse/KeyStateManager;->a:Lee/cyber/smartid/tse/KeyStateManager;

    .line 199
    :cond_0
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 201
    :cond_1
    :goto_0
    sget-object v0, Lee/cyber/smartid/tse/KeyStateManager;->a:Lee/cyber/smartid/tse/KeyStateManager;

    return-object v0
.end method

.method static synthetic h(Lee/cyber/smartid/tse/KeyStateManager;)Lee/cyber/smartid/tse/inter/KeyManagerAccess;
    .locals 0

    .line 68
    iget-object p0, p0, Lee/cyber/smartid/tse/KeyStateManager;->d:Lee/cyber/smartid/tse/inter/KeyManagerAccess;

    return-object p0
.end method

.method static synthetic i(Lee/cyber/smartid/tse/KeyStateManager;)Lee/cyber/smartid/tse/inter/WallClock;
    .locals 0

    .line 68
    iget-object p0, p0, Lee/cyber/smartid/tse/KeyStateManager;->r:Lee/cyber/smartid/tse/inter/WallClock;

    return-object p0
.end method

.method static synthetic j(Lee/cyber/smartid/tse/KeyStateManager;)Lee/cyber/smartid/tse/inter/ResourceAccess;
    .locals 0

    .line 68
    iget-object p0, p0, Lee/cyber/smartid/tse/KeyStateManager;->c:Lee/cyber/smartid/tse/inter/ResourceAccess;

    return-object p0
.end method

.method static synthetic k(Lee/cyber/smartid/tse/KeyStateManager;)Ljava/lang/String;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 68
    invoke-direct {p0}, Lee/cyber/smartid/tse/KeyStateManager;->b()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method static synthetic l(Lee/cyber/smartid/tse/KeyStateManager;)Lee/cyber/smartid/tse/inter/ExternalResourceAccess;
    .locals 0

    .line 68
    iget-object p0, p0, Lee/cyber/smartid/tse/KeyStateManager;->q:Lee/cyber/smartid/tse/inter/ExternalResourceAccess;

    return-object p0
.end method


# virtual methods
.method a(Ljava/lang/String;Ljava/lang/String;)Lee/cyber/smartid/tse/dto/jsonrpc/resp/CheckLocalPendingStateResp;
    .locals 5

    .line 1312
    iget-object v0, p0, Lee/cyber/smartid/tse/KeyStateManager;->k:Ljava/lang/Object;

    monitor-enter v0

    .line 1313
    :try_start_0
    iget-object v1, p0, Lee/cyber/smartid/tse/KeyStateManager;->d:Lee/cyber/smartid/tse/inter/KeyManagerAccess;

    invoke-interface {v1, p2}, Lee/cyber/smartid/tse/inter/KeyManagerAccess;->getKeyStateByKeyId(Ljava/lang/String;)Lee/cyber/smartid/tse/dto/KeyState;

    move-result-object v1

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    .line 1318
    invoke-virtual {v1}, Lee/cyber/smartid/tse/dto/KeyState;->isActiveSign()Z

    move-result v3

    if-eqz v3, :cond_0

    const-string v2, "confirmTransaction"

    .line 1321
    invoke-virtual {v1}, Lee/cyber/smartid/tse/dto/KeyState;->getTransactionUUID()Ljava/lang/String;

    move-result-object v1

    move-object v4, v2

    move-object v2, v1

    move-object v1, v4

    goto :goto_0

    :cond_0
    if-eqz v1, :cond_1

    .line 1322
    invoke-virtual {v1}, Lee/cyber/smartid/tse/dto/KeyState;->isActiveSubmitClientSecondPart()Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "submitClientSecondPart"

    goto :goto_0

    :cond_1
    const-string v1, "none"

    .line 1332
    :goto_0
    new-instance v3, Lee/cyber/smartid/tse/dto/jsonrpc/resp/CheckLocalPendingStateResp;

    invoke-direct {v3, p1, p2, v1, v2}, Lee/cyber/smartid/tse/dto/jsonrpc/resp/CheckLocalPendingStateResp;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    monitor-exit v0

    return-object v3

    :catchall_0
    move-exception p1

    .line 1333
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method a(Ljava/lang/String;Lee/cyber/smartid/tse/inter/TSEListener;Ljava/lang/Class;)Lee/cyber/smartid/tse/inter/TSEListener;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lee/cyber/smartid/tse/inter/TSEListener;",
            ">(",
            "Ljava/lang/String;",
            "TT;",
            "Ljava/lang/Class<",
            "+",
            "Lee/cyber/smartid/tse/inter/TSEListener;",
            ">;)TT;"
        }
    .end annotation

    if-nez p2, :cond_1

    if-eqz p3, :cond_0

    .line 1094
    invoke-virtual {p3}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object p2

    const-class p3, Lee/cyber/smartid/tse/inter/SubmitClientSecondPartListener;

    invoke-virtual {p3}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p2, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_0

    .line 1095
    iget-object p2, p0, Lee/cyber/smartid/tse/KeyStateManager;->t:Lee/cyber/smartid/tse/inter/LogAccess;

    const-string p3, "chainSystemListenerIfNeeded: Created a SubmitClientSecondPartListener"

    invoke-interface {p2, p3}, Lee/cyber/smartid/tse/inter/LogAccess;->d(Ljava/lang/String;)V

    .line 1098
    new-instance p2, Lee/cyber/smartid/tse/KeyStateManager$13;

    invoke-direct {p2, p0, p1}, Lee/cyber/smartid/tse/KeyStateManager$13;-><init>(Lee/cyber/smartid/tse/KeyStateManager;Ljava/lang/String;)V

    return-object p2

    :cond_0
    const/4 p1, 0x0

    return-object p1

    .line 1114
    :cond_1
    instance-of p3, p2, Lee/cyber/smartid/tse/inter/SubmitClientSecondPartListener;

    if-eqz p3, :cond_2

    .line 1115
    iget-object p3, p0, Lee/cyber/smartid/tse/KeyStateManager;->t:Lee/cyber/smartid/tse/inter/LogAccess;

    const-string v0, "chainSystemListenerIfNeeded: Chained a SubmitClientSecondPartListener"

    invoke-interface {p3, v0}, Lee/cyber/smartid/tse/inter/LogAccess;->d(Ljava/lang/String;)V

    .line 1117
    new-instance p3, Lee/cyber/smartid/tse/KeyStateManager$14;

    invoke-direct {p3, p0, p2, p1}, Lee/cyber/smartid/tse/KeyStateManager$14;-><init>(Lee/cyber/smartid/tse/KeyStateManager;Lee/cyber/smartid/tse/inter/TSEListener;Ljava/lang/String;)V

    return-object p3

    :cond_2
    return-object p2
.end method

.method a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/Class<",
            "+",
            "Lee/cyber/smartid/tse/inter/TSEListener;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .line 1164
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    return-object p1

    :cond_0
    if-eqz p3, :cond_1

    .line 1166
    invoke-virtual {p3}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object p1

    const-class v0, Lee/cyber/smartid/tse/inter/SubmitClientSecondPartListener;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result p1

    if-eqz p1, :cond_1

    .line 1167
    iget-object p1, p0, Lee/cyber/smartid/tse/KeyStateManager;->t:Lee/cyber/smartid/tse/inter/LogAccess;

    const-string p3, "createSystemListenerTagIfNeeded: Created a tag for SubmitClientSecondPartListener"

    invoke-interface {p1, p3}, Lee/cyber/smartid/tse/inter/LogAccess;->d(Ljava/lang/String;)V

    .line 1168
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string p3, "ee.cyber.smartid.TAG_SUBMIT_CLIENT_SECOND_PART_"

    invoke-virtual {p1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1

    :cond_1
    if-eqz p3, :cond_2

    .line 1169
    invoke-virtual {p3}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object p1

    const-class p3, Lee/cyber/smartid/tse/inter/RefreshCloneDetectionListener;

    invoke-virtual {p3}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object p3

    invoke-static {p1, p3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result p1

    if-eqz p1, :cond_2

    .line 1170
    iget-object p1, p0, Lee/cyber/smartid/tse/KeyStateManager;->t:Lee/cyber/smartid/tse/inter/LogAccess;

    const-string p3, "createSystemListenerTagIfNeeded: Created a tag for RefreshCloneDetectionListener"

    invoke-interface {p1, p3}, Lee/cyber/smartid/tse/inter/LogAccess;->d(Ljava/lang/String;)V

    .line 1171
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string p3, "ee.cyber.smartid.TAG_REFRESH_CLONE_DETECTION_KEYSTATE_ID_"

    invoke-virtual {p1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1

    :cond_2
    const/4 p1, 0x0

    return-object p1
.end method

.method a(Landroid/os/Bundle;)V
    .locals 9

    .line 909
    iget-object v0, p0, Lee/cyber/smartid/tse/KeyStateManager;->k:Ljava/lang/Object;

    monitor-enter v0

    .line 912
    :try_start_0
    iget-object v1, p0, Lee/cyber/smartid/tse/KeyStateManager;->q:Lee/cyber/smartid/tse/inter/ExternalResourceAccess;

    invoke-interface {v1}, Lee/cyber/smartid/tse/inter/ExternalResourceAccess;->getKeyIds()Ljava/util/ArrayList;

    move-result-object v1

    if-eqz v1, :cond_6

    .line 913
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-nez v2, :cond_0

    goto/16 :goto_2

    .line 917
    :cond_0
    iget-object v2, p0, Lee/cyber/smartid/tse/KeyStateManager;->t:Lee/cyber/smartid/tse/inter/LogAccess;

    const-string v3, "updateCloneDetection - starting .."

    invoke-interface {v2, v3}, Lee/cyber/smartid/tse/inter/LogAccess;->d(Ljava/lang/String;)V

    .line 922
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 924
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    const/4 v3, 0x0

    :cond_1
    const/4 v4, 0x0

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 925
    iget-object v6, p0, Lee/cyber/smartid/tse/KeyStateManager;->d:Lee/cyber/smartid/tse/inter/KeyManagerAccess;

    invoke-interface {v6, v5}, Lee/cyber/smartid/tse/inter/KeyManagerAccess;->getKey(Ljava/lang/String;)Lee/cyber/smartid/tse/dto/Key;

    move-result-object v6

    if-nez v6, :cond_2

    goto :goto_0

    .line 931
    :cond_2
    iget-object v6, p0, Lee/cyber/smartid/tse/KeyStateManager;->d:Lee/cyber/smartid/tse/inter/KeyManagerAccess;

    invoke-interface {v6, v5}, Lee/cyber/smartid/tse/inter/KeyManagerAccess;->getKeyStateByKeyId(Ljava/lang/String;)Lee/cyber/smartid/tse/dto/KeyState;

    move-result-object v6

    .line 932
    iget-object v7, p0, Lee/cyber/smartid/tse/KeyStateManager;->d:Lee/cyber/smartid/tse/inter/KeyManagerAccess;

    iget-object v8, p0, Lee/cyber/smartid/tse/KeyStateManager;->c:Lee/cyber/smartid/tse/inter/ResourceAccess;

    invoke-interface {v8, v5}, Lee/cyber/smartid/tse/inter/ResourceAccess;->getKeyStateIdByKeyId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v7, v5}, Lee/cyber/smartid/tse/inter/KeyManagerAccess;->getKeyStateMetaByKeyStateId(Ljava/lang/String;)Lee/cyber/smartid/tse/dto/KeyStateMeta;

    move-result-object v5

    .line 934
    invoke-direct {p0, v2, v6, v5}, Lee/cyber/smartid/tse/KeyStateManager;->a(Ljava/util/ArrayList;Lee/cyber/smartid/tse/dto/KeyState;Lee/cyber/smartid/tse/dto/KeyStateMeta;)Z

    move-result v5

    if-nez v5, :cond_3

    if-eqz v4, :cond_1

    :cond_3
    const/4 v4, 0x1

    goto :goto_0

    :cond_4
    if-eqz v4, :cond_5

    .line 940
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_7

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lee/cyber/smartid/tse/dto/ProtoKeyState;

    .line 941
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "ee.cyber.smartid.TAG_REFRESH_CLONE_DETECTION_KEYSTATE_ID_"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Lee/cyber/smartid/tse/dto/ProtoKeyState;->getId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v2}, Lee/cyber/smartid/tse/KeyStateManager;->b(Lee/cyber/smartid/tse/dto/ProtoKeyState;)Lee/cyber/smartid/tse/inter/TSEListener;

    move-result-object v4

    invoke-virtual {p0, v3, v2, p1, v4}, Lee/cyber/smartid/tse/KeyStateManager;->queue(Ljava/lang/String;Lee/cyber/smartid/tse/dto/ProtoKeyState;Landroid/os/Bundle;Lee/cyber/smartid/tse/inter/TSEListener;)V

    .line 942
    iget-object v3, p0, Lee/cyber/smartid/tse/KeyStateManager;->t:Lee/cyber/smartid/tse/inter/LogAccess;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "updateCloneDetection - key type "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Lee/cyber/smartid/tse/dto/ProtoKeyState;->getKeyType()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, " is in idle state for accountUUID "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Lee/cyber/smartid/tse/dto/ProtoKeyState;->getAccountUUID()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, ", checking for refresh clone detection"

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v3, v2}, Lee/cyber/smartid/tse/inter/LogAccess;->d(Ljava/lang/String;)V

    goto :goto_1

    .line 945
    :cond_5
    iget-object p1, p0, Lee/cyber/smartid/tse/KeyStateManager;->t:Lee/cyber/smartid/tse/inter/LogAccess;

    const-string v1, "updateCloneDetection - no need for updateCloneDetection"

    invoke-interface {p1, v1}, Lee/cyber/smartid/tse/inter/LogAccess;->d(Ljava/lang/String;)V

    goto :goto_3

    .line 914
    :cond_6
    :goto_2
    iget-object p1, p0, Lee/cyber/smartid/tse/KeyStateManager;->t:Lee/cyber/smartid/tse/inter/LogAccess;

    const-string v1, "updateCloneDetection - no need, no key ids found .."

    invoke-interface {p1, v1}, Lee/cyber/smartid/tse/inter/LogAccess;->d(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 915
    :try_start_1
    monitor-exit v0

    return-void

    :catchall_0
    move-exception p1

    goto :goto_4

    :catch_0
    move-exception p1

    .line 948
    iget-object v1, p0, Lee/cyber/smartid/tse/KeyStateManager;->t:Lee/cyber/smartid/tse/inter/LogAccess;

    const-string v2, "updateCloneDetection"

    invoke-interface {v1, v2, p1}, Lee/cyber/smartid/tse/inter/LogAccess;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 950
    :cond_7
    :goto_3
    monitor-exit v0

    return-void

    :goto_4
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw p1
.end method

.method a(Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lee/cyber/smartid/cryptolib/dto/StorageException;
        }
    .end annotation

    .line 1040
    iget-object v0, p0, Lee/cyber/smartid/tse/KeyStateManager;->d:Lee/cyber/smartid/tse/inter/KeyManagerAccess;

    invoke-interface {v0, p1}, Lee/cyber/smartid/tse/inter/KeyManagerAccess;->getKeyStateById(Ljava/lang/String;)Lee/cyber/smartid/tse/dto/KeyState;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 1041
    invoke-virtual {v0}, Lee/cyber/smartid/tse/dto/KeyState;->isInActiveState()Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_1

    .line 1045
    :cond_0
    iget-object v1, p0, Lee/cyber/smartid/tse/KeyStateManager;->d:Lee/cyber/smartid/tse/inter/KeyManagerAccess;

    invoke-interface {v1, p1}, Lee/cyber/smartid/tse/inter/KeyManagerAccess;->getKeyStateMetaByKeyStateId(Ljava/lang/String;)Lee/cyber/smartid/tse/dto/KeyStateMeta;

    move-result-object p1

    .line 1046
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1047
    invoke-direct {p0, v1, v0, p1}, Lee/cyber/smartid/tse/KeyStateManager;->a(Ljava/util/ArrayList;Lee/cyber/smartid/tse/dto/KeyState;Lee/cyber/smartid/tse/dto/KeyStateMeta;)Z

    .line 1048
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result p1

    if-lez p1, :cond_1

    .line 1049
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lee/cyber/smartid/tse/dto/ProtoKeyState;

    .line 1050
    iget-object v2, p0, Lee/cyber/smartid/tse/KeyStateManager;->t:Lee/cyber/smartid/tse/inter/LogAccess;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "updateCloneDetection - triggering clone detection for "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lee/cyber/smartid/tse/inter/LogAccess;->d(Ljava/lang/String;)V

    .line 1051
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ee.cyber.smartid.TAG_REFRESH_CLONE_DETECTION_KEYSTATE_ID_"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Lee/cyber/smartid/tse/dto/KeyState;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v1}, Lee/cyber/smartid/tse/KeyStateManager;->b(Lee/cyber/smartid/tse/dto/ProtoKeyState;)Lee/cyber/smartid/tse/inter/TSEListener;

    move-result-object v3

    invoke-virtual {p0, v2, v1, p2, v3}, Lee/cyber/smartid/tse/KeyStateManager;->queue(Ljava/lang/String;Lee/cyber/smartid/tse/dto/ProtoKeyState;Landroid/os/Bundle;Lee/cyber/smartid/tse/inter/TSEListener;)V

    goto :goto_0

    .line 1054
    :cond_1
    iget-object p1, p0, Lee/cyber/smartid/tse/KeyStateManager;->t:Lee/cyber/smartid/tse/inter/LogAccess;

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "updateCloneDetection - No clone detection was triggered for "

    invoke-virtual {p2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Lee/cyber/smartid/tse/dto/KeyState;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-interface {p1, p2}, Lee/cyber/smartid/tse/inter/LogAccess;->d(Ljava/lang/String;)V

    :cond_2
    return-void

    .line 1042
    :cond_3
    :goto_1
    iget-object p1, p0, Lee/cyber/smartid/tse/KeyStateManager;->t:Lee/cyber/smartid/tse/inter/LogAccess;

    const-string p2, "updateCloneDetection - KeyState is null or in an active state, aborting!"

    invoke-interface {p1, p2}, Lee/cyber/smartid/tse/inter/LogAccess;->w(Ljava/lang/String;)V

    return-void
.end method

.method b(Landroid/os/Bundle;)V
    .locals 11

    .line 1210
    iget-object v0, p0, Lee/cyber/smartid/tse/KeyStateManager;->k:Ljava/lang/Object;

    monitor-enter v0

    .line 1213
    :try_start_0
    iget-object v1, p0, Lee/cyber/smartid/tse/KeyStateManager;->q:Lee/cyber/smartid/tse/inter/ExternalResourceAccess;

    invoke-interface {v1}, Lee/cyber/smartid/tse/inter/ExternalResourceAccess;->getKeyIds()Ljava/util/ArrayList;

    move-result-object v1

    if-eqz v1, :cond_8

    .line 1214
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-nez v2, :cond_0

    goto/16 :goto_1

    .line 1218
    :cond_0
    iget-object v2, p0, Lee/cyber/smartid/tse/KeyStateManager;->t:Lee/cyber/smartid/tse/inter/LogAccess;

    const-string v3, "checkKeyStates - starting .."

    invoke-interface {v2, v3}, Lee/cyber/smartid/tse/inter/LogAccess;->d(Ljava/lang/String;)V

    const/4 v2, 0x0

    .line 1222
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_7

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 1223
    iget-object v4, p0, Lee/cyber/smartid/tse/KeyStateManager;->d:Lee/cyber/smartid/tse/inter/KeyManagerAccess;

    invoke-interface {v4, v3}, Lee/cyber/smartid/tse/inter/KeyManagerAccess;->getKey(Ljava/lang/String;)Lee/cyber/smartid/tse/dto/Key;

    move-result-object v4

    if-nez v4, :cond_2

    goto :goto_0

    .line 1229
    :cond_2
    iget-object v4, p0, Lee/cyber/smartid/tse/KeyStateManager;->d:Lee/cyber/smartid/tse/inter/KeyManagerAccess;

    invoke-interface {v4, v3}, Lee/cyber/smartid/tse/inter/KeyManagerAccess;->getKeyStateByKeyId(Ljava/lang/String;)Lee/cyber/smartid/tse/dto/KeyState;

    move-result-object v4

    .line 1230
    iget-object v5, p0, Lee/cyber/smartid/tse/KeyStateManager;->d:Lee/cyber/smartid/tse/inter/KeyManagerAccess;

    iget-object v6, p0, Lee/cyber/smartid/tse/KeyStateManager;->c:Lee/cyber/smartid/tse/inter/ResourceAccess;

    invoke-interface {v6, v3}, Lee/cyber/smartid/tse/inter/ResourceAccess;->getKeyStateIdByKeyId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-interface {v5, v6}, Lee/cyber/smartid/tse/inter/KeyManagerAccess;->getKeyStateMetaByKeyStateId(Ljava/lang/String;)Lee/cyber/smartid/tse/dto/KeyStateMeta;

    move-result-object v5

    if-eqz v4, :cond_1

    .line 1231
    invoke-virtual {v4}, Lee/cyber/smartid/tse/dto/KeyState;->isInActiveState()Z

    move-result v6

    if-nez v6, :cond_3

    goto :goto_0

    .line 1236
    :cond_3
    iget-object v6, p0, Lee/cyber/smartid/tse/KeyStateManager;->r:Lee/cyber/smartid/tse/inter/WallClock;

    invoke-virtual {v5, v6}, Lee/cyber/smartid/tse/dto/KeyStateMeta;->isKeyLocked(Lee/cyber/smartid/tse/inter/WallClock;)Z

    move-result v6

    if-nez v6, :cond_4

    invoke-virtual {v4}, Lee/cyber/smartid/tse/dto/KeyState;->getId()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6}, Lee/cyber/smartid/tse/KeyStateManager;->isKeyStateInProgress(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_4

    invoke-virtual {v4}, Lee/cyber/smartid/tse/dto/KeyState;->getId()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6}, Lee/cyber/smartid/tse/KeyStateManager;->isKeyStateQueued(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_4

    .line 1237
    iget-object v2, p0, Lee/cyber/smartid/tse/KeyStateManager;->t:Lee/cyber/smartid/tse/inter/LogAccess;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "checkKeyStates - key "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, ", we will try to resolve .."

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lee/cyber/smartid/tse/inter/LogAccess;->d(Ljava/lang/String;)V

    const/4 v2, 0x0

    .line 1239
    invoke-direct {p0, v2, v4}, Lee/cyber/smartid/tse/KeyStateManager;->a(Ljava/lang/String;Lee/cyber/smartid/tse/dto/ProtoKeyState;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v4}, Lee/cyber/smartid/tse/dto/ProtoKeyState;->retryFrom(Lee/cyber/smartid/tse/dto/KeyState;)Lee/cyber/smartid/tse/dto/ProtoKeyState;

    move-result-object v3

    invoke-direct {p0, v4}, Lee/cyber/smartid/tse/KeyStateManager;->b(Lee/cyber/smartid/tse/dto/ProtoKeyState;)Lee/cyber/smartid/tse/inter/TSEListener;

    move-result-object v4

    invoke-virtual {p0, v2, v3, p1, v4}, Lee/cyber/smartid/tse/KeyStateManager;->queue(Ljava/lang/String;Lee/cyber/smartid/tse/dto/ProtoKeyState;Landroid/os/Bundle;Lee/cyber/smartid/tse/inter/TSEListener;)V

    const/4 v2, 0x1

    goto :goto_0

    .line 1241
    :cond_4
    iget-object v6, p0, Lee/cyber/smartid/tse/KeyStateManager;->r:Lee/cyber/smartid/tse/inter/WallClock;

    invoke-virtual {v5, v6}, Lee/cyber/smartid/tse/dto/KeyStateMeta;->isKeyLocked(Lee/cyber/smartid/tse/inter/WallClock;)Z

    move-result v6

    if-eqz v6, :cond_6

    invoke-virtual {v4}, Lee/cyber/smartid/tse/dto/KeyState;->getId()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6}, Lee/cyber/smartid/tse/KeyStateManager;->isKeyStateInProgress(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_6

    invoke-virtual {v4}, Lee/cyber/smartid/tse/dto/KeyState;->getId()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6}, Lee/cyber/smartid/tse/KeyStateManager;->isKeyStateQueued(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_6

    .line 1243
    invoke-virtual {v5}, Lee/cyber/smartid/tse/dto/KeyStateMeta;->isKeyLockedPermanently()Z

    move-result v6

    if-nez v6, :cond_5

    iget-object v6, p0, Lee/cyber/smartid/tse/KeyStateManager;->r:Lee/cyber/smartid/tse/inter/WallClock;

    invoke-virtual {v5, v6}, Lee/cyber/smartid/tse/dto/KeyStateMeta;->getKeyLockRemainingDuration(Lee/cyber/smartid/tse/inter/WallClock;)J

    move-result-wide v6

    const-wide/16 v8, -0x1

    cmp-long v10, v6, v8

    if-eqz v10, :cond_5

    .line 1245
    iget-object v6, p0, Lee/cyber/smartid/tse/KeyStateManager;->t:Lee/cyber/smartid/tse/inter/LogAccess;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "checkKeyStates - key "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, " is in active state, but temp locked, setting a new alarm .."

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v6, v3}, Lee/cyber/smartid/tse/inter/LogAccess;->d(Ljava/lang/String;)V

    .line 1246
    iget-object v3, p0, Lee/cyber/smartid/tse/KeyStateManager;->l:Lee/cyber/smartid/tse/SmartIdTSE$TSEAlarmAccess;

    invoke-virtual {v4}, Lee/cyber/smartid/tse/dto/KeyState;->getId()Ljava/lang/String;

    move-result-object v4

    iget-object v6, p0, Lee/cyber/smartid/tse/KeyStateManager;->r:Lee/cyber/smartid/tse/inter/WallClock;

    invoke-virtual {v5, v6}, Lee/cyber/smartid/tse/dto/KeyStateMeta;->getKeyLockRemainingDuration(Lee/cyber/smartid/tse/inter/WallClock;)J

    move-result-wide v5

    const-wide/16 v7, 0x1388

    add-long/2addr v5, v7

    invoke-virtual {v3, v4, v5, v6}, Lee/cyber/smartid/tse/SmartIdTSE$TSEAlarmAccess;->setKeyStateSolveAlarm(Ljava/lang/String;J)V

    goto/16 :goto_0

    .line 1249
    :cond_5
    iget-object v4, p0, Lee/cyber/smartid/tse/KeyStateManager;->t:Lee/cyber/smartid/tse/inter/LogAccess;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "checkKeyStates - key "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, " is in active state, but perma locked, skipping .."

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v4, v3}, Lee/cyber/smartid/tse/inter/LogAccess;->d(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1252
    :cond_6
    iget-object v4, p0, Lee/cyber/smartid/tse/KeyStateManager;->t:Lee/cyber/smartid/tse/inter/LogAccess;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "checkKeyStates - key "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, " is in active state, already in progress, we will do nothing .."

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v4, v3}, Lee/cyber/smartid/tse/inter/LogAccess;->d(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_7
    if-nez v2, :cond_9

    .line 1257
    iget-object p1, p0, Lee/cyber/smartid/tse/KeyStateManager;->t:Lee/cyber/smartid/tse/inter/LogAccess;

    const-string v1, "checkKeyStates - no need resolve any KeyStates .."

    invoke-interface {p1, v1}, Lee/cyber/smartid/tse/inter/LogAccess;->d(Ljava/lang/String;)V

    goto :goto_2

    .line 1215
    :cond_8
    :goto_1
    iget-object p1, p0, Lee/cyber/smartid/tse/KeyStateManager;->t:Lee/cyber/smartid/tse/inter/LogAccess;

    const-string v1, "checkKeyStates - no need, no keyIds found .."

    invoke-interface {p1, v1}, Lee/cyber/smartid/tse/inter/LogAccess;->d(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1216
    :try_start_1
    monitor-exit v0

    return-void

    :catchall_0
    move-exception p1

    goto :goto_3

    :catch_0
    move-exception p1

    .line 1260
    iget-object v1, p0, Lee/cyber/smartid/tse/KeyStateManager;->t:Lee/cyber/smartid/tse/inter/LogAccess;

    const-string v2, "checkKeyStates"

    invoke-interface {v1, v2, p1}, Lee/cyber/smartid/tse/inter/LogAccess;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1262
    :cond_9
    :goto_2
    monitor-exit v0

    return-void

    :goto_3
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw p1
.end method

.method b(Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 5

    .line 1184
    iget-object v0, p0, Lee/cyber/smartid/tse/KeyStateManager;->k:Ljava/lang/Object;

    monitor-enter v0

    .line 1185
    :try_start_0
    iget-object v1, p0, Lee/cyber/smartid/tse/KeyStateManager;->d:Lee/cyber/smartid/tse/inter/KeyManagerAccess;

    invoke-interface {v1, p1}, Lee/cyber/smartid/tse/inter/KeyManagerAccess;->getKeyStateById(Ljava/lang/String;)Lee/cyber/smartid/tse/dto/KeyState;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 1186
    invoke-virtual {v1}, Lee/cyber/smartid/tse/dto/KeyState;->isInActiveState()Z

    move-result v2

    if-nez v2, :cond_0

    goto :goto_1

    .line 1190
    :cond_0
    iget-object v2, p0, Lee/cyber/smartid/tse/KeyStateManager;->t:Lee/cyber/smartid/tse/inter/LogAccess;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "checkKeyState called for "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lee/cyber/smartid/tse/inter/LogAccess;->d(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v2, 0x0

    .line 1192
    :try_start_1
    invoke-direct {p0, v2, v1}, Lee/cyber/smartid/tse/KeyStateManager;->a(Ljava/lang/String;Lee/cyber/smartid/tse/dto/ProtoKeyState;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1}, Lee/cyber/smartid/tse/dto/ProtoKeyState;->retryFrom(Lee/cyber/smartid/tse/dto/KeyState;)Lee/cyber/smartid/tse/dto/ProtoKeyState;

    move-result-object v3

    invoke-direct {p0, v1}, Lee/cyber/smartid/tse/KeyStateManager;->b(Lee/cyber/smartid/tse/dto/ProtoKeyState;)Lee/cyber/smartid/tse/inter/TSEListener;

    move-result-object v4

    invoke-virtual {p0, v2, v3, p2, v4}, Lee/cyber/smartid/tse/KeyStateManager;->queue(Ljava/lang/String;Lee/cyber/smartid/tse/dto/ProtoKeyState;Landroid/os/Bundle;Lee/cyber/smartid/tse/inter/TSEListener;)V
    :try_end_1
    .catch Lee/cyber/smartid/cryptolib/dto/StorageException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    move-exception p2

    .line 1194
    :try_start_2
    iget-object v2, p0, Lee/cyber/smartid/tse/KeyStateManager;->t:Lee/cyber/smartid/tse/inter/LogAccess;

    const-string v3, "checkKeyState "

    invoke-interface {v2, v3, p2}, Lee/cyber/smartid/tse/inter/LogAccess;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1196
    iget-object p2, p0, Lee/cyber/smartid/tse/KeyStateManager;->d:Lee/cyber/smartid/tse/inter/KeyManagerAccess;

    invoke-interface {p2, p1}, Lee/cyber/smartid/tse/inter/KeyManagerAccess;->getKeyStateMetaByKeyStateId(Ljava/lang/String;)Lee/cyber/smartid/tse/dto/KeyStateMeta;

    move-result-object p1

    .line 1197
    iget-object p2, p0, Lee/cyber/smartid/tse/KeyStateManager;->l:Lee/cyber/smartid/tse/SmartIdTSE$TSEAlarmAccess;

    invoke-virtual {p2, v1, p1}, Lee/cyber/smartid/tse/SmartIdTSE$TSEAlarmAccess;->setKeyStateSolveAlarm(Lee/cyber/smartid/tse/dto/KeyState;Lee/cyber/smartid/tse/dto/KeyStateMeta;)V

    .line 1199
    :goto_0
    monitor-exit v0

    return-void

    .line 1187
    :cond_1
    :goto_1
    iget-object p1, p0, Lee/cyber/smartid/tse/KeyStateManager;->t:Lee/cyber/smartid/tse/inter/LogAccess;

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "checkKeyState invalid call for "

    invoke-virtual {p2, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-interface {p1, p2}, Lee/cyber/smartid/tse/inter/LogAccess;->w(Ljava/lang/String;)V

    .line 1188
    monitor-exit v0

    return-void

    :catchall_0
    move-exception p1

    .line 1199
    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw p1
.end method

.method public checkLocalPendingState(Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/tse/inter/CheckLocalPendingStateListener;)V
    .locals 1

    .line 1278
    iget-object v0, p0, Lee/cyber/smartid/tse/KeyStateManager;->e:Lee/cyber/smartid/tse/inter/ListenerAccess;

    invoke-interface {v0, p1, p3}, Lee/cyber/smartid/tse/inter/ListenerAccess;->setListener(Ljava/lang/String;Lee/cyber/smartid/tse/inter/TSEListener;)V

    .line 1279
    new-instance p3, Ljava/lang/Thread;

    new-instance v0, Lee/cyber/smartid/tse/KeyStateManager$15;

    invoke-direct {v0, p0, p1, p2}, Lee/cyber/smartid/tse/KeyStateManager$15;-><init>(Lee/cyber/smartid/tse/KeyStateManager;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p3, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 1307
    invoke-virtual {p3}, Ljava/lang/Thread;->start()V

    return-void
.end method

.method public confirmTransaction(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Lee/cyber/smartid/tse/inter/ConfirmTransactionListener;)V
    .locals 21

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v0, p2

    move-object/from16 v12, p10

    .line 778
    iget-object v3, v1, Lee/cyber/smartid/tse/KeyStateManager;->t:Lee/cyber/smartid/tse/inter/LogAccess;

    const-string v4, "confirmTransaction"

    invoke-interface {v3, v4}, Lee/cyber/smartid/tse/inter/LogAccess;->d(Ljava/lang/String;)V

    .line 779
    iget-object v13, v1, Lee/cyber/smartid/tse/KeyStateManager;->k:Ljava/lang/Object;

    monitor-enter v13

    .line 780
    :try_start_0
    iget-object v3, v1, Lee/cyber/smartid/tse/KeyStateManager;->t:Lee/cyber/smartid/tse/inter/LogAccess;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "confirmTransaction - keyType: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-object/from16 v6, p3

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Lee/cyber/smartid/tse/inter/LogAccess;->d(Ljava/lang/String;)V

    .line 781
    iget-object v3, v1, Lee/cyber/smartid/tse/KeyStateManager;->d:Lee/cyber/smartid/tse/inter/KeyManagerAccess;

    invoke-interface {v3, v0}, Lee/cyber/smartid/tse/inter/KeyManagerAccess;->getKey(Ljava/lang/String;)Lee/cyber/smartid/tse/dto/Key;

    move-result-object v3

    if-nez v3, :cond_0

    .line 785
    iget-object v0, v1, Lee/cyber/smartid/tse/KeyStateManager;->e:Lee/cyber/smartid/tse/inter/ListenerAccess;

    new-instance v3, Lee/cyber/smartid/tse/KeyStateManager$4;

    invoke-direct {v3, v1, v12, v2}, Lee/cyber/smartid/tse/KeyStateManager$4;-><init>(Lee/cyber/smartid/tse/KeyStateManager;Lee/cyber/smartid/tse/inter/ConfirmTransactionListener;Ljava/lang/String;)V

    invoke-interface {v0, v3}, Lee/cyber/smartid/tse/inter/ListenerAccess;->notifyUI(Ljava/lang/Runnable;)V

    .line 793
    monitor-exit v13

    return-void

    .line 797
    :cond_0
    iget-object v4, v1, Lee/cyber/smartid/tse/KeyStateManager;->c:Lee/cyber/smartid/tse/inter/ResourceAccess;

    invoke-virtual {v3}, Lee/cyber/smartid/tse/dto/Key;->getSZId()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Lee/cyber/smartid/tse/inter/ResourceAccess;->getPreferredSZKTKPublicKey(Ljava/lang/String;)Lee/cyber/smartid/tse/dto/KTKPublicKey;

    move-result-object v4

    if-nez v4, :cond_1

    .line 800
    iget-object v0, v1, Lee/cyber/smartid/tse/KeyStateManager;->e:Lee/cyber/smartid/tse/inter/ListenerAccess;

    new-instance v3, Lee/cyber/smartid/tse/KeyStateManager$5;

    invoke-direct {v3, v1, v12, v2}, Lee/cyber/smartid/tse/KeyStateManager$5;-><init>(Lee/cyber/smartid/tse/KeyStateManager;Lee/cyber/smartid/tse/inter/ConfirmTransactionListener;Ljava/lang/String;)V

    invoke-interface {v0, v3}, Lee/cyber/smartid/tse/inter/ListenerAccess;->notifyUI(Ljava/lang/Runnable;)V

    .line 808
    monitor-exit v13
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    .line 815
    :cond_1
    :try_start_1
    iget-object v14, v1, Lee/cyber/smartid/tse/KeyStateManager;->m:Lee/cyber/smartid/cryptolib/inter/SigningOp;

    invoke-virtual {v3}, Lee/cyber/smartid/tse/dto/Key;->getD1Prime()Lee/cyber/smartid/cryptolib/dto/ClientShare;

    move-result-object v15

    invoke-virtual {v3}, Lee/cyber/smartid/tse/dto/Key;->getN1()Ljava/lang/String;

    move-result-object v16

    invoke-virtual {v3}, Lee/cyber/smartid/tse/dto/Key;->getCompositeModulusBits()I

    move-result v17

    move-object/from16 v18, p8

    move-object/from16 v19, p6

    move-object/from16 v20, p7

    invoke-interface/range {v14 .. v20}, Lee/cyber/smartid/cryptolib/inter/SigningOp;->sign(Lee/cyber/smartid/cryptolib/dto/ClientShare;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5
    :try_end_1
    .catch Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException; {:try_start_1 .. :try_end_1} :catch_4
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_3
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 843
    :try_start_2
    invoke-direct/range {p0 .. p0}, Lee/cyber/smartid/tse/KeyStateManager;->b()Ljava/lang/String;

    move-result-object v7
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 858
    :try_start_3
    iget-object v8, v1, Lee/cyber/smartid/tse/KeyStateManager;->p:Lee/cyber/smartid/cryptolib/inter/CryptoOp;

    const-string v9, "CLIENTSIGNATURESHARE"

    invoke-virtual {v3}, Lee/cyber/smartid/tse/dto/Key;->getKeySize()I

    move-result v3

    invoke-static {v4, v8, v5, v9, v3}, Lee/cyber/smartid/tse/util/Util;->encryptToKTKEncryptedJWE(Lee/cyber/smartid/tse/dto/KTKPublicKey;Lee/cyber/smartid/cryptolib/inter/CryptoOp;Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v9
    :try_end_3
    .catch Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 873
    :try_start_4
    iget-object v3, v1, Lee/cyber/smartid/tse/KeyStateManager;->c:Lee/cyber/smartid/tse/inter/ResourceAccess;

    invoke-interface {v3, v0}, Lee/cyber/smartid/tse/inter/ResourceAccess;->getKeyStateIdByKeyId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v4, p2

    move-object/from16 v5, p4

    move-object/from16 v6, p3

    move-object/from16 v8, p5

    move-object/from16 v10, p6

    move-object/from16 v11, p7

    invoke-static/range {v3 .. v11}, Lee/cyber/smartid/tse/dto/ProtoKeyState;->forSign(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lee/cyber/smartid/tse/dto/ProtoKeyState;

    move-result-object v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move-object/from16 v3, p9

    .line 876
    :try_start_5
    invoke-virtual {v1, v2, v0, v3, v12}, Lee/cyber/smartid/tse/KeyStateManager;->queue(Ljava/lang/String;Lee/cyber/smartid/tse/dto/ProtoKeyState;Landroid/os/Bundle;Lee/cyber/smartid/tse/inter/TSEListener;)V
    :try_end_5
    .catch Lee/cyber/smartid/cryptolib/dto/StorageException; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 890
    :try_start_6
    monitor-exit v13

    return-void

    :catch_0
    move-exception v0

    move-object v3, v0

    .line 878
    iget-object v0, v1, Lee/cyber/smartid/tse/KeyStateManager;->t:Lee/cyber/smartid/tse/inter/LogAccess;

    const-string v4, "confirmTransaction"

    invoke-interface {v0, v4, v3}, Lee/cyber/smartid/tse/inter/LogAccess;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 879
    new-instance v0, Lee/cyber/smartid/tse/KeyStateManager$10;

    invoke-direct {v0, v1, v12, v2, v3}, Lee/cyber/smartid/tse/KeyStateManager$10;-><init>(Lee/cyber/smartid/tse/KeyStateManager;Lee/cyber/smartid/tse/inter/ConfirmTransactionListener;Ljava/lang/String;Lee/cyber/smartid/cryptolib/dto/StorageException;)V

    invoke-direct {v1, v0}, Lee/cyber/smartid/tse/KeyStateManager;->a(Ljava/lang/Runnable;)V

    .line 888
    monitor-exit v13

    return-void

    :catch_1
    move-exception v0

    .line 860
    iget-object v3, v1, Lee/cyber/smartid/tse/KeyStateManager;->t:Lee/cyber/smartid/tse/inter/LogAccess;

    const-string v4, "confirmTransaction"

    invoke-interface {v3, v4, v0}, Lee/cyber/smartid/tse/inter/LogAccess;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 861
    new-instance v3, Lee/cyber/smartid/tse/KeyStateManager$9;

    invoke-direct {v3, v1, v12, v2, v0}, Lee/cyber/smartid/tse/KeyStateManager$9;-><init>(Lee/cyber/smartid/tse/KeyStateManager;Lee/cyber/smartid/tse/inter/ConfirmTransactionListener;Ljava/lang/String;Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;)V

    invoke-direct {v1, v3}, Lee/cyber/smartid/tse/KeyStateManager;->a(Ljava/lang/Runnable;)V

    .line 869
    monitor-exit v13

    return-void

    :catch_2
    move-exception v0

    move-object v3, v0

    .line 845
    new-instance v0, Lee/cyber/smartid/tse/KeyStateManager$8;

    invoke-direct {v0, v1, v12, v2, v3}, Lee/cyber/smartid/tse/KeyStateManager$8;-><init>(Lee/cyber/smartid/tse/KeyStateManager;Lee/cyber/smartid/tse/inter/ConfirmTransactionListener;Ljava/lang/String;Ljava/lang/Exception;)V

    invoke-direct {v1, v0}, Lee/cyber/smartid/tse/KeyStateManager;->a(Ljava/lang/Runnable;)V

    .line 853
    monitor-exit v13

    return-void

    :catch_3
    move-exception v0

    .line 828
    iget-object v3, v1, Lee/cyber/smartid/tse/KeyStateManager;->t:Lee/cyber/smartid/tse/inter/LogAccess;

    const-string v4, "confirmTransaction"

    invoke-interface {v3, v4, v0}, Lee/cyber/smartid/tse/inter/LogAccess;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 829
    iget-object v3, v1, Lee/cyber/smartid/tse/KeyStateManager;->e:Lee/cyber/smartid/tse/inter/ListenerAccess;

    new-instance v4, Lee/cyber/smartid/tse/KeyStateManager$7;

    invoke-direct {v4, v1, v12, v2, v0}, Lee/cyber/smartid/tse/KeyStateManager$7;-><init>(Lee/cyber/smartid/tse/KeyStateManager;Lee/cyber/smartid/tse/inter/ConfirmTransactionListener;Ljava/lang/String;Ljava/lang/Exception;)V

    invoke-interface {v3, v4}, Lee/cyber/smartid/tse/inter/ListenerAccess;->notifyUI(Ljava/lang/Runnable;)V

    .line 837
    monitor-exit v13

    return-void

    :catch_4
    move-exception v0

    .line 817
    iget-object v3, v1, Lee/cyber/smartid/tse/KeyStateManager;->t:Lee/cyber/smartid/tse/inter/LogAccess;

    const-string v4, "confirmTransaction"

    invoke-interface {v3, v4, v0}, Lee/cyber/smartid/tse/inter/LogAccess;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 818
    iget-object v3, v1, Lee/cyber/smartid/tse/KeyStateManager;->e:Lee/cyber/smartid/tse/inter/ListenerAccess;

    new-instance v4, Lee/cyber/smartid/tse/KeyStateManager$6;

    invoke-direct {v4, v1, v12, v2, v0}, Lee/cyber/smartid/tse/KeyStateManager$6;-><init>(Lee/cyber/smartid/tse/KeyStateManager;Lee/cyber/smartid/tse/inter/ConfirmTransactionListener;Ljava/lang/String;Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;)V

    invoke-interface {v3, v4}, Lee/cyber/smartid/tse/inter/ListenerAccess;->notifyUI(Ljava/lang/Runnable;)V

    .line 826
    monitor-exit v13

    return-void

    :catchall_0
    move-exception v0

    .line 890
    monitor-exit v13
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    throw v0
.end method

.method public getKeyStateInProgressSize()I
    .locals 2

    .line 439
    iget-object v0, p0, Lee/cyber/smartid/tse/KeyStateManager;->j:Ljava/lang/Object;

    monitor-enter v0

    .line 440
    :try_start_0
    iget-object v1, p0, Lee/cyber/smartid/tse/KeyStateManager;->f:Ljava/util/HashSet;

    invoke-virtual {v1}, Ljava/util/HashSet;->size()I

    move-result v1

    monitor-exit v0

    return v1

    :catchall_0
    move-exception v1

    .line 441
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public getKeyStateQueueSize()I
    .locals 2

    .line 425
    iget-object v0, p0, Lee/cyber/smartid/tse/KeyStateManager;->j:Ljava/lang/Object;

    monitor-enter v0

    .line 426
    :try_start_0
    iget-object v1, p0, Lee/cyber/smartid/tse/KeyStateManager;->i:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->size()I

    move-result v1

    monitor-exit v0

    return v1

    :catchall_0
    move-exception v1

    .line 427
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public isAnyKeyStateInProgress()Z
    .locals 2

    .line 576
    iget-object v0, p0, Lee/cyber/smartid/tse/KeyStateManager;->j:Ljava/lang/Object;

    monitor-enter v0

    .line 577
    :try_start_0
    iget-object v1, p0, Lee/cyber/smartid/tse/KeyStateManager;->f:Ljava/util/HashSet;

    invoke-virtual {v1}, Ljava/util/HashSet;->size()I

    move-result v1

    if-lez v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    monitor-exit v0

    return v1

    :catchall_0
    move-exception v1

    .line 578
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public isKeyStateInProgress(Ljava/lang/String;)Z
    .locals 2

    .line 590
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 p1, 0x0

    return p1

    .line 593
    :cond_0
    iget-object v0, p0, Lee/cyber/smartid/tse/KeyStateManager;->j:Ljava/lang/Object;

    monitor-enter v0

    .line 594
    :try_start_0
    iget-object v1, p0, Lee/cyber/smartid/tse/KeyStateManager;->f:Ljava/util/HashSet;

    invoke-virtual {v1, p1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result p1

    monitor-exit v0

    return p1

    :catchall_0
    move-exception p1

    .line 595
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method public isKeyStateQueued(Ljava/lang/String;)Z
    .locals 4

    .line 403
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    return v1

    .line 406
    :cond_0
    iget-object v0, p0, Lee/cyber/smartid/tse/KeyStateManager;->j:Ljava/lang/Object;

    monitor-enter v0

    .line 407
    :try_start_0
    iget-object v2, p0, Lee/cyber/smartid/tse/KeyStateManager;->i:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v2}, Ljava/util/concurrent/ConcurrentLinkedQueue;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lee/cyber/smartid/tse/dto/KeyStateQueue;

    .line 408
    invoke-virtual {v3}, Lee/cyber/smartid/tse/dto/KeyStateQueue;->getState()Lee/cyber/smartid/tse/dto/ProtoKeyState;

    move-result-object v3

    invoke-virtual {v3}, Lee/cyber/smartid/tse/dto/ProtoKeyState;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    const/4 p1, 0x1

    .line 409
    monitor-exit v0

    return p1

    .line 412
    :cond_2
    monitor-exit v0

    return v1

    :catchall_0
    move-exception p1

    .line 413
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method public queue(Ljava/lang/String;Lee/cyber/smartid/tse/dto/ProtoKeyState;Landroid/os/Bundle;Lee/cyber/smartid/tse/inter/TSEListener;)V
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lee/cyber/smartid/cryptolib/dto/StorageException;
        }
    .end annotation

    .line 611
    iget-object v0, p0, Lee/cyber/smartid/tse/KeyStateManager;->j:Ljava/lang/Object;

    monitor-enter v0

    .line 614
    :try_start_0
    instance-of v1, p2, Lee/cyber/smartid/tse/dto/KeyState;

    const-wide/16 v2, 0x3f4    # 5.0E-321

    const/4 v4, 0x1

    if-eqz v1, :cond_0

    .line 615
    iget-object p3, p0, Lee/cyber/smartid/tse/KeyStateManager;->t:Lee/cyber/smartid/tse/inter/LogAccess;

    const-string v1, "queue -  A KeyState object was given instead of ProtoKeyState, aborting .."

    invoke-interface {p3, v1}, Lee/cyber/smartid/tse/inter/LogAccess;->e(Ljava/lang/String;)V

    .line 616
    iget-object p3, p0, Lee/cyber/smartid/tse/KeyStateManager;->r:Lee/cyber/smartid/tse/inter/WallClock;

    iget-object v1, p0, Lee/cyber/smartid/tse/KeyStateManager;->c:Lee/cyber/smartid/tse/inter/ResourceAccess;

    invoke-interface {v1}, Lee/cyber/smartid/tse/inter/ResourceAccess;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    sget v5, Lee/cyber/smartid/tse/R$string;->err_invalid_transaction_state_2:I

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-virtual {p2}, Lee/cyber/smartid/tse/dto/ProtoKeyState;->getHumanReadableNameForType()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-virtual {p2}, Lee/cyber/smartid/tse/dto/ProtoKeyState;->getHumanReadableNameForOperationOrigin()Ljava/lang/String;

    move-result-object p2

    aput-object p2, v6, v4

    invoke-virtual {v1, v5, v6}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p2

    invoke-static {p3, v2, v3, p2}, Lee/cyber/smartid/tse/dto/TSEError;->from(Lee/cyber/smartid/tse/inter/WallClock;JLjava/lang/String;)Lee/cyber/smartid/tse/dto/TSEError;

    move-result-object p2

    invoke-virtual {p2, v4}, Lee/cyber/smartid/tse/dto/TSEError;->asNonRetriable(Z)Lee/cyber/smartid/tse/dto/TSEError;

    move-result-object p2

    invoke-direct {p0, p4, p1, p2}, Lee/cyber/smartid/tse/KeyStateManager;->a(Lee/cyber/smartid/tse/inter/TSEListener;Ljava/lang/String;Lee/cyber/smartid/tse/dto/TSEError;)V

    .line 617
    monitor-exit v0

    return-void

    .line 622
    :cond_0
    iget-object v1, p0, Lee/cyber/smartid/tse/KeyStateManager;->d:Lee/cyber/smartid/tse/inter/KeyManagerAccess;

    invoke-virtual {p2}, Lee/cyber/smartid/tse/dto/ProtoKeyState;->getId()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v1, v5}, Lee/cyber/smartid/tse/inter/KeyManagerAccess;->getKeyStateById(Ljava/lang/String;)Lee/cyber/smartid/tse/dto/KeyState;

    move-result-object v1

    if-nez v1, :cond_1

    .line 625
    iget-object p2, p0, Lee/cyber/smartid/tse/KeyStateManager;->t:Lee/cyber/smartid/tse/inter/LogAccess;

    const-string p3, "queue - Existing KeyState is missing, aborting!"

    invoke-interface {p2, p3}, Lee/cyber/smartid/tse/inter/LogAccess;->e(Ljava/lang/String;)V

    .line 626
    iget-object p2, p0, Lee/cyber/smartid/tse/KeyStateManager;->r:Lee/cyber/smartid/tse/inter/WallClock;

    iget-object p3, p0, Lee/cyber/smartid/tse/KeyStateManager;->c:Lee/cyber/smartid/tse/inter/ResourceAccess;

    invoke-interface {p3}, Lee/cyber/smartid/tse/inter/ResourceAccess;->getApplicationContext()Landroid/content/Context;

    move-result-object p3

    sget v1, Lee/cyber/smartid/tse/R$string;->err_invalid_transaction_state:I

    invoke-virtual {p3, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p3

    invoke-static {p2, v2, v3, p3}, Lee/cyber/smartid/tse/dto/TSEError;->from(Lee/cyber/smartid/tse/inter/WallClock;JLjava/lang/String;)Lee/cyber/smartid/tse/dto/TSEError;

    move-result-object p2

    invoke-virtual {p2, v4}, Lee/cyber/smartid/tse/dto/TSEError;->asNonRetriable(Z)Lee/cyber/smartid/tse/dto/TSEError;

    move-result-object p2

    invoke-direct {p0, p4, p1, p2}, Lee/cyber/smartid/tse/KeyStateManager;->a(Lee/cyber/smartid/tse/inter/TSEListener;Ljava/lang/String;Lee/cyber/smartid/tse/dto/TSEError;)V

    .line 627
    monitor-exit v0

    return-void

    .line 631
    :cond_1
    invoke-virtual {p2}, Lee/cyber/smartid/tse/dto/ProtoKeyState;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lee/cyber/smartid/tse/KeyStateManager;->isKeyStateInProgress(Ljava/lang/String;)Z

    move-result v2

    .line 633
    invoke-virtual {v1}, Lee/cyber/smartid/tse/dto/KeyState;->isInActiveState()Z

    move-result v3

    if-eqz v2, :cond_2

    .line 637
    iget-object v1, p0, Lee/cyber/smartid/tse/KeyStateManager;->t:Lee/cyber/smartid/tse/inter/LogAccess;

    const-string v2, "queue - Existing one is in progress, need to queue the new state"

    invoke-interface {v1, v2}, Lee/cyber/smartid/tse/inter/LogAccess;->d(Ljava/lang/String;)V

    .line 638
    invoke-direct {p0, p1, p2, p3, p4}, Lee/cyber/smartid/tse/KeyStateManager;->b(Ljava/lang/String;Lee/cyber/smartid/tse/dto/ProtoKeyState;Landroid/os/Bundle;Lee/cyber/smartid/tse/inter/TSEListener;)V

    goto/16 :goto_0

    :cond_2
    if-nez v2, :cond_3

    if-nez v3, :cond_3

    .line 642
    iget-object v1, p0, Lee/cyber/smartid/tse/KeyStateManager;->t:Lee/cyber/smartid/tse/inter/LogAccess;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "queue - Existing state is idle and nothing in progress, starting the new state "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Lee/cyber/smartid/tse/dto/ProtoKeyState;->getHumanReadableNameForType()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lee/cyber/smartid/tse/inter/LogAccess;->d(Ljava/lang/String;)V

    .line 643
    invoke-direct {p0, p1, p2, p3, p4}, Lee/cyber/smartid/tse/KeyStateManager;->a(Ljava/lang/String;Lee/cyber/smartid/tse/dto/ProtoKeyState;Landroid/os/Bundle;Lee/cyber/smartid/tse/inter/TSEListener;)V

    goto :goto_0

    :cond_3
    if-nez v2, :cond_5

    if-eqz v3, :cond_5

    .line 646
    invoke-virtual {v1, p2}, Lee/cyber/smartid/tse/dto/KeyState;->isRetriableByProto(Lee/cyber/smartid/tse/dto/ProtoKeyState;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 650
    iget-object v2, p0, Lee/cyber/smartid/tse/KeyStateManager;->t:Lee/cyber/smartid/tse/inter/LogAccess;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "queue - We already have this exact state in storage, re-starting this "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Lee/cyber/smartid/tse/dto/KeyState;->getHumanReadableNameForType()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " one"

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v2, v1}, Lee/cyber/smartid/tse/inter/LogAccess;->d(Ljava/lang/String;)V

    .line 651
    invoke-direct {p0, p1, p2, p3, p4}, Lee/cyber/smartid/tse/KeyStateManager;->a(Ljava/lang/String;Lee/cyber/smartid/tse/dto/ProtoKeyState;Landroid/os/Bundle;Lee/cyber/smartid/tse/inter/TSEListener;)V

    goto :goto_0

    .line 656
    :cond_4
    invoke-static {v1}, Lee/cyber/smartid/tse/dto/ProtoKeyState;->retryFrom(Lee/cyber/smartid/tse/dto/KeyState;)Lee/cyber/smartid/tse/dto/ProtoKeyState;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {p0, v3, v2, p3, v3}, Lee/cyber/smartid/tse/KeyStateManager;->a(Ljava/lang/String;Lee/cyber/smartid/tse/dto/ProtoKeyState;Landroid/os/Bundle;Lee/cyber/smartid/tse/inter/TSEListener;)V

    .line 657
    invoke-direct {p0, p1, p2, p3, p4}, Lee/cyber/smartid/tse/KeyStateManager;->b(Ljava/lang/String;Lee/cyber/smartid/tse/dto/ProtoKeyState;Landroid/os/Bundle;Lee/cyber/smartid/tse/inter/TSEListener;)V

    .line 658
    iget-object p1, p0, Lee/cyber/smartid/tse/KeyStateManager;->t:Lee/cyber/smartid/tse/inter/LogAccess;

    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    const-string p4, "queue - We have a different previous state ("

    invoke-virtual {p3, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Lee/cyber/smartid/tse/dto/KeyState;->getHumanReadableNameForType()Ljava/lang/String;

    move-result-object p4

    invoke-virtual {p3, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p4, "), starting that, queuing the new one ("

    invoke-virtual {p3, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Lee/cyber/smartid/tse/dto/ProtoKeyState;->getHumanReadableNameForType()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p2, ")"

    invoke-virtual {p3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-interface {p1, p2}, Lee/cyber/smartid/tse/inter/LogAccess;->d(Ljava/lang/String;)V

    .line 661
    :cond_5
    :goto_0
    monitor-exit v0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method public retryLocalPendingState(Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/tse/inter/TSEListener;)V
    .locals 3

    .line 1352
    iget-object v0, p0, Lee/cyber/smartid/tse/KeyStateManager;->t:Lee/cyber/smartid/tse/inter/LogAccess;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "retryLocalPendingState - tag: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, ", listener: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lee/cyber/smartid/tse/inter/LogAccess;->d(Ljava/lang/String;)V

    .line 1353
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lee/cyber/smartid/tse/KeyStateManager$16;

    invoke-direct {v1, p0, p2, p3, p1}, Lee/cyber/smartid/tse/KeyStateManager$16;-><init>(Lee/cyber/smartid/tse/KeyStateManager;Ljava/lang/String;Lee/cyber/smartid/tse/inter/TSEListener;Ljava/lang/String;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 1403
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    return-void
.end method

.method public setListener(Lee/cyber/smartid/tse/inter/StateWorkerJobListener;)V
    .locals 0

    .line 1412
    iput-object p1, p0, Lee/cyber/smartid/tse/KeyStateManager;->s:Lee/cyber/smartid/tse/inter/StateWorkerJobListener;

    return-void
.end method

.method public submitClientSecondPart(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Lee/cyber/smartid/tse/inter/SubmitClientSecondPartListener;)V
    .locals 12

    move-object v9, p0

    move-object v4, p1

    .line 683
    iget-object v0, v9, Lee/cyber/smartid/tse/KeyStateManager;->t:Lee/cyber/smartid/tse/inter/LogAccess;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "submitClientSecondPart - "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lee/cyber/smartid/tse/inter/LogAccess;->d(Ljava/lang/String;)V

    .line 684
    iget-object v0, v9, Lee/cyber/smartid/tse/KeyStateManager;->e:Lee/cyber/smartid/tse/inter/ListenerAccess;

    move-object/from16 v8, p7

    invoke-interface {v0, p1, v8}, Lee/cyber/smartid/tse/inter/ListenerAccess;->setListener(Ljava/lang/String;Lee/cyber/smartid/tse/inter/TSEListener;)V

    .line 685
    new-instance v10, Ljava/lang/Thread;

    new-instance v11, Lee/cyber/smartid/tse/KeyStateManager$3;

    move-object v0, v11

    move-object v1, p0

    move-object/from16 v2, p4

    move-object v3, p2

    move-object v5, p3

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    invoke-direct/range {v0 .. v8}, Lee/cyber/smartid/tse/KeyStateManager$3;-><init>(Lee/cyber/smartid/tse/KeyStateManager;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Lee/cyber/smartid/tse/inter/SubmitClientSecondPartListener;)V

    invoke-direct {v10, v11}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 749
    invoke-virtual {v10}, Ljava/lang/Thread;->start()V

    return-void
.end method
