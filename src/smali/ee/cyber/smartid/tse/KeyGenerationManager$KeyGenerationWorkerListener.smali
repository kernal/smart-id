.class public interface abstract Lee/cyber/smartid/tse/KeyGenerationManager$KeyGenerationWorkerListener;
.super Ljava/lang/Object;
.source "KeyGenerationManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lee/cyber/smartid/tse/KeyGenerationManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "KeyGenerationWorkerListener"
.end annotation


# virtual methods
.method public abstract onGenerateKeysFailed(Ljava/lang/String;Ljava/lang/Exception;)V
.end method

.method public abstract onGenerateKeysProgress(Ljava/lang/String;II)V
.end method

.method public abstract onGenerateKeysSuccess(Ljava/lang/String;Ljava/util/ArrayList;J[Lee/cyber/smartid/cryptolib/dto/TestResult;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList<",
            "[",
            "Ljava/lang/String;",
            ">;J[",
            "Lee/cyber/smartid/cryptolib/dto/TestResult;",
            ")V"
        }
    .end annotation
.end method
