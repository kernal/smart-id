.class final Lee/cyber/smartid/tse/network/VersionInterceptor;
.super Ljava/lang/Object;
.source "VersionInterceptor.java"

# interfaces
.implements Lokhttp3/t;


# instance fields
.field private final a:Lee/cyber/smartid/tse/inter/ResourceAccess;

.field private b:Lee/cyber/smartid/tse/inter/LogAccess;


# direct methods
.method public constructor <init>(Lee/cyber/smartid/tse/inter/ResourceAccess;)V
    .locals 1

    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    invoke-static {p0}, Lee/cyber/smartid/tse/util/Log;->getInstance(Ljava/lang/Object;)Lee/cyber/smartid/tse/util/Log;

    move-result-object v0

    iput-object v0, p0, Lee/cyber/smartid/tse/network/VersionInterceptor;->b:Lee/cyber/smartid/tse/inter/LogAccess;

    .line 35
    iput-object p1, p0, Lee/cyber/smartid/tse/network/VersionInterceptor;->a:Lee/cyber/smartid/tse/inter/ResourceAccess;

    return-void
.end method


# virtual methods
.method public intercept(Lokhttp3/t$a;)Lokhttp3/ab;
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 47
    invoke-interface {p1}, Lokhttp3/t$a;->a()Lokhttp3/z;

    move-result-object v0

    .line 48
    invoke-virtual {v0}, Lokhttp3/z;->e()Lokhttp3/z$a;

    move-result-object v0

    .line 49
    iget-object v1, p0, Lee/cyber/smartid/tse/network/VersionInterceptor;->a:Lee/cyber/smartid/tse/inter/ResourceAccess;

    invoke-interface {v1}, Lee/cyber/smartid/tse/inter/ResourceAccess;->getLibraryVersion()Ljava/lang/String;

    move-result-object v1

    .line 50
    iget-object v2, p0, Lee/cyber/smartid/tse/network/VersionInterceptor;->a:Lee/cyber/smartid/tse/inter/ResourceAccess;

    invoke-interface {v2}, Lee/cyber/smartid/tse/inter/ResourceAccess;->getAppVersion()Ljava/lang/String;

    move-result-object v2

    .line 51
    iget-object v3, p0, Lee/cyber/smartid/tse/network/VersionInterceptor;->a:Lee/cyber/smartid/tse/inter/ResourceAccess;

    invoke-interface {v3}, Lee/cyber/smartid/tse/inter/ResourceAccess;->getPlatform()Ljava/lang/String;

    move-result-object v3

    const-string v4, "X-Smart-ID-Library"

    .line 53
    invoke-virtual {v0, v4, v1}, Lokhttp3/z$a;->a(Ljava/lang/String;Ljava/lang/String;)Lokhttp3/z$a;

    move-result-object v0

    const-string v4, "X-Smart-ID-UI"

    .line 54
    invoke-virtual {v0, v4, v2}, Lokhttp3/z$a;->a(Ljava/lang/String;Ljava/lang/String;)Lokhttp3/z$a;

    move-result-object v0

    const-string v4, "X-Smart-ID-Platform"

    .line 55
    invoke-virtual {v0, v4, v3}, Lokhttp3/z$a;->a(Ljava/lang/String;Ljava/lang/String;)Lokhttp3/z$a;

    move-result-object v0

    .line 56
    invoke-virtual {v0}, Lokhttp3/z$a;->a()Lokhttp3/z;

    move-result-object v0

    .line 57
    iget-object v4, p0, Lee/cyber/smartid/tse/network/VersionInterceptor;->b:Lee/cyber/smartid/tse/inter/LogAccess;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "intercept: Added version info (library: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", app: "

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", platform: "

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ") to "

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Lokhttp3/z;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " request"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v4, v1}, Lee/cyber/smartid/tse/inter/LogAccess;->d(Ljava/lang/String;)V

    .line 58
    invoke-interface {p1, v0}, Lokhttp3/t$a;->a(Lokhttp3/z;)Lokhttp3/ab;

    move-result-object p1

    return-object p1
.end method
