.class final Lee/cyber/smartid/tse/network/RawJSONDeserializer;
.super Ljava/lang/Object;
.source "RawJSONDeserializer.java"

# interfaces
.implements Lcom/google/gson/k;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/gson/k<",
        "Lee/cyber/smartid/tse/dto/RawJson;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public deserialize(Lcom/google/gson/l;Ljava/lang/reflect/Type;Lcom/google/gson/j;)Lee/cyber/smartid/tse/dto/RawJson;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/gson/p;
        }
    .end annotation

    if-eqz p1, :cond_0

    .line 46
    new-instance p2, Lee/cyber/smartid/tse/dto/RawJson;

    invoke-virtual {p1}, Lcom/google/gson/l;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Lee/cyber/smartid/tse/dto/RawJson;-><init>(Ljava/lang/String;)V

    return-object p2

    :cond_0
    const/4 p1, 0x0

    return-object p1
.end method

.method public bridge synthetic deserialize(Lcom/google/gson/l;Ljava/lang/reflect/Type;Lcom/google/gson/j;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/gson/p;
        }
    .end annotation

    .line 23
    invoke-virtual {p0, p1, p2, p3}, Lee/cyber/smartid/tse/network/RawJSONDeserializer;->deserialize(Lcom/google/gson/l;Ljava/lang/reflect/Type;Lcom/google/gson/j;)Lee/cyber/smartid/tse/dto/RawJson;

    move-result-object p1

    return-object p1
.end method
