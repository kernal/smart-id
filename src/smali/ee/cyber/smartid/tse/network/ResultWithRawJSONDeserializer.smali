.class final Lee/cyber/smartid/tse/network/ResultWithRawJSONDeserializer;
.super Ljava/lang/Object;
.source "ResultWithRawJSONDeserializer.java"

# interfaces
.implements Lcom/google/gson/k;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/gson/k<",
        "Lee/cyber/smartid/tse/dto/ResultWithRawJson;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/google/gson/f;


# direct methods
.method public constructor <init>()V
    .locals 3

    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    new-instance v0, Lcom/google/gson/g;

    invoke-direct {v0}, Lcom/google/gson/g;-><init>()V

    .line 36
    const-class v1, Lee/cyber/smartid/tse/dto/RawJson;

    new-instance v2, Lee/cyber/smartid/tse/network/RawJSONDeserializer;

    invoke-direct {v2}, Lee/cyber/smartid/tse/network/RawJSONDeserializer;-><init>()V

    invoke-virtual {v0, v1, v2}, Lcom/google/gson/g;->a(Ljava/lang/reflect/Type;Ljava/lang/Object;)Lcom/google/gson/g;

    .line 37
    invoke-virtual {v0}, Lcom/google/gson/g;->a()Lcom/google/gson/f;

    move-result-object v0

    iput-object v0, p0, Lee/cyber/smartid/tse/network/ResultWithRawJSONDeserializer;->a:Lcom/google/gson/f;

    return-void
.end method


# virtual methods
.method public deserialize(Lcom/google/gson/l;Ljava/lang/reflect/Type;Lcom/google/gson/j;)Lee/cyber/smartid/tse/dto/ResultWithRawJson;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/gson/p;
        }
    .end annotation

    if-eqz p1, :cond_1

    if-eqz p2, :cond_1

    .line 54
    check-cast p2, Ljava/lang/reflect/ParameterizedType;

    invoke-interface {p2}, Ljava/lang/reflect/ParameterizedType;->getActualTypeArguments()[Ljava/lang/reflect/Type;

    move-result-object p2

    if-eqz p2, :cond_0

    .line 55
    array-length p3, p2

    const/4 v0, 0x1

    if-lt p3, v0, :cond_0

    .line 59
    new-instance p3, Lee/cyber/smartid/tse/dto/ResultWithRawJson;

    invoke-virtual {p1}, Lcom/google/gson/l;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lee/cyber/smartid/tse/network/ResultWithRawJSONDeserializer;->a:Lcom/google/gson/f;

    invoke-virtual {p1}, Lcom/google/gson/l;->toString()Ljava/lang/String;

    move-result-object p1

    const/4 v2, 0x0

    aget-object p2, p2, v2

    invoke-virtual {v1, p1, p2}, Lcom/google/gson/f;->a(Ljava/lang/String;Ljava/lang/reflect/Type;)Ljava/lang/Object;

    move-result-object p1

    invoke-direct {p3, v0, p1}, Lee/cyber/smartid/tse/dto/ResultWithRawJson;-><init>(Ljava/lang/String;Ljava/lang/Object;)V

    return-object p3

    .line 56
    :cond_0
    new-instance p1, Lcom/google/gson/p;

    const-string p2, "Unable to get the actual type arguments during deserialization"

    invoke-direct {p1, p2}, Lcom/google/gson/p;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_1
    const/4 p1, 0x0

    return-object p1
.end method

.method public bridge synthetic deserialize(Lcom/google/gson/l;Ljava/lang/reflect/Type;Lcom/google/gson/j;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/gson/p;
        }
    .end annotation

    .line 27
    invoke-virtual {p0, p1, p2, p3}, Lee/cyber/smartid/tse/network/ResultWithRawJSONDeserializer;->deserialize(Lcom/google/gson/l;Ljava/lang/reflect/Type;Lcom/google/gson/j;)Lee/cyber/smartid/tse/dto/ResultWithRawJson;

    move-result-object p1

    return-object p1
.end method
