.class public Lee/cyber/smartid/tse/network/TSEAPI;
.super Ljava/lang/Object;
.source "TSEAPI.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lee/cyber/smartid/tse/network/TSEAPI$TSEAPIService;,
        Lee/cyber/smartid/tse/network/TSEAPI$XSplitKeyTrigger;
    }
.end annotation


# static fields
.field public static final DEFAULT_X_SPLIT_KEY_TRIGGER:Ljava/lang/String; = "external"

.field public static final EXTRA_X_SPLIT_KEY_TRIGGER:Ljava/lang/String; = "ee.cyber.smartid.tse.EXTRA_X_SPLIT_KEY_TRIGGER"

.field public static final HTTP_CODE_CLIENT_TOO_OLD:I = 0x1e0

.field public static final HTTP_CODE_OK:I = 0xc8

.field public static final HTTP_CODE_SYSTEM_UNDER_MAINTENANCE:I = 0x244

.field public static final HTTP_CODE_UNAUTHORIZED:I = 0x191

.field public static final METHOD_GET_FRESHNESS_TOKEN:Ljava/lang/String; = "getFreshnessToken"

.field public static final METHOD_REFRESH_CLONE_DETECTION:Ljava/lang/String; = "refreshCloneDetection"

.field public static final METHOD_SUBMIT_CLIENT_SECOND_PART:Ljava/lang/String; = "submitClient2ndPart"

.field public static final METHOD_SUBMIT_SIGNATURE:Ljava/lang/String; = "submitSignature"

.field public static final PARAMETER_ONE_TIME_PASSWORD:Ljava/lang/String; = "oneTimePassword"

.field public static final TIMEOUT_CONNECT:J = 0x2710L

.field public static final TIMEOUT_READ:J = 0x3a98L

.field public static final TIMEOUT_WRITE:J = 0x3a98L

.field public static final URL_PATH_PROTECTED:Ljava/lang/String; = "v2/protected"

.field public static final URL_PATH_PUBLIC:Ljava/lang/String; = "v2/public"

.field public static final URL_PROTOCOL_HTTPS:Ljava/lang/String; = "https://"

.field public static final X_SPLIT_KEY_TRIGGER_EXTERNAL:Ljava/lang/String; = "external"

.field public static final X_SPLIT_KEY_TRIGGER_INTERNAL_ALARM:Ljava/lang/String; = "internal-alarm"

.field public static final X_SPLIT_KEY_TRIGGER_INTERNAL_BLOCKING_RETRY:Ljava/lang/String; = "internal-blocking-retry"

.field public static final X_SPLIT_KEY_TRIGGER_INTERNAL_SETUP:Ljava/lang/String; = "internal-setup"

.field private static final a:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lokhttp3/h;",
            ">;"
        }
    .end annotation
.end field

.field private static volatile b:Lee/cyber/smartid/tse/network/TSEAPI;


# instance fields
.field private final c:Lee/cyber/smartid/tse/network/TSEAPI$TSEAPIService;

.field private final d:Lc/s;

.field private e:Lee/cyber/smartid/tse/inter/LogAccess;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 109
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lee/cyber/smartid/tse/network/TSEAPI;->a:Ljava/util/ArrayList;

    .line 113
    sget-object v0, Lee/cyber/smartid/tse/network/TSEAPI;->a:Ljava/util/ArrayList;

    sget-object v1, Lokhttp3/h;->bc:Lokhttp3/h;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 114
    sget-object v0, Lee/cyber/smartid/tse/network/TSEAPI;->a:Ljava/util/ArrayList;

    sget-object v1, Lokhttp3/h;->aY:Lokhttp3/h;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 115
    sget-object v0, Lee/cyber/smartid/tse/network/TSEAPI;->a:Ljava/util/ArrayList;

    sget-object v1, Lokhttp3/h;->aU:Lokhttp3/h;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 116
    sget-object v0, Lee/cyber/smartid/tse/network/TSEAPI;->a:Ljava/util/ArrayList;

    sget-object v1, Lokhttp3/h;->aQ:Lokhttp3/h;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 117
    sget-object v0, Lee/cyber/smartid/tse/network/TSEAPI;->a:Ljava/util/ArrayList;

    sget-object v1, Lokhttp3/h;->bb:Lokhttp3/h;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 118
    sget-object v0, Lee/cyber/smartid/tse/network/TSEAPI;->a:Ljava/util/ArrayList;

    sget-object v1, Lokhttp3/h;->aX:Lokhttp3/h;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 119
    sget-object v0, Lee/cyber/smartid/tse/network/TSEAPI;->a:Ljava/util/ArrayList;

    sget-object v1, Lokhttp3/h;->aT:Lokhttp3/h;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 120
    sget-object v0, Lee/cyber/smartid/tse/network/TSEAPI;->a:Ljava/util/ArrayList;

    sget-object v1, Lokhttp3/h;->aP:Lokhttp3/h;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Ljava/lang/String;ZLjava/util/ArrayList;Lee/cyber/smartid/tse/inter/ResourceAccess;Lee/cyber/smartid/tse/inter/WallClock;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Z",
            "Ljava/util/ArrayList<",
            "Landroid/util/Pair<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;",
            "Lee/cyber/smartid/tse/inter/ResourceAccess;",
            "Lee/cyber/smartid/tse/inter/WallClock;",
            ")V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 247
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 193
    invoke-static {p0}, Lee/cyber/smartid/tse/util/Log;->getInstance(Ljava/lang/Object;)Lee/cyber/smartid/tse/util/Log;

    move-result-object v0

    iput-object v0, p0, Lee/cyber/smartid/tse/network/TSEAPI;->e:Lee/cyber/smartid/tse/inter/LogAccess;

    .line 249
    new-instance v0, Lee/cyber/smartid/tse/network/HammerTimeInterceptor;

    invoke-direct {v0, p5, p6}, Lee/cyber/smartid/tse/network/HammerTimeInterceptor;-><init>(Lee/cyber/smartid/tse/inter/ResourceAccess;Lee/cyber/smartid/tse/inter/WallClock;)V

    .line 251
    new-instance p6, Lee/cyber/smartid/tse/network/AuthenticationInterceptor;

    invoke-direct {p6, p5}, Lee/cyber/smartid/tse/network/AuthenticationInterceptor;-><init>(Lee/cyber/smartid/tse/inter/ResourceAccess;)V

    .line 253
    new-instance v1, Lee/cyber/smartid/tse/network/VersionInterceptor;

    invoke-direct {v1, p5}, Lee/cyber/smartid/tse/network/VersionInterceptor;-><init>(Lee/cyber/smartid/tse/inter/ResourceAccess;)V

    .line 255
    new-instance p5, Lokhttp3/a/a;

    new-instance v2, Lee/cyber/smartid/tse/network/TSEAPI$1;

    invoke-direct {v2, p0}, Lee/cyber/smartid/tse/network/TSEAPI$1;-><init>(Lee/cyber/smartid/tse/network/TSEAPI;)V

    invoke-direct {p5, v2}, Lokhttp3/a/a;-><init>(Lokhttp3/a/a$b;)V

    .line 262
    sget-object v2, Lokhttp3/a/a$a;->d:Lokhttp3/a/a$a;

    invoke-virtual {p5, v2}, Lokhttp3/a/a;->a(Lokhttp3/a/a$a;)Lokhttp3/a/a;

    .line 266
    invoke-static {p1, p2, p3, p4}, Lee/cyber/smartid/tse/network/TSEAPI;->createRequestClient(Landroid/content/Context;Ljava/lang/String;ZLjava/util/ArrayList;)Lokhttp3/w$a;

    move-result-object p1

    .line 267
    sget-object p3, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x2710

    invoke-virtual {p1, v2, v3, p3}, Lokhttp3/w$a;->a(JLjava/util/concurrent/TimeUnit;)Lokhttp3/w$a;

    .line 268
    invoke-virtual {p1, v0}, Lokhttp3/w$a;->a(Lokhttp3/t;)Lokhttp3/w$a;

    .line 269
    invoke-virtual {p1, p6}, Lokhttp3/w$a;->a(Lokhttp3/t;)Lokhttp3/w$a;

    .line 270
    invoke-virtual {p1, v1}, Lokhttp3/w$a;->a(Lokhttp3/t;)Lokhttp3/w$a;

    .line 271
    invoke-virtual {p1, p5}, Lokhttp3/w$a;->a(Lokhttp3/t;)Lokhttp3/w$a;

    .line 274
    invoke-virtual {p1}, Lokhttp3/w$a;->a()Lokhttp3/w;

    move-result-object p1

    .line 276
    invoke-static {}, Lee/cyber/smartid/tse/network/TSEAPI;->createGSONBuilderForAPIResponses()Lcom/google/gson/g;

    move-result-object p3

    .line 279
    new-instance p4, Lc/s$a;

    invoke-direct {p4}, Lc/s$a;-><init>()V

    .line 280
    invoke-virtual {p4, p2}, Lc/s$a;->a(Ljava/lang/String;)Lc/s$a;

    move-result-object p2

    .line 281
    invoke-virtual {p2, p1}, Lc/s$a;->a(Lokhttp3/w;)Lc/s$a;

    move-result-object p1

    .line 282
    invoke-virtual {p3}, Lcom/google/gson/g;->a()Lcom/google/gson/f;

    move-result-object p2

    invoke-static {p2}, Lc/a/a/a;->a(Lcom/google/gson/f;)Lc/a/a/a;

    move-result-object p2

    invoke-virtual {p1, p2}, Lc/s$a;->a(Lc/f$a;)Lc/s$a;

    move-result-object p1

    .line 283
    invoke-virtual {p1}, Lc/s$a;->a()Lc/s;

    move-result-object p1

    iput-object p1, p0, Lee/cyber/smartid/tse/network/TSEAPI;->d:Lc/s;

    .line 284
    iget-object p1, p0, Lee/cyber/smartid/tse/network/TSEAPI;->d:Lc/s;

    const-class p2, Lee/cyber/smartid/tse/network/TSEAPI$TSEAPIService;

    invoke-virtual {p1, p2}, Lc/s;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lee/cyber/smartid/tse/network/TSEAPI$TSEAPIService;

    iput-object p1, p0, Lee/cyber/smartid/tse/network/TSEAPI;->c:Lee/cyber/smartid/tse/network/TSEAPI$TSEAPIService;

    return-void
.end method

.method static synthetic a(Lee/cyber/smartid/tse/network/TSEAPI;)Lee/cyber/smartid/tse/inter/LogAccess;
    .locals 0

    .line 55
    iget-object p0, p0, Lee/cyber/smartid/tse/network/TSEAPI;->e:Lee/cyber/smartid/tse/inter/LogAccess;

    return-object p0
.end method

.method private static a(Ljava/util/ArrayList;)Lokhttp3/g;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Landroid/util/Pair<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;)",
            "Lokhttp3/g;"
        }
    .end annotation

    const/4 v0, 0x0

    if-eqz p0, :cond_1

    .line 384
    :try_start_0
    invoke-virtual {p0}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_1

    .line 385
    new-instance v1, Lokhttp3/g$a;

    invoke-direct {v1}, Lokhttp3/g$a;-><init>()V

    .line 386
    invoke-virtual {p0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/util/Pair;

    .line 387
    iget-object v3, v2, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v3, Ljava/lang/String;

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    iget-object v6, v2, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v6, Ljava/lang/String;

    aput-object v6, v4, v5

    invoke-virtual {v1, v3, v4}, Lokhttp3/g$a;->a(Ljava/lang/String;[Ljava/lang/String;)Lokhttp3/g$a;

    .line 388
    const-class v3, Lee/cyber/smartid/tse/network/TSEAPI;

    invoke-static {v3}, Lee/cyber/smartid/tse/util/Log;->getInstance(Ljava/lang/Class;)Lee/cyber/smartid/tse/util/Log;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "enforcePinsIfAny - adding \""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v5, v2, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v5, Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, "\" - \""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, v2, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "\""

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Lee/cyber/smartid/tse/util/Log;->d(Ljava/lang/String;)V

    goto :goto_0

    .line 390
    :cond_0
    invoke-virtual {v1}, Lokhttp3/g$a;->a()Lokhttp3/g;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception p0

    .line 393
    const-class v1, Lee/cyber/smartid/tse/network/TSEAPI;

    invoke-static {v1}, Lee/cyber/smartid/tse/util/Log;->getInstance(Ljava/lang/Class;)Lee/cyber/smartid/tse/util/Log;

    move-result-object v1

    const-string v2, "enforcePinsIfAny"

    invoke-virtual {v1, v2, p0}, Lee/cyber/smartid/tse/util/Log;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_1
    :goto_1
    return-object v0
.end method

.method private static a(Landroid/content/Context;)V
    .locals 2

    .line 373
    :try_start_0
    invoke-static {p0}, Lcom/google/android/gms/f/a;->a(Landroid/content/Context;)V

    .line 374
    const-class p0, Lee/cyber/smartid/tse/network/TSEAPI;

    invoke-static {p0}, Lee/cyber/smartid/tse/util/Log;->getInstance(Ljava/lang/Class;)Lee/cyber/smartid/tse/util/Log;

    move-result-object p0

    const-string v0, "patchSSLProvider patched"

    invoke-virtual {p0, v0}, Lee/cyber/smartid/tse/util/Log;->d(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p0

    .line 376
    const-class v0, Lee/cyber/smartid/tse/network/TSEAPI;

    invoke-static {v0}, Lee/cyber/smartid/tse/util/Log;->getInstance(Ljava/lang/Class;)Lee/cyber/smartid/tse/util/Log;

    move-result-object v0

    const-string v1, "patchSSLProvider"

    invoke-virtual {v0, v1, p0}, Lee/cyber/smartid/tse/util/Log;->d(Ljava/lang/String;Ljava/lang/Throwable;)V

    :goto_0
    return-void
.end method

.method public static createGSONBuilderForAPIResponses()Lcom/google/gson/g;
    .locals 3

    .line 318
    new-instance v0, Lcom/google/gson/g;

    invoke-direct {v0}, Lcom/google/gson/g;-><init>()V

    .line 320
    const-class v1, Lee/cyber/smartid/tse/dto/ResultWithRawJson;

    new-instance v2, Lee/cyber/smartid/tse/network/ResultWithRawJSONDeserializer;

    invoke-direct {v2}, Lee/cyber/smartid/tse/network/ResultWithRawJSONDeserializer;-><init>()V

    invoke-virtual {v0, v1, v2}, Lcom/google/gson/g;->a(Ljava/lang/reflect/Type;Ljava/lang/Object;)Lcom/google/gson/g;

    .line 322
    const-class v1, Lee/cyber/smartid/tse/dto/RawJson;

    new-instance v2, Lee/cyber/smartid/tse/network/RawJSONDeserializer;

    invoke-direct {v2}, Lee/cyber/smartid/tse/network/RawJSONDeserializer;-><init>()V

    invoke-virtual {v0, v1, v2}, Lcom/google/gson/g;->a(Ljava/lang/reflect/Type;Ljava/lang/Object;)Lcom/google/gson/g;

    return-object v0
.end method

.method public static createNewInstance(Landroid/content/Context;Ljava/lang/String;ZLjava/util/ArrayList;Lee/cyber/smartid/tse/inter/ResourceAccess;Lee/cyber/smartid/tse/inter/WallClock;)Lee/cyber/smartid/tse/network/TSEAPI;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Z",
            "Ljava/util/ArrayList<",
            "Landroid/util/Pair<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;",
            "Lee/cyber/smartid/tse/inter/ResourceAccess;",
            "Lee/cyber/smartid/tse/inter/WallClock;",
            ")",
            "Lee/cyber/smartid/tse/network/TSEAPI;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 241
    const-class v0, Lee/cyber/smartid/tse/network/TSEAPI;

    monitor-enter v0

    .line 242
    :try_start_0
    new-instance v8, Lee/cyber/smartid/tse/network/TSEAPI;

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    move-object v1, v8

    move-object v3, p1

    move v4, p2

    move-object v5, p3

    move-object v6, p4

    move-object v7, p5

    invoke-direct/range {v1 .. v7}, Lee/cyber/smartid/tse/network/TSEAPI;-><init>(Landroid/content/Context;Ljava/lang/String;ZLjava/util/ArrayList;Lee/cyber/smartid/tse/inter/ResourceAccess;Lee/cyber/smartid/tse/inter/WallClock;)V

    sput-object v8, Lee/cyber/smartid/tse/network/TSEAPI;->b:Lee/cyber/smartid/tse/network/TSEAPI;

    .line 243
    sget-object p0, Lee/cyber/smartid/tse/network/TSEAPI;->b:Lee/cyber/smartid/tse/network/TSEAPI;

    monitor-exit v0

    return-object p0

    :catchall_0
    move-exception p0

    .line 244
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p0
.end method

.method public static createRequestClient(Landroid/content/Context;Ljava/lang/String;ZLjava/util/ArrayList;)Lokhttp3/w$a;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Z",
            "Ljava/util/ArrayList<",
            "Landroid/util/Pair<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;)",
            "Lokhttp3/w$a;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 344
    invoke-static {p0, p1, p2}, Lee/cyber/smartid/tse/util/Util;->validateBaseUrlParams(Landroid/content/Context;Ljava/lang/String;Z)V

    .line 346
    invoke-static {p0}, Lee/cyber/smartid/tse/network/TSEAPI;->a(Landroid/content/Context;)V

    .line 348
    new-instance p0, Lokhttp3/w$a;

    invoke-direct {p0}, Lokhttp3/w$a;-><init>()V

    sget-object p2, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v0, 0x3a98

    .line 349
    invoke-virtual {p0, v0, v1, p2}, Lokhttp3/w$a;->b(JLjava/util/concurrent/TimeUnit;)Lokhttp3/w$a;

    move-result-object p0

    sget-object p2, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    .line 350
    invoke-virtual {p0, v0, v1, p2}, Lokhttp3/w$a;->c(JLjava/util/concurrent/TimeUnit;)Lokhttp3/w$a;

    move-result-object p0

    .line 353
    invoke-static {p1}, Lee/cyber/smartid/tse/util/Util;->isHTTPSUrl(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 355
    new-instance p1, Lokhttp3/k$a;

    sget-object p2, Lokhttp3/k;->b:Lokhttp3/k;

    invoke-direct {p1, p2}, Lokhttp3/k$a;-><init>(Lokhttp3/k;)V

    const/4 p2, 0x1

    new-array v0, p2, [Lokhttp3/ae;

    const/4 v1, 0x0

    sget-object v2, Lokhttp3/ae;->b:Lokhttp3/ae;

    aput-object v2, v0, v1

    .line 356
    invoke-virtual {p1, v0}, Lokhttp3/k$a;->a([Lokhttp3/ae;)Lokhttp3/k$a;

    move-result-object p1

    .line 357
    invoke-virtual {p1, p2}, Lokhttp3/k$a;->a(Z)Lokhttp3/k$a;

    move-result-object p1

    sget-object p2, Lee/cyber/smartid/tse/network/TSEAPI;->a:Ljava/util/ArrayList;

    .line 358
    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Lokhttp3/h;

    invoke-virtual {p2, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object p2

    check-cast p2, [Lokhttp3/h;

    invoke-virtual {p1, p2}, Lokhttp3/k$a;->a([Lokhttp3/h;)Lokhttp3/k$a;

    move-result-object p1

    .line 359
    invoke-virtual {p1}, Lokhttp3/k$a;->a()Lokhttp3/k;

    move-result-object p1

    .line 361
    invoke-static {p1}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    invoke-virtual {p0, p1}, Lokhttp3/w$a;->a(Ljava/util/List;)Lokhttp3/w$a;

    .line 363
    invoke-static {p3}, Lee/cyber/smartid/tse/network/TSEAPI;->a(Ljava/util/ArrayList;)Lokhttp3/g;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 365
    invoke-virtual {p0, p1}, Lokhttp3/w$a;->a(Lokhttp3/g;)Lokhttp3/w$a;

    :cond_0
    return-object p0
.end method

.method public static getInstance(Landroid/content/Context;Ljava/lang/String;ZLee/cyber/smartid/tse/inter/ResourceAccess;Ljava/util/ArrayList;Lee/cyber/smartid/tse/inter/WallClock;)Lee/cyber/smartid/tse/network/TSEAPI;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Z",
            "Lee/cyber/smartid/tse/inter/ResourceAccess;",
            "Ljava/util/ArrayList<",
            "Landroid/util/Pair<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;",
            "Lee/cyber/smartid/tse/inter/WallClock;",
            ")",
            "Lee/cyber/smartid/tse/network/TSEAPI;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 213
    sget-object v0, Lee/cyber/smartid/tse/network/TSEAPI;->b:Lee/cyber/smartid/tse/network/TSEAPI;

    if-nez v0, :cond_1

    .line 214
    const-class v0, Lee/cyber/smartid/tse/network/TSEAPI;

    monitor-enter v0

    .line 215
    :try_start_0
    sget-object v1, Lee/cyber/smartid/tse/network/TSEAPI;->b:Lee/cyber/smartid/tse/network/TSEAPI;

    if-nez v1, :cond_0

    move-object v1, p0

    move-object v2, p1

    move v3, p2

    move-object v4, p4

    move-object v5, p3

    move-object v6, p5

    .line 216
    invoke-static/range {v1 .. v6}, Lee/cyber/smartid/tse/network/TSEAPI;->createNewInstance(Landroid/content/Context;Ljava/lang/String;ZLjava/util/ArrayList;Lee/cyber/smartid/tse/inter/ResourceAccess;Lee/cyber/smartid/tse/inter/WallClock;)Lee/cyber/smartid/tse/network/TSEAPI;

    .line 218
    :cond_0
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception p0

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p0

    .line 220
    :cond_1
    :goto_0
    sget-object p0, Lee/cyber/smartid/tse/network/TSEAPI;->b:Lee/cyber/smartid/tse/network/TSEAPI;

    return-object p0
.end method


# virtual methods
.method public createAPI(Ljava/lang/Class;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class<",
            "TT;>;)TT;"
        }
    .end annotation

    .line 302
    iget-object v0, p0, Lee/cyber/smartid/tse/network/TSEAPI;->d:Lc/s;

    if-nez v0, :cond_0

    const/4 p1, 0x0

    return-object p1

    :cond_0
    if-eqz p1, :cond_1

    .line 308
    invoke-virtual {v0, p1}, Lc/s;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    return-object p1

    .line 306
    :cond_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "Interface can\'t be null"

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public getService()Lee/cyber/smartid/tse/network/TSEAPI$TSEAPIService;
    .locals 1

    .line 405
    iget-object v0, p0, Lee/cyber/smartid/tse/network/TSEAPI;->c:Lee/cyber/smartid/tse/network/TSEAPI$TSEAPIService;

    return-object v0
.end method
