.class public abstract Lee/cyber/smartid/tse/network/RPCCallback;
.super Ljava/lang/Object;
.source "RPCCallback.java"

# interfaces
.implements Lc/d;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lc/d<",
        "TT;>;"
    }
.end annotation


# instance fields
.field private a:Lee/cyber/smartid/tse/inter/WallClock;

.field private b:Lee/cyber/smartid/tse/inter/ListenerAccess;

.field private c:Lee/cyber/smartid/tse/inter/LogAccess;

.field private d:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCRequest;Lee/cyber/smartid/tse/SmartIdTSE;)V
    .locals 1

    const/4 v0, 0x0

    .line 66
    invoke-direct {p0, p1, v0, v0}, Lee/cyber/smartid/tse/network/RPCCallback;-><init>(Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCRequest;Lee/cyber/smartid/tse/inter/WallClock;Lee/cyber/smartid/tse/inter/ListenerAccess;)V

    .line 67
    invoke-virtual {p2, p0}, Lee/cyber/smartid/tse/SmartIdTSE;->initializeAPICallback(Lee/cyber/smartid/tse/network/RPCCallback;)V

    return-void
.end method

.method public constructor <init>(Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCRequest;Lee/cyber/smartid/tse/inter/WallClock;Lee/cyber/smartid/tse/inter/ListenerAccess;)V
    .locals 1

    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    invoke-static {p0}, Lee/cyber/smartid/tse/util/Log;->getInstance(Ljava/lang/Object;)Lee/cyber/smartid/tse/util/Log;

    move-result-object v0

    iput-object v0, p0, Lee/cyber/smartid/tse/network/RPCCallback;->c:Lee/cyber/smartid/tse/inter/LogAccess;

    if-eqz p1, :cond_0

    .line 54
    invoke-virtual {p1}, Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCRequest;->getId()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lee/cyber/smartid/tse/network/RPCCallback;->d:Ljava/lang/String;

    .line 56
    :cond_0
    invoke-virtual {p0, p2, p3}, Lee/cyber/smartid/tse/network/RPCCallback;->initCallback(Lee/cyber/smartid/tse/inter/WallClock;Lee/cyber/smartid/tse/inter/ListenerAccess;)V

    return-void
.end method

.method private a()V
    .locals 1

    .line 161
    iget-object v0, p0, Lee/cyber/smartid/tse/network/RPCCallback;->b:Lee/cyber/smartid/tse/inter/ListenerAccess;

    if-nez v0, :cond_0

    return-void

    .line 164
    :cond_0
    invoke-interface {v0}, Lee/cyber/smartid/tse/inter/ListenerAccess;->setHammerTimeEventTimestamp()V

    return-void
.end method

.method private a(Ljava/lang/Exception;)V
    .locals 3

    .line 173
    iget-object v0, p0, Lee/cyber/smartid/tse/network/RPCCallback;->b:Lee/cyber/smartid/tse/inter/ListenerAccess;

    if-nez v0, :cond_0

    return-void

    .line 176
    :cond_0
    invoke-interface {v0}, Lee/cyber/smartid/tse/inter/ListenerAccess;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lee/cyber/smartid/tse/network/RPCCallback;->a:Lee/cyber/smartid/tse/inter/WallClock;

    invoke-static {v1, v2, p1}, Lee/cyber/smartid/tse/dto/TSEError;->from(Landroid/content/Context;Lee/cyber/smartid/tse/inter/WallClock;Ljava/lang/Throwable;)Lee/cyber/smartid/tse/dto/TSEError;

    move-result-object p1

    invoke-interface {v0, p1}, Lee/cyber/smartid/tse/inter/ListenerAccess;->notifySystemEventsListeners(Lee/cyber/smartid/tse/dto/TSEError;)V

    return-void
.end method

.method public static getBody(Lc/r;)Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCResponse;
    .locals 1

    .line 188
    invoke-virtual {p0}, Lc/r;->e()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 189
    invoke-virtual {p0}, Lc/r;->e()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCResponse;

    return-object p0

    .line 191
    :cond_0
    invoke-virtual {p0}, Lc/r;->f()Lokhttp3/ac;

    move-result-object p0

    invoke-static {p0}, Lee/cyber/smartid/tse/util/Util;->getErrorResponseIfAny(Lokhttp3/ac;)Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCResponse;

    move-result-object p0

    return-object p0
.end method

.method public static isForId(Ljava/lang/String;ILee/cyber/smartid/tse/dto/jsonrpc/base/RPCResponse;)Z
    .locals 2

    .line 217
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    return v1

    :cond_0
    const/16 v0, 0xc8

    if-eq p1, v0, :cond_1

    return v1

    :cond_1
    if-nez p2, :cond_2

    const/4 p0, 0x0

    return p0

    .line 227
    :cond_2
    invoke-virtual {p2, p0}, Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCResponse;->isResponseFor(Ljava/lang/String;)Z

    move-result p0

    return p0
.end method


# virtual methods
.method public initCallback(Lee/cyber/smartid/tse/inter/WallClock;Lee/cyber/smartid/tse/inter/ListenerAccess;)V
    .locals 0

    .line 77
    iput-object p1, p0, Lee/cyber/smartid/tse/network/RPCCallback;->a:Lee/cyber/smartid/tse/inter/WallClock;

    .line 78
    iput-object p2, p0, Lee/cyber/smartid/tse/network/RPCCallback;->b:Lee/cyber/smartid/tse/inter/ListenerAccess;

    return-void
.end method

.method public abstract onFailure(Lc/b;Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCError;Ljava/lang/Throwable;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lc/b<",
            "TT;>;",
            "Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCError;",
            "Ljava/lang/Throwable;",
            ")V"
        }
    .end annotation
.end method

.method public onFailure(Lc/b;Ljava/lang/Throwable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lc/b<",
            "TT;>;",
            "Ljava/lang/Throwable;",
            ")V"
        }
    .end annotation

    const/4 v0, 0x0

    .line 202
    invoke-virtual {p0, p1, v0, p2}, Lee/cyber/smartid/tse/network/RPCCallback;->onFailure(Lc/b;Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCError;Ljava/lang/Throwable;)V

    .line 203
    invoke-static {p2}, Lee/cyber/smartid/tse/dto/TSEError;->isServerSideError(Ljava/lang/Throwable;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 204
    invoke-direct {p0}, Lee/cyber/smartid/tse/network/RPCCallback;->a()V

    :cond_0
    return-void
.end method

.method public onResponse(Lc/b;Lc/r;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lc/b<",
            "TT;>;",
            "Lc/r<",
            "TT;>;)V"
        }
    .end annotation

    .line 89
    invoke-virtual {p2}, Lc/r;->e()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p2}, Lc/r;->e()Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCResponse;

    if-eqz v0, :cond_0

    goto :goto_0

    .line 91
    :cond_0
    new-instance p1, Ljava/lang/RuntimeException;

    const-string p2, "RPCCallback can only be used for objects extending RPCResponse class!"

    invoke-direct {p1, p2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 95
    :cond_1
    :goto_0
    invoke-virtual {p2}, Lc/r;->a()Lokhttp3/ab;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {p2}, Lc/r;->a()Lokhttp3/ab;

    move-result-object v0

    invoke-virtual {v0}, Lokhttp3/ab;->e()Lokhttp3/r;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 96
    iget-object v0, p0, Lee/cyber/smartid/tse/network/RPCCallback;->c:Lee/cyber/smartid/tse/inter/LogAccess;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onResponse handshake: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Lc/r;->a()Lokhttp3/ab;

    move-result-object v2

    invoke-virtual {v2}, Lokhttp3/ab;->e()Lokhttp3/r;

    move-result-object v2

    invoke-virtual {v2}, Lokhttp3/r;->a()Lokhttp3/h;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lee/cyber/smartid/tse/inter/LogAccess;->d(Ljava/lang/String;)V

    .line 100
    :cond_2
    invoke-virtual {p2}, Lc/r;->b()I

    move-result v0

    const/16 v1, 0x191

    const/4 v2, 0x0

    if-ne v0, v1, :cond_3

    .line 102
    new-instance p2, Lee/cyber/smartid/tse/dto/AuthorizationRequiredException;

    invoke-direct {p2}, Lee/cyber/smartid/tse/dto/AuthorizationRequiredException;-><init>()V

    invoke-virtual {p0, p1, v2, p2}, Lee/cyber/smartid/tse/network/RPCCallback;->onFailure(Lc/b;Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCError;Ljava/lang/Throwable;)V

    .line 103
    new-instance p1, Lee/cyber/smartid/tse/dto/AuthorizationRequiredException;

    invoke-direct {p1}, Lee/cyber/smartid/tse/dto/AuthorizationRequiredException;-><init>()V

    invoke-direct {p0, p1}, Lee/cyber/smartid/tse/network/RPCCallback;->a(Ljava/lang/Exception;)V

    .line 104
    invoke-direct {p0}, Lee/cyber/smartid/tse/network/RPCCallback;->a()V

    return-void

    .line 106
    :cond_3
    invoke-virtual {p2}, Lc/r;->b()I

    move-result v0

    const/16 v1, 0x1e0

    if-ne v0, v1, :cond_4

    .line 108
    new-instance p2, Lee/cyber/smartid/tse/dto/ClientTooOldException;

    invoke-direct {p2}, Lee/cyber/smartid/tse/dto/ClientTooOldException;-><init>()V

    invoke-virtual {p0, p1, v2, p2}, Lee/cyber/smartid/tse/network/RPCCallback;->onFailure(Lc/b;Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCError;Ljava/lang/Throwable;)V

    .line 109
    new-instance p1, Lee/cyber/smartid/tse/dto/ClientTooOldException;

    invoke-direct {p1}, Lee/cyber/smartid/tse/dto/ClientTooOldException;-><init>()V

    invoke-direct {p0, p1}, Lee/cyber/smartid/tse/network/RPCCallback;->a(Ljava/lang/Exception;)V

    .line 110
    invoke-direct {p0}, Lee/cyber/smartid/tse/network/RPCCallback;->a()V

    return-void

    .line 112
    :cond_4
    invoke-virtual {p2}, Lc/r;->b()I

    move-result v0

    const/16 v1, 0x244

    if-ne v0, v1, :cond_5

    .line 114
    new-instance p2, Lee/cyber/smartid/tse/dto/SystemUnderMaintenanceException;

    invoke-direct {p2}, Lee/cyber/smartid/tse/dto/SystemUnderMaintenanceException;-><init>()V

    invoke-virtual {p0, p1, v2, p2}, Lee/cyber/smartid/tse/network/RPCCallback;->onFailure(Lc/b;Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCError;Ljava/lang/Throwable;)V

    .line 115
    new-instance p1, Lee/cyber/smartid/tse/dto/SystemUnderMaintenanceException;

    invoke-direct {p1}, Lee/cyber/smartid/tse/dto/SystemUnderMaintenanceException;-><init>()V

    invoke-direct {p0, p1}, Lee/cyber/smartid/tse/network/RPCCallback;->a(Ljava/lang/Exception;)V

    .line 116
    invoke-direct {p0}, Lee/cyber/smartid/tse/network/RPCCallback;->a()V

    return-void

    .line 121
    :cond_5
    invoke-static {p2}, Lee/cyber/smartid/tse/network/RPCCallback;->getBody(Lc/r;)Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCResponse;

    move-result-object v0

    .line 125
    iget-object v1, p0, Lee/cyber/smartid/tse/network/RPCCallback;->d:Ljava/lang/String;

    invoke-virtual {p2}, Lc/r;->b()I

    move-result v3

    invoke-static {v1, v3, v0}, Lee/cyber/smartid/tse/network/RPCCallback;->isForId(Ljava/lang/String;ILee/cyber/smartid/tse/dto/jsonrpc/base/RPCResponse;)Z

    move-result v1

    if-nez v1, :cond_6

    .line 127
    new-instance p2, Lee/cyber/smartid/tse/dto/InvalidResponseIdException;

    invoke-direct {p2}, Lee/cyber/smartid/tse/dto/InvalidResponseIdException;-><init>()V

    invoke-virtual {p0, p1, v2, p2}, Lee/cyber/smartid/tse/network/RPCCallback;->onFailure(Lc/b;Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCError;Ljava/lang/Throwable;)V

    .line 128
    invoke-direct {p0}, Lee/cyber/smartid/tse/network/RPCCallback;->a()V

    return-void

    :cond_6
    if-nez v0, :cond_7

    .line 134
    invoke-virtual {p2}, Lc/r;->b()I

    move-result v1

    const/16 v3, 0xc8

    if-eq v1, v3, :cond_7

    .line 136
    new-instance v0, Lee/cyber/smartid/tse/dto/HttpErrorCodeResponseException;

    invoke-virtual {p2}, Lc/r;->b()I

    move-result p2

    invoke-direct {v0, p2}, Lee/cyber/smartid/tse/dto/HttpErrorCodeResponseException;-><init>(I)V

    invoke-virtual {p0, p1, v2, v0}, Lee/cyber/smartid/tse/network/RPCCallback;->onFailure(Lc/b;Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCError;Ljava/lang/Throwable;)V

    .line 137
    invoke-direct {p0}, Lee/cyber/smartid/tse/network/RPCCallback;->a()V

    return-void

    :cond_7
    if-eqz v0, :cond_8

    .line 142
    invoke-virtual {v0}, Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCResponse;->isError()Z

    move-result v1

    if-eqz v1, :cond_8

    .line 143
    invoke-virtual {v0}, Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCResponse;->getError()Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCError;

    move-result-object p2

    invoke-virtual {p0, p1, p2, v2}, Lee/cyber/smartid/tse/network/RPCCallback;->onFailure(Lc/b;Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCError;Ljava/lang/Throwable;)V

    .line 144
    invoke-direct {p0}, Lee/cyber/smartid/tse/network/RPCCallback;->a()V

    return-void

    .line 149
    :cond_8
    invoke-virtual {p0, p1, p2}, Lee/cyber/smartid/tse/network/RPCCallback;->onValidate(Lc/b;Lc/r;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 150
    invoke-virtual {p0, p1, p2}, Lee/cyber/smartid/tse/network/RPCCallback;->onSuccess(Lc/b;Lc/r;)V

    goto :goto_1

    .line 152
    :cond_9
    invoke-direct {p0}, Lee/cyber/smartid/tse/network/RPCCallback;->a()V

    .line 153
    new-instance p2, Lee/cyber/smartid/tse/dto/InvalidResponseResultException;

    invoke-direct {p2}, Lee/cyber/smartid/tse/dto/InvalidResponseResultException;-><init>()V

    invoke-virtual {p0, p1, v2, p2}, Lee/cyber/smartid/tse/network/RPCCallback;->onFailure(Lc/b;Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCError;Ljava/lang/Throwable;)V

    :goto_1
    return-void
.end method

.method public abstract onSuccess(Lc/b;Lc/r;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lc/b<",
            "TT;>;",
            "Lc/r<",
            "TT;>;)V"
        }
    .end annotation
.end method

.method public abstract onValidate(Lc/b;Lc/r;)Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lc/b<",
            "TT;>;",
            "Lc/r<",
            "TT;>;)Z"
        }
    .end annotation
.end method
