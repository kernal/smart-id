.class public interface abstract Lee/cyber/smartid/tse/network/TSEAPI$TSEAPIService;
.super Ljava/lang/Object;
.source "TSEAPI.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lee/cyber/smartid/tse/network/TSEAPI;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "TSEAPIService"
.end annotation


# virtual methods
.method public abstract getNewFreshnessToken(Ljava/lang/String;Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCRequest;)Lc/b;
    .param p1    # Ljava/lang/String;
        .annotation runtime Lc/b/i;
            a = "X-SplitKey-Trigger"
        .end annotation
    .end param
    .param p2    # Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCRequest;
        .annotation runtime Lc/b/a;
        .end annotation
    .end param
    .annotation runtime Lc/b/k;
        a = {
            "Content-Type: application/json-rpc",
            "Accept-Charset: utf-8"
        }
    .end annotation

    .annotation runtime Lc/b/o;
        a = "v2/protected"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCRequest;",
            ")",
            "Lc/b<",
            "Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCResponse<",
            "Lee/cyber/smartid/tse/dto/jsonrpc/result/NewFreshnessTokenResult;",
            ">;>;"
        }
    .end annotation
.end method

.method public abstract refreshCloneDetection(Ljava/lang/String;Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCRequest;)Lc/b;
    .param p1    # Ljava/lang/String;
        .annotation runtime Lc/b/i;
            a = "X-SplitKey-Trigger"
        .end annotation
    .end param
    .param p2    # Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCRequest;
        .annotation runtime Lc/b/a;
        .end annotation
    .end param
    .annotation runtime Lc/b/k;
        a = {
            "Content-Type: application/json-rpc",
            "Accept-Charset: utf-8"
        }
    .end annotation

    .annotation runtime Lc/b/o;
        a = "v2/protected"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCRequest;",
            ")",
            "Lc/b<",
            "Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCResponse<",
            "Lee/cyber/smartid/tse/dto/jsonrpc/result/RefreshCloneDetectionResult;",
            ">;>;"
        }
    .end annotation
.end method

.method public abstract submitClientSecondPart(Ljava/lang/String;Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCRequest;)Lc/b;
    .param p1    # Ljava/lang/String;
        .annotation runtime Lc/b/i;
            a = "X-SplitKey-Trigger"
        .end annotation
    .end param
    .param p2    # Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCRequest;
        .annotation runtime Lc/b/a;
        .end annotation
    .end param
    .annotation runtime Lc/b/k;
        a = {
            "Content-Type: application/json-rpc",
            "Accept-Charset: utf-8"
        }
    .end annotation

    .annotation runtime Lc/b/o;
        a = "v2/protected"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCRequest;",
            ")",
            "Lc/b<",
            "Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCResponse<",
            "Lee/cyber/smartid/tse/dto/jsonrpc/result/SubmitClientSecondPartResult;",
            ">;>;"
        }
    .end annotation
.end method

.method public abstract submitSignature(Ljava/lang/String;Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCRequest;)Lc/b;
    .param p1    # Ljava/lang/String;
        .annotation runtime Lc/b/i;
            a = "X-SplitKey-Trigger"
        .end annotation
    .end param
    .param p2    # Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCRequest;
        .annotation runtime Lc/b/a;
        .end annotation
    .end param
    .annotation runtime Lc/b/k;
        a = {
            "Content-Type: application/json-rpc",
            "Accept-Charset: utf-8"
        }
    .end annotation

    .annotation runtime Lc/b/o;
        a = "v2/protected"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCRequest;",
            ")",
            "Lc/b<",
            "Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCResponse<",
            "Lee/cyber/smartid/tse/dto/jsonrpc/result/SubmitSignatureResult;",
            ">;>;"
        }
    .end annotation
.end method
