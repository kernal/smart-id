.class public abstract Lee/cyber/smartid/tse/network/RPCCallbackNonUIThread;
.super Lee/cyber/smartid/tse/network/RPCCallback;
.source "RPCCallbackNonUIThread.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lee/cyber/smartid/tse/network/RPCCallback<",
        "TT;>;"
    }
.end annotation


# direct methods
.method public constructor <init>(Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCRequest;Lee/cyber/smartid/tse/SmartIdTSE;)V
    .locals 0

    .line 43
    invoke-direct {p0, p1, p2}, Lee/cyber/smartid/tse/network/RPCCallback;-><init>(Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCRequest;Lee/cyber/smartid/tse/SmartIdTSE;)V

    return-void
.end method

.method public constructor <init>(Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCRequest;Lee/cyber/smartid/tse/inter/WallClock;Lee/cyber/smartid/tse/inter/ListenerAccess;)V
    .locals 0

    .line 33
    invoke-direct {p0, p1, p2, p3}, Lee/cyber/smartid/tse/network/RPCCallback;-><init>(Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCRequest;Lee/cyber/smartid/tse/inter/WallClock;Lee/cyber/smartid/tse/inter/ListenerAccess;)V

    return-void
.end method

.method static synthetic a(Lee/cyber/smartid/tse/network/RPCCallbackNonUIThread;Lc/b;Lc/r;)V
    .locals 0

    .line 23
    invoke-super {p0, p1, p2}, Lee/cyber/smartid/tse/network/RPCCallback;->onResponse(Lc/b;Lc/r;)V

    return-void
.end method

.method static synthetic a(Lee/cyber/smartid/tse/network/RPCCallbackNonUIThread;Lc/b;Ljava/lang/Throwable;)V
    .locals 0

    .line 23
    invoke-super {p0, p1, p2}, Lee/cyber/smartid/tse/network/RPCCallback;->onFailure(Lc/b;Ljava/lang/Throwable;)V

    return-void
.end method


# virtual methods
.method public onFailure(Lc/b;Ljava/lang/Throwable;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lc/b<",
            "TT;>;",
            "Ljava/lang/Throwable;",
            ")V"
        }
    .end annotation

    .line 70
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lee/cyber/smartid/tse/network/RPCCallbackNonUIThread$2;

    invoke-direct {v1, p0, p1, p2}, Lee/cyber/smartid/tse/network/RPCCallbackNonUIThread$2;-><init>(Lee/cyber/smartid/tse/network/RPCCallbackNonUIThread;Lc/b;Ljava/lang/Throwable;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 75
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    return-void
.end method

.method public onResponse(Lc/b;Lc/r;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lc/b<",
            "TT;>;",
            "Lc/r<",
            "TT;>;)V"
        }
    .end annotation

    .line 54
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lee/cyber/smartid/tse/network/RPCCallbackNonUIThread$1;

    invoke-direct {v1, p0, p1, p2}, Lee/cyber/smartid/tse/network/RPCCallbackNonUIThread$1;-><init>(Lee/cyber/smartid/tse/network/RPCCallbackNonUIThread;Lc/b;Lc/r;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 59
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    return-void
.end method
