.class final Lee/cyber/smartid/tse/network/HammerTimeInterceptor;
.super Ljava/lang/Object;
.source "HammerTimeInterceptor.java"

# interfaces
.implements Lokhttp3/t;


# instance fields
.field private final a:Lee/cyber/smartid/tse/inter/ResourceAccess;

.field private final b:Lee/cyber/smartid/tse/inter/WallClock;

.field private c:Lee/cyber/smartid/tse/inter/LogAccess;


# direct methods
.method public constructor <init>(Lee/cyber/smartid/tse/inter/ResourceAccess;Lee/cyber/smartid/tse/inter/WallClock;)V
    .locals 1

    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    invoke-static {p0}, Lee/cyber/smartid/tse/util/Log;->getInstance(Ljava/lang/Object;)Lee/cyber/smartid/tse/util/Log;

    move-result-object v0

    iput-object v0, p0, Lee/cyber/smartid/tse/network/HammerTimeInterceptor;->c:Lee/cyber/smartid/tse/inter/LogAccess;

    .line 39
    iput-object p1, p0, Lee/cyber/smartid/tse/network/HammerTimeInterceptor;->a:Lee/cyber/smartid/tse/inter/ResourceAccess;

    .line 40
    iput-object p2, p0, Lee/cyber/smartid/tse/network/HammerTimeInterceptor;->b:Lee/cyber/smartid/tse/inter/WallClock;

    return-void
.end method

.method private b()V
    .locals 8

    .line 73
    invoke-virtual {p0}, Lee/cyber/smartid/tse/network/HammerTimeInterceptor;->a()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-lez v4, :cond_0

    .line 75
    iget-object v2, p0, Lee/cyber/smartid/tse/network/HammerTimeInterceptor;->c:Lee/cyber/smartid/tse/inter/LogAccess;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "addDelayIfNeeded - Delaying the request for "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    long-to-double v4, v0

    const-wide v6, 0x408f400000000000L    # 1000.0

    div-double/2addr v4, v6

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    const-string v4, " sec"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lee/cyber/smartid/tse/inter/LogAccess;->d(Ljava/lang/String;)V

    .line 76
    invoke-static {v0, v1}, Landroid/os/SystemClock;->sleep(J)V

    :cond_0
    return-void
.end method


# virtual methods
.method a()J
    .locals 7

    .line 64
    iget-object v0, p0, Lee/cyber/smartid/tse/network/HammerTimeInterceptor;->b:Lee/cyber/smartid/tse/inter/WallClock;

    invoke-interface {v0}, Lee/cyber/smartid/tse/inter/WallClock;->currentTimeMillis()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-ltz v4, :cond_2

    iget-object v0, p0, Lee/cyber/smartid/tse/network/HammerTimeInterceptor;->b:Lee/cyber/smartid/tse/inter/WallClock;

    invoke-interface {v0}, Lee/cyber/smartid/tse/inter/WallClock;->currentTimeMillis()J

    move-result-wide v0

    iget-object v4, p0, Lee/cyber/smartid/tse/network/HammerTimeInterceptor;->a:Lee/cyber/smartid/tse/inter/ResourceAccess;

    invoke-interface {v4}, Lee/cyber/smartid/tse/inter/ResourceAccess;->getLastHammerTimeEventTimestamp()J

    move-result-wide v4

    cmp-long v6, v0, v4

    if-ltz v6, :cond_2

    iget-object v0, p0, Lee/cyber/smartid/tse/network/HammerTimeInterceptor;->a:Lee/cyber/smartid/tse/inter/ResourceAccess;

    invoke-interface {v0}, Lee/cyber/smartid/tse/inter/ResourceAccess;->getMinInteractiveRequestDelay()J

    move-result-wide v0

    cmp-long v4, v0, v2

    if-gtz v4, :cond_0

    goto :goto_0

    .line 68
    :cond_0
    iget-object v0, p0, Lee/cyber/smartid/tse/network/HammerTimeInterceptor;->b:Lee/cyber/smartid/tse/inter/WallClock;

    invoke-interface {v0}, Lee/cyber/smartid/tse/inter/WallClock;->currentTimeMillis()J

    move-result-wide v0

    iget-object v4, p0, Lee/cyber/smartid/tse/network/HammerTimeInterceptor;->a:Lee/cyber/smartid/tse/inter/ResourceAccess;

    invoke-interface {v4}, Lee/cyber/smartid/tse/inter/ResourceAccess;->getLastHammerTimeEventTimestamp()J

    move-result-wide v4

    sub-long/2addr v0, v4

    .line 69
    iget-object v4, p0, Lee/cyber/smartid/tse/network/HammerTimeInterceptor;->a:Lee/cyber/smartid/tse/inter/ResourceAccess;

    invoke-interface {v4}, Lee/cyber/smartid/tse/inter/ResourceAccess;->getMinInteractiveRequestDelay()J

    move-result-wide v4

    cmp-long v6, v4, v0

    if-gtz v6, :cond_1

    goto :goto_0

    :cond_1
    iget-object v2, p0, Lee/cyber/smartid/tse/network/HammerTimeInterceptor;->a:Lee/cyber/smartid/tse/inter/ResourceAccess;

    invoke-interface {v2}, Lee/cyber/smartid/tse/inter/ResourceAccess;->getMinInteractiveRequestDelay()J

    move-result-wide v2

    sub-long/2addr v2, v0

    :cond_2
    :goto_0
    return-wide v2
.end method

.method public intercept(Lokhttp3/t$a;)Lokhttp3/ab;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 52
    invoke-interface {p1}, Lokhttp3/t$a;->a()Lokhttp3/z;

    move-result-object v0

    .line 53
    invoke-direct {p0}, Lee/cyber/smartid/tse/network/HammerTimeInterceptor;->b()V

    .line 54
    invoke-interface {p1, v0}, Lokhttp3/t$a;->a(Lokhttp3/z;)Lokhttp3/ab;

    move-result-object p1

    return-object p1
.end method
