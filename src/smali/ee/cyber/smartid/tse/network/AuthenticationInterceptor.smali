.class final Lee/cyber/smartid/tse/network/AuthenticationInterceptor;
.super Ljava/lang/Object;
.source "AuthenticationInterceptor.java"

# interfaces
.implements Lokhttp3/t;


# instance fields
.field private final a:Lee/cyber/smartid/tse/inter/ResourceAccess;

.field private final b:Lee/cyber/smartid/tse/inter/LogAccess;


# direct methods
.method public constructor <init>(Lee/cyber/smartid/tse/inter/ResourceAccess;)V
    .locals 1

    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    invoke-static {p0}, Lee/cyber/smartid/tse/util/Log;->getInstance(Ljava/lang/Object;)Lee/cyber/smartid/tse/util/Log;

    move-result-object v0

    iput-object v0, p0, Lee/cyber/smartid/tse/network/AuthenticationInterceptor;->b:Lee/cyber/smartid/tse/inter/LogAccess;

    .line 59
    iput-object p1, p0, Lee/cyber/smartid/tse/network/AuthenticationInterceptor;->a:Lee/cyber/smartid/tse/inter/ResourceAccess;

    return-void
.end method


# virtual methods
.method a()Ljava/lang/String;
    .locals 3

    .line 93
    iget-object v0, p0, Lee/cyber/smartid/tse/network/AuthenticationInterceptor;->a:Lee/cyber/smartid/tse/inter/ResourceAccess;

    invoke-interface {v0}, Lee/cyber/smartid/tse/inter/ResourceAccess;->isDeviceRegistrationDone()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return-object v0

    .line 96
    :cond_0
    iget-object v0, p0, Lee/cyber/smartid/tse/network/AuthenticationInterceptor;->a:Lee/cyber/smartid/tse/inter/ResourceAccess;

    invoke-interface {v0}, Lee/cyber/smartid/tse/inter/ResourceAccess;->getAppInstanceStorageId()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lee/cyber/smartid/tse/inter/ResourceAccess;->loadString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lee/cyber/smartid/tse/network/AuthenticationInterceptor;->a:Lee/cyber/smartid/tse/inter/ResourceAccess;

    invoke-interface {v1}, Lee/cyber/smartid/tse/inter/ResourceAccess;->getPasswordStorageId()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lee/cyber/smartid/tse/inter/ResourceAccess;->loadString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lee/cyber/smartid/tse/network/AuthenticationInterceptor;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 8

    const-string v0, "utf-8"

    .line 110
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    return-object v2

    .line 115
    :cond_0
    :try_start_0
    new-instance v1, Ljava/lang/String;

    const-string v3, "%s:%s"

    const/4 v4, 0x2

    new-array v5, v4, [Ljava/lang/Object;

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    const-string v7, ""

    if-eqz v6, :cond_1

    move-object p1, v7

    :cond_1
    const/4 v6, 0x0

    :try_start_1
    aput-object p1, v5, v6

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p1

    if-eqz p1, :cond_2

    move-object p2, v7

    :cond_2
    const/4 p1, 0x1

    aput-object p2, v5, p1

    invoke-static {v3, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p2, v0}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object p2

    invoke-static {p2, v4}, Landroid/util/Base64;->encode([BI)[B

    move-result-object p2

    invoke-direct {v1, p2, v0}, Ljava/lang/String;-><init>([BLjava/lang/String;)V
    :try_end_1
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_1 .. :try_end_1} :catch_0

    .line 121
    new-array p2, v4, [Ljava/lang/Object;

    const-string v0, "Basic"

    aput-object v0, p2, v6

    aput-object v1, p2, p1

    const-string p1, "%s %s"

    invoke-static {p1, p2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    return-object p1

    :catch_0
    move-exception p1

    .line 118
    iget-object p2, p0, Lee/cyber/smartid/tse/network/AuthenticationInterceptor;->b:Lee/cyber/smartid/tse/inter/LogAccess;

    const-string v0, "createBasicAuthenticationHeaderValue"

    invoke-interface {p2, v0, p1}, Lee/cyber/smartid/tse/inter/LogAccess;->wtf(Ljava/lang/String;Ljava/lang/Throwable;)V

    return-object v2
.end method

.method public intercept(Lokhttp3/t$a;)Lokhttp3/ab;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 71
    invoke-interface {p1}, Lokhttp3/t$a;->a()Lokhttp3/z;

    move-result-object v0

    .line 72
    invoke-virtual {p0}, Lee/cyber/smartid/tse/network/AuthenticationInterceptor;->a()Ljava/lang/String;

    move-result-object v1

    .line 73
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 74
    iget-object v1, p0, Lee/cyber/smartid/tse/network/AuthenticationInterceptor;->b:Lee/cyber/smartid/tse/inter/LogAccess;

    const-string v2, "intercept: Auth token not added"

    invoke-interface {v1, v2}, Lee/cyber/smartid/tse/inter/LogAccess;->d(Ljava/lang/String;)V

    .line 76
    invoke-interface {p1, v0}, Lokhttp3/t$a;->a(Lokhttp3/z;)Lokhttp3/ab;

    move-result-object p1

    return-object p1

    .line 78
    :cond_0
    invoke-virtual {v0}, Lokhttp3/z;->e()Lokhttp3/z$a;

    move-result-object v0

    const-string v2, "Authorization"

    .line 79
    invoke-virtual {v0, v2, v1}, Lokhttp3/z$a;->a(Ljava/lang/String;Ljava/lang/String;)Lokhttp3/z$a;

    move-result-object v0

    invoke-virtual {v0}, Lokhttp3/z$a;->a()Lokhttp3/z;

    move-result-object v0

    .line 80
    iget-object v1, p0, Lee/cyber/smartid/tse/network/AuthenticationInterceptor;->b:Lee/cyber/smartid/tse/inter/LogAccess;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "intercept: Added auth token to "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Lokhttp3/z;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, " request"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lee/cyber/smartid/tse/inter/LogAccess;->d(Ljava/lang/String;)V

    .line 81
    invoke-interface {p1, v0}, Lokhttp3/t$a;->a(Lokhttp3/z;)Lokhttp3/ab;

    move-result-object p1

    return-object p1
.end method
