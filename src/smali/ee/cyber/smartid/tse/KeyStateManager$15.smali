.class Lee/cyber/smartid/tse/KeyStateManager$15;
.super Ljava/lang/Object;
.source "KeyStateManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lee/cyber/smartid/tse/KeyStateManager;->checkLocalPendingState(Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/tse/inter/CheckLocalPendingStateListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:Ljava/lang/String;

.field final synthetic c:Lee/cyber/smartid/tse/KeyStateManager;


# direct methods
.method constructor <init>(Lee/cyber/smartid/tse/KeyStateManager;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 1279
    iput-object p1, p0, Lee/cyber/smartid/tse/KeyStateManager$15;->c:Lee/cyber/smartid/tse/KeyStateManager;

    iput-object p2, p0, Lee/cyber/smartid/tse/KeyStateManager$15;->a:Ljava/lang/String;

    iput-object p3, p0, Lee/cyber/smartid/tse/KeyStateManager$15;->b:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .line 1282
    iget-object v0, p0, Lee/cyber/smartid/tse/KeyStateManager$15;->c:Lee/cyber/smartid/tse/KeyStateManager;

    invoke-static {v0}, Lee/cyber/smartid/tse/KeyStateManager;->g(Lee/cyber/smartid/tse/KeyStateManager;)Ljava/lang/Object;

    move-result-object v0

    monitor-enter v0

    .line 1284
    :try_start_0
    iget-object v1, p0, Lee/cyber/smartid/tse/KeyStateManager$15;->c:Lee/cyber/smartid/tse/KeyStateManager;

    iget-object v2, p0, Lee/cyber/smartid/tse/KeyStateManager$15;->a:Ljava/lang/String;

    iget-object v3, p0, Lee/cyber/smartid/tse/KeyStateManager$15;->b:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lee/cyber/smartid/tse/KeyStateManager;->a(Ljava/lang/String;Ljava/lang/String;)Lee/cyber/smartid/tse/dto/jsonrpc/resp/CheckLocalPendingStateResp;

    move-result-object v1

    .line 1285
    iget-object v2, p0, Lee/cyber/smartid/tse/KeyStateManager$15;->c:Lee/cyber/smartid/tse/KeyStateManager;

    new-instance v3, Lee/cyber/smartid/tse/KeyStateManager$15$1;

    invoke-direct {v3, p0, v1}, Lee/cyber/smartid/tse/KeyStateManager$15$1;-><init>(Lee/cyber/smartid/tse/KeyStateManager$15;Lee/cyber/smartid/tse/dto/jsonrpc/resp/CheckLocalPendingStateResp;)V

    invoke-static {v2, v3}, Lee/cyber/smartid/tse/KeyStateManager;->a(Lee/cyber/smartid/tse/KeyStateManager;Ljava/lang/Runnable;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v1

    goto :goto_1

    :catch_0
    move-exception v1

    .line 1296
    :try_start_1
    iget-object v2, p0, Lee/cyber/smartid/tse/KeyStateManager$15;->c:Lee/cyber/smartid/tse/KeyStateManager;

    invoke-static {v2}, Lee/cyber/smartid/tse/KeyStateManager;->b(Lee/cyber/smartid/tse/KeyStateManager;)Lee/cyber/smartid/tse/inter/LogAccess;

    move-result-object v2

    const-string v3, "checkLocalPendingState"

    invoke-interface {v2, v3, v1}, Lee/cyber/smartid/tse/inter/LogAccess;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1297
    iget-object v2, p0, Lee/cyber/smartid/tse/KeyStateManager$15;->c:Lee/cyber/smartid/tse/KeyStateManager;

    new-instance v3, Lee/cyber/smartid/tse/KeyStateManager$15$2;

    invoke-direct {v3, p0, v1}, Lee/cyber/smartid/tse/KeyStateManager$15$2;-><init>(Lee/cyber/smartid/tse/KeyStateManager$15;Ljava/lang/Throwable;)V

    invoke-static {v2, v3}, Lee/cyber/smartid/tse/KeyStateManager;->a(Lee/cyber/smartid/tse/KeyStateManager;Ljava/lang/Runnable;)V

    .line 1305
    :goto_0
    monitor-exit v0

    return-void

    :goto_1
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method
