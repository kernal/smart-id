.class public Lee/cyber/smartid/tse/SmartIdTSE$TSEListenerAccess;
.super Ljava/lang/Object;
.source "SmartIdTSE.java"

# interfaces
.implements Lee/cyber/smartid/tse/inter/ListenerAccess;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lee/cyber/smartid/tse/SmartIdTSE;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "TSEListenerAccess"
.end annotation


# instance fields
.field final synthetic a:Lee/cyber/smartid/tse/SmartIdTSE;


# direct methods
.method private constructor <init>(Lee/cyber/smartid/tse/SmartIdTSE;)V
    .locals 0

    .line 590
    iput-object p1, p0, Lee/cyber/smartid/tse/SmartIdTSE$TSEListenerAccess;->a:Lee/cyber/smartid/tse/SmartIdTSE;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lee/cyber/smartid/tse/SmartIdTSE;Lee/cyber/smartid/tse/SmartIdTSE$1;)V
    .locals 0

    .line 588
    invoke-direct {p0, p1}, Lee/cyber/smartid/tse/SmartIdTSE$TSEListenerAccess;-><init>(Lee/cyber/smartid/tse/SmartIdTSE;)V

    return-void
.end method


# virtual methods
.method public getApplicationContext()Landroid/content/Context;
    .locals 1

    .line 625
    iget-object v0, p0, Lee/cyber/smartid/tse/SmartIdTSE$TSEListenerAccess;->a:Lee/cyber/smartid/tse/SmartIdTSE;

    invoke-static {v0}, Lee/cyber/smartid/tse/SmartIdTSE;->j(Lee/cyber/smartid/tse/SmartIdTSE;)Lee/cyber/smartid/tse/inter/ResourceAccess;

    move-result-object v0

    invoke-interface {v0}, Lee/cyber/smartid/tse/inter/ResourceAccess;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    return-object v0
.end method

.method public getListener(Ljava/lang/String;ZLjava/lang/Class;)Lee/cyber/smartid/tse/inter/TSEListener;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lee/cyber/smartid/tse/inter/TSEListener;",
            ">(",
            "Ljava/lang/String;",
            "Z",
            "Ljava/lang/Class<",
            "TT;>;)TT;"
        }
    .end annotation

    .line 595
    iget-object v0, p0, Lee/cyber/smartid/tse/SmartIdTSE$TSEListenerAccess;->a:Lee/cyber/smartid/tse/SmartIdTSE;

    invoke-static {v0, p1, p2, p3}, Lee/cyber/smartid/tse/SmartIdTSE;->a(Lee/cyber/smartid/tse/SmartIdTSE;Ljava/lang/String;ZLjava/lang/Class;)Lee/cyber/smartid/tse/inter/TSEListener;

    move-result-object p1

    return-object p1
.end method

.method public notifyError(Ljava/lang/String;Lee/cyber/smartid/tse/inter/TSEListener;Lee/cyber/smartid/tse/dto/TSEError;)V
    .locals 1

    .line 605
    iget-object v0, p0, Lee/cyber/smartid/tse/SmartIdTSE$TSEListenerAccess;->a:Lee/cyber/smartid/tse/SmartIdTSE;

    invoke-static {v0, p1, p2, p3}, Lee/cyber/smartid/tse/SmartIdTSE;->a(Lee/cyber/smartid/tse/SmartIdTSE;Ljava/lang/String;Lee/cyber/smartid/tse/inter/TSEListener;Lee/cyber/smartid/tse/dto/TSEError;)V

    return-void
.end method

.method public notifySystemEventsListeners(Lee/cyber/smartid/tse/dto/TSEError;)V
    .locals 1

    .line 615
    iget-object v0, p0, Lee/cyber/smartid/tse/SmartIdTSE$TSEListenerAccess;->a:Lee/cyber/smartid/tse/SmartIdTSE;

    invoke-static {v0, p1}, Lee/cyber/smartid/tse/SmartIdTSE;->a(Lee/cyber/smartid/tse/SmartIdTSE;Lee/cyber/smartid/tse/dto/TSEError;)V

    return-void
.end method

.method public notifyUI(Ljava/lang/Runnable;)V
    .locals 1

    .line 610
    iget-object v0, p0, Lee/cyber/smartid/tse/SmartIdTSE$TSEListenerAccess;->a:Lee/cyber/smartid/tse/SmartIdTSE;

    invoke-static {v0, p1}, Lee/cyber/smartid/tse/SmartIdTSE;->a(Lee/cyber/smartid/tse/SmartIdTSE;Ljava/lang/Runnable;)V

    return-void
.end method

.method public setHammerTimeEventTimestamp()V
    .locals 1

    .line 620
    iget-object v0, p0, Lee/cyber/smartid/tse/SmartIdTSE$TSEListenerAccess;->a:Lee/cyber/smartid/tse/SmartIdTSE;

    invoke-static {v0}, Lee/cyber/smartid/tse/SmartIdTSE;->j(Lee/cyber/smartid/tse/SmartIdTSE;)Lee/cyber/smartid/tse/inter/ResourceAccess;

    move-result-object v0

    invoke-interface {v0}, Lee/cyber/smartid/tse/inter/ResourceAccess;->setHammerTimeEventTimestamp()V

    return-void
.end method

.method public setListener(Ljava/lang/String;Lee/cyber/smartid/tse/inter/TSEListener;)V
    .locals 1

    .line 600
    iget-object v0, p0, Lee/cyber/smartid/tse/SmartIdTSE$TSEListenerAccess;->a:Lee/cyber/smartid/tse/SmartIdTSE;

    invoke-virtual {v0, p1, p2}, Lee/cyber/smartid/tse/SmartIdTSE;->setListener(Ljava/lang/String;Lee/cyber/smartid/tse/inter/TSEListener;)V

    return-void
.end method
