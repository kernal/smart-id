.class Lee/cyber/smartid/tse/KeyGenerationManager$4;
.super Ljava/lang/Object;
.source "KeyGenerationManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lee/cyber/smartid/tse/KeyGenerationManager;->a(Lee/cyber/smartid/tse/inter/WallClock;Ljava/lang/Exception;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Ljava/lang/Exception;

.field final synthetic b:Lee/cyber/smartid/tse/KeyGenerationManager;


# direct methods
.method constructor <init>(Lee/cyber/smartid/tse/KeyGenerationManager;Ljava/lang/Exception;)V
    .locals 0

    .line 215
    iput-object p1, p0, Lee/cyber/smartid/tse/KeyGenerationManager$4;->b:Lee/cyber/smartid/tse/KeyGenerationManager;

    iput-object p2, p0, Lee/cyber/smartid/tse/KeyGenerationManager$4;->a:Ljava/lang/Exception;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .line 218
    iget-object v0, p0, Lee/cyber/smartid/tse/KeyGenerationManager$4;->b:Lee/cyber/smartid/tse/KeyGenerationManager;

    invoke-static {v0}, Lee/cyber/smartid/tse/KeyGenerationManager;->c(Lee/cyber/smartid/tse/KeyGenerationManager;)Lee/cyber/smartid/tse/KeyGenerationManager$KeyGenerationWorkerListener;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lee/cyber/smartid/tse/KeyGenerationManager$4;->a:Ljava/lang/Exception;

    if-eqz v0, :cond_0

    .line 219
    iget-object v0, p0, Lee/cyber/smartid/tse/KeyGenerationManager$4;->b:Lee/cyber/smartid/tse/KeyGenerationManager;

    invoke-static {v0}, Lee/cyber/smartid/tse/KeyGenerationManager;->c(Lee/cyber/smartid/tse/KeyGenerationManager;)Lee/cyber/smartid/tse/KeyGenerationManager$KeyGenerationWorkerListener;

    move-result-object v0

    iget-object v1, p0, Lee/cyber/smartid/tse/KeyGenerationManager$4;->b:Lee/cyber/smartid/tse/KeyGenerationManager;

    invoke-static {v1}, Lee/cyber/smartid/tse/KeyGenerationManager;->d(Lee/cyber/smartid/tse/KeyGenerationManager;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lee/cyber/smartid/tse/KeyGenerationManager$4;->a:Ljava/lang/Exception;

    invoke-interface {v0, v1, v2}, Lee/cyber/smartid/tse/KeyGenerationManager$KeyGenerationWorkerListener;->onGenerateKeysFailed(Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0

    .line 220
    :cond_0
    iget-object v0, p0, Lee/cyber/smartid/tse/KeyGenerationManager$4;->b:Lee/cyber/smartid/tse/KeyGenerationManager;

    invoke-static {v0}, Lee/cyber/smartid/tse/KeyGenerationManager;->c(Lee/cyber/smartid/tse/KeyGenerationManager;)Lee/cyber/smartid/tse/KeyGenerationManager$KeyGenerationWorkerListener;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 221
    iget-object v0, p0, Lee/cyber/smartid/tse/KeyGenerationManager$4;->b:Lee/cyber/smartid/tse/KeyGenerationManager;

    invoke-static {v0}, Lee/cyber/smartid/tse/KeyGenerationManager;->c(Lee/cyber/smartid/tse/KeyGenerationManager;)Lee/cyber/smartid/tse/KeyGenerationManager$KeyGenerationWorkerListener;

    move-result-object v1

    iget-object v0, p0, Lee/cyber/smartid/tse/KeyGenerationManager$4;->b:Lee/cyber/smartid/tse/KeyGenerationManager;

    invoke-static {v0}, Lee/cyber/smartid/tse/KeyGenerationManager;->d(Lee/cyber/smartid/tse/KeyGenerationManager;)Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lee/cyber/smartid/tse/KeyGenerationManager$4;->b:Lee/cyber/smartid/tse/KeyGenerationManager;

    invoke-static {v0}, Lee/cyber/smartid/tse/KeyGenerationManager;->e(Lee/cyber/smartid/tse/KeyGenerationManager;)Ljava/util/ArrayList;

    move-result-object v3

    iget-object v0, p0, Lee/cyber/smartid/tse/KeyGenerationManager$4;->b:Lee/cyber/smartid/tse/KeyGenerationManager;

    invoke-static {v0}, Lee/cyber/smartid/tse/KeyGenerationManager;->g(Lee/cyber/smartid/tse/KeyGenerationManager;)J

    move-result-wide v4

    iget-object v0, p0, Lee/cyber/smartid/tse/KeyGenerationManager$4;->b:Lee/cyber/smartid/tse/KeyGenerationManager;

    invoke-static {v0}, Lee/cyber/smartid/tse/KeyGenerationManager;->h(Lee/cyber/smartid/tse/KeyGenerationManager;)J

    move-result-wide v6

    sub-long/2addr v4, v6

    iget-object v0, p0, Lee/cyber/smartid/tse/KeyGenerationManager$4;->b:Lee/cyber/smartid/tse/KeyGenerationManager;

    invoke-static {v0}, Lee/cyber/smartid/tse/KeyGenerationManager;->i(Lee/cyber/smartid/tse/KeyGenerationManager;)[Lee/cyber/smartid/cryptolib/dto/TestResult;

    move-result-object v6

    invoke-interface/range {v1 .. v6}, Lee/cyber/smartid/tse/KeyGenerationManager$KeyGenerationWorkerListener;->onGenerateKeysSuccess(Ljava/lang/String;Ljava/util/ArrayList;J[Lee/cyber/smartid/cryptolib/dto/TestResult;)V

    .line 223
    :cond_1
    :goto_0
    iget-object v0, p0, Lee/cyber/smartid/tse/KeyGenerationManager$4;->b:Lee/cyber/smartid/tse/KeyGenerationManager;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lee/cyber/smartid/tse/KeyGenerationManager;->a(Lee/cyber/smartid/tse/KeyGenerationManager;Ljava/lang/String;)Ljava/lang/String;

    .line 224
    iget-object v0, p0, Lee/cyber/smartid/tse/KeyGenerationManager$4;->b:Lee/cyber/smartid/tse/KeyGenerationManager;

    invoke-static {v0, v1}, Lee/cyber/smartid/tse/KeyGenerationManager;->a(Lee/cyber/smartid/tse/KeyGenerationManager;Lee/cyber/smartid/tse/KeyGenerationManager$KeyGenerationWorkerListener;)Lee/cyber/smartid/tse/KeyGenerationManager$KeyGenerationWorkerListener;

    return-void
.end method
