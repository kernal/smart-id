.class Lee/cyber/smartid/tse/KeyManager$1;
.super Ljava/lang/Object;
.source "KeyManager.java"

# interfaces
.implements Lee/cyber/smartid/tse/KeyGenerationManager$KeyGenerationWorkerListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lee/cyber/smartid/tse/KeyManager;->generateKeys(Ljava/lang/String;IILjava/math/BigInteger;ILee/cyber/smartid/tse/inter/WallClock;Lee/cyber/smartid/tse/inter/GenerateKeysListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:I

.field final synthetic b:Lee/cyber/smartid/tse/inter/WallClock;

.field final synthetic c:Lee/cyber/smartid/tse/KeyManager;


# direct methods
.method constructor <init>(Lee/cyber/smartid/tse/KeyManager;ILee/cyber/smartid/tse/inter/WallClock;)V
    .locals 0

    .line 259
    iput-object p1, p0, Lee/cyber/smartid/tse/KeyManager$1;->c:Lee/cyber/smartid/tse/KeyManager;

    iput p2, p0, Lee/cyber/smartid/tse/KeyManager$1;->a:I

    iput-object p3, p0, Lee/cyber/smartid/tse/KeyManager$1;->b:Lee/cyber/smartid/tse/inter/WallClock;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onGenerateKeysFailed(Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 2

    .line 315
    iget-object v0, p0, Lee/cyber/smartid/tse/KeyManager$1;->c:Lee/cyber/smartid/tse/KeyManager;

    invoke-static {v0}, Lee/cyber/smartid/tse/KeyManager;->a(Lee/cyber/smartid/tse/KeyManager;)Lee/cyber/smartid/tse/inter/ListenerAccess;

    move-result-object v0

    new-instance v1, Lee/cyber/smartid/tse/KeyManager$1$4;

    invoke-direct {v1, p0, p1, p2}, Lee/cyber/smartid/tse/KeyManager$1$4;-><init>(Lee/cyber/smartid/tse/KeyManager$1;Ljava/lang/String;Ljava/lang/Exception;)V

    invoke-interface {v0, v1}, Lee/cyber/smartid/tse/inter/ListenerAccess;->notifyUI(Ljava/lang/Runnable;)V

    return-void
.end method

.method public onGenerateKeysProgress(Ljava/lang/String;II)V
    .locals 2

    .line 302
    iget-object v0, p0, Lee/cyber/smartid/tse/KeyManager$1;->c:Lee/cyber/smartid/tse/KeyManager;

    invoke-static {v0}, Lee/cyber/smartid/tse/KeyManager;->a(Lee/cyber/smartid/tse/KeyManager;)Lee/cyber/smartid/tse/inter/ListenerAccess;

    move-result-object v0

    new-instance v1, Lee/cyber/smartid/tse/KeyManager$1$3;

    invoke-direct {v1, p0, p1, p2, p3}, Lee/cyber/smartid/tse/KeyManager$1$3;-><init>(Lee/cyber/smartid/tse/KeyManager$1;Ljava/lang/String;II)V

    invoke-interface {v0, v1}, Lee/cyber/smartid/tse/inter/ListenerAccess;->notifyUI(Ljava/lang/Runnable;)V

    return-void
.end method

.method public onGenerateKeysSuccess(Ljava/lang/String;Ljava/util/ArrayList;J[Lee/cyber/smartid/cryptolib/dto/TestResult;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList<",
            "[",
            "Ljava/lang/String;",
            ">;J[",
            "Lee/cyber/smartid/cryptolib/dto/TestResult;",
            ")V"
        }
    .end annotation

    .line 264
    iget-object v0, p0, Lee/cyber/smartid/tse/KeyManager$1;->c:Lee/cyber/smartid/tse/KeyManager;

    iget v1, p0, Lee/cyber/smartid/tse/KeyManager$1;->a:I

    invoke-static {v0, p2, v1}, Lee/cyber/smartid/tse/KeyManager;->a(Lee/cyber/smartid/tse/KeyManager;Ljava/util/ArrayList;I)Ljava/util/ArrayList;

    move-result-object v5

    .line 268
    :try_start_0
    iget-object p2, p0, Lee/cyber/smartid/tse/KeyManager$1;->c:Lee/cyber/smartid/tse/KeyManager;

    invoke-static {p2, p5}, Lee/cyber/smartid/tse/KeyManager;->a(Lee/cyber/smartid/tse/KeyManager;[Lee/cyber/smartid/cryptolib/dto/TestResult;)V

    .line 270
    iget-object p2, p0, Lee/cyber/smartid/tse/KeyManager$1;->c:Lee/cyber/smartid/tse/KeyManager;

    invoke-static {p2}, Lee/cyber/smartid/tse/KeyManager;->a(Lee/cyber/smartid/tse/KeyManager;)Lee/cyber/smartid/tse/inter/ListenerAccess;

    move-result-object p2

    new-instance v0, Lee/cyber/smartid/tse/KeyManager$1$1;

    invoke-direct {v0, p0, p1, p5}, Lee/cyber/smartid/tse/KeyManager$1$1;-><init>(Lee/cyber/smartid/tse/KeyManager$1;Ljava/lang/String;[Lee/cyber/smartid/cryptolib/dto/TestResult;)V

    invoke-interface {p2, v0}, Lee/cyber/smartid/tse/inter/ListenerAccess;->notifyUI(Ljava/lang/Runnable;)V
    :try_end_0
    .catch Lee/cyber/smartid/cryptolib/dto/StorageException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p2

    .line 281
    iget-object p5, p0, Lee/cyber/smartid/tse/KeyManager$1;->c:Lee/cyber/smartid/tse/KeyManager;

    invoke-static {p5}, Lee/cyber/smartid/tse/KeyManager;->b(Lee/cyber/smartid/tse/KeyManager;)Lee/cyber/smartid/tse/util/Log;

    move-result-object p5

    const-string v0, "generateKeys"

    invoke-virtual {p5, v0, p2}, Lee/cyber/smartid/tse/util/Log;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 285
    :goto_0
    new-instance p2, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object p5

    invoke-direct {p2, p5}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 286
    iget-object p5, p0, Lee/cyber/smartid/tse/KeyManager$1;->c:Lee/cyber/smartid/tse/KeyManager;

    invoke-static {p5, v5}, Lee/cyber/smartid/tse/KeyManager;->a(Lee/cyber/smartid/tse/KeyManager;Ljava/util/ArrayList;)Ljava/lang/Runnable;

    move-result-object p5

    const-wide/32 v0, 0x927c0

    invoke-virtual {p2, p5, v0, v1}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 289
    iget-object p2, p0, Lee/cyber/smartid/tse/KeyManager$1;->c:Lee/cyber/smartid/tse/KeyManager;

    invoke-static {p2}, Lee/cyber/smartid/tse/KeyManager;->a(Lee/cyber/smartid/tse/KeyManager;)Lee/cyber/smartid/tse/inter/ListenerAccess;

    move-result-object p2

    new-instance p5, Lee/cyber/smartid/tse/KeyManager$1$2;

    move-object v2, p5

    move-object v3, p0

    move-object v4, p1

    move-wide v6, p3

    invoke-direct/range {v2 .. v7}, Lee/cyber/smartid/tse/KeyManager$1$2;-><init>(Lee/cyber/smartid/tse/KeyManager$1;Ljava/lang/String;Ljava/util/ArrayList;J)V

    invoke-interface {p2, p5}, Lee/cyber/smartid/tse/inter/ListenerAccess;->notifyUI(Ljava/lang/Runnable;)V

    return-void
.end method
