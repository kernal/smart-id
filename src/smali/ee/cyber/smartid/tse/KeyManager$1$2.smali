.class Lee/cyber/smartid/tse/KeyManager$1$2;
.super Ljava/lang/Object;
.source "KeyManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lee/cyber/smartid/tse/KeyManager$1;->onGenerateKeysSuccess(Ljava/lang/String;Ljava/util/ArrayList;J[Lee/cyber/smartid/cryptolib/dto/TestResult;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:Ljava/util/ArrayList;

.field final synthetic c:J

.field final synthetic d:Lee/cyber/smartid/tse/KeyManager$1;


# direct methods
.method constructor <init>(Lee/cyber/smartid/tse/KeyManager$1;Ljava/lang/String;Ljava/util/ArrayList;J)V
    .locals 0

    .line 289
    iput-object p1, p0, Lee/cyber/smartid/tse/KeyManager$1$2;->d:Lee/cyber/smartid/tse/KeyManager$1;

    iput-object p2, p0, Lee/cyber/smartid/tse/KeyManager$1$2;->a:Ljava/lang/String;

    iput-object p3, p0, Lee/cyber/smartid/tse/KeyManager$1$2;->b:Ljava/util/ArrayList;

    iput-wide p4, p0, Lee/cyber/smartid/tse/KeyManager$1$2;->c:J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .line 292
    iget-object v0, p0, Lee/cyber/smartid/tse/KeyManager$1$2;->d:Lee/cyber/smartid/tse/KeyManager$1;

    iget-object v0, v0, Lee/cyber/smartid/tse/KeyManager$1;->c:Lee/cyber/smartid/tse/KeyManager;

    invoke-static {v0}, Lee/cyber/smartid/tse/KeyManager;->a(Lee/cyber/smartid/tse/KeyManager;)Lee/cyber/smartid/tse/inter/ListenerAccess;

    move-result-object v0

    iget-object v1, p0, Lee/cyber/smartid/tse/KeyManager$1$2;->a:Ljava/lang/String;

    const-class v2, Lee/cyber/smartid/tse/inter/GenerateKeysListener;

    const/4 v3, 0x1

    invoke-interface {v0, v1, v3, v2}, Lee/cyber/smartid/tse/inter/ListenerAccess;->getListener(Ljava/lang/String;ZLjava/lang/Class;)Lee/cyber/smartid/tse/inter/TSEListener;

    move-result-object v0

    check-cast v0, Lee/cyber/smartid/tse/inter/GenerateKeysListener;

    if-eqz v0, :cond_0

    .line 294
    iget-object v1, p0, Lee/cyber/smartid/tse/KeyManager$1$2;->a:Ljava/lang/String;

    new-instance v2, Lee/cyber/smartid/tse/dto/jsonrpc/resp/GenerateKeysResp;

    iget-object v3, p0, Lee/cyber/smartid/tse/KeyManager$1$2;->b:Ljava/util/ArrayList;

    iget-wide v4, p0, Lee/cyber/smartid/tse/KeyManager$1$2;->c:J

    invoke-direct {v2, v1, v3, v4, v5}, Lee/cyber/smartid/tse/dto/jsonrpc/resp/GenerateKeysResp;-><init>(Ljava/lang/String;Ljava/util/ArrayList;J)V

    invoke-interface {v0, v1, v2}, Lee/cyber/smartid/tse/inter/GenerateKeysListener;->onGenerateKeysSuccess(Ljava/lang/String;Lee/cyber/smartid/tse/dto/jsonrpc/resp/GenerateKeysResp;)V

    :cond_0
    return-void
.end method
