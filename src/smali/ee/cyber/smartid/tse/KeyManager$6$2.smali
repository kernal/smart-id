.class Lee/cyber/smartid/tse/KeyManager$6$2;
.super Ljava/lang/Object;
.source "KeyManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lee/cyber/smartid/tse/KeyManager$6;->onValidatePinFailed(Ljava/lang/String;Lee/cyber/smartid/tse/dto/TSEError;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lee/cyber/smartid/tse/dto/TSEError;

.field final synthetic b:Lee/cyber/smartid/tse/KeyManager$6;


# direct methods
.method constructor <init>(Lee/cyber/smartid/tse/KeyManager$6;Lee/cyber/smartid/tse/dto/TSEError;)V
    .locals 0

    .line 609
    iput-object p1, p0, Lee/cyber/smartid/tse/KeyManager$6$2;->b:Lee/cyber/smartid/tse/KeyManager$6;

    iput-object p2, p0, Lee/cyber/smartid/tse/KeyManager$6$2;->a:Lee/cyber/smartid/tse/dto/TSEError;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .line 613
    iget-object v0, p0, Lee/cyber/smartid/tse/KeyManager$6$2;->b:Lee/cyber/smartid/tse/KeyManager$6;

    iget-object v0, v0, Lee/cyber/smartid/tse/KeyManager$6;->e:Lee/cyber/smartid/tse/KeyManager;

    invoke-static {v0}, Lee/cyber/smartid/tse/KeyManager;->a(Lee/cyber/smartid/tse/KeyManager;)Lee/cyber/smartid/tse/inter/ListenerAccess;

    move-result-object v0

    iget-object v1, p0, Lee/cyber/smartid/tse/KeyManager$6$2;->b:Lee/cyber/smartid/tse/KeyManager$6;

    iget-object v1, v1, Lee/cyber/smartid/tse/KeyManager$6;->a:Ljava/lang/String;

    const-class v2, Lee/cyber/smartid/tse/inter/EncryptKeyListener;

    const/4 v3, 0x1

    invoke-interface {v0, v1, v3, v2}, Lee/cyber/smartid/tse/inter/ListenerAccess;->getListener(Ljava/lang/String;ZLjava/lang/Class;)Lee/cyber/smartid/tse/inter/TSEListener;

    move-result-object v0

    check-cast v0, Lee/cyber/smartid/tse/inter/EncryptKeyListener;

    if-eqz v0, :cond_0

    .line 615
    iget-object v1, p0, Lee/cyber/smartid/tse/KeyManager$6$2;->b:Lee/cyber/smartid/tse/KeyManager$6;

    iget-object v1, v1, Lee/cyber/smartid/tse/KeyManager$6;->a:Ljava/lang/String;

    iget-object v2, p0, Lee/cyber/smartid/tse/KeyManager$6$2;->a:Lee/cyber/smartid/tse/dto/TSEError;

    invoke-interface {v0, v1, v2}, Lee/cyber/smartid/tse/inter/EncryptKeyListener;->onEncryptKeyFailed(Ljava/lang/String;Lee/cyber/smartid/tse/dto/TSEError;)V

    :cond_0
    return-void
.end method
