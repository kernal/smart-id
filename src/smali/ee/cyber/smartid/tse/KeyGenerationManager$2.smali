.class Lee/cyber/smartid/tse/KeyGenerationManager$2;
.super Ljava/lang/Object;
.source "KeyGenerationManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lee/cyber/smartid/tse/KeyGenerationManager;->generateKeys(Ljava/lang/String;IILjava/math/BigInteger;Lee/cyber/smartid/cryptolib/inter/KeyGenerationOp;Lee/cyber/smartid/cryptolib/inter/RandomGenerationOp;Lee/cyber/smartid/tse/inter/WallClock;Lee/cyber/smartid/tse/inter/ListenerAccess;Lee/cyber/smartid/tse/inter/ResourceAccess;Lee/cyber/smartid/tse/KeyGenerationManager$KeyGenerationWorkerListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lee/cyber/smartid/cryptolib/inter/RandomGenerationOp;

.field final synthetic b:Lee/cyber/smartid/tse/inter/WallClock;

.field final synthetic c:Lee/cyber/smartid/tse/inter/ResourceAccess;

.field final synthetic d:Lee/cyber/smartid/tse/KeyGenerationManager;


# direct methods
.method constructor <init>(Lee/cyber/smartid/tse/KeyGenerationManager;Lee/cyber/smartid/cryptolib/inter/RandomGenerationOp;Lee/cyber/smartid/tse/inter/WallClock;Lee/cyber/smartid/tse/inter/ResourceAccess;)V
    .locals 0

    .line 143
    iput-object p1, p0, Lee/cyber/smartid/tse/KeyGenerationManager$2;->d:Lee/cyber/smartid/tse/KeyGenerationManager;

    iput-object p2, p0, Lee/cyber/smartid/tse/KeyGenerationManager$2;->a:Lee/cyber/smartid/cryptolib/inter/RandomGenerationOp;

    iput-object p3, p0, Lee/cyber/smartid/tse/KeyGenerationManager$2;->b:Lee/cyber/smartid/tse/inter/WallClock;

    iput-object p4, p0, Lee/cyber/smartid/tse/KeyGenerationManager$2;->c:Lee/cyber/smartid/tse/inter/ResourceAccess;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 12

    .line 146
    iget-object v0, p0, Lee/cyber/smartid/tse/KeyGenerationManager$2;->d:Lee/cyber/smartid/tse/KeyGenerationManager;

    invoke-static {v0}, Lee/cyber/smartid/tse/KeyGenerationManager;->a(Lee/cyber/smartid/tse/KeyGenerationManager;)I

    move-result v0

    const/4 v1, 0x1

    if-ge v0, v1, :cond_0

    return-void

    .line 153
    :cond_0
    :try_start_0
    iget-object v0, p0, Lee/cyber/smartid/tse/KeyGenerationManager$2;->a:Lee/cyber/smartid/cryptolib/inter/RandomGenerationOp;

    iget-object v2, p0, Lee/cyber/smartid/tse/KeyGenerationManager$2;->d:Lee/cyber/smartid/tse/KeyGenerationManager;

    invoke-static {v2}, Lee/cyber/smartid/tse/KeyGenerationManager;->a(Lee/cyber/smartid/tse/KeyGenerationManager;)I

    move-result v2

    invoke-interface {v0, v2}, Lee/cyber/smartid/cryptolib/inter/RandomGenerationOp;->testCSPRNGQuality(I)[Lee/cyber/smartid/cryptolib/dto/TestResult;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 155
    iget-object v2, p0, Lee/cyber/smartid/tse/KeyGenerationManager$2;->d:Lee/cyber/smartid/tse/KeyGenerationManager;

    invoke-static {v2}, Lee/cyber/smartid/tse/KeyGenerationManager;->b(Lee/cyber/smartid/tse/KeyGenerationManager;)Lee/cyber/smartid/tse/util/Log;

    move-result-object v2

    const-string v3, "generateKeys - run"

    invoke-virtual {v2, v3, v0}, Lee/cyber/smartid/tse/util/Log;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    const/4 v0, 0x4

    .line 157
    new-array v0, v0, [Lee/cyber/smartid/cryptolib/dto/TestResult;

    .line 158
    new-instance v2, Lee/cyber/smartid/cryptolib/dto/TestResult;

    iget-object v3, p0, Lee/cyber/smartid/tse/KeyGenerationManager$2;->d:Lee/cyber/smartid/tse/KeyGenerationManager;

    invoke-static {v3}, Lee/cyber/smartid/tse/KeyGenerationManager;->a(Lee/cyber/smartid/tse/KeyGenerationManager;)I

    move-result v3

    const/4 v4, 0x0

    const-string v5, "FIPS 140-1 Monobit test"

    invoke-direct {v2, v5, v4, v3}, Lee/cyber/smartid/cryptolib/dto/TestResult;-><init>(Ljava/lang/String;II)V

    aput-object v2, v0, v4

    .line 159
    new-instance v2, Lee/cyber/smartid/cryptolib/dto/TestResult;

    iget-object v3, p0, Lee/cyber/smartid/tse/KeyGenerationManager$2;->d:Lee/cyber/smartid/tse/KeyGenerationManager;

    invoke-static {v3}, Lee/cyber/smartid/tse/KeyGenerationManager;->a(Lee/cyber/smartid/tse/KeyGenerationManager;)I

    move-result v3

    const-string v5, "FIPS 140-1 Poker test"

    invoke-direct {v2, v5, v4, v3}, Lee/cyber/smartid/cryptolib/dto/TestResult;-><init>(Ljava/lang/String;II)V

    aput-object v2, v0, v1

    const/4 v1, 0x2

    .line 160
    new-instance v2, Lee/cyber/smartid/cryptolib/dto/TestResult;

    iget-object v3, p0, Lee/cyber/smartid/tse/KeyGenerationManager$2;->d:Lee/cyber/smartid/tse/KeyGenerationManager;

    invoke-static {v3}, Lee/cyber/smartid/tse/KeyGenerationManager;->a(Lee/cyber/smartid/tse/KeyGenerationManager;)I

    move-result v3

    const-string v5, "FIPS 140-1 Runs test"

    invoke-direct {v2, v5, v4, v3}, Lee/cyber/smartid/cryptolib/dto/TestResult;-><init>(Ljava/lang/String;II)V

    aput-object v2, v0, v1

    const/4 v1, 0x3

    .line 161
    new-instance v2, Lee/cyber/smartid/cryptolib/dto/TestResult;

    iget-object v3, p0, Lee/cyber/smartid/tse/KeyGenerationManager$2;->d:Lee/cyber/smartid/tse/KeyGenerationManager;

    invoke-static {v3}, Lee/cyber/smartid/tse/KeyGenerationManager;->a(Lee/cyber/smartid/tse/KeyGenerationManager;)I

    move-result v3

    const-string v5, "FIPS 140-1 Long Runs test"

    invoke-direct {v2, v5, v4, v3}, Lee/cyber/smartid/cryptolib/dto/TestResult;-><init>(Ljava/lang/String;II)V

    aput-object v2, v0, v1

    :goto_0
    move-object v11, v0

    .line 163
    iget-object v6, p0, Lee/cyber/smartid/tse/KeyGenerationManager$2;->d:Lee/cyber/smartid/tse/KeyGenerationManager;

    iget-object v7, p0, Lee/cyber/smartid/tse/KeyGenerationManager$2;->b:Lee/cyber/smartid/tse/inter/WallClock;

    iget-object v8, p0, Lee/cyber/smartid/tse/KeyGenerationManager$2;->c:Lee/cyber/smartid/tse/inter/ResourceAccess;

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-static/range {v6 .. v11}, Lee/cyber/smartid/tse/KeyGenerationManager;->a(Lee/cyber/smartid/tse/KeyGenerationManager;Lee/cyber/smartid/tse/inter/WallClock;Lee/cyber/smartid/tse/inter/ResourceAccess;[Ljava/lang/String;Ljava/lang/Exception;[Lee/cyber/smartid/cryptolib/dto/TestResult;)V

    return-void
.end method
