.class Lee/cyber/smartid/tse/KeyStateManager$15$1;
.super Ljava/lang/Object;
.source "KeyStateManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lee/cyber/smartid/tse/KeyStateManager$15;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lee/cyber/smartid/tse/dto/jsonrpc/resp/CheckLocalPendingStateResp;

.field final synthetic b:Lee/cyber/smartid/tse/KeyStateManager$15;


# direct methods
.method constructor <init>(Lee/cyber/smartid/tse/KeyStateManager$15;Lee/cyber/smartid/tse/dto/jsonrpc/resp/CheckLocalPendingStateResp;)V
    .locals 0

    .line 1285
    iput-object p1, p0, Lee/cyber/smartid/tse/KeyStateManager$15$1;->b:Lee/cyber/smartid/tse/KeyStateManager$15;

    iput-object p2, p0, Lee/cyber/smartid/tse/KeyStateManager$15$1;->a:Lee/cyber/smartid/tse/dto/jsonrpc/resp/CheckLocalPendingStateResp;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .line 1288
    iget-object v0, p0, Lee/cyber/smartid/tse/KeyStateManager$15$1;->b:Lee/cyber/smartid/tse/KeyStateManager$15;

    iget-object v0, v0, Lee/cyber/smartid/tse/KeyStateManager$15;->c:Lee/cyber/smartid/tse/KeyStateManager;

    invoke-static {v0}, Lee/cyber/smartid/tse/KeyStateManager;->f(Lee/cyber/smartid/tse/KeyStateManager;)Lee/cyber/smartid/tse/inter/ListenerAccess;

    move-result-object v0

    iget-object v1, p0, Lee/cyber/smartid/tse/KeyStateManager$15$1;->b:Lee/cyber/smartid/tse/KeyStateManager$15;

    iget-object v1, v1, Lee/cyber/smartid/tse/KeyStateManager$15;->a:Ljava/lang/String;

    const-class v2, Lee/cyber/smartid/tse/inter/CheckLocalPendingStateListener;

    const/4 v3, 0x1

    invoke-interface {v0, v1, v3, v2}, Lee/cyber/smartid/tse/inter/ListenerAccess;->getListener(Ljava/lang/String;ZLjava/lang/Class;)Lee/cyber/smartid/tse/inter/TSEListener;

    move-result-object v0

    check-cast v0, Lee/cyber/smartid/tse/inter/CheckLocalPendingStateListener;

    if-eqz v0, :cond_0

    .line 1291
    iget-object v1, p0, Lee/cyber/smartid/tse/KeyStateManager$15$1;->b:Lee/cyber/smartid/tse/KeyStateManager$15;

    iget-object v1, v1, Lee/cyber/smartid/tse/KeyStateManager$15;->a:Ljava/lang/String;

    iget-object v2, p0, Lee/cyber/smartid/tse/KeyStateManager$15$1;->a:Lee/cyber/smartid/tse/dto/jsonrpc/resp/CheckLocalPendingStateResp;

    invoke-interface {v0, v1, v2}, Lee/cyber/smartid/tse/inter/CheckLocalPendingStateListener;->onCheckLocalPendingStateSuccess(Ljava/lang/String;Lee/cyber/smartid/tse/dto/jsonrpc/resp/CheckLocalPendingStateResp;)V

    :cond_0
    return-void
.end method
