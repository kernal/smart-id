.class Lee/cyber/smartid/tse/SmartIdTSE$1;
.super Ljava/lang/Object;
.source "SmartIdTSE.java"

# interfaces
.implements Lee/cyber/smartid/tse/inter/StateWorkerJobListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lee/cyber/smartid/tse/SmartIdTSE;->initializeTSE()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lee/cyber/smartid/tse/SmartIdTSE;


# direct methods
.method constructor <init>(Lee/cyber/smartid/tse/SmartIdTSE;)V
    .locals 0

    .line 879
    iput-object p1, p0, Lee/cyber/smartid/tse/SmartIdTSE$1;->a:Lee/cyber/smartid/tse/SmartIdTSE;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onJobCompleted(Ljava/lang/String;)V
    .locals 3

    .line 897
    iget-object v0, p0, Lee/cyber/smartid/tse/SmartIdTSE$1;->a:Lee/cyber/smartid/tse/SmartIdTSE;

    invoke-static {v0}, Lee/cyber/smartid/tse/SmartIdTSE;->k(Lee/cyber/smartid/tse/SmartIdTSE;)Lee/cyber/smartid/tse/inter/LogAccess;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "KeyStateManager onJobCompleted: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, p1}, Lee/cyber/smartid/tse/inter/LogAccess;->d(Ljava/lang/String;)V

    return-void
.end method

.method public onJobDequeued(Ljava/lang/String;)V
    .locals 3

    .line 887
    iget-object v0, p0, Lee/cyber/smartid/tse/SmartIdTSE$1;->a:Lee/cyber/smartid/tse/SmartIdTSE;

    invoke-static {v0}, Lee/cyber/smartid/tse/SmartIdTSE;->k(Lee/cyber/smartid/tse/SmartIdTSE;)Lee/cyber/smartid/tse/inter/LogAccess;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "KeyStateManager onJobDequeued: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, p1}, Lee/cyber/smartid/tse/inter/LogAccess;->d(Ljava/lang/String;)V

    return-void
.end method

.method public onJobQueued(Ljava/lang/String;)V
    .locals 3

    .line 882
    iget-object v0, p0, Lee/cyber/smartid/tse/SmartIdTSE$1;->a:Lee/cyber/smartid/tse/SmartIdTSE;

    invoke-static {v0}, Lee/cyber/smartid/tse/SmartIdTSE;->k(Lee/cyber/smartid/tse/SmartIdTSE;)Lee/cyber/smartid/tse/inter/LogAccess;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "KeyStateManager onJobQueued: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, p1}, Lee/cyber/smartid/tse/inter/LogAccess;->d(Ljava/lang/String;)V

    return-void
.end method

.method public onJobStarted(Ljava/lang/String;)V
    .locals 3

    .line 892
    iget-object v0, p0, Lee/cyber/smartid/tse/SmartIdTSE$1;->a:Lee/cyber/smartid/tse/SmartIdTSE;

    invoke-static {v0}, Lee/cyber/smartid/tse/SmartIdTSE;->k(Lee/cyber/smartid/tse/SmartIdTSE;)Lee/cyber/smartid/tse/inter/LogAccess;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "KeyStateManager onJobStarted: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, p1}, Lee/cyber/smartid/tse/inter/LogAccess;->d(Ljava/lang/String;)V

    return-void
.end method
