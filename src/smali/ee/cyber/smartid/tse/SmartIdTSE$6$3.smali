.class Lee/cyber/smartid/tse/SmartIdTSE$6$3;
.super Ljava/lang/Object;
.source "SmartIdTSE.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lee/cyber/smartid/tse/SmartIdTSE$6;->onFailure(Lc/b;Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCError;Ljava/lang/Throwable;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCError;

.field final synthetic b:Ljava/lang/Throwable;

.field final synthetic c:Lee/cyber/smartid/tse/SmartIdTSE$6;


# direct methods
.method constructor <init>(Lee/cyber/smartid/tse/SmartIdTSE$6;Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCError;Ljava/lang/Throwable;)V
    .locals 0

    .line 2364
    iput-object p1, p0, Lee/cyber/smartid/tse/SmartIdTSE$6$3;->c:Lee/cyber/smartid/tse/SmartIdTSE$6;

    iput-object p2, p0, Lee/cyber/smartid/tse/SmartIdTSE$6$3;->a:Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCError;

    iput-object p3, p0, Lee/cyber/smartid/tse/SmartIdTSE$6$3;->b:Ljava/lang/Throwable;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .line 2367
    iget-object v0, p0, Lee/cyber/smartid/tse/SmartIdTSE$6$3;->c:Lee/cyber/smartid/tse/SmartIdTSE$6;

    iget-object v0, v0, Lee/cyber/smartid/tse/SmartIdTSE$6;->c:Lee/cyber/smartid/tse/SmartIdTSE;

    iget-object v1, p0, Lee/cyber/smartid/tse/SmartIdTSE$6$3;->c:Lee/cyber/smartid/tse/SmartIdTSE$6;

    iget-object v1, v1, Lee/cyber/smartid/tse/SmartIdTSE$6;->b:Ljava/lang/String;

    iget-object v2, p0, Lee/cyber/smartid/tse/SmartIdTSE$6$3;->c:Lee/cyber/smartid/tse/SmartIdTSE$6;

    iget-object v2, v2, Lee/cyber/smartid/tse/SmartIdTSE$6;->c:Lee/cyber/smartid/tse/SmartIdTSE;

    iget-object v3, p0, Lee/cyber/smartid/tse/SmartIdTSE$6$3;->c:Lee/cyber/smartid/tse/SmartIdTSE$6;

    iget-object v3, v3, Lee/cyber/smartid/tse/SmartIdTSE$6;->b:Ljava/lang/String;

    const-class v4, Lee/cyber/smartid/tse/inter/NewFreshnessTokenListener;

    const/4 v5, 0x1

    invoke-static {v2, v3, v5, v4}, Lee/cyber/smartid/tse/SmartIdTSE;->a(Lee/cyber/smartid/tse/SmartIdTSE;Ljava/lang/String;ZLjava/lang/Class;)Lee/cyber/smartid/tse/inter/TSEListener;

    move-result-object v2

    iget-object v3, p0, Lee/cyber/smartid/tse/SmartIdTSE$6$3;->c:Lee/cyber/smartid/tse/SmartIdTSE$6;

    iget-object v3, v3, Lee/cyber/smartid/tse/SmartIdTSE$6;->c:Lee/cyber/smartid/tse/SmartIdTSE;

    invoke-static {v3}, Lee/cyber/smartid/tse/SmartIdTSE;->b(Lee/cyber/smartid/tse/SmartIdTSE;)Landroid/content/Context;

    move-result-object v3

    iget-object v4, p0, Lee/cyber/smartid/tse/SmartIdTSE$6$3;->c:Lee/cyber/smartid/tse/SmartIdTSE$6;

    iget-object v4, v4, Lee/cyber/smartid/tse/SmartIdTSE$6;->c:Lee/cyber/smartid/tse/SmartIdTSE;

    invoke-static {v4}, Lee/cyber/smartid/tse/SmartIdTSE;->e(Lee/cyber/smartid/tse/SmartIdTSE;)Lee/cyber/smartid/tse/inter/WallClock;

    move-result-object v4

    iget-object v5, p0, Lee/cyber/smartid/tse/SmartIdTSE$6$3;->a:Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCError;

    iget-object v6, p0, Lee/cyber/smartid/tse/SmartIdTSE$6$3;->b:Ljava/lang/Throwable;

    invoke-static {v3, v4, v5, v6}, Lee/cyber/smartid/tse/dto/TSEError;->from(Landroid/content/Context;Lee/cyber/smartid/tse/inter/WallClock;Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCError;Ljava/lang/Throwable;)Lee/cyber/smartid/tse/dto/TSEError;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, Lee/cyber/smartid/tse/SmartIdTSE;->a(Lee/cyber/smartid/tse/SmartIdTSE;Ljava/lang/String;Lee/cyber/smartid/tse/inter/TSEListener;Lee/cyber/smartid/tse/dto/TSEError;)V

    return-void
.end method
