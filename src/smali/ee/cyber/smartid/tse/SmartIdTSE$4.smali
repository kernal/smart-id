.class Lee/cyber/smartid/tse/SmartIdTSE$4;
.super Ljava/lang/Object;
.source "SmartIdTSE.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lee/cyber/smartid/tse/SmartIdTSE;->a(Lee/cyber/smartid/tse/dto/TSEError;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lee/cyber/smartid/tse/dto/TSEError;

.field final synthetic b:Lee/cyber/smartid/tse/SmartIdTSE;


# direct methods
.method constructor <init>(Lee/cyber/smartid/tse/SmartIdTSE;Lee/cyber/smartid/tse/dto/TSEError;)V
    .locals 0

    .line 1481
    iput-object p1, p0, Lee/cyber/smartid/tse/SmartIdTSE$4;->b:Lee/cyber/smartid/tse/SmartIdTSE;

    iput-object p2, p0, Lee/cyber/smartid/tse/SmartIdTSE$4;->a:Lee/cyber/smartid/tse/dto/TSEError;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .line 1485
    iget-object v0, p0, Lee/cyber/smartid/tse/SmartIdTSE$4;->a:Lee/cyber/smartid/tse/dto/TSEError;

    if-nez v0, :cond_0

    .line 1486
    iget-object v0, p0, Lee/cyber/smartid/tse/SmartIdTSE$4;->b:Lee/cyber/smartid/tse/SmartIdTSE;

    invoke-static {v0}, Lee/cyber/smartid/tse/SmartIdTSE;->k(Lee/cyber/smartid/tse/SmartIdTSE;)Lee/cyber/smartid/tse/inter/LogAccess;

    move-result-object v0

    const-string v1, "notifySystemEventsListeners  - error is null, aborting .."

    invoke-interface {v0, v1}, Lee/cyber/smartid/tse/inter/LogAccess;->e(Ljava/lang/String;)V

    return-void

    .line 1489
    :cond_0
    iget-object v0, p0, Lee/cyber/smartid/tse/SmartIdTSE$4;->b:Lee/cyber/smartid/tse/SmartIdTSE;

    invoke-static {v0}, Lee/cyber/smartid/tse/SmartIdTSE;->k(Lee/cyber/smartid/tse/SmartIdTSE;)Lee/cyber/smartid/tse/inter/LogAccess;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "notifySystemEventsListeners called, error: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lee/cyber/smartid/tse/SmartIdTSE$4;->a:Lee/cyber/smartid/tse/dto/TSEError;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lee/cyber/smartid/tse/inter/LogAccess;->d(Ljava/lang/String;)V

    .line 1490
    iget-object v0, p0, Lee/cyber/smartid/tse/SmartIdTSE$4;->b:Lee/cyber/smartid/tse/SmartIdTSE;

    invoke-static {v0}, Lee/cyber/smartid/tse/SmartIdTSE;->l(Lee/cyber/smartid/tse/SmartIdTSE;)Ljava/util/WeakHashMap;

    move-result-object v0

    monitor-enter v0

    .line 1491
    :try_start_0
    iget-object v1, p0, Lee/cyber/smartid/tse/SmartIdTSE$4;->b:Lee/cyber/smartid/tse/SmartIdTSE;

    invoke-static {v1}, Lee/cyber/smartid/tse/SmartIdTSE;->l(Lee/cyber/smartid/tse/SmartIdTSE;)Ljava/util/WeakHashMap;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/WeakHashMap;->size()I

    move-result v1

    if-lez v1, :cond_2

    .line 1492
    iget-object v1, p0, Lee/cyber/smartid/tse/SmartIdTSE$4;->b:Lee/cyber/smartid/tse/SmartIdTSE;

    invoke-static {v1}, Lee/cyber/smartid/tse/SmartIdTSE;->l(Lee/cyber/smartid/tse/SmartIdTSE;)Ljava/util/WeakHashMap;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/WeakHashMap;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 1493
    iget-object v3, p0, Lee/cyber/smartid/tse/SmartIdTSE$4;->b:Lee/cyber/smartid/tse/SmartIdTSE;

    invoke-static {v3}, Lee/cyber/smartid/tse/SmartIdTSE;->l(Lee/cyber/smartid/tse/SmartIdTSE;)Ljava/util/WeakHashMap;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/util/WeakHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lee/cyber/smartid/tse/inter/TSEListener;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1495
    :try_start_1
    instance-of v4, v3, Lee/cyber/smartid/tse/inter/SystemEventListener;

    if-eqz v4, :cond_1

    .line 1496
    iget-object v4, p0, Lee/cyber/smartid/tse/SmartIdTSE$4;->b:Lee/cyber/smartid/tse/SmartIdTSE;

    invoke-static {v4}, Lee/cyber/smartid/tse/SmartIdTSE;->k(Lee/cyber/smartid/tse/SmartIdTSE;)Lee/cyber/smartid/tse/inter/LogAccess;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "notifySystemEventsListeners - notifying listener "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Lee/cyber/smartid/tse/inter/LogAccess;->d(Ljava/lang/String;)V

    .line 1497
    check-cast v3, Lee/cyber/smartid/tse/inter/SystemEventListener;

    iget-object v4, p0, Lee/cyber/smartid/tse/SmartIdTSE$4;->a:Lee/cyber/smartid/tse/dto/TSEError;

    invoke-interface {v3, v2, v4}, Lee/cyber/smartid/tse/inter/SystemEventListener;->onSystemErrorEvent(Ljava/lang/String;Lee/cyber/smartid/tse/dto/TSEError;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v2

    .line 1500
    :try_start_2
    iget-object v3, p0, Lee/cyber/smartid/tse/SmartIdTSE$4;->b:Lee/cyber/smartid/tse/SmartIdTSE;

    invoke-static {v3}, Lee/cyber/smartid/tse/SmartIdTSE;->k(Lee/cyber/smartid/tse/SmartIdTSE;)Lee/cyber/smartid/tse/inter/LogAccess;

    move-result-object v3

    const-string v4, "notifySystemEventsListeners"

    invoke-interface {v3, v4, v2}, Lee/cyber/smartid/tse/inter/LogAccess;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 1504
    :cond_2
    monitor-exit v0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1
.end method
