.class Lee/cyber/smartid/tse/KeyManager$13$4;
.super Ljava/lang/Object;
.source "KeyManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lee/cyber/smartid/tse/KeyManager$13;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lee/cyber/smartid/tse/KeyManager$13;


# direct methods
.method constructor <init>(Lee/cyber/smartid/tse/KeyManager$13;)V
    .locals 0

    .line 974
    iput-object p1, p0, Lee/cyber/smartid/tse/KeyManager$13$4;->a:Lee/cyber/smartid/tse/KeyManager$13;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .line 977
    iget-object v0, p0, Lee/cyber/smartid/tse/KeyManager$13$4;->a:Lee/cyber/smartid/tse/KeyManager$13;

    iget-object v0, v0, Lee/cyber/smartid/tse/KeyManager$13;->d:Lee/cyber/smartid/tse/KeyManager;

    invoke-static {v0}, Lee/cyber/smartid/tse/KeyManager;->a(Lee/cyber/smartid/tse/KeyManager;)Lee/cyber/smartid/tse/inter/ListenerAccess;

    move-result-object v0

    iget-object v1, p0, Lee/cyber/smartid/tse/KeyManager$13$4;->a:Lee/cyber/smartid/tse/KeyManager$13;

    iget-object v1, v1, Lee/cyber/smartid/tse/KeyManager$13;->b:Ljava/lang/String;

    iget-object v2, p0, Lee/cyber/smartid/tse/KeyManager$13$4;->a:Lee/cyber/smartid/tse/KeyManager$13;

    iget-object v2, v2, Lee/cyber/smartid/tse/KeyManager$13;->d:Lee/cyber/smartid/tse/KeyManager;

    invoke-static {v2}, Lee/cyber/smartid/tse/KeyManager;->a(Lee/cyber/smartid/tse/KeyManager;)Lee/cyber/smartid/tse/inter/ListenerAccess;

    move-result-object v2

    iget-object v3, p0, Lee/cyber/smartid/tse/KeyManager$13$4;->a:Lee/cyber/smartid/tse/KeyManager$13;

    iget-object v3, v3, Lee/cyber/smartid/tse/KeyManager$13;->b:Ljava/lang/String;

    const-class v4, Lee/cyber/smartid/tse/inter/ValidateKeyCreationResponseListener;

    const/4 v5, 0x1

    invoke-interface {v2, v3, v5, v4}, Lee/cyber/smartid/tse/inter/ListenerAccess;->getListener(Ljava/lang/String;ZLjava/lang/Class;)Lee/cyber/smartid/tse/inter/TSEListener;

    move-result-object v2

    iget-object v3, p0, Lee/cyber/smartid/tse/KeyManager$13$4;->a:Lee/cyber/smartid/tse/KeyManager$13;

    iget-object v3, v3, Lee/cyber/smartid/tse/KeyManager$13;->d:Lee/cyber/smartid/tse/KeyManager;

    invoke-static {v3}, Lee/cyber/smartid/tse/KeyManager;->d(Lee/cyber/smartid/tse/KeyManager;)Lee/cyber/smartid/tse/inter/WallClock;

    move-result-object v3

    iget-object v4, p0, Lee/cyber/smartid/tse/KeyManager$13$4;->a:Lee/cyber/smartid/tse/KeyManager$13;

    iget-object v4, v4, Lee/cyber/smartid/tse/KeyManager$13;->d:Lee/cyber/smartid/tse/KeyManager;

    invoke-static {v4}, Lee/cyber/smartid/tse/KeyManager;->c(Lee/cyber/smartid/tse/KeyManager;)Lee/cyber/smartid/tse/inter/ResourceAccess;

    move-result-object v4

    invoke-interface {v4}, Lee/cyber/smartid/tse/inter/ResourceAccess;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    sget v5, Lee/cyber/smartid/tse/R$string;->err_no_such_keys_found_regenerate_keys_2:I

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    const-wide/16 v5, 0x3f3

    invoke-static {v3, v5, v6, v4}, Lee/cyber/smartid/tse/dto/TSEError;->from(Lee/cyber/smartid/tse/inter/WallClock;JLjava/lang/String;)Lee/cyber/smartid/tse/dto/TSEError;

    move-result-object v3

    invoke-interface {v0, v1, v2, v3}, Lee/cyber/smartid/tse/inter/ListenerAccess;->notifyError(Ljava/lang/String;Lee/cyber/smartid/tse/inter/TSEListener;Lee/cyber/smartid/tse/dto/TSEError;)V

    return-void
.end method
