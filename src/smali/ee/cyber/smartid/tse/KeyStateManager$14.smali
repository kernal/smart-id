.class Lee/cyber/smartid/tse/KeyStateManager$14;
.super Ljava/lang/Object;
.source "KeyStateManager.java"

# interfaces
.implements Lee/cyber/smartid/tse/inter/SubmitClientSecondPartListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lee/cyber/smartid/tse/KeyStateManager;->a(Ljava/lang/String;Lee/cyber/smartid/tse/inter/TSEListener;Ljava/lang/Class;)Lee/cyber/smartid/tse/inter/TSEListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lee/cyber/smartid/tse/inter/TSEListener;

.field final synthetic b:Ljava/lang/String;

.field final synthetic c:Lee/cyber/smartid/tse/KeyStateManager;


# direct methods
.method constructor <init>(Lee/cyber/smartid/tse/KeyStateManager;Lee/cyber/smartid/tse/inter/TSEListener;Ljava/lang/String;)V
    .locals 0

    .line 1117
    iput-object p1, p0, Lee/cyber/smartid/tse/KeyStateManager$14;->c:Lee/cyber/smartid/tse/KeyStateManager;

    iput-object p2, p0, Lee/cyber/smartid/tse/KeyStateManager$14;->a:Lee/cyber/smartid/tse/inter/TSEListener;

    iput-object p3, p0, Lee/cyber/smartid/tse/KeyStateManager$14;->b:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onSubmitClientSecondPartFailed(Ljava/lang/String;Lee/cyber/smartid/tse/dto/TSEError;)V
    .locals 3

    .line 1131
    :try_start_0
    iget-object v0, p0, Lee/cyber/smartid/tse/KeyStateManager$14;->a:Lee/cyber/smartid/tse/inter/TSEListener;

    check-cast v0, Lee/cyber/smartid/tse/inter/SubmitClientSecondPartListener;

    invoke-interface {v0, p1, p2}, Lee/cyber/smartid/tse/inter/SubmitClientSecondPartListener;->onSubmitClientSecondPartFailed(Ljava/lang/String;Lee/cyber/smartid/tse/dto/TSEError;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 1133
    iget-object v1, p0, Lee/cyber/smartid/tse/KeyStateManager$14;->c:Lee/cyber/smartid/tse/KeyStateManager;

    invoke-static {v1}, Lee/cyber/smartid/tse/KeyStateManager;->b(Lee/cyber/smartid/tse/KeyStateManager;)Lee/cyber/smartid/tse/inter/LogAccess;

    move-result-object v1

    const-string v2, "chainSystemListenerIfNeeded"

    invoke-interface {v1, v2, v0}, Lee/cyber/smartid/tse/inter/LogAccess;->d(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1135
    :goto_0
    iget-object v0, p0, Lee/cyber/smartid/tse/KeyStateManager$14;->c:Lee/cyber/smartid/tse/KeyStateManager;

    invoke-static {v0}, Lee/cyber/smartid/tse/KeyStateManager;->l(Lee/cyber/smartid/tse/KeyStateManager;)Lee/cyber/smartid/tse/inter/ExternalResourceAccess;

    move-result-object v0

    iget-object v1, p0, Lee/cyber/smartid/tse/KeyStateManager$14;->b:Ljava/lang/String;

    invoke-virtual {p2, v1}, Lee/cyber/smartid/tse/dto/TSEError;->withKeyId(Ljava/lang/String;)Lee/cyber/smartid/tse/dto/TSEError;

    move-result-object p2

    invoke-interface {v0, p1, p2}, Lee/cyber/smartid/tse/inter/ExternalResourceAccess;->onSubmitClientSecondPartFailed(Ljava/lang/String;Lee/cyber/smartid/tse/dto/TSEError;)V

    return-void
.end method

.method public onSubmitClientSecondPartSuccess(Ljava/lang/String;Lee/cyber/smartid/tse/dto/jsonrpc/resp/SubmitClientSecondPartResp;)V
    .locals 3

    .line 1121
    :try_start_0
    iget-object v0, p0, Lee/cyber/smartid/tse/KeyStateManager$14;->a:Lee/cyber/smartid/tse/inter/TSEListener;

    check-cast v0, Lee/cyber/smartid/tse/inter/SubmitClientSecondPartListener;

    invoke-interface {v0, p1, p2}, Lee/cyber/smartid/tse/inter/SubmitClientSecondPartListener;->onSubmitClientSecondPartSuccess(Ljava/lang/String;Lee/cyber/smartid/tse/dto/jsonrpc/resp/SubmitClientSecondPartResp;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 1123
    iget-object v1, p0, Lee/cyber/smartid/tse/KeyStateManager$14;->c:Lee/cyber/smartid/tse/KeyStateManager;

    invoke-static {v1}, Lee/cyber/smartid/tse/KeyStateManager;->b(Lee/cyber/smartid/tse/KeyStateManager;)Lee/cyber/smartid/tse/inter/LogAccess;

    move-result-object v1

    const-string v2, "chainSystemListenerIfNeeded"

    invoke-interface {v1, v2, v0}, Lee/cyber/smartid/tse/inter/LogAccess;->d(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1125
    :goto_0
    iget-object v0, p0, Lee/cyber/smartid/tse/KeyStateManager$14;->c:Lee/cyber/smartid/tse/KeyStateManager;

    invoke-static {v0}, Lee/cyber/smartid/tse/KeyStateManager;->l(Lee/cyber/smartid/tse/KeyStateManager;)Lee/cyber/smartid/tse/inter/ExternalResourceAccess;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lee/cyber/smartid/tse/inter/ExternalResourceAccess;->onSubmitClientSecondPartSuccess(Ljava/lang/String;Lee/cyber/smartid/tse/dto/jsonrpc/resp/SubmitClientSecondPartResp;)V

    return-void
.end method
