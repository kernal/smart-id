.class public Lee/cyber/smartid/tse/util/Util;
.super Ljava/lang/Object;
.source "Util.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static a(Landroid/os/Bundle;Ljava/util/concurrent/atomic/AtomicInteger;)Ljava/lang/String;
    .locals 2

    if-eqz p0, :cond_0

    const-string v0, "ee.cyber.smartid.tse.EXTRA_X_SPLIT_KEY_TRIGGER"

    .line 365
    invoke-virtual {p0, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 366
    invoke-virtual {p0, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    :cond_0
    const-string p0, "external"

    :goto_0
    if-eqz p1, :cond_1

    .line 369
    invoke-virtual {p1}, Ljava/util/concurrent/atomic/AtomicInteger;->intValue()I

    move-result p1

    if-lez p1, :cond_1

    const-string p0, "internal-blocking-retry"

    :cond_1
    return-object p0
.end method

.method public static acquireWakeLock(Landroid/os/PowerManager$WakeLock;J)V
    .locals 3

    if-nez p0, :cond_0

    .line 55
    const-class p0, Lee/cyber/smartid/tse/util/Util;

    invoke-static {p0}, Lee/cyber/smartid/tse/util/Log;->getInstance(Ljava/lang/Class;)Lee/cyber/smartid/tse/util/Log;

    move-result-object p0

    const-string p1, "wakeLock: trying to take a NULL wakelock!"

    invoke-virtual {p0, p1}, Lee/cyber/smartid/tse/util/Log;->w(Ljava/lang/String;)V

    return-void

    .line 58
    :cond_0
    const-class v0, Lee/cyber/smartid/tse/util/Util;

    invoke-static {v0}, Lee/cyber/smartid/tse/util/Log;->getInstance(Ljava/lang/Class;)Lee/cyber/smartid/tse/util/Log;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "acquireWakeLock (before): "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lee/cyber/smartid/tse/util/Log;->d(Ljava/lang/String;)V

    .line 59
    invoke-virtual {p0, p1, p2}, Landroid/os/PowerManager$WakeLock;->acquire(J)V

    .line 60
    const-class p1, Lee/cyber/smartid/tse/util/Util;

    invoke-static {p1}, Lee/cyber/smartid/tse/util/Log;->getInstance(Ljava/lang/Class;)Lee/cyber/smartid/tse/util/Log;

    move-result-object p1

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "acquireWakeLock (after): "

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {p1, p0}, Lee/cyber/smartid/tse/util/Log;->d(Ljava/lang/String;)V

    return-void
.end method

.method public static addXSplitKeyTriggerExtra(Landroid/os/Bundle;Ljava/lang/String;)Landroid/os/Bundle;
    .locals 1

    if-eqz p0, :cond_0

    const-string v0, "ee.cyber.smartid.tse.EXTRA_X_SPLIT_KEY_TRIGGER"

    .line 348
    invoke-virtual {p0, v0, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static closeClosable(Ljava/io/Closeable;)V
    .locals 0

    if-nez p0, :cond_0

    return-void

    .line 192
    :cond_0
    :try_start_0
    invoke-interface {p0}, Ljava/io/Closeable;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    return-void
.end method

.method public static contains(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1

    .line 276
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0, p1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result p0

    if-eqz p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method public static containsIgnoreCase(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1

    .line 287
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {p0, v0}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object p0

    sget-object v0, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {p1, v0}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result p0

    if-eqz p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method public static encryptToKTKEncryptedJWE(Lee/cyber/smartid/tse/dto/KTKPublicKey;Lee/cyber/smartid/cryptolib/inter/CryptoOp;Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;
        }
    .end annotation

    .line 302
    invoke-virtual {p0}, Lee/cyber/smartid/tse/dto/KTKPublicKey;->getPublicKey()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lee/cyber/smartid/tse/dto/KTKPublicKey;->getKeyId()Ljava/lang/String;

    move-result-object v3

    const-string v4, "RSA-OAEP"

    const-string v5, "A128CBC-HS256"

    const-string v6, "SERVER"

    move-object v0, p1

    move-object v1, p2

    move-object v7, p3

    move v8, p4

    invoke-interface/range {v0 .. v8}, Lee/cyber/smartid/cryptolib/inter/CryptoOp;->encryptToKTKEncryptedJWE(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static equals(Landroid/os/Bundle;Landroid/os/Bundle;)Z
    .locals 6

    const/4 v0, 0x0

    if-nez p0, :cond_0

    if-eqz p1, :cond_0

    return v0

    :cond_0
    if-eqz p0, :cond_1

    if-nez p1, :cond_1

    return v0

    :cond_1
    const/4 v1, 0x1

    if-nez p0, :cond_2

    return v1

    .line 391
    :cond_2
    invoke-virtual {p0}, Landroid/os/Bundle;->size()I

    move-result v2

    invoke-virtual {p1}, Landroid/os/Bundle;->size()I

    move-result v3

    if-eq v2, v3, :cond_3

    return v0

    .line 395
    :cond_3
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    .line 396
    invoke-virtual {p0}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 397
    invoke-virtual {p1}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 401
    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_4
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_a

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 402
    invoke-virtual {p0, v3}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_9

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_5

    goto :goto_0

    .line 405
    :cond_5
    invoke-virtual {p0, v3}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    .line 406
    invoke-virtual {p1, v3}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    .line 407
    instance-of v5, v4, Landroid/os/Bundle;

    if-eqz v5, :cond_6

    instance-of v5, v3, Landroid/os/Bundle;

    if-eqz v5, :cond_6

    .line 408
    check-cast v4, Landroid/os/Bundle;

    check-cast v3, Landroid/os/Bundle;

    invoke-static {v4, v3}, Lee/cyber/smartid/tse/util/Util;->equals(Landroid/os/Bundle;Landroid/os/Bundle;)Z

    move-result v3

    if-nez v3, :cond_4

    return v0

    :cond_6
    if-nez v4, :cond_7

    if-eqz v3, :cond_7

    return v0

    :cond_7
    if-eqz v4, :cond_8

    if-nez v3, :cond_8

    return v0

    :cond_8
    if-eqz v4, :cond_4

    .line 415
    invoke-virtual {v4, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_4

    :cond_9
    :goto_0
    return v0

    :cond_a
    return v1
.end method

.method public static generateUUID()Ljava/lang/String;
    .locals 1

    .line 204
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getApkDigestSha256(Landroid/content/Context;Lee/cyber/smartid/cryptolib/CryptoLib;)Ljava/lang/String;
    .locals 2

    const/4 v0, 0x0

    if-eqz p0, :cond_2

    if-nez p1, :cond_0

    goto :goto_0

    .line 254
    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageCodePath()Ljava/lang/String;

    move-result-object p0

    .line 255
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    return-object v0

    .line 261
    :cond_1
    :try_start_0
    new-instance v1, Ljava/io/FileInputStream;

    invoke-direct {v1, p0}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    const-string p0, "SHA-256"

    invoke-virtual {p1, v1, p0}, Lee/cyber/smartid/cryptolib/CryptoLib;->digestAndEncodeToBase64(Ljava/io/InputStream;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p0

    .line 263
    const-class p1, Lee/cyber/smartid/tse/util/Util;

    invoke-static {p1}, Lee/cyber/smartid/tse/util/Log;->getInstance(Ljava/lang/Class;)Lee/cyber/smartid/tse/util/Log;

    move-result-object p1

    const-string v1, "getApkDigestSha256"

    invoke-virtual {p1, v1, p0}, Lee/cyber/smartid/tse/util/Log;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_2
    :goto_0
    return-object v0
.end method

.method public static getErrorResponseIfAny(Lokhttp3/ac;)Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCResponse;
    .locals 4

    const-string v0, "getErrorResponseIfAny"

    const/4 v1, 0x0

    if-nez p0, :cond_0

    return-object v1

    .line 219
    :cond_0
    :try_start_0
    invoke-virtual {p0}, Lokhttp3/ac;->e()Ljava/lang/String;

    move-result-object p0
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p0

    .line 221
    const-class v2, Lee/cyber/smartid/tse/util/Util;

    invoke-static {v2}, Lee/cyber/smartid/tse/util/Log;->getInstance(Ljava/lang/Class;)Lee/cyber/smartid/tse/util/Log;

    move-result-object v2

    invoke-virtual {v2, v0, p0}, Lee/cyber/smartid/tse/util/Log;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object p0, v1

    .line 224
    :goto_0
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    return-object v1

    .line 231
    :cond_1
    :try_start_1
    invoke-static {}, Lee/cyber/smartid/tse/network/TSEAPI;->createGSONBuilderForAPIResponses()Lcom/google/gson/g;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/gson/g;->a()Lcom/google/gson/f;

    move-result-object v2

    const-class v3, Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCResponse;

    invoke-virtual {v2, p0, v3}, Lcom/google/gson/f;->a(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCResponse;
    :try_end_1
    .catch Lcom/google/gson/u; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_2

    :catch_1
    move-exception p0

    .line 235
    const-class v2, Lee/cyber/smartid/tse/util/Util;

    invoke-static {v2}, Lee/cyber/smartid/tse/util/Log;->getInstance(Ljava/lang/Class;)Lee/cyber/smartid/tse/util/Log;

    move-result-object v2

    invoke-virtual {v2, v0, p0}, Lee/cyber/smartid/tse/util/Log;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    :catch_2
    move-exception p0

    .line 233
    const-class v2, Lee/cyber/smartid/tse/util/Util;

    invoke-static {v2}, Lee/cyber/smartid/tse/util/Log;->getInstance(Ljava/lang/Class;)Lee/cyber/smartid/tse/util/Log;

    move-result-object v2

    invoke-virtual {v2, v0, p0}, Lee/cyber/smartid/tse/util/Log;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    :goto_1
    move-object p0, v1

    :goto_2
    return-object p0
.end method

.method public static getKeyLockRemainingDuration(Lee/cyber/smartid/tse/dto/jsonrpc/AccountKeyStatus;JJ)J
    .locals 7

    .line 144
    invoke-static {p0}, Lee/cyber/smartid/tse/util/Util;->isKeyLockedPermanently(Lee/cyber/smartid/tse/dto/jsonrpc/AccountKeyStatus;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-wide/16 p0, -0x1

    return-wide p0

    .line 146
    :cond_0
    invoke-static {p0, p1, p2, p3, p4}, Lee/cyber/smartid/tse/util/Util;->isKeyLocked(Lee/cyber/smartid/tse/dto/jsonrpc/AccountKeyStatus;JJ)Z

    move-result v0

    const-wide/16 v1, 0x0

    if-nez v0, :cond_1

    return-wide v1

    .line 148
    :cond_1
    invoke-virtual {p0}, Lee/cyber/smartid/tse/dto/jsonrpc/AccountKeyStatus;->getLockInfo()Lee/cyber/smartid/tse/dto/jsonrpc/AccountKeyLockInfo;

    move-result-object v0

    if-nez v0, :cond_2

    return-wide v1

    .line 151
    :cond_2
    invoke-virtual {p0}, Lee/cyber/smartid/tse/dto/jsonrpc/AccountKeyStatus;->getLockInfo()Lee/cyber/smartid/tse/dto/jsonrpc/AccountKeyLockInfo;

    move-result-object p0

    invoke-virtual {p0}, Lee/cyber/smartid/tse/dto/jsonrpc/AccountKeyLockInfo;->getLockDurationSec()I

    move-result p0

    int-to-long v3, p0

    const-wide/16 v5, 0x3e8

    mul-long v3, v3, v5

    add-long/2addr p1, v3

    const-wide/16 v3, 0x1

    add-long/2addr p1, v3

    sub-long/2addr p1, p3

    .line 152
    invoke-static {v1, v2, p1, p2}, Ljava/lang/Math;->max(JJ)J

    move-result-wide p0

    return-wide p0
.end method

.method public static isHTTPSUrl(Ljava/lang/String;)Z
    .locals 1

    .line 312
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {p0, v0}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object p0

    const-string v0, "https://"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result p0

    if-eqz p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method public static isKeyLocked(Lee/cyber/smartid/tse/dto/jsonrpc/AccountKeyStatus;JJ)Z
    .locals 7

    const/4 v0, 0x0

    if-nez p0, :cond_0

    return v0

    .line 124
    :cond_0
    invoke-static {p0}, Lee/cyber/smartid/tse/util/Util;->isKeyLockedPermanently(Lee/cyber/smartid/tse/dto/jsonrpc/AccountKeyStatus;)Z

    move-result v1

    const/4 v2, 0x1

    if-eqz v1, :cond_1

    return v2

    .line 126
    :cond_1
    invoke-virtual {p0}, Lee/cyber/smartid/tse/dto/jsonrpc/AccountKeyStatus;->getStatus()Ljava/lang/String;

    move-result-object v1

    const-string v3, "LOCKED"

    invoke-static {v1, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    const-wide/16 v3, 0x0

    cmp-long v1, p1, v3

    if-ltz v1, :cond_2

    invoke-virtual {p0}, Lee/cyber/smartid/tse/dto/jsonrpc/AccountKeyStatus;->getLockInfo()Lee/cyber/smartid/tse/dto/jsonrpc/AccountKeyLockInfo;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {p0}, Lee/cyber/smartid/tse/dto/jsonrpc/AccountKeyStatus;->getLockInfo()Lee/cyber/smartid/tse/dto/jsonrpc/AccountKeyLockInfo;

    move-result-object p0

    invoke-virtual {p0}, Lee/cyber/smartid/tse/dto/jsonrpc/AccountKeyLockInfo;->getLockDurationSec()I

    move-result p0

    int-to-long v3, p0

    const-wide/16 v5, 0x3e8

    mul-long v3, v3, v5

    add-long/2addr p1, v3

    cmp-long p0, p1, p3

    if-ltz p0, :cond_2

    return v2

    :cond_2
    return v0
.end method

.method public static isKeyLockedPermanently(Lee/cyber/smartid/tse/dto/jsonrpc/AccountKeyStatus;)Z
    .locals 4

    const/4 v0, 0x0

    if-nez p0, :cond_0

    return v0

    .line 97
    :cond_0
    invoke-virtual {p0}, Lee/cyber/smartid/tse/dto/jsonrpc/AccountKeyStatus;->getStatus()Ljava/lang/String;

    move-result-object v1

    const-string v2, "REVOKED"

    invoke-static {v1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    const/4 v2, 0x1

    if-nez v1, :cond_3

    invoke-virtual {p0}, Lee/cyber/smartid/tse/dto/jsonrpc/AccountKeyStatus;->getStatus()Ljava/lang/String;

    move-result-object v1

    const-string v3, "EXPIRED"

    invoke-static {v1, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    goto :goto_0

    .line 101
    :cond_1
    invoke-virtual {p0}, Lee/cyber/smartid/tse/dto/jsonrpc/AccountKeyStatus;->getStatus()Ljava/lang/String;

    move-result-object v1

    const-string v3, "LOCKED"

    invoke-static {v1, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {p0}, Lee/cyber/smartid/tse/dto/jsonrpc/AccountKeyStatus;->getLockInfo()Lee/cyber/smartid/tse/dto/jsonrpc/AccountKeyLockInfo;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {p0}, Lee/cyber/smartid/tse/dto/jsonrpc/AccountKeyStatus;->getLockInfo()Lee/cyber/smartid/tse/dto/jsonrpc/AccountKeyLockInfo;

    move-result-object p0

    invoke-virtual {p0}, Lee/cyber/smartid/tse/dto/jsonrpc/AccountKeyLockInfo;->getPinAttemptsLeftInTotal()I

    move-result p0

    if-gtz p0, :cond_2

    return v2

    :cond_2
    return v0

    :cond_3
    :goto_0
    return v2
.end method

.method public static isOneOrMorePRNGTestsFailed([Lee/cyber/smartid/cryptolib/dto/TestResult;)Z
    .locals 4

    const/4 v0, 0x0

    if-eqz p0, :cond_2

    .line 441
    array-length v1, p0

    if-nez v1, :cond_0

    goto :goto_1

    .line 444
    :cond_0
    array-length v1, p0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_2

    aget-object v3, p0, v2

    .line 445
    invoke-virtual {v3}, Lee/cyber/smartid/cryptolib/dto/TestResult;->getFailCount()I

    move-result v3

    if-lez v3, :cond_1

    const/4 p0, 0x1

    return p0

    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_2
    :goto_1
    return v0
.end method

.method public static releaseWakeLock(Landroid/os/PowerManager$WakeLock;)V
    .locals 3

    const-string v0, "releaseWakeLock: trying to release a NULL wakelock!"

    if-eqz p0, :cond_0

    .line 71
    :try_start_0
    invoke-virtual {p0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 72
    const-class v0, Lee/cyber/smartid/tse/util/Util;

    invoke-static {v0}, Lee/cyber/smartid/tse/util/Log;->getInstance(Ljava/lang/Class;)Lee/cyber/smartid/tse/util/Log;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "releaseWakeLock (before): "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lee/cyber/smartid/tse/util/Log;->d(Ljava/lang/String;)V

    .line 73
    invoke-virtual {p0}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 74
    const-class v0, Lee/cyber/smartid/tse/util/Util;

    invoke-static {v0}, Lee/cyber/smartid/tse/util/Log;->getInstance(Ljava/lang/Class;)Lee/cyber/smartid/tse/util/Log;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "releaseWakeLock (after): "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v0, p0}, Lee/cyber/smartid/tse/util/Log;->d(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 77
    :cond_0
    :try_start_1
    new-instance p0, Ljava/io/IOException;

    invoke-direct {p0, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p0
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    :catch_0
    move-exception p0

    goto :goto_0

    :catch_1
    move-exception p0

    .line 79
    :try_start_2
    const-class v1, Lee/cyber/smartid/tse/util/Util;

    invoke-static {v1}, Lee/cyber/smartid/tse/util/Log;->getInstance(Ljava/lang/Class;)Lee/cyber/smartid/tse/util/Log;

    move-result-object v1

    invoke-virtual {v1, v0, p0}, Lee/cyber/smartid/tse/util/Log;->w(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_1

    .line 83
    :goto_0
    const-class v0, Lee/cyber/smartid/tse/util/Util;

    invoke-static {v0}, Lee/cyber/smartid/tse/util/Log;->getInstance(Ljava/lang/Class;)Lee/cyber/smartid/tse/util/Log;

    move-result-object v0

    const-string v1, "releaseWakeLock"

    invoke-virtual {v0, v1, p0}, Lee/cyber/smartid/tse/util/Log;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    :goto_1
    return-void
.end method

.method public static toString(Ljava/lang/Throwable;)Ljava/lang/String;
    .locals 4

    const/4 v0, 0x0

    if-nez p0, :cond_0

    return-object v0

    .line 170
    :cond_0
    :try_start_0
    new-instance v1, Ljava/io/StringWriter;

    invoke-direct {v1}, Ljava/io/StringWriter;-><init>()V

    .line 171
    new-instance v2, Ljava/io/PrintWriter;

    invoke-direct {v2, v1}, Ljava/io/PrintWriter;-><init>(Ljava/io/Writer;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 172
    :try_start_1
    invoke-virtual {p0, v2}, Ljava/lang/Throwable;->printStackTrace(Ljava/io/PrintWriter;)V

    .line 173
    invoke-virtual {v1}, Ljava/io/StringWriter;->toString()Ljava/lang/String;

    move-result-object v0
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_1

    :catch_0
    move-exception p0

    goto :goto_0

    :catchall_0
    move-exception p0

    move-object v2, v0

    goto :goto_2

    :catch_1
    move-exception p0

    move-object v2, v0

    .line 175
    :goto_0
    :try_start_2
    const-class v1, Lee/cyber/smartid/tse/util/Util;

    invoke-static {v1}, Lee/cyber/smartid/tse/util/Log;->getInstance(Ljava/lang/Class;)Lee/cyber/smartid/tse/util/Log;

    move-result-object v1

    const-string v3, "toString"

    invoke-virtual {v1, v3, p0}, Lee/cyber/smartid/tse/util/Log;->e(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 177
    :goto_1
    invoke-static {v2}, Lee/cyber/smartid/tse/util/Util;->closeClosable(Ljava/io/Closeable;)V

    return-object v0

    :catchall_1
    move-exception p0

    :goto_2
    invoke-static {v2}, Lee/cyber/smartid/tse/util/Util;->closeClosable(Ljava/io/Closeable;)V

    .line 178
    throw p0
.end method

.method public static validateBaseUrlParams(Landroid/content/Context;Ljava/lang/String;Z)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 329
    invoke-static {p1}, Lee/cyber/smartid/tse/util/Util;->isHTTPSUrl(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    if-eqz p2, :cond_0

    goto :goto_0

    .line 331
    :cond_0
    new-instance p1, Ljava/io/IOException;

    sget p2, Lee/cyber/smartid/tse/R$string;->err_invalid_base_url_1:I

    invoke-virtual {p0, p2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p0

    invoke-direct {p1, p0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 332
    :cond_1
    :goto_0
    invoke-static {p1}, Lee/cyber/smartid/tse/util/Util;->isHTTPSUrl(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_3

    if-nez p2, :cond_2

    goto :goto_1

    .line 334
    :cond_2
    new-instance p1, Ljava/io/IOException;

    sget p2, Lee/cyber/smartid/tse/R$string;->err_invalid_base_url_2:I

    invoke-virtual {p0, p2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p0

    invoke-direct {p1, p0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_3
    :goto_1
    return-void
.end method

.method public static validateResponse(Lc/r;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lee/cyber/smartid/tse/inter/Validatable;",
            ">(",
            "Lc/r<",
            "Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCResponse<",
            "TT;>;>;)Z"
        }
    .end annotation

    if-eqz p0, :cond_0

    .line 431
    invoke-virtual {p0}, Lc/r;->e()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lc/r;->e()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCResponse;

    invoke-virtual {v0}, Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCResponse;->getResult()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lc/r;->e()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCResponse;

    invoke-virtual {p0}, Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCResponse;->getResult()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lee/cyber/smartid/tse/inter/Validatable;

    invoke-interface {p0}, Lee/cyber/smartid/tse/inter/Validatable;->isValid()Z

    move-result p0

    if-eqz p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method
