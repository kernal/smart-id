.class Lee/cyber/smartid/tse/util/KeyStateRunnable$14;
.super Ljava/lang/Object;
.source "KeyStateRunnable.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lee/cyber/smartid/tse/util/KeyStateRunnable;->c()Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lee/cyber/smartid/tse/dto/jsonrpc/resp/SubmitClientSecondPartResp;

.field final synthetic b:Lee/cyber/smartid/tse/util/KeyStateRunnable;


# direct methods
.method constructor <init>(Lee/cyber/smartid/tse/util/KeyStateRunnable;Lee/cyber/smartid/tse/dto/jsonrpc/resp/SubmitClientSecondPartResp;)V
    .locals 0

    .line 676
    iput-object p1, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable$14;->b:Lee/cyber/smartid/tse/util/KeyStateRunnable;

    iput-object p2, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable$14;->a:Lee/cyber/smartid/tse/dto/jsonrpc/resp/SubmitClientSecondPartResp;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .line 680
    iget-object v0, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable$14;->b:Lee/cyber/smartid/tse/util/KeyStateRunnable;

    invoke-static {v0}, Lee/cyber/smartid/tse/util/KeyStateRunnable;->a(Lee/cyber/smartid/tse/util/KeyStateRunnable;)Ljava/lang/String;

    move-result-object v1

    const-class v2, Lee/cyber/smartid/tse/inter/SubmitClientSecondPartListener;

    const/4 v3, 0x1

    invoke-static {v0, v1, v3, v2}, Lee/cyber/smartid/tse/util/KeyStateRunnable;->a(Lee/cyber/smartid/tse/util/KeyStateRunnable;Ljava/lang/String;ZLjava/lang/Class;)Lee/cyber/smartid/tse/inter/TSEListener;

    move-result-object v0

    check-cast v0, Lee/cyber/smartid/tse/inter/SubmitClientSecondPartListener;

    const-string v1, "handleSubmitClientSecondPart"

    if-eqz v0, :cond_0

    .line 683
    :try_start_0
    iget-object v2, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable$14;->b:Lee/cyber/smartid/tse/util/KeyStateRunnable;

    invoke-static {v2}, Lee/cyber/smartid/tse/util/KeyStateRunnable;->a(Lee/cyber/smartid/tse/util/KeyStateRunnable;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable$14;->a:Lee/cyber/smartid/tse/dto/jsonrpc/resp/SubmitClientSecondPartResp;

    invoke-interface {v0, v2, v3}, Lee/cyber/smartid/tse/inter/SubmitClientSecondPartListener;->onSubmitClientSecondPartSuccess(Ljava/lang/String;Lee/cyber/smartid/tse/dto/jsonrpc/resp/SubmitClientSecondPartResp;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v2

    .line 686
    iget-object v3, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable$14;->b:Lee/cyber/smartid/tse/util/KeyStateRunnable;

    invoke-static {v3}, Lee/cyber/smartid/tse/util/KeyStateRunnable;->e(Lee/cyber/smartid/tse/util/KeyStateRunnable;)Lee/cyber/smartid/tse/inter/LogAccess;

    move-result-object v3

    invoke-interface {v3, v1, v2}, Lee/cyber/smartid/tse/inter/LogAccess;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 690
    :try_start_1
    iget-object v0, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable$14;->b:Lee/cyber/smartid/tse/util/KeyStateRunnable;

    invoke-static {v0}, Lee/cyber/smartid/tse/util/KeyStateRunnable;->f(Lee/cyber/smartid/tse/util/KeyStateRunnable;)Lee/cyber/smartid/tse/inter/ExternalResourceAccess;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 691
    iget-object v0, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable$14;->b:Lee/cyber/smartid/tse/util/KeyStateRunnable;

    invoke-static {v0}, Lee/cyber/smartid/tse/util/KeyStateRunnable;->f(Lee/cyber/smartid/tse/util/KeyStateRunnable;)Lee/cyber/smartid/tse/inter/ExternalResourceAccess;

    move-result-object v0

    iget-object v2, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable$14;->b:Lee/cyber/smartid/tse/util/KeyStateRunnable;

    invoke-static {v2}, Lee/cyber/smartid/tse/util/KeyStateRunnable;->a(Lee/cyber/smartid/tse/util/KeyStateRunnable;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable$14;->a:Lee/cyber/smartid/tse/dto/jsonrpc/resp/SubmitClientSecondPartResp;

    invoke-interface {v0, v2, v3}, Lee/cyber/smartid/tse/inter/ExternalResourceAccess;->onSubmitClientSecondPartSuccess(Ljava/lang/String;Lee/cyber/smartid/tse/dto/jsonrpc/resp/SubmitClientSecondPartResp;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    :catch_1
    move-exception v0

    .line 694
    iget-object v2, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable$14;->b:Lee/cyber/smartid/tse/util/KeyStateRunnable;

    invoke-static {v2}, Lee/cyber/smartid/tse/util/KeyStateRunnable;->e(Lee/cyber/smartid/tse/util/KeyStateRunnable;)Lee/cyber/smartid/tse/inter/LogAccess;

    move-result-object v2

    invoke-interface {v2, v1, v0}, Lee/cyber/smartid/tse/inter/LogAccess;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_1
    :goto_1
    return-void
.end method
