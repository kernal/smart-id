.class Lee/cyber/smartid/tse/util/KeyStateRunnable$7;
.super Ljava/lang/Object;
.source "KeyStateRunnable.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lee/cyber/smartid/tse/util/KeyStateRunnable;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lee/cyber/smartid/tse/util/KeyStateRunnable;


# direct methods
.method constructor <init>(Lee/cyber/smartid/tse/util/KeyStateRunnable;)V
    .locals 0

    .line 318
    iput-object p1, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable$7;->a:Lee/cyber/smartid/tse/util/KeyStateRunnable;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .line 321
    iget-object v0, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable$7;->a:Lee/cyber/smartid/tse/util/KeyStateRunnable;

    invoke-static {v0}, Lee/cyber/smartid/tse/util/KeyStateRunnable;->a(Lee/cyber/smartid/tse/util/KeyStateRunnable;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable$7;->a:Lee/cyber/smartid/tse/util/KeyStateRunnable;

    invoke-static {v2}, Lee/cyber/smartid/tse/util/KeyStateRunnable;->a(Lee/cyber/smartid/tse/util/KeyStateRunnable;)Ljava/lang/String;

    move-result-object v3

    const-class v4, Lee/cyber/smartid/tse/inter/TSEListener;

    const/4 v5, 0x1

    invoke-static {v2, v3, v5, v4}, Lee/cyber/smartid/tse/util/KeyStateRunnable;->a(Lee/cyber/smartid/tse/util/KeyStateRunnable;Ljava/lang/String;ZLjava/lang/Class;)Lee/cyber/smartid/tse/inter/TSEListener;

    move-result-object v2

    iget-object v3, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable$7;->a:Lee/cyber/smartid/tse/util/KeyStateRunnable;

    invoke-static {v3}, Lee/cyber/smartid/tse/util/KeyStateRunnable;->b(Lee/cyber/smartid/tse/util/KeyStateRunnable;)Lee/cyber/smartid/tse/inter/WallClock;

    move-result-object v3

    iget-object v4, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable$7;->a:Lee/cyber/smartid/tse/util/KeyStateRunnable;

    invoke-static {v4}, Lee/cyber/smartid/tse/util/KeyStateRunnable;->d(Lee/cyber/smartid/tse/util/KeyStateRunnable;)Lee/cyber/smartid/tse/inter/ResourceAccess;

    move-result-object v4

    invoke-interface {v4}, Lee/cyber/smartid/tse/inter/ResourceAccess;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    sget v5, Lee/cyber/smartid/tse/R$string;->err_invalid_transaction_state:I

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    const-wide/16 v5, 0x3f4    # 5.0E-321

    invoke-static {v3, v5, v6, v4}, Lee/cyber/smartid/tse/dto/TSEError;->from(Lee/cyber/smartid/tse/inter/WallClock;JLjava/lang/String;)Lee/cyber/smartid/tse/dto/TSEError;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, Lee/cyber/smartid/tse/util/KeyStateRunnable;->a(Lee/cyber/smartid/tse/util/KeyStateRunnable;Ljava/lang/String;Lee/cyber/smartid/tse/inter/TSEListener;Lee/cyber/smartid/tse/dto/TSEError;)V

    return-void
.end method
