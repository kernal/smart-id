.class Lee/cyber/smartid/tse/util/KeyStateRunnable$12;
.super Ljava/lang/Object;
.source "KeyStateRunnable.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lee/cyber/smartid/tse/util/KeyStateRunnable;->b()Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lee/cyber/smartid/tse/dto/jsonrpc/resp/RefreshCloneDetectionResp;

.field final synthetic b:Lee/cyber/smartid/tse/util/KeyStateRunnable;


# direct methods
.method constructor <init>(Lee/cyber/smartid/tse/util/KeyStateRunnable;Lee/cyber/smartid/tse/dto/jsonrpc/resp/RefreshCloneDetectionResp;)V
    .locals 0

    .line 565
    iput-object p1, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable$12;->b:Lee/cyber/smartid/tse/util/KeyStateRunnable;

    iput-object p2, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable$12;->a:Lee/cyber/smartid/tse/dto/jsonrpc/resp/RefreshCloneDetectionResp;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .line 568
    iget-object v0, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable$12;->b:Lee/cyber/smartid/tse/util/KeyStateRunnable;

    invoke-static {v0}, Lee/cyber/smartid/tse/util/KeyStateRunnable;->a(Lee/cyber/smartid/tse/util/KeyStateRunnable;)Ljava/lang/String;

    move-result-object v1

    const-class v2, Lee/cyber/smartid/tse/inter/RefreshCloneDetectionListener;

    const/4 v3, 0x1

    invoke-static {v0, v1, v3, v2}, Lee/cyber/smartid/tse/util/KeyStateRunnable;->a(Lee/cyber/smartid/tse/util/KeyStateRunnable;Ljava/lang/String;ZLjava/lang/Class;)Lee/cyber/smartid/tse/inter/TSEListener;

    move-result-object v0

    check-cast v0, Lee/cyber/smartid/tse/inter/RefreshCloneDetectionListener;

    if-eqz v0, :cond_0

    .line 570
    iget-object v1, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable$12;->b:Lee/cyber/smartid/tse/util/KeyStateRunnable;

    invoke-static {v1}, Lee/cyber/smartid/tse/util/KeyStateRunnable;->a(Lee/cyber/smartid/tse/util/KeyStateRunnable;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable$12;->a:Lee/cyber/smartid/tse/dto/jsonrpc/resp/RefreshCloneDetectionResp;

    invoke-interface {v0, v1, v2}, Lee/cyber/smartid/tse/inter/RefreshCloneDetectionListener;->onRefreshCloneDetectionSuccess(Ljava/lang/String;Lee/cyber/smartid/tse/dto/jsonrpc/resp/RefreshCloneDetectionResp;)V

    :cond_0
    return-void
.end method
