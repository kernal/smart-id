.class public final Lee/cyber/smartid/tse/util/KeyStateRunnable;
.super Ljava/lang/Object;
.source "KeyStateRunnable.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lee/cyber/smartid/tse/util/KeyStateRunnable$ValidateResponseCallback;
    }
.end annotation


# static fields
.field public static final RETRY_COUNT_BLOCKING:I = 0x3


# instance fields
.field private final a:Lee/cyber/smartid/tse/inter/StateWorkerJobListener;

.field private final b:Lee/cyber/smartid/tse/network/TSEAPI$TSEAPIService;

.field private final c:Ljava/lang/String;

.field private final d:Lee/cyber/smartid/tse/dto/ProtoKeyState;

.field private e:Lee/cyber/smartid/tse/dto/KeyState;

.field private f:Lee/cyber/smartid/tse/dto/KeyStateMeta;

.field private final g:Lee/cyber/smartid/tse/inter/ResourceAccess;

.field private final h:Lee/cyber/smartid/tse/inter/ExternalResourceAccess;

.field private final i:Lee/cyber/smartid/tse/inter/KeyStorageAccess;

.field private final j:Lee/cyber/smartid/tse/inter/ListenerAccess;

.field private final k:Lee/cyber/smartid/tse/SmartIdTSE$TSEAlarmAccess;

.field private final l:Landroid/os/Handler;

.field private final m:Lee/cyber/smartid/tse/inter/WallClock;

.field private final n:Landroid/os/Bundle;

.field private final o:Lee/cyber/smartid/tse/inter/LogAccess;


# direct methods
.method public constructor <init>(Lee/cyber/smartid/tse/dto/ProtoKeyState;Ljava/lang/String;Lee/cyber/smartid/tse/inter/ResourceAccess;Lee/cyber/smartid/tse/inter/ExternalResourceAccess;Lee/cyber/smartid/tse/network/TSEAPI$TSEAPIService;Landroid/os/Handler;Lee/cyber/smartid/tse/inter/StateWorkerJobListener;Lee/cyber/smartid/tse/inter/ListenerAccess;Lee/cyber/smartid/tse/inter/KeyStorageAccess;Lee/cyber/smartid/tse/SmartIdTSE$TSEAlarmAccess;Lee/cyber/smartid/tse/inter/WallClock;Landroid/os/Bundle;)V
    .locals 1

    .line 172
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 133
    invoke-static {p0}, Lee/cyber/smartid/tse/util/Log;->getInstance(Ljava/lang/Object;)Lee/cyber/smartid/tse/util/Log;

    move-result-object v0

    iput-object v0, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable;->o:Lee/cyber/smartid/tse/inter/LogAccess;

    .line 173
    iput-object p1, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable;->d:Lee/cyber/smartid/tse/dto/ProtoKeyState;

    .line 174
    iput-object p3, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable;->g:Lee/cyber/smartid/tse/inter/ResourceAccess;

    .line 175
    iput-object p4, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable;->h:Lee/cyber/smartid/tse/inter/ExternalResourceAccess;

    .line 176
    iput-object p2, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable;->c:Ljava/lang/String;

    .line 177
    iput-object p5, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable;->b:Lee/cyber/smartid/tse/network/TSEAPI$TSEAPIService;

    .line 178
    iput-object p7, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable;->a:Lee/cyber/smartid/tse/inter/StateWorkerJobListener;

    .line 179
    iput-object p6, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable;->l:Landroid/os/Handler;

    .line 180
    iput-object p8, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable;->j:Lee/cyber/smartid/tse/inter/ListenerAccess;

    .line 181
    iput-object p9, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable;->i:Lee/cyber/smartid/tse/inter/KeyStorageAccess;

    .line 182
    iput-object p10, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable;->k:Lee/cyber/smartid/tse/SmartIdTSE$TSEAlarmAccess;

    .line 183
    iput-object p11, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable;->m:Lee/cyber/smartid/tse/inter/WallClock;

    .line 184
    iput-object p12, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable;->n:Landroid/os/Bundle;

    return-void
.end method

.method public constructor <init>(Lee/cyber/smartid/tse/dto/ProtoKeyState;Ljava/lang/String;Lee/cyber/smartid/tse/inter/ResourceAccess;Lee/cyber/smartid/tse/inter/ExternalResourceAccess;Lee/cyber/smartid/tse/network/TSEAPI$TSEAPIService;Lee/cyber/smartid/tse/inter/StateWorkerJobListener;Lee/cyber/smartid/tse/inter/ListenerAccess;Lee/cyber/smartid/tse/inter/KeyStorageAccess;Lee/cyber/smartid/tse/SmartIdTSE$TSEAlarmAccess;Lee/cyber/smartid/tse/inter/WallClock;Landroid/os/Bundle;)V
    .locals 13

    .line 152
    new-instance v6, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-direct {v6, v0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    invoke-direct/range {v0 .. v12}, Lee/cyber/smartid/tse/util/KeyStateRunnable;-><init>(Lee/cyber/smartid/tse/dto/ProtoKeyState;Ljava/lang/String;Lee/cyber/smartid/tse/inter/ResourceAccess;Lee/cyber/smartid/tse/inter/ExternalResourceAccess;Lee/cyber/smartid/tse/network/TSEAPI$TSEAPIService;Landroid/os/Handler;Lee/cyber/smartid/tse/inter/StateWorkerJobListener;Lee/cyber/smartid/tse/inter/ListenerAccess;Lee/cyber/smartid/tse/inter/KeyStorageAccess;Lee/cyber/smartid/tse/SmartIdTSE$TSEAlarmAccess;Lee/cyber/smartid/tse/inter/WallClock;Landroid/os/Bundle;)V

    return-void
.end method

.method private a(Lc/r;Lee/cyber/smartid/tse/dto/TSEError;)Lee/cyber/smartid/tse/dto/TSEError;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lee/cyber/smartid/tse/dto/jsonrpc/result/RefreshCloneDetectionResult;",
            ">(",
            "Lc/r<",
            "Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCResponse<",
            "TT;>;>;",
            "Lee/cyber/smartid/tse/dto/TSEError;",
            ")",
            "Lee/cyber/smartid/tse/dto/TSEError;"
        }
    .end annotation

    if-nez p2, :cond_1

    if-eqz p1, :cond_0

    .line 714
    invoke-virtual {p1}, Lc/r;->e()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lc/r;->e()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCResponse;

    invoke-virtual {v0}, Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCResponse;->getResult()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lc/r;->e()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCResponse;

    invoke-virtual {p1}, Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCResponse;->getResult()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lee/cyber/smartid/tse/dto/jsonrpc/result/RefreshCloneDetectionResult;

    invoke-virtual {p1}, Lee/cyber/smartid/tse/dto/jsonrpc/result/RefreshCloneDetectionResult;->getResponseData()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p1

    if-eqz p1, :cond_1

    .line 716
    :cond_0
    iget-object p1, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable;->o:Lee/cyber/smartid/tse/inter/LogAccess;

    const-string p2, "createErrorIfNoResults - we failed to get a refreshCloneDetection payload, but no error object, something is very wrong, creating one"

    invoke-interface {p1, p2}, Lee/cyber/smartid/tse/inter/LogAccess;->wtf(Ljava/lang/String;)V

    .line 717
    iget-object p1, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable;->m:Lee/cyber/smartid/tse/inter/WallClock;

    const-wide/16 v0, 0x3ec

    iget-object p2, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable;->g:Lee/cyber/smartid/tse/inter/ResourceAccess;

    invoke-interface {p2}, Lee/cyber/smartid/tse/inter/ResourceAccess;->getApplicationContext()Landroid/content/Context;

    move-result-object p2

    sget v2, Lee/cyber/smartid/tse/R$string;->err_server_response_invalid:I

    invoke-virtual {p2, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p2

    invoke-static {p1, v0, v1, p2}, Lee/cyber/smartid/tse/dto/TSEError;->from(Lee/cyber/smartid/tse/inter/WallClock;JLjava/lang/String;)Lee/cyber/smartid/tse/dto/TSEError;

    move-result-object p2

    :cond_1
    return-object p2
.end method

.method private a(Lee/cyber/smartid/tse/dto/TSEError;)Lee/cyber/smartid/tse/dto/TSEError;
    .locals 4

    if-nez p1, :cond_0

    .line 754
    iget-object p1, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable;->m:Lee/cyber/smartid/tse/inter/WallClock;

    const-wide/16 v0, 0x40a

    iget-object v2, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable;->g:Lee/cyber/smartid/tse/inter/ResourceAccess;

    invoke-interface {v2}, Lee/cyber/smartid/tse/inter/ResourceAccess;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    sget v3, Lee/cyber/smartid/tse/R$string;->err_unable_to_acquire_freshness_token:I

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {p1, v0, v1, v2}, Lee/cyber/smartid/tse/dto/TSEError;->from(Lee/cyber/smartid/tse/inter/WallClock;JLjava/lang/String;)Lee/cyber/smartid/tse/dto/TSEError;

    move-result-object p1

    :cond_0
    return-object p1
.end method

.method private a(Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCRequest;Lc/r;Ljava/lang/Throwable;Lee/cyber/smartid/tse/util/KeyStateRunnable$ValidateResponseCallback;)Lee/cyber/smartid/tse/dto/TSEError;
    .locals 2

    if-eqz p2, :cond_0

    .line 907
    invoke-virtual {p2}, Lc/r;->b()I

    move-result v0

    const/16 v1, 0x191

    if-ne v0, v1, :cond_0

    .line 908
    iget-object p1, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable;->g:Lee/cyber/smartid/tse/inter/ResourceAccess;

    invoke-interface {p1}, Lee/cyber/smartid/tse/inter/ResourceAccess;->setHammerTimeEventTimestamp()V

    .line 909
    iget-object p1, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable;->g:Lee/cyber/smartid/tse/inter/ResourceAccess;

    invoke-interface {p1}, Lee/cyber/smartid/tse/inter/ResourceAccess;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    iget-object p2, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable;->m:Lee/cyber/smartid/tse/inter/WallClock;

    new-instance p3, Lee/cyber/smartid/tse/dto/AuthorizationRequiredException;

    invoke-direct {p3}, Lee/cyber/smartid/tse/dto/AuthorizationRequiredException;-><init>()V

    invoke-static {p1, p2, p3}, Lee/cyber/smartid/tse/dto/TSEError;->from(Landroid/content/Context;Lee/cyber/smartid/tse/inter/WallClock;Ljava/lang/Throwable;)Lee/cyber/smartid/tse/dto/TSEError;

    move-result-object p1

    .line 910
    invoke-direct {p0, p1}, Lee/cyber/smartid/tse/util/KeyStateRunnable;->e(Lee/cyber/smartid/tse/dto/TSEError;)V

    return-object p1

    :cond_0
    if-eqz p2, :cond_1

    .line 912
    invoke-virtual {p2}, Lc/r;->b()I

    move-result v0

    const/16 v1, 0x1e0

    if-ne v0, v1, :cond_1

    .line 913
    iget-object p1, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable;->g:Lee/cyber/smartid/tse/inter/ResourceAccess;

    invoke-interface {p1}, Lee/cyber/smartid/tse/inter/ResourceAccess;->setHammerTimeEventTimestamp()V

    .line 914
    iget-object p1, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable;->g:Lee/cyber/smartid/tse/inter/ResourceAccess;

    invoke-interface {p1}, Lee/cyber/smartid/tse/inter/ResourceAccess;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    iget-object p2, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable;->m:Lee/cyber/smartid/tse/inter/WallClock;

    new-instance p3, Lee/cyber/smartid/tse/dto/ClientTooOldException;

    invoke-direct {p3}, Lee/cyber/smartid/tse/dto/ClientTooOldException;-><init>()V

    invoke-static {p1, p2, p3}, Lee/cyber/smartid/tse/dto/TSEError;->from(Landroid/content/Context;Lee/cyber/smartid/tse/inter/WallClock;Ljava/lang/Throwable;)Lee/cyber/smartid/tse/dto/TSEError;

    move-result-object p1

    .line 915
    invoke-direct {p0, p1}, Lee/cyber/smartid/tse/util/KeyStateRunnable;->e(Lee/cyber/smartid/tse/dto/TSEError;)V

    return-object p1

    :cond_1
    if-eqz p2, :cond_2

    .line 917
    invoke-virtual {p2}, Lc/r;->b()I

    move-result v0

    const/16 v1, 0x244

    if-ne v0, v1, :cond_2

    .line 918
    iget-object p1, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable;->g:Lee/cyber/smartid/tse/inter/ResourceAccess;

    invoke-interface {p1}, Lee/cyber/smartid/tse/inter/ResourceAccess;->setHammerTimeEventTimestamp()V

    .line 919
    iget-object p1, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable;->g:Lee/cyber/smartid/tse/inter/ResourceAccess;

    invoke-interface {p1}, Lee/cyber/smartid/tse/inter/ResourceAccess;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    iget-object p2, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable;->m:Lee/cyber/smartid/tse/inter/WallClock;

    new-instance p3, Lee/cyber/smartid/tse/dto/SystemUnderMaintenanceException;

    invoke-direct {p3}, Lee/cyber/smartid/tse/dto/SystemUnderMaintenanceException;-><init>()V

    invoke-static {p1, p2, p3}, Lee/cyber/smartid/tse/dto/TSEError;->from(Landroid/content/Context;Lee/cyber/smartid/tse/inter/WallClock;Ljava/lang/Throwable;)Lee/cyber/smartid/tse/dto/TSEError;

    move-result-object p1

    .line 920
    invoke-direct {p0, p1}, Lee/cyber/smartid/tse/util/KeyStateRunnable;->e(Lee/cyber/smartid/tse/dto/TSEError;)V

    return-object p1

    :cond_2
    if-eqz p3, :cond_4

    .line 923
    invoke-static {p3}, Lee/cyber/smartid/tse/dto/TSEError;->isServerSideError(Ljava/lang/Throwable;)Z

    move-result p1

    if-eqz p1, :cond_3

    .line 924
    iget-object p1, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable;->g:Lee/cyber/smartid/tse/inter/ResourceAccess;

    invoke-interface {p1}, Lee/cyber/smartid/tse/inter/ResourceAccess;->setHammerTimeEventTimestamp()V

    .line 926
    :cond_3
    iget-object p1, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable;->g:Lee/cyber/smartid/tse/inter/ResourceAccess;

    invoke-interface {p1}, Lee/cyber/smartid/tse/inter/ResourceAccess;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    iget-object p2, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable;->m:Lee/cyber/smartid/tse/inter/WallClock;

    invoke-static {p1, p2, p3}, Lee/cyber/smartid/tse/dto/TSEError;->from(Landroid/content/Context;Lee/cyber/smartid/tse/inter/WallClock;Ljava/lang/Throwable;)Lee/cyber/smartid/tse/dto/TSEError;

    move-result-object p1

    return-object p1

    :cond_4
    if-nez p2, :cond_5

    .line 931
    iget-object p1, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable;->g:Lee/cyber/smartid/tse/inter/ResourceAccess;

    invoke-interface {p1}, Lee/cyber/smartid/tse/inter/ResourceAccess;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    iget-object p2, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable;->m:Lee/cyber/smartid/tse/inter/WallClock;

    new-instance p3, Lee/cyber/smartid/tse/dto/InvalidResponseResultException;

    invoke-direct {p3}, Lee/cyber/smartid/tse/dto/InvalidResponseResultException;-><init>()V

    invoke-static {p1, p2, p3}, Lee/cyber/smartid/tse/dto/TSEError;->from(Landroid/content/Context;Lee/cyber/smartid/tse/inter/WallClock;Ljava/lang/Throwable;)Lee/cyber/smartid/tse/dto/TSEError;

    move-result-object p1

    return-object p1

    .line 936
    :cond_5
    invoke-static {p2}, Lee/cyber/smartid/tse/network/RPCCallback;->getBody(Lc/r;)Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCResponse;

    move-result-object p3

    .line 939
    invoke-virtual {p1}, Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCRequest;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2}, Lc/r;->b()I

    move-result v1

    invoke-static {v0, v1, p3}, Lee/cyber/smartid/tse/network/RPCCallback;->isForId(Ljava/lang/String;ILee/cyber/smartid/tse/dto/jsonrpc/base/RPCResponse;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 940
    iget-object p1, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable;->g:Lee/cyber/smartid/tse/inter/ResourceAccess;

    invoke-interface {p1}, Lee/cyber/smartid/tse/inter/ResourceAccess;->setHammerTimeEventTimestamp()V

    .line 941
    iget-object p1, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable;->g:Lee/cyber/smartid/tse/inter/ResourceAccess;

    invoke-interface {p1}, Lee/cyber/smartid/tse/inter/ResourceAccess;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    iget-object p2, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable;->m:Lee/cyber/smartid/tse/inter/WallClock;

    new-instance p3, Lee/cyber/smartid/tse/dto/InvalidResponseIdException;

    invoke-direct {p3}, Lee/cyber/smartid/tse/dto/InvalidResponseIdException;-><init>()V

    invoke-static {p1, p2, p3}, Lee/cyber/smartid/tse/dto/TSEError;->from(Landroid/content/Context;Lee/cyber/smartid/tse/inter/WallClock;Ljava/lang/Throwable;)Lee/cyber/smartid/tse/dto/TSEError;

    move-result-object p1

    return-object p1

    :cond_6
    if-nez p3, :cond_7

    .line 946
    invoke-virtual {p2}, Lc/r;->b()I

    move-result v0

    const/16 v1, 0xc8

    if-eq v0, v1, :cond_7

    .line 947
    iget-object p1, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable;->g:Lee/cyber/smartid/tse/inter/ResourceAccess;

    invoke-interface {p1}, Lee/cyber/smartid/tse/inter/ResourceAccess;->setHammerTimeEventTimestamp()V

    .line 948
    iget-object p1, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable;->g:Lee/cyber/smartid/tse/inter/ResourceAccess;

    invoke-interface {p1}, Lee/cyber/smartid/tse/inter/ResourceAccess;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    iget-object p3, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable;->m:Lee/cyber/smartid/tse/inter/WallClock;

    new-instance p4, Lee/cyber/smartid/tse/dto/HttpErrorCodeResponseException;

    invoke-virtual {p2}, Lc/r;->b()I

    move-result p2

    invoke-direct {p4, p2}, Lee/cyber/smartid/tse/dto/HttpErrorCodeResponseException;-><init>(I)V

    invoke-static {p1, p3, p4}, Lee/cyber/smartid/tse/dto/TSEError;->from(Landroid/content/Context;Lee/cyber/smartid/tse/inter/WallClock;Ljava/lang/Throwable;)Lee/cyber/smartid/tse/dto/TSEError;

    move-result-object p1

    return-object p1

    :cond_7
    const/4 v0, 0x0

    if-eqz p3, :cond_9

    .line 951
    invoke-virtual {p3}, Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCResponse;->isError()Z

    move-result v1

    if-eqz v1, :cond_9

    .line 952
    iget-object p2, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable;->g:Lee/cyber/smartid/tse/inter/ResourceAccess;

    invoke-interface {p2}, Lee/cyber/smartid/tse/inter/ResourceAccess;->setHammerTimeEventTimestamp()V

    .line 953
    iget-object p2, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable;->g:Lee/cyber/smartid/tse/inter/ResourceAccess;

    invoke-interface {p2}, Lee/cyber/smartid/tse/inter/ResourceAccess;->getApplicationContext()Landroid/content/Context;

    move-result-object p2

    iget-object p4, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable;->m:Lee/cyber/smartid/tse/inter/WallClock;

    invoke-virtual {p3}, Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCResponse;->getError()Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCError;

    move-result-object p3

    invoke-static {p2, p4, p3, v0}, Lee/cyber/smartid/tse/dto/TSEError;->from(Landroid/content/Context;Lee/cyber/smartid/tse/inter/WallClock;Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCError;Ljava/lang/Throwable;)Lee/cyber/smartid/tse/dto/TSEError;

    move-result-object p2

    .line 954
    invoke-virtual {p1}, Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCRequest;->getMethod()Ljava/lang/String;

    move-result-object p3

    const-string p4, "submitSignature"

    invoke-static {p4, p3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result p3

    if-eqz p3, :cond_8

    invoke-virtual {p1}, Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCRequest;->getParams()Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCParams;

    move-result-object p3

    instance-of p3, p3, Lee/cyber/smartid/tse/dto/jsonrpc/param/SubmitSignatureParams;

    if-eqz p3, :cond_8

    .line 955
    invoke-virtual {p2}, Lee/cyber/smartid/tse/dto/TSEError;->createOrGetExtras()Landroid/os/Bundle;

    move-result-object p3

    invoke-virtual {p1}, Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCRequest;->getParams()Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCParams;

    move-result-object p1

    check-cast p1, Lee/cyber/smartid/tse/dto/jsonrpc/param/SubmitSignatureParams;

    invoke-virtual {p1}, Lee/cyber/smartid/tse/dto/jsonrpc/param/SubmitSignatureParams;->getTransactionUUID()Ljava/lang/String;

    move-result-object p1

    const-string p4, "ee.cyber.smartid.EXTRA_TRANSACTION_ID"

    invoke-virtual {p3, p4, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_8
    return-object p2

    :cond_9
    if-eqz p4, :cond_a

    .line 959
    invoke-virtual {p2}, Lc/r;->e()Ljava/lang/Object;

    move-result-object p1

    if-eqz p1, :cond_a

    invoke-virtual {p2}, Lc/r;->e()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCResponse;

    invoke-interface {p4, p1}, Lee/cyber/smartid/tse/util/KeyStateRunnable$ValidateResponseCallback;->isValidResponse(Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCResponse;)Z

    move-result p1

    if-nez p1, :cond_a

    .line 961
    iget-object p1, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable;->g:Lee/cyber/smartid/tse/inter/ResourceAccess;

    invoke-interface {p1}, Lee/cyber/smartid/tse/inter/ResourceAccess;->setHammerTimeEventTimestamp()V

    .line 962
    iget-object p1, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable;->g:Lee/cyber/smartid/tse/inter/ResourceAccess;

    invoke-interface {p1}, Lee/cyber/smartid/tse/inter/ResourceAccess;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    iget-object p2, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable;->m:Lee/cyber/smartid/tse/inter/WallClock;

    new-instance p3, Lee/cyber/smartid/tse/dto/InvalidResponseResultException;

    invoke-direct {p3}, Lee/cyber/smartid/tse/dto/InvalidResponseResultException;-><init>()V

    invoke-static {p1, p2, p3}, Lee/cyber/smartid/tse/dto/TSEError;->from(Landroid/content/Context;Lee/cyber/smartid/tse/inter/WallClock;Ljava/lang/Throwable;)Lee/cyber/smartid/tse/dto/TSEError;

    move-result-object p1

    return-object p1

    :cond_a
    return-object v0
.end method

.method private a(Ljava/util/concurrent/atomic/AtomicInteger;)Lee/cyber/smartid/tse/dto/TSEError;
    .locals 7

    const-string v0, "updateFreshnessToken"

    .line 784
    iget-object v1, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable;->o:Lee/cyber/smartid/tse/inter/LogAccess;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "updateFreshnessToken - startRetryCount: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lee/cyber/smartid/tse/inter/LogAccess;->d(Ljava/lang/String;)V

    .line 786
    new-instance v1, Lee/cyber/smartid/tse/dto/jsonrpc/param/NewFreshnessTokenParams;

    invoke-direct {v1}, Lee/cyber/smartid/tse/dto/jsonrpc/param/NewFreshnessTokenParams;-><init>()V

    .line 787
    iget-object v2, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable;->e:Lee/cyber/smartid/tse/dto/KeyState;

    invoke-virtual {v2}, Lee/cyber/smartid/tse/dto/KeyState;->getAccountUUID()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lee/cyber/smartid/tse/dto/jsonrpc/param/NewFreshnessTokenParams;->setAccountUUID(Ljava/lang/String;)V

    .line 788
    iget-object v2, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable;->e:Lee/cyber/smartid/tse/dto/KeyState;

    invoke-virtual {v2}, Lee/cyber/smartid/tse/dto/KeyState;->getKeyType()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lee/cyber/smartid/tse/dto/jsonrpc/param/NewFreshnessTokenParams;->setKeyType(Ljava/lang/String;)V

    .line 789
    new-instance v2, Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCRequest;

    const-string v3, "getFreshnessToken"

    invoke-direct {v2, v3, v1}, Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCRequest;-><init>(Ljava/lang/String;Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCParams;)V

    :cond_0
    const/4 v1, 0x3

    const/4 v3, 0x0

    .line 804
    :try_start_0
    invoke-virtual {p1}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v4

    if-lez v4, :cond_1

    .line 805
    iget-object v4, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable;->o:Lee/cyber/smartid/tse/inter/LogAccess;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "updateFreshnessToken - overall retries at "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v6, "/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Lee/cyber/smartid/tse/inter/LogAccess;->d(Ljava/lang/String;)V

    .line 806
    iget-object v4, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable;->g:Lee/cyber/smartid/tse/inter/ResourceAccess;

    invoke-virtual {p1}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v5

    invoke-interface {v4, v5}, Lee/cyber/smartid/tse/inter/ResourceAccess;->getAutomaticRequestRetryDelay(I)J

    move-result-wide v4

    invoke-static {v4, v5}, Landroid/os/SystemClock;->sleep(J)V

    .line 808
    :cond_1
    iget-object v4, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable;->b:Lee/cyber/smartid/tse/network/TSEAPI$TSEAPIService;

    iget-object v5, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable;->n:Landroid/os/Bundle;

    invoke-static {v5, p1}, Lee/cyber/smartid/tse/util/Util;->a(Landroid/os/Bundle;Ljava/util/concurrent/atomic/AtomicInteger;)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5, v2}, Lee/cyber/smartid/tse/network/TSEAPI$TSEAPIService;->getNewFreshnessToken(Ljava/lang/String;Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCRequest;)Lc/b;

    move-result-object v4

    invoke-interface {v4}, Lc/b;->a()Lc/r;

    move-result-object v4
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v5, v3

    goto :goto_1

    :catch_0
    move-exception v4

    .line 814
    iget-object v5, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable;->o:Lee/cyber/smartid/tse/inter/LogAccess;

    invoke-interface {v5, v0, v4}, Lee/cyber/smartid/tse/inter/LogAccess;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    :catch_1
    move-exception v4

    .line 811
    iget-object v5, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable;->o:Lee/cyber/smartid/tse/inter/LogAccess;

    invoke-interface {v5, v0, v4}, Lee/cyber/smartid/tse/inter/LogAccess;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    :goto_0
    move-object v5, v4

    move-object v4, v3

    .line 816
    :goto_1
    new-instance v6, Lee/cyber/smartid/tse/util/KeyStateRunnable$15;

    invoke-direct {v6, p0}, Lee/cyber/smartid/tse/util/KeyStateRunnable$15;-><init>(Lee/cyber/smartid/tse/util/KeyStateRunnable;)V

    invoke-direct {p0, v2, v4, v5, v6}, Lee/cyber/smartid/tse/util/KeyStateRunnable;->a(Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCRequest;Lc/r;Ljava/lang/Throwable;Lee/cyber/smartid/tse/util/KeyStateRunnable$ValidateResponseCallback;)Lee/cyber/smartid/tse/dto/TSEError;

    move-result-object v5

    if-eqz v5, :cond_2

    .line 823
    iget-object v6, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable;->f:Lee/cyber/smartid/tse/dto/KeyStateMeta;

    invoke-direct {p0, v6, v5}, Lee/cyber/smartid/tse/util/KeyStateRunnable;->a(Lee/cyber/smartid/tse/dto/KeyStateMeta;Lee/cyber/smartid/tse/dto/TSEError;)Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-virtual {p1}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    move-result v6

    if-le v6, v1, :cond_0

    :cond_2
    if-eqz v5, :cond_3

    return-object v5

    .line 832
    :cond_3
    :try_start_1
    iget-object p1, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable;->i:Lee/cyber/smartid/tse/inter/KeyStorageAccess;

    iget-object v1, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable;->e:Lee/cyber/smartid/tse/dto/KeyState;

    invoke-virtual {v1}, Lee/cyber/smartid/tse/dto/KeyState;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4}, Lc/r;->e()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCResponse;

    invoke-virtual {v2}, Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCResponse;->getResult()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lee/cyber/smartid/tse/dto/jsonrpc/result/NewFreshnessTokenResult;

    invoke-virtual {v2}, Lee/cyber/smartid/tse/dto/jsonrpc/result/NewFreshnessTokenResult;->getFreshnessToken()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1, v1, v2}, Lee/cyber/smartid/tse/inter/KeyStorageAccess;->updateFreshnessToken(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Lee/cyber/smartid/cryptolib/dto/StorageException; {:try_start_1 .. :try_end_1} :catch_2

    return-object v3

    :catch_2
    move-exception p1

    .line 835
    iget-object v1, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable;->o:Lee/cyber/smartid/tse/inter/LogAccess;

    invoke-interface {v1, v0, p1}, Lee/cyber/smartid/tse/inter/LogAccess;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 836
    iget-object v0, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable;->g:Lee/cyber/smartid/tse/inter/ResourceAccess;

    invoke-interface {v0}, Lee/cyber/smartid/tse/inter/ResourceAccess;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable;->m:Lee/cyber/smartid/tse/inter/WallClock;

    invoke-static {v0, v1, p1}, Lee/cyber/smartid/tse/dto/TSEError;->from(Landroid/content/Context;Lee/cyber/smartid/tse/inter/WallClock;Ljava/lang/Throwable;)Lee/cyber/smartid/tse/dto/TSEError;

    move-result-object p1

    return-object p1
.end method

.method static synthetic a(Lee/cyber/smartid/tse/util/KeyStateRunnable;Ljava/lang/String;ZLjava/lang/Class;)Lee/cyber/smartid/tse/inter/TSEListener;
    .locals 0

    .line 68
    invoke-direct {p0, p1, p2, p3}, Lee/cyber/smartid/tse/util/KeyStateRunnable;->a(Ljava/lang/String;ZLjava/lang/Class;)Lee/cyber/smartid/tse/inter/TSEListener;

    move-result-object p0

    return-object p0
.end method

.method private a(Ljava/lang/String;ZLjava/lang/Class;)Lee/cyber/smartid/tse/inter/TSEListener;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lee/cyber/smartid/tse/inter/TSEListener;",
            ">(",
            "Ljava/lang/String;",
            "Z",
            "Ljava/lang/Class<",
            "TT;>;)TT;"
        }
    .end annotation

    .line 1125
    iget-object v0, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable;->j:Lee/cyber/smartid/tse/inter/ListenerAccess;

    if-nez v0, :cond_0

    const/4 p1, 0x0

    return-object p1

    .line 1128
    :cond_0
    invoke-interface {v0, p1, p2, p3}, Lee/cyber/smartid/tse/inter/ListenerAccess;->getListener(Ljava/lang/String;ZLjava/lang/Class;)Lee/cyber/smartid/tse/inter/TSEListener;

    move-result-object p1

    return-object p1
.end method

.method static synthetic a(Lee/cyber/smartid/tse/util/KeyStateRunnable;)Ljava/lang/String;
    .locals 0

    .line 68
    iget-object p0, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable;->c:Ljava/lang/String;

    return-object p0
.end method

.method private a(Lee/cyber/smartid/tse/dto/TSEError;Lee/cyber/smartid/tse/dto/KeyStateMeta;)V
    .locals 5

    .line 761
    :try_start_0
    iget-object v0, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable;->e:Lee/cyber/smartid/tse/dto/KeyState;

    invoke-virtual {v0}, Lee/cyber/smartid/tse/dto/KeyState;->isRollbackAllowed()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0, p1}, Lee/cyber/smartid/tse/util/KeyStateRunnable;->b(Lee/cyber/smartid/tse/dto/TSEError;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 762
    iget-object v0, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable;->o:Lee/cyber/smartid/tse/inter/LogAccess;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onKeyStateSolveFailed - rollback for "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable;->e:Lee/cyber/smartid/tse/dto/KeyState;

    invoke-virtual {v2}, Lee/cyber/smartid/tse/dto/KeyState;->getHumanReadableNameForType()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, " error "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lee/cyber/smartid/tse/dto/TSEError;->getCode()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v2, "!"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lee/cyber/smartid/tse/inter/LogAccess;->d(Ljava/lang/String;)V

    .line 764
    iget-object v0, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable;->i:Lee/cyber/smartid/tse/inter/KeyStorageAccess;

    iget-object v1, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable;->e:Lee/cyber/smartid/tse/dto/KeyState;

    invoke-virtual {v1}, Lee/cyber/smartid/tse/dto/KeyState;->getId()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable;->e:Lee/cyber/smartid/tse/dto/KeyState;

    invoke-virtual {v2}, Lee/cyber/smartid/tse/dto/KeyState;->getKeyId()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable;->e:Lee/cyber/smartid/tse/dto/KeyState;

    invoke-virtual {v3}, Lee/cyber/smartid/tse/dto/KeyState;->getAccountUUID()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable;->e:Lee/cyber/smartid/tse/dto/KeyState;

    invoke-virtual {v4}, Lee/cyber/smartid/tse/dto/KeyState;->getKeyType()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v0, v1, v2, v3, v4}, Lee/cyber/smartid/tse/inter/KeyStorageAccess;->rollbackKeyStateOperation(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 765
    :cond_0
    invoke-direct {p0, p1}, Lee/cyber/smartid/tse/util/KeyStateRunnable;->b(Lee/cyber/smartid/tse/dto/TSEError;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 766
    iget-object v0, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable;->o:Lee/cyber/smartid/tse/inter/LogAccess;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onKeyStateSolveFailed - fatal error for "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable;->e:Lee/cyber/smartid/tse/dto/KeyState;

    invoke-virtual {v2}, Lee/cyber/smartid/tse/dto/KeyState;->getHumanReadableNameForType()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, ", no rollback allowed!"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lee/cyber/smartid/tse/inter/LogAccess;->d(Ljava/lang/String;)V

    .line 769
    :cond_1
    :goto_0
    iget-object v0, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable;->m:Lee/cyber/smartid/tse/inter/WallClock;

    iget-object v1, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable;->e:Lee/cyber/smartid/tse/dto/KeyState;

    invoke-virtual {v1}, Lee/cyber/smartid/tse/dto/KeyState;->getKeyType()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, p1}, Lee/cyber/smartid/tse/util/KeyStateRunnable;->c(Lee/cyber/smartid/tse/dto/TSEError;)Z

    move-result v2

    invoke-virtual {p2, v0, v1, p1, v2}, Lee/cyber/smartid/tse/dto/KeyStateMeta;->updateKeyStatusFrom(Lee/cyber/smartid/tse/inter/WallClock;Ljava/lang/String;Lee/cyber/smartid/tse/dto/TSEError;Z)V

    .line 770
    invoke-virtual {p2, p1}, Lee/cyber/smartid/tse/dto/KeyStateMeta;->updateLastErrorTypeForm(Lee/cyber/smartid/tse/dto/TSEError;)V

    .line 771
    iget-object p1, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable;->i:Lee/cyber/smartid/tse/inter/KeyStorageAccess;

    invoke-interface {p1, p2}, Lee/cyber/smartid/tse/inter/KeyStorageAccess;->updateKeyStateMeta(Lee/cyber/smartid/tse/dto/KeyStateMeta;)V
    :try_end_0
    .catch Lee/cyber/smartid/cryptolib/dto/StorageException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception p1

    .line 774
    iget-object p2, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable;->o:Lee/cyber/smartid/tse/inter/LogAccess;

    const-string v0, "onKeyStateSolveFailed"

    invoke-interface {p2, v0, p1}, Lee/cyber/smartid/tse/inter/LogAccess;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    :goto_1
    return-void
.end method

.method static synthetic a(Lee/cyber/smartid/tse/util/KeyStateRunnable;Ljava/lang/String;Lee/cyber/smartid/tse/inter/TSEListener;Lee/cyber/smartid/tse/dto/TSEError;)V
    .locals 0

    .line 68
    invoke-direct {p0, p1, p2, p3}, Lee/cyber/smartid/tse/util/KeyStateRunnable;->a(Ljava/lang/String;Lee/cyber/smartid/tse/inter/TSEListener;Lee/cyber/smartid/tse/dto/TSEError;)V

    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 4

    .line 1025
    iget-object v0, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable;->i:Lee/cyber/smartid/tse/inter/KeyStorageAccess;

    invoke-interface {v0, p1}, Lee/cyber/smartid/tse/inter/KeyStorageAccess;->getKeyStateById(Ljava/lang/String;)Lee/cyber/smartid/tse/dto/KeyState;

    move-result-object p1

    if-eqz p1, :cond_4

    .line 1026
    invoke-virtual {p1}, Lee/cyber/smartid/tse/dto/KeyState;->isInActiveState()Z

    move-result v0

    if-nez v0, :cond_0

    goto/16 :goto_2

    .line 1035
    :cond_0
    iget-object v0, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable;->i:Lee/cyber/smartid/tse/inter/KeyStorageAccess;

    iget-object v1, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable;->d:Lee/cyber/smartid/tse/dto/ProtoKeyState;

    invoke-virtual {v1}, Lee/cyber/smartid/tse/dto/ProtoKeyState;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lee/cyber/smartid/tse/inter/KeyStorageAccess;->getKeyStateMetaByKeyStateId(Ljava/lang/String;)Lee/cyber/smartid/tse/dto/KeyStateMeta;

    move-result-object v0

    if-nez v0, :cond_1

    .line 1037
    iget-object v0, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable;->o:Lee/cyber/smartid/tse/inter/LogAccess;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "handleAlarmIfNeeded: Unable to set an alarm for state "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string p1, ", KeyStateMeta is null!"

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, p1}, Lee/cyber/smartid/tse/inter/LogAccess;->w(Ljava/lang/String;)V

    return-void

    .line 1043
    :cond_1
    invoke-static {}, Lee/cyber/smartid/tse/util/KeyStateRunnable;->getFirstLongTermRetryCount()I

    move-result v1

    .line 1045
    invoke-virtual {v0}, Lee/cyber/smartid/tse/dto/KeyStateMeta;->isInLongTermRetryMode()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {v0}, Lee/cyber/smartid/tse/dto/KeyStateMeta;->getCurrentRetry()I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_2
    move v2, v1

    :goto_0
    invoke-virtual {v0, v2}, Lee/cyber/smartid/tse/dto/KeyStateMeta;->setCurrentRetry(I)V

    .line 1048
    invoke-virtual {v0}, Lee/cyber/smartid/tse/dto/KeyStateMeta;->getCurrentRetry()I

    move-result v2

    invoke-static {v2, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    invoke-virtual {v0, v1}, Lee/cyber/smartid/tse/dto/KeyStateMeta;->setCurrentRetry(I)V

    .line 1052
    :try_start_0
    iget-object v1, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable;->i:Lee/cyber/smartid/tse/inter/KeyStorageAccess;

    invoke-interface {v1, v0}, Lee/cyber/smartid/tse/inter/KeyStorageAccess;->updateKeyStateMeta(Lee/cyber/smartid/tse/dto/KeyStateMeta;)V
    :try_end_0
    .catch Lee/cyber/smartid/cryptolib/dto/StorageException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v1

    .line 1054
    iget-object v2, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable;->o:Lee/cyber/smartid/tse/inter/LogAccess;

    const-string v3, "handleAlarmIfNeeded"

    invoke-interface {v2, v3, v1}, Lee/cyber/smartid/tse/inter/LogAccess;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1058
    :goto_1
    iget-object v1, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable;->k:Lee/cyber/smartid/tse/SmartIdTSE$TSEAlarmAccess;

    invoke-virtual {v1, p1, v0}, Lee/cyber/smartid/tse/SmartIdTSE$TSEAlarmAccess;->setKeyStateSolveAlarm(Lee/cyber/smartid/tse/dto/KeyState;Lee/cyber/smartid/tse/dto/KeyStateMeta;)V

    .line 1060
    iget-object v1, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable;->m:Lee/cyber/smartid/tse/inter/WallClock;

    invoke-virtual {v0, v1}, Lee/cyber/smartid/tse/dto/KeyStateMeta;->isKeyLocked(Lee/cyber/smartid/tse/inter/WallClock;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1061
    iget-object v1, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable;->o:Lee/cyber/smartid/tse/inter/LogAccess;

    const-string v2, "handleAlarmIfNeeded: Key is currently locked"

    invoke-interface {v1, v2}, Lee/cyber/smartid/tse/inter/LogAccess;->d(Ljava/lang/String;)V

    .line 1063
    :cond_3
    iget-object v1, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable;->o:Lee/cyber/smartid/tse/inter/LogAccess;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "handleAlarmIfNeeded: Alarm (retry #"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Lee/cyber/smartid/tse/dto/KeyStateMeta;->getCurrentRetry()I

    move-result v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v0, ") scheduled for "

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-interface {v1, p1}, Lee/cyber/smartid/tse/inter/LogAccess;->d(Ljava/lang/String;)V

    return-void

    .line 1029
    :cond_4
    :goto_2
    iget-object v0, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable;->o:Lee/cyber/smartid/tse/inter/LogAccess;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "handleAlarmIfNeeded: No need for a retry alarm for "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lee/cyber/smartid/tse/inter/LogAccess;->d(Ljava/lang/String;)V

    if-eqz p1, :cond_5

    .line 1031
    iget-object v0, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable;->k:Lee/cyber/smartid/tse/SmartIdTSE$TSEAlarmAccess;

    invoke-virtual {p1}, Lee/cyber/smartid/tse/dto/KeyState;->getId()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lee/cyber/smartid/tse/SmartIdTSE$TSEAlarmAccess;->clearKeyStateSolveAlarm(Ljava/lang/String;)V

    :cond_5
    return-void
.end method

.method private a(Ljava/lang/String;Lee/cyber/smartid/tse/dto/TSEError;)V
    .locals 2

    .line 968
    iget-object v0, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable;->l:Landroid/os/Handler;

    new-instance v1, Lee/cyber/smartid/tse/util/KeyStateRunnable$16;

    invoke-direct {v1, p0, p1, p2}, Lee/cyber/smartid/tse/util/KeyStateRunnable$16;-><init>(Lee/cyber/smartid/tse/util/KeyStateRunnable;Ljava/lang/String;Lee/cyber/smartid/tse/dto/TSEError;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method private a(Ljava/lang/String;Lee/cyber/smartid/tse/inter/TSEListener;Lee/cyber/smartid/tse/dto/TSEError;)V
    .locals 1

    .line 1152
    iget-object v0, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable;->j:Lee/cyber/smartid/tse/inter/ListenerAccess;

    if-eqz v0, :cond_0

    .line 1153
    invoke-interface {v0, p1, p2, p3}, Lee/cyber/smartid/tse/inter/ListenerAccess;->notifyError(Ljava/lang/String;Lee/cyber/smartid/tse/inter/TSEListener;Lee/cyber/smartid/tse/dto/TSEError;)V

    :cond_0
    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/tse/dto/TSEError;)V
    .locals 2

    .line 993
    iget-object v0, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable;->l:Landroid/os/Handler;

    new-instance v1, Lee/cyber/smartid/tse/util/KeyStateRunnable$18;

    invoke-direct {v1, p0, p3, p1, p2}, Lee/cyber/smartid/tse/util/KeyStateRunnable$18;-><init>(Lee/cyber/smartid/tse/util/KeyStateRunnable;Lee/cyber/smartid/tse/dto/TSEError;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method private a()Z
    .locals 14

    .line 363
    iget-object v0, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable;->o:Lee/cyber/smartid/tse/inter/LogAccess;

    const-string v1, "handleSign"

    invoke-interface {v0, v1}, Lee/cyber/smartid/tse/inter/LogAccess;->d(Ljava/lang/String;)V

    .line 370
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v2, 0x0

    invoke-direct {v0, v2}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    .line 376
    :cond_0
    invoke-direct {p0}, Lee/cyber/smartid/tse/util/KeyStateRunnable;->d()Ljava/lang/String;

    move-result-object v3

    .line 377
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    const/4 v5, 0x0

    if-eqz v4, :cond_1

    .line 379
    iget-object v3, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable;->o:Lee/cyber/smartid/tse/inter/LogAccess;

    const-string v4, "handleSign - we are missing the freshnessToken, lets get one  .."

    invoke-interface {v3, v4}, Lee/cyber/smartid/tse/inter/LogAccess;->d(Ljava/lang/String;)V

    .line 380
    invoke-direct {p0, v0}, Lee/cyber/smartid/tse/util/KeyStateRunnable;->a(Ljava/util/concurrent/atomic/AtomicInteger;)Lee/cyber/smartid/tse/dto/TSEError;

    move-result-object v3

    .line 381
    invoke-direct {p0}, Lee/cyber/smartid/tse/util/KeyStateRunnable;->d()Ljava/lang/String;

    move-result-object v4

    move-object v10, v4

    goto :goto_0

    :cond_1
    move-object v10, v3

    move-object v3, v5

    .line 384
    :goto_0
    invoke-static {v10}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    const/4 v12, 0x3

    if-eqz v4, :cond_2

    iget-object v4, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable;->f:Lee/cyber/smartid/tse/dto/KeyStateMeta;

    invoke-virtual {v4}, Lee/cyber/smartid/tse/dto/KeyStateMeta;->isInLongTermRetryMode()Z

    move-result v4

    if-nez v4, :cond_2

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    move-result v4

    if-gt v4, v12, :cond_2

    .line 386
    iget-object v4, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable;->o:Lee/cyber/smartid/tse/inter/LogAccess;

    const-string v6, "handleSign - someone took our freshnessToken, lets get another one  .."

    invoke-interface {v4, v6}, Lee/cyber/smartid/tse/inter/LogAccess;->d(Ljava/lang/String;)V

    .line 388
    invoke-direct {p0, v3}, Lee/cyber/smartid/tse/util/KeyStateRunnable;->a(Lee/cyber/smartid/tse/dto/TSEError;)Lee/cyber/smartid/tse/dto/TSEError;

    move-result-object v3

    goto/16 :goto_2

    .line 390
    :cond_2
    invoke-static {v10}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 392
    iget-object v0, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable;->o:Lee/cyber/smartid/tse/inter/LogAccess;

    const-string v4, "handleSign - unable to get a freshnessToken, aborting .."

    invoke-interface {v0, v4}, Lee/cyber/smartid/tse/inter/LogAccess;->d(Ljava/lang/String;)V

    .line 394
    invoke-direct {p0, v3}, Lee/cyber/smartid/tse/util/KeyStateRunnable;->a(Lee/cyber/smartid/tse/dto/TSEError;)Lee/cyber/smartid/tse/dto/TSEError;

    move-result-object v0

    goto/16 :goto_3

    .line 401
    :cond_3
    new-instance v3, Lee/cyber/smartid/tse/dto/jsonrpc/param/SubmitSignatureParams;

    iget-object v4, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable;->e:Lee/cyber/smartid/tse/dto/KeyState;

    invoke-virtual {v4}, Lee/cyber/smartid/tse/dto/KeyState;->getAccountUUID()Ljava/lang/String;

    move-result-object v7

    iget-object v4, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable;->e:Lee/cyber/smartid/tse/dto/KeyState;

    invoke-virtual {v4}, Lee/cyber/smartid/tse/dto/KeyState;->getPayload()Ljava/lang/String;

    move-result-object v8

    iget-object v4, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable;->e:Lee/cyber/smartid/tse/dto/KeyState;

    invoke-virtual {v4}, Lee/cyber/smartid/tse/dto/KeyState;->getPayloadEncoding()Ljava/lang/String;

    move-result-object v9

    iget-object v4, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable;->e:Lee/cyber/smartid/tse/dto/KeyState;

    invoke-virtual {v4}, Lee/cyber/smartid/tse/dto/KeyState;->getTransactionUUID()Ljava/lang/String;

    move-result-object v11

    move-object v6, v3

    invoke-direct/range {v6 .. v11}, Lee/cyber/smartid/tse/dto/jsonrpc/param/SubmitSignatureParams;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 402
    new-instance v4, Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCRequest;

    const-string v6, "submitSignature"

    invoke-direct {v4, v6, v3}, Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCRequest;-><init>(Ljava/lang/String;Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCParams;)V

    .line 403
    iget-object v3, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable;->b:Lee/cyber/smartid/tse/network/TSEAPI$TSEAPIService;

    iget-object v6, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable;->n:Landroid/os/Bundle;

    invoke-static {v6, v0}, Lee/cyber/smartid/tse/util/Util;->a(Landroid/os/Bundle;Ljava/util/concurrent/atomic/AtomicInteger;)Ljava/lang/String;

    move-result-object v6

    invoke-interface {v3, v6, v4}, Lee/cyber/smartid/tse/network/TSEAPI$TSEAPIService;->submitSignature(Ljava/lang/String;Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCRequest;)Lc/b;

    move-result-object v3

    .line 407
    :try_start_0
    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v6

    if-lez v6, :cond_4

    .line 408
    iget-object v6, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable;->o:Lee/cyber/smartid/tse/inter/LogAccess;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "handleSign - overall retries at "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v8, "/"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v6, v7}, Lee/cyber/smartid/tse/inter/LogAccess;->d(Ljava/lang/String;)V

    .line 410
    :cond_4
    invoke-interface {v3}, Lc/b;->a()Lc/r;

    move-result-object v3
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v13, v5

    move-object v5, v3

    move-object v3, v13

    goto :goto_1

    :catch_0
    move-exception v3

    .line 416
    iget-object v6, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable;->o:Lee/cyber/smartid/tse/inter/LogAccess;

    invoke-interface {v6, v1, v3}, Lee/cyber/smartid/tse/inter/LogAccess;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    :catch_1
    move-exception v3

    .line 413
    iget-object v6, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable;->o:Lee/cyber/smartid/tse/inter/LogAccess;

    invoke-interface {v6, v1, v3}, Lee/cyber/smartid/tse/inter/LogAccess;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 418
    :goto_1
    new-instance v6, Lee/cyber/smartid/tse/util/KeyStateRunnable$9;

    invoke-direct {v6, p0}, Lee/cyber/smartid/tse/util/KeyStateRunnable$9;-><init>(Lee/cyber/smartid/tse/util/KeyStateRunnable;)V

    invoke-direct {p0, v4, v5, v3, v6}, Lee/cyber/smartid/tse/util/KeyStateRunnable;->a(Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCRequest;Lc/r;Ljava/lang/Throwable;Lee/cyber/smartid/tse/util/KeyStateRunnable$ValidateResponseCallback;)Lee/cyber/smartid/tse/dto/TSEError;

    move-result-object v3

    :goto_2
    if-eqz v3, :cond_5

    .line 425
    iget-object v4, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable;->f:Lee/cyber/smartid/tse/dto/KeyStateMeta;

    invoke-direct {p0, v4, v3}, Lee/cyber/smartid/tse/util/KeyStateRunnable;->a(Lee/cyber/smartid/tse/dto/KeyStateMeta;Lee/cyber/smartid/tse/dto/TSEError;)Z

    move-result v4

    if-eqz v4, :cond_5

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    move-result v4

    if-le v4, v12, :cond_0

    :cond_5
    move-object v0, v3

    .line 427
    :goto_3
    invoke-direct {p0, v5, v0}, Lee/cyber/smartid/tse/util/KeyStateRunnable;->a(Lc/r;Lee/cyber/smartid/tse/dto/TSEError;)Lee/cyber/smartid/tse/dto/TSEError;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 431
    iget-object v1, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable;->f:Lee/cyber/smartid/tse/dto/KeyStateMeta;

    invoke-direct {p0, v0, v1}, Lee/cyber/smartid/tse/util/KeyStateRunnable;->a(Lee/cyber/smartid/tse/dto/TSEError;Lee/cyber/smartid/tse/dto/KeyStateMeta;)V

    .line 432
    iget-object v1, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable;->c:Ljava/lang/String;

    invoke-direct {p0, v1, v0}, Lee/cyber/smartid/tse/util/KeyStateRunnable;->a(Ljava/lang/String;Lee/cyber/smartid/tse/dto/TSEError;)V

    return v2

    .line 439
    :cond_6
    :try_start_1
    iget-object v6, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable;->i:Lee/cyber/smartid/tse/inter/KeyStorageAccess;

    iget-object v3, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable;->e:Lee/cyber/smartid/tse/dto/KeyState;

    invoke-virtual {v3}, Lee/cyber/smartid/tse/dto/KeyState;->getId()Ljava/lang/String;

    move-result-object v7

    iget-object v3, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable;->e:Lee/cyber/smartid/tse/dto/KeyState;

    invoke-virtual {v3}, Lee/cyber/smartid/tse/dto/KeyState;->getKeyId()Ljava/lang/String;

    move-result-object v8

    iget-object v3, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable;->e:Lee/cyber/smartid/tse/dto/KeyState;

    invoke-virtual {v3}, Lee/cyber/smartid/tse/dto/KeyState;->getAccountUUID()Ljava/lang/String;

    move-result-object v9

    iget-object v3, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable;->e:Lee/cyber/smartid/tse/dto/KeyState;

    invoke-virtual {v3}, Lee/cyber/smartid/tse/dto/KeyState;->getKeyType()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v5}, Lc/r;->e()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCResponse;

    invoke-virtual {v3}, Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCResponse;->getResult()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lee/cyber/smartid/tse/dto/jsonrpc/result/SubmitSignatureResult;

    invoke-virtual {v3}, Lee/cyber/smartid/tse/dto/jsonrpc/result/SubmitSignatureResult;->getResponseData()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v5}, Lc/r;->e()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCResponse;

    invoke-virtual {v3}, Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCResponse;->getResult()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lee/cyber/smartid/tse/dto/jsonrpc/result/SubmitSignatureResult;

    invoke-virtual {v3}, Lee/cyber/smartid/tse/dto/jsonrpc/result/SubmitSignatureResult;->getResponseDataEncoding()Ljava/lang/String;

    move-result-object v12

    invoke-interface/range {v6 .. v12}, Lee/cyber/smartid/tse/inter/KeyStorageAccess;->finishKeyStateOperation(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Lee/cyber/smartid/cryptolib/dto/StorageException; {:try_start_1 .. :try_end_1} :catch_2

    .line 448
    invoke-direct {p0, v5, v0}, Lee/cyber/smartid/tse/util/KeyStateRunnable;->b(Lc/r;Lee/cyber/smartid/tse/dto/TSEError;)Lee/cyber/smartid/tse/dto/TSEError;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 451
    iget-object v1, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable;->c:Ljava/lang/String;

    invoke-direct {p0, v1, v0}, Lee/cyber/smartid/tse/util/KeyStateRunnable;->a(Ljava/lang/String;Lee/cyber/smartid/tse/dto/TSEError;)V

    goto :goto_4

    .line 454
    :cond_7
    iget-object v0, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable;->e:Lee/cyber/smartid/tse/dto/KeyState;

    invoke-virtual {v0}, Lee/cyber/smartid/tse/dto/KeyState;->getTransactionUUID()Ljava/lang/String;

    move-result-object v0

    .line 455
    new-instance v1, Lee/cyber/smartid/tse/dto/jsonrpc/resp/ConfirmTransactionResp;

    iget-object v2, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable;->c:Ljava/lang/String;

    invoke-direct {v1, v0, v2}, Lee/cyber/smartid/tse/dto/jsonrpc/resp/ConfirmTransactionResp;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 456
    iget-object v0, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable;->l:Landroid/os/Handler;

    new-instance v2, Lee/cyber/smartid/tse/util/KeyStateRunnable$10;

    invoke-direct {v2, p0, v1}, Lee/cyber/smartid/tse/util/KeyStateRunnable$10;-><init>(Lee/cyber/smartid/tse/util/KeyStateRunnable;Lee/cyber/smartid/tse/dto/jsonrpc/resp/ConfirmTransactionResp;)V

    invoke-virtual {v0, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :goto_4
    const/4 v0, 0x1

    return v0

    :catch_2
    move-exception v0

    .line 441
    iget-object v3, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable;->o:Lee/cyber/smartid/tse/inter/LogAccess;

    invoke-interface {v3, v1, v0}, Lee/cyber/smartid/tse/inter/LogAccess;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 443
    iget-object v1, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable;->c:Ljava/lang/String;

    iget-object v3, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable;->g:Lee/cyber/smartid/tse/inter/ResourceAccess;

    invoke-interface {v3}, Lee/cyber/smartid/tse/inter/ResourceAccess;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    iget-object v4, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable;->m:Lee/cyber/smartid/tse/inter/WallClock;

    invoke-static {v3, v4, v0}, Lee/cyber/smartid/tse/dto/TSEError;->from(Landroid/content/Context;Lee/cyber/smartid/tse/inter/WallClock;Ljava/lang/Throwable;)Lee/cyber/smartid/tse/dto/TSEError;

    move-result-object v0

    invoke-direct {p0, v1, v0}, Lee/cyber/smartid/tse/util/KeyStateRunnable;->a(Ljava/lang/String;Lee/cyber/smartid/tse/dto/TSEError;)V

    return v2
.end method

.method private a(Lee/cyber/smartid/tse/dto/KeyStateMeta;Lee/cyber/smartid/tse/dto/TSEError;)Z
    .locals 7

    .line 871
    iget-object v0, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable;->e:Lee/cyber/smartid/tse/dto/KeyState;

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lee/cyber/smartid/tse/dto/KeyStateMeta;->isInLongTermRetryMode()Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_0
    if-nez p2, :cond_2

    :cond_1
    const/4 v0, 0x1

    goto :goto_1

    .line 877
    :cond_2
    invoke-virtual {p2}, Lee/cyber/smartid/tse/dto/TSEError;->getCode()J

    move-result-wide v3

    const-wide/16 v5, -0x7920

    cmp-long v0, v3, v5

    if-nez v0, :cond_3

    goto :goto_0

    .line 880
    :cond_3
    invoke-virtual {p2}, Lee/cyber/smartid/tse/dto/TSEError;->getCode()J

    move-result-wide v3

    const-wide/16 v5, 0x44c

    cmp-long v0, v3, v5

    if-nez v0, :cond_4

    goto :goto_0

    .line 883
    :cond_4
    invoke-virtual {p2}, Lee/cyber/smartid/tse/dto/TSEError;->getCode()J

    move-result-wide v3

    const-wide/16 v5, -0x7fbc

    cmp-long v0, v3, v5

    if-nez v0, :cond_5

    goto :goto_0

    .line 886
    :cond_5
    invoke-virtual {p2}, Lee/cyber/smartid/tse/dto/TSEError;->getCode()J

    move-result-wide v3

    const-wide/16 v5, -0x7f58

    cmp-long v0, v3, v5

    if-nez v0, :cond_6

    goto :goto_0

    .line 889
    :cond_6
    invoke-virtual {p2}, Lee/cyber/smartid/tse/dto/TSEError;->getCode()J

    move-result-wide v3

    const-wide/16 v5, -0x7f59

    cmp-long v0, v3, v5

    if-nez v0, :cond_7

    goto :goto_0

    .line 892
    :cond_7
    invoke-virtual {p2}, Lee/cyber/smartid/tse/dto/TSEError;->getCode()J

    move-result-wide v3

    const-wide/16 v5, -0x7f5a

    cmp-long v0, v3, v5

    if-nez v0, :cond_8

    goto :goto_0

    .line 895
    :cond_8
    invoke-direct {p0, p2}, Lee/cyber/smartid/tse/util/KeyStateRunnable;->b(Lee/cyber/smartid/tse/dto/TSEError;)Z

    move-result v0

    if-eqz v0, :cond_1

    goto :goto_0

    .line 899
    :goto_1
    iget-object v3, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable;->o:Lee/cyber/smartid/tse/inter/LogAccess;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "shouldRetry: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v5, ", isInLongTermRetryMode: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v5, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable;->e:Lee/cyber/smartid/tse/dto/KeyState;

    if-eqz v5, :cond_9

    invoke-virtual {p1}, Lee/cyber/smartid/tse/dto/KeyStateMeta;->isInLongTermRetryMode()Z

    move-result p1

    if-eqz p1, :cond_9

    goto :goto_2

    :cond_9
    const/4 v1, 0x0

    :goto_2
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string p1, ", smartIdError: "

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-interface {v3, p1}, Lee/cyber/smartid/tse/inter/LogAccess;->d(Ljava/lang/String;)V

    return v0
.end method

.method static synthetic a(Lee/cyber/smartid/tse/util/KeyStateRunnable;Lee/cyber/smartid/tse/dto/TSEError;)Z
    .locals 0

    .line 68
    invoke-direct {p0, p1}, Lee/cyber/smartid/tse/util/KeyStateRunnable;->b(Lee/cyber/smartid/tse/dto/TSEError;)Z

    move-result p0

    return p0
.end method

.method private b(Lc/r;Lee/cyber/smartid/tse/dto/TSEError;)Lee/cyber/smartid/tse/dto/TSEError;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lee/cyber/smartid/tse/dto/jsonrpc/result/RefreshCloneDetectionResult;",
            ">(",
            "Lc/r<",
            "Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCResponse<",
            "TT;>;>;",
            "Lee/cyber/smartid/tse/dto/TSEError;",
            ")",
            "Lee/cyber/smartid/tse/dto/TSEError;"
        }
    .end annotation

    if-nez p2, :cond_0

    .line 734
    invoke-virtual {p1}, Lc/r;->e()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lc/r;->e()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCResponse;

    invoke-virtual {v0}, Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCResponse;->getResult()Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Lee/cyber/smartid/tse/dto/jsonrpc/result/SubmitSignatureResult;

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lc/r;->e()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCResponse;

    invoke-virtual {v0}, Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCResponse;->getResult()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lee/cyber/smartid/tse/dto/jsonrpc/result/SubmitSignatureResult;

    invoke-virtual {v0}, Lee/cyber/smartid/tse/dto/jsonrpc/result/SubmitSignatureResult;->isSuccessfullySigned()Z

    move-result v0

    if-nez v0, :cond_0

    .line 736
    iget-object p2, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable;->o:Lee/cyber/smartid/tse/inter/LogAccess;

    const-string v0, "createErrorIfNoResults - Signature was attempted but it failed, creating a SmartIdError object"

    invoke-interface {p2, v0}, Lee/cyber/smartid/tse/inter/LogAccess;->d(Ljava/lang/String;)V

    .line 738
    invoke-virtual {p1}, Lc/r;->e()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCResponse;

    invoke-virtual {p1}, Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCResponse;->getResult()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lee/cyber/smartid/tse/dto/jsonrpc/result/SubmitSignatureResult;

    .line 739
    iget-object p2, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable;->m:Lee/cyber/smartid/tse/inter/WallClock;

    invoke-virtual {p1}, Lee/cyber/smartid/tse/dto/jsonrpc/result/SubmitSignatureResult;->getErrorCodeForResult()J

    move-result-wide v0

    iget-object v2, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable;->g:Lee/cyber/smartid/tse/inter/ResourceAccess;

    invoke-interface {v2}, Lee/cyber/smartid/tse/inter/ResourceAccess;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {p1, v2}, Lee/cyber/smartid/tse/dto/jsonrpc/result/SubmitSignatureResult;->getErrorMessageForResult(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-static {p2, v0, v1, v2}, Lee/cyber/smartid/tse/dto/TSEError;->from(Lee/cyber/smartid/tse/inter/WallClock;JLjava/lang/String;)Lee/cyber/smartid/tse/dto/TSEError;

    move-result-object p2

    .line 740
    invoke-virtual {p2}, Lee/cyber/smartid/tse/dto/TSEError;->createOrGetExtras()Landroid/os/Bundle;

    move-result-object v0

    invoke-virtual {p1}, Lee/cyber/smartid/tse/dto/jsonrpc/result/SubmitSignatureResult;->getKeyInfo()Lee/cyber/smartid/tse/dto/jsonrpc/AccountKeyStatus;

    move-result-object p1

    const-string v1, "ee.cyber.smartid.EXTRA_KEY_STATUS_INFO"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    :cond_0
    return-object p2
.end method

.method static synthetic b(Lee/cyber/smartid/tse/util/KeyStateRunnable;)Lee/cyber/smartid/tse/inter/WallClock;
    .locals 0

    .line 68
    iget-object p0, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable;->m:Lee/cyber/smartid/tse/inter/WallClock;

    return-object p0
.end method

.method private b(Ljava/lang/String;Lee/cyber/smartid/tse/dto/TSEError;)V
    .locals 2

    .line 981
    iget-object v0, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable;->l:Landroid/os/Handler;

    new-instance v1, Lee/cyber/smartid/tse/util/KeyStateRunnable$17;

    invoke-direct {v1, p0, p1, p2}, Lee/cyber/smartid/tse/util/KeyStateRunnable$17;-><init>(Lee/cyber/smartid/tse/util/KeyStateRunnable;Ljava/lang/String;Lee/cyber/smartid/tse/dto/TSEError;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method private b()Z
    .locals 14

    .line 471
    iget-object v0, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable;->o:Lee/cyber/smartid/tse/inter/LogAccess;

    const-string v1, "handleCloneDetection"

    invoke-interface {v0, v1}, Lee/cyber/smartid/tse/inter/LogAccess;->d(Ljava/lang/String;)V

    .line 478
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v2, 0x0

    invoke-direct {v0, v2}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    .line 484
    :cond_0
    invoke-direct {p0}, Lee/cyber/smartid/tse/util/KeyStateRunnable;->d()Ljava/lang/String;

    move-result-object v3

    .line 486
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    const/4 v5, 0x0

    if-eqz v4, :cond_1

    .line 488
    iget-object v3, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable;->o:Lee/cyber/smartid/tse/inter/LogAccess;

    const-string v4, "handleCloneDetection - we are missing the freshnessToken, lets get one  .."

    invoke-interface {v3, v4}, Lee/cyber/smartid/tse/inter/LogAccess;->d(Ljava/lang/String;)V

    .line 489
    invoke-direct {p0, v0}, Lee/cyber/smartid/tse/util/KeyStateRunnable;->a(Ljava/util/concurrent/atomic/AtomicInteger;)Lee/cyber/smartid/tse/dto/TSEError;

    move-result-object v3

    .line 490
    invoke-direct {p0}, Lee/cyber/smartid/tse/util/KeyStateRunnable;->d()Ljava/lang/String;

    move-result-object v4

    move-object v13, v4

    move-object v4, v3

    move-object v3, v13

    goto :goto_0

    :cond_1
    move-object v4, v5

    .line 493
    :goto_0
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    const/4 v7, 0x3

    if-eqz v6, :cond_2

    iget-object v6, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable;->f:Lee/cyber/smartid/tse/dto/KeyStateMeta;

    invoke-virtual {v6}, Lee/cyber/smartid/tse/dto/KeyStateMeta;->isInLongTermRetryMode()Z

    move-result v6

    if-nez v6, :cond_2

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    move-result v6

    if-gt v6, v7, :cond_2

    .line 495
    iget-object v3, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable;->o:Lee/cyber/smartid/tse/inter/LogAccess;

    const-string v6, "handleCloneDetection - someone took our freshnessToken, lets get another one  .."

    invoke-interface {v3, v6}, Lee/cyber/smartid/tse/inter/LogAccess;->d(Ljava/lang/String;)V

    .line 497
    invoke-direct {p0, v4}, Lee/cyber/smartid/tse/util/KeyStateRunnable;->a(Lee/cyber/smartid/tse/dto/TSEError;)Lee/cyber/smartid/tse/dto/TSEError;

    move-result-object v3

    goto/16 :goto_2

    .line 499
    :cond_2
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 501
    iget-object v0, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable;->o:Lee/cyber/smartid/tse/inter/LogAccess;

    const-string v3, "handleCloneDetection - unable to get a freshnessToken, aborting .."

    invoke-interface {v0, v3}, Lee/cyber/smartid/tse/inter/LogAccess;->d(Ljava/lang/String;)V

    .line 503
    invoke-direct {p0, v4}, Lee/cyber/smartid/tse/util/KeyStateRunnable;->a(Lee/cyber/smartid/tse/dto/TSEError;)Lee/cyber/smartid/tse/dto/TSEError;

    move-result-object v0

    goto/16 :goto_3

    .line 511
    :cond_3
    new-instance v4, Lee/cyber/smartid/tse/dto/jsonrpc/param/RefreshCloneDetectionParams;

    iget-object v6, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable;->e:Lee/cyber/smartid/tse/dto/KeyState;

    invoke-virtual {v6}, Lee/cyber/smartid/tse/dto/KeyState;->getAccountUUID()Ljava/lang/String;

    move-result-object v6

    iget-object v8, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable;->e:Lee/cyber/smartid/tse/dto/KeyState;

    invoke-virtual {v8}, Lee/cyber/smartid/tse/dto/KeyState;->getPayload()Ljava/lang/String;

    move-result-object v8

    iget-object v9, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable;->e:Lee/cyber/smartid/tse/dto/KeyState;

    invoke-virtual {v9}, Lee/cyber/smartid/tse/dto/KeyState;->getPayloadEncoding()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v4, v6, v8, v9, v3}, Lee/cyber/smartid/tse/dto/jsonrpc/param/RefreshCloneDetectionParams;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 512
    new-instance v3, Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCRequest;

    const-string v6, "refreshCloneDetection"

    invoke-direct {v3, v6, v4}, Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCRequest;-><init>(Ljava/lang/String;Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCParams;)V

    .line 513
    iget-object v4, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable;->b:Lee/cyber/smartid/tse/network/TSEAPI$TSEAPIService;

    iget-object v6, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable;->n:Landroid/os/Bundle;

    invoke-static {v6, v0}, Lee/cyber/smartid/tse/util/Util;->a(Landroid/os/Bundle;Ljava/util/concurrent/atomic/AtomicInteger;)Ljava/lang/String;

    move-result-object v6

    invoke-interface {v4, v6, v3}, Lee/cyber/smartid/tse/network/TSEAPI$TSEAPIService;->refreshCloneDetection(Ljava/lang/String;Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCRequest;)Lc/b;

    move-result-object v4

    .line 517
    :try_start_0
    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v6

    if-lez v6, :cond_4

    .line 518
    iget-object v6, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable;->o:Lee/cyber/smartid/tse/inter/LogAccess;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "handleSCloneDetection - overall retries at "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v9, "/"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-interface {v6, v8}, Lee/cyber/smartid/tse/inter/LogAccess;->d(Ljava/lang/String;)V

    .line 520
    :cond_4
    invoke-interface {v4}, Lc/b;->a()Lc/r;

    move-result-object v4
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v13, v5

    move-object v5, v4

    move-object v4, v13

    goto :goto_1

    :catch_0
    move-exception v4

    .line 526
    iget-object v6, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable;->o:Lee/cyber/smartid/tse/inter/LogAccess;

    invoke-interface {v6, v1, v4}, Lee/cyber/smartid/tse/inter/LogAccess;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    :catch_1
    move-exception v4

    .line 523
    iget-object v6, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable;->o:Lee/cyber/smartid/tse/inter/LogAccess;

    invoke-interface {v6, v1, v4}, Lee/cyber/smartid/tse/inter/LogAccess;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 528
    :goto_1
    new-instance v6, Lee/cyber/smartid/tse/util/KeyStateRunnable$11;

    invoke-direct {v6, p0}, Lee/cyber/smartid/tse/util/KeyStateRunnable$11;-><init>(Lee/cyber/smartid/tse/util/KeyStateRunnable;)V

    invoke-direct {p0, v3, v5, v4, v6}, Lee/cyber/smartid/tse/util/KeyStateRunnable;->a(Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCRequest;Lc/r;Ljava/lang/Throwable;Lee/cyber/smartid/tse/util/KeyStateRunnable$ValidateResponseCallback;)Lee/cyber/smartid/tse/dto/TSEError;

    move-result-object v3

    :goto_2
    if-eqz v3, :cond_5

    .line 535
    iget-object v4, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable;->f:Lee/cyber/smartid/tse/dto/KeyStateMeta;

    invoke-direct {p0, v4, v3}, Lee/cyber/smartid/tse/util/KeyStateRunnable;->a(Lee/cyber/smartid/tse/dto/KeyStateMeta;Lee/cyber/smartid/tse/dto/TSEError;)Z

    move-result v4

    if-eqz v4, :cond_5

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    move-result v4

    if-le v4, v7, :cond_0

    :cond_5
    move-object v0, v3

    .line 538
    :goto_3
    invoke-direct {p0, v5, v0}, Lee/cyber/smartid/tse/util/KeyStateRunnable;->a(Lc/r;Lee/cyber/smartid/tse/dto/TSEError;)Lee/cyber/smartid/tse/dto/TSEError;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 542
    iget-object v1, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable;->f:Lee/cyber/smartid/tse/dto/KeyStateMeta;

    invoke-direct {p0, v0, v1}, Lee/cyber/smartid/tse/util/KeyStateRunnable;->a(Lee/cyber/smartid/tse/dto/TSEError;Lee/cyber/smartid/tse/dto/KeyStateMeta;)V

    .line 543
    iget-object v1, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable;->c:Ljava/lang/String;

    invoke-direct {p0, v1, v0}, Lee/cyber/smartid/tse/util/KeyStateRunnable;->b(Ljava/lang/String;Lee/cyber/smartid/tse/dto/TSEError;)V

    return v2

    .line 550
    :cond_6
    :try_start_1
    iget-object v6, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable;->i:Lee/cyber/smartid/tse/inter/KeyStorageAccess;

    iget-object v3, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable;->e:Lee/cyber/smartid/tse/dto/KeyState;

    invoke-virtual {v3}, Lee/cyber/smartid/tse/dto/KeyState;->getId()Ljava/lang/String;

    move-result-object v7

    iget-object v3, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable;->e:Lee/cyber/smartid/tse/dto/KeyState;

    invoke-virtual {v3}, Lee/cyber/smartid/tse/dto/KeyState;->getKeyId()Ljava/lang/String;

    move-result-object v8

    iget-object v3, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable;->e:Lee/cyber/smartid/tse/dto/KeyState;

    invoke-virtual {v3}, Lee/cyber/smartid/tse/dto/KeyState;->getAccountUUID()Ljava/lang/String;

    move-result-object v9

    iget-object v3, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable;->e:Lee/cyber/smartid/tse/dto/KeyState;

    invoke-virtual {v3}, Lee/cyber/smartid/tse/dto/KeyState;->getKeyType()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v5}, Lc/r;->e()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCResponse;

    invoke-virtual {v3}, Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCResponse;->getResult()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lee/cyber/smartid/tse/dto/jsonrpc/result/RefreshCloneDetectionResult;

    invoke-virtual {v3}, Lee/cyber/smartid/tse/dto/jsonrpc/result/RefreshCloneDetectionResult;->getResponseData()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v5}, Lc/r;->e()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCResponse;

    invoke-virtual {v3}, Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCResponse;->getResult()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lee/cyber/smartid/tse/dto/jsonrpc/result/RefreshCloneDetectionResult;

    invoke-virtual {v3}, Lee/cyber/smartid/tse/dto/jsonrpc/result/RefreshCloneDetectionResult;->getResponseDataEncoding()Ljava/lang/String;

    move-result-object v12

    invoke-interface/range {v6 .. v12}, Lee/cyber/smartid/tse/inter/KeyStorageAccess;->finishKeyStateOperation(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Lee/cyber/smartid/cryptolib/dto/StorageException; {:try_start_1 .. :try_end_1} :catch_2

    .line 559
    invoke-direct {p0, v5, v0}, Lee/cyber/smartid/tse/util/KeyStateRunnable;->b(Lc/r;Lee/cyber/smartid/tse/dto/TSEError;)Lee/cyber/smartid/tse/dto/TSEError;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 562
    iget-object v1, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable;->c:Ljava/lang/String;

    invoke-direct {p0, v1, v0}, Lee/cyber/smartid/tse/util/KeyStateRunnable;->b(Ljava/lang/String;Lee/cyber/smartid/tse/dto/TSEError;)V

    goto :goto_4

    .line 564
    :cond_7
    new-instance v0, Lee/cyber/smartid/tse/dto/jsonrpc/resp/RefreshCloneDetectionResp;

    iget-object v1, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable;->c:Ljava/lang/String;

    invoke-direct {v0, v1}, Lee/cyber/smartid/tse/dto/jsonrpc/resp/RefreshCloneDetectionResp;-><init>(Ljava/lang/String;)V

    .line 565
    iget-object v1, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable;->l:Landroid/os/Handler;

    new-instance v2, Lee/cyber/smartid/tse/util/KeyStateRunnable$12;

    invoke-direct {v2, p0, v0}, Lee/cyber/smartid/tse/util/KeyStateRunnable$12;-><init>(Lee/cyber/smartid/tse/util/KeyStateRunnable;Lee/cyber/smartid/tse/dto/jsonrpc/resp/RefreshCloneDetectionResp;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :goto_4
    const/4 v0, 0x1

    return v0

    :catch_2
    move-exception v0

    .line 552
    iget-object v3, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable;->o:Lee/cyber/smartid/tse/inter/LogAccess;

    invoke-interface {v3, v1, v0}, Lee/cyber/smartid/tse/inter/LogAccess;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 554
    iget-object v1, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable;->c:Ljava/lang/String;

    iget-object v3, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable;->g:Lee/cyber/smartid/tse/inter/ResourceAccess;

    invoke-interface {v3}, Lee/cyber/smartid/tse/inter/ResourceAccess;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    iget-object v4, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable;->m:Lee/cyber/smartid/tse/inter/WallClock;

    invoke-static {v3, v4, v0}, Lee/cyber/smartid/tse/dto/TSEError;->from(Landroid/content/Context;Lee/cyber/smartid/tse/inter/WallClock;Ljava/lang/Throwable;)Lee/cyber/smartid/tse/dto/TSEError;

    move-result-object v0

    invoke-direct {p0, v1, v0}, Lee/cyber/smartid/tse/util/KeyStateRunnable;->b(Ljava/lang/String;Lee/cyber/smartid/tse/dto/TSEError;)V

    return v2
.end method

.method private b(Lee/cyber/smartid/tse/dto/TSEError;)Z
    .locals 5

    if-eqz p1, :cond_1

    .line 849
    invoke-virtual {p1}, Lee/cyber/smartid/tse/dto/TSEError;->getCode()J

    move-result-wide v0

    const-wide/16 v2, -0x7923

    cmp-long v4, v0, v2

    if-eqz v4, :cond_0

    invoke-virtual {p1}, Lee/cyber/smartid/tse/dto/TSEError;->getCode()J

    move-result-wide v0

    const-wide/16 v2, -0x7919

    cmp-long v4, v0, v2

    if-eqz v4, :cond_0

    invoke-virtual {p1}, Lee/cyber/smartid/tse/dto/TSEError;->getCode()J

    move-result-wide v0

    const-wide/16 v2, -0x791f

    cmp-long v4, v0, v2

    if-eqz v4, :cond_0

    invoke-virtual {p1}, Lee/cyber/smartid/tse/dto/TSEError;->getCode()J

    move-result-wide v0

    const-wide/16 v2, -0x7921

    cmp-long v4, v0, v2

    if-eqz v4, :cond_0

    invoke-virtual {p1}, Lee/cyber/smartid/tse/dto/TSEError;->getCode()J

    move-result-wide v0

    const-wide/16 v2, -0x791c

    cmp-long v4, v0, v2

    if-eqz v4, :cond_0

    invoke-virtual {p1}, Lee/cyber/smartid/tse/dto/TSEError;->getCode()J

    move-result-wide v0

    const-wide/16 v2, -0x792b

    cmp-long v4, v0, v2

    if-eqz v4, :cond_0

    invoke-virtual {p1}, Lee/cyber/smartid/tse/dto/TSEError;->getCode()J

    move-result-wide v0

    const-wide/16 v2, -0x7931

    cmp-long v4, v0, v2

    if-eqz v4, :cond_0

    invoke-direct {p0, p1}, Lee/cyber/smartid/tse/util/KeyStateRunnable;->d(Lee/cyber/smartid/tse/dto/TSEError;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0, p1}, Lee/cyber/smartid/tse/util/KeyStateRunnable;->c(Lee/cyber/smartid/tse/dto/TSEError;)Z

    move-result p1

    if-eqz p1, :cond_1

    :cond_0
    const/4 p1, 0x1

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method static synthetic c(Lee/cyber/smartid/tse/util/KeyStateRunnable;)Lee/cyber/smartid/tse/dto/ProtoKeyState;
    .locals 0

    .line 68
    iget-object p0, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable;->d:Lee/cyber/smartid/tse/dto/ProtoKeyState;

    return-object p0
.end method

.method private c()Z
    .locals 14

    .line 580
    iget-object v0, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable;->o:Lee/cyber/smartid/tse/inter/LogAccess;

    const-string v1, "handleSubmitClientSecondPart"

    invoke-interface {v0, v1}, Lee/cyber/smartid/tse/inter/LogAccess;->d(Ljava/lang/String;)V

    .line 587
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v2, 0x0

    invoke-direct {v0, v2}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    .line 593
    :cond_0
    invoke-direct {p0}, Lee/cyber/smartid/tse/util/KeyStateRunnable;->d()Ljava/lang/String;

    move-result-object v3

    .line 595
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    const/4 v5, 0x0

    if-eqz v4, :cond_1

    .line 597
    iget-object v3, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable;->o:Lee/cyber/smartid/tse/inter/LogAccess;

    const-string v4, "handleSubmitClientSecondPart - we are missing the freshnessToken, lets get one  .."

    invoke-interface {v3, v4}, Lee/cyber/smartid/tse/inter/LogAccess;->d(Ljava/lang/String;)V

    .line 598
    invoke-direct {p0, v0}, Lee/cyber/smartid/tse/util/KeyStateRunnable;->a(Ljava/util/concurrent/atomic/AtomicInteger;)Lee/cyber/smartid/tse/dto/TSEError;

    move-result-object v3

    .line 599
    invoke-direct {p0}, Lee/cyber/smartid/tse/util/KeyStateRunnable;->d()Ljava/lang/String;

    move-result-object v4

    move-object v13, v4

    move-object v4, v3

    move-object v3, v13

    goto :goto_0

    :cond_1
    move-object v4, v5

    .line 602
    :goto_0
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    const/4 v7, 0x3

    if-eqz v6, :cond_2

    iget-object v6, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable;->f:Lee/cyber/smartid/tse/dto/KeyStateMeta;

    invoke-virtual {v6}, Lee/cyber/smartid/tse/dto/KeyStateMeta;->isInLongTermRetryMode()Z

    move-result v6

    if-nez v6, :cond_2

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    move-result v6

    if-gt v6, v7, :cond_2

    .line 604
    iget-object v3, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable;->o:Lee/cyber/smartid/tse/inter/LogAccess;

    const-string v6, "handleSubmitClientSecondPart - someone took our freshnessToken, lets get another one  .."

    invoke-interface {v3, v6}, Lee/cyber/smartid/tse/inter/LogAccess;->d(Ljava/lang/String;)V

    .line 606
    invoke-direct {p0, v4}, Lee/cyber/smartid/tse/util/KeyStateRunnable;->a(Lee/cyber/smartid/tse/dto/TSEError;)Lee/cyber/smartid/tse/dto/TSEError;

    move-result-object v3

    goto/16 :goto_2

    .line 608
    :cond_2
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 610
    iget-object v0, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable;->o:Lee/cyber/smartid/tse/inter/LogAccess;

    const-string v3, "handleSubmitClientSecondPart - unable to get a freshnessToken, aborting .."

    invoke-interface {v0, v3}, Lee/cyber/smartid/tse/inter/LogAccess;->d(Ljava/lang/String;)V

    .line 612
    invoke-direct {p0, v4}, Lee/cyber/smartid/tse/util/KeyStateRunnable;->a(Lee/cyber/smartid/tse/dto/TSEError;)Lee/cyber/smartid/tse/dto/TSEError;

    move-result-object v0

    goto/16 :goto_3

    .line 620
    :cond_3
    new-instance v4, Lee/cyber/smartid/tse/dto/jsonrpc/param/SubmitClientSecondPartParams;

    iget-object v6, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable;->e:Lee/cyber/smartid/tse/dto/KeyState;

    invoke-virtual {v6}, Lee/cyber/smartid/tse/dto/KeyState;->getPayload()Ljava/lang/String;

    move-result-object v6

    iget-object v8, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable;->e:Lee/cyber/smartid/tse/dto/KeyState;

    invoke-virtual {v8}, Lee/cyber/smartid/tse/dto/KeyState;->getPayloadEncoding()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v4, v6, v8, v3}, Lee/cyber/smartid/tse/dto/jsonrpc/param/SubmitClientSecondPartParams;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 621
    new-instance v3, Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCRequest;

    const-string v6, "submitClient2ndPart"

    invoke-direct {v3, v6, v4}, Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCRequest;-><init>(Ljava/lang/String;Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCParams;)V

    .line 622
    iget-object v4, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable;->b:Lee/cyber/smartid/tse/network/TSEAPI$TSEAPIService;

    iget-object v6, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable;->n:Landroid/os/Bundle;

    invoke-static {v6, v0}, Lee/cyber/smartid/tse/util/Util;->a(Landroid/os/Bundle;Ljava/util/concurrent/atomic/AtomicInteger;)Ljava/lang/String;

    move-result-object v6

    invoke-interface {v4, v6, v3}, Lee/cyber/smartid/tse/network/TSEAPI$TSEAPIService;->submitClientSecondPart(Ljava/lang/String;Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCRequest;)Lc/b;

    move-result-object v4

    .line 626
    :try_start_0
    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v6

    if-lez v6, :cond_4

    .line 627
    iget-object v6, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable;->o:Lee/cyber/smartid/tse/inter/LogAccess;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "handleSubmitClientSecondPart - overall retries at "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v9, "/"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-interface {v6, v8}, Lee/cyber/smartid/tse/inter/LogAccess;->d(Ljava/lang/String;)V

    .line 629
    :cond_4
    invoke-interface {v4}, Lc/b;->a()Lc/r;

    move-result-object v4
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v13, v5

    move-object v5, v4

    move-object v4, v13

    goto :goto_1

    :catch_0
    move-exception v4

    .line 635
    iget-object v6, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable;->o:Lee/cyber/smartid/tse/inter/LogAccess;

    invoke-interface {v6, v1, v4}, Lee/cyber/smartid/tse/inter/LogAccess;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    :catch_1
    move-exception v4

    .line 632
    iget-object v6, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable;->o:Lee/cyber/smartid/tse/inter/LogAccess;

    invoke-interface {v6, v1, v4}, Lee/cyber/smartid/tse/inter/LogAccess;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 637
    :goto_1
    new-instance v6, Lee/cyber/smartid/tse/util/KeyStateRunnable$13;

    invoke-direct {v6, p0}, Lee/cyber/smartid/tse/util/KeyStateRunnable$13;-><init>(Lee/cyber/smartid/tse/util/KeyStateRunnable;)V

    invoke-direct {p0, v3, v5, v4, v6}, Lee/cyber/smartid/tse/util/KeyStateRunnable;->a(Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCRequest;Lc/r;Ljava/lang/Throwable;Lee/cyber/smartid/tse/util/KeyStateRunnable$ValidateResponseCallback;)Lee/cyber/smartid/tse/dto/TSEError;

    move-result-object v3

    :goto_2
    if-eqz v3, :cond_5

    .line 644
    iget-object v4, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable;->f:Lee/cyber/smartid/tse/dto/KeyStateMeta;

    invoke-direct {p0, v4, v3}, Lee/cyber/smartid/tse/util/KeyStateRunnable;->a(Lee/cyber/smartid/tse/dto/KeyStateMeta;Lee/cyber/smartid/tse/dto/TSEError;)Z

    move-result v4

    if-eqz v4, :cond_5

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    move-result v4

    if-le v4, v7, :cond_0

    :cond_5
    move-object v0, v3

    .line 647
    :goto_3
    invoke-direct {p0, v5, v0}, Lee/cyber/smartid/tse/util/KeyStateRunnable;->a(Lc/r;Lee/cyber/smartid/tse/dto/TSEError;)Lee/cyber/smartid/tse/dto/TSEError;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 651
    iget-object v1, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable;->f:Lee/cyber/smartid/tse/dto/KeyStateMeta;

    invoke-direct {p0, v0, v1}, Lee/cyber/smartid/tse/util/KeyStateRunnable;->a(Lee/cyber/smartid/tse/dto/TSEError;Lee/cyber/smartid/tse/dto/KeyStateMeta;)V

    .line 652
    iget-object v1, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable;->c:Ljava/lang/String;

    iget-object v3, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable;->e:Lee/cyber/smartid/tse/dto/KeyState;

    invoke-virtual {v3}, Lee/cyber/smartid/tse/dto/KeyState;->getKeyId()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v1, v3, v0}, Lee/cyber/smartid/tse/util/KeyStateRunnable;->a(Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/tse/dto/TSEError;)V

    return v2

    .line 659
    :cond_6
    :try_start_1
    iget-object v6, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable;->i:Lee/cyber/smartid/tse/inter/KeyStorageAccess;

    iget-object v3, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable;->e:Lee/cyber/smartid/tse/dto/KeyState;

    invoke-virtual {v3}, Lee/cyber/smartid/tse/dto/KeyState;->getId()Ljava/lang/String;

    move-result-object v7

    iget-object v3, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable;->e:Lee/cyber/smartid/tse/dto/KeyState;

    invoke-virtual {v3}, Lee/cyber/smartid/tse/dto/KeyState;->getKeyId()Ljava/lang/String;

    move-result-object v8

    iget-object v3, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable;->e:Lee/cyber/smartid/tse/dto/KeyState;

    invoke-virtual {v3}, Lee/cyber/smartid/tse/dto/KeyState;->getAccountUUID()Ljava/lang/String;

    move-result-object v9

    iget-object v3, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable;->e:Lee/cyber/smartid/tse/dto/KeyState;

    invoke-virtual {v3}, Lee/cyber/smartid/tse/dto/KeyState;->getKeyType()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v5}, Lc/r;->e()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCResponse;

    invoke-virtual {v3}, Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCResponse;->getResult()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lee/cyber/smartid/tse/dto/jsonrpc/result/SubmitClientSecondPartResult;

    invoke-virtual {v3}, Lee/cyber/smartid/tse/dto/jsonrpc/result/SubmitClientSecondPartResult;->getResponseData()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v5}, Lc/r;->e()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCResponse;

    invoke-virtual {v3}, Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCResponse;->getResult()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lee/cyber/smartid/tse/dto/jsonrpc/result/SubmitClientSecondPartResult;

    invoke-virtual {v3}, Lee/cyber/smartid/tse/dto/jsonrpc/result/SubmitClientSecondPartResult;->getResponseDataEncoding()Ljava/lang/String;

    move-result-object v12

    invoke-interface/range {v6 .. v12}, Lee/cyber/smartid/tse/inter/KeyStorageAccess;->finishKeyStateOperation(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Lee/cyber/smartid/cryptolib/dto/StorageException; {:try_start_1 .. :try_end_1} :catch_2

    .line 668
    invoke-direct {p0, v5, v0}, Lee/cyber/smartid/tse/util/KeyStateRunnable;->b(Lc/r;Lee/cyber/smartid/tse/dto/TSEError;)Lee/cyber/smartid/tse/dto/TSEError;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 671
    iget-object v1, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable;->c:Ljava/lang/String;

    iget-object v2, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable;->e:Lee/cyber/smartid/tse/dto/KeyState;

    invoke-virtual {v2}, Lee/cyber/smartid/tse/dto/KeyState;->getKeyId()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v1, v2, v0}, Lee/cyber/smartid/tse/util/KeyStateRunnable;->a(Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/tse/dto/TSEError;)V

    goto :goto_4

    .line 675
    :cond_7
    new-instance v0, Lee/cyber/smartid/tse/dto/jsonrpc/resp/SubmitClientSecondPartResp;

    iget-object v1, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable;->c:Ljava/lang/String;

    iget-object v2, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable;->e:Lee/cyber/smartid/tse/dto/KeyState;

    invoke-virtual {v2}, Lee/cyber/smartid/tse/dto/KeyState;->getKeyId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v5}, Lc/r;->e()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCResponse;

    invoke-virtual {v3}, Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCResponse;->getResult()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lee/cyber/smartid/tse/dto/jsonrpc/result/SubmitClientSecondPartResult;

    invoke-virtual {v3}, Lee/cyber/smartid/tse/dto/jsonrpc/result/SubmitClientSecondPartResult;->getCsrTransactionUUID()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lee/cyber/smartid/tse/dto/jsonrpc/resp/SubmitClientSecondPartResp;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 676
    iget-object v1, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable;->l:Landroid/os/Handler;

    new-instance v2, Lee/cyber/smartid/tse/util/KeyStateRunnable$14;

    invoke-direct {v2, p0, v0}, Lee/cyber/smartid/tse/util/KeyStateRunnable$14;-><init>(Lee/cyber/smartid/tse/util/KeyStateRunnable;Lee/cyber/smartid/tse/dto/jsonrpc/resp/SubmitClientSecondPartResp;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :goto_4
    const/4 v0, 0x1

    return v0

    :catch_2
    move-exception v0

    .line 661
    iget-object v3, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable;->o:Lee/cyber/smartid/tse/inter/LogAccess;

    invoke-interface {v3, v1, v0}, Lee/cyber/smartid/tse/inter/LogAccess;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 663
    iget-object v1, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable;->c:Ljava/lang/String;

    iget-object v3, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable;->e:Lee/cyber/smartid/tse/dto/KeyState;

    invoke-virtual {v3}, Lee/cyber/smartid/tse/dto/KeyState;->getKeyId()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable;->g:Lee/cyber/smartid/tse/inter/ResourceAccess;

    invoke-interface {v4}, Lee/cyber/smartid/tse/inter/ResourceAccess;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    iget-object v5, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable;->m:Lee/cyber/smartid/tse/inter/WallClock;

    invoke-static {v4, v5, v0}, Lee/cyber/smartid/tse/dto/TSEError;->from(Landroid/content/Context;Lee/cyber/smartid/tse/inter/WallClock;Ljava/lang/Throwable;)Lee/cyber/smartid/tse/dto/TSEError;

    move-result-object v0

    invoke-direct {p0, v1, v3, v0}, Lee/cyber/smartid/tse/util/KeyStateRunnable;->a(Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/tse/dto/TSEError;)V

    return v2
.end method

.method private c(Lee/cyber/smartid/tse/dto/TSEError;)Z
    .locals 5

    if-eqz p1, :cond_1

    .line 859
    invoke-virtual {p1}, Lee/cyber/smartid/tse/dto/TSEError;->getCode()J

    move-result-wide v0

    const-wide/16 v2, -0x791a

    cmp-long v4, v0, v2

    if-eqz v4, :cond_0

    invoke-virtual {p1}, Lee/cyber/smartid/tse/dto/TSEError;->getCode()J

    move-result-wide v0

    const-wide/16 v2, -0x791d

    cmp-long v4, v0, v2

    if-eqz v4, :cond_0

    invoke-virtual {p1}, Lee/cyber/smartid/tse/dto/TSEError;->getCode()J

    move-result-wide v0

    const-wide/16 v2, -0x7930

    cmp-long v4, v0, v2

    if-eqz v4, :cond_0

    invoke-virtual {p1}, Lee/cyber/smartid/tse/dto/TSEError;->getCode()J

    move-result-wide v0

    const-wide/16 v2, -0x792f

    cmp-long p1, v0, v2

    if-nez p1, :cond_1

    :cond_0
    const/4 p1, 0x1

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method static synthetic d(Lee/cyber/smartid/tse/util/KeyStateRunnable;)Lee/cyber/smartid/tse/inter/ResourceAccess;
    .locals 0

    .line 68
    iget-object p0, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable;->g:Lee/cyber/smartid/tse/inter/ResourceAccess;

    return-object p0
.end method

.method private d()Ljava/lang/String;
    .locals 3

    .line 1106
    :try_start_0
    iget-object v0, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable;->i:Lee/cyber/smartid/tse/inter/KeyStorageAccess;

    iget-object v1, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable;->e:Lee/cyber/smartid/tse/dto/KeyState;

    invoke-virtual {v1}, Lee/cyber/smartid/tse/dto/KeyState;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lee/cyber/smartid/tse/inter/KeyStorageAccess;->consumeFreshnessToken(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0
    :try_end_0
    .catch Lee/cyber/smartid/cryptolib/dto/StorageException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    .line 1109
    iget-object v1, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable;->o:Lee/cyber/smartid/tse/inter/LogAccess;

    const-string v2, "consumeFreshnessToken"

    invoke-interface {v1, v2, v0}, Lee/cyber/smartid/tse/inter/LogAccess;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    const/4 v0, 0x0

    return-object v0
.end method

.method private d(Lee/cyber/smartid/tse/dto/TSEError;)Z
    .locals 5

    if-eqz p1, :cond_1

    .line 1078
    invoke-virtual {p1}, Lee/cyber/smartid/tse/dto/TSEError;->getKeyStatus()Lee/cyber/smartid/tse/dto/jsonrpc/AccountKeyStatus;

    move-result-object v0

    if-nez v0, :cond_0

    goto :goto_0

    .line 1081
    :cond_0
    invoke-virtual {p1}, Lee/cyber/smartid/tse/dto/TSEError;->getKeyStatus()Lee/cyber/smartid/tse/dto/jsonrpc/AccountKeyStatus;

    move-result-object v0

    invoke-virtual {p1}, Lee/cyber/smartid/tse/dto/TSEError;->getCreatedAtTimestamp()J

    move-result-wide v1

    iget-object p1, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable;->m:Lee/cyber/smartid/tse/inter/WallClock;

    invoke-interface {p1}, Lee/cyber/smartid/tse/inter/WallClock;->currentTimeMillis()J

    move-result-wide v3

    invoke-static {v0, v1, v2, v3, v4}, Lee/cyber/smartid/tse/util/Util;->isKeyLocked(Lee/cyber/smartid/tse/dto/jsonrpc/AccountKeyStatus;JJ)Z

    move-result p1

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x0

    return p1
.end method

.method static synthetic e(Lee/cyber/smartid/tse/util/KeyStateRunnable;)Lee/cyber/smartid/tse/inter/LogAccess;
    .locals 0

    .line 68
    iget-object p0, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable;->o:Lee/cyber/smartid/tse/inter/LogAccess;

    return-object p0
.end method

.method private e(Lee/cyber/smartid/tse/dto/TSEError;)V
    .locals 1

    .line 1138
    iget-object v0, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable;->j:Lee/cyber/smartid/tse/inter/ListenerAccess;

    if-eqz v0, :cond_0

    .line 1139
    invoke-interface {v0, p1}, Lee/cyber/smartid/tse/inter/ListenerAccess;->notifySystemEventsListeners(Lee/cyber/smartid/tse/dto/TSEError;)V

    :cond_0
    return-void
.end method

.method static synthetic f(Lee/cyber/smartid/tse/util/KeyStateRunnable;)Lee/cyber/smartid/tse/inter/ExternalResourceAccess;
    .locals 0

    .line 68
    iget-object p0, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable;->h:Lee/cyber/smartid/tse/inter/ExternalResourceAccess;

    return-object p0
.end method

.method public static getFirstLongTermRetryCount()I
    .locals 1

    const/4 v0, 0x4

    return v0
.end method


# virtual methods
.method public run()V
    .locals 20

    move-object/from16 v1, p0

    const-string v2, "run - ended for "

    const-string v3, "run"

    .line 193
    iget-object v4, v1, Lee/cyber/smartid/tse/util/KeyStateRunnable;->o:Lee/cyber/smartid/tse/inter/LogAccess;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "run - started for "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v6, v1, Lee/cyber/smartid/tse/util/KeyStateRunnable;->d:Lee/cyber/smartid/tse/dto/ProtoKeyState;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Lee/cyber/smartid/tse/inter/LogAccess;->d(Ljava/lang/String;)V

    .line 194
    iget-object v4, v1, Lee/cyber/smartid/tse/util/KeyStateRunnable;->a:Lee/cyber/smartid/tse/inter/StateWorkerJobListener;

    const/4 v5, 0x0

    if-eqz v4, :cond_1

    .line 195
    iget-object v6, v1, Lee/cyber/smartid/tse/util/KeyStateRunnable;->d:Lee/cyber/smartid/tse/dto/ProtoKeyState;

    if-eqz v6, :cond_0

    invoke-virtual {v6}, Lee/cyber/smartid/tse/dto/ProtoKeyState;->getId()Ljava/lang/String;

    move-result-object v6

    goto :goto_0

    :cond_0
    move-object v6, v5

    :goto_0
    invoke-interface {v4, v6}, Lee/cyber/smartid/tse/inter/StateWorkerJobListener;->onJobStarted(Ljava/lang/String;)V

    .line 201
    :cond_1
    :try_start_0
    iget-object v4, v1, Lee/cyber/smartid/tse/util/KeyStateRunnable;->d:Lee/cyber/smartid/tse/dto/ProtoKeyState;

    instance-of v4, v4, Lee/cyber/smartid/tse/dto/KeyState;

    if-eqz v4, :cond_6

    .line 202
    iget-object v4, v1, Lee/cyber/smartid/tse/util/KeyStateRunnable;->o:Lee/cyber/smartid/tse/inter/LogAccess;

    const-string v6, "run - ProtoKeyState was a KeyState, this is not allowed, aborting .."

    invoke-interface {v4, v6}, Lee/cyber/smartid/tse/inter/LogAccess;->e(Ljava/lang/String;)V

    .line 203
    iget-object v4, v1, Lee/cyber/smartid/tse/util/KeyStateRunnable;->l:Landroid/os/Handler;

    new-instance v6, Lee/cyber/smartid/tse/util/KeyStateRunnable$1;

    invoke-direct {v6, v1}, Lee/cyber/smartid/tse/util/KeyStateRunnable$1;-><init>(Lee/cyber/smartid/tse/util/KeyStateRunnable;)V

    invoke-virtual {v4, v6}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_8
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 347
    :try_start_1
    iget-object v4, v1, Lee/cyber/smartid/tse/util/KeyStateRunnable;->d:Lee/cyber/smartid/tse/dto/ProtoKeyState;

    if-eqz v4, :cond_2

    .line 349
    iget-object v4, v1, Lee/cyber/smartid/tse/util/KeyStateRunnable;->d:Lee/cyber/smartid/tse/dto/ProtoKeyState;

    invoke-virtual {v4}, Lee/cyber/smartid/tse/dto/ProtoKeyState;->getId()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v4}, Lee/cyber/smartid/tse/util/KeyStateRunnable;->a(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    :catch_0
    move-exception v0

    move-object v4, v0

    .line 352
    iget-object v6, v1, Lee/cyber/smartid/tse/util/KeyStateRunnable;->o:Lee/cyber/smartid/tse/inter/LogAccess;

    invoke-interface {v6, v3, v4}, Lee/cyber/smartid/tse/inter/LogAccess;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 355
    :cond_2
    :goto_1
    iget-object v3, v1, Lee/cyber/smartid/tse/util/KeyStateRunnable;->a:Lee/cyber/smartid/tse/inter/StateWorkerJobListener;

    if-eqz v3, :cond_5

    .line 356
    iget-object v4, v1, Lee/cyber/smartid/tse/util/KeyStateRunnable;->e:Lee/cyber/smartid/tse/dto/KeyState;

    if-eqz v4, :cond_3

    invoke-virtual {v4}, Lee/cyber/smartid/tse/dto/KeyState;->getId()Ljava/lang/String;

    move-result-object v5

    goto :goto_2

    :cond_3
    iget-object v4, v1, Lee/cyber/smartid/tse/util/KeyStateRunnable;->d:Lee/cyber/smartid/tse/dto/ProtoKeyState;

    if-eqz v4, :cond_4

    invoke-virtual {v4}, Lee/cyber/smartid/tse/dto/ProtoKeyState;->getId()Ljava/lang/String;

    move-result-object v5

    :cond_4
    :goto_2
    invoke-interface {v3, v5}, Lee/cyber/smartid/tse/inter/StateWorkerJobListener;->onJobCompleted(Ljava/lang/String;)V

    .line 358
    :cond_5
    iget-object v3, v1, Lee/cyber/smartid/tse/util/KeyStateRunnable;->o:Lee/cyber/smartid/tse/inter/LogAccess;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    :goto_3
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, v1, Lee/cyber/smartid/tse/util/KeyStateRunnable;->d:Lee/cyber/smartid/tse/dto/ProtoKeyState;

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v3, v2}, Lee/cyber/smartid/tse/inter/LogAccess;->d(Ljava/lang/String;)V

    return-void

    .line 211
    :cond_6
    :try_start_2
    iget-object v4, v1, Lee/cyber/smartid/tse/util/KeyStateRunnable;->d:Lee/cyber/smartid/tse/dto/ProtoKeyState;

    if-eqz v4, :cond_24

    iget-object v4, v1, Lee/cyber/smartid/tse/util/KeyStateRunnable;->d:Lee/cyber/smartid/tse/dto/ProtoKeyState;

    invoke-virtual {v4}, Lee/cyber/smartid/tse/dto/ProtoKeyState;->isInActiveState()Z

    move-result v4

    if-nez v4, :cond_7

    goto/16 :goto_10

    .line 226
    :cond_7
    iget-object v4, v1, Lee/cyber/smartid/tse/util/KeyStateRunnable;->i:Lee/cyber/smartid/tse/inter/KeyStorageAccess;

    iget-object v6, v1, Lee/cyber/smartid/tse/util/KeyStateRunnable;->d:Lee/cyber/smartid/tse/dto/ProtoKeyState;

    invoke-virtual {v6}, Lee/cyber/smartid/tse/dto/ProtoKeyState;->getId()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v4, v6}, Lee/cyber/smartid/tse/inter/KeyStorageAccess;->getKeyStateById(Ljava/lang/String;)Lee/cyber/smartid/tse/dto/KeyState;

    move-result-object v4

    if-nez v4, :cond_c

    .line 229
    iget-object v4, v1, Lee/cyber/smartid/tse/util/KeyStateRunnable;->l:Landroid/os/Handler;

    new-instance v6, Lee/cyber/smartid/tse/util/KeyStateRunnable$3;

    invoke-direct {v6, v1}, Lee/cyber/smartid/tse/util/KeyStateRunnable$3;-><init>(Lee/cyber/smartid/tse/util/KeyStateRunnable;)V

    invoke-virtual {v4, v6}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 235
    iget-object v4, v1, Lee/cyber/smartid/tse/util/KeyStateRunnable;->o:Lee/cyber/smartid/tse/inter/LogAccess;

    const-string v6, "run - New protoState start attempted while the old state was null! Aborting .."

    invoke-interface {v4, v6}, Lee/cyber/smartid/tse/inter/LogAccess;->e(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_8
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 347
    :try_start_3
    iget-object v4, v1, Lee/cyber/smartid/tse/util/KeyStateRunnable;->d:Lee/cyber/smartid/tse/dto/ProtoKeyState;

    if-eqz v4, :cond_8

    .line 349
    iget-object v4, v1, Lee/cyber/smartid/tse/util/KeyStateRunnable;->d:Lee/cyber/smartid/tse/dto/ProtoKeyState;

    invoke-virtual {v4}, Lee/cyber/smartid/tse/dto/ProtoKeyState;->getId()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v4}, Lee/cyber/smartid/tse/util/KeyStateRunnable;->a(Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_4

    :catch_1
    move-exception v0

    move-object v4, v0

    .line 352
    iget-object v6, v1, Lee/cyber/smartid/tse/util/KeyStateRunnable;->o:Lee/cyber/smartid/tse/inter/LogAccess;

    invoke-interface {v6, v3, v4}, Lee/cyber/smartid/tse/inter/LogAccess;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 355
    :cond_8
    :goto_4
    iget-object v3, v1, Lee/cyber/smartid/tse/util/KeyStateRunnable;->a:Lee/cyber/smartid/tse/inter/StateWorkerJobListener;

    if-eqz v3, :cond_b

    .line 356
    iget-object v4, v1, Lee/cyber/smartid/tse/util/KeyStateRunnable;->e:Lee/cyber/smartid/tse/dto/KeyState;

    if-eqz v4, :cond_9

    invoke-virtual {v4}, Lee/cyber/smartid/tse/dto/KeyState;->getId()Ljava/lang/String;

    move-result-object v5

    goto :goto_5

    :cond_9
    iget-object v4, v1, Lee/cyber/smartid/tse/util/KeyStateRunnable;->d:Lee/cyber/smartid/tse/dto/ProtoKeyState;

    if-eqz v4, :cond_a

    invoke-virtual {v4}, Lee/cyber/smartid/tse/dto/ProtoKeyState;->getId()Ljava/lang/String;

    move-result-object v5

    :cond_a
    :goto_5
    invoke-interface {v3, v5}, Lee/cyber/smartid/tse/inter/StateWorkerJobListener;->onJobCompleted(Ljava/lang/String;)V

    .line 358
    :cond_b
    iget-object v3, v1, Lee/cyber/smartid/tse/util/KeyStateRunnable;->o:Lee/cyber/smartid/tse/inter/LogAccess;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    goto :goto_3

    .line 242
    :cond_c
    :try_start_4
    iget-object v6, v1, Lee/cyber/smartid/tse/util/KeyStateRunnable;->d:Lee/cyber/smartid/tse/dto/ProtoKeyState;

    invoke-virtual {v6}, Lee/cyber/smartid/tse/dto/ProtoKeyState;->isRetry()Z

    move-result v6

    if-eqz v6, :cond_12

    .line 244
    invoke-virtual {v4}, Lee/cyber/smartid/tse/dto/KeyState;->isInActiveState()Z

    move-result v6

    if-eqz v6, :cond_d

    iget-object v6, v1, Lee/cyber/smartid/tse/util/KeyStateRunnable;->d:Lee/cyber/smartid/tse/dto/ProtoKeyState;

    invoke-virtual {v4, v6}, Lee/cyber/smartid/tse/dto/KeyState;->isRetriableByProto(Lee/cyber/smartid/tse/dto/ProtoKeyState;)Z

    move-result v6

    if-eqz v6, :cond_d

    .line 246
    iget-object v6, v1, Lee/cyber/smartid/tse/util/KeyStateRunnable;->o:Lee/cyber/smartid/tse/inter/LogAccess;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "run - this is a retry of "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v8, v1, Lee/cyber/smartid/tse/util/KeyStateRunnable;->d:Lee/cyber/smartid/tse/dto/ProtoKeyState;

    invoke-virtual {v8}, Lee/cyber/smartid/tse/dto/ProtoKeyState;->getHumanReadableNameForType()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v8, ", no need to start a new operation"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v6, v7}, Lee/cyber/smartid/tse/inter/LogAccess;->d(Ljava/lang/String;)V

    .line 247
    iput-object v4, v1, Lee/cyber/smartid/tse/util/KeyStateRunnable;->e:Lee/cyber/smartid/tse/dto/KeyState;

    .line 248
    iget-object v4, v1, Lee/cyber/smartid/tse/util/KeyStateRunnable;->i:Lee/cyber/smartid/tse/inter/KeyStorageAccess;

    iget-object v6, v1, Lee/cyber/smartid/tse/util/KeyStateRunnable;->d:Lee/cyber/smartid/tse/dto/ProtoKeyState;

    invoke-virtual {v6}, Lee/cyber/smartid/tse/dto/ProtoKeyState;->getId()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v4, v6}, Lee/cyber/smartid/tse/inter/KeyStorageAccess;->getKeyStateMetaByKeyStateId(Ljava/lang/String;)Lee/cyber/smartid/tse/dto/KeyStateMeta;

    move-result-object v4

    iput-object v4, v1, Lee/cyber/smartid/tse/util/KeyStateRunnable;->f:Lee/cyber/smartid/tse/dto/KeyStateMeta;

    goto/16 :goto_8

    .line 251
    :cond_d
    iget-object v6, v1, Lee/cyber/smartid/tse/util/KeyStateRunnable;->o:Lee/cyber/smartid/tse/inter/LogAccess;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "run - Retry state for "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v8, v1, Lee/cyber/smartid/tse/util/KeyStateRunnable;->d:Lee/cyber/smartid/tse/dto/ProtoKeyState;

    invoke-virtual {v8}, Lee/cyber/smartid/tse/dto/ProtoKeyState;->getHumanReadableNameForType()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v8, " has already been completed! \nProtoKeyState: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v8, v1, Lee/cyber/smartid/tse/util/KeyStateRunnable;->d:Lee/cyber/smartid/tse/dto/ProtoKeyState;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v8, "\nKeyState: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v4, "\nAborting .."

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v6, v4}, Lee/cyber/smartid/tse/inter/LogAccess;->d(Ljava/lang/String;)V

    .line 252
    iget-object v4, v1, Lee/cyber/smartid/tse/util/KeyStateRunnable;->l:Landroid/os/Handler;

    new-instance v6, Lee/cyber/smartid/tse/util/KeyStateRunnable$4;

    invoke-direct {v6, v1}, Lee/cyber/smartid/tse/util/KeyStateRunnable$4;-><init>(Lee/cyber/smartid/tse/util/KeyStateRunnable;)V

    invoke-virtual {v4, v6}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
    :try_end_4
    .catch Lee/cyber/smartid/cryptolib/dto/StorageException; {:try_start_4 .. :try_end_4} :catch_5
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_8
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 347
    :try_start_5
    iget-object v4, v1, Lee/cyber/smartid/tse/util/KeyStateRunnable;->d:Lee/cyber/smartid/tse/dto/ProtoKeyState;

    if-eqz v4, :cond_e

    .line 349
    iget-object v4, v1, Lee/cyber/smartid/tse/util/KeyStateRunnable;->d:Lee/cyber/smartid/tse/dto/ProtoKeyState;

    invoke-virtual {v4}, Lee/cyber/smartid/tse/dto/ProtoKeyState;->getId()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v4}, Lee/cyber/smartid/tse/util/KeyStateRunnable;->a(Ljava/lang/String;)V
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_2

    goto :goto_6

    :catch_2
    move-exception v0

    move-object v4, v0

    .line 352
    iget-object v6, v1, Lee/cyber/smartid/tse/util/KeyStateRunnable;->o:Lee/cyber/smartid/tse/inter/LogAccess;

    invoke-interface {v6, v3, v4}, Lee/cyber/smartid/tse/inter/LogAccess;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 355
    :cond_e
    :goto_6
    iget-object v3, v1, Lee/cyber/smartid/tse/util/KeyStateRunnable;->a:Lee/cyber/smartid/tse/inter/StateWorkerJobListener;

    if-eqz v3, :cond_11

    .line 356
    iget-object v4, v1, Lee/cyber/smartid/tse/util/KeyStateRunnable;->e:Lee/cyber/smartid/tse/dto/KeyState;

    if-eqz v4, :cond_f

    invoke-virtual {v4}, Lee/cyber/smartid/tse/dto/KeyState;->getId()Ljava/lang/String;

    move-result-object v5

    goto :goto_7

    :cond_f
    iget-object v4, v1, Lee/cyber/smartid/tse/util/KeyStateRunnable;->d:Lee/cyber/smartid/tse/dto/ProtoKeyState;

    if-eqz v4, :cond_10

    invoke-virtual {v4}, Lee/cyber/smartid/tse/dto/ProtoKeyState;->getId()Ljava/lang/String;

    move-result-object v5

    :cond_10
    :goto_7
    invoke-interface {v3, v5}, Lee/cyber/smartid/tse/inter/StateWorkerJobListener;->onJobCompleted(Ljava/lang/String;)V

    .line 358
    :cond_11
    iget-object v3, v1, Lee/cyber/smartid/tse/util/KeyStateRunnable;->o:Lee/cyber/smartid/tse/inter/LogAccess;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    goto/16 :goto_3

    .line 262
    :cond_12
    :try_start_6
    invoke-virtual {v4}, Lee/cyber/smartid/tse/dto/KeyState;->isInActiveState()Z

    move-result v6

    if-nez v6, :cond_1b

    .line 263
    iget-object v4, v1, Lee/cyber/smartid/tse/util/KeyStateRunnable;->o:Lee/cyber/smartid/tse/inter/LogAccess;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "run - this is a new state ("

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v7, v1, Lee/cyber/smartid/tse/util/KeyStateRunnable;->d:Lee/cyber/smartid/tse/dto/ProtoKeyState;

    invoke-virtual {v7}, Lee/cyber/smartid/tse/dto/ProtoKeyState;->getHumanReadableNameForType()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v7, ")"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v4, v6}, Lee/cyber/smartid/tse/inter/LogAccess;->d(Ljava/lang/String;)V

    .line 267
    iget-object v7, v1, Lee/cyber/smartid/tse/util/KeyStateRunnable;->i:Lee/cyber/smartid/tse/inter/KeyStorageAccess;

    iget-object v4, v1, Lee/cyber/smartid/tse/util/KeyStateRunnable;->d:Lee/cyber/smartid/tse/dto/ProtoKeyState;

    invoke-virtual {v4}, Lee/cyber/smartid/tse/dto/ProtoKeyState;->getId()Ljava/lang/String;

    move-result-object v8

    iget-object v4, v1, Lee/cyber/smartid/tse/util/KeyStateRunnable;->d:Lee/cyber/smartid/tse/dto/ProtoKeyState;

    invoke-virtual {v4}, Lee/cyber/smartid/tse/dto/ProtoKeyState;->getKeyId()Ljava/lang/String;

    move-result-object v9

    iget-object v4, v1, Lee/cyber/smartid/tse/util/KeyStateRunnable;->d:Lee/cyber/smartid/tse/dto/ProtoKeyState;

    invoke-virtual {v4}, Lee/cyber/smartid/tse/dto/ProtoKeyState;->getType()I

    move-result v10

    iget-object v4, v1, Lee/cyber/smartid/tse/util/KeyStateRunnable;->d:Lee/cyber/smartid/tse/dto/ProtoKeyState;

    invoke-virtual {v4}, Lee/cyber/smartid/tse/dto/ProtoKeyState;->getAccountUUID()Ljava/lang/String;

    move-result-object v11

    iget-object v4, v1, Lee/cyber/smartid/tse/util/KeyStateRunnable;->d:Lee/cyber/smartid/tse/dto/ProtoKeyState;

    invoke-virtual {v4}, Lee/cyber/smartid/tse/dto/ProtoKeyState;->getTransactionUUID()Ljava/lang/String;

    move-result-object v12

    iget-object v4, v1, Lee/cyber/smartid/tse/util/KeyStateRunnable;->d:Lee/cyber/smartid/tse/dto/ProtoKeyState;

    invoke-virtual {v4}, Lee/cyber/smartid/tse/dto/ProtoKeyState;->getKeyType()Ljava/lang/String;

    move-result-object v13

    iget-object v4, v1, Lee/cyber/smartid/tse/util/KeyStateRunnable;->d:Lee/cyber/smartid/tse/dto/ProtoKeyState;

    invoke-virtual {v4}, Lee/cyber/smartid/tse/dto/ProtoKeyState;->getSignatureShare()Ljava/lang/String;

    move-result-object v14

    iget-object v4, v1, Lee/cyber/smartid/tse/util/KeyStateRunnable;->d:Lee/cyber/smartid/tse/dto/ProtoKeyState;

    invoke-virtual {v4}, Lee/cyber/smartid/tse/dto/ProtoKeyState;->getDigest()Ljava/lang/String;

    move-result-object v15

    iget-object v4, v1, Lee/cyber/smartid/tse/util/KeyStateRunnable;->d:Lee/cyber/smartid/tse/dto/ProtoKeyState;

    invoke-virtual {v4}, Lee/cyber/smartid/tse/dto/ProtoKeyState;->getDigestAlgorithm()Ljava/lang/String;

    move-result-object v16

    iget-object v4, v1, Lee/cyber/smartid/tse/util/KeyStateRunnable;->d:Lee/cyber/smartid/tse/dto/ProtoKeyState;

    invoke-virtual {v4}, Lee/cyber/smartid/tse/dto/ProtoKeyState;->getNonce()Ljava/lang/String;

    move-result-object v17

    iget-object v4, v1, Lee/cyber/smartid/tse/util/KeyStateRunnable;->d:Lee/cyber/smartid/tse/dto/ProtoKeyState;

    invoke-virtual {v4}, Lee/cyber/smartid/tse/dto/ProtoKeyState;->getClientShareSecondPart()Ljava/lang/String;

    move-result-object v18

    iget-object v4, v1, Lee/cyber/smartid/tse/util/KeyStateRunnable;->d:Lee/cyber/smartid/tse/dto/ProtoKeyState;

    invoke-virtual {v4}, Lee/cyber/smartid/tse/dto/ProtoKeyState;->getClientModulus()Ljava/lang/String;

    move-result-object v19

    invoke-interface/range {v7 .. v19}, Lee/cyber/smartid/tse/inter/KeyStorageAccess;->startKeyStateOperation(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/support/v4/e/j;

    move-result-object v4

    .line 268
    iget-object v6, v4, Landroid/support/v4/e/j;->a:Ljava/lang/Object;

    check-cast v6, Lee/cyber/smartid/tse/dto/KeyState;

    iput-object v6, v1, Lee/cyber/smartid/tse/util/KeyStateRunnable;->e:Lee/cyber/smartid/tse/dto/KeyState;

    .line 269
    iget-object v4, v4, Landroid/support/v4/e/j;->b:Ljava/lang/Object;

    check-cast v4, Lee/cyber/smartid/tse/dto/KeyStateMeta;

    iput-object v4, v1, Lee/cyber/smartid/tse/util/KeyStateRunnable;->f:Lee/cyber/smartid/tse/dto/KeyStateMeta;
    :try_end_6
    .catch Lee/cyber/smartid/cryptolib/dto/StorageException; {:try_start_6 .. :try_end_6} :catch_5
    .catch Ljava/lang/Throwable; {:try_start_6 .. :try_end_6} :catch_8
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 297
    :goto_8
    :try_start_7
    iget-object v4, v1, Lee/cyber/smartid/tse/util/KeyStateRunnable;->e:Lee/cyber/smartid/tse/dto/KeyState;

    if-eqz v4, :cond_1a

    .line 300
    iget-object v4, v1, Lee/cyber/smartid/tse/util/KeyStateRunnable;->e:Lee/cyber/smartid/tse/dto/KeyState;

    invoke-virtual {v4}, Lee/cyber/smartid/tse/dto/KeyState;->isActiveSign()Z

    move-result v4

    if-eqz v4, :cond_13

    .line 301
    iget-object v4, v1, Lee/cyber/smartid/tse/util/KeyStateRunnable;->o:Lee/cyber/smartid/tse/inter/LogAccess;

    const-string v6, "run - isActiveSign"

    invoke-interface {v4, v6}, Lee/cyber/smartid/tse/inter/LogAccess;->d(Ljava/lang/String;)V

    .line 303
    invoke-direct/range {p0 .. p0}, Lee/cyber/smartid/tse/util/KeyStateRunnable;->a()Z

    move-result v4

    .line 304
    iget-object v6, v1, Lee/cyber/smartid/tse/util/KeyStateRunnable;->o:Lee/cyber/smartid/tse/inter/LogAccess;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "run - handleSign success: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v6, v4}, Lee/cyber/smartid/tse/inter/LogAccess;->d(Ljava/lang/String;)V

    goto :goto_9

    .line 305
    :cond_13
    iget-object v4, v1, Lee/cyber/smartid/tse/util/KeyStateRunnable;->e:Lee/cyber/smartid/tse/dto/KeyState;

    invoke-virtual {v4}, Lee/cyber/smartid/tse/dto/KeyState;->isActiveCloneDetection()Z

    move-result v4

    if-eqz v4, :cond_14

    .line 306
    iget-object v4, v1, Lee/cyber/smartid/tse/util/KeyStateRunnable;->o:Lee/cyber/smartid/tse/inter/LogAccess;

    const-string v6, "run - isActiveCloneDetection"

    invoke-interface {v4, v6}, Lee/cyber/smartid/tse/inter/LogAccess;->d(Ljava/lang/String;)V

    .line 308
    invoke-direct/range {p0 .. p0}, Lee/cyber/smartid/tse/util/KeyStateRunnable;->b()Z

    move-result v4

    .line 309
    iget-object v6, v1, Lee/cyber/smartid/tse/util/KeyStateRunnable;->o:Lee/cyber/smartid/tse/inter/LogAccess;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "run - handleCloneDetection success: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v6, v4}, Lee/cyber/smartid/tse/inter/LogAccess;->d(Ljava/lang/String;)V

    goto :goto_9

    .line 310
    :cond_14
    iget-object v4, v1, Lee/cyber/smartid/tse/util/KeyStateRunnable;->e:Lee/cyber/smartid/tse/dto/KeyState;

    invoke-virtual {v4}, Lee/cyber/smartid/tse/dto/KeyState;->isActiveSubmitClientSecondPart()Z

    move-result v4

    if-eqz v4, :cond_15

    .line 311
    iget-object v4, v1, Lee/cyber/smartid/tse/util/KeyStateRunnable;->o:Lee/cyber/smartid/tse/inter/LogAccess;

    const-string v6, "run - isActiveSubmitClientSecondPart"

    invoke-interface {v4, v6}, Lee/cyber/smartid/tse/inter/LogAccess;->d(Ljava/lang/String;)V

    .line 313
    invoke-direct/range {p0 .. p0}, Lee/cyber/smartid/tse/util/KeyStateRunnable;->c()Z

    move-result v4

    .line 314
    iget-object v6, v1, Lee/cyber/smartid/tse/util/KeyStateRunnable;->o:Lee/cyber/smartid/tse/inter/LogAccess;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "run - handleSubmitClientSecondPart success: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v6, v4}, Lee/cyber/smartid/tse/inter/LogAccess;->d(Ljava/lang/String;)V

    goto :goto_9

    .line 317
    :cond_15
    iget-object v4, v1, Lee/cyber/smartid/tse/util/KeyStateRunnable;->o:Lee/cyber/smartid/tse/inter/LogAccess;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "run - action not supported: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v7, v1, Lee/cyber/smartid/tse/util/KeyStateRunnable;->e:Lee/cyber/smartid/tse/dto/KeyState;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v4, v6}, Lee/cyber/smartid/tse/inter/LogAccess;->e(Ljava/lang/String;)V

    .line 318
    iget-object v4, v1, Lee/cyber/smartid/tse/util/KeyStateRunnable;->l:Landroid/os/Handler;

    new-instance v6, Lee/cyber/smartid/tse/util/KeyStateRunnable$7;

    invoke-direct {v6, v1}, Lee/cyber/smartid/tse/util/KeyStateRunnable$7;-><init>(Lee/cyber/smartid/tse/util/KeyStateRunnable;)V

    invoke-virtual {v4, v6}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
    :try_end_7
    .catch Ljava/lang/Throwable; {:try_start_7 .. :try_end_7} :catch_8
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 347
    :goto_9
    :try_start_8
    iget-object v4, v1, Lee/cyber/smartid/tse/util/KeyStateRunnable;->d:Lee/cyber/smartid/tse/dto/ProtoKeyState;

    if-eqz v4, :cond_16

    .line 349
    iget-object v4, v1, Lee/cyber/smartid/tse/util/KeyStateRunnable;->d:Lee/cyber/smartid/tse/dto/ProtoKeyState;

    invoke-virtual {v4}, Lee/cyber/smartid/tse/dto/ProtoKeyState;->getId()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v4}, Lee/cyber/smartid/tse/util/KeyStateRunnable;->a(Ljava/lang/String;)V
    :try_end_8
    .catch Ljava/lang/Throwable; {:try_start_8 .. :try_end_8} :catch_3

    goto :goto_a

    :catch_3
    move-exception v0

    move-object v4, v0

    .line 352
    iget-object v6, v1, Lee/cyber/smartid/tse/util/KeyStateRunnable;->o:Lee/cyber/smartid/tse/inter/LogAccess;

    invoke-interface {v6, v3, v4}, Lee/cyber/smartid/tse/inter/LogAccess;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 355
    :cond_16
    :goto_a
    iget-object v3, v1, Lee/cyber/smartid/tse/util/KeyStateRunnable;->a:Lee/cyber/smartid/tse/inter/StateWorkerJobListener;

    if-eqz v3, :cond_19

    .line 356
    iget-object v4, v1, Lee/cyber/smartid/tse/util/KeyStateRunnable;->e:Lee/cyber/smartid/tse/dto/KeyState;

    if-eqz v4, :cond_17

    invoke-virtual {v4}, Lee/cyber/smartid/tse/dto/KeyState;->getId()Ljava/lang/String;

    move-result-object v5

    goto :goto_b

    :cond_17
    iget-object v4, v1, Lee/cyber/smartid/tse/util/KeyStateRunnable;->d:Lee/cyber/smartid/tse/dto/ProtoKeyState;

    if-eqz v4, :cond_18

    invoke-virtual {v4}, Lee/cyber/smartid/tse/dto/ProtoKeyState;->getId()Ljava/lang/String;

    move-result-object v5

    :cond_18
    :goto_b
    invoke-interface {v3, v5}, Lee/cyber/smartid/tse/inter/StateWorkerJobListener;->onJobCompleted(Ljava/lang/String;)V

    .line 358
    :cond_19
    iget-object v3, v1, Lee/cyber/smartid/tse/util/KeyStateRunnable;->o:Lee/cyber/smartid/tse/inter/LogAccess;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    goto/16 :goto_15

    .line 299
    :cond_1a
    :try_start_9
    new-instance v4, Ljava/lang/NullPointerException;

    const-string v6, "State can\'t be null at KeyStateRunnable run() method!"

    invoke-direct {v4, v6}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v4
    :try_end_9
    .catch Ljava/lang/Throwable; {:try_start_9 .. :try_end_9} :catch_8
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    .line 273
    :cond_1b
    :try_start_a
    iget-object v6, v1, Lee/cyber/smartid/tse/util/KeyStateRunnable;->o:Lee/cyber/smartid/tse/inter/LogAccess;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "run - New state ("

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v8, v1, Lee/cyber/smartid/tse/util/KeyStateRunnable;->d:Lee/cyber/smartid/tse/dto/ProtoKeyState;

    invoke-virtual {v8}, Lee/cyber/smartid/tse/dto/ProtoKeyState;->getHumanReadableNameForType()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v8, ") start attempted while the old state ("

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Lee/cyber/smartid/tse/dto/KeyState;->getHumanReadableNameForType()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, ") is still idle! Aborting .."

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v6, v4}, Lee/cyber/smartid/tse/inter/LogAccess;->e(Ljava/lang/String;)V

    .line 274
    iget-object v4, v1, Lee/cyber/smartid/tse/util/KeyStateRunnable;->l:Landroid/os/Handler;

    new-instance v6, Lee/cyber/smartid/tse/util/KeyStateRunnable$5;

    invoke-direct {v6, v1}, Lee/cyber/smartid/tse/util/KeyStateRunnable$5;-><init>(Lee/cyber/smartid/tse/util/KeyStateRunnable;)V

    invoke-virtual {v4, v6}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
    :try_end_a
    .catch Lee/cyber/smartid/cryptolib/dto/StorageException; {:try_start_a .. :try_end_a} :catch_5
    .catch Ljava/lang/Throwable; {:try_start_a .. :try_end_a} :catch_8
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    .line 347
    :try_start_b
    iget-object v4, v1, Lee/cyber/smartid/tse/util/KeyStateRunnable;->d:Lee/cyber/smartid/tse/dto/ProtoKeyState;

    if-eqz v4, :cond_1c

    .line 349
    iget-object v4, v1, Lee/cyber/smartid/tse/util/KeyStateRunnable;->d:Lee/cyber/smartid/tse/dto/ProtoKeyState;

    invoke-virtual {v4}, Lee/cyber/smartid/tse/dto/ProtoKeyState;->getId()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v4}, Lee/cyber/smartid/tse/util/KeyStateRunnable;->a(Ljava/lang/String;)V
    :try_end_b
    .catch Ljava/lang/Throwable; {:try_start_b .. :try_end_b} :catch_4

    goto :goto_c

    :catch_4
    move-exception v0

    move-object v4, v0

    .line 352
    iget-object v6, v1, Lee/cyber/smartid/tse/util/KeyStateRunnable;->o:Lee/cyber/smartid/tse/inter/LogAccess;

    invoke-interface {v6, v3, v4}, Lee/cyber/smartid/tse/inter/LogAccess;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 355
    :cond_1c
    :goto_c
    iget-object v3, v1, Lee/cyber/smartid/tse/util/KeyStateRunnable;->a:Lee/cyber/smartid/tse/inter/StateWorkerJobListener;

    if-eqz v3, :cond_1f

    .line 356
    iget-object v4, v1, Lee/cyber/smartid/tse/util/KeyStateRunnable;->e:Lee/cyber/smartid/tse/dto/KeyState;

    if-eqz v4, :cond_1d

    invoke-virtual {v4}, Lee/cyber/smartid/tse/dto/KeyState;->getId()Ljava/lang/String;

    move-result-object v5

    goto :goto_d

    :cond_1d
    iget-object v4, v1, Lee/cyber/smartid/tse/util/KeyStateRunnable;->d:Lee/cyber/smartid/tse/dto/ProtoKeyState;

    if-eqz v4, :cond_1e

    invoke-virtual {v4}, Lee/cyber/smartid/tse/dto/ProtoKeyState;->getId()Ljava/lang/String;

    move-result-object v5

    :cond_1e
    :goto_d
    invoke-interface {v3, v5}, Lee/cyber/smartid/tse/inter/StateWorkerJobListener;->onJobCompleted(Ljava/lang/String;)V

    .line 358
    :cond_1f
    iget-object v3, v1, Lee/cyber/smartid/tse/util/KeyStateRunnable;->o:Lee/cyber/smartid/tse/inter/LogAccess;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    goto/16 :goto_3

    :catch_5
    move-exception v0

    move-object v4, v0

    .line 285
    :try_start_c
    iget-object v6, v1, Lee/cyber/smartid/tse/util/KeyStateRunnable;->o:Lee/cyber/smartid/tse/inter/LogAccess;

    invoke-interface {v6, v3, v4}, Lee/cyber/smartid/tse/inter/LogAccess;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 287
    iget-object v4, v1, Lee/cyber/smartid/tse/util/KeyStateRunnable;->l:Landroid/os/Handler;

    new-instance v6, Lee/cyber/smartid/tse/util/KeyStateRunnable$6;

    invoke-direct {v6, v1}, Lee/cyber/smartid/tse/util/KeyStateRunnable$6;-><init>(Lee/cyber/smartid/tse/util/KeyStateRunnable;)V

    invoke-virtual {v4, v6}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
    :try_end_c
    .catch Ljava/lang/Throwable; {:try_start_c .. :try_end_c} :catch_8
    .catchall {:try_start_c .. :try_end_c} :catchall_0

    .line 347
    :try_start_d
    iget-object v4, v1, Lee/cyber/smartid/tse/util/KeyStateRunnable;->d:Lee/cyber/smartid/tse/dto/ProtoKeyState;

    if-eqz v4, :cond_20

    .line 349
    iget-object v4, v1, Lee/cyber/smartid/tse/util/KeyStateRunnable;->d:Lee/cyber/smartid/tse/dto/ProtoKeyState;

    invoke-virtual {v4}, Lee/cyber/smartid/tse/dto/ProtoKeyState;->getId()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v4}, Lee/cyber/smartid/tse/util/KeyStateRunnable;->a(Ljava/lang/String;)V
    :try_end_d
    .catch Ljava/lang/Throwable; {:try_start_d .. :try_end_d} :catch_6

    goto :goto_e

    :catch_6
    move-exception v0

    move-object v4, v0

    .line 352
    iget-object v6, v1, Lee/cyber/smartid/tse/util/KeyStateRunnable;->o:Lee/cyber/smartid/tse/inter/LogAccess;

    invoke-interface {v6, v3, v4}, Lee/cyber/smartid/tse/inter/LogAccess;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 355
    :cond_20
    :goto_e
    iget-object v3, v1, Lee/cyber/smartid/tse/util/KeyStateRunnable;->a:Lee/cyber/smartid/tse/inter/StateWorkerJobListener;

    if-eqz v3, :cond_23

    .line 356
    iget-object v4, v1, Lee/cyber/smartid/tse/util/KeyStateRunnable;->e:Lee/cyber/smartid/tse/dto/KeyState;

    if-eqz v4, :cond_21

    invoke-virtual {v4}, Lee/cyber/smartid/tse/dto/KeyState;->getId()Ljava/lang/String;

    move-result-object v5

    goto :goto_f

    :cond_21
    iget-object v4, v1, Lee/cyber/smartid/tse/util/KeyStateRunnable;->d:Lee/cyber/smartid/tse/dto/ProtoKeyState;

    if-eqz v4, :cond_22

    invoke-virtual {v4}, Lee/cyber/smartid/tse/dto/ProtoKeyState;->getId()Ljava/lang/String;

    move-result-object v5

    :cond_22
    :goto_f
    invoke-interface {v3, v5}, Lee/cyber/smartid/tse/inter/StateWorkerJobListener;->onJobCompleted(Ljava/lang/String;)V

    .line 358
    :cond_23
    iget-object v3, v1, Lee/cyber/smartid/tse/util/KeyStateRunnable;->o:Lee/cyber/smartid/tse/inter/LogAccess;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    goto/16 :goto_3

    .line 212
    :cond_24
    :goto_10
    :try_start_e
    iget-object v4, v1, Lee/cyber/smartid/tse/util/KeyStateRunnable;->o:Lee/cyber/smartid/tse/inter/LogAccess;

    const-string v6, "run - ProtoKeyState is null or not in active state, aborting .."

    invoke-interface {v4, v6}, Lee/cyber/smartid/tse/inter/LogAccess;->e(Ljava/lang/String;)V

    .line 213
    iget-object v4, v1, Lee/cyber/smartid/tse/util/KeyStateRunnable;->l:Landroid/os/Handler;

    new-instance v6, Lee/cyber/smartid/tse/util/KeyStateRunnable$2;

    invoke-direct {v6, v1}, Lee/cyber/smartid/tse/util/KeyStateRunnable$2;-><init>(Lee/cyber/smartid/tse/util/KeyStateRunnable;)V

    invoke-virtual {v4, v6}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
    :try_end_e
    .catch Ljava/lang/Throwable; {:try_start_e .. :try_end_e} :catch_8
    .catchall {:try_start_e .. :try_end_e} :catchall_0

    .line 347
    :try_start_f
    iget-object v4, v1, Lee/cyber/smartid/tse/util/KeyStateRunnable;->d:Lee/cyber/smartid/tse/dto/ProtoKeyState;

    if-eqz v4, :cond_25

    .line 349
    iget-object v4, v1, Lee/cyber/smartid/tse/util/KeyStateRunnable;->d:Lee/cyber/smartid/tse/dto/ProtoKeyState;

    invoke-virtual {v4}, Lee/cyber/smartid/tse/dto/ProtoKeyState;->getId()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v4}, Lee/cyber/smartid/tse/util/KeyStateRunnable;->a(Ljava/lang/String;)V
    :try_end_f
    .catch Ljava/lang/Throwable; {:try_start_f .. :try_end_f} :catch_7

    goto :goto_11

    :catch_7
    move-exception v0

    move-object v4, v0

    .line 352
    iget-object v6, v1, Lee/cyber/smartid/tse/util/KeyStateRunnable;->o:Lee/cyber/smartid/tse/inter/LogAccess;

    invoke-interface {v6, v3, v4}, Lee/cyber/smartid/tse/inter/LogAccess;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 355
    :cond_25
    :goto_11
    iget-object v3, v1, Lee/cyber/smartid/tse/util/KeyStateRunnable;->a:Lee/cyber/smartid/tse/inter/StateWorkerJobListener;

    if-eqz v3, :cond_28

    .line 356
    iget-object v4, v1, Lee/cyber/smartid/tse/util/KeyStateRunnable;->e:Lee/cyber/smartid/tse/dto/KeyState;

    if-eqz v4, :cond_26

    invoke-virtual {v4}, Lee/cyber/smartid/tse/dto/KeyState;->getId()Ljava/lang/String;

    move-result-object v5

    goto :goto_12

    :cond_26
    iget-object v4, v1, Lee/cyber/smartid/tse/util/KeyStateRunnable;->d:Lee/cyber/smartid/tse/dto/ProtoKeyState;

    if-eqz v4, :cond_27

    invoke-virtual {v4}, Lee/cyber/smartid/tse/dto/ProtoKeyState;->getId()Ljava/lang/String;

    move-result-object v5

    :cond_27
    :goto_12
    invoke-interface {v3, v5}, Lee/cyber/smartid/tse/inter/StateWorkerJobListener;->onJobCompleted(Ljava/lang/String;)V

    .line 358
    :cond_28
    iget-object v3, v1, Lee/cyber/smartid/tse/util/KeyStateRunnable;->o:Lee/cyber/smartid/tse/inter/LogAccess;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    goto/16 :goto_3

    :catchall_0
    move-exception v0

    move-object v4, v0

    goto :goto_16

    :catch_8
    move-exception v0

    move-object v4, v0

    .line 331
    :try_start_10
    iget-object v6, v1, Lee/cyber/smartid/tse/util/KeyStateRunnable;->l:Landroid/os/Handler;

    if-eqz v6, :cond_29

    .line 332
    iget-object v6, v1, Lee/cyber/smartid/tse/util/KeyStateRunnable;->l:Landroid/os/Handler;

    new-instance v7, Lee/cyber/smartid/tse/util/KeyStateRunnable$8;

    invoke-direct {v7, v1, v4}, Lee/cyber/smartid/tse/util/KeyStateRunnable$8;-><init>(Lee/cyber/smartid/tse/util/KeyStateRunnable;Ljava/lang/Throwable;)V

    invoke-virtual {v6, v7}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 343
    :cond_29
    iget-object v6, v1, Lee/cyber/smartid/tse/util/KeyStateRunnable;->o:Lee/cyber/smartid/tse/inter/LogAccess;

    invoke-interface {v6, v3, v4}, Lee/cyber/smartid/tse/inter/LogAccess;->e(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_10
    .catchall {:try_start_10 .. :try_end_10} :catchall_0

    .line 347
    :try_start_11
    iget-object v4, v1, Lee/cyber/smartid/tse/util/KeyStateRunnable;->d:Lee/cyber/smartid/tse/dto/ProtoKeyState;

    if-eqz v4, :cond_2a

    .line 349
    iget-object v4, v1, Lee/cyber/smartid/tse/util/KeyStateRunnable;->d:Lee/cyber/smartid/tse/dto/ProtoKeyState;

    invoke-virtual {v4}, Lee/cyber/smartid/tse/dto/ProtoKeyState;->getId()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v4}, Lee/cyber/smartid/tse/util/KeyStateRunnable;->a(Ljava/lang/String;)V
    :try_end_11
    .catch Ljava/lang/Throwable; {:try_start_11 .. :try_end_11} :catch_9

    goto :goto_13

    :catch_9
    move-exception v0

    move-object v4, v0

    .line 352
    iget-object v6, v1, Lee/cyber/smartid/tse/util/KeyStateRunnable;->o:Lee/cyber/smartid/tse/inter/LogAccess;

    invoke-interface {v6, v3, v4}, Lee/cyber/smartid/tse/inter/LogAccess;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 355
    :cond_2a
    :goto_13
    iget-object v3, v1, Lee/cyber/smartid/tse/util/KeyStateRunnable;->a:Lee/cyber/smartid/tse/inter/StateWorkerJobListener;

    if-eqz v3, :cond_2d

    .line 356
    iget-object v4, v1, Lee/cyber/smartid/tse/util/KeyStateRunnable;->e:Lee/cyber/smartid/tse/dto/KeyState;

    if-eqz v4, :cond_2b

    invoke-virtual {v4}, Lee/cyber/smartid/tse/dto/KeyState;->getId()Ljava/lang/String;

    move-result-object v5

    goto :goto_14

    :cond_2b
    iget-object v4, v1, Lee/cyber/smartid/tse/util/KeyStateRunnable;->d:Lee/cyber/smartid/tse/dto/ProtoKeyState;

    if-eqz v4, :cond_2c

    invoke-virtual {v4}, Lee/cyber/smartid/tse/dto/ProtoKeyState;->getId()Ljava/lang/String;

    move-result-object v5

    :cond_2c
    :goto_14
    invoke-interface {v3, v5}, Lee/cyber/smartid/tse/inter/StateWorkerJobListener;->onJobCompleted(Ljava/lang/String;)V

    .line 358
    :cond_2d
    iget-object v3, v1, Lee/cyber/smartid/tse/util/KeyStateRunnable;->o:Lee/cyber/smartid/tse/inter/LogAccess;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    :goto_15
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, v1, Lee/cyber/smartid/tse/util/KeyStateRunnable;->d:Lee/cyber/smartid/tse/dto/ProtoKeyState;

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v3, v2}, Lee/cyber/smartid/tse/inter/LogAccess;->d(Ljava/lang/String;)V

    return-void

    .line 347
    :goto_16
    :try_start_12
    iget-object v6, v1, Lee/cyber/smartid/tse/util/KeyStateRunnable;->d:Lee/cyber/smartid/tse/dto/ProtoKeyState;

    if-eqz v6, :cond_2e

    .line 349
    iget-object v6, v1, Lee/cyber/smartid/tse/util/KeyStateRunnable;->d:Lee/cyber/smartid/tse/dto/ProtoKeyState;

    invoke-virtual {v6}, Lee/cyber/smartid/tse/dto/ProtoKeyState;->getId()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v1, v6}, Lee/cyber/smartid/tse/util/KeyStateRunnable;->a(Ljava/lang/String;)V
    :try_end_12
    .catch Ljava/lang/Throwable; {:try_start_12 .. :try_end_12} :catch_a

    goto :goto_17

    :catch_a
    move-exception v0

    move-object v6, v0

    .line 352
    iget-object v7, v1, Lee/cyber/smartid/tse/util/KeyStateRunnable;->o:Lee/cyber/smartid/tse/inter/LogAccess;

    invoke-interface {v7, v3, v6}, Lee/cyber/smartid/tse/inter/LogAccess;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 355
    :cond_2e
    :goto_17
    iget-object v3, v1, Lee/cyber/smartid/tse/util/KeyStateRunnable;->a:Lee/cyber/smartid/tse/inter/StateWorkerJobListener;

    if-eqz v3, :cond_31

    .line 356
    iget-object v6, v1, Lee/cyber/smartid/tse/util/KeyStateRunnable;->e:Lee/cyber/smartid/tse/dto/KeyState;

    if-nez v6, :cond_2f

    iget-object v6, v1, Lee/cyber/smartid/tse/util/KeyStateRunnable;->d:Lee/cyber/smartid/tse/dto/ProtoKeyState;

    if-eqz v6, :cond_30

    invoke-virtual {v6}, Lee/cyber/smartid/tse/dto/ProtoKeyState;->getId()Ljava/lang/String;

    move-result-object v5

    goto :goto_18

    :cond_2f
    invoke-virtual {v6}, Lee/cyber/smartid/tse/dto/KeyState;->getId()Ljava/lang/String;

    move-result-object v5

    :cond_30
    :goto_18
    invoke-interface {v3, v5}, Lee/cyber/smartid/tse/inter/StateWorkerJobListener;->onJobCompleted(Ljava/lang/String;)V

    .line 358
    :cond_31
    iget-object v3, v1, Lee/cyber/smartid/tse/util/KeyStateRunnable;->o:Lee/cyber/smartid/tse/inter/LogAccess;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, v1, Lee/cyber/smartid/tse/util/KeyStateRunnable;->d:Lee/cyber/smartid/tse/dto/ProtoKeyState;

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v3, v2}, Lee/cyber/smartid/tse/inter/LogAccess;->d(Ljava/lang/String;)V

    .line 359
    throw v4
.end method
