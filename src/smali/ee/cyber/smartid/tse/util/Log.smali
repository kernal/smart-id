.class public Lee/cyber/smartid/tse/util/Log;
.super Ljava/lang/Object;
.source "Log.java"

# interfaces
.implements Lee/cyber/smartid/tse/inter/LogAccess;


# static fields
.field private static volatile a:Lee/cyber/smartid/tse/inter/LogAccess;


# instance fields
.field private b:Ljava/lang/String;


# direct methods
.method private constructor <init>(Ljava/lang/String;)V
    .locals 0

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-object p1, p0, Lee/cyber/smartid/tse/util/Log;->b:Ljava/lang/String;

    return-void
.end method

.method public static getInstance(Ljava/lang/Class;)Lee/cyber/smartid/tse/util/Log;
    .locals 0

    .line 40
    invoke-virtual {p0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object p0

    invoke-static {p0}, Lee/cyber/smartid/tse/util/Log;->getInstance(Ljava/lang/String;)Lee/cyber/smartid/tse/util/Log;

    move-result-object p0

    return-object p0
.end method

.method public static getInstance(Ljava/lang/Object;)Lee/cyber/smartid/tse/util/Log;
    .locals 0

    .line 30
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p0

    invoke-static {p0}, Lee/cyber/smartid/tse/util/Log;->getInstance(Ljava/lang/Class;)Lee/cyber/smartid/tse/util/Log;

    move-result-object p0

    return-object p0
.end method

.method public static getInstance(Ljava/lang/String;)Lee/cyber/smartid/tse/util/Log;
    .locals 1

    .line 50
    new-instance v0, Lee/cyber/smartid/tse/util/Log;

    invoke-direct {v0, p0}, Lee/cyber/smartid/tse/util/Log;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static setImpl(Lee/cyber/smartid/tse/inter/LogAccess;)V
    .locals 0

    .line 59
    sput-object p0, Lee/cyber/smartid/tse/util/Log;->a:Lee/cyber/smartid/tse/inter/LogAccess;

    return-void
.end method


# virtual methods
.method public d(Ljava/lang/String;)V
    .locals 1

    const/4 v0, 0x0

    .line 83
    invoke-virtual {p0, p1, v0}, Lee/cyber/smartid/tse/util/Log;->d(Ljava/lang/String;Ljava/lang/Throwable;)V

    return-void
.end method

.method public d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 0

    .line 93
    sget-object p1, Lee/cyber/smartid/tse/util/Log;->a:Lee/cyber/smartid/tse/inter/LogAccess;

    if-nez p1, :cond_0

    return-void

    .line 96
    :cond_0
    sget-object p1, Lee/cyber/smartid/tse/util/Log;->a:Lee/cyber/smartid/tse/inter/LogAccess;

    invoke-interface {p1, p2, p3}, Lee/cyber/smartid/tse/inter/LogAccess;->d(Ljava/lang/String;Ljava/lang/Throwable;)V

    return-void
.end method

.method public d(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 1

    .line 88
    iget-object v0, p0, Lee/cyber/smartid/tse/util/Log;->b:Ljava/lang/String;

    invoke-virtual {p0, v0, p1, p2}, Lee/cyber/smartid/tse/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    return-void
.end method

.method public e(Ljava/lang/String;)V
    .locals 1

    const/4 v0, 0x0

    .line 101
    invoke-virtual {p0, p1, v0}, Lee/cyber/smartid/tse/util/Log;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    return-void
.end method

.method public e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 0

    .line 111
    sget-object p1, Lee/cyber/smartid/tse/util/Log;->a:Lee/cyber/smartid/tse/inter/LogAccess;

    if-nez p1, :cond_0

    return-void

    .line 114
    :cond_0
    sget-object p1, Lee/cyber/smartid/tse/util/Log;->a:Lee/cyber/smartid/tse/inter/LogAccess;

    invoke-interface {p1, p2, p3}, Lee/cyber/smartid/tse/inter/LogAccess;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    return-void
.end method

.method public e(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 1

    .line 106
    iget-object v0, p0, Lee/cyber/smartid/tse/util/Log;->b:Ljava/lang/String;

    invoke-virtual {p0, v0, p1, p2}, Lee/cyber/smartid/tse/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    return-void
.end method

.method public i(Ljava/lang/String;)V
    .locals 1

    const/4 v0, 0x0

    .line 64
    invoke-virtual {p0, p1, v0}, Lee/cyber/smartid/tse/util/Log;->i(Ljava/lang/String;Ljava/lang/Throwable;)V

    return-void
.end method

.method public i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 0

    .line 75
    sget-object p1, Lee/cyber/smartid/tse/util/Log;->a:Lee/cyber/smartid/tse/inter/LogAccess;

    if-nez p1, :cond_0

    return-void

    .line 78
    :cond_0
    sget-object p1, Lee/cyber/smartid/tse/util/Log;->a:Lee/cyber/smartid/tse/inter/LogAccess;

    invoke-interface {p1, p2, p3}, Lee/cyber/smartid/tse/inter/LogAccess;->i(Ljava/lang/String;Ljava/lang/Throwable;)V

    return-void
.end method

.method public i(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 1

    .line 69
    iget-object v0, p0, Lee/cyber/smartid/tse/util/Log;->b:Ljava/lang/String;

    invoke-virtual {p0, v0, p1, p2}, Lee/cyber/smartid/tse/util/Log;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    return-void
.end method

.method public w(Ljava/lang/String;)V
    .locals 1

    const/4 v0, 0x0

    .line 119
    invoke-virtual {p0, p1, v0}, Lee/cyber/smartid/tse/util/Log;->w(Ljava/lang/String;Ljava/lang/Throwable;)V

    return-void
.end method

.method public w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 0

    .line 129
    sget-object p1, Lee/cyber/smartid/tse/util/Log;->a:Lee/cyber/smartid/tse/inter/LogAccess;

    if-nez p1, :cond_0

    return-void

    .line 132
    :cond_0
    sget-object p1, Lee/cyber/smartid/tse/util/Log;->a:Lee/cyber/smartid/tse/inter/LogAccess;

    invoke-interface {p1, p2, p3}, Lee/cyber/smartid/tse/inter/LogAccess;->w(Ljava/lang/String;Ljava/lang/Throwable;)V

    return-void
.end method

.method public w(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 1

    .line 124
    iget-object v0, p0, Lee/cyber/smartid/tse/util/Log;->b:Ljava/lang/String;

    invoke-virtual {p0, v0, p1, p2}, Lee/cyber/smartid/tse/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    return-void
.end method

.method public wtf(Ljava/lang/String;)V
    .locals 1

    const/4 v0, 0x0

    .line 137
    invoke-virtual {p0, p1, v0}, Lee/cyber/smartid/tse/util/Log;->wtf(Ljava/lang/String;Ljava/lang/Throwable;)V

    return-void
.end method

.method public wtf(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 0

    .line 147
    sget-object p1, Lee/cyber/smartid/tse/util/Log;->a:Lee/cyber/smartid/tse/inter/LogAccess;

    if-nez p1, :cond_0

    return-void

    .line 150
    :cond_0
    sget-object p1, Lee/cyber/smartid/tse/util/Log;->a:Lee/cyber/smartid/tse/inter/LogAccess;

    invoke-interface {p1, p2, p3}, Lee/cyber/smartid/tse/inter/LogAccess;->wtf(Ljava/lang/String;Ljava/lang/Throwable;)V

    return-void
.end method

.method public wtf(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 1

    .line 142
    iget-object v0, p0, Lee/cyber/smartid/tse/util/Log;->b:Ljava/lang/String;

    invoke-virtual {p0, v0, p1, p2}, Lee/cyber/smartid/tse/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    return-void
.end method
