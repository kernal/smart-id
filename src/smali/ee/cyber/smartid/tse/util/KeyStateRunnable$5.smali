.class Lee/cyber/smartid/tse/util/KeyStateRunnable$5;
.super Ljava/lang/Object;
.source "KeyStateRunnable.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lee/cyber/smartid/tse/util/KeyStateRunnable;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lee/cyber/smartid/tse/util/KeyStateRunnable;


# direct methods
.method constructor <init>(Lee/cyber/smartid/tse/util/KeyStateRunnable;)V
    .locals 0

    .line 274
    iput-object p1, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable$5;->a:Lee/cyber/smartid/tse/util/KeyStateRunnable;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 10

    .line 277
    iget-object v0, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable$5;->a:Lee/cyber/smartid/tse/util/KeyStateRunnable;

    invoke-static {v0}, Lee/cyber/smartid/tse/util/KeyStateRunnable;->a(Lee/cyber/smartid/tse/util/KeyStateRunnable;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable$5;->a:Lee/cyber/smartid/tse/util/KeyStateRunnable;

    invoke-static {v2}, Lee/cyber/smartid/tse/util/KeyStateRunnable;->a(Lee/cyber/smartid/tse/util/KeyStateRunnable;)Ljava/lang/String;

    move-result-object v3

    const-class v4, Lee/cyber/smartid/tse/inter/TSEListener;

    const/4 v5, 0x1

    invoke-static {v2, v3, v5, v4}, Lee/cyber/smartid/tse/util/KeyStateRunnable;->a(Lee/cyber/smartid/tse/util/KeyStateRunnable;Ljava/lang/String;ZLjava/lang/Class;)Lee/cyber/smartid/tse/inter/TSEListener;

    move-result-object v2

    iget-object v3, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable$5;->a:Lee/cyber/smartid/tse/util/KeyStateRunnable;

    invoke-static {v3}, Lee/cyber/smartid/tse/util/KeyStateRunnable;->b(Lee/cyber/smartid/tse/util/KeyStateRunnable;)Lee/cyber/smartid/tse/inter/WallClock;

    move-result-object v3

    iget-object v4, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable$5;->a:Lee/cyber/smartid/tse/util/KeyStateRunnable;

    invoke-static {v4}, Lee/cyber/smartid/tse/util/KeyStateRunnable;->d(Lee/cyber/smartid/tse/util/KeyStateRunnable;)Lee/cyber/smartid/tse/inter/ResourceAccess;

    move-result-object v4

    invoke-interface {v4}, Lee/cyber/smartid/tse/inter/ResourceAccess;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    sget v6, Lee/cyber/smartid/tse/R$string;->err_invalid_transaction_state_2:I

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    iget-object v8, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable$5;->a:Lee/cyber/smartid/tse/util/KeyStateRunnable;

    invoke-static {v8}, Lee/cyber/smartid/tse/util/KeyStateRunnable;->c(Lee/cyber/smartid/tse/util/KeyStateRunnable;)Lee/cyber/smartid/tse/dto/ProtoKeyState;

    move-result-object v8

    invoke-virtual {v8}, Lee/cyber/smartid/tse/dto/ProtoKeyState;->getHumanReadableNameForType()Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x0

    aput-object v8, v7, v9

    iget-object v8, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable$5;->a:Lee/cyber/smartid/tse/util/KeyStateRunnable;

    invoke-static {v8}, Lee/cyber/smartid/tse/util/KeyStateRunnable;->c(Lee/cyber/smartid/tse/util/KeyStateRunnable;)Lee/cyber/smartid/tse/dto/ProtoKeyState;

    move-result-object v8

    invoke-virtual {v8}, Lee/cyber/smartid/tse/dto/ProtoKeyState;->getHumanReadableNameForOperationOrigin()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v7, v5

    invoke-virtual {v4, v6, v7}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    const-wide/16 v5, 0x3f4    # 5.0E-321

    invoke-static {v3, v5, v6, v4}, Lee/cyber/smartid/tse/dto/TSEError;->from(Lee/cyber/smartid/tse/inter/WallClock;JLjava/lang/String;)Lee/cyber/smartid/tse/dto/TSEError;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, Lee/cyber/smartid/tse/util/KeyStateRunnable;->a(Lee/cyber/smartid/tse/util/KeyStateRunnable;Ljava/lang/String;Lee/cyber/smartid/tse/inter/TSEListener;Lee/cyber/smartid/tse/dto/TSEError;)V

    return-void
.end method
