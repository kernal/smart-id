.class public final Lee/cyber/smartid/tse/util/PRNGFixes;
.super Ljava/lang/Object;
.source "PRNGFixes.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lee/cyber/smartid/tse/util/PRNGFixes$LinuxPRNGSecureRandom;,
        Lee/cyber/smartid/tse/util/PRNGFixes$LinuxPRNGSecureRandomProvider;
    }
.end annotation


# static fields
.field private static final a:[B


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 53
    invoke-static {}, Lee/cyber/smartid/tse/util/PRNGFixes;->e()[B

    move-result-object v0

    sput-object v0, Lee/cyber/smartid/tse/util/PRNGFixes;->a:[B

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Z)V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/SecurityException;
        }
    .end annotation

    .line 122
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x12

    if-le v0, v1, :cond_0

    .line 124
    const-class p0, Lee/cyber/smartid/tse/util/PRNGFixes;

    invoke-static {p0}, Lee/cyber/smartid/tse/util/Log;->getInstance(Ljava/lang/Class;)Lee/cyber/smartid/tse/util/Log;

    move-result-object p0

    const-string v0, "installLinuxPRNGSecureRandom not needed"

    invoke-virtual {p0, v0}, Lee/cyber/smartid/tse/util/Log;->d(Ljava/lang/String;)V

    return-void

    .line 128
    :cond_0
    const-class v0, Lee/cyber/smartid/tse/util/PRNGFixes;

    invoke-static {v0}, Lee/cyber/smartid/tse/util/Log;->getInstance(Ljava/lang/Class;)Lee/cyber/smartid/tse/util/Log;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "installLinuxPRNGSecureRandom may be needed - providers: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "SecureRandom.SHA1PRNG"

    invoke-static {v2}, Ljava/security/Security;->getProviders(Ljava/lang/String;)[Ljava/security/Provider;

    move-result-object v3

    invoke-static {v3}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lee/cyber/smartid/tse/util/Log;->d(Ljava/lang/String;)V

    .line 129
    invoke-static {v2}, Ljava/security/Security;->getProviders(Ljava/lang/String;)[Ljava/security/Provider;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    .line 133
    array-length v3, v0

    if-lez v3, :cond_1

    aget-object v3, v0, v1

    if-eqz v3, :cond_1

    aget-object v3, v0, v1

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    const-string v4, "com.google.android.gms.org.conscrypt.OpenSSLProvider"

    invoke-static {v4, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 134
    const-class p0, Lee/cyber/smartid/tse/util/PRNGFixes;

    invoke-static {p0}, Lee/cyber/smartid/tse/util/Log;->getInstance(Ljava/lang/Class;)Lee/cyber/smartid/tse/util/Log;

    move-result-object p0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "installLinuxPRNGSecureRandom: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    aget-object v0, v0, v1

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, " is installed at the first position, we don\'t need to add the \"LinuxPRNGSecureRandomProvider\" provider."

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lee/cyber/smartid/tse/util/Log;->d(Ljava/lang/String;)V

    return-void

    :cond_1
    const/4 v3, 0x1

    if-eqz v0, :cond_3

    .line 140
    array-length v4, v0

    if-lt v4, v3, :cond_3

    const-class v4, Lee/cyber/smartid/tse/util/PRNGFixes$LinuxPRNGSecureRandomProvider;

    aget-object v5, v0, v1

    invoke-virtual {v5}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    goto :goto_0

    :cond_2
    if-eqz v0, :cond_5

    .line 150
    array-length p0, v0

    if-lez p0, :cond_5

    const-class p0, Lee/cyber/smartid/tse/util/PRNGFixes$LinuxPRNGSecureRandomProvider;

    aget-object v0, v0, v1

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_5

    .line 151
    const-class p0, Lee/cyber/smartid/tse/util/PRNGFixes;

    invoke-static {p0}, Lee/cyber/smartid/tse/util/Log;->getInstance(Ljava/lang/Class;)Lee/cyber/smartid/tse/util/Log;

    move-result-object p0

    const-string v0, "installLinuxPRNGSecureRandom \"LinuxPRNGSecureRandomProvider\"is already installed at the first position"

    invoke-virtual {p0, v0}, Lee/cyber/smartid/tse/util/Log;->d(Ljava/lang/String;)V

    goto :goto_1

    .line 141
    :cond_3
    :goto_0
    const-class v0, Lee/cyber/smartid/tse/util/PRNGFixes;

    invoke-static {v0}, Lee/cyber/smartid/tse/util/Log;->getInstance(Ljava/lang/Class;)Lee/cyber/smartid/tse/util/Log;

    move-result-object v0

    const-string v1, "installLinuxPRNGSecureRandom \"LinuxPRNGSecureRandomProvider\" is not installed at the first position, lets add the \"LinuxPRNGSecureRandomProvider\""

    invoke-virtual {v0, v1}, Lee/cyber/smartid/tse/util/Log;->d(Ljava/lang/String;)V

    .line 142
    new-instance v0, Lee/cyber/smartid/tse/util/PRNGFixes$LinuxPRNGSecureRandomProvider;

    invoke-direct {v0}, Lee/cyber/smartid/tse/util/PRNGFixes$LinuxPRNGSecureRandomProvider;-><init>()V

    if-eqz p0, :cond_4

    .line 144
    const-class p0, Lee/cyber/smartid/tse/util/PRNGFixes;

    invoke-static {p0}, Lee/cyber/smartid/tse/util/Log;->getInstance(Ljava/lang/Class;)Lee/cyber/smartid/tse/util/Log;

    move-result-object p0

    const-string v1, "installLinuxPRNGSecureRandom - removing the \"LinuxPRNGSecureRandomProvider\" from other positions than the first"

    invoke-virtual {p0, v1}, Lee/cyber/smartid/tse/util/Log;->d(Ljava/lang/String;)V

    .line 145
    invoke-virtual {v0}, Lee/cyber/smartid/tse/util/PRNGFixes$LinuxPRNGSecureRandomProvider;->getName()Ljava/lang/String;

    move-result-object p0

    invoke-static {p0}, Ljava/security/Security;->removeProvider(Ljava/lang/String;)V

    .line 147
    :cond_4
    const-class p0, Lee/cyber/smartid/tse/util/PRNGFixes;

    invoke-static {p0}, Lee/cyber/smartid/tse/util/Log;->getInstance(Ljava/lang/Class;)Lee/cyber/smartid/tse/util/Log;

    move-result-object p0

    const-string v1, "installLinuxPRNGSecureRandom - adding the \"LinuxPRNGSecureRandomProvider\" to the first position"

    invoke-virtual {p0, v1}, Lee/cyber/smartid/tse/util/Log;->d(Ljava/lang/String;)V

    .line 148
    invoke-static {v0, v3}, Ljava/security/Security;->insertProviderAt(Ljava/security/Provider;I)I

    .line 157
    :cond_5
    :goto_1
    new-instance p0, Ljava/security/SecureRandom;

    invoke-direct {p0}, Ljava/security/SecureRandom;-><init>()V

    .line 158
    const-class v0, Lee/cyber/smartid/tse/util/PRNGFixes$LinuxPRNGSecureRandomProvider;

    invoke-virtual {p0}, Ljava/security/SecureRandom;->getProvider()Ljava/security/Provider;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    :try_start_0
    const-string p0, "SHA1PRNG"

    .line 164
    invoke-static {p0}, Ljava/security/SecureRandom;->getInstance(Ljava/lang/String;)Ljava/security/SecureRandom;

    move-result-object p0
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0

    .line 168
    const-class v0, Lee/cyber/smartid/tse/util/PRNGFixes$LinuxPRNGSecureRandomProvider;

    invoke-virtual {p0}, Ljava/security/SecureRandom;->getProvider()Ljava/security/Provider;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 173
    const-class p0, Lee/cyber/smartid/tse/util/PRNGFixes;

    invoke-static {p0}, Lee/cyber/smartid/tse/util/Log;->getInstance(Ljava/lang/Class;)Lee/cyber/smartid/tse/util/Log;

    move-result-object p0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "installLinuxPRNGSecureRandom successful - providers: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/security/Security;->getProviders(Ljava/lang/String;)[Ljava/security/Provider;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lee/cyber/smartid/tse/util/Log;->d(Ljava/lang/String;)V

    return-void

    .line 169
    :cond_6
    new-instance v0, Ljava/lang/SecurityException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "SecureRandom.getInstance(\"SHA1PRNG\") backed by wrong Provider: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 171
    invoke-virtual {p0}, Ljava/security/SecureRandom;->getProvider()Ljava/security/Provider;

    move-result-object p0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p0

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v0, p0}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v0

    :catch_0
    move-exception p0

    .line 166
    new-instance v0, Ljava/lang/SecurityException;

    const-string v1, "SHA1PRNG not available"

    invoke-direct {v0, v1, p0}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0

    .line 159
    :cond_7
    new-instance v0, Ljava/lang/SecurityException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "new SecureRandom() backed by wrong Provider: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/security/SecureRandom;->getProvider()Ljava/security/Provider;

    move-result-object p0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p0

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v0, p0}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method static synthetic a()[B
    .locals 1

    .line 49
    invoke-static {}, Lee/cyber/smartid/tse/util/PRNGFixes;->c()[B

    move-result-object v0

    return-object v0
.end method

.method public static apply()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/SecurityException;
        }
    .end annotation

    .line 68
    invoke-static {}, Lee/cyber/smartid/tse/util/PRNGFixes;->b()V

    const/4 v0, 0x0

    .line 70
    :try_start_0
    invoke-static {v0}, Lee/cyber/smartid/tse/util/PRNGFixes;->a(Z)V
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    const/4 v0, 0x1

    .line 73
    invoke-static {v0}, Lee/cyber/smartid/tse/util/PRNGFixes;->a(Z)V

    :goto_0
    return-void
.end method

.method private static b()V
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/SecurityException;
        }
    .end annotation

    const-string v0, "org.apache.harmony.xnet.provider.jsse.NativeCrypto"

    .line 84
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x10

    if-lt v1, v2, :cond_2

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x12

    if-le v1, v2, :cond_0

    goto/16 :goto_0

    .line 90
    :cond_0
    const-class v1, Lee/cyber/smartid/tse/util/PRNGFixes;

    invoke-static {v1}, Lee/cyber/smartid/tse/util/Log;->getInstance(Ljava/lang/Class;)Lee/cyber/smartid/tse/util/Log;

    move-result-object v1

    const-string v2, "applyOpenSSLFix needed"

    invoke-virtual {v1, v2}, Lee/cyber/smartid/tse/util/Log;->d(Ljava/lang/String;)V

    .line 93
    :try_start_0
    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    const-string v2, "RAND_seed"

    const/4 v3, 0x1

    new-array v4, v3, [Ljava/lang/Class;

    const-class v5, [B

    const/4 v6, 0x0

    aput-object v5, v4, v6

    .line 94
    invoke-virtual {v1, v2, v4}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    new-array v2, v3, [Ljava/lang/Object;

    .line 95
    invoke-static {}, Lee/cyber/smartid/tse/util/PRNGFixes;->c()[B

    move-result-object v4

    aput-object v4, v2, v6

    const/4 v4, 0x0

    invoke-virtual {v1, v4, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 98
    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    const-string v1, "RAND_load_file"

    const/4 v2, 0x2

    new-array v5, v2, [Ljava/lang/Class;

    const-class v7, Ljava/lang/String;

    aput-object v7, v5, v6

    sget-object v7, Ljava/lang/Long;->TYPE:Ljava/lang/Class;

    aput-object v7, v5, v3

    .line 100
    invoke-virtual {v0, v1, v5}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    new-array v1, v2, [Ljava/lang/Object;

    const-string v2, "/dev/urandom"

    aput-object v2, v1, v6

    const/16 v2, 0x400

    .line 101
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v1, v3

    invoke-virtual {v0, v4, v1}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 98
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ne v0, v2, :cond_1

    .line 107
    const-class v0, Lee/cyber/smartid/tse/util/PRNGFixes;

    invoke-static {v0}, Lee/cyber/smartid/tse/util/Log;->getInstance(Ljava/lang/Class;)Lee/cyber/smartid/tse/util/Log;

    move-result-object v0

    const-string v1, "applyOpenSSLFix successful"

    invoke-virtual {v0, v1}, Lee/cyber/smartid/tse/util/Log;->d(Ljava/lang/String;)V

    return-void

    .line 103
    :cond_1
    new-instance v1, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unexpected number of bytes read from Linux PRNG: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception v0

    .line 109
    new-instance v1, Ljava/lang/SecurityException;

    const-string v2, "Failed to seed OpenSSL PRNG"

    invoke-direct {v1, v2, v0}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 87
    :cond_2
    :goto_0
    const-class v0, Lee/cyber/smartid/tse/util/PRNGFixes;

    invoke-static {v0}, Lee/cyber/smartid/tse/util/Log;->getInstance(Ljava/lang/Class;)Lee/cyber/smartid/tse/util/Log;

    move-result-object v0

    const-string v1, "applyOpenSSLFix not needed"

    invoke-virtual {v0, v1}, Lee/cyber/smartid/tse/util/Log;->d(Ljava/lang/String;)V

    return-void
.end method

.method private static c()[B
    .locals 4

    .line 322
    :try_start_0
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 323
    new-instance v1, Ljava/io/DataOutputStream;

    invoke-direct {v1, v0}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 326
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/io/DataOutputStream;->writeLong(J)V

    .line 327
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/io/DataOutputStream;->writeLong(J)V

    .line 328
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 329
    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 330
    sget-object v2, Lee/cyber/smartid/tse/util/PRNGFixes;->a:[B

    invoke-virtual {v1, v2}, Ljava/io/DataOutputStream;->write([B)V

    .line 331
    invoke-virtual {v1}, Ljava/io/DataOutputStream;->close()V

    .line 332
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    .line 334
    new-instance v1, Ljava/lang/SecurityException;

    const-string v2, "Failed to generate seed"

    invoke-direct {v1, v2, v0}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method private static d()Ljava/lang/String;
    .locals 4

    const/4 v0, 0x0

    .line 347
    :try_start_0
    const-class v1, Landroid/os/Build;

    const-string v2, "SERIAL"

    invoke-virtual {v1, v2}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object v1

    :catch_0
    move-exception v1

    .line 349
    const-class v2, Lee/cyber/smartid/tse/util/PRNGFixes;

    invoke-static {v2}, Lee/cyber/smartid/tse/util/Log;->getInstance(Ljava/lang/Class;)Lee/cyber/smartid/tse/util/Log;

    move-result-object v2

    const-string v3, "getDeviceSerialNumber"

    invoke-virtual {v2, v3, v1}, Lee/cyber/smartid/tse/util/Log;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    return-object v0
.end method

.method private static e()[B
    .locals 2

    .line 355
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 356
    sget-object v1, Landroid/os/Build;->FINGERPRINT:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 358
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 360
    :cond_0
    invoke-static {}, Lee/cyber/smartid/tse/util/PRNGFixes;->d()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 362
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 365
    :cond_1
    :try_start_0
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "UTF-8"

    invoke-virtual {v0, v1}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v0
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    .line 367
    :catch_0
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "UTF-8 encoding not supported"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
