.class Lee/cyber/smartid/tse/util/KeyStateRunnable$8;
.super Ljava/lang/Object;
.source "KeyStateRunnable.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lee/cyber/smartid/tse/util/KeyStateRunnable;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Ljava/lang/Throwable;

.field final synthetic b:Lee/cyber/smartid/tse/util/KeyStateRunnable;


# direct methods
.method constructor <init>(Lee/cyber/smartid/tse/util/KeyStateRunnable;Ljava/lang/Throwable;)V
    .locals 0

    .line 332
    iput-object p1, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable$8;->b:Lee/cyber/smartid/tse/util/KeyStateRunnable;

    iput-object p2, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable$8;->a:Ljava/lang/Throwable;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .line 336
    :try_start_0
    iget-object v0, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable$8;->b:Lee/cyber/smartid/tse/util/KeyStateRunnable;

    iget-object v1, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable$8;->b:Lee/cyber/smartid/tse/util/KeyStateRunnable;

    invoke-static {v1}, Lee/cyber/smartid/tse/util/KeyStateRunnable;->a(Lee/cyber/smartid/tse/util/KeyStateRunnable;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable$8;->b:Lee/cyber/smartid/tse/util/KeyStateRunnable;

    iget-object v3, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable$8;->b:Lee/cyber/smartid/tse/util/KeyStateRunnable;

    invoke-static {v3}, Lee/cyber/smartid/tse/util/KeyStateRunnable;->a(Lee/cyber/smartid/tse/util/KeyStateRunnable;)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    const-class v5, Lee/cyber/smartid/tse/inter/TSEListener;

    invoke-static {v2, v3, v4, v5}, Lee/cyber/smartid/tse/util/KeyStateRunnable;->a(Lee/cyber/smartid/tse/util/KeyStateRunnable;Ljava/lang/String;ZLjava/lang/Class;)Lee/cyber/smartid/tse/inter/TSEListener;

    move-result-object v2

    iget-object v3, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable$8;->b:Lee/cyber/smartid/tse/util/KeyStateRunnable;

    invoke-static {v3}, Lee/cyber/smartid/tse/util/KeyStateRunnable;->d(Lee/cyber/smartid/tse/util/KeyStateRunnable;)Lee/cyber/smartid/tse/inter/ResourceAccess;

    move-result-object v3

    invoke-interface {v3}, Lee/cyber/smartid/tse/inter/ResourceAccess;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    iget-object v4, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable$8;->b:Lee/cyber/smartid/tse/util/KeyStateRunnable;

    invoke-static {v4}, Lee/cyber/smartid/tse/util/KeyStateRunnable;->b(Lee/cyber/smartid/tse/util/KeyStateRunnable;)Lee/cyber/smartid/tse/inter/WallClock;

    move-result-object v4

    iget-object v5, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable$8;->a:Ljava/lang/Throwable;

    invoke-static {v3, v4, v5}, Lee/cyber/smartid/tse/dto/TSEError;->from(Landroid/content/Context;Lee/cyber/smartid/tse/inter/WallClock;Ljava/lang/Throwable;)Lee/cyber/smartid/tse/dto/TSEError;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, Lee/cyber/smartid/tse/util/KeyStateRunnable;->a(Lee/cyber/smartid/tse/util/KeyStateRunnable;Ljava/lang/String;Lee/cyber/smartid/tse/inter/TSEListener;Lee/cyber/smartid/tse/dto/TSEError;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 338
    iget-object v1, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable$8;->b:Lee/cyber/smartid/tse/util/KeyStateRunnable;

    invoke-static {v1}, Lee/cyber/smartid/tse/util/KeyStateRunnable;->e(Lee/cyber/smartid/tse/util/KeyStateRunnable;)Lee/cyber/smartid/tse/inter/LogAccess;

    move-result-object v1

    const-string v2, "run"

    invoke-interface {v1, v2, v0}, Lee/cyber/smartid/tse/inter/LogAccess;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    :goto_0
    return-void
.end method
