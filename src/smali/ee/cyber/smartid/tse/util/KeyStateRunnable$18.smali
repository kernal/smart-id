.class Lee/cyber/smartid/tse/util/KeyStateRunnable$18;
.super Ljava/lang/Object;
.source "KeyStateRunnable.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lee/cyber/smartid/tse/util/KeyStateRunnable;->a(Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/tse/dto/TSEError;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lee/cyber/smartid/tse/dto/TSEError;

.field final synthetic b:Ljava/lang/String;

.field final synthetic c:Ljava/lang/String;

.field final synthetic d:Lee/cyber/smartid/tse/util/KeyStateRunnable;


# direct methods
.method constructor <init>(Lee/cyber/smartid/tse/util/KeyStateRunnable;Lee/cyber/smartid/tse/dto/TSEError;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 993
    iput-object p1, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable$18;->d:Lee/cyber/smartid/tse/util/KeyStateRunnable;

    iput-object p2, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable$18;->a:Lee/cyber/smartid/tse/dto/TSEError;

    iput-object p3, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable$18;->b:Ljava/lang/String;

    iput-object p4, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable$18;->c:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .line 996
    iget-object v0, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable$18;->d:Lee/cyber/smartid/tse/util/KeyStateRunnable;

    iget-object v1, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable$18;->a:Lee/cyber/smartid/tse/dto/TSEError;

    invoke-static {v0, v1}, Lee/cyber/smartid/tse/util/KeyStateRunnable;->a(Lee/cyber/smartid/tse/util/KeyStateRunnable;Lee/cyber/smartid/tse/dto/TSEError;)Z

    move-result v0

    const/4 v1, 0x1

    xor-int/2addr v0, v1

    .line 998
    iget-object v2, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable$18;->d:Lee/cyber/smartid/tse/util/KeyStateRunnable;

    iget-object v3, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable$18;->b:Ljava/lang/String;

    const-class v4, Lee/cyber/smartid/tse/inter/SubmitClientSecondPartListener;

    invoke-static {v2, v3, v1, v4}, Lee/cyber/smartid/tse/util/KeyStateRunnable;->a(Lee/cyber/smartid/tse/util/KeyStateRunnable;Ljava/lang/String;ZLjava/lang/Class;)Lee/cyber/smartid/tse/inter/TSEListener;

    move-result-object v2

    check-cast v2, Lee/cyber/smartid/tse/inter/SubmitClientSecondPartListener;

    const/4 v3, 0x0

    const-string v4, "notifySubmitClientSecondPartFail"

    if-eqz v2, :cond_1

    .line 1001
    :try_start_0
    iget-object v5, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable$18;->b:Ljava/lang/String;

    iget-object v6, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable$18;->a:Lee/cyber/smartid/tse/dto/TSEError;

    if-nez v0, :cond_0

    const/4 v7, 0x1

    goto :goto_0

    :cond_0
    const/4 v7, 0x0

    :goto_0
    invoke-virtual {v6, v7}, Lee/cyber/smartid/tse/dto/TSEError;->asNonRetriable(Z)Lee/cyber/smartid/tse/dto/TSEError;

    move-result-object v6

    invoke-interface {v2, v5, v6}, Lee/cyber/smartid/tse/inter/SubmitClientSecondPartListener;->onSubmitClientSecondPartFailed(Ljava/lang/String;Lee/cyber/smartid/tse/dto/TSEError;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v5

    .line 1004
    iget-object v6, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable$18;->d:Lee/cyber/smartid/tse/util/KeyStateRunnable;

    invoke-static {v6}, Lee/cyber/smartid/tse/util/KeyStateRunnable;->e(Lee/cyber/smartid/tse/util/KeyStateRunnable;)Lee/cyber/smartid/tse/inter/LogAccess;

    move-result-object v6

    invoke-interface {v6, v4, v5}, Lee/cyber/smartid/tse/inter/LogAccess;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_1
    :goto_1
    if-nez v2, :cond_3

    .line 1009
    :try_start_1
    iget-object v2, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable$18;->d:Lee/cyber/smartid/tse/util/KeyStateRunnable;

    invoke-static {v2}, Lee/cyber/smartid/tse/util/KeyStateRunnable;->f(Lee/cyber/smartid/tse/util/KeyStateRunnable;)Lee/cyber/smartid/tse/inter/ExternalResourceAccess;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 1010
    iget-object v2, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable$18;->d:Lee/cyber/smartid/tse/util/KeyStateRunnable;

    invoke-static {v2}, Lee/cyber/smartid/tse/util/KeyStateRunnable;->f(Lee/cyber/smartid/tse/util/KeyStateRunnable;)Lee/cyber/smartid/tse/inter/ExternalResourceAccess;

    move-result-object v2

    iget-object v5, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable$18;->b:Ljava/lang/String;

    iget-object v6, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable$18;->a:Lee/cyber/smartid/tse/dto/TSEError;

    if-nez v0, :cond_2

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    invoke-virtual {v6, v1}, Lee/cyber/smartid/tse/dto/TSEError;->asNonRetriable(Z)Lee/cyber/smartid/tse/dto/TSEError;

    move-result-object v0

    iget-object v1, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable$18;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lee/cyber/smartid/tse/dto/TSEError;->withKeyId(Ljava/lang/String;)Lee/cyber/smartid/tse/dto/TSEError;

    move-result-object v0

    invoke-interface {v2, v5, v0}, Lee/cyber/smartid/tse/inter/ExternalResourceAccess;->onSubmitClientSecondPartFailed(Ljava/lang/String;Lee/cyber/smartid/tse/dto/TSEError;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_3

    :catch_1
    move-exception v0

    .line 1013
    iget-object v1, p0, Lee/cyber/smartid/tse/util/KeyStateRunnable$18;->d:Lee/cyber/smartid/tse/util/KeyStateRunnable;

    invoke-static {v1}, Lee/cyber/smartid/tse/util/KeyStateRunnable;->e(Lee/cyber/smartid/tse/util/KeyStateRunnable;)Lee/cyber/smartid/tse/inter/LogAccess;

    move-result-object v1

    invoke-interface {v1, v4, v0}, Lee/cyber/smartid/tse/inter/LogAccess;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_3
    :goto_3
    return-void
.end method
