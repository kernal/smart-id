.class Lee/cyber/smartid/tse/KeyManager$13$10;
.super Ljava/lang/Object;
.source "KeyManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lee/cyber/smartid/tse/KeyManager$13;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lee/cyber/smartid/tse/dto/jsonrpc/resp/ValidateKeyCreationResp;

.field final synthetic b:Lee/cyber/smartid/tse/KeyManager$13;


# direct methods
.method constructor <init>(Lee/cyber/smartid/tse/KeyManager$13;Lee/cyber/smartid/tse/dto/jsonrpc/resp/ValidateKeyCreationResp;)V
    .locals 0

    .line 1041
    iput-object p1, p0, Lee/cyber/smartid/tse/KeyManager$13$10;->b:Lee/cyber/smartid/tse/KeyManager$13;

    iput-object p2, p0, Lee/cyber/smartid/tse/KeyManager$13$10;->a:Lee/cyber/smartid/tse/dto/jsonrpc/resp/ValidateKeyCreationResp;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .line 1044
    iget-object v0, p0, Lee/cyber/smartid/tse/KeyManager$13$10;->b:Lee/cyber/smartid/tse/KeyManager$13;

    iget-object v0, v0, Lee/cyber/smartid/tse/KeyManager$13;->d:Lee/cyber/smartid/tse/KeyManager;

    invoke-static {v0}, Lee/cyber/smartid/tse/KeyManager;->a(Lee/cyber/smartid/tse/KeyManager;)Lee/cyber/smartid/tse/inter/ListenerAccess;

    move-result-object v0

    iget-object v1, p0, Lee/cyber/smartid/tse/KeyManager$13$10;->b:Lee/cyber/smartid/tse/KeyManager$13;

    iget-object v1, v1, Lee/cyber/smartid/tse/KeyManager$13;->b:Ljava/lang/String;

    const-class v2, Lee/cyber/smartid/tse/inter/ValidateKeyCreationResponseListener;

    const/4 v3, 0x1

    invoke-interface {v0, v1, v3, v2}, Lee/cyber/smartid/tse/inter/ListenerAccess;->getListener(Ljava/lang/String;ZLjava/lang/Class;)Lee/cyber/smartid/tse/inter/TSEListener;

    move-result-object v0

    check-cast v0, Lee/cyber/smartid/tse/inter/ValidateKeyCreationResponseListener;

    if-eqz v0, :cond_0

    .line 1046
    iget-object v1, p0, Lee/cyber/smartid/tse/KeyManager$13$10;->b:Lee/cyber/smartid/tse/KeyManager$13;

    iget-object v1, v1, Lee/cyber/smartid/tse/KeyManager$13;->b:Ljava/lang/String;

    iget-object v2, p0, Lee/cyber/smartid/tse/KeyManager$13$10;->a:Lee/cyber/smartid/tse/dto/jsonrpc/resp/ValidateKeyCreationResp;

    invoke-interface {v0, v1, v2}, Lee/cyber/smartid/tse/inter/ValidateKeyCreationResponseListener;->onValidateKeyCreationResponseSuccess(Ljava/lang/String;Lee/cyber/smartid/tse/dto/jsonrpc/resp/ValidateKeyCreationResp;)V

    :cond_0
    return-void
.end method
