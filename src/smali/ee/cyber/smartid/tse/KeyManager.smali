.class final Lee/cyber/smartid/tse/KeyManager;
.super Ljava/lang/Object;
.source "KeyManager.java"

# interfaces
.implements Lee/cyber/smartid/tse/inter/KeyManagerAccess;


# static fields
.field private static final a:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lee/cyber/smartid/tse/dto/PinValidationRule;",
            ">;"
        }
    .end annotation
.end field

.field private static volatile b:Lee/cyber/smartid/tse/KeyManager;


# instance fields
.field private final c:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Lee/cyber/smartid/tse/dto/KeyMaterial;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Lee/cyber/smartid/tse/inter/ResourceAccess;

.field private final e:Lee/cyber/smartid/tse/inter/ListenerAccess;

.field private final f:Lee/cyber/smartid/tse/inter/KeyStorageAccess;

.field private final g:Lee/cyber/smartid/tse/inter/AlarmAccess;

.field private final h:Lee/cyber/smartid/cryptolib/inter/KeyGenerationOp;

.field private final i:Lee/cyber/smartid/cryptolib/inter/RandomGenerationOp;

.field private final j:Lee/cyber/smartid/cryptolib/inter/StorageOp;

.field private final k:Lee/cyber/smartid/cryptolib/inter/EncodingOp;

.field private final l:Lee/cyber/smartid/cryptolib/inter/CryptoOp;

.field private final m:Lee/cyber/smartid/tse/inter/WallClock;

.field private final n:Ljava/lang/Object;

.field private final o:Lee/cyber/smartid/tse/util/Log;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 92
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lee/cyber/smartid/tse/KeyManager;->a:Ljava/util/ArrayList;

    .line 95
    sget-object v0, Lee/cyber/smartid/tse/KeyManager;->a:Ljava/util/ArrayList;

    new-instance v1, Lee/cyber/smartid/tse/dto/PinValidationRuleRegExp;

    const-string v2, "^([0-9])\\1*$"

    invoke-direct {v1, v2}, Lee/cyber/smartid/tse/dto/PinValidationRuleRegExp;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 96
    sget-object v0, Lee/cyber/smartid/tse/KeyManager;->a:Ljava/util/ArrayList;

    new-instance v1, Lee/cyber/smartid/tse/dto/PinValidationRuleRegExp;

    const-string v2, "^\\d{0,3}$"

    invoke-direct {v1, v2}, Lee/cyber/smartid/tse/dto/PinValidationRuleRegExp;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 97
    sget-object v0, Lee/cyber/smartid/tse/KeyManager;->a:Ljava/util/ArrayList;

    new-instance v1, Lee/cyber/smartid/tse/dto/PinValidationRuleIsContainedIn;

    const-string v2, "012345678901234567890"

    invoke-direct {v1, v2}, Lee/cyber/smartid/tse/dto/PinValidationRuleIsContainedIn;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 98
    sget-object v0, Lee/cyber/smartid/tse/KeyManager;->a:Ljava/util/ArrayList;

    new-instance v1, Lee/cyber/smartid/tse/dto/PinValidationRuleIsContainedIn;

    const-string v2, "987654321098765432109"

    invoke-direct {v1, v2}, Lee/cyber/smartid/tse/dto/PinValidationRuleIsContainedIn;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method private constructor <init>(Lee/cyber/smartid/tse/inter/KeyStorageAccess;Lee/cyber/smartid/tse/inter/ResourceAccess;Lee/cyber/smartid/tse/inter/ListenerAccess;Lee/cyber/smartid/tse/inter/AlarmAccess;Lee/cyber/smartid/cryptolib/inter/KeyGenerationOp;Lee/cyber/smartid/cryptolib/inter/RandomGenerationOp;Lee/cyber/smartid/cryptolib/inter/StorageOp;Lee/cyber/smartid/cryptolib/inter/EncodingOp;Lee/cyber/smartid/cryptolib/inter/CryptoOp;Lee/cyber/smartid/tse/inter/WallClock;)V
    .locals 1

    .line 227
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 117
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lee/cyber/smartid/tse/KeyManager;->c:Ljava/util/HashMap;

    .line 161
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lee/cyber/smartid/tse/KeyManager;->n:Ljava/lang/Object;

    .line 165
    invoke-static {p0}, Lee/cyber/smartid/tse/util/Log;->getInstance(Ljava/lang/Object;)Lee/cyber/smartid/tse/util/Log;

    move-result-object v0

    iput-object v0, p0, Lee/cyber/smartid/tse/KeyManager;->o:Lee/cyber/smartid/tse/util/Log;

    .line 228
    iput-object p2, p0, Lee/cyber/smartid/tse/KeyManager;->d:Lee/cyber/smartid/tse/inter/ResourceAccess;

    .line 229
    iput-object p3, p0, Lee/cyber/smartid/tse/KeyManager;->e:Lee/cyber/smartid/tse/inter/ListenerAccess;

    .line 230
    iput-object p1, p0, Lee/cyber/smartid/tse/KeyManager;->f:Lee/cyber/smartid/tse/inter/KeyStorageAccess;

    .line 231
    iput-object p4, p0, Lee/cyber/smartid/tse/KeyManager;->g:Lee/cyber/smartid/tse/inter/AlarmAccess;

    .line 232
    iput-object p5, p0, Lee/cyber/smartid/tse/KeyManager;->h:Lee/cyber/smartid/cryptolib/inter/KeyGenerationOp;

    .line 233
    iput-object p6, p0, Lee/cyber/smartid/tse/KeyManager;->i:Lee/cyber/smartid/cryptolib/inter/RandomGenerationOp;

    .line 234
    iput-object p7, p0, Lee/cyber/smartid/tse/KeyManager;->j:Lee/cyber/smartid/cryptolib/inter/StorageOp;

    .line 235
    iput-object p8, p0, Lee/cyber/smartid/tse/KeyManager;->k:Lee/cyber/smartid/cryptolib/inter/EncodingOp;

    .line 236
    iput-object p9, p0, Lee/cyber/smartid/tse/KeyManager;->l:Lee/cyber/smartid/cryptolib/inter/CryptoOp;

    .line 237
    iput-object p10, p0, Lee/cyber/smartid/tse/KeyManager;->m:Lee/cyber/smartid/tse/inter/WallClock;

    return-void
.end method

.method private a(Ljava/math/BigInteger;Ljava/lang/String;I)Lee/cyber/smartid/cryptolib/dto/ClientShare;
    .locals 1

    .line 714
    iget-object v0, p0, Lee/cyber/smartid/tse/KeyManager;->l:Lee/cyber/smartid/cryptolib/inter/CryptoOp;

    invoke-interface {v0, p1, p2, p3}, Lee/cyber/smartid/cryptolib/inter/CryptoOp;->encryptKey(Ljava/math/BigInteger;Ljava/lang/String;I)Lee/cyber/smartid/cryptolib/dto/ClientShare;

    move-result-object p1

    return-object p1
.end method

.method static synthetic a(Lee/cyber/smartid/tse/KeyManager;)Lee/cyber/smartid/tse/inter/ListenerAccess;
    .locals 0

    .line 84
    iget-object p0, p0, Lee/cyber/smartid/tse/KeyManager;->e:Lee/cyber/smartid/tse/inter/ListenerAccess;

    return-object p0
.end method

.method static synthetic a(Lee/cyber/smartid/tse/KeyManager;Ljava/util/ArrayList;)Ljava/lang/Runnable;
    .locals 0

    .line 84
    invoke-direct {p0, p1}, Lee/cyber/smartid/tse/KeyManager;->a(Ljava/util/ArrayList;)Ljava/lang/Runnable;

    move-result-object p0

    return-object p0
.end method

.method private a(Ljava/util/ArrayList;)Ljava/lang/Runnable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/lang/Runnable;"
        }
    .end annotation

    .line 334
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    if-eqz p1, :cond_0

    .line 336
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 339
    :cond_0
    new-instance p1, Lee/cyber/smartid/tse/KeyManager$2;

    invoke-direct {p1, p0, v0}, Lee/cyber/smartid/tse/KeyManager$2;-><init>(Lee/cyber/smartid/tse/KeyManager;Ljava/util/ArrayList;)V

    return-object p1
.end method

.method static synthetic a(Lee/cyber/smartid/tse/KeyManager;Ljava/util/ArrayList;I)Ljava/util/ArrayList;
    .locals 0

    .line 84
    invoke-direct {p0, p1, p2}, Lee/cyber/smartid/tse/KeyManager;->a(Ljava/util/ArrayList;I)Ljava/util/ArrayList;

    move-result-object p0

    return-object p0
.end method

.method private a(Ljava/util/ArrayList;I)Ljava/util/ArrayList;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "[",
            "Ljava/lang/String;",
            ">;I)",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 359
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    if-eqz p1, :cond_3

    .line 360
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-nez v1, :cond_0

    goto :goto_2

    .line 365
    :cond_0
    iget-object v1, p0, Lee/cyber/smartid/tse/KeyManager;->i:Lee/cyber/smartid/cryptolib/inter/RandomGenerationOp;

    invoke-interface {v1}, Lee/cyber/smartid/cryptolib/inter/RandomGenerationOp;->getRandom()Ljava/security/SecureRandom;

    move-result-object v1

    .line 366
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Ljava/lang/String;

    .line 367
    new-instance v4, Ljava/math/BigInteger;

    const/16 v5, 0x200

    invoke-direct {v4, v5, v1}, Ljava/math/BigInteger;-><init>(ILjava/util/Random;)V

    const/16 v6, 0x10

    invoke-virtual {v4, v6}, Ljava/math/BigInteger;->toString(I)Ljava/lang/String;

    move-result-object v4

    .line 368
    :goto_1
    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 370
    new-instance v4, Ljava/math/BigInteger;

    invoke-direct {v4, v5, v1}, Ljava/math/BigInteger;-><init>(ILjava/util/Random;)V

    invoke-virtual {v4, v6}, Ljava/math/BigInteger;->toString(I)Ljava/lang/String;

    move-result-object v4

    goto :goto_1

    .line 372
    :cond_1
    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 374
    new-instance v5, Lee/cyber/smartid/tse/dto/KeyMaterial;

    const/4 v6, 0x0

    aget-object v6, v3, v6

    const/4 v7, 0x1

    aget-object v7, v3, v7

    const/4 v8, 0x2

    aget-object v3, v3, v8

    invoke-direct {v5, v6, v7, v3, p2}, Lee/cyber/smartid/tse/dto/KeyMaterial;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-direct {p0, v4, v5}, Lee/cyber/smartid/tse/KeyManager;->a(Ljava/lang/String;Lee/cyber/smartid/tse/dto/KeyMaterial;)V

    goto :goto_0

    .line 377
    :cond_2
    invoke-virtual {p1}, Ljava/util/ArrayList;->clear()V

    :cond_3
    :goto_2
    return-object v0
.end method

.method private a()Ljava/util/HashMap;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Lee/cyber/smartid/tse/dto/EncryptKeyWrapper;",
            ">;"
        }
    .end annotation

    .line 720
    iget-object v0, p0, Lee/cyber/smartid/tse/KeyManager;->j:Lee/cyber/smartid/cryptolib/inter/StorageOp;

    iget-object v1, p0, Lee/cyber/smartid/tse/KeyManager;->d:Lee/cyber/smartid/tse/inter/ResourceAccess;

    invoke-interface {v1}, Lee/cyber/smartid/tse/inter/ResourceAccess;->getEncryptedKeysStorageId()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lee/cyber/smartid/tse/KeyManager$11;

    invoke-direct {v2, p0}, Lee/cyber/smartid/tse/KeyManager$11;-><init>(Lee/cyber/smartid/tse/KeyManager;)V

    .line 721
    invoke-virtual {v2}, Lee/cyber/smartid/tse/KeyManager$11;->getType()Ljava/lang/reflect/Type;

    move-result-object v2

    .line 720
    invoke-interface {v0, v1, v2}, Lee/cyber/smartid/cryptolib/inter/StorageOp;->retrieveData(Ljava/lang/String;Ljava/lang/reflect/Type;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/HashMap;

    if-nez v0, :cond_0

    .line 723
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    :cond_0
    return-object v0
.end method

.method static synthetic a(Lee/cyber/smartid/tse/KeyManager;Lee/cyber/smartid/tse/dto/EncryptKeyWrapper;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lee/cyber/smartid/cryptolib/dto/StorageException;
        }
    .end annotation

    .line 84
    invoke-direct {p0, p1}, Lee/cyber/smartid/tse/KeyManager;->a(Lee/cyber/smartid/tse/dto/EncryptKeyWrapper;)V

    return-void
.end method

.method static synthetic a(Lee/cyber/smartid/tse/KeyManager;Ljava/lang/String;)V
    .locals 0

    .line 84
    invoke-direct {p0, p1}, Lee/cyber/smartid/tse/KeyManager;->b(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic a(Lee/cyber/smartid/tse/KeyManager;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 84
    invoke-direct {p0, p1, p2, p3, p4}, Lee/cyber/smartid/tse/KeyManager;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic a(Lee/cyber/smartid/tse/KeyManager;[Lee/cyber/smartid/cryptolib/dto/TestResult;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lee/cyber/smartid/cryptolib/dto/StorageException;
        }
    .end annotation

    .line 84
    invoke-direct {p0, p1}, Lee/cyber/smartid/tse/KeyManager;->a([Lee/cyber/smartid/cryptolib/dto/TestResult;)V

    return-void
.end method

.method private a(Lee/cyber/smartid/tse/dto/EncryptKeyWrapper;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lee/cyber/smartid/cryptolib/dto/StorageException;
        }
    .end annotation

    .line 742
    iget-object v0, p0, Lee/cyber/smartid/tse/KeyManager;->n:Ljava/lang/Object;

    monitor-enter v0

    if-eqz p1, :cond_0

    .line 743
    :try_start_0
    invoke-virtual {p1}, Lee/cyber/smartid/tse/dto/EncryptKeyWrapper;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 747
    iget-object v1, p0, Lee/cyber/smartid/tse/KeyManager;->o:Lee/cyber/smartid/tse/util/Log;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "addEncryptedKey - id: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lee/cyber/smartid/tse/dto/EncryptKeyWrapper;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lee/cyber/smartid/tse/util/Log;->d(Ljava/lang/String;)V

    .line 748
    invoke-direct {p0, p1}, Lee/cyber/smartid/tse/KeyManager;->b(Lee/cyber/smartid/tse/dto/EncryptKeyWrapper;)V

    .line 751
    invoke-virtual {p1}, Lee/cyber/smartid/tse/dto/EncryptKeyWrapper;->getId()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lee/cyber/smartid/tse/KeyManager;->a(Ljava/lang/String;)V

    .line 752
    monitor-exit v0

    return-void

    .line 744
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v1, "Key references or the key data can\'t be empty!"

    invoke-direct {p1, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    :catchall_0
    move-exception p1

    .line 752
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method private a(Ljava/lang/String;J)V
    .locals 3

    .line 837
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 840
    :cond_0
    iget-object v0, p0, Lee/cyber/smartid/tse/KeyManager;->o:Lee/cyber/smartid/tse/util/Log;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setEncryptedKeyRemoveAlarm: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lee/cyber/smartid/tse/util/Log;->d(Ljava/lang/String;)V

    .line 841
    iget-object v0, p0, Lee/cyber/smartid/tse/KeyManager;->g:Lee/cyber/smartid/tse/inter/AlarmAccess;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ee.cyber.smartid.ACTION_REMOVE_ENCRYPTED_KEYS_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p2, p3, p1}, Lee/cyber/smartid/tse/inter/AlarmAccess;->scheduleAlarmFor(Ljava/lang/String;JLjava/lang/String;)V

    return-void
.end method

.method private a(Ljava/lang/String;Lee/cyber/smartid/tse/dto/KeyMaterial;)V
    .locals 2

    .line 391
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    if-nez p2, :cond_0

    goto :goto_0

    .line 395
    :cond_0
    iget-object v0, p0, Lee/cyber/smartid/tse/KeyManager;->c:Ljava/util/HashMap;

    monitor-enter v0

    .line 396
    :try_start_0
    iget-object v1, p0, Lee/cyber/smartid/tse/KeyManager;->c:Ljava/util/HashMap;

    invoke-virtual {v1, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 397
    monitor-exit v0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1

    .line 392
    :cond_1
    :goto_0
    iget-object p1, p0, Lee/cyber/smartid/tse/KeyManager;->o:Lee/cyber/smartid/tse/util/Log;

    const-string p2, "addGeneratedKeyToMemory: Empty reference or key value is not allowed!"

    invoke-virtual {p1, p2}, Lee/cyber/smartid/tse/util/Log;->w(Ljava/lang/String;)V

    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 9

    .line 625
    iget-object v0, p0, Lee/cyber/smartid/tse/KeyManager;->o:Lee/cyber/smartid/tse/util/Log;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "encryptKeyInternal - "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lee/cyber/smartid/tse/util/Log;->d(Ljava/lang/String;)V

    .line 627
    invoke-direct {p0, p2}, Lee/cyber/smartid/tse/KeyManager;->c(Ljava/lang/String;)Lee/cyber/smartid/tse/dto/KeyMaterial;

    move-result-object v0

    if-nez v0, :cond_0

    .line 630
    iget-object p2, p0, Lee/cyber/smartid/tse/KeyManager;->e:Lee/cyber/smartid/tse/inter/ListenerAccess;

    new-instance p3, Lee/cyber/smartid/tse/KeyManager$7;

    invoke-direct {p3, p0, p1}, Lee/cyber/smartid/tse/KeyManager$7;-><init>(Lee/cyber/smartid/tse/KeyManager;Ljava/lang/String;)V

    invoke-interface {p2, p3}, Lee/cyber/smartid/tse/inter/ListenerAccess;->notifyUI(Ljava/lang/Runnable;)V

    return-void

    .line 640
    :cond_0
    iget-object v1, p0, Lee/cyber/smartid/tse/KeyManager;->d:Lee/cyber/smartid/tse/inter/ResourceAccess;

    invoke-interface {v1, p4}, Lee/cyber/smartid/tse/inter/ResourceAccess;->getPreferredSZKTKPublicKey(Ljava/lang/String;)Lee/cyber/smartid/tse/dto/KTKPublicKey;

    move-result-object v6

    if-nez v6, :cond_1

    .line 642
    iget-object p2, p0, Lee/cyber/smartid/tse/KeyManager;->e:Lee/cyber/smartid/tse/inter/ListenerAccess;

    new-instance p3, Lee/cyber/smartid/tse/KeyManager$8;

    invoke-direct {p3, p0, p1}, Lee/cyber/smartid/tse/KeyManager$8;-><init>(Lee/cyber/smartid/tse/KeyManager;Ljava/lang/String;)V

    invoke-interface {p2, p3}, Lee/cyber/smartid/tse/inter/ListenerAccess;->notifyUI(Ljava/lang/Runnable;)V

    return-void

    .line 652
    :cond_1
    new-instance v4, Lee/cyber/smartid/tse/dto/EncryptKeyWrapper;

    iget-object p4, p0, Lee/cyber/smartid/tse/KeyManager;->m:Lee/cyber/smartid/tse/inter/WallClock;

    invoke-direct {v4, p4, p2}, Lee/cyber/smartid/tse/dto/EncryptKeyWrapper;-><init>(Lee/cyber/smartid/tse/inter/WallClock;Ljava/lang/String;)V

    if-eqz p3, :cond_2

    .line 654
    invoke-virtual {p3}, Ljava/lang/String;->length()I

    move-result p4

    goto :goto_0

    :cond_2
    const/4 p4, -0x1

    .line 656
    :goto_0
    :try_start_0
    new-instance v1, Lee/cyber/smartid/tse/dto/Key;

    iget-object v2, p0, Lee/cyber/smartid/tse/KeyManager;->k:Lee/cyber/smartid/cryptolib/inter/EncodingOp;

    invoke-virtual {v0}, Lee/cyber/smartid/tse/dto/KeyMaterial;->getD1Prime()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lee/cyber/smartid/cryptolib/inter/EncodingOp;->decodeDecimalFromBase64(Ljava/lang/String;)Ljava/math/BigInteger;

    move-result-object v2

    invoke-virtual {v0}, Lee/cyber/smartid/tse/dto/KeyMaterial;->getKeySize()I

    move-result v3

    invoke-direct {p0, v2, p3, v3}, Lee/cyber/smartid/tse/KeyManager;->a(Ljava/math/BigInteger;Ljava/lang/String;I)Lee/cyber/smartid/cryptolib/dto/ClientShare;

    move-result-object p3

    invoke-virtual {v0}, Lee/cyber/smartid/tse/dto/KeyMaterial;->getN1()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v6}, Lee/cyber/smartid/tse/dto/KTKPublicKey;->getSzId()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, p3, v2, p4, v3}, Lee/cyber/smartid/tse/dto/Key;-><init>(Lee/cyber/smartid/cryptolib/dto/ClientShare;Ljava/lang/String;ILjava/lang/String;)V

    .line 657
    iget-object p3, p0, Lee/cyber/smartid/tse/KeyManager;->l:Lee/cyber/smartid/cryptolib/inter/CryptoOp;

    invoke-virtual {v0}, Lee/cyber/smartid/tse/dto/KeyMaterial;->getD1PrimePrime()Ljava/lang/String;

    move-result-object p4

    const-string v2, "CLIENT2NDPART"

    invoke-virtual {v0}, Lee/cyber/smartid/tse/dto/KeyMaterial;->getKeySize()I

    move-result v0

    invoke-static {v6, p3, p4, v2, v0}, Lee/cyber/smartid/tse/util/Util;->encryptToKTKEncryptedJWE(Lee/cyber/smartid/tse/dto/KTKPublicKey;Lee/cyber/smartid/cryptolib/inter/CryptoOp;Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object p3

    const-string p4, "JWE"

    invoke-virtual {v4, v1, p3, p4}, Lee/cyber/smartid/tse/dto/EncryptKeyWrapper;->setKeyData(Lee/cyber/smartid/tse/dto/Key;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 673
    new-instance v7, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object p3

    invoke-direct {v7, p3}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 674
    new-instance p3, Ljava/lang/Thread;

    new-instance p4, Lee/cyber/smartid/tse/KeyManager$10;

    move-object v2, p4

    move-object v3, p0

    move-object v5, p2

    move-object v8, p1

    invoke-direct/range {v2 .. v8}, Lee/cyber/smartid/tse/KeyManager$10;-><init>(Lee/cyber/smartid/tse/KeyManager;Lee/cyber/smartid/tse/dto/EncryptKeyWrapper;Ljava/lang/String;Lee/cyber/smartid/tse/dto/KTKPublicKey;Landroid/os/Handler;Ljava/lang/String;)V

    invoke-direct {p3, p4}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 702
    invoke-virtual {p3}, Ljava/lang/Thread;->start()V

    return-void

    :catch_0
    move-exception p2

    .line 659
    iget-object p3, p0, Lee/cyber/smartid/tse/KeyManager;->e:Lee/cyber/smartid/tse/inter/ListenerAccess;

    new-instance p4, Lee/cyber/smartid/tse/KeyManager$9;

    invoke-direct {p4, p0, p1, p2}, Lee/cyber/smartid/tse/KeyManager$9;-><init>(Lee/cyber/smartid/tse/KeyManager;Ljava/lang/String;Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;)V

    invoke-interface {p3, p4}, Lee/cyber/smartid/tse/inter/ListenerAccess;->notifyUI(Ljava/lang/Runnable;)V

    return-void
.end method

.method private a([Lee/cyber/smartid/cryptolib/dto/TestResult;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lee/cyber/smartid/cryptolib/dto/StorageException;
        }
    .end annotation

    .line 431
    iget-object v0, p0, Lee/cyber/smartid/tse/KeyManager;->j:Lee/cyber/smartid/cryptolib/inter/StorageOp;

    iget-object v1, p0, Lee/cyber/smartid/tse/KeyManager;->d:Lee/cyber/smartid/tse/inter/ResourceAccess;

    invoke-interface {v1}, Lee/cyber/smartid/tse/inter/ResourceAccess;->getLastPRNGTestResultStorageId()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Lee/cyber/smartid/cryptolib/inter/StorageOp;->storeData(Ljava/lang/String;Ljava/lang/Object;)V

    return-void
.end method

.method static synthetic a(Lee/cyber/smartid/tse/KeyManager;Lee/cyber/smartid/tse/dto/KTKPublicKey;Ljava/lang/String;Ljava/math/BigInteger;Lcom/a/a/n;Lee/cyber/smartid/tse/dto/jsonrpc/KeyData;)Z
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;
        }
    .end annotation

    .line 84
    invoke-direct/range {p0 .. p5}, Lee/cyber/smartid/tse/KeyManager;->a(Lee/cyber/smartid/tse/dto/KTKPublicKey;Ljava/lang/String;Ljava/math/BigInteger;Lcom/a/a/n;Lee/cyber/smartid/tse/dto/jsonrpc/KeyData;)Z

    move-result p0

    return p0
.end method

.method private a(Lee/cyber/smartid/tse/dto/KTKPublicKey;Ljava/lang/String;Ljava/math/BigInteger;Lcom/a/a/n;Lee/cyber/smartid/tse/dto/jsonrpc/KeyData;)Z
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;
        }
    .end annotation

    const/4 v0, 0x2

    .line 1295
    new-array v8, v0, [Lee/cyber/smartid/cryptolib/dto/ValuePair;

    .line 1296
    new-instance v0, Lee/cyber/smartid/cryptolib/dto/ValuePair;

    iget-object v1, p0, Lee/cyber/smartid/tse/KeyManager;->k:Lee/cyber/smartid/cryptolib/inter/EncodingOp;

    invoke-interface {v1, p3}, Lee/cyber/smartid/cryptolib/inter/EncodingOp;->encodeDecimalToBase64(Ljava/math/BigInteger;)Ljava/lang/String;

    move-result-object p3

    const-string v1, "clientDhPublicKey"

    invoke-direct {v0, v1, p3}, Lee/cyber/smartid/cryptolib/dto/ValuePair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    const/4 p3, 0x0

    aput-object v0, v8, p3

    .line 1297
    new-instance p3, Lee/cyber/smartid/cryptolib/dto/ValuePair;

    invoke-virtual {p5}, Lee/cyber/smartid/tse/dto/jsonrpc/KeyData;->getServerDhPublicKey()Ljava/lang/String;

    move-result-object p5

    const-string v0, "serverDhPublicKey"

    invoke-direct {p3, v0, p5}, Lee/cyber/smartid/cryptolib/dto/ValuePair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    const/4 p5, 0x1

    aput-object p3, v8, p5

    .line 1298
    iget-object v1, p0, Lee/cyber/smartid/tse/KeyManager;->l:Lee/cyber/smartid/cryptolib/inter/CryptoOp;

    invoke-virtual {p4}, Lcom/a/a/n;->a()Lcom/a/a/v;

    move-result-object p3

    invoke-virtual {p3}, Lcom/a/a/v;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lee/cyber/smartid/tse/dto/KTKPublicKey;->getPublicKey()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Lee/cyber/smartid/tse/dto/KTKPublicKey;->getKeyId()Ljava/lang/String;

    move-result-object v4

    const-string v6, "CLIENT"

    const-string v7, "DH"

    move-object v5, p2

    invoke-interface/range {v1 .. v8}, Lee/cyber/smartid/cryptolib/inter/CryptoOp;->verifyKTKSignedJWSAndCheckForRequiredContentValues(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Lee/cyber/smartid/cryptolib/dto/ValuePair;)Z

    move-result p1

    return p1
.end method

.method static synthetic b(Lee/cyber/smartid/tse/KeyManager;)Lee/cyber/smartid/tse/util/Log;
    .locals 0

    .line 84
    iget-object p0, p0, Lee/cyber/smartid/tse/KeyManager;->o:Lee/cyber/smartid/tse/util/Log;

    return-object p0
.end method

.method static synthetic b(Lee/cyber/smartid/tse/KeyManager;Lee/cyber/smartid/tse/dto/EncryptKeyWrapper;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lee/cyber/smartid/cryptolib/dto/StorageException;
        }
    .end annotation

    .line 84
    invoke-direct {p0, p1}, Lee/cyber/smartid/tse/KeyManager;->b(Lee/cyber/smartid/tse/dto/EncryptKeyWrapper;)V

    return-void
.end method

.method private b(Lee/cyber/smartid/tse/dto/EncryptKeyWrapper;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lee/cyber/smartid/cryptolib/dto/StorageException;
        }
    .end annotation

    .line 757
    iget-object v0, p0, Lee/cyber/smartid/tse/KeyManager;->n:Ljava/lang/Object;

    monitor-enter v0

    if-eqz p1, :cond_0

    .line 758
    :try_start_0
    invoke-virtual {p1}, Lee/cyber/smartid/tse/dto/EncryptKeyWrapper;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 761
    iget-object v1, p0, Lee/cyber/smartid/tse/KeyManager;->o:Lee/cyber/smartid/tse/util/Log;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "updateEncryptedKey - id: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lee/cyber/smartid/tse/dto/EncryptKeyWrapper;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lee/cyber/smartid/tse/util/Log;->d(Ljava/lang/String;)V

    .line 763
    invoke-direct {p0}, Lee/cyber/smartid/tse/KeyManager;->a()Ljava/util/HashMap;

    move-result-object v1

    .line 764
    invoke-virtual {p1}, Lee/cyber/smartid/tse/dto/EncryptKeyWrapper;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 766
    iget-object p1, p0, Lee/cyber/smartid/tse/KeyManager;->j:Lee/cyber/smartid/cryptolib/inter/StorageOp;

    iget-object v2, p0, Lee/cyber/smartid/tse/KeyManager;->d:Lee/cyber/smartid/tse/inter/ResourceAccess;

    invoke-interface {v2}, Lee/cyber/smartid/tse/inter/ResourceAccess;->getEncryptedKeysStorageId()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1, v2, v1}, Lee/cyber/smartid/cryptolib/inter/StorageOp;->storeData(Ljava/lang/String;Ljava/lang/Object;)V

    .line 767
    monitor-exit v0

    return-void

    .line 759
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v1, "Key references or the key data can\'t be empty!"

    invoke-direct {p1, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    :catchall_0
    move-exception p1

    .line 767
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method private b(Ljava/lang/String;)V
    .locals 2

    .line 406
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 407
    iget-object p1, p0, Lee/cyber/smartid/tse/KeyManager;->o:Lee/cyber/smartid/tse/util/Log;

    const-string v0, "removeGeneratedKeyFromMemory: Empty reference is not allowed!"

    invoke-virtual {p1, v0}, Lee/cyber/smartid/tse/util/Log;->w(Ljava/lang/String;)V

    return-void

    .line 410
    :cond_0
    iget-object v0, p0, Lee/cyber/smartid/tse/KeyManager;->c:Ljava/util/HashMap;

    monitor-enter v0

    .line 411
    :try_start_0
    iget-object v1, p0, Lee/cyber/smartid/tse/KeyManager;->c:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 412
    monitor-exit v0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method private c(Ljava/lang/String;)Lee/cyber/smartid/tse/dto/KeyMaterial;
    .locals 2

    .line 424
    iget-object v0, p0, Lee/cyber/smartid/tse/KeyManager;->c:Ljava/util/HashMap;

    monitor-enter v0

    .line 425
    :try_start_0
    iget-object v1, p0, Lee/cyber/smartid/tse/KeyManager;->c:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lee/cyber/smartid/tse/dto/KeyMaterial;

    monitor-exit v0

    return-object p1

    :catchall_0
    move-exception p1

    .line 426
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method static synthetic c(Lee/cyber/smartid/tse/KeyManager;)Lee/cyber/smartid/tse/inter/ResourceAccess;
    .locals 0

    .line 84
    iget-object p0, p0, Lee/cyber/smartid/tse/KeyManager;->d:Lee/cyber/smartid/tse/inter/ResourceAccess;

    return-object p0
.end method

.method public static createNewInstanceForTesting(Lee/cyber/smartid/tse/inter/KeyStorageAccess;Lee/cyber/smartid/tse/inter/ResourceAccess;Lee/cyber/smartid/tse/inter/ListenerAccess;Lee/cyber/smartid/tse/inter/AlarmAccess;Lee/cyber/smartid/cryptolib/inter/KeyGenerationOp;Lee/cyber/smartid/cryptolib/inter/RandomGenerationOp;Lee/cyber/smartid/cryptolib/inter/StorageOp;Lee/cyber/smartid/cryptolib/inter/EncodingOp;Lee/cyber/smartid/cryptolib/inter/CryptoOp;Lee/cyber/smartid/tse/inter/WallClock;)Lee/cyber/smartid/tse/KeyManager;
    .locals 12

    .line 210
    new-instance v11, Lee/cyber/smartid/tse/KeyManager;

    move-object v0, v11

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    invoke-direct/range {v0 .. v10}, Lee/cyber/smartid/tse/KeyManager;-><init>(Lee/cyber/smartid/tse/inter/KeyStorageAccess;Lee/cyber/smartid/tse/inter/ResourceAccess;Lee/cyber/smartid/tse/inter/ListenerAccess;Lee/cyber/smartid/tse/inter/AlarmAccess;Lee/cyber/smartid/cryptolib/inter/KeyGenerationOp;Lee/cyber/smartid/cryptolib/inter/RandomGenerationOp;Lee/cyber/smartid/cryptolib/inter/StorageOp;Lee/cyber/smartid/cryptolib/inter/EncodingOp;Lee/cyber/smartid/cryptolib/inter/CryptoOp;Lee/cyber/smartid/tse/inter/WallClock;)V

    return-object v11
.end method

.method static synthetic d(Lee/cyber/smartid/tse/KeyManager;)Lee/cyber/smartid/tse/inter/WallClock;
    .locals 0

    .line 84
    iget-object p0, p0, Lee/cyber/smartid/tse/KeyManager;->m:Lee/cyber/smartid/tse/inter/WallClock;

    return-object p0
.end method

.method private d(Ljava/lang/String;)V
    .locals 3

    .line 845
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 848
    :cond_0
    iget-object v0, p0, Lee/cyber/smartid/tse/KeyManager;->o:Lee/cyber/smartid/tse/util/Log;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "clearEncryptedKeyRemoveAlarm: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lee/cyber/smartid/tse/util/Log;->d(Ljava/lang/String;)V

    .line 849
    iget-object v0, p0, Lee/cyber/smartid/tse/KeyManager;->g:Lee/cyber/smartid/tse/inter/AlarmAccess;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ee.cyber.smartid.ACTION_REMOVE_ENCRYPTED_KEYS_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Lee/cyber/smartid/tse/inter/AlarmAccess;->clearAlarmFor(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic e(Lee/cyber/smartid/tse/KeyManager;)Lee/cyber/smartid/cryptolib/inter/KeyGenerationOp;
    .locals 0

    .line 84
    iget-object p0, p0, Lee/cyber/smartid/tse/KeyManager;->h:Lee/cyber/smartid/cryptolib/inter/KeyGenerationOp;

    return-object p0
.end method

.method static synthetic f(Lee/cyber/smartid/tse/KeyManager;)Lee/cyber/smartid/cryptolib/inter/EncodingOp;
    .locals 0

    .line 84
    iget-object p0, p0, Lee/cyber/smartid/tse/KeyManager;->k:Lee/cyber/smartid/cryptolib/inter/EncodingOp;

    return-object p0
.end method

.method static synthetic g(Lee/cyber/smartid/tse/KeyManager;)Lee/cyber/smartid/cryptolib/inter/CryptoOp;
    .locals 0

    .line 84
    iget-object p0, p0, Lee/cyber/smartid/tse/KeyManager;->l:Lee/cyber/smartid/cryptolib/inter/CryptoOp;

    return-object p0
.end method

.method public static getInstance(Lee/cyber/smartid/tse/inter/ResourceAccess;Lee/cyber/smartid/tse/inter/ListenerAccess;Lee/cyber/smartid/tse/inter/AlarmAccess;Lee/cyber/smartid/cryptolib/inter/KeyGenerationOp;Lee/cyber/smartid/cryptolib/inter/RandomGenerationOp;Lee/cyber/smartid/cryptolib/inter/StorageOp;Lee/cyber/smartid/cryptolib/inter/EncodingOp;Lee/cyber/smartid/cryptolib/inter/CryptoOp;Lee/cyber/smartid/tse/inter/WallClock;)Lee/cyber/smartid/tse/KeyManager;
    .locals 13

    .line 182
    sget-object v0, Lee/cyber/smartid/tse/KeyManager;->b:Lee/cyber/smartid/tse/KeyManager;

    if-nez v0, :cond_1

    .line 183
    const-class v1, Lee/cyber/smartid/tse/KeyStateManager;

    monitor-enter v1

    .line 184
    :try_start_0
    sget-object v0, Lee/cyber/smartid/tse/KeyManager;->b:Lee/cyber/smartid/tse/KeyManager;

    if-nez v0, :cond_0

    .line 185
    new-instance v0, Lee/cyber/smartid/tse/KeyManager;

    move-object v4, p0

    move-object/from16 v9, p5

    move-object/from16 v10, p6

    move-object/from16 v11, p7

    move-object/from16 v12, p8

    invoke-static {v11, v9, v10, v12, p0}, Lee/cyber/smartid/tse/KeyStorage;->a(Lee/cyber/smartid/cryptolib/inter/CryptoOp;Lee/cyber/smartid/cryptolib/inter/StorageOp;Lee/cyber/smartid/cryptolib/inter/EncodingOp;Lee/cyber/smartid/tse/inter/WallClock;Lee/cyber/smartid/tse/inter/ResourceAccess;)Lee/cyber/smartid/tse/KeyStorage;

    move-result-object v3

    move-object v2, v0

    move-object v4, p0

    move-object v5, p1

    move-object v6, p2

    move-object/from16 v7, p3

    move-object/from16 v8, p4

    move-object/from16 v9, p5

    move-object/from16 v10, p6

    move-object/from16 v11, p7

    move-object/from16 v12, p8

    invoke-direct/range {v2 .. v12}, Lee/cyber/smartid/tse/KeyManager;-><init>(Lee/cyber/smartid/tse/inter/KeyStorageAccess;Lee/cyber/smartid/tse/inter/ResourceAccess;Lee/cyber/smartid/tse/inter/ListenerAccess;Lee/cyber/smartid/tse/inter/AlarmAccess;Lee/cyber/smartid/cryptolib/inter/KeyGenerationOp;Lee/cyber/smartid/cryptolib/inter/RandomGenerationOp;Lee/cyber/smartid/cryptolib/inter/StorageOp;Lee/cyber/smartid/cryptolib/inter/EncodingOp;Lee/cyber/smartid/cryptolib/inter/CryptoOp;Lee/cyber/smartid/tse/inter/WallClock;)V

    sput-object v0, Lee/cyber/smartid/tse/KeyManager;->b:Lee/cyber/smartid/tse/KeyManager;

    .line 187
    :cond_0
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 189
    :cond_1
    :goto_0
    sget-object v0, Lee/cyber/smartid/tse/KeyManager;->b:Lee/cyber/smartid/tse/KeyManager;

    return-object v0
.end method

.method static synthetic h(Lee/cyber/smartid/tse/KeyManager;)Lee/cyber/smartid/tse/inter/KeyStorageAccess;
    .locals 0

    .line 84
    iget-object p0, p0, Lee/cyber/smartid/tse/KeyManager;->f:Lee/cyber/smartid/tse/inter/KeyStorageAccess;

    return-object p0
.end method


# virtual methods
.method a(Lee/cyber/smartid/tse/dto/PinInfo;)Lee/cyber/smartid/tse/dto/PinValidationError;
    .locals 1

    .line 529
    sget-object v0, Lee/cyber/smartid/tse/KeyManager;->a:Ljava/util/ArrayList;

    invoke-virtual {p0, p1, v0}, Lee/cyber/smartid/tse/KeyManager;->a(Lee/cyber/smartid/tse/dto/PinInfo;Ljava/util/ArrayList;)Lee/cyber/smartid/tse/dto/PinValidationError;

    move-result-object p1

    return-object p1
.end method

.method a(Lee/cyber/smartid/tse/dto/PinInfo;Ljava/util/ArrayList;)Lee/cyber/smartid/tse/dto/PinValidationError;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lee/cyber/smartid/tse/dto/PinInfo;",
            "Ljava/util/ArrayList<",
            "Lee/cyber/smartid/tse/dto/PinValidationRule;",
            ">;)",
            "Lee/cyber/smartid/tse/dto/PinValidationError;"
        }
    .end annotation

    const/4 v0, 0x0

    if-nez p1, :cond_0

    return-object v0

    :cond_0
    if-nez p2, :cond_1

    return-object v0

    .line 551
    :cond_1
    invoke-virtual {p2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :cond_2
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lee/cyber/smartid/tse/dto/PinValidationRule;

    .line 552
    invoke-virtual {p1}, Lee/cyber/smartid/tse/dto/PinInfo;->getPin()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lee/cyber/smartid/tse/dto/PinValidationRule;->isValid(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 554
    iget-object p2, p0, Lee/cyber/smartid/tse/KeyManager;->d:Lee/cyber/smartid/tse/inter/ResourceAccess;

    invoke-interface {p2}, Lee/cyber/smartid/tse/inter/ResourceAccess;->getApplicationContext()Landroid/content/Context;

    move-result-object p2

    invoke-virtual {v1, p2, p1}, Lee/cyber/smartid/tse/dto/PinValidationRule;->createErrorFor(Landroid/content/Context;Lee/cyber/smartid/tse/dto/PinInfo;)Lee/cyber/smartid/tse/dto/PinValidationError;

    move-result-object p1

    return-object p1

    :cond_3
    return-object v0
.end method

.method a(Ljava/lang/String;)V
    .locals 2

    const-wide/32 v0, 0x240c8400

    .line 833
    invoke-direct {p0, p1, v0, v1}, Lee/cyber/smartid/tse/KeyManager;->a(Ljava/lang/String;J)V

    return-void
.end method

.method a(Ljava/lang/String;Lee/cyber/smartid/tse/inter/KeyMaterialApplier;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lee/cyber/smartid/tse/dto/NoSuchKeysException;
        }
    .end annotation

    .line 1069
    invoke-static {p1}, Lee/cyber/smartid/tse/dto/EncryptKeyWrapper;->getId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lee/cyber/smartid/tse/KeyManager;->loadEncryptedKey(Ljava/lang/String;)Lee/cyber/smartid/tse/dto/EncryptKeyWrapper;

    move-result-object p1

    if-eqz p1, :cond_1

    if-eqz p2, :cond_0

    .line 1076
    invoke-virtual {p1}, Lee/cyber/smartid/tse/dto/EncryptKeyWrapper;->getN1()Ljava/lang/String;

    move-result-object p1

    invoke-interface {p2, p1}, Lee/cyber/smartid/tse/inter/KeyMaterialApplier;->applyN1(Ljava/lang/String;)V

    :cond_0
    return-void

    .line 1071
    :cond_1
    new-instance p1, Lee/cyber/smartid/tse/dto/NoSuchKeysException;

    iget-object p2, p0, Lee/cyber/smartid/tse/KeyManager;->d:Lee/cyber/smartid/tse/inter/ResourceAccess;

    invoke-interface {p2}, Lee/cyber/smartid/tse/inter/ResourceAccess;->getApplicationContext()Landroid/content/Context;

    move-result-object p2

    sget v0, Lee/cyber/smartid/tse/R$string;->err_no_such_keys_found_regenerate_keys_2:I

    invoke-virtual {p2, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Lee/cyber/smartid/tse/dto/NoSuchKeysException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method a(Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/tse/dto/jsonrpc/KeyData;Lee/cyber/smartid/tse/inter/ValidateKeyCreationResponseListener;)V
    .locals 1

    .line 939
    iget-object v0, p0, Lee/cyber/smartid/tse/KeyManager;->e:Lee/cyber/smartid/tse/inter/ListenerAccess;

    invoke-interface {v0, p1, p4}, Lee/cyber/smartid/tse/inter/ListenerAccess;->setListener(Ljava/lang/String;Lee/cyber/smartid/tse/inter/TSEListener;)V

    .line 940
    new-instance p4, Ljava/lang/Thread;

    new-instance v0, Lee/cyber/smartid/tse/KeyManager$13;

    invoke-direct {v0, p0, p3, p1, p2}, Lee/cyber/smartid/tse/KeyManager$13;-><init>(Lee/cyber/smartid/tse/KeyManager;Lee/cyber/smartid/tse/dto/jsonrpc/KeyData;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p4, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 1052
    invoke-virtual {p4}, Ljava/lang/Thread;->start()V

    return-void
.end method

.method a(Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/tse/inter/GenerateDiffieHellmanKeyPairListener;)V
    .locals 2

    .line 863
    iget-object v0, p0, Lee/cyber/smartid/tse/KeyManager;->e:Lee/cyber/smartid/tse/inter/ListenerAccess;

    invoke-interface {v0, p1, p3}, Lee/cyber/smartid/tse/inter/ListenerAccess;->setListener(Ljava/lang/String;Lee/cyber/smartid/tse/inter/TSEListener;)V

    .line 864
    iget-object p3, p0, Lee/cyber/smartid/tse/KeyManager;->o:Lee/cyber/smartid/tse/util/Log;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "generateDiffieHellmanKeyPairForTSEKey - tag: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", tseKeyReference: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, Lee/cyber/smartid/tse/util/Log;->d(Ljava/lang/String;)V

    .line 865
    new-instance p3, Ljava/lang/Thread;

    new-instance v0, Lee/cyber/smartid/tse/KeyManager$12;

    invoke-direct {v0, p0, p2, p1}, Lee/cyber/smartid/tse/KeyManager$12;-><init>(Lee/cyber/smartid/tse/KeyManager;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p3, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 919
    invoke-virtual {p3}, Ljava/lang/Thread;->start()V

    return-void
.end method

.method a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/tse/dto/jsonrpc/KeyData;Lee/cyber/smartid/tse/inter/InitializeKeyAndKeyStatesListener;)V
    .locals 11

    move-object v8, p0

    .line 1099
    iget-object v0, v8, Lee/cyber/smartid/tse/KeyManager;->e:Lee/cyber/smartid/tse/inter/ListenerAccess;

    move-object v4, p1

    move-object/from16 v1, p7

    invoke-interface {v0, p1, v1}, Lee/cyber/smartid/tse/inter/ListenerAccess;->setListener(Ljava/lang/String;Lee/cyber/smartid/tse/inter/TSEListener;)V

    .line 1100
    new-instance v9, Ljava/lang/Thread;

    new-instance v10, Lee/cyber/smartid/tse/KeyManager$14;

    move-object v0, v10

    move-object v1, p0

    move-object v2, p4

    move-object/from16 v3, p6

    move-object v5, p3

    move-object v6, p2

    move-object/from16 v7, p5

    invoke-direct/range {v0 .. v7}, Lee/cyber/smartid/tse/KeyManager$14;-><init>(Lee/cyber/smartid/tse/KeyManager;Ljava/lang/String;Lee/cyber/smartid/tse/dto/jsonrpc/KeyData;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v9, v10}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 1290
    invoke-virtual {v9}, Ljava/lang/Thread;->start()V

    return-void
.end method

.method public consumeFreshnessToken(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lee/cyber/smartid/cryptolib/dto/StorageException;
        }
    .end annotation

    .line 1374
    iget-object v0, p0, Lee/cyber/smartid/tse/KeyManager;->f:Lee/cyber/smartid/tse/inter/KeyStorageAccess;

    invoke-interface {v0, p1}, Lee/cyber/smartid/tse/inter/KeyStorageAccess;->consumeFreshnessToken(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public deleteKeyAndRelatedObjects(Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lee/cyber/smartid/cryptolib/dto/StorageException;
        }
    .end annotation

    .line 1390
    iget-object v0, p0, Lee/cyber/smartid/tse/KeyManager;->f:Lee/cyber/smartid/tse/inter/KeyStorageAccess;

    invoke-interface {v0, p1}, Lee/cyber/smartid/tse/inter/KeyStorageAccess;->deleteKeyAndRelatedObjects(Ljava/lang/String;)V

    return-void
.end method

.method public encryptKey(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/tse/inter/EncryptKeyListener;)V
    .locals 7

    .line 580
    iget-object v0, p0, Lee/cyber/smartid/tse/KeyManager;->e:Lee/cyber/smartid/tse/inter/ListenerAccess;

    invoke-interface {v0, p1, p5}, Lee/cyber/smartid/tse/inter/ListenerAccess;->setListener(Ljava/lang/String;Lee/cyber/smartid/tse/inter/TSEListener;)V

    .line 581
    iget-object p5, p0, Lee/cyber/smartid/tse/KeyManager;->o:Lee/cyber/smartid/tse/util/Log;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "encryptKey - "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p5, v0}, Lee/cyber/smartid/tse/util/Log;->d(Ljava/lang/String;)V

    .line 584
    new-instance p5, Ljava/util/ArrayList;

    invoke-direct {p5}, Ljava/util/ArrayList;-><init>()V

    .line 585
    new-instance v0, Lee/cyber/smartid/tse/dto/PinInfo;

    invoke-direct {v0, p2, p3}, Lee/cyber/smartid/tse/dto/PinInfo;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 586
    new-instance v0, Lee/cyber/smartid/tse/KeyManager$6;

    move-object v1, v0

    move-object v2, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    move-object v6, p4

    invoke-direct/range {v1 .. v6}, Lee/cyber/smartid/tse/KeyManager$6;-><init>(Lee/cyber/smartid/tse/KeyManager;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    const-string p1, "ee.cyber.smartid.tse.TAG_SMART_ID_TSE_ENCRYPT_KEY_VALIDATE_PINS"

    invoke-virtual {p0, p1, p5, v0}, Lee/cyber/smartid/tse/KeyManager;->validatePins(Ljava/lang/String;Ljava/util/ArrayList;Lee/cyber/smartid/tse/inter/ValidatePinListener;)V

    return-void
.end method

.method public finishKeyStateOperation(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lee/cyber/smartid/cryptolib/dto/StorageException;
        }
    .end annotation

    .line 1358
    iget-object v0, p0, Lee/cyber/smartid/tse/KeyManager;->f:Lee/cyber/smartid/tse/inter/KeyStorageAccess;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-interface/range {v0 .. v6}, Lee/cyber/smartid/tse/inter/KeyStorageAccess;->finishKeyStateOperation(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public generateKeys(Ljava/lang/String;IILjava/math/BigInteger;ILee/cyber/smartid/tse/inter/WallClock;Lee/cyber/smartid/tse/inter/GenerateKeysListener;)V
    .locals 13

    move-object v0, p0

    .line 258
    iget-object v1, v0, Lee/cyber/smartid/tse/KeyManager;->e:Lee/cyber/smartid/tse/inter/ListenerAccess;

    move-object v3, p1

    move-object/from16 v2, p7

    invoke-interface {v1, p1, v2}, Lee/cyber/smartid/tse/inter/ListenerAccess;->setListener(Ljava/lang/String;Lee/cyber/smartid/tse/inter/TSEListener;)V

    .line 259
    new-instance v2, Lee/cyber/smartid/tse/KeyGenerationManager;

    invoke-direct {v2}, Lee/cyber/smartid/tse/KeyGenerationManager;-><init>()V

    iget-object v7, v0, Lee/cyber/smartid/tse/KeyManager;->h:Lee/cyber/smartid/cryptolib/inter/KeyGenerationOp;

    iget-object v8, v0, Lee/cyber/smartid/tse/KeyManager;->i:Lee/cyber/smartid/cryptolib/inter/RandomGenerationOp;

    iget-object v10, v0, Lee/cyber/smartid/tse/KeyManager;->e:Lee/cyber/smartid/tse/inter/ListenerAccess;

    iget-object v11, v0, Lee/cyber/smartid/tse/KeyManager;->d:Lee/cyber/smartid/tse/inter/ResourceAccess;

    new-instance v12, Lee/cyber/smartid/tse/KeyManager$1;

    move/from16 v1, p5

    move-object/from16 v9, p6

    invoke-direct {v12, p0, v1, v9}, Lee/cyber/smartid/tse/KeyManager$1;-><init>(Lee/cyber/smartid/tse/KeyManager;ILee/cyber/smartid/tse/inter/WallClock;)V

    move v4, p2

    move/from16 v5, p3

    move-object/from16 v6, p4

    invoke-virtual/range {v2 .. v12}, Lee/cyber/smartid/tse/KeyGenerationManager;->generateKeys(Ljava/lang/String;IILjava/math/BigInteger;Lee/cyber/smartid/cryptolib/inter/KeyGenerationOp;Lee/cyber/smartid/cryptolib/inter/RandomGenerationOp;Lee/cyber/smartid/tse/inter/WallClock;Lee/cyber/smartid/tse/inter/ListenerAccess;Lee/cyber/smartid/tse/inter/ResourceAccess;Lee/cyber/smartid/tse/KeyGenerationManager$KeyGenerationWorkerListener;)V

    return-void
.end method

.method public getKey(Ljava/lang/String;)Lee/cyber/smartid/tse/dto/Key;
    .locals 1

    .line 1385
    iget-object v0, p0, Lee/cyber/smartid/tse/KeyManager;->f:Lee/cyber/smartid/tse/inter/KeyStorageAccess;

    invoke-interface {v0, p1}, Lee/cyber/smartid/tse/inter/KeyStorageAccess;->getKey(Ljava/lang/String;)Lee/cyber/smartid/tse/dto/Key;

    move-result-object p1

    return-object p1
.end method

.method public getKeyPinLength(Ljava/lang/String;)I
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lee/cyber/smartid/tse/dto/NoSuchKeysException;
        }
    .end annotation

    .line 1309
    invoke-virtual {p0, p1}, Lee/cyber/smartid/tse/KeyManager;->getKey(Ljava/lang/String;)Lee/cyber/smartid/tse/dto/Key;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 1313
    invoke-virtual {p1}, Lee/cyber/smartid/tse/dto/Key;->getKeyPinLength()I

    move-result p1

    return p1

    .line 1311
    :cond_0
    new-instance p1, Lee/cyber/smartid/tse/dto/NoSuchKeysException;

    iget-object v0, p0, Lee/cyber/smartid/tse/KeyManager;->d:Lee/cyber/smartid/tse/inter/ResourceAccess;

    invoke-interface {v0}, Lee/cyber/smartid/tse/inter/ResourceAccess;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    sget v1, Lee/cyber/smartid/tse/R$string;->err_no_such_key_found:I

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Lee/cyber/smartid/tse/dto/NoSuchKeysException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public getKeyStateById(Ljava/lang/String;)Lee/cyber/smartid/tse/dto/KeyState;
    .locals 1

    .line 1325
    iget-object v0, p0, Lee/cyber/smartid/tse/KeyManager;->f:Lee/cyber/smartid/tse/inter/KeyStorageAccess;

    invoke-interface {v0, p1}, Lee/cyber/smartid/tse/inter/KeyStorageAccess;->getKeyStateById(Ljava/lang/String;)Lee/cyber/smartid/tse/dto/KeyState;

    move-result-object p1

    return-object p1
.end method

.method public getKeyStateByKeyId(Ljava/lang/String;)Lee/cyber/smartid/tse/dto/KeyState;
    .locals 1

    .line 1319
    iget-object v0, p0, Lee/cyber/smartid/tse/KeyManager;->f:Lee/cyber/smartid/tse/inter/KeyStorageAccess;

    invoke-interface {v0, p1}, Lee/cyber/smartid/tse/inter/KeyStorageAccess;->getKeyStateByKeyId(Ljava/lang/String;)Lee/cyber/smartid/tse/dto/KeyState;

    move-result-object p1

    return-object p1
.end method

.method public getKeyStateMetaById(Ljava/lang/String;)Lee/cyber/smartid/tse/dto/KeyStateMeta;
    .locals 1

    .line 1337
    iget-object v0, p0, Lee/cyber/smartid/tse/KeyManager;->f:Lee/cyber/smartid/tse/inter/KeyStorageAccess;

    invoke-interface {v0, p1}, Lee/cyber/smartid/tse/inter/KeyStorageAccess;->getKeyStateMetaById(Ljava/lang/String;)Lee/cyber/smartid/tse/dto/KeyStateMeta;

    move-result-object p1

    return-object p1
.end method

.method public getKeyStateMetaByKeyStateId(Ljava/lang/String;)Lee/cyber/smartid/tse/dto/KeyStateMeta;
    .locals 1

    .line 1331
    iget-object v0, p0, Lee/cyber/smartid/tse/KeyManager;->f:Lee/cyber/smartid/tse/inter/KeyStorageAccess;

    invoke-interface {v0, p1}, Lee/cyber/smartid/tse/inter/KeyStorageAccess;->getKeyStateMetaByKeyStateId(Ljava/lang/String;)Lee/cyber/smartid/tse/dto/KeyStateMeta;

    move-result-object p1

    return-object p1
.end method

.method public getPRNGTestResult()[Lee/cyber/smartid/cryptolib/dto/TestResult;
    .locals 3

    .line 445
    iget-object v0, p0, Lee/cyber/smartid/tse/KeyManager;->j:Lee/cyber/smartid/cryptolib/inter/StorageOp;

    iget-object v1, p0, Lee/cyber/smartid/tse/KeyManager;->d:Lee/cyber/smartid/tse/inter/ResourceAccess;

    invoke-interface {v1}, Lee/cyber/smartid/tse/inter/ResourceAccess;->getLastPRNGTestResultStorageId()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lee/cyber/smartid/tse/KeyManager$3;

    invoke-direct {v2, p0}, Lee/cyber/smartid/tse/KeyManager$3;-><init>(Lee/cyber/smartid/tse/KeyManager;)V

    .line 446
    invoke-virtual {v2}, Lee/cyber/smartid/tse/KeyManager$3;->getType()Ljava/lang/reflect/Type;

    move-result-object v2

    .line 445
    invoke-interface {v0, v1, v2}, Lee/cyber/smartid/cryptolib/inter/StorageOp;->retrieveData(Ljava/lang/String;Ljava/lang/reflect/Type;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lee/cyber/smartid/cryptolib/dto/TestResult;

    return-object v0
.end method

.method public initializeKey(Ljava/lang/String;Lee/cyber/smartid/tse/dto/Key;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lee/cyber/smartid/cryptolib/dto/StorageException;
        }
    .end annotation

    .line 1379
    iget-object v0, p0, Lee/cyber/smartid/tse/KeyManager;->f:Lee/cyber/smartid/tse/inter/KeyStorageAccess;

    invoke-interface {v0, p1, p2}, Lee/cyber/smartid/tse/inter/KeyStorageAccess;->initializeKey(Ljava/lang/String;Lee/cyber/smartid/tse/dto/Key;)V

    return-void
.end method

.method public initializeKeyState(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lee/cyber/smartid/cryptolib/dto/StorageException;
        }
    .end annotation

    .line 1347
    iget-object v0, p0, Lee/cyber/smartid/tse/KeyManager;->f:Lee/cyber/smartid/tse/inter/KeyStorageAccess;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-interface/range {v0 .. v5}, Lee/cyber/smartid/tse/inter/KeyStorageAccess;->initializeKeyState(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public loadEncryptedKey(Ljava/lang/String;)Lee/cyber/smartid/tse/dto/EncryptKeyWrapper;
    .locals 2

    .line 731
    iget-object v0, p0, Lee/cyber/smartid/tse/KeyManager;->n:Ljava/lang/Object;

    monitor-enter v0

    .line 732
    :try_start_0
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 p1, 0x0

    .line 733
    monitor-exit v0

    return-object p1

    .line 735
    :cond_0
    invoke-direct {p0}, Lee/cyber/smartid/tse/KeyManager;->a()Ljava/util/HashMap;

    move-result-object v1

    .line 736
    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lee/cyber/smartid/tse/dto/EncryptKeyWrapper;

    monitor-exit v0

    return-object p1

    :catchall_0
    move-exception p1

    .line 737
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method public removeAllExpiredEncryptedKeys()V
    .locals 9

    .line 797
    iget-object v0, p0, Lee/cyber/smartid/tse/KeyManager;->n:Ljava/lang/Object;

    monitor-enter v0

    .line 798
    :try_start_0
    iget-object v1, p0, Lee/cyber/smartid/tse/KeyManager;->o:Lee/cyber/smartid/tse/util/Log;

    const-string v2, "removeAllExpiredEncryptedKeys"

    invoke-virtual {v1, v2}, Lee/cyber/smartid/tse/util/Log;->d(Ljava/lang/String;)V

    .line 800
    invoke-direct {p0}, Lee/cyber/smartid/tse/KeyManager;->a()Ljava/util/HashMap;

    move-result-object v1

    .line 801
    invoke-virtual {v1}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 802
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 803
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 804
    invoke-virtual {v1, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lee/cyber/smartid/tse/dto/EncryptKeyWrapper;

    const-wide/32 v4, 0x240c8400

    if-eqz v3, :cond_1

    .line 805
    iget-object v6, p0, Lee/cyber/smartid/tse/KeyManager;->m:Lee/cyber/smartid/tse/inter/WallClock;

    invoke-virtual {v3, v6, v4, v5}, Lee/cyber/smartid/tse/dto/EncryptKeyWrapper;->isExpired(Lee/cyber/smartid/tse/inter/WallClock;J)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 806
    iget-object v4, p0, Lee/cyber/smartid/tse/KeyManager;->o:Lee/cyber/smartid/tse/util/Log;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "removeAllExpiredEncryptedKeys - removing expired wrapper with id: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Lee/cyber/smartid/tse/dto/EncryptKeyWrapper;->getId()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lee/cyber/smartid/tse/util/Log;->d(Ljava/lang/String;)V

    .line 807
    invoke-interface {v2}, Ljava/util/Iterator;->remove()V

    .line 809
    invoke-virtual {v3}, Lee/cyber/smartid/tse/dto/EncryptKeyWrapper;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3}, Lee/cyber/smartid/tse/KeyManager;->d(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    if-eqz v3, :cond_0

    .line 812
    iget-object v6, p0, Lee/cyber/smartid/tse/KeyManager;->o:Lee/cyber/smartid/tse/util/Log;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "removeAllExpiredEncryptedKeys updating the alarm for wrapper with id: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Lee/cyber/smartid/tse/dto/EncryptKeyWrapper;->getId()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lee/cyber/smartid/tse/util/Log;->d(Ljava/lang/String;)V

    .line 813
    invoke-virtual {v3}, Lee/cyber/smartid/tse/dto/EncryptKeyWrapper;->getId()Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lee/cyber/smartid/tse/KeyManager;->m:Lee/cyber/smartid/tse/inter/WallClock;

    invoke-virtual {v3, v7, v4, v5}, Lee/cyber/smartid/tse/dto/EncryptKeyWrapper;->getRemainingExpirationDelay(Lee/cyber/smartid/tse/inter/WallClock;J)J

    move-result-wide v3

    invoke-direct {p0, v6, v3, v4}, Lee/cyber/smartid/tse/KeyManager;->a(Ljava/lang/String;J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 819
    :cond_2
    :try_start_1
    iget-object v2, p0, Lee/cyber/smartid/tse/KeyManager;->j:Lee/cyber/smartid/cryptolib/inter/StorageOp;

    iget-object v3, p0, Lee/cyber/smartid/tse/KeyManager;->d:Lee/cyber/smartid/tse/inter/ResourceAccess;

    invoke-interface {v3}, Lee/cyber/smartid/tse/inter/ResourceAccess;->getEncryptedKeysStorageId()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3, v1}, Lee/cyber/smartid/cryptolib/inter/StorageOp;->storeData(Ljava/lang/String;Ljava/lang/Object;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :catch_0
    move-exception v1

    .line 822
    :try_start_2
    iget-object v2, p0, Lee/cyber/smartid/tse/KeyManager;->o:Lee/cyber/smartid/tse/util/Log;

    const-string v3, "removeAllExpiredEncryptedKeys"

    invoke-virtual {v2, v3, v1}, Lee/cyber/smartid/tse/util/Log;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 824
    :goto_1
    monitor-exit v0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1
.end method

.method public removeEncryptedKey(Ljava/lang/String;)V
    .locals 4

    .line 772
    iget-object v0, p0, Lee/cyber/smartid/tse/KeyManager;->n:Ljava/lang/Object;

    monitor-enter v0

    .line 773
    :try_start_0
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 774
    monitor-exit v0

    return-void

    .line 776
    :cond_0
    iget-object v1, p0, Lee/cyber/smartid/tse/KeyManager;->o:Lee/cyber/smartid/tse/util/Log;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "removeEncryptedKey - id: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lee/cyber/smartid/tse/util/Log;->d(Ljava/lang/String;)V

    .line 777
    invoke-direct {p0}, Lee/cyber/smartid/tse/KeyManager;->a()Ljava/util/HashMap;

    move-result-object v1

    .line 778
    invoke-virtual {v1, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 781
    :try_start_1
    iget-object v2, p0, Lee/cyber/smartid/tse/KeyManager;->j:Lee/cyber/smartid/cryptolib/inter/StorageOp;

    iget-object v3, p0, Lee/cyber/smartid/tse/KeyManager;->d:Lee/cyber/smartid/tse/inter/ResourceAccess;

    invoke-interface {v3}, Lee/cyber/smartid/tse/inter/ResourceAccess;->getEncryptedKeysStorageId()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3, v1}, Lee/cyber/smartid/cryptolib/inter/StorageOp;->storeData(Ljava/lang/String;Ljava/lang/Object;)V
    :try_end_1
    .catch Lee/cyber/smartid/cryptolib/dto/StorageException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v1

    .line 784
    :try_start_2
    iget-object v2, p0, Lee/cyber/smartid/tse/KeyManager;->o:Lee/cyber/smartid/tse/util/Log;

    const-string v3, "removeEncryptedKey"

    invoke-virtual {v2, v3, v1}, Lee/cyber/smartid/tse/util/Log;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 787
    :goto_0
    invoke-direct {p0, p1}, Lee/cyber/smartid/tse/KeyManager;->d(Ljava/lang/String;)V

    .line 788
    monitor-exit v0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw p1
.end method

.method public rollbackKeyStateOperation(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lee/cyber/smartid/cryptolib/dto/StorageException;
        }
    .end annotation

    .line 1363
    iget-object v0, p0, Lee/cyber/smartid/tse/KeyManager;->f:Lee/cyber/smartid/tse/inter/KeyStorageAccess;

    invoke-interface {v0, p1, p2, p3, p4}, Lee/cyber/smartid/tse/inter/KeyStorageAccess;->rollbackKeyStateOperation(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public startKeyStateOperation(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/support/v4/e/j;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "I",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Landroid/support/v4/e/j<",
            "Lee/cyber/smartid/tse/dto/KeyState;",
            "Lee/cyber/smartid/tse/dto/KeyStateMeta;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lee/cyber/smartid/cryptolib/dto/StorageException;
        }
    .end annotation

    move-object v0, p0

    .line 1353
    iget-object v1, v0, Lee/cyber/smartid/tse/KeyManager;->f:Lee/cyber/smartid/tse/inter/KeyStorageAccess;

    move-object v2, p1

    move-object/from16 v3, p2

    move/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    invoke-interface/range {v1 .. v13}, Lee/cyber/smartid/tse/inter/KeyStorageAccess;->startKeyStateOperation(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/support/v4/e/j;

    move-result-object v1

    return-object v1
.end method

.method public updateFreshnessToken(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lee/cyber/smartid/cryptolib/dto/StorageException;
        }
    .end annotation

    .line 1368
    iget-object v0, p0, Lee/cyber/smartid/tse/KeyManager;->f:Lee/cyber/smartid/tse/inter/KeyStorageAccess;

    invoke-interface {v0, p1, p2}, Lee/cyber/smartid/tse/inter/KeyStorageAccess;->updateFreshnessToken(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public updateKeyStateMeta(Lee/cyber/smartid/tse/dto/KeyStateMeta;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lee/cyber/smartid/cryptolib/dto/StorageException;
        }
    .end annotation

    .line 1342
    iget-object v0, p0, Lee/cyber/smartid/tse/KeyManager;->f:Lee/cyber/smartid/tse/inter/KeyStorageAccess;

    invoke-interface {v0, p1}, Lee/cyber/smartid/tse/inter/KeyStorageAccess;->updateKeyStateMeta(Lee/cyber/smartid/tse/dto/KeyStateMeta;)V

    return-void
.end method

.method public validatePins(Ljava/lang/String;Ljava/util/ArrayList;Lee/cyber/smartid/tse/inter/ValidatePinListener;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList<",
            "Lee/cyber/smartid/tse/dto/PinInfo;",
            ">;",
            "Lee/cyber/smartid/tse/inter/ValidatePinListener;",
            ")V"
        }
    .end annotation

    .line 464
    iget-object v0, p0, Lee/cyber/smartid/tse/KeyManager;->o:Lee/cyber/smartid/tse/util/Log;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "validatePins - tag "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lee/cyber/smartid/tse/util/Log;->d(Ljava/lang/String;)V

    .line 465
    iget-object v0, p0, Lee/cyber/smartid/tse/KeyManager;->e:Lee/cyber/smartid/tse/inter/ListenerAccess;

    invoke-interface {v0, p1, p3}, Lee/cyber/smartid/tse/inter/ListenerAccess;->setListener(Ljava/lang/String;Lee/cyber/smartid/tse/inter/TSEListener;)V

    if-eqz p2, :cond_1

    .line 467
    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result p3

    if-nez p3, :cond_0

    goto :goto_0

    .line 480
    :cond_0
    new-instance p3, Ljava/lang/Thread;

    new-instance v0, Lee/cyber/smartid/tse/KeyManager$5;

    invoke-direct {v0, p0, p2, p1}, Lee/cyber/smartid/tse/KeyManager$5;-><init>(Lee/cyber/smartid/tse/KeyManager;Ljava/util/ArrayList;Ljava/lang/String;)V

    invoke-direct {p3, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 516
    invoke-virtual {p3}, Ljava/lang/Thread;->start()V

    return-void

    .line 469
    :cond_1
    :goto_0
    iget-object p2, p0, Lee/cyber/smartid/tse/KeyManager;->e:Lee/cyber/smartid/tse/inter/ListenerAccess;

    new-instance p3, Lee/cyber/smartid/tse/KeyManager$4;

    invoke-direct {p3, p0, p1}, Lee/cyber/smartid/tse/KeyManager$4;-><init>(Lee/cyber/smartid/tse/KeyManager;Ljava/lang/String;)V

    invoke-interface {p2, p3}, Lee/cyber/smartid/tse/inter/ListenerAccess;->notifyUI(Ljava/lang/Runnable;)V

    return-void
.end method
