.class Lee/cyber/smartid/tse/KeyManager$6;
.super Ljava/lang/Object;
.source "KeyManager.java"

# interfaces
.implements Lee/cyber/smartid/tse/inter/ValidatePinListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lee/cyber/smartid/tse/KeyManager;->encryptKey(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/tse/inter/EncryptKeyListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:Ljava/lang/String;

.field final synthetic c:Ljava/lang/String;

.field final synthetic d:Ljava/lang/String;

.field final synthetic e:Lee/cyber/smartid/tse/KeyManager;


# direct methods
.method constructor <init>(Lee/cyber/smartid/tse/KeyManager;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 586
    iput-object p1, p0, Lee/cyber/smartid/tse/KeyManager$6;->e:Lee/cyber/smartid/tse/KeyManager;

    iput-object p2, p0, Lee/cyber/smartid/tse/KeyManager$6;->a:Ljava/lang/String;

    iput-object p3, p0, Lee/cyber/smartid/tse/KeyManager$6;->b:Ljava/lang/String;

    iput-object p4, p0, Lee/cyber/smartid/tse/KeyManager$6;->c:Ljava/lang/String;

    iput-object p5, p0, Lee/cyber/smartid/tse/KeyManager$6;->d:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onValidatePinFailed(Ljava/lang/String;Lee/cyber/smartid/tse/dto/TSEError;)V
    .locals 1

    .line 609
    iget-object p1, p0, Lee/cyber/smartid/tse/KeyManager$6;->e:Lee/cyber/smartid/tse/KeyManager;

    invoke-static {p1}, Lee/cyber/smartid/tse/KeyManager;->a(Lee/cyber/smartid/tse/KeyManager;)Lee/cyber/smartid/tse/inter/ListenerAccess;

    move-result-object p1

    new-instance v0, Lee/cyber/smartid/tse/KeyManager$6$2;

    invoke-direct {v0, p0, p2}, Lee/cyber/smartid/tse/KeyManager$6$2;-><init>(Lee/cyber/smartid/tse/KeyManager$6;Lee/cyber/smartid/tse/dto/TSEError;)V

    invoke-interface {p1, v0}, Lee/cyber/smartid/tse/inter/ListenerAccess;->notifyUI(Ljava/lang/Runnable;)V

    return-void
.end method

.method public onValidatePinSuccess(Ljava/lang/String;Lee/cyber/smartid/tse/dto/jsonrpc/resp/ValidatePinResp;)V
    .locals 3

    .line 590
    invoke-virtual {p2}, Lee/cyber/smartid/tse/dto/jsonrpc/resp/ValidatePinResp;->isValid()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 592
    iget-object p1, p0, Lee/cyber/smartid/tse/KeyManager$6;->e:Lee/cyber/smartid/tse/KeyManager;

    iget-object p2, p0, Lee/cyber/smartid/tse/KeyManager$6;->a:Ljava/lang/String;

    iget-object v0, p0, Lee/cyber/smartid/tse/KeyManager$6;->b:Ljava/lang/String;

    iget-object v1, p0, Lee/cyber/smartid/tse/KeyManager$6;->c:Ljava/lang/String;

    iget-object v2, p0, Lee/cyber/smartid/tse/KeyManager$6;->d:Ljava/lang/String;

    invoke-static {p1, p2, v0, v1, v2}, Lee/cyber/smartid/tse/KeyManager;->a(Lee/cyber/smartid/tse/KeyManager;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 594
    :cond_0
    iget-object p1, p0, Lee/cyber/smartid/tse/KeyManager$6;->e:Lee/cyber/smartid/tse/KeyManager;

    invoke-static {p1}, Lee/cyber/smartid/tse/KeyManager;->a(Lee/cyber/smartid/tse/KeyManager;)Lee/cyber/smartid/tse/inter/ListenerAccess;

    move-result-object p1

    new-instance v0, Lee/cyber/smartid/tse/KeyManager$6$1;

    invoke-direct {v0, p0, p2}, Lee/cyber/smartid/tse/KeyManager$6$1;-><init>(Lee/cyber/smartid/tse/KeyManager$6;Lee/cyber/smartid/tse/dto/jsonrpc/resp/ValidatePinResp;)V

    invoke-interface {p1, v0}, Lee/cyber/smartid/tse/inter/ListenerAccess;->notifyUI(Ljava/lang/Runnable;)V

    :goto_0
    return-void
.end method
