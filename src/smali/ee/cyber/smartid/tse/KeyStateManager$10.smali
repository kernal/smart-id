.class Lee/cyber/smartid/tse/KeyStateManager$10;
.super Ljava/lang/Object;
.source "KeyStateManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lee/cyber/smartid/tse/KeyStateManager;->confirmTransaction(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Lee/cyber/smartid/tse/inter/ConfirmTransactionListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lee/cyber/smartid/tse/inter/ConfirmTransactionListener;

.field final synthetic b:Ljava/lang/String;

.field final synthetic c:Lee/cyber/smartid/cryptolib/dto/StorageException;

.field final synthetic d:Lee/cyber/smartid/tse/KeyStateManager;


# direct methods
.method constructor <init>(Lee/cyber/smartid/tse/KeyStateManager;Lee/cyber/smartid/tse/inter/ConfirmTransactionListener;Ljava/lang/String;Lee/cyber/smartid/cryptolib/dto/StorageException;)V
    .locals 0

    .line 879
    iput-object p1, p0, Lee/cyber/smartid/tse/KeyStateManager$10;->d:Lee/cyber/smartid/tse/KeyStateManager;

    iput-object p2, p0, Lee/cyber/smartid/tse/KeyStateManager$10;->a:Lee/cyber/smartid/tse/inter/ConfirmTransactionListener;

    iput-object p3, p0, Lee/cyber/smartid/tse/KeyStateManager$10;->b:Ljava/lang/String;

    iput-object p4, p0, Lee/cyber/smartid/tse/KeyStateManager$10;->c:Lee/cyber/smartid/cryptolib/dto/StorageException;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .line 882
    iget-object v0, p0, Lee/cyber/smartid/tse/KeyStateManager$10;->a:Lee/cyber/smartid/tse/inter/ConfirmTransactionListener;

    if-eqz v0, :cond_0

    .line 883
    iget-object v1, p0, Lee/cyber/smartid/tse/KeyStateManager$10;->b:Ljava/lang/String;

    iget-object v2, p0, Lee/cyber/smartid/tse/KeyStateManager$10;->d:Lee/cyber/smartid/tse/KeyStateManager;

    invoke-static {v2}, Lee/cyber/smartid/tse/KeyStateManager;->j(Lee/cyber/smartid/tse/KeyStateManager;)Lee/cyber/smartid/tse/inter/ResourceAccess;

    move-result-object v2

    invoke-interface {v2}, Lee/cyber/smartid/tse/inter/ResourceAccess;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lee/cyber/smartid/tse/KeyStateManager$10;->d:Lee/cyber/smartid/tse/KeyStateManager;

    invoke-static {v3}, Lee/cyber/smartid/tse/KeyStateManager;->i(Lee/cyber/smartid/tse/KeyStateManager;)Lee/cyber/smartid/tse/inter/WallClock;

    move-result-object v3

    iget-object v4, p0, Lee/cyber/smartid/tse/KeyStateManager$10;->c:Lee/cyber/smartid/cryptolib/dto/StorageException;

    invoke-static {v2, v3, v4}, Lee/cyber/smartid/tse/dto/TSEError;->from(Landroid/content/Context;Lee/cyber/smartid/tse/inter/WallClock;Ljava/lang/Throwable;)Lee/cyber/smartid/tse/dto/TSEError;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lee/cyber/smartid/tse/inter/ConfirmTransactionListener;->onConfirmTransactionFailed(Ljava/lang/String;Lee/cyber/smartid/tse/dto/TSEError;)V

    :cond_0
    return-void
.end method
