.class Lee/cyber/smartid/tse/KeyGenerationManager$1;
.super Ljava/lang/Object;
.source "KeyGenerationManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lee/cyber/smartid/tse/KeyGenerationManager;->generateKeys(Ljava/lang/String;IILjava/math/BigInteger;Lee/cyber/smartid/cryptolib/inter/KeyGenerationOp;Lee/cyber/smartid/cryptolib/inter/RandomGenerationOp;Lee/cyber/smartid/tse/inter/WallClock;Lee/cyber/smartid/tse/inter/ListenerAccess;Lee/cyber/smartid/tse/inter/ResourceAccess;Lee/cyber/smartid/tse/KeyGenerationManager$KeyGenerationWorkerListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lee/cyber/smartid/cryptolib/inter/KeyGenerationOp;

.field final synthetic b:I

.field final synthetic c:Ljava/math/BigInteger;

.field final synthetic d:Lee/cyber/smartid/tse/inter/WallClock;

.field final synthetic e:Lee/cyber/smartid/tse/inter/ResourceAccess;

.field final synthetic f:Lee/cyber/smartid/tse/KeyGenerationManager;


# direct methods
.method constructor <init>(Lee/cyber/smartid/tse/KeyGenerationManager;Lee/cyber/smartid/cryptolib/inter/KeyGenerationOp;ILjava/math/BigInteger;Lee/cyber/smartid/tse/inter/WallClock;Lee/cyber/smartid/tse/inter/ResourceAccess;)V
    .locals 0

    .line 127
    iput-object p1, p0, Lee/cyber/smartid/tse/KeyGenerationManager$1;->f:Lee/cyber/smartid/tse/KeyGenerationManager;

    iput-object p2, p0, Lee/cyber/smartid/tse/KeyGenerationManager$1;->a:Lee/cyber/smartid/cryptolib/inter/KeyGenerationOp;

    iput p3, p0, Lee/cyber/smartid/tse/KeyGenerationManager$1;->b:I

    iput-object p4, p0, Lee/cyber/smartid/tse/KeyGenerationManager$1;->c:Ljava/math/BigInteger;

    iput-object p5, p0, Lee/cyber/smartid/tse/KeyGenerationManager$1;->d:Lee/cyber/smartid/tse/inter/WallClock;

    iput-object p6, p0, Lee/cyber/smartid/tse/KeyGenerationManager$1;->e:Lee/cyber/smartid/tse/inter/ResourceAccess;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    const/4 v0, 0x0

    .line 133
    :try_start_0
    iget-object v1, p0, Lee/cyber/smartid/tse/KeyGenerationManager$1;->a:Lee/cyber/smartid/cryptolib/inter/KeyGenerationOp;

    iget v2, p0, Lee/cyber/smartid/tse/KeyGenerationManager$1;->b:I

    iget-object v3, p0, Lee/cyber/smartid/tse/KeyGenerationManager$1;->c:Ljava/math/BigInteger;

    invoke-interface {v1, v2, v3}, Lee/cyber/smartid/cryptolib/inter/KeyGenerationOp;->generatePQAndCreateKeyShare(ILjava/math/BigInteger;)[Ljava/lang/String;

    move-result-object v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-object v6, v0

    move-object v5, v1

    goto :goto_0

    :catch_0
    move-exception v1

    move-object v5, v0

    move-object v6, v1

    .line 137
    :goto_0
    iget-object v2, p0, Lee/cyber/smartid/tse/KeyGenerationManager$1;->f:Lee/cyber/smartid/tse/KeyGenerationManager;

    iget-object v3, p0, Lee/cyber/smartid/tse/KeyGenerationManager$1;->d:Lee/cyber/smartid/tse/inter/WallClock;

    iget-object v4, p0, Lee/cyber/smartid/tse/KeyGenerationManager$1;->e:Lee/cyber/smartid/tse/inter/ResourceAccess;

    const/4 v7, 0x0

    invoke-static/range {v2 .. v7}, Lee/cyber/smartid/tse/KeyGenerationManager;->a(Lee/cyber/smartid/tse/KeyGenerationManager;Lee/cyber/smartid/tse/inter/WallClock;Lee/cyber/smartid/tse/inter/ResourceAccess;[Ljava/lang/String;Ljava/lang/Exception;[Lee/cyber/smartid/cryptolib/dto/TestResult;)V

    return-void
.end method
