.class Lee/cyber/smartid/tse/KeyStateManager$16;
.super Ljava/lang/Object;
.source "KeyStateManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lee/cyber/smartid/tse/KeyStateManager;->retryLocalPendingState(Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/tse/inter/TSEListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:Lee/cyber/smartid/tse/inter/TSEListener;

.field final synthetic c:Ljava/lang/String;

.field final synthetic d:Lee/cyber/smartid/tse/KeyStateManager;


# direct methods
.method constructor <init>(Lee/cyber/smartid/tse/KeyStateManager;Ljava/lang/String;Lee/cyber/smartid/tse/inter/TSEListener;Ljava/lang/String;)V
    .locals 0

    .line 1353
    iput-object p1, p0, Lee/cyber/smartid/tse/KeyStateManager$16;->d:Lee/cyber/smartid/tse/KeyStateManager;

    iput-object p2, p0, Lee/cyber/smartid/tse/KeyStateManager$16;->a:Ljava/lang/String;

    iput-object p3, p0, Lee/cyber/smartid/tse/KeyStateManager$16;->b:Lee/cyber/smartid/tse/inter/TSEListener;

    iput-object p4, p0, Lee/cyber/smartid/tse/KeyStateManager$16;->c:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .line 1356
    iget-object v0, p0, Lee/cyber/smartid/tse/KeyStateManager$16;->d:Lee/cyber/smartid/tse/KeyStateManager;

    invoke-static {v0}, Lee/cyber/smartid/tse/KeyStateManager;->g(Lee/cyber/smartid/tse/KeyStateManager;)Ljava/lang/Object;

    move-result-object v0

    monitor-enter v0

    const/4 v1, 0x0

    .line 1360
    :try_start_0
    iget-object v2, p0, Lee/cyber/smartid/tse/KeyStateManager$16;->a:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1361
    iget-object v1, p0, Lee/cyber/smartid/tse/KeyStateManager$16;->d:Lee/cyber/smartid/tse/KeyStateManager;

    invoke-static {v1}, Lee/cyber/smartid/tse/KeyStateManager;->h(Lee/cyber/smartid/tse/KeyStateManager;)Lee/cyber/smartid/tse/inter/KeyManagerAccess;

    move-result-object v1

    iget-object v2, p0, Lee/cyber/smartid/tse/KeyStateManager$16;->a:Ljava/lang/String;

    invoke-interface {v1, v2}, Lee/cyber/smartid/tse/inter/KeyManagerAccess;->getKeyStateByKeyId(Ljava/lang/String;)Lee/cyber/smartid/tse/dto/KeyState;

    move-result-object v1

    :cond_0
    if-eqz v1, :cond_5

    .line 1365
    invoke-virtual {v1}, Lee/cyber/smartid/tse/dto/KeyState;->isActiveSign()Z

    move-result v2

    if-nez v2, :cond_1

    invoke-virtual {v1}, Lee/cyber/smartid/tse/dto/KeyState;->isActiveSubmitClientSecondPart()Z

    move-result v2

    if-nez v2, :cond_1

    goto :goto_0

    .line 1378
    :cond_1
    iget-object v2, p0, Lee/cyber/smartid/tse/KeyStateManager$16;->b:Lee/cyber/smartid/tse/inter/TSEListener;

    if-eqz v2, :cond_4

    invoke-virtual {v1}, Lee/cyber/smartid/tse/dto/KeyState;->isActiveSign()Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lee/cyber/smartid/tse/KeyStateManager$16;->b:Lee/cyber/smartid/tse/inter/TSEListener;

    instance-of v2, v2, Lee/cyber/smartid/tse/inter/ConfirmTransactionListener;

    if-eqz v2, :cond_3

    :cond_2
    invoke-virtual {v1}, Lee/cyber/smartid/tse/dto/KeyState;->isActiveSubmitClientSecondPart()Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lee/cyber/smartid/tse/KeyStateManager$16;->b:Lee/cyber/smartid/tse/inter/TSEListener;

    instance-of v2, v2, Lee/cyber/smartid/tse/inter/SubmitClientSecondPartListener;

    if-nez v2, :cond_4

    .line 1379
    :cond_3
    iget-object v1, p0, Lee/cyber/smartid/tse/KeyStateManager$16;->d:Lee/cyber/smartid/tse/KeyStateManager;

    new-instance v2, Lee/cyber/smartid/tse/KeyStateManager$16$2;

    invoke-direct {v2, p0}, Lee/cyber/smartid/tse/KeyStateManager$16$2;-><init>(Lee/cyber/smartid/tse/KeyStateManager$16;)V

    invoke-static {v1, v2}, Lee/cyber/smartid/tse/KeyStateManager;->a(Lee/cyber/smartid/tse/KeyStateManager;Ljava/lang/Runnable;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1386
    :try_start_1
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void

    .line 1388
    :cond_4
    :try_start_2
    iget-object v2, p0, Lee/cyber/smartid/tse/KeyStateManager$16;->d:Lee/cyber/smartid/tse/KeyStateManager;

    invoke-static {v2}, Lee/cyber/smartid/tse/KeyStateManager;->b(Lee/cyber/smartid/tse/KeyStateManager;)Lee/cyber/smartid/tse/inter/LogAccess;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "retryLocalPendingState: retrying "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v4, p0, Lee/cyber/smartid/tse/KeyStateManager$16;->a:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lee/cyber/smartid/tse/inter/LogAccess;->d(Ljava/lang/String;)V

    .line 1390
    iget-object v2, p0, Lee/cyber/smartid/tse/KeyStateManager$16;->d:Lee/cyber/smartid/tse/KeyStateManager;

    iget-object v3, p0, Lee/cyber/smartid/tse/KeyStateManager$16;->c:Ljava/lang/String;

    invoke-static {v1}, Lee/cyber/smartid/tse/dto/ProtoKeyState;->retryFrom(Lee/cyber/smartid/tse/dto/KeyState;)Lee/cyber/smartid/tse/dto/ProtoKeyState;

    move-result-object v1

    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    const-string v5, "external"

    invoke-static {v4, v5}, Lee/cyber/smartid/tse/util/Util;->addXSplitKeyTriggerExtra(Landroid/os/Bundle;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v4

    iget-object v5, p0, Lee/cyber/smartid/tse/KeyStateManager$16;->b:Lee/cyber/smartid/tse/inter/TSEListener;

    invoke-virtual {v2, v3, v1, v4, v5}, Lee/cyber/smartid/tse/KeyStateManager;->queue(Ljava/lang/String;Lee/cyber/smartid/tse/dto/ProtoKeyState;Landroid/os/Bundle;Lee/cyber/smartid/tse/inter/TSEListener;)V

    goto :goto_1

    .line 1366
    :cond_5
    :goto_0
    iget-object v1, p0, Lee/cyber/smartid/tse/KeyStateManager$16;->d:Lee/cyber/smartid/tse/KeyStateManager;

    invoke-static {v1}, Lee/cyber/smartid/tse/KeyStateManager;->b(Lee/cyber/smartid/tse/KeyStateManager;)Lee/cyber/smartid/tse/inter/LogAccess;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "retryLocalPendingState: didn\'t find an active state to retry for keyId "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lee/cyber/smartid/tse/KeyStateManager$16;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lee/cyber/smartid/tse/inter/LogAccess;->d(Ljava/lang/String;)V

    .line 1368
    iget-object v1, p0, Lee/cyber/smartid/tse/KeyStateManager$16;->d:Lee/cyber/smartid/tse/KeyStateManager;

    new-instance v2, Lee/cyber/smartid/tse/KeyStateManager$16$1;

    invoke-direct {v2, p0}, Lee/cyber/smartid/tse/KeyStateManager$16$1;-><init>(Lee/cyber/smartid/tse/KeyStateManager$16;)V

    invoke-static {v1, v2}, Lee/cyber/smartid/tse/KeyStateManager;->a(Lee/cyber/smartid/tse/KeyStateManager;Ljava/lang/Runnable;)V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1375
    :try_start_3
    monitor-exit v0

    return-void

    :catchall_0
    move-exception v1

    goto :goto_2

    :catch_0
    move-exception v1

    .line 1392
    iget-object v2, p0, Lee/cyber/smartid/tse/KeyStateManager$16;->d:Lee/cyber/smartid/tse/KeyStateManager;

    invoke-static {v2}, Lee/cyber/smartid/tse/KeyStateManager;->b(Lee/cyber/smartid/tse/KeyStateManager;)Lee/cyber/smartid/tse/inter/LogAccess;

    move-result-object v2

    const-string v3, "retryLocalPendingState"

    invoke-interface {v2, v3, v1}, Lee/cyber/smartid/tse/inter/LogAccess;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1393
    iget-object v2, p0, Lee/cyber/smartid/tse/KeyStateManager$16;->d:Lee/cyber/smartid/tse/KeyStateManager;

    new-instance v3, Lee/cyber/smartid/tse/KeyStateManager$16$3;

    invoke-direct {v3, p0, v1}, Lee/cyber/smartid/tse/KeyStateManager$16$3;-><init>(Lee/cyber/smartid/tse/KeyStateManager$16;Ljava/lang/Throwable;)V

    invoke-static {v2, v3}, Lee/cyber/smartid/tse/KeyStateManager;->a(Lee/cyber/smartid/tse/KeyStateManager;Ljava/lang/Runnable;)V

    .line 1401
    :goto_1
    monitor-exit v0

    return-void

    :goto_2
    monitor-exit v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v1
.end method
