.class Lee/cyber/smartid/tse/KeyManager$10;
.super Ljava/lang/Object;
.source "KeyManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lee/cyber/smartid/tse/KeyManager;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lee/cyber/smartid/tse/dto/EncryptKeyWrapper;

.field final synthetic b:Ljava/lang/String;

.field final synthetic c:Lee/cyber/smartid/tse/dto/KTKPublicKey;

.field final synthetic d:Landroid/os/Handler;

.field final synthetic e:Ljava/lang/String;

.field final synthetic f:Lee/cyber/smartid/tse/KeyManager;


# direct methods
.method constructor <init>(Lee/cyber/smartid/tse/KeyManager;Lee/cyber/smartid/tse/dto/EncryptKeyWrapper;Ljava/lang/String;Lee/cyber/smartid/tse/dto/KTKPublicKey;Landroid/os/Handler;Ljava/lang/String;)V
    .locals 0

    .line 674
    iput-object p1, p0, Lee/cyber/smartid/tse/KeyManager$10;->f:Lee/cyber/smartid/tse/KeyManager;

    iput-object p2, p0, Lee/cyber/smartid/tse/KeyManager$10;->a:Lee/cyber/smartid/tse/dto/EncryptKeyWrapper;

    iput-object p3, p0, Lee/cyber/smartid/tse/KeyManager$10;->b:Ljava/lang/String;

    iput-object p4, p0, Lee/cyber/smartid/tse/KeyManager$10;->c:Lee/cyber/smartid/tse/dto/KTKPublicKey;

    iput-object p5, p0, Lee/cyber/smartid/tse/KeyManager$10;->d:Landroid/os/Handler;

    iput-object p6, p0, Lee/cyber/smartid/tse/KeyManager$10;->e:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(ZLjava/lang/Exception;)V
    .locals 2

    .line 689
    iget-object v0, p0, Lee/cyber/smartid/tse/KeyManager$10;->d:Landroid/os/Handler;

    new-instance v1, Lee/cyber/smartid/tse/KeyManager$10$1;

    invoke-direct {v1, p0, p1, p2}, Lee/cyber/smartid/tse/KeyManager$10$1;-><init>(Lee/cyber/smartid/tse/KeyManager$10;ZLjava/lang/Exception;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .line 678
    :try_start_0
    iget-object v0, p0, Lee/cyber/smartid/tse/KeyManager$10;->f:Lee/cyber/smartid/tse/KeyManager;

    iget-object v1, p0, Lee/cyber/smartid/tse/KeyManager$10;->a:Lee/cyber/smartid/tse/dto/EncryptKeyWrapper;

    invoke-static {v0, v1}, Lee/cyber/smartid/tse/KeyManager;->a(Lee/cyber/smartid/tse/KeyManager;Lee/cyber/smartid/tse/dto/EncryptKeyWrapper;)V

    .line 679
    iget-object v0, p0, Lee/cyber/smartid/tse/KeyManager$10;->f:Lee/cyber/smartid/tse/KeyManager;

    iget-object v1, p0, Lee/cyber/smartid/tse/KeyManager$10;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lee/cyber/smartid/tse/KeyManager;->a(Lee/cyber/smartid/tse/KeyManager;Ljava/lang/String;)V

    .line 680
    iget-object v0, p0, Lee/cyber/smartid/tse/KeyManager$10;->f:Lee/cyber/smartid/tse/KeyManager;

    invoke-static {v0}, Lee/cyber/smartid/tse/KeyManager;->b(Lee/cyber/smartid/tse/KeyManager;)Lee/cyber/smartid/tse/util/Log;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "encryptKeyInternal - stored the key for Secure Zone szId: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lee/cyber/smartid/tse/KeyManager$10;->c:Lee/cyber/smartid/tse/dto/KTKPublicKey;

    invoke-virtual {v2}, Lee/cyber/smartid/tse/dto/KTKPublicKey;->getSzId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, ", keyId: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lee/cyber/smartid/tse/KeyManager$10;->c:Lee/cyber/smartid/tse/dto/KTKPublicKey;

    invoke-virtual {v2}, Lee/cyber/smartid/tse/dto/KTKPublicKey;->getKeyId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lee/cyber/smartid/tse/util/Log;->d(Ljava/lang/String;)V

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 681
    invoke-direct {p0, v0, v1}, Lee/cyber/smartid/tse/KeyManager$10;->a(ZLjava/lang/Exception;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 683
    iget-object v1, p0, Lee/cyber/smartid/tse/KeyManager$10;->f:Lee/cyber/smartid/tse/KeyManager;

    invoke-static {v1}, Lee/cyber/smartid/tse/KeyManager;->b(Lee/cyber/smartid/tse/KeyManager;)Lee/cyber/smartid/tse/util/Log;

    move-result-object v1

    const-string v2, "encryptKey"

    invoke-virtual {v1, v2, v0}, Lee/cyber/smartid/tse/util/Log;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    const/4 v1, 0x0

    .line 684
    invoke-direct {p0, v1, v0}, Lee/cyber/smartid/tse/KeyManager$10;->a(ZLjava/lang/Exception;)V

    :goto_0
    return-void
.end method
