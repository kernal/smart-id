.class Lee/cyber/smartid/tse/SmartIdTSE$6;
.super Lee/cyber/smartid/tse/network/RPCCallbackNonUIThread;
.source "SmartIdTSE.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lee/cyber/smartid/tse/SmartIdTSE;->requestNewFreshnessToken(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/tse/inter/NewFreshnessTokenListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lee/cyber/smartid/tse/network/RPCCallbackNonUIThread<",
        "Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCResponse<",
        "Lee/cyber/smartid/tse/dto/jsonrpc/result/NewFreshnessTokenResult;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:Ljava/lang/String;

.field final synthetic c:Lee/cyber/smartid/tse/SmartIdTSE;


# direct methods
.method constructor <init>(Lee/cyber/smartid/tse/SmartIdTSE;Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCRequest;Lee/cyber/smartid/tse/SmartIdTSE;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 2334
    iput-object p1, p0, Lee/cyber/smartid/tse/SmartIdTSE$6;->c:Lee/cyber/smartid/tse/SmartIdTSE;

    iput-object p4, p0, Lee/cyber/smartid/tse/SmartIdTSE$6;->a:Ljava/lang/String;

    iput-object p5, p0, Lee/cyber/smartid/tse/SmartIdTSE$6;->b:Ljava/lang/String;

    invoke-direct {p0, p2, p3}, Lee/cyber/smartid/tse/network/RPCCallbackNonUIThread;-><init>(Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCRequest;Lee/cyber/smartid/tse/SmartIdTSE;)V

    return-void
.end method


# virtual methods
.method public onFailure(Lc/b;Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCError;Ljava/lang/Throwable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lc/b<",
            "Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCResponse<",
            "Lee/cyber/smartid/tse/dto/jsonrpc/result/NewFreshnessTokenResult;",
            ">;>;",
            "Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCError;",
            "Ljava/lang/Throwable;",
            ")V"
        }
    .end annotation

    .line 2364
    iget-object p1, p0, Lee/cyber/smartid/tse/SmartIdTSE$6;->c:Lee/cyber/smartid/tse/SmartIdTSE;

    new-instance v0, Lee/cyber/smartid/tse/SmartIdTSE$6$3;

    invoke-direct {v0, p0, p2, p3}, Lee/cyber/smartid/tse/SmartIdTSE$6$3;-><init>(Lee/cyber/smartid/tse/SmartIdTSE$6;Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCError;Ljava/lang/Throwable;)V

    invoke-static {p1, v0}, Lee/cyber/smartid/tse/SmartIdTSE;->a(Lee/cyber/smartid/tse/SmartIdTSE;Ljava/lang/Runnable;)V

    return-void
.end method

.method public onSuccess(Lc/b;Lc/r;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lc/b<",
            "Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCResponse<",
            "Lee/cyber/smartid/tse/dto/jsonrpc/result/NewFreshnessTokenResult;",
            ">;>;",
            "Lc/r<",
            "Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCResponse<",
            "Lee/cyber/smartid/tse/dto/jsonrpc/result/NewFreshnessTokenResult;",
            ">;>;)V"
        }
    .end annotation

    .line 2340
    :try_start_0
    iget-object p1, p0, Lee/cyber/smartid/tse/SmartIdTSE$6;->c:Lee/cyber/smartid/tse/SmartIdTSE;

    iget-object v0, p0, Lee/cyber/smartid/tse/SmartIdTSE$6;->a:Ljava/lang/String;

    invoke-virtual {p2}, Lc/r;->e()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCResponse;

    invoke-virtual {p2}, Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCResponse;->getResult()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lee/cyber/smartid/tse/dto/jsonrpc/result/NewFreshnessTokenResult;

    invoke-virtual {p2}, Lee/cyber/smartid/tse/dto/jsonrpc/result/NewFreshnessTokenResult;->getFreshnessToken()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, v0, p2}, Lee/cyber/smartid/tse/SmartIdTSE;->updateFreshnessToken(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Lee/cyber/smartid/cryptolib/dto/StorageException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2351
    iget-object p1, p0, Lee/cyber/smartid/tse/SmartIdTSE$6;->c:Lee/cyber/smartid/tse/SmartIdTSE;

    new-instance p2, Lee/cyber/smartid/tse/SmartIdTSE$6$2;

    invoke-direct {p2, p0}, Lee/cyber/smartid/tse/SmartIdTSE$6$2;-><init>(Lee/cyber/smartid/tse/SmartIdTSE$6;)V

    invoke-static {p1, p2}, Lee/cyber/smartid/tse/SmartIdTSE;->a(Lee/cyber/smartid/tse/SmartIdTSE;Ljava/lang/Runnable;)V

    return-void

    :catch_0
    move-exception p1

    .line 2342
    iget-object p2, p0, Lee/cyber/smartid/tse/SmartIdTSE$6;->c:Lee/cyber/smartid/tse/SmartIdTSE;

    invoke-static {p2}, Lee/cyber/smartid/tse/SmartIdTSE;->k(Lee/cyber/smartid/tse/SmartIdTSE;)Lee/cyber/smartid/tse/inter/LogAccess;

    move-result-object p2

    const-string v0, "requestNewFreshnessToken"

    invoke-interface {p2, v0, p1}, Lee/cyber/smartid/tse/inter/LogAccess;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2343
    iget-object p2, p0, Lee/cyber/smartid/tse/SmartIdTSE$6;->c:Lee/cyber/smartid/tse/SmartIdTSE;

    new-instance v0, Lee/cyber/smartid/tse/SmartIdTSE$6$1;

    invoke-direct {v0, p0, p1}, Lee/cyber/smartid/tse/SmartIdTSE$6$1;-><init>(Lee/cyber/smartid/tse/SmartIdTSE$6;Lee/cyber/smartid/cryptolib/dto/StorageException;)V

    invoke-static {p2, v0}, Lee/cyber/smartid/tse/SmartIdTSE;->a(Lee/cyber/smartid/tse/SmartIdTSE;Ljava/lang/Runnable;)V

    return-void
.end method

.method public onValidate(Lc/b;Lc/r;)Z
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lc/b<",
            "Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCResponse<",
            "Lee/cyber/smartid/tse/dto/jsonrpc/result/NewFreshnessTokenResult;",
            ">;>;",
            "Lc/r<",
            "Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCResponse<",
            "Lee/cyber/smartid/tse/dto/jsonrpc/result/NewFreshnessTokenResult;",
            ">;>;)Z"
        }
    .end annotation

    if-eqz p2, :cond_0

    .line 2375
    invoke-virtual {p2}, Lc/r;->e()Ljava/lang/Object;

    move-result-object p1

    if-eqz p1, :cond_0

    invoke-virtual {p2}, Lc/r;->e()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCResponse;

    invoke-virtual {p1}, Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCResponse;->getResult()Ljava/lang/Object;

    move-result-object p1

    if-eqz p1, :cond_0

    invoke-virtual {p2}, Lc/r;->e()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCResponse;

    invoke-virtual {p1}, Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCResponse;->getResult()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lee/cyber/smartid/tse/dto/jsonrpc/result/NewFreshnessTokenResult;

    invoke-virtual {p1}, Lee/cyber/smartid/tse/dto/jsonrpc/result/NewFreshnessTokenResult;->isValid()Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method
