.class Lee/cyber/smartid/tse/KeyStateManager$3$2;
.super Ljava/lang/Object;
.source "KeyStateManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lee/cyber/smartid/tse/KeyStateManager$3;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lee/cyber/smartid/tse/KeyStateManager$3;


# direct methods
.method constructor <init>(Lee/cyber/smartid/tse/KeyStateManager$3;)V
    .locals 0

    .line 706
    iput-object p1, p0, Lee/cyber/smartid/tse/KeyStateManager$3$2;->a:Lee/cyber/smartid/tse/KeyStateManager$3;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .line 709
    iget-object v0, p0, Lee/cyber/smartid/tse/KeyStateManager$3$2;->a:Lee/cyber/smartid/tse/KeyStateManager$3;

    iget-object v0, v0, Lee/cyber/smartid/tse/KeyStateManager$3;->h:Lee/cyber/smartid/tse/KeyStateManager;

    iget-object v1, p0, Lee/cyber/smartid/tse/KeyStateManager$3$2;->a:Lee/cyber/smartid/tse/KeyStateManager$3;

    iget-object v1, v1, Lee/cyber/smartid/tse/KeyStateManager$3;->h:Lee/cyber/smartid/tse/KeyStateManager;

    invoke-static {v1}, Lee/cyber/smartid/tse/KeyStateManager;->f(Lee/cyber/smartid/tse/KeyStateManager;)Lee/cyber/smartid/tse/inter/ListenerAccess;

    move-result-object v1

    iget-object v2, p0, Lee/cyber/smartid/tse/KeyStateManager$3$2;->a:Lee/cyber/smartid/tse/KeyStateManager$3;

    iget-object v2, v2, Lee/cyber/smartid/tse/KeyStateManager$3;->c:Ljava/lang/String;

    const-class v3, Lee/cyber/smartid/tse/inter/SubmitClientSecondPartListener;

    const/4 v4, 0x1

    invoke-interface {v1, v2, v4, v3}, Lee/cyber/smartid/tse/inter/ListenerAccess;->getListener(Ljava/lang/String;ZLjava/lang/Class;)Lee/cyber/smartid/tse/inter/TSEListener;

    move-result-object v1

    iget-object v2, p0, Lee/cyber/smartid/tse/KeyStateManager$3$2;->a:Lee/cyber/smartid/tse/KeyStateManager$3;

    iget-object v2, v2, Lee/cyber/smartid/tse/KeyStateManager$3;->c:Ljava/lang/String;

    iget-object v3, p0, Lee/cyber/smartid/tse/KeyStateManager$3$2;->a:Lee/cyber/smartid/tse/KeyStateManager$3;

    iget-object v3, v3, Lee/cyber/smartid/tse/KeyStateManager$3;->h:Lee/cyber/smartid/tse/KeyStateManager;

    invoke-static {v3}, Lee/cyber/smartid/tse/KeyStateManager;->i(Lee/cyber/smartid/tse/KeyStateManager;)Lee/cyber/smartid/tse/inter/WallClock;

    move-result-object v3

    iget-object v5, p0, Lee/cyber/smartid/tse/KeyStateManager$3$2;->a:Lee/cyber/smartid/tse/KeyStateManager$3;

    iget-object v5, v5, Lee/cyber/smartid/tse/KeyStateManager$3;->h:Lee/cyber/smartid/tse/KeyStateManager;

    invoke-static {v5}, Lee/cyber/smartid/tse/KeyStateManager;->j(Lee/cyber/smartid/tse/KeyStateManager;)Lee/cyber/smartid/tse/inter/ResourceAccess;

    move-result-object v5

    invoke-interface {v5}, Lee/cyber/smartid/tse/inter/ResourceAccess;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    sget v6, Lee/cyber/smartid/tse/R$string;->err_no_such_key_found:I

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    const-wide/16 v6, 0x3f3

    invoke-static {v3, v6, v7, v5}, Lee/cyber/smartid/tse/dto/TSEError;->from(Lee/cyber/smartid/tse/inter/WallClock;JLjava/lang/String;)Lee/cyber/smartid/tse/dto/TSEError;

    move-result-object v3

    invoke-virtual {v3, v4}, Lee/cyber/smartid/tse/dto/TSEError;->asNonRetriable(Z)Lee/cyber/smartid/tse/dto/TSEError;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, Lee/cyber/smartid/tse/KeyStateManager;->a(Lee/cyber/smartid/tse/KeyStateManager;Lee/cyber/smartid/tse/inter/TSEListener;Ljava/lang/String;Lee/cyber/smartid/tse/dto/TSEError;)V

    return-void
.end method
