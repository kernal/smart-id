.class Lee/cyber/smartid/tse/SmartIdTSE$6$2;
.super Ljava/lang/Object;
.source "SmartIdTSE.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lee/cyber/smartid/tse/SmartIdTSE$6;->onSuccess(Lc/b;Lc/r;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lee/cyber/smartid/tse/SmartIdTSE$6;


# direct methods
.method constructor <init>(Lee/cyber/smartid/tse/SmartIdTSE$6;)V
    .locals 0

    .line 2351
    iput-object p1, p0, Lee/cyber/smartid/tse/SmartIdTSE$6$2;->a:Lee/cyber/smartid/tse/SmartIdTSE$6;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .line 2354
    iget-object v0, p0, Lee/cyber/smartid/tse/SmartIdTSE$6$2;->a:Lee/cyber/smartid/tse/SmartIdTSE$6;

    iget-object v0, v0, Lee/cyber/smartid/tse/SmartIdTSE$6;->c:Lee/cyber/smartid/tse/SmartIdTSE;

    iget-object v1, p0, Lee/cyber/smartid/tse/SmartIdTSE$6$2;->a:Lee/cyber/smartid/tse/SmartIdTSE$6;

    iget-object v1, v1, Lee/cyber/smartid/tse/SmartIdTSE$6;->b:Ljava/lang/String;

    const-class v2, Lee/cyber/smartid/tse/inter/NewFreshnessTokenListener;

    const/4 v3, 0x1

    invoke-static {v0, v1, v3, v2}, Lee/cyber/smartid/tse/SmartIdTSE;->a(Lee/cyber/smartid/tse/SmartIdTSE;Ljava/lang/String;ZLjava/lang/Class;)Lee/cyber/smartid/tse/inter/TSEListener;

    move-result-object v0

    check-cast v0, Lee/cyber/smartid/tse/inter/NewFreshnessTokenListener;

    if-eqz v0, :cond_0

    .line 2356
    iget-object v1, p0, Lee/cyber/smartid/tse/SmartIdTSE$6$2;->a:Lee/cyber/smartid/tse/SmartIdTSE$6;

    iget-object v1, v1, Lee/cyber/smartid/tse/SmartIdTSE$6;->b:Ljava/lang/String;

    new-instance v2, Lee/cyber/smartid/tse/dto/jsonrpc/resp/NewFreshnessTokenResp;

    iget-object v3, p0, Lee/cyber/smartid/tse/SmartIdTSE$6$2;->a:Lee/cyber/smartid/tse/SmartIdTSE$6;

    iget-object v3, v3, Lee/cyber/smartid/tse/SmartIdTSE$6;->b:Ljava/lang/String;

    invoke-direct {v2, v3}, Lee/cyber/smartid/tse/dto/jsonrpc/resp/NewFreshnessTokenResp;-><init>(Ljava/lang/String;)V

    invoke-interface {v0, v1, v2}, Lee/cyber/smartid/tse/inter/NewFreshnessTokenListener;->onNewFreshnessTokenSuccess(Ljava/lang/String;Lee/cyber/smartid/tse/dto/jsonrpc/resp/NewFreshnessTokenResp;)V

    :cond_0
    return-void
.end method
