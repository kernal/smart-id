.class Lee/cyber/smartid/tse/KeyStateManager$6;
.super Ljava/lang/Object;
.source "KeyStateManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lee/cyber/smartid/tse/KeyStateManager;->confirmTransaction(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Lee/cyber/smartid/tse/inter/ConfirmTransactionListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lee/cyber/smartid/tse/inter/ConfirmTransactionListener;

.field final synthetic b:Ljava/lang/String;

.field final synthetic c:Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;

.field final synthetic d:Lee/cyber/smartid/tse/KeyStateManager;


# direct methods
.method constructor <init>(Lee/cyber/smartid/tse/KeyStateManager;Lee/cyber/smartid/tse/inter/ConfirmTransactionListener;Ljava/lang/String;Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;)V
    .locals 0

    .line 818
    iput-object p1, p0, Lee/cyber/smartid/tse/KeyStateManager$6;->d:Lee/cyber/smartid/tse/KeyStateManager;

    iput-object p2, p0, Lee/cyber/smartid/tse/KeyStateManager$6;->a:Lee/cyber/smartid/tse/inter/ConfirmTransactionListener;

    iput-object p3, p0, Lee/cyber/smartid/tse/KeyStateManager$6;->b:Ljava/lang/String;

    iput-object p4, p0, Lee/cyber/smartid/tse/KeyStateManager$6;->c:Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .line 821
    iget-object v0, p0, Lee/cyber/smartid/tse/KeyStateManager$6;->a:Lee/cyber/smartid/tse/inter/ConfirmTransactionListener;

    if-eqz v0, :cond_1

    .line 822
    iget-object v1, p0, Lee/cyber/smartid/tse/KeyStateManager$6;->b:Ljava/lang/String;

    iget-object v2, p0, Lee/cyber/smartid/tse/KeyStateManager$6;->c:Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;

    invoke-virtual {v2}, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;->getErrorCode()I

    move-result v2

    const/16 v3, 0x67

    if-ne v2, v3, :cond_0

    iget-object v2, p0, Lee/cyber/smartid/tse/KeyStateManager$6;->d:Lee/cyber/smartid/tse/KeyStateManager;

    invoke-static {v2}, Lee/cyber/smartid/tse/KeyStateManager;->i(Lee/cyber/smartid/tse/KeyStateManager;)Lee/cyber/smartid/tse/inter/WallClock;

    move-result-object v2

    const-wide/16 v3, 0x3f2

    iget-object v5, p0, Lee/cyber/smartid/tse/KeyStateManager$6;->d:Lee/cyber/smartid/tse/KeyStateManager;

    invoke-static {v5}, Lee/cyber/smartid/tse/KeyStateManager;->j(Lee/cyber/smartid/tse/KeyStateManager;)Lee/cyber/smartid/tse/inter/ResourceAccess;

    move-result-object v5

    invoke-interface {v5}, Lee/cyber/smartid/tse/inter/ResourceAccess;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    sget v6, Lee/cyber/smartid/tse/R$string;->err_invalid_signature_hash_type:I

    goto :goto_0

    :cond_0
    iget-object v2, p0, Lee/cyber/smartid/tse/KeyStateManager$6;->d:Lee/cyber/smartid/tse/KeyStateManager;

    invoke-static {v2}, Lee/cyber/smartid/tse/KeyStateManager;->i(Lee/cyber/smartid/tse/KeyStateManager;)Lee/cyber/smartid/tse/inter/WallClock;

    move-result-object v2

    const-wide/16 v3, 0x3f1

    iget-object v5, p0, Lee/cyber/smartid/tse/KeyStateManager$6;->d:Lee/cyber/smartid/tse/KeyStateManager;

    invoke-static {v5}, Lee/cyber/smartid/tse/KeyStateManager;->j(Lee/cyber/smartid/tse/KeyStateManager;)Lee/cyber/smartid/tse/inter/ResourceAccess;

    move-result-object v5

    invoke-interface {v5}, Lee/cyber/smartid/tse/inter/ResourceAccess;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    sget v6, Lee/cyber/smartid/tse/R$string;->err_unknown:I

    :goto_0
    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lee/cyber/smartid/tse/KeyStateManager$6;->c:Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;

    invoke-static {v2, v3, v4, v5, v6}, Lee/cyber/smartid/tse/dto/TSEError;->from(Lee/cyber/smartid/tse/inter/WallClock;JLjava/lang/String;Ljava/lang/Throwable;)Lee/cyber/smartid/tse/dto/TSEError;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lee/cyber/smartid/tse/inter/ConfirmTransactionListener;->onConfirmTransactionFailed(Ljava/lang/String;Lee/cyber/smartid/tse/dto/TSEError;)V

    :cond_1
    return-void
.end method
