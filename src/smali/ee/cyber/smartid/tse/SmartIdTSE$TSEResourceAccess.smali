.class public Lee/cyber/smartid/tse/SmartIdTSE$TSEResourceAccess;
.super Ljava/lang/Object;
.source "SmartIdTSE.java"

# interfaces
.implements Lee/cyber/smartid/tse/inter/ResourceAccess;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lee/cyber/smartid/tse/SmartIdTSE;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "TSEResourceAccess"
.end annotation


# instance fields
.field final synthetic a:Lee/cyber/smartid/tse/SmartIdTSE;

.field private final b:Ljava/lang/String;

.field private final c:Ljava/lang/String;

.field private final d:Ljava/lang/String;

.field private final e:Ljava/lang/String;

.field private final f:Ljava/lang/String;

.field private final g:Ljava/lang/String;

.field private final h:Ljava/lang/String;


# direct methods
.method private constructor <init>(Lee/cyber/smartid/tse/SmartIdTSE;)V
    .locals 0

    .line 462
    iput-object p1, p0, Lee/cyber/smartid/tse/SmartIdTSE$TSEResourceAccess;->a:Lee/cyber/smartid/tse/SmartIdTSE;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string p1, "ee.cyber.smartid.APP_ENCRYPTED_KEYS_ID_%1$s"

    .line 454
    iput-object p1, p0, Lee/cyber/smartid/tse/SmartIdTSE$TSEResourceAccess;->b:Ljava/lang/String;

    const-string p1, "ee.cyber.smartid.APP_PASSWORD_DATA_%1$s"

    .line 455
    iput-object p1, p0, Lee/cyber/smartid/tse/SmartIdTSE$TSEResourceAccess;->c:Ljava/lang/String;

    const-string p1, "ee.cyber.smartid.APP_INSTANCE_ID_DATA_%1$s"

    .line 456
    iput-object p1, p0, Lee/cyber/smartid/tse/SmartIdTSE$TSEResourceAccess;->d:Ljava/lang/String;

    const-string p1, "ee.cyber.smartid.APP_KEY_STATE_FOR_KEY_ID_%1$s"

    .line 457
    iput-object p1, p0, Lee/cyber/smartid/tse/SmartIdTSE$TSEResourceAccess;->e:Ljava/lang/String;

    const-string p1, "ee.cyber.smartid.APP_KEY_STATE_META_FOR_KEY_STATE_ID_%1$s"

    .line 458
    iput-object p1, p0, Lee/cyber/smartid/tse/SmartIdTSE$TSEResourceAccess;->f:Ljava/lang/String;

    const-string p1, "ee.cyber.smartid.APP_FRESHNESS_TOKEN_KEY_STATE_ID_%1$s"

    .line 459
    iput-object p1, p0, Lee/cyber/smartid/tse/SmartIdTSE$TSEResourceAccess;->g:Ljava/lang/String;

    const-string p1, "APP_LAST_PRNG_TEST_RESULT_%1$s"

    .line 460
    iput-object p1, p0, Lee/cyber/smartid/tse/SmartIdTSE$TSEResourceAccess;->h:Ljava/lang/String;

    return-void
.end method

.method synthetic constructor <init>(Lee/cyber/smartid/tse/SmartIdTSE;Lee/cyber/smartid/tse/SmartIdTSE$1;)V
    .locals 0

    .line 453
    invoke-direct {p0, p1}, Lee/cyber/smartid/tse/SmartIdTSE$TSEResourceAccess;-><init>(Lee/cyber/smartid/tse/SmartIdTSE;)V

    return-void
.end method


# virtual methods
.method public getAppInstanceStorageId()Ljava/lang/String;
    .locals 3

    const/4 v0, 0x1

    .line 530
    new-array v0, v0, [Ljava/lang/Object;

    invoke-virtual {p0}, Lee/cyber/smartid/tse/SmartIdTSE$TSEResourceAccess;->getDeviceFingerPrint()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    const-string v1, "ee.cyber.smartid.APP_INSTANCE_ID_DATA_%1$s"

    invoke-static {v1, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getAppVersion()Ljava/lang/String;
    .locals 1

    .line 520
    iget-object v0, p0, Lee/cyber/smartid/tse/SmartIdTSE$TSEResourceAccess;->a:Lee/cyber/smartid/tse/SmartIdTSE;

    invoke-static {v0}, Lee/cyber/smartid/tse/SmartIdTSE;->a(Lee/cyber/smartid/tse/SmartIdTSE;)Lee/cyber/smartid/tse/inter/ExternalResourceAccess;

    move-result-object v0

    invoke-interface {v0}, Lee/cyber/smartid/tse/inter/ExternalResourceAccess;->getAppVersion()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getApplicationContext()Landroid/content/Context;
    .locals 1

    .line 472
    iget-object v0, p0, Lee/cyber/smartid/tse/SmartIdTSE$TSEResourceAccess;->a:Lee/cyber/smartid/tse/SmartIdTSE;

    invoke-static {v0}, Lee/cyber/smartid/tse/SmartIdTSE;->b(Lee/cyber/smartid/tse/SmartIdTSE;)Landroid/content/Context;

    move-result-object v0

    return-object v0
.end method

.method public getAutomaticRequestRetryDelay(I)J
    .locals 2

    .line 546
    iget-object v0, p0, Lee/cyber/smartid/tse/SmartIdTSE$TSEResourceAccess;->a:Lee/cyber/smartid/tse/SmartIdTSE;

    invoke-virtual {v0, p1}, Lee/cyber/smartid/tse/SmartIdTSE;->getAutomaticRequestRetryDelay(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public getDeviceFingerPrint()Ljava/lang/String;
    .locals 1

    .line 467
    iget-object v0, p0, Lee/cyber/smartid/tse/SmartIdTSE$TSEResourceAccess;->a:Lee/cyber/smartid/tse/SmartIdTSE;

    invoke-static {v0}, Lee/cyber/smartid/tse/SmartIdTSE;->a(Lee/cyber/smartid/tse/SmartIdTSE;)Lee/cyber/smartid/tse/inter/ExternalResourceAccess;

    move-result-object v0

    invoke-interface {v0}, Lee/cyber/smartid/tse/inter/ExternalResourceAccess;->getDeviceFingerPrint()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getEncryptedKeysStorageId()Ljava/lang/String;
    .locals 3

    const/4 v0, 0x1

    .line 575
    new-array v0, v0, [Ljava/lang/Object;

    invoke-virtual {p0}, Lee/cyber/smartid/tse/SmartIdTSE$TSEResourceAccess;->getDeviceFingerPrint()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    const-string v1, "ee.cyber.smartid.APP_ENCRYPTED_KEYS_ID_%1$s"

    invoke-static {v1, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getFreshnessTokenId(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    const/4 v0, 0x1

    .line 490
    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const-string p1, "ee.cyber.smartid.APP_FRESHNESS_TOKEN_KEY_STATE_ID_%1$s"

    invoke-static {p1, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public getKeyStateIdByKeyId(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    const/4 v0, 0x1

    .line 478
    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const-string p1, "ee.cyber.smartid.APP_KEY_STATE_FOR_KEY_ID_%1$s"

    invoke-static {p1, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public getKeyStateMetaIdByKeyStateId(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    const/4 v0, 0x1

    .line 484
    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const-string p1, "ee.cyber.smartid.APP_KEY_STATE_META_FOR_KEY_STATE_ID_%1$s"

    invoke-static {p1, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public getLastHammerTimeEventTimestamp()J
    .locals 2

    .line 500
    iget-object v0, p0, Lee/cyber/smartid/tse/SmartIdTSE$TSEResourceAccess;->a:Lee/cyber/smartid/tse/SmartIdTSE;

    invoke-static {v0}, Lee/cyber/smartid/tse/SmartIdTSE;->d(Lee/cyber/smartid/tse/SmartIdTSE;)J

    move-result-wide v0

    return-wide v0
.end method

.method public getLastPRNGTestResultStorageId()Ljava/lang/String;
    .locals 3

    const/4 v0, 0x1

    .line 569
    new-array v0, v0, [Ljava/lang/Object;

    invoke-virtual {p0}, Lee/cyber/smartid/tse/SmartIdTSE$TSEResourceAccess;->getDeviceFingerPrint()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    const-string v1, "APP_LAST_PRNG_TEST_RESULT_%1$s"

    invoke-static {v1, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getLibraryVersion()Ljava/lang/String;
    .locals 1

    .line 515
    iget-object v0, p0, Lee/cyber/smartid/tse/SmartIdTSE$TSEResourceAccess;->a:Lee/cyber/smartid/tse/SmartIdTSE;

    invoke-static {v0}, Lee/cyber/smartid/tse/SmartIdTSE;->a(Lee/cyber/smartid/tse/SmartIdTSE;)Lee/cyber/smartid/tse/inter/ExternalResourceAccess;

    move-result-object v0

    invoke-interface {v0}, Lee/cyber/smartid/tse/inter/ExternalResourceAccess;->getLibraryVersion()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getMinInteractiveRequestDelay()J
    .locals 2

    .line 495
    iget-object v0, p0, Lee/cyber/smartid/tse/SmartIdTSE$TSEResourceAccess;->a:Lee/cyber/smartid/tse/SmartIdTSE;

    invoke-static {v0}, Lee/cyber/smartid/tse/SmartIdTSE;->c(Lee/cyber/smartid/tse/SmartIdTSE;)J

    move-result-wide v0

    return-wide v0
.end method

.method public getPRNGTestLoopCount()I
    .locals 1

    .line 559
    iget-object v0, p0, Lee/cyber/smartid/tse/SmartIdTSE$TSEResourceAccess;->a:Lee/cyber/smartid/tse/SmartIdTSE;

    invoke-static {v0}, Lee/cyber/smartid/tse/SmartIdTSE;->g(Lee/cyber/smartid/tse/SmartIdTSE;)I

    move-result v0

    return v0
.end method

.method public getPRNGTestSuppressFailure()Z
    .locals 1

    .line 580
    iget-object v0, p0, Lee/cyber/smartid/tse/SmartIdTSE$TSEResourceAccess;->a:Lee/cyber/smartid/tse/SmartIdTSE;

    invoke-static {v0}, Lee/cyber/smartid/tse/SmartIdTSE;->i(Lee/cyber/smartid/tse/SmartIdTSE;)Z

    move-result v0

    return v0
.end method

.method public getPasswordStorageId()Ljava/lang/String;
    .locals 3

    const/4 v0, 0x1

    .line 535
    new-array v0, v0, [Ljava/lang/Object;

    invoke-virtual {p0}, Lee/cyber/smartid/tse/SmartIdTSE$TSEResourceAccess;->getDeviceFingerPrint()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    const-string v1, "ee.cyber.smartid.APP_PASSWORD_DATA_%1$s"

    invoke-static {v1, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getPlatform()Ljava/lang/String;
    .locals 1

    .line 510
    iget-object v0, p0, Lee/cyber/smartid/tse/SmartIdTSE$TSEResourceAccess;->a:Lee/cyber/smartid/tse/SmartIdTSE;

    invoke-static {v0}, Lee/cyber/smartid/tse/SmartIdTSE;->a(Lee/cyber/smartid/tse/SmartIdTSE;)Lee/cyber/smartid/tse/inter/ExternalResourceAccess;

    move-result-object v0

    invoke-interface {v0}, Lee/cyber/smartid/tse/inter/ExternalResourceAccess;->getPlatform()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getPreferredSZKTKPublicKey(Ljava/lang/String;)Lee/cyber/smartid/tse/dto/KTKPublicKey;
    .locals 1

    .line 553
    iget-object v0, p0, Lee/cyber/smartid/tse/SmartIdTSE$TSEResourceAccess;->a:Lee/cyber/smartid/tse/SmartIdTSE;

    invoke-static {v0, p1}, Lee/cyber/smartid/tse/SmartIdTSE;->a(Lee/cyber/smartid/tse/SmartIdTSE;Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object p1

    .line 554
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lee/cyber/smartid/tse/dto/KTKPublicKey;

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return-object p1
.end method

.method public getRefreshCloneDetectionInterval()J
    .locals 2

    .line 564
    iget-object v0, p0, Lee/cyber/smartid/tse/SmartIdTSE$TSEResourceAccess;->a:Lee/cyber/smartid/tse/SmartIdTSE;

    invoke-static {v0}, Lee/cyber/smartid/tse/SmartIdTSE;->h(Lee/cyber/smartid/tse/SmartIdTSE;)J

    move-result-wide v0

    return-wide v0
.end method

.method public isDeviceRegistrationDone()Z
    .locals 1

    .line 525
    iget-object v0, p0, Lee/cyber/smartid/tse/SmartIdTSE$TSEResourceAccess;->a:Lee/cyber/smartid/tse/SmartIdTSE;

    invoke-virtual {v0}, Lee/cyber/smartid/tse/SmartIdTSE;->isDeviceRegistrationDone()Z

    move-result v0

    return v0
.end method

.method public loadString(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .line 540
    iget-object v0, p0, Lee/cyber/smartid/tse/SmartIdTSE$TSEResourceAccess;->a:Lee/cyber/smartid/tse/SmartIdTSE;

    invoke-static {v0}, Lee/cyber/smartid/tse/SmartIdTSE;->f(Lee/cyber/smartid/tse/SmartIdTSE;)Lee/cyber/smartid/cryptolib/CryptoLib;

    move-result-object v0

    new-instance v1, Lee/cyber/smartid/tse/SmartIdTSE$TSEResourceAccess$1;

    invoke-direct {v1, p0}, Lee/cyber/smartid/tse/SmartIdTSE$TSEResourceAccess$1;-><init>(Lee/cyber/smartid/tse/SmartIdTSE$TSEResourceAccess;)V

    .line 541
    invoke-virtual {v1}, Lee/cyber/smartid/tse/SmartIdTSE$TSEResourceAccess$1;->getType()Ljava/lang/reflect/Type;

    move-result-object v1

    .line 540
    invoke-virtual {v0, p1, v1}, Lee/cyber/smartid/cryptolib/CryptoLib;->retrieveData(Ljava/lang/String;Ljava/lang/reflect/Type;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    return-object p1
.end method

.method public setHammerTimeEventTimestamp()V
    .locals 3

    .line 505
    iget-object v0, p0, Lee/cyber/smartid/tse/SmartIdTSE$TSEResourceAccess;->a:Lee/cyber/smartid/tse/SmartIdTSE;

    invoke-static {v0}, Lee/cyber/smartid/tse/SmartIdTSE;->e(Lee/cyber/smartid/tse/SmartIdTSE;)Lee/cyber/smartid/tse/inter/WallClock;

    move-result-object v1

    invoke-interface {v1}, Lee/cyber/smartid/tse/inter/WallClock;->currentTimeMillis()J

    move-result-wide v1

    invoke-static {v0, v1, v2}, Lee/cyber/smartid/tse/SmartIdTSE;->a(Lee/cyber/smartid/tse/SmartIdTSE;J)J

    return-void
.end method
