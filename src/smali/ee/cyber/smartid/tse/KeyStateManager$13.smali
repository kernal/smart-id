.class Lee/cyber/smartid/tse/KeyStateManager$13;
.super Ljava/lang/Object;
.source "KeyStateManager.java"

# interfaces
.implements Lee/cyber/smartid/tse/inter/SubmitClientSecondPartListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lee/cyber/smartid/tse/KeyStateManager;->a(Ljava/lang/String;Lee/cyber/smartid/tse/inter/TSEListener;Ljava/lang/Class;)Lee/cyber/smartid/tse/inter/TSEListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:Lee/cyber/smartid/tse/KeyStateManager;


# direct methods
.method constructor <init>(Lee/cyber/smartid/tse/KeyStateManager;Ljava/lang/String;)V
    .locals 0

    .line 1098
    iput-object p1, p0, Lee/cyber/smartid/tse/KeyStateManager$13;->b:Lee/cyber/smartid/tse/KeyStateManager;

    iput-object p2, p0, Lee/cyber/smartid/tse/KeyStateManager$13;->a:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onSubmitClientSecondPartFailed(Ljava/lang/String;Lee/cyber/smartid/tse/dto/TSEError;)V
    .locals 2

    .line 1106
    iget-object v0, p0, Lee/cyber/smartid/tse/KeyStateManager$13;->b:Lee/cyber/smartid/tse/KeyStateManager;

    invoke-static {v0}, Lee/cyber/smartid/tse/KeyStateManager;->l(Lee/cyber/smartid/tse/KeyStateManager;)Lee/cyber/smartid/tse/inter/ExternalResourceAccess;

    move-result-object v0

    iget-object v1, p0, Lee/cyber/smartid/tse/KeyStateManager$13;->a:Ljava/lang/String;

    invoke-virtual {p2, v1}, Lee/cyber/smartid/tse/dto/TSEError;->withKeyId(Ljava/lang/String;)Lee/cyber/smartid/tse/dto/TSEError;

    move-result-object p2

    invoke-interface {v0, p1, p2}, Lee/cyber/smartid/tse/inter/ExternalResourceAccess;->onSubmitClientSecondPartFailed(Ljava/lang/String;Lee/cyber/smartid/tse/dto/TSEError;)V

    return-void
.end method

.method public onSubmitClientSecondPartSuccess(Ljava/lang/String;Lee/cyber/smartid/tse/dto/jsonrpc/resp/SubmitClientSecondPartResp;)V
    .locals 1

    .line 1101
    iget-object v0, p0, Lee/cyber/smartid/tse/KeyStateManager$13;->b:Lee/cyber/smartid/tse/KeyStateManager;

    invoke-static {v0}, Lee/cyber/smartid/tse/KeyStateManager;->l(Lee/cyber/smartid/tse/KeyStateManager;)Lee/cyber/smartid/tse/inter/ExternalResourceAccess;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lee/cyber/smartid/tse/inter/ExternalResourceAccess;->onSubmitClientSecondPartSuccess(Ljava/lang/String;Lee/cyber/smartid/tse/dto/jsonrpc/resp/SubmitClientSecondPartResp;)V

    return-void
.end method
