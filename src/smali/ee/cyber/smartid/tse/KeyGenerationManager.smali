.class final Lee/cyber/smartid/tse/KeyGenerationManager;
.super Ljava/lang/Object;
.source "KeyGenerationManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lee/cyber/smartid/tse/KeyGenerationManager$KeyGenerationWorkerListener;
    }
.end annotation


# instance fields
.field private a:Lee/cyber/smartid/tse/KeyGenerationManager$KeyGenerationWorkerListener;

.field private volatile b:Ljava/util/concurrent/ExecutorService;

.field private final c:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "[",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private volatile d:I

.field private e:Lee/cyber/smartid/tse/inter/ListenerAccess;

.field private f:Ljava/lang/String;

.field private volatile g:J

.field private volatile h:I

.field private volatile i:J

.field private volatile j:[Lee/cyber/smartid/cryptolib/dto/TestResult;

.field private final k:Lee/cyber/smartid/tse/util/Log;


# direct methods
.method constructor <init>()V
    .locals 1

    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lee/cyber/smartid/tse/KeyGenerationManager;->c:Ljava/util/ArrayList;

    .line 86
    invoke-static {p0}, Lee/cyber/smartid/tse/util/Log;->getInstance(Ljava/lang/Object;)Lee/cyber/smartid/tse/util/Log;

    move-result-object v0

    iput-object v0, p0, Lee/cyber/smartid/tse/KeyGenerationManager;->k:Lee/cyber/smartid/tse/util/Log;

    return-void
.end method

.method static synthetic a(Lee/cyber/smartid/tse/KeyGenerationManager;)I
    .locals 0

    .line 37
    iget p0, p0, Lee/cyber/smartid/tse/KeyGenerationManager;->h:I

    return p0
.end method

.method static synthetic a(Lee/cyber/smartid/tse/KeyGenerationManager;Lee/cyber/smartid/tse/KeyGenerationManager$KeyGenerationWorkerListener;)Lee/cyber/smartid/tse/KeyGenerationManager$KeyGenerationWorkerListener;
    .locals 0

    .line 37
    iput-object p1, p0, Lee/cyber/smartid/tse/KeyGenerationManager;->a:Lee/cyber/smartid/tse/KeyGenerationManager$KeyGenerationWorkerListener;

    return-object p1
.end method

.method static synthetic a(Lee/cyber/smartid/tse/KeyGenerationManager;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .line 37
    iput-object p1, p0, Lee/cyber/smartid/tse/KeyGenerationManager;->f:Ljava/lang/String;

    return-object p1
.end method

.method private a()V
    .locals 1

    .line 230
    iget-object v0, p0, Lee/cyber/smartid/tse/KeyGenerationManager;->b:Ljava/util/concurrent/ExecutorService;

    if-nez v0, :cond_0

    return-void

    .line 233
    :cond_0
    iget-object v0, p0, Lee/cyber/smartid/tse/KeyGenerationManager;->b:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v0}, Ljava/util/concurrent/ExecutorService;->shutdown()V

    const/4 v0, 0x0

    .line 234
    iput-object v0, p0, Lee/cyber/smartid/tse/KeyGenerationManager;->b:Ljava/util/concurrent/ExecutorService;

    return-void
.end method

.method static synthetic a(Lee/cyber/smartid/tse/KeyGenerationManager;Lee/cyber/smartid/tse/inter/WallClock;Lee/cyber/smartid/tse/inter/ResourceAccess;[Ljava/lang/String;Ljava/lang/Exception;[Lee/cyber/smartid/cryptolib/dto/TestResult;)V
    .locals 0

    .line 37
    invoke-direct/range {p0 .. p5}, Lee/cyber/smartid/tse/KeyGenerationManager;->a(Lee/cyber/smartid/tse/inter/WallClock;Lee/cyber/smartid/tse/inter/ResourceAccess;[Ljava/lang/String;Ljava/lang/Exception;[Lee/cyber/smartid/cryptolib/dto/TestResult;)V

    return-void
.end method

.method private a(Lee/cyber/smartid/tse/inter/WallClock;Lee/cyber/smartid/tse/inter/ResourceAccess;[Ljava/lang/String;Ljava/lang/Exception;[Lee/cyber/smartid/cryptolib/dto/TestResult;)V
    .locals 3

    .line 171
    iget-object v0, p0, Lee/cyber/smartid/tse/KeyGenerationManager;->c:Ljava/util/ArrayList;

    monitor-enter v0

    if-eqz p3, :cond_0

    if-nez p4, :cond_0

    .line 173
    :try_start_0
    iget-object v1, p0, Lee/cyber/smartid/tse/KeyGenerationManager;->c:Ljava/util/ArrayList;

    invoke-virtual {v1, p3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 174
    iget-object p3, p0, Lee/cyber/smartid/tse/KeyGenerationManager;->k:Lee/cyber/smartid/tse/util/Log;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onGenerationResult, keys size: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lee/cyber/smartid/tse/KeyGenerationManager;->c:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p3, v1}, Lee/cyber/smartid/tse/util/Log;->d(Ljava/lang/String;)V

    .line 177
    iget-object p3, p0, Lee/cyber/smartid/tse/KeyGenerationManager;->e:Lee/cyber/smartid/tse/inter/ListenerAccess;

    new-instance v1, Lee/cyber/smartid/tse/KeyGenerationManager$3;

    invoke-direct {v1, p0}, Lee/cyber/smartid/tse/KeyGenerationManager$3;-><init>(Lee/cyber/smartid/tse/KeyGenerationManager;)V

    invoke-interface {p3, v1}, Lee/cyber/smartid/tse/inter/ListenerAccess;->notifyUI(Ljava/lang/Runnable;)V

    :cond_0
    if-eqz p5, :cond_1

    .line 187
    iget-object p3, p0, Lee/cyber/smartid/tse/KeyGenerationManager;->j:[Lee/cyber/smartid/cryptolib/dto/TestResult;

    if-nez p3, :cond_1

    .line 188
    iget-object p3, p0, Lee/cyber/smartid/tse/KeyGenerationManager;->k:Lee/cyber/smartid/tse/util/Log;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onGenerationResult, got test results: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {p5}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p3, v1}, Lee/cyber/smartid/tse/util/Log;->d(Ljava/lang/String;)V

    .line 189
    iput-object p5, p0, Lee/cyber/smartid/tse/KeyGenerationManager;->j:[Lee/cyber/smartid/cryptolib/dto/TestResult;

    .line 193
    :cond_1
    monitor-enter p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 194
    :try_start_1
    iget-object p3, p0, Lee/cyber/smartid/tse/KeyGenerationManager;->b:Ljava/util/concurrent/ExecutorService;

    if-eqz p3, :cond_5

    iget-object p3, p0, Lee/cyber/smartid/tse/KeyGenerationManager;->c:Ljava/util/ArrayList;

    invoke-virtual {p3}, Ljava/util/ArrayList;->size()I

    move-result p3

    iget p5, p0, Lee/cyber/smartid/tse/KeyGenerationManager;->d:I

    if-lt p3, p5, :cond_2

    iget-object p3, p0, Lee/cyber/smartid/tse/KeyGenerationManager;->j:[Lee/cyber/smartid/cryptolib/dto/TestResult;

    if-nez p3, :cond_3

    iget p3, p0, Lee/cyber/smartid/tse/KeyGenerationManager;->h:I

    const/4 p5, 0x1

    if-lt p3, p5, :cond_3

    :cond_2
    if-eqz p4, :cond_5

    :cond_3
    if-nez p4, :cond_4

    .line 199
    invoke-interface {p2}, Lee/cyber/smartid/tse/inter/ResourceAccess;->getPRNGTestSuppressFailure()Z

    move-result p3

    if-nez p3, :cond_4

    iget-object p3, p0, Lee/cyber/smartid/tse/KeyGenerationManager;->j:[Lee/cyber/smartid/cryptolib/dto/TestResult;

    invoke-static {p3}, Lee/cyber/smartid/tse/util/Util;->isOneOrMorePRNGTestsFailed([Lee/cyber/smartid/cryptolib/dto/TestResult;)Z

    move-result p3

    if-eqz p3, :cond_4

    .line 200
    new-instance p4, Lee/cyber/smartid/tse/dto/PRNGTestsFailedException;

    invoke-interface {p2}, Lee/cyber/smartid/tse/inter/ResourceAccess;->getApplicationContext()Landroid/content/Context;

    move-result-object p2

    sget p3, Lee/cyber/smartid/tse/R$string;->err_prng_tests_failed:I

    invoke-virtual {p2, p3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p2

    iget-object p3, p0, Lee/cyber/smartid/tse/KeyGenerationManager;->j:[Lee/cyber/smartid/cryptolib/dto/TestResult;

    invoke-direct {p4, p2, p3}, Lee/cyber/smartid/tse/dto/PRNGTestsFailedException;-><init>(Ljava/lang/String;[Lee/cyber/smartid/cryptolib/dto/TestResult;)V

    .line 203
    :cond_4
    invoke-direct {p0, p1, p4}, Lee/cyber/smartid/tse/KeyGenerationManager;->a(Lee/cyber/smartid/tse/inter/WallClock;Ljava/lang/Exception;)V

    .line 205
    invoke-direct {p0}, Lee/cyber/smartid/tse/KeyGenerationManager;->a()V

    .line 207
    :cond_5
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 208
    :try_start_2
    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    return-void

    :catchall_0
    move-exception p1

    .line 207
    :try_start_3
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw p1

    :catchall_1
    move-exception p1

    .line 208
    monitor-exit v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw p1
.end method

.method private a(Lee/cyber/smartid/tse/inter/WallClock;Ljava/lang/Exception;)V
    .locals 2

    .line 214
    invoke-interface {p1}, Lee/cyber/smartid/tse/inter/WallClock;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lee/cyber/smartid/tse/KeyGenerationManager;->i:J

    .line 215
    iget-object p1, p0, Lee/cyber/smartid/tse/KeyGenerationManager;->e:Lee/cyber/smartid/tse/inter/ListenerAccess;

    new-instance v0, Lee/cyber/smartid/tse/KeyGenerationManager$4;

    invoke-direct {v0, p0, p2}, Lee/cyber/smartid/tse/KeyGenerationManager$4;-><init>(Lee/cyber/smartid/tse/KeyGenerationManager;Ljava/lang/Exception;)V

    invoke-interface {p1, v0}, Lee/cyber/smartid/tse/inter/ListenerAccess;->notifyUI(Ljava/lang/Runnable;)V

    return-void
.end method

.method static synthetic b(Lee/cyber/smartid/tse/KeyGenerationManager;)Lee/cyber/smartid/tse/util/Log;
    .locals 0

    .line 37
    iget-object p0, p0, Lee/cyber/smartid/tse/KeyGenerationManager;->k:Lee/cyber/smartid/tse/util/Log;

    return-object p0
.end method

.method static synthetic c(Lee/cyber/smartid/tse/KeyGenerationManager;)Lee/cyber/smartid/tse/KeyGenerationManager$KeyGenerationWorkerListener;
    .locals 0

    .line 37
    iget-object p0, p0, Lee/cyber/smartid/tse/KeyGenerationManager;->a:Lee/cyber/smartid/tse/KeyGenerationManager$KeyGenerationWorkerListener;

    return-object p0
.end method

.method static synthetic d(Lee/cyber/smartid/tse/KeyGenerationManager;)Ljava/lang/String;
    .locals 0

    .line 37
    iget-object p0, p0, Lee/cyber/smartid/tse/KeyGenerationManager;->f:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic e(Lee/cyber/smartid/tse/KeyGenerationManager;)Ljava/util/ArrayList;
    .locals 0

    .line 37
    iget-object p0, p0, Lee/cyber/smartid/tse/KeyGenerationManager;->c:Ljava/util/ArrayList;

    return-object p0
.end method

.method static synthetic f(Lee/cyber/smartid/tse/KeyGenerationManager;)I
    .locals 0

    .line 37
    iget p0, p0, Lee/cyber/smartid/tse/KeyGenerationManager;->d:I

    return p0
.end method

.method static synthetic g(Lee/cyber/smartid/tse/KeyGenerationManager;)J
    .locals 2

    .line 37
    iget-wide v0, p0, Lee/cyber/smartid/tse/KeyGenerationManager;->i:J

    return-wide v0
.end method

.method static synthetic h(Lee/cyber/smartid/tse/KeyGenerationManager;)J
    .locals 2

    .line 37
    iget-wide v0, p0, Lee/cyber/smartid/tse/KeyGenerationManager;->g:J

    return-wide v0
.end method

.method static synthetic i(Lee/cyber/smartid/tse/KeyGenerationManager;)[Lee/cyber/smartid/cryptolib/dto/TestResult;
    .locals 0

    .line 37
    iget-object p0, p0, Lee/cyber/smartid/tse/KeyGenerationManager;->j:[Lee/cyber/smartid/cryptolib/dto/TestResult;

    return-object p0
.end method


# virtual methods
.method public generateKeys(Ljava/lang/String;IILjava/math/BigInteger;Lee/cyber/smartid/cryptolib/inter/KeyGenerationOp;Lee/cyber/smartid/cryptolib/inter/RandomGenerationOp;Lee/cyber/smartid/tse/inter/WallClock;Lee/cyber/smartid/tse/inter/ListenerAccess;Lee/cyber/smartid/tse/inter/ResourceAccess;Lee/cyber/smartid/tse/KeyGenerationManager$KeyGenerationWorkerListener;)V
    .locals 14

    move-object v7, p0

    move/from16 v8, p2

    move-object/from16 v9, p7

    move-object v0, p1

    .line 111
    iput-object v0, v7, Lee/cyber/smartid/tse/KeyGenerationManager;->f:Ljava/lang/String;

    move-object/from16 v0, p10

    .line 112
    iput-object v0, v7, Lee/cyber/smartid/tse/KeyGenerationManager;->a:Lee/cyber/smartid/tse/KeyGenerationManager$KeyGenerationWorkerListener;

    const/4 v0, 0x0

    .line 113
    invoke-static {v8, v0}, Ljava/lang/Math;->max(II)I

    move-result v1

    iput v1, v7, Lee/cyber/smartid/tse/KeyGenerationManager;->d:I

    .line 114
    invoke-interface/range {p7 .. p7}, Lee/cyber/smartid/tse/inter/WallClock;->currentTimeMillis()J

    move-result-wide v1

    iput-wide v1, v7, Lee/cyber/smartid/tse/KeyGenerationManager;->g:J

    .line 115
    invoke-interface/range {p9 .. p9}, Lee/cyber/smartid/tse/inter/ResourceAccess;->getPRNGTestLoopCount()I

    move-result v1

    iput v1, v7, Lee/cyber/smartid/tse/KeyGenerationManager;->h:I

    move-object/from16 v1, p8

    .line 116
    iput-object v1, v7, Lee/cyber/smartid/tse/KeyGenerationManager;->e:Lee/cyber/smartid/tse/inter/ListenerAccess;

    if-gtz v8, :cond_0

    .line 117
    iget v1, v7, Lee/cyber/smartid/tse/KeyGenerationManager;->h:I

    if-gtz v1, :cond_0

    const/4 v0, 0x0

    .line 118
    invoke-direct {p0, v9, v0}, Lee/cyber/smartid/tse/KeyGenerationManager;->a(Lee/cyber/smartid/tse/inter/WallClock;Ljava/lang/Exception;)V

    return-void

    .line 122
    :cond_0
    iget v1, v7, Lee/cyber/smartid/tse/KeyGenerationManager;->h:I

    if-lez v1, :cond_1

    const/4 v1, 0x1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v1, v8

    int-to-double v1, v1

    const-wide/high16 v3, 0x4010000000000000L    # 4.0

    .line 123
    invoke-static {v1, v2, v3, v4}, Ljava/lang/Math;->min(DD)D

    move-result-wide v1

    double-to-int v10, v1

    .line 124
    invoke-static {v10}, Ljava/util/concurrent/Executors;->newFixedThreadPool(I)Ljava/util/concurrent/ExecutorService;

    move-result-object v1

    iput-object v1, v7, Lee/cyber/smartid/tse/KeyGenerationManager;->b:Ljava/util/concurrent/ExecutorService;

    const/4 v11, 0x0

    :goto_1
    if-ge v11, v8, :cond_2

    .line 127
    iget-object v12, v7, Lee/cyber/smartid/tse/KeyGenerationManager;->b:Ljava/util/concurrent/ExecutorService;

    new-instance v13, Lee/cyber/smartid/tse/KeyGenerationManager$1;

    move-object v0, v13

    move-object v1, p0

    move-object/from16 v2, p5

    move/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p7

    move-object/from16 v6, p9

    invoke-direct/range {v0 .. v6}, Lee/cyber/smartid/tse/KeyGenerationManager$1;-><init>(Lee/cyber/smartid/tse/KeyGenerationManager;Lee/cyber/smartid/cryptolib/inter/KeyGenerationOp;ILjava/math/BigInteger;Lee/cyber/smartid/tse/inter/WallClock;Lee/cyber/smartid/tse/inter/ResourceAccess;)V

    invoke-interface {v12, v13}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    add-int/lit8 v11, v11, 0x1

    goto :goto_1

    .line 142
    :cond_2
    iget v0, v7, Lee/cyber/smartid/tse/KeyGenerationManager;->h:I

    if-lez v0, :cond_3

    .line 143
    iget-object v0, v7, Lee/cyber/smartid/tse/KeyGenerationManager;->b:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lee/cyber/smartid/tse/KeyGenerationManager$2;

    move-object/from16 v2, p6

    move-object/from16 v3, p9

    invoke-direct {v1, p0, v2, v9, v3}, Lee/cyber/smartid/tse/KeyGenerationManager$2;-><init>(Lee/cyber/smartid/tse/KeyGenerationManager;Lee/cyber/smartid/cryptolib/inter/RandomGenerationOp;Lee/cyber/smartid/tse/inter/WallClock;Lee/cyber/smartid/tse/inter/ResourceAccess;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    .line 167
    :cond_3
    iget-object v0, v7, Lee/cyber/smartid/tse/KeyGenerationManager;->k:Lee/cyber/smartid/tse/util/Log;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "generateKeys called, key count: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v2, ", prngTestsLoopCount: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v2, v7, Lee/cyber/smartid/tse/KeyGenerationManager;->h:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v2, ", worker count: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lee/cyber/smartid/tse/util/Log;->d(Ljava/lang/String;)V

    return-void
.end method
