.class Lee/cyber/smartid/tse/KeyStateManager$16$1;
.super Ljava/lang/Object;
.source "KeyStateManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lee/cyber/smartid/tse/KeyStateManager$16;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lee/cyber/smartid/tse/KeyStateManager$16;


# direct methods
.method constructor <init>(Lee/cyber/smartid/tse/KeyStateManager$16;)V
    .locals 0

    .line 1368
    iput-object p1, p0, Lee/cyber/smartid/tse/KeyStateManager$16$1;->a:Lee/cyber/smartid/tse/KeyStateManager$16;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .line 1372
    iget-object v0, p0, Lee/cyber/smartid/tse/KeyStateManager$16$1;->a:Lee/cyber/smartid/tse/KeyStateManager$16;

    iget-object v0, v0, Lee/cyber/smartid/tse/KeyStateManager$16;->d:Lee/cyber/smartid/tse/KeyStateManager;

    iget-object v1, p0, Lee/cyber/smartid/tse/KeyStateManager$16$1;->a:Lee/cyber/smartid/tse/KeyStateManager$16;

    iget-object v1, v1, Lee/cyber/smartid/tse/KeyStateManager$16;->b:Lee/cyber/smartid/tse/inter/TSEListener;

    iget-object v2, p0, Lee/cyber/smartid/tse/KeyStateManager$16$1;->a:Lee/cyber/smartid/tse/KeyStateManager$16;

    iget-object v2, v2, Lee/cyber/smartid/tse/KeyStateManager$16;->c:Ljava/lang/String;

    iget-object v3, p0, Lee/cyber/smartid/tse/KeyStateManager$16$1;->a:Lee/cyber/smartid/tse/KeyStateManager$16;

    iget-object v3, v3, Lee/cyber/smartid/tse/KeyStateManager$16;->d:Lee/cyber/smartid/tse/KeyStateManager;

    invoke-static {v3}, Lee/cyber/smartid/tse/KeyStateManager;->i(Lee/cyber/smartid/tse/KeyStateManager;)Lee/cyber/smartid/tse/inter/WallClock;

    move-result-object v3

    iget-object v4, p0, Lee/cyber/smartid/tse/KeyStateManager$16$1;->a:Lee/cyber/smartid/tse/KeyStateManager$16;

    iget-object v4, v4, Lee/cyber/smartid/tse/KeyStateManager$16;->d:Lee/cyber/smartid/tse/KeyStateManager;

    invoke-static {v4}, Lee/cyber/smartid/tse/KeyStateManager;->j(Lee/cyber/smartid/tse/KeyStateManager;)Lee/cyber/smartid/tse/inter/ResourceAccess;

    move-result-object v4

    invoke-interface {v4}, Lee/cyber/smartid/tse/inter/ResourceAccess;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    sget v5, Lee/cyber/smartid/tse/R$string;->err_local_pending_state_not_found:I

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    const-wide/16 v5, 0x406

    invoke-static {v3, v5, v6, v4}, Lee/cyber/smartid/tse/dto/TSEError;->from(Lee/cyber/smartid/tse/inter/WallClock;JLjava/lang/String;)Lee/cyber/smartid/tse/dto/TSEError;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, Lee/cyber/smartid/tse/KeyStateManager;->a(Lee/cyber/smartid/tse/KeyStateManager;Lee/cyber/smartid/tse/inter/TSEListener;Ljava/lang/String;Lee/cyber/smartid/tse/dto/TSEError;)V

    return-void
.end method
