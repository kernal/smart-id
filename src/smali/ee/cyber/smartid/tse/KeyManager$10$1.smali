.class Lee/cyber/smartid/tse/KeyManager$10$1;
.super Ljava/lang/Object;
.source "KeyManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lee/cyber/smartid/tse/KeyManager$10;->a(ZLjava/lang/Exception;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Z

.field final synthetic b:Ljava/lang/Exception;

.field final synthetic c:Lee/cyber/smartid/tse/KeyManager$10;


# direct methods
.method constructor <init>(Lee/cyber/smartid/tse/KeyManager$10;ZLjava/lang/Exception;)V
    .locals 0

    .line 689
    iput-object p1, p0, Lee/cyber/smartid/tse/KeyManager$10$1;->c:Lee/cyber/smartid/tse/KeyManager$10;

    iput-boolean p2, p0, Lee/cyber/smartid/tse/KeyManager$10$1;->a:Z

    iput-object p3, p0, Lee/cyber/smartid/tse/KeyManager$10$1;->b:Ljava/lang/Exception;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .line 693
    iget-object v0, p0, Lee/cyber/smartid/tse/KeyManager$10$1;->c:Lee/cyber/smartid/tse/KeyManager$10;

    iget-object v0, v0, Lee/cyber/smartid/tse/KeyManager$10;->f:Lee/cyber/smartid/tse/KeyManager;

    invoke-static {v0}, Lee/cyber/smartid/tse/KeyManager;->a(Lee/cyber/smartid/tse/KeyManager;)Lee/cyber/smartid/tse/inter/ListenerAccess;

    move-result-object v0

    iget-object v1, p0, Lee/cyber/smartid/tse/KeyManager$10$1;->c:Lee/cyber/smartid/tse/KeyManager$10;

    iget-object v1, v1, Lee/cyber/smartid/tse/KeyManager$10;->e:Ljava/lang/String;

    const-class v2, Lee/cyber/smartid/tse/inter/EncryptKeyListener;

    const/4 v3, 0x1

    invoke-interface {v0, v1, v3, v2}, Lee/cyber/smartid/tse/inter/ListenerAccess;->getListener(Ljava/lang/String;ZLjava/lang/Class;)Lee/cyber/smartid/tse/inter/TSEListener;

    move-result-object v0

    check-cast v0, Lee/cyber/smartid/tse/inter/EncryptKeyListener;

    if-eqz v0, :cond_0

    .line 694
    iget-boolean v1, p0, Lee/cyber/smartid/tse/KeyManager$10$1;->a:Z

    if-eqz v1, :cond_0

    .line 695
    iget-object v1, p0, Lee/cyber/smartid/tse/KeyManager$10$1;->c:Lee/cyber/smartid/tse/KeyManager$10;

    iget-object v1, v1, Lee/cyber/smartid/tse/KeyManager$10;->e:Ljava/lang/String;

    new-instance v2, Lee/cyber/smartid/tse/dto/jsonrpc/resp/EncryptKeyResp;

    iget-object v3, p0, Lee/cyber/smartid/tse/KeyManager$10$1;->c:Lee/cyber/smartid/tse/KeyManager$10;

    iget-object v3, v3, Lee/cyber/smartid/tse/KeyManager$10;->e:Ljava/lang/String;

    iget-object v4, p0, Lee/cyber/smartid/tse/KeyManager$10$1;->c:Lee/cyber/smartid/tse/KeyManager$10;

    iget-object v4, v4, Lee/cyber/smartid/tse/KeyManager$10;->b:Ljava/lang/String;

    invoke-direct {v2, v3, v4}, Lee/cyber/smartid/tse/dto/jsonrpc/resp/EncryptKeyResp;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1, v2}, Lee/cyber/smartid/tse/inter/EncryptKeyListener;->onEncryptKeySuccess(Ljava/lang/String;Lee/cyber/smartid/tse/dto/jsonrpc/resp/EncryptKeyResp;)V

    goto :goto_0

    :cond_0
    if-eqz v0, :cond_1

    .line 697
    iget-object v1, p0, Lee/cyber/smartid/tse/KeyManager$10$1;->c:Lee/cyber/smartid/tse/KeyManager$10;

    iget-object v1, v1, Lee/cyber/smartid/tse/KeyManager$10;->e:Ljava/lang/String;

    iget-object v2, p0, Lee/cyber/smartid/tse/KeyManager$10$1;->c:Lee/cyber/smartid/tse/KeyManager$10;

    iget-object v2, v2, Lee/cyber/smartid/tse/KeyManager$10;->f:Lee/cyber/smartid/tse/KeyManager;

    invoke-static {v2}, Lee/cyber/smartid/tse/KeyManager;->c(Lee/cyber/smartid/tse/KeyManager;)Lee/cyber/smartid/tse/inter/ResourceAccess;

    move-result-object v2

    invoke-interface {v2}, Lee/cyber/smartid/tse/inter/ResourceAccess;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lee/cyber/smartid/tse/KeyManager$10$1;->c:Lee/cyber/smartid/tse/KeyManager$10;

    iget-object v3, v3, Lee/cyber/smartid/tse/KeyManager$10;->f:Lee/cyber/smartid/tse/KeyManager;

    invoke-static {v3}, Lee/cyber/smartid/tse/KeyManager;->d(Lee/cyber/smartid/tse/KeyManager;)Lee/cyber/smartid/tse/inter/WallClock;

    move-result-object v3

    iget-object v4, p0, Lee/cyber/smartid/tse/KeyManager$10$1;->b:Ljava/lang/Exception;

    invoke-static {v2, v3, v4}, Lee/cyber/smartid/tse/dto/TSEError;->from(Landroid/content/Context;Lee/cyber/smartid/tse/inter/WallClock;Ljava/lang/Throwable;)Lee/cyber/smartid/tse/dto/TSEError;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lee/cyber/smartid/tse/inter/EncryptKeyListener;->onEncryptKeyFailed(Ljava/lang/String;Lee/cyber/smartid/tse/dto/TSEError;)V

    :cond_1
    :goto_0
    return-void
.end method
