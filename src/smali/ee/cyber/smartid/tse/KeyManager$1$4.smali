.class Lee/cyber/smartid/tse/KeyManager$1$4;
.super Ljava/lang/Object;
.source "KeyManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lee/cyber/smartid/tse/KeyManager$1;->onGenerateKeysFailed(Ljava/lang/String;Ljava/lang/Exception;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:Ljava/lang/Exception;

.field final synthetic c:Lee/cyber/smartid/tse/KeyManager$1;


# direct methods
.method constructor <init>(Lee/cyber/smartid/tse/KeyManager$1;Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 0

    .line 315
    iput-object p1, p0, Lee/cyber/smartid/tse/KeyManager$1$4;->c:Lee/cyber/smartid/tse/KeyManager$1;

    iput-object p2, p0, Lee/cyber/smartid/tse/KeyManager$1$4;->a:Ljava/lang/String;

    iput-object p3, p0, Lee/cyber/smartid/tse/KeyManager$1$4;->b:Ljava/lang/Exception;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .line 318
    iget-object v0, p0, Lee/cyber/smartid/tse/KeyManager$1$4;->c:Lee/cyber/smartid/tse/KeyManager$1;

    iget-object v0, v0, Lee/cyber/smartid/tse/KeyManager$1;->c:Lee/cyber/smartid/tse/KeyManager;

    invoke-static {v0}, Lee/cyber/smartid/tse/KeyManager;->a(Lee/cyber/smartid/tse/KeyManager;)Lee/cyber/smartid/tse/inter/ListenerAccess;

    move-result-object v0

    iget-object v1, p0, Lee/cyber/smartid/tse/KeyManager$1$4;->a:Ljava/lang/String;

    iget-object v2, p0, Lee/cyber/smartid/tse/KeyManager$1$4;->c:Lee/cyber/smartid/tse/KeyManager$1;

    iget-object v2, v2, Lee/cyber/smartid/tse/KeyManager$1;->c:Lee/cyber/smartid/tse/KeyManager;

    invoke-static {v2}, Lee/cyber/smartid/tse/KeyManager;->a(Lee/cyber/smartid/tse/KeyManager;)Lee/cyber/smartid/tse/inter/ListenerAccess;

    move-result-object v2

    iget-object v3, p0, Lee/cyber/smartid/tse/KeyManager$1$4;->a:Ljava/lang/String;

    const-class v4, Lee/cyber/smartid/tse/inter/GenerateKeysListener;

    const/4 v5, 0x1

    invoke-interface {v2, v3, v5, v4}, Lee/cyber/smartid/tse/inter/ListenerAccess;->getListener(Ljava/lang/String;ZLjava/lang/Class;)Lee/cyber/smartid/tse/inter/TSEListener;

    move-result-object v2

    iget-object v3, p0, Lee/cyber/smartid/tse/KeyManager$1$4;->c:Lee/cyber/smartid/tse/KeyManager$1;

    iget-object v3, v3, Lee/cyber/smartid/tse/KeyManager$1;->c:Lee/cyber/smartid/tse/KeyManager;

    invoke-static {v3}, Lee/cyber/smartid/tse/KeyManager;->c(Lee/cyber/smartid/tse/KeyManager;)Lee/cyber/smartid/tse/inter/ResourceAccess;

    move-result-object v3

    invoke-interface {v3}, Lee/cyber/smartid/tse/inter/ResourceAccess;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    iget-object v4, p0, Lee/cyber/smartid/tse/KeyManager$1$4;->c:Lee/cyber/smartid/tse/KeyManager$1;

    iget-object v4, v4, Lee/cyber/smartid/tse/KeyManager$1;->b:Lee/cyber/smartid/tse/inter/WallClock;

    iget-object v5, p0, Lee/cyber/smartid/tse/KeyManager$1$4;->b:Ljava/lang/Exception;

    invoke-static {v3, v4, v5}, Lee/cyber/smartid/tse/dto/TSEError;->from(Landroid/content/Context;Lee/cyber/smartid/tse/inter/WallClock;Ljava/lang/Throwable;)Lee/cyber/smartid/tse/dto/TSEError;

    move-result-object v3

    invoke-interface {v0, v1, v2, v3}, Lee/cyber/smartid/tse/inter/ListenerAccess;->notifyError(Ljava/lang/String;Lee/cyber/smartid/tse/inter/TSEListener;Lee/cyber/smartid/tse/dto/TSEError;)V

    return-void
.end method
