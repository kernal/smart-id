.class public Lee/cyber/smartid/tse/SmartIdTSE$TSEAlarmAccess;
.super Ljava/lang/Object;
.source "SmartIdTSE.java"

# interfaces
.implements Lee/cyber/smartid/tse/inter/AlarmAccess;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lee/cyber/smartid/tse/SmartIdTSE;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "TSEAlarmAccess"
.end annotation


# instance fields
.field final synthetic a:Lee/cyber/smartid/tse/SmartIdTSE;

.field private b:Lee/cyber/smartid/tse/inter/AlarmAccess;


# direct methods
.method public constructor <init>(Lee/cyber/smartid/tse/SmartIdTSE;Lee/cyber/smartid/tse/inter/AlarmAccess;)V
    .locals 0

    .line 671
    iput-object p1, p0, Lee/cyber/smartid/tse/SmartIdTSE$TSEAlarmAccess;->a:Lee/cyber/smartid/tse/SmartIdTSE;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 672
    iput-object p2, p0, Lee/cyber/smartid/tse/SmartIdTSE$TSEAlarmAccess;->b:Lee/cyber/smartid/tse/inter/AlarmAccess;

    return-void
.end method


# virtual methods
.method public addAlarmHandler(Lee/cyber/smartid/tse/inter/AlarmAccess$AlarmHandler;)V
    .locals 1

    .line 687
    iget-object v0, p0, Lee/cyber/smartid/tse/SmartIdTSE$TSEAlarmAccess;->b:Lee/cyber/smartid/tse/inter/AlarmAccess;

    invoke-interface {v0, p1}, Lee/cyber/smartid/tse/inter/AlarmAccess;->addAlarmHandler(Lee/cyber/smartid/tse/inter/AlarmAccess$AlarmHandler;)V

    return-void
.end method

.method public clearAlarmFor(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 682
    iget-object p2, p0, Lee/cyber/smartid/tse/SmartIdTSE$TSEAlarmAccess;->b:Lee/cyber/smartid/tse/inter/AlarmAccess;

    invoke-interface {p2, p1, p1}, Lee/cyber/smartid/tse/inter/AlarmAccess;->clearAlarmFor(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public clearKeyStateSolveAlarm(Ljava/lang/String;)V
    .locals 3

    .line 714
    iget-object v0, p0, Lee/cyber/smartid/tse/SmartIdTSE$TSEAlarmAccess;->a:Lee/cyber/smartid/tse/SmartIdTSE;

    invoke-static {v0}, Lee/cyber/smartid/tse/SmartIdTSE;->k(Lee/cyber/smartid/tse/SmartIdTSE;)Lee/cyber/smartid/tse/inter/LogAccess;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "clearKeyStateSolveAlarm: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lee/cyber/smartid/tse/inter/LogAccess;->d(Ljava/lang/String;)V

    .line 715
    iget-object v0, p0, Lee/cyber/smartid/tse/SmartIdTSE$TSEAlarmAccess;->b:Lee/cyber/smartid/tse/inter/AlarmAccess;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ee.cyber.smartid.ACTION_SOLVE_KEY_STATE_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Lee/cyber/smartid/tse/inter/AlarmAccess;->clearAlarmFor(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public removeAlarmHandler(Lee/cyber/smartid/tse/inter/AlarmAccess$AlarmHandler;)V
    .locals 1

    .line 692
    iget-object v0, p0, Lee/cyber/smartid/tse/SmartIdTSE$TSEAlarmAccess;->b:Lee/cyber/smartid/tse/inter/AlarmAccess;

    invoke-interface {v0, p1}, Lee/cyber/smartid/tse/inter/AlarmAccess;->removeAlarmHandler(Lee/cyber/smartid/tse/inter/AlarmAccess$AlarmHandler;)V

    return-void
.end method

.method public scheduleAlarmFor(Ljava/lang/String;JLjava/lang/String;)V
    .locals 1

    .line 677
    iget-object v0, p0, Lee/cyber/smartid/tse/SmartIdTSE$TSEAlarmAccess;->b:Lee/cyber/smartid/tse/inter/AlarmAccess;

    invoke-interface {v0, p1, p2, p3, p4}, Lee/cyber/smartid/tse/inter/AlarmAccess;->scheduleAlarmFor(Ljava/lang/String;JLjava/lang/String;)V

    return-void
.end method

.method public setKeyStateSolveAlarm(Lee/cyber/smartid/tse/dto/KeyState;Lee/cyber/smartid/tse/dto/KeyStateMeta;)V
    .locals 6

    if-nez p1, :cond_0

    .line 737
    iget-object p1, p0, Lee/cyber/smartid/tse/SmartIdTSE$TSEAlarmAccess;->a:Lee/cyber/smartid/tse/SmartIdTSE;

    invoke-static {p1}, Lee/cyber/smartid/tse/SmartIdTSE;->k(Lee/cyber/smartid/tse/SmartIdTSE;)Lee/cyber/smartid/tse/inter/LogAccess;

    move-result-object p1

    const-string p2, "setKeyStateSolveAlarm - state NULL, aborting .."

    invoke-interface {p1, p2}, Lee/cyber/smartid/tse/inter/LogAccess;->w(Ljava/lang/String;)V

    return-void

    .line 740
    :cond_0
    iget-object v0, p0, Lee/cyber/smartid/tse/SmartIdTSE$TSEAlarmAccess;->a:Lee/cyber/smartid/tse/SmartIdTSE;

    invoke-static {v0}, Lee/cyber/smartid/tse/SmartIdTSE;->k(Lee/cyber/smartid/tse/SmartIdTSE;)Lee/cyber/smartid/tse/inter/LogAccess;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setKeyStateSolveAlarm: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lee/cyber/smartid/tse/inter/LogAccess;->d(Ljava/lang/String;)V

    .line 742
    iget-object v0, p0, Lee/cyber/smartid/tse/SmartIdTSE$TSEAlarmAccess;->a:Lee/cyber/smartid/tse/SmartIdTSE;

    invoke-static {v0}, Lee/cyber/smartid/tse/SmartIdTSE;->j(Lee/cyber/smartid/tse/SmartIdTSE;)Lee/cyber/smartid/tse/inter/ResourceAccess;

    move-result-object v0

    invoke-virtual {p2}, Lee/cyber/smartid/tse/dto/KeyStateMeta;->getCurrentRetry()I

    move-result v1

    invoke-interface {v0, v1}, Lee/cyber/smartid/tse/inter/ResourceAccess;->getAutomaticRequestRetryDelay(I)J

    move-result-wide v0

    .line 744
    invoke-virtual {p2}, Lee/cyber/smartid/tse/dto/KeyStateMeta;->isKeyLockedPermanently()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 745
    iget-object p1, p0, Lee/cyber/smartid/tse/SmartIdTSE$TSEAlarmAccess;->a:Lee/cyber/smartid/tse/SmartIdTSE;

    invoke-static {p1}, Lee/cyber/smartid/tse/SmartIdTSE;->k(Lee/cyber/smartid/tse/SmartIdTSE;)Lee/cyber/smartid/tse/inter/LogAccess;

    move-result-object p1

    const-string p2, "setKeyStateSolveAlarm - Key is permanently locked, skipping a retry .."

    invoke-interface {p1, p2}, Lee/cyber/smartid/tse/inter/LogAccess;->d(Ljava/lang/String;)V

    return-void

    .line 747
    :cond_1
    iget-object v2, p0, Lee/cyber/smartid/tse/SmartIdTSE$TSEAlarmAccess;->a:Lee/cyber/smartid/tse/SmartIdTSE;

    invoke-static {v2}, Lee/cyber/smartid/tse/SmartIdTSE;->e(Lee/cyber/smartid/tse/SmartIdTSE;)Lee/cyber/smartid/tse/inter/WallClock;

    move-result-object v2

    invoke-virtual {p2, v2}, Lee/cyber/smartid/tse/dto/KeyStateMeta;->isKeyLocked(Lee/cyber/smartid/tse/inter/WallClock;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 748
    iget-object v2, p0, Lee/cyber/smartid/tse/SmartIdTSE$TSEAlarmAccess;->a:Lee/cyber/smartid/tse/SmartIdTSE;

    invoke-static {v2}, Lee/cyber/smartid/tse/SmartIdTSE;->e(Lee/cyber/smartid/tse/SmartIdTSE;)Lee/cyber/smartid/tse/inter/WallClock;

    move-result-object v2

    invoke-virtual {p2, v2}, Lee/cyber/smartid/tse/dto/KeyStateMeta;->getKeyLockRemainingDuration(Lee/cyber/smartid/tse/inter/WallClock;)J

    move-result-wide v2

    const-wide/16 v4, -0x1

    cmp-long p2, v2, v4

    if-eqz p2, :cond_2

    cmp-long p2, v2, v0

    if-ltz p2, :cond_2

    const-wide/16 v0, 0x3e8

    add-long/2addr v0, v2

    .line 751
    iget-object p2, p0, Lee/cyber/smartid/tse/SmartIdTSE$TSEAlarmAccess;->a:Lee/cyber/smartid/tse/SmartIdTSE;

    invoke-static {p2}, Lee/cyber/smartid/tse/SmartIdTSE;->k(Lee/cyber/smartid/tse/SmartIdTSE;)Lee/cyber/smartid/tse/inter/LogAccess;

    move-result-object p2

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "setKeyStateSolveAlarm - Key is locked ("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v2, ") longer than our retry delay ("

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v2, "), updating retry delay.."

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p2, v2}, Lee/cyber/smartid/tse/inter/LogAccess;->d(Ljava/lang/String;)V

    .line 754
    :cond_2
    iget-object p2, p0, Lee/cyber/smartid/tse/SmartIdTSE$TSEAlarmAccess;->b:Lee/cyber/smartid/tse/inter/AlarmAccess;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ee.cyber.smartid.ACTION_SOLVE_KEY_STATE_"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lee/cyber/smartid/tse/dto/KeyState;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lee/cyber/smartid/tse/dto/KeyState;->getId()Ljava/lang/String;

    move-result-object p1

    invoke-interface {p2, v2, v0, v1, p1}, Lee/cyber/smartid/tse/inter/AlarmAccess;->scheduleAlarmFor(Ljava/lang/String;JLjava/lang/String;)V

    return-void
.end method

.method public setKeyStateSolveAlarm(Ljava/lang/String;J)V
    .locals 3

    .line 726
    iget-object v0, p0, Lee/cyber/smartid/tse/SmartIdTSE$TSEAlarmAccess;->b:Lee/cyber/smartid/tse/inter/AlarmAccess;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ee.cyber.smartid.ACTION_SOLVE_KEY_STATE_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p2, p3, p1}, Lee/cyber/smartid/tse/inter/AlarmAccess;->scheduleAlarmFor(Ljava/lang/String;JLjava/lang/String;)V

    return-void
.end method

.method public setRefreshCloneDetectionAlarm()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lee/cyber/smartid/cryptolib/dto/StorageException;
        }
    .end annotation

    .line 703
    iget-object v0, p0, Lee/cyber/smartid/tse/SmartIdTSE$TSEAlarmAccess;->a:Lee/cyber/smartid/tse/SmartIdTSE;

    invoke-static {v0}, Lee/cyber/smartid/tse/SmartIdTSE;->k(Lee/cyber/smartid/tse/SmartIdTSE;)Lee/cyber/smartid/tse/inter/LogAccess;

    move-result-object v0

    const-string v1, "setRefreshCloneDetectionAlarm"

    invoke-interface {v0, v1}, Lee/cyber/smartid/tse/inter/LogAccess;->d(Ljava/lang/String;)V

    .line 704
    iget-object v0, p0, Lee/cyber/smartid/tse/SmartIdTSE$TSEAlarmAccess;->b:Lee/cyber/smartid/tse/inter/AlarmAccess;

    iget-object v1, p0, Lee/cyber/smartid/tse/SmartIdTSE$TSEAlarmAccess;->a:Lee/cyber/smartid/tse/SmartIdTSE;

    invoke-static {v1}, Lee/cyber/smartid/tse/SmartIdTSE;->j(Lee/cyber/smartid/tse/SmartIdTSE;)Lee/cyber/smartid/tse/inter/ResourceAccess;

    move-result-object v1

    invoke-interface {v1}, Lee/cyber/smartid/tse/inter/ResourceAccess;->getRefreshCloneDetectionInterval()J

    move-result-wide v1

    const-string v3, "ee.cyber.smartid.ACTION_REFRESH_CLONE_DETECTION"

    const/4 v4, 0x0

    invoke-interface {v0, v3, v1, v2, v4}, Lee/cyber/smartid/tse/inter/AlarmAccess;->scheduleAlarmFor(Ljava/lang/String;JLjava/lang/String;)V

    .line 705
    iget-object v0, p0, Lee/cyber/smartid/tse/SmartIdTSE$TSEAlarmAccess;->a:Lee/cyber/smartid/tse/SmartIdTSE;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lee/cyber/smartid/tse/SmartIdTSE;->a(Lee/cyber/smartid/tse/SmartIdTSE;Z)V

    return-void
.end method
