.class Lee/cyber/smartid/tse/KeyManager$5;
.super Ljava/lang/Object;
.source "KeyManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lee/cyber/smartid/tse/KeyManager;->validatePins(Ljava/lang/String;Ljava/util/ArrayList;Lee/cyber/smartid/tse/inter/ValidatePinListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Ljava/util/ArrayList;

.field final synthetic b:Ljava/lang/String;

.field final synthetic c:Lee/cyber/smartid/tse/KeyManager;


# direct methods
.method constructor <init>(Lee/cyber/smartid/tse/KeyManager;Ljava/util/ArrayList;Ljava/lang/String;)V
    .locals 0

    .line 480
    iput-object p1, p0, Lee/cyber/smartid/tse/KeyManager$5;->c:Lee/cyber/smartid/tse/KeyManager;

    iput-object p2, p0, Lee/cyber/smartid/tse/KeyManager$5;->a:Ljava/util/ArrayList;

    iput-object p3, p0, Lee/cyber/smartid/tse/KeyManager$5;->b:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .line 483
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 485
    iget-object v1, p0, Lee/cyber/smartid/tse/KeyManager$5;->a:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lee/cyber/smartid/tse/dto/PinInfo;

    .line 487
    :try_start_0
    iget-object v3, p0, Lee/cyber/smartid/tse/KeyManager$5;->c:Lee/cyber/smartid/tse/KeyManager;

    invoke-virtual {v3, v2}, Lee/cyber/smartid/tse/KeyManager;->a(Lee/cyber/smartid/tse/dto/PinInfo;)Lee/cyber/smartid/tse/dto/PinValidationError;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 489
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    .line 493
    iget-object v2, p0, Lee/cyber/smartid/tse/KeyManager$5;->c:Lee/cyber/smartid/tse/KeyManager;

    invoke-static {v2}, Lee/cyber/smartid/tse/KeyManager;->a(Lee/cyber/smartid/tse/KeyManager;)Lee/cyber/smartid/tse/inter/ListenerAccess;

    move-result-object v2

    new-instance v3, Lee/cyber/smartid/tse/KeyManager$5$1;

    invoke-direct {v3, p0, v1}, Lee/cyber/smartid/tse/KeyManager$5$1;-><init>(Lee/cyber/smartid/tse/KeyManager$5;Ljava/lang/Exception;)V

    invoke-interface {v2, v3}, Lee/cyber/smartid/tse/inter/ListenerAccess;->notifyUI(Ljava/lang/Runnable;)V

    .line 506
    :cond_1
    iget-object v1, p0, Lee/cyber/smartid/tse/KeyManager$5;->c:Lee/cyber/smartid/tse/KeyManager;

    invoke-static {v1}, Lee/cyber/smartid/tse/KeyManager;->a(Lee/cyber/smartid/tse/KeyManager;)Lee/cyber/smartid/tse/inter/ListenerAccess;

    move-result-object v1

    new-instance v2, Lee/cyber/smartid/tse/KeyManager$5$2;

    invoke-direct {v2, p0, v0}, Lee/cyber/smartid/tse/KeyManager$5$2;-><init>(Lee/cyber/smartid/tse/KeyManager$5;Ljava/util/ArrayList;)V

    invoke-interface {v1, v2}, Lee/cyber/smartid/tse/inter/ListenerAccess;->notifyUI(Ljava/lang/Runnable;)V

    return-void
.end method
