.class Lee/cyber/smartid/tse/KeyManager$14;
.super Ljava/lang/Object;
.source "KeyManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lee/cyber/smartid/tse/KeyManager;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/tse/dto/jsonrpc/KeyData;Lee/cyber/smartid/tse/inter/InitializeKeyAndKeyStatesListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:Lee/cyber/smartid/tse/dto/jsonrpc/KeyData;

.field final synthetic c:Ljava/lang/String;

.field final synthetic d:Ljava/lang/String;

.field final synthetic e:Ljava/lang/String;

.field final synthetic f:Ljava/lang/String;

.field final synthetic g:Lee/cyber/smartid/tse/KeyManager;


# direct methods
.method constructor <init>(Lee/cyber/smartid/tse/KeyManager;Ljava/lang/String;Lee/cyber/smartid/tse/dto/jsonrpc/KeyData;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 1100
    iput-object p1, p0, Lee/cyber/smartid/tse/KeyManager$14;->g:Lee/cyber/smartid/tse/KeyManager;

    iput-object p2, p0, Lee/cyber/smartid/tse/KeyManager$14;->a:Ljava/lang/String;

    iput-object p3, p0, Lee/cyber/smartid/tse/KeyManager$14;->b:Lee/cyber/smartid/tse/dto/jsonrpc/KeyData;

    iput-object p4, p0, Lee/cyber/smartid/tse/KeyManager$14;->c:Ljava/lang/String;

    iput-object p5, p0, Lee/cyber/smartid/tse/KeyManager$14;->d:Ljava/lang/String;

    iput-object p6, p0, Lee/cyber/smartid/tse/KeyManager$14;->e:Ljava/lang/String;

    iput-object p7, p0, Lee/cyber/smartid/tse/KeyManager$14;->f:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 11

    .line 1105
    iget-object v0, p0, Lee/cyber/smartid/tse/KeyManager$14;->a:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_7

    iget-object v0, p0, Lee/cyber/smartid/tse/KeyManager$14;->b:Lee/cyber/smartid/tse/dto/jsonrpc/KeyData;

    if-eqz v0, :cond_7

    invoke-virtual {v0}, Lee/cyber/smartid/tse/dto/jsonrpc/KeyData;->getResponseData()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto/16 :goto_0

    .line 1113
    :cond_0
    iget-object v0, p0, Lee/cyber/smartid/tse/KeyManager$14;->b:Lee/cyber/smartid/tse/dto/jsonrpc/KeyData;

    invoke-virtual {v0}, Lee/cyber/smartid/tse/dto/jsonrpc/KeyData;->getServerDhMessageEncoding()Ljava/lang/String;

    move-result-object v0

    const-string v1, "JWE"

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1115
    iget-object v0, p0, Lee/cyber/smartid/tse/KeyManager$14;->g:Lee/cyber/smartid/tse/KeyManager;

    invoke-static {v0}, Lee/cyber/smartid/tse/KeyManager;->a(Lee/cyber/smartid/tse/KeyManager;)Lee/cyber/smartid/tse/inter/ListenerAccess;

    move-result-object v0

    new-instance v1, Lee/cyber/smartid/tse/KeyManager$14$2;

    invoke-direct {v1, p0}, Lee/cyber/smartid/tse/KeyManager$14$2;-><init>(Lee/cyber/smartid/tse/KeyManager$14;)V

    invoke-interface {v0, v1}, Lee/cyber/smartid/tse/inter/ListenerAccess;->notifyUI(Ljava/lang/Runnable;)V

    return-void

    .line 1122
    :cond_1
    iget-object v0, p0, Lee/cyber/smartid/tse/KeyManager$14;->b:Lee/cyber/smartid/tse/dto/jsonrpc/KeyData;

    invoke-virtual {v0}, Lee/cyber/smartid/tse/dto/jsonrpc/KeyData;->getResponseDataEncoding()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1124
    iget-object v0, p0, Lee/cyber/smartid/tse/KeyManager$14;->g:Lee/cyber/smartid/tse/KeyManager;

    invoke-static {v0}, Lee/cyber/smartid/tse/KeyManager;->a(Lee/cyber/smartid/tse/KeyManager;)Lee/cyber/smartid/tse/inter/ListenerAccess;

    move-result-object v0

    new-instance v1, Lee/cyber/smartid/tse/KeyManager$14$3;

    invoke-direct {v1, p0}, Lee/cyber/smartid/tse/KeyManager$14$3;-><init>(Lee/cyber/smartid/tse/KeyManager$14;)V

    invoke-interface {v0, v1}, Lee/cyber/smartid/tse/inter/ListenerAccess;->notifyUI(Ljava/lang/Runnable;)V

    return-void

    .line 1133
    :cond_2
    iget-object v0, p0, Lee/cyber/smartid/tse/KeyManager$14;->g:Lee/cyber/smartid/tse/KeyManager;

    iget-object v1, p0, Lee/cyber/smartid/tse/KeyManager$14;->d:Ljava/lang/String;

    invoke-static {v1}, Lee/cyber/smartid/tse/dto/EncryptKeyWrapper;->getId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lee/cyber/smartid/tse/KeyManager;->loadEncryptedKey(Ljava/lang/String;)Lee/cyber/smartid/tse/dto/EncryptKeyWrapper;

    move-result-object v0

    if-nez v0, :cond_3

    .line 1135
    iget-object v0, p0, Lee/cyber/smartid/tse/KeyManager$14;->g:Lee/cyber/smartid/tse/KeyManager;

    invoke-static {v0}, Lee/cyber/smartid/tse/KeyManager;->a(Lee/cyber/smartid/tse/KeyManager;)Lee/cyber/smartid/tse/inter/ListenerAccess;

    move-result-object v0

    new-instance v1, Lee/cyber/smartid/tse/KeyManager$14$4;

    invoke-direct {v1, p0}, Lee/cyber/smartid/tse/KeyManager$14$4;-><init>(Lee/cyber/smartid/tse/KeyManager$14;)V

    invoke-interface {v0, v1}, Lee/cyber/smartid/tse/inter/ListenerAccess;->notifyUI(Ljava/lang/Runnable;)V

    return-void

    .line 1145
    :cond_3
    iget-object v1, p0, Lee/cyber/smartid/tse/KeyManager$14;->g:Lee/cyber/smartid/tse/KeyManager;

    invoke-static {v1}, Lee/cyber/smartid/tse/KeyManager;->c(Lee/cyber/smartid/tse/KeyManager;)Lee/cyber/smartid/tse/inter/ResourceAccess;

    move-result-object v1

    invoke-virtual {v0}, Lee/cyber/smartid/tse/dto/EncryptKeyWrapper;->getKey()Lee/cyber/smartid/tse/dto/Key;

    move-result-object v2

    invoke-virtual {v2}, Lee/cyber/smartid/tse/dto/Key;->getSZId()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lee/cyber/smartid/tse/inter/ResourceAccess;->getPreferredSZKTKPublicKey(Ljava/lang/String;)Lee/cyber/smartid/tse/dto/KTKPublicKey;

    move-result-object v4

    if-nez v4, :cond_4

    .line 1147
    iget-object v0, p0, Lee/cyber/smartid/tse/KeyManager$14;->g:Lee/cyber/smartid/tse/KeyManager;

    invoke-static {v0}, Lee/cyber/smartid/tse/KeyManager;->a(Lee/cyber/smartid/tse/KeyManager;)Lee/cyber/smartid/tse/inter/ListenerAccess;

    move-result-object v0

    new-instance v1, Lee/cyber/smartid/tse/KeyManager$14$5;

    invoke-direct {v1, p0}, Lee/cyber/smartid/tse/KeyManager$14$5;-><init>(Lee/cyber/smartid/tse/KeyManager$14;)V

    invoke-interface {v0, v1}, Lee/cyber/smartid/tse/inter/ListenerAccess;->notifyUI(Ljava/lang/Runnable;)V

    return-void

    .line 1160
    :cond_4
    :try_start_0
    iget-object v1, p0, Lee/cyber/smartid/tse/KeyManager$14;->g:Lee/cyber/smartid/tse/KeyManager;

    invoke-static {v1}, Lee/cyber/smartid/tse/KeyManager;->e(Lee/cyber/smartid/tse/KeyManager;)Lee/cyber/smartid/cryptolib/inter/KeyGenerationOp;

    move-result-object v1

    invoke-virtual {v0}, Lee/cyber/smartid/tse/dto/EncryptKeyWrapper;->getDhKeyPair()Lee/cyber/smartid/cryptolib/dto/DiffieHellmanKeyPair;

    move-result-object v2

    iget-object v3, p0, Lee/cyber/smartid/tse/KeyManager$14;->g:Lee/cyber/smartid/tse/KeyManager;

    invoke-static {v3}, Lee/cyber/smartid/tse/KeyManager;->f(Lee/cyber/smartid/tse/KeyManager;)Lee/cyber/smartid/cryptolib/inter/EncodingOp;

    move-result-object v3

    iget-object v5, p0, Lee/cyber/smartid/tse/KeyManager$14;->b:Lee/cyber/smartid/tse/dto/jsonrpc/KeyData;

    invoke-virtual {v5}, Lee/cyber/smartid/tse/dto/jsonrpc/KeyData;->getServerDhPublicKey()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v3, v5}, Lee/cyber/smartid/cryptolib/inter/EncodingOp;->decodeDecimalFromBase64(Ljava/lang/String;)Ljava/math/BigInteger;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Lee/cyber/smartid/cryptolib/inter/KeyGenerationOp;->calculateConcatKDFWithSHA256(Lee/cyber/smartid/cryptolib/dto/DiffieHellmanKeyPair;Ljava/math/BigInteger;)[B

    move-result-object v1
    :try_end_0
    .catch Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException; {:try_start_0 .. :try_end_0} :catch_6

    .line 1176
    :try_start_1
    iget-object v2, p0, Lee/cyber/smartid/tse/KeyManager$14;->g:Lee/cyber/smartid/tse/KeyManager;

    invoke-static {v2}, Lee/cyber/smartid/tse/KeyManager;->g(Lee/cyber/smartid/tse/KeyManager;)Lee/cyber/smartid/cryptolib/inter/CryptoOp;

    move-result-object v5

    iget-object v2, p0, Lee/cyber/smartid/tse/KeyManager$14;->b:Lee/cyber/smartid/tse/dto/jsonrpc/KeyData;

    invoke-virtual {v2}, Lee/cyber/smartid/tse/dto/jsonrpc/KeyData;->getServerDhMessage()Ljava/lang/String;

    move-result-object v6

    const/4 v8, 0x0

    const/4 v9, 0x0

    const-string v10, "CLIENT"

    move-object v7, v1

    invoke-interface/range {v5 .. v10}, Lee/cyber/smartid/cryptolib/inter/CryptoOp;->decryptFromTEKEncryptedJWE(Ljava/lang/String;[BLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/a/a/n;

    move-result-object v7

    .line 1177
    invoke-virtual {v7}, Lcom/a/a/n;->c()Lcom/a/a/m;

    move-result-object v2

    invoke-virtual {v2}, Lcom/a/a/m;->a()Ljava/lang/String;

    move-result-object v2
    :try_end_1
    .catch Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException; {:try_start_1 .. :try_end_1} :catch_5

    .line 1189
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 1190
    iget-object v0, p0, Lee/cyber/smartid/tse/KeyManager$14;->g:Lee/cyber/smartid/tse/KeyManager;

    invoke-static {v0}, Lee/cyber/smartid/tse/KeyManager;->a(Lee/cyber/smartid/tse/KeyManager;)Lee/cyber/smartid/tse/inter/ListenerAccess;

    move-result-object v0

    new-instance v1, Lee/cyber/smartid/tse/KeyManager$14$8;

    invoke-direct {v1, p0}, Lee/cyber/smartid/tse/KeyManager$14$8;-><init>(Lee/cyber/smartid/tse/KeyManager$14;)V

    invoke-interface {v0, v1}, Lee/cyber/smartid/tse/inter/ListenerAccess;->notifyUI(Ljava/lang/Runnable;)V

    return-void

    .line 1202
    :cond_5
    :try_start_2
    iget-object v3, p0, Lee/cyber/smartid/tse/KeyManager$14;->g:Lee/cyber/smartid/tse/KeyManager;

    invoke-virtual {v0}, Lee/cyber/smartid/tse/dto/EncryptKeyWrapper;->getDhKeyPair()Lee/cyber/smartid/cryptolib/dto/DiffieHellmanKeyPair;

    move-result-object v5

    invoke-virtual {v5}, Lee/cyber/smartid/cryptolib/dto/DiffieHellmanKeyPair;->getPublicKey()Ljava/math/BigInteger;

    move-result-object v6

    iget-object v8, p0, Lee/cyber/smartid/tse/KeyManager$14;->b:Lee/cyber/smartid/tse/dto/jsonrpc/KeyData;

    move-object v5, v2

    invoke-static/range {v3 .. v8}, Lee/cyber/smartid/tse/KeyManager;->a(Lee/cyber/smartid/tse/KeyManager;Lee/cyber/smartid/tse/dto/KTKPublicKey;Ljava/lang/String;Ljava/math/BigInteger;Lcom/a/a/n;Lee/cyber/smartid/tse/dto/jsonrpc/KeyData;)Z

    move-result v3
    :try_end_2
    .catch Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException; {:try_start_2 .. :try_end_2} :catch_4

    if-nez v3, :cond_6

    .line 1214
    iget-object v0, p0, Lee/cyber/smartid/tse/KeyManager$14;->g:Lee/cyber/smartid/tse/KeyManager;

    invoke-static {v0}, Lee/cyber/smartid/tse/KeyManager;->a(Lee/cyber/smartid/tse/KeyManager;)Lee/cyber/smartid/tse/inter/ListenerAccess;

    move-result-object v0

    new-instance v1, Lee/cyber/smartid/tse/KeyManager$14$10;

    invoke-direct {v1, p0}, Lee/cyber/smartid/tse/KeyManager$14$10;-><init>(Lee/cyber/smartid/tse/KeyManager$14;)V

    invoke-interface {v0, v1}, Lee/cyber/smartid/tse/inter/ListenerAccess;->notifyUI(Ljava/lang/Runnable;)V

    return-void

    .line 1224
    :cond_6
    invoke-virtual {v0}, Lee/cyber/smartid/tse/dto/EncryptKeyWrapper;->getKey()Lee/cyber/smartid/tse/dto/Key;

    move-result-object v3

    iget-object v4, p0, Lee/cyber/smartid/tse/KeyManager$14;->g:Lee/cyber/smartid/tse/KeyManager;

    invoke-static {v4}, Lee/cyber/smartid/tse/KeyManager;->f(Lee/cyber/smartid/tse/KeyManager;)Lee/cyber/smartid/cryptolib/inter/EncodingOp;

    move-result-object v4

    invoke-interface {v4, v1}, Lee/cyber/smartid/cryptolib/inter/EncodingOp;->encodeBytesToBase64([B)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v2, v1}, Lee/cyber/smartid/tse/dto/Key;->setDHDerivedKey(Ljava/lang/String;Ljava/lang/String;)V

    .line 1225
    invoke-virtual {v0}, Lee/cyber/smartid/tse/dto/EncryptKeyWrapper;->getKey()Lee/cyber/smartid/tse/dto/Key;

    move-result-object v1

    iget-object v2, p0, Lee/cyber/smartid/tse/KeyManager$14;->b:Lee/cyber/smartid/tse/dto/jsonrpc/KeyData;

    invoke-virtual {v1, v2}, Lee/cyber/smartid/tse/dto/Key;->setServerData(Lee/cyber/smartid/tse/dto/jsonrpc/KeyData;)V

    .line 1227
    :try_start_3
    iget-object v1, p0, Lee/cyber/smartid/tse/KeyManager$14;->g:Lee/cyber/smartid/tse/KeyManager;

    iget-object v2, p0, Lee/cyber/smartid/tse/KeyManager$14;->a:Ljava/lang/String;

    invoke-virtual {v0}, Lee/cyber/smartid/tse/dto/EncryptKeyWrapper;->getKey()Lee/cyber/smartid/tse/dto/Key;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lee/cyber/smartid/tse/KeyManager;->initializeKey(Ljava/lang/String;Lee/cyber/smartid/tse/dto/Key;)V
    :try_end_3
    .catch Lee/cyber/smartid/cryptolib/dto/StorageException; {:try_start_3 .. :try_end_3} :catch_3

    .line 1241
    :try_start_4
    iget-object v1, p0, Lee/cyber/smartid/tse/KeyManager$14;->g:Lee/cyber/smartid/tse/KeyManager;

    invoke-static {v1}, Lee/cyber/smartid/tse/KeyManager;->c(Lee/cyber/smartid/tse/KeyManager;)Lee/cyber/smartid/tse/inter/ResourceAccess;

    move-result-object v1

    invoke-interface {v1}, Lee/cyber/smartid/tse/inter/ResourceAccess;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lee/cyber/smartid/tse/KeyManager$14;->a:Ljava/lang/String;

    iget-object v1, p0, Lee/cyber/smartid/tse/KeyManager$14;->b:Lee/cyber/smartid/tse/dto/jsonrpc/KeyData;

    invoke-virtual {v1}, Lee/cyber/smartid/tse/dto/jsonrpc/KeyData;->getResponseData()Ljava/lang/String;

    move-result-object v4

    iget-object v1, p0, Lee/cyber/smartid/tse/KeyManager$14;->b:Lee/cyber/smartid/tse/dto/jsonrpc/KeyData;

    invoke-virtual {v1}, Lee/cyber/smartid/tse/dto/jsonrpc/KeyData;->getResponseDataEncoding()Ljava/lang/String;

    move-result-object v5

    iget-object v1, p0, Lee/cyber/smartid/tse/KeyManager$14;->g:Lee/cyber/smartid/tse/KeyManager;

    invoke-static {v1}, Lee/cyber/smartid/tse/KeyManager;->g(Lee/cyber/smartid/tse/KeyManager;)Lee/cyber/smartid/cryptolib/inter/CryptoOp;

    move-result-object v6

    iget-object v1, p0, Lee/cyber/smartid/tse/KeyManager$14;->g:Lee/cyber/smartid/tse/KeyManager;

    invoke-static {v1}, Lee/cyber/smartid/tse/KeyManager;->h(Lee/cyber/smartid/tse/KeyManager;)Lee/cyber/smartid/tse/inter/KeyStorageAccess;

    move-result-object v7

    iget-object v1, p0, Lee/cyber/smartid/tse/KeyManager$14;->g:Lee/cyber/smartid/tse/KeyManager;

    invoke-static {v1}, Lee/cyber/smartid/tse/KeyManager;->f(Lee/cyber/smartid/tse/KeyManager;)Lee/cyber/smartid/cryptolib/inter/EncodingOp;

    move-result-object v8

    invoke-static/range {v2 .. v8}, Lee/cyber/smartid/tse/dto/jsonrpc/payload/OneTimePasswordSZPayload;->decryptAndDeserialize(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/cryptolib/inter/CryptoOp;Lee/cyber/smartid/tse/inter/KeyStorageAccess;Lee/cyber/smartid/cryptolib/inter/EncodingOp;)Lee/cyber/smartid/tse/dto/jsonrpc/payload/OneTimePasswordSZPayload;

    move-result-object v1

    invoke-virtual {v1}, Lee/cyber/smartid/tse/dto/jsonrpc/payload/OneTimePasswordSZPayload;->getOneTimePassword()Ljava/lang/String;

    move-result-object v7
    :try_end_4
    .catch Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException; {:try_start_4 .. :try_end_4} :catch_2

    .line 1254
    :try_start_5
    iget-object v2, p0, Lee/cyber/smartid/tse/KeyManager$14;->g:Lee/cyber/smartid/tse/KeyManager;

    iget-object v1, p0, Lee/cyber/smartid/tse/KeyManager$14;->g:Lee/cyber/smartid/tse/KeyManager;

    invoke-static {v1}, Lee/cyber/smartid/tse/KeyManager;->c(Lee/cyber/smartid/tse/KeyManager;)Lee/cyber/smartid/tse/inter/ResourceAccess;

    move-result-object v1

    iget-object v3, p0, Lee/cyber/smartid/tse/KeyManager$14;->a:Ljava/lang/String;

    invoke-interface {v1, v3}, Lee/cyber/smartid/tse/inter/ResourceAccess;->getKeyStateIdByKeyId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lee/cyber/smartid/tse/KeyManager$14;->a:Ljava/lang/String;

    iget-object v5, p0, Lee/cyber/smartid/tse/KeyManager$14;->e:Ljava/lang/String;

    iget-object v6, p0, Lee/cyber/smartid/tse/KeyManager$14;->f:Ljava/lang/String;

    invoke-virtual/range {v2 .. v7}, Lee/cyber/smartid/tse/KeyManager;->initializeKeyState(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_5
    .catch Lee/cyber/smartid/cryptolib/dto/StorageException; {:try_start_5 .. :try_end_5} :catch_1

    const/4 v1, 0x0

    .line 1266
    invoke-virtual {v0, v1}, Lee/cyber/smartid/tse/dto/EncryptKeyWrapper;->setDHKeyPair(Lee/cyber/smartid/cryptolib/dto/DiffieHellmanKeyPair;)V

    .line 1268
    :try_start_6
    iget-object v1, p0, Lee/cyber/smartid/tse/KeyManager$14;->g:Lee/cyber/smartid/tse/KeyManager;

    invoke-static {v1, v0}, Lee/cyber/smartid/tse/KeyManager;->b(Lee/cyber/smartid/tse/KeyManager;Lee/cyber/smartid/tse/dto/EncryptKeyWrapper;)V
    :try_end_6
    .catch Lee/cyber/smartid/cryptolib/dto/StorageException; {:try_start_6 .. :try_end_6} :catch_0

    .line 1280
    iget-object v0, p0, Lee/cyber/smartid/tse/KeyManager$14;->g:Lee/cyber/smartid/tse/KeyManager;

    invoke-static {v0}, Lee/cyber/smartid/tse/KeyManager;->a(Lee/cyber/smartid/tse/KeyManager;)Lee/cyber/smartid/tse/inter/ListenerAccess;

    move-result-object v0

    new-instance v1, Lee/cyber/smartid/tse/KeyManager$14$15;

    invoke-direct {v1, p0}, Lee/cyber/smartid/tse/KeyManager$14$15;-><init>(Lee/cyber/smartid/tse/KeyManager$14;)V

    invoke-interface {v0, v1}, Lee/cyber/smartid/tse/inter/ListenerAccess;->notifyUI(Ljava/lang/Runnable;)V

    return-void

    :catch_0
    move-exception v0

    .line 1270
    iget-object v1, p0, Lee/cyber/smartid/tse/KeyManager$14;->g:Lee/cyber/smartid/tse/KeyManager;

    invoke-static {v1}, Lee/cyber/smartid/tse/KeyManager;->a(Lee/cyber/smartid/tse/KeyManager;)Lee/cyber/smartid/tse/inter/ListenerAccess;

    move-result-object v1

    new-instance v2, Lee/cyber/smartid/tse/KeyManager$14$14;

    invoke-direct {v2, p0, v0}, Lee/cyber/smartid/tse/KeyManager$14$14;-><init>(Lee/cyber/smartid/tse/KeyManager$14;Lee/cyber/smartid/cryptolib/dto/StorageException;)V

    invoke-interface {v1, v2}, Lee/cyber/smartid/tse/inter/ListenerAccess;->notifyUI(Ljava/lang/Runnable;)V

    return-void

    :catch_1
    move-exception v0

    .line 1256
    iget-object v1, p0, Lee/cyber/smartid/tse/KeyManager$14;->g:Lee/cyber/smartid/tse/KeyManager;

    invoke-static {v1}, Lee/cyber/smartid/tse/KeyManager;->a(Lee/cyber/smartid/tse/KeyManager;)Lee/cyber/smartid/tse/inter/ListenerAccess;

    move-result-object v1

    new-instance v2, Lee/cyber/smartid/tse/KeyManager$14$13;

    invoke-direct {v2, p0, v0}, Lee/cyber/smartid/tse/KeyManager$14$13;-><init>(Lee/cyber/smartid/tse/KeyManager$14;Lee/cyber/smartid/cryptolib/dto/StorageException;)V

    invoke-interface {v1, v2}, Lee/cyber/smartid/tse/inter/ListenerAccess;->notifyUI(Ljava/lang/Runnable;)V

    return-void

    :catch_2
    move-exception v0

    .line 1243
    iget-object v1, p0, Lee/cyber/smartid/tse/KeyManager$14;->g:Lee/cyber/smartid/tse/KeyManager;

    invoke-static {v1}, Lee/cyber/smartid/tse/KeyManager;->a(Lee/cyber/smartid/tse/KeyManager;)Lee/cyber/smartid/tse/inter/ListenerAccess;

    move-result-object v1

    new-instance v2, Lee/cyber/smartid/tse/KeyManager$14$12;

    invoke-direct {v2, p0, v0}, Lee/cyber/smartid/tse/KeyManager$14$12;-><init>(Lee/cyber/smartid/tse/KeyManager$14;Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;)V

    invoke-interface {v1, v2}, Lee/cyber/smartid/tse/inter/ListenerAccess;->notifyUI(Ljava/lang/Runnable;)V

    return-void

    :catch_3
    move-exception v0

    .line 1229
    iget-object v1, p0, Lee/cyber/smartid/tse/KeyManager$14;->g:Lee/cyber/smartid/tse/KeyManager;

    invoke-static {v1}, Lee/cyber/smartid/tse/KeyManager;->a(Lee/cyber/smartid/tse/KeyManager;)Lee/cyber/smartid/tse/inter/ListenerAccess;

    move-result-object v1

    new-instance v2, Lee/cyber/smartid/tse/KeyManager$14$11;

    invoke-direct {v2, p0, v0}, Lee/cyber/smartid/tse/KeyManager$14$11;-><init>(Lee/cyber/smartid/tse/KeyManager$14;Lee/cyber/smartid/cryptolib/dto/StorageException;)V

    invoke-interface {v1, v2}, Lee/cyber/smartid/tse/inter/ListenerAccess;->notifyUI(Ljava/lang/Runnable;)V

    return-void

    .line 1204
    :catch_4
    iget-object v0, p0, Lee/cyber/smartid/tse/KeyManager$14;->g:Lee/cyber/smartid/tse/KeyManager;

    invoke-static {v0}, Lee/cyber/smartid/tse/KeyManager;->a(Lee/cyber/smartid/tse/KeyManager;)Lee/cyber/smartid/tse/inter/ListenerAccess;

    move-result-object v0

    new-instance v1, Lee/cyber/smartid/tse/KeyManager$14$9;

    invoke-direct {v1, p0}, Lee/cyber/smartid/tse/KeyManager$14$9;-><init>(Lee/cyber/smartid/tse/KeyManager$14;)V

    invoke-interface {v0, v1}, Lee/cyber/smartid/tse/inter/ListenerAccess;->notifyUI(Ljava/lang/Runnable;)V

    return-void

    .line 1179
    :catch_5
    iget-object v0, p0, Lee/cyber/smartid/tse/KeyManager$14;->g:Lee/cyber/smartid/tse/KeyManager;

    invoke-static {v0}, Lee/cyber/smartid/tse/KeyManager;->a(Lee/cyber/smartid/tse/KeyManager;)Lee/cyber/smartid/tse/inter/ListenerAccess;

    move-result-object v0

    new-instance v1, Lee/cyber/smartid/tse/KeyManager$14$7;

    invoke-direct {v1, p0}, Lee/cyber/smartid/tse/KeyManager$14$7;-><init>(Lee/cyber/smartid/tse/KeyManager$14;)V

    invoke-interface {v0, v1}, Lee/cyber/smartid/tse/inter/ListenerAccess;->notifyUI(Ljava/lang/Runnable;)V

    return-void

    :catch_6
    move-exception v0

    .line 1162
    iget-object v1, p0, Lee/cyber/smartid/tse/KeyManager$14;->g:Lee/cyber/smartid/tse/KeyManager;

    invoke-static {v1}, Lee/cyber/smartid/tse/KeyManager;->a(Lee/cyber/smartid/tse/KeyManager;)Lee/cyber/smartid/tse/inter/ListenerAccess;

    move-result-object v1

    new-instance v2, Lee/cyber/smartid/tse/KeyManager$14$6;

    invoke-direct {v2, p0, v0}, Lee/cyber/smartid/tse/KeyManager$14$6;-><init>(Lee/cyber/smartid/tse/KeyManager$14;Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;)V

    invoke-interface {v1, v2}, Lee/cyber/smartid/tse/inter/ListenerAccess;->notifyUI(Ljava/lang/Runnable;)V

    return-void

    .line 1106
    :cond_7
    :goto_0
    iget-object v0, p0, Lee/cyber/smartid/tse/KeyManager$14;->g:Lee/cyber/smartid/tse/KeyManager;

    invoke-static {v0}, Lee/cyber/smartid/tse/KeyManager;->a(Lee/cyber/smartid/tse/KeyManager;)Lee/cyber/smartid/tse/inter/ListenerAccess;

    move-result-object v0

    new-instance v1, Lee/cyber/smartid/tse/KeyManager$14$1;

    invoke-direct {v1, p0}, Lee/cyber/smartid/tse/KeyManager$14$1;-><init>(Lee/cyber/smartid/tse/KeyManager$14;)V

    invoke-interface {v0, v1}, Lee/cyber/smartid/tse/inter/ListenerAccess;->notifyUI(Ljava/lang/Runnable;)V

    return-void
.end method
