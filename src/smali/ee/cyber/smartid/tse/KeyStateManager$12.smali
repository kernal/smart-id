.class Lee/cyber/smartid/tse/KeyStateManager$12;
.super Ljava/lang/Object;
.source "KeyStateManager.java"

# interfaces
.implements Lee/cyber/smartid/tse/inter/RefreshCloneDetectionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lee/cyber/smartid/tse/KeyStateManager;->b(Lee/cyber/smartid/tse/dto/ProtoKeyState;)Lee/cyber/smartid/tse/inter/TSEListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lee/cyber/smartid/tse/KeyStateManager;


# direct methods
.method constructor <init>(Lee/cyber/smartid/tse/KeyStateManager;)V
    .locals 0

    .line 1075
    iput-object p1, p0, Lee/cyber/smartid/tse/KeyStateManager$12;->a:Lee/cyber/smartid/tse/KeyStateManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onRefreshCloneDetectionFailed(Ljava/lang/String;Lee/cyber/smartid/tse/dto/TSEError;)V
    .locals 3

    .line 1083
    iget-object v0, p0, Lee/cyber/smartid/tse/KeyStateManager$12;->a:Lee/cyber/smartid/tse/KeyStateManager;

    invoke-static {v0}, Lee/cyber/smartid/tse/KeyStateManager;->b(Lee/cyber/smartid/tse/KeyStateManager;)Lee/cyber/smartid/tse/inter/LogAccess;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "updateKeyStates - onRefreshCloneDetectionFailed: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, ", error: "

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, p1}, Lee/cyber/smartid/tse/inter/LogAccess;->e(Ljava/lang/String;)V

    return-void
.end method

.method public onRefreshCloneDetectionSuccess(Ljava/lang/String;Lee/cyber/smartid/tse/dto/jsonrpc/resp/RefreshCloneDetectionResp;)V
    .locals 2

    .line 1078
    iget-object p1, p0, Lee/cyber/smartid/tse/KeyStateManager$12;->a:Lee/cyber/smartid/tse/KeyStateManager;

    invoke-static {p1}, Lee/cyber/smartid/tse/KeyStateManager;->b(Lee/cyber/smartid/tse/KeyStateManager;)Lee/cyber/smartid/tse/inter/LogAccess;

    move-result-object p1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "updateKeyStates - onRefreshCloneDetectionSuccess: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-interface {p1, p2}, Lee/cyber/smartid/tse/inter/LogAccess;->d(Ljava/lang/String;)V

    return-void
.end method
