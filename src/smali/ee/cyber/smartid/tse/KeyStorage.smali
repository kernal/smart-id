.class final Lee/cyber/smartid/tse/KeyStorage;
.super Ljava/lang/Object;
.source "KeyStorage.java"

# interfaces
.implements Lee/cyber/smartid/tse/inter/KeyStorageAccess;


# static fields
.field private static volatile a:Lee/cyber/smartid/tse/KeyStorage;


# instance fields
.field private final b:Lee/cyber/smartid/cryptolib/inter/CryptoOp;

.field private final c:Lee/cyber/smartid/cryptolib/inter/StorageOp;

.field private final d:Lee/cyber/smartid/cryptolib/inter/EncodingOp;

.field private final e:Ljava/lang/Object;

.field private final f:Lee/cyber/smartid/tse/inter/LogAccess;

.field private final g:Lee/cyber/smartid/tse/inter/ResourceAccess;

.field private final h:Lee/cyber/smartid/tse/inter/WallClock;


# direct methods
.method private constructor <init>(Lee/cyber/smartid/cryptolib/inter/CryptoOp;Lee/cyber/smartid/cryptolib/inter/StorageOp;Lee/cyber/smartid/cryptolib/inter/EncodingOp;Lee/cyber/smartid/tse/inter/WallClock;Lee/cyber/smartid/tse/inter/ResourceAccess;)V
    .locals 1

    .line 83
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 60
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lee/cyber/smartid/tse/KeyStorage;->e:Ljava/lang/Object;

    .line 64
    const-class v0, Lee/cyber/smartid/tse/KeyStorage;

    invoke-static {v0}, Lee/cyber/smartid/tse/util/Log;->getInstance(Ljava/lang/Class;)Lee/cyber/smartid/tse/util/Log;

    move-result-object v0

    iput-object v0, p0, Lee/cyber/smartid/tse/KeyStorage;->f:Lee/cyber/smartid/tse/inter/LogAccess;

    .line 84
    iput-object p1, p0, Lee/cyber/smartid/tse/KeyStorage;->b:Lee/cyber/smartid/cryptolib/inter/CryptoOp;

    .line 85
    iput-object p2, p0, Lee/cyber/smartid/tse/KeyStorage;->c:Lee/cyber/smartid/cryptolib/inter/StorageOp;

    .line 86
    iput-object p3, p0, Lee/cyber/smartid/tse/KeyStorage;->d:Lee/cyber/smartid/cryptolib/inter/EncodingOp;

    .line 87
    iput-object p4, p0, Lee/cyber/smartid/tse/KeyStorage;->h:Lee/cyber/smartid/tse/inter/WallClock;

    .line 88
    iput-object p5, p0, Lee/cyber/smartid/tse/KeyStorage;->g:Lee/cyber/smartid/tse/inter/ResourceAccess;

    return-void
.end method

.method static a(Lee/cyber/smartid/cryptolib/inter/CryptoOp;Lee/cyber/smartid/cryptolib/inter/StorageOp;Lee/cyber/smartid/cryptolib/inter/EncodingOp;Lee/cyber/smartid/tse/inter/WallClock;Lee/cyber/smartid/tse/inter/ResourceAccess;)Lee/cyber/smartid/tse/KeyStorage;
    .locals 8

    .line 102
    sget-object v0, Lee/cyber/smartid/tse/KeyStorage;->a:Lee/cyber/smartid/tse/KeyStorage;

    if-nez v0, :cond_1

    .line 103
    const-class v0, Lee/cyber/smartid/tse/KeyStorage;

    monitor-enter v0

    .line 104
    :try_start_0
    sget-object v1, Lee/cyber/smartid/tse/KeyStorage;->a:Lee/cyber/smartid/tse/KeyStorage;

    if-nez v1, :cond_0

    .line 105
    new-instance v1, Lee/cyber/smartid/tse/KeyStorage;

    move-object v2, v1

    move-object v3, p0

    move-object v4, p1

    move-object v5, p2

    move-object v6, p3

    move-object v7, p4

    invoke-direct/range {v2 .. v7}, Lee/cyber/smartid/tse/KeyStorage;-><init>(Lee/cyber/smartid/cryptolib/inter/CryptoOp;Lee/cyber/smartid/cryptolib/inter/StorageOp;Lee/cyber/smartid/cryptolib/inter/EncodingOp;Lee/cyber/smartid/tse/inter/WallClock;Lee/cyber/smartid/tse/inter/ResourceAccess;)V

    sput-object v1, Lee/cyber/smartid/tse/KeyStorage;->a:Lee/cyber/smartid/tse/KeyStorage;

    .line 107
    :cond_0
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception p0

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p0

    .line 109
    :cond_1
    :goto_0
    sget-object p0, Lee/cyber/smartid/tse/KeyStorage;->a:Lee/cyber/smartid/tse/KeyStorage;

    return-object p0
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lee/cyber/smartid/cryptolib/dto/StorageException;
        }
    .end annotation

    .line 199
    iget-object v0, p0, Lee/cyber/smartid/tse/KeyStorage;->e:Ljava/lang/Object;

    monitor-enter v0

    .line 200
    :try_start_0
    invoke-static {p5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 204
    iget-object v1, p0, Lee/cyber/smartid/tse/KeyStorage;->c:Lee/cyber/smartid/cryptolib/inter/StorageOp;

    invoke-static {p1, p2, p3, p4, p5}, Lee/cyber/smartid/tse/dto/KeyState;->forIdle(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lee/cyber/smartid/tse/dto/KeyState;

    move-result-object p2

    invoke-interface {v1, p1, p2}, Lee/cyber/smartid/cryptolib/inter/StorageOp;->storeData(Ljava/lang/String;Ljava/lang/Object;)V

    .line 205
    iget-object p2, p0, Lee/cyber/smartid/tse/KeyStorage;->c:Lee/cyber/smartid/cryptolib/inter/StorageOp;

    iget-object p3, p0, Lee/cyber/smartid/tse/KeyStorage;->g:Lee/cyber/smartid/tse/inter/ResourceAccess;

    invoke-interface {p3, p1}, Lee/cyber/smartid/tse/inter/ResourceAccess;->getKeyStateMetaIdByKeyStateId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p3

    iget-object p4, p0, Lee/cyber/smartid/tse/KeyStorage;->h:Lee/cyber/smartid/tse/inter/WallClock;

    iget-object p5, p0, Lee/cyber/smartid/tse/KeyStorage;->g:Lee/cyber/smartid/tse/inter/ResourceAccess;

    invoke-static {p4, p5, p1}, Lee/cyber/smartid/tse/dto/KeyStateMeta;->forIdle(Lee/cyber/smartid/tse/inter/WallClock;Lee/cyber/smartid/tse/inter/ResourceAccess;Ljava/lang/String;)Lee/cyber/smartid/tse/dto/KeyStateMeta;

    move-result-object p1

    invoke-interface {p2, p3, p1}, Lee/cyber/smartid/cryptolib/inter/StorageOp;->storeData(Ljava/lang/String;Ljava/lang/Object;)V

    .line 206
    monitor-exit v0

    return-void

    .line 201
    :cond_0
    new-instance p1, Lee/cyber/smartid/cryptolib/dto/StorageException;

    const/4 p2, 0x1

    const-string p3, "The oneTimePassword can\'t be null!"

    invoke-direct {p1, p2, p3}, Lee/cyber/smartid/cryptolib/dto/StorageException;-><init>(ILjava/lang/String;)V

    throw p1

    :catchall_0
    move-exception p1

    .line 206
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method


# virtual methods
.method public consumeFreshnessToken(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lee/cyber/smartid/cryptolib/dto/StorageException;
        }
    .end annotation

    .line 388
    iget-object v0, p0, Lee/cyber/smartid/tse/KeyStorage;->e:Ljava/lang/Object;

    monitor-enter v0

    .line 389
    :try_start_0
    iget-object v1, p0, Lee/cyber/smartid/tse/KeyStorage;->f:Lee/cyber/smartid/tse/inter/LogAccess;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "consumeFreshnessToken - keyStateId: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lee/cyber/smartid/tse/inter/LogAccess;->d(Ljava/lang/String;)V

    .line 390
    iget-object v1, p0, Lee/cyber/smartid/tse/KeyStorage;->c:Lee/cyber/smartid/cryptolib/inter/StorageOp;

    iget-object v2, p0, Lee/cyber/smartid/tse/KeyStorage;->g:Lee/cyber/smartid/tse/inter/ResourceAccess;

    invoke-interface {v2, p1}, Lee/cyber/smartid/tse/inter/ResourceAccess;->getFreshnessTokenId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lee/cyber/smartid/tse/KeyStorage$3;

    invoke-direct {v3, p0}, Lee/cyber/smartid/tse/KeyStorage$3;-><init>(Lee/cyber/smartid/tse/KeyStorage;)V

    .line 391
    invoke-virtual {v3}, Lee/cyber/smartid/tse/KeyStorage$3;->getType()Ljava/lang/reflect/Type;

    move-result-object v3

    .line 390
    invoke-interface {v1, v2, v3}, Lee/cyber/smartid/cryptolib/inter/StorageOp;->retrieveData(Ljava/lang/String;Ljava/lang/reflect/Type;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 392
    iget-object v2, p0, Lee/cyber/smartid/tse/KeyStorage;->c:Lee/cyber/smartid/cryptolib/inter/StorageOp;

    iget-object v3, p0, Lee/cyber/smartid/tse/KeyStorage;->g:Lee/cyber/smartid/tse/inter/ResourceAccess;

    invoke-interface {v3, p1}, Lee/cyber/smartid/tse/inter/ResourceAccess;->getFreshnessTokenId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-interface {v2, p1}, Lee/cyber/smartid/cryptolib/inter/StorageOp;->removeData(Ljava/lang/String;)V

    .line 393
    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception p1

    .line 394
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method public deleteKeyAndRelatedObjects(Ljava/lang/String;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lee/cyber/smartid/cryptolib/dto/StorageException;
        }
    .end annotation

    .line 421
    iget-object v0, p0, Lee/cyber/smartid/tse/KeyStorage;->e:Ljava/lang/Object;

    monitor-enter v0

    .line 422
    :try_start_0
    iget-object v1, p0, Lee/cyber/smartid/tse/KeyStorage;->f:Lee/cyber/smartid/tse/inter/LogAccess;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "deleteKeyAndRelatedObjects - keyId: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lee/cyber/smartid/tse/inter/LogAccess;->d(Ljava/lang/String;)V

    .line 423
    iget-object v1, p0, Lee/cyber/smartid/tse/KeyStorage;->c:Lee/cyber/smartid/cryptolib/inter/StorageOp;

    invoke-interface {v1, p1}, Lee/cyber/smartid/cryptolib/inter/StorageOp;->removeData(Ljava/lang/String;)V

    .line 424
    iget-object v1, p0, Lee/cyber/smartid/tse/KeyStorage;->c:Lee/cyber/smartid/cryptolib/inter/StorageOp;

    iget-object v2, p0, Lee/cyber/smartid/tse/KeyStorage;->g:Lee/cyber/smartid/tse/inter/ResourceAccess;

    invoke-interface {v2, p1}, Lee/cyber/smartid/tse/inter/ResourceAccess;->getKeyStateIdByKeyId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lee/cyber/smartid/cryptolib/inter/StorageOp;->removeData(Ljava/lang/String;)V

    .line 425
    iget-object v1, p0, Lee/cyber/smartid/tse/KeyStorage;->c:Lee/cyber/smartid/cryptolib/inter/StorageOp;

    iget-object v2, p0, Lee/cyber/smartid/tse/KeyStorage;->g:Lee/cyber/smartid/tse/inter/ResourceAccess;

    iget-object v3, p0, Lee/cyber/smartid/tse/KeyStorage;->g:Lee/cyber/smartid/tse/inter/ResourceAccess;

    invoke-interface {v3, p1}, Lee/cyber/smartid/tse/inter/ResourceAccess;->getKeyStateIdByKeyId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lee/cyber/smartid/tse/inter/ResourceAccess;->getKeyStateMetaIdByKeyStateId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lee/cyber/smartid/cryptolib/inter/StorageOp;->removeData(Ljava/lang/String;)V

    .line 426
    iget-object v1, p0, Lee/cyber/smartid/tse/KeyStorage;->c:Lee/cyber/smartid/cryptolib/inter/StorageOp;

    iget-object v2, p0, Lee/cyber/smartid/tse/KeyStorage;->g:Lee/cyber/smartid/tse/inter/ResourceAccess;

    iget-object v3, p0, Lee/cyber/smartid/tse/KeyStorage;->g:Lee/cyber/smartid/tse/inter/ResourceAccess;

    invoke-interface {v3, p1}, Lee/cyber/smartid/tse/inter/ResourceAccess;->getKeyStateIdByKeyId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-interface {v2, p1}, Lee/cyber/smartid/tse/inter/ResourceAccess;->getFreshnessTokenId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-interface {v1, p1}, Lee/cyber/smartid/cryptolib/inter/StorageOp;->removeData(Ljava/lang/String;)V

    .line 427
    monitor-exit v0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method public finishKeyStateOperation(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 13
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lee/cyber/smartid/cryptolib/dto/StorageException;
        }
    .end annotation

    move-object v8, p0

    .line 319
    iget-object v9, v8, Lee/cyber/smartid/tse/KeyStorage;->e:Ljava/lang/Object;

    monitor-enter v9

    .line 320
    :try_start_0
    iget-object v0, v8, Lee/cyber/smartid/tse/KeyStorage;->f:Lee/cyber/smartid/tse/inter/LogAccess;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "finishKeyStateOperation - accountUUID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-object/from16 v10, p3

    invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, ", keyType: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-object/from16 v11, p4

    invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lee/cyber/smartid/tse/inter/LogAccess;->d(Ljava/lang/String;)V

    .line 322
    invoke-virtual {p0, p1}, Lee/cyber/smartid/tse/KeyStorage;->getKeyStateById(Ljava/lang/String;)Lee/cyber/smartid/tse/dto/KeyState;

    move-result-object v0

    const/4 v12, 0x1

    if-eqz v0, :cond_5

    .line 325
    invoke-virtual {v0}, Lee/cyber/smartid/tse/dto/KeyState;->isInActiveState()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 327
    invoke-virtual {v0}, Lee/cyber/smartid/tse/dto/KeyState;->getPayload()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 329
    invoke-virtual {v0}, Lee/cyber/smartid/tse/dto/KeyState;->getPayloadEncoding()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 331
    invoke-static/range {p5 .. p5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 335
    invoke-virtual {p0, p1}, Lee/cyber/smartid/tse/KeyStorage;->getKeyStateMetaByKeyStateId(Ljava/lang/String;)Lee/cyber/smartid/tse/dto/KeyStateMeta;

    move-result-object v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    .line 343
    :try_start_1
    iget-object v0, v8, Lee/cyber/smartid/tse/KeyStorage;->g:Lee/cyber/smartid/tse/inter/ResourceAccess;

    invoke-interface {v0}, Lee/cyber/smartid/tse/inter/ResourceAccess;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iget-object v5, v8, Lee/cyber/smartid/tse/KeyStorage;->b:Lee/cyber/smartid/cryptolib/inter/CryptoOp;

    iget-object v7, v8, Lee/cyber/smartid/tse/KeyStorage;->d:Lee/cyber/smartid/cryptolib/inter/EncodingOp;

    move-object v2, p2

    move-object/from16 v3, p5

    move-object/from16 v4, p6

    move-object v6, p0

    invoke-static/range {v1 .. v7}, Lee/cyber/smartid/tse/dto/jsonrpc/payload/OneTimePasswordSZPayload;->decryptAndDeserializeOTP(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/cryptolib/inter/CryptoOp;Lee/cyber/smartid/tse/inter/KeyStorageAccess;Lee/cyber/smartid/cryptolib/inter/EncodingOp;)Ljava/lang/String;

    move-result-object v6
    :try_end_1
    .catch Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    .line 348
    :try_start_2
    invoke-direct/range {v1 .. v6}, Lee/cyber/smartid/tse/KeyStorage;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 349
    monitor-exit v9

    return-void

    :catch_0
    move-exception v0

    .line 345
    new-instance v1, Lee/cyber/smartid/cryptolib/dto/StorageException;

    invoke-virtual {v0}, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v12, v0}, Lee/cyber/smartid/cryptolib/dto/StorageException;-><init>(ILjava/lang/String;)V

    throw v1

    .line 337
    :cond_0
    new-instance v0, Lee/cyber/smartid/cryptolib/dto/StorageException;

    const-string v1, "The existing KeyStateMeta can\'t be null!"

    invoke-direct {v0, v12, v1}, Lee/cyber/smartid/cryptolib/dto/StorageException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 332
    :cond_1
    new-instance v0, Lee/cyber/smartid/cryptolib/dto/StorageException;

    const-string v1, "The refreshCloneDetectionPayload of the new KeyState can\'t be null!"

    invoke-direct {v0, v12, v1}, Lee/cyber/smartid/cryptolib/dto/StorageException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 330
    :cond_2
    new-instance v0, Lee/cyber/smartid/cryptolib/dto/StorageException;

    const-string v1, "The payload encoding of the existing KeyState can\'t be empty!"

    invoke-direct {v0, v12, v1}, Lee/cyber/smartid/cryptolib/dto/StorageException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 328
    :cond_3
    new-instance v0, Lee/cyber/smartid/cryptolib/dto/StorageException;

    const-string v1, "The payload of the existing KeyState can\'t be null!"

    invoke-direct {v0, v12, v1}, Lee/cyber/smartid/cryptolib/dto/StorageException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 326
    :cond_4
    new-instance v0, Lee/cyber/smartid/cryptolib/dto/StorageException;

    const-string v1, "The existing KeyState must be active!"

    invoke-direct {v0, v12, v1}, Lee/cyber/smartid/cryptolib/dto/StorageException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 324
    :cond_5
    new-instance v0, Lee/cyber/smartid/cryptolib/dto/StorageException;

    const-string v1, "The existing KeyState can\'t be null!"

    invoke-direct {v0, v12, v1}, Lee/cyber/smartid/cryptolib/dto/StorageException;-><init>(ILjava/lang/String;)V

    throw v0

    :catchall_0
    move-exception v0

    .line 349
    monitor-exit v9
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0
.end method

.method public getKey(Ljava/lang/String;)Lee/cyber/smartid/tse/dto/Key;
    .locals 3

    .line 413
    iget-object v0, p0, Lee/cyber/smartid/tse/KeyStorage;->e:Ljava/lang/Object;

    monitor-enter v0

    .line 414
    :try_start_0
    iget-object v1, p0, Lee/cyber/smartid/tse/KeyStorage;->c:Lee/cyber/smartid/cryptolib/inter/StorageOp;

    new-instance v2, Lee/cyber/smartid/tse/KeyStorage$5;

    invoke-direct {v2, p0}, Lee/cyber/smartid/tse/KeyStorage$5;-><init>(Lee/cyber/smartid/tse/KeyStorage;)V

    .line 415
    invoke-virtual {v2}, Lee/cyber/smartid/tse/KeyStorage$5;->getType()Ljava/lang/reflect/Type;

    move-result-object v2

    .line 414
    invoke-interface {v1, p1, v2}, Lee/cyber/smartid/cryptolib/inter/StorageOp;->retrieveData(Ljava/lang/String;Ljava/lang/reflect/Type;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lee/cyber/smartid/tse/dto/Key;

    monitor-exit v0

    return-object p1

    :catchall_0
    move-exception p1

    .line 416
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method public getKeyStateById(Ljava/lang/String;)Lee/cyber/smartid/tse/dto/KeyState;
    .locals 3

    .line 142
    iget-object v0, p0, Lee/cyber/smartid/tse/KeyStorage;->e:Ljava/lang/Object;

    monitor-enter v0

    .line 143
    :try_start_0
    iget-object v1, p0, Lee/cyber/smartid/tse/KeyStorage;->c:Lee/cyber/smartid/cryptolib/inter/StorageOp;

    new-instance v2, Lee/cyber/smartid/tse/KeyStorage$1;

    invoke-direct {v2, p0}, Lee/cyber/smartid/tse/KeyStorage$1;-><init>(Lee/cyber/smartid/tse/KeyStorage;)V

    .line 144
    invoke-virtual {v2}, Lee/cyber/smartid/tse/KeyStorage$1;->getType()Ljava/lang/reflect/Type;

    move-result-object v2

    .line 143
    invoke-interface {v1, p1, v2}, Lee/cyber/smartid/cryptolib/inter/StorageOp;->retrieveData(Ljava/lang/String;Ljava/lang/reflect/Type;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lee/cyber/smartid/tse/dto/KeyState;

    monitor-exit v0

    return-object p1

    :catchall_0
    move-exception p1

    .line 145
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method public getKeyStateByKeyId(Ljava/lang/String;)Lee/cyber/smartid/tse/dto/KeyState;
    .locals 2

    .line 132
    iget-object v0, p0, Lee/cyber/smartid/tse/KeyStorage;->e:Ljava/lang/Object;

    monitor-enter v0

    .line 133
    :try_start_0
    iget-object v1, p0, Lee/cyber/smartid/tse/KeyStorage;->g:Lee/cyber/smartid/tse/inter/ResourceAccess;

    invoke-interface {v1, p1}, Lee/cyber/smartid/tse/inter/ResourceAccess;->getKeyStateIdByKeyId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lee/cyber/smartid/tse/KeyStorage;->getKeyStateById(Ljava/lang/String;)Lee/cyber/smartid/tse/dto/KeyState;

    move-result-object p1

    monitor-exit v0

    return-object p1

    :catchall_0
    move-exception p1

    .line 134
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method public getKeyStateMetaById(Ljava/lang/String;)Lee/cyber/smartid/tse/dto/KeyStateMeta;
    .locals 3

    .line 163
    iget-object v0, p0, Lee/cyber/smartid/tse/KeyStorage;->e:Ljava/lang/Object;

    monitor-enter v0

    .line 164
    :try_start_0
    iget-object v1, p0, Lee/cyber/smartid/tse/KeyStorage;->c:Lee/cyber/smartid/cryptolib/inter/StorageOp;

    new-instance v2, Lee/cyber/smartid/tse/KeyStorage$2;

    invoke-direct {v2, p0}, Lee/cyber/smartid/tse/KeyStorage$2;-><init>(Lee/cyber/smartid/tse/KeyStorage;)V

    .line 165
    invoke-virtual {v2}, Lee/cyber/smartid/tse/KeyStorage$2;->getType()Ljava/lang/reflect/Type;

    move-result-object v2

    .line 164
    invoke-interface {v1, p1, v2}, Lee/cyber/smartid/cryptolib/inter/StorageOp;->retrieveData(Ljava/lang/String;Ljava/lang/reflect/Type;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lee/cyber/smartid/tse/dto/KeyStateMeta;

    monitor-exit v0

    return-object p1

    :catchall_0
    move-exception p1

    .line 166
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method public getKeyStateMetaByKeyStateId(Ljava/lang/String;)Lee/cyber/smartid/tse/dto/KeyStateMeta;
    .locals 2

    .line 153
    iget-object v0, p0, Lee/cyber/smartid/tse/KeyStorage;->e:Ljava/lang/Object;

    monitor-enter v0

    .line 154
    :try_start_0
    iget-object v1, p0, Lee/cyber/smartid/tse/KeyStorage;->g:Lee/cyber/smartid/tse/inter/ResourceAccess;

    invoke-interface {v1, p1}, Lee/cyber/smartid/tse/inter/ResourceAccess;->getKeyStateMetaIdByKeyStateId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lee/cyber/smartid/tse/KeyStorage;->getKeyStateMetaById(Ljava/lang/String;)Lee/cyber/smartid/tse/dto/KeyStateMeta;

    move-result-object p1

    monitor-exit v0

    return-object p1

    :catchall_0
    move-exception p1

    .line 155
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method public initializeKey(Ljava/lang/String;Lee/cyber/smartid/tse/dto/Key;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lee/cyber/smartid/cryptolib/dto/StorageException;
        }
    .end annotation

    .line 399
    iget-object v0, p0, Lee/cyber/smartid/tse/KeyStorage;->e:Ljava/lang/Object;

    monitor-enter v0

    .line 400
    :try_start_0
    iget-object v1, p0, Lee/cyber/smartid/tse/KeyStorage;->f:Lee/cyber/smartid/tse/inter/LogAccess;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "initializeKey - keyId: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lee/cyber/smartid/tse/inter/LogAccess;->d(Ljava/lang/String;)V

    .line 401
    iget-object v1, p0, Lee/cyber/smartid/tse/KeyStorage;->c:Lee/cyber/smartid/cryptolib/inter/StorageOp;

    new-instance v2, Lee/cyber/smartid/tse/KeyStorage$4;

    invoke-direct {v2, p0}, Lee/cyber/smartid/tse/KeyStorage$4;-><init>(Lee/cyber/smartid/tse/KeyStorage;)V

    .line 402
    invoke-virtual {v2}, Lee/cyber/smartid/tse/KeyStorage$4;->getType()Ljava/lang/reflect/Type;

    move-result-object v2

    .line 401
    invoke-interface {v1, p1, v2}, Lee/cyber/smartid/cryptolib/inter/StorageOp;->retrieveData(Ljava/lang/String;Ljava/lang/reflect/Type;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lee/cyber/smartid/tse/dto/Key;

    if-nez v1, :cond_0

    .line 406
    iget-object v1, p0, Lee/cyber/smartid/tse/KeyStorage;->c:Lee/cyber/smartid/cryptolib/inter/StorageOp;

    invoke-interface {v1, p1, p2}, Lee/cyber/smartid/cryptolib/inter/StorageOp;->storeData(Ljava/lang/String;Ljava/lang/Object;)V

    .line 407
    monitor-exit v0

    return-void

    .line 404
    :cond_0
    new-instance p1, Lee/cyber/smartid/cryptolib/dto/StorageException;

    const/4 p2, 0x1

    const-string v1, "There is already a Key created for this id!"

    invoke-direct {p1, p2, v1}, Lee/cyber/smartid/cryptolib/dto/StorageException;-><init>(ILjava/lang/String;)V

    throw p1

    :catchall_0
    move-exception p1

    .line 407
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method public initializeKeyState(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lee/cyber/smartid/cryptolib/dto/StorageException;
        }
    .end annotation

    .line 211
    iget-object v0, p0, Lee/cyber/smartid/tse/KeyStorage;->e:Ljava/lang/Object;

    monitor-enter v0

    .line 212
    :try_start_0
    iget-object v1, p0, Lee/cyber/smartid/tse/KeyStorage;->f:Lee/cyber/smartid/tse/inter/LogAccess;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "initializeKeyState - accountUUID: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, ", keyType: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lee/cyber/smartid/tse/inter/LogAccess;->d(Ljava/lang/String;)V

    .line 213
    invoke-static {p5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    const/4 v2, 0x1

    if-nez v1, :cond_3

    .line 217
    invoke-virtual {p0, p1}, Lee/cyber/smartid/tse/KeyStorage;->getKeyStateById(Ljava/lang/String;)Lee/cyber/smartid/tse/dto/KeyState;

    move-result-object v1

    if-nez v1, :cond_2

    .line 219
    invoke-virtual {p0, p1}, Lee/cyber/smartid/tse/KeyStorage;->getKeyStateMetaByKeyStateId(Ljava/lang/String;)Lee/cyber/smartid/tse/dto/KeyStateMeta;

    move-result-object v1

    if-nez v1, :cond_1

    .line 222
    invoke-virtual {p0, p2}, Lee/cyber/smartid/tse/KeyStorage;->getKey(Ljava/lang/String;)Lee/cyber/smartid/tse/dto/Key;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 223
    iget-object v3, p0, Lee/cyber/smartid/tse/KeyStorage;->d:Lee/cyber/smartid/cryptolib/inter/EncodingOp;

    invoke-virtual {v1, v3}, Lee/cyber/smartid/tse/dto/Key;->getDHDerivedKeyBytes(Lee/cyber/smartid/cryptolib/inter/EncodingOp;)[B

    move-result-object v1

    if-eqz v1, :cond_0

    .line 226
    invoke-direct/range {p0 .. p5}, Lee/cyber/smartid/tse/KeyStorage;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 227
    monitor-exit v0

    return-void

    .line 224
    :cond_0
    new-instance p1, Lee/cyber/smartid/cryptolib/dto/StorageException;

    const-string p2, "The Diffie-Hellman derived key material can\'t be null when calling initializeKeyState!"

    invoke-direct {p1, v2, p2}, Lee/cyber/smartid/cryptolib/dto/StorageException;-><init>(ILjava/lang/String;)V

    throw p1

    .line 220
    :cond_1
    new-instance p1, Lee/cyber/smartid/cryptolib/dto/StorageException;

    const-string p2, "There can\'t be any excising meta states when calling initializeKeyState!"

    invoke-direct {p1, v2, p2}, Lee/cyber/smartid/cryptolib/dto/StorageException;-><init>(ILjava/lang/String;)V

    throw p1

    .line 218
    :cond_2
    new-instance p1, Lee/cyber/smartid/cryptolib/dto/StorageException;

    const-string p2, "There can\'t be any excising states when calling initializeKeyState!"

    invoke-direct {p1, v2, p2}, Lee/cyber/smartid/cryptolib/dto/StorageException;-><init>(ILjava/lang/String;)V

    throw p1

    .line 214
    :cond_3
    new-instance p1, Lee/cyber/smartid/cryptolib/dto/StorageException;

    const-string p2, "The oneTimePassword can\'t be null!"

    invoke-direct {p1, v2, p2}, Lee/cyber/smartid/cryptolib/dto/StorageException;-><init>(ILjava/lang/String;)V

    throw p1

    :catchall_0
    move-exception p1

    .line 227
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method public rollbackKeyStateOperation(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lee/cyber/smartid/cryptolib/dto/StorageException;
        }
    .end annotation

    .line 354
    iget-object v0, p0, Lee/cyber/smartid/tse/KeyStorage;->e:Ljava/lang/Object;

    monitor-enter v0

    .line 355
    :try_start_0
    iget-object v1, p0, Lee/cyber/smartid/tse/KeyStorage;->f:Lee/cyber/smartid/tse/inter/LogAccess;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "rollbackKeyStateOperation - accountUUID: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, ", keyType: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lee/cyber/smartid/tse/inter/LogAccess;->d(Ljava/lang/String;)V

    .line 356
    invoke-virtual {p0, p1}, Lee/cyber/smartid/tse/KeyStorage;->getKeyStateById(Ljava/lang/String;)Lee/cyber/smartid/tse/dto/KeyState;

    move-result-object v1

    const/4 v2, 0x1

    if-eqz v1, :cond_4

    .line 359
    invoke-virtual {v1}, Lee/cyber/smartid/tse/dto/KeyState;->isInActiveState()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 361
    invoke-virtual {v1}, Lee/cyber/smartid/tse/dto/KeyState;->isRollbackAllowed()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 363
    invoke-virtual {v1}, Lee/cyber/smartid/tse/dto/KeyState;->getOneTimePassword()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 367
    invoke-virtual {p0, p1}, Lee/cyber/smartid/tse/KeyStorage;->getKeyStateMetaByKeyStateId(Ljava/lang/String;)Lee/cyber/smartid/tse/dto/KeyStateMeta;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 372
    invoke-virtual {v1}, Lee/cyber/smartid/tse/dto/KeyState;->getOneTimePassword()Ljava/lang/String;

    move-result-object v9

    move-object v4, p0

    move-object v5, p1

    move-object v6, p2

    move-object v7, p3

    move-object v8, p4

    invoke-direct/range {v4 .. v9}, Lee/cyber/smartid/tse/KeyStorage;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 373
    monitor-exit v0

    return-void

    .line 369
    :cond_0
    new-instance p1, Lee/cyber/smartid/cryptolib/dto/StorageException;

    const-string p2, "The existing KeyStateMeta can\'t be null!"

    invoke-direct {p1, v2, p2}, Lee/cyber/smartid/cryptolib/dto/StorageException;-><init>(ILjava/lang/String;)V

    throw p1

    .line 364
    :cond_1
    new-instance p1, Lee/cyber/smartid/cryptolib/dto/StorageException;

    const-string p2, "The oneTimePassword of the existing KeyState can\'t be null!"

    invoke-direct {p1, v2, p2}, Lee/cyber/smartid/cryptolib/dto/StorageException;-><init>(ILjava/lang/String;)V

    throw p1

    .line 362
    :cond_2
    new-instance p1, Lee/cyber/smartid/cryptolib/dto/StorageException;

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string p3, "The existing KeyState ("

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Lee/cyber/smartid/tse/dto/KeyState;->getHumanReadableNameForType()Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p3, ") does not support rollback!"

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, v2, p2}, Lee/cyber/smartid/cryptolib/dto/StorageException;-><init>(ILjava/lang/String;)V

    throw p1

    .line 360
    :cond_3
    new-instance p1, Lee/cyber/smartid/cryptolib/dto/StorageException;

    const-string p2, "The existing KeyState must be active!"

    invoke-direct {p1, v2, p2}, Lee/cyber/smartid/cryptolib/dto/StorageException;-><init>(ILjava/lang/String;)V

    throw p1

    .line 358
    :cond_4
    new-instance p1, Lee/cyber/smartid/cryptolib/dto/StorageException;

    const-string p2, "The existing KeyState can\'t be null!"

    invoke-direct {p1, v2, p2}, Lee/cyber/smartid/cryptolib/dto/StorageException;-><init>(ILjava/lang/String;)V

    throw p1

    :catchall_0
    move-exception p1

    .line 373
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method public startKeyStateOperation(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/support/v4/e/j;
    .locals 21
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "I",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Landroid/support/v4/e/j<",
            "Lee/cyber/smartid/tse/dto/KeyState;",
            "Lee/cyber/smartid/tse/dto/KeyStateMeta;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lee/cyber/smartid/cryptolib/dto/StorageException;
        }
    .end annotation

    move-object/from16 v1, p0

    move-object/from16 v0, p1

    move/from16 v2, p3

    .line 234
    iget-object v12, v1, Lee/cyber/smartid/tse/KeyStorage;->e:Ljava/lang/Object;

    monitor-enter v12

    .line 235
    :try_start_0
    iget-object v3, v1, Lee/cyber/smartid/tse/KeyStorage;->f:Lee/cyber/smartid/tse/inter/LogAccess;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "startKeyStateOperation - keyOperationType: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v5, ", accountUUID: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-object/from16 v5, p4

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v6, ", keyType: "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-object/from16 v6, p6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Lee/cyber/smartid/tse/inter/LogAccess;->d(Ljava/lang/String;)V

    const/4 v3, 0x1

    if-eqz v2, :cond_1b

    .line 238
    invoke-static/range {p10 .. p10}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1a

    if-ne v2, v3, :cond_1

    .line 240
    invoke-static/range {p5 .. p5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    goto :goto_0

    .line 241
    :cond_0
    new-instance v0, Lee/cyber/smartid/cryptolib/dto/StorageException;

    const-string v2, "The transactionUUID can\'t be empty if you are doing a sign operation. Are you trying to store an idle state with startKeyStateOperation?"

    invoke-direct {v0, v3, v2}, Lee/cyber/smartid/cryptolib/dto/StorageException;-><init>(ILjava/lang/String;)V

    throw v0

    :cond_1
    :goto_0
    if-ne v2, v3, :cond_3

    .line 242
    invoke-static/range {p7 .. p7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_2

    goto :goto_1

    .line 243
    :cond_2
    new-instance v0, Lee/cyber/smartid/cryptolib/dto/StorageException;

    const-string v2, "The signatureShare can\'t be empty if you are doing a sign operation. Are you trying to store an idle state with startKeyStateOperation?"

    invoke-direct {v0, v3, v2}, Lee/cyber/smartid/cryptolib/dto/StorageException;-><init>(ILjava/lang/String;)V

    throw v0

    :cond_3
    :goto_1
    if-ne v2, v3, :cond_5

    .line 244
    invoke-static/range {p8 .. p8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_4

    goto :goto_2

    .line 245
    :cond_4
    new-instance v0, Lee/cyber/smartid/cryptolib/dto/StorageException;

    const-string v2, "The digest can\'t be empty if you are doing a sign operation. Are you trying to store an idle state with startKeyStateOperation?"

    invoke-direct {v0, v3, v2}, Lee/cyber/smartid/cryptolib/dto/StorageException;-><init>(ILjava/lang/String;)V

    throw v0

    :cond_5
    :goto_2
    if-ne v2, v3, :cond_7

    .line 246
    invoke-static/range {p9 .. p9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_6

    goto :goto_3

    .line 247
    :cond_6
    new-instance v0, Lee/cyber/smartid/cryptolib/dto/StorageException;

    const-string v2, "The digestAlgorithm can\'t be empty if you are doing a sign operation. Are you trying to store an idle state with startKeyStateOperation?"

    invoke-direct {v0, v3, v2}, Lee/cyber/smartid/cryptolib/dto/StorageException;-><init>(ILjava/lang/String;)V

    throw v0

    :cond_7
    :goto_3
    const/4 v4, 0x4

    if-ne v2, v4, :cond_9

    .line 248
    invoke-static/range {p11 .. p11}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_8

    goto :goto_4

    .line 249
    :cond_8
    new-instance v0, Lee/cyber/smartid/cryptolib/dto/StorageException;

    const-string v2, "The clientShareSecondPart can\'t be empty if you are doing a submit client second part operation. Are you trying to store an idle state with startKeyStateOperation?"

    invoke-direct {v0, v3, v2}, Lee/cyber/smartid/cryptolib/dto/StorageException;-><init>(ILjava/lang/String;)V

    throw v0

    :cond_9
    :goto_4
    if-ne v2, v4, :cond_b

    .line 250
    invoke-static/range {p12 .. p12}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_a

    goto :goto_5

    .line 251
    :cond_a
    new-instance v0, Lee/cyber/smartid/cryptolib/dto/StorageException;

    const-string v2, "The clientModulus can\'t be empty if you are doing a submit client second part operation. Are you trying to store an idle state with startKeyStateOperation?"

    invoke-direct {v0, v3, v2}, Lee/cyber/smartid/cryptolib/dto/StorageException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 253
    :cond_b
    :goto_5
    iget-object v7, v1, Lee/cyber/smartid/tse/KeyStorage;->g:Lee/cyber/smartid/tse/inter/ResourceAccess;

    invoke-interface {v7, v0}, Lee/cyber/smartid/tse/inter/ResourceAccess;->getKeyStateMetaIdByKeyStateId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    .line 254
    invoke-virtual/range {p0 .. p1}, Lee/cyber/smartid/tse/KeyStorage;->getKeyStateById(Ljava/lang/String;)Lee/cyber/smartid/tse/dto/KeyState;

    move-result-object v7

    if-eqz v7, :cond_19

    .line 257
    invoke-virtual {v7}, Lee/cyber/smartid/tse/dto/KeyState;->isInActiveState()Z

    move-result v8

    if-nez v8, :cond_18

    .line 259
    invoke-virtual {v7}, Lee/cyber/smartid/tse/dto/KeyState;->getSignatureShare()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_17

    .line 261
    invoke-virtual {v7}, Lee/cyber/smartid/tse/dto/KeyState;->getNonce()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_16

    .line 263
    invoke-virtual {v7}, Lee/cyber/smartid/tse/dto/KeyState;->getClientShareSecondPart()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_15

    .line 265
    invoke-virtual {v7}, Lee/cyber/smartid/tse/dto/KeyState;->getDigest()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_14

    .line 267
    invoke-virtual {v7}, Lee/cyber/smartid/tse/dto/KeyState;->getDigestAlgorithm()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_13

    .line 269
    invoke-virtual {v7}, Lee/cyber/smartid/tse/dto/KeyState;->getClientModulus()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_12

    .line 271
    invoke-virtual {v7}, Lee/cyber/smartid/tse/dto/KeyState;->getTransactionUUID()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_11

    .line 275
    invoke-virtual/range {p0 .. p1}, Lee/cyber/smartid/tse/KeyStorage;->getKeyStateMetaByKeyStateId(Ljava/lang/String;)Lee/cyber/smartid/tse/dto/KeyStateMeta;

    move-result-object v8

    if-eqz v8, :cond_10

    move-object/from16 v8, p2

    .line 281
    invoke-virtual {v1, v8}, Lee/cyber/smartid/tse/KeyStorage;->getKey(Ljava/lang/String;)Lee/cyber/smartid/tse/dto/Key;

    move-result-object v9

    if-eqz v9, :cond_f

    .line 282
    iget-object v10, v1, Lee/cyber/smartid/tse/KeyStorage;->d:Lee/cyber/smartid/cryptolib/inter/EncodingOp;

    invoke-virtual {v9, v10}, Lee/cyber/smartid/tse/dto/Key;->getDHDerivedKeyBytes(Lee/cyber/smartid/cryptolib/inter/EncodingOp;)[B

    move-result-object v10

    if-eqz v10, :cond_f

    invoke-virtual {v9}, Lee/cyber/smartid/tse/dto/Key;->getDhDerivedKeyId()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_f

    if-ne v2, v3, :cond_c

    .line 291
    new-instance v2, Lee/cyber/smartid/tse/dto/jsonrpc/payload/SubmitSignatureSZPayload;

    const-string v16, "JWE"

    invoke-virtual {v7}, Lee/cyber/smartid/tse/dto/KeyState;->getOneTimePassword()Ljava/lang/String;

    move-result-object v19

    move-object v14, v2

    move-object/from16 v15, p7

    move-object/from16 v17, p9

    move-object/from16 v18, p8

    move-object/from16 v20, p10

    invoke-direct/range {v14 .. v20}, Lee/cyber/smartid/tse/dto/jsonrpc/payload/SubmitSignatureSZPayload;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v3, v1, Lee/cyber/smartid/tse/KeyStorage;->b:Lee/cyber/smartid/cryptolib/inter/CryptoOp;

    iget-object v4, v1, Lee/cyber/smartid/tse/KeyStorage;->d:Lee/cyber/smartid/cryptolib/inter/EncodingOp;

    invoke-virtual {v9, v4}, Lee/cyber/smartid/tse/dto/Key;->getDHDerivedKeyBytes(Lee/cyber/smartid/cryptolib/inter/EncodingOp;)[B

    move-result-object v4

    invoke-virtual {v9}, Lee/cyber/smartid/tse/dto/Key;->getDhDerivedKeyId()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v2, v3, v4, v9}, Lee/cyber/smartid/tse/dto/jsonrpc/payload/SubmitSignatureSZPayload;->serializeAndEncrypt(Lee/cyber/smartid/cryptolib/inter/CryptoOp;[BLjava/lang/String;)Ljava/lang/String;

    move-result-object v9

    const-string v10, "JWE"

    .line 292
    invoke-virtual {v7}, Lee/cyber/smartid/tse/dto/KeyState;->getOneTimePassword()Ljava/lang/String;

    move-result-object v11

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p6

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p8

    move-object/from16 v8, p9

    invoke-static/range {v2 .. v11}, Lee/cyber/smartid/tse/dto/KeyState;->forSign(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lee/cyber/smartid/tse/dto/KeyState;

    move-result-object v2

    .line 293
    iget-object v3, v1, Lee/cyber/smartid/tse/KeyStorage;->h:Lee/cyber/smartid/tse/inter/WallClock;

    iget-object v4, v1, Lee/cyber/smartid/tse/KeyStorage;->g:Lee/cyber/smartid/tse/inter/ResourceAccess;

    invoke-static {v3, v4, v0}, Lee/cyber/smartid/tse/dto/KeyStateMeta;->forActive(Lee/cyber/smartid/tse/inter/WallClock;Lee/cyber/smartid/tse/inter/ResourceAccess;Ljava/lang/String;)Lee/cyber/smartid/tse/dto/KeyStateMeta;

    move-result-object v3

    goto/16 :goto_6

    :cond_c
    const/4 v10, 0x3

    if-ne v2, v10, :cond_d

    .line 296
    new-instance v2, Lee/cyber/smartid/tse/dto/jsonrpc/payload/RefreshCloneDetectionSZPayload;

    invoke-virtual {v7}, Lee/cyber/smartid/tse/dto/KeyState;->getOneTimePassword()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v10, p10

    invoke-direct {v2, v3, v10}, Lee/cyber/smartid/tse/dto/jsonrpc/payload/RefreshCloneDetectionSZPayload;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v3, v1, Lee/cyber/smartid/tse/KeyStorage;->b:Lee/cyber/smartid/cryptolib/inter/CryptoOp;

    iget-object v4, v1, Lee/cyber/smartid/tse/KeyStorage;->d:Lee/cyber/smartid/cryptolib/inter/EncodingOp;

    invoke-virtual {v9, v4}, Lee/cyber/smartid/tse/dto/Key;->getDHDerivedKeyBytes(Lee/cyber/smartid/cryptolib/inter/EncodingOp;)[B

    move-result-object v4

    invoke-virtual {v9}, Lee/cyber/smartid/tse/dto/Key;->getDhDerivedKeyId()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v2, v3, v4, v9}, Lee/cyber/smartid/tse/dto/jsonrpc/payload/RefreshCloneDetectionSZPayload;->serializeAndEncrypt(Lee/cyber/smartid/cryptolib/inter/CryptoOp;[BLjava/lang/String;)Ljava/lang/String;

    move-result-object v9

    const-string v10, "JWE"

    .line 297
    invoke-virtual {v7}, Lee/cyber/smartid/tse/dto/KeyState;->getOneTimePassword()Ljava/lang/String;

    move-result-object v11

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p4

    move-object/from16 v5, p6

    move-object v6, v9

    move-object v7, v10

    move-object v8, v11

    invoke-static/range {v2 .. v8}, Lee/cyber/smartid/tse/dto/KeyState;->forCloneDetection(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lee/cyber/smartid/tse/dto/KeyState;

    move-result-object v2

    .line 298
    iget-object v3, v1, Lee/cyber/smartid/tse/KeyStorage;->h:Lee/cyber/smartid/tse/inter/WallClock;

    iget-object v4, v1, Lee/cyber/smartid/tse/KeyStorage;->g:Lee/cyber/smartid/tse/inter/ResourceAccess;

    invoke-static {v3, v4, v0}, Lee/cyber/smartid/tse/dto/KeyStateMeta;->forActive(Lee/cyber/smartid/tse/inter/WallClock;Lee/cyber/smartid/tse/inter/ResourceAccess;Ljava/lang/String;)Lee/cyber/smartid/tse/dto/KeyStateMeta;

    move-result-object v3

    goto :goto_6

    :cond_d
    move-object/from16 v10, p10

    if-ne v2, v4, :cond_e

    .line 301
    new-instance v2, Lee/cyber/smartid/tse/dto/jsonrpc/payload/SubmitClientSecondPartSZPayload;

    const-string v16, "JWE"

    invoke-virtual {v7}, Lee/cyber/smartid/tse/dto/KeyState;->getOneTimePassword()Ljava/lang/String;

    move-result-object v18

    move-object v14, v2

    move-object/from16 v15, p11

    move-object/from16 v17, p12

    move-object/from16 v19, p10

    invoke-direct/range {v14 .. v19}, Lee/cyber/smartid/tse/dto/jsonrpc/payload/SubmitClientSecondPartSZPayload;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v3, v1, Lee/cyber/smartid/tse/KeyStorage;->b:Lee/cyber/smartid/cryptolib/inter/CryptoOp;

    iget-object v4, v1, Lee/cyber/smartid/tse/KeyStorage;->d:Lee/cyber/smartid/cryptolib/inter/EncodingOp;

    invoke-virtual {v9, v4}, Lee/cyber/smartid/tse/dto/Key;->getDHDerivedKeyBytes(Lee/cyber/smartid/cryptolib/inter/EncodingOp;)[B

    move-result-object v4

    invoke-virtual {v9}, Lee/cyber/smartid/tse/dto/Key;->getDhDerivedKeyId()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v2, v3, v4, v9}, Lee/cyber/smartid/tse/dto/jsonrpc/payload/SubmitClientSecondPartSZPayload;->serializeAndEncrypt(Lee/cyber/smartid/cryptolib/inter/CryptoOp;[BLjava/lang/String;)Ljava/lang/String;

    move-result-object v9

    const-string v10, "JWE"

    .line 302
    invoke-virtual {v7}, Lee/cyber/smartid/tse/dto/KeyState;->getOneTimePassword()Ljava/lang/String;

    move-result-object v11

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p4

    move-object/from16 v5, p6

    move-object v6, v9

    move-object v7, v10

    move-object v8, v11

    invoke-static/range {v2 .. v8}, Lee/cyber/smartid/tse/dto/KeyState;->forSubmitClientSecondPart(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lee/cyber/smartid/tse/dto/KeyState;

    move-result-object v2

    .line 303
    iget-object v3, v1, Lee/cyber/smartid/tse/KeyStorage;->h:Lee/cyber/smartid/tse/inter/WallClock;

    iget-object v4, v1, Lee/cyber/smartid/tse/KeyStorage;->g:Lee/cyber/smartid/tse/inter/ResourceAccess;

    invoke-static {v3, v4, v0}, Lee/cyber/smartid/tse/dto/KeyStateMeta;->forActive(Lee/cyber/smartid/tse/inter/WallClock;Lee/cyber/smartid/tse/inter/ResourceAccess;Ljava/lang/String;)Lee/cyber/smartid/tse/dto/KeyStateMeta;

    move-result-object v3

    .line 310
    :goto_6
    iget-object v4, v1, Lee/cyber/smartid/tse/KeyStorage;->c:Lee/cyber/smartid/cryptolib/inter/StorageOp;

    invoke-interface {v4, v0, v2}, Lee/cyber/smartid/cryptolib/inter/StorageOp;->storeData(Ljava/lang/String;Ljava/lang/Object;)V

    .line 311
    iget-object v0, v1, Lee/cyber/smartid/tse/KeyStorage;->c:Lee/cyber/smartid/cryptolib/inter/StorageOp;

    invoke-interface {v0, v13, v3}, Lee/cyber/smartid/cryptolib/inter/StorageOp;->storeData(Ljava/lang/String;Ljava/lang/Object;)V

    .line 313
    new-instance v0, Landroid/support/v4/e/j;

    invoke-direct {v0, v2, v3}, Landroid/support/v4/e/j;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    monitor-exit v12

    return-object v0

    .line 305
    :cond_e
    new-instance v0, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Unsupported operation type \""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v2, "\"!"

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v3, v2}, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 283
    :cond_f
    new-instance v0, Lee/cyber/smartid/cryptolib/dto/StorageException;

    const-string v2, "Unable to retrieve the Diffie-Hellman derived key material!"

    invoke-direct {v0, v3, v2}, Lee/cyber/smartid/cryptolib/dto/StorageException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 277
    :cond_10
    new-instance v0, Lee/cyber/smartid/cryptolib/dto/StorageException;

    const-string v2, "The existing KeyStateMeta can\'t be null!"

    invoke-direct {v0, v3, v2}, Lee/cyber/smartid/cryptolib/dto/StorageException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 272
    :cond_11
    new-instance v0, Lee/cyber/smartid/cryptolib/dto/StorageException;

    const-string v2, "The transactionUUID of the old state needs to be empty!"

    invoke-direct {v0, v3, v2}, Lee/cyber/smartid/cryptolib/dto/StorageException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 270
    :cond_12
    new-instance v0, Lee/cyber/smartid/cryptolib/dto/StorageException;

    const-string v2, "The clientModulus of the old state needs to be empty!"

    invoke-direct {v0, v3, v2}, Lee/cyber/smartid/cryptolib/dto/StorageException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 268
    :cond_13
    new-instance v0, Lee/cyber/smartid/cryptolib/dto/StorageException;

    const-string v2, "The digestAlgorithm of the old state needs to be empty!"

    invoke-direct {v0, v3, v2}, Lee/cyber/smartid/cryptolib/dto/StorageException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 266
    :cond_14
    new-instance v0, Lee/cyber/smartid/cryptolib/dto/StorageException;

    const-string v2, "The digest of the old state needs to be empty!"

    invoke-direct {v0, v3, v2}, Lee/cyber/smartid/cryptolib/dto/StorageException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 264
    :cond_15
    new-instance v0, Lee/cyber/smartid/cryptolib/dto/StorageException;

    const-string v2, "The ClientShareSecondPart of the old state needs to be empty!"

    invoke-direct {v0, v3, v2}, Lee/cyber/smartid/cryptolib/dto/StorageException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 262
    :cond_16
    new-instance v0, Lee/cyber/smartid/cryptolib/dto/StorageException;

    const-string v2, "The nonce of the old state needs to be empty!"

    invoke-direct {v0, v3, v2}, Lee/cyber/smartid/cryptolib/dto/StorageException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 260
    :cond_17
    new-instance v0, Lee/cyber/smartid/cryptolib/dto/StorageException;

    const-string v2, "The signatureShare of the old state needs to be empty!"

    invoke-direct {v0, v3, v2}, Lee/cyber/smartid/cryptolib/dto/StorageException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 258
    :cond_18
    new-instance v0, Lee/cyber/smartid/cryptolib/dto/StorageException;

    const-string v2, "The old KeyState needs to be idle!"

    invoke-direct {v0, v3, v2}, Lee/cyber/smartid/cryptolib/dto/StorageException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 256
    :cond_19
    new-instance v0, Lee/cyber/smartid/cryptolib/dto/StorageException;

    const-string v2, "The existing KeyState can\'t be null!"

    invoke-direct {v0, v3, v2}, Lee/cyber/smartid/cryptolib/dto/StorageException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 239
    :cond_1a
    new-instance v0, Lee/cyber/smartid/cryptolib/dto/StorageException;

    const-string v2, "The nonce can\'t be empty. Are you trying to store an idle state with startKeyStateOperation?"

    invoke-direct {v0, v3, v2}, Lee/cyber/smartid/cryptolib/dto/StorageException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 237
    :cond_1b
    new-instance v0, Lee/cyber/smartid/cryptolib/dto/StorageException;

    const-string v2, "Idle states can\'t be stored via this method!"

    invoke-direct {v0, v3, v2}, Lee/cyber/smartid/cryptolib/dto/StorageException;-><init>(ILjava/lang/String;)V

    throw v0

    :catchall_0
    move-exception v0

    .line 314
    monitor-exit v12
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public updateFreshnessToken(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lee/cyber/smartid/cryptolib/dto/StorageException;
        }
    .end annotation

    .line 378
    iget-object v0, p0, Lee/cyber/smartid/tse/KeyStorage;->e:Ljava/lang/Object;

    monitor-enter v0

    .line 379
    :try_start_0
    iget-object v1, p0, Lee/cyber/smartid/tse/KeyStorage;->f:Lee/cyber/smartid/tse/inter/LogAccess;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "updateFreshnessToken - keyStateId: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lee/cyber/smartid/tse/inter/LogAccess;->d(Ljava/lang/String;)V

    .line 380
    iget-object v1, p0, Lee/cyber/smartid/tse/KeyStorage;->c:Lee/cyber/smartid/cryptolib/inter/StorageOp;

    iget-object v2, p0, Lee/cyber/smartid/tse/KeyStorage;->g:Lee/cyber/smartid/tse/inter/ResourceAccess;

    invoke-interface {v2, p1}, Lee/cyber/smartid/tse/inter/ResourceAccess;->getFreshnessTokenId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-interface {v1, p1, p2}, Lee/cyber/smartid/cryptolib/inter/StorageOp;->storeData(Ljava/lang/String;Ljava/lang/Object;)V

    .line 381
    monitor-exit v0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method public updateKeyStateMeta(Lee/cyber/smartid/tse/dto/KeyStateMeta;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lee/cyber/smartid/cryptolib/dto/StorageException;
        }
    .end annotation

    .line 171
    iget-object v0, p0, Lee/cyber/smartid/tse/KeyStorage;->e:Ljava/lang/Object;

    monitor-enter v0

    const/4 v1, 0x1

    if-eqz p1, :cond_1

    .line 176
    :try_start_0
    invoke-virtual {p1}, Lee/cyber/smartid/tse/dto/KeyStateMeta;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lee/cyber/smartid/tse/KeyStorage;->getKeyStateMetaById(Ljava/lang/String;)Lee/cyber/smartid/tse/dto/KeyStateMeta;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 180
    iget-object v1, p0, Lee/cyber/smartid/tse/KeyStorage;->f:Lee/cyber/smartid/tse/inter/LogAccess;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "updateKeyStateMeta - id: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lee/cyber/smartid/tse/dto/KeyStateMeta;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lee/cyber/smartid/tse/inter/LogAccess;->d(Ljava/lang/String;)V

    .line 181
    iget-object v1, p0, Lee/cyber/smartid/tse/KeyStorage;->c:Lee/cyber/smartid/cryptolib/inter/StorageOp;

    invoke-virtual {p1}, Lee/cyber/smartid/tse/dto/KeyStateMeta;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2, p1}, Lee/cyber/smartid/cryptolib/inter/StorageOp;->storeData(Ljava/lang/String;Ljava/lang/Object;)V

    .line 182
    monitor-exit v0

    return-void

    .line 178
    :cond_0
    new-instance p1, Lee/cyber/smartid/cryptolib/dto/StorageException;

    const-string v2, "The existing KeyStateMeta can\'t be null!"

    invoke-direct {p1, v1, v2}, Lee/cyber/smartid/cryptolib/dto/StorageException;-><init>(ILjava/lang/String;)V

    throw p1

    :catchall_0
    move-exception p1

    goto :goto_0

    .line 174
    :cond_1
    new-instance p1, Lee/cyber/smartid/cryptolib/dto/StorageException;

    const-string v2, "KeyStateMeta can\'t be null!"

    invoke-direct {p1, v1, v2}, Lee/cyber/smartid/cryptolib/dto/StorageException;-><init>(ILjava/lang/String;)V

    throw p1

    .line 182
    :goto_0
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method
