.class Lee/cyber/smartid/tse/KeyManager$13$9;
.super Ljava/lang/Object;
.source "KeyManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lee/cyber/smartid/tse/KeyManager$13;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;

.field final synthetic b:Lee/cyber/smartid/tse/KeyManager$13;


# direct methods
.method constructor <init>(Lee/cyber/smartid/tse/KeyManager$13;Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;)V
    .locals 0

    .line 1032
    iput-object p1, p0, Lee/cyber/smartid/tse/KeyManager$13$9;->b:Lee/cyber/smartid/tse/KeyManager$13;

    iput-object p2, p0, Lee/cyber/smartid/tse/KeyManager$13$9;->a:Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .line 1035
    iget-object v0, p0, Lee/cyber/smartid/tse/KeyManager$13$9;->b:Lee/cyber/smartid/tse/KeyManager$13;

    iget-object v0, v0, Lee/cyber/smartid/tse/KeyManager$13;->d:Lee/cyber/smartid/tse/KeyManager;

    invoke-static {v0}, Lee/cyber/smartid/tse/KeyManager;->a(Lee/cyber/smartid/tse/KeyManager;)Lee/cyber/smartid/tse/inter/ListenerAccess;

    move-result-object v0

    iget-object v1, p0, Lee/cyber/smartid/tse/KeyManager$13$9;->b:Lee/cyber/smartid/tse/KeyManager$13;

    iget-object v1, v1, Lee/cyber/smartid/tse/KeyManager$13;->b:Ljava/lang/String;

    iget-object v2, p0, Lee/cyber/smartid/tse/KeyManager$13$9;->b:Lee/cyber/smartid/tse/KeyManager$13;

    iget-object v2, v2, Lee/cyber/smartid/tse/KeyManager$13;->d:Lee/cyber/smartid/tse/KeyManager;

    invoke-static {v2}, Lee/cyber/smartid/tse/KeyManager;->a(Lee/cyber/smartid/tse/KeyManager;)Lee/cyber/smartid/tse/inter/ListenerAccess;

    move-result-object v2

    iget-object v3, p0, Lee/cyber/smartid/tse/KeyManager$13$9;->b:Lee/cyber/smartid/tse/KeyManager$13;

    iget-object v3, v3, Lee/cyber/smartid/tse/KeyManager$13;->b:Ljava/lang/String;

    const-class v4, Lee/cyber/smartid/tse/inter/ValidateKeyCreationResponseListener;

    const/4 v5, 0x1

    invoke-interface {v2, v3, v5, v4}, Lee/cyber/smartid/tse/inter/ListenerAccess;->getListener(Ljava/lang/String;ZLjava/lang/Class;)Lee/cyber/smartid/tse/inter/TSEListener;

    move-result-object v2

    iget-object v3, p0, Lee/cyber/smartid/tse/KeyManager$13$9;->b:Lee/cyber/smartid/tse/KeyManager$13;

    iget-object v3, v3, Lee/cyber/smartid/tse/KeyManager$13;->d:Lee/cyber/smartid/tse/KeyManager;

    invoke-static {v3}, Lee/cyber/smartid/tse/KeyManager;->d(Lee/cyber/smartid/tse/KeyManager;)Lee/cyber/smartid/tse/inter/WallClock;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lee/cyber/smartid/tse/KeyManager$13$9;->b:Lee/cyber/smartid/tse/KeyManager$13;

    iget-object v5, v5, Lee/cyber/smartid/tse/KeyManager$13;->d:Lee/cyber/smartid/tse/KeyManager;

    invoke-static {v5}, Lee/cyber/smartid/tse/KeyManager;->c(Lee/cyber/smartid/tse/KeyManager;)Lee/cyber/smartid/tse/inter/ResourceAccess;

    move-result-object v5

    invoke-interface {v5}, Lee/cyber/smartid/tse/inter/ResourceAccess;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    sget v6, Lee/cyber/smartid/tse/R$string;->err_failed_to_decrypt_server_response:I

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v5, p0, Lee/cyber/smartid/tse/KeyManager$13$9;->a:Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;

    invoke-virtual {v5}, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const-wide/16 v5, 0x3fa

    invoke-static {v3, v5, v6, v4}, Lee/cyber/smartid/tse/dto/TSEError;->from(Lee/cyber/smartid/tse/inter/WallClock;JLjava/lang/String;)Lee/cyber/smartid/tse/dto/TSEError;

    move-result-object v3

    invoke-interface {v0, v1, v2, v3}, Lee/cyber/smartid/tse/inter/ListenerAccess;->notifyError(Ljava/lang/String;Lee/cyber/smartid/tse/inter/TSEListener;Lee/cyber/smartid/tse/dto/TSEError;)V

    return-void
.end method
