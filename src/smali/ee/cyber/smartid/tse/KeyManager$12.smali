.class Lee/cyber/smartid/tse/KeyManager$12;
.super Ljava/lang/Object;
.source "KeyManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lee/cyber/smartid/tse/KeyManager;->a(Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/tse/inter/GenerateDiffieHellmanKeyPairListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:Ljava/lang/String;

.field final synthetic c:Lee/cyber/smartid/tse/KeyManager;


# direct methods
.method constructor <init>(Lee/cyber/smartid/tse/KeyManager;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 865
    iput-object p1, p0, Lee/cyber/smartid/tse/KeyManager$12;->c:Lee/cyber/smartid/tse/KeyManager;

    iput-object p2, p0, Lee/cyber/smartid/tse/KeyManager$12;->a:Ljava/lang/String;

    iput-object p3, p0, Lee/cyber/smartid/tse/KeyManager$12;->b:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .line 869
    iget-object v0, p0, Lee/cyber/smartid/tse/KeyManager$12;->c:Lee/cyber/smartid/tse/KeyManager;

    iget-object v1, p0, Lee/cyber/smartid/tse/KeyManager$12;->a:Ljava/lang/String;

    invoke-static {v1}, Lee/cyber/smartid/tse/dto/EncryptKeyWrapper;->getId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lee/cyber/smartid/tse/KeyManager;->loadEncryptedKey(Ljava/lang/String;)Lee/cyber/smartid/tse/dto/EncryptKeyWrapper;

    move-result-object v0

    if-nez v0, :cond_0

    .line 871
    iget-object v0, p0, Lee/cyber/smartid/tse/KeyManager$12;->c:Lee/cyber/smartid/tse/KeyManager;

    invoke-static {v0}, Lee/cyber/smartid/tse/KeyManager;->a(Lee/cyber/smartid/tse/KeyManager;)Lee/cyber/smartid/tse/inter/ListenerAccess;

    move-result-object v0

    new-instance v1, Lee/cyber/smartid/tse/KeyManager$12$1;

    invoke-direct {v1, p0}, Lee/cyber/smartid/tse/KeyManager$12$1;-><init>(Lee/cyber/smartid/tse/KeyManager$12;)V

    invoke-interface {v0, v1}, Lee/cyber/smartid/tse/inter/ListenerAccess;->notifyUI(Ljava/lang/Runnable;)V

    return-void

    .line 882
    :cond_0
    :try_start_0
    iget-object v1, p0, Lee/cyber/smartid/tse/KeyManager$12;->c:Lee/cyber/smartid/tse/KeyManager;

    invoke-static {v1}, Lee/cyber/smartid/tse/KeyManager;->e(Lee/cyber/smartid/tse/KeyManager;)Lee/cyber/smartid/cryptolib/inter/KeyGenerationOp;

    move-result-object v1

    const-wide/16 v2, 0xe

    invoke-static {v2, v3}, Lee/cyber/smartid/cryptolib/dto/DiffieHellmanGroup;->createGroup(J)Lee/cyber/smartid/cryptolib/dto/DiffieHellmanGroup;

    move-result-object v2

    invoke-interface {v1, v2}, Lee/cyber/smartid/cryptolib/inter/KeyGenerationOp;->generateDiffieHellmanKeyPair(Lee/cyber/smartid/cryptolib/dto/DiffieHellmanGroup;)Lee/cyber/smartid/cryptolib/dto/DiffieHellmanKeyPair;

    move-result-object v1
    :try_end_0
    .catch Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException; {:try_start_0 .. :try_end_0} :catch_1

    .line 893
    invoke-virtual {v0, v1}, Lee/cyber/smartid/tse/dto/EncryptKeyWrapper;->setDHKeyPair(Lee/cyber/smartid/cryptolib/dto/DiffieHellmanKeyPair;)V

    .line 895
    :try_start_1
    iget-object v2, p0, Lee/cyber/smartid/tse/KeyManager$12;->c:Lee/cyber/smartid/tse/KeyManager;

    invoke-static {v2, v0}, Lee/cyber/smartid/tse/KeyManager;->b(Lee/cyber/smartid/tse/KeyManager;Lee/cyber/smartid/tse/dto/EncryptKeyWrapper;)V
    :try_end_1
    .catch Lee/cyber/smartid/cryptolib/dto/StorageException; {:try_start_1 .. :try_end_1} :catch_0

    .line 906
    iget-object v0, p0, Lee/cyber/smartid/tse/KeyManager$12;->c:Lee/cyber/smartid/tse/KeyManager;

    invoke-static {v0}, Lee/cyber/smartid/tse/KeyManager;->f(Lee/cyber/smartid/tse/KeyManager;)Lee/cyber/smartid/cryptolib/inter/EncodingOp;

    move-result-object v0

    invoke-virtual {v1}, Lee/cyber/smartid/cryptolib/dto/DiffieHellmanKeyPair;->getPublicKey()Ljava/math/BigInteger;

    move-result-object v1

    invoke-interface {v0, v1}, Lee/cyber/smartid/cryptolib/inter/EncodingOp;->encodeDecimalToBase64(Ljava/math/BigInteger;)Ljava/lang/String;

    move-result-object v0

    .line 909
    iget-object v1, p0, Lee/cyber/smartid/tse/KeyManager$12;->c:Lee/cyber/smartid/tse/KeyManager;

    invoke-static {v1}, Lee/cyber/smartid/tse/KeyManager;->a(Lee/cyber/smartid/tse/KeyManager;)Lee/cyber/smartid/tse/inter/ListenerAccess;

    move-result-object v1

    new-instance v2, Lee/cyber/smartid/tse/KeyManager$12$4;

    invoke-direct {v2, p0, v0}, Lee/cyber/smartid/tse/KeyManager$12$4;-><init>(Lee/cyber/smartid/tse/KeyManager$12;Ljava/lang/String;)V

    invoke-interface {v1, v2}, Lee/cyber/smartid/tse/inter/ListenerAccess;->notifyUI(Ljava/lang/Runnable;)V

    return-void

    :catch_0
    move-exception v0

    .line 897
    iget-object v1, p0, Lee/cyber/smartid/tse/KeyManager$12;->c:Lee/cyber/smartid/tse/KeyManager;

    invoke-static {v1}, Lee/cyber/smartid/tse/KeyManager;->a(Lee/cyber/smartid/tse/KeyManager;)Lee/cyber/smartid/tse/inter/ListenerAccess;

    move-result-object v1

    new-instance v2, Lee/cyber/smartid/tse/KeyManager$12$3;

    invoke-direct {v2, p0, v0}, Lee/cyber/smartid/tse/KeyManager$12$3;-><init>(Lee/cyber/smartid/tse/KeyManager$12;Lee/cyber/smartid/cryptolib/dto/StorageException;)V

    invoke-interface {v1, v2}, Lee/cyber/smartid/tse/inter/ListenerAccess;->notifyUI(Ljava/lang/Runnable;)V

    return-void

    :catch_1
    move-exception v0

    .line 884
    iget-object v1, p0, Lee/cyber/smartid/tse/KeyManager$12;->c:Lee/cyber/smartid/tse/KeyManager;

    invoke-static {v1}, Lee/cyber/smartid/tse/KeyManager;->a(Lee/cyber/smartid/tse/KeyManager;)Lee/cyber/smartid/tse/inter/ListenerAccess;

    move-result-object v1

    new-instance v2, Lee/cyber/smartid/tse/KeyManager$12$2;

    invoke-direct {v2, p0, v0}, Lee/cyber/smartid/tse/KeyManager$12$2;-><init>(Lee/cyber/smartid/tse/KeyManager$12;Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;)V

    invoke-interface {v1, v2}, Lee/cyber/smartid/tse/inter/ListenerAccess;->notifyUI(Ljava/lang/Runnable;)V

    return-void
.end method
