.class public Lee/cyber/smartid/tse/SmartIdTSE;
.super Ljava/lang/Object;
.source "SmartIdTSE.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lee/cyber/smartid/tse/SmartIdTSE$TSEAlarmAccess;,
        Lee/cyber/smartid/tse/SmartIdTSE$TSEAlarmHandler;,
        Lee/cyber/smartid/tse/SmartIdTSE$TSEListenerAccess;,
        Lee/cyber/smartid/tse/SmartIdTSE$TSEResourceAccess;,
        Lee/cyber/smartid/tse/SmartIdTSE$DataEncodingType;,
        Lee/cyber/smartid/tse/SmartIdTSE$AccountKeyType;,
        Lee/cyber/smartid/tse/SmartIdTSE$AccountKeyStatusI;,
        Lee/cyber/smartid/tse/SmartIdTSE$AccountStatus;,
        Lee/cyber/smartid/tse/SmartIdTSE$AccountKeyCertificateType;
    }
.end annotation


# static fields
.field public static final ACCOUNT_KEY_STATUS_EXPIRED:Ljava/lang/String; = "EXPIRED"

.field public static final ACCOUNT_KEY_STATUS_IN_PREPARATION:Ljava/lang/String; = "IN_PREPARATION"

.field public static final ACCOUNT_KEY_STATUS_LOCKED:Ljava/lang/String; = "LOCKED"

.field public static final ACCOUNT_KEY_STATUS_OK:Ljava/lang/String; = "OK"

.field public static final ACCOUNT_KEY_STATUS_REVOKED:Ljava/lang/String; = "REVOKED"

.field public static final ACCOUNT_KEY_TYPE_AUTHENTICATION:Ljava/lang/String; = "AUTHENTICATION"

.field public static final ACCOUNT_KEY_TYPE_SIGNATURE:Ljava/lang/String; = "SIGNATURE"

.field public static final ACCOUNT_STATUS_DISABLED:Ljava/lang/String; = "DISABLED"

.field public static final ACCOUNT_STATUS_ENABLED:Ljava/lang/String; = "ENABLED"

.field public static final CERTIFICATE_TYPE_ADVANCED:Ljava/lang/String; = "ADVANCED"

.field public static final CERTIFICATE_TYPE_QUALIFIED:Ljava/lang/String; = "QUALIFIED"

.field public static final DATA_ENCODING_TYPE_JWE:Ljava/lang/String; = "JWE"

.field private static volatile a:Lee/cyber/smartid/tse/SmartIdTSE;


# instance fields
.field private final b:Landroid/content/Context;

.field private final c:Lee/cyber/smartid/cryptolib/CryptoLib;

.field private final d:Lee/cyber/smartid/tse/SmartIdTSE$TSEListenerAccess;

.field private final e:Lee/cyber/smartid/tse/SmartIdTSE$TSEAlarmAccess;

.field private final f:Lee/cyber/smartid/tse/inter/ResourceAccess;

.field private final g:Lee/cyber/smartid/tse/inter/ExternalResourceAccess;

.field private final h:Lee/cyber/smartid/tse/inter/WallClock;

.field private volatile i:Z

.field private j:Lee/cyber/smartid/tse/inter/LogAccess;

.field private k:Landroid/os/PowerManager$WakeLock;

.field private final l:Ljava/lang/Object;

.field private final m:Ljava/util/WeakHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/WeakHashMap<",
            "Ljava/lang/String;",
            "Lee/cyber/smartid/tse/inter/TSEListener;",
            ">;"
        }
    .end annotation
.end field

.field private n:Ljava/lang/String;

.field private o:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Landroid/util/Pair<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private p:J

.field private q:J

.field private r:J

.field private s:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lee/cyber/smartid/tse/dto/KTKPublicKey;",
            ">;"
        }
    .end annotation
.end field

.field private t:I

.field private u:Z

.field private v:Z

.field private w:Lee/cyber/smartid/tse/network/TSEAPI;

.field private x:Lee/cyber/smartid/tse/KeyManager;

.field private y:Lee/cyber/smartid/tse/KeyStateManager;

.field private volatile z:J


# direct methods
.method private constructor <init>(Landroid/content/Context;Lee/cyber/smartid/tse/inter/ExternalResourceAccess;Lee/cyber/smartid/tse/inter/AlarmAccess;Lee/cyber/smartid/tse/inter/WallClock;Lee/cyber/smartid/tse/inter/LogAccess;)V
    .locals 1

    .line 801
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 383
    invoke-static {p0}, Lee/cyber/smartid/tse/util/Log;->getInstance(Ljava/lang/Object;)Lee/cyber/smartid/tse/util/Log;

    move-result-object v0

    iput-object v0, p0, Lee/cyber/smartid/tse/SmartIdTSE;->j:Lee/cyber/smartid/tse/inter/LogAccess;

    .line 391
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lee/cyber/smartid/tse/SmartIdTSE;->l:Ljava/lang/Object;

    .line 395
    new-instance v0, Ljava/util/WeakHashMap;

    invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V

    iput-object v0, p0, Lee/cyber/smartid/tse/SmartIdTSE;->m:Ljava/util/WeakHashMap;

    if-eqz p1, :cond_4

    if-eqz p5, :cond_3

    if-eqz p3, :cond_2

    if-eqz p2, :cond_1

    if-eqz p4, :cond_0

    .line 814
    invoke-static {p5}, Lee/cyber/smartid/tse/util/Log;->setImpl(Lee/cyber/smartid/tse/inter/LogAccess;)V

    .line 815
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    iput-object p1, p0, Lee/cyber/smartid/tse/SmartIdTSE;->b:Landroid/content/Context;

    .line 816
    invoke-static {}, Lee/cyber/smartid/cryptolib/CryptoLib;->newInstance()Lee/cyber/smartid/cryptolib/CryptoLib;

    move-result-object p1

    iget-object p5, p0, Lee/cyber/smartid/tse/SmartIdTSE;->b:Landroid/content/Context;

    invoke-virtual {p1, p5}, Lee/cyber/smartid/cryptolib/CryptoLib;->setAndroidDefaults(Landroid/content/Context;)Lee/cyber/smartid/cryptolib/CryptoLib;

    move-result-object p1

    iput-object p1, p0, Lee/cyber/smartid/tse/SmartIdTSE;->c:Lee/cyber/smartid/cryptolib/CryptoLib;

    .line 817
    new-instance p1, Lee/cyber/smartid/tse/SmartIdTSE$TSEResourceAccess;

    const/4 p5, 0x0

    invoke-direct {p1, p0, p5}, Lee/cyber/smartid/tse/SmartIdTSE$TSEResourceAccess;-><init>(Lee/cyber/smartid/tse/SmartIdTSE;Lee/cyber/smartid/tse/SmartIdTSE$1;)V

    iput-object p1, p0, Lee/cyber/smartid/tse/SmartIdTSE;->f:Lee/cyber/smartid/tse/inter/ResourceAccess;

    .line 818
    new-instance p1, Lee/cyber/smartid/tse/SmartIdTSE$TSEListenerAccess;

    invoke-direct {p1, p0, p5}, Lee/cyber/smartid/tse/SmartIdTSE$TSEListenerAccess;-><init>(Lee/cyber/smartid/tse/SmartIdTSE;Lee/cyber/smartid/tse/SmartIdTSE$1;)V

    iput-object p1, p0, Lee/cyber/smartid/tse/SmartIdTSE;->d:Lee/cyber/smartid/tse/SmartIdTSE$TSEListenerAccess;

    .line 819
    new-instance p1, Lee/cyber/smartid/tse/SmartIdTSE$TSEAlarmAccess;

    invoke-direct {p1, p0, p3}, Lee/cyber/smartid/tse/SmartIdTSE$TSEAlarmAccess;-><init>(Lee/cyber/smartid/tse/SmartIdTSE;Lee/cyber/smartid/tse/inter/AlarmAccess;)V

    iput-object p1, p0, Lee/cyber/smartid/tse/SmartIdTSE;->e:Lee/cyber/smartid/tse/SmartIdTSE$TSEAlarmAccess;

    .line 820
    iget-object p1, p0, Lee/cyber/smartid/tse/SmartIdTSE;->e:Lee/cyber/smartid/tse/SmartIdTSE$TSEAlarmAccess;

    new-instance p3, Lee/cyber/smartid/tse/SmartIdTSE$TSEAlarmHandler;

    invoke-direct {p3, p0}, Lee/cyber/smartid/tse/SmartIdTSE$TSEAlarmHandler;-><init>(Lee/cyber/smartid/tse/SmartIdTSE;)V

    invoke-virtual {p1, p3}, Lee/cyber/smartid/tse/SmartIdTSE$TSEAlarmAccess;->addAlarmHandler(Lee/cyber/smartid/tse/inter/AlarmAccess$AlarmHandler;)V

    .line 821
    iput-object p2, p0, Lee/cyber/smartid/tse/SmartIdTSE;->g:Lee/cyber/smartid/tse/inter/ExternalResourceAccess;

    .line 822
    iput-object p4, p0, Lee/cyber/smartid/tse/SmartIdTSE;->h:Lee/cyber/smartid/tse/inter/WallClock;

    return-void

    .line 811
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "ClockAccess can\'t be null!"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 809
    :cond_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "ExternalAccess can\'t be null!"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 807
    :cond_2
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "AlarmAccess can\'t be null!"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 805
    :cond_3
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "LogAccess can\'t be null!"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 803
    :cond_4
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "Context can\'t be null!"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method static a(IJ)J
    .locals 4

    const/4 v0, 0x1

    .line 1398
    invoke-static {v0, p0}, Ljava/lang/Math;->max(II)I

    move-result p0

    const-wide/16 v0, 0x0

    cmp-long v2, p1, v0

    if-gez v2, :cond_0

    const-wide p1, 0x7fffffffffffffffL

    :cond_0
    const/16 v0, 0x1f

    if-ge p0, v0, :cond_1

    long-to-double p1, p1

    const-wide/high16 v0, 0x4000000000000000L    # 2.0

    add-int/lit8 v2, p0, 0x1

    int-to-double v2, v2

    .line 1407
    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v0

    const-wide v2, 0x408f400000000000L    # 1000.0

    mul-double v0, v0, v2

    invoke-static {p1, p2, v0, v1}, Ljava/lang/Math;->min(DD)D

    move-result-wide p1

    double-to-long p1, p1

    .line 1411
    :cond_1
    const-class v0, Lee/cyber/smartid/tse/SmartIdTSE;

    invoke-static {v0}, Lee/cyber/smartid/tse/util/Log;->getInstance(Ljava/lang/Class;)Lee/cyber/smartid/tse/util/Log;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getAutomaticRequestRetryDelay - retry "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p0, ", backoff: "

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v0, p0}, Lee/cyber/smartid/tse/util/Log;->d(Ljava/lang/String;)V

    return-wide p1
.end method

.method static synthetic a(Lee/cyber/smartid/tse/SmartIdTSE;J)J
    .locals 0

    .line 101
    iput-wide p1, p0, Lee/cyber/smartid/tse/SmartIdTSE;->z:J

    return-wide p1
.end method

.method static synthetic a(Lee/cyber/smartid/tse/SmartIdTSE;)Lee/cyber/smartid/tse/inter/ExternalResourceAccess;
    .locals 0

    .line 101
    iget-object p0, p0, Lee/cyber/smartid/tse/SmartIdTSE;->g:Lee/cyber/smartid/tse/inter/ExternalResourceAccess;

    return-object p0
.end method

.method static synthetic a(Lee/cyber/smartid/tse/SmartIdTSE;Ljava/lang/String;ZLjava/lang/Class;)Lee/cyber/smartid/tse/inter/TSEListener;
    .locals 0

    .line 101
    invoke-direct {p0, p1, p2, p3}, Lee/cyber/smartid/tse/SmartIdTSE;->a(Ljava/lang/String;ZLjava/lang/Class;)Lee/cyber/smartid/tse/inter/TSEListener;

    move-result-object p0

    return-object p0
.end method

.method private a(Ljava/lang/String;ZLjava/lang/Class;)Lee/cyber/smartid/tse/inter/TSEListener;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lee/cyber/smartid/tse/inter/TSEListener;",
            ">(",
            "Ljava/lang/String;",
            "Z",
            "Ljava/lang/Class<",
            "TT;>;)TT;"
        }
    .end annotation

    .line 1133
    iget-object v0, p0, Lee/cyber/smartid/tse/SmartIdTSE;->j:Lee/cyber/smartid/tse/inter/LogAccess;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getListener: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, ", forgetListener: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lee/cyber/smartid/tse/inter/LogAccess;->d(Ljava/lang/String;)V

    .line 1135
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_3

    if-nez p3, :cond_0

    goto :goto_2

    .line 1138
    :cond_0
    iget-object v0, p0, Lee/cyber/smartid/tse/SmartIdTSE;->m:Ljava/util/WeakHashMap;

    monitor-enter v0

    .line 1139
    :try_start_0
    iget-object v2, p0, Lee/cyber/smartid/tse/SmartIdTSE;->m:Ljava/util/WeakHashMap;

    invoke-virtual {v2, p1}, Ljava/util/WeakHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lee/cyber/smartid/tse/SmartIdTSE;->m:Ljava/util/WeakHashMap;

    invoke-virtual {v2, p1}, Ljava/util/WeakHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {p3, v2}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result p3

    if-eqz p3, :cond_2

    if-eqz p2, :cond_1

    .line 1141
    iget-object p2, p0, Lee/cyber/smartid/tse/SmartIdTSE;->m:Ljava/util/WeakHashMap;

    invoke-virtual {p2, p1}, Ljava/util/WeakHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    :goto_0
    check-cast p1, Lee/cyber/smartid/tse/inter/TSEListener;

    goto :goto_1

    :cond_1
    iget-object p2, p0, Lee/cyber/smartid/tse/SmartIdTSE;->m:Ljava/util/WeakHashMap;

    invoke-virtual {p2, p1}, Ljava/util/WeakHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    goto :goto_0

    :goto_1
    monitor-exit v0

    return-object p1

    .line 1143
    :cond_2
    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception p1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1

    :cond_3
    :goto_2
    return-object v1
.end method

.method private a(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .line 1638
    iget-object v0, p0, Lee/cyber/smartid/tse/SmartIdTSE;->c:Lee/cyber/smartid/cryptolib/CryptoLib;

    new-instance v1, Lee/cyber/smartid/tse/SmartIdTSE$5;

    invoke-direct {v1, p0}, Lee/cyber/smartid/tse/SmartIdTSE$5;-><init>(Lee/cyber/smartid/tse/SmartIdTSE;)V

    .line 1639
    invoke-virtual {v1}, Lee/cyber/smartid/tse/SmartIdTSE$5;->getType()Ljava/lang/reflect/Type;

    move-result-object v1

    .line 1638
    invoke-virtual {v0, p1, v1}, Lee/cyber/smartid/cryptolib/CryptoLib;->retrieveData(Ljava/lang/String;Ljava/lang/reflect/Type;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    return-object p1
.end method

.method static synthetic a(Lee/cyber/smartid/tse/SmartIdTSE;Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 0

    .line 101
    invoke-direct {p0, p1}, Lee/cyber/smartid/tse/SmartIdTSE;->e(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object p0

    return-object p0
.end method

.method private a(Ljava/util/Properties;)Ljava/util/ArrayList;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Properties;",
            ")",
            "Ljava/util/ArrayList<",
            "Landroid/util/Pair<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    const-string v0, "pins"

    .line 1303
    invoke-virtual {p1, v0}, Ljava/util/Properties;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 1304
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 p1, 0x0

    return-object p1

    .line 1308
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    const-string v1, ";"

    .line 1309
    invoke-virtual {p1, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object p1

    .line 1310
    array-length v1, p1

    const/4 v2, 0x0

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v1, :cond_3

    aget-object v4, p1, v3

    const-string v5, ","

    .line 1311
    invoke-virtual {v4, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 1312
    array-length v5, v4

    const/4 v6, 0x2

    if-ne v5, v6, :cond_2

    aget-object v5, v4, v2

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_2

    const/4 v5, 0x1

    aget-object v6, v4, v5

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_1

    goto :goto_1

    .line 1315
    :cond_1
    new-instance v6, Landroid/util/Pair;

    aget-object v7, v4, v2

    invoke-virtual {v7}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v7

    aget-object v4, v4, v5

    invoke-virtual {v4}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v6, v7, v4}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_2
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_3
    return-object v0
.end method

.method private a()V
    .locals 5

    const-string v0, "postInitService"

    .line 923
    invoke-direct {p0}, Lee/cyber/smartid/tse/SmartIdTSE;->e()Landroid/os/PowerManager$WakeLock;

    move-result-object v1

    const-wide/32 v2, 0x1d4c0

    invoke-static {v1, v2, v3}, Lee/cyber/smartid/tse/util/Util;->acquireWakeLock(Landroid/os/PowerManager$WakeLock;J)V

    .line 925
    :try_start_0
    iget-object v1, p0, Lee/cyber/smartid/tse/SmartIdTSE;->j:Lee/cyber/smartid/tse/inter/LogAccess;

    const-string v2, "postInitTSE called"

    invoke-interface {v1, v2}, Lee/cyber/smartid/tse/inter/LogAccess;->d(Ljava/lang/String;)V

    .line 926
    invoke-direct {p0}, Lee/cyber/smartid/tse/SmartIdTSE;->j()Z

    move-result v1

    .line 927
    invoke-direct {p0}, Lee/cyber/smartid/tse/SmartIdTSE;->k()Z

    move-result v2
    :try_end_0
    .catch Ljava/lang/Error; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const-string v3, "internal-setup"

    if-nez v2, :cond_0

    const/4 v2, 0x1

    .line 932
    :try_start_1
    invoke-direct {p0, v2}, Lee/cyber/smartid/tse/SmartIdTSE;->c(Z)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Error; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v2

    .line 935
    :try_start_2
    iget-object v4, p0, Lee/cyber/smartid/tse/SmartIdTSE;->j:Lee/cyber/smartid/tse/inter/LogAccess;

    invoke-interface {v4, v0, v2}, Lee/cyber/smartid/tse/inter/LogAccess;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 937
    :goto_0
    iget-object v2, p0, Lee/cyber/smartid/tse/SmartIdTSE;->y:Lee/cyber/smartid/tse/KeyStateManager;

    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    invoke-static {v4, v3}, Lee/cyber/smartid/tse/util/Util;->addXSplitKeyTriggerExtra(Landroid/os/Bundle;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v4

    invoke-virtual {v2, v4}, Lee/cyber/smartid/tse/KeyStateManager;->b(Landroid/os/Bundle;)V

    :cond_0
    if-nez v1, :cond_1

    .line 941
    iget-object v1, p0, Lee/cyber/smartid/tse/SmartIdTSE;->y:Lee/cyber/smartid/tse/KeyStateManager;

    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    invoke-static {v2, v3}, Lee/cyber/smartid/tse/util/Util;->addXSplitKeyTriggerExtra(Landroid/os/Bundle;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {v1, v2}, Lee/cyber/smartid/tse/KeyStateManager;->a(Landroid/os/Bundle;)V
    :try_end_2
    .catch Ljava/lang/Error; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 943
    :try_start_3
    iget-object v1, p0, Lee/cyber/smartid/tse/SmartIdTSE;->e:Lee/cyber/smartid/tse/SmartIdTSE$TSEAlarmAccess;

    invoke-virtual {v1}, Lee/cyber/smartid/tse/SmartIdTSE$TSEAlarmAccess;->setRefreshCloneDetectionAlarm()V
    :try_end_3
    .catch Lee/cyber/smartid/cryptolib/dto/StorageException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/lang/Error; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    :catch_1
    move-exception v1

    .line 946
    :try_start_4
    iget-object v2, p0, Lee/cyber/smartid/tse/SmartIdTSE;->j:Lee/cyber/smartid/tse/inter/LogAccess;

    invoke-interface {v2, v0, v1}, Lee/cyber/smartid/tse/inter/LogAccess;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 951
    :cond_1
    :goto_1
    iget-object v1, p0, Lee/cyber/smartid/tse/SmartIdTSE;->x:Lee/cyber/smartid/tse/KeyManager;

    invoke-virtual {v1}, Lee/cyber/smartid/tse/KeyManager;->removeAllExpiredEncryptedKeys()V
    :try_end_4
    .catch Ljava/lang/Error; {:try_start_4 .. :try_end_4} :catch_2
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_2

    :catchall_0
    move-exception v0

    goto :goto_3

    :catch_2
    move-exception v1

    .line 953
    :try_start_5
    iget-object v2, p0, Lee/cyber/smartid/tse/SmartIdTSE;->j:Lee/cyber/smartid/tse/inter/LogAccess;

    invoke-interface {v2, v0, v1}, Lee/cyber/smartid/tse/inter/LogAccess;->d(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 956
    :goto_2
    invoke-direct {p0}, Lee/cyber/smartid/tse/SmartIdTSE;->e()Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    invoke-static {v0}, Lee/cyber/smartid/tse/util/Util;->releaseWakeLock(Landroid/os/PowerManager$WakeLock;)V

    return-void

    :goto_3
    invoke-direct {p0}, Lee/cyber/smartid/tse/SmartIdTSE;->e()Landroid/os/PowerManager$WakeLock;

    move-result-object v1

    invoke-static {v1}, Lee/cyber/smartid/tse/util/Util;->releaseWakeLock(Landroid/os/PowerManager$WakeLock;)V

    .line 957
    throw v0
.end method

.method private a(I)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lee/cyber/smartid/cryptolib/dto/StorageException;
        }
    .end annotation

    .line 1026
    iget-object v0, p0, Lee/cyber/smartid/tse/SmartIdTSE;->j:Lee/cyber/smartid/tse/inter/LogAccess;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onUpgrade - versionTo: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lee/cyber/smartid/tse/inter/LogAccess;->d(Ljava/lang/String;)V

    .line 1028
    new-instance v0, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;

    iget-object v1, p0, Lee/cyber/smartid/tse/SmartIdTSE;->b:Landroid/content/Context;

    sget v2, Lee/cyber/smartid/tse/R$string;->err_unknown_storage_upgrade_to_x:I

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object p1

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    const/16 v1, 0x77

    invoke-direct {v0, v1, p1}, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;-><init>(ILjava/lang/String;)V

    throw v0
.end method

.method private a(II)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lee/cyber/smartid/cryptolib/dto/StorageException;
        }
    .end annotation

    const/4 v0, 0x5

    if-nez p1, :cond_0

    if-ne p2, v0, :cond_0

    .line 996
    invoke-direct {p0}, Lee/cyber/smartid/tse/SmartIdTSE;->d()Z

    move-result v1

    if-nez v1, :cond_0

    .line 998
    iget-object p1, p0, Lee/cyber/smartid/tse/SmartIdTSE;->j:Lee/cyber/smartid/tse/inter/LogAccess;

    const-string v0, "handleUpgrade: this is a clean install, no need to upgrade"

    invoke-interface {p1, v0}, Lee/cyber/smartid/tse/inter/LogAccess;->d(Ljava/lang/String;)V

    .line 999
    invoke-direct {p0, p2}, Lee/cyber/smartid/tse/SmartIdTSE;->b(I)V

    goto :goto_1

    :cond_0
    const/16 v1, 0x84

    const/4 v2, 0x0

    const/4 v3, 0x2

    const/4 v4, 0x1

    if-ge p1, p2, :cond_3

    if-lt p1, v0, :cond_2

    .line 1005
    iget-object v0, p0, Lee/cyber/smartid/tse/SmartIdTSE;->j:Lee/cyber/smartid/tse/inter/LogAccess;

    new-array v1, v3, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    const-string v2, "upgrade initiate version update from version %1$s to version %2$s"

    invoke-static {v2, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lee/cyber/smartid/tse/inter/LogAccess;->d(Ljava/lang/String;)V

    :goto_0
    sub-int v0, p2, p1

    if-gt v4, v0, :cond_1

    add-int v0, p1, v4

    .line 1007
    invoke-direct {p0, v0}, Lee/cyber/smartid/tse/SmartIdTSE;->a(I)V

    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 1009
    :cond_1
    invoke-direct {p0, p2}, Lee/cyber/smartid/tse/SmartIdTSE;->b(I)V

    goto :goto_1

    .line 1003
    :cond_2
    new-instance p1, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;

    iget-object p2, p0, Lee/cyber/smartid/tse/SmartIdTSE;->b:Landroid/content/Context;

    sget v0, Lee/cyber/smartid/tse/R$string;->err_upgrade_to_mdv2_not_supported:I

    invoke-virtual {p2, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, v1, p2}, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;-><init>(ILjava/lang/String;)V

    throw p1

    :cond_3
    if-ne p1, p2, :cond_4

    .line 1011
    iget-object p1, p0, Lee/cyber/smartid/tse/SmartIdTSE;->j:Lee/cyber/smartid/tse/inter/LogAccess;

    const-string p2, "handleUpgrade: fromVersion and toVersion are the same, no need for upgrade"

    invoke-interface {p1, p2}, Lee/cyber/smartid/tse/inter/LogAccess;->d(Ljava/lang/String;)V

    :goto_1
    return-void

    .line 1013
    :cond_4
    iget-object v0, p0, Lee/cyber/smartid/tse/SmartIdTSE;->j:Lee/cyber/smartid/tse/inter/LogAccess;

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    aput-object p2, v3, v2

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v3, v4

    const-string p1, "handleUpgrade: toVersion %1$s lower than fromVersion %2$s"

    invoke-static {p1, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, p1}, Lee/cyber/smartid/tse/inter/LogAccess;->w(Ljava/lang/String;)V

    .line 1014
    new-instance p1, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;

    iget-object p2, p0, Lee/cyber/smartid/tse/SmartIdTSE;->b:Landroid/content/Context;

    sget v0, Lee/cyber/smartid/tse/R$string;->err_storage_version_downgrade_is_not_supported_use_a_clean_install:I

    invoke-virtual {p2, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, v1, p2}, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;-><init>(ILjava/lang/String;)V

    throw p1
.end method

.method private a(Landroid/os/Bundle;)V
    .locals 3

    .line 1817
    invoke-virtual {p0}, Lee/cyber/smartid/tse/SmartIdTSE;->isInitializeTSEDone()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1818
    iget-object p1, p0, Lee/cyber/smartid/tse/SmartIdTSE;->j:Lee/cyber/smartid/tse/inter/LogAccess;

    const-string v0, "triggerKeyStateSolve: Init not done yet, aborting .."

    invoke-interface {p1, v0}, Lee/cyber/smartid/tse/inter/LogAccess;->e(Ljava/lang/String;)V

    return-void

    .line 1822
    :cond_0
    invoke-direct {p0}, Lee/cyber/smartid/tse/SmartIdTSE;->e()Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    const-wide/32 v1, 0x1d4c0

    invoke-static {v0, v1, v2}, Lee/cyber/smartid/tse/util/Util;->acquireWakeLock(Landroid/os/PowerManager$WakeLock;J)V

    .line 1823
    iget-object v0, p0, Lee/cyber/smartid/tse/SmartIdTSE;->j:Lee/cyber/smartid/tse/inter/LogAccess;

    const-string v1, "triggerKeyStateSolve"

    invoke-interface {v0, v1}, Lee/cyber/smartid/tse/inter/LogAccess;->d(Ljava/lang/String;)V

    const/4 v0, 0x1

    .line 1827
    :try_start_0
    invoke-direct {p0, v0}, Lee/cyber/smartid/tse/SmartIdTSE;->c(Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception p1

    goto :goto_4

    :catch_0
    move-exception p1

    goto :goto_2

    :catch_1
    move-exception v0

    .line 1830
    :try_start_1
    iget-object v2, p0, Lee/cyber/smartid/tse/SmartIdTSE;->j:Lee/cyber/smartid/tse/inter/LogAccess;

    invoke-interface {v2, v1, v0}, Lee/cyber/smartid/tse/inter/LogAccess;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1833
    :goto_0
    iget-object v0, p0, Lee/cyber/smartid/tse/SmartIdTSE;->y:Lee/cyber/smartid/tse/KeyStateManager;

    invoke-virtual {v0, p1}, Lee/cyber/smartid/tse/KeyStateManager;->b(Landroid/os/Bundle;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1837
    :goto_1
    invoke-direct {p0}, Lee/cyber/smartid/tse/SmartIdTSE;->e()Landroid/os/PowerManager$WakeLock;

    move-result-object p1

    invoke-static {p1}, Lee/cyber/smartid/tse/util/Util;->releaseWakeLock(Landroid/os/PowerManager$WakeLock;)V

    goto :goto_3

    .line 1835
    :goto_2
    :try_start_2
    iget-object v0, p0, Lee/cyber/smartid/tse/SmartIdTSE;->j:Lee/cyber/smartid/tse/inter/LogAccess;

    invoke-interface {v0, v1, p1}, Lee/cyber/smartid/tse/inter/LogAccess;->e(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    :goto_3
    return-void

    .line 1837
    :goto_4
    invoke-direct {p0}, Lee/cyber/smartid/tse/SmartIdTSE;->e()Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    invoke-static {v0}, Lee/cyber/smartid/tse/util/Util;->releaseWakeLock(Landroid/os/PowerManager$WakeLock;)V

    .line 1838
    throw p1
.end method

.method static synthetic a(Lee/cyber/smartid/tse/SmartIdTSE;Lee/cyber/smartid/tse/dto/TSEError;)V
    .locals 0

    .line 101
    invoke-direct {p0, p1}, Lee/cyber/smartid/tse/SmartIdTSE;->a(Lee/cyber/smartid/tse/dto/TSEError;)V

    return-void
.end method

.method static synthetic a(Lee/cyber/smartid/tse/SmartIdTSE;Ljava/lang/Runnable;)V
    .locals 0

    .line 101
    invoke-direct {p0, p1}, Lee/cyber/smartid/tse/SmartIdTSE;->a(Ljava/lang/Runnable;)V

    return-void
.end method

.method static synthetic a(Lee/cyber/smartid/tse/SmartIdTSE;Ljava/lang/String;Lee/cyber/smartid/tse/inter/TSEListener;Lee/cyber/smartid/tse/dto/TSEError;)V
    .locals 0

    .line 101
    invoke-direct {p0, p1, p2, p3}, Lee/cyber/smartid/tse/SmartIdTSE;->a(Ljava/lang/String;Lee/cyber/smartid/tse/inter/TSEListener;Lee/cyber/smartid/tse/dto/TSEError;)V

    return-void
.end method

.method static synthetic a(Lee/cyber/smartid/tse/SmartIdTSE;Ljava/lang/String;Ljava/lang/String;ZZ)V
    .locals 0

    .line 101
    invoke-direct {p0, p1, p2, p3, p4}, Lee/cyber/smartid/tse/SmartIdTSE;->a(Ljava/lang/String;Ljava/lang/String;ZZ)V

    return-void
.end method

.method static synthetic a(Lee/cyber/smartid/tse/SmartIdTSE;Z)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lee/cyber/smartid/cryptolib/dto/StorageException;
        }
    .end annotation

    .line 101
    invoke-direct {p0, p1}, Lee/cyber/smartid/tse/SmartIdTSE;->b(Z)V

    return-void
.end method

.method private a(Lee/cyber/smartid/tse/dto/TSEError;)V
    .locals 2

    .line 1481
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    new-instance v1, Lee/cyber/smartid/tse/SmartIdTSE$4;

    invoke-direct {v1, p0, p1}, Lee/cyber/smartid/tse/SmartIdTSE$4;-><init>(Lee/cyber/smartid/tse/SmartIdTSE;Lee/cyber/smartid/tse/dto/TSEError;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method private a(Ljava/lang/Runnable;)V
    .locals 2

    .line 1354
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 1355
    invoke-virtual {v0, p1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method private a(Ljava/lang/String;Lee/cyber/smartid/tse/inter/TSEListener;Lee/cyber/smartid/tse/dto/TSEError;)V
    .locals 11

    const-string v0, "notifyError"

    if-eqz p3, :cond_0

    goto :goto_0

    .line 1426
    :cond_0
    iget-object p3, p0, Lee/cyber/smartid/tse/SmartIdTSE;->h:Lee/cyber/smartid/tse/inter/WallClock;

    const-wide/16 v1, 0x0

    iget-object v3, p0, Lee/cyber/smartid/tse/SmartIdTSE;->b:Landroid/content/Context;

    sget v4, Lee/cyber/smartid/tse/R$string;->err_unknown:I

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {p3, v1, v2, v3}, Lee/cyber/smartid/tse/dto/TSEError;->from(Lee/cyber/smartid/tse/inter/WallClock;JLjava/lang/String;)Lee/cyber/smartid/tse/dto/TSEError;

    move-result-object p3

    .line 1427
    :goto_0
    instance-of v1, p2, Lee/cyber/smartid/tse/inter/ConfirmTransactionListener;

    if-eqz v1, :cond_1

    .line 1428
    check-cast p2, Lee/cyber/smartid/tse/inter/ConfirmTransactionListener;

    invoke-interface {p2, p1, p3}, Lee/cyber/smartid/tse/inter/ConfirmTransactionListener;->onConfirmTransactionFailed(Ljava/lang/String;Lee/cyber/smartid/tse/dto/TSEError;)V

    goto/16 :goto_4

    .line 1429
    :cond_1
    instance-of v1, p2, Lee/cyber/smartid/tse/inter/RefreshCloneDetectionListener;

    if-eqz v1, :cond_2

    .line 1430
    check-cast p2, Lee/cyber/smartid/tse/inter/RefreshCloneDetectionListener;

    invoke-interface {p2, p1, p3}, Lee/cyber/smartid/tse/inter/RefreshCloneDetectionListener;->onRefreshCloneDetectionFailed(Ljava/lang/String;Lee/cyber/smartid/tse/dto/TSEError;)V

    goto/16 :goto_4

    .line 1431
    :cond_2
    instance-of v1, p2, Lee/cyber/smartid/tse/inter/NewFreshnessTokenListener;

    if-eqz v1, :cond_3

    .line 1432
    check-cast p2, Lee/cyber/smartid/tse/inter/NewFreshnessTokenListener;

    invoke-interface {p2, p1, p3}, Lee/cyber/smartid/tse/inter/NewFreshnessTokenListener;->onNewFreshnessTokenFailed(Ljava/lang/String;Lee/cyber/smartid/tse/dto/TSEError;)V

    goto/16 :goto_4

    .line 1433
    :cond_3
    instance-of v1, p2, Lee/cyber/smartid/tse/inter/CheckLocalPendingStateListener;

    if-eqz v1, :cond_4

    .line 1434
    check-cast p2, Lee/cyber/smartid/tse/inter/CheckLocalPendingStateListener;

    invoke-interface {p2, p1, p3}, Lee/cyber/smartid/tse/inter/CheckLocalPendingStateListener;->onCheckLocalPendingStateFailed(Ljava/lang/String;Lee/cyber/smartid/tse/dto/TSEError;)V

    goto/16 :goto_4

    .line 1435
    :cond_4
    instance-of v1, p2, Lee/cyber/smartid/tse/inter/InitializeKeyAndKeyStatesListener;

    if-eqz v1, :cond_5

    .line 1436
    check-cast p2, Lee/cyber/smartid/tse/inter/InitializeKeyAndKeyStatesListener;

    invoke-interface {p2, p1, p3}, Lee/cyber/smartid/tse/inter/InitializeKeyAndKeyStatesListener;->onInitializeKeyAndKeyStatesFailed(Ljava/lang/String;Lee/cyber/smartid/tse/dto/TSEError;)V

    goto/16 :goto_4

    .line 1437
    :cond_5
    instance-of v1, p2, Lee/cyber/smartid/tse/inter/ValidateKeyCreationResponseListener;

    if-eqz v1, :cond_6

    .line 1438
    check-cast p2, Lee/cyber/smartid/tse/inter/ValidateKeyCreationResponseListener;

    invoke-interface {p2, p1, p3}, Lee/cyber/smartid/tse/inter/ValidateKeyCreationResponseListener;->onValidateKeyCreationResponseFailed(Ljava/lang/String;Lee/cyber/smartid/tse/dto/TSEError;)V

    goto/16 :goto_4

    .line 1439
    :cond_6
    instance-of v1, p2, Lee/cyber/smartid/tse/inter/EncryptKeyListener;

    if-eqz v1, :cond_7

    .line 1440
    check-cast p2, Lee/cyber/smartid/tse/inter/EncryptKeyListener;

    invoke-interface {p2, p1, p3}, Lee/cyber/smartid/tse/inter/EncryptKeyListener;->onEncryptKeyFailed(Ljava/lang/String;Lee/cyber/smartid/tse/dto/TSEError;)V

    goto/16 :goto_4

    .line 1441
    :cond_7
    instance-of v1, p2, Lee/cyber/smartid/tse/inter/GenerateKeysListener;

    if-eqz v1, :cond_8

    .line 1442
    check-cast p2, Lee/cyber/smartid/tse/inter/GenerateKeysListener;

    invoke-interface {p2, p1, p3}, Lee/cyber/smartid/tse/inter/GenerateKeysListener;->onGenerateKeysFailed(Ljava/lang/String;Lee/cyber/smartid/tse/dto/TSEError;)V

    goto/16 :goto_4

    .line 1443
    :cond_8
    instance-of v1, p2, Lee/cyber/smartid/tse/inter/GenerateDiffieHellmanKeyPairListener;

    if-eqz v1, :cond_9

    .line 1444
    check-cast p2, Lee/cyber/smartid/tse/inter/GenerateDiffieHellmanKeyPairListener;

    invoke-interface {p2, p1, p3}, Lee/cyber/smartid/tse/inter/GenerateDiffieHellmanKeyPairListener;->onGenerateDiffieHellmanKeyPairFailed(Ljava/lang/String;Lee/cyber/smartid/tse/dto/TSEError;)V

    goto/16 :goto_4

    .line 1445
    :cond_9
    instance-of v1, p2, Lee/cyber/smartid/tse/inter/SubmitClientSecondPartListener;

    if-eqz v1, :cond_a

    .line 1446
    check-cast p2, Lee/cyber/smartid/tse/inter/SubmitClientSecondPartListener;

    invoke-interface {p2, p1, p3}, Lee/cyber/smartid/tse/inter/SubmitClientSecondPartListener;->onSubmitClientSecondPartFailed(Ljava/lang/String;Lee/cyber/smartid/tse/dto/TSEError;)V

    goto/16 :goto_4

    :cond_a
    if-eqz p2, :cond_d

    .line 1448
    iget-object v1, p0, Lee/cyber/smartid/tse/SmartIdTSE;->j:Lee/cyber/smartid/tse/inter/LogAccess;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "notifyError: Unknown listener class "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, " Please add implementation for this to the notifyError() method!"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lee/cyber/smartid/tse/inter/LogAccess;->e(Ljava/lang/String;)V

    .line 1449
    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getDeclaredMethods()[Ljava/lang/reflect/Method;

    move-result-object v1

    if-eqz v1, :cond_d

    .line 1450
    array-length v2, v1

    if-lez v2, :cond_d

    .line 1451
    array-length v2, v1

    const/4 v3, 0x0

    const/4 v4, 0x0

    :goto_1
    if-ge v4, v2, :cond_d

    aget-object v5, v1, v4

    .line 1452
    invoke-virtual {v5}, Ljava/lang/reflect/Method;->getParameterTypes()[Ljava/lang/Class;

    move-result-object v6

    if-eqz v6, :cond_c

    .line 1453
    array-length v7, v6

    const/4 v8, 0x2

    if-ne v7, v8, :cond_c

    aget-object v7, v6, v3

    invoke-virtual {v7}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v7

    const-class v9, Ljava/lang/String;

    invoke-virtual {v9}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v9

    invoke-static {v7, v9}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_c

    const/4 v7, 0x1

    aget-object v6, v6, v7

    invoke-virtual {v6}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v6

    const-class v9, Lee/cyber/smartid/tse/dto/TSEError;

    invoke-virtual {v9}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v9

    invoke-static {v6, v9}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_b

    goto :goto_2

    .line 1459
    :cond_b
    :try_start_0
    iget-object v6, p0, Lee/cyber/smartid/tse/SmartIdTSE;->j:Lee/cyber/smartid/tse/inter/LogAccess;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "notifyError: Calling though reflection the method "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/reflect/Method;->getName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-interface {v6, v9}, Lee/cyber/smartid/tse/inter/LogAccess;->d(Ljava/lang/String;)V

    .line 1460
    new-array v6, v8, [Ljava/lang/Object;

    aput-object p1, v6, v3

    aput-object p3, v6, v7

    invoke-virtual {v5, p2, v6}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_3

    :catch_0
    move-exception v5

    .line 1468
    iget-object v6, p0, Lee/cyber/smartid/tse/SmartIdTSE;->j:Lee/cyber/smartid/tse/inter/LogAccess;

    invoke-interface {v6, v0, v5}, Lee/cyber/smartid/tse/inter/LogAccess;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_3

    :catch_1
    move-exception v5

    .line 1466
    iget-object v6, p0, Lee/cyber/smartid/tse/SmartIdTSE;->j:Lee/cyber/smartid/tse/inter/LogAccess;

    invoke-interface {v6, v0, v5}, Lee/cyber/smartid/tse/inter/LogAccess;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_3

    :catch_2
    move-exception v5

    .line 1464
    iget-object v6, p0, Lee/cyber/smartid/tse/SmartIdTSE;->j:Lee/cyber/smartid/tse/inter/LogAccess;

    invoke-interface {v6, v0, v5}, Lee/cyber/smartid/tse/inter/LogAccess;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_3

    :catch_3
    move-exception v5

    .line 1462
    iget-object v6, p0, Lee/cyber/smartid/tse/SmartIdTSE;->j:Lee/cyber/smartid/tse/inter/LogAccess;

    invoke-interface {v6, v0, v5}, Lee/cyber/smartid/tse/inter/LogAccess;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_3

    .line 1454
    :cond_c
    :goto_2
    iget-object v6, p0, Lee/cyber/smartid/tse/SmartIdTSE;->j:Lee/cyber/smartid/tse/inter/LogAccess;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "notifyError: Unusable method "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/reflect/Method;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v6, v5}, Lee/cyber/smartid/tse/inter/LogAccess;->d(Ljava/lang/String;)V

    :goto_3
    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_1

    :cond_d
    :goto_4
    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lee/cyber/smartid/cryptolib/dto/StorageException;
        }
    .end annotation

    .line 1650
    iget-object v0, p0, Lee/cyber/smartid/tse/SmartIdTSE;->c:Lee/cyber/smartid/cryptolib/CryptoLib;

    invoke-virtual {v0, p1, p2}, Lee/cyber/smartid/cryptolib/CryptoLib;->storeData(Ljava/lang/String;Ljava/lang/Object;)V

    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;ZZ)V
    .locals 3

    .line 1730
    iget-object v0, p0, Lee/cyber/smartid/tse/SmartIdTSE;->j:Lee/cyber/smartid/tse/inter/LogAccess;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onHandleAlarm - tag: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, ", targetId: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, ", isInitializeTSEDone: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v2, ", wasInitDoneForThis: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lee/cyber/smartid/tse/inter/LogAccess;->d(Ljava/lang/String;)V

    const-string v0, "ee.cyber.smartid.ACTION_REMOVE_ENCRYPTED_KEYS_"

    .line 1731
    invoke-static {p1, v0}, Lee/cyber/smartid/tse/util/Util;->contains(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    if-eqz p3, :cond_1

    if-eqz p4, :cond_0

    .line 1733
    iget-object p1, p0, Lee/cyber/smartid/tse/SmartIdTSE;->j:Lee/cyber/smartid/tse/inter/LogAccess;

    const-string p2, "onHandleAlarm - no need for trigger ACTION_REMOVE_ENCRYPTED_KEYS, initService was just completed .."

    invoke-interface {p1, p2}, Lee/cyber/smartid/tse/inter/LogAccess;->d(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1735
    :cond_0
    iget-object p1, p0, Lee/cyber/smartid/tse/SmartIdTSE;->j:Lee/cyber/smartid/tse/inter/LogAccess;

    const-string p2, "onHandleAlarm - triggering ACTION_REMOVE_ENCRYPTED_KEYS .."

    invoke-interface {p1, p2}, Lee/cyber/smartid/tse/inter/LogAccess;->d(Ljava/lang/String;)V

    .line 1736
    invoke-direct {p0}, Lee/cyber/smartid/tse/SmartIdTSE;->l()V

    goto/16 :goto_0

    .line 1738
    :cond_1
    invoke-static {p1, v0}, Lee/cyber/smartid/tse/util/Util;->contains(Ljava/lang/String;Ljava/lang/String;)Z

    move-result p4

    if-eqz p4, :cond_2

    if-nez p3, :cond_2

    .line 1739
    iget-object p1, p0, Lee/cyber/smartid/tse/SmartIdTSE;->x:Lee/cyber/smartid/tse/KeyManager;

    invoke-virtual {p1, p2}, Lee/cyber/smartid/tse/KeyManager;->a(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_2
    const-string p4, "android.intent.action.ACTION_SHUTDOWN"

    .line 1740
    invoke-static {p4, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result p4

    if-eqz p4, :cond_3

    .line 1741
    iget-object p1, p0, Lee/cyber/smartid/tse/SmartIdTSE;->j:Lee/cyber/smartid/tse/inter/LogAccess;

    const-string p2, "onHandleAlarm - triggering Intent.ACTION_SHUTDOWN .."

    invoke-interface {p1, p2}, Lee/cyber/smartid/tse/inter/LogAccess;->d(Ljava/lang/String;)V

    .line 1742
    invoke-direct {p0}, Lee/cyber/smartid/tse/SmartIdTSE;->m()V

    goto/16 :goto_0

    :cond_3
    const-string p4, "ee.cyber.smartid.ACTION_REFRESH_CLONE_DETECTION"

    if-eqz p3, :cond_4

    .line 1743
    invoke-static {p4, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1744
    iget-object p1, p0, Lee/cyber/smartid/tse/SmartIdTSE;->j:Lee/cyber/smartid/tse/inter/LogAccess;

    const-string p2, "onHandleAlarm - triggering ACTION_REFRESH_CLONE_DETECTION .."

    invoke-interface {p1, p2}, Lee/cyber/smartid/tse/inter/LogAccess;->d(Ljava/lang/String;)V

    .line 1745
    new-instance p1, Landroid/os/Bundle;

    invoke-direct {p1}, Landroid/os/Bundle;-><init>()V

    const-string p2, "internal-alarm"

    invoke-static {p1, p2}, Lee/cyber/smartid/tse/util/Util;->addXSplitKeyTriggerExtra(Landroid/os/Bundle;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object p1

    invoke-direct {p0, p1}, Lee/cyber/smartid/tse/SmartIdTSE;->b(Landroid/os/Bundle;)V

    goto/16 :goto_0

    :cond_4
    if-nez p3, :cond_5

    .line 1746
    invoke-static {p4, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result p4

    if-eqz p4, :cond_5

    .line 1747
    iget-object p1, p0, Lee/cyber/smartid/tse/SmartIdTSE;->j:Lee/cyber/smartid/tse/inter/LogAccess;

    const-string p2, "onHandleAlarm - can\'t trigger ACTION_REFRESH_CLONE_DETECTION, no init yet .."

    invoke-interface {p1, p2}, Lee/cyber/smartid/tse/inter/LogAccess;->d(Ljava/lang/String;)V

    .line 1750
    :try_start_0
    iget-object p1, p0, Lee/cyber/smartid/tse/SmartIdTSE;->e:Lee/cyber/smartid/tse/SmartIdTSE$TSEAlarmAccess;

    invoke-virtual {p1}, Lee/cyber/smartid/tse/SmartIdTSE$TSEAlarmAccess;->setRefreshCloneDetectionAlarm()V
    :try_end_0
    .catch Lee/cyber/smartid/cryptolib/dto/StorageException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    :catch_0
    move-exception p1

    .line 1752
    iget-object p2, p0, Lee/cyber/smartid/tse/SmartIdTSE;->j:Lee/cyber/smartid/tse/inter/LogAccess;

    const-string p3, "onHandleAlarm"

    invoke-interface {p2, p3, p1}, Lee/cyber/smartid/tse/inter/LogAccess;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    :cond_5
    if-eqz p3, :cond_6

    const-string p4, "android.intent.action.BOOT_COMPLETED"

    .line 1754
    invoke-static {p4, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result p4

    if-eqz p4, :cond_6

    .line 1755
    iget-object p1, p0, Lee/cyber/smartid/tse/SmartIdTSE;->j:Lee/cyber/smartid/tse/inter/LogAccess;

    const-string p2, "onHandleAlarm - triggering ACTION_BOOT_COMPLETED .."

    invoke-interface {p1, p2}, Lee/cyber/smartid/tse/inter/LogAccess;->d(Ljava/lang/String;)V

    .line 1756
    invoke-direct {p0}, Lee/cyber/smartid/tse/SmartIdTSE;->n()V

    goto :goto_0

    :cond_6
    if-eqz p3, :cond_7

    const-string p4, "android.intent.action.MY_PACKAGE_REPLACED"

    .line 1757
    invoke-static {p4, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result p4

    if-eqz p4, :cond_7

    .line 1758
    iget-object p1, p0, Lee/cyber/smartid/tse/SmartIdTSE;->j:Lee/cyber/smartid/tse/inter/LogAccess;

    const-string p2, "onHandleAlarm - triggering ACTION_MY_PACKAGE_REPLACED .."

    invoke-interface {p1, p2}, Lee/cyber/smartid/tse/inter/LogAccess;->d(Ljava/lang/String;)V

    .line 1759
    invoke-direct {p0}, Lee/cyber/smartid/tse/SmartIdTSE;->n()V

    goto :goto_0

    :cond_7
    if-eqz p3, :cond_8

    const-string p4, "ee.cyber.smartid.ACTION_SOLVE_KEY_STATE_"

    .line 1760
    invoke-static {p1, p4}, Lee/cyber/smartid/tse/util/Util;->contains(Ljava/lang/String;Ljava/lang/String;)Z

    move-result p4

    if-eqz p4, :cond_8

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p4

    if-nez p4, :cond_8

    .line 1761
    iget-object p1, p0, Lee/cyber/smartid/tse/SmartIdTSE;->j:Lee/cyber/smartid/tse/inter/LogAccess;

    const-string p3, "onHandleAlarm - triggering ACTION_SOLVE_KEY_STATE .."

    invoke-interface {p1, p3}, Lee/cyber/smartid/tse/inter/LogAccess;->d(Ljava/lang/String;)V

    .line 1762
    invoke-direct {p0, p2}, Lee/cyber/smartid/tse/SmartIdTSE;->d(Ljava/lang/String;)V

    goto :goto_0

    :cond_8
    if-eqz p3, :cond_9

    const-string p3, "ee.cyber.smartid.ACTION_REFRESH_CLONE_DETECTION_SINGLE_KEY"

    .line 1763
    invoke-static {p1, p3}, Lee/cyber/smartid/tse/util/Util;->contains(Ljava/lang/String;Ljava/lang/String;)Z

    move-result p3

    if-eqz p3, :cond_9

    .line 1764
    iget-object p3, p0, Lee/cyber/smartid/tse/SmartIdTSE;->j:Lee/cyber/smartid/tse/inter/LogAccess;

    new-instance p4, Ljava/lang/StringBuilder;

    invoke-direct {p4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "onHandleAlarm - triggering "

    invoke-virtual {p4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, " .."

    invoke-virtual {p4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-interface {p3, p1}, Lee/cyber/smartid/tse/inter/LogAccess;->d(Ljava/lang/String;)V

    .line 1765
    invoke-direct {p0, p2}, Lee/cyber/smartid/tse/SmartIdTSE;->c(Ljava/lang/String;)V

    :cond_9
    :goto_0
    return-void
.end method

.method private a(Ljava/lang/String;Z)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lee/cyber/smartid/cryptolib/dto/StorageException;
        }
    .end annotation

    .line 1685
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 1688
    :cond_0
    iget-object v0, p0, Lee/cyber/smartid/tse/SmartIdTSE;->j:Lee/cyber/smartid/tse/inter/LogAccess;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setActionDoneForThisInstall - key: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, ", isDone: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lee/cyber/smartid/tse/inter/LogAccess;->d(Ljava/lang/String;)V

    if-eqz p2, :cond_1

    .line 1689
    iget-object p2, p0, Lee/cyber/smartid/tse/SmartIdTSE;->b:Landroid/content/Context;

    iget-object v0, p0, Lee/cyber/smartid/tse/SmartIdTSE;->c:Lee/cyber/smartid/cryptolib/CryptoLib;

    invoke-static {p2, v0}, Lee/cyber/smartid/tse/util/Util;->getApkDigestSha256(Landroid/content/Context;Lee/cyber/smartid/cryptolib/CryptoLib;)Ljava/lang/String;

    move-result-object p2

    goto :goto_0

    :cond_1
    const/4 p2, 0x0

    :goto_0
    invoke-direct {p0, p1, p2}, Lee/cyber/smartid/tse/SmartIdTSE;->a(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private a(Z)V
    .locals 0

    .line 1079
    iput-boolean p1, p0, Lee/cyber/smartid/tse/SmartIdTSE;->i:Z

    return-void
.end method

.method static synthetic b(Lee/cyber/smartid/tse/SmartIdTSE;)Landroid/content/Context;
    .locals 0

    .line 101
    iget-object p0, p0, Lee/cyber/smartid/tse/SmartIdTSE;->b:Landroid/content/Context;

    return-object p0
.end method

.method private b(Ljava/util/Properties;)Ljava/util/ArrayList;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Properties;",
            ")",
            "Ljava/util/ArrayList<",
            "Lee/cyber/smartid/tse/dto/KTKPublicKey;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const-string v0, "szKTKPublicKeys"

    .line 1330
    invoke-virtual {p1, v0}, Ljava/util/Properties;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 1331
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 p1, 0x0

    return-object p1

    .line 1335
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    const-string v1, ";"

    .line 1336
    invoke-virtual {p1, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object p1

    .line 1337
    array-length v1, p1

    const/4 v2, 0x0

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v1, :cond_2

    aget-object v4, p1, v3

    const-string v5, ","

    .line 1338
    invoke-virtual {v4, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 1339
    array-length v5, v4

    const/4 v6, 0x3

    const/4 v7, 0x1

    if-ne v5, v6, :cond_1

    aget-object v5, v4, v2

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_1

    aget-object v5, v4, v7

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_1

    const/4 v5, 0x2

    aget-object v6, v4, v5

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_1

    .line 1343
    new-instance v6, Lee/cyber/smartid/tse/dto/KTKPublicKey;

    aget-object v8, v4, v2

    invoke-virtual {v8}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v8

    aget-object v7, v4, v7

    invoke-virtual {v7}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v7

    aget-object v4, v4, v5

    invoke-virtual {v4}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v6, v8, v7, v4}, Lee/cyber/smartid/tse/dto/KTKPublicKey;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 1341
    :cond_1
    new-instance p1, Ljava/io/IOException;

    iget-object v0, p0, Lee/cyber/smartid/tse/SmartIdTSE;->b:Landroid/content/Context;

    sget v1, Lee/cyber/smartid/tse/R$string;->err_invalid_sz_ktk_key_data_value:I

    new-array v3, v7, [Ljava/lang/Object;

    const-string v4, "tse.properties"

    aput-object v4, v3, v2

    invoke-virtual {v0, v1, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_2
    return-object v0
.end method

.method private b()V
    .locals 2

    .line 980
    invoke-virtual {p0}, Lee/cyber/smartid/tse/SmartIdTSE;->isInitializeTSEDone()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 981
    :cond_0
    iget-object v0, p0, Lee/cyber/smartid/tse/SmartIdTSE;->j:Lee/cyber/smartid/tse/inter/LogAccess;

    const-string v1, "throwIfNoInit: Init not yet done!"

    invoke-interface {v0, v1}, Lee/cyber/smartid/tse/inter/LogAccess;->e(Ljava/lang/String;)V

    .line 982
    new-instance v0, Lee/cyber/smartid/tse/dto/InitializationNotCompletedException;

    const-string v1, "SmartIdTSE not initialized. Did you call SmartIdTSE.initializeTSE() first?"

    invoke-direct {v0, v1}, Lee/cyber/smartid/tse/dto/InitializationNotCompletedException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private b(I)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lee/cyber/smartid/cryptolib/dto/StorageException;
        }
    .end annotation

    .line 1057
    iget-object v0, p0, Lee/cyber/smartid/tse/SmartIdTSE;->c:Lee/cyber/smartid/cryptolib/CryptoLib;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    const-string v1, "ee.cyber.smartid.TAG_TSE_STORAGE_VERSION"

    invoke-virtual {v0, v1, p1}, Lee/cyber/smartid/cryptolib/CryptoLib;->storeData(Ljava/lang/String;Ljava/lang/Object;)V

    return-void
.end method

.method private b(Landroid/os/Bundle;)V
    .locals 4

    const-string v0, "handleRefreshCloneDetectionAlarm"

    .line 1850
    :try_start_0
    iget-object v1, p0, Lee/cyber/smartid/tse/SmartIdTSE;->e:Lee/cyber/smartid/tse/SmartIdTSE$TSEAlarmAccess;

    invoke-virtual {v1}, Lee/cyber/smartid/tse/SmartIdTSE$TSEAlarmAccess;->setRefreshCloneDetectionAlarm()V
    :try_end_0
    .catch Lee/cyber/smartid/cryptolib/dto/StorageException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    .line 1853
    iget-object v2, p0, Lee/cyber/smartid/tse/SmartIdTSE;->j:Lee/cyber/smartid/tse/inter/LogAccess;

    invoke-interface {v2, v0, v1}, Lee/cyber/smartid/tse/inter/LogAccess;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1856
    :goto_0
    invoke-virtual {p0}, Lee/cyber/smartid/tse/SmartIdTSE;->isInitializeTSEDone()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1857
    iget-object p1, p0, Lee/cyber/smartid/tse/SmartIdTSE;->j:Lee/cyber/smartid/tse/inter/LogAccess;

    const-string v0, "handleRefreshCloneDetectionAlarm: Init not done yet, aborting!"

    invoke-interface {p1, v0}, Lee/cyber/smartid/tse/inter/LogAccess;->e(Ljava/lang/String;)V

    return-void

    .line 1861
    :cond_0
    invoke-direct {p0}, Lee/cyber/smartid/tse/SmartIdTSE;->e()Landroid/os/PowerManager$WakeLock;

    move-result-object v1

    const-wide/32 v2, 0x1d4c0

    invoke-static {v1, v2, v3}, Lee/cyber/smartid/tse/util/Util;->acquireWakeLock(Landroid/os/PowerManager$WakeLock;J)V

    .line 1863
    :try_start_1
    iget-object v1, p0, Lee/cyber/smartid/tse/SmartIdTSE;->j:Lee/cyber/smartid/tse/inter/LogAccess;

    invoke-interface {v1, v0}, Lee/cyber/smartid/tse/inter/LogAccess;->d(Ljava/lang/String;)V

    .line 1865
    iget-object v1, p0, Lee/cyber/smartid/tse/SmartIdTSE;->y:Lee/cyber/smartid/tse/KeyStateManager;

    invoke-virtual {v1, p1}, Lee/cyber/smartid/tse/KeyStateManager;->a(Landroid/os/Bundle;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception p1

    goto :goto_2

    :catch_1
    move-exception p1

    .line 1867
    :try_start_2
    iget-object v1, p0, Lee/cyber/smartid/tse/SmartIdTSE;->j:Lee/cyber/smartid/tse/inter/LogAccess;

    invoke-interface {v1, v0, p1}, Lee/cyber/smartid/tse/inter/LogAccess;->e(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1869
    :goto_1
    invoke-direct {p0}, Lee/cyber/smartid/tse/SmartIdTSE;->e()Landroid/os/PowerManager$WakeLock;

    move-result-object p1

    invoke-static {p1}, Lee/cyber/smartid/tse/util/Util;->releaseWakeLock(Landroid/os/PowerManager$WakeLock;)V

    return-void

    :goto_2
    invoke-direct {p0}, Lee/cyber/smartid/tse/SmartIdTSE;->e()Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    invoke-static {v0}, Lee/cyber/smartid/tse/util/Util;->releaseWakeLock(Landroid/os/PowerManager$WakeLock;)V

    .line 1870
    throw p1
.end method

.method private b(Z)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lee/cyber/smartid/cryptolib/dto/StorageException;
        }
    .end annotation

    .line 1703
    invoke-direct {p0}, Lee/cyber/smartid/tse/SmartIdTSE;->h()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, p1}, Lee/cyber/smartid/tse/SmartIdTSE;->a(Ljava/lang/String;Z)V

    return-void
.end method

.method private b(Ljava/lang/String;)Z
    .locals 4

    .line 1668
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    return v1

    .line 1671
    :cond_0
    invoke-direct {p0, p1}, Lee/cyber/smartid/tse/SmartIdTSE;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1672
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, p0, Lee/cyber/smartid/tse/SmartIdTSE;->b:Landroid/content/Context;

    iget-object v3, p0, Lee/cyber/smartid/tse/SmartIdTSE;->c:Lee/cyber/smartid/cryptolib/CryptoLib;

    invoke-static {v2, v3}, Lee/cyber/smartid/tse/util/Util;->getApkDigestSha256(Landroid/content/Context;Lee/cyber/smartid/cryptolib/CryptoLib;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v1, 0x1

    .line 1673
    :cond_1
    iget-object v0, p0, Lee/cyber/smartid/tse/SmartIdTSE;->j:Lee/cyber/smartid/tse/inter/LogAccess;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "isActionDoneForThisInstall - key: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, ", isDone: "

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, p1}, Lee/cyber/smartid/tse/inter/LogAccess;->d(Ljava/lang/String;)V

    return v1
.end method

.method private c()I
    .locals 3

    .line 1032
    iget-object v0, p0, Lee/cyber/smartid/tse/SmartIdTSE;->c:Lee/cyber/smartid/cryptolib/CryptoLib;

    new-instance v1, Lee/cyber/smartid/tse/SmartIdTSE$2;

    invoke-direct {v1, p0}, Lee/cyber/smartid/tse/SmartIdTSE$2;-><init>(Lee/cyber/smartid/tse/SmartIdTSE;)V

    .line 1033
    invoke-virtual {v1}, Lee/cyber/smartid/tse/SmartIdTSE$2;->getType()Ljava/lang/reflect/Type;

    move-result-object v1

    const-string v2, "ee.cyber.smartid.TAG_TSE_STORAGE_VERSION"

    .line 1032
    invoke-virtual {v0, v2, v1}, Lee/cyber/smartid/cryptolib/CryptoLib;->retrieveData(Ljava/lang/String;Ljava/lang/reflect/Type;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    if-nez v0, :cond_0

    .line 1037
    iget-object v0, p0, Lee/cyber/smartid/tse/SmartIdTSE;->g:Lee/cyber/smartid/tse/inter/ExternalResourceAccess;

    invoke-interface {v0}, Lee/cyber/smartid/tse/inter/ExternalResourceAccess;->getAppStorageVersion()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 1039
    :cond_0
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method static synthetic c(Lee/cyber/smartid/tse/SmartIdTSE;)J
    .locals 2

    .line 101
    iget-wide v0, p0, Lee/cyber/smartid/tse/SmartIdTSE;->q:J

    return-wide v0
.end method

.method private c(Ljava/lang/String;)V
    .locals 4

    const-string v0, "handleRefreshCloneDetectionSingleKeyAlarm"

    .line 1879
    invoke-virtual {p0}, Lee/cyber/smartid/tse/SmartIdTSE;->isInitializeTSEDone()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1880
    iget-object p1, p0, Lee/cyber/smartid/tse/SmartIdTSE;->j:Lee/cyber/smartid/tse/inter/LogAccess;

    const-string v0, "handleRefreshCloneDetectionSingleKeyAlarm: Init not done yet, aborting!"

    invoke-interface {p1, v0}, Lee/cyber/smartid/tse/inter/LogAccess;->e(Ljava/lang/String;)V

    return-void

    .line 1884
    :cond_0
    invoke-direct {p0}, Lee/cyber/smartid/tse/SmartIdTSE;->e()Landroid/os/PowerManager$WakeLock;

    move-result-object v1

    const-wide/32 v2, 0x1d4c0

    invoke-static {v1, v2, v3}, Lee/cyber/smartid/tse/util/Util;->acquireWakeLock(Landroid/os/PowerManager$WakeLock;J)V

    .line 1886
    :try_start_0
    iget-object v1, p0, Lee/cyber/smartid/tse/SmartIdTSE;->j:Lee/cyber/smartid/tse/inter/LogAccess;

    invoke-interface {v1, v0}, Lee/cyber/smartid/tse/inter/LogAccess;->d(Ljava/lang/String;)V

    .line 1888
    iget-object v1, p0, Lee/cyber/smartid/tse/SmartIdTSE;->y:Lee/cyber/smartid/tse/KeyStateManager;

    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    const-string v3, "internal-alarm"

    invoke-static {v2, v3}, Lee/cyber/smartid/tse/util/Util;->addXSplitKeyTriggerExtra(Landroid/os/Bundle;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {v1, p1, v2}, Lee/cyber/smartid/tse/KeyStateManager;->a(Ljava/lang/String;Landroid/os/Bundle;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception p1

    goto :goto_1

    :catch_0
    move-exception p1

    .line 1890
    :try_start_1
    iget-object v1, p0, Lee/cyber/smartid/tse/SmartIdTSE;->j:Lee/cyber/smartid/tse/inter/LogAccess;

    invoke-interface {v1, v0, p1}, Lee/cyber/smartid/tse/inter/LogAccess;->e(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1892
    :goto_0
    invoke-direct {p0}, Lee/cyber/smartid/tse/SmartIdTSE;->e()Landroid/os/PowerManager$WakeLock;

    move-result-object p1

    invoke-static {p1}, Lee/cyber/smartid/tse/util/Util;->releaseWakeLock(Landroid/os/PowerManager$WakeLock;)V

    return-void

    :goto_1
    invoke-direct {p0}, Lee/cyber/smartid/tse/SmartIdTSE;->e()Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    invoke-static {v0}, Lee/cyber/smartid/tse/util/Util;->releaseWakeLock(Landroid/os/PowerManager$WakeLock;)V

    .line 1893
    throw p1
.end method

.method private c(Z)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lee/cyber/smartid/cryptolib/dto/StorageException;
        }
    .end annotation

    .line 1717
    invoke-direct {p0}, Lee/cyber/smartid/tse/SmartIdTSE;->i()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, p1}, Lee/cyber/smartid/tse/SmartIdTSE;->a(Ljava/lang/String;Z)V

    return-void
.end method

.method static synthetic d(Lee/cyber/smartid/tse/SmartIdTSE;)J
    .locals 2

    .line 101
    iget-wide v0, p0, Lee/cyber/smartid/tse/SmartIdTSE;->z:J

    return-wide v0
.end method

.method private d(Ljava/lang/String;)V
    .locals 3

    .line 1929
    invoke-virtual {p0}, Lee/cyber/smartid/tse/SmartIdTSE;->isInitializeTSEDone()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1930
    iget-object p1, p0, Lee/cyber/smartid/tse/SmartIdTSE;->j:Lee/cyber/smartid/tse/inter/LogAccess;

    const-string v0, "handleKeyStateSolveAlarm - initService is not called, aborting .."

    invoke-interface {p1, v0}, Lee/cyber/smartid/tse/inter/LogAccess;->e(Ljava/lang/String;)V

    return-void

    .line 1932
    :cond_0
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1933
    iget-object p1, p0, Lee/cyber/smartid/tse/SmartIdTSE;->j:Lee/cyber/smartid/tse/inter/LogAccess;

    const-string v0, "handleKeyStateSolveAlarm - no EXTRA_ALARM_TARGET_ID, aborting .."

    invoke-interface {p1, v0}, Lee/cyber/smartid/tse/inter/LogAccess;->e(Ljava/lang/String;)V

    return-void

    .line 1937
    :cond_1
    invoke-direct {p0}, Lee/cyber/smartid/tse/SmartIdTSE;->e()Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    const-wide/32 v1, 0x1d4c0

    invoke-static {v0, v1, v2}, Lee/cyber/smartid/tse/util/Util;->acquireWakeLock(Landroid/os/PowerManager$WakeLock;J)V

    .line 1940
    :try_start_0
    iget-object v0, p0, Lee/cyber/smartid/tse/SmartIdTSE;->j:Lee/cyber/smartid/tse/inter/LogAccess;

    const-string v1, "handleKeyStateSolveAlarm: init done, calling retryKeyStateSolve.."

    invoke-interface {v0, v1}, Lee/cyber/smartid/tse/inter/LogAccess;->d(Ljava/lang/String;)V

    .line 1941
    iget-object v0, p0, Lee/cyber/smartid/tse/SmartIdTSE;->y:Lee/cyber/smartid/tse/KeyStateManager;

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v2, "internal-alarm"

    invoke-static {v1, v2}, Lee/cyber/smartid/tse/util/Util;->addXSplitKeyTriggerExtra(Landroid/os/Bundle;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lee/cyber/smartid/tse/KeyStateManager;->b(Ljava/lang/String;Landroid/os/Bundle;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception p1

    goto :goto_1

    :catch_0
    move-exception p1

    .line 1943
    :try_start_1
    iget-object v0, p0, Lee/cyber/smartid/tse/SmartIdTSE;->j:Lee/cyber/smartid/tse/inter/LogAccess;

    const-string v1, "handleKeyStateSolveAlarm"

    invoke-interface {v0, v1, p1}, Lee/cyber/smartid/tse/inter/LogAccess;->e(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1945
    :goto_0
    invoke-direct {p0}, Lee/cyber/smartid/tse/SmartIdTSE;->e()Landroid/os/PowerManager$WakeLock;

    move-result-object p1

    invoke-static {p1}, Lee/cyber/smartid/tse/util/Util;->releaseWakeLock(Landroid/os/PowerManager$WakeLock;)V

    return-void

    :goto_1
    invoke-direct {p0}, Lee/cyber/smartid/tse/SmartIdTSE;->e()Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    invoke-static {v0}, Lee/cyber/smartid/tse/util/Util;->releaseWakeLock(Landroid/os/PowerManager$WakeLock;)V

    .line 1946
    throw p1
.end method

.method private d()Z
    .locals 3

    .line 1048
    iget-object v0, p0, Lee/cyber/smartid/tse/SmartIdTSE;->c:Lee/cyber/smartid/cryptolib/CryptoLib;

    new-instance v1, Lee/cyber/smartid/tse/SmartIdTSE$3;

    invoke-direct {v1, p0}, Lee/cyber/smartid/tse/SmartIdTSE$3;-><init>(Lee/cyber/smartid/tse/SmartIdTSE;)V

    .line 1049
    invoke-virtual {v1}, Lee/cyber/smartid/tse/SmartIdTSE$3;->getType()Ljava/lang/reflect/Type;

    move-result-object v1

    const-string v2, "ee.cyber.smartid.TAG_TSE_STORAGE_VERSION"

    .line 1048
    invoke-virtual {v0, v2, v1}, Lee/cyber/smartid/cryptolib/CryptoLib;->retrieveData(Ljava/lang/String;Ljava/lang/reflect/Type;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private e()Landroid/os/PowerManager$WakeLock;
    .locals 3

    .line 1088
    iget-object v0, p0, Lee/cyber/smartid/tse/SmartIdTSE;->k:Landroid/os/PowerManager$WakeLock;

    if-nez v0, :cond_1

    .line 1089
    iget-object v0, p0, Lee/cyber/smartid/tse/SmartIdTSE;->b:Landroid/content/Context;

    const-string v1, "power"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    if-eqz v0, :cond_0

    const/4 v1, 0x1

    const-string v2, "ee.cyber.smartid.tse.WAKELOCK_KEY_FOR_SMARTIDTSE"

    .line 1091
    invoke-virtual {v0, v1, v2}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    iput-object v0, p0, Lee/cyber/smartid/tse/SmartIdTSE;->k:Landroid/os/PowerManager$WakeLock;

    .line 1093
    :cond_1
    iget-object v0, p0, Lee/cyber/smartid/tse/SmartIdTSE;->k:Landroid/os/PowerManager$WakeLock;

    return-object v0
.end method

.method static synthetic e(Lee/cyber/smartid/tse/SmartIdTSE;)Lee/cyber/smartid/tse/inter/WallClock;
    .locals 0

    .line 101
    iget-object p0, p0, Lee/cyber/smartid/tse/SmartIdTSE;->h:Lee/cyber/smartid/tse/inter/WallClock;

    return-object p0
.end method

.method private e(Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList<",
            "Lee/cyber/smartid/tse/dto/KTKPublicKey;",
            ">;"
        }
    .end annotation

    .line 2426
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 2427
    iget-object v1, p0, Lee/cyber/smartid/tse/SmartIdTSE;->s:Ljava/util/ArrayList;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_1

    .line 2428
    iget-object v1, p0, Lee/cyber/smartid/tse/SmartIdTSE;->s:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lee/cyber/smartid/tse/dto/KTKPublicKey;

    .line 2429
    invoke-virtual {v2}, Lee/cyber/smartid/tse/dto/KTKPublicKey;->getSzId()Ljava/lang/String;

    move-result-object v3

    invoke-static {p1, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 2430
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_1
    return-object v0
.end method

.method static synthetic f(Lee/cyber/smartid/tse/SmartIdTSE;)Lee/cyber/smartid/cryptolib/CryptoLib;
    .locals 0

    .line 101
    iget-object p0, p0, Lee/cyber/smartid/tse/SmartIdTSE;->c:Lee/cyber/smartid/cryptolib/CryptoLib;

    return-object p0
.end method

.method private f()Ljava/util/Properties;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const-string v0, "tse.properties"

    .line 1189
    new-instance v1, Ljava/util/Properties;

    invoke-direct {v1}, Ljava/util/Properties;-><init>()V

    .line 1193
    :try_start_0
    iget-object v2, p0, Lee/cyber/smartid/tse/SmartIdTSE;->b:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v2

    .line 1194
    invoke-virtual {v1, v2}, Ljava/util/Properties;->load(Ljava/io/InputStream;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1198
    invoke-static {v2}, Lee/cyber/smartid/tse/util/Util;->closeClosable(Ljava/io/Closeable;)V

    return-object v1

    .line 1196
    :catch_0
    new-instance v1, Ljava/io/IOException;

    iget-object v2, p0, Lee/cyber/smartid/tse/SmartIdTSE;->b:Landroid/content/Context;

    sget v3, Lee/cyber/smartid/tse/R$string;->err_failed_to_load_properties_from_x:I

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v0, v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method static synthetic g(Lee/cyber/smartid/tse/SmartIdTSE;)I
    .locals 0

    .line 101
    iget p0, p0, Lee/cyber/smartid/tse/SmartIdTSE;->t:I

    return p0
.end method

.method private g()V
    .locals 13
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1209
    invoke-direct {p0}, Lee/cyber/smartid/tse/SmartIdTSE;->f()Ljava/util/Properties;

    move-result-object v0

    const-string v1, "baseUrl"

    .line 1211
    invoke-virtual {v0, v1}, Ljava/util/Properties;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lee/cyber/smartid/tse/SmartIdTSE;->n:Ljava/lang/String;

    .line 1212
    iget-object v1, p0, Lee/cyber/smartid/tse/SmartIdTSE;->n:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 1213
    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lee/cyber/smartid/tse/SmartIdTSE;->n:Ljava/lang/String;

    .line 1215
    :cond_0
    iget-object v1, p0, Lee/cyber/smartid/tse/SmartIdTSE;->n:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    const-string v2, "tse.properties"

    const/4 v3, 0x1

    const/4 v4, 0x0

    if-nez v1, :cond_7

    .line 1218
    iget-object v1, p0, Lee/cyber/smartid/tse/SmartIdTSE;->j:Lee/cyber/smartid/tse/inter/LogAccess;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "parseProperties - propAPIBaseUrl: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v6, p0, Lee/cyber/smartid/tse/SmartIdTSE;->n:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v1, v5}, Lee/cyber/smartid/tse/inter/LogAccess;->d(Ljava/lang/String;)V

    const-string v1, "allowHTTP"

    .line 1221
    invoke-virtual {v0, v1}, Ljava/util/Properties;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v1

    .line 1223
    iget-object v5, p0, Lee/cyber/smartid/tse/SmartIdTSE;->b:Landroid/content/Context;

    iget-object v6, p0, Lee/cyber/smartid/tse/SmartIdTSE;->n:Ljava/lang/String;

    invoke-static {v5, v6, v1}, Lee/cyber/smartid/tse/util/Util;->validateBaseUrlParams(Landroid/content/Context;Ljava/lang/String;Z)V

    .line 1225
    iput-boolean v1, p0, Lee/cyber/smartid/tse/SmartIdTSE;->v:Z

    .line 1226
    iget-object v1, p0, Lee/cyber/smartid/tse/SmartIdTSE;->j:Lee/cyber/smartid/tse/inter/LogAccess;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "parseProperties - propAllowHttp: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v6, p0, Lee/cyber/smartid/tse/SmartIdTSE;->v:Z

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v1, v5}, Lee/cyber/smartid/tse/inter/LogAccess;->d(Ljava/lang/String;)V

    const/4 v1, 0x2

    const/4 v5, 0x0

    :try_start_0
    const-string v6, "maxAutomaticRetryBackoff"

    .line 1230
    invoke-virtual {v0, v6}, Ljava/util/Properties;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_a

    .line 1231
    :try_start_1
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_1

    invoke-static {v6}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v7

    goto :goto_0

    :cond_1
    const-wide/32 v7, 0x112a880

    :goto_0
    const-wide/16 v9, 0x0

    invoke-static {v7, v8, v9, v10}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v7

    iput-wide v7, p0, Lee/cyber/smartid/tse/SmartIdTSE;->p:J
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_b

    .line 1235
    iget-object v6, p0, Lee/cyber/smartid/tse/SmartIdTSE;->j:Lee/cyber/smartid/tse/inter/LogAccess;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "parseProperties - propMaxAutomaticRetryBackoff: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v11, p0, Lee/cyber/smartid/tse/SmartIdTSE;->p:J

    invoke-virtual {v7, v11, v12}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v8, " ms"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v6, v7}, Lee/cyber/smartid/tse/inter/LogAccess;->d(Ljava/lang/String;)V

    :try_start_2
    const-string v6, "minInteractiveRequestRetryDelay"

    .line 1239
    invoke-virtual {v0, v6}, Ljava/util/Properties;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6
    :try_end_2
    .catch Ljava/lang/NumberFormatException; {:try_start_2 .. :try_end_2} :catch_8

    .line 1240
    :try_start_3
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_2

    invoke-static {v6}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v11

    goto :goto_1

    :cond_2
    const-wide/16 v11, 0x1388

    :goto_1
    invoke-static {v11, v12, v9, v10}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v9

    iput-wide v9, p0, Lee/cyber/smartid/tse/SmartIdTSE;->q:J
    :try_end_3
    .catch Ljava/lang/NumberFormatException; {:try_start_3 .. :try_end_3} :catch_9

    .line 1244
    iget-object v6, p0, Lee/cyber/smartid/tse/SmartIdTSE;->j:Lee/cyber/smartid/tse/inter/LogAccess;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "parseProperties - propMinInteractiveRequestRetryDelay: "

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v9, p0, Lee/cyber/smartid/tse/SmartIdTSE;->q:J

    invoke-virtual {v7, v9, v10}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v6, v7}, Lee/cyber/smartid/tse/inter/LogAccess;->d(Ljava/lang/String;)V

    :try_start_4
    const-string v6, "refreshCloneDetectionInterval"

    .line 1248
    invoke-virtual {v0, v6}, Ljava/util/Properties;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6
    :try_end_4
    .catch Ljava/lang/NumberFormatException; {:try_start_4 .. :try_end_4} :catch_6

    .line 1249
    :try_start_5
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_3

    invoke-static {v6}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v9

    goto :goto_2

    :cond_3
    const-wide/32 v9, 0x240c8400

    :goto_2
    const-wide/16 v11, 0x7530

    invoke-static {v9, v10, v11, v12}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v9

    iput-wide v9, p0, Lee/cyber/smartid/tse/SmartIdTSE;->r:J
    :try_end_5
    .catch Ljava/lang/NumberFormatException; {:try_start_5 .. :try_end_5} :catch_7

    .line 1253
    iget-object v6, p0, Lee/cyber/smartid/tse/SmartIdTSE;->j:Lee/cyber/smartid/tse/inter/LogAccess;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "parseProperties - propRefreshCloneDetectionInterval: "

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v9, p0, Lee/cyber/smartid/tse/SmartIdTSE;->r:J

    invoke-virtual {v7, v9, v10}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v6, v7}, Lee/cyber/smartid/tse/inter/LogAccess;->d(Ljava/lang/String;)V

    .line 1256
    :try_start_6
    invoke-direct {p0, v0}, Lee/cyber/smartid/tse/SmartIdTSE;->a(Ljava/util/Properties;)Ljava/util/ArrayList;

    move-result-object v6

    iput-object v6, p0, Lee/cyber/smartid/tse/SmartIdTSE;->o:Ljava/util/ArrayList;
    :try_end_6
    .catch Ljava/lang/Throwable; {:try_start_6 .. :try_end_6} :catch_5

    .line 1260
    iget-object v6, p0, Lee/cyber/smartid/tse/SmartIdTSE;->j:Lee/cyber/smartid/tse/inter/LogAccess;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "parseProperties - propAPIPins count: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v8, p0, Lee/cyber/smartid/tse/SmartIdTSE;->o:Ljava/util/ArrayList;

    if-eqz v8, :cond_4

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v8

    invoke-static {v8}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v8

    goto :goto_3

    :cond_4
    const-string v8, "0"

    :goto_3
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v6, v7}, Lee/cyber/smartid/tse/inter/LogAccess;->d(Ljava/lang/String;)V

    .line 1263
    :try_start_7
    invoke-direct {p0, v0}, Lee/cyber/smartid/tse/SmartIdTSE;->b(Ljava/util/Properties;)Ljava/util/ArrayList;

    move-result-object v6

    iput-object v6, p0, Lee/cyber/smartid/tse/SmartIdTSE;->s:Ljava/util/ArrayList;
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_4
    .catch Ljava/lang/Throwable; {:try_start_7 .. :try_end_7} :catch_3

    .line 1270
    iget-object v6, p0, Lee/cyber/smartid/tse/SmartIdTSE;->s:Ljava/util/ArrayList;

    if-eqz v6, :cond_6

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-eqz v6, :cond_6

    .line 1273
    iget-object v6, p0, Lee/cyber/smartid/tse/SmartIdTSE;->j:Lee/cyber/smartid/tse/inter/LogAccess;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "parseProperties - SZ KTK key count: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v8, p0, Lee/cyber/smartid/tse/SmartIdTSE;->s:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v6, v7}, Lee/cyber/smartid/tse/inter/LogAccess;->d(Ljava/lang/String;)V

    :try_start_8
    const-string v6, "prngTestLoopCount"

    .line 1277
    invoke-virtual {v0, v6}, Ljava/util/Properties;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6
    :try_end_8
    .catch Ljava/lang/NumberFormatException; {:try_start_8 .. :try_end_8} :catch_1

    .line 1278
    :try_start_9
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_5

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v7

    goto :goto_4

    :cond_5
    const/16 v7, 0x19

    :goto_4
    invoke-static {v7, v4}, Ljava/lang/Math;->max(II)I

    move-result v7

    iput v7, p0, Lee/cyber/smartid/tse/SmartIdTSE;->t:I
    :try_end_9
    .catch Ljava/lang/NumberFormatException; {:try_start_9 .. :try_end_9} :catch_2

    .line 1282
    iget-object v6, p0, Lee/cyber/smartid/tse/SmartIdTSE;->j:Lee/cyber/smartid/tse/inter/LogAccess;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "parseProperties - propPRNGTestLoopCount: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v8, p0, Lee/cyber/smartid/tse/SmartIdTSE;->t:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v6, v7}, Lee/cyber/smartid/tse/inter/LogAccess;->d(Ljava/lang/String;)V

    :try_start_a
    const-string v6, "prngTestSuppressFailure"

    .line 1286
    invoke-virtual {v0, v6}, Ljava/util/Properties;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 1287
    invoke-static {v5}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lee/cyber/smartid/tse/SmartIdTSE;->u:Z
    :try_end_a
    .catch Ljava/lang/Throwable; {:try_start_a .. :try_end_a} :catch_0

    .line 1291
    iget-object v0, p0, Lee/cyber/smartid/tse/SmartIdTSE;->j:Lee/cyber/smartid/tse/inter/LogAccess;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "parseProperties - propPRNGTestSuppressFailure: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v2, p0, Lee/cyber/smartid/tse/SmartIdTSE;->u:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lee/cyber/smartid/tse/inter/LogAccess;->d(Ljava/lang/String;)V

    return-void

    .line 1289
    :catch_0
    new-instance v0, Ljava/io/IOException;

    iget-object v6, p0, Lee/cyber/smartid/tse/SmartIdTSE;->b:Landroid/content/Context;

    sget v7, Lee/cyber/smartid/tse/R$string;->err_invalid_prng_suppress_failure_boolean:I

    new-array v1, v1, [Ljava/lang/Object;

    aput-object v2, v1, v4

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-virtual {v6, v7, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :catch_1
    move-object v6, v5

    .line 1280
    :catch_2
    new-instance v0, Ljava/io/IOException;

    iget-object v5, p0, Lee/cyber/smartid/tse/SmartIdTSE;->b:Landroid/content/Context;

    sget v7, Lee/cyber/smartid/tse/R$string;->err_invalid_prng_test_loop_count:I

    new-array v1, v1, [Ljava/lang/Object;

    aput-object v2, v1, v4

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-virtual {v5, v7, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1271
    :cond_6
    new-instance v0, Ljava/io/IOException;

    iget-object v1, p0, Lee/cyber/smartid/tse/SmartIdTSE;->b:Landroid/content/Context;

    sget v5, Lee/cyber/smartid/tse/R$string;->err_invalid_sz_ktk_keys_value:I

    new-array v3, v3, [Ljava/lang/Object;

    aput-object v2, v3, v4

    invoke-virtual {v1, v5, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1268
    :catch_3
    new-instance v0, Ljava/io/IOException;

    iget-object v1, p0, Lee/cyber/smartid/tse/SmartIdTSE;->b:Landroid/content/Context;

    sget v5, Lee/cyber/smartid/tse/R$string;->err_invalid_sz_ktk_keys_value:I

    new-array v3, v3, [Ljava/lang/Object;

    aput-object v2, v3, v4

    invoke-virtual {v1, v5, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :catch_4
    move-exception v0

    .line 1266
    throw v0

    .line 1258
    :catch_5
    new-instance v0, Ljava/io/IOException;

    iget-object v1, p0, Lee/cyber/smartid/tse/SmartIdTSE;->b:Landroid/content/Context;

    sget v5, Lee/cyber/smartid/tse/R$string;->err_invalid_pins_value:I

    new-array v3, v3, [Ljava/lang/Object;

    aput-object v2, v3, v4

    invoke-virtual {v1, v5, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :catch_6
    move-object v6, v5

    .line 1251
    :catch_7
    new-instance v0, Ljava/io/IOException;

    iget-object v5, p0, Lee/cyber/smartid/tse/SmartIdTSE;->b:Landroid/content/Context;

    sget v7, Lee/cyber/smartid/tse/R$string;->err_invalid_clone_detection_interval:I

    new-array v1, v1, [Ljava/lang/Object;

    aput-object v2, v1, v4

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-virtual {v5, v7, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :catch_8
    move-object v6, v5

    .line 1242
    :catch_9
    new-instance v0, Ljava/io/IOException;

    iget-object v5, p0, Lee/cyber/smartid/tse/SmartIdTSE;->b:Landroid/content/Context;

    sget v7, Lee/cyber/smartid/tse/R$string;->err_invalid_interactive_delay:I

    new-array v1, v1, [Ljava/lang/Object;

    aput-object v2, v1, v4

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-virtual {v5, v7, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :catch_a
    move-object v6, v5

    .line 1233
    :catch_b
    new-instance v0, Ljava/io/IOException;

    iget-object v5, p0, Lee/cyber/smartid/tse/SmartIdTSE;->b:Landroid/content/Context;

    sget v7, Lee/cyber/smartid/tse/R$string;->err_invalid_automatic_backoff_value:I

    new-array v1, v1, [Ljava/lang/Object;

    aput-object v2, v1, v4

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-virtual {v5, v7, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1216
    :cond_7
    new-instance v0, Ljava/io/IOException;

    iget-object v1, p0, Lee/cyber/smartid/tse/SmartIdTSE;->b:Landroid/content/Context;

    sget v5, Lee/cyber/smartid/tse/R$string;->err_no_base_url:I

    new-array v3, v3, [Ljava/lang/Object;

    aput-object v2, v3, v4

    invoke-virtual {v1, v5, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static getInstance(Landroid/content/Context;Lee/cyber/smartid/tse/inter/ExternalResourceAccess;Lee/cyber/smartid/tse/inter/AlarmAccess;Lee/cyber/smartid/tse/inter/WallClock;Lee/cyber/smartid/tse/inter/LogAccess;)Lee/cyber/smartid/tse/SmartIdTSE;
    .locals 8

    .line 776
    sget-object v0, Lee/cyber/smartid/tse/SmartIdTSE;->a:Lee/cyber/smartid/tse/SmartIdTSE;

    if-nez v0, :cond_1

    .line 777
    const-class v0, Lee/cyber/smartid/tse/SmartIdTSE;

    monitor-enter v0

    .line 778
    :try_start_0
    sget-object v1, Lee/cyber/smartid/tse/SmartIdTSE;->a:Lee/cyber/smartid/tse/SmartIdTSE;

    if-nez v1, :cond_0

    .line 779
    new-instance v1, Lee/cyber/smartid/tse/SmartIdTSE;

    move-object v2, v1

    move-object v3, p0

    move-object v4, p1

    move-object v5, p2

    move-object v6, p3

    move-object v7, p4

    invoke-direct/range {v2 .. v7}, Lee/cyber/smartid/tse/SmartIdTSE;-><init>(Landroid/content/Context;Lee/cyber/smartid/tse/inter/ExternalResourceAccess;Lee/cyber/smartid/tse/inter/AlarmAccess;Lee/cyber/smartid/tse/inter/WallClock;Lee/cyber/smartid/tse/inter/LogAccess;)V

    sput-object v1, Lee/cyber/smartid/tse/SmartIdTSE;->a:Lee/cyber/smartid/tse/SmartIdTSE;

    .line 781
    :cond_0
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception p0

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p0

    .line 783
    :cond_1
    :goto_0
    sget-object p0, Lee/cyber/smartid/tse/SmartIdTSE;->a:Lee/cyber/smartid/tse/SmartIdTSE;

    return-object p0
.end method

.method static synthetic h(Lee/cyber/smartid/tse/SmartIdTSE;)J
    .locals 2

    .line 101
    iget-wide v0, p0, Lee/cyber/smartid/tse/SmartIdTSE;->r:J

    return-wide v0
.end method

.method private h()Ljava/lang/String;
    .locals 2

    .line 1654
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ee.cyber.smartid.PREF_IS_REFRESH_CLONE_DETECTION_ALARM_SCHEDULED_DIGEST_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lee/cyber/smartid/tse/SmartIdTSE;->g:Lee/cyber/smartid/tse/inter/ExternalResourceAccess;

    invoke-interface {v1}, Lee/cyber/smartid/tse/inter/ExternalResourceAccess;->getDeviceFingerPrint()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private i()Ljava/lang/String;
    .locals 2

    .line 1658
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ee.cyber.smartid.PREF_IS_POST_BOOT_KEY_STATE_CHECK_DONE_DIGEST_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lee/cyber/smartid/tse/SmartIdTSE;->g:Lee/cyber/smartid/tse/inter/ExternalResourceAccess;

    invoke-interface {v1}, Lee/cyber/smartid/tse/inter/ExternalResourceAccess;->getDeviceFingerPrint()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic i(Lee/cyber/smartid/tse/SmartIdTSE;)Z
    .locals 0

    .line 101
    iget-boolean p0, p0, Lee/cyber/smartid/tse/SmartIdTSE;->u:Z

    return p0
.end method

.method static synthetic j(Lee/cyber/smartid/tse/SmartIdTSE;)Lee/cyber/smartid/tse/inter/ResourceAccess;
    .locals 0

    .line 101
    iget-object p0, p0, Lee/cyber/smartid/tse/SmartIdTSE;->f:Lee/cyber/smartid/tse/inter/ResourceAccess;

    return-object p0
.end method

.method private j()Z
    .locals 1

    .line 1698
    invoke-direct {p0}, Lee/cyber/smartid/tse/SmartIdTSE;->h()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lee/cyber/smartid/tse/SmartIdTSE;->b(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static synthetic k(Lee/cyber/smartid/tse/SmartIdTSE;)Lee/cyber/smartid/tse/inter/LogAccess;
    .locals 0

    .line 101
    iget-object p0, p0, Lee/cyber/smartid/tse/SmartIdTSE;->j:Lee/cyber/smartid/tse/inter/LogAccess;

    return-object p0
.end method

.method private k()Z
    .locals 1

    .line 1712
    invoke-direct {p0}, Lee/cyber/smartid/tse/SmartIdTSE;->i()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lee/cyber/smartid/tse/SmartIdTSE;->b(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static synthetic l(Lee/cyber/smartid/tse/SmartIdTSE;)Ljava/util/WeakHashMap;
    .locals 0

    .line 101
    iget-object p0, p0, Lee/cyber/smartid/tse/SmartIdTSE;->m:Ljava/util/WeakHashMap;

    return-object p0
.end method

.method private l()V
    .locals 3

    .line 1775
    invoke-direct {p0}, Lee/cyber/smartid/tse/SmartIdTSE;->e()Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    const-wide/32 v1, 0x1d4c0

    invoke-static {v0, v1, v2}, Lee/cyber/smartid/tse/util/Util;->acquireWakeLock(Landroid/os/PowerManager$WakeLock;J)V

    .line 1778
    :try_start_0
    iget-object v0, p0, Lee/cyber/smartid/tse/SmartIdTSE;->j:Lee/cyber/smartid/tse/inter/LogAccess;

    const-string v1, "handleRemoveEncryptedKeysAlarm: init done, calling retryKeyStateSolve.."

    invoke-interface {v0, v1}, Lee/cyber/smartid/tse/inter/LogAccess;->d(Ljava/lang/String;)V

    .line 1779
    iget-object v0, p0, Lee/cyber/smartid/tse/SmartIdTSE;->x:Lee/cyber/smartid/tse/KeyManager;

    invoke-virtual {v0}, Lee/cyber/smartid/tse/KeyManager;->removeAllExpiredEncryptedKeys()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    goto :goto_1

    :catch_0
    move-exception v0

    .line 1781
    :try_start_1
    iget-object v1, p0, Lee/cyber/smartid/tse/SmartIdTSE;->j:Lee/cyber/smartid/tse/inter/LogAccess;

    const-string v2, "handleRemoveEncryptedKeysAlarm"

    invoke-interface {v1, v2, v0}, Lee/cyber/smartid/tse/inter/LogAccess;->e(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1783
    :goto_0
    invoke-direct {p0}, Lee/cyber/smartid/tse/SmartIdTSE;->e()Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    invoke-static {v0}, Lee/cyber/smartid/tse/util/Util;->releaseWakeLock(Landroid/os/PowerManager$WakeLock;)V

    return-void

    :goto_1
    invoke-direct {p0}, Lee/cyber/smartid/tse/SmartIdTSE;->e()Landroid/os/PowerManager$WakeLock;

    move-result-object v1

    invoke-static {v1}, Lee/cyber/smartid/tse/util/Util;->releaseWakeLock(Landroid/os/PowerManager$WakeLock;)V

    .line 1784
    throw v0
.end method

.method private m()V
    .locals 4

    const-string v0, "handleSystemShutdown "

    .line 1793
    invoke-direct {p0}, Lee/cyber/smartid/tse/SmartIdTSE;->e()Landroid/os/PowerManager$WakeLock;

    move-result-object v1

    const-wide/32 v2, 0x1d4c0

    invoke-static {v1, v2, v3}, Lee/cyber/smartid/tse/util/Util;->acquireWakeLock(Landroid/os/PowerManager$WakeLock;J)V

    .line 1796
    :try_start_0
    iget-object v1, p0, Lee/cyber/smartid/tse/SmartIdTSE;->j:Lee/cyber/smartid/tse/inter/LogAccess;

    const-string v2, "handleSystemShutdown"

    invoke-interface {v1, v2}, Lee/cyber/smartid/tse/inter/LogAccess;->d(Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 1798
    invoke-direct {p0, v1}, Lee/cyber/smartid/tse/SmartIdTSE;->b(Z)V

    .line 1799
    invoke-direct {p0, v1}, Lee/cyber/smartid/tse/SmartIdTSE;->c(Z)V
    :try_end_0
    .catch Lee/cyber/smartid/cryptolib/dto/StorageException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    goto :goto_1

    :catch_0
    move-exception v1

    .line 1804
    :try_start_1
    iget-object v2, p0, Lee/cyber/smartid/tse/SmartIdTSE;->j:Lee/cyber/smartid/tse/inter/LogAccess;

    invoke-interface {v2, v0, v1}, Lee/cyber/smartid/tse/inter/LogAccess;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    :catch_1
    move-exception v1

    .line 1802
    iget-object v2, p0, Lee/cyber/smartid/tse/SmartIdTSE;->j:Lee/cyber/smartid/tse/inter/LogAccess;

    invoke-interface {v2, v0, v1}, Lee/cyber/smartid/tse/inter/LogAccess;->e(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1807
    :goto_0
    invoke-direct {p0}, Lee/cyber/smartid/tse/SmartIdTSE;->e()Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    invoke-static {v0}, Lee/cyber/smartid/tse/util/Util;->releaseWakeLock(Landroid/os/PowerManager$WakeLock;)V

    return-void

    :goto_1
    invoke-direct {p0}, Lee/cyber/smartid/tse/SmartIdTSE;->e()Landroid/os/PowerManager$WakeLock;

    move-result-object v1

    invoke-static {v1}, Lee/cyber/smartid/tse/util/Util;->releaseWakeLock(Landroid/os/PowerManager$WakeLock;)V

    .line 1808
    throw v0
.end method

.method private n()V
    .locals 5

    const-string v0, "internal-setup"

    const-string v1, "handleBootCompletedOrPackageReplaced "

    .line 1902
    invoke-direct {p0}, Lee/cyber/smartid/tse/SmartIdTSE;->e()Landroid/os/PowerManager$WakeLock;

    move-result-object v2

    const-wide/32 v3, 0x1d4c0

    invoke-static {v2, v3, v4}, Lee/cyber/smartid/tse/util/Util;->acquireWakeLock(Landroid/os/PowerManager$WakeLock;J)V

    .line 1905
    :try_start_0
    iget-object v2, p0, Lee/cyber/smartid/tse/SmartIdTSE;->j:Lee/cyber/smartid/tse/inter/LogAccess;

    const-string v3, "handleBootCompletedOrPackageReplaced"

    invoke-interface {v2, v3}, Lee/cyber/smartid/tse/inter/LogAccess;->d(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v2, 0x0

    .line 1907
    :try_start_1
    invoke-direct {p0, v2}, Lee/cyber/smartid/tse/SmartIdTSE;->b(Z)V

    .line 1908
    invoke-direct {p0, v2}, Lee/cyber/smartid/tse/SmartIdTSE;->c(Z)V
    :try_end_1
    .catch Lee/cyber/smartid/cryptolib/dto/StorageException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v2

    .line 1911
    :try_start_2
    iget-object v3, p0, Lee/cyber/smartid/tse/SmartIdTSE;->j:Lee/cyber/smartid/tse/inter/LogAccess;

    invoke-interface {v3, v1, v2}, Lee/cyber/smartid/tse/inter/LogAccess;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1913
    :goto_0
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    invoke-static {v2, v0}, Lee/cyber/smartid/tse/util/Util;->addXSplitKeyTriggerExtra(Landroid/os/Bundle;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v2

    invoke-direct {p0, v2}, Lee/cyber/smartid/tse/SmartIdTSE;->a(Landroid/os/Bundle;)V

    .line 1914
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    invoke-static {v2, v0}, Lee/cyber/smartid/tse/util/Util;->addXSplitKeyTriggerExtra(Landroid/os/Bundle;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    invoke-direct {p0, v0}, Lee/cyber/smartid/tse/SmartIdTSE;->b(Landroid/os/Bundle;)V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    goto :goto_2

    :catch_1
    move-exception v0

    .line 1916
    :try_start_3
    iget-object v2, p0, Lee/cyber/smartid/tse/SmartIdTSE;->j:Lee/cyber/smartid/tse/inter/LogAccess;

    invoke-interface {v2, v1, v0}, Lee/cyber/smartid/tse/inter/LogAccess;->e(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1918
    :goto_1
    invoke-direct {p0}, Lee/cyber/smartid/tse/SmartIdTSE;->e()Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    invoke-static {v0}, Lee/cyber/smartid/tse/util/Util;->releaseWakeLock(Landroid/os/PowerManager$WakeLock;)V

    return-void

    :goto_2
    invoke-direct {p0}, Lee/cyber/smartid/tse/SmartIdTSE;->e()Landroid/os/PowerManager$WakeLock;

    move-result-object v1

    invoke-static {v1}, Lee/cyber/smartid/tse/util/Util;->releaseWakeLock(Landroid/os/PowerManager$WakeLock;)V

    .line 1919
    throw v0
.end method


# virtual methods
.method public addKeyMaterialValuesForServer(Ljava/lang/String;Lee/cyber/smartid/tse/inter/KeyMaterialApplier;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lee/cyber/smartid/tse/dto/NoSuchKeysException;
        }
    .end annotation

    .line 2247
    invoke-direct {p0}, Lee/cyber/smartid/tse/SmartIdTSE;->b()V

    .line 2249
    iget-object v0, p0, Lee/cyber/smartid/tse/SmartIdTSE;->x:Lee/cyber/smartid/tse/KeyManager;

    invoke-virtual {v0, p1, p2}, Lee/cyber/smartid/tse/KeyManager;->a(Ljava/lang/String;Lee/cyber/smartid/tse/inter/KeyMaterialApplier;)V

    return-void
.end method

.method public checkLocalPendingState(Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/tse/inter/CheckLocalPendingStateListener;)V
    .locals 1

    .line 2023
    invoke-direct {p0}, Lee/cyber/smartid/tse/SmartIdTSE;->b()V

    .line 2025
    iget-object v0, p0, Lee/cyber/smartid/tse/SmartIdTSE;->y:Lee/cyber/smartid/tse/KeyStateManager;

    invoke-virtual {v0, p1, p2, p3}, Lee/cyber/smartid/tse/KeyStateManager;->checkLocalPendingState(Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/tse/inter/CheckLocalPendingStateListener;)V

    return-void
.end method

.method public confirmTransaction(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/tse/inter/ConfirmTransactionListener;)V
    .locals 15

    move-object v0, p0

    .line 2000
    invoke-direct {p0}, Lee/cyber/smartid/tse/SmartIdTSE;->b()V

    .line 2001
    iget-object v1, v0, Lee/cyber/smartid/tse/SmartIdTSE;->j:Lee/cyber/smartid/tse/inter/LogAccess;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "confirmTransaction - "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-object/from16 v3, p1

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lee/cyber/smartid/tse/inter/LogAccess;->d(Ljava/lang/String;)V

    .line 2003
    iget-object v4, v0, Lee/cyber/smartid/tse/SmartIdTSE;->y:Lee/cyber/smartid/tse/KeyStateManager;

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v2, "external"

    invoke-static {v1, v2}, Lee/cyber/smartid/tse/util/Util;->addXSplitKeyTriggerExtra(Landroid/os/Bundle;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v13

    move-object/from16 v5, p1

    move-object/from16 v6, p2

    move-object/from16 v7, p3

    move-object/from16 v8, p4

    move-object/from16 v9, p5

    move-object/from16 v10, p6

    move-object/from16 v11, p7

    move-object/from16 v12, p8

    move-object/from16 v14, p9

    invoke-virtual/range {v4 .. v14}, Lee/cyber/smartid/tse/KeyStateManager;->confirmTransaction(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Lee/cyber/smartid/tse/inter/ConfirmTransactionListener;)V

    return-void
.end method

.method public consumeFreshnessToken(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lee/cyber/smartid/cryptolib/dto/StorageException;
        }
    .end annotation

    .line 1553
    invoke-direct {p0}, Lee/cyber/smartid/tse/SmartIdTSE;->b()V

    .line 1554
    iget-object v0, p0, Lee/cyber/smartid/tse/SmartIdTSE;->c:Lee/cyber/smartid/cryptolib/CryptoLib;

    monitor-enter v0

    .line 1555
    :try_start_0
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 p1, 0x0

    .line 1556
    monitor-exit v0

    return-object p1

    .line 1558
    :cond_0
    iget-object v1, p0, Lee/cyber/smartid/tse/SmartIdTSE;->x:Lee/cyber/smartid/tse/KeyManager;

    iget-object v2, p0, Lee/cyber/smartid/tse/SmartIdTSE;->f:Lee/cyber/smartid/tse/inter/ResourceAccess;

    invoke-interface {v2, p1}, Lee/cyber/smartid/tse/inter/ResourceAccess;->getKeyStateIdByKeyId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Lee/cyber/smartid/tse/KeyManager;->consumeFreshnessToken(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    monitor-exit v0

    return-object p1

    :catchall_0
    move-exception p1

    .line 1559
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method public createAPI(Ljava/lang/Class;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class<",
            "TT;>;)TT;"
        }
    .end annotation

    .line 2067
    invoke-direct {p0}, Lee/cyber/smartid/tse/SmartIdTSE;->b()V

    .line 2068
    iget-object v0, p0, Lee/cyber/smartid/tse/SmartIdTSE;->w:Lee/cyber/smartid/tse/network/TSEAPI;

    invoke-virtual {v0, p1}, Lee/cyber/smartid/tse/network/TSEAPI;->createAPI(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public createRequestClient()Lokhttp3/w$a;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1178
    invoke-direct {p0}, Lee/cyber/smartid/tse/SmartIdTSE;->b()V

    .line 1179
    iget-object v0, p0, Lee/cyber/smartid/tse/SmartIdTSE;->b:Landroid/content/Context;

    iget-object v1, p0, Lee/cyber/smartid/tse/SmartIdTSE;->n:Ljava/lang/String;

    iget-boolean v2, p0, Lee/cyber/smartid/tse/SmartIdTSE;->v:Z

    iget-object v3, p0, Lee/cyber/smartid/tse/SmartIdTSE;->o:Ljava/util/ArrayList;

    invoke-static {v0, v1, v2, v3}, Lee/cyber/smartid/tse/network/TSEAPI;->createRequestClient(Landroid/content/Context;Ljava/lang/String;ZLjava/util/ArrayList;)Lokhttp3/w$a;

    move-result-object v0

    return-object v0
.end method

.method public deleteKeyAndRelatedObjects(Ljava/lang/String;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lee/cyber/smartid/cryptolib/dto/StorageException;
        }
    .end annotation

    .line 1626
    invoke-direct {p0}, Lee/cyber/smartid/tse/SmartIdTSE;->b()V

    .line 1627
    iget-object v0, p0, Lee/cyber/smartid/tse/SmartIdTSE;->x:Lee/cyber/smartid/tse/KeyManager;

    invoke-virtual {v0, p1}, Lee/cyber/smartid/tse/KeyManager;->deleteKeyAndRelatedObjects(Ljava/lang/String;)V

    .line 1628
    iget-object v0, p0, Lee/cyber/smartid/tse/SmartIdTSE;->e:Lee/cyber/smartid/tse/SmartIdTSE$TSEAlarmAccess;

    iget-object v1, p0, Lee/cyber/smartid/tse/SmartIdTSE;->f:Lee/cyber/smartid/tse/inter/ResourceAccess;

    invoke-interface {v1, p1}, Lee/cyber/smartid/tse/inter/ResourceAccess;->getKeyStateIdByKeyId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lee/cyber/smartid/tse/SmartIdTSE$TSEAlarmAccess;->clearKeyStateSolveAlarm(Ljava/lang/String;)V

    return-void
.end method

.method public deleteKeysAndRelatedObjects(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lee/cyber/smartid/cryptolib/dto/StorageException;
        }
    .end annotation

    .line 1604
    invoke-direct {p0}, Lee/cyber/smartid/tse/SmartIdTSE;->b()V

    .line 1607
    invoke-virtual {p0, p1}, Lee/cyber/smartid/tse/SmartIdTSE;->deleteKeyAndRelatedObjects(Ljava/lang/String;)V

    .line 1608
    invoke-virtual {p0, p2}, Lee/cyber/smartid/tse/SmartIdTSE;->deleteKeyAndRelatedObjects(Ljava/lang/String;)V

    return-void
.end method

.method public encryptKey(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/tse/inter/EncryptKeyListener;)V
    .locals 6

    .line 2177
    invoke-direct {p0}, Lee/cyber/smartid/tse/SmartIdTSE;->b()V

    .line 2179
    iget-object v0, p0, Lee/cyber/smartid/tse/SmartIdTSE;->x:Lee/cyber/smartid/tse/KeyManager;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-virtual/range {v0 .. v5}, Lee/cyber/smartid/tse/KeyManager;->encryptKey(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/tse/inter/EncryptKeyListener;)V

    return-void
.end method

.method public generateDiffieHellmanKeyPairForTSEKey(Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/tse/inter/GenerateDiffieHellmanKeyPairListener;)V
    .locals 1

    .line 2199
    invoke-direct {p0}, Lee/cyber/smartid/tse/SmartIdTSE;->b()V

    .line 2201
    iget-object v0, p0, Lee/cyber/smartid/tse/SmartIdTSE;->x:Lee/cyber/smartid/tse/KeyManager;

    invoke-virtual {v0, p1, p2, p3}, Lee/cyber/smartid/tse/KeyManager;->a(Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/tse/inter/GenerateDiffieHellmanKeyPairListener;)V

    return-void
.end method

.method public generateKeys(Ljava/lang/String;IILjava/math/BigInteger;ILee/cyber/smartid/tse/inter/GenerateKeysListener;)V
    .locals 8

    .line 2110
    invoke-direct {p0}, Lee/cyber/smartid/tse/SmartIdTSE;->b()V

    .line 2112
    iget-object v0, p0, Lee/cyber/smartid/tse/SmartIdTSE;->x:Lee/cyber/smartid/tse/KeyManager;

    iget-object v6, p0, Lee/cyber/smartid/tse/SmartIdTSE;->h:Lee/cyber/smartid/tse/inter/WallClock;

    move-object v1, p1

    move v2, p2

    move v3, p3

    move-object v4, p4

    move v5, p5

    move-object v7, p6

    invoke-virtual/range {v0 .. v7}, Lee/cyber/smartid/tse/KeyManager;->generateKeys(Ljava/lang/String;IILjava/math/BigInteger;ILee/cyber/smartid/tse/inter/WallClock;Lee/cyber/smartid/tse/inter/GenerateKeysListener;)V

    return-void
.end method

.method public generateVerificationCode(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .line 2395
    invoke-direct {p0}, Lee/cyber/smartid/tse/SmartIdTSE;->b()V

    .line 2396
    iget-object v0, p0, Lee/cyber/smartid/tse/SmartIdTSE;->c:Lee/cyber/smartid/cryptolib/CryptoLib;

    invoke-virtual {v0, p1}, Lee/cyber/smartid/cryptolib/CryptoLib;->generateVerificationCode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public getAutomaticRequestRetryDelay(I)J
    .locals 2

    .line 1376
    invoke-direct {p0}, Lee/cyber/smartid/tse/SmartIdTSE;->b()V

    .line 1377
    iget-wide v0, p0, Lee/cyber/smartid/tse/SmartIdTSE;->p:J

    invoke-static {p1, v0, v1}, Lee/cyber/smartid/tse/SmartIdTSE;->a(IJ)J

    move-result-wide v0

    return-wide v0
.end method

.method public getKeyPinLength(Ljava/lang/String;)I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lee/cyber/smartid/tse/dto/NoSuchKeysException;
        }
    .end annotation

    .line 2267
    invoke-direct {p0}, Lee/cyber/smartid/tse/SmartIdTSE;->b()V

    .line 2269
    iget-object v0, p0, Lee/cyber/smartid/tse/SmartIdTSE;->x:Lee/cyber/smartid/tse/KeyManager;

    invoke-virtual {v0, p1}, Lee/cyber/smartid/tse/KeyManager;->getKeyPinLength(Ljava/lang/String;)I

    move-result p1

    return p1
.end method

.method public getKnownServerKeys(Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 2409
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 2410
    invoke-direct {p0, p1}, Lee/cyber/smartid/tse/SmartIdTSE;->e(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object p1

    const/4 v1, 0x0

    .line 2411
    :goto_0
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 2412
    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lee/cyber/smartid/tse/dto/KTKPublicKey;

    invoke-virtual {v2}, Lee/cyber/smartid/tse/dto/KTKPublicKey;->getKeyId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return-object v0
.end method

.method public getPRNGTestResult()[Lee/cyber/smartid/cryptolib/dto/TestResult;
    .locals 1

    .line 2130
    invoke-direct {p0}, Lee/cyber/smartid/tse/SmartIdTSE;->b()V

    .line 2132
    iget-object v0, p0, Lee/cyber/smartid/tse/SmartIdTSE;->x:Lee/cyber/smartid/tse/KeyManager;

    invoke-virtual {v0}, Lee/cyber/smartid/tse/KeyManager;->getPRNGTestResult()[Lee/cyber/smartid/cryptolib/dto/TestResult;

    move-result-object v0

    return-object v0
.end method

.method public getVersion()Ljava/lang/String;
    .locals 1

    const-string v0, "10.3.3_RELEASE"

    return-object v0
.end method

.method public initializeAPICallback(Lee/cyber/smartid/tse/network/RPCCallback;)V
    .locals 2

    .line 2083
    invoke-direct {p0}, Lee/cyber/smartid/tse/SmartIdTSE;->b()V

    if-eqz p1, :cond_0

    .line 2088
    iget-object v0, p0, Lee/cyber/smartid/tse/SmartIdTSE;->h:Lee/cyber/smartid/tse/inter/WallClock;

    iget-object v1, p0, Lee/cyber/smartid/tse/SmartIdTSE;->d:Lee/cyber/smartid/tse/SmartIdTSE$TSEListenerAccess;

    invoke-virtual {p1, v0, v1}, Lee/cyber/smartid/tse/network/RPCCallback;->initCallback(Lee/cyber/smartid/tse/inter/WallClock;Lee/cyber/smartid/tse/inter/ListenerAccess;)V

    return-void

    .line 2086
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "RPCCallback can\'t be null"

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public initializeKeyAndKeyStates(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/tse/dto/jsonrpc/KeyData;Lee/cyber/smartid/tse/inter/InitializeKeyAndKeyStatesListener;)V
    .locals 8

    .line 1585
    invoke-direct {p0}, Lee/cyber/smartid/tse/SmartIdTSE;->b()V

    .line 1587
    iget-object v0, p0, Lee/cyber/smartid/tse/SmartIdTSE;->x:Lee/cyber/smartid/tse/KeyManager;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    move-object v7, p7

    invoke-virtual/range {v0 .. v7}, Lee/cyber/smartid/tse/KeyManager;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/tse/dto/jsonrpc/KeyData;Lee/cyber/smartid/tse/inter/InitializeKeyAndKeyStatesListener;)V

    return-void
.end method

.method public initializeTSE()V
    .locals 17
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lee/cyber/smartid/tse/dto/InitTSEException;
        }
    .end annotation

    move-object/from16 v1, p0

    .line 842
    iget-object v2, v1, Lee/cyber/smartid/tse/SmartIdTSE;->l:Ljava/lang/Object;

    monitor-enter v2

    const-wide/16 v3, 0x0

    .line 845
    :try_start_0
    iget-object v0, v1, Lee/cyber/smartid/tse/SmartIdTSE;->j:Lee/cyber/smartid/tse/inter/LogAccess;

    const-string v5, "initializeTSE - thread start"

    invoke-interface {v0, v5}, Lee/cyber/smartid/tse/inter/LogAccess;->d(Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 847
    invoke-direct {v1, v0}, Lee/cyber/smartid/tse/SmartIdTSE;->a(Z)V

    .line 850
    iget-object v0, v1, Lee/cyber/smartid/tse/SmartIdTSE;->j:Lee/cyber/smartid/tse/inter/LogAccess;

    const-string v5, "initService - reading props .."

    invoke-interface {v0, v5}, Lee/cyber/smartid/tse/inter/LogAccess;->d(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const-wide/16 v5, 0x3f6

    .line 852
    :try_start_1
    invoke-direct/range {p0 .. p0}, Lee/cyber/smartid/tse/SmartIdTSE;->g()V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 856
    :try_start_2
    iget-object v0, v1, Lee/cyber/smartid/tse/SmartIdTSE;->j:Lee/cyber/smartid/tse/inter/LogAccess;

    const-string v5, "initService - doing PRNG fixes .."

    invoke-interface {v0, v5}, Lee/cyber/smartid/tse/inter/LogAccess;->d(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    const-wide/16 v5, 0x3ff

    .line 858
    :try_start_3
    invoke-static {}, Lee/cyber/smartid/tse/util/PRNGFixes;->apply()V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 861
    :try_start_4
    iget-object v0, v1, Lee/cyber/smartid/tse/SmartIdTSE;->j:Lee/cyber/smartid/tse/inter/LogAccess;

    const-string v5, "initializeTSE - creating KeyManager .."

    invoke-interface {v0, v5}, Lee/cyber/smartid/tse/inter/LogAccess;->d(Ljava/lang/String;)V

    .line 863
    iget-object v6, v1, Lee/cyber/smartid/tse/SmartIdTSE;->f:Lee/cyber/smartid/tse/inter/ResourceAccess;

    iget-object v7, v1, Lee/cyber/smartid/tse/SmartIdTSE;->d:Lee/cyber/smartid/tse/SmartIdTSE$TSEListenerAccess;

    iget-object v8, v1, Lee/cyber/smartid/tse/SmartIdTSE;->e:Lee/cyber/smartid/tse/SmartIdTSE$TSEAlarmAccess;

    iget-object v9, v1, Lee/cyber/smartid/tse/SmartIdTSE;->c:Lee/cyber/smartid/cryptolib/CryptoLib;

    iget-object v10, v1, Lee/cyber/smartid/tse/SmartIdTSE;->c:Lee/cyber/smartid/cryptolib/CryptoLib;

    iget-object v11, v1, Lee/cyber/smartid/tse/SmartIdTSE;->c:Lee/cyber/smartid/cryptolib/CryptoLib;

    iget-object v12, v1, Lee/cyber/smartid/tse/SmartIdTSE;->c:Lee/cyber/smartid/cryptolib/CryptoLib;

    iget-object v13, v1, Lee/cyber/smartid/tse/SmartIdTSE;->c:Lee/cyber/smartid/cryptolib/CryptoLib;

    iget-object v14, v1, Lee/cyber/smartid/tse/SmartIdTSE;->h:Lee/cyber/smartid/tse/inter/WallClock;

    invoke-static/range {v6 .. v14}, Lee/cyber/smartid/tse/KeyManager;->getInstance(Lee/cyber/smartid/tse/inter/ResourceAccess;Lee/cyber/smartid/tse/inter/ListenerAccess;Lee/cyber/smartid/tse/inter/AlarmAccess;Lee/cyber/smartid/cryptolib/inter/KeyGenerationOp;Lee/cyber/smartid/cryptolib/inter/RandomGenerationOp;Lee/cyber/smartid/cryptolib/inter/StorageOp;Lee/cyber/smartid/cryptolib/inter/EncodingOp;Lee/cyber/smartid/cryptolib/inter/CryptoOp;Lee/cyber/smartid/tse/inter/WallClock;)Lee/cyber/smartid/tse/KeyManager;

    move-result-object v0

    iput-object v0, v1, Lee/cyber/smartid/tse/SmartIdTSE;->x:Lee/cyber/smartid/tse/KeyManager;

    .line 866
    iget-object v0, v1, Lee/cyber/smartid/tse/SmartIdTSE;->j:Lee/cyber/smartid/tse/inter/LogAccess;

    const-string v5, "initService - upgrading storage .."

    invoke-interface {v0, v5}, Lee/cyber/smartid/tse/inter/LogAccess;->d(Ljava/lang/String;)V
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_2
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    const-wide/16 v5, 0x400

    .line 868
    :try_start_5
    invoke-direct/range {p0 .. p0}, Lee/cyber/smartid/tse/SmartIdTSE;->c()I

    move-result v0

    const/4 v7, 0x5

    invoke-direct {v1, v0, v7}, Lee/cyber/smartid/tse/SmartIdTSE;->a(II)V
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 872
    :try_start_6
    iget-object v0, v1, Lee/cyber/smartid/tse/SmartIdTSE;->j:Lee/cyber/smartid/tse/inter/LogAccess;

    const-string v5, "initializeTSE - creating the API interface .."

    invoke-interface {v0, v5}, Lee/cyber/smartid/tse/inter/LogAccess;->d(Ljava/lang/String;)V

    .line 873
    iget-object v6, v1, Lee/cyber/smartid/tse/SmartIdTSE;->b:Landroid/content/Context;

    iget-object v7, v1, Lee/cyber/smartid/tse/SmartIdTSE;->n:Ljava/lang/String;

    iget-boolean v8, v1, Lee/cyber/smartid/tse/SmartIdTSE;->v:Z

    iget-object v9, v1, Lee/cyber/smartid/tse/SmartIdTSE;->o:Ljava/util/ArrayList;

    iget-object v10, v1, Lee/cyber/smartid/tse/SmartIdTSE;->f:Lee/cyber/smartid/tse/inter/ResourceAccess;

    iget-object v11, v1, Lee/cyber/smartid/tse/SmartIdTSE;->h:Lee/cyber/smartid/tse/inter/WallClock;

    invoke-static/range {v6 .. v11}, Lee/cyber/smartid/tse/network/TSEAPI;->createNewInstance(Landroid/content/Context;Ljava/lang/String;ZLjava/util/ArrayList;Lee/cyber/smartid/tse/inter/ResourceAccess;Lee/cyber/smartid/tse/inter/WallClock;)Lee/cyber/smartid/tse/network/TSEAPI;

    move-result-object v0

    iput-object v0, v1, Lee/cyber/smartid/tse/SmartIdTSE;->w:Lee/cyber/smartid/tse/network/TSEAPI;

    .line 876
    iget-object v0, v1, Lee/cyber/smartid/tse/SmartIdTSE;->j:Lee/cyber/smartid/tse/inter/LogAccess;

    const-string v5, "initializeTSE - starting up stuff .."

    invoke-interface {v0, v5}, Lee/cyber/smartid/tse/inter/LogAccess;->d(Ljava/lang/String;)V

    .line 878
    iget-object v6, v1, Lee/cyber/smartid/tse/SmartIdTSE;->f:Lee/cyber/smartid/tse/inter/ResourceAccess;

    iget-object v0, v1, Lee/cyber/smartid/tse/SmartIdTSE;->w:Lee/cyber/smartid/tse/network/TSEAPI;

    invoke-virtual {v0}, Lee/cyber/smartid/tse/network/TSEAPI;->getService()Lee/cyber/smartid/tse/network/TSEAPI$TSEAPIService;

    move-result-object v7

    iget-object v8, v1, Lee/cyber/smartid/tse/SmartIdTSE;->d:Lee/cyber/smartid/tse/SmartIdTSE$TSEListenerAccess;

    iget-object v9, v1, Lee/cyber/smartid/tse/SmartIdTSE;->x:Lee/cyber/smartid/tse/KeyManager;

    iget-object v10, v1, Lee/cyber/smartid/tse/SmartIdTSE;->e:Lee/cyber/smartid/tse/SmartIdTSE$TSEAlarmAccess;

    iget-object v11, v1, Lee/cyber/smartid/tse/SmartIdTSE;->c:Lee/cyber/smartid/cryptolib/CryptoLib;

    iget-object v12, v1, Lee/cyber/smartid/tse/SmartIdTSE;->c:Lee/cyber/smartid/cryptolib/CryptoLib;

    iget-object v13, v1, Lee/cyber/smartid/tse/SmartIdTSE;->c:Lee/cyber/smartid/cryptolib/CryptoLib;

    iget-object v14, v1, Lee/cyber/smartid/tse/SmartIdTSE;->c:Lee/cyber/smartid/cryptolib/CryptoLib;

    iget-object v15, v1, Lee/cyber/smartid/tse/SmartIdTSE;->g:Lee/cyber/smartid/tse/inter/ExternalResourceAccess;

    iget-object v0, v1, Lee/cyber/smartid/tse/SmartIdTSE;->h:Lee/cyber/smartid/tse/inter/WallClock;

    move-object/from16 v16, v0

    invoke-static/range {v6 .. v16}, Lee/cyber/smartid/tse/KeyStateManager;->getInstance(Lee/cyber/smartid/tse/inter/ResourceAccess;Lee/cyber/smartid/tse/network/TSEAPI$TSEAPIService;Lee/cyber/smartid/tse/inter/ListenerAccess;Lee/cyber/smartid/tse/inter/KeyManagerAccess;Lee/cyber/smartid/tse/SmartIdTSE$TSEAlarmAccess;Lee/cyber/smartid/cryptolib/inter/SigningOp;Lee/cyber/smartid/cryptolib/inter/EncodingOp;Lee/cyber/smartid/cryptolib/inter/RandomGenerationOp;Lee/cyber/smartid/cryptolib/inter/CryptoOp;Lee/cyber/smartid/tse/inter/ExternalResourceAccess;Lee/cyber/smartid/tse/inter/WallClock;)Lee/cyber/smartid/tse/KeyStateManager;

    move-result-object v0

    iput-object v0, v1, Lee/cyber/smartid/tse/SmartIdTSE;->y:Lee/cyber/smartid/tse/KeyStateManager;

    .line 879
    iget-object v0, v1, Lee/cyber/smartid/tse/SmartIdTSE;->y:Lee/cyber/smartid/tse/KeyStateManager;

    new-instance v5, Lee/cyber/smartid/tse/SmartIdTSE$1;

    invoke-direct {v5, v1}, Lee/cyber/smartid/tse/SmartIdTSE$1;-><init>(Lee/cyber/smartid/tse/SmartIdTSE;)V

    invoke-virtual {v0, v5}, Lee/cyber/smartid/tse/KeyStateManager;->setListener(Lee/cyber/smartid/tse/inter/StateWorkerJobListener;)V

    const/4 v0, 0x1

    .line 900
    invoke-direct {v1, v0}, Lee/cyber/smartid/tse/SmartIdTSE;->a(Z)V
    :try_end_6
    .catch Ljava/lang/Throwable; {:try_start_6 .. :try_end_6} :catch_2
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 909
    :try_start_7
    invoke-direct/range {p0 .. p0}, Lee/cyber/smartid/tse/SmartIdTSE;->a()V
    :try_end_7
    .catch Ljava/lang/Error; {:try_start_7 .. :try_end_7} :catch_0
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v0

    move-object v3, v0

    .line 912
    :try_start_8
    iget-object v0, v1, Lee/cyber/smartid/tse/SmartIdTSE;->j:Lee/cyber/smartid/tse/inter/LogAccess;

    const-string v4, "initializeTSE - postInitTSE"

    invoke-interface {v0, v4, v3}, Lee/cyber/smartid/tse/inter/LogAccess;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 915
    :goto_0
    monitor-exit v2

    return-void

    :catch_1
    move-exception v0

    move-wide v3, v5

    goto :goto_1

    :catchall_0
    move-exception v0

    goto :goto_2

    :catch_2
    move-exception v0

    .line 902
    :goto_1
    iget-object v5, v1, Lee/cyber/smartid/tse/SmartIdTSE;->j:Lee/cyber/smartid/tse/inter/LogAccess;

    const-string v6, "initializeTSE"

    invoke-interface {v5, v6, v0}, Lee/cyber/smartid/tse/inter/LogAccess;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 904
    new-instance v5, Lee/cyber/smartid/tse/dto/InitTSEException;

    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v5, v3, v4, v0}, Lee/cyber/smartid/tse/dto/InitTSEException;-><init>(JLjava/lang/String;)V

    throw v5

    .line 915
    :goto_2
    monitor-exit v2
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    throw v0
.end method

.method public isDeviceRegistrationDone()Z
    .locals 1

    .line 2286
    invoke-direct {p0}, Lee/cyber/smartid/tse/SmartIdTSE;->b()V

    .line 2287
    iget-object v0, p0, Lee/cyber/smartid/tse/SmartIdTSE;->f:Lee/cyber/smartid/tse/inter/ResourceAccess;

    invoke-interface {v0}, Lee/cyber/smartid/tse/inter/ResourceAccess;->getAppInstanceStorageId()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lee/cyber/smartid/tse/SmartIdTSE;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lee/cyber/smartid/tse/SmartIdTSE;->f:Lee/cyber/smartid/tse/inter/ResourceAccess;

    invoke-interface {v0}, Lee/cyber/smartid/tse/inter/ResourceAccess;->getPasswordStorageId()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lee/cyber/smartid/tse/SmartIdTSE;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isInitializeTSEDone()Z
    .locals 2

    .line 971
    iget-object v0, p0, Lee/cyber/smartid/tse/SmartIdTSE;->l:Ljava/lang/Object;

    monitor-enter v0

    .line 972
    :try_start_0
    iget-boolean v1, p0, Lee/cyber/smartid/tse/SmartIdTSE;->i:Z

    monitor-exit v0

    return v1

    :catchall_0
    move-exception v1

    .line 973
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public removeListener(Ljava/lang/String;)V
    .locals 2

    .line 1157
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 1160
    :cond_0
    iget-object v0, p0, Lee/cyber/smartid/tse/SmartIdTSE;->m:Ljava/util/WeakHashMap;

    monitor-enter v0

    .line 1161
    :try_start_0
    iget-object v1, p0, Lee/cyber/smartid/tse/SmartIdTSE;->m:Ljava/util/WeakHashMap;

    invoke-virtual {v1, p1}, Ljava/util/WeakHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1162
    monitor-exit v0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method public requestNewFreshnessToken(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/tse/inter/NewFreshnessTokenListener;)V
    .locals 6

    .line 2326
    invoke-direct {p0}, Lee/cyber/smartid/tse/SmartIdTSE;->b()V

    .line 2327
    iget-object v0, p0, Lee/cyber/smartid/tse/SmartIdTSE;->j:Lee/cyber/smartid/tse/inter/LogAccess;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "requestNewFreshnessToken - accountUUID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, ", keyType: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lee/cyber/smartid/tse/inter/LogAccess;->d(Ljava/lang/String;)V

    .line 2328
    invoke-virtual {p0, p1, p5}, Lee/cyber/smartid/tse/SmartIdTSE;->setListener(Ljava/lang/String;Lee/cyber/smartid/tse/inter/TSEListener;)V

    .line 2329
    new-instance p5, Lee/cyber/smartid/tse/dto/jsonrpc/param/NewFreshnessTokenParams;

    invoke-direct {p5}, Lee/cyber/smartid/tse/dto/jsonrpc/param/NewFreshnessTokenParams;-><init>()V

    .line 2330
    invoke-virtual {p5, p3}, Lee/cyber/smartid/tse/dto/jsonrpc/param/NewFreshnessTokenParams;->setAccountUUID(Ljava/lang/String;)V

    .line 2331
    invoke-virtual {p5, p4}, Lee/cyber/smartid/tse/dto/jsonrpc/param/NewFreshnessTokenParams;->setKeyType(Ljava/lang/String;)V

    .line 2332
    new-instance v2, Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCRequest;

    const-string p3, "getFreshnessToken"

    invoke-direct {v2, p3, p5}, Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCRequest;-><init>(Ljava/lang/String;Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCParams;)V

    .line 2333
    iget-object p3, p0, Lee/cyber/smartid/tse/SmartIdTSE;->w:Lee/cyber/smartid/tse/network/TSEAPI;

    invoke-virtual {p3}, Lee/cyber/smartid/tse/network/TSEAPI;->getService()Lee/cyber/smartid/tse/network/TSEAPI$TSEAPIService;

    move-result-object p3

    const-string p4, "external"

    invoke-interface {p3, p4, v2}, Lee/cyber/smartid/tse/network/TSEAPI$TSEAPIService;->getNewFreshnessToken(Ljava/lang/String;Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCRequest;)Lc/b;

    move-result-object p3

    .line 2334
    new-instance p4, Lee/cyber/smartid/tse/SmartIdTSE$6;

    move-object v0, p4

    move-object v1, p0

    move-object v3, p0

    move-object v4, p2

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, Lee/cyber/smartid/tse/SmartIdTSE$6;-><init>(Lee/cyber/smartid/tse/SmartIdTSE;Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCRequest;Lee/cyber/smartid/tse/SmartIdTSE;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {p3, p4}, Lc/b;->a(Lc/d;)V

    return-void
.end method

.method public retryLocalPendingState(Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/tse/inter/TSEListener;)V
    .locals 3

    .line 2046
    invoke-direct {p0}, Lee/cyber/smartid/tse/SmartIdTSE;->b()V

    .line 2048
    iget-object v0, p0, Lee/cyber/smartid/tse/SmartIdTSE;->y:Lee/cyber/smartid/tse/KeyStateManager;

    const/4 v1, 0x0

    if-eqz p3, :cond_0

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    goto :goto_0

    :cond_0
    move-object v2, v1

    :goto_0
    invoke-virtual {v0, p1, p2, v2}, Lee/cyber/smartid/tse/KeyStateManager;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/String;

    move-result-object p1

    iget-object v2, p0, Lee/cyber/smartid/tse/SmartIdTSE;->y:Lee/cyber/smartid/tse/KeyStateManager;

    if-eqz p3, :cond_1

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    :cond_1
    invoke-virtual {v2, p2, p3, v1}, Lee/cyber/smartid/tse/KeyStateManager;->a(Ljava/lang/String;Lee/cyber/smartid/tse/inter/TSEListener;Ljava/lang/Class;)Lee/cyber/smartid/tse/inter/TSEListener;

    move-result-object p3

    invoke-virtual {v0, p1, p2, p3}, Lee/cyber/smartid/tse/KeyStateManager;->retryLocalPendingState(Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/tse/inter/TSEListener;)V

    return-void
.end method

.method public setDeviceCredentials(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lee/cyber/smartid/cryptolib/dto/StorageException;
        }
    .end annotation

    .line 2305
    invoke-direct {p0}, Lee/cyber/smartid/tse/SmartIdTSE;->b()V

    .line 2306
    iget-object v0, p0, Lee/cyber/smartid/tse/SmartIdTSE;->c:Lee/cyber/smartid/cryptolib/CryptoLib;

    iget-object v1, p0, Lee/cyber/smartid/tse/SmartIdTSE;->f:Lee/cyber/smartid/tse/inter/ResourceAccess;

    invoke-interface {v1}, Lee/cyber/smartid/tse/inter/ResourceAccess;->getAppInstanceStorageId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Lee/cyber/smartid/cryptolib/CryptoLib;->storeData(Ljava/lang/String;Ljava/lang/Object;)V

    .line 2307
    iget-object p1, p0, Lee/cyber/smartid/tse/SmartIdTSE;->c:Lee/cyber/smartid/cryptolib/CryptoLib;

    iget-object v0, p0, Lee/cyber/smartid/tse/SmartIdTSE;->f:Lee/cyber/smartid/tse/inter/ResourceAccess;

    invoke-interface {v0}, Lee/cyber/smartid/tse/inter/ResourceAccess;->getPasswordStorageId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0, p2}, Lee/cyber/smartid/cryptolib/CryptoLib;->storeData(Ljava/lang/String;Ljava/lang/Object;)V

    return-void
.end method

.method public setListener(Ljava/lang/String;Lee/cyber/smartid/tse/inter/TSEListener;)V
    .locals 3

    .line 1109
    iget-object v0, p0, Lee/cyber/smartid/tse/SmartIdTSE;->j:Lee/cyber/smartid/tse/inter/LogAccess;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setListener: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, ", listener: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lee/cyber/smartid/tse/inter/LogAccess;->d(Ljava/lang/String;)V

    .line 1110
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 1113
    :cond_0
    iget-object v0, p0, Lee/cyber/smartid/tse/SmartIdTSE;->m:Ljava/util/WeakHashMap;

    monitor-enter v0

    if-nez p2, :cond_1

    .line 1115
    :try_start_0
    invoke-virtual {p0, p1}, Lee/cyber/smartid/tse/SmartIdTSE;->removeListener(Ljava/lang/String;)V

    goto :goto_0

    .line 1117
    :cond_1
    iget-object v1, p0, Lee/cyber/smartid/tse/SmartIdTSE;->m:Ljava/util/WeakHashMap;

    invoke-virtual {v1, p1, p2}, Ljava/util/WeakHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1119
    :goto_0
    monitor-exit v0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method public submitClientSecondPart(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/tse/inter/SubmitClientSecondPartListener;)V
    .locals 8

    .line 1968
    invoke-direct {p0}, Lee/cyber/smartid/tse/SmartIdTSE;->b()V

    .line 1969
    iget-object v1, p0, Lee/cyber/smartid/tse/SmartIdTSE;->j:Lee/cyber/smartid/tse/inter/LogAccess;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "submitClientSecondPart - "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v3}, Lee/cyber/smartid/tse/inter/LogAccess;->d(Ljava/lang/String;)V

    .line 1971
    iget-object v1, p0, Lee/cyber/smartid/tse/SmartIdTSE;->y:Lee/cyber/smartid/tse/KeyStateManager;

    const-class v3, Lee/cyber/smartid/tse/inter/SubmitClientSecondPartListener;

    invoke-virtual {v1, p1, p2, v3}, Lee/cyber/smartid/tse/KeyStateManager;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v3

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v4, "external"

    invoke-static {v0, v4}, Lee/cyber/smartid/tse/util/Util;->addXSplitKeyTriggerExtra(Landroid/os/Bundle;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v6

    iget-object v0, p0, Lee/cyber/smartid/tse/SmartIdTSE;->y:Lee/cyber/smartid/tse/KeyStateManager;

    const-class v4, Lee/cyber/smartid/tse/inter/SubmitClientSecondPartListener;

    invoke-virtual {v0, p2, p6, v4}, Lee/cyber/smartid/tse/KeyStateManager;->a(Ljava/lang/String;Lee/cyber/smartid/tse/inter/TSEListener;Ljava/lang/Class;)Lee/cyber/smartid/tse/inter/TSEListener;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lee/cyber/smartid/tse/inter/SubmitClientSecondPartListener;

    move-object v0, v1

    move-object v1, v3

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-virtual/range {v0 .. v7}, Lee/cyber/smartid/tse/KeyStateManager;->submitClientSecondPart(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Lee/cyber/smartid/tse/inter/SubmitClientSecondPartListener;)V

    return-void
.end method

.method public updateFreshnessToken(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lee/cyber/smartid/cryptolib/dto/StorageException;
        }
    .end annotation

    .line 1526
    invoke-direct {p0}, Lee/cyber/smartid/tse/SmartIdTSE;->b()V

    .line 1527
    iget-object v0, p0, Lee/cyber/smartid/tse/SmartIdTSE;->c:Lee/cyber/smartid/cryptolib/CryptoLib;

    monitor-enter v0

    .line 1528
    :try_start_0
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    .line 1531
    :cond_0
    iget-object v1, p0, Lee/cyber/smartid/tse/SmartIdTSE;->x:Lee/cyber/smartid/tse/KeyManager;

    iget-object v2, p0, Lee/cyber/smartid/tse/SmartIdTSE;->f:Lee/cyber/smartid/tse/inter/ResourceAccess;

    invoke-interface {v2, p1}, Lee/cyber/smartid/tse/inter/ResourceAccess;->getKeyStateIdByKeyId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, p2}, Lee/cyber/smartid/tse/KeyManager;->updateFreshnessToken(Ljava/lang/String;Ljava/lang/String;)V

    .line 1532
    iget-object v1, p0, Lee/cyber/smartid/tse/SmartIdTSE;->j:Lee/cyber/smartid/tse/inter/LogAccess;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "updateFreshnessToken - updated the freshnessToken ("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p2, ") for "

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-interface {v1, p1}, Lee/cyber/smartid/tse/inter/LogAccess;->d(Ljava/lang/String;)V

    .line 1533
    monitor-exit v0

    return-void

    .line 1529
    :cond_1
    :goto_0
    monitor-exit v0

    return-void

    :catchall_0
    move-exception p1

    .line 1533
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method public validateKeyCreationResponse(Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/tse/dto/jsonrpc/KeyData;Lee/cyber/smartid/tse/inter/ValidateKeyCreationResponseListener;)V
    .locals 1

    .line 2223
    invoke-direct {p0}, Lee/cyber/smartid/tse/SmartIdTSE;->b()V

    .line 2225
    iget-object v0, p0, Lee/cyber/smartid/tse/SmartIdTSE;->x:Lee/cyber/smartid/tse/KeyManager;

    invoke-virtual {v0, p1, p2, p3, p4}, Lee/cyber/smartid/tse/KeyManager;->a(Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/tse/dto/jsonrpc/KeyData;Lee/cyber/smartid/tse/inter/ValidateKeyCreationResponseListener;)V

    return-void
.end method

.method public validatePins(Ljava/lang/String;Ljava/util/ArrayList;Lee/cyber/smartid/tse/inter/ValidatePinListener;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList<",
            "Lee/cyber/smartid/tse/dto/PinInfo;",
            ">;",
            "Lee/cyber/smartid/tse/inter/ValidatePinListener;",
            ")V"
        }
    .end annotation

    .line 2151
    invoke-direct {p0}, Lee/cyber/smartid/tse/SmartIdTSE;->b()V

    .line 2153
    iget-object v0, p0, Lee/cyber/smartid/tse/SmartIdTSE;->x:Lee/cyber/smartid/tse/KeyManager;

    invoke-virtual {v0, p1, p2, p3}, Lee/cyber/smartid/tse/KeyManager;->validatePins(Ljava/lang/String;Ljava/util/ArrayList;Lee/cyber/smartid/tse/inter/ValidatePinListener;)V

    return-void
.end method
