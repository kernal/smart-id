.class public interface abstract Lee/cyber/smartid/tse/inter/AlarmAccess;
.super Ljava/lang/Object;
.source "AlarmAccess.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lee/cyber/smartid/tse/inter/AlarmAccess$AlarmHandler;
    }
.end annotation


# virtual methods
.method public abstract addAlarmHandler(Lee/cyber/smartid/tse/inter/AlarmAccess$AlarmHandler;)V
.end method

.method public abstract clearAlarmFor(Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract removeAlarmHandler(Lee/cyber/smartid/tse/inter/AlarmAccess$AlarmHandler;)V
.end method

.method public abstract scheduleAlarmFor(Ljava/lang/String;JLjava/lang/String;)V
.end method
