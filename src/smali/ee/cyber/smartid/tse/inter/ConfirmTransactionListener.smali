.class public interface abstract Lee/cyber/smartid/tse/inter/ConfirmTransactionListener;
.super Ljava/lang/Object;
.source "ConfirmTransactionListener.java"

# interfaces
.implements Lee/cyber/smartid/tse/inter/TSEListener;


# virtual methods
.method public abstract onConfirmTransactionFailed(Ljava/lang/String;Lee/cyber/smartid/tse/dto/TSEError;)V
.end method

.method public abstract onConfirmTransactionSuccess(Ljava/lang/String;Lee/cyber/smartid/tse/dto/jsonrpc/resp/ConfirmTransactionResp;)V
.end method
