.class public interface abstract Lee/cyber/smartid/tse/inter/KeyStorageAccess;
.super Ljava/lang/Object;
.source "KeyStorageAccess.java"


# virtual methods
.method public abstract consumeFreshnessToken(Ljava/lang/String;)Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lee/cyber/smartid/cryptolib/dto/StorageException;
        }
    .end annotation
.end method

.method public abstract deleteKeyAndRelatedObjects(Ljava/lang/String;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lee/cyber/smartid/cryptolib/dto/StorageException;
        }
    .end annotation
.end method

.method public abstract finishKeyStateOperation(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lee/cyber/smartid/cryptolib/dto/StorageException;
        }
    .end annotation
.end method

.method public abstract getKey(Ljava/lang/String;)Lee/cyber/smartid/tse/dto/Key;
.end method

.method public abstract getKeyStateById(Ljava/lang/String;)Lee/cyber/smartid/tse/dto/KeyState;
.end method

.method public abstract getKeyStateByKeyId(Ljava/lang/String;)Lee/cyber/smartid/tse/dto/KeyState;
.end method

.method public abstract getKeyStateMetaById(Ljava/lang/String;)Lee/cyber/smartid/tse/dto/KeyStateMeta;
.end method

.method public abstract getKeyStateMetaByKeyStateId(Ljava/lang/String;)Lee/cyber/smartid/tse/dto/KeyStateMeta;
.end method

.method public abstract initializeKey(Ljava/lang/String;Lee/cyber/smartid/tse/dto/Key;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lee/cyber/smartid/cryptolib/dto/StorageException;
        }
    .end annotation
.end method

.method public abstract initializeKeyState(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lee/cyber/smartid/cryptolib/dto/StorageException;
        }
    .end annotation
.end method

.method public abstract rollbackKeyStateOperation(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lee/cyber/smartid/cryptolib/dto/StorageException;
        }
    .end annotation
.end method

.method public abstract startKeyStateOperation(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/support/v4/e/j;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "I",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Landroid/support/v4/e/j<",
            "Lee/cyber/smartid/tse/dto/KeyState;",
            "Lee/cyber/smartid/tse/dto/KeyStateMeta;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lee/cyber/smartid/cryptolib/dto/StorageException;
        }
    .end annotation
.end method

.method public abstract updateFreshnessToken(Ljava/lang/String;Ljava/lang/String;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lee/cyber/smartid/cryptolib/dto/StorageException;
        }
    .end annotation
.end method

.method public abstract updateKeyStateMeta(Lee/cyber/smartid/tse/dto/KeyStateMeta;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lee/cyber/smartid/cryptolib/dto/StorageException;
        }
    .end annotation
.end method
