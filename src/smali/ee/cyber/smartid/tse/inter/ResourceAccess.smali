.class public interface abstract Lee/cyber/smartid/tse/inter/ResourceAccess;
.super Ljava/lang/Object;
.source "ResourceAccess.java"


# virtual methods
.method public abstract getAppInstanceStorageId()Ljava/lang/String;
.end method

.method public abstract getAppVersion()Ljava/lang/String;
.end method

.method public abstract getApplicationContext()Landroid/content/Context;
.end method

.method public abstract getAutomaticRequestRetryDelay(I)J
.end method

.method public abstract getDeviceFingerPrint()Ljava/lang/String;
.end method

.method public abstract getEncryptedKeysStorageId()Ljava/lang/String;
.end method

.method public abstract getFreshnessTokenId(Ljava/lang/String;)Ljava/lang/String;
.end method

.method public abstract getKeyStateIdByKeyId(Ljava/lang/String;)Ljava/lang/String;
.end method

.method public abstract getKeyStateMetaIdByKeyStateId(Ljava/lang/String;)Ljava/lang/String;
.end method

.method public abstract getLastHammerTimeEventTimestamp()J
.end method

.method public abstract getLastPRNGTestResultStorageId()Ljava/lang/String;
.end method

.method public abstract getLibraryVersion()Ljava/lang/String;
.end method

.method public abstract getMinInteractiveRequestDelay()J
.end method

.method public abstract getPRNGTestLoopCount()I
.end method

.method public abstract getPRNGTestSuppressFailure()Z
.end method

.method public abstract getPasswordStorageId()Ljava/lang/String;
.end method

.method public abstract getPlatform()Ljava/lang/String;
.end method

.method public abstract getPreferredSZKTKPublicKey(Ljava/lang/String;)Lee/cyber/smartid/tse/dto/KTKPublicKey;
.end method

.method public abstract getRefreshCloneDetectionInterval()J
.end method

.method public abstract isDeviceRegistrationDone()Z
.end method

.method public abstract loadString(Ljava/lang/String;)Ljava/lang/String;
.end method

.method public abstract setHammerTimeEventTimestamp()V
.end method
