.class public interface abstract Lee/cyber/smartid/tse/inter/GenerateKeysListener;
.super Ljava/lang/Object;
.source "GenerateKeysListener.java"

# interfaces
.implements Lee/cyber/smartid/tse/inter/TSEListener;


# virtual methods
.method public abstract onGenerateKeysFailed(Ljava/lang/String;Lee/cyber/smartid/tse/dto/TSEError;)V
.end method

.method public abstract onGenerateKeysPRNGTestUpdate(Ljava/lang/String;[Lee/cyber/smartid/cryptolib/dto/TestResult;)V
.end method

.method public abstract onGenerateKeysProgress(Ljava/lang/String;II)V
.end method

.method public abstract onGenerateKeysSuccess(Ljava/lang/String;Lee/cyber/smartid/tse/dto/jsonrpc/resp/GenerateKeysResp;)V
.end method
