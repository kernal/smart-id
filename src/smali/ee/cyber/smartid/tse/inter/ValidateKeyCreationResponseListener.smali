.class public interface abstract Lee/cyber/smartid/tse/inter/ValidateKeyCreationResponseListener;
.super Ljava/lang/Object;
.source "ValidateKeyCreationResponseListener.java"

# interfaces
.implements Lee/cyber/smartid/tse/inter/TSEListener;


# virtual methods
.method public abstract onValidateKeyCreationResponseFailed(Ljava/lang/String;Lee/cyber/smartid/tse/dto/TSEError;)V
.end method

.method public abstract onValidateKeyCreationResponseSuccess(Ljava/lang/String;Lee/cyber/smartid/tse/dto/jsonrpc/resp/ValidateKeyCreationResp;)V
.end method
