.class public interface abstract Lee/cyber/smartid/tse/inter/ExternalResourceAccess;
.super Ljava/lang/Object;
.source "ExternalResourceAccess.java"


# virtual methods
.method public abstract getAccountUUIDs()Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getAppStorageVersion()I
.end method

.method public abstract getAppVersion()Ljava/lang/String;
.end method

.method public abstract getDeviceFingerPrint()Ljava/lang/String;
.end method

.method public abstract getKeyIds()Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getKeyIdsWithAccountUUIDAndKeyType()Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Lee/cyber/smartid/tse/dto/Triple<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end method

.method public abstract getKeyTypes()[Ljava/lang/String;
.end method

.method public abstract getLibraryVersion()Ljava/lang/String;
.end method

.method public abstract getPlatform()Ljava/lang/String;
.end method

.method public abstract onSubmitClientSecondPartFailed(Ljava/lang/String;Lee/cyber/smartid/tse/dto/TSEError;)V
.end method

.method public abstract onSubmitClientSecondPartSuccess(Ljava/lang/String;Lee/cyber/smartid/tse/dto/jsonrpc/resp/SubmitClientSecondPartResp;)V
.end method
