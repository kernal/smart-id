.class public interface abstract Lee/cyber/smartid/tse/inter/ListenerAccess;
.super Ljava/lang/Object;
.source "ListenerAccess.java"


# virtual methods
.method public abstract getApplicationContext()Landroid/content/Context;
.end method

.method public abstract getListener(Ljava/lang/String;ZLjava/lang/Class;)Lee/cyber/smartid/tse/inter/TSEListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lee/cyber/smartid/tse/inter/TSEListener;",
            ">(",
            "Ljava/lang/String;",
            "Z",
            "Ljava/lang/Class<",
            "TT;>;)TT;"
        }
    .end annotation
.end method

.method public abstract notifyError(Ljava/lang/String;Lee/cyber/smartid/tse/inter/TSEListener;Lee/cyber/smartid/tse/dto/TSEError;)V
.end method

.method public abstract notifySystemEventsListeners(Lee/cyber/smartid/tse/dto/TSEError;)V
.end method

.method public abstract notifyUI(Ljava/lang/Runnable;)V
.end method

.method public abstract setHammerTimeEventTimestamp()V
.end method

.method public abstract setListener(Ljava/lang/String;Lee/cyber/smartid/tse/inter/TSEListener;)V
.end method
