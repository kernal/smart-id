.class public abstract Lee/cyber/smartid/tse/inter/SZPayload;
.super Ljava/lang/Object;
.source "SZPayload.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = 0x6eccbfc45541dd20L


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public serializeAndEncrypt(Lee/cyber/smartid/cryptolib/inter/CryptoOp;[BLjava/lang/String;)Ljava/lang/String;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;
        }
    .end annotation

    .line 46
    invoke-virtual {p0}, Lee/cyber/smartid/tse/inter/SZPayload;->serializeToJSONString()Ljava/lang/String;

    move-result-object v1

    .line 47
    invoke-static {p0}, Lee/cyber/smartid/tse/util/Log;->getInstance(Ljava/lang/Object;)Lee/cyber/smartid/tse/util/Log;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "serializeAndEncrypt: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lee/cyber/smartid/tse/util/Log;->d(Ljava/lang/String;)V

    const-string v5, "SERVER"

    move-object v0, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p3

    .line 48
    invoke-interface/range {v0 .. v5}, Lee/cyber/smartid/cryptolib/inter/CryptoOp;->encryptToTEKEncryptedJWE(Ljava/lang/String;[BLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method protected serializeToJSONString()Ljava/lang/String;
    .locals 1

    .line 32
    invoke-static {}, Lee/cyber/smartid/tse/network/TSEAPI;->createGSONBuilderForAPIResponses()Lcom/google/gson/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/gson/g;->a()Lcom/google/gson/f;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/gson/f;->b(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
