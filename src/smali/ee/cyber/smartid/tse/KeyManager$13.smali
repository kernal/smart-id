.class Lee/cyber/smartid/tse/KeyManager$13;
.super Ljava/lang/Object;
.source "KeyManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lee/cyber/smartid/tse/KeyManager;->a(Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/tse/dto/jsonrpc/KeyData;Lee/cyber/smartid/tse/inter/ValidateKeyCreationResponseListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lee/cyber/smartid/tse/dto/jsonrpc/KeyData;

.field final synthetic b:Ljava/lang/String;

.field final synthetic c:Ljava/lang/String;

.field final synthetic d:Lee/cyber/smartid/tse/KeyManager;


# direct methods
.method constructor <init>(Lee/cyber/smartid/tse/KeyManager;Lee/cyber/smartid/tse/dto/jsonrpc/KeyData;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 940
    iput-object p1, p0, Lee/cyber/smartid/tse/KeyManager$13;->d:Lee/cyber/smartid/tse/KeyManager;

    iput-object p2, p0, Lee/cyber/smartid/tse/KeyManager$13;->a:Lee/cyber/smartid/tse/dto/jsonrpc/KeyData;

    iput-object p3, p0, Lee/cyber/smartid/tse/KeyManager$13;->b:Ljava/lang/String;

    iput-object p4, p0, Lee/cyber/smartid/tse/KeyManager$13;->c:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 11

    .line 944
    iget-object v0, p0, Lee/cyber/smartid/tse/KeyManager$13;->a:Lee/cyber/smartid/tse/dto/jsonrpc/KeyData;

    if-eqz v0, :cond_8

    invoke-virtual {v0}, Lee/cyber/smartid/tse/dto/jsonrpc/KeyData;->getServerDhMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto/16 :goto_1

    .line 952
    :cond_0
    iget-object v0, p0, Lee/cyber/smartid/tse/KeyManager$13;->a:Lee/cyber/smartid/tse/dto/jsonrpc/KeyData;

    invoke-virtual {v0}, Lee/cyber/smartid/tse/dto/jsonrpc/KeyData;->getServerDhMessageEncoding()Ljava/lang/String;

    move-result-object v0

    const-string v1, "JWE"

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 954
    iget-object v0, p0, Lee/cyber/smartid/tse/KeyManager$13;->d:Lee/cyber/smartid/tse/KeyManager;

    invoke-static {v0}, Lee/cyber/smartid/tse/KeyManager;->a(Lee/cyber/smartid/tse/KeyManager;)Lee/cyber/smartid/tse/inter/ListenerAccess;

    move-result-object v0

    new-instance v1, Lee/cyber/smartid/tse/KeyManager$13$2;

    invoke-direct {v1, p0}, Lee/cyber/smartid/tse/KeyManager$13$2;-><init>(Lee/cyber/smartid/tse/KeyManager$13;)V

    invoke-interface {v0, v1}, Lee/cyber/smartid/tse/inter/ListenerAccess;->notifyUI(Ljava/lang/Runnable;)V

    return-void

    .line 961
    :cond_1
    iget-object v0, p0, Lee/cyber/smartid/tse/KeyManager$13;->a:Lee/cyber/smartid/tse/dto/jsonrpc/KeyData;

    invoke-virtual {v0}, Lee/cyber/smartid/tse/dto/jsonrpc/KeyData;->getResponseDataEncoding()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 963
    iget-object v0, p0, Lee/cyber/smartid/tse/KeyManager$13;->d:Lee/cyber/smartid/tse/KeyManager;

    invoke-static {v0}, Lee/cyber/smartid/tse/KeyManager;->a(Lee/cyber/smartid/tse/KeyManager;)Lee/cyber/smartid/tse/inter/ListenerAccess;

    move-result-object v0

    new-instance v1, Lee/cyber/smartid/tse/KeyManager$13$3;

    invoke-direct {v1, p0}, Lee/cyber/smartid/tse/KeyManager$13$3;-><init>(Lee/cyber/smartid/tse/KeyManager$13;)V

    invoke-interface {v0, v1}, Lee/cyber/smartid/tse/inter/ListenerAccess;->notifyUI(Ljava/lang/Runnable;)V

    return-void

    .line 972
    :cond_2
    iget-object v0, p0, Lee/cyber/smartid/tse/KeyManager$13;->d:Lee/cyber/smartid/tse/KeyManager;

    iget-object v1, p0, Lee/cyber/smartid/tse/KeyManager$13;->c:Ljava/lang/String;

    invoke-static {v1}, Lee/cyber/smartid/tse/dto/EncryptKeyWrapper;->getId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lee/cyber/smartid/tse/KeyManager;->loadEncryptedKey(Ljava/lang/String;)Lee/cyber/smartid/tse/dto/EncryptKeyWrapper;

    move-result-object v0

    if-nez v0, :cond_3

    .line 974
    iget-object v0, p0, Lee/cyber/smartid/tse/KeyManager$13;->d:Lee/cyber/smartid/tse/KeyManager;

    invoke-static {v0}, Lee/cyber/smartid/tse/KeyManager;->a(Lee/cyber/smartid/tse/KeyManager;)Lee/cyber/smartid/tse/inter/ListenerAccess;

    move-result-object v0

    new-instance v1, Lee/cyber/smartid/tse/KeyManager$13$4;

    invoke-direct {v1, p0}, Lee/cyber/smartid/tse/KeyManager$13$4;-><init>(Lee/cyber/smartid/tse/KeyManager$13;)V

    invoke-interface {v0, v1}, Lee/cyber/smartid/tse/inter/ListenerAccess;->notifyUI(Ljava/lang/Runnable;)V

    return-void

    .line 981
    :cond_3
    invoke-virtual {v0}, Lee/cyber/smartid/tse/dto/EncryptKeyWrapper;->getDhKeyPair()Lee/cyber/smartid/cryptolib/dto/DiffieHellmanKeyPair;

    move-result-object v1

    if-nez v1, :cond_4

    .line 982
    iget-object v0, p0, Lee/cyber/smartid/tse/KeyManager$13;->d:Lee/cyber/smartid/tse/KeyManager;

    invoke-static {v0}, Lee/cyber/smartid/tse/KeyManager;->a(Lee/cyber/smartid/tse/KeyManager;)Lee/cyber/smartid/tse/inter/ListenerAccess;

    move-result-object v0

    new-instance v1, Lee/cyber/smartid/tse/KeyManager$13$5;

    invoke-direct {v1, p0}, Lee/cyber/smartid/tse/KeyManager$13$5;-><init>(Lee/cyber/smartid/tse/KeyManager$13;)V

    invoke-interface {v0, v1}, Lee/cyber/smartid/tse/inter/ListenerAccess;->notifyUI(Ljava/lang/Runnable;)V

    return-void

    .line 989
    :cond_4
    invoke-virtual {v0}, Lee/cyber/smartid/tse/dto/EncryptKeyWrapper;->getKey()Lee/cyber/smartid/tse/dto/Key;

    move-result-object v1

    if-eqz v1, :cond_7

    invoke-virtual {v0}, Lee/cyber/smartid/tse/dto/EncryptKeyWrapper;->getKey()Lee/cyber/smartid/tse/dto/Key;

    move-result-object v1

    invoke-virtual {v1}, Lee/cyber/smartid/tse/dto/Key;->getSZId()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_5

    goto/16 :goto_0

    .line 1000
    :cond_5
    iget-object v1, p0, Lee/cyber/smartid/tse/KeyManager$13;->d:Lee/cyber/smartid/tse/KeyManager;

    invoke-static {v1}, Lee/cyber/smartid/tse/KeyManager;->c(Lee/cyber/smartid/tse/KeyManager;)Lee/cyber/smartid/tse/inter/ResourceAccess;

    move-result-object v1

    invoke-virtual {v0}, Lee/cyber/smartid/tse/dto/EncryptKeyWrapper;->getKey()Lee/cyber/smartid/tse/dto/Key;

    move-result-object v2

    invoke-virtual {v2}, Lee/cyber/smartid/tse/dto/Key;->getSZId()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lee/cyber/smartid/tse/inter/ResourceAccess;->getPreferredSZKTKPublicKey(Ljava/lang/String;)Lee/cyber/smartid/tse/dto/KTKPublicKey;

    move-result-object v4

    if-nez v4, :cond_6

    .line 1002
    iget-object v0, p0, Lee/cyber/smartid/tse/KeyManager$13;->d:Lee/cyber/smartid/tse/KeyManager;

    invoke-static {v0}, Lee/cyber/smartid/tse/KeyManager;->a(Lee/cyber/smartid/tse/KeyManager;)Lee/cyber/smartid/tse/inter/ListenerAccess;

    move-result-object v0

    new-instance v1, Lee/cyber/smartid/tse/KeyManager$13$7;

    invoke-direct {v1, p0}, Lee/cyber/smartid/tse/KeyManager$13$7;-><init>(Lee/cyber/smartid/tse/KeyManager$13;)V

    invoke-interface {v0, v1}, Lee/cyber/smartid/tse/inter/ListenerAccess;->notifyUI(Ljava/lang/Runnable;)V

    return-void

    .line 1016
    :cond_6
    :try_start_0
    iget-object v1, p0, Lee/cyber/smartid/tse/KeyManager$13;->d:Lee/cyber/smartid/tse/KeyManager;

    invoke-static {v1}, Lee/cyber/smartid/tse/KeyManager;->g(Lee/cyber/smartid/tse/KeyManager;)Lee/cyber/smartid/cryptolib/inter/CryptoOp;

    move-result-object v5

    iget-object v1, p0, Lee/cyber/smartid/tse/KeyManager$13;->a:Lee/cyber/smartid/tse/dto/jsonrpc/KeyData;

    invoke-virtual {v1}, Lee/cyber/smartid/tse/dto/jsonrpc/KeyData;->getServerDhMessage()Ljava/lang/String;

    move-result-object v6

    iget-object v1, p0, Lee/cyber/smartid/tse/KeyManager$13;->d:Lee/cyber/smartid/tse/KeyManager;

    invoke-static {v1}, Lee/cyber/smartid/tse/KeyManager;->e(Lee/cyber/smartid/tse/KeyManager;)Lee/cyber/smartid/cryptolib/inter/KeyGenerationOp;

    move-result-object v1

    invoke-virtual {v0}, Lee/cyber/smartid/tse/dto/EncryptKeyWrapper;->getDhKeyPair()Lee/cyber/smartid/cryptolib/dto/DiffieHellmanKeyPair;

    move-result-object v2

    iget-object v3, p0, Lee/cyber/smartid/tse/KeyManager$13;->d:Lee/cyber/smartid/tse/KeyManager;

    invoke-static {v3}, Lee/cyber/smartid/tse/KeyManager;->f(Lee/cyber/smartid/tse/KeyManager;)Lee/cyber/smartid/cryptolib/inter/EncodingOp;

    move-result-object v3

    iget-object v7, p0, Lee/cyber/smartid/tse/KeyManager$13;->a:Lee/cyber/smartid/tse/dto/jsonrpc/KeyData;

    invoke-virtual {v7}, Lee/cyber/smartid/tse/dto/jsonrpc/KeyData;->getServerDhPublicKey()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v3, v7}, Lee/cyber/smartid/cryptolib/inter/EncodingOp;->decodeDecimalFromBase64(Ljava/lang/String;)Ljava/math/BigInteger;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Lee/cyber/smartid/cryptolib/inter/KeyGenerationOp;->calculateConcatKDFWithSHA256(Lee/cyber/smartid/cryptolib/dto/DiffieHellmanKeyPair;Ljava/math/BigInteger;)[B

    move-result-object v7

    const/4 v8, 0x0

    const/4 v9, 0x0

    const-string v10, "CLIENT"

    invoke-interface/range {v5 .. v10}, Lee/cyber/smartid/cryptolib/inter/CryptoOp;->decryptFromTEKEncryptedJWE(Ljava/lang/String;[BLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/a/a/n;

    move-result-object v7

    .line 1017
    invoke-virtual {v7}, Lcom/a/a/n;->c()Lcom/a/a/m;

    move-result-object v1

    invoke-virtual {v1}, Lcom/a/a/m;->a()Ljava/lang/String;

    move-result-object v5
    :try_end_0
    .catch Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException; {:try_start_0 .. :try_end_0} :catch_1

    .line 1030
    :try_start_1
    iget-object v3, p0, Lee/cyber/smartid/tse/KeyManager$13;->d:Lee/cyber/smartid/tse/KeyManager;

    invoke-virtual {v0}, Lee/cyber/smartid/tse/dto/EncryptKeyWrapper;->getDhKeyPair()Lee/cyber/smartid/cryptolib/dto/DiffieHellmanKeyPair;

    move-result-object v0

    invoke-virtual {v0}, Lee/cyber/smartid/cryptolib/dto/DiffieHellmanKeyPair;->getPublicKey()Ljava/math/BigInteger;

    move-result-object v6

    iget-object v8, p0, Lee/cyber/smartid/tse/KeyManager$13;->a:Lee/cyber/smartid/tse/dto/jsonrpc/KeyData;

    invoke-static/range {v3 .. v8}, Lee/cyber/smartid/tse/KeyManager;->a(Lee/cyber/smartid/tse/KeyManager;Lee/cyber/smartid/tse/dto/KTKPublicKey;Ljava/lang/String;Ljava/math/BigInteger;Lcom/a/a/n;Lee/cyber/smartid/tse/dto/jsonrpc/KeyData;)Z

    move-result v0
    :try_end_1
    .catch Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException; {:try_start_1 .. :try_end_1} :catch_0

    .line 1040
    new-instance v1, Lee/cyber/smartid/tse/dto/jsonrpc/resp/ValidateKeyCreationResp;

    iget-object v2, p0, Lee/cyber/smartid/tse/KeyManager$13;->b:Ljava/lang/String;

    invoke-direct {v1, v2, v0}, Lee/cyber/smartid/tse/dto/jsonrpc/resp/ValidateKeyCreationResp;-><init>(Ljava/lang/String;Z)V

    .line 1041
    iget-object v0, p0, Lee/cyber/smartid/tse/KeyManager$13;->d:Lee/cyber/smartid/tse/KeyManager;

    invoke-static {v0}, Lee/cyber/smartid/tse/KeyManager;->a(Lee/cyber/smartid/tse/KeyManager;)Lee/cyber/smartid/tse/inter/ListenerAccess;

    move-result-object v0

    new-instance v2, Lee/cyber/smartid/tse/KeyManager$13$10;

    invoke-direct {v2, p0, v1}, Lee/cyber/smartid/tse/KeyManager$13$10;-><init>(Lee/cyber/smartid/tse/KeyManager$13;Lee/cyber/smartid/tse/dto/jsonrpc/resp/ValidateKeyCreationResp;)V

    invoke-interface {v0, v2}, Lee/cyber/smartid/tse/inter/ListenerAccess;->notifyUI(Ljava/lang/Runnable;)V

    return-void

    :catch_0
    move-exception v0

    .line 1032
    iget-object v1, p0, Lee/cyber/smartid/tse/KeyManager$13;->d:Lee/cyber/smartid/tse/KeyManager;

    invoke-static {v1}, Lee/cyber/smartid/tse/KeyManager;->a(Lee/cyber/smartid/tse/KeyManager;)Lee/cyber/smartid/tse/inter/ListenerAccess;

    move-result-object v1

    new-instance v2, Lee/cyber/smartid/tse/KeyManager$13$9;

    invoke-direct {v2, p0, v0}, Lee/cyber/smartid/tse/KeyManager$13$9;-><init>(Lee/cyber/smartid/tse/KeyManager$13;Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;)V

    invoke-interface {v1, v2}, Lee/cyber/smartid/tse/inter/ListenerAccess;->notifyUI(Ljava/lang/Runnable;)V

    return-void

    :catch_1
    move-exception v0

    .line 1019
    iget-object v1, p0, Lee/cyber/smartid/tse/KeyManager$13;->d:Lee/cyber/smartid/tse/KeyManager;

    invoke-static {v1}, Lee/cyber/smartid/tse/KeyManager;->a(Lee/cyber/smartid/tse/KeyManager;)Lee/cyber/smartid/tse/inter/ListenerAccess;

    move-result-object v1

    new-instance v2, Lee/cyber/smartid/tse/KeyManager$13$8;

    invoke-direct {v2, p0, v0}, Lee/cyber/smartid/tse/KeyManager$13$8;-><init>(Lee/cyber/smartid/tse/KeyManager$13;Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;)V

    invoke-interface {v1, v2}, Lee/cyber/smartid/tse/inter/ListenerAccess;->notifyUI(Ljava/lang/Runnable;)V

    return-void

    .line 990
    :cond_7
    :goto_0
    iget-object v0, p0, Lee/cyber/smartid/tse/KeyManager$13;->d:Lee/cyber/smartid/tse/KeyManager;

    invoke-static {v0}, Lee/cyber/smartid/tse/KeyManager;->a(Lee/cyber/smartid/tse/KeyManager;)Lee/cyber/smartid/tse/inter/ListenerAccess;

    move-result-object v0

    new-instance v1, Lee/cyber/smartid/tse/KeyManager$13$6;

    invoke-direct {v1, p0}, Lee/cyber/smartid/tse/KeyManager$13$6;-><init>(Lee/cyber/smartid/tse/KeyManager$13;)V

    invoke-interface {v0, v1}, Lee/cyber/smartid/tse/inter/ListenerAccess;->notifyUI(Ljava/lang/Runnable;)V

    return-void

    .line 945
    :cond_8
    :goto_1
    iget-object v0, p0, Lee/cyber/smartid/tse/KeyManager$13;->d:Lee/cyber/smartid/tse/KeyManager;

    invoke-static {v0}, Lee/cyber/smartid/tse/KeyManager;->a(Lee/cyber/smartid/tse/KeyManager;)Lee/cyber/smartid/tse/inter/ListenerAccess;

    move-result-object v0

    new-instance v1, Lee/cyber/smartid/tse/KeyManager$13$1;

    invoke-direct {v1, p0}, Lee/cyber/smartid/tse/KeyManager$13$1;-><init>(Lee/cyber/smartid/tse/KeyManager$13;)V

    invoke-interface {v0, v1}, Lee/cyber/smartid/tse/inter/ListenerAccess;->notifyUI(Ljava/lang/Runnable;)V

    return-void
.end method
