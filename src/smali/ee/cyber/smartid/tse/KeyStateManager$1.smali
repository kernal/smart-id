.class Lee/cyber/smartid/tse/KeyStateManager$1;
.super Ljava/lang/Object;
.source "KeyStateManager.java"

# interfaces
.implements Lee/cyber/smartid/tse/inter/StateWorkerJobListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lee/cyber/smartid/tse/KeyStateManager;->a(Ljava/lang/String;Lee/cyber/smartid/tse/dto/ProtoKeyState;Landroid/os/Bundle;Lee/cyber/smartid/tse/inter/TSEListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lee/cyber/smartid/tse/KeyStateManager;


# direct methods
.method constructor <init>(Lee/cyber/smartid/tse/KeyStateManager;)V
    .locals 0

    .line 286
    iput-object p1, p0, Lee/cyber/smartid/tse/KeyStateManager$1;->a:Lee/cyber/smartid/tse/KeyStateManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onJobCompleted(Ljava/lang/String;)V
    .locals 3

    .line 310
    iget-object v0, p0, Lee/cyber/smartid/tse/KeyStateManager$1;->a:Lee/cyber/smartid/tse/KeyStateManager;

    invoke-static {v0}, Lee/cyber/smartid/tse/KeyStateManager;->c(Lee/cyber/smartid/tse/KeyStateManager;)Ljava/lang/Object;

    move-result-object v0

    monitor-enter v0

    .line 312
    :try_start_0
    iget-object v1, p0, Lee/cyber/smartid/tse/KeyStateManager$1;->a:Lee/cyber/smartid/tse/KeyStateManager;

    invoke-static {v1, p1}, Lee/cyber/smartid/tse/KeyStateManager;->a(Lee/cyber/smartid/tse/KeyStateManager;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 314
    :try_start_1
    iget-object v1, p0, Lee/cyber/smartid/tse/KeyStateManager$1;->a:Lee/cyber/smartid/tse/KeyStateManager;

    invoke-static {v1}, Lee/cyber/smartid/tse/KeyStateManager;->a(Lee/cyber/smartid/tse/KeyStateManager;)Lee/cyber/smartid/tse/inter/StateWorkerJobListener;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 315
    iget-object v1, p0, Lee/cyber/smartid/tse/KeyStateManager$1;->a:Lee/cyber/smartid/tse/KeyStateManager;

    invoke-static {v1}, Lee/cyber/smartid/tse/KeyStateManager;->a(Lee/cyber/smartid/tse/KeyStateManager;)Lee/cyber/smartid/tse/inter/StateWorkerJobListener;

    move-result-object v1

    invoke-interface {v1, p1}, Lee/cyber/smartid/tse/inter/StateWorkerJobListener;->onJobCompleted(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 318
    :try_start_2
    iget-object v1, p0, Lee/cyber/smartid/tse/KeyStateManager$1;->a:Lee/cyber/smartid/tse/KeyStateManager;

    invoke-static {v1}, Lee/cyber/smartid/tse/KeyStateManager;->b(Lee/cyber/smartid/tse/KeyStateManager;)Lee/cyber/smartid/tse/inter/LogAccess;

    move-result-object v1

    const-string v2, "startKeyState - onJobStarted"

    invoke-interface {v1, v2, p1}, Lee/cyber/smartid/tse/inter/LogAccess;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 320
    :cond_0
    :goto_0
    iget-object p1, p0, Lee/cyber/smartid/tse/KeyStateManager$1;->a:Lee/cyber/smartid/tse/KeyStateManager;

    invoke-static {p1}, Lee/cyber/smartid/tse/KeyStateManager;->d(Lee/cyber/smartid/tse/KeyStateManager;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 324
    :try_start_3
    iget-object p1, p0, Lee/cyber/smartid/tse/KeyStateManager$1;->a:Lee/cyber/smartid/tse/KeyStateManager;

    invoke-static {p1}, Lee/cyber/smartid/tse/KeyStateManager;->e(Lee/cyber/smartid/tse/KeyStateManager;)Landroid/os/PowerManager$WakeLock;

    move-result-object p1

    :goto_1
    invoke-static {p1}, Lee/cyber/smartid/tse/util/Util;->releaseWakeLock(Landroid/os/PowerManager$WakeLock;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_2

    :catchall_0
    move-exception p1

    goto :goto_3

    :catch_1
    move-exception p1

    .line 322
    :try_start_4
    iget-object v1, p0, Lee/cyber/smartid/tse/KeyStateManager$1;->a:Lee/cyber/smartid/tse/KeyStateManager;

    invoke-static {v1}, Lee/cyber/smartid/tse/KeyStateManager;->b(Lee/cyber/smartid/tse/KeyStateManager;)Lee/cyber/smartid/tse/inter/LogAccess;

    move-result-object v1

    const-string v2, "startKeyState - onJobCompleted"

    invoke-interface {v1, v2, p1}, Lee/cyber/smartid/tse/inter/LogAccess;->e(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 324
    :try_start_5
    iget-object p1, p0, Lee/cyber/smartid/tse/KeyStateManager$1;->a:Lee/cyber/smartid/tse/KeyStateManager;

    invoke-static {p1}, Lee/cyber/smartid/tse/KeyStateManager;->e(Lee/cyber/smartid/tse/KeyStateManager;)Landroid/os/PowerManager$WakeLock;

    move-result-object p1

    goto :goto_1

    .line 326
    :goto_2
    monitor-exit v0

    return-void

    .line 324
    :goto_3
    iget-object v1, p0, Lee/cyber/smartid/tse/KeyStateManager$1;->a:Lee/cyber/smartid/tse/KeyStateManager;

    invoke-static {v1}, Lee/cyber/smartid/tse/KeyStateManager;->e(Lee/cyber/smartid/tse/KeyStateManager;)Landroid/os/PowerManager$WakeLock;

    move-result-object v1

    invoke-static {v1}, Lee/cyber/smartid/tse/util/Util;->releaseWakeLock(Landroid/os/PowerManager$WakeLock;)V

    .line 325
    throw p1

    :catchall_1
    move-exception p1

    .line 326
    monitor-exit v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    throw p1
.end method

.method public onJobDequeued(Ljava/lang/String;)V
    .locals 0

    return-void
.end method

.method public onJobQueued(Ljava/lang/String;)V
    .locals 0

    return-void
.end method

.method public onJobStarted(Ljava/lang/String;)V
    .locals 2

    .line 300
    :try_start_0
    iget-object v0, p0, Lee/cyber/smartid/tse/KeyStateManager$1;->a:Lee/cyber/smartid/tse/KeyStateManager;

    invoke-static {v0}, Lee/cyber/smartid/tse/KeyStateManager;->a(Lee/cyber/smartid/tse/KeyStateManager;)Lee/cyber/smartid/tse/inter/StateWorkerJobListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 301
    iget-object v0, p0, Lee/cyber/smartid/tse/KeyStateManager$1;->a:Lee/cyber/smartid/tse/KeyStateManager;

    invoke-static {v0}, Lee/cyber/smartid/tse/KeyStateManager;->a(Lee/cyber/smartid/tse/KeyStateManager;)Lee/cyber/smartid/tse/inter/StateWorkerJobListener;

    move-result-object v0

    invoke-interface {v0, p1}, Lee/cyber/smartid/tse/inter/StateWorkerJobListener;->onJobStarted(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 304
    iget-object v0, p0, Lee/cyber/smartid/tse/KeyStateManager$1;->a:Lee/cyber/smartid/tse/KeyStateManager;

    invoke-static {v0}, Lee/cyber/smartid/tse/KeyStateManager;->b(Lee/cyber/smartid/tse/KeyStateManager;)Lee/cyber/smartid/tse/inter/LogAccess;

    move-result-object v0

    const-string v1, "startKeyState - onJobStarted"

    invoke-interface {v0, v1, p1}, Lee/cyber/smartid/tse/inter/LogAccess;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_0
    :goto_0
    return-void
.end method
