.class Lee/cyber/smartid/tse/KeyGenerationManager$3;
.super Ljava/lang/Object;
.source "KeyGenerationManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lee/cyber/smartid/tse/KeyGenerationManager;->a(Lee/cyber/smartid/tse/inter/WallClock;Lee/cyber/smartid/tse/inter/ResourceAccess;[Ljava/lang/String;Ljava/lang/Exception;[Lee/cyber/smartid/cryptolib/dto/TestResult;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lee/cyber/smartid/tse/KeyGenerationManager;


# direct methods
.method constructor <init>(Lee/cyber/smartid/tse/KeyGenerationManager;)V
    .locals 0

    .line 177
    iput-object p1, p0, Lee/cyber/smartid/tse/KeyGenerationManager$3;->a:Lee/cyber/smartid/tse/KeyGenerationManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .line 180
    iget-object v0, p0, Lee/cyber/smartid/tse/KeyGenerationManager$3;->a:Lee/cyber/smartid/tse/KeyGenerationManager;

    invoke-static {v0}, Lee/cyber/smartid/tse/KeyGenerationManager;->c(Lee/cyber/smartid/tse/KeyGenerationManager;)Lee/cyber/smartid/tse/KeyGenerationManager$KeyGenerationWorkerListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 181
    iget-object v0, p0, Lee/cyber/smartid/tse/KeyGenerationManager$3;->a:Lee/cyber/smartid/tse/KeyGenerationManager;

    invoke-static {v0}, Lee/cyber/smartid/tse/KeyGenerationManager;->c(Lee/cyber/smartid/tse/KeyGenerationManager;)Lee/cyber/smartid/tse/KeyGenerationManager$KeyGenerationWorkerListener;

    move-result-object v0

    iget-object v1, p0, Lee/cyber/smartid/tse/KeyGenerationManager$3;->a:Lee/cyber/smartid/tse/KeyGenerationManager;

    invoke-static {v1}, Lee/cyber/smartid/tse/KeyGenerationManager;->d(Lee/cyber/smartid/tse/KeyGenerationManager;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lee/cyber/smartid/tse/KeyGenerationManager$3;->a:Lee/cyber/smartid/tse/KeyGenerationManager;

    invoke-static {v2}, Lee/cyber/smartid/tse/KeyGenerationManager;->e(Lee/cyber/smartid/tse/KeyGenerationManager;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    iget-object v3, p0, Lee/cyber/smartid/tse/KeyGenerationManager$3;->a:Lee/cyber/smartid/tse/KeyGenerationManager;

    invoke-static {v3}, Lee/cyber/smartid/tse/KeyGenerationManager;->f(Lee/cyber/smartid/tse/KeyGenerationManager;)I

    move-result v3

    invoke-interface {v0, v1, v2, v3}, Lee/cyber/smartid/tse/KeyGenerationManager$KeyGenerationWorkerListener;->onGenerateKeysProgress(Ljava/lang/String;II)V

    :cond_0
    return-void
.end method
