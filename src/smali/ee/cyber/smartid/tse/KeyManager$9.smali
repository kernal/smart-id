.class Lee/cyber/smartid/tse/KeyManager$9;
.super Ljava/lang/Object;
.source "KeyManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lee/cyber/smartid/tse/KeyManager;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;

.field final synthetic c:Lee/cyber/smartid/tse/KeyManager;


# direct methods
.method constructor <init>(Lee/cyber/smartid/tse/KeyManager;Ljava/lang/String;Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;)V
    .locals 0

    .line 659
    iput-object p1, p0, Lee/cyber/smartid/tse/KeyManager$9;->c:Lee/cyber/smartid/tse/KeyManager;

    iput-object p2, p0, Lee/cyber/smartid/tse/KeyManager$9;->a:Ljava/lang/String;

    iput-object p3, p0, Lee/cyber/smartid/tse/KeyManager$9;->b:Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .line 662
    iget-object v0, p0, Lee/cyber/smartid/tse/KeyManager$9;->c:Lee/cyber/smartid/tse/KeyManager;

    invoke-static {v0}, Lee/cyber/smartid/tse/KeyManager;->a(Lee/cyber/smartid/tse/KeyManager;)Lee/cyber/smartid/tse/inter/ListenerAccess;

    move-result-object v0

    iget-object v1, p0, Lee/cyber/smartid/tse/KeyManager$9;->a:Ljava/lang/String;

    iget-object v2, p0, Lee/cyber/smartid/tse/KeyManager$9;->c:Lee/cyber/smartid/tse/KeyManager;

    invoke-static {v2}, Lee/cyber/smartid/tse/KeyManager;->a(Lee/cyber/smartid/tse/KeyManager;)Lee/cyber/smartid/tse/inter/ListenerAccess;

    move-result-object v2

    iget-object v3, p0, Lee/cyber/smartid/tse/KeyManager$9;->a:Ljava/lang/String;

    const-class v4, Lee/cyber/smartid/tse/inter/EncryptKeyListener;

    const/4 v5, 0x1

    invoke-interface {v2, v3, v5, v4}, Lee/cyber/smartid/tse/inter/ListenerAccess;->getListener(Ljava/lang/String;ZLjava/lang/Class;)Lee/cyber/smartid/tse/inter/TSEListener;

    move-result-object v2

    iget-object v3, p0, Lee/cyber/smartid/tse/KeyManager$9;->c:Lee/cyber/smartid/tse/KeyManager;

    invoke-static {v3}, Lee/cyber/smartid/tse/KeyManager;->d(Lee/cyber/smartid/tse/KeyManager;)Lee/cyber/smartid/tse/inter/WallClock;

    move-result-object v3

    iget-object v4, p0, Lee/cyber/smartid/tse/KeyManager$9;->c:Lee/cyber/smartid/tse/KeyManager;

    invoke-static {v4}, Lee/cyber/smartid/tse/KeyManager;->c(Lee/cyber/smartid/tse/KeyManager;)Lee/cyber/smartid/tse/inter/ResourceAccess;

    move-result-object v4

    invoke-interface {v4}, Lee/cyber/smartid/tse/inter/ResourceAccess;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    sget v5, Lee/cyber/smartid/tse/R$string;->err_failed_to_encrypt_the_server_part_of_the_private_key:I

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lee/cyber/smartid/tse/KeyManager$9;->b:Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;

    const-wide/16 v6, 0x3fa

    invoke-static {v3, v6, v7, v4, v5}, Lee/cyber/smartid/tse/dto/TSEError;->from(Lee/cyber/smartid/tse/inter/WallClock;JLjava/lang/String;Ljava/lang/Throwable;)Lee/cyber/smartid/tse/dto/TSEError;

    move-result-object v3

    invoke-interface {v0, v1, v2, v3}, Lee/cyber/smartid/tse/inter/ListenerAccess;->notifyError(Ljava/lang/String;Lee/cyber/smartid/tse/inter/TSEListener;Lee/cyber/smartid/tse/dto/TSEError;)V

    return-void
.end method
