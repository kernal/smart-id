.class Lee/cyber/smartid/tse/KeyManager$1$1;
.super Ljava/lang/Object;
.source "KeyManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lee/cyber/smartid/tse/KeyManager$1;->onGenerateKeysSuccess(Ljava/lang/String;Ljava/util/ArrayList;J[Lee/cyber/smartid/cryptolib/dto/TestResult;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:[Lee/cyber/smartid/cryptolib/dto/TestResult;

.field final synthetic c:Lee/cyber/smartid/tse/KeyManager$1;


# direct methods
.method constructor <init>(Lee/cyber/smartid/tse/KeyManager$1;Ljava/lang/String;[Lee/cyber/smartid/cryptolib/dto/TestResult;)V
    .locals 0

    .line 270
    iput-object p1, p0, Lee/cyber/smartid/tse/KeyManager$1$1;->c:Lee/cyber/smartid/tse/KeyManager$1;

    iput-object p2, p0, Lee/cyber/smartid/tse/KeyManager$1$1;->a:Ljava/lang/String;

    iput-object p3, p0, Lee/cyber/smartid/tse/KeyManager$1$1;->b:[Lee/cyber/smartid/cryptolib/dto/TestResult;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .line 273
    iget-object v0, p0, Lee/cyber/smartid/tse/KeyManager$1$1;->c:Lee/cyber/smartid/tse/KeyManager$1;

    iget-object v0, v0, Lee/cyber/smartid/tse/KeyManager$1;->c:Lee/cyber/smartid/tse/KeyManager;

    invoke-static {v0}, Lee/cyber/smartid/tse/KeyManager;->a(Lee/cyber/smartid/tse/KeyManager;)Lee/cyber/smartid/tse/inter/ListenerAccess;

    move-result-object v0

    iget-object v1, p0, Lee/cyber/smartid/tse/KeyManager$1$1;->a:Ljava/lang/String;

    const-class v2, Lee/cyber/smartid/tse/inter/GenerateKeysListener;

    const/4 v3, 0x0

    invoke-interface {v0, v1, v3, v2}, Lee/cyber/smartid/tse/inter/ListenerAccess;->getListener(Ljava/lang/String;ZLjava/lang/Class;)Lee/cyber/smartid/tse/inter/TSEListener;

    move-result-object v0

    check-cast v0, Lee/cyber/smartid/tse/inter/GenerateKeysListener;

    if-eqz v0, :cond_0

    .line 275
    iget-object v1, p0, Lee/cyber/smartid/tse/KeyManager$1$1;->a:Ljava/lang/String;

    iget-object v2, p0, Lee/cyber/smartid/tse/KeyManager$1$1;->b:[Lee/cyber/smartid/cryptolib/dto/TestResult;

    invoke-interface {v0, v1, v2}, Lee/cyber/smartid/tse/inter/GenerateKeysListener;->onGenerateKeysPRNGTestUpdate(Ljava/lang/String;[Lee/cyber/smartid/cryptolib/dto/TestResult;)V

    :cond_0
    return-void
.end method
