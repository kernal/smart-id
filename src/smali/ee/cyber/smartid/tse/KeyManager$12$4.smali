.class Lee/cyber/smartid/tse/KeyManager$12$4;
.super Ljava/lang/Object;
.source "KeyManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lee/cyber/smartid/tse/KeyManager$12;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:Lee/cyber/smartid/tse/KeyManager$12;


# direct methods
.method constructor <init>(Lee/cyber/smartid/tse/KeyManager$12;Ljava/lang/String;)V
    .locals 0

    .line 909
    iput-object p1, p0, Lee/cyber/smartid/tse/KeyManager$12$4;->b:Lee/cyber/smartid/tse/KeyManager$12;

    iput-object p2, p0, Lee/cyber/smartid/tse/KeyManager$12$4;->a:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .line 912
    iget-object v0, p0, Lee/cyber/smartid/tse/KeyManager$12$4;->b:Lee/cyber/smartid/tse/KeyManager$12;

    iget-object v0, v0, Lee/cyber/smartid/tse/KeyManager$12;->c:Lee/cyber/smartid/tse/KeyManager;

    invoke-static {v0}, Lee/cyber/smartid/tse/KeyManager;->a(Lee/cyber/smartid/tse/KeyManager;)Lee/cyber/smartid/tse/inter/ListenerAccess;

    move-result-object v0

    iget-object v1, p0, Lee/cyber/smartid/tse/KeyManager$12$4;->b:Lee/cyber/smartid/tse/KeyManager$12;

    iget-object v1, v1, Lee/cyber/smartid/tse/KeyManager$12;->b:Ljava/lang/String;

    const-class v2, Lee/cyber/smartid/tse/inter/GenerateDiffieHellmanKeyPairListener;

    const/4 v3, 0x1

    invoke-interface {v0, v1, v3, v2}, Lee/cyber/smartid/tse/inter/ListenerAccess;->getListener(Ljava/lang/String;ZLjava/lang/Class;)Lee/cyber/smartid/tse/inter/TSEListener;

    move-result-object v0

    check-cast v0, Lee/cyber/smartid/tse/inter/GenerateDiffieHellmanKeyPairListener;

    if-eqz v0, :cond_0

    .line 914
    iget-object v1, p0, Lee/cyber/smartid/tse/KeyManager$12$4;->b:Lee/cyber/smartid/tse/KeyManager$12;

    iget-object v1, v1, Lee/cyber/smartid/tse/KeyManager$12;->b:Ljava/lang/String;

    new-instance v2, Lee/cyber/smartid/tse/dto/jsonrpc/resp/GenerateDiffieHellmanKeyPairResp;

    iget-object v3, p0, Lee/cyber/smartid/tse/KeyManager$12$4;->b:Lee/cyber/smartid/tse/KeyManager$12;

    iget-object v3, v3, Lee/cyber/smartid/tse/KeyManager$12;->b:Ljava/lang/String;

    iget-object v4, p0, Lee/cyber/smartid/tse/KeyManager$12$4;->a:Ljava/lang/String;

    invoke-direct {v2, v3, v4}, Lee/cyber/smartid/tse/dto/jsonrpc/resp/GenerateDiffieHellmanKeyPairResp;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1, v2}, Lee/cyber/smartid/tse/inter/GenerateDiffieHellmanKeyPairListener;->onGenerateDiffieHellmanKeyPairSuccess(Ljava/lang/String;Lee/cyber/smartid/tse/dto/jsonrpc/resp/GenerateDiffieHellmanKeyPairResp;)V

    :cond_0
    return-void
.end method
