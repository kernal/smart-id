.class Lee/cyber/smartid/tse/KeyStateManager$3;
.super Ljava/lang/Object;
.source "KeyStateManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lee/cyber/smartid/tse/KeyStateManager;->submitClientSecondPart(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Lee/cyber/smartid/tse/inter/SubmitClientSecondPartListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:Ljava/lang/String;

.field final synthetic c:Ljava/lang/String;

.field final synthetic d:Ljava/lang/String;

.field final synthetic e:Ljava/lang/String;

.field final synthetic f:Landroid/os/Bundle;

.field final synthetic g:Lee/cyber/smartid/tse/inter/SubmitClientSecondPartListener;

.field final synthetic h:Lee/cyber/smartid/tse/KeyStateManager;


# direct methods
.method constructor <init>(Lee/cyber/smartid/tse/KeyStateManager;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Lee/cyber/smartid/tse/inter/SubmitClientSecondPartListener;)V
    .locals 0

    .line 685
    iput-object p1, p0, Lee/cyber/smartid/tse/KeyStateManager$3;->h:Lee/cyber/smartid/tse/KeyStateManager;

    iput-object p2, p0, Lee/cyber/smartid/tse/KeyStateManager$3;->a:Ljava/lang/String;

    iput-object p3, p0, Lee/cyber/smartid/tse/KeyStateManager$3;->b:Ljava/lang/String;

    iput-object p4, p0, Lee/cyber/smartid/tse/KeyStateManager$3;->c:Ljava/lang/String;

    iput-object p5, p0, Lee/cyber/smartid/tse/KeyStateManager$3;->d:Ljava/lang/String;

    iput-object p6, p0, Lee/cyber/smartid/tse/KeyStateManager$3;->e:Ljava/lang/String;

    iput-object p7, p0, Lee/cyber/smartid/tse/KeyStateManager$3;->f:Landroid/os/Bundle;

    iput-object p8, p0, Lee/cyber/smartid/tse/KeyStateManager$3;->g:Lee/cyber/smartid/tse/inter/SubmitClientSecondPartListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 10

    .line 688
    iget-object v0, p0, Lee/cyber/smartid/tse/KeyStateManager$3;->h:Lee/cyber/smartid/tse/KeyStateManager;

    invoke-static {v0}, Lee/cyber/smartid/tse/KeyStateManager;->g(Lee/cyber/smartid/tse/KeyStateManager;)Ljava/lang/Object;

    move-result-object v0

    monitor-enter v0

    .line 689
    :try_start_0
    iget-object v1, p0, Lee/cyber/smartid/tse/KeyStateManager$3;->h:Lee/cyber/smartid/tse/KeyStateManager;

    invoke-static {v1}, Lee/cyber/smartid/tse/KeyStateManager;->b(Lee/cyber/smartid/tse/KeyStateManager;)Lee/cyber/smartid/tse/inter/LogAccess;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "submitClientSecondPart - keyType: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lee/cyber/smartid/tse/KeyStateManager$3;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lee/cyber/smartid/tse/inter/LogAccess;->d(Ljava/lang/String;)V

    .line 690
    iget-object v1, p0, Lee/cyber/smartid/tse/KeyStateManager$3;->h:Lee/cyber/smartid/tse/KeyStateManager;

    invoke-static {v1}, Lee/cyber/smartid/tse/KeyStateManager;->h(Lee/cyber/smartid/tse/KeyStateManager;)Lee/cyber/smartid/tse/inter/KeyManagerAccess;

    move-result-object v1

    iget-object v2, p0, Lee/cyber/smartid/tse/KeyStateManager$3;->b:Ljava/lang/String;

    invoke-interface {v1, v2}, Lee/cyber/smartid/tse/inter/KeyManagerAccess;->getKey(Ljava/lang/String;)Lee/cyber/smartid/tse/dto/Key;

    move-result-object v1

    if-nez v1, :cond_0

    .line 693
    iget-object v1, p0, Lee/cyber/smartid/tse/KeyStateManager$3;->h:Lee/cyber/smartid/tse/KeyStateManager;

    invoke-static {v1}, Lee/cyber/smartid/tse/KeyStateManager;->f(Lee/cyber/smartid/tse/KeyStateManager;)Lee/cyber/smartid/tse/inter/ListenerAccess;

    move-result-object v1

    new-instance v2, Lee/cyber/smartid/tse/KeyStateManager$3$1;

    invoke-direct {v2, p0}, Lee/cyber/smartid/tse/KeyStateManager$3$1;-><init>(Lee/cyber/smartid/tse/KeyStateManager$3;)V

    invoke-interface {v1, v2}, Lee/cyber/smartid/tse/inter/ListenerAccess;->notifyUI(Ljava/lang/Runnable;)V

    .line 699
    monitor-exit v0

    return-void

    .line 703
    :cond_0
    iget-object v1, p0, Lee/cyber/smartid/tse/KeyStateManager$3;->h:Lee/cyber/smartid/tse/KeyStateManager;

    invoke-static {v1}, Lee/cyber/smartid/tse/KeyStateManager;->h(Lee/cyber/smartid/tse/KeyStateManager;)Lee/cyber/smartid/tse/inter/KeyManagerAccess;

    move-result-object v1

    iget-object v2, p0, Lee/cyber/smartid/tse/KeyStateManager$3;->d:Ljava/lang/String;

    invoke-static {v2}, Lee/cyber/smartid/tse/dto/EncryptKeyWrapper;->getId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lee/cyber/smartid/tse/inter/KeyManagerAccess;->loadEncryptedKey(Ljava/lang/String;)Lee/cyber/smartid/tse/dto/EncryptKeyWrapper;

    move-result-object v1

    if-nez v1, :cond_1

    .line 706
    iget-object v1, p0, Lee/cyber/smartid/tse/KeyStateManager$3;->h:Lee/cyber/smartid/tse/KeyStateManager;

    invoke-static {v1}, Lee/cyber/smartid/tse/KeyStateManager;->f(Lee/cyber/smartid/tse/KeyStateManager;)Lee/cyber/smartid/tse/inter/ListenerAccess;

    move-result-object v1

    new-instance v2, Lee/cyber/smartid/tse/KeyStateManager$3$2;

    invoke-direct {v2, p0}, Lee/cyber/smartid/tse/KeyStateManager$3$2;-><init>(Lee/cyber/smartid/tse/KeyStateManager$3;)V

    invoke-interface {v1, v2}, Lee/cyber/smartid/tse/inter/ListenerAccess;->notifyUI(Ljava/lang/Runnable;)V

    .line 712
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    .line 717
    :cond_1
    :try_start_1
    iget-object v2, p0, Lee/cyber/smartid/tse/KeyStateManager$3;->h:Lee/cyber/smartid/tse/KeyStateManager;

    invoke-static {v2}, Lee/cyber/smartid/tse/KeyStateManager;->k(Lee/cyber/smartid/tse/KeyStateManager;)Ljava/lang/String;

    move-result-object v7
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 730
    :try_start_2
    iget-object v2, p0, Lee/cyber/smartid/tse/KeyStateManager$3;->h:Lee/cyber/smartid/tse/KeyStateManager;

    invoke-static {v2}, Lee/cyber/smartid/tse/KeyStateManager;->j(Lee/cyber/smartid/tse/KeyStateManager;)Lee/cyber/smartid/tse/inter/ResourceAccess;

    move-result-object v2

    iget-object v3, p0, Lee/cyber/smartid/tse/KeyStateManager$3;->b:Ljava/lang/String;

    invoke-interface {v2, v3}, Lee/cyber/smartid/tse/inter/ResourceAccess;->getKeyStateIdByKeyId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lee/cyber/smartid/tse/KeyStateManager$3;->b:Ljava/lang/String;

    iget-object v5, p0, Lee/cyber/smartid/tse/KeyStateManager$3;->e:Ljava/lang/String;

    iget-object v6, p0, Lee/cyber/smartid/tse/KeyStateManager$3;->a:Ljava/lang/String;

    invoke-virtual {v1}, Lee/cyber/smartid/tse/dto/EncryptKeyWrapper;->getD1PrimePrimeEncoded()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v1}, Lee/cyber/smartid/tse/dto/EncryptKeyWrapper;->getN1()Ljava/lang/String;

    move-result-object v9

    invoke-static/range {v3 .. v9}, Lee/cyber/smartid/tse/dto/ProtoKeyState;->forSubmitClientSecondPart(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lee/cyber/smartid/tse/dto/ProtoKeyState;

    move-result-object v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 733
    :try_start_3
    iget-object v3, p0, Lee/cyber/smartid/tse/KeyStateManager$3;->h:Lee/cyber/smartid/tse/KeyStateManager;

    invoke-static {v3}, Lee/cyber/smartid/tse/KeyStateManager;->h(Lee/cyber/smartid/tse/KeyStateManager;)Lee/cyber/smartid/tse/inter/KeyManagerAccess;

    move-result-object v3

    invoke-virtual {v1}, Lee/cyber/smartid/tse/dto/EncryptKeyWrapper;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v3, v1}, Lee/cyber/smartid/tse/inter/KeyManagerAccess;->removeEncryptedKey(Ljava/lang/String;)V

    .line 735
    iget-object v1, p0, Lee/cyber/smartid/tse/KeyStateManager$3;->h:Lee/cyber/smartid/tse/KeyStateManager;

    iget-object v3, p0, Lee/cyber/smartid/tse/KeyStateManager$3;->c:Ljava/lang/String;

    iget-object v4, p0, Lee/cyber/smartid/tse/KeyStateManager$3;->f:Landroid/os/Bundle;

    iget-object v5, p0, Lee/cyber/smartid/tse/KeyStateManager$3;->g:Lee/cyber/smartid/tse/inter/SubmitClientSecondPartListener;

    invoke-virtual {v1, v3, v2, v4, v5}, Lee/cyber/smartid/tse/KeyStateManager;->queue(Ljava/lang/String;Lee/cyber/smartid/tse/dto/ProtoKeyState;Landroid/os/Bundle;Lee/cyber/smartid/tse/inter/TSEListener;)V
    :try_end_3
    .catch Lee/cyber/smartid/cryptolib/dto/StorageException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 747
    :try_start_4
    monitor-exit v0

    return-void

    :catch_0
    move-exception v1

    .line 737
    iget-object v2, p0, Lee/cyber/smartid/tse/KeyStateManager$3;->h:Lee/cyber/smartid/tse/KeyStateManager;

    invoke-static {v2}, Lee/cyber/smartid/tse/KeyStateManager;->b(Lee/cyber/smartid/tse/KeyStateManager;)Lee/cyber/smartid/tse/inter/LogAccess;

    move-result-object v2

    const-string v3, "submitClientSecondPart"

    invoke-interface {v2, v3, v1}, Lee/cyber/smartid/tse/inter/LogAccess;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 738
    iget-object v2, p0, Lee/cyber/smartid/tse/KeyStateManager$3;->h:Lee/cyber/smartid/tse/KeyStateManager;

    new-instance v3, Lee/cyber/smartid/tse/KeyStateManager$3$4;

    invoke-direct {v3, p0, v1}, Lee/cyber/smartid/tse/KeyStateManager$3$4;-><init>(Lee/cyber/smartid/tse/KeyStateManager$3;Lee/cyber/smartid/cryptolib/dto/StorageException;)V

    invoke-static {v2, v3}, Lee/cyber/smartid/tse/KeyStateManager;->a(Lee/cyber/smartid/tse/KeyStateManager;Ljava/lang/Runnable;)V

    .line 745
    monitor-exit v0

    return-void

    :catch_1
    move-exception v1

    .line 720
    iget-object v2, p0, Lee/cyber/smartid/tse/KeyStateManager$3;->h:Lee/cyber/smartid/tse/KeyStateManager;

    invoke-static {v2}, Lee/cyber/smartid/tse/KeyStateManager;->f(Lee/cyber/smartid/tse/KeyStateManager;)Lee/cyber/smartid/tse/inter/ListenerAccess;

    move-result-object v2

    new-instance v3, Lee/cyber/smartid/tse/KeyStateManager$3$3;

    invoke-direct {v3, p0, v1}, Lee/cyber/smartid/tse/KeyStateManager$3$3;-><init>(Lee/cyber/smartid/tse/KeyStateManager$3;Ljava/lang/Throwable;)V

    invoke-interface {v2, v3}, Lee/cyber/smartid/tse/inter/ListenerAccess;->notifyUI(Ljava/lang/Runnable;)V

    .line 726
    monitor-exit v0

    return-void

    :catchall_0
    move-exception v1

    .line 747
    monitor-exit v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    throw v1
.end method
