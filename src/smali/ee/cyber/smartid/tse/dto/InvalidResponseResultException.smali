.class public Lee/cyber/smartid/tse/dto/InvalidResponseResultException;
.super Ljava/io/IOException;
.source "InvalidResponseResultException.java"


# static fields
.field private static final serialVersionUID:J = 0x1faa45d81c959b00L


# direct methods
.method public constructor <init>()V
    .locals 1

    const-string v0, "Invalid response result data in the JSON-RPC request."

    .line 21
    invoke-direct {p0, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 0

    .line 30
    invoke-direct {p0, p1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    return-void
.end method
