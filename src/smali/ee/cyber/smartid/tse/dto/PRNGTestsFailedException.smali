.class public Lee/cyber/smartid/tse/dto/PRNGTestsFailedException;
.super Ljava/io/IOException;
.source "PRNGTestsFailedException.java"


# static fields
.field private static final serialVersionUID:J = 0x1a042776ac049fabL


# instance fields
.field private final testResults:[Lee/cyber/smartid/cryptolib/dto/TestResult;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 23
    invoke-direct {p0}, Ljava/io/IOException;-><init>()V

    const/4 v0, 0x0

    .line 24
    iput-object v0, p0, Lee/cyber/smartid/tse/dto/PRNGTestsFailedException;->testResults:[Lee/cyber/smartid/cryptolib/dto/TestResult;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/Throwable;[Lee/cyber/smartid/cryptolib/dto/TestResult;)V
    .locals 0

    .line 64
    invoke-direct {p0, p1, p2}, Ljava/io/IOException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 65
    iput-object p3, p0, Lee/cyber/smartid/tse/dto/PRNGTestsFailedException;->testResults:[Lee/cyber/smartid/cryptolib/dto/TestResult;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;[Lee/cyber/smartid/cryptolib/dto/TestResult;)V
    .locals 0

    .line 52
    invoke-direct {p0, p1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    .line 53
    iput-object p2, p0, Lee/cyber/smartid/tse/dto/PRNGTestsFailedException;->testResults:[Lee/cyber/smartid/cryptolib/dto/TestResult;

    return-void
.end method

.method public constructor <init>(Ljava/lang/Throwable;[Lee/cyber/smartid/cryptolib/dto/TestResult;)V
    .locals 0

    .line 75
    invoke-direct {p0, p1}, Ljava/io/IOException;-><init>(Ljava/lang/Throwable;)V

    .line 76
    iput-object p2, p0, Lee/cyber/smartid/tse/dto/PRNGTestsFailedException;->testResults:[Lee/cyber/smartid/cryptolib/dto/TestResult;

    return-void
.end method


# virtual methods
.method public getCode()J
    .locals 2

    const-wide/16 v0, 0x418

    return-wide v0
.end method

.method public getTestResults()[Lee/cyber/smartid/cryptolib/dto/TestResult;
    .locals 1

    .line 42
    iget-object v0, p0, Lee/cyber/smartid/tse/dto/PRNGTestsFailedException;->testResults:[Lee/cyber/smartid/cryptolib/dto/TestResult;

    return-object v0
.end method
