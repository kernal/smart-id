.class public Lee/cyber/smartid/tse/dto/PinInfo;
.super Ljava/lang/Object;
.source "PinInfo.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = -0x1fc1777872f9aadeL


# instance fields
.field private pin:Ljava/lang/String;

.field private reference:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p1, p0, Lee/cyber/smartid/tse/dto/PinInfo;->reference:Ljava/lang/String;

    .line 28
    iput-object p2, p0, Lee/cyber/smartid/tse/dto/PinInfo;->pin:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public getPin()Ljava/lang/String;
    .locals 1

    .line 55
    iget-object v0, p0, Lee/cyber/smartid/tse/dto/PinInfo;->pin:Ljava/lang/String;

    return-object v0
.end method

.method public getReference()Ljava/lang/String;
    .locals 1

    .line 37
    iget-object v0, p0, Lee/cyber/smartid/tse/dto/PinInfo;->reference:Ljava/lang/String;

    return-object v0
.end method

.method public setPin(Ljava/lang/String;)V
    .locals 0

    .line 64
    iput-object p1, p0, Lee/cyber/smartid/tse/dto/PinInfo;->pin:Ljava/lang/String;

    return-void
.end method

.method public setReference(Ljava/lang/String;)V
    .locals 0

    .line 46
    iput-object p1, p0, Lee/cyber/smartid/tse/dto/PinInfo;->reference:Ljava/lang/String;

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .line 74
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "PinInfo{reference=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lee/cyber/smartid/tse/dto/PinInfo;->reference:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v2, ", pin=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lee/cyber/smartid/tse/dto/PinInfo;->pin:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
