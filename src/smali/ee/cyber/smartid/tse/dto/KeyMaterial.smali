.class public Lee/cyber/smartid/tse/dto/KeyMaterial;
.super Ljava/lang/Object;
.source "KeyMaterial.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = -0x572a9e1b1cb235e2L


# instance fields
.field private d1Prime:Ljava/lang/String;

.field private d1PrimePrime:Ljava/lang/String;

.field private keySize:I

.field private n1:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 0

    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    iput-object p1, p0, Lee/cyber/smartid/tse/dto/KeyMaterial;->d1Prime:Ljava/lang/String;

    .line 44
    iput-object p2, p0, Lee/cyber/smartid/tse/dto/KeyMaterial;->d1PrimePrime:Ljava/lang/String;

    .line 45
    iput-object p3, p0, Lee/cyber/smartid/tse/dto/KeyMaterial;->n1:Ljava/lang/String;

    .line 46
    iput p4, p0, Lee/cyber/smartid/tse/dto/KeyMaterial;->keySize:I

    return-void
.end method


# virtual methods
.method public getD1Prime()Ljava/lang/String;
    .locals 1

    .line 55
    iget-object v0, p0, Lee/cyber/smartid/tse/dto/KeyMaterial;->d1Prime:Ljava/lang/String;

    return-object v0
.end method

.method public getD1PrimePrime()Ljava/lang/String;
    .locals 1

    .line 64
    iget-object v0, p0, Lee/cyber/smartid/tse/dto/KeyMaterial;->d1PrimePrime:Ljava/lang/String;

    return-object v0
.end method

.method public getKeySize()I
    .locals 1

    .line 82
    iget v0, p0, Lee/cyber/smartid/tse/dto/KeyMaterial;->keySize:I

    return v0
.end method

.method public getN1()Ljava/lang/String;
    .locals 1

    .line 73
    iget-object v0, p0, Lee/cyber/smartid/tse/dto/KeyMaterial;->n1:Ljava/lang/String;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .line 92
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "KeyMaterial{d1Prime=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lee/cyber/smartid/tse/dto/KeyMaterial;->d1Prime:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v2, ", d1PrimePrime=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lee/cyber/smartid/tse/dto/KeyMaterial;->d1PrimePrime:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v2, ", n1=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lee/cyber/smartid/tse/dto/KeyMaterial;->n1:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v1, ", keySize="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lee/cyber/smartid/tse/dto/KeyMaterial;->keySize:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
