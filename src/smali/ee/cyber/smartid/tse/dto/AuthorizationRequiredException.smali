.class public Lee/cyber/smartid/tse/dto/AuthorizationRequiredException;
.super Ljava/io/IOException;
.source "AuthorizationRequiredException.java"


# static fields
.field private static final serialVersionUID:J = 0x61a3d0f8281a28abL


# direct methods
.method public constructor <init>()V
    .locals 1

    const-string v0, "Invalid or missing authorization"

    .line 21
    invoke-direct {p0, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 0

    .line 30
    invoke-direct {p0, p1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    return-void
.end method
