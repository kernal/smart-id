.class public Lee/cyber/smartid/tse/dto/PinValidationRuleIsContainedIn;
.super Lee/cyber/smartid/tse/dto/PinValidationRule;
.source "PinValidationRuleIsContainedIn.java"


# static fields
.field private static final serialVersionUID:J = 0x50ddf9954ecbd13dL


# instance fields
.field private value:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 0

    .line 26
    invoke-direct {p0}, Lee/cyber/smartid/tse/dto/PinValidationRule;-><init>()V

    .line 27
    iput-object p1, p0, Lee/cyber/smartid/tse/dto/PinValidationRuleIsContainedIn;->value:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public isValid(Ljava/lang/String;)Z
    .locals 2

    if-nez p1, :cond_0

    const/4 p1, 0x0

    return p1

    .line 34
    :cond_0
    iget-object v0, p0, Lee/cyber/smartid/tse/dto/PinValidationRuleIsContainedIn;->value:Ljava/lang/String;

    const/4 v1, 0x1

    if-nez v0, :cond_1

    return v1

    .line 37
    :cond_1
    invoke-static {v0, p1}, Lee/cyber/smartid/tse/util/Util;->containsIgnoreCase(Ljava/lang/String;Ljava/lang/String;)Z

    move-result p1

    xor-int/2addr p1, v1

    return p1
.end method
