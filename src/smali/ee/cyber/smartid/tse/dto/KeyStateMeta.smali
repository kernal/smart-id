.class public Lee/cyber/smartid/tse/dto/KeyStateMeta;
.super Ljava/lang/Object;
.source "KeyStateMeta.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final CURRENT_FORMAT_VERSION:I = 0x2

.field public static final LAST_ERROR_TYPE_NETWORK:I = 0x1

.field public static final LAST_ERROR_TYPE_UNKNOWN:I = 0x0

.field private static final serialVersionUID:J = 0x40394dcc8b82f8afL


# instance fields
.field private currentRetry:I

.field private formatVersion:I

.field private final id:Ljava/lang/String;

.field private keyStatus:Lee/cyber/smartid/tse/dto/jsonrpc/AccountKeyStatus;

.field private keyStatusTimestamp:J

.field private lastErrorType:I

.field private final timestamp:J


# direct methods
.method private constructor <init>(Lee/cyber/smartid/tse/inter/WallClock;Ljava/lang/String;)V
    .locals 0

    .line 79
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 80
    iput-object p2, p0, Lee/cyber/smartid/tse/dto/KeyStateMeta;->id:Ljava/lang/String;

    .line 81
    invoke-interface {p1}, Lee/cyber/smartid/tse/inter/WallClock;->currentTimeMillis()J

    move-result-wide p1

    iput-wide p1, p0, Lee/cyber/smartid/tse/dto/KeyStateMeta;->timestamp:J

    return-void
.end method

.method private static createKeyStateMetaId(Lee/cyber/smartid/tse/inter/ResourceAccess;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .line 150
    invoke-interface {p0, p1}, Lee/cyber/smartid/tse/inter/ResourceAccess;->getKeyStateMetaIdByKeyStateId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static forActive(Lee/cyber/smartid/tse/inter/WallClock;Lee/cyber/smartid/tse/inter/ResourceAccess;Ljava/lang/String;)Lee/cyber/smartid/tse/dto/KeyStateMeta;
    .locals 1

    .line 108
    new-instance v0, Lee/cyber/smartid/tse/dto/KeyStateMeta;

    invoke-static {p1, p2}, Lee/cyber/smartid/tse/dto/KeyStateMeta;->createKeyStateMetaId(Lee/cyber/smartid/tse/inter/ResourceAccess;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p0, p1}, Lee/cyber/smartid/tse/dto/KeyStateMeta;-><init>(Lee/cyber/smartid/tse/inter/WallClock;Ljava/lang/String;)V

    const/4 p0, 0x2

    .line 109
    iput p0, v0, Lee/cyber/smartid/tse/dto/KeyStateMeta;->formatVersion:I

    const/4 p0, 0x0

    .line 110
    iput p0, v0, Lee/cyber/smartid/tse/dto/KeyStateMeta;->currentRetry:I

    return-object v0
.end method

.method public static forIdle(Lee/cyber/smartid/tse/inter/WallClock;Lee/cyber/smartid/tse/inter/ResourceAccess;Ljava/lang/String;)Lee/cyber/smartid/tse/dto/KeyStateMeta;
    .locals 1

    .line 93
    new-instance v0, Lee/cyber/smartid/tse/dto/KeyStateMeta;

    invoke-static {p1, p2}, Lee/cyber/smartid/tse/dto/KeyStateMeta;->createKeyStateMetaId(Lee/cyber/smartid/tse/inter/ResourceAccess;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p0, p1}, Lee/cyber/smartid/tse/dto/KeyStateMeta;-><init>(Lee/cyber/smartid/tse/inter/WallClock;Ljava/lang/String;)V

    const/4 p0, 0x2

    .line 94
    iput p0, v0, Lee/cyber/smartid/tse/dto/KeyStateMeta;->formatVersion:I

    const/4 p0, 0x0

    .line 95
    iput p0, v0, Lee/cyber/smartid/tse/dto/KeyStateMeta;->currentRetry:I

    return-object v0
.end method


# virtual methods
.method public getCurrentRetry()I
    .locals 1

    .line 168
    iget v0, p0, Lee/cyber/smartid/tse/dto/KeyStateMeta;->currentRetry:I

    return v0
.end method

.method public getFormatVersion()I
    .locals 1

    .line 139
    iget v0, p0, Lee/cyber/smartid/tse/dto/KeyStateMeta;->formatVersion:I

    return v0
.end method

.method public getId()Ljava/lang/String;
    .locals 1

    .line 121
    iget-object v0, p0, Lee/cyber/smartid/tse/dto/KeyStateMeta;->id:Ljava/lang/String;

    return-object v0
.end method

.method public getKeyLockRemainingDuration(Lee/cyber/smartid/tse/inter/WallClock;)J
    .locals 5

    .line 261
    iget-object v0, p0, Lee/cyber/smartid/tse/dto/KeyStateMeta;->keyStatus:Lee/cyber/smartid/tse/dto/jsonrpc/AccountKeyStatus;

    iget-wide v1, p0, Lee/cyber/smartid/tse/dto/KeyStateMeta;->keyStatusTimestamp:J

    invoke-interface {p1}, Lee/cyber/smartid/tse/inter/WallClock;->currentTimeMillis()J

    move-result-wide v3

    invoke-static {v0, v1, v2, v3, v4}, Lee/cyber/smartid/tse/util/Util;->getKeyLockRemainingDuration(Lee/cyber/smartid/tse/dto/jsonrpc/AccountKeyStatus;JJ)J

    move-result-wide v0

    return-wide v0
.end method

.method public getLastErrorType()I
    .locals 1

    .line 283
    iget v0, p0, Lee/cyber/smartid/tse/dto/KeyStateMeta;->lastErrorType:I

    return v0
.end method

.method public getTimestamp()J
    .locals 2

    .line 130
    iget-wide v0, p0, Lee/cyber/smartid/tse/dto/KeyStateMeta;->timestamp:J

    return-wide v0
.end method

.method public isFirstLongTermRetry()Z
    .locals 2

    .line 187
    invoke-virtual {p0}, Lee/cyber/smartid/tse/dto/KeyStateMeta;->isInLongTermRetryMode()Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lee/cyber/smartid/tse/dto/KeyStateMeta;->currentRetry:I

    invoke-static {}, Lee/cyber/smartid/tse/util/KeyStateRunnable;->getFirstLongTermRetryCount()I

    move-result v1

    if-gt v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isInLongTermRetryMode()Z
    .locals 1

    .line 177
    iget v0, p0, Lee/cyber/smartid/tse/dto/KeyStateMeta;->currentRetry:I

    if-lez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isKeyLocked(Lee/cyber/smartid/tse/inter/WallClock;)Z
    .locals 5

    .line 242
    iget-object v0, p0, Lee/cyber/smartid/tse/dto/KeyStateMeta;->keyStatus:Lee/cyber/smartid/tse/dto/jsonrpc/AccountKeyStatus;

    if-eqz v0, :cond_0

    iget-wide v1, p0, Lee/cyber/smartid/tse/dto/KeyStateMeta;->keyStatusTimestamp:J

    invoke-interface {p1}, Lee/cyber/smartid/tse/inter/WallClock;->currentTimeMillis()J

    move-result-wide v3

    invoke-static {v0, v1, v2, v3, v4}, Lee/cyber/smartid/tse/util/Util;->isKeyLocked(Lee/cyber/smartid/tse/dto/jsonrpc/AccountKeyStatus;JJ)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public isKeyLockedPermanently()Z
    .locals 1

    .line 251
    iget-object v0, p0, Lee/cyber/smartid/tse/dto/KeyStateMeta;->keyStatus:Lee/cyber/smartid/tse/dto/jsonrpc/AccountKeyStatus;

    if-eqz v0, :cond_0

    invoke-static {v0}, Lee/cyber/smartid/tse/util/Util;->isKeyLockedPermanently(Lee/cyber/smartid/tse/dto/jsonrpc/AccountKeyStatus;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public setCurrentRetry(I)V
    .locals 0

    .line 159
    iput p1, p0, Lee/cyber/smartid/tse/dto/KeyStateMeta;->currentRetry:I

    return-void
.end method

.method public setKeyStatus(Lee/cyber/smartid/tse/dto/jsonrpc/AccountKeyStatus;J)V
    .locals 0

    .line 231
    iput-wide p2, p0, Lee/cyber/smartid/tse/dto/KeyStateMeta;->keyStatusTimestamp:J

    .line 232
    iput-object p1, p0, Lee/cyber/smartid/tse/dto/KeyStateMeta;->keyStatus:Lee/cyber/smartid/tse/dto/jsonrpc/AccountKeyStatus;

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .line 293
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "KeyStateMeta{id=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lee/cyber/smartid/tse/dto/KeyStateMeta;->id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v1, ", timestamp="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lee/cyber/smartid/tse/dto/KeyStateMeta;->timestamp:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ", currentRetry="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lee/cyber/smartid/tse/dto/KeyStateMeta;->currentRetry:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", keyStatusTimestamp="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lee/cyber/smartid/tse/dto/KeyStateMeta;->keyStatusTimestamp:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ", keyStatus="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lee/cyber/smartid/tse/dto/KeyStateMeta;->keyStatus:Lee/cyber/smartid/tse/dto/jsonrpc/AccountKeyStatus;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", lastErrorType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lee/cyber/smartid/tse/dto/KeyStateMeta;->lastErrorType:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", formatVersion="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lee/cyber/smartid/tse/dto/KeyStateMeta;->formatVersion:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public updateKeyStatusFrom(Lee/cyber/smartid/tse/dto/KeyStateMeta;)V
    .locals 3

    if-eqz p1, :cond_1

    .line 217
    iget-object v0, p1, Lee/cyber/smartid/tse/dto/KeyStateMeta;->keyStatus:Lee/cyber/smartid/tse/dto/jsonrpc/AccountKeyStatus;

    if-nez v0, :cond_0

    goto :goto_0

    .line 220
    :cond_0
    iget-wide v1, p1, Lee/cyber/smartid/tse/dto/KeyStateMeta;->keyStatusTimestamp:J

    invoke-virtual {p0, v0, v1, v2}, Lee/cyber/smartid/tse/dto/KeyStateMeta;->setKeyStatus(Lee/cyber/smartid/tse/dto/jsonrpc/AccountKeyStatus;J)V

    :cond_1
    :goto_0
    return-void
.end method

.method public updateKeyStatusFrom(Lee/cyber/smartid/tse/inter/WallClock;Ljava/lang/String;Lee/cyber/smartid/tse/dto/TSEError;Z)V
    .locals 7

    if-nez p3, :cond_0

    return-void

    :cond_0
    if-eqz p4, :cond_1

    .line 205
    new-instance p3, Lee/cyber/smartid/tse/dto/jsonrpc/AccountKeyStatus;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const-string v2, "REVOKED"

    move-object v0, p3

    move-object v1, p2

    invoke-direct/range {v0 .. v6}, Lee/cyber/smartid/tse/dto/jsonrpc/AccountKeyStatus;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/tse/dto/jsonrpc/AccountKeyLockInfo;Lee/cyber/smartid/tse/dto/jsonrpc/AccountKeyCertificate;)V

    invoke-interface {p1}, Lee/cyber/smartid/tse/inter/WallClock;->currentTimeMillis()J

    move-result-wide p1

    invoke-virtual {p0, p3, p1, p2}, Lee/cyber/smartid/tse/dto/KeyStateMeta;->setKeyStatus(Lee/cyber/smartid/tse/dto/jsonrpc/AccountKeyStatus;J)V

    goto :goto_0

    .line 206
    :cond_1
    invoke-virtual {p3}, Lee/cyber/smartid/tse/dto/TSEError;->getKeyStatus()Lee/cyber/smartid/tse/dto/jsonrpc/AccountKeyStatus;

    move-result-object p1

    if-eqz p1, :cond_2

    .line 207
    invoke-virtual {p3}, Lee/cyber/smartid/tse/dto/TSEError;->getKeyStatus()Lee/cyber/smartid/tse/dto/jsonrpc/AccountKeyStatus;

    move-result-object p1

    invoke-virtual {p3}, Lee/cyber/smartid/tse/dto/TSEError;->getCreatedAtTimestamp()J

    move-result-wide p2

    invoke-virtual {p0, p1, p2, p3}, Lee/cyber/smartid/tse/dto/KeyStateMeta;->setKeyStatus(Lee/cyber/smartid/tse/dto/jsonrpc/AccountKeyStatus;J)V

    :cond_2
    :goto_0
    return-void
.end method

.method public updateLastErrorTypeForm(Lee/cyber/smartid/tse/dto/TSEError;)V
    .locals 4

    if-eqz p1, :cond_0

    .line 270
    invoke-virtual {p1}, Lee/cyber/smartid/tse/dto/TSEError;->getCode()J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    cmp-long p1, v0, v2

    if-nez p1, :cond_0

    const/4 p1, 0x1

    .line 271
    iput p1, p0, Lee/cyber/smartid/tse/dto/KeyStateMeta;->lastErrorType:I

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    .line 273
    iput p1, p0, Lee/cyber/smartid/tse/dto/KeyStateMeta;->lastErrorType:I

    :goto_0
    return-void
.end method
