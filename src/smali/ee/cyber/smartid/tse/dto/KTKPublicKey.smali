.class public Lee/cyber/smartid/tse/dto/KTKPublicKey;
.super Ljava/lang/Object;
.source "KTKPublicKey.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = -0x1f82e4d4e9e2a562L


# instance fields
.field private final keyId:Ljava/lang/String;

.field private final publicKey:Ljava/lang/String;

.field private final szId:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iput-object p1, p0, Lee/cyber/smartid/tse/dto/KTKPublicKey;->szId:Ljava/lang/String;

    .line 34
    iput-object p2, p0, Lee/cyber/smartid/tse/dto/KTKPublicKey;->keyId:Ljava/lang/String;

    .line 35
    iput-object p3, p0, Lee/cyber/smartid/tse/dto/KTKPublicKey;->publicKey:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public getKeyId()Ljava/lang/String;
    .locals 1

    .line 55
    iget-object v0, p0, Lee/cyber/smartid/tse/dto/KTKPublicKey;->keyId:Ljava/lang/String;

    return-object v0
.end method

.method public getPublicKey()Ljava/lang/String;
    .locals 1

    .line 65
    iget-object v0, p0, Lee/cyber/smartid/tse/dto/KTKPublicKey;->publicKey:Ljava/lang/String;

    return-object v0
.end method

.method public getSzId()Ljava/lang/String;
    .locals 1

    .line 45
    iget-object v0, p0, Lee/cyber/smartid/tse/dto/KTKPublicKey;->szId:Ljava/lang/String;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .line 70
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "KTKPublicKey{szId=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lee/cyber/smartid/tse/dto/KTKPublicKey;->szId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v2, ", keyId=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lee/cyber/smartid/tse/dto/KTKPublicKey;->keyId:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v2, ", publicKey=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lee/cyber/smartid/tse/dto/KTKPublicKey;->publicKey:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
