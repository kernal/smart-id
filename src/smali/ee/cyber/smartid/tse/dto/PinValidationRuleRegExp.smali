.class public Lee/cyber/smartid/tse/dto/PinValidationRuleRegExp;
.super Lee/cyber/smartid/tse/dto/PinValidationRule;
.source "PinValidationRuleRegExp.java"


# static fields
.field private static final serialVersionUID:J = 0x12556fdc6510def0L


# instance fields
.field private final pattern:Ljava/util/regex/Pattern;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 0

    .line 26
    invoke-direct {p0}, Lee/cyber/smartid/tse/dto/PinValidationRule;-><init>()V

    .line 27
    invoke-static {p1}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object p1

    iput-object p1, p0, Lee/cyber/smartid/tse/dto/PinValidationRuleRegExp;->pattern:Ljava/util/regex/Pattern;

    return-void
.end method


# virtual methods
.method public isValid(Ljava/lang/String;)Z
    .locals 2

    if-nez p1, :cond_0

    const/4 p1, 0x0

    return p1

    .line 36
    :cond_0
    :try_start_0
    iget-object v0, p0, Lee/cyber/smartid/tse/dto/PinValidationRuleRegExp;->pattern:Ljava/util/regex/Pattern;

    invoke-virtual {v0, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object p1

    .line 37
    invoke-virtual {p1}, Ljava/util/regex/Matcher;->find()Z

    move-result p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    xor-int/lit8 p1, p1, 0x1

    return p1

    :catch_0
    move-exception p1

    .line 39
    invoke-static {p0}, Lee/cyber/smartid/tse/util/Log;->getInstance(Ljava/lang/Object;)Lee/cyber/smartid/tse/util/Log;

    move-result-object v0

    const-string v1, "isValid"

    invoke-virtual {v0, v1, p1}, Lee/cyber/smartid/tse/util/Log;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 40
    throw p1
.end method
