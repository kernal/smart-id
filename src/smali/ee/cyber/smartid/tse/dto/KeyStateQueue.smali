.class public Lee/cyber/smartid/tse/dto/KeyStateQueue;
.super Ljava/lang/Object;
.source "KeyStateQueue.java"


# instance fields
.field private final extras:Landroid/os/Bundle;

.field private final listener:Lee/cyber/smartid/tse/inter/TSEListener;

.field private final state:Lee/cyber/smartid/tse/dto/ProtoKeyState;

.field private final tag:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lee/cyber/smartid/tse/dto/ProtoKeyState;Ljava/lang/String;Landroid/os/Bundle;Lee/cyber/smartid/tse/inter/TSEListener;)V
    .locals 0

    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iput-object p1, p0, Lee/cyber/smartid/tse/dto/KeyStateQueue;->state:Lee/cyber/smartid/tse/dto/ProtoKeyState;

    .line 34
    iput-object p2, p0, Lee/cyber/smartid/tse/dto/KeyStateQueue;->tag:Ljava/lang/String;

    .line 35
    iput-object p3, p0, Lee/cyber/smartid/tse/dto/KeyStateQueue;->extras:Landroid/os/Bundle;

    .line 36
    iput-object p4, p0, Lee/cyber/smartid/tse/dto/KeyStateQueue;->listener:Lee/cyber/smartid/tse/inter/TSEListener;

    return-void
.end method


# virtual methods
.method public getExtras()Landroid/os/Bundle;
    .locals 1

    .line 73
    iget-object v0, p0, Lee/cyber/smartid/tse/dto/KeyStateQueue;->extras:Landroid/os/Bundle;

    return-object v0
.end method

.method public getListener()Lee/cyber/smartid/tse/inter/TSEListener;
    .locals 1

    .line 54
    iget-object v0, p0, Lee/cyber/smartid/tse/dto/KeyStateQueue;->listener:Lee/cyber/smartid/tse/inter/TSEListener;

    return-object v0
.end method

.method public getState()Lee/cyber/smartid/tse/dto/ProtoKeyState;
    .locals 1

    .line 45
    iget-object v0, p0, Lee/cyber/smartid/tse/dto/KeyStateQueue;->state:Lee/cyber/smartid/tse/dto/ProtoKeyState;

    return-object v0
.end method

.method public getTag()Ljava/lang/String;
    .locals 1

    .line 63
    iget-object v0, p0, Lee/cyber/smartid/tse/dto/KeyStateQueue;->tag:Ljava/lang/String;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 83
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "KeyStateQueue{state="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lee/cyber/smartid/tse/dto/KeyStateQueue;->state:Lee/cyber/smartid/tse/dto/ProtoKeyState;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", tag=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lee/cyber/smartid/tse/dto/KeyStateQueue;->tag:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v1, ", listener="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lee/cyber/smartid/tse/dto/KeyStateQueue;->listener:Lee/cyber/smartid/tse/inter/TSEListener;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", extras="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lee/cyber/smartid/tse/dto/KeyStateQueue;->extras:Landroid/os/Bundle;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
