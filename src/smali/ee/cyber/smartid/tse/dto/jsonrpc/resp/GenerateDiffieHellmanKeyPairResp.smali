.class public Lee/cyber/smartid/tse/dto/jsonrpc/resp/GenerateDiffieHellmanKeyPairResp;
.super Lee/cyber/smartid/tse/dto/jsonrpc/resp/BaseResp;
.source "GenerateDiffieHellmanKeyPairResp.java"


# static fields
.field private static final serialVersionUID:J = -0x4605949ab3951a15L


# instance fields
.field private final diffieHellmanPublicKeyEncoded:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 26
    invoke-direct {p0, p1}, Lee/cyber/smartid/tse/dto/jsonrpc/resp/BaseResp;-><init>(Ljava/lang/String;)V

    .line 27
    iput-object p2, p0, Lee/cyber/smartid/tse/dto/jsonrpc/resp/GenerateDiffieHellmanKeyPairResp;->diffieHellmanPublicKeyEncoded:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public getDiffieHellmanPublicKeyEncoded()Ljava/lang/String;
    .locals 1

    .line 37
    iget-object v0, p0, Lee/cyber/smartid/tse/dto/jsonrpc/resp/GenerateDiffieHellmanKeyPairResp;->diffieHellmanPublicKeyEncoded:Ljava/lang/String;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 42
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "GenerateDiffieHellmanKeyPairResp{diffieHellmanPublicKey=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lee/cyber/smartid/tse/dto/jsonrpc/resp/GenerateDiffieHellmanKeyPairResp;->diffieHellmanPublicKeyEncoded:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v1, "} "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 44
    invoke-super {p0}, Lee/cyber/smartid/tse/dto/jsonrpc/resp/BaseResp;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
