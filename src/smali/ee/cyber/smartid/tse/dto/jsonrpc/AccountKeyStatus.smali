.class public Lee/cyber/smartid/tse/dto/jsonrpc/AccountKeyStatus;
.super Ljava/lang/Object;
.source "AccountKeyStatus.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field public static final ACCOUNT_KEY_STATUS_EXPIRED:Ljava/lang/String; = "EXPIRED"

.field public static final ACCOUNT_KEY_STATUS_IN_PREPARATION:Ljava/lang/String; = "IN_PREPARATION"

.field public static final ACCOUNT_KEY_STATUS_LOCKED:Ljava/lang/String; = "LOCKED"

.field public static final ACCOUNT_KEY_STATUS_OK:Ljava/lang/String; = "OK"

.field public static final ACCOUNT_KEY_STATUS_REVOKED:Ljava/lang/String; = "REVOKED"

.field public static final ACCOUNT_KEY_TYPE_AUTHENTICATION:Ljava/lang/String; = "AUTHENTICATION"

.field public static final ACCOUNT_KEY_TYPE_SIGNATURE:Ljava/lang/String; = "SIGNATURE"

.field private static final serialVersionUID:J = 0x1f6b531b08a566dL


# instance fields
.field private certificate:Lee/cyber/smartid/tse/dto/jsonrpc/AccountKeyCertificate;

.field private documentCreationTime:Ljava/lang/String;

.field private documentNumber:Ljava/lang/String;

.field private keyType:Ljava/lang/String;

.field private lockInfo:Lee/cyber/smartid/tse/dto/jsonrpc/AccountKeyLockInfo;

.field private status:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 88
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 89
    iput-object p1, p0, Lee/cyber/smartid/tse/dto/jsonrpc/AccountKeyStatus;->keyType:Ljava/lang/String;

    .line 90
    iput-object p2, p0, Lee/cyber/smartid/tse/dto/jsonrpc/AccountKeyStatus;->status:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/tse/dto/jsonrpc/AccountKeyLockInfo;Lee/cyber/smartid/tse/dto/jsonrpc/AccountKeyCertificate;)V
    .locals 0

    .line 104
    invoke-direct {p0, p1, p2}, Lee/cyber/smartid/tse/dto/jsonrpc/AccountKeyStatus;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 105
    iput-object p3, p0, Lee/cyber/smartid/tse/dto/jsonrpc/AccountKeyStatus;->documentNumber:Ljava/lang/String;

    .line 106
    iput-object p4, p0, Lee/cyber/smartid/tse/dto/jsonrpc/AccountKeyStatus;->documentCreationTime:Ljava/lang/String;

    .line 107
    iput-object p5, p0, Lee/cyber/smartid/tse/dto/jsonrpc/AccountKeyStatus;->lockInfo:Lee/cyber/smartid/tse/dto/jsonrpc/AccountKeyLockInfo;

    .line 108
    iput-object p6, p0, Lee/cyber/smartid/tse/dto/jsonrpc/AccountKeyStatus;->certificate:Lee/cyber/smartid/tse/dto/jsonrpc/AccountKeyCertificate;

    return-void
.end method


# virtual methods
.method public getCertificate()Lee/cyber/smartid/tse/dto/jsonrpc/AccountKeyCertificate;
    .locals 1

    .line 151
    iget-object v0, p0, Lee/cyber/smartid/tse/dto/jsonrpc/AccountKeyStatus;->certificate:Lee/cyber/smartid/tse/dto/jsonrpc/AccountKeyCertificate;

    return-object v0
.end method

.method public getDocumentCreationTime()Ljava/lang/String;
    .locals 1

    .line 171
    iget-object v0, p0, Lee/cyber/smartid/tse/dto/jsonrpc/AccountKeyStatus;->documentCreationTime:Ljava/lang/String;

    return-object v0
.end method

.method public getDocumentNumber()Ljava/lang/String;
    .locals 1

    .line 161
    iget-object v0, p0, Lee/cyber/smartid/tse/dto/jsonrpc/AccountKeyStatus;->documentNumber:Ljava/lang/String;

    return-object v0
.end method

.method public getKeyType()Ljava/lang/String;
    .locals 1

    .line 119
    iget-object v0, p0, Lee/cyber/smartid/tse/dto/jsonrpc/AccountKeyStatus;->keyType:Ljava/lang/String;

    return-object v0
.end method

.method public getLockInfo()Lee/cyber/smartid/tse/dto/jsonrpc/AccountKeyLockInfo;
    .locals 1

    .line 140
    iget-object v0, p0, Lee/cyber/smartid/tse/dto/jsonrpc/AccountKeyStatus;->lockInfo:Lee/cyber/smartid/tse/dto/jsonrpc/AccountKeyLockInfo;

    return-object v0
.end method

.method public getStatus()Ljava/lang/String;
    .locals 1

    .line 130
    iget-object v0, p0, Lee/cyber/smartid/tse/dto/jsonrpc/AccountKeyStatus;->status:Ljava/lang/String;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .line 181
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "AccountKeyStatus{keyType=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lee/cyber/smartid/tse/dto/jsonrpc/AccountKeyStatus;->keyType:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v2, ", status=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lee/cyber/smartid/tse/dto/jsonrpc/AccountKeyStatus;->status:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v2, ", documentNumber=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lee/cyber/smartid/tse/dto/jsonrpc/AccountKeyStatus;->documentNumber:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v2, ", documentCreationTime=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lee/cyber/smartid/tse/dto/jsonrpc/AccountKeyStatus;->documentCreationTime:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v1, ", lockInfo="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lee/cyber/smartid/tse/dto/jsonrpc/AccountKeyStatus;->lockInfo:Lee/cyber/smartid/tse/dto/jsonrpc/AccountKeyLockInfo;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", certificate="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lee/cyber/smartid/tse/dto/jsonrpc/AccountKeyStatus;->certificate:Lee/cyber/smartid/tse/dto/jsonrpc/AccountKeyCertificate;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
