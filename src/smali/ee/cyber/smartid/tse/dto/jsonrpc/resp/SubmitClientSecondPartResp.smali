.class public Lee/cyber/smartid/tse/dto/jsonrpc/resp/SubmitClientSecondPartResp;
.super Lee/cyber/smartid/tse/dto/jsonrpc/resp/BaseResp;
.source "SubmitClientSecondPartResp.java"


# static fields
.field private static final serialVersionUID:J = 0x400a1bf19c13af1eL


# instance fields
.field private final csrTransactionUUID:Ljava/lang/String;

.field private final keyId:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 30
    invoke-direct {p0, p1}, Lee/cyber/smartid/tse/dto/jsonrpc/resp/BaseResp;-><init>(Ljava/lang/String;)V

    .line 31
    iput-object p2, p0, Lee/cyber/smartid/tse/dto/jsonrpc/resp/SubmitClientSecondPartResp;->keyId:Ljava/lang/String;

    .line 32
    iput-object p3, p0, Lee/cyber/smartid/tse/dto/jsonrpc/resp/SubmitClientSecondPartResp;->csrTransactionUUID:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public getCsrTransactionUUID()Ljava/lang/String;
    .locals 1

    .line 52
    iget-object v0, p0, Lee/cyber/smartid/tse/dto/jsonrpc/resp/SubmitClientSecondPartResp;->csrTransactionUUID:Ljava/lang/String;

    return-object v0
.end method

.method public getKeyId()Ljava/lang/String;
    .locals 1

    .line 42
    iget-object v0, p0, Lee/cyber/smartid/tse/dto/jsonrpc/resp/SubmitClientSecondPartResp;->keyId:Ljava/lang/String;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .line 57
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "SubmitClientSecondPartResp{keyId=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lee/cyber/smartid/tse/dto/jsonrpc/resp/SubmitClientSecondPartResp;->keyId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v2, ", csrTransactionUUID=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lee/cyber/smartid/tse/dto/jsonrpc/resp/SubmitClientSecondPartResp;->csrTransactionUUID:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v1, "} "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 60
    invoke-super {p0}, Lee/cyber/smartid/tse/dto/jsonrpc/resp/BaseResp;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
