.class public Lee/cyber/smartid/tse/dto/jsonrpc/param/SubmitSignatureParams;
.super Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCParams;
.source "SubmitSignatureParams.java"


# static fields
.field private static final serialVersionUID:J = 0xe5107cd0f9a8c14L


# instance fields
.field private final accountUUID:Ljava/lang/String;

.field private final freshnessToken:Ljava/lang/String;

.field private final requestData:Ljava/lang/String;

.field private final requestDataEncoding:Ljava/lang/String;

.field private final transactionUUID:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 39
    invoke-direct {p0}, Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCParams;-><init>()V

    .line 40
    iput-object p1, p0, Lee/cyber/smartid/tse/dto/jsonrpc/param/SubmitSignatureParams;->accountUUID:Ljava/lang/String;

    .line 41
    iput-object p2, p0, Lee/cyber/smartid/tse/dto/jsonrpc/param/SubmitSignatureParams;->requestData:Ljava/lang/String;

    .line 42
    iput-object p3, p0, Lee/cyber/smartid/tse/dto/jsonrpc/param/SubmitSignatureParams;->requestDataEncoding:Ljava/lang/String;

    .line 43
    iput-object p4, p0, Lee/cyber/smartid/tse/dto/jsonrpc/param/SubmitSignatureParams;->freshnessToken:Ljava/lang/String;

    .line 44
    iput-object p5, p0, Lee/cyber/smartid/tse/dto/jsonrpc/param/SubmitSignatureParams;->transactionUUID:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public getTransactionUUID()Ljava/lang/String;
    .locals 1

    .line 54
    iget-object v0, p0, Lee/cyber/smartid/tse/dto/jsonrpc/param/SubmitSignatureParams;->transactionUUID:Ljava/lang/String;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .line 64
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "SubmitSignatureParams{accountUUID=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lee/cyber/smartid/tse/dto/jsonrpc/param/SubmitSignatureParams;->accountUUID:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v2, ", requestData=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lee/cyber/smartid/tse/dto/jsonrpc/param/SubmitSignatureParams;->requestData:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v2, ", requestDataEncoding=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lee/cyber/smartid/tse/dto/jsonrpc/param/SubmitSignatureParams;->requestDataEncoding:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v2, ", freshnessToken=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lee/cyber/smartid/tse/dto/jsonrpc/param/SubmitSignatureParams;->freshnessToken:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v2, ", transactionUUID=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lee/cyber/smartid/tse/dto/jsonrpc/param/SubmitSignatureParams;->transactionUUID:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v1, "} "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 70
    invoke-super {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
