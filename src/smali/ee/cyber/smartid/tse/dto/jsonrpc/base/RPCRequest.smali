.class public Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCRequest;
.super Ljava/lang/Object;
.source "RPCRequest.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = 0x4fad094f0adc59dL


# instance fields
.field private id:Ljava/lang/String;

.field private jsonrpc:Ljava/lang/String;

.field private method:Ljava/lang/String;

.field private params:Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCParams;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCParams;)V
    .locals 3

    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "2.0"

    .line 21
    iput-object v0, p0, Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCRequest;->jsonrpc:Ljava/lang/String;

    .line 34
    invoke-static {}, Lee/cyber/smartid/tse/util/Util;->generateUUID()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    const/16 v2, 0x8

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCRequest;->id:Ljava/lang/String;

    .line 35
    iput-object p1, p0, Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCRequest;->method:Ljava/lang/String;

    .line 36
    iput-object p2, p0, Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCRequest;->params:Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCParams;

    return-void
.end method


# virtual methods
.method public getId()Ljava/lang/String;
    .locals 1

    .line 45
    iget-object v0, p0, Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCRequest;->id:Ljava/lang/String;

    return-object v0
.end method

.method public getMethod()Ljava/lang/String;
    .locals 1

    .line 64
    iget-object v0, p0, Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCRequest;->method:Ljava/lang/String;

    return-object v0
.end method

.method public getParams()Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCParams;
    .locals 1

    .line 73
    iget-object v0, p0, Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCRequest;->params:Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCParams;

    return-object v0
.end method

.method public setId(Ljava/lang/String;)V
    .locals 0

    .line 55
    iput-object p1, p0, Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCRequest;->id:Ljava/lang/String;

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .line 83
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "RPCRequest{jsonrpc=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCRequest;->jsonrpc:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v2, ", id="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCRequest;->id:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, ", method=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCRequest;->method:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v1, ", params="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCRequest;->params:Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCParams;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
