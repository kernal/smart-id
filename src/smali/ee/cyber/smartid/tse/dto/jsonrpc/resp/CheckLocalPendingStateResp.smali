.class public Lee/cyber/smartid/tse/dto/jsonrpc/resp/CheckLocalPendingStateResp;
.super Lee/cyber/smartid/tse/dto/jsonrpc/resp/BaseResp;
.source "CheckLocalPendingStateResp.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lee/cyber/smartid/tse/dto/jsonrpc/resp/CheckLocalPendingStateResp$CheckLocalPendingStateType;
    }
.end annotation


# static fields
.field public static final LOCAL_PENDING_STATE_TYPE_CONFIRM_TRANSACTION:Ljava/lang/String; = "confirmTransaction"

.field public static final LOCAL_PENDING_STATE_TYPE_NONE:Ljava/lang/String; = "none"

.field public static final LOCAL_PENDING_STATE_TYPE_SUBMIT_CLIENT_SECOND_PART:Ljava/lang/String; = "submitClientSecondPart"

.field private static final serialVersionUID:J = 0x589c04dd1581b89aL


# instance fields
.field private final keyId:Ljava/lang/String;

.field private final localPendingStateType:Ljava/lang/String;

.field private final transactionUUID:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 75
    invoke-direct {p0, p1}, Lee/cyber/smartid/tse/dto/jsonrpc/resp/BaseResp;-><init>(Ljava/lang/String;)V

    .line 76
    iput-object p2, p0, Lee/cyber/smartid/tse/dto/jsonrpc/resp/CheckLocalPendingStateResp;->keyId:Ljava/lang/String;

    .line 77
    iput-object p4, p0, Lee/cyber/smartid/tse/dto/jsonrpc/resp/CheckLocalPendingStateResp;->transactionUUID:Ljava/lang/String;

    .line 78
    iput-object p3, p0, Lee/cyber/smartid/tse/dto/jsonrpc/resp/CheckLocalPendingStateResp;->localPendingStateType:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public getKeyId()Ljava/lang/String;
    .locals 1

    .line 89
    iget-object v0, p0, Lee/cyber/smartid/tse/dto/jsonrpc/resp/CheckLocalPendingStateResp;->keyId:Ljava/lang/String;

    return-object v0
.end method

.method public getLocalPendingStateType()Ljava/lang/String;
    .locals 1

    .line 101
    iget-object v0, p0, Lee/cyber/smartid/tse/dto/jsonrpc/resp/CheckLocalPendingStateResp;->localPendingStateType:Ljava/lang/String;

    return-object v0
.end method

.method public getTransactionUUID()Ljava/lang/String;
    .locals 1

    .line 120
    iget-object v0, p0, Lee/cyber/smartid/tse/dto/jsonrpc/resp/CheckLocalPendingStateResp;->transactionUUID:Ljava/lang/String;

    return-object v0
.end method

.method public hasLocalPendingState()Z
    .locals 2

    .line 110
    iget-object v0, p0, Lee/cyber/smartid/tse/dto/jsonrpc/resp/CheckLocalPendingStateResp;->keyId:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lee/cyber/smartid/tse/dto/jsonrpc/resp/CheckLocalPendingStateResp;->localPendingStateType:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lee/cyber/smartid/tse/dto/jsonrpc/resp/CheckLocalPendingStateResp;->localPendingStateType:Ljava/lang/String;

    const-string v1, "none"

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .line 130
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "CheckLocalPendingStateResp{, localPendingStateType=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lee/cyber/smartid/tse/dto/jsonrpc/resp/CheckLocalPendingStateResp;->localPendingStateType:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v2, ", keyId=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lee/cyber/smartid/tse/dto/jsonrpc/resp/CheckLocalPendingStateResp;->keyId:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v2, ", transactionUUID=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lee/cyber/smartid/tse/dto/jsonrpc/resp/CheckLocalPendingStateResp;->transactionUUID:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v1, "} "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 134
    invoke-super {p0}, Lee/cyber/smartid/tse/dto/jsonrpc/resp/BaseResp;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
