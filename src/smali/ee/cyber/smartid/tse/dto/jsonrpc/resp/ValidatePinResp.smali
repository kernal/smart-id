.class public Lee/cyber/smartid/tse/dto/jsonrpc/resp/ValidatePinResp;
.super Lee/cyber/smartid/tse/dto/jsonrpc/resp/BaseResp;
.source "ValidatePinResp.java"


# static fields
.field private static final serialVersionUID:J = -0x72dc02077f1dee52L


# instance fields
.field private final errors:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lee/cyber/smartid/tse/dto/PinValidationError;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList<",
            "Lee/cyber/smartid/tse/dto/PinValidationError;",
            ">;)V"
        }
    .end annotation

    .line 30
    invoke-direct {p0, p1}, Lee/cyber/smartid/tse/dto/jsonrpc/resp/BaseResp;-><init>(Ljava/lang/String;)V

    .line 31
    iput-object p2, p0, Lee/cyber/smartid/tse/dto/jsonrpc/resp/ValidatePinResp;->errors:Ljava/util/ArrayList;

    return-void
.end method


# virtual methods
.method public getErrors()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Lee/cyber/smartid/tse/dto/PinValidationError;",
            ">;"
        }
    .end annotation

    .line 42
    iget-object v0, p0, Lee/cyber/smartid/tse/dto/jsonrpc/resp/ValidatePinResp;->errors:Ljava/util/ArrayList;

    return-object v0
.end method

.method public isValid()Z
    .locals 1

    .line 51
    iget-object v0, p0, Lee/cyber/smartid/tse/dto/jsonrpc/resp/ValidatePinResp;->errors:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-gtz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 61
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ValidatePinResp{errors="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lee/cyber/smartid/tse/dto/jsonrpc/resp/ValidatePinResp;->errors:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, "} "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 63
    invoke-super {p0}, Lee/cyber/smartid/tse/dto/jsonrpc/resp/BaseResp;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
