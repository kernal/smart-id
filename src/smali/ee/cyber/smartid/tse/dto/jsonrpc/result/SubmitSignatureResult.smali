.class public Lee/cyber/smartid/tse/dto/jsonrpc/result/SubmitSignatureResult;
.super Lee/cyber/smartid/tse/dto/jsonrpc/result/RefreshCloneDetectionResult;
.source "SubmitSignatureResult.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lee/cyber/smartid/tse/dto/jsonrpc/result/SubmitSignatureResult$SubmitSignatureResultCode;
    }
.end annotation


# static fields
.field public static final SUBMIT_SIGNATURE_RESULT_CODE_OK:Ljava/lang/String; = "OK"

.field public static final SUBMIT_SIGNATURE_RESULT_CODE_TIME_LOCKED:Ljava/lang/String; = "TIMELOCKED"

.field public static final SUBMIT_SIGNATURE_RESULT_CODE_WRONG_PIN:Ljava/lang/String; = "WRONG_PIN"

.field private static final serialVersionUID:J = 0x59542a57d2ce974cL


# instance fields
.field private keyInfo:Lee/cyber/smartid/tse/dto/jsonrpc/AccountKeyStatus;

.field private result:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 65
    invoke-direct {p0}, Lee/cyber/smartid/tse/dto/jsonrpc/result/RefreshCloneDetectionResult;-><init>()V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/tse/dto/jsonrpc/AccountKeyStatus;)V
    .locals 0

    .line 77
    invoke-direct {p0, p1, p2}, Lee/cyber/smartid/tse/dto/jsonrpc/result/RefreshCloneDetectionResult;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 78
    iput-object p3, p0, Lee/cyber/smartid/tse/dto/jsonrpc/result/SubmitSignatureResult;->result:Ljava/lang/String;

    .line 79
    iput-object p4, p0, Lee/cyber/smartid/tse/dto/jsonrpc/result/SubmitSignatureResult;->keyInfo:Lee/cyber/smartid/tse/dto/jsonrpc/AccountKeyStatus;

    return-void
.end method


# virtual methods
.method public getErrorCodeForResult()J
    .locals 5

    .line 119
    iget-object v0, p0, Lee/cyber/smartid/tse/dto/jsonrpc/result/SubmitSignatureResult;->result:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v1

    const v2, -0x7a62b43d

    const/4 v3, 0x2

    const/4 v4, 0x1

    if-eq v1, v2, :cond_2

    const/16 v2, 0x9dc

    if-eq v1, v2, :cond_1

    const v2, 0x768e6417

    if-eq v1, v2, :cond_0

    goto :goto_0

    :cond_0
    const-string v1, "TIMELOCKED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x2

    goto :goto_1

    :cond_1
    const-string v1, "OK"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x0

    goto :goto_1

    :cond_2
    const-string v1, "WRONG_PIN"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x1

    goto :goto_1

    :cond_3
    :goto_0
    const/4 v0, -0x1

    :goto_1
    const-wide/16 v1, 0x0

    if-eqz v0, :cond_6

    if-eq v0, v4, :cond_5

    if-eq v0, v3, :cond_4

    return-wide v1

    :cond_4
    const-wide/16 v0, -0x792b

    return-wide v0

    :cond_5
    const-wide/16 v0, -0x7920

    return-wide v0

    :cond_6
    return-wide v1
.end method

.method public getErrorMessageForResult(Landroid/content/Context;)Ljava/lang/String;
    .locals 5

    .line 139
    iget-object v0, p0, Lee/cyber/smartid/tse/dto/jsonrpc/result/SubmitSignatureResult;->result:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v1

    const v2, -0x7a62b43d

    const/4 v3, 0x2

    const/4 v4, 0x1

    if-eq v1, v2, :cond_2

    const/16 v2, 0x9dc

    if-eq v1, v2, :cond_1

    const v2, 0x768e6417

    if-eq v1, v2, :cond_0

    goto :goto_0

    :cond_0
    const-string v1, "TIMELOCKED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x2

    goto :goto_1

    :cond_1
    const-string v1, "OK"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x0

    goto :goto_1

    :cond_2
    const-string v1, "WRONG_PIN"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x1

    goto :goto_1

    :cond_3
    :goto_0
    const/4 v0, -0x1

    :goto_1
    if-eqz v0, :cond_6

    if-eq v0, v4, :cond_5

    if-eq v0, v3, :cond_4

    .line 147
    sget v0, Lee/cyber/smartid/tse/R$string;->err_unknown:I

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 145
    :cond_4
    sget v0, Lee/cyber/smartid/tse/R$string;->err_key_unusable:I

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 143
    :cond_5
    sget v0, Lee/cyber/smartid/tse/R$string;->err_wrong_pin:I

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 141
    :cond_6
    sget v0, Lee/cyber/smartid/tse/R$string;->err_unknown:I

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public getKeyInfo()Lee/cyber/smartid/tse/dto/jsonrpc/AccountKeyStatus;
    .locals 1

    .line 109
    iget-object v0, p0, Lee/cyber/smartid/tse/dto/jsonrpc/result/SubmitSignatureResult;->keyInfo:Lee/cyber/smartid/tse/dto/jsonrpc/AccountKeyStatus;

    return-object v0
.end method

.method public getResult()Ljava/lang/String;
    .locals 1

    .line 90
    iget-object v0, p0, Lee/cyber/smartid/tse/dto/jsonrpc/result/SubmitSignatureResult;->result:Ljava/lang/String;

    return-object v0
.end method

.method public isSuccessfullySigned()Z
    .locals 2

    .line 99
    iget-object v0, p0, Lee/cyber/smartid/tse/dto/jsonrpc/result/SubmitSignatureResult;->result:Ljava/lang/String;

    const-string v1, "OK"

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 153
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "SubmitSignatureResult{result=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lee/cyber/smartid/tse/dto/jsonrpc/result/SubmitSignatureResult;->result:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v1, ", keyInfo="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lee/cyber/smartid/tse/dto/jsonrpc/result/SubmitSignatureResult;->keyInfo:Lee/cyber/smartid/tse/dto/jsonrpc/AccountKeyStatus;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, "} "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 156
    invoke-super {p0}, Lee/cyber/smartid/tse/dto/jsonrpc/result/RefreshCloneDetectionResult;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
