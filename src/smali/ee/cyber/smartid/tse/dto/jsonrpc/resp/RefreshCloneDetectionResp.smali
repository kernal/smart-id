.class public Lee/cyber/smartid/tse/dto/jsonrpc/resp/RefreshCloneDetectionResp;
.super Lee/cyber/smartid/tse/dto/jsonrpc/resp/BaseResp;
.source "RefreshCloneDetectionResp.java"


# static fields
.field private static final serialVersionUID:J = -0x12fcde20f46ce4dcL


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 0

    .line 22
    invoke-direct {p0, p1}, Lee/cyber/smartid/tse/dto/jsonrpc/resp/BaseResp;-><init>(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public setTag(Ljava/lang/String;)V
    .locals 0

    .line 30
    iput-object p1, p0, Lee/cyber/smartid/tse/dto/jsonrpc/resp/RefreshCloneDetectionResp;->tag:Ljava/lang/String;

    return-void
.end method
