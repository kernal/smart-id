.class public Lee/cyber/smartid/tse/dto/jsonrpc/payload/SubmitSignatureSZPayload;
.super Lee/cyber/smartid/tse/dto/jsonrpc/payload/RefreshCloneDetectionSZPayload;
.source "SubmitSignatureSZPayload.java"


# static fields
.field private static final serialVersionUID:J = 0x1a951000769e46abL


# instance fields
.field protected final digest:Ljava/lang/String;

.field protected final digestAlgorithm:Ljava/lang/String;

.field protected final signatureShare:Ljava/lang/String;

.field protected final signatureShareEncoding:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 39
    invoke-direct {p0, p5, p6}, Lee/cyber/smartid/tse/dto/jsonrpc/payload/RefreshCloneDetectionSZPayload;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 40
    iput-object p1, p0, Lee/cyber/smartid/tse/dto/jsonrpc/payload/SubmitSignatureSZPayload;->signatureShare:Ljava/lang/String;

    .line 41
    iput-object p2, p0, Lee/cyber/smartid/tse/dto/jsonrpc/payload/SubmitSignatureSZPayload;->signatureShareEncoding:Ljava/lang/String;

    .line 42
    iput-object p3, p0, Lee/cyber/smartid/tse/dto/jsonrpc/payload/SubmitSignatureSZPayload;->digestAlgorithm:Ljava/lang/String;

    .line 43
    iput-object p4, p0, Lee/cyber/smartid/tse/dto/jsonrpc/payload/SubmitSignatureSZPayload;->digest:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 3

    .line 48
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "SubmitSignatureSZPayload{signatureShare=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lee/cyber/smartid/tse/dto/jsonrpc/payload/SubmitSignatureSZPayload;->signatureShare:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v2, ", signatureShareEncoding=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lee/cyber/smartid/tse/dto/jsonrpc/payload/SubmitSignatureSZPayload;->signatureShareEncoding:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v2, ", digestAlgorithm=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lee/cyber/smartid/tse/dto/jsonrpc/payload/SubmitSignatureSZPayload;->digestAlgorithm:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v2, ", digest=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lee/cyber/smartid/tse/dto/jsonrpc/payload/SubmitSignatureSZPayload;->digest:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v1, "} "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 53
    invoke-super {p0}, Lee/cyber/smartid/tse/dto/jsonrpc/payload/RefreshCloneDetectionSZPayload;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
