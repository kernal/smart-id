.class public Lee/cyber/smartid/tse/dto/jsonrpc/param/NewFreshnessTokenParams;
.super Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCParams;
.source "NewFreshnessTokenParams.java"


# static fields
.field private static final serialVersionUID:J = 0x7eba0ffccaa0c7ceL


# instance fields
.field private accountUUID:Ljava/lang/String;

.field private keyType:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 15
    invoke-direct {p0}, Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCParams;-><init>()V

    return-void
.end method


# virtual methods
.method public setAccountUUID(Ljava/lang/String;)V
    .locals 0

    .line 28
    iput-object p1, p0, Lee/cyber/smartid/tse/dto/jsonrpc/param/NewFreshnessTokenParams;->accountUUID:Ljava/lang/String;

    return-void
.end method

.method public setKeyType(Ljava/lang/String;)V
    .locals 0

    .line 37
    iput-object p1, p0, Lee/cyber/smartid/tse/dto/jsonrpc/param/NewFreshnessTokenParams;->keyType:Ljava/lang/String;

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .line 47
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "NewFreshnessTokenParams{accountUUID=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lee/cyber/smartid/tse/dto/jsonrpc/param/NewFreshnessTokenParams;->accountUUID:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v2, ", keyType=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lee/cyber/smartid/tse/dto/jsonrpc/param/NewFreshnessTokenParams;->keyType:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
