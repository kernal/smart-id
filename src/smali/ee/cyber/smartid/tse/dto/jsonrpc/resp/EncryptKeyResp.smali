.class public Lee/cyber/smartid/tse/dto/jsonrpc/resp/EncryptKeyResp;
.super Lee/cyber/smartid/tse/dto/jsonrpc/resp/BaseResp;
.source "EncryptKeyResp.java"


# static fields
.field private static final serialVersionUID:J = -0x14a34f69231774faL


# instance fields
.field private final keyReference:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 25
    invoke-direct {p0, p1}, Lee/cyber/smartid/tse/dto/jsonrpc/resp/BaseResp;-><init>(Ljava/lang/String;)V

    .line 26
    iput-object p2, p0, Lee/cyber/smartid/tse/dto/jsonrpc/resp/EncryptKeyResp;->keyReference:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public getKeyReference()Ljava/lang/String;
    .locals 1

    .line 35
    iget-object v0, p0, Lee/cyber/smartid/tse/dto/jsonrpc/resp/EncryptKeyResp;->keyReference:Ljava/lang/String;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 45
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "EncryptKeyResp{keyReference=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lee/cyber/smartid/tse/dto/jsonrpc/resp/EncryptKeyResp;->keyReference:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v1, "} "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 47
    invoke-super {p0}, Lee/cyber/smartid/tse/dto/jsonrpc/resp/BaseResp;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
