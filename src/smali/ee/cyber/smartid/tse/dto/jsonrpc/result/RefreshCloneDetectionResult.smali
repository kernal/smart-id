.class public Lee/cyber/smartid/tse/dto/jsonrpc/result/RefreshCloneDetectionResult;
.super Ljava/lang/Object;
.source "RefreshCloneDetectionResult.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = 0x3a736ec1c6722dcL


# instance fields
.field protected responseData:Ljava/lang/String;

.field protected responseDataEncoding:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    iput-object p1, p0, Lee/cyber/smartid/tse/dto/jsonrpc/result/RefreshCloneDetectionResult;->responseData:Ljava/lang/String;

    .line 37
    iput-object p2, p0, Lee/cyber/smartid/tse/dto/jsonrpc/result/RefreshCloneDetectionResult;->responseDataEncoding:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public getResponseData()Ljava/lang/String;
    .locals 1

    .line 46
    iget-object v0, p0, Lee/cyber/smartid/tse/dto/jsonrpc/result/RefreshCloneDetectionResult;->responseData:Ljava/lang/String;

    return-object v0
.end method

.method public getResponseDataEncoding()Ljava/lang/String;
    .locals 1

    .line 55
    iget-object v0, p0, Lee/cyber/smartid/tse/dto/jsonrpc/result/RefreshCloneDetectionResult;->responseDataEncoding:Ljava/lang/String;

    return-object v0
.end method

.method public isValid()Z
    .locals 1

    .line 64
    iget-object v0, p0, Lee/cyber/smartid/tse/dto/jsonrpc/result/RefreshCloneDetectionResult;->responseData:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lee/cyber/smartid/tse/dto/jsonrpc/result/RefreshCloneDetectionResult;->responseDataEncoding:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .line 69
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "RefreshCloneDetectionResult{responseData=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lee/cyber/smartid/tse/dto/jsonrpc/result/RefreshCloneDetectionResult;->responseData:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v2, ", responseDataEncoding=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lee/cyber/smartid/tse/dto/jsonrpc/result/RefreshCloneDetectionResult;->responseDataEncoding:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
