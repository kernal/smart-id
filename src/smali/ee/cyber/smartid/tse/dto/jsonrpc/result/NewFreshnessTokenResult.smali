.class public Lee/cyber/smartid/tse/dto/jsonrpc/result/NewFreshnessTokenResult;
.super Ljava/lang/Object;
.source "NewFreshnessTokenResult.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = -0x36610fb30b9c4ce6L


# instance fields
.field private freshnessToken:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getFreshnessToken()Ljava/lang/String;
    .locals 1

    .line 38
    iget-object v0, p0, Lee/cyber/smartid/tse/dto/jsonrpc/result/NewFreshnessTokenResult;->freshnessToken:Ljava/lang/String;

    return-object v0
.end method

.method public isValid()Z
    .locals 1

    .line 56
    iget-object v0, p0, Lee/cyber/smartid/tse/dto/jsonrpc/result/NewFreshnessTokenResult;->freshnessToken:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public setFreshnessToken(Ljava/lang/String;)V
    .locals 0

    .line 47
    iput-object p1, p0, Lee/cyber/smartid/tse/dto/jsonrpc/result/NewFreshnessTokenResult;->freshnessToken:Ljava/lang/String;

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 27
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "NewFreshnessTokenResult{freshnessToken=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lee/cyber/smartid/tse/dto/jsonrpc/result/NewFreshnessTokenResult;->freshnessToken:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
