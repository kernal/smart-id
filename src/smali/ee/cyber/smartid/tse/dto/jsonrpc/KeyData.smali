.class public Lee/cyber/smartid/tse/dto/jsonrpc/KeyData;
.super Ljava/lang/Object;
.source "KeyData.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = 0x48ae51fe599e5cc9L


# instance fields
.field private compositeModulus:Ljava/lang/String;

.field private compositeModulusBits:I

.field private freshnessToken:Ljava/lang/String;

.field private responseData:Ljava/lang/String;

.field private responseDataEncoding:Ljava/lang/String;

.field private serverDhMessage:Ljava/lang/String;

.field private serverDhMessageEncoding:Ljava/lang/String;

.field private serverDhPublicKey:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getCompositeModulus()Ljava/lang/String;
    .locals 1

    .line 39
    iget-object v0, p0, Lee/cyber/smartid/tse/dto/jsonrpc/KeyData;->compositeModulus:Ljava/lang/String;

    return-object v0
.end method

.method public getCompositeModulusBits()I
    .locals 1

    .line 48
    iget v0, p0, Lee/cyber/smartid/tse/dto/jsonrpc/KeyData;->compositeModulusBits:I

    return v0
.end method

.method public getFreshnessToken()Ljava/lang/String;
    .locals 1

    .line 102
    iget-object v0, p0, Lee/cyber/smartid/tse/dto/jsonrpc/KeyData;->freshnessToken:Ljava/lang/String;

    return-object v0
.end method

.method public getResponseData()Ljava/lang/String;
    .locals 1

    .line 93
    iget-object v0, p0, Lee/cyber/smartid/tse/dto/jsonrpc/KeyData;->responseData:Ljava/lang/String;

    return-object v0
.end method

.method public getResponseDataEncoding()Ljava/lang/String;
    .locals 1

    .line 84
    iget-object v0, p0, Lee/cyber/smartid/tse/dto/jsonrpc/KeyData;->responseDataEncoding:Ljava/lang/String;

    return-object v0
.end method

.method public getServerDhMessage()Ljava/lang/String;
    .locals 1

    .line 75
    iget-object v0, p0, Lee/cyber/smartid/tse/dto/jsonrpc/KeyData;->serverDhMessage:Ljava/lang/String;

    return-object v0
.end method

.method public getServerDhMessageEncoding()Ljava/lang/String;
    .locals 1

    .line 66
    iget-object v0, p0, Lee/cyber/smartid/tse/dto/jsonrpc/KeyData;->serverDhMessageEncoding:Ljava/lang/String;

    return-object v0
.end method

.method public getServerDhPublicKey()Ljava/lang/String;
    .locals 1

    .line 57
    iget-object v0, p0, Lee/cyber/smartid/tse/dto/jsonrpc/KeyData;->serverDhPublicKey:Ljava/lang/String;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .line 107
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "KeyData{compositeModulus=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lee/cyber/smartid/tse/dto/jsonrpc/KeyData;->compositeModulus:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v2, ", compositeModulusBits="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v2, p0, Lee/cyber/smartid/tse/dto/jsonrpc/KeyData;->compositeModulusBits:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v2, ", serverDhPublicKey=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lee/cyber/smartid/tse/dto/jsonrpc/KeyData;->serverDhPublicKey:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v2, ", serverDhMessageEncoding=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lee/cyber/smartid/tse/dto/jsonrpc/KeyData;->serverDhMessageEncoding:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v2, ", serverDhMessage=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lee/cyber/smartid/tse/dto/jsonrpc/KeyData;->serverDhMessage:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v2, ", responseDataEncoding=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lee/cyber/smartid/tse/dto/jsonrpc/KeyData;->responseDataEncoding:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v2, ", responseData=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lee/cyber/smartid/tse/dto/jsonrpc/KeyData;->responseData:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v2, ", freshnessToken=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lee/cyber/smartid/tse/dto/jsonrpc/KeyData;->freshnessToken:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
