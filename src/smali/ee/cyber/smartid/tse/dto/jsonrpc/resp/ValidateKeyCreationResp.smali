.class public Lee/cyber/smartid/tse/dto/jsonrpc/resp/ValidateKeyCreationResp;
.super Lee/cyber/smartid/tse/dto/jsonrpc/resp/BaseResp;
.source "ValidateKeyCreationResp.java"


# static fields
.field private static final serialVersionUID:J = -0x6ab39ee21ca3b720L


# instance fields
.field private final isValid:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;Z)V
    .locals 0

    .line 23
    invoke-direct {p0, p1}, Lee/cyber/smartid/tse/dto/jsonrpc/resp/BaseResp;-><init>(Ljava/lang/String;)V

    .line 24
    iput-boolean p2, p0, Lee/cyber/smartid/tse/dto/jsonrpc/resp/ValidateKeyCreationResp;->isValid:Z

    return-void
.end method


# virtual methods
.method public isValid()Z
    .locals 1

    .line 33
    iget-boolean v0, p0, Lee/cyber/smartid/tse/dto/jsonrpc/resp/ValidateKeyCreationResp;->isValid:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 38
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ValidateKeyCreationResp{isValid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lee/cyber/smartid/tse/dto/jsonrpc/resp/ValidateKeyCreationResp;->isValid:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, "} "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 40
    invoke-super {p0}, Lee/cyber/smartid/tse/dto/jsonrpc/resp/BaseResp;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
