.class public Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCErrorData;
.super Ljava/lang/Object;
.source "RPCErrorData.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = 0x4e86ce33d5d8047eL


# instance fields
.field private keyInfo:Lee/cyber/smartid/tse/dto/jsonrpc/AccountKeyStatus;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public constructor <init>(Lee/cyber/smartid/tse/dto/jsonrpc/AccountKeyStatus;)V
    .locals 0

    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iput-object p1, p0, Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCErrorData;->keyInfo:Lee/cyber/smartid/tse/dto/jsonrpc/AccountKeyStatus;

    return-void
.end method


# virtual methods
.method public getKeyStatusInfo()Lee/cyber/smartid/tse/dto/jsonrpc/AccountKeyStatus;
    .locals 1

    .line 42
    iget-object v0, p0, Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCErrorData;->keyInfo:Lee/cyber/smartid/tse/dto/jsonrpc/AccountKeyStatus;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 52
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "RPCErrorData{keyInfo="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCErrorData;->keyInfo:Lee/cyber/smartid/tse/dto/jsonrpc/AccountKeyStatus;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
