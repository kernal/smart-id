.class public Lee/cyber/smartid/tse/dto/jsonrpc/payload/SubmitClientSecondPartSZPayload;
.super Lee/cyber/smartid/tse/dto/jsonrpc/payload/RefreshCloneDetectionSZPayload;
.source "SubmitClientSecondPartSZPayload.java"


# static fields
.field private static final serialVersionUID:J = 0x1a951000769e46abL


# instance fields
.field protected final client2ndPart:Ljava/lang/String;

.field protected final client2ndPartEncoding:Ljava/lang/String;

.field protected final clientModulus:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 36
    invoke-direct {p0, p4, p5}, Lee/cyber/smartid/tse/dto/jsonrpc/payload/RefreshCloneDetectionSZPayload;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 37
    iput-object p1, p0, Lee/cyber/smartid/tse/dto/jsonrpc/payload/SubmitClientSecondPartSZPayload;->client2ndPart:Ljava/lang/String;

    .line 38
    iput-object p2, p0, Lee/cyber/smartid/tse/dto/jsonrpc/payload/SubmitClientSecondPartSZPayload;->client2ndPartEncoding:Ljava/lang/String;

    .line 39
    iput-object p3, p0, Lee/cyber/smartid/tse/dto/jsonrpc/payload/SubmitClientSecondPartSZPayload;->clientModulus:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 3

    .line 44
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "SubmitClientSecondPartSZPayload{client2ndPart=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lee/cyber/smartid/tse/dto/jsonrpc/payload/SubmitClientSecondPartSZPayload;->client2ndPart:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v2, ", client2ndPartEncoding=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lee/cyber/smartid/tse/dto/jsonrpc/payload/SubmitClientSecondPartSZPayload;->client2ndPartEncoding:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v2, ", clientModulus=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lee/cyber/smartid/tse/dto/jsonrpc/payload/SubmitClientSecondPartSZPayload;->clientModulus:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v1, "} "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 48
    invoke-super {p0}, Lee/cyber/smartid/tse/dto/jsonrpc/payload/RefreshCloneDetectionSZPayload;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
