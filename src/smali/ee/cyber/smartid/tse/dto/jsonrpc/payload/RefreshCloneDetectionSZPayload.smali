.class public Lee/cyber/smartid/tse/dto/jsonrpc/payload/RefreshCloneDetectionSZPayload;
.super Lee/cyber/smartid/tse/dto/jsonrpc/payload/OneTimePasswordSZPayload;
.source "RefreshCloneDetectionSZPayload.java"


# static fields
.field private static final serialVersionUID:J = 0x1a951000769e46abL


# instance fields
.field protected final retransmitNonce:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 26
    invoke-direct {p0, p1}, Lee/cyber/smartid/tse/dto/jsonrpc/payload/OneTimePasswordSZPayload;-><init>(Ljava/lang/String;)V

    .line 27
    iput-object p2, p0, Lee/cyber/smartid/tse/dto/jsonrpc/payload/RefreshCloneDetectionSZPayload;->retransmitNonce:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 2

    .line 32
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "RefreshCloneDetectionSZPayload{retransmitNonce=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lee/cyber/smartid/tse/dto/jsonrpc/payload/RefreshCloneDetectionSZPayload;->retransmitNonce:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v1, "} "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 34
    invoke-super {p0}, Lee/cyber/smartid/tse/dto/jsonrpc/payload/OneTimePasswordSZPayload;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
