.class public Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCError;
.super Ljava/lang/Object;
.source "RPCError.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = 0x52dc003ed23e04a3L


# instance fields
.field private code:J

.field private data:Lee/cyber/smartid/tse/dto/ResultWithRawJson;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lee/cyber/smartid/tse/dto/ResultWithRawJson<",
            "Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCErrorData;",
            ">;"
        }
    .end annotation
.end field

.field private message:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 3

    const/4 v0, 0x0

    const-wide/16 v1, 0x0

    .line 27
    invoke-direct {p0, v1, v2, v0, v0}, Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCError;-><init>(JLjava/lang/String;Lee/cyber/smartid/tse/dto/ResultWithRawJson;)V

    return-void
.end method

.method public constructor <init>(JLjava/lang/String;Lee/cyber/smartid/tse/dto/ResultWithRawJson;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/lang/String;",
            "Lee/cyber/smartid/tse/dto/ResultWithRawJson<",
            "Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCErrorData;",
            ">;)V"
        }
    .end annotation

    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    iput-wide p1, p0, Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCError;->code:J

    .line 39
    iput-object p3, p0, Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCError;->message:Ljava/lang/String;

    .line 40
    iput-object p4, p0, Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCError;->data:Lee/cyber/smartid/tse/dto/ResultWithRawJson;

    return-void
.end method


# virtual methods
.method public getCode()J
    .locals 2

    .line 49
    iget-wide v0, p0, Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCError;->code:J

    return-wide v0
.end method

.method public getData()Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCErrorData;
    .locals 1

    .line 67
    iget-object v0, p0, Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCError;->data:Lee/cyber/smartid/tse/dto/ResultWithRawJson;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return-object v0

    .line 70
    :cond_0
    invoke-virtual {v0}, Lee/cyber/smartid/tse/dto/ResultWithRawJson;->getDeserialized()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCErrorData;

    return-object v0
.end method

.method public getDataRaw()Ljava/lang/String;
    .locals 1

    .line 79
    iget-object v0, p0, Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCError;->data:Lee/cyber/smartid/tse/dto/ResultWithRawJson;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return-object v0

    .line 82
    :cond_0
    invoke-virtual {v0}, Lee/cyber/smartid/tse/dto/ResultWithRawJson;->getSerialized()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getMessage()Ljava/lang/String;
    .locals 1

    .line 58
    iget-object v0, p0, Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCError;->message:Ljava/lang/String;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .line 92
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "RPCError{code="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCError;->code:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ", message=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCError;->message:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v1, ", data="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCError;->data:Lee/cyber/smartid/tse/dto/ResultWithRawJson;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
