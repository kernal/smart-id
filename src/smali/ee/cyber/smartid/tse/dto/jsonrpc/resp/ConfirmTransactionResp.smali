.class public Lee/cyber/smartid/tse/dto/jsonrpc/resp/ConfirmTransactionResp;
.super Lee/cyber/smartid/tse/dto/jsonrpc/resp/BaseResp;
.source "ConfirmTransactionResp.java"


# static fields
.field private static final serialVersionUID:J = 0x61e1078289288b2bL


# instance fields
.field private final transactionUUID:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lee/cyber/smartid/tse/dto/jsonrpc/resp/ConfirmTransactionResp;)V
    .locals 1

    .line 33
    invoke-virtual {p1}, Lee/cyber/smartid/tse/dto/jsonrpc/resp/ConfirmTransactionResp;->getTag()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lee/cyber/smartid/tse/dto/jsonrpc/resp/BaseResp;-><init>(Ljava/lang/String;)V

    .line 34
    iget-object p1, p1, Lee/cyber/smartid/tse/dto/jsonrpc/resp/ConfirmTransactionResp;->transactionUUID:Ljava/lang/String;

    iput-object p1, p0, Lee/cyber/smartid/tse/dto/jsonrpc/resp/ConfirmTransactionResp;->transactionUUID:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 23
    invoke-direct {p0, p2}, Lee/cyber/smartid/tse/dto/jsonrpc/resp/BaseResp;-><init>(Ljava/lang/String;)V

    .line 24
    iput-object p1, p0, Lee/cyber/smartid/tse/dto/jsonrpc/resp/ConfirmTransactionResp;->transactionUUID:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public getTransactionUUID()Ljava/lang/String;
    .locals 1

    .line 43
    iget-object v0, p0, Lee/cyber/smartid/tse/dto/jsonrpc/resp/ConfirmTransactionResp;->transactionUUID:Ljava/lang/String;

    return-object v0
.end method
