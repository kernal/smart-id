.class public Lee/cyber/smartid/tse/dto/jsonrpc/payload/OneTimePasswordSZPayload;
.super Lee/cyber/smartid/tse/inter/SZPayload;
.source "OneTimePasswordSZPayload.java"


# static fields
.field private static final serialVersionUID:J = 0x3d8400884546307fL


# instance fields
.field protected oneTimePassword:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 0

    .line 37
    invoke-direct {p0}, Lee/cyber/smartid/tse/inter/SZPayload;-><init>()V

    .line 38
    iput-object p1, p0, Lee/cyber/smartid/tse/dto/jsonrpc/payload/OneTimePasswordSZPayload;->oneTimePassword:Ljava/lang/String;

    return-void
.end method

.method public static decryptAndDeserialize(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/cryptolib/inter/CryptoOp;Lee/cyber/smartid/tse/inter/KeyStorageAccess;Lee/cyber/smartid/cryptolib/inter/EncodingOp;)Lee/cyber/smartid/tse/dto/jsonrpc/payload/OneTimePasswordSZPayload;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;
        }
    .end annotation

    if-eqz p0, :cond_3

    if-eqz p4, :cond_3

    if-eqz p6, :cond_3

    if-nez p5, :cond_0

    goto :goto_0

    :cond_0
    const-string v0, "JWE"

    .line 71
    invoke-static {p3, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 75
    invoke-interface {p5, p1}, Lee/cyber/smartid/tse/inter/KeyStorageAccess;->getKey(Ljava/lang/String;)Lee/cyber/smartid/tse/dto/Key;

    move-result-object p1

    if-eqz p1, :cond_1

    .line 76
    invoke-virtual {p1, p6}, Lee/cyber/smartid/tse/dto/Key;->getDHDerivedKeyBytes(Lee/cyber/smartid/cryptolib/inter/EncodingOp;)[B

    move-result-object p3

    if-eqz p3, :cond_1

    .line 81
    invoke-virtual {p1, p6}, Lee/cyber/smartid/tse/dto/Key;->getDHDerivedKeyBytes(Lee/cyber/smartid/cryptolib/inter/EncodingOp;)[B

    move-result-object v2

    invoke-virtual {p1}, Lee/cyber/smartid/tse/dto/Key;->getDhDerivedKeyId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Lee/cyber/smartid/tse/dto/Key;->getDhDerivedKeyId()Ljava/lang/String;

    move-result-object v4

    const-string v5, "CLIENT"

    move-object v0, p4

    move-object v1, p2

    invoke-interface/range {v0 .. v5}, Lee/cyber/smartid/cryptolib/inter/CryptoOp;->decryptFromTEKEncryptedJWE(Ljava/lang/String;[BLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/a/a/n;

    move-result-object p0

    invoke-virtual {p0}, Lcom/a/a/n;->a()Lcom/a/a/v;

    move-result-object p0

    invoke-virtual {p0}, Lcom/a/a/v;->toString()Ljava/lang/String;

    move-result-object p0

    .line 82
    invoke-static {}, Lee/cyber/smartid/tse/network/TSEAPI;->createGSONBuilderForAPIResponses()Lcom/google/gson/g;

    move-result-object p1

    invoke-virtual {p1}, Lcom/google/gson/g;->a()Lcom/google/gson/f;

    move-result-object p1

    const-class p2, Lee/cyber/smartid/tse/dto/jsonrpc/payload/OneTimePasswordSZPayload;

    invoke-virtual {p1, p0, p2}, Lcom/google/gson/f;->a(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lee/cyber/smartid/tse/dto/jsonrpc/payload/OneTimePasswordSZPayload;

    return-object p0

    .line 77
    :cond_1
    new-instance p1, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;

    const/16 p2, 0x7d

    sget p3, Lee/cyber/smartid/tse/R$string;->err_unable_to_decrypt_illegal_key_provided:I

    invoke-virtual {p0, p3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p0

    invoke-direct {p1, p2, p0}, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;-><init>(ILjava/lang/String;)V

    throw p1

    .line 72
    :cond_2
    new-instance p1, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;

    const/16 p2, 0x73

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p0

    sget p4, Lee/cyber/smartid/tse/R$string;->err_unsupported_response_encoding_x:I

    const/4 p5, 0x1

    new-array p5, p5, [Ljava/lang/Object;

    const/4 p6, 0x0

    aput-object p3, p5, p6

    invoke-virtual {p0, p4, p5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    invoke-direct {p1, p2, p0}, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;-><init>(ILjava/lang/String;)V

    throw p1

    .line 70
    :cond_3
    :goto_0
    new-instance p1, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;

    const/16 p2, 0x68

    if-eqz p0, :cond_4

    sget p3, Lee/cyber/smartid/tse/R$string;->err_encryption_failed_invalid_parameters:I

    invoke-virtual {p0, p3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p0

    goto :goto_1

    :cond_4
    const-string p0, ""

    :goto_1
    invoke-direct {p1, p2, p0}, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;-><init>(ILjava/lang/String;)V

    throw p1
.end method

.method public static decryptAndDeserializeOTP(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/cryptolib/inter/CryptoOp;Lee/cyber/smartid/tse/inter/KeyStorageAccess;Lee/cyber/smartid/cryptolib/inter/EncodingOp;)Ljava/lang/String;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;
        }
    .end annotation

    .line 101
    invoke-static/range {p0 .. p6}, Lee/cyber/smartid/tse/dto/jsonrpc/payload/OneTimePasswordSZPayload;->decryptAndDeserialize(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/cryptolib/inter/CryptoOp;Lee/cyber/smartid/tse/inter/KeyStorageAccess;Lee/cyber/smartid/cryptolib/inter/EncodingOp;)Lee/cyber/smartid/tse/dto/jsonrpc/payload/OneTimePasswordSZPayload;

    move-result-object p1

    iget-object p1, p1, Lee/cyber/smartid/tse/dto/jsonrpc/payload/OneTimePasswordSZPayload;->oneTimePassword:Ljava/lang/String;

    .line 102
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p2

    if-nez p2, :cond_0

    return-object p1

    .line 103
    :cond_0
    new-instance p1, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;

    const/16 p2, 0x68

    sget p3, Lee/cyber/smartid/tse/R$string;->err_failed_to_decrypt_server_response:I

    invoke-virtual {p0, p3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p0

    invoke-direct {p1, p2, p0}, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;-><init>(ILjava/lang/String;)V

    throw p1
.end method


# virtual methods
.method public getOneTimePassword()Ljava/lang/String;
    .locals 1

    .line 48
    iget-object v0, p0, Lee/cyber/smartid/tse/dto/jsonrpc/payload/OneTimePasswordSZPayload;->oneTimePassword:Ljava/lang/String;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 110
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "OneTimePasswordSZPayload{oneTimePassword=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lee/cyber/smartid/tse/dto/jsonrpc/payload/OneTimePasswordSZPayload;->oneTimePassword:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v1, "} "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 112
    invoke-super {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
