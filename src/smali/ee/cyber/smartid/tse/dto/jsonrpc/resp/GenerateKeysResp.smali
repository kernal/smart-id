.class public Lee/cyber/smartid/tse/dto/jsonrpc/resp/GenerateKeysResp;
.super Lee/cyber/smartid/tse/dto/jsonrpc/resp/BaseResp;
.source "GenerateKeysResp.java"


# static fields
.field private static final serialVersionUID:J = -0x5f3ea2e0db36fcc7L


# instance fields
.field private keyRefs:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private timeTakenMS:J


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/util/ArrayList;J)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;J)V"
        }
    .end annotation

    .line 28
    invoke-direct {p0, p1}, Lee/cyber/smartid/tse/dto/jsonrpc/resp/BaseResp;-><init>(Ljava/lang/String;)V

    .line 29
    iput-object p2, p0, Lee/cyber/smartid/tse/dto/jsonrpc/resp/GenerateKeysResp;->keyRefs:Ljava/util/ArrayList;

    .line 30
    iput-wide p3, p0, Lee/cyber/smartid/tse/dto/jsonrpc/resp/GenerateKeysResp;->timeTakenMS:J

    return-void
.end method


# virtual methods
.method public getKeyRefs()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 39
    iget-object v0, p0, Lee/cyber/smartid/tse/dto/jsonrpc/resp/GenerateKeysResp;->keyRefs:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getTimeTakenMS()J
    .locals 2

    .line 48
    iget-wide v0, p0, Lee/cyber/smartid/tse/dto/jsonrpc/resp/GenerateKeysResp;->timeTakenMS:J

    return-wide v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .line 58
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "GenerateKeysResp{keyRefs="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lee/cyber/smartid/tse/dto/jsonrpc/resp/GenerateKeysResp;->keyRefs:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", timeTakenMS="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lee/cyber/smartid/tse/dto/jsonrpc/resp/GenerateKeysResp;->timeTakenMS:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
