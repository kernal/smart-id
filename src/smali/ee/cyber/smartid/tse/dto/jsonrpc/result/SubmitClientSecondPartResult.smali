.class public Lee/cyber/smartid/tse/dto/jsonrpc/result/SubmitClientSecondPartResult;
.super Lee/cyber/smartid/tse/dto/jsonrpc/result/RefreshCloneDetectionResult;
.source "SubmitClientSecondPartResult.java"


# static fields
.field private static final serialVersionUID:J = -0x12b23507b78ed3e8L


# instance fields
.field private csrTransactionUUID:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 21
    invoke-direct {p0}, Lee/cyber/smartid/tse/dto/jsonrpc/result/RefreshCloneDetectionResult;-><init>()V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 33
    invoke-direct {p0, p1, p2}, Lee/cyber/smartid/tse/dto/jsonrpc/result/RefreshCloneDetectionResult;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 34
    iput-object p3, p0, Lee/cyber/smartid/tse/dto/jsonrpc/result/SubmitClientSecondPartResult;->csrTransactionUUID:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public getCsrTransactionUUID()Ljava/lang/String;
    .locals 1

    .line 43
    iget-object v0, p0, Lee/cyber/smartid/tse/dto/jsonrpc/result/SubmitClientSecondPartResult;->csrTransactionUUID:Ljava/lang/String;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 48
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "SubmitClientSecondPartResult{} "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-super {p0}, Lee/cyber/smartid/tse/dto/jsonrpc/result/RefreshCloneDetectionResult;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
