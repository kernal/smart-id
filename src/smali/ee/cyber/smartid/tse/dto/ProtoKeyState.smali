.class public Lee/cyber/smartid/tse/dto/ProtoKeyState;
.super Ljava/lang/Object;
.source "ProtoKeyState.java"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lee/cyber/smartid/tse/dto/ProtoKeyState$KeyOperationOrigin;,
        Lee/cyber/smartid/tse/dto/ProtoKeyState$KeyOperationType;
    }
.end annotation


# static fields
.field private static final CURRENT_FORMAT_VERSION:I = 0x3

.field public static final FORMAT_VERSION_1:I = 0x1

.field public static final FORMAT_VERSION_2:I = 0x2

.field public static final FORMAT_VERSION_3:I = 0x3

.field protected static final OPERATION_ORIGIN_NEW:I = 0x1

.field protected static final OPERATION_ORIGIN_NONE:I = 0x0

.field protected static final OPERATION_ORIGIN_RETRY:I = 0x2

.field public static final TYPE_CLONE_DETECTION:I = 0x3

.field public static final TYPE_IDLE:I = 0x0

.field public static final TYPE_SIGN:I = 0x1

.field public static final TYPE_SUBMIT_CLIENT_SECOND_PART:I = 0x4

.field private static final serialVersionUID:J = 0x5430a8a2b923ff9cL


# instance fields
.field protected final accountUUID:Ljava/lang/String;

.field protected clientModulus:Ljava/lang/String;

.field protected clientShareSecondPart:Ljava/lang/String;

.field protected digest:Ljava/lang/String;

.field protected digestAlgorithm:Ljava/lang/String;

.field protected formatVersion:I

.field protected final id:Ljava/lang/String;

.field protected final keyId:Ljava/lang/String;

.field protected keyType:Ljava/lang/String;

.field protected nonce:Ljava/lang/String;

.field protected operationOrigin:I

.field protected signatureShare:Ljava/lang/String;

.field protected transactionUUID:Ljava/lang/String;

.field protected type:I


# direct methods
.method protected constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 203
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 204
    iput-object p1, p0, Lee/cyber/smartid/tse/dto/ProtoKeyState;->id:Ljava/lang/String;

    .line 205
    iput-object p2, p0, Lee/cyber/smartid/tse/dto/ProtoKeyState;->keyId:Ljava/lang/String;

    .line 206
    iput-object p3, p0, Lee/cyber/smartid/tse/dto/ProtoKeyState;->accountUUID:Ljava/lang/String;

    return-void
.end method

.method public static forCloneDetection(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lee/cyber/smartid/tse/dto/ProtoKeyState;
    .locals 1

    .line 301
    new-instance v0, Lee/cyber/smartid/tse/dto/ProtoKeyState;

    invoke-direct {v0, p0, p1, p2}, Lee/cyber/smartid/tse/dto/ProtoKeyState;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    const/4 p0, 0x3

    .line 302
    iput p0, v0, Lee/cyber/smartid/tse/dto/ProtoKeyState;->type:I

    const/4 p1, 0x1

    .line 303
    iput p1, v0, Lee/cyber/smartid/tse/dto/ProtoKeyState;->operationOrigin:I

    const/4 p1, 0x0

    .line 304
    iput-object p1, v0, Lee/cyber/smartid/tse/dto/ProtoKeyState;->transactionUUID:Ljava/lang/String;

    .line 305
    iput-object p4, v0, Lee/cyber/smartid/tse/dto/ProtoKeyState;->nonce:Ljava/lang/String;

    .line 306
    iput-object p1, v0, Lee/cyber/smartid/tse/dto/ProtoKeyState;->signatureShare:Ljava/lang/String;

    .line 307
    iput-object p1, v0, Lee/cyber/smartid/tse/dto/ProtoKeyState;->digest:Ljava/lang/String;

    .line 308
    iput-object p1, v0, Lee/cyber/smartid/tse/dto/ProtoKeyState;->digestAlgorithm:Ljava/lang/String;

    .line 309
    iput-object p3, v0, Lee/cyber/smartid/tse/dto/ProtoKeyState;->keyType:Ljava/lang/String;

    .line 310
    iput-object p1, v0, Lee/cyber/smartid/tse/dto/ProtoKeyState;->clientShareSecondPart:Ljava/lang/String;

    .line 311
    iput-object p1, v0, Lee/cyber/smartid/tse/dto/ProtoKeyState;->clientModulus:Ljava/lang/String;

    .line 312
    iput p0, v0, Lee/cyber/smartid/tse/dto/ProtoKeyState;->formatVersion:I

    return-object v0
.end method

.method public static forIdle(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lee/cyber/smartid/tse/dto/ProtoKeyState;
    .locals 1

    .line 219
    new-instance v0, Lee/cyber/smartid/tse/dto/ProtoKeyState;

    invoke-direct {v0, p0, p1, p2}, Lee/cyber/smartid/tse/dto/ProtoKeyState;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    const/4 p0, 0x0

    .line 220
    iput p0, v0, Lee/cyber/smartid/tse/dto/ProtoKeyState;->type:I

    .line 221
    iput p0, v0, Lee/cyber/smartid/tse/dto/ProtoKeyState;->operationOrigin:I

    const/4 p0, 0x0

    .line 222
    iput-object p0, v0, Lee/cyber/smartid/tse/dto/ProtoKeyState;->transactionUUID:Ljava/lang/String;

    .line 223
    iput-object p0, v0, Lee/cyber/smartid/tse/dto/ProtoKeyState;->signatureShare:Ljava/lang/String;

    .line 224
    iput-object p0, v0, Lee/cyber/smartid/tse/dto/ProtoKeyState;->digest:Ljava/lang/String;

    .line 225
    iput-object p0, v0, Lee/cyber/smartid/tse/dto/ProtoKeyState;->digestAlgorithm:Ljava/lang/String;

    .line 226
    iput-object p3, v0, Lee/cyber/smartid/tse/dto/ProtoKeyState;->keyType:Ljava/lang/String;

    .line 227
    iput-object p0, v0, Lee/cyber/smartid/tse/dto/ProtoKeyState;->clientShareSecondPart:Ljava/lang/String;

    .line 228
    iput-object p0, v0, Lee/cyber/smartid/tse/dto/ProtoKeyState;->clientModulus:Ljava/lang/String;

    const/4 p0, 0x3

    .line 229
    iput p0, v0, Lee/cyber/smartid/tse/dto/ProtoKeyState;->formatVersion:I

    return-object v0
.end method

.method private static forRetry(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)Lee/cyber/smartid/tse/dto/ProtoKeyState;
    .locals 1

    .line 245
    new-instance v0, Lee/cyber/smartid/tse/dto/ProtoKeyState;

    invoke-direct {v0, p0, p1, p2}, Lee/cyber/smartid/tse/dto/ProtoKeyState;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 246
    iput p5, v0, Lee/cyber/smartid/tse/dto/ProtoKeyState;->type:I

    const/4 p0, 0x2

    .line 247
    iput p0, v0, Lee/cyber/smartid/tse/dto/ProtoKeyState;->operationOrigin:I

    .line 248
    iput-object p3, v0, Lee/cyber/smartid/tse/dto/ProtoKeyState;->transactionUUID:Ljava/lang/String;

    const/4 p0, 0x0

    .line 249
    iput-object p0, v0, Lee/cyber/smartid/tse/dto/ProtoKeyState;->nonce:Ljava/lang/String;

    .line 250
    iput-object p0, v0, Lee/cyber/smartid/tse/dto/ProtoKeyState;->signatureShare:Ljava/lang/String;

    .line 251
    iput-object p0, v0, Lee/cyber/smartid/tse/dto/ProtoKeyState;->digest:Ljava/lang/String;

    .line 252
    iput-object p0, v0, Lee/cyber/smartid/tse/dto/ProtoKeyState;->digestAlgorithm:Ljava/lang/String;

    .line 253
    iput-object p4, v0, Lee/cyber/smartid/tse/dto/ProtoKeyState;->keyType:Ljava/lang/String;

    .line 254
    iput-object p0, v0, Lee/cyber/smartid/tse/dto/ProtoKeyState;->clientShareSecondPart:Ljava/lang/String;

    .line 255
    iput-object p0, v0, Lee/cyber/smartid/tse/dto/ProtoKeyState;->clientModulus:Ljava/lang/String;

    const/4 p0, 0x3

    .line 256
    iput p0, v0, Lee/cyber/smartid/tse/dto/ProtoKeyState;->formatVersion:I

    return-object v0
.end method

.method public static forSign(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lee/cyber/smartid/tse/dto/ProtoKeyState;
    .locals 1

    .line 275
    new-instance v0, Lee/cyber/smartid/tse/dto/ProtoKeyState;

    invoke-direct {v0, p0, p1, p2}, Lee/cyber/smartid/tse/dto/ProtoKeyState;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    const/4 p0, 0x1

    .line 276
    iput p0, v0, Lee/cyber/smartid/tse/dto/ProtoKeyState;->type:I

    .line 277
    iput p0, v0, Lee/cyber/smartid/tse/dto/ProtoKeyState;->operationOrigin:I

    .line 278
    iput-object p5, v0, Lee/cyber/smartid/tse/dto/ProtoKeyState;->transactionUUID:Ljava/lang/String;

    .line 279
    iput-object p4, v0, Lee/cyber/smartid/tse/dto/ProtoKeyState;->nonce:Ljava/lang/String;

    .line 280
    iput-object p6, v0, Lee/cyber/smartid/tse/dto/ProtoKeyState;->signatureShare:Ljava/lang/String;

    .line 281
    iput-object p7, v0, Lee/cyber/smartid/tse/dto/ProtoKeyState;->digest:Ljava/lang/String;

    .line 282
    iput-object p8, v0, Lee/cyber/smartid/tse/dto/ProtoKeyState;->digestAlgorithm:Ljava/lang/String;

    .line 283
    iput-object p3, v0, Lee/cyber/smartid/tse/dto/ProtoKeyState;->keyType:Ljava/lang/String;

    const/4 p0, 0x0

    .line 284
    iput-object p0, v0, Lee/cyber/smartid/tse/dto/ProtoKeyState;->clientShareSecondPart:Ljava/lang/String;

    .line 285
    iput-object p0, v0, Lee/cyber/smartid/tse/dto/ProtoKeyState;->clientModulus:Ljava/lang/String;

    const/4 p0, 0x3

    .line 286
    iput p0, v0, Lee/cyber/smartid/tse/dto/ProtoKeyState;->formatVersion:I

    return-object v0
.end method

.method public static forSubmitClientSecondPart(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lee/cyber/smartid/tse/dto/ProtoKeyState;
    .locals 1

    .line 329
    new-instance v0, Lee/cyber/smartid/tse/dto/ProtoKeyState;

    invoke-direct {v0, p0, p1, p2}, Lee/cyber/smartid/tse/dto/ProtoKeyState;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    const/4 p0, 0x4

    .line 330
    iput p0, v0, Lee/cyber/smartid/tse/dto/ProtoKeyState;->type:I

    const/4 p0, 0x1

    .line 331
    iput p0, v0, Lee/cyber/smartid/tse/dto/ProtoKeyState;->operationOrigin:I

    const/4 p0, 0x0

    .line 332
    iput-object p0, v0, Lee/cyber/smartid/tse/dto/ProtoKeyState;->transactionUUID:Ljava/lang/String;

    .line 333
    iput-object p4, v0, Lee/cyber/smartid/tse/dto/ProtoKeyState;->nonce:Ljava/lang/String;

    .line 334
    iput-object p0, v0, Lee/cyber/smartid/tse/dto/ProtoKeyState;->signatureShare:Ljava/lang/String;

    .line 335
    iput-object p0, v0, Lee/cyber/smartid/tse/dto/ProtoKeyState;->digest:Ljava/lang/String;

    .line 336
    iput-object p0, v0, Lee/cyber/smartid/tse/dto/ProtoKeyState;->digestAlgorithm:Ljava/lang/String;

    .line 337
    iput-object p3, v0, Lee/cyber/smartid/tse/dto/ProtoKeyState;->keyType:Ljava/lang/String;

    .line 338
    iput-object p5, v0, Lee/cyber/smartid/tse/dto/ProtoKeyState;->clientShareSecondPart:Ljava/lang/String;

    .line 339
    iput-object p6, v0, Lee/cyber/smartid/tse/dto/ProtoKeyState;->clientModulus:Ljava/lang/String;

    const/4 p0, 0x3

    .line 340
    iput p0, v0, Lee/cyber/smartid/tse/dto/ProtoKeyState;->formatVersion:I

    return-object v0
.end method

.method public static retryFrom(Lee/cyber/smartid/tse/dto/KeyState;)Lee/cyber/smartid/tse/dto/ProtoKeyState;
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    if-eqz p0, :cond_3

    .line 185
    invoke-virtual {p0}, Lee/cyber/smartid/tse/dto/KeyState;->isActiveCloneDetection()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 186
    iget-object v1, p0, Lee/cyber/smartid/tse/dto/KeyState;->id:Ljava/lang/String;

    iget-object v2, p0, Lee/cyber/smartid/tse/dto/KeyState;->keyId:Ljava/lang/String;

    iget-object v3, p0, Lee/cyber/smartid/tse/dto/KeyState;->accountUUID:Ljava/lang/String;

    invoke-virtual {p0}, Lee/cyber/smartid/tse/dto/KeyState;->getTransactionUUID()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lee/cyber/smartid/tse/dto/KeyState;->keyType:Ljava/lang/String;

    const/4 v6, 0x3

    invoke-static/range {v1 .. v6}, Lee/cyber/smartid/tse/dto/ProtoKeyState;->forRetry(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)Lee/cyber/smartid/tse/dto/ProtoKeyState;

    move-result-object p0

    return-object p0

    .line 187
    :cond_0
    invoke-virtual {p0}, Lee/cyber/smartid/tse/dto/KeyState;->isActiveSubmitClientSecondPart()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 188
    iget-object v1, p0, Lee/cyber/smartid/tse/dto/KeyState;->id:Ljava/lang/String;

    iget-object v2, p0, Lee/cyber/smartid/tse/dto/KeyState;->keyId:Ljava/lang/String;

    iget-object v3, p0, Lee/cyber/smartid/tse/dto/KeyState;->accountUUID:Ljava/lang/String;

    invoke-virtual {p0}, Lee/cyber/smartid/tse/dto/KeyState;->getTransactionUUID()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lee/cyber/smartid/tse/dto/KeyState;->keyType:Ljava/lang/String;

    const/4 v6, 0x4

    invoke-static/range {v1 .. v6}, Lee/cyber/smartid/tse/dto/ProtoKeyState;->forRetry(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)Lee/cyber/smartid/tse/dto/ProtoKeyState;

    move-result-object p0

    return-object p0

    .line 189
    :cond_1
    invoke-virtual {p0}, Lee/cyber/smartid/tse/dto/KeyState;->isActiveSign()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 190
    iget-object v1, p0, Lee/cyber/smartid/tse/dto/KeyState;->id:Ljava/lang/String;

    iget-object v2, p0, Lee/cyber/smartid/tse/dto/KeyState;->keyId:Ljava/lang/String;

    iget-object v3, p0, Lee/cyber/smartid/tse/dto/KeyState;->accountUUID:Ljava/lang/String;

    invoke-virtual {p0}, Lee/cyber/smartid/tse/dto/KeyState;->getTransactionUUID()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lee/cyber/smartid/tse/dto/KeyState;->keyType:Ljava/lang/String;

    const/4 v6, 0x1

    invoke-static/range {v1 .. v6}, Lee/cyber/smartid/tse/dto/ProtoKeyState;->forRetry(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)Lee/cyber/smartid/tse/dto/ProtoKeyState;

    move-result-object p0

    return-object p0

    .line 192
    :cond_2
    new-instance p0, Ljava/lang/IllegalArgumentException;

    const-string v0, "Invalid keyState, unable to convert!"

    invoke-direct {p0, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p0

    .line 183
    :cond_3
    new-instance p0, Ljava/lang/IllegalArgumentException;

    const-string v0, "The keyState param can\'t be null!"

    invoke-direct {p0, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p0, p1, :cond_0

    return v0

    .line 569
    :cond_0
    instance-of v1, p1, Lee/cyber/smartid/tse/dto/ProtoKeyState;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 574
    :cond_1
    check-cast p1, Lee/cyber/smartid/tse/dto/ProtoKeyState;

    .line 576
    iget v1, p0, Lee/cyber/smartid/tse/dto/ProtoKeyState;->type:I

    iget v3, p1, Lee/cyber/smartid/tse/dto/ProtoKeyState;->type:I

    if-eq v1, v3, :cond_2

    return v2

    .line 579
    :cond_2
    iget v1, p0, Lee/cyber/smartid/tse/dto/ProtoKeyState;->operationOrigin:I

    iget v3, p1, Lee/cyber/smartid/tse/dto/ProtoKeyState;->operationOrigin:I

    if-eq v1, v3, :cond_3

    return v2

    .line 582
    :cond_3
    iget-object v1, p0, Lee/cyber/smartid/tse/dto/ProtoKeyState;->id:Ljava/lang/String;

    iget-object v3, p1, Lee/cyber/smartid/tse/dto/ProtoKeyState;->id:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    return v2

    .line 585
    :cond_4
    iget-object v1, p0, Lee/cyber/smartid/tse/dto/ProtoKeyState;->keyId:Ljava/lang/String;

    iget-object v3, p1, Lee/cyber/smartid/tse/dto/ProtoKeyState;->keyId:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    return v2

    .line 588
    :cond_5
    iget-object v1, p0, Lee/cyber/smartid/tse/dto/ProtoKeyState;->nonce:Ljava/lang/String;

    if-eqz v1, :cond_6

    iget-object v3, p1, Lee/cyber/smartid/tse/dto/ProtoKeyState;->nonce:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    goto :goto_0

    :cond_6
    iget-object v1, p1, Lee/cyber/smartid/tse/dto/ProtoKeyState;->nonce:Ljava/lang/String;

    if-eqz v1, :cond_7

    :goto_0
    return v2

    .line 592
    :cond_7
    iget-object v1, p0, Lee/cyber/smartid/tse/dto/ProtoKeyState;->accountUUID:Ljava/lang/String;

    if-eqz v1, :cond_8

    iget-object v3, p1, Lee/cyber/smartid/tse/dto/ProtoKeyState;->accountUUID:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_9

    goto :goto_1

    :cond_8
    iget-object v1, p1, Lee/cyber/smartid/tse/dto/ProtoKeyState;->accountUUID:Ljava/lang/String;

    if-eqz v1, :cond_9

    :goto_1
    return v2

    .line 595
    :cond_9
    iget-object v1, p0, Lee/cyber/smartid/tse/dto/ProtoKeyState;->transactionUUID:Ljava/lang/String;

    if-eqz v1, :cond_a

    iget-object v3, p1, Lee/cyber/smartid/tse/dto/ProtoKeyState;->transactionUUID:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_b

    goto :goto_2

    :cond_a
    iget-object v1, p1, Lee/cyber/smartid/tse/dto/ProtoKeyState;->transactionUUID:Ljava/lang/String;

    if-eqz v1, :cond_b

    :goto_2
    return v2

    .line 598
    :cond_b
    iget-object v1, p0, Lee/cyber/smartid/tse/dto/ProtoKeyState;->digest:Ljava/lang/String;

    if-eqz v1, :cond_c

    iget-object v3, p1, Lee/cyber/smartid/tse/dto/ProtoKeyState;->digest:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_d

    goto :goto_3

    :cond_c
    iget-object v1, p1, Lee/cyber/smartid/tse/dto/ProtoKeyState;->digest:Ljava/lang/String;

    if-eqz v1, :cond_d

    :goto_3
    return v2

    .line 601
    :cond_d
    iget-object v1, p0, Lee/cyber/smartid/tse/dto/ProtoKeyState;->digestAlgorithm:Ljava/lang/String;

    if-eqz v1, :cond_e

    iget-object v3, p1, Lee/cyber/smartid/tse/dto/ProtoKeyState;->digestAlgorithm:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_f

    goto :goto_4

    :cond_e
    iget-object v1, p1, Lee/cyber/smartid/tse/dto/ProtoKeyState;->digestAlgorithm:Ljava/lang/String;

    if-eqz v1, :cond_f

    :goto_4
    return v2

    .line 604
    :cond_f
    iget-object v1, p0, Lee/cyber/smartid/tse/dto/ProtoKeyState;->signatureShare:Ljava/lang/String;

    if-eqz v1, :cond_10

    iget-object v3, p1, Lee/cyber/smartid/tse/dto/ProtoKeyState;->signatureShare:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_11

    goto :goto_5

    :cond_10
    iget-object v1, p1, Lee/cyber/smartid/tse/dto/ProtoKeyState;->signatureShare:Ljava/lang/String;

    if-eqz v1, :cond_11

    :goto_5
    return v2

    .line 607
    :cond_11
    iget-object v1, p0, Lee/cyber/smartid/tse/dto/ProtoKeyState;->clientShareSecondPart:Ljava/lang/String;

    if-eqz v1, :cond_12

    iget-object v3, p1, Lee/cyber/smartid/tse/dto/ProtoKeyState;->clientShareSecondPart:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_13

    goto :goto_6

    :cond_12
    iget-object v1, p1, Lee/cyber/smartid/tse/dto/ProtoKeyState;->clientShareSecondPart:Ljava/lang/String;

    if-eqz v1, :cond_13

    :goto_6
    return v2

    .line 611
    :cond_13
    iget-object v1, p0, Lee/cyber/smartid/tse/dto/ProtoKeyState;->clientModulus:Ljava/lang/String;

    if-eqz v1, :cond_14

    iget-object v3, p1, Lee/cyber/smartid/tse/dto/ProtoKeyState;->clientModulus:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_15

    goto :goto_7

    :cond_14
    iget-object v1, p1, Lee/cyber/smartid/tse/dto/ProtoKeyState;->clientModulus:Ljava/lang/String;

    if-eqz v1, :cond_15

    :goto_7
    return v2

    .line 614
    :cond_15
    iget-object v1, p0, Lee/cyber/smartid/tse/dto/ProtoKeyState;->keyType:Ljava/lang/String;

    if-eqz v1, :cond_16

    iget-object p1, p1, Lee/cyber/smartid/tse/dto/ProtoKeyState;->keyType:Ljava/lang/String;

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_8

    :cond_16
    iget-object p1, p1, Lee/cyber/smartid/tse/dto/ProtoKeyState;->keyType:Ljava/lang/String;

    if-nez p1, :cond_17

    goto :goto_8

    :cond_17
    const/4 v0, 0x0

    :goto_8
    return v0
.end method

.method public getAccountUUID()Ljava/lang/String;
    .locals 1

    .line 371
    iget-object v0, p0, Lee/cyber/smartid/tse/dto/ProtoKeyState;->accountUUID:Ljava/lang/String;

    return-object v0
.end method

.method public getClientModulus()Ljava/lang/String;
    .locals 1

    .line 460
    iget-object v0, p0, Lee/cyber/smartid/tse/dto/ProtoKeyState;->clientModulus:Ljava/lang/String;

    return-object v0
.end method

.method public getClientShareSecondPart()Ljava/lang/String;
    .locals 1

    .line 470
    iget-object v0, p0, Lee/cyber/smartid/tse/dto/ProtoKeyState;->clientShareSecondPart:Ljava/lang/String;

    return-object v0
.end method

.method public getDigest()Ljava/lang/String;
    .locals 1

    .line 481
    iget-object v0, p0, Lee/cyber/smartid/tse/dto/ProtoKeyState;->digest:Ljava/lang/String;

    return-object v0
.end method

.method public getDigestAlgorithm()Ljava/lang/String;
    .locals 1

    .line 491
    iget-object v0, p0, Lee/cyber/smartid/tse/dto/ProtoKeyState;->digestAlgorithm:Ljava/lang/String;

    return-object v0
.end method

.method public getFormatVersion()I
    .locals 1

    .line 500
    iget v0, p0, Lee/cyber/smartid/tse/dto/ProtoKeyState;->formatVersion:I

    return v0
.end method

.method public getHumanReadableNameForOperationOrigin()Ljava/lang/String;
    .locals 2

    .line 442
    iget v0, p0, Lee/cyber/smartid/tse/dto/ProtoKeyState;->operationOrigin:I

    if-eqz v0, :cond_2

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    const-string v0, "OPERATION_ORIGIN_UNKNOWN"

    return-object v0

    :cond_0
    const-string v0, "OPERATION_ORIGIN_RETRY"

    return-object v0

    :cond_1
    const-string v0, "OPERATION_ORIGIN_NEW"

    return-object v0

    :cond_2
    const-string v0, "OPERATION_ORIGIN_NONE"

    return-object v0
.end method

.method public getHumanReadableNameForType()Ljava/lang/String;
    .locals 2

    .line 422
    iget v0, p0, Lee/cyber/smartid/tse/dto/ProtoKeyState;->type:I

    if-eqz v0, :cond_3

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-eq v0, v1, :cond_1

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    const-string v0, "TYPE_UNKNOWN"

    return-object v0

    :cond_0
    const-string v0, "TYPE_SUBMIT_CLIENT_SECOND_PART"

    return-object v0

    :cond_1
    const-string v0, "TYPE_CLONE_DETECTION"

    return-object v0

    :cond_2
    const-string v0, "TYPE_SIGN"

    return-object v0

    :cond_3
    const-string v0, "TYPE_IDLE"

    return-object v0
.end method

.method public getId()Ljava/lang/String;
    .locals 1

    .line 351
    iget-object v0, p0, Lee/cyber/smartid/tse/dto/ProtoKeyState;->id:Ljava/lang/String;

    return-object v0
.end method

.method public getKeyId()Ljava/lang/String;
    .locals 1

    .line 361
    iget-object v0, p0, Lee/cyber/smartid/tse/dto/ProtoKeyState;->keyId:Ljava/lang/String;

    return-object v0
.end method

.method public getKeyType()Ljava/lang/String;
    .locals 1

    .line 402
    iget-object v0, p0, Lee/cyber/smartid/tse/dto/ProtoKeyState;->keyType:Ljava/lang/String;

    return-object v0
.end method

.method public getNonce()Ljava/lang/String;
    .locals 1

    .line 648
    iget-object v0, p0, Lee/cyber/smartid/tse/dto/ProtoKeyState;->nonce:Ljava/lang/String;

    return-object v0
.end method

.method public getSignatureShare()Ljava/lang/String;
    .locals 1

    .line 391
    iget-object v0, p0, Lee/cyber/smartid/tse/dto/ProtoKeyState;->signatureShare:Ljava/lang/String;

    return-object v0
.end method

.method public getTransactionUUID()Ljava/lang/String;
    .locals 1

    .line 381
    iget-object v0, p0, Lee/cyber/smartid/tse/dto/ProtoKeyState;->transactionUUID:Ljava/lang/String;

    return-object v0
.end method

.method public getType()I
    .locals 1

    .line 413
    iget v0, p0, Lee/cyber/smartid/tse/dto/ProtoKeyState;->type:I

    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 624
    iget-object v0, p0, Lee/cyber/smartid/tse/dto/ProtoKeyState;->id:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x1f

    .line 625
    iget-object v1, p0, Lee/cyber/smartid/tse/dto/ProtoKeyState;->keyId:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    .line 626
    iget v1, p0, Lee/cyber/smartid/tse/dto/ProtoKeyState;->type:I

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    .line 627
    iget v1, p0, Lee/cyber/smartid/tse/dto/ProtoKeyState;->operationOrigin:I

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    .line 628
    iget-object v1, p0, Lee/cyber/smartid/tse/dto/ProtoKeyState;->nonce:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    .line 630
    iget-object v1, p0, Lee/cyber/smartid/tse/dto/ProtoKeyState;->accountUUID:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    .line 631
    iget-object v1, p0, Lee/cyber/smartid/tse/dto/ProtoKeyState;->transactionUUID:Ljava/lang/String;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    .line 632
    iget-object v1, p0, Lee/cyber/smartid/tse/dto/ProtoKeyState;->digest:Ljava/lang/String;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    .line 633
    iget-object v1, p0, Lee/cyber/smartid/tse/dto/ProtoKeyState;->digestAlgorithm:Ljava/lang/String;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    .line 634
    iget-object v1, p0, Lee/cyber/smartid/tse/dto/ProtoKeyState;->signatureShare:Ljava/lang/String;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    .line 635
    iget-object v1, p0, Lee/cyber/smartid/tse/dto/ProtoKeyState;->clientShareSecondPart:Ljava/lang/String;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_6

    :cond_6
    const/4 v1, 0x0

    :goto_6
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    .line 636
    iget-object v1, p0, Lee/cyber/smartid/tse/dto/ProtoKeyState;->clientModulus:Ljava/lang/String;

    if-eqz v1, :cond_7

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_7

    :cond_7
    const/4 v1, 0x0

    :goto_7
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    .line 637
    iget-object v1, p0, Lee/cyber/smartid/tse/dto/ProtoKeyState;->keyType:Ljava/lang/String;

    if-eqz v1, :cond_8

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    :cond_8
    add-int/2addr v0, v2

    return v0
.end method

.method public isActiveCloneDetection()Z
    .locals 2

    .line 527
    iget v0, p0, Lee/cyber/smartid/tse/dto/ProtoKeyState;->type:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isActiveSign()Z
    .locals 2

    .line 518
    iget v0, p0, Lee/cyber/smartid/tse/dto/ProtoKeyState;->type:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return v1
.end method

.method public isActiveSubmitClientSecondPart()Z
    .locals 2

    .line 537
    iget v0, p0, Lee/cyber/smartid/tse/dto/ProtoKeyState;->type:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isInActiveState()Z
    .locals 1

    .line 509
    invoke-virtual {p0}, Lee/cyber/smartid/tse/dto/ProtoKeyState;->isActiveSign()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lee/cyber/smartid/tse/dto/ProtoKeyState;->isActiveCloneDetection()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lee/cyber/smartid/tse/dto/ProtoKeyState;->isActiveSubmitClientSecondPart()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method public isRetry()Z
    .locals 2

    .line 555
    iget v0, p0, Lee/cyber/smartid/tse/dto/ProtoKeyState;->operationOrigin:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public setSignatureShare(Ljava/lang/String;)V
    .locals 0

    .line 546
    iput-object p1, p0, Lee/cyber/smartid/tse/dto/ProtoKeyState;->signatureShare:Ljava/lang/String;

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 653
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ProtoKeyState{id=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lee/cyber/smartid/tse/dto/ProtoKeyState;->id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v2, ", keyId=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lee/cyber/smartid/tse/dto/ProtoKeyState;->keyId:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v2, ", type="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v2, p0, Lee/cyber/smartid/tse/dto/ProtoKeyState;->type:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v2, "("

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 656
    invoke-virtual {p0}, Lee/cyber/smartid/tse/dto/ProtoKeyState;->getHumanReadableNameForType()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, "), operationOrigin="

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v3, p0, Lee/cyber/smartid/tse/dto/ProtoKeyState;->operationOrigin:I

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 657
    invoke-virtual {p0}, Lee/cyber/smartid/tse/dto/ProtoKeyState;->getHumanReadableNameForOperationOrigin()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "), accountUUID=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lee/cyber/smartid/tse/dto/ProtoKeyState;->accountUUID:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v2, ", transactionUUID=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lee/cyber/smartid/tse/dto/ProtoKeyState;->transactionUUID:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v2, ", keyType=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lee/cyber/smartid/tse/dto/ProtoKeyState;->keyType:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v1, ", formatVersion="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lee/cyber/smartid/tse/dto/ProtoKeyState;->formatVersion:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
