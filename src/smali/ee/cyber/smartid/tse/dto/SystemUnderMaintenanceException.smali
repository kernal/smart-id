.class public Lee/cyber/smartid/tse/dto/SystemUnderMaintenanceException;
.super Ljava/io/IOException;
.source "SystemUnderMaintenanceException.java"


# static fields
.field private static final serialVersionUID:J = -0x747a268e486f7839L


# direct methods
.method public constructor <init>()V
    .locals 1

    const-string v0, "System is under maintenance, retry later"

    .line 21
    invoke-direct {p0, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 0

    .line 30
    invoke-direct {p0, p1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    return-void
.end method
