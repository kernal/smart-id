.class public Lee/cyber/smartid/tse/dto/InitTSEException;
.super Ljava/io/IOException;
.source "InitTSEException.java"


# static fields
.field private static final serialVersionUID:J = -0x205df51f68081a20L


# instance fields
.field private errorCode:J


# direct methods
.method public constructor <init>(JLjava/lang/String;)V
    .locals 0

    .line 30
    invoke-direct {p0, p3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    .line 31
    iput-wide p1, p0, Lee/cyber/smartid/tse/dto/InitTSEException;->errorCode:J

    return-void
.end method


# virtual methods
.method public getErrorCode()J
    .locals 2

    .line 39
    iget-wide v0, p0, Lee/cyber/smartid/tse/dto/InitTSEException;->errorCode:J

    return-wide v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .line 49
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "InitTSEException{errorCode="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lee/cyber/smartid/tse/dto/InitTSEException;->errorCode:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, "} "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 51
    invoke-super {p0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
