.class public abstract Lee/cyber/smartid/tse/dto/PinValidationRule;
.super Ljava/lang/Object;
.source "PinValidationRule.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = -0x6e15c2896be8d6f3L


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public createErrorFor(Landroid/content/Context;Lee/cyber/smartid/tse/dto/PinInfo;)Lee/cyber/smartid/tse/dto/PinValidationError;
    .locals 4

    if-eqz p1, :cond_1

    if-nez p2, :cond_0

    goto :goto_0

    .line 42
    :cond_0
    new-instance v0, Lee/cyber/smartid/tse/dto/PinValidationError;

    invoke-virtual {p2}, Lee/cyber/smartid/tse/dto/PinInfo;->getReference()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2}, Lee/cyber/smartid/tse/dto/PinInfo;->getPin()Ljava/lang/String;

    move-result-object p2

    const/4 v2, 0x1

    sget v3, Lee/cyber/smartid/tse/R$string;->err_forbidden_pin_pattern:I

    invoke-virtual {p1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, v1, p2, v2, p1}, Lee/cyber/smartid/tse/dto/PinValidationError;-><init>(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V

    return-object v0

    :cond_1
    :goto_0
    const/4 p1, 0x0

    return-object p1
.end method

.method public abstract isValid(Ljava/lang/String;)Z
.end method
