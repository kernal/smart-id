.class public Lee/cyber/smartid/tse/dto/TSEError;
.super Lee/cyber/smartid/tse/dto/BaseError;
.source "TSEError.java"


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lee/cyber/smartid/tse/dto/TSEError;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 293
    new-instance v0, Lee/cyber/smartid/tse/dto/TSEError$1;

    invoke-direct {v0}, Lee/cyber/smartid/tse/dto/TSEError$1;-><init>()V

    sput-object v0, Lee/cyber/smartid/tse/dto/TSEError;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method protected constructor <init>(Landroid/os/Parcel;)V
    .locals 0

    .line 65
    invoke-direct {p0, p1}, Lee/cyber/smartid/tse/dto/BaseError;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method private constructor <init>(Lee/cyber/smartid/tse/inter/WallClock;JLjava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 10

    if-eqz p1, :cond_0

    .line 56
    invoke-interface {p1}, Lee/cyber/smartid/tse/inter/WallClock;->currentTimeMillis()J

    move-result-wide v0

    goto :goto_0

    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    :goto_0
    move-wide v8, v0

    move-object v2, p0

    move-wide v3, p2

    move-object v5, p4

    move-object/from16 v6, p6

    move-object v7, p5

    invoke-direct/range {v2 .. v9}, Lee/cyber/smartid/tse/dto/BaseError;-><init>(JLjava/lang/String;Landroid/os/Bundle;Ljava/lang/String;J)V

    return-void
.end method

.method public static from(Landroid/content/Context;Lee/cyber/smartid/tse/inter/WallClock;Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCError;Ljava/lang/Throwable;)Lee/cyber/smartid/tse/dto/TSEError;
    .locals 7

    if-eqz p2, :cond_0

    .line 122
    invoke-static {p1, p2}, Lee/cyber/smartid/tse/dto/TSEError;->from(Lee/cyber/smartid/tse/inter/WallClock;Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCError;)Lee/cyber/smartid/tse/dto/TSEError;

    move-result-object p0

    return-object p0

    :cond_0
    if-eqz p3, :cond_1

    .line 124
    invoke-static {p0, p1, p3}, Lee/cyber/smartid/tse/dto/TSEError;->from(Landroid/content/Context;Lee/cyber/smartid/tse/inter/WallClock;Ljava/lang/Throwable;)Lee/cyber/smartid/tse/dto/TSEError;

    move-result-object p0

    return-object p0

    .line 126
    :cond_1
    new-instance p0, Lee/cyber/smartid/tse/dto/TSEError;

    const-wide/16 v2, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v6}, Lee/cyber/smartid/tse/dto/TSEError;-><init>(Lee/cyber/smartid/tse/inter/WallClock;JLjava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V

    return-object p0
.end method

.method public static from(Landroid/content/Context;Lee/cyber/smartid/tse/inter/WallClock;Ljava/lang/Throwable;)Lee/cyber/smartid/tse/dto/TSEError;
    .locals 8

    .line 139
    new-instance v7, Lee/cyber/smartid/tse/dto/TSEError;

    const-wide/16 v2, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object v0, v7

    move-object v1, p1

    invoke-direct/range {v0 .. v6}, Lee/cyber/smartid/tse/dto/TSEError;-><init>(Lee/cyber/smartid/tse/inter/WallClock;JLjava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 140
    instance-of p1, p2, Lee/cyber/smartid/tse/dto/AuthorizationRequiredException;

    if-eqz p1, :cond_0

    const-wide/16 v0, 0x3f7

    .line 141
    iput-wide v0, v7, Lee/cyber/smartid/tse/dto/TSEError;->code:J

    .line 142
    sget p1, Lee/cyber/smartid/tse/R$string;->err_invalid_or_missing_authorization:I

    invoke-virtual {p0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p0

    iput-object p0, v7, Lee/cyber/smartid/tse/dto/TSEError;->message:Ljava/lang/String;

    goto/16 :goto_3

    .line 143
    :cond_0
    instance-of p1, p2, Lee/cyber/smartid/tse/dto/ClientTooOldException;

    if-eqz p1, :cond_1

    const-wide/16 v0, 0x44c

    .line 144
    iput-wide v0, v7, Lee/cyber/smartid/tse/dto/TSEError;->code:J

    .line 145
    sget p1, Lee/cyber/smartid/tse/R$string;->err_client_too_old:I

    invoke-virtual {p0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p0

    iput-object p0, v7, Lee/cyber/smartid/tse/dto/TSEError;->message:Ljava/lang/String;

    goto/16 :goto_3

    .line 146
    :cond_1
    instance-of p1, p2, Lee/cyber/smartid/tse/dto/SystemUnderMaintenanceException;

    if-eqz p1, :cond_2

    const-wide/16 v0, 0x44d

    .line 147
    iput-wide v0, v7, Lee/cyber/smartid/tse/dto/TSEError;->code:J

    .line 148
    sget p1, Lee/cyber/smartid/tse/R$string;->err_system_under_maintenance:I

    invoke-virtual {p0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p0

    iput-object p0, v7, Lee/cyber/smartid/tse/dto/TSEError;->message:Ljava/lang/String;

    goto/16 :goto_3

    .line 149
    :cond_2
    instance-of p1, p2, Ljavax/net/ssl/SSLException;

    if-nez p1, :cond_15

    instance-of p1, p2, Ljava/net/UnknownServiceException;

    if-eqz p1, :cond_3

    goto/16 :goto_2

    .line 152
    :cond_3
    instance-of p1, p2, Ljava/net/UnknownHostException;

    if-eqz p1, :cond_4

    const-wide/16 v0, 0x40e

    .line 153
    iput-wide v0, v7, Lee/cyber/smartid/tse/dto/TSEError;->code:J

    .line 154
    sget p1, Lee/cyber/smartid/tse/R$string;->err_failed_to_resolve_the_hostname_exception:I

    invoke-virtual {p0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p0

    iput-object p0, v7, Lee/cyber/smartid/tse/dto/TSEError;->message:Ljava/lang/String;

    goto/16 :goto_3

    .line 155
    :cond_4
    instance-of p1, p2, Ljavax/security/cert/CertificateException;

    if-eqz p1, :cond_5

    const-wide/16 v0, 0x40f

    .line 156
    iput-wide v0, v7, Lee/cyber/smartid/tse/dto/TSEError;->code:J

    .line 157
    sget p1, Lee/cyber/smartid/tse/R$string;->err_certificate_exception:I

    invoke-virtual {p0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p0

    iput-object p0, v7, Lee/cyber/smartid/tse/dto/TSEError;->message:Ljava/lang/String;

    goto/16 :goto_3

    .line 158
    :cond_5
    instance-of p1, p2, Ljava/net/ProtocolException;

    if-eqz p1, :cond_6

    const-wide/16 v0, 0x410

    .line 159
    iput-wide v0, v7, Lee/cyber/smartid/tse/dto/TSEError;->code:J

    .line 160
    sget p1, Lee/cyber/smartid/tse/R$string;->err_protocol_exception:I

    invoke-virtual {p0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p0

    iput-object p0, v7, Lee/cyber/smartid/tse/dto/TSEError;->message:Ljava/lang/String;

    goto/16 :goto_3

    .line 161
    :cond_6
    instance-of p1, p2, Ljava/net/MalformedURLException;

    if-eqz p1, :cond_7

    const-wide/16 v0, 0x411

    .line 162
    iput-wide v0, v7, Lee/cyber/smartid/tse/dto/TSEError;->code:J

    .line 163
    sget p1, Lee/cyber/smartid/tse/R$string;->err_malformed_url_exception:I

    invoke-virtual {p0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p0

    iput-object p0, v7, Lee/cyber/smartid/tse/dto/TSEError;->message:Ljava/lang/String;

    goto/16 :goto_3

    .line 164
    :cond_7
    instance-of p1, p2, Ljava/security/GeneralSecurityException;

    if-eqz p1, :cond_8

    const-wide/16 v0, 0x412

    .line 165
    iput-wide v0, v7, Lee/cyber/smartid/tse/dto/TSEError;->code:J

    .line 166
    sget p1, Lee/cyber/smartid/tse/R$string;->err_general_security_exception:I

    invoke-virtual {p0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p0

    iput-object p0, v7, Lee/cyber/smartid/tse/dto/TSEError;->message:Ljava/lang/String;

    goto/16 :goto_3

    .line 167
    :cond_8
    instance-of p1, p2, Lee/cyber/smartid/tse/dto/InvalidResponseIdException;

    if-eqz p1, :cond_9

    const-wide/16 v0, 0x3eb

    .line 168
    iput-wide v0, v7, Lee/cyber/smartid/tse/dto/TSEError;->code:J

    .line 169
    sget p1, Lee/cyber/smartid/tse/R$string;->err_server_response_id_invalid:I

    invoke-virtual {p0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p0

    iput-object p0, v7, Lee/cyber/smartid/tse/dto/TSEError;->message:Ljava/lang/String;

    goto/16 :goto_3

    .line 170
    :cond_9
    instance-of p1, p2, Lee/cyber/smartid/tse/dto/InvalidResponseResultException;

    if-eqz p1, :cond_a

    const-wide/16 v0, 0x3ec

    .line 171
    iput-wide v0, v7, Lee/cyber/smartid/tse/dto/TSEError;->code:J

    .line 172
    sget p1, Lee/cyber/smartid/tse/R$string;->err_server_response_invalid:I

    invoke-virtual {p0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p0

    iput-object p0, v7, Lee/cyber/smartid/tse/dto/TSEError;->message:Ljava/lang/String;

    goto/16 :goto_3

    .line 173
    :cond_a
    instance-of p1, p2, Ljava/net/SocketTimeoutException;

    if-eqz p1, :cond_b

    const-wide/16 v0, 0x3e9

    .line 174
    iput-wide v0, v7, Lee/cyber/smartid/tse/dto/TSEError;->code:J

    .line 175
    sget p1, Lee/cyber/smartid/tse/R$string;->err_server_response_timeout:I

    invoke-virtual {p0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p0

    iput-object p0, v7, Lee/cyber/smartid/tse/dto/TSEError;->message:Ljava/lang/String;

    goto/16 :goto_3

    .line 176
    :cond_b
    instance-of p1, p2, Lee/cyber/smartid/tse/dto/HttpErrorCodeResponseException;

    const/4 v0, 0x1

    if-eqz p1, :cond_c

    .line 177
    move-object p1, p2

    check-cast p1, Lee/cyber/smartid/tse/dto/HttpErrorCodeResponseException;

    invoke-virtual {p1}, Lee/cyber/smartid/tse/dto/HttpErrorCodeResponseException;->getServerResponseCode()I

    move-result p1

    const-wide/16 v1, 0x40b

    .line 178
    iput-wide v1, v7, Lee/cyber/smartid/tse/dto/TSEError;->code:J

    .line 179
    sget v1, Lee/cyber/smartid/tse/R$string;->err_server_response_non_200_OK_X:I

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v0, v2

    invoke-virtual {p0, v1, v0}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    iput-object p0, v7, Lee/cyber/smartid/tse/dto/TSEError;->message:Ljava/lang/String;

    .line 180
    invoke-virtual {v7}, Lee/cyber/smartid/tse/dto/TSEError;->createOrGetExtras()Landroid/os/Bundle;

    move-result-object p0

    const-string v0, "ee.cyber.smartid.EXTRA_SERVER_HTTP_RESPONSE_CODE"

    invoke-virtual {p0, v0, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    goto/16 :goto_3

    .line 181
    :cond_c
    instance-of p1, p2, Lee/cyber/smartid/cryptolib/dto/StorageException;

    if-eqz p1, :cond_d

    move-object v1, p2

    check-cast v1, Lee/cyber/smartid/cryptolib/dto/StorageException;

    invoke-virtual {v1}, Lee/cyber/smartid/cryptolib/dto/StorageException;->getErrorCode()I

    move-result v1

    if-ne v1, v0, :cond_d

    const-wide/16 v0, 0x404

    .line 182
    iput-wide v0, v7, Lee/cyber/smartid/tse/dto/TSEError;->code:J

    .line 183
    sget p1, Lee/cyber/smartid/tse/R$string;->err_failed_to_write_to_storage:I

    invoke-virtual {p0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p0

    iput-object p0, v7, Lee/cyber/smartid/tse/dto/TSEError;->message:Ljava/lang/String;

    goto/16 :goto_3

    :cond_d
    if-eqz p1, :cond_e

    .line 184
    move-object p1, p2

    check-cast p1, Lee/cyber/smartid/cryptolib/dto/StorageException;

    invoke-virtual {p1}, Lee/cyber/smartid/cryptolib/dto/StorageException;->getErrorCode()I

    move-result p1

    const/4 v0, 0x2

    if-ne p1, v0, :cond_e

    const-wide/16 v0, 0x405

    .line 185
    iput-wide v0, v7, Lee/cyber/smartid/tse/dto/TSEError;->code:J

    .line 186
    sget p1, Lee/cyber/smartid/tse/R$string;->err_failed_to_read_from_storage:I

    invoke-virtual {p0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p0

    iput-object p0, v7, Lee/cyber/smartid/tse/dto/TSEError;->message:Ljava/lang/String;

    goto/16 :goto_3

    .line 187
    :cond_e
    instance-of p1, p2, Lee/cyber/smartid/tse/dto/PRNGTestsFailedException;

    if-eqz p1, :cond_f

    .line 188
    move-object p0, p2

    check-cast p0, Lee/cyber/smartid/tse/dto/PRNGTestsFailedException;

    invoke-virtual {p0}, Lee/cyber/smartid/tse/dto/PRNGTestsFailedException;->getCode()J

    move-result-wide v0

    iput-wide v0, v7, Lee/cyber/smartid/tse/dto/TSEError;->code:J

    .line 189
    invoke-virtual {p2}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object p1

    iput-object p1, v7, Lee/cyber/smartid/tse/dto/TSEError;->message:Ljava/lang/String;

    .line 190
    invoke-virtual {v7}, Lee/cyber/smartid/tse/dto/TSEError;->createOrGetExtras()Landroid/os/Bundle;

    move-result-object p1

    invoke-virtual {p0}, Lee/cyber/smartid/tse/dto/PRNGTestsFailedException;->getTestResults()[Lee/cyber/smartid/cryptolib/dto/TestResult;

    move-result-object p0

    const-string v0, "ee.cyber.smartid.EXTRA_KEY_PRNG_TEST_RESULTS"

    invoke-virtual {p1, v0, p0}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    goto :goto_3

    .line 191
    :cond_f
    instance-of p1, p2, Ljava/io/IOException;

    if-eqz p1, :cond_10

    const-wide/16 v0, 0x3e8

    .line 192
    iput-wide v0, v7, Lee/cyber/smartid/tse/dto/TSEError;->code:J

    .line 193
    sget p1, Lee/cyber/smartid/tse/R$string;->err_network_connection:I

    invoke-virtual {p0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p0

    iput-object p0, v7, Lee/cyber/smartid/tse/dto/TSEError;->message:Ljava/lang/String;

    goto :goto_3

    .line 194
    :cond_10
    instance-of p1, p2, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;

    if-eqz p1, :cond_13

    .line 195
    move-object p0, p2

    check-cast p0, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;

    invoke-virtual {p0}, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;->getErrorCode()I

    move-result p1

    const/16 v0, 0x73

    if-ne p1, v0, :cond_11

    const-wide/16 p0, 0x40d

    .line 196
    iput-wide p0, v7, Lee/cyber/smartid/tse/dto/TSEError;->code:J

    goto :goto_0

    .line 197
    :cond_11
    invoke-virtual {p0}, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;->getErrorCode()I

    move-result p1

    const/16 v0, 0x68

    if-ne p1, v0, :cond_12

    const-wide/16 p0, 0x3fa

    .line 198
    iput-wide p0, v7, Lee/cyber/smartid/tse/dto/TSEError;->code:J

    goto :goto_0

    .line 200
    :cond_12
    invoke-virtual {p0}, Lee/cyber/smartid/cryptolib/dto/CryptoRuntimeException;->getErrorCode()I

    move-result p0

    int-to-long p0, p0

    iput-wide p0, v7, Lee/cyber/smartid/tse/dto/TSEError;->code:J

    .line 202
    :goto_0
    invoke-virtual {p2}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object p0

    iput-object p0, v7, Lee/cyber/smartid/tse/dto/TSEError;->message:Ljava/lang/String;

    goto :goto_3

    :cond_13
    const-wide/16 v0, 0x0

    .line 204
    iput-wide v0, v7, Lee/cyber/smartid/tse/dto/TSEError;->code:J

    if-eqz p2, :cond_14

    .line 205
    invoke-virtual {p2}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p1

    if-nez p1, :cond_14

    invoke-virtual {p2}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object p0

    goto :goto_1

    :cond_14
    sget p1, Lee/cyber/smartid/tse/R$string;->err_unknown:I

    invoke-virtual {p0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p0

    :goto_1
    iput-object p0, v7, Lee/cyber/smartid/tse/dto/TSEError;->message:Ljava/lang/String;

    goto :goto_3

    :cond_15
    :goto_2
    const-wide/16 v0, 0x3f9

    .line 150
    iput-wide v0, v7, Lee/cyber/smartid/tse/dto/TSEError;->code:J

    .line 151
    sget p1, Lee/cyber/smartid/tse/R$string;->err_ssl_exception:I

    invoke-virtual {p0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p0

    iput-object p0, v7, Lee/cyber/smartid/tse/dto/TSEError;->message:Ljava/lang/String;

    .line 209
    :goto_3
    invoke-static {p2}, Lee/cyber/smartid/tse/util/Util;->toString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object p0

    iput-object p0, v7, Lee/cyber/smartid/tse/dto/TSEError;->trace:Ljava/lang/String;

    return-object v7
.end method

.method public static from(Lee/cyber/smartid/tse/inter/WallClock;JLjava/lang/String;)Lee/cyber/smartid/tse/dto/TSEError;
    .locals 8

    .line 78
    new-instance v7, Lee/cyber/smartid/tse/dto/TSEError;

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object v0, v7

    move-object v1, p0

    move-wide v2, p1

    move-object v4, p3

    invoke-direct/range {v0 .. v6}, Lee/cyber/smartid/tse/dto/TSEError;-><init>(Lee/cyber/smartid/tse/inter/WallClock;JLjava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V

    return-object v7
.end method

.method public static from(Lee/cyber/smartid/tse/inter/WallClock;JLjava/lang/String;Ljava/io/Serializable;Ljava/lang/String;)Lee/cyber/smartid/tse/dto/TSEError;
    .locals 8

    .line 247
    new-instance v7, Lee/cyber/smartid/tse/dto/TSEError;

    const-wide/16 v2, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object v0, v7

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Lee/cyber/smartid/tse/dto/TSEError;-><init>(Lee/cyber/smartid/tse/inter/WallClock;JLjava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 248
    iput-wide p1, v7, Lee/cyber/smartid/tse/dto/TSEError;->code:J

    .line 249
    iput-object p3, v7, Lee/cyber/smartid/tse/dto/TSEError;->message:Ljava/lang/String;

    .line 250
    invoke-static {p5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p0

    if-nez p0, :cond_0

    if-eqz p4, :cond_0

    .line 251
    invoke-virtual {v7}, Lee/cyber/smartid/tse/dto/TSEError;->createOrGetExtras()Landroid/os/Bundle;

    move-result-object p0

    invoke-virtual {p0, p5, p4}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    :cond_0
    return-object v7
.end method

.method public static from(Lee/cyber/smartid/tse/inter/WallClock;JLjava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Lee/cyber/smartid/tse/dto/TSEError;
    .locals 8

    .line 107
    new-instance v7, Lee/cyber/smartid/tse/dto/TSEError;

    move-object v0, v7

    move-object v1, p0

    move-wide v2, p1

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lee/cyber/smartid/tse/dto/TSEError;-><init>(Lee/cyber/smartid/tse/inter/WallClock;JLjava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V

    return-object v7
.end method

.method public static from(Lee/cyber/smartid/tse/inter/WallClock;JLjava/lang/String;Ljava/lang/Throwable;)Lee/cyber/smartid/tse/dto/TSEError;
    .locals 8

    .line 92
    new-instance v7, Lee/cyber/smartid/tse/dto/TSEError;

    invoke-static {p4}, Lee/cyber/smartid/tse/util/Util;->toString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    move-object v0, v7

    move-object v1, p0

    move-wide v2, p1

    move-object v4, p3

    invoke-direct/range {v0 .. v6}, Lee/cyber/smartid/tse/dto/TSEError;-><init>(Lee/cyber/smartid/tse/inter/WallClock;JLjava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V

    return-object v7
.end method

.method public static from(Lee/cyber/smartid/tse/inter/WallClock;Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCError;)Lee/cyber/smartid/tse/dto/TSEError;
    .locals 8

    .line 223
    new-instance v7, Lee/cyber/smartid/tse/dto/TSEError;

    const-wide/16 v2, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object v0, v7

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Lee/cyber/smartid/tse/dto/TSEError;-><init>(Lee/cyber/smartid/tse/inter/WallClock;JLjava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 224
    invoke-virtual {p1}, Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCError;->getCode()J

    move-result-wide v0

    iput-wide v0, v7, Lee/cyber/smartid/tse/dto/TSEError;->code:J

    .line 225
    invoke-virtual {p1}, Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCError;->getMessage()Ljava/lang/String;

    move-result-object p0

    iput-object p0, v7, Lee/cyber/smartid/tse/dto/TSEError;->message:Ljava/lang/String;

    .line 226
    invoke-virtual {p1}, Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCError;->getData()Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCErrorData;

    move-result-object p0

    if-eqz p0, :cond_0

    invoke-virtual {p1}, Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCError;->getData()Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCErrorData;

    move-result-object p0

    invoke-virtual {p0}, Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCErrorData;->getKeyStatusInfo()Lee/cyber/smartid/tse/dto/jsonrpc/AccountKeyStatus;

    move-result-object p0

    if-eqz p0, :cond_0

    .line 227
    invoke-virtual {v7}, Lee/cyber/smartid/tse/dto/TSEError;->createOrGetExtras()Landroid/os/Bundle;

    move-result-object p0

    invoke-virtual {p1}, Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCError;->getData()Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCErrorData;

    move-result-object v0

    invoke-virtual {v0}, Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCErrorData;->getKeyStatusInfo()Lee/cyber/smartid/tse/dto/jsonrpc/AccountKeyStatus;

    move-result-object v0

    const-string v1, "ee.cyber.smartid.EXTRA_KEY_STATUS_INFO"

    invoke-virtual {p0, v1, v0}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 229
    :cond_0
    invoke-virtual {p1}, Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCError;->getData()Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCErrorData;

    move-result-object p0

    if-eqz p0, :cond_1

    invoke-virtual {p1}, Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCError;->getDataRaw()Ljava/lang/String;

    move-result-object p0

    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p0

    if-nez p0, :cond_1

    .line 230
    invoke-virtual {v7}, Lee/cyber/smartid/tse/dto/TSEError;->createOrGetExtras()Landroid/os/Bundle;

    move-result-object p0

    invoke-virtual {p1}, Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCError;->getDataRaw()Ljava/lang/String;

    move-result-object p1

    const-string v0, "ee.cyber.smartid.EXTRA_RAW_ERROR_DATA"

    invoke-virtual {p0, v0, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    return-object v7
.end method


# virtual methods
.method public asNonRetriable(Z)Lee/cyber/smartid/tse/dto/TSEError;
    .locals 2

    .line 264
    invoke-virtual {p0}, Lee/cyber/smartid/tse/dto/TSEError;->createOrGetExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "ee.cyber.smartid.EXTRA_IS_NON_RETRIABLE"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    return-object p0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 287
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "TSEError{} "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-super {p0}, Lee/cyber/smartid/tse/dto/BaseError;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public withKeyId(Ljava/lang/String;)Lee/cyber/smartid/tse/dto/TSEError;
    .locals 2

    .line 276
    invoke-virtual {p0}, Lee/cyber/smartid/tse/dto/TSEError;->createOrGetExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "ee.cyber.smartid.EXTRA_KEY_ID"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    return-object p0
.end method
