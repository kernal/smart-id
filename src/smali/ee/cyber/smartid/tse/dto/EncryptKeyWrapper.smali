.class public Lee/cyber/smartid/tse/dto/EncryptKeyWrapper;
.super Ljava/lang/Object;
.source "EncryptKeyWrapper.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final CURRENT_FORMAT_VERSION:I = 0x3

.field private static final serialVersionUID:J = -0x71a6333ae9cf5507L


# instance fields
.field private clientKeyEncoding:Ljava/lang/String;

.field private final creationTimestamp:J

.field private d1PrimePrimeEncoded:Ljava/lang/String;

.field private dhKeyPair:Lee/cyber/smartid/cryptolib/dto/DiffieHellmanKeyPair;

.field private final formatVersion:I

.field private key:Lee/cyber/smartid/tse/dto/Key;

.field private final keyReference:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lee/cyber/smartid/tse/inter/WallClock;Ljava/lang/String;)V
    .locals 0

    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    iput-object p2, p0, Lee/cyber/smartid/tse/dto/EncryptKeyWrapper;->keyReference:Ljava/lang/String;

    .line 44
    invoke-interface {p1}, Lee/cyber/smartid/tse/inter/WallClock;->currentTimeMillis()J

    move-result-wide p1

    iput-wide p1, p0, Lee/cyber/smartid/tse/dto/EncryptKeyWrapper;->creationTimestamp:J

    const/4 p1, 0x3

    .line 45
    iput p1, p0, Lee/cyber/smartid/tse/dto/EncryptKeyWrapper;->formatVersion:I

    return-void
.end method

.method public static getId(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .line 88
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "encrypt_key_wrapper_for_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public getClientKeyEncoding()Ljava/lang/String;
    .locals 1

    .line 107
    iget-object v0, p0, Lee/cyber/smartid/tse/dto/EncryptKeyWrapper;->clientKeyEncoding:Ljava/lang/String;

    return-object v0
.end method

.method public getD1PrimePrimeEncoded()Ljava/lang/String;
    .locals 1

    .line 137
    iget-object v0, p0, Lee/cyber/smartid/tse/dto/EncryptKeyWrapper;->d1PrimePrimeEncoded:Ljava/lang/String;

    return-object v0
.end method

.method public getDhKeyPair()Lee/cyber/smartid/cryptolib/dto/DiffieHellmanKeyPair;
    .locals 1

    .line 77
    iget-object v0, p0, Lee/cyber/smartid/tse/dto/EncryptKeyWrapper;->dhKeyPair:Lee/cyber/smartid/cryptolib/dto/DiffieHellmanKeyPair;

    return-object v0
.end method

.method public getId()Ljava/lang/String;
    .locals 1

    .line 98
    iget-object v0, p0, Lee/cyber/smartid/tse/dto/EncryptKeyWrapper;->keyReference:Ljava/lang/String;

    invoke-static {v0}, Lee/cyber/smartid/tse/dto/EncryptKeyWrapper;->getId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getKey()Lee/cyber/smartid/tse/dto/Key;
    .locals 1

    .line 116
    iget-object v0, p0, Lee/cyber/smartid/tse/dto/EncryptKeyWrapper;->key:Lee/cyber/smartid/tse/dto/Key;

    return-object v0
.end method

.method public getN1()Ljava/lang/String;
    .locals 1

    .line 125
    iget-object v0, p0, Lee/cyber/smartid/tse/dto/EncryptKeyWrapper;->key:Lee/cyber/smartid/tse/dto/Key;

    if-eqz v0, :cond_0

    .line 126
    invoke-virtual {v0}, Lee/cyber/smartid/tse/dto/Key;->getN1()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public getRemainingExpirationDelay(Lee/cyber/smartid/tse/inter/WallClock;J)J
    .locals 6

    const-wide/16 v0, 0x0

    cmp-long v2, p2, v0

    if-lez v2, :cond_1

    .line 159
    invoke-virtual {p0, p1, p2, p3}, Lee/cyber/smartid/tse/dto/EncryptKeyWrapper;->isExpired(Lee/cyber/smartid/tse/inter/WallClock;J)Z

    move-result v2

    if-eqz v2, :cond_0

    goto :goto_0

    .line 163
    :cond_0
    invoke-interface {p1}, Lee/cyber/smartid/tse/inter/WallClock;->currentTimeMillis()J

    move-result-wide v2

    iget-wide v4, p0, Lee/cyber/smartid/tse/dto/EncryptKeyWrapper;->creationTimestamp:J

    sub-long/2addr v2, v4

    sub-long/2addr p2, v2

    invoke-static {p2, p3, v0, v1}, Ljava/lang/Math;->max(JJ)J

    move-result-wide p1

    return-wide p1

    :cond_1
    :goto_0
    return-wide v0
.end method

.method public isExpired(Lee/cyber/smartid/tse/inter/WallClock;J)Z
    .locals 4

    const-wide/16 v0, 0x0

    cmp-long v2, p2, v0

    if-lez v2, :cond_0

    .line 148
    invoke-interface {p1}, Lee/cyber/smartid/tse/inter/WallClock;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lee/cyber/smartid/tse/dto/EncryptKeyWrapper;->creationTimestamp:J

    sub-long/2addr v0, v2

    cmp-long p1, v0, p2

    if-ltz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public setDHKeyPair(Lee/cyber/smartid/cryptolib/dto/DiffieHellmanKeyPair;)V
    .locals 0

    .line 67
    iput-object p1, p0, Lee/cyber/smartid/tse/dto/EncryptKeyWrapper;->dhKeyPair:Lee/cyber/smartid/cryptolib/dto/DiffieHellmanKeyPair;

    return-void
.end method

.method public setKeyData(Lee/cyber/smartid/tse/dto/Key;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 56
    iput-object p1, p0, Lee/cyber/smartid/tse/dto/EncryptKeyWrapper;->key:Lee/cyber/smartid/tse/dto/Key;

    .line 57
    iput-object p2, p0, Lee/cyber/smartid/tse/dto/EncryptKeyWrapper;->d1PrimePrimeEncoded:Ljava/lang/String;

    .line 58
    iput-object p3, p0, Lee/cyber/smartid/tse/dto/EncryptKeyWrapper;->clientKeyEncoding:Ljava/lang/String;

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 173
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "EncryptKeyWrapper{keyReference=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lee/cyber/smartid/tse/dto/EncryptKeyWrapper;->keyReference:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v2, ", creationTimestamp="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v2, p0, Lee/cyber/smartid/tse/dto/EncryptKeyWrapper;->creationTimestamp:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v2, ", key="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lee/cyber/smartid/tse/dto/EncryptKeyWrapper;->key:Lee/cyber/smartid/tse/dto/Key;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v2, ", d1PrimePrimeEncoded=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lee/cyber/smartid/tse/dto/EncryptKeyWrapper;->d1PrimePrimeEncoded:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v2, ", clientKeyEncoding=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lee/cyber/smartid/tse/dto/EncryptKeyWrapper;->clientKeyEncoding:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v1, ", formatVersion="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lee/cyber/smartid/tse/dto/EncryptKeyWrapper;->formatVersion:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
