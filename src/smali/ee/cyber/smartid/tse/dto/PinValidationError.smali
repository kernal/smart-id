.class public Lee/cyber/smartid/tse/dto/PinValidationError;
.super Lee/cyber/smartid/tse/dto/PinInfo;
.source "PinValidationError.java"


# static fields
.field public static final ERROR_CODE_PIN_VALIDATION_FORBIDDEN_PATTERN:I = 0x1

.field public static final ERROR_CODE_PIN_VALIDATION_NONE:I = 0x0

.field private static final serialVersionUID:J = -0x16f06be240917977L


# instance fields
.field private final errorCode:I

.field private final errorMessage:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V
    .locals 0

    .line 41
    invoke-direct {p0, p1, p2}, Lee/cyber/smartid/tse/dto/PinInfo;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 42
    iput p3, p0, Lee/cyber/smartid/tse/dto/PinValidationError;->errorCode:I

    .line 43
    iput-object p4, p0, Lee/cyber/smartid/tse/dto/PinValidationError;->errorMessage:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public getErrorCode()I
    .locals 1

    .line 52
    iget v0, p0, Lee/cyber/smartid/tse/dto/PinValidationError;->errorCode:I

    return v0
.end method

.method public getErrorMessage()Ljava/lang/String;
    .locals 1

    .line 61
    iget-object v0, p0, Lee/cyber/smartid/tse/dto/PinValidationError;->errorMessage:Ljava/lang/String;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 71
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "PinValidationError{errorCode="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lee/cyber/smartid/tse/dto/PinValidationError;->errorCode:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", errorMessage="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lee/cyber/smartid/tse/dto/PinValidationError;->errorMessage:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "} "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 74
    invoke-super {p0}, Lee/cyber/smartid/tse/dto/PinInfo;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
