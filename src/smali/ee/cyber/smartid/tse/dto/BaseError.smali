.class public abstract Lee/cyber/smartid/tse/dto/BaseError;
.super Ljava/lang/Object;
.source "BaseError.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lee/cyber/smartid/tse/dto/BaseError$InvalidTransactionStateSource;
    }
.end annotation


# static fields
.field public static final ERROR_CERTIFICATE_EXCEPTION:J = 0x40fL

.field public static final ERROR_CODE_AUTHORIZATION_INVALID_OR_MISSING:J = 0x3f7L

.field public static final ERROR_CODE_CLIENT_TOO_OLD:J = 0x44cL

.field public static final ERROR_CODE_DATA_UPGRADE_FAILED:J = 0x400L

.field public static final ERROR_CODE_ENCRYPTION_DECRYPTION_FAILURE:J = 0x3faL

.field public static final ERROR_CODE_HASH_TYPE_MISMATCH:J = 0x3f2L

.field public static final ERROR_CODE_INVALID_LISTENER_TYPE:J = 0x407L

.field public static final ERROR_CODE_INVALID_PARAMETERS:J = 0x403L

.field public static final ERROR_CODE_INVALID_PINS:J = 0x3feL

.field public static final ERROR_CODE_INVALID_TRANSACTION_STATE:J = 0x3f4L

.field public static final ERROR_CODE_LOCAL_PENDING_STATE_NOT_FOUND:J = 0x406L

.field public static final ERROR_CODE_NETWORK_FAILURE:J = 0x3e8L

.field public static final ERROR_CODE_NO_SUCH_KEY:J = 0x3f3L

.field public static final ERROR_CODE_NO_SUCH_KEYS:J = 0x3efL

.field public static final ERROR_CODE_NO_SUCH_KTK:J = 0x413L

.field public static final ERROR_CODE_PRNG_ENTROPY_FIX_FAILED:J = 0x3ffL

.field public static final ERROR_CODE_PRNG_TESTS_FAILED:J = 0x418L

.field public static final ERROR_CODE_READING_CONFIGURATION_FILE_FAILED:J = 0x3f6L

.field public static final ERROR_CODE_RESPONSE_ID_INVALID:J = 0x3ebL

.field public static final ERROR_CODE_RESPONSE_INVALID:J = 0x3ecL

.field public static final ERROR_CODE_RESPONSE_TIMEOUT:J = 0x3e9L

.field public static final ERROR_CODE_SERVER_ACCOUNT_LOCKED:J = -0x7921L

.field public static final ERROR_CODE_SERVER_ACCOUNT_NOT_FOUND:J = -0x791aL

.field public static final ERROR_CODE_SERVER_CLONE_DETECTED:J = -0x792fL

.field public static final ERROR_CODE_SERVER_FRESHNESS_TOKEN_INVALID:J = -0x791eL

.field public static final ERROR_CODE_SERVER_INTERNAL_ERROR:J = -0x7f5bL

.field public static final ERROR_CODE_SERVER_INVALID_METHOD_PARAMETERS:J = -0x7f5aL

.field public static final ERROR_CODE_SERVER_INVALID_REQUEST:J = -0x7f58L

.field public static final ERROR_CODE_SERVER_JSON_PARSE_ERROR:J = -0x7fbcL

.field public static final ERROR_CODE_SERVER_KEY_MATERIAL_UNSUITABLE:J = -0x7930L

.field public static final ERROR_CODE_SERVER_KEY_NOT_FOUND:J = -0x791dL

.field public static final ERROR_CODE_SERVER_KEY_SET_MISMATCH:J = -0x791cL

.field public static final ERROR_CODE_SERVER_KEY_UNUSABLE:J = -0x792bL

.field public static final ERROR_CODE_SERVER_METHOD_NOT_FOUND:J = -0x7f59L

.field public static final ERROR_CODE_SERVER_RESPONDED_WITH_NON_200_OK_CODE:J = 0x40bL

.field public static final ERROR_CODE_SERVER_RE_KEY_PROCESS_NOT_FOUND:J = -0x7931L

.field public static final ERROR_CODE_SERVER_SIGNATURE_MISMATCH:J = -0x791bL

.field public static final ERROR_CODE_SERVER_TRANSACTION_ALREADY_COMPLETED:J = -0x791fL

.field public static final ERROR_CODE_SERVER_TRANSACTION_EXPIRED:J = -0x7923L

.field public static final ERROR_CODE_SERVER_TRANSACTION_NOT_FOUND:J = -0x7919L

.field public static final ERROR_CODE_SERVER_USER_NOT_FOUND:J = -0x791aL
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final ERROR_CODE_SERVER_WRONG_PIN:J = -0x7920L

.field public static final ERROR_CODE_SIGNING_FAILURE:J = 0x3f1L

.field public static final ERROR_CODE_SSL_EXCEPTION:J = 0x3f9L

.field public static final ERROR_CODE_STORAGE_READ_FAILED:J = 0x405L

.field public static final ERROR_CODE_STORAGE_WRITE_FAILED:J = 0x404L

.field public static final ERROR_CODE_SYSTEM_UNDER_MAINTENANCE:J = 0x44dL

.field public static final ERROR_CODE_TSE_INIT_FAILED:J = 0x408L

.field public static final ERROR_CODE_UNABLE_TO_ACQUIRE_FRESHNESS_TOKEN:J = 0x40aL

.field public static final ERROR_CODE_UNKNOWN:J = 0x0L

.field public static final ERROR_CODE_UNKNOWN_KEY_TYPE:J = 0x409L

.field public static final ERROR_GENERAL_SECURITY_EXCEPTION:J = 0x412L

.field public static final ERROR_MALFORMED_URL_EXCEPTION:J = 0x411L

.field public static final ERROR_PROTOCOL_EXCEPTION:J = 0x410L

.field public static final ERROR_SERVER_UNSUPPORTED_RESPONSE_ENCODING:J = 0x40dL

.field public static final ERROR_UNKNOWN_HOST_EXCEPTION:J = 0x40eL

.field public static final EXTRA_INVALID_TRANSACTION_STATE_SOURCE:Ljava/lang/String; = "ee.cyber.smartid.EXTRA_INVALID_TRANSACTION_STATE_SOURCE"

.field public static final EXTRA_IS_NON_RETRIABLE:Ljava/lang/String; = "ee.cyber.smartid.EXTRA_IS_NON_RETRIABLE"

.field public static final EXTRA_KEY_ID:Ljava/lang/String; = "ee.cyber.smartid.EXTRA_KEY_ID"

.field public static final EXTRA_KEY_PRNG_TEST_RESULTS:Ljava/lang/String; = "ee.cyber.smartid.EXTRA_KEY_PRNG_TEST_RESULTS"

.field public static final EXTRA_KEY_STATUS_INFO:Ljava/lang/String; = "ee.cyber.smartid.EXTRA_KEY_STATUS_INFO"

.field public static final EXTRA_PIN_VALIDATION_RESP:Ljava/lang/String; = "ee.cyber.smartid.EXTRA_PIN_VALIDATION_RESP"

.field public static final EXTRA_RAW_ERROR_DATA:Ljava/lang/String; = "ee.cyber.smartid.EXTRA_RAW_ERROR_DATA"

.field public static final EXTRA_SERVER_HTTP_RESPONSE_CODE:Ljava/lang/String; = "ee.cyber.smartid.EXTRA_SERVER_HTTP_RESPONSE_CODE"

.field public static final EXTRA_TRANSACTION_ID:Ljava/lang/String; = "ee.cyber.smartid.EXTRA_TRANSACTION_ID"

.field public static final INVALID_TRANSACTION_STATE_SOURCE_NETWORK:I = 0x1

.field public static final INVALID_TRANSACTION_STATE_SOURCE_UNKNOWN:I


# instance fields
.field protected code:J

.field protected final createdAtTimestamp:J

.field protected extras:Landroid/os/Bundle;

.field protected message:Ljava/lang/String;

.field protected trace:Ljava/lang/String;


# direct methods
.method public constructor <init>(J)V
    .locals 0

    .line 372
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 373
    iput-wide p1, p0, Lee/cyber/smartid/tse/dto/BaseError;->createdAtTimestamp:J

    return-void
.end method

.method public constructor <init>(JLjava/lang/String;Landroid/os/Bundle;Ljava/lang/String;J)V
    .locals 0

    .line 385
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 386
    iput-wide p1, p0, Lee/cyber/smartid/tse/dto/BaseError;->code:J

    .line 387
    iput-object p3, p0, Lee/cyber/smartid/tse/dto/BaseError;->message:Ljava/lang/String;

    .line 388
    iput-object p4, p0, Lee/cyber/smartid/tse/dto/BaseError;->extras:Landroid/os/Bundle;

    .line 389
    iput-object p5, p0, Lee/cyber/smartid/tse/dto/BaseError;->trace:Ljava/lang/String;

    .line 390
    iput-wide p6, p0, Lee/cyber/smartid/tse/dto/BaseError;->createdAtTimestamp:J

    return-void
.end method

.method protected constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .line 398
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 399
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lee/cyber/smartid/tse/dto/BaseError;->code:J

    .line 400
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lee/cyber/smartid/tse/dto/BaseError;->message:Ljava/lang/String;

    .line 401
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readBundle(Ljava/lang/ClassLoader;)Landroid/os/Bundle;

    move-result-object v0

    iput-object v0, p0, Lee/cyber/smartid/tse/dto/BaseError;->extras:Landroid/os/Bundle;

    .line 402
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lee/cyber/smartid/tse/dto/BaseError;->trace:Ljava/lang/String;

    .line 403
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lee/cyber/smartid/tse/dto/BaseError;->createdAtTimestamp:J

    return-void
.end method

.method public static isServerSideError(Ljava/lang/Throwable;)Z
    .locals 2

    .line 718
    instance-of v0, p0, Ljava/net/SocketTimeoutException;

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    return v1

    .line 720
    :cond_0
    instance-of v0, p0, Lee/cyber/smartid/tse/dto/AuthorizationRequiredException;

    if-eqz v0, :cond_1

    return v1

    .line 722
    :cond_1
    instance-of v0, p0, Lee/cyber/smartid/tse/dto/ClientTooOldException;

    if-eqz v0, :cond_2

    return v1

    .line 724
    :cond_2
    instance-of v0, p0, Lee/cyber/smartid/tse/dto/SystemUnderMaintenanceException;

    if-eqz v0, :cond_3

    return v1

    .line 726
    :cond_3
    instance-of p0, p0, Lee/cyber/smartid/tse/dto/InvalidResponseResultException;

    if-eqz p0, :cond_4

    return v1

    :cond_4
    const/4 p0, 0x0

    return p0
.end method


# virtual methods
.method public createOrGetExtras()Landroid/os/Bundle;
    .locals 1

    .line 442
    iget-object v0, p0, Lee/cyber/smartid/tse/dto/BaseError;->extras:Landroid/os/Bundle;

    if-nez v0, :cond_0

    .line 443
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, Lee/cyber/smartid/tse/dto/BaseError;->extras:Landroid/os/Bundle;

    .line 445
    :cond_0
    iget-object v0, p0, Lee/cyber/smartid/tse/dto/BaseError;->extras:Landroid/os/Bundle;

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 7

    const/4 v0, 0x1

    if-ne p0, p1, :cond_0

    return v0

    :cond_0
    const/4 v1, 0x0

    if-eqz p1, :cond_a

    .line 784
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_1

    goto :goto_3

    .line 788
    :cond_1
    check-cast p1, Lee/cyber/smartid/tse/dto/BaseError;

    .line 790
    iget-wide v2, p0, Lee/cyber/smartid/tse/dto/BaseError;->code:J

    iget-wide v4, p1, Lee/cyber/smartid/tse/dto/BaseError;->code:J

    cmp-long v6, v2, v4

    if-eqz v6, :cond_2

    return v1

    .line 793
    :cond_2
    iget-wide v2, p0, Lee/cyber/smartid/tse/dto/BaseError;->createdAtTimestamp:J

    iget-wide v4, p1, Lee/cyber/smartid/tse/dto/BaseError;->createdAtTimestamp:J

    cmp-long v6, v2, v4

    if-eqz v6, :cond_3

    return v1

    .line 796
    :cond_3
    iget-object v2, p0, Lee/cyber/smartid/tse/dto/BaseError;->message:Ljava/lang/String;

    if-eqz v2, :cond_4

    iget-object v3, p1, Lee/cyber/smartid/tse/dto/BaseError;->message:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    goto :goto_0

    :cond_4
    iget-object v2, p1, Lee/cyber/smartid/tse/dto/BaseError;->message:Ljava/lang/String;

    if-eqz v2, :cond_5

    :goto_0
    return v1

    .line 800
    :cond_5
    iget-object v2, p0, Lee/cyber/smartid/tse/dto/BaseError;->extras:Landroid/os/Bundle;

    if-eqz v2, :cond_6

    iget-object v3, p1, Lee/cyber/smartid/tse/dto/BaseError;->extras:Landroid/os/Bundle;

    invoke-static {v2, v3}, Lee/cyber/smartid/tse/util/Util;->equals(Landroid/os/Bundle;Landroid/os/Bundle;)Z

    move-result v2

    if-nez v2, :cond_7

    goto :goto_1

    :cond_6
    iget-object v2, p1, Lee/cyber/smartid/tse/dto/BaseError;->extras:Landroid/os/Bundle;

    if-eqz v2, :cond_7

    :goto_1
    return v1

    .line 803
    :cond_7
    iget-object v2, p0, Lee/cyber/smartid/tse/dto/BaseError;->trace:Ljava/lang/String;

    if-eqz v2, :cond_8

    iget-object p1, p1, Lee/cyber/smartid/tse/dto/BaseError;->trace:Ljava/lang/String;

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_2

    :cond_8
    iget-object p1, p1, Lee/cyber/smartid/tse/dto/BaseError;->trace:Ljava/lang/String;

    if-nez p1, :cond_9

    goto :goto_2

    :cond_9
    const/4 v0, 0x0

    :goto_2
    return v0

    :cond_a
    :goto_3
    return v1
.end method

.method public getCode()J
    .locals 2

    .line 412
    iget-wide v0, p0, Lee/cyber/smartid/tse/dto/BaseError;->code:J

    return-wide v0
.end method

.method public getCreatedAtTimestamp()J
    .locals 2

    .line 580
    iget-wide v0, p0, Lee/cyber/smartid/tse/dto/BaseError;->createdAtTimestamp:J

    return-wide v0
.end method

.method public getExtras()Landroid/os/Bundle;
    .locals 1

    .line 465
    iget-object v0, p0, Lee/cyber/smartid/tse/dto/BaseError;->extras:Landroid/os/Bundle;

    return-object v0
.end method

.method public getHTTPResponseCode()I
    .locals 2

    .line 555
    iget-object v0, p0, Lee/cyber/smartid/tse/dto/BaseError;->extras:Landroid/os/Bundle;

    if-eqz v0, :cond_1

    const-string v1, "ee.cyber.smartid.EXTRA_SERVER_HTTP_RESPONSE_CODE"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    .line 558
    :cond_0
    iget-object v0, p0, Lee/cyber/smartid/tse/dto/BaseError;->extras:Landroid/os/Bundle;

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    return v0

    :cond_1
    :goto_0
    const/16 v0, 0xc8

    return v0
.end method

.method public getHumanReadableErrorCodeName()Ljava/lang/String;
    .locals 5

    .line 599
    iget-wide v0, p0, Lee/cyber/smartid/tse/dto/BaseError;->code:J

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    const-string v0, "ERROR_CODE_UNKNOWN"

    return-object v0

    :cond_0
    const-wide/16 v2, 0x3e8

    cmp-long v4, v0, v2

    if-nez v4, :cond_1

    const-string v0, "ERROR_CODE_NETWORK_FAILURE"

    return-object v0

    :cond_1
    const-wide/16 v2, 0x3e9

    cmp-long v4, v0, v2

    if-nez v4, :cond_2

    const-string v0, "ERROR_CODE_RESPONSE_TIMEOUT"

    return-object v0

    :cond_2
    const-wide/16 v2, 0x3eb

    cmp-long v4, v0, v2

    if-nez v4, :cond_3

    const-string v0, "ERROR_CODE_RESPONSE_ID_INVALID"

    return-object v0

    :cond_3
    const-wide/16 v2, 0x3ec

    cmp-long v4, v0, v2

    if-nez v4, :cond_4

    const-string v0, "ERROR_CODE_RESPONSE_INVALID"

    return-object v0

    :cond_4
    const-wide/16 v2, 0x3ef

    cmp-long v4, v0, v2

    if-nez v4, :cond_5

    const-string v0, "ERROR_CODE_NO_SUCH_KEYS"

    return-object v0

    :cond_5
    const-wide/16 v2, 0x3f1

    cmp-long v4, v0, v2

    if-nez v4, :cond_6

    const-string v0, "ERROR_CODE_SIGNING_FAILURE"

    return-object v0

    :cond_6
    const-wide/16 v2, 0x3f2

    cmp-long v4, v0, v2

    if-nez v4, :cond_7

    const-string v0, "ERROR_CODE_HASH_TYPE_MISMATCH"

    return-object v0

    :cond_7
    const-wide/16 v2, 0x3f3

    cmp-long v4, v0, v2

    if-nez v4, :cond_8

    const-string v0, "ERROR_CODE_NO_SUCH_KEY"

    return-object v0

    :cond_8
    const-wide/16 v2, 0x3f4    # 5.0E-321

    cmp-long v4, v0, v2

    if-nez v4, :cond_9

    const-string v0, "ERROR_CODE_INVALID_TRANSACTION_STATE"

    return-object v0

    :cond_9
    const-wide/16 v2, 0x3f6

    cmp-long v4, v0, v2

    if-nez v4, :cond_a

    const-string v0, "ERROR_CODE_READING_CONFIGURATION_FILE_FAILED"

    return-object v0

    :cond_a
    const-wide/16 v2, 0x3f7

    cmp-long v4, v0, v2

    if-nez v4, :cond_b

    const-string v0, "ERROR_CODE_AUTHORIZATION_INVALID_OR_MISSING"

    return-object v0

    :cond_b
    const-wide/16 v2, 0x3f9

    cmp-long v4, v0, v2

    if-nez v4, :cond_c

    const-string v0, "ERROR_CODE_SSL_EXCEPTION"

    return-object v0

    :cond_c
    const-wide/16 v2, 0x3fa

    cmp-long v4, v0, v2

    if-nez v4, :cond_d

    const-string v0, "ERROR_CODE_ENCRYPTION_DECRYPTION_FAILURE"

    return-object v0

    :cond_d
    const-wide/16 v2, 0x3fe

    cmp-long v4, v0, v2

    if-nez v4, :cond_e

    const-string v0, "ERROR_CODE_INVALID_PINS"

    return-object v0

    :cond_e
    const-wide/16 v2, 0x3ff

    cmp-long v4, v0, v2

    if-nez v4, :cond_f

    const-string v0, "ERROR_CODE_PRNG_ENTROPY_FIX_FAILED"

    return-object v0

    :cond_f
    const-wide/16 v2, 0x400

    cmp-long v4, v0, v2

    if-nez v4, :cond_10

    const-string v0, "ERROR_CODE_DATA_UPGRADE_FAILED"

    return-object v0

    :cond_10
    const-wide/16 v2, 0x403

    cmp-long v4, v0, v2

    if-nez v4, :cond_11

    const-string v0, "ERROR_CODE_INVALID_PARAMETERS"

    return-object v0

    :cond_11
    const-wide/16 v2, 0x44c

    cmp-long v4, v0, v2

    if-nez v4, :cond_12

    const-string v0, "ERROR_CODE_CLIENT_TOO_OLD"

    return-object v0

    :cond_12
    const-wide/16 v2, 0x44d

    cmp-long v4, v0, v2

    if-nez v4, :cond_13

    const-string v0, "ERROR_CODE_SYSTEM_UNDER_MAINTENANCE"

    return-object v0

    :cond_13
    const-wide/16 v2, 0x404

    cmp-long v4, v0, v2

    if-nez v4, :cond_14

    const-string v0, "ERROR_CODE_STORAGE_WRITE_FAILED"

    return-object v0

    :cond_14
    const-wide/16 v2, 0x405

    cmp-long v4, v0, v2

    if-nez v4, :cond_15

    const-string v0, "ERROR_CODE_STORAGE_READ_FAILED"

    return-object v0

    :cond_15
    const-wide/16 v2, -0x7919

    cmp-long v4, v0, v2

    if-nez v4, :cond_16

    const-string v0, "ERROR_CODE_SERVER_TRANSACTION_NOT_FOUND"

    return-object v0

    :cond_16
    const-wide/16 v2, -0x791a

    cmp-long v4, v0, v2

    if-nez v4, :cond_17

    const-string v0, "ERROR_CODE_SERVER_ACCOUNT_NOT_FOUND"

    return-object v0

    :cond_17
    const-wide/16 v2, -0x791b

    cmp-long v4, v0, v2

    if-nez v4, :cond_18

    const-string v0, "ERROR_CODE_SERVER_SIGNATURE_MISMATCH"

    return-object v0

    :cond_18
    const-wide/16 v2, -0x791c

    cmp-long v4, v0, v2

    if-nez v4, :cond_19

    const-string v0, "ERROR_CODE_SERVER_KEY_SET_MISMATCH"

    return-object v0

    :cond_19
    const-wide/16 v2, -0x791d

    cmp-long v4, v0, v2

    if-nez v4, :cond_1a

    const-string v0, "ERROR_CODE_SERVER_KEY_NOT_FOUND"

    return-object v0

    :cond_1a
    const-wide/16 v2, -0x791e

    cmp-long v4, v0, v2

    if-nez v4, :cond_1b

    const-string v0, "ERROR_CODE_SERVER_FRESHNESS_TOKEN_INVALID"

    return-object v0

    :cond_1b
    const-wide/16 v2, -0x791f

    cmp-long v4, v0, v2

    if-nez v4, :cond_1c

    const-string v0, "ERROR_CODE_SERVER_TRANSACTION_ALREADY_COMPLETED"

    return-object v0

    :cond_1c
    const-wide/16 v2, -0x7920

    cmp-long v4, v0, v2

    if-nez v4, :cond_1d

    const-string v0, "ERROR_CODE_SERVER_WRONG_PIN"

    return-object v0

    :cond_1d
    const-wide/16 v2, -0x7921

    cmp-long v4, v0, v2

    if-nez v4, :cond_1e

    const-string v0, "ERROR_CODE_SERVER_ACCOUNT_LOCKED"

    return-object v0

    :cond_1e
    const-wide/16 v2, -0x7923

    cmp-long v4, v0, v2

    if-nez v4, :cond_1f

    const-string v0, "ERROR_CODE_SERVER_TRANSACTION_EXPIRED"

    return-object v0

    :cond_1f
    const-wide/16 v2, -0x792b

    cmp-long v4, v0, v2

    if-nez v4, :cond_20

    const-string v0, "ERROR_CODE_SERVER_KEY_UNUSABLE"

    return-object v0

    :cond_20
    const-wide/16 v2, -0x7f59

    cmp-long v4, v0, v2

    if-nez v4, :cond_21

    const-string v0, "ERROR_CODE_SERVER_METHOD_NOT_FOUND"

    return-object v0

    :cond_21
    const-wide/16 v2, -0x7f5a

    cmp-long v4, v0, v2

    if-nez v4, :cond_22

    const-string v0, "ERROR_CODE_SERVER_INVALID_METHOD_PARAMETERS"

    return-object v0

    :cond_22
    const-wide/16 v2, -0x7f5b

    cmp-long v4, v0, v2

    if-nez v4, :cond_23

    const-string v0, "ERROR_CODE_SERVER_INTERNAL_ERROR"

    return-object v0

    :cond_23
    const-wide/16 v2, 0x406

    cmp-long v4, v0, v2

    if-nez v4, :cond_24

    const-string v0, "ERROR_CODE_LOCAL_PENDING_STATE_NOT_FOUND"

    return-object v0

    :cond_24
    const-wide/16 v2, 0x407

    cmp-long v4, v0, v2

    if-nez v4, :cond_25

    const-string v0, "ERROR_CODE_INVALID_LISTENER_TYPE"

    return-object v0

    :cond_25
    const-wide/16 v2, 0x408

    cmp-long v4, v0, v2

    if-nez v4, :cond_26

    const-string v0, "ERROR_CODE_TSE_INIT_FAILED"

    return-object v0

    :cond_26
    const-wide/16 v2, -0x7fbc

    cmp-long v4, v0, v2

    if-nez v4, :cond_27

    const-string v0, "ERROR_CODE_SERVER_JSON_PARSE_ERROR"

    return-object v0

    :cond_27
    const-wide/16 v2, -0x7f58

    cmp-long v4, v0, v2

    if-nez v4, :cond_28

    const-string v0, "ERROR_CODE_SERVER_INVALID_REQUEST"

    return-object v0

    :cond_28
    const-wide/16 v2, 0x40a

    cmp-long v4, v0, v2

    if-nez v4, :cond_29

    const-string v0, "ERROR_CODE_UNABLE_TO_ACQUIRE_FRESHNESS_TOKEN"

    return-object v0

    :cond_29
    const-wide/16 v2, 0x40b

    cmp-long v4, v0, v2

    if-nez v4, :cond_2a

    const-string v0, "ERROR_CODE_SERVER_RESPONDED_WITH_NON_200_OK_CODE"

    return-object v0

    :cond_2a
    const-wide/16 v2, 0x40d

    cmp-long v4, v0, v2

    if-nez v4, :cond_2b

    const-string v0, "ERROR_SERVER_UNSUPPORTED_RESPONSE_ENCODING"

    return-object v0

    :cond_2b
    const-wide/16 v2, -0x7930

    cmp-long v4, v0, v2

    if-nez v4, :cond_2c

    const-string v0, "ERROR_CODE_SERVER_KEY_MATERIAL_UNSUITABLE"

    return-object v0

    :cond_2c
    const-wide/16 v2, -0x792f

    cmp-long v4, v0, v2

    if-nez v4, :cond_2d

    const-string v0, "ERROR_CODE_SERVER_CLONE_DETECTED"

    return-object v0

    :cond_2d
    const-wide/16 v2, -0x7931

    cmp-long v4, v0, v2

    if-nez v4, :cond_2e

    const-string v0, "ERROR_CODE_SERVER_RE_KEY_PROCESS_NOT_FOUND"

    return-object v0

    :cond_2e
    const-wide/16 v2, 0x40e

    cmp-long v4, v0, v2

    if-nez v4, :cond_2f

    const-string v0, "ERROR_UNKNOWN_HOST_EXCEPTION"

    return-object v0

    :cond_2f
    const-wide/16 v2, 0x40f

    cmp-long v4, v0, v2

    if-nez v4, :cond_30

    const-string v0, "ERROR_CERTIFICATE_EXCEPTION"

    return-object v0

    :cond_30
    const-wide/16 v2, 0x410

    cmp-long v4, v0, v2

    if-nez v4, :cond_31

    const-string v0, "ERROR_PROTOCOL_EXCEPTION"

    return-object v0

    :cond_31
    const-wide/16 v2, 0x411

    cmp-long v4, v0, v2

    if-nez v4, :cond_32

    const-string v0, "ERROR_MALFORMED_URL_EXCEPTION"

    return-object v0

    :cond_32
    const-wide/16 v2, 0x412

    cmp-long v4, v0, v2

    if-nez v4, :cond_33

    const-string v0, "ERROR_GENERAL_SECURITY_EXCEPTION"

    return-object v0

    :cond_33
    const-wide/16 v2, 0x413

    cmp-long v4, v0, v2

    if-nez v4, :cond_34

    const-string v0, "ERROR_CODE_NO_SUCH_KTK"

    return-object v0

    :cond_34
    const-wide/16 v2, 0x418

    cmp-long v4, v0, v2

    if-nez v4, :cond_35

    const-string v0, "ERROR_CODE_PRNG_TESTS_FAILED"

    return-object v0

    .line 708
    :cond_35
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ERROR_WITH_CODE_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lee/cyber/smartid/tse/dto/BaseError;->code:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getInvalidTransactionStateSource()I
    .locals 3

    .line 543
    iget-object v0, p0, Lee/cyber/smartid/tse/dto/BaseError;->extras:Landroid/os/Bundle;

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    const-string v2, "ee.cyber.smartid.EXTRA_INVALID_TRANSACTION_STATE_SOURCE"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    .line 546
    :cond_0
    iget-object v0, p0, Lee/cyber/smartid/tse/dto/BaseError;->extras:Landroid/os/Bundle;

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0

    :cond_1
    :goto_0
    return v1
.end method

.method public getKeyStatus()Lee/cyber/smartid/tse/dto/jsonrpc/AccountKeyStatus;
    .locals 2

    .line 477
    iget-object v0, p0, Lee/cyber/smartid/tse/dto/BaseError;->extras:Landroid/os/Bundle;

    if-eqz v0, :cond_1

    const-string v1, "ee.cyber.smartid.EXTRA_KEY_STATUS_INFO"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    .line 480
    :cond_0
    iget-object v0, p0, Lee/cyber/smartid/tse/dto/BaseError;->extras:Landroid/os/Bundle;

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lee/cyber/smartid/tse/dto/jsonrpc/AccountKeyStatus;

    return-object v0

    :cond_1
    :goto_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public getMessage()Ljava/lang/String;
    .locals 1

    .line 432
    iget-object v0, p0, Lee/cyber/smartid/tse/dto/BaseError;->message:Ljava/lang/String;

    return-object v0
.end method

.method public getPRNGTestResults()[Lee/cyber/smartid/cryptolib/dto/TestResult;
    .locals 2

    .line 568
    iget-object v0, p0, Lee/cyber/smartid/tse/dto/BaseError;->extras:Landroid/os/Bundle;

    if-eqz v0, :cond_1

    const-string v1, "ee.cyber.smartid.EXTRA_KEY_PRNG_TEST_RESULTS"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    .line 571
    :cond_0
    iget-object v0, p0, Lee/cyber/smartid/tse/dto/BaseError;->extras:Landroid/os/Bundle;

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, [Lee/cyber/smartid/cryptolib/dto/TestResult;

    check-cast v0, [Lee/cyber/smartid/cryptolib/dto/TestResult;

    return-object v0

    :cond_1
    :goto_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public getPinValidationResp()Lee/cyber/smartid/tse/dto/jsonrpc/resp/ValidatePinResp;
    .locals 2

    .line 491
    iget-object v0, p0, Lee/cyber/smartid/tse/dto/BaseError;->extras:Landroid/os/Bundle;

    if-eqz v0, :cond_1

    const-string v1, "ee.cyber.smartid.EXTRA_PIN_VALIDATION_RESP"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    .line 494
    :cond_0
    iget-object v0, p0, Lee/cyber/smartid/tse/dto/BaseError;->extras:Landroid/os/Bundle;

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lee/cyber/smartid/tse/dto/jsonrpc/resp/ValidatePinResp;

    return-object v0

    :cond_1
    :goto_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public getRawErrorData()Ljava/lang/String;
    .locals 2

    .line 516
    iget-object v0, p0, Lee/cyber/smartid/tse/dto/BaseError;->extras:Landroid/os/Bundle;

    if-eqz v0, :cond_1

    const-string v1, "ee.cyber.smartid.EXTRA_RAW_ERROR_DATA"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    .line 519
    :cond_0
    iget-object v0, p0, Lee/cyber/smartid/tse/dto/BaseError;->extras:Landroid/os/Bundle;

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_1
    :goto_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public getTrace()Ljava/lang/String;
    .locals 1

    .line 422
    iget-object v0, p0, Lee/cyber/smartid/tse/dto/BaseError;->trace:Ljava/lang/String;

    return-object v0
.end method

.method public getTransactionUUID()Ljava/lang/String;
    .locals 2

    .line 530
    iget-object v0, p0, Lee/cyber/smartid/tse/dto/BaseError;->extras:Landroid/os/Bundle;

    if-eqz v0, :cond_1

    const-string v1, "ee.cyber.smartid.EXTRA_TRANSACTION_ID"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    .line 533
    :cond_0
    iget-object v0, p0, Lee/cyber/smartid/tse/dto/BaseError;->extras:Landroid/os/Bundle;

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_1
    :goto_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public hasExtras()Z
    .locals 1

    .line 455
    iget-object v0, p0, Lee/cyber/smartid/tse/dto/BaseError;->extras:Landroid/os/Bundle;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 7

    .line 813
    iget-wide v0, p0, Lee/cyber/smartid/tse/dto/BaseError;->code:J

    const/16 v2, 0x20

    ushr-long v3, v0, v2

    xor-long/2addr v0, v3

    long-to-int v1, v0

    mul-int/lit8 v1, v1, 0x1f

    .line 814
    iget-object v0, p0, Lee/cyber/smartid/tse/dto/BaseError;->message:Ljava/lang/String;

    const/4 v3, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    add-int/2addr v1, v0

    mul-int/lit8 v1, v1, 0x1f

    .line 815
    iget-object v0, p0, Lee/cyber/smartid/tse/dto/BaseError;->extras:Landroid/os/Bundle;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_1

    :cond_1
    const/4 v0, 0x0

    :goto_1
    add-int/2addr v1, v0

    mul-int/lit8 v1, v1, 0x1f

    .line 816
    iget-object v0, p0, Lee/cyber/smartid/tse/dto/BaseError;->trace:Ljava/lang/String;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v3

    :cond_2
    add-int/2addr v1, v3

    mul-int/lit8 v1, v1, 0x1f

    .line 817
    iget-wide v3, p0, Lee/cyber/smartid/tse/dto/BaseError;->createdAtTimestamp:J

    ushr-long v5, v3, v2

    xor-long v2, v3, v5

    long-to-int v0, v2

    add-int/2addr v1, v0

    return v1
.end method

.method public isNonRetriable()Z
    .locals 3

    .line 589
    iget-object v0, p0, Lee/cyber/smartid/tse/dto/BaseError;->extras:Landroid/os/Bundle;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    const-string v2, "ee.cyber.smartid.EXTRA_IS_NON_RETRIABLE"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lee/cyber/smartid/tse/dto/BaseError;->extras:Landroid/os/Bundle;

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v1, 0x1

    :cond_0
    return v1
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .line 739
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Error{code="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lee/cyber/smartid/tse/dto/BaseError;->code:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 740
    invoke-virtual {p0}, Lee/cyber/smartid/tse/dto/BaseError;->getHumanReadableErrorCodeName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "), message=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lee/cyber/smartid/tse/dto/BaseError;->message:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v2, ", trace=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lee/cyber/smartid/tse/dto/BaseError;->trace:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v1, ", extras="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lee/cyber/smartid/tse/dto/BaseError;->extras:Landroid/os/Bundle;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", createdAtTimestamp="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lee/cyber/smartid/tse/dto/BaseError;->createdAtTimestamp:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .line 766
    iget-wide v0, p0, Lee/cyber/smartid/tse/dto/BaseError;->code:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 767
    iget-object p2, p0, Lee/cyber/smartid/tse/dto/BaseError;->message:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 768
    iget-object p2, p0, Lee/cyber/smartid/tse/dto/BaseError;->extras:Landroid/os/Bundle;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeBundle(Landroid/os/Bundle;)V

    .line 769
    iget-object p2, p0, Lee/cyber/smartid/tse/dto/BaseError;->trace:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 770
    iget-wide v0, p0, Lee/cyber/smartid/tse/dto/BaseError;->createdAtTimestamp:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    return-void
.end method
