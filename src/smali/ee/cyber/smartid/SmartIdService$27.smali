.class Lee/cyber/smartid/SmartIdService$27;
.super Ljava/lang/Object;
.source "SmartIdService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lee/cyber/smartid/SmartIdService;->a(Ljava/lang/String;ZLee/cyber/smartid/inter/UpdateDeviceListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lee/cyber/smartid/inter/UpdateDeviceListener;

.field final synthetic b:Ljava/lang/String;

.field final synthetic c:Lee/cyber/smartid/SmartIdService;


# direct methods
.method constructor <init>(Lee/cyber/smartid/SmartIdService;Lee/cyber/smartid/inter/UpdateDeviceListener;Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lee/cyber/smartid/SmartIdService$27;->c:Lee/cyber/smartid/SmartIdService;

    iput-object p2, p0, Lee/cyber/smartid/SmartIdService$27;->a:Lee/cyber/smartid/inter/UpdateDeviceListener;

    iput-object p3, p0, Lee/cyber/smartid/SmartIdService$27;->b:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/SmartIdService$27;->a:Lee/cyber/smartid/inter/UpdateDeviceListener;

    if-eqz v0, :cond_0

    .line 2
    iget-object v1, p0, Lee/cyber/smartid/SmartIdService$27;->b:Ljava/lang/String;

    new-instance v2, Lee/cyber/smartid/dto/jsonrpc/resp/UpdateDeviceResp;

    const-string v3, "NO_UPDATE_NEEDED"

    invoke-direct {v2, v1, v3}, Lee/cyber/smartid/dto/jsonrpc/resp/UpdateDeviceResp;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1, v2}, Lee/cyber/smartid/inter/UpdateDeviceListener;->onUpdateDeviceSuccess(Ljava/lang/String;Lee/cyber/smartid/dto/jsonrpc/resp/UpdateDeviceResp;)V

    :cond_0
    return-void
.end method
