.class Lee/cyber/smartid/SmartIdService$SmartIdListenerAccess;
.super Ljava/lang/Object;
.source "SmartIdService.java"

# interfaces
.implements Lee/cyber/smartid/inter/ListenerAccess;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lee/cyber/smartid/SmartIdService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SmartIdListenerAccess"
.end annotation


# instance fields
.field final synthetic a:Lee/cyber/smartid/SmartIdService;


# direct methods
.method private constructor <init>(Lee/cyber/smartid/SmartIdService;)V
    .locals 0

    .line 2
    iput-object p1, p0, Lee/cyber/smartid/SmartIdService$SmartIdListenerAccess;->a:Lee/cyber/smartid/SmartIdService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lee/cyber/smartid/SmartIdService;Lee/cyber/smartid/SmartIdService$1;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lee/cyber/smartid/SmartIdService$SmartIdListenerAccess;-><init>(Lee/cyber/smartid/SmartIdService;)V

    return-void
.end method


# virtual methods
.method public getListener(Ljava/lang/String;ZLjava/lang/Class;)Lee/cyber/smartid/inter/ServiceListener;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lee/cyber/smartid/inter/ServiceListener;",
            ">(",
            "Ljava/lang/String;",
            "Z",
            "Ljava/lang/Class<",
            "TT;>;)TT;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/SmartIdService$SmartIdListenerAccess;->a:Lee/cyber/smartid/SmartIdService;

    invoke-static {v0, p1, p2, p3}, Lee/cyber/smartid/SmartIdService;->a(Lee/cyber/smartid/SmartIdService;Ljava/lang/String;ZLjava/lang/Class;)Lee/cyber/smartid/inter/ServiceListener;

    move-result-object p1

    return-object p1
.end method

.method public getListenersForType(ZLjava/lang/Class;)[Landroid/support/v4/e/j;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lee/cyber/smartid/inter/ServiceListener;",
            ">(Z",
            "Ljava/lang/Class<",
            "TT;>;)[",
            "Landroid/support/v4/e/j<",
            "Ljava/lang/String;",
            "TT;>;"
        }
    .end annotation

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 2
    iget-object v1, p0, Lee/cyber/smartid/SmartIdService$SmartIdListenerAccess;->a:Lee/cyber/smartid/SmartIdService;

    invoke-static {v1}, Lee/cyber/smartid/SmartIdService;->x(Lee/cyber/smartid/SmartIdService;)Ljava/util/WeakHashMap;

    move-result-object v1

    monitor-enter v1

    .line 3
    :try_start_0
    iget-object v2, p0, Lee/cyber/smartid/SmartIdService$SmartIdListenerAccess;->a:Lee/cyber/smartid/SmartIdService;

    invoke-static {v2}, Lee/cyber/smartid/SmartIdService;->x(Lee/cyber/smartid/SmartIdService;)Ljava/util/WeakHashMap;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/WeakHashMap;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 4
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 5
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 6
    iget-object v4, p0, Lee/cyber/smartid/SmartIdService$SmartIdListenerAccess;->a:Lee/cyber/smartid/SmartIdService;

    invoke-static {v4}, Lee/cyber/smartid/SmartIdService;->x(Lee/cyber/smartid/SmartIdService;)Ljava/util/WeakHashMap;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/util/WeakHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lee/cyber/smartid/inter/ServiceListener;

    if-eqz v4, :cond_0

    .line 7
    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {p2, v5}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 8
    new-instance v5, Landroid/support/v4/e/j;

    invoke-direct {v5, v3, v4}, Landroid/support/v4/e/j;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    if-eqz p1, :cond_0

    .line 9
    invoke-interface {v2}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 10
    :cond_1
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 11
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result p1

    new-array p1, p1, [Landroid/support/v4/e/j;

    .line 12
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object p1

    check-cast p1, [Landroid/support/v4/e/j;

    return-object p1

    :catchall_0
    move-exception p1

    .line 13
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw p1
.end method

.method public getTSEListenerToServiceListenerMapper()Lee/cyber/smartid/manager/inter/TSEListenerToServiceListenerMapper;
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/SmartIdService$SmartIdListenerAccess;->a:Lee/cyber/smartid/SmartIdService;

    invoke-static {v0}, Lee/cyber/smartid/SmartIdService;->y(Lee/cyber/smartid/SmartIdService;)Lee/cyber/smartid/manager/inter/TSEListenerToServiceListenerMapper;

    move-result-object v0

    return-object v0
.end method

.method public notifyAlarmHandlers(Ljava/lang/String;Ljava/lang/String;ZZ)V
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/SmartIdService$SmartIdListenerAccess;->a:Lee/cyber/smartid/SmartIdService;

    invoke-static {v0, p1, p2, p3, p4}, Lee/cyber/smartid/SmartIdService;->a(Lee/cyber/smartid/SmartIdService;Ljava/lang/String;Ljava/lang/String;ZZ)V

    return-void
.end method

.method public notifyError(Lee/cyber/smartid/inter/ServiceListener;Ljava/lang/String;Lee/cyber/smartid/dto/SmartIdError;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/SmartIdService$SmartIdListenerAccess;->a:Lee/cyber/smartid/SmartIdService;

    invoke-static {v0, p2, p1, p3}, Lee/cyber/smartid/SmartIdService;->a(Lee/cyber/smartid/SmartIdService;Ljava/lang/String;Lee/cyber/smartid/inter/ServiceListener;Lee/cyber/smartid/dto/SmartIdError;)V

    return-void
.end method

.method public notifySystemEventsListeners(Lee/cyber/smartid/dto/SmartIdError;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/SmartIdService$SmartIdListenerAccess;->a:Lee/cyber/smartid/SmartIdService;

    invoke-static {v0, p1}, Lee/cyber/smartid/SmartIdService;->a(Lee/cyber/smartid/SmartIdService;Lee/cyber/smartid/dto/SmartIdError;)V

    return-void
.end method

.method public setListener(Ljava/lang/String;Lee/cyber/smartid/inter/ServiceListener;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/SmartIdService$SmartIdListenerAccess;->a:Lee/cyber/smartid/SmartIdService;

    invoke-virtual {v0, p1, p2}, Lee/cyber/smartid/SmartIdService;->setListener(Ljava/lang/String;Lee/cyber/smartid/inter/ServiceListener;)V

    return-void
.end method
