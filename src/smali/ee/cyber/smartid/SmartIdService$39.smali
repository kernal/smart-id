.class Lee/cyber/smartid/SmartIdService$39;
.super Lee/cyber/smartid/tse/network/RPCCallback;
.source "SmartIdService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lee/cyber/smartid/SmartIdService;->getRegistrationToken(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/inter/GetRegistrationTokenListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lee/cyber/smartid/tse/network/RPCCallback<",
        "Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCResponse<",
        "Lee/cyber/smartid/dto/jsonrpc/result/GetRegTokenNonceResult;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:Lee/cyber/smartid/SmartIdService;


# direct methods
.method constructor <init>(Lee/cyber/smartid/SmartIdService;Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCRequest;Lee/cyber/smartid/tse/SmartIdTSE;Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lee/cyber/smartid/SmartIdService$39;->b:Lee/cyber/smartid/SmartIdService;

    iput-object p4, p0, Lee/cyber/smartid/SmartIdService$39;->a:Ljava/lang/String;

    invoke-direct {p0, p2, p3}, Lee/cyber/smartid/tse/network/RPCCallback;-><init>(Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCRequest;Lee/cyber/smartid/tse/SmartIdTSE;)V

    return-void
.end method


# virtual methods
.method public onFailure(Lc/b;Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCError;Ljava/lang/Throwable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lc/b<",
            "Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCResponse<",
            "Lee/cyber/smartid/dto/jsonrpc/result/GetRegTokenNonceResult;",
            ">;>;",
            "Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCError;",
            "Ljava/lang/Throwable;",
            ")V"
        }
    .end annotation

    .line 1
    iget-object p1, p0, Lee/cyber/smartid/SmartIdService$39;->b:Lee/cyber/smartid/SmartIdService;

    new-instance v0, Lee/cyber/smartid/SmartIdService$39$1;

    invoke-direct {v0, p0, p2, p3}, Lee/cyber/smartid/SmartIdService$39$1;-><init>(Lee/cyber/smartid/SmartIdService$39;Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCError;Ljava/lang/Throwable;)V

    invoke-static {p1, v0}, Lee/cyber/smartid/SmartIdService;->a(Lee/cyber/smartid/SmartIdService;Ljava/lang/Runnable;)V

    return-void
.end method

.method public onSuccess(Lc/b;Lc/r;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lc/b<",
            "Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCResponse<",
            "Lee/cyber/smartid/dto/jsonrpc/result/GetRegTokenNonceResult;",
            ">;>;",
            "Lc/r<",
            "Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCResponse<",
            "Lee/cyber/smartid/dto/jsonrpc/result/GetRegTokenNonceResult;",
            ">;>;)V"
        }
    .end annotation

    .line 1
    iget-object p1, p0, Lee/cyber/smartid/SmartIdService$39;->b:Lee/cyber/smartid/SmartIdService;

    iget-object v0, p0, Lee/cyber/smartid/SmartIdService$39;->a:Ljava/lang/String;

    const-class v1, Lee/cyber/smartid/inter/GetRegistrationTokenListener;

    const/4 v2, 0x1

    invoke-static {p1, v0, v2, v1}, Lee/cyber/smartid/SmartIdService;->a(Lee/cyber/smartid/SmartIdService;Ljava/lang/String;ZLjava/lang/Class;)Lee/cyber/smartid/inter/ServiceListener;

    move-result-object p1

    check-cast p1, Lee/cyber/smartid/inter/GetRegistrationTokenListener;

    if-nez p1, :cond_0

    return-void

    .line 2
    :cond_0
    iget-object v6, p0, Lee/cyber/smartid/SmartIdService$39;->a:Ljava/lang/String;

    new-instance v7, Lee/cyber/smartid/dto/jsonrpc/resp/GetRegistrationTokenResp;

    invoke-virtual {p2}, Lc/r;->e()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCResponse;

    invoke-virtual {v0}, Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCResponse;->getResult()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lee/cyber/smartid/dto/jsonrpc/result/GetRegTokenNonceResult;

    invoke-virtual {v0}, Lee/cyber/smartid/dto/jsonrpc/result/GetRegTokenNonceResult;->getNonce()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2}, Lc/r;->e()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCResponse;

    invoke-virtual {v0}, Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCResponse;->getResult()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lee/cyber/smartid/dto/jsonrpc/result/GetRegTokenNonceResult;

    invoke-virtual {v0}, Lee/cyber/smartid/dto/jsonrpc/result/GetRegTokenNonceResult;->getToken()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p2}, Lc/r;->e()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCResponse;

    invoke-virtual {p2}, Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCResponse;->getResult()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lee/cyber/smartid/dto/jsonrpc/result/GetRegTokenNonceResult;

    invoke-virtual {p2}, Lee/cyber/smartid/dto/jsonrpc/result/GetRegTokenNonceResult;->getTimeToLiveSec()J

    move-result-wide v4

    move-object v0, v7

    move-object v1, v6

    invoke-direct/range {v0 .. v5}, Lee/cyber/smartid/dto/jsonrpc/resp/GetRegistrationTokenResp;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;J)V

    invoke-interface {p1, v6, v7}, Lee/cyber/smartid/inter/GetRegistrationTokenListener;->onGetRegistrationTokenSuccess(Ljava/lang/String;Lee/cyber/smartid/dto/jsonrpc/resp/GetRegistrationTokenResp;)V

    return-void
.end method

.method public onValidate(Lc/b;Lc/r;)Z
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lc/b<",
            "Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCResponse<",
            "Lee/cyber/smartid/dto/jsonrpc/result/GetRegTokenNonceResult;",
            ">;>;",
            "Lc/r<",
            "Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCResponse<",
            "Lee/cyber/smartid/dto/jsonrpc/result/GetRegTokenNonceResult;",
            ">;>;)Z"
        }
    .end annotation

    .line 1
    invoke-static {p2}, Lee/cyber/smartid/util/Util;->validateResponse(Lc/r;)Z

    move-result p1

    return p1
.end method
