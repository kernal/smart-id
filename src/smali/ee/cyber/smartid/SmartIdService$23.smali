.class Lee/cyber/smartid/SmartIdService$23;
.super Ljava/lang/Object;
.source "SmartIdService.java"

# interfaces
.implements Lee/cyber/smartid/tse/inter/SubmitClientSecondPartListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lee/cyber/smartid/SmartIdService;->b(Ljava/lang/String;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lee/cyber/smartid/SmartIdService;


# direct methods
.method constructor <init>(Lee/cyber/smartid/SmartIdService;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lee/cyber/smartid/SmartIdService$23;->a:Lee/cyber/smartid/SmartIdService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onSubmitClientSecondPartFailed(Ljava/lang/String;Lee/cyber/smartid/tse/dto/TSEError;)V
    .locals 2

    .line 1
    invoke-static {}, Lee/cyber/smartid/SmartIdService;->a()Lee/cyber/smartid/util/Log;

    move-result-object p1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "retryLocalPendingState ACCOUNT_KEY_TYPE_AUTHENTICATION onSubmitClientSecondPartFailed: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    return-void
.end method

.method public onSubmitClientSecondPartSuccess(Ljava/lang/String;Lee/cyber/smartid/tse/dto/jsonrpc/resp/SubmitClientSecondPartResp;)V
    .locals 2

    .line 1
    invoke-static {}, Lee/cyber/smartid/SmartIdService;->a()Lee/cyber/smartid/util/Log;

    move-result-object p1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "retryLocalPendingState ACCOUNT_KEY_TYPE_AUTHENTICATION onSubmitClientSecondPartSuccess: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    return-void
.end method
