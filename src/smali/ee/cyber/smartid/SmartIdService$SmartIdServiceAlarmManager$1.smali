.class Lee/cyber/smartid/SmartIdService$SmartIdServiceAlarmManager$1;
.super Ljava/lang/Object;
.source "SmartIdService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lee/cyber/smartid/SmartIdService$SmartIdServiceAlarmManager;->onReceive(Landroid/content/Context;Landroid/content/Intent;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Landroid/content/Context;

.field final synthetic b:Landroid/content/Intent;

.field final synthetic c:Lee/cyber/smartid/SmartIdService$SmartIdServiceAlarmManager;


# direct methods
.method constructor <init>(Lee/cyber/smartid/SmartIdService$SmartIdServiceAlarmManager;Landroid/content/Context;Landroid/content/Intent;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lee/cyber/smartid/SmartIdService$SmartIdServiceAlarmManager$1;->c:Lee/cyber/smartid/SmartIdService$SmartIdServiceAlarmManager;

    iput-object p2, p0, Lee/cyber/smartid/SmartIdService$SmartIdServiceAlarmManager$1;->a:Landroid/content/Context;

    iput-object p3, p0, Lee/cyber/smartid/SmartIdService$SmartIdServiceAlarmManager$1;->b:Landroid/content/Intent;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .line 1
    :try_start_0
    iget-object v0, p0, Lee/cyber/smartid/SmartIdService$SmartIdServiceAlarmManager$1;->c:Lee/cyber/smartid/SmartIdService$SmartIdServiceAlarmManager;

    iget-object v1, p0, Lee/cyber/smartid/SmartIdService$SmartIdServiceAlarmManager$1;->a:Landroid/content/Context;

    iget-object v2, p0, Lee/cyber/smartid/SmartIdService$SmartIdServiceAlarmManager$1;->b:Landroid/content/Intent;

    invoke-static {v0, v1, v2}, Lee/cyber/smartid/SmartIdService$SmartIdServiceAlarmManager;->a(Lee/cyber/smartid/SmartIdService$SmartIdServiceAlarmManager;Landroid/content/Context;Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    goto :goto_1

    :catch_0
    move-exception v0

    .line 3
    :try_start_1
    iget-object v1, p0, Lee/cyber/smartid/SmartIdService$SmartIdServiceAlarmManager$1;->c:Lee/cyber/smartid/SmartIdService$SmartIdServiceAlarmManager;

    invoke-static {v1}, Lee/cyber/smartid/SmartIdService$SmartIdServiceAlarmManager;->a(Lee/cyber/smartid/SmartIdService$SmartIdServiceAlarmManager;)Lee/cyber/smartid/util/Log;

    move-result-object v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    const-string v2, "onReceive"

    :try_start_2
    invoke-virtual {v1, v2, v0}, Lee/cyber/smartid/util/Log;->e(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 2
    :goto_0
    iget-object v0, p0, Lee/cyber/smartid/SmartIdService$SmartIdServiceAlarmManager$1;->c:Lee/cyber/smartid/SmartIdService$SmartIdServiceAlarmManager;

    iget-object v1, p0, Lee/cyber/smartid/SmartIdService$SmartIdServiceAlarmManager$1;->a:Landroid/content/Context;

    invoke-static {v0, v1}, Lee/cyber/smartid/SmartIdService$SmartIdServiceAlarmManager;->a(Lee/cyber/smartid/SmartIdService$SmartIdServiceAlarmManager;Landroid/content/Context;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    invoke-static {v0}, Lee/cyber/smartid/util/Util;->releaseWakeLock(Landroid/os/PowerManager$WakeLock;)V

    return-void

    .line 4
    :goto_1
    iget-object v1, p0, Lee/cyber/smartid/SmartIdService$SmartIdServiceAlarmManager$1;->c:Lee/cyber/smartid/SmartIdService$SmartIdServiceAlarmManager;

    iget-object v2, p0, Lee/cyber/smartid/SmartIdService$SmartIdServiceAlarmManager$1;->a:Landroid/content/Context;

    invoke-static {v1, v2}, Lee/cyber/smartid/SmartIdService$SmartIdServiceAlarmManager;->a(Lee/cyber/smartid/SmartIdService$SmartIdServiceAlarmManager;Landroid/content/Context;)Landroid/os/PowerManager$WakeLock;

    move-result-object v1

    invoke-static {v1}, Lee/cyber/smartid/util/Util;->releaseWakeLock(Landroid/os/PowerManager$WakeLock;)V

    throw v0
.end method
