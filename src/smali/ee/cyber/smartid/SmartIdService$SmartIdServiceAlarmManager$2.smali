.class Lee/cyber/smartid/SmartIdService$SmartIdServiceAlarmManager$2;
.super Ljava/lang/Object;
.source "SmartIdService.java"

# interfaces
.implements Lee/cyber/smartid/inter/InitServiceListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lee/cyber/smartid/SmartIdService$SmartIdServiceAlarmManager;->a(Landroid/content/Context;Landroid/content/Intent;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Landroid/content/Context;

.field final synthetic b:Landroid/content/Intent;

.field final synthetic c:Lee/cyber/smartid/SmartIdService$SmartIdServiceAlarmManager;


# direct methods
.method constructor <init>(Lee/cyber/smartid/SmartIdService$SmartIdServiceAlarmManager;Landroid/content/Context;Landroid/content/Intent;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lee/cyber/smartid/SmartIdService$SmartIdServiceAlarmManager$2;->c:Lee/cyber/smartid/SmartIdService$SmartIdServiceAlarmManager;

    iput-object p2, p0, Lee/cyber/smartid/SmartIdService$SmartIdServiceAlarmManager$2;->a:Landroid/content/Context;

    iput-object p3, p0, Lee/cyber/smartid/SmartIdService$SmartIdServiceAlarmManager$2;->b:Landroid/content/Intent;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a()V
    .locals 4

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/SmartIdService$SmartIdServiceAlarmManager$2;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lee/cyber/smartid/SmartIdService;->getInstance(Landroid/content/Context;)Lee/cyber/smartid/SmartIdService;

    move-result-object v0

    .line 2
    iget-object v1, p0, Lee/cyber/smartid/SmartIdService$SmartIdServiceAlarmManager$2;->b:Landroid/content/Intent;

    if-eqz v1, :cond_0

    .line 3
    invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lee/cyber/smartid/SmartIdService$SmartIdServiceAlarmManager$2;->b:Landroid/content/Intent;

    const-string v3, "ee.cyber.smartid.EXTRA_ALARM_TARGET_ID"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v0, v1, v2, v3, v3}, Lee/cyber/smartid/SmartIdService;->a(Lee/cyber/smartid/SmartIdService;Ljava/lang/String;Ljava/lang/String;ZZ)V

    .line 4
    :cond_0
    iget-object v0, p0, Lee/cyber/smartid/SmartIdService$SmartIdServiceAlarmManager$2;->c:Lee/cyber/smartid/SmartIdService$SmartIdServiceAlarmManager;

    iget-object v1, p0, Lee/cyber/smartid/SmartIdService$SmartIdServiceAlarmManager$2;->a:Landroid/content/Context;

    invoke-static {v0, v1}, Lee/cyber/smartid/SmartIdService$SmartIdServiceAlarmManager;->a(Lee/cyber/smartid/SmartIdService$SmartIdServiceAlarmManager;Landroid/content/Context;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    invoke-static {v0}, Lee/cyber/smartid/util/Util;->releaseWakeLock(Landroid/os/PowerManager$WakeLock;)V

    return-void
.end method


# virtual methods
.method public onInitServiceFailed(Ljava/lang/String;Lee/cyber/smartid/dto/SmartIdError;)V
    .locals 2

    .line 1
    iget-object p1, p0, Lee/cyber/smartid/SmartIdService$SmartIdServiceAlarmManager$2;->c:Lee/cyber/smartid/SmartIdService$SmartIdServiceAlarmManager;

    invoke-static {p1}, Lee/cyber/smartid/SmartIdService$SmartIdServiceAlarmManager;->a(Lee/cyber/smartid/SmartIdService$SmartIdServiceAlarmManager;)Lee/cyber/smartid/util/Log;

    move-result-object p1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "handleOnReceive - onInitServiceFailed: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Lee/cyber/smartid/util/Log;->e(Ljava/lang/String;)V

    .line 2
    invoke-direct {p0}, Lee/cyber/smartid/SmartIdService$SmartIdServiceAlarmManager$2;->a()V

    return-void
.end method

.method public onInitServiceInteractiveUpgradeFailed(Ljava/lang/String;ILee/cyber/smartid/dto/SmartIdError;)V
    .locals 1

    .line 1
    iget-object p1, p0, Lee/cyber/smartid/SmartIdService$SmartIdServiceAlarmManager$2;->c:Lee/cyber/smartid/SmartIdService$SmartIdServiceAlarmManager;

    invoke-static {p1}, Lee/cyber/smartid/SmartIdService$SmartIdServiceAlarmManager;->a(Lee/cyber/smartid/SmartIdService$SmartIdServiceAlarmManager;)Lee/cyber/smartid/util/Log;

    move-result-object p1

    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "handleOnReceive - onInitServiceInteractiveUpgradeFailed - interactiveUpgradeType: "

    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Lee/cyber/smartid/util/Log;->e(Ljava/lang/String;)V

    .line 2
    invoke-direct {p0}, Lee/cyber/smartid/SmartIdService$SmartIdServiceAlarmManager$2;->a()V

    return-void
.end method

.method public onInitServiceInteractiveUpgradeStarted(Ljava/lang/String;I)V
    .locals 2

    .line 1
    iget-object p1, p0, Lee/cyber/smartid/SmartIdService$SmartIdServiceAlarmManager$2;->c:Lee/cyber/smartid/SmartIdService$SmartIdServiceAlarmManager;

    invoke-static {p1}, Lee/cyber/smartid/SmartIdService$SmartIdServiceAlarmManager;->a(Lee/cyber/smartid/SmartIdService$SmartIdServiceAlarmManager;)Lee/cyber/smartid/util/Log;

    move-result-object p1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "handleOnReceive - onInitServiceInteractiveUpgradeStarted - interactiveUpgradeType: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    return-void
.end method

.method public onInitServiceSuccess(Ljava/lang/String;[Lee/cyber/smartid/dto/SmartIdError;)V
    .locals 2

    .line 1
    iget-object p1, p0, Lee/cyber/smartid/SmartIdService$SmartIdServiceAlarmManager$2;->c:Lee/cyber/smartid/SmartIdService$SmartIdServiceAlarmManager;

    invoke-static {p1}, Lee/cyber/smartid/SmartIdService$SmartIdServiceAlarmManager;->a(Lee/cyber/smartid/SmartIdService$SmartIdServiceAlarmManager;)Lee/cyber/smartid/util/Log;

    move-result-object p1

    const-string p2, "handleOnReceive - onInitServiceSuccess"

    invoke-virtual {p1, p2}, Lee/cyber/smartid/util/Log;->d(Ljava/lang/String;)V

    .line 2
    iget-object p1, p0, Lee/cyber/smartid/SmartIdService$SmartIdServiceAlarmManager$2;->c:Lee/cyber/smartid/SmartIdService$SmartIdServiceAlarmManager;

    iget-object p2, p0, Lee/cyber/smartid/SmartIdService$SmartIdServiceAlarmManager$2;->a:Landroid/content/Context;

    iget-object v0, p0, Lee/cyber/smartid/SmartIdService$SmartIdServiceAlarmManager$2;->b:Landroid/content/Intent;

    const/4 v1, 0x1

    invoke-static {p1, p2, v0, v1}, Lee/cyber/smartid/SmartIdService$SmartIdServiceAlarmManager;->a(Lee/cyber/smartid/SmartIdService$SmartIdServiceAlarmManager;Landroid/content/Context;Landroid/content/Intent;Z)V

    .line 3
    iget-object p1, p0, Lee/cyber/smartid/SmartIdService$SmartIdServiceAlarmManager$2;->c:Lee/cyber/smartid/SmartIdService$SmartIdServiceAlarmManager;

    iget-object p2, p0, Lee/cyber/smartid/SmartIdService$SmartIdServiceAlarmManager$2;->a:Landroid/content/Context;

    invoke-static {p1, p2}, Lee/cyber/smartid/SmartIdService$SmartIdServiceAlarmManager;->a(Lee/cyber/smartid/SmartIdService$SmartIdServiceAlarmManager;Landroid/content/Context;)Landroid/os/PowerManager$WakeLock;

    move-result-object p1

    invoke-static {p1}, Lee/cyber/smartid/util/Util;->releaseWakeLock(Landroid/os/PowerManager$WakeLock;)V

    return-void
.end method
