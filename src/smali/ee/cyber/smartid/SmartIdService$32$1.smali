.class Lee/cyber/smartid/SmartIdService$32$1;
.super Ljava/lang/Object;
.source "SmartIdService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lee/cyber/smartid/SmartIdService$32;->onSuccess(Lc/b;Lc/r;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lc/r;

.field final synthetic b:Lee/cyber/smartid/SmartIdService$32;


# direct methods
.method constructor <init>(Lee/cyber/smartid/SmartIdService$32;Lc/r;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lee/cyber/smartid/SmartIdService$32$1;->b:Lee/cyber/smartid/SmartIdService$32;

    iput-object p2, p0, Lee/cyber/smartid/SmartIdService$32$1;->a:Lc/r;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .line 1
    iget-object v0, p0, Lee/cyber/smartid/SmartIdService$32$1;->b:Lee/cyber/smartid/SmartIdService$32;

    iget-object v1, v0, Lee/cyber/smartid/SmartIdService$32;->d:Lee/cyber/smartid/SmartIdService;

    iget-object v0, v0, Lee/cyber/smartid/SmartIdService$32;->c:Ljava/lang/String;

    const-class v2, Lee/cyber/smartid/inter/UpdateDeviceListener;

    const/4 v3, 0x1

    invoke-static {v1, v0, v3, v2}, Lee/cyber/smartid/SmartIdService;->a(Lee/cyber/smartid/SmartIdService;Ljava/lang/String;ZLjava/lang/Class;)Lee/cyber/smartid/inter/ServiceListener;

    move-result-object v0

    check-cast v0, Lee/cyber/smartid/inter/UpdateDeviceListener;

    if-nez v0, :cond_0

    return-void

    .line 2
    :cond_0
    iget-object v1, p0, Lee/cyber/smartid/SmartIdService$32$1;->b:Lee/cyber/smartid/SmartIdService$32;

    iget-object v1, v1, Lee/cyber/smartid/SmartIdService$32;->c:Ljava/lang/String;

    new-instance v2, Lee/cyber/smartid/dto/jsonrpc/resp/UpdateDeviceResp;

    iget-object v3, p0, Lee/cyber/smartid/SmartIdService$32$1;->a:Lc/r;

    invoke-virtual {v3}, Lc/r;->e()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCResponse;

    invoke-virtual {v3}, Lee/cyber/smartid/tse/dto/jsonrpc/base/RPCResponse;->getResult()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lee/cyber/smartid/dto/jsonrpc/result/UpdateDeviceResult;

    invoke-virtual {v3}, Lee/cyber/smartid/dto/jsonrpc/result/UpdateDeviceResult;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v1, v3}, Lee/cyber/smartid/dto/jsonrpc/resp/UpdateDeviceResp;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1, v2}, Lee/cyber/smartid/inter/UpdateDeviceListener;->onUpdateDeviceSuccess(Ljava/lang/String;Lee/cyber/smartid/dto/jsonrpc/resp/UpdateDeviceResp;)V

    .line 3
    iget-object v0, p0, Lee/cyber/smartid/SmartIdService$32$1;->b:Lee/cyber/smartid/SmartIdService$32;

    iget-object v0, v0, Lee/cyber/smartid/SmartIdService$32;->d:Lee/cyber/smartid/SmartIdService;

    invoke-static {v0}, Lee/cyber/smartid/SmartIdService;->p(Lee/cyber/smartid/SmartIdService;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    invoke-static {v0}, Lee/cyber/smartid/util/Util;->releaseWakeLock(Landroid/os/PowerManager$WakeLock;)V

    return-void
.end method
