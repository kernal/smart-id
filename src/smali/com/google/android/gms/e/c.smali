.class public final Lcom/google/android/gms/e/c;
.super Ljava/lang/Object;


# static fields
.field public static final a:Lcom/google/android/gms/common/api/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/common/api/a<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final b:Lcom/google/android/gms/e/d;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field private static final c:Lcom/google/android/gms/common/api/a$g;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/common/api/a$g<",
            "Lcom/google/android/gms/d/i/n;",
            ">;"
        }
    .end annotation
.end field

.field private static final d:Lcom/google/android/gms/common/api/a$a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/common/api/a$a<",
            "Lcom/google/android/gms/d/i/n;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private static final e:Lcom/google/android/gms/e/q;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    new-instance v0, Lcom/google/android/gms/common/api/a$g;

    invoke-direct {v0}, Lcom/google/android/gms/common/api/a$g;-><init>()V

    sput-object v0, Lcom/google/android/gms/e/c;->c:Lcom/google/android/gms/common/api/a$g;

    new-instance v0, Lcom/google/android/gms/e/p;

    invoke-direct {v0}, Lcom/google/android/gms/e/p;-><init>()V

    sput-object v0, Lcom/google/android/gms/e/c;->d:Lcom/google/android/gms/common/api/a$a;

    new-instance v0, Lcom/google/android/gms/common/api/a;

    sget-object v1, Lcom/google/android/gms/e/c;->d:Lcom/google/android/gms/common/api/a$a;

    sget-object v2, Lcom/google/android/gms/e/c;->c:Lcom/google/android/gms/common/api/a$g;

    const-string v3, "SafetyNet.API"

    invoke-direct {v0, v3, v1, v2}, Lcom/google/android/gms/common/api/a;-><init>(Ljava/lang/String;Lcom/google/android/gms/common/api/a$a;Lcom/google/android/gms/common/api/a$g;)V

    sput-object v0, Lcom/google/android/gms/e/c;->a:Lcom/google/android/gms/common/api/a;

    new-instance v0, Lcom/google/android/gms/d/i/k;

    invoke-direct {v0}, Lcom/google/android/gms/d/i/k;-><init>()V

    sput-object v0, Lcom/google/android/gms/e/c;->b:Lcom/google/android/gms/e/d;

    new-instance v0, Lcom/google/android/gms/d/i/o;

    invoke-direct {v0}, Lcom/google/android/gms/d/i/o;-><init>()V

    sput-object v0, Lcom/google/android/gms/e/c;->e:Lcom/google/android/gms/e/q;

    return-void
.end method

.method public static a(Landroid/content/Context;)Lcom/google/android/gms/e/e;
    .locals 1

    new-instance v0, Lcom/google/android/gms/e/e;

    invoke-direct {v0, p0}, Lcom/google/android/gms/e/e;-><init>(Landroid/content/Context;)V

    return-object v0
.end method
