.class public final Lcom/google/android/gms/d/h/ld;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/d/h/bu;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/gms/d/h/bu<",
        "Lcom/google/android/gms/d/h/lg;",
        ">;"
    }
.end annotation


# static fields
.field private static a:Lcom/google/android/gms/d/h/ld;


# instance fields
.field private final b:Lcom/google/android/gms/d/h/bu;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/d/h/bu<",
            "Lcom/google/android/gms/d/h/lg;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 13
    new-instance v0, Lcom/google/android/gms/d/h/ld;

    invoke-direct {v0}, Lcom/google/android/gms/d/h/ld;-><init>()V

    sput-object v0, Lcom/google/android/gms/d/h/ld;->a:Lcom/google/android/gms/d/h/ld;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .line 8
    new-instance v0, Lcom/google/android/gms/d/h/lf;

    invoke-direct {v0}, Lcom/google/android/gms/d/h/lf;-><init>()V

    invoke-static {v0}, Lcom/google/android/gms/d/h/bt;->a(Ljava/lang/Object;)Lcom/google/android/gms/d/h/bu;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/d/h/ld;-><init>(Lcom/google/android/gms/d/h/bu;)V

    return-void
.end method

.method private constructor <init>(Lcom/google/android/gms/d/h/bu;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/gms/d/h/bu<",
            "Lcom/google/android/gms/d/h/lg;",
            ">;)V"
        }
    .end annotation

    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 6
    invoke-static {p1}, Lcom/google/android/gms/d/h/bt;->a(Lcom/google/android/gms/d/h/bu;)Lcom/google/android/gms/d/h/bu;

    move-result-object p1

    iput-object p1, p0, Lcom/google/android/gms/d/h/ld;->b:Lcom/google/android/gms/d/h/bu;

    return-void
.end method

.method public static b()Z
    .locals 1

    .line 1
    sget-object v0, Lcom/google/android/gms/d/h/ld;->a:Lcom/google/android/gms/d/h/ld;

    invoke-virtual {v0}, Lcom/google/android/gms/d/h/ld;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/d/h/lg;

    invoke-interface {v0}, Lcom/google/android/gms/d/h/lg;->a()Z

    move-result v0

    return v0
.end method

.method public static c()Z
    .locals 1

    .line 2
    sget-object v0, Lcom/google/android/gms/d/h/ld;->a:Lcom/google/android/gms/d/h/ld;

    invoke-virtual {v0}, Lcom/google/android/gms/d/h/ld;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/d/h/lg;

    invoke-interface {v0}, Lcom/google/android/gms/d/h/lg;->b()Z

    move-result v0

    return v0
.end method

.method public static d()Z
    .locals 1

    .line 3
    sget-object v0, Lcom/google/android/gms/d/h/ld;->a:Lcom/google/android/gms/d/h/ld;

    invoke-virtual {v0}, Lcom/google/android/gms/d/h/ld;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/d/h/lg;

    invoke-interface {v0}, Lcom/google/android/gms/d/h/lg;->c()Z

    move-result v0

    return v0
.end method

.method public static e()Z
    .locals 1

    .line 4
    sget-object v0, Lcom/google/android/gms/d/h/ld;->a:Lcom/google/android/gms/d/h/ld;

    invoke-virtual {v0}, Lcom/google/android/gms/d/h/ld;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/d/h/lg;

    invoke-interface {v0}, Lcom/google/android/gms/d/h/lg;->d()Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final synthetic a()Ljava/lang/Object;
    .locals 1

    .line 11
    iget-object v0, p0, Lcom/google/android/gms/d/h/ld;->b:Lcom/google/android/gms/d/h/bu;

    invoke-interface {v0}, Lcom/google/android/gms/d/h/bu;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/d/h/lg;

    return-object v0
.end method
