.class public final Lcom/google/android/gms/d/h/iy;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/d/h/bu;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/gms/d/h/bu<",
        "Lcom/google/android/gms/d/h/jc;",
        ">;"
    }
.end annotation


# static fields
.field private static a:Lcom/google/android/gms/d/h/iy;


# instance fields
.field private final b:Lcom/google/android/gms/d/h/bu;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/d/h/bu<",
            "Lcom/google/android/gms/d/h/jc;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 12
    new-instance v0, Lcom/google/android/gms/d/h/iy;

    invoke-direct {v0}, Lcom/google/android/gms/d/h/iy;-><init>()V

    sput-object v0, Lcom/google/android/gms/d/h/iy;->a:Lcom/google/android/gms/d/h/iy;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .line 7
    new-instance v0, Lcom/google/android/gms/d/h/jb;

    invoke-direct {v0}, Lcom/google/android/gms/d/h/jb;-><init>()V

    invoke-static {v0}, Lcom/google/android/gms/d/h/bt;->a(Ljava/lang/Object;)Lcom/google/android/gms/d/h/bu;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/d/h/iy;-><init>(Lcom/google/android/gms/d/h/bu;)V

    return-void
.end method

.method private constructor <init>(Lcom/google/android/gms/d/h/bu;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/gms/d/h/bu<",
            "Lcom/google/android/gms/d/h/jc;",
            ">;)V"
        }
    .end annotation

    .line 4
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 5
    invoke-static {p1}, Lcom/google/android/gms/d/h/bt;->a(Lcom/google/android/gms/d/h/bu;)Lcom/google/android/gms/d/h/bu;

    move-result-object p1

    iput-object p1, p0, Lcom/google/android/gms/d/h/iy;->b:Lcom/google/android/gms/d/h/bu;

    return-void
.end method

.method public static b()Z
    .locals 1

    .line 1
    sget-object v0, Lcom/google/android/gms/d/h/iy;->a:Lcom/google/android/gms/d/h/iy;

    invoke-virtual {v0}, Lcom/google/android/gms/d/h/iy;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/d/h/jc;

    invoke-interface {v0}, Lcom/google/android/gms/d/h/jc;->a()Z

    move-result v0

    return v0
.end method

.method public static c()Z
    .locals 1

    .line 2
    sget-object v0, Lcom/google/android/gms/d/h/iy;->a:Lcom/google/android/gms/d/h/iy;

    invoke-virtual {v0}, Lcom/google/android/gms/d/h/iy;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/d/h/jc;

    invoke-interface {v0}, Lcom/google/android/gms/d/h/jc;->b()Z

    move-result v0

    return v0
.end method

.method public static d()Z
    .locals 1

    .line 3
    sget-object v0, Lcom/google/android/gms/d/h/iy;->a:Lcom/google/android/gms/d/h/iy;

    invoke-virtual {v0}, Lcom/google/android/gms/d/h/iy;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/d/h/jc;

    invoke-interface {v0}, Lcom/google/android/gms/d/h/jc;->c()Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final synthetic a()Ljava/lang/Object;
    .locals 1

    .line 10
    iget-object v0, p0, Lcom/google/android/gms/d/h/iy;->b:Lcom/google/android/gms/d/h/bu;

    invoke-interface {v0}, Lcom/google/android/gms/d/h/bu;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/d/h/jc;

    return-object v0
.end method
