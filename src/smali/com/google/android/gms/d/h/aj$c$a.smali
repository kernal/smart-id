.class public final Lcom/google/android/gms/d/h/aj$c$a;
.super Lcom/google/android/gms/d/h/ds$a;

# interfaces
.implements Lcom/google/android/gms/d/h/fg;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/d/h/aj$c;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/gms/d/h/ds$a<",
        "Lcom/google/android/gms/d/h/aj$c;",
        "Lcom/google/android/gms/d/h/aj$c$a;",
        ">;",
        "Lcom/google/android/gms/d/h/fg;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 1

    .line 1
    invoke-static {}, Lcom/google/android/gms/d/h/aj$c;->k()Lcom/google/android/gms/d/h/aj$c;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/d/h/ds$a;-><init>(Lcom/google/android/gms/d/h/ds;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/gms/d/h/ai;)V
    .locals 0

    .line 36
    invoke-direct {p0}, Lcom/google/android/gms/d/h/aj$c$a;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(ILcom/google/android/gms/d/h/aj$e$a;)Lcom/google/android/gms/d/h/aj$c$a;
    .locals 1

    .line 11
    invoke-virtual {p0}, Lcom/google/android/gms/d/h/ds$a;->p()V

    .line 12
    iget-object v0, p0, Lcom/google/android/gms/d/h/aj$c$a;->a:Lcom/google/android/gms/d/h/ds;

    check-cast v0, Lcom/google/android/gms/d/h/aj$c;

    invoke-static {v0, p1, p2}, Lcom/google/android/gms/d/h/aj$c;->a(Lcom/google/android/gms/d/h/aj$c;ILcom/google/android/gms/d/h/aj$e$a;)V

    return-object p0
.end method

.method public final a(ILcom/google/android/gms/d/h/aj$e;)Lcom/google/android/gms/d/h/aj$c$a;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/google/android/gms/d/h/ds$a;->p()V

    .line 9
    iget-object v0, p0, Lcom/google/android/gms/d/h/aj$c$a;->a:Lcom/google/android/gms/d/h/ds;

    check-cast v0, Lcom/google/android/gms/d/h/aj$c;

    invoke-static {v0, p1, p2}, Lcom/google/android/gms/d/h/aj$c;->a(Lcom/google/android/gms/d/h/aj$c;ILcom/google/android/gms/d/h/aj$e;)V

    return-object p0
.end method

.method public final a(J)Lcom/google/android/gms/d/h/aj$c$a;
    .locals 1

    .line 29
    invoke-virtual {p0}, Lcom/google/android/gms/d/h/ds$a;->p()V

    .line 30
    iget-object v0, p0, Lcom/google/android/gms/d/h/aj$c$a;->a:Lcom/google/android/gms/d/h/ds;

    check-cast v0, Lcom/google/android/gms/d/h/aj$c;

    invoke-static {v0, p1, p2}, Lcom/google/android/gms/d/h/aj$c;->a(Lcom/google/android/gms/d/h/aj$c;J)V

    return-object p0
.end method

.method public final a(Lcom/google/android/gms/d/h/aj$e$a;)Lcom/google/android/gms/d/h/aj$c$a;
    .locals 1

    .line 17
    invoke-virtual {p0}, Lcom/google/android/gms/d/h/ds$a;->p()V

    .line 18
    iget-object v0, p0, Lcom/google/android/gms/d/h/aj$c$a;->a:Lcom/google/android/gms/d/h/ds;

    check-cast v0, Lcom/google/android/gms/d/h/aj$c;

    invoke-static {v0, p1}, Lcom/google/android/gms/d/h/aj$c;->a(Lcom/google/android/gms/d/h/aj$c;Lcom/google/android/gms/d/h/aj$e$a;)V

    return-object p0
.end method

.method public final a(Lcom/google/android/gms/d/h/aj$e;)Lcom/google/android/gms/d/h/aj$c$a;
    .locals 1

    .line 14
    invoke-virtual {p0}, Lcom/google/android/gms/d/h/ds$a;->p()V

    .line 15
    iget-object v0, p0, Lcom/google/android/gms/d/h/aj$c$a;->a:Lcom/google/android/gms/d/h/ds;

    check-cast v0, Lcom/google/android/gms/d/h/aj$c;

    invoke-static {v0, p1}, Lcom/google/android/gms/d/h/aj$c;->a(Lcom/google/android/gms/d/h/aj$c;Lcom/google/android/gms/d/h/aj$e;)V

    return-object p0
.end method

.method public final a(Ljava/lang/String;)Lcom/google/android/gms/d/h/aj$c$a;
    .locals 1

    .line 24
    invoke-virtual {p0}, Lcom/google/android/gms/d/h/ds$a;->p()V

    .line 25
    iget-object v0, p0, Lcom/google/android/gms/d/h/aj$c$a;->a:Lcom/google/android/gms/d/h/ds;

    check-cast v0, Lcom/google/android/gms/d/h/aj$c;

    invoke-static {v0, p1}, Lcom/google/android/gms/d/h/aj$c;->a(Lcom/google/android/gms/d/h/aj$c;Ljava/lang/String;)V

    return-object p0
.end method

.method public final a(I)Lcom/google/android/gms/d/h/aj$e;
    .locals 1

    .line 7
    iget-object v0, p0, Lcom/google/android/gms/d/h/aj$c$a;->a:Lcom/google/android/gms/d/h/ds;

    check-cast v0, Lcom/google/android/gms/d/h/aj$c;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/d/h/aj$c;->a(I)Lcom/google/android/gms/d/h/aj$e;

    move-result-object p1

    return-object p1
.end method

.method public final a()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/google/android/gms/d/h/aj$e;",
            ">;"
        }
    .end annotation

    .line 3
    iget-object v0, p0, Lcom/google/android/gms/d/h/aj$c$a;->a:Lcom/google/android/gms/d/h/ds;

    check-cast v0, Lcom/google/android/gms/d/h/aj$c;

    .line 4
    invoke-virtual {v0}, Lcom/google/android/gms/d/h/aj$c;->a()Ljava/util/List;

    move-result-object v0

    .line 5
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final b()I
    .locals 1

    .line 6
    iget-object v0, p0, Lcom/google/android/gms/d/h/aj$c$a;->a:Lcom/google/android/gms/d/h/ds;

    check-cast v0, Lcom/google/android/gms/d/h/aj$c;

    invoke-virtual {v0}, Lcom/google/android/gms/d/h/aj$c;->b()I

    move-result v0

    return v0
.end method

.method public final b(I)Lcom/google/android/gms/d/h/aj$c$a;
    .locals 1

    .line 20
    invoke-virtual {p0}, Lcom/google/android/gms/d/h/ds$a;->p()V

    .line 21
    iget-object v0, p0, Lcom/google/android/gms/d/h/aj$c$a;->a:Lcom/google/android/gms/d/h/ds;

    check-cast v0, Lcom/google/android/gms/d/h/aj$c;

    invoke-static {v0, p1}, Lcom/google/android/gms/d/h/aj$c;->a(Lcom/google/android/gms/d/h/aj$c;I)V

    return-object p0
.end method

.method public final b(J)Lcom/google/android/gms/d/h/aj$c$a;
    .locals 1

    .line 33
    invoke-virtual {p0}, Lcom/google/android/gms/d/h/ds$a;->p()V

    .line 34
    iget-object v0, p0, Lcom/google/android/gms/d/h/aj$c$a;->a:Lcom/google/android/gms/d/h/ds;

    check-cast v0, Lcom/google/android/gms/d/h/aj$c;

    invoke-static {v0, p1, p2}, Lcom/google/android/gms/d/h/aj$c;->b(Lcom/google/android/gms/d/h/aj$c;J)V

    return-object p0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .line 23
    iget-object v0, p0, Lcom/google/android/gms/d/h/aj$c$a;->a:Lcom/google/android/gms/d/h/ds;

    check-cast v0, Lcom/google/android/gms/d/h/aj$c;

    invoke-virtual {v0}, Lcom/google/android/gms/d/h/aj$c;->c()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final d()Z
    .locals 1

    .line 27
    iget-object v0, p0, Lcom/google/android/gms/d/h/aj$c$a;->a:Lcom/google/android/gms/d/h/ds;

    check-cast v0, Lcom/google/android/gms/d/h/aj$c;

    invoke-virtual {v0}, Lcom/google/android/gms/d/h/aj$c;->d()Z

    move-result v0

    return v0
.end method

.method public final e()J
    .locals 2

    .line 28
    iget-object v0, p0, Lcom/google/android/gms/d/h/aj$c$a;->a:Lcom/google/android/gms/d/h/ds;

    check-cast v0, Lcom/google/android/gms/d/h/aj$c;

    invoke-virtual {v0}, Lcom/google/android/gms/d/h/aj$c;->e()J

    move-result-wide v0

    return-wide v0
.end method

.method public final f()J
    .locals 2

    .line 32
    iget-object v0, p0, Lcom/google/android/gms/d/h/aj$c$a;->a:Lcom/google/android/gms/d/h/ds;

    check-cast v0, Lcom/google/android/gms/d/h/aj$c;

    invoke-virtual {v0}, Lcom/google/android/gms/d/h/aj$c;->g()J

    move-result-wide v0

    return-wide v0
.end method
