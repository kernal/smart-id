.class final enum Lcom/google/android/gms/d/h/dp;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/google/android/gms/d/h/dp;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/android/gms/d/h/dp;

.field public static final enum b:Lcom/google/android/gms/d/h/dp;

.field public static final enum c:Lcom/google/android/gms/d/h/dp;

.field public static final enum d:Lcom/google/android/gms/d/h/dp;

.field private static final synthetic f:[Lcom/google/android/gms/d/h/dp;


# instance fields
.field private final e:Z


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .line 5
    new-instance v0, Lcom/google/android/gms/d/h/dp;

    const/4 v1, 0x0

    const-string v2, "SCALAR"

    invoke-direct {v0, v2, v1, v1}, Lcom/google/android/gms/d/h/dp;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, Lcom/google/android/gms/d/h/dp;->a:Lcom/google/android/gms/d/h/dp;

    .line 6
    new-instance v0, Lcom/google/android/gms/d/h/dp;

    const/4 v2, 0x1

    const-string v3, "VECTOR"

    invoke-direct {v0, v3, v2, v2}, Lcom/google/android/gms/d/h/dp;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, Lcom/google/android/gms/d/h/dp;->b:Lcom/google/android/gms/d/h/dp;

    .line 7
    new-instance v0, Lcom/google/android/gms/d/h/dp;

    const/4 v3, 0x2

    const-string v4, "PACKED_VECTOR"

    invoke-direct {v0, v4, v3, v2}, Lcom/google/android/gms/d/h/dp;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, Lcom/google/android/gms/d/h/dp;->c:Lcom/google/android/gms/d/h/dp;

    .line 8
    new-instance v0, Lcom/google/android/gms/d/h/dp;

    const/4 v4, 0x3

    const-string v5, "MAP"

    invoke-direct {v0, v5, v4, v1}, Lcom/google/android/gms/d/h/dp;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, Lcom/google/android/gms/d/h/dp;->d:Lcom/google/android/gms/d/h/dp;

    const/4 v0, 0x4

    .line 9
    new-array v0, v0, [Lcom/google/android/gms/d/h/dp;

    sget-object v5, Lcom/google/android/gms/d/h/dp;->a:Lcom/google/android/gms/d/h/dp;

    aput-object v5, v0, v1

    sget-object v1, Lcom/google/android/gms/d/h/dp;->b:Lcom/google/android/gms/d/h/dp;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/gms/d/h/dp;->c:Lcom/google/android/gms/d/h/dp;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/gms/d/h/dp;->d:Lcom/google/android/gms/d/h/dp;

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/android/gms/d/h/dp;->f:[Lcom/google/android/gms/d/h/dp;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IZ)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)V"
        }
    .end annotation

    .line 2
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 3
    iput-boolean p3, p0, Lcom/google/android/gms/d/h/dp;->e:Z

    return-void
.end method

.method public static values()[Lcom/google/android/gms/d/h/dp;
    .locals 1

    .line 1
    sget-object v0, Lcom/google/android/gms/d/h/dp;->f:[Lcom/google/android/gms/d/h/dp;

    invoke-virtual {v0}, [Lcom/google/android/gms/d/h/dp;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/gms/d/h/dp;

    return-object v0
.end method
