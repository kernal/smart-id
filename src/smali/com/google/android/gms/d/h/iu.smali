.class public final Lcom/google/android/gms/d/h/iu;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/d/h/iv;


# static fields
.field private static final a:Lcom/google/android/gms/d/h/be;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/d/h/be<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private static final b:Lcom/google/android/gms/d/h/be;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/d/h/be<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .line 3
    new-instance v0, Lcom/google/android/gms/d/h/bl;

    const-string v1, "com.google.android.gms.measurement"

    .line 4
    invoke-static {v1}, Lcom/google/android/gms/d/h/bf;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/gms/d/h/bl;-><init>(Landroid/net/Uri;)V

    const-string v1, "measurement.upload_dsid_enabled"

    const/4 v2, 0x0

    .line 5
    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/d/h/bl;->a(Ljava/lang/String;Z)Lcom/google/android/gms/d/h/be;

    move-result-object v1

    sput-object v1, Lcom/google/android/gms/d/h/iu;->a:Lcom/google/android/gms/d/h/be;

    const-string v1, "measurement.id.upload_dsid_enabled"

    const-wide/16 v2, 0x0

    .line 6
    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/d/h/bl;->a(Ljava/lang/String;J)Lcom/google/android/gms/d/h/be;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/d/h/iu;->b:Lcom/google/android/gms/d/h/be;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .line 2
    sget-object v0, Lcom/google/android/gms/d/h/iu;->a:Lcom/google/android/gms/d/h/be;

    invoke-virtual {v0}, Lcom/google/android/gms/d/h/be;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method
