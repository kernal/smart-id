.class public final Lcom/google/android/gms/d/h/kd;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/d/h/bu;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/gms/d/h/bu<",
        "Lcom/google/android/gms/d/h/kc;",
        ">;"
    }
.end annotation


# static fields
.field private static a:Lcom/google/android/gms/d/h/kd;


# instance fields
.field private final b:Lcom/google/android/gms/d/h/bu;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/d/h/bu<",
            "Lcom/google/android/gms/d/h/kc;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 10
    new-instance v0, Lcom/google/android/gms/d/h/kd;

    invoke-direct {v0}, Lcom/google/android/gms/d/h/kd;-><init>()V

    sput-object v0, Lcom/google/android/gms/d/h/kd;->a:Lcom/google/android/gms/d/h/kd;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .line 5
    new-instance v0, Lcom/google/android/gms/d/h/kf;

    invoke-direct {v0}, Lcom/google/android/gms/d/h/kf;-><init>()V

    invoke-static {v0}, Lcom/google/android/gms/d/h/bt;->a(Ljava/lang/Object;)Lcom/google/android/gms/d/h/bu;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/d/h/kd;-><init>(Lcom/google/android/gms/d/h/bu;)V

    return-void
.end method

.method private constructor <init>(Lcom/google/android/gms/d/h/bu;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/gms/d/h/bu<",
            "Lcom/google/android/gms/d/h/kc;",
            ">;)V"
        }
    .end annotation

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3
    invoke-static {p1}, Lcom/google/android/gms/d/h/bt;->a(Lcom/google/android/gms/d/h/bu;)Lcom/google/android/gms/d/h/bu;

    move-result-object p1

    iput-object p1, p0, Lcom/google/android/gms/d/h/kd;->b:Lcom/google/android/gms/d/h/bu;

    return-void
.end method

.method public static b()Z
    .locals 1

    .line 1
    sget-object v0, Lcom/google/android/gms/d/h/kd;->a:Lcom/google/android/gms/d/h/kd;

    invoke-virtual {v0}, Lcom/google/android/gms/d/h/kd;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/d/h/kc;

    invoke-interface {v0}, Lcom/google/android/gms/d/h/kc;->a()Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final synthetic a()Ljava/lang/Object;
    .locals 1

    .line 8
    iget-object v0, p0, Lcom/google/android/gms/d/h/kd;->b:Lcom/google/android/gms/d/h/bu;

    invoke-interface {v0}, Lcom/google/android/gms/d/h/bu;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/d/h/kc;

    return-object v0
.end method
