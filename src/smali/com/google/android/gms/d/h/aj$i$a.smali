.class public final Lcom/google/android/gms/d/h/aj$i$a;
.super Lcom/google/android/gms/d/h/ds$a;

# interfaces
.implements Lcom/google/android/gms/d/h/fg;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/d/h/aj$i;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/gms/d/h/ds$a<",
        "Lcom/google/android/gms/d/h/aj$i;",
        "Lcom/google/android/gms/d/h/aj$i$a;",
        ">;",
        "Lcom/google/android/gms/d/h/fg;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 1

    .line 1
    invoke-static {}, Lcom/google/android/gms/d/h/aj$i;->k()Lcom/google/android/gms/d/h/aj$i;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/d/h/ds$a;-><init>(Lcom/google/android/gms/d/h/ds;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/gms/d/h/ai;)V
    .locals 0

    .line 27
    invoke-direct {p0}, Lcom/google/android/gms/d/h/aj$i$a;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/gms/d/h/aj$i$a;
    .locals 1

    .line 6
    invoke-virtual {p0}, Lcom/google/android/gms/d/h/ds$a;->p()V

    .line 7
    iget-object v0, p0, Lcom/google/android/gms/d/h/aj$i$a;->a:Lcom/google/android/gms/d/h/ds;

    check-cast v0, Lcom/google/android/gms/d/h/aj$i;

    invoke-static {v0}, Lcom/google/android/gms/d/h/aj$i;->a(Lcom/google/android/gms/d/h/aj$i;)V

    return-object p0
.end method

.method public final a(I)Lcom/google/android/gms/d/h/aj$i$a;
    .locals 1

    .line 18
    invoke-virtual {p0}, Lcom/google/android/gms/d/h/ds$a;->p()V

    .line 19
    iget-object v0, p0, Lcom/google/android/gms/d/h/aj$i$a;->a:Lcom/google/android/gms/d/h/ds;

    check-cast v0, Lcom/google/android/gms/d/h/aj$i;

    invoke-static {v0, p1}, Lcom/google/android/gms/d/h/aj$i;->a(Lcom/google/android/gms/d/h/aj$i;I)V

    return-object p0
.end method

.method public final a(Ljava/lang/Iterable;)Lcom/google/android/gms/d/h/aj$i$a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable<",
            "+",
            "Ljava/lang/Long;",
            ">;)",
            "Lcom/google/android/gms/d/h/aj$i$a;"
        }
    .end annotation

    .line 3
    invoke-virtual {p0}, Lcom/google/android/gms/d/h/ds$a;->p()V

    .line 4
    iget-object v0, p0, Lcom/google/android/gms/d/h/aj$i$a;->a:Lcom/google/android/gms/d/h/ds;

    check-cast v0, Lcom/google/android/gms/d/h/aj$i;

    invoke-static {v0, p1}, Lcom/google/android/gms/d/h/aj$i;->a(Lcom/google/android/gms/d/h/aj$i;Ljava/lang/Iterable;)V

    return-object p0
.end method

.method public final b()Lcom/google/android/gms/d/h/aj$i$a;
    .locals 1

    .line 12
    invoke-virtual {p0}, Lcom/google/android/gms/d/h/ds$a;->p()V

    .line 13
    iget-object v0, p0, Lcom/google/android/gms/d/h/aj$i$a;->a:Lcom/google/android/gms/d/h/ds;

    check-cast v0, Lcom/google/android/gms/d/h/aj$i;

    invoke-static {v0}, Lcom/google/android/gms/d/h/aj$i;->b(Lcom/google/android/gms/d/h/aj$i;)V

    return-object p0
.end method

.method public final b(I)Lcom/google/android/gms/d/h/aj$i$a;
    .locals 1

    .line 24
    invoke-virtual {p0}, Lcom/google/android/gms/d/h/ds$a;->p()V

    .line 25
    iget-object v0, p0, Lcom/google/android/gms/d/h/aj$i$a;->a:Lcom/google/android/gms/d/h/ds;

    check-cast v0, Lcom/google/android/gms/d/h/aj$i;

    invoke-static {v0, p1}, Lcom/google/android/gms/d/h/aj$i;->b(Lcom/google/android/gms/d/h/aj$i;I)V

    return-object p0
.end method

.method public final b(Ljava/lang/Iterable;)Lcom/google/android/gms/d/h/aj$i$a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable<",
            "+",
            "Ljava/lang/Long;",
            ">;)",
            "Lcom/google/android/gms/d/h/aj$i$a;"
        }
    .end annotation

    .line 9
    invoke-virtual {p0}, Lcom/google/android/gms/d/h/ds$a;->p()V

    .line 10
    iget-object v0, p0, Lcom/google/android/gms/d/h/aj$i$a;->a:Lcom/google/android/gms/d/h/ds;

    check-cast v0, Lcom/google/android/gms/d/h/aj$i;

    invoke-static {v0, p1}, Lcom/google/android/gms/d/h/aj$i;->b(Lcom/google/android/gms/d/h/aj$i;Ljava/lang/Iterable;)V

    return-object p0
.end method

.method public final c(Ljava/lang/Iterable;)Lcom/google/android/gms/d/h/aj$i$a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable<",
            "+",
            "Lcom/google/android/gms/d/h/aj$b;",
            ">;)",
            "Lcom/google/android/gms/d/h/aj$i$a;"
        }
    .end annotation

    .line 15
    invoke-virtual {p0}, Lcom/google/android/gms/d/h/ds$a;->p()V

    .line 16
    iget-object v0, p0, Lcom/google/android/gms/d/h/aj$i$a;->a:Lcom/google/android/gms/d/h/ds;

    check-cast v0, Lcom/google/android/gms/d/h/aj$i;

    invoke-static {v0, p1}, Lcom/google/android/gms/d/h/aj$i;->c(Lcom/google/android/gms/d/h/aj$i;Ljava/lang/Iterable;)V

    return-object p0
.end method

.method public final d(Ljava/lang/Iterable;)Lcom/google/android/gms/d/h/aj$i$a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable<",
            "+",
            "Lcom/google/android/gms/d/h/aj$j;",
            ">;)",
            "Lcom/google/android/gms/d/h/aj$i$a;"
        }
    .end annotation

    .line 21
    invoke-virtual {p0}, Lcom/google/android/gms/d/h/ds$a;->p()V

    .line 22
    iget-object v0, p0, Lcom/google/android/gms/d/h/aj$i$a;->a:Lcom/google/android/gms/d/h/ds;

    check-cast v0, Lcom/google/android/gms/d/h/aj$i;

    invoke-static {v0, p1}, Lcom/google/android/gms/d/h/aj$i;->d(Lcom/google/android/gms/d/h/aj$i;Ljava/lang/Iterable;)V

    return-object p0
.end method
