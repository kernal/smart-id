.class final Lcom/google/android/gms/d/h/dg;
.super Lcom/google/android/gms/d/h/dh;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/gms/d/h/dh<",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/google/android/gms/d/h/dh;-><init>()V

    return-void
.end method


# virtual methods
.method final a(Ljava/util/Map$Entry;)I
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map$Entry<",
            "**>;)I"
        }
    .end annotation

    .line 9
    invoke-interface {p1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    .line 10
    new-instance p1, Ljava/lang/NoSuchMethodError;

    invoke-direct {p1}, Ljava/lang/NoSuchMethodError;-><init>()V

    throw p1
.end method

.method final a(Ljava/lang/Object;)Lcom/google/android/gms/d/h/di;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            ")",
            "Lcom/google/android/gms/d/h/di<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .line 3
    check-cast p1, Lcom/google/android/gms/d/h/ds$b;

    iget-object p1, p1, Lcom/google/android/gms/d/h/ds$b;->zzaic:Lcom/google/android/gms/d/h/di;

    return-object p1
.end method

.method final a(Lcom/google/android/gms/d/h/df;Lcom/google/android/gms/d/h/fe;I)Ljava/lang/Object;
    .locals 0

    .line 14
    invoke-virtual {p1, p2, p3}, Lcom/google/android/gms/d/h/df;->a(Lcom/google/android/gms/d/h/fe;I)Lcom/google/android/gms/d/h/ds$e;

    move-result-object p1

    return-object p1
.end method

.method final a(Lcom/google/android/gms/d/h/fu;Ljava/lang/Object;Lcom/google/android/gms/d/h/df;Lcom/google/android/gms/d/h/di;Ljava/lang/Object;Lcom/google/android/gms/d/h/gm;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<UT:",
            "Ljava/lang/Object;",
            "UB:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/android/gms/d/h/fu;",
            "Ljava/lang/Object;",
            "Lcom/google/android/gms/d/h/df;",
            "Lcom/google/android/gms/d/h/di<",
            "Ljava/lang/Object;",
            ">;TUB;",
            "Lcom/google/android/gms/d/h/gm<",
            "TUT;TUB;>;)TUB;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 7
    new-instance p1, Ljava/lang/NoSuchMethodError;

    invoke-direct {p1}, Ljava/lang/NoSuchMethodError;-><init>()V

    throw p1
.end method

.method final a(Lcom/google/android/gms/d/h/ci;Ljava/lang/Object;Lcom/google/android/gms/d/h/df;Lcom/google/android/gms/d/h/di;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/gms/d/h/ci;",
            "Ljava/lang/Object;",
            "Lcom/google/android/gms/d/h/df;",
            "Lcom/google/android/gms/d/h/di<",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 16
    new-instance p1, Ljava/lang/NoSuchMethodError;

    invoke-direct {p1}, Ljava/lang/NoSuchMethodError;-><init>()V

    throw p1
.end method

.method final a(Lcom/google/android/gms/d/h/fu;Ljava/lang/Object;Lcom/google/android/gms/d/h/df;Lcom/google/android/gms/d/h/di;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/gms/d/h/fu;",
            "Ljava/lang/Object;",
            "Lcom/google/android/gms/d/h/df;",
            "Lcom/google/android/gms/d/h/di<",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 15
    new-instance p1, Ljava/lang/NoSuchMethodError;

    invoke-direct {p1}, Ljava/lang/NoSuchMethodError;-><init>()V

    throw p1
.end method

.method final a(Lcom/google/android/gms/d/h/hk;Ljava/util/Map$Entry;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/gms/d/h/hk;",
            "Ljava/util/Map$Entry<",
            "**>;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 12
    invoke-interface {p2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    .line 13
    new-instance p1, Ljava/lang/NoSuchMethodError;

    invoke-direct {p1}, Ljava/lang/NoSuchMethodError;-><init>()V

    throw p1
.end method

.method final a(Lcom/google/android/gms/d/h/fe;)Z
    .locals 0

    .line 2
    instance-of p1, p1, Lcom/google/android/gms/d/h/ds$b;

    return p1
.end method

.method final b(Ljava/lang/Object;)Lcom/google/android/gms/d/h/di;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            ")",
            "Lcom/google/android/gms/d/h/di<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .line 4
    check-cast p1, Lcom/google/android/gms/d/h/ds$b;

    invoke-virtual {p1}, Lcom/google/android/gms/d/h/ds$b;->a()Lcom/google/android/gms/d/h/di;

    move-result-object p1

    return-object p1
.end method

.method final c(Ljava/lang/Object;)V
    .locals 0

    .line 5
    invoke-virtual {p0, p1}, Lcom/google/android/gms/d/h/dh;->a(Ljava/lang/Object;)Lcom/google/android/gms/d/h/di;

    move-result-object p1

    invoke-virtual {p1}, Lcom/google/android/gms/d/h/di;->b()V

    return-void
.end method
