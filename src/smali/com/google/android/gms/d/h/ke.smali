.class public final Lcom/google/android/gms/d/h/ke;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/d/h/bu;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/gms/d/h/bu<",
        "Lcom/google/android/gms/d/h/kh;",
        ">;"
    }
.end annotation


# static fields
.field private static a:Lcom/google/android/gms/d/h/ke;


# instance fields
.field private final b:Lcom/google/android/gms/d/h/bu;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/d/h/bu<",
            "Lcom/google/android/gms/d/h/kh;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 14
    new-instance v0, Lcom/google/android/gms/d/h/ke;

    invoke-direct {v0}, Lcom/google/android/gms/d/h/ke;-><init>()V

    sput-object v0, Lcom/google/android/gms/d/h/ke;->a:Lcom/google/android/gms/d/h/ke;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .line 9
    new-instance v0, Lcom/google/android/gms/d/h/kg;

    invoke-direct {v0}, Lcom/google/android/gms/d/h/kg;-><init>()V

    invoke-static {v0}, Lcom/google/android/gms/d/h/bt;->a(Ljava/lang/Object;)Lcom/google/android/gms/d/h/bu;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/d/h/ke;-><init>(Lcom/google/android/gms/d/h/bu;)V

    return-void
.end method

.method private constructor <init>(Lcom/google/android/gms/d/h/bu;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/gms/d/h/bu<",
            "Lcom/google/android/gms/d/h/kh;",
            ">;)V"
        }
    .end annotation

    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 7
    invoke-static {p1}, Lcom/google/android/gms/d/h/bt;->a(Lcom/google/android/gms/d/h/bu;)Lcom/google/android/gms/d/h/bu;

    move-result-object p1

    iput-object p1, p0, Lcom/google/android/gms/d/h/ke;->b:Lcom/google/android/gms/d/h/bu;

    return-void
.end method

.method public static b()Z
    .locals 1

    .line 1
    sget-object v0, Lcom/google/android/gms/d/h/ke;->a:Lcom/google/android/gms/d/h/ke;

    invoke-virtual {v0}, Lcom/google/android/gms/d/h/ke;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/d/h/kh;

    invoke-interface {v0}, Lcom/google/android/gms/d/h/kh;->a()Z

    move-result v0

    return v0
.end method

.method public static c()D
    .locals 2

    .line 2
    sget-object v0, Lcom/google/android/gms/d/h/ke;->a:Lcom/google/android/gms/d/h/ke;

    invoke-virtual {v0}, Lcom/google/android/gms/d/h/ke;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/d/h/kh;

    invoke-interface {v0}, Lcom/google/android/gms/d/h/kh;->b()D

    move-result-wide v0

    return-wide v0
.end method

.method public static d()J
    .locals 2

    .line 3
    sget-object v0, Lcom/google/android/gms/d/h/ke;->a:Lcom/google/android/gms/d/h/ke;

    invoke-virtual {v0}, Lcom/google/android/gms/d/h/ke;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/d/h/kh;

    invoke-interface {v0}, Lcom/google/android/gms/d/h/kh;->c()J

    move-result-wide v0

    return-wide v0
.end method

.method public static e()J
    .locals 2

    .line 4
    sget-object v0, Lcom/google/android/gms/d/h/ke;->a:Lcom/google/android/gms/d/h/ke;

    invoke-virtual {v0}, Lcom/google/android/gms/d/h/ke;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/d/h/kh;

    invoke-interface {v0}, Lcom/google/android/gms/d/h/kh;->d()J

    move-result-wide v0

    return-wide v0
.end method

.method public static f()Ljava/lang/String;
    .locals 1

    .line 5
    sget-object v0, Lcom/google/android/gms/d/h/ke;->a:Lcom/google/android/gms/d/h/ke;

    invoke-virtual {v0}, Lcom/google/android/gms/d/h/ke;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/d/h/kh;

    invoke-interface {v0}, Lcom/google/android/gms/d/h/kh;->e()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final synthetic a()Ljava/lang/Object;
    .locals 1

    .line 12
    iget-object v0, p0, Lcom/google/android/gms/d/h/ke;->b:Lcom/google/android/gms/d/h/bu;

    invoke-interface {v0}, Lcom/google/android/gms/d/h/bu;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/d/h/kh;

    return-object v0
.end method
