.class public abstract Lcom/google/android/gms/d/h/ds$b;
.super Lcom/google/android/gms/d/h/ds;

# interfaces
.implements Lcom/google/android/gms/d/h/fg;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/d/h/ds;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<MessageType:",
        "Lcom/google/android/gms/d/h/ds$b<",
        "TMessageType;TBuilderType;>;BuilderType:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/google/android/gms/d/h/ds<",
        "TMessageType;TBuilderType;>;",
        "Lcom/google/android/gms/d/h/fg;"
    }
.end annotation


# instance fields
.field protected zzaic:Lcom/google/android/gms/d/h/di;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/d/h/di<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/google/android/gms/d/h/ds;-><init>()V

    .line 2
    invoke-static {}, Lcom/google/android/gms/d/h/di;->a()Lcom/google/android/gms/d/h/di;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/d/h/ds$b;->zzaic:Lcom/google/android/gms/d/h/di;

    return-void
.end method


# virtual methods
.method final a()Lcom/google/android/gms/d/h/di;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/gms/d/h/di<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .line 3
    iget-object v0, p0, Lcom/google/android/gms/d/h/ds$b;->zzaic:Lcom/google/android/gms/d/h/di;

    invoke-virtual {v0}, Lcom/google/android/gms/d/h/di;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 4
    iget-object v0, p0, Lcom/google/android/gms/d/h/ds$b;->zzaic:Lcom/google/android/gms/d/h/di;

    invoke-virtual {v0}, Lcom/google/android/gms/d/h/di;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/d/h/di;

    iput-object v0, p0, Lcom/google/android/gms/d/h/ds$b;->zzaic:Lcom/google/android/gms/d/h/di;

    .line 5
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/d/h/ds$b;->zzaic:Lcom/google/android/gms/d/h/di;

    return-object v0
.end method
