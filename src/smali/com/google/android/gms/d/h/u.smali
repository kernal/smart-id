.class final Lcom/google/android/gms/d/h/u;
.super Lcom/google/android/gms/d/h/mb$a;


# instance fields
.field private final synthetic c:Landroid/app/Activity;

.field private final synthetic d:Landroid/os/Bundle;

.field private final synthetic e:Lcom/google/android/gms/d/h/mb$b;


# direct methods
.method constructor <init>(Lcom/google/android/gms/d/h/mb$b;Landroid/app/Activity;Landroid/os/Bundle;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/google/android/gms/d/h/u;->e:Lcom/google/android/gms/d/h/mb$b;

    iput-object p2, p0, Lcom/google/android/gms/d/h/u;->c:Landroid/app/Activity;

    iput-object p3, p0, Lcom/google/android/gms/d/h/u;->d:Landroid/os/Bundle;

    iget-object p1, p1, Lcom/google/android/gms/d/h/mb$b;->a:Lcom/google/android/gms/d/h/mb;

    invoke-direct {p0, p1}, Lcom/google/android/gms/d/h/mb$a;-><init>(Lcom/google/android/gms/d/h/mb;)V

    return-void
.end method


# virtual methods
.method final a()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .line 2
    iget-object v0, p0, Lcom/google/android/gms/d/h/u;->e:Lcom/google/android/gms/d/h/mb$b;

    iget-object v0, v0, Lcom/google/android/gms/d/h/mb$b;->a:Lcom/google/android/gms/d/h/mb;

    invoke-static {v0}, Lcom/google/android/gms/d/h/mb;->c(Lcom/google/android/gms/d/h/mb;)Lcom/google/android/gms/d/h/iz;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/d/h/u;->c:Landroid/app/Activity;

    .line 3
    invoke-static {v1}, Lcom/google/android/gms/c/b;->a(Ljava/lang/Object;)Lcom/google/android/gms/c/a;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/d/h/u;->d:Landroid/os/Bundle;

    iget-wide v3, p0, Lcom/google/android/gms/d/h/u;->b:J

    .line 4
    invoke-interface {v0, v1, v2, v3, v4}, Lcom/google/android/gms/d/h/iz;->onActivityCreated(Lcom/google/android/gms/c/a;Landroid/os/Bundle;J)V

    return-void
.end method
