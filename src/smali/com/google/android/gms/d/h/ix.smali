.class public final Lcom/google/android/gms/d/h/ix;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/d/h/bu;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/gms/d/h/bu<",
        "Lcom/google/android/gms/d/h/iw;",
        ">;"
    }
.end annotation


# static fields
.field private static a:Lcom/google/android/gms/d/h/ix;


# instance fields
.field private final b:Lcom/google/android/gms/d/h/bu;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/d/h/bu<",
            "Lcom/google/android/gms/d/h/iw;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 10
    new-instance v0, Lcom/google/android/gms/d/h/ix;

    invoke-direct {v0}, Lcom/google/android/gms/d/h/ix;-><init>()V

    sput-object v0, Lcom/google/android/gms/d/h/ix;->a:Lcom/google/android/gms/d/h/ix;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .line 5
    new-instance v0, Lcom/google/android/gms/d/h/ja;

    invoke-direct {v0}, Lcom/google/android/gms/d/h/ja;-><init>()V

    invoke-static {v0}, Lcom/google/android/gms/d/h/bt;->a(Ljava/lang/Object;)Lcom/google/android/gms/d/h/bu;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/d/h/ix;-><init>(Lcom/google/android/gms/d/h/bu;)V

    return-void
.end method

.method private constructor <init>(Lcom/google/android/gms/d/h/bu;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/gms/d/h/bu<",
            "Lcom/google/android/gms/d/h/iw;",
            ">;)V"
        }
    .end annotation

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3
    invoke-static {p1}, Lcom/google/android/gms/d/h/bt;->a(Lcom/google/android/gms/d/h/bu;)Lcom/google/android/gms/d/h/bu;

    move-result-object p1

    iput-object p1, p0, Lcom/google/android/gms/d/h/ix;->b:Lcom/google/android/gms/d/h/bu;

    return-void
.end method

.method public static b()Z
    .locals 1

    .line 1
    sget-object v0, Lcom/google/android/gms/d/h/ix;->a:Lcom/google/android/gms/d/h/ix;

    invoke-virtual {v0}, Lcom/google/android/gms/d/h/ix;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/d/h/iw;

    invoke-interface {v0}, Lcom/google/android/gms/d/h/iw;->a()Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final synthetic a()Ljava/lang/Object;
    .locals 1

    .line 8
    iget-object v0, p0, Lcom/google/android/gms/d/h/ix;->b:Lcom/google/android/gms/d/h/bu;

    invoke-interface {v0}, Lcom/google/android/gms/d/h/bu;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/d/h/iw;

    return-object v0
.end method
