.class public final Lcom/google/android/gms/d/h/ab$a$a;
.super Lcom/google/android/gms/d/h/ds$a;

# interfaces
.implements Lcom/google/android/gms/d/h/fg;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/d/h/ab$a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/gms/d/h/ds$a<",
        "Lcom/google/android/gms/d/h/ab$a;",
        "Lcom/google/android/gms/d/h/ab$a$a;",
        ">;",
        "Lcom/google/android/gms/d/h/fg;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 1

    .line 1
    invoke-static {}, Lcom/google/android/gms/d/h/ab$a;->m()Lcom/google/android/gms/d/h/ab$a;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/d/h/ds$a;-><init>(Lcom/google/android/gms/d/h/ds;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/gms/d/h/aa;)V
    .locals 0

    .line 12
    invoke-direct {p0}, Lcom/google/android/gms/d/h/ab$a$a;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(ILcom/google/android/gms/d/h/ab$b;)Lcom/google/android/gms/d/h/ab$a$a;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/google/android/gms/d/h/ds$a;->p()V

    .line 10
    iget-object v0, p0, Lcom/google/android/gms/d/h/ab$a$a;->a:Lcom/google/android/gms/d/h/ds;

    check-cast v0, Lcom/google/android/gms/d/h/ab$a;

    invoke-static {v0, p1, p2}, Lcom/google/android/gms/d/h/ab$a;->a(Lcom/google/android/gms/d/h/ab$a;ILcom/google/android/gms/d/h/ab$b;)V

    return-object p0
.end method

.method public final a(Ljava/lang/String;)Lcom/google/android/gms/d/h/ab$a$a;
    .locals 1

    .line 4
    invoke-virtual {p0}, Lcom/google/android/gms/d/h/ds$a;->p()V

    .line 5
    iget-object v0, p0, Lcom/google/android/gms/d/h/ab$a$a;->a:Lcom/google/android/gms/d/h/ds;

    check-cast v0, Lcom/google/android/gms/d/h/ab$a;

    invoke-static {v0, p1}, Lcom/google/android/gms/d/h/ab$a;->a(Lcom/google/android/gms/d/h/ab$a;Ljava/lang/String;)V

    return-object p0
.end method

.method public final a(I)Lcom/google/android/gms/d/h/ab$b;
    .locals 1

    .line 8
    iget-object v0, p0, Lcom/google/android/gms/d/h/ab$a$a;->a:Lcom/google/android/gms/d/h/ds;

    check-cast v0, Lcom/google/android/gms/d/h/ab$a;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/d/h/ab$a;->a(I)Lcom/google/android/gms/d/h/ab$b;

    move-result-object p1

    return-object p1
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .line 3
    iget-object v0, p0, Lcom/google/android/gms/d/h/ab$a$a;->a:Lcom/google/android/gms/d/h/ds;

    check-cast v0, Lcom/google/android/gms/d/h/ab$a;

    invoke-virtual {v0}, Lcom/google/android/gms/d/h/ab$a;->c()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b()I
    .locals 1

    .line 7
    iget-object v0, p0, Lcom/google/android/gms/d/h/ab$a$a;->a:Lcom/google/android/gms/d/h/ds;

    check-cast v0, Lcom/google/android/gms/d/h/ab$a;

    invoke-virtual {v0}, Lcom/google/android/gms/d/h/ab$a;->e()I

    move-result v0

    return v0
.end method
