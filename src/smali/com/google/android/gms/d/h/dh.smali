.class abstract Lcom/google/android/gms/d/h/dh;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::",
        "Lcom/google/android/gms/d/h/dk<",
        "TT;>;>",
        "Ljava/lang/Object;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method abstract a(Ljava/util/Map$Entry;)I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map$Entry<",
            "**>;)I"
        }
    .end annotation
.end method

.method abstract a(Ljava/lang/Object;)Lcom/google/android/gms/d/h/di;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            ")",
            "Lcom/google/android/gms/d/h/di<",
            "TT;>;"
        }
    .end annotation
.end method

.method abstract a(Lcom/google/android/gms/d/h/df;Lcom/google/android/gms/d/h/fe;I)Ljava/lang/Object;
.end method

.method abstract a(Lcom/google/android/gms/d/h/fu;Ljava/lang/Object;Lcom/google/android/gms/d/h/df;Lcom/google/android/gms/d/h/di;Ljava/lang/Object;Lcom/google/android/gms/d/h/gm;)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<UT:",
            "Ljava/lang/Object;",
            "UB:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/android/gms/d/h/fu;",
            "Ljava/lang/Object;",
            "Lcom/google/android/gms/d/h/df;",
            "Lcom/google/android/gms/d/h/di<",
            "TT;>;TUB;",
            "Lcom/google/android/gms/d/h/gm<",
            "TUT;TUB;>;)TUB;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method abstract a(Lcom/google/android/gms/d/h/ci;Ljava/lang/Object;Lcom/google/android/gms/d/h/df;Lcom/google/android/gms/d/h/di;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/gms/d/h/ci;",
            "Ljava/lang/Object;",
            "Lcom/google/android/gms/d/h/df;",
            "Lcom/google/android/gms/d/h/di<",
            "TT;>;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method abstract a(Lcom/google/android/gms/d/h/fu;Ljava/lang/Object;Lcom/google/android/gms/d/h/df;Lcom/google/android/gms/d/h/di;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/gms/d/h/fu;",
            "Ljava/lang/Object;",
            "Lcom/google/android/gms/d/h/df;",
            "Lcom/google/android/gms/d/h/di<",
            "TT;>;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method abstract a(Lcom/google/android/gms/d/h/hk;Ljava/util/Map$Entry;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/gms/d/h/hk;",
            "Ljava/util/Map$Entry<",
            "**>;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method abstract a(Lcom/google/android/gms/d/h/fe;)Z
.end method

.method abstract b(Ljava/lang/Object;)Lcom/google/android/gms/d/h/di;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            ")",
            "Lcom/google/android/gms/d/h/di<",
            "TT;>;"
        }
    .end annotation
.end method

.method abstract c(Ljava/lang/Object;)V
.end method
