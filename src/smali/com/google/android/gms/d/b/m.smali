.class abstract Lcom/google/android/gms/d/b/m;
.super Lcom/google/android/gms/common/api/internal/n;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/gms/common/api/internal/n<",
        "Lcom/google/android/gms/d/b/i;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field private a:Lcom/google/android/gms/h/i;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/h/i<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/gms/common/api/internal/n;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/gms/d/b/k;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/gms/d/b/m;-><init>()V

    return-void
.end method


# virtual methods
.method protected final a(Lcom/google/android/gms/common/api/Status;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/d/b/m;->a:Lcom/google/android/gms/h/i;

    invoke-static {p1, v0}, Lcom/google/android/gms/common/api/internal/o;->a(Lcom/google/android/gms/common/api/Status;Lcom/google/android/gms/h/i;)V

    return-void
.end method

.method protected synthetic a(Lcom/google/android/gms/common/api/a$b;Lcom/google/android/gms/h/i;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    check-cast p1, Lcom/google/android/gms/d/b/i;

    iput-object p2, p0, Lcom/google/android/gms/d/b/m;->a:Lcom/google/android/gms/h/i;

    invoke-virtual {p1}, Lcom/google/android/gms/d/b/i;->w()Landroid/os/IInterface;

    move-result-object p1

    check-cast p1, Lcom/google/android/gms/d/b/e;

    invoke-virtual {p0, p1}, Lcom/google/android/gms/d/b/m;->a(Lcom/google/android/gms/d/b/e;)V

    return-void
.end method

.method protected abstract a(Lcom/google/android/gms/d/b/e;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method
