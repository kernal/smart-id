.class public final Lcom/google/gson/internal/bind/i;
.super Ljava/lang/Object;
.source "TypeAdapters.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/gson/internal/bind/i$a;
    }
.end annotation


# static fields
.field public static final A:Lcom/google/gson/w;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/w<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final B:Lcom/google/gson/w;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/w<",
            "Ljava/math/BigDecimal;",
            ">;"
        }
    .end annotation
.end field

.field public static final C:Lcom/google/gson/w;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/w<",
            "Ljava/math/BigInteger;",
            ">;"
        }
    .end annotation
.end field

.field public static final D:Lcom/google/gson/x;

.field public static final E:Lcom/google/gson/w;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/w<",
            "Ljava/lang/StringBuilder;",
            ">;"
        }
    .end annotation
.end field

.field public static final F:Lcom/google/gson/x;

.field public static final G:Lcom/google/gson/w;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/w<",
            "Ljava/lang/StringBuffer;",
            ">;"
        }
    .end annotation
.end field

.field public static final H:Lcom/google/gson/x;

.field public static final I:Lcom/google/gson/w;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/w<",
            "Ljava/net/URL;",
            ">;"
        }
    .end annotation
.end field

.field public static final J:Lcom/google/gson/x;

.field public static final K:Lcom/google/gson/w;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/w<",
            "Ljava/net/URI;",
            ">;"
        }
    .end annotation
.end field

.field public static final L:Lcom/google/gson/x;

.field public static final M:Lcom/google/gson/w;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/w<",
            "Ljava/net/InetAddress;",
            ">;"
        }
    .end annotation
.end field

.field public static final N:Lcom/google/gson/x;

.field public static final O:Lcom/google/gson/w;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/w<",
            "Ljava/util/UUID;",
            ">;"
        }
    .end annotation
.end field

.field public static final P:Lcom/google/gson/x;

.field public static final Q:Lcom/google/gson/w;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/w<",
            "Ljava/util/Currency;",
            ">;"
        }
    .end annotation
.end field

.field public static final R:Lcom/google/gson/x;

.field public static final S:Lcom/google/gson/x;

.field public static final T:Lcom/google/gson/w;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/w<",
            "Ljava/util/Calendar;",
            ">;"
        }
    .end annotation
.end field

.field public static final U:Lcom/google/gson/x;

.field public static final V:Lcom/google/gson/w;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/w<",
            "Ljava/util/Locale;",
            ">;"
        }
    .end annotation
.end field

.field public static final W:Lcom/google/gson/x;

.field public static final X:Lcom/google/gson/w;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/w<",
            "Lcom/google/gson/l;",
            ">;"
        }
    .end annotation
.end field

.field public static final Y:Lcom/google/gson/x;

.field public static final Z:Lcom/google/gson/x;

.field public static final a:Lcom/google/gson/w;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/w<",
            "Ljava/lang/Class;",
            ">;"
        }
    .end annotation
.end field

.field public static final b:Lcom/google/gson/x;

.field public static final c:Lcom/google/gson/w;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/w<",
            "Ljava/util/BitSet;",
            ">;"
        }
    .end annotation
.end field

.field public static final d:Lcom/google/gson/x;

.field public static final e:Lcom/google/gson/w;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/w<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static final f:Lcom/google/gson/w;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/w<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static final g:Lcom/google/gson/x;

.field public static final h:Lcom/google/gson/w;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/w<",
            "Ljava/lang/Number;",
            ">;"
        }
    .end annotation
.end field

.field public static final i:Lcom/google/gson/x;

.field public static final j:Lcom/google/gson/w;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/w<",
            "Ljava/lang/Number;",
            ">;"
        }
    .end annotation
.end field

.field public static final k:Lcom/google/gson/x;

.field public static final l:Lcom/google/gson/w;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/w<",
            "Ljava/lang/Number;",
            ">;"
        }
    .end annotation
.end field

.field public static final m:Lcom/google/gson/x;

.field public static final n:Lcom/google/gson/w;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/w<",
            "Ljava/util/concurrent/atomic/AtomicInteger;",
            ">;"
        }
    .end annotation
.end field

.field public static final o:Lcom/google/gson/x;

.field public static final p:Lcom/google/gson/w;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/w<",
            "Ljava/util/concurrent/atomic/AtomicBoolean;",
            ">;"
        }
    .end annotation
.end field

.field public static final q:Lcom/google/gson/x;

.field public static final r:Lcom/google/gson/w;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/w<",
            "Ljava/util/concurrent/atomic/AtomicIntegerArray;",
            ">;"
        }
    .end annotation
.end field

.field public static final s:Lcom/google/gson/x;

.field public static final t:Lcom/google/gson/w;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/w<",
            "Ljava/lang/Number;",
            ">;"
        }
    .end annotation
.end field

.field public static final u:Lcom/google/gson/w;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/w<",
            "Ljava/lang/Number;",
            ">;"
        }
    .end annotation
.end field

.field public static final v:Lcom/google/gson/w;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/w<",
            "Ljava/lang/Number;",
            ">;"
        }
    .end annotation
.end field

.field public static final w:Lcom/google/gson/w;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/w<",
            "Ljava/lang/Number;",
            ">;"
        }
    .end annotation
.end field

.field public static final x:Lcom/google/gson/x;

.field public static final y:Lcom/google/gson/w;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/w<",
            "Ljava/lang/Character;",
            ">;"
        }
    .end annotation
.end field

.field public static final z:Lcom/google/gson/x;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 69
    new-instance v0, Lcom/google/gson/internal/bind/i$1;

    invoke-direct {v0}, Lcom/google/gson/internal/bind/i$1;-><init>()V

    .line 80
    invoke-virtual {v0}, Lcom/google/gson/internal/bind/i$1;->nullSafe()Lcom/google/gson/w;

    move-result-object v0

    sput-object v0, Lcom/google/gson/internal/bind/i;->a:Lcom/google/gson/w;

    .line 82
    const-class v0, Ljava/lang/Class;

    sget-object v1, Lcom/google/gson/internal/bind/i;->a:Lcom/google/gson/w;

    invoke-static {v0, v1}, Lcom/google/gson/internal/bind/i;->a(Ljava/lang/Class;Lcom/google/gson/w;)Lcom/google/gson/x;

    move-result-object v0

    sput-object v0, Lcom/google/gson/internal/bind/i;->b:Lcom/google/gson/x;

    .line 84
    new-instance v0, Lcom/google/gson/internal/bind/i$12;

    invoke-direct {v0}, Lcom/google/gson/internal/bind/i$12;-><init>()V

    .line 129
    invoke-virtual {v0}, Lcom/google/gson/internal/bind/i$12;->nullSafe()Lcom/google/gson/w;

    move-result-object v0

    sput-object v0, Lcom/google/gson/internal/bind/i;->c:Lcom/google/gson/w;

    .line 131
    const-class v0, Ljava/util/BitSet;

    sget-object v1, Lcom/google/gson/internal/bind/i;->c:Lcom/google/gson/w;

    invoke-static {v0, v1}, Lcom/google/gson/internal/bind/i;->a(Ljava/lang/Class;Lcom/google/gson/w;)Lcom/google/gson/x;

    move-result-object v0

    sput-object v0, Lcom/google/gson/internal/bind/i;->d:Lcom/google/gson/x;

    .line 133
    new-instance v0, Lcom/google/gson/internal/bind/i$22;

    invoke-direct {v0}, Lcom/google/gson/internal/bind/i$22;-><init>()V

    sput-object v0, Lcom/google/gson/internal/bind/i;->e:Lcom/google/gson/w;

    .line 156
    new-instance v0, Lcom/google/gson/internal/bind/i$24;

    invoke-direct {v0}, Lcom/google/gson/internal/bind/i$24;-><init>()V

    sput-object v0, Lcom/google/gson/internal/bind/i;->f:Lcom/google/gson/w;

    .line 170
    sget-object v0, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    const-class v1, Ljava/lang/Boolean;

    sget-object v2, Lcom/google/gson/internal/bind/i;->e:Lcom/google/gson/w;

    .line 171
    invoke-static {v0, v1, v2}, Lcom/google/gson/internal/bind/i;->a(Ljava/lang/Class;Ljava/lang/Class;Lcom/google/gson/w;)Lcom/google/gson/x;

    move-result-object v0

    sput-object v0, Lcom/google/gson/internal/bind/i;->g:Lcom/google/gson/x;

    .line 173
    new-instance v0, Lcom/google/gson/internal/bind/i$25;

    invoke-direct {v0}, Lcom/google/gson/internal/bind/i$25;-><init>()V

    sput-object v0, Lcom/google/gson/internal/bind/i;->h:Lcom/google/gson/w;

    .line 193
    sget-object v0, Ljava/lang/Byte;->TYPE:Ljava/lang/Class;

    const-class v1, Ljava/lang/Byte;

    sget-object v2, Lcom/google/gson/internal/bind/i;->h:Lcom/google/gson/w;

    .line 194
    invoke-static {v0, v1, v2}, Lcom/google/gson/internal/bind/i;->a(Ljava/lang/Class;Ljava/lang/Class;Lcom/google/gson/w;)Lcom/google/gson/x;

    move-result-object v0

    sput-object v0, Lcom/google/gson/internal/bind/i;->i:Lcom/google/gson/x;

    .line 196
    new-instance v0, Lcom/google/gson/internal/bind/i$26;

    invoke-direct {v0}, Lcom/google/gson/internal/bind/i$26;-><init>()V

    sput-object v0, Lcom/google/gson/internal/bind/i;->j:Lcom/google/gson/w;

    .line 215
    sget-object v0, Ljava/lang/Short;->TYPE:Ljava/lang/Class;

    const-class v1, Ljava/lang/Short;

    sget-object v2, Lcom/google/gson/internal/bind/i;->j:Lcom/google/gson/w;

    .line 216
    invoke-static {v0, v1, v2}, Lcom/google/gson/internal/bind/i;->a(Ljava/lang/Class;Ljava/lang/Class;Lcom/google/gson/w;)Lcom/google/gson/x;

    move-result-object v0

    sput-object v0, Lcom/google/gson/internal/bind/i;->k:Lcom/google/gson/x;

    .line 218
    new-instance v0, Lcom/google/gson/internal/bind/i$27;

    invoke-direct {v0}, Lcom/google/gson/internal/bind/i$27;-><init>()V

    sput-object v0, Lcom/google/gson/internal/bind/i;->l:Lcom/google/gson/w;

    .line 236
    sget-object v0, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    const-class v1, Ljava/lang/Integer;

    sget-object v2, Lcom/google/gson/internal/bind/i;->l:Lcom/google/gson/w;

    .line 237
    invoke-static {v0, v1, v2}, Lcom/google/gson/internal/bind/i;->a(Ljava/lang/Class;Ljava/lang/Class;Lcom/google/gson/w;)Lcom/google/gson/x;

    move-result-object v0

    sput-object v0, Lcom/google/gson/internal/bind/i;->m:Lcom/google/gson/x;

    .line 239
    new-instance v0, Lcom/google/gson/internal/bind/i$28;

    invoke-direct {v0}, Lcom/google/gson/internal/bind/i$28;-><init>()V

    .line 250
    invoke-virtual {v0}, Lcom/google/gson/internal/bind/i$28;->nullSafe()Lcom/google/gson/w;

    move-result-object v0

    sput-object v0, Lcom/google/gson/internal/bind/i;->n:Lcom/google/gson/w;

    .line 251
    const-class v0, Ljava/util/concurrent/atomic/AtomicInteger;

    sget-object v1, Lcom/google/gson/internal/bind/i;->n:Lcom/google/gson/w;

    .line 252
    invoke-static {v0, v1}, Lcom/google/gson/internal/bind/i;->a(Ljava/lang/Class;Lcom/google/gson/w;)Lcom/google/gson/x;

    move-result-object v0

    sput-object v0, Lcom/google/gson/internal/bind/i;->o:Lcom/google/gson/x;

    .line 254
    new-instance v0, Lcom/google/gson/internal/bind/i$29;

    invoke-direct {v0}, Lcom/google/gson/internal/bind/i$29;-><init>()V

    .line 261
    invoke-virtual {v0}, Lcom/google/gson/internal/bind/i$29;->nullSafe()Lcom/google/gson/w;

    move-result-object v0

    sput-object v0, Lcom/google/gson/internal/bind/i;->p:Lcom/google/gson/w;

    .line 262
    const-class v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    sget-object v1, Lcom/google/gson/internal/bind/i;->p:Lcom/google/gson/w;

    .line 263
    invoke-static {v0, v1}, Lcom/google/gson/internal/bind/i;->a(Ljava/lang/Class;Lcom/google/gson/w;)Lcom/google/gson/x;

    move-result-object v0

    sput-object v0, Lcom/google/gson/internal/bind/i;->q:Lcom/google/gson/x;

    .line 265
    new-instance v0, Lcom/google/gson/internal/bind/i$2;

    invoke-direct {v0}, Lcom/google/gson/internal/bind/i$2;-><init>()V

    .line 292
    invoke-virtual {v0}, Lcom/google/gson/internal/bind/i$2;->nullSafe()Lcom/google/gson/w;

    move-result-object v0

    sput-object v0, Lcom/google/gson/internal/bind/i;->r:Lcom/google/gson/w;

    .line 293
    const-class v0, Ljava/util/concurrent/atomic/AtomicIntegerArray;

    sget-object v1, Lcom/google/gson/internal/bind/i;->r:Lcom/google/gson/w;

    .line 294
    invoke-static {v0, v1}, Lcom/google/gson/internal/bind/i;->a(Ljava/lang/Class;Lcom/google/gson/w;)Lcom/google/gson/x;

    move-result-object v0

    sput-object v0, Lcom/google/gson/internal/bind/i;->s:Lcom/google/gson/x;

    .line 296
    new-instance v0, Lcom/google/gson/internal/bind/i$3;

    invoke-direct {v0}, Lcom/google/gson/internal/bind/i$3;-><init>()V

    sput-object v0, Lcom/google/gson/internal/bind/i;->t:Lcom/google/gson/w;

    .line 315
    new-instance v0, Lcom/google/gson/internal/bind/i$4;

    invoke-direct {v0}, Lcom/google/gson/internal/bind/i$4;-><init>()V

    sput-object v0, Lcom/google/gson/internal/bind/i;->u:Lcom/google/gson/w;

    .line 330
    new-instance v0, Lcom/google/gson/internal/bind/i$5;

    invoke-direct {v0}, Lcom/google/gson/internal/bind/i$5;-><init>()V

    sput-object v0, Lcom/google/gson/internal/bind/i;->v:Lcom/google/gson/w;

    .line 345
    new-instance v0, Lcom/google/gson/internal/bind/i$6;

    invoke-direct {v0}, Lcom/google/gson/internal/bind/i$6;-><init>()V

    sput-object v0, Lcom/google/gson/internal/bind/i;->w:Lcom/google/gson/w;

    .line 366
    const-class v0, Ljava/lang/Number;

    sget-object v1, Lcom/google/gson/internal/bind/i;->w:Lcom/google/gson/w;

    invoke-static {v0, v1}, Lcom/google/gson/internal/bind/i;->a(Ljava/lang/Class;Lcom/google/gson/w;)Lcom/google/gson/x;

    move-result-object v0

    sput-object v0, Lcom/google/gson/internal/bind/i;->x:Lcom/google/gson/x;

    .line 368
    new-instance v0, Lcom/google/gson/internal/bind/i$7;

    invoke-direct {v0}, Lcom/google/gson/internal/bind/i$7;-><init>()V

    sput-object v0, Lcom/google/gson/internal/bind/i;->y:Lcom/google/gson/w;

    .line 387
    sget-object v0, Ljava/lang/Character;->TYPE:Ljava/lang/Class;

    const-class v1, Ljava/lang/Character;

    sget-object v2, Lcom/google/gson/internal/bind/i;->y:Lcom/google/gson/w;

    .line 388
    invoke-static {v0, v1, v2}, Lcom/google/gson/internal/bind/i;->a(Ljava/lang/Class;Ljava/lang/Class;Lcom/google/gson/w;)Lcom/google/gson/x;

    move-result-object v0

    sput-object v0, Lcom/google/gson/internal/bind/i;->z:Lcom/google/gson/x;

    .line 390
    new-instance v0, Lcom/google/gson/internal/bind/i$8;

    invoke-direct {v0}, Lcom/google/gson/internal/bind/i$8;-><init>()V

    sput-object v0, Lcom/google/gson/internal/bind/i;->A:Lcom/google/gson/w;

    .line 410
    new-instance v0, Lcom/google/gson/internal/bind/i$9;

    invoke-direct {v0}, Lcom/google/gson/internal/bind/i$9;-><init>()V

    sput-object v0, Lcom/google/gson/internal/bind/i;->B:Lcom/google/gson/w;

    .line 428
    new-instance v0, Lcom/google/gson/internal/bind/i$10;

    invoke-direct {v0}, Lcom/google/gson/internal/bind/i$10;-><init>()V

    sput-object v0, Lcom/google/gson/internal/bind/i;->C:Lcom/google/gson/w;

    .line 446
    const-class v0, Ljava/lang/String;

    sget-object v1, Lcom/google/gson/internal/bind/i;->A:Lcom/google/gson/w;

    invoke-static {v0, v1}, Lcom/google/gson/internal/bind/i;->a(Ljava/lang/Class;Lcom/google/gson/w;)Lcom/google/gson/x;

    move-result-object v0

    sput-object v0, Lcom/google/gson/internal/bind/i;->D:Lcom/google/gson/x;

    .line 448
    new-instance v0, Lcom/google/gson/internal/bind/i$11;

    invoke-direct {v0}, Lcom/google/gson/internal/bind/i$11;-><init>()V

    sput-object v0, Lcom/google/gson/internal/bind/i;->E:Lcom/google/gson/w;

    .line 463
    const-class v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/google/gson/internal/bind/i;->E:Lcom/google/gson/w;

    .line 464
    invoke-static {v0, v1}, Lcom/google/gson/internal/bind/i;->a(Ljava/lang/Class;Lcom/google/gson/w;)Lcom/google/gson/x;

    move-result-object v0

    sput-object v0, Lcom/google/gson/internal/bind/i;->F:Lcom/google/gson/x;

    .line 466
    new-instance v0, Lcom/google/gson/internal/bind/i$13;

    invoke-direct {v0}, Lcom/google/gson/internal/bind/i$13;-><init>()V

    sput-object v0, Lcom/google/gson/internal/bind/i;->G:Lcom/google/gson/w;

    .line 481
    const-class v0, Ljava/lang/StringBuffer;

    sget-object v1, Lcom/google/gson/internal/bind/i;->G:Lcom/google/gson/w;

    .line 482
    invoke-static {v0, v1}, Lcom/google/gson/internal/bind/i;->a(Ljava/lang/Class;Lcom/google/gson/w;)Lcom/google/gson/x;

    move-result-object v0

    sput-object v0, Lcom/google/gson/internal/bind/i;->H:Lcom/google/gson/x;

    .line 484
    new-instance v0, Lcom/google/gson/internal/bind/i$14;

    invoke-direct {v0}, Lcom/google/gson/internal/bind/i$14;-><init>()V

    sput-object v0, Lcom/google/gson/internal/bind/i;->I:Lcom/google/gson/w;

    .line 500
    const-class v0, Ljava/net/URL;

    sget-object v1, Lcom/google/gson/internal/bind/i;->I:Lcom/google/gson/w;

    invoke-static {v0, v1}, Lcom/google/gson/internal/bind/i;->a(Ljava/lang/Class;Lcom/google/gson/w;)Lcom/google/gson/x;

    move-result-object v0

    sput-object v0, Lcom/google/gson/internal/bind/i;->J:Lcom/google/gson/x;

    .line 502
    new-instance v0, Lcom/google/gson/internal/bind/i$15;

    invoke-direct {v0}, Lcom/google/gson/internal/bind/i$15;-><init>()V

    sput-object v0, Lcom/google/gson/internal/bind/i;->K:Lcom/google/gson/w;

    .line 522
    const-class v0, Ljava/net/URI;

    sget-object v1, Lcom/google/gson/internal/bind/i;->K:Lcom/google/gson/w;

    invoke-static {v0, v1}, Lcom/google/gson/internal/bind/i;->a(Ljava/lang/Class;Lcom/google/gson/w;)Lcom/google/gson/x;

    move-result-object v0

    sput-object v0, Lcom/google/gson/internal/bind/i;->L:Lcom/google/gson/x;

    .line 524
    new-instance v0, Lcom/google/gson/internal/bind/i$16;

    invoke-direct {v0}, Lcom/google/gson/internal/bind/i$16;-><init>()V

    sput-object v0, Lcom/google/gson/internal/bind/i;->M:Lcom/google/gson/w;

    .line 540
    const-class v0, Ljava/net/InetAddress;

    sget-object v1, Lcom/google/gson/internal/bind/i;->M:Lcom/google/gson/w;

    .line 541
    invoke-static {v0, v1}, Lcom/google/gson/internal/bind/i;->b(Ljava/lang/Class;Lcom/google/gson/w;)Lcom/google/gson/x;

    move-result-object v0

    sput-object v0, Lcom/google/gson/internal/bind/i;->N:Lcom/google/gson/x;

    .line 543
    new-instance v0, Lcom/google/gson/internal/bind/i$17;

    invoke-direct {v0}, Lcom/google/gson/internal/bind/i$17;-><init>()V

    sput-object v0, Lcom/google/gson/internal/bind/i;->O:Lcom/google/gson/w;

    .line 558
    const-class v0, Ljava/util/UUID;

    sget-object v1, Lcom/google/gson/internal/bind/i;->O:Lcom/google/gson/w;

    invoke-static {v0, v1}, Lcom/google/gson/internal/bind/i;->a(Ljava/lang/Class;Lcom/google/gson/w;)Lcom/google/gson/x;

    move-result-object v0

    sput-object v0, Lcom/google/gson/internal/bind/i;->P:Lcom/google/gson/x;

    .line 560
    new-instance v0, Lcom/google/gson/internal/bind/i$18;

    invoke-direct {v0}, Lcom/google/gson/internal/bind/i$18;-><init>()V

    .line 569
    invoke-virtual {v0}, Lcom/google/gson/internal/bind/i$18;->nullSafe()Lcom/google/gson/w;

    move-result-object v0

    sput-object v0, Lcom/google/gson/internal/bind/i;->Q:Lcom/google/gson/w;

    .line 570
    const-class v0, Ljava/util/Currency;

    sget-object v1, Lcom/google/gson/internal/bind/i;->Q:Lcom/google/gson/w;

    invoke-static {v0, v1}, Lcom/google/gson/internal/bind/i;->a(Ljava/lang/Class;Lcom/google/gson/w;)Lcom/google/gson/x;

    move-result-object v0

    sput-object v0, Lcom/google/gson/internal/bind/i;->R:Lcom/google/gson/x;

    .line 572
    new-instance v0, Lcom/google/gson/internal/bind/TypeAdapters$26;

    invoke-direct {v0}, Lcom/google/gson/internal/bind/TypeAdapters$26;-><init>()V

    sput-object v0, Lcom/google/gson/internal/bind/i;->S:Lcom/google/gson/x;

    .line 593
    new-instance v0, Lcom/google/gson/internal/bind/i$19;

    invoke-direct {v0}, Lcom/google/gson/internal/bind/i$19;-><init>()V

    sput-object v0, Lcom/google/gson/internal/bind/i;->T:Lcom/google/gson/w;

    .line 658
    const-class v0, Ljava/util/Calendar;

    const-class v1, Ljava/util/GregorianCalendar;

    sget-object v2, Lcom/google/gson/internal/bind/i;->T:Lcom/google/gson/w;

    .line 659
    invoke-static {v0, v1, v2}, Lcom/google/gson/internal/bind/i;->b(Ljava/lang/Class;Ljava/lang/Class;Lcom/google/gson/w;)Lcom/google/gson/x;

    move-result-object v0

    sput-object v0, Lcom/google/gson/internal/bind/i;->U:Lcom/google/gson/x;

    .line 661
    new-instance v0, Lcom/google/gson/internal/bind/i$20;

    invoke-direct {v0}, Lcom/google/gson/internal/bind/i$20;-><init>()V

    sput-object v0, Lcom/google/gson/internal/bind/i;->V:Lcom/google/gson/w;

    .line 696
    const-class v0, Ljava/util/Locale;

    sget-object v1, Lcom/google/gson/internal/bind/i;->V:Lcom/google/gson/w;

    invoke-static {v0, v1}, Lcom/google/gson/internal/bind/i;->a(Ljava/lang/Class;Lcom/google/gson/w;)Lcom/google/gson/x;

    move-result-object v0

    sput-object v0, Lcom/google/gson/internal/bind/i;->W:Lcom/google/gson/x;

    .line 698
    new-instance v0, Lcom/google/gson/internal/bind/i$21;

    invoke-direct {v0}, Lcom/google/gson/internal/bind/i$21;-><init>()V

    sput-object v0, Lcom/google/gson/internal/bind/i;->X:Lcom/google/gson/w;

    .line 770
    const-class v0, Lcom/google/gson/l;

    sget-object v1, Lcom/google/gson/internal/bind/i;->X:Lcom/google/gson/w;

    .line 771
    invoke-static {v0, v1}, Lcom/google/gson/internal/bind/i;->b(Ljava/lang/Class;Lcom/google/gson/w;)Lcom/google/gson/x;

    move-result-object v0

    sput-object v0, Lcom/google/gson/internal/bind/i;->Y:Lcom/google/gson/x;

    .line 808
    new-instance v0, Lcom/google/gson/internal/bind/TypeAdapters$30;

    invoke-direct {v0}, Lcom/google/gson/internal/bind/TypeAdapters$30;-><init>()V

    sput-object v0, Lcom/google/gson/internal/bind/i;->Z:Lcom/google/gson/x;

    return-void
.end method

.method public static a(Lcom/google/gson/b/a;Lcom/google/gson/w;)Lcom/google/gson/x;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<TT:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/gson/b/a<",
            "TTT;>;",
            "Lcom/google/gson/w<",
            "TTT;>;)",
            "Lcom/google/gson/x;"
        }
    .end annotation

    .line 824
    new-instance v0, Lcom/google/gson/internal/bind/TypeAdapters$31;

    invoke-direct {v0, p0, p1}, Lcom/google/gson/internal/bind/TypeAdapters$31;-><init>(Lcom/google/gson/b/a;Lcom/google/gson/w;)V

    return-object v0
.end method

.method public static a(Ljava/lang/Class;Lcom/google/gson/w;)Lcom/google/gson/x;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<TT:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class<",
            "TTT;>;",
            "Lcom/google/gson/w<",
            "TTT;>;)",
            "Lcom/google/gson/x;"
        }
    .end annotation

    .line 834
    new-instance v0, Lcom/google/gson/internal/bind/TypeAdapters$32;

    invoke-direct {v0, p0, p1}, Lcom/google/gson/internal/bind/TypeAdapters$32;-><init>(Ljava/lang/Class;Lcom/google/gson/w;)V

    return-object v0
.end method

.method public static a(Ljava/lang/Class;Ljava/lang/Class;Lcom/google/gson/w;)Lcom/google/gson/x;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<TT:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class<",
            "TTT;>;",
            "Ljava/lang/Class<",
            "TTT;>;",
            "Lcom/google/gson/w<",
            "-TTT;>;)",
            "Lcom/google/gson/x;"
        }
    .end annotation

    .line 847
    new-instance v0, Lcom/google/gson/internal/bind/TypeAdapters$33;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/gson/internal/bind/TypeAdapters$33;-><init>(Ljava/lang/Class;Ljava/lang/Class;Lcom/google/gson/w;)V

    return-object v0
.end method

.method public static b(Ljava/lang/Class;Lcom/google/gson/w;)Lcom/google/gson/x;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T1:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class<",
            "TT1;>;",
            "Lcom/google/gson/w<",
            "TT1;>;)",
            "Lcom/google/gson/x;"
        }
    .end annotation

    .line 881
    new-instance v0, Lcom/google/gson/internal/bind/TypeAdapters$35;

    invoke-direct {v0, p0, p1}, Lcom/google/gson/internal/bind/TypeAdapters$35;-><init>(Ljava/lang/Class;Lcom/google/gson/w;)V

    return-object v0
.end method

.method public static b(Ljava/lang/Class;Ljava/lang/Class;Lcom/google/gson/w;)Lcom/google/gson/x;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<TT:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class<",
            "TTT;>;",
            "Ljava/lang/Class<",
            "+TTT;>;",
            "Lcom/google/gson/w<",
            "-TTT;>;)",
            "Lcom/google/gson/x;"
        }
    .end annotation

    .line 862
    new-instance v0, Lcom/google/gson/internal/bind/TypeAdapters$34;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/gson/internal/bind/TypeAdapters$34;-><init>(Ljava/lang/Class;Ljava/lang/Class;Lcom/google/gson/w;)V

    return-object v0
.end method
