.class Lcom/stagnationlab/sk/a/a$2;
.super Ljava/lang/Object;
.source "Api.java"

# interfaces
.implements Lee/cyber/smartid/inter/ConfirmTransactionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/stagnationlab/sk/a/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/stagnationlab/sk/a/a/h;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/stagnationlab/sk/a/a/h;

.field final synthetic b:Lcom/stagnationlab/sk/a/a;


# direct methods
.method constructor <init>(Lcom/stagnationlab/sk/a/a;Lcom/stagnationlab/sk/a/a/h;)V
    .locals 0

    .line 429
    iput-object p1, p0, Lcom/stagnationlab/sk/a/a$2;->b:Lcom/stagnationlab/sk/a/a;

    iput-object p2, p0, Lcom/stagnationlab/sk/a/a$2;->a:Lcom/stagnationlab/sk/a/a/h;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onConfirmTransactionFailed(Ljava/lang/String;Lee/cyber/smartid/dto/SmartIdError;)V
    .locals 2

    .line 439
    new-instance p1, Lcom/stagnationlab/sk/d/b;

    invoke-direct {p1, p2}, Lcom/stagnationlab/sk/d/b;-><init>(Lee/cyber/smartid/dto/SmartIdError;)V

    const-string v0, "Api"

    const-string v1, "confirmTransaction failed"

    invoke-static {v0, v1, p1}, Lcom/stagnationlab/sk/f;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 441
    new-instance p1, Lcom/stagnationlab/sk/c/a;

    const-string v0, "confirmTransaction"

    invoke-direct {p1, p2, v0}, Lcom/stagnationlab/sk/c/a;-><init>(Lee/cyber/smartid/dto/SmartIdError;Ljava/lang/String;)V

    .line 442
    iget-object p2, p0, Lcom/stagnationlab/sk/a/a$2;->b:Lcom/stagnationlab/sk/a/a;

    invoke-static {p2, p1}, Lcom/stagnationlab/sk/a/a;->b(Lcom/stagnationlab/sk/a/a;Lcom/stagnationlab/sk/c/a;)V

    .line 443
    iget-object p2, p0, Lcom/stagnationlab/sk/a/a$2;->a:Lcom/stagnationlab/sk/a/a/h;

    invoke-interface {p2, p1}, Lcom/stagnationlab/sk/a/a/h;->a(Lcom/stagnationlab/sk/c/a;)V

    return-void
.end method

.method public onConfirmTransactionSuccess(Ljava/lang/String;Lee/cyber/smartid/tse/dto/jsonrpc/resp/ConfirmTransactionResp;)V
    .locals 0

    .line 434
    iget-object p1, p0, Lcom/stagnationlab/sk/a/a$2;->a:Lcom/stagnationlab/sk/a/a/h;

    invoke-interface {p1}, Lcom/stagnationlab/sk/a/a/h;->a()V

    return-void
.end method
