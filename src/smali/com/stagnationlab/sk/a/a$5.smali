.class Lcom/stagnationlab/sk/a/a$5;
.super Ljava/lang/Object;
.source "Api.java"

# interfaces
.implements Lee/cyber/smartid/inter/CancelTransactionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/stagnationlab/sk/a/a;->a(Lcom/stagnationlab/sk/models/Transaction;Lcom/stagnationlab/sk/a/a/d;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/stagnationlab/sk/a/a/d;

.field final synthetic b:Lcom/stagnationlab/sk/a/a;


# direct methods
.method constructor <init>(Lcom/stagnationlab/sk/a/a;Lcom/stagnationlab/sk/a/a/d;)V
    .locals 0

    .line 544
    iput-object p1, p0, Lcom/stagnationlab/sk/a/a$5;->b:Lcom/stagnationlab/sk/a/a;

    iput-object p2, p0, Lcom/stagnationlab/sk/a/a$5;->a:Lcom/stagnationlab/sk/a/a/d;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCancelTransactionFailed(Ljava/lang/String;Lee/cyber/smartid/dto/SmartIdError;)V
    .locals 2

    .line 554
    new-instance p1, Lcom/stagnationlab/sk/d/b;

    invoke-direct {p1, p2}, Lcom/stagnationlab/sk/d/b;-><init>(Lee/cyber/smartid/dto/SmartIdError;)V

    const-string v0, "Api"

    const-string v1, "Cancel transaction failed"

    invoke-static {v0, v1, p1}, Lcom/stagnationlab/sk/f;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 555
    new-instance p1, Lcom/stagnationlab/sk/c/a;

    const-string v0, "cancelTransaction"

    invoke-direct {p1, p2, v0}, Lcom/stagnationlab/sk/c/a;-><init>(Lee/cyber/smartid/dto/SmartIdError;Ljava/lang/String;)V

    .line 556
    iget-object p2, p0, Lcom/stagnationlab/sk/a/a$5;->b:Lcom/stagnationlab/sk/a/a;

    invoke-static {p2, p1}, Lcom/stagnationlab/sk/a/a;->b(Lcom/stagnationlab/sk/a/a;Lcom/stagnationlab/sk/c/a;)V

    .line 557
    iget-object p2, p0, Lcom/stagnationlab/sk/a/a$5;->a:Lcom/stagnationlab/sk/a/a/d;

    invoke-interface {p2, p1}, Lcom/stagnationlab/sk/a/a/d;->a(Lcom/stagnationlab/sk/c/a;)V

    return-void
.end method

.method public onCancelTransactionSuccess(Ljava/lang/String;Lee/cyber/smartid/dto/jsonrpc/resp/CancelTransactionResp;)V
    .locals 0

    .line 549
    iget-object p1, p0, Lcom/stagnationlab/sk/a/a$5;->a:Lcom/stagnationlab/sk/a/a/d;

    invoke-interface {p1}, Lcom/stagnationlab/sk/a/a/d;->a()V

    return-void
.end method
