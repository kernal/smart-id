.class Lcom/stagnationlab/sk/a/a$24;
.super Ljava/lang/Object;
.source "Api.java"

# interfaces
.implements Lee/cyber/smartid/inter/GetAccountStatusListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/stagnationlab/sk/a/a;->a(ZLcom/stagnationlab/sk/a/a/y;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/stagnationlab/sk/a/a/y;

.field final synthetic b:Lcom/stagnationlab/sk/a/a;


# direct methods
.method constructor <init>(Lcom/stagnationlab/sk/a/a;Lcom/stagnationlab/sk/a/a/y;)V
    .locals 0

    .line 1191
    iput-object p1, p0, Lcom/stagnationlab/sk/a/a$24;->b:Lcom/stagnationlab/sk/a/a;

    iput-object p2, p0, Lcom/stagnationlab/sk/a/a$24;->a:Lcom/stagnationlab/sk/a/a/y;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onGetAccountStatusFailed(Ljava/lang/String;Lee/cyber/smartid/dto/SmartIdError;)V
    .locals 2

    .line 1205
    new-instance p1, Lcom/stagnationlab/sk/d/b;

    invoke-direct {p1, p2}, Lcom/stagnationlab/sk/d/b;-><init>(Lee/cyber/smartid/dto/SmartIdError;)V

    const-string v0, "Api"

    const-string v1, "Get account status failed"

    invoke-static {v0, v1, p1}, Lcom/stagnationlab/sk/f;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1206
    new-instance p1, Lcom/stagnationlab/sk/c/a;

    const-string v0, "getAccountStatus"

    invoke-direct {p1, p2, v0}, Lcom/stagnationlab/sk/c/a;-><init>(Lee/cyber/smartid/dto/SmartIdError;Ljava/lang/String;)V

    .line 1208
    invoke-virtual {p1}, Lcom/stagnationlab/sk/c/a;->h()Z

    move-result p2

    if-eqz p2, :cond_0

    iget-object p2, p0, Lcom/stagnationlab/sk/a/a$24;->b:Lcom/stagnationlab/sk/a/a;

    invoke-static {p2}, Lcom/stagnationlab/sk/a/a;->d(Lcom/stagnationlab/sk/a/a;)Lcom/stagnationlab/sk/models/Account;

    move-result-object p2

    if-eqz p2, :cond_0

    .line 1209
    iget-object p2, p0, Lcom/stagnationlab/sk/a/a$24;->b:Lcom/stagnationlab/sk/a/a;

    invoke-static {p2}, Lcom/stagnationlab/sk/a/a;->d(Lcom/stagnationlab/sk/a/a;)Lcom/stagnationlab/sk/models/Account;

    move-result-object p2

    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Lcom/stagnationlab/sk/models/Account;->a(Z)V

    .line 1211
    :cond_0
    iget-object p2, p0, Lcom/stagnationlab/sk/a/a$24;->a:Lcom/stagnationlab/sk/a/a/y;

    invoke-interface {p2, p1}, Lcom/stagnationlab/sk/a/a/y;->a(Lcom/stagnationlab/sk/c/a;)V

    return-void
.end method

.method public onGetAccountStatusSuccess(Ljava/lang/String;Lee/cyber/smartid/dto/jsonrpc/resp/GetAccountStatusResp;)V
    .locals 1

    .line 1196
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "Got account status: "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string v0, "Api"

    invoke-static {v0, p1}, Lcom/stagnationlab/sk/f;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1198
    iget-object p1, p0, Lcom/stagnationlab/sk/a/a$24;->b:Lcom/stagnationlab/sk/a/a;

    invoke-static {p1}, Lcom/stagnationlab/sk/a/a;->d(Lcom/stagnationlab/sk/a/a;)Lcom/stagnationlab/sk/models/Account;

    move-result-object p1

    invoke-virtual {p1, p2}, Lcom/stagnationlab/sk/models/Account;->a(Lee/cyber/smartid/dto/jsonrpc/resp/GetAccountStatusResp;)V

    .line 1200
    iget-object p1, p0, Lcom/stagnationlab/sk/a/a$24;->a:Lcom/stagnationlab/sk/a/a/y;

    invoke-interface {p1}, Lcom/stagnationlab/sk/a/a/y;->a()V

    return-void
.end method
