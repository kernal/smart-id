.class final Lcom/stagnationlab/sk/a/d$1;
.super Landroid/os/AsyncTask;
.source "Storage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/stagnationlab/sk/a/d;->a(Landroid/content/Context;Lcom/stagnationlab/sk/e/a;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask<",
        "Landroid/content/Context;",
        "Ljava/lang/Void;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/stagnationlab/sk/e/a;


# direct methods
.method constructor <init>(Lcom/stagnationlab/sk/e/a;)V
    .locals 0

    .line 42
    iput-object p1, p0, Lcom/stagnationlab/sk/a/d$1;->a:Lcom/stagnationlab/sk/e/a;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected varargs a([Landroid/content/Context;)Ljava/lang/String;
    .locals 4

    const/4 v0, 0x0

    .line 45
    aget-object p1, p1, v0

    .line 46
    invoke-static {p1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object p1

    invoke-static {p1}, Lcom/stagnationlab/sk/a/d;->a(Landroid/content/SharedPreferences;)Landroid/content/SharedPreferences;

    .line 49
    invoke-static {}, Lcom/stagnationlab/sk/a/d;->l()Ljava/util/Map;

    move-result-object p1

    sget-object v0, Lcom/stagnationlab/sk/a/d$a;->d:Lcom/stagnationlab/sk/a/d$a;

    .line 51
    invoke-static {}, Lcom/stagnationlab/sk/a/d;->k()Landroid/content/SharedPreferences;

    move-result-object v1

    sget-object v2, Lcom/stagnationlab/sk/a/d$a;->d:Lcom/stagnationlab/sk/a/d$a;

    invoke-virtual {v2}, Lcom/stagnationlab/sk/a/d$a;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {}, Lcom/stagnationlab/sk/MyApplication;->a()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 49
    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 54
    invoke-static {}, Lcom/stagnationlab/sk/a/d;->l()Ljava/util/Map;

    move-result-object p1

    sget-object v0, Lcom/stagnationlab/sk/a/d$a;->e:Lcom/stagnationlab/sk/a/d$a;

    .line 56
    invoke-static {}, Lcom/stagnationlab/sk/a/d;->k()Landroid/content/SharedPreferences;

    move-result-object v1

    sget-object v2, Lcom/stagnationlab/sk/a/d$a;->e:Lcom/stagnationlab/sk/a/d$a;

    invoke-virtual {v2}, Lcom/stagnationlab/sk/a/d$a;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 54
    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-object v3
.end method

.method protected a(Ljava/lang/String;)V
    .locals 1

    const-string p1, "Storage"

    const-string v0, "Storage initialized"

    .line 63
    invoke-static {p1, v0}, Lcom/stagnationlab/sk/f;->d(Ljava/lang/String;Ljava/lang/String;)V

    const/4 p1, 0x1

    .line 65
    invoke-static {p1}, Lcom/stagnationlab/sk/a/d;->f(Z)Z

    .line 66
    iget-object p1, p0, Lcom/stagnationlab/sk/a/d$1;->a:Lcom/stagnationlab/sk/e/a;

    invoke-interface {p1}, Lcom/stagnationlab/sk/e/a;->a()V

    return-void
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 42
    check-cast p1, [Landroid/content/Context;

    invoke-virtual {p0, p1}, Lcom/stagnationlab/sk/a/d$1;->a([Landroid/content/Context;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .line 42
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/stagnationlab/sk/a/d$1;->a(Ljava/lang/String;)V

    return-void
.end method
