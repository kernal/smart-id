.class Lcom/stagnationlab/sk/a/a$20;
.super Ljava/lang/Object;
.source "Api.java"

# interfaces
.implements Lee/cyber/smartid/inter/GetKeyPinLengthListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/stagnationlab/sk/a/a;->a(Lcom/stagnationlab/sk/models/Transaction$Type;Lcom/stagnationlab/sk/a/a/p;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/stagnationlab/sk/a/a/p;

.field final synthetic b:Lcom/stagnationlab/sk/a/a;


# direct methods
.method constructor <init>(Lcom/stagnationlab/sk/a/a;Lcom/stagnationlab/sk/a/a/p;)V
    .locals 0

    .line 1096
    iput-object p1, p0, Lcom/stagnationlab/sk/a/a$20;->b:Lcom/stagnationlab/sk/a/a;

    iput-object p2, p0, Lcom/stagnationlab/sk/a/a$20;->a:Lcom/stagnationlab/sk/a/a/p;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onGetKeyPinLengthFailed(Ljava/lang/String;Lee/cyber/smartid/dto/SmartIdError;)V
    .locals 2

    .line 1106
    new-instance p1, Lcom/stagnationlab/sk/d/b;

    invoke-direct {p1, p2}, Lcom/stagnationlab/sk/d/b;-><init>(Lee/cyber/smartid/dto/SmartIdError;)V

    const-string v0, "Api"

    const-string v1, "Get key pin length failed"

    invoke-static {v0, v1, p1}, Lcom/stagnationlab/sk/f;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    const-string p1, "getKeyPinLength"

    .line 1107
    invoke-static {p2, p1}, Lcom/stagnationlab/sk/c/a;->a(Lee/cyber/smartid/dto/SmartIdError;Ljava/lang/String;)V

    .line 1108
    iget-object p1, p0, Lcom/stagnationlab/sk/a/a$20;->a:Lcom/stagnationlab/sk/a/a/p;

    const/4 p2, -0x1

    invoke-interface {p1, p2}, Lcom/stagnationlab/sk/a/a/p;->a(I)V

    return-void
.end method

.method public onGetKeyPinLengthSuccess(Ljava/lang/String;Lee/cyber/smartid/dto/jsonrpc/resp/GetKeyPinLengthResp;)V
    .locals 0

    .line 1101
    iget-object p1, p0, Lcom/stagnationlab/sk/a/a$20;->a:Lcom/stagnationlab/sk/a/a/p;

    invoke-virtual {p2}, Lee/cyber/smartid/dto/jsonrpc/resp/GetKeyPinLengthResp;->getKeyPinLength()I

    move-result p2

    invoke-interface {p1, p2}, Lcom/stagnationlab/sk/a/a/p;->a(I)V

    return-void
.end method
