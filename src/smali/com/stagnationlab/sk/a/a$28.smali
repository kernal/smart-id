.class Lcom/stagnationlab/sk/a/a$28;
.super Ljava/lang/Object;
.source "Api.java"

# interfaces
.implements Lee/cyber/smartid/inter/CancelInteractiveUpgradeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/stagnationlab/sk/a/a;->a(Lcom/stagnationlab/sk/a/a/b;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/stagnationlab/sk/a/a/b;

.field final synthetic b:Lcom/stagnationlab/sk/a/a;


# direct methods
.method constructor <init>(Lcom/stagnationlab/sk/a/a;Lcom/stagnationlab/sk/a/a/b;)V
    .locals 0

    .line 1336
    iput-object p1, p0, Lcom/stagnationlab/sk/a/a$28;->b:Lcom/stagnationlab/sk/a/a;

    iput-object p2, p0, Lcom/stagnationlab/sk/a/a$28;->a:Lcom/stagnationlab/sk/a/a/b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCancelInteractiveUpgradeFailed(Ljava/lang/String;Lee/cyber/smartid/dto/SmartIdError;)V
    .locals 2

    .line 1346
    new-instance p1, Lcom/stagnationlab/sk/d/b;

    invoke-direct {p1, p2}, Lcom/stagnationlab/sk/d/b;-><init>(Lee/cyber/smartid/dto/SmartIdError;)V

    const-string v0, "Api"

    const-string v1, "Cancel interactive upgrade failed"

    invoke-static {v0, v1, p1}, Lcom/stagnationlab/sk/f;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1347
    iget-object p1, p0, Lcom/stagnationlab/sk/a/a$28;->a:Lcom/stagnationlab/sk/a/a/b;

    new-instance v0, Lcom/stagnationlab/sk/c/a;

    const-string v1, "cancelInteractiveUpgrade"

    invoke-direct {v0, p2, v1}, Lcom/stagnationlab/sk/c/a;-><init>(Lee/cyber/smartid/dto/SmartIdError;Ljava/lang/String;)V

    invoke-interface {p1, v0}, Lcom/stagnationlab/sk/a/a/b;->a(Lcom/stagnationlab/sk/c/a;)V

    return-void
.end method

.method public onCancelInteractiveUpgradeSuccess(Ljava/lang/String;Lee/cyber/smartid/dto/jsonrpc/resp/CancelInteractiveUpgradeResp;)V
    .locals 0

    .line 1341
    iget-object p1, p0, Lcom/stagnationlab/sk/a/a$28;->a:Lcom/stagnationlab/sk/a/a/b;

    invoke-interface {p1}, Lcom/stagnationlab/sk/a/a/b;->a()V

    return-void
.end method
