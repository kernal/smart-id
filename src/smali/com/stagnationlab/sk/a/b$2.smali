.class Lcom/stagnationlab/sk/a/b$2;
.super Ljava/lang/Object;
.source "HybridInitializer.java"

# interfaces
.implements Lcom/stagnationlab/sk/a/a/y;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/stagnationlab/sk/a/b;->a(Lcom/stagnationlab/sk/models/Account;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/stagnationlab/sk/models/Account;

.field final synthetic b:Lcom/stagnationlab/sk/a/b;


# direct methods
.method constructor <init>(Lcom/stagnationlab/sk/a/b;Lcom/stagnationlab/sk/models/Account;)V
    .locals 0

    .line 113
    iput-object p1, p0, Lcom/stagnationlab/sk/a/b$2;->b:Lcom/stagnationlab/sk/a/b;

    iput-object p2, p0, Lcom/stagnationlab/sk/a/b$2;->a:Lcom/stagnationlab/sk/models/Account;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    .line 116
    iget-object v0, p0, Lcom/stagnationlab/sk/a/b$2;->a:Lcom/stagnationlab/sk/models/Account;

    invoke-virtual {v0}, Lcom/stagnationlab/sk/models/Account;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 117
    sget-object v0, Lcom/stagnationlab/sk/util/i$b;->a:Lcom/stagnationlab/sk/util/i$b;

    invoke-static {v0}, Lcom/stagnationlab/sk/util/i;->a(Lcom/stagnationlab/sk/util/i$b;)V

    .line 120
    :cond_0
    iget-object v0, p0, Lcom/stagnationlab/sk/a/b$2;->b:Lcom/stagnationlab/sk/a/b;

    invoke-static {v0}, Lcom/stagnationlab/sk/a/b;->c(Lcom/stagnationlab/sk/a/b;)Lcom/stagnationlab/sk/h/d;

    move-result-object v0

    new-instance v1, Lcom/stagnationlab/sk/a/b$2$1;

    invoke-direct {v1, p0}, Lcom/stagnationlab/sk/a/b$2$1;-><init>(Lcom/stagnationlab/sk/a/b$2;)V

    invoke-virtual {v0, v1}, Lcom/stagnationlab/sk/h/d;->a(Lcom/stagnationlab/sk/d;)V

    return-void
.end method

.method public a(Lcom/stagnationlab/sk/c/a;)V
    .locals 3

    .line 131
    invoke-virtual {p1}, Lcom/stagnationlab/sk/c/a;->h()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 132
    iget-object p1, p0, Lcom/stagnationlab/sk/a/b$2;->b:Lcom/stagnationlab/sk/a/b;

    const/4 v0, 0x0

    invoke-static {p1, v0}, Lcom/stagnationlab/sk/a/b;->a(Lcom/stagnationlab/sk/a/b;Z)V

    return-void

    .line 137
    :cond_0
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/stagnationlab/sk/a/b$2;->b:Lcom/stagnationlab/sk/a/b;

    invoke-static {v1}, Lcom/stagnationlab/sk/a/b;->a(Lcom/stagnationlab/sk/a/b;)Lcom/stagnationlab/sk/MainActivity;

    move-result-object v1

    const-class v2, Lcom/stagnationlab/sk/ErrorActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "error"

    .line 138
    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 139
    iget-object p1, p0, Lcom/stagnationlab/sk/a/b$2;->b:Lcom/stagnationlab/sk/a/b;

    invoke-static {p1}, Lcom/stagnationlab/sk/a/b;->a(Lcom/stagnationlab/sk/a/b;)Lcom/stagnationlab/sk/MainActivity;

    move-result-object p1

    invoke-virtual {p1, v0}, Lcom/stagnationlab/sk/MainActivity;->startActivity(Landroid/content/Intent;)V

    .line 140
    iget-object p1, p0, Lcom/stagnationlab/sk/a/b$2;->b:Lcom/stagnationlab/sk/a/b;

    invoke-static {p1}, Lcom/stagnationlab/sk/a/b;->a(Lcom/stagnationlab/sk/a/b;)Lcom/stagnationlab/sk/MainActivity;

    move-result-object p1

    invoke-virtual {p1}, Lcom/stagnationlab/sk/MainActivity;->finish()V

    return-void
.end method
