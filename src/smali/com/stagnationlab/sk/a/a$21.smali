.class Lcom/stagnationlab/sk/a/a$21;
.super Ljava/lang/Object;
.source "Api.java"

# interfaces
.implements Lee/cyber/smartid/inter/CancelRPRequestListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/stagnationlab/sk/a/a;->a(Lcom/stagnationlab/sk/models/RpRequest;Lcom/stagnationlab/sk/a/a/c;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/stagnationlab/sk/a/a/c;

.field final synthetic b:Lcom/stagnationlab/sk/a/a;


# direct methods
.method constructor <init>(Lcom/stagnationlab/sk/a/a;Lcom/stagnationlab/sk/a/a/c;)V
    .locals 0

    .line 1139
    iput-object p1, p0, Lcom/stagnationlab/sk/a/a$21;->b:Lcom/stagnationlab/sk/a/a;

    iput-object p2, p0, Lcom/stagnationlab/sk/a/a$21;->a:Lcom/stagnationlab/sk/a/a/c;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCancelRPRequestFailed(Ljava/lang/String;Lee/cyber/smartid/dto/SmartIdError;)V
    .locals 2

    .line 1148
    new-instance p1, Lcom/stagnationlab/sk/d/b;

    invoke-direct {p1, p2}, Lcom/stagnationlab/sk/d/b;-><init>(Lee/cyber/smartid/dto/SmartIdError;)V

    const-string v0, "Api"

    const-string v1, "Cancel RPRequest failed"

    invoke-static {v0, v1, p1}, Lcom/stagnationlab/sk/f;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1149
    new-instance p1, Lcom/stagnationlab/sk/c/a;

    const-string v0, "cancelRPRequest"

    invoke-direct {p1, p2, v0}, Lcom/stagnationlab/sk/c/a;-><init>(Lee/cyber/smartid/dto/SmartIdError;Ljava/lang/String;)V

    .line 1150
    iget-object p2, p0, Lcom/stagnationlab/sk/a/a$21;->b:Lcom/stagnationlab/sk/a/a;

    invoke-static {p2, p1}, Lcom/stagnationlab/sk/a/a;->b(Lcom/stagnationlab/sk/a/a;Lcom/stagnationlab/sk/c/a;)V

    .line 1151
    iget-object p2, p0, Lcom/stagnationlab/sk/a/a$21;->a:Lcom/stagnationlab/sk/a/a/c;

    invoke-interface {p2, p1}, Lcom/stagnationlab/sk/a/a/c;->a(Lcom/stagnationlab/sk/c/a;)V

    return-void
.end method

.method public onCancelRPRequestSuccess(Ljava/lang/String;Lee/cyber/smartid/dto/jsonrpc/resp/CancelRPRequestResp;)V
    .locals 0

    const-string p1, "Api"

    const-string p2, "Cancel RPRequest success"

    .line 1142
    invoke-static {p1, p2}, Lcom/stagnationlab/sk/f;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1143
    iget-object p1, p0, Lcom/stagnationlab/sk/a/a$21;->a:Lcom/stagnationlab/sk/a/a/c;

    invoke-interface {p1}, Lcom/stagnationlab/sk/a/a/c;->a()V

    return-void
.end method
