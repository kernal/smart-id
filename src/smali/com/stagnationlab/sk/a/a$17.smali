.class Lcom/stagnationlab/sk/a/a$17;
.super Ljava/lang/Object;
.source "Api.java"

# interfaces
.implements Lee/cyber/smartid/inter/EncryptKeysListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/stagnationlab/sk/a/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/stagnationlab/sk/a/a/l;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/stagnationlab/sk/a/a/l;

.field final synthetic b:Lcom/stagnationlab/sk/a/a;


# direct methods
.method constructor <init>(Lcom/stagnationlab/sk/a/a;Lcom/stagnationlab/sk/a/a/l;)V
    .locals 0

    .line 983
    iput-object p1, p0, Lcom/stagnationlab/sk/a/a$17;->b:Lcom/stagnationlab/sk/a/a;

    iput-object p2, p0, Lcom/stagnationlab/sk/a/a$17;->a:Lcom/stagnationlab/sk/a/a/l;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onEncryptKeysFailed(Ljava/lang/String;Lee/cyber/smartid/dto/SmartIdError;)V
    .locals 2

    .line 991
    new-instance p1, Lcom/stagnationlab/sk/d/b;

    invoke-direct {p1, p2}, Lcom/stagnationlab/sk/d/b;-><init>(Lee/cyber/smartid/dto/SmartIdError;)V

    const-string v0, "Api"

    const-string v1, "Encrypt keys failed"

    invoke-static {v0, v1, p1}, Lcom/stagnationlab/sk/f;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 996
    iget-object p1, p0, Lcom/stagnationlab/sk/a/a$17;->a:Lcom/stagnationlab/sk/a/a/l;

    new-instance v0, Lcom/stagnationlab/sk/c/a;

    const-string v1, "encryptKeys"

    invoke-direct {v0, p2, v1}, Lcom/stagnationlab/sk/c/a;-><init>(Lee/cyber/smartid/dto/SmartIdError;Ljava/lang/String;)V

    invoke-interface {p1, v0}, Lcom/stagnationlab/sk/a/a/l;->a(Lcom/stagnationlab/sk/c/a;)V

    return-void
.end method

.method public onEncryptKeysSuccess(Ljava/lang/String;Lee/cyber/smartid/dto/jsonrpc/resp/EncryptKeysResp;)V
    .locals 0

    .line 986
    iget-object p1, p0, Lcom/stagnationlab/sk/a/a$17;->a:Lcom/stagnationlab/sk/a/a/l;

    invoke-interface {p1}, Lcom/stagnationlab/sk/a/a/l;->a()V

    return-void
.end method
