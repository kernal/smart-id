.class public Lcom/stagnationlab/sk/a/d;
.super Ljava/lang/Object;
.source "Storage.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/stagnationlab/sk/a/d$a;
    }
.end annotation


# static fields
.field private static a:Landroid/content/SharedPreferences; = null

.field private static b:Z = false

.field private static c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Lcom/stagnationlab/sk/a/d$a;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 27
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/stagnationlab/sk/a/d;->c:Ljava/util/Map;

    return-void
.end method

.method private static a(Lcom/stagnationlab/sk/a/d$a;I)I
    .locals 1

    .line 227
    sget-boolean v0, Lcom/stagnationlab/sk/a/d;->b:Z

    if-nez v0, :cond_0

    return p1

    .line 231
    :cond_0
    sget-object v0, Lcom/stagnationlab/sk/a/d;->a:Landroid/content/SharedPreferences;

    invoke-virtual {p0}, Lcom/stagnationlab/sk/a/d$a;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-interface {v0, p0, p1}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result p0

    return p0
.end method

.method static synthetic a(Landroid/content/SharedPreferences;)Landroid/content/SharedPreferences;
    .locals 0

    .line 23
    sput-object p0, Lcom/stagnationlab/sk/a/d;->a:Landroid/content/SharedPreferences;

    return-object p0
.end method

.method private static a(Lcom/stagnationlab/sk/a/d$a;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .line 192
    sget-boolean v0, Lcom/stagnationlab/sk/a/d;->b:Z

    if-nez v0, :cond_0

    return-object p1

    .line 195
    :cond_0
    sget-object v0, Lcom/stagnationlab/sk/a/d;->c:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object p1, Lcom/stagnationlab/sk/a/d;->c:Ljava/util/Map;

    invoke-interface {p1, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    move-object p1, p0

    check-cast p1, Ljava/lang/String;

    :cond_1
    return-object p1
.end method

.method public static a(Landroid/content/Context;Lcom/stagnationlab/sk/e/a;)V
    .locals 2

    .line 42
    new-instance v0, Lcom/stagnationlab/sk/a/d$1;

    invoke-direct {v0, p1}, Lcom/stagnationlab/sk/a/d$1;-><init>(Lcom/stagnationlab/sk/e/a;)V

    const/4 p1, 0x1

    new-array p1, p1, [Landroid/content/Context;

    const/4 v1, 0x0

    aput-object p0, p1, v1

    .line 69
    invoke-virtual {v0, p1}, Lcom/stagnationlab/sk/a/d$1;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

.method private static a(Lcom/stagnationlab/sk/a/d$a;)V
    .locals 1

    .line 137
    sget-boolean v0, Lcom/stagnationlab/sk/a/d;->b:Z

    if-nez v0, :cond_0

    return-void

    .line 141
    :cond_0
    sget-object v0, Lcom/stagnationlab/sk/a/d;->c:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 142
    sget-object v0, Lcom/stagnationlab/sk/a/d;->a:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-virtual {p0}, Lcom/stagnationlab/sk/a/d$a;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-interface {v0, p0}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object p0

    invoke-interface {p0}, Landroid/content/SharedPreferences$Editor;->apply()V

    return-void
.end method

.method public static a(Lcom/stagnationlab/sk/b/a;)V
    .locals 1

    .line 133
    sget-object v0, Lcom/stagnationlab/sk/a/d$a;->g:Lcom/stagnationlab/sk/a/d$a;

    invoke-virtual {p0}, Lcom/stagnationlab/sk/b/a;->a()I

    move-result p0

    invoke-static {v0, p0}, Lcom/stagnationlab/sk/a/d;->b(Lcom/stagnationlab/sk/a/d$a;I)V

    return-void
.end method

.method public static a(Lcom/stagnationlab/sk/b/b;)V
    .locals 1

    .line 121
    sget-object v0, Lcom/stagnationlab/sk/a/d$a;->f:Lcom/stagnationlab/sk/a/d$a;

    invoke-virtual {p0}, Lcom/stagnationlab/sk/b/b;->a()I

    move-result p0

    invoke-static {v0, p0}, Lcom/stagnationlab/sk/a/d;->b(Lcom/stagnationlab/sk/a/d$a;I)V

    return-void
.end method

.method public static a(Ljava/lang/String;)V
    .locals 1

    .line 166
    sget-object v0, Lcom/stagnationlab/sk/a/d$a;->d:Lcom/stagnationlab/sk/a/d$a;

    invoke-static {v0, p0}, Lcom/stagnationlab/sk/a/d;->b(Lcom/stagnationlab/sk/a/d$a;Ljava/lang/String;)V

    return-void
.end method

.method public static a(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/stagnationlab/sk/models/LegalGuardian;",
            ">;)V"
        }
    .end annotation

    if-nez p0, :cond_0

    .line 183
    sget-object p0, Lcom/stagnationlab/sk/a/d$a;->e:Lcom/stagnationlab/sk/a/d$a;

    invoke-static {p0}, Lcom/stagnationlab/sk/a/d;->a(Lcom/stagnationlab/sk/a/d$a;)V

    goto :goto_0

    .line 185
    :cond_0
    new-instance v0, Lcom/google/gson/f;

    invoke-direct {v0}, Lcom/google/gson/f;-><init>()V

    invoke-virtual {v0, p0}, Lcom/google/gson/f;->b(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    .line 187
    sget-object v0, Lcom/stagnationlab/sk/a/d$a;->e:Lcom/stagnationlab/sk/a/d$a;

    invoke-static {v0, p0}, Lcom/stagnationlab/sk/a/d;->b(Lcom/stagnationlab/sk/a/d$a;Ljava/lang/String;)V

    :goto_0
    return-void
.end method

.method public static a(Z)V
    .locals 1

    .line 89
    sget-object v0, Lcom/stagnationlab/sk/a/d$a;->c:Lcom/stagnationlab/sk/a/d$a;

    invoke-static {v0, p0}, Lcom/stagnationlab/sk/a/d;->b(Lcom/stagnationlab/sk/a/d$a;Z)V

    return-void
.end method

.method public static a()Z
    .locals 2

    .line 73
    sget-object v0, Lcom/stagnationlab/sk/a/d$a;->c:Lcom/stagnationlab/sk/a/d$a;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/stagnationlab/sk/a/d;->a(Lcom/stagnationlab/sk/a/d$a;Z)Z

    move-result v0

    return v0
.end method

.method public static a(Landroid/content/Context;)Z
    .locals 2

    .line 81
    sget-boolean v0, Lcom/stagnationlab/sk/a/d;->b:Z

    if-nez v0, :cond_0

    .line 82
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object p0

    sput-object p0, Lcom/stagnationlab/sk/a/d;->a:Landroid/content/SharedPreferences;

    .line 85
    :cond_0
    sget-object p0, Lcom/stagnationlab/sk/a/d$a;->i:Lcom/stagnationlab/sk/a/d$a;

    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-static {p0, v0, v1}, Lcom/stagnationlab/sk/a/d;->a(Lcom/stagnationlab/sk/a/d$a;ZZ)Z

    move-result p0

    return p0
.end method

.method private static a(Lcom/stagnationlab/sk/a/d$a;Z)Z
    .locals 1

    const/4 v0, 0x0

    .line 208
    invoke-static {p0, p1, v0}, Lcom/stagnationlab/sk/a/d;->a(Lcom/stagnationlab/sk/a/d$a;ZZ)Z

    move-result p0

    return p0
.end method

.method private static a(Lcom/stagnationlab/sk/a/d$a;ZZ)Z
    .locals 1

    .line 212
    sget-boolean v0, Lcom/stagnationlab/sk/a/d;->b:Z

    if-nez v0, :cond_0

    if-nez p2, :cond_0

    return p1

    .line 216
    :cond_0
    sget-object p2, Lcom/stagnationlab/sk/a/d;->a:Landroid/content/SharedPreferences;

    invoke-virtual {p0}, Lcom/stagnationlab/sk/a/d$a;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-interface {p2, p0, p1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result p0

    return p0
.end method

.method private static b(Lcom/stagnationlab/sk/a/d$a;I)V
    .locals 1

    .line 235
    sget-boolean v0, Lcom/stagnationlab/sk/a/d;->b:Z

    if-nez v0, :cond_0

    return-void

    .line 239
    :cond_0
    sget-object v0, Lcom/stagnationlab/sk/a/d;->a:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-virtual {p0}, Lcom/stagnationlab/sk/a/d$a;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-interface {v0, p0, p1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object p0

    invoke-interface {p0}, Landroid/content/SharedPreferences$Editor;->apply()V

    return-void
.end method

.method private static b(Lcom/stagnationlab/sk/a/d$a;Ljava/lang/String;)V
    .locals 1

    .line 199
    sget-boolean v0, Lcom/stagnationlab/sk/a/d;->b:Z

    if-nez v0, :cond_0

    return-void

    .line 203
    :cond_0
    sget-object v0, Lcom/stagnationlab/sk/a/d;->c:Ljava/util/Map;

    invoke-interface {v0, p0, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 204
    sget-object v0, Lcom/stagnationlab/sk/a/d;->a:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-virtual {p0}, Lcom/stagnationlab/sk/a/d$a;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-interface {v0, p0, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object p0

    invoke-interface {p0}, Landroid/content/SharedPreferences$Editor;->apply()V

    return-void
.end method

.method private static b(Lcom/stagnationlab/sk/a/d$a;Z)V
    .locals 1

    .line 220
    sget-boolean v0, Lcom/stagnationlab/sk/a/d;->b:Z

    if-nez v0, :cond_0

    return-void

    .line 223
    :cond_0
    sget-object v0, Lcom/stagnationlab/sk/a/d;->a:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-virtual {p0}, Lcom/stagnationlab/sk/a/d$a;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-interface {v0, p0, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object p0

    invoke-interface {p0}, Landroid/content/SharedPreferences$Editor;->apply()V

    return-void
.end method

.method public static b(Z)V
    .locals 1

    .line 97
    sget-object v0, Lcom/stagnationlab/sk/a/d$a;->h:Lcom/stagnationlab/sk/a/d$a;

    invoke-static {v0, p0}, Lcom/stagnationlab/sk/a/d;->b(Lcom/stagnationlab/sk/a/d$a;Z)V

    return-void
.end method

.method public static b()Z
    .locals 2

    .line 77
    sget-object v0, Lcom/stagnationlab/sk/a/d$a;->i:Lcom/stagnationlab/sk/a/d$a;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/stagnationlab/sk/a/d;->a(Lcom/stagnationlab/sk/a/d$a;Z)Z

    move-result v0

    return v0
.end method

.method public static c(Z)V
    .locals 1

    .line 101
    sget-object v0, Lcom/stagnationlab/sk/a/d$a;->i:Lcom/stagnationlab/sk/a/d$a;

    invoke-static {v0, p0}, Lcom/stagnationlab/sk/a/d;->b(Lcom/stagnationlab/sk/a/d$a;Z)V

    return-void
.end method

.method public static c()Z
    .locals 2

    .line 93
    sget-object v0, Lcom/stagnationlab/sk/a/d$a;->h:Lcom/stagnationlab/sk/a/d$a;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/stagnationlab/sk/a/d;->a(Lcom/stagnationlab/sk/a/d$a;Z)Z

    move-result v0

    return v0
.end method

.method public static d(Z)V
    .locals 1

    .line 113
    sget-object v0, Lcom/stagnationlab/sk/a/d$a;->a:Lcom/stagnationlab/sk/a/d$a;

    invoke-static {v0, p0}, Lcom/stagnationlab/sk/a/d;->b(Lcom/stagnationlab/sk/a/d$a;Z)V

    return-void
.end method

.method public static d()Z
    .locals 2

    .line 105
    sget-object v0, Lcom/stagnationlab/sk/a/d$a;->a:Lcom/stagnationlab/sk/a/d$a;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/stagnationlab/sk/a/d;->a(Lcom/stagnationlab/sk/a/d$a;Z)Z

    move-result v0

    return v0
.end method

.method public static e(Z)V
    .locals 1

    .line 117
    sget-object v0, Lcom/stagnationlab/sk/a/d$a;->b:Lcom/stagnationlab/sk/a/d$a;

    invoke-static {v0, p0}, Lcom/stagnationlab/sk/a/d;->b(Lcom/stagnationlab/sk/a/d$a;Z)V

    return-void
.end method

.method public static e()Z
    .locals 2

    .line 109
    sget-object v0, Lcom/stagnationlab/sk/a/d$a;->b:Lcom/stagnationlab/sk/a/d$a;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/stagnationlab/sk/a/d;->a(Lcom/stagnationlab/sk/a/d$a;Z)Z

    move-result v0

    return v0
.end method

.method public static f()Lcom/stagnationlab/sk/b/b;
    .locals 2

    .line 125
    sget-object v0, Lcom/stagnationlab/sk/a/d$a;->f:Lcom/stagnationlab/sk/a/d$a;

    sget-object v1, Lcom/stagnationlab/sk/b/b;->c:Lcom/stagnationlab/sk/b/b;

    invoke-virtual {v1}, Lcom/stagnationlab/sk/b/b;->a()I

    move-result v1

    invoke-static {v0, v1}, Lcom/stagnationlab/sk/a/d;->a(Lcom/stagnationlab/sk/a/d$a;I)I

    move-result v0

    invoke-static {v0}, Lcom/stagnationlab/sk/b/b;->a(I)Lcom/stagnationlab/sk/b/b;

    move-result-object v0

    return-object v0
.end method

.method static synthetic f(Z)Z
    .locals 0

    .line 23
    sput-boolean p0, Lcom/stagnationlab/sk/a/d;->b:Z

    return p0
.end method

.method public static g()Lcom/stagnationlab/sk/b/a;
    .locals 2

    .line 129
    sget-object v0, Lcom/stagnationlab/sk/a/d$a;->g:Lcom/stagnationlab/sk/a/d$a;

    sget-object v1, Lcom/stagnationlab/sk/b/a;->b:Lcom/stagnationlab/sk/b/a;

    invoke-virtual {v1}, Lcom/stagnationlab/sk/b/a;->a()I

    move-result v1

    invoke-static {v0, v1}, Lcom/stagnationlab/sk/a/d;->a(Lcom/stagnationlab/sk/a/d$a;I)I

    move-result v0

    invoke-static {v0}, Lcom/stagnationlab/sk/b/a;->a(I)Lcom/stagnationlab/sk/b/a;

    move-result-object v0

    return-object v0
.end method

.method static h()V
    .locals 2

    .line 146
    sget-boolean v0, Lcom/stagnationlab/sk/a/d;->b:Z

    if-nez v0, :cond_0

    return-void

    .line 150
    :cond_0
    sget-object v0, Lcom/stagnationlab/sk/a/d;->c:Ljava/util/Map;

    sget-object v1, Lcom/stagnationlab/sk/a/d$a;->e:Lcom/stagnationlab/sk/a/d$a;

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 152
    sget-object v0, Lcom/stagnationlab/sk/a/d;->a:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    sget-object v1, Lcom/stagnationlab/sk/a/d$a;->a:Lcom/stagnationlab/sk/a/d$a;

    .line 153
    invoke-virtual {v1}, Lcom/stagnationlab/sk/a/d$a;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    sget-object v1, Lcom/stagnationlab/sk/a/d$a;->b:Lcom/stagnationlab/sk/a/d$a;

    .line 154
    invoke-virtual {v1}, Lcom/stagnationlab/sk/a/d$a;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    sget-object v1, Lcom/stagnationlab/sk/a/d$a;->e:Lcom/stagnationlab/sk/a/d$a;

    .line 155
    invoke-virtual {v1}, Lcom/stagnationlab/sk/a/d$a;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    sget-object v1, Lcom/stagnationlab/sk/a/d$a;->f:Lcom/stagnationlab/sk/a/d$a;

    .line 156
    invoke-virtual {v1}, Lcom/stagnationlab/sk/a/d$a;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    sget-object v1, Lcom/stagnationlab/sk/a/d$a;->c:Lcom/stagnationlab/sk/a/d$a;

    .line 157
    invoke-virtual {v1}, Lcom/stagnationlab/sk/a/d$a;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 158
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    return-void
.end method

.method public static i()Ljava/lang/String;
    .locals 2

    .line 162
    sget-object v0, Lcom/stagnationlab/sk/a/d$a;->d:Lcom/stagnationlab/sk/a/d$a;

    invoke-static {}, Lcom/stagnationlab/sk/MyApplication;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/stagnationlab/sk/a/d;->a(Lcom/stagnationlab/sk/a/d$a;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static j()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/stagnationlab/sk/models/LegalGuardian;",
            ">;"
        }
    .end annotation

    .line 170
    sget-object v0, Lcom/stagnationlab/sk/a/d$a;->e:Lcom/stagnationlab/sk/a/d$a;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/stagnationlab/sk/a/d;->a(Lcom/stagnationlab/sk/a/d$a;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    return-object v1

    .line 176
    :cond_0
    new-instance v1, Lcom/stagnationlab/sk/a/d$2;

    invoke-direct {v1}, Lcom/stagnationlab/sk/a/d$2;-><init>()V

    invoke-virtual {v1}, Lcom/stagnationlab/sk/a/d$2;->getType()Ljava/lang/reflect/Type;

    move-result-object v1

    .line 178
    new-instance v2, Lcom/google/gson/f;

    invoke-direct {v2}, Lcom/google/gson/f;-><init>()V

    invoke-virtual {v2, v0, v1}, Lcom/google/gson/f;->a(Ljava/lang/String;Ljava/lang/reflect/Type;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    return-object v0
.end method

.method static synthetic k()Landroid/content/SharedPreferences;
    .locals 1

    .line 23
    sget-object v0, Lcom/stagnationlab/sk/a/d;->a:Landroid/content/SharedPreferences;

    return-object v0
.end method

.method static synthetic l()Ljava/util/Map;
    .locals 1

    .line 23
    sget-object v0, Lcom/stagnationlab/sk/a/d;->c:Ljava/util/Map;

    return-object v0
.end method
