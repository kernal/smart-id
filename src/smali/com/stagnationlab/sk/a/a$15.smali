.class Lcom/stagnationlab/sk/a/a$15;
.super Ljava/lang/Object;
.source "Api.java"

# interfaces
.implements Lee/cyber/smartid/inter/GetTransactionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/stagnationlab/sk/a/a;->a(Ljava/lang/String;Lcom/stagnationlab/sk/a/a/t;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/stagnationlab/sk/a/a/t;

.field final synthetic b:Lcom/stagnationlab/sk/a/a;


# direct methods
.method constructor <init>(Lcom/stagnationlab/sk/a/a;Lcom/stagnationlab/sk/a/a/t;)V
    .locals 0

    .line 915
    iput-object p1, p0, Lcom/stagnationlab/sk/a/a$15;->b:Lcom/stagnationlab/sk/a/a;

    iput-object p2, p0, Lcom/stagnationlab/sk/a/a$15;->a:Lcom/stagnationlab/sk/a/a/t;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onGetTransactionFailed(Ljava/lang/String;Lee/cyber/smartid/dto/SmartIdError;)V
    .locals 2

    .line 925
    new-instance p1, Lcom/stagnationlab/sk/d/b;

    invoke-direct {p1, p2}, Lcom/stagnationlab/sk/d/b;-><init>(Lee/cyber/smartid/dto/SmartIdError;)V

    const-string v0, "Api"

    const-string v1, "Get transaction failed"

    invoke-static {v0, v1, p1}, Lcom/stagnationlab/sk/f;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 927
    iget-object p1, p0, Lcom/stagnationlab/sk/a/a$15;->a:Lcom/stagnationlab/sk/a/a/t;

    new-instance v0, Lcom/stagnationlab/sk/c/a;

    const-string v1, "getTransaction"

    invoke-direct {v0, p2, v1}, Lcom/stagnationlab/sk/c/a;-><init>(Lee/cyber/smartid/dto/SmartIdError;Ljava/lang/String;)V

    invoke-interface {p1, v0}, Lcom/stagnationlab/sk/a/a/t;->a(Lcom/stagnationlab/sk/c/a;)V

    return-void
.end method

.method public onGetTransactionSuccess(Ljava/lang/String;Lee/cyber/smartid/dto/jsonrpc/resp/GetTransactionResp;)V
    .locals 1

    .line 918
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "Get transaction success: "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string v0, "Api"

    invoke-static {v0, p1}, Lcom/stagnationlab/sk/f;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 920
    iget-object p1, p0, Lcom/stagnationlab/sk/a/a$15;->a:Lcom/stagnationlab/sk/a/a/t;

    new-instance v0, Lcom/stagnationlab/sk/models/Transaction;

    invoke-direct {v0, p2}, Lcom/stagnationlab/sk/models/Transaction;-><init>(Lee/cyber/smartid/dto/jsonrpc/resp/GetTransactionResp;)V

    invoke-interface {p1, v0}, Lcom/stagnationlab/sk/a/a/t;->a(Lcom/stagnationlab/sk/models/Transaction;)V

    return-void
.end method
