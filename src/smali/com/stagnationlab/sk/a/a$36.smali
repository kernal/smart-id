.class Lcom/stagnationlab/sk/a/a$36;
.super Ljava/lang/Object;
.source "Api.java"

# interfaces
.implements Lee/cyber/smartid/inter/CreateTransactionForRPRequestListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/stagnationlab/sk/a/a;->a(Lcom/stagnationlab/sk/models/RpRequest;Lcom/stagnationlab/sk/a/a/t;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/stagnationlab/sk/a/a/t;

.field final synthetic b:Lcom/stagnationlab/sk/a/a;


# direct methods
.method constructor <init>(Lcom/stagnationlab/sk/a/a;Lcom/stagnationlab/sk/a/a/t;)V
    .locals 0

    .line 397
    iput-object p1, p0, Lcom/stagnationlab/sk/a/a$36;->b:Lcom/stagnationlab/sk/a/a;

    iput-object p2, p0, Lcom/stagnationlab/sk/a/a$36;->a:Lcom/stagnationlab/sk/a/a/t;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCreateTransactionForRPRequestFailed(Ljava/lang/String;Lee/cyber/smartid/dto/SmartIdError;)V
    .locals 2

    .line 407
    new-instance p1, Lcom/stagnationlab/sk/d/b;

    invoke-direct {p1, p2}, Lcom/stagnationlab/sk/d/b;-><init>(Lee/cyber/smartid/dto/SmartIdError;)V

    const-string v0, "Api"

    const-string v1, "Create transaction for RP request failed"

    invoke-static {v0, v1, p1}, Lcom/stagnationlab/sk/f;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 408
    new-instance p1, Lcom/stagnationlab/sk/c/a;

    const-string v0, "createTransactionForRPRequest"

    invoke-direct {p1, p2, v0}, Lcom/stagnationlab/sk/c/a;-><init>(Lee/cyber/smartid/dto/SmartIdError;Ljava/lang/String;)V

    .line 409
    iget-object p2, p0, Lcom/stagnationlab/sk/a/a$36;->b:Lcom/stagnationlab/sk/a/a;

    invoke-static {p2, p1}, Lcom/stagnationlab/sk/a/a;->b(Lcom/stagnationlab/sk/a/a;Lcom/stagnationlab/sk/c/a;)V

    .line 410
    iget-object p2, p0, Lcom/stagnationlab/sk/a/a$36;->a:Lcom/stagnationlab/sk/a/a/t;

    invoke-interface {p2, p1}, Lcom/stagnationlab/sk/a/a/t;->a(Lcom/stagnationlab/sk/c/a;)V

    return-void
.end method

.method public onCreateTransactionForRPRequestSuccess(Ljava/lang/String;Lee/cyber/smartid/dto/jsonrpc/resp/CreateTransactionForRpRequestResp;)V
    .locals 1

    .line 402
    iget-object p1, p0, Lcom/stagnationlab/sk/a/a$36;->a:Lcom/stagnationlab/sk/a/a/t;

    new-instance v0, Lcom/stagnationlab/sk/models/Transaction;

    invoke-direct {v0, p2}, Lcom/stagnationlab/sk/models/Transaction;-><init>(Lee/cyber/smartid/dto/jsonrpc/resp/GetTransactionResp;)V

    invoke-interface {p1, v0}, Lcom/stagnationlab/sk/a/a/t;->a(Lcom/stagnationlab/sk/models/Transaction;)V

    return-void
.end method
