.class Lcom/stagnationlab/sk/a/b$1;
.super Ljava/lang/Object;
.source "HybridInitializer.java"

# interfaces
.implements Lcom/stagnationlab/sk/a/a/a;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/stagnationlab/sk/a/b;->b()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/stagnationlab/sk/a/b;


# direct methods
.method constructor <init>(Lcom/stagnationlab/sk/a/b;)V
    .locals 0

    .line 74
    iput-object p1, p0, Lcom/stagnationlab/sk/a/b$1;->a:Lcom/stagnationlab/sk/a/b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/stagnationlab/sk/a/a;Z)V
    .locals 1

    .line 77
    iget-object v0, p0, Lcom/stagnationlab/sk/a/b$1;->a:Lcom/stagnationlab/sk/a/b;

    invoke-static {v0}, Lcom/stagnationlab/sk/a/b;->a(Lcom/stagnationlab/sk/a/b;)Lcom/stagnationlab/sk/MainActivity;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/stagnationlab/sk/a/a;->a(Landroid/app/Activity;)V

    .line 79
    invoke-virtual {p1}, Lcom/stagnationlab/sk/a/a;->b()Lcom/stagnationlab/sk/models/Account;

    move-result-object p1

    if-nez p1, :cond_1

    .line 82
    invoke-static {}, Lcom/stagnationlab/sk/a/d;->c()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 83
    sget-object p1, Lcom/stagnationlab/sk/util/i$b;->a:Lcom/stagnationlab/sk/util/i$b;

    invoke-static {p1}, Lcom/stagnationlab/sk/util/i;->a(Lcom/stagnationlab/sk/util/i$b;)V

    .line 86
    :cond_0
    iget-object p1, p0, Lcom/stagnationlab/sk/a/b$1;->a:Lcom/stagnationlab/sk/a/b;

    const/4 p2, 0x0

    invoke-static {p1, p2}, Lcom/stagnationlab/sk/a/b;->a(Lcom/stagnationlab/sk/a/b;Z)V

    return-void

    .line 91
    :cond_1
    iget-object v0, p0, Lcom/stagnationlab/sk/a/b$1;->a:Lcom/stagnationlab/sk/a/b;

    invoke-static {v0, p1, p2}, Lcom/stagnationlab/sk/a/b;->a(Lcom/stagnationlab/sk/a/b;Lcom/stagnationlab/sk/models/Account;Z)V

    return-void
.end method

.method public a(Lcom/stagnationlab/sk/c/a;)V
    .locals 1

    .line 96
    iget-object v0, p0, Lcom/stagnationlab/sk/a/b$1;->a:Lcom/stagnationlab/sk/a/b;

    invoke-static {v0, p1}, Lcom/stagnationlab/sk/a/b;->a(Lcom/stagnationlab/sk/a/b;Lcom/stagnationlab/sk/c/a;)V

    return-void
.end method

.method public b(Lcom/stagnationlab/sk/c/a;)V
    .locals 3

    .line 101
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/stagnationlab/sk/a/b$1;->a:Lcom/stagnationlab/sk/a/b;

    invoke-static {v1}, Lcom/stagnationlab/sk/a/b;->a(Lcom/stagnationlab/sk/a/b;)Lcom/stagnationlab/sk/MainActivity;

    move-result-object v1

    const-class v2, Lcom/stagnationlab/sk/ErrorActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "error"

    .line 102
    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 103
    iget-object p1, p0, Lcom/stagnationlab/sk/a/b$1;->a:Lcom/stagnationlab/sk/a/b;

    invoke-static {p1}, Lcom/stagnationlab/sk/a/b;->a(Lcom/stagnationlab/sk/a/b;)Lcom/stagnationlab/sk/MainActivity;

    move-result-object p1

    invoke-virtual {p1, v0}, Lcom/stagnationlab/sk/MainActivity;->startActivity(Landroid/content/Intent;)V

    .line 104
    iget-object p1, p0, Lcom/stagnationlab/sk/a/b$1;->a:Lcom/stagnationlab/sk/a/b;

    invoke-static {p1}, Lcom/stagnationlab/sk/a/b;->a(Lcom/stagnationlab/sk/a/b;)Lcom/stagnationlab/sk/MainActivity;

    move-result-object p1

    invoke-virtual {p1}, Lcom/stagnationlab/sk/MainActivity;->finish()V

    return-void
.end method
