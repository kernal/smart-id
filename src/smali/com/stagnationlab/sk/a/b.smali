.class public Lcom/stagnationlab/sk/a/b;
.super Ljava/lang/Object;
.source "HybridInitializer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/stagnationlab/sk/a/b$a;
    }
.end annotation


# instance fields
.field private a:Lcom/stagnationlab/sk/h/d;

.field private b:Lcom/stagnationlab/sk/a/a;

.field private c:Lcom/stagnationlab/sk/MainActivity;

.field private d:Lcom/stagnationlab/sk/a/b$a;

.field private e:Lcom/stagnationlab/sk/a/a/u;

.field private f:Lcom/stagnationlab/sk/c/a;

.field private g:Lcom/stagnationlab/sk/models/HybridInitData;


# direct methods
.method public constructor <init>(Lcom/stagnationlab/sk/MainActivity;Lcom/stagnationlab/sk/h/d;)V
    .locals 2

    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 36
    iput-object v0, p0, Lcom/stagnationlab/sk/a/b;->f:Lcom/stagnationlab/sk/c/a;

    .line 40
    iput-object p1, p0, Lcom/stagnationlab/sk/a/b;->c:Lcom/stagnationlab/sk/MainActivity;

    .line 41
    iput-object p2, p0, Lcom/stagnationlab/sk/a/b;->a:Lcom/stagnationlab/sk/h/d;

    .line 42
    iput-object v0, p0, Lcom/stagnationlab/sk/a/b;->e:Lcom/stagnationlab/sk/a/a/u;

    .line 43
    new-instance v0, Lcom/stagnationlab/sk/models/HybridInitData;

    invoke-direct {v0}, Lcom/stagnationlab/sk/models/HybridInitData;-><init>()V

    iput-object v0, p0, Lcom/stagnationlab/sk/a/b;->g:Lcom/stagnationlab/sk/models/HybridInitData;

    .line 44
    sget-object v0, Lcom/stagnationlab/sk/a/b$a;->a:Lcom/stagnationlab/sk/a/b$a;

    iput-object v0, p0, Lcom/stagnationlab/sk/a/b;->d:Lcom/stagnationlab/sk/a/b$a;

    .line 46
    invoke-virtual {p2, p0}, Lcom/stagnationlab/sk/h/d;->a(Lcom/stagnationlab/sk/a/b;)V

    .line 48
    iget-object p2, p0, Lcom/stagnationlab/sk/a/b;->g:Lcom/stagnationlab/sk/models/HybridInitData;

    .line 49
    invoke-virtual {p1}, Lcom/stagnationlab/sk/MainActivity;->getIntent()Landroid/content/Intent;

    move-result-object p1

    const-string v0, "showNotificationInfo"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result p1

    .line 48
    invoke-virtual {p2, p1}, Lcom/stagnationlab/sk/models/HybridInitData;->a(Z)V

    return-void
.end method

.method static synthetic a(Lcom/stagnationlab/sk/a/b;)Lcom/stagnationlab/sk/MainActivity;
    .locals 0

    .line 22
    iget-object p0, p0, Lcom/stagnationlab/sk/a/b;->c:Lcom/stagnationlab/sk/MainActivity;

    return-object p0
.end method

.method static synthetic a(Lcom/stagnationlab/sk/a/b;Lcom/stagnationlab/sk/c/a;)V
    .locals 0

    .line 22
    invoke-direct {p0, p1}, Lcom/stagnationlab/sk/a/b;->a(Lcom/stagnationlab/sk/c/a;)V

    return-void
.end method

.method static synthetic a(Lcom/stagnationlab/sk/a/b;Lcom/stagnationlab/sk/models/Account;Z)V
    .locals 0

    .line 22
    invoke-direct {p0, p1, p2}, Lcom/stagnationlab/sk/a/b;->a(Lcom/stagnationlab/sk/models/Account;Z)V

    return-void
.end method

.method static synthetic a(Lcom/stagnationlab/sk/a/b;Z)V
    .locals 0

    .line 22
    invoke-direct {p0, p1}, Lcom/stagnationlab/sk/a/b;->a(Z)V

    return-void
.end method

.method static synthetic a(Lcom/stagnationlab/sk/a/b;ZZZ)V
    .locals 0

    .line 22
    invoke-direct {p0, p1, p2, p3}, Lcom/stagnationlab/sk/a/b;->a(ZZZ)V

    return-void
.end method

.method private a(Lcom/stagnationlab/sk/c/a;)V
    .locals 0

    .line 182
    iput-object p1, p0, Lcom/stagnationlab/sk/a/b;->f:Lcom/stagnationlab/sk/c/a;

    .line 184
    iget-object p1, p0, Lcom/stagnationlab/sk/a/b;->e:Lcom/stagnationlab/sk/a/a/u;

    if-nez p1, :cond_0

    .line 185
    sget-object p1, Lcom/stagnationlab/sk/a/b$a;->c:Lcom/stagnationlab/sk/a/b$a;

    iput-object p1, p0, Lcom/stagnationlab/sk/a/b;->d:Lcom/stagnationlab/sk/a/b$a;

    goto :goto_0

    .line 187
    :cond_0
    invoke-direct {p0}, Lcom/stagnationlab/sk/a/b;->c()V

    :goto_0
    return-void
.end method

.method private a(Lcom/stagnationlab/sk/models/Account;Z)V
    .locals 3

    .line 110
    iget-object v0, p0, Lcom/stagnationlab/sk/a/b;->b:Lcom/stagnationlab/sk/a/a;

    iget-object v1, p0, Lcom/stagnationlab/sk/a/b;->c:Lcom/stagnationlab/sk/MainActivity;

    new-instance v2, Lcom/stagnationlab/sk/a/b$2;

    invoke-direct {v2, p0, p1}, Lcom/stagnationlab/sk/a/b$2;-><init>(Lcom/stagnationlab/sk/a/b;Lcom/stagnationlab/sk/models/Account;)V

    invoke-virtual {v0, v1, p2, v2}, Lcom/stagnationlab/sk/a/a;->a(Landroid/app/Activity;ZLcom/stagnationlab/sk/a/a/y;)V

    return-void
.end method

.method private a(Z)V
    .locals 2

    .line 147
    iget-object v0, p0, Lcom/stagnationlab/sk/a/b;->b:Lcom/stagnationlab/sk/a/a;

    new-instance v1, Lcom/stagnationlab/sk/a/b$3;

    invoke-direct {v1, p0}, Lcom/stagnationlab/sk/a/b$3;-><init>(Lcom/stagnationlab/sk/a/b;)V

    invoke-virtual {v0, v1}, Lcom/stagnationlab/sk/a/a;->a(Lcom/stagnationlab/sk/a/a/q;)V

    if-nez p1, :cond_0

    .line 157
    iget-object p1, p0, Lcom/stagnationlab/sk/a/b;->b:Lcom/stagnationlab/sk/a/a;

    const/4 v0, 0x0

    new-instance v1, Lcom/stagnationlab/sk/a/b$4;

    invoke-direct {v1, p0}, Lcom/stagnationlab/sk/a/b$4;-><init>(Lcom/stagnationlab/sk/a/b;)V

    invoke-virtual {p1, v0, v1}, Lcom/stagnationlab/sk/a/a;->a(ZLcom/stagnationlab/sk/a/a/f;)V

    :cond_0
    return-void
.end method

.method private a(ZZZ)V
    .locals 0

    .line 172
    invoke-static {p1, p2, p3}, Lcom/stagnationlab/sk/util/j;->a(ZZZ)Ljava/util/Map;

    move-result-object p1

    .line 174
    iget-object p2, p0, Lcom/stagnationlab/sk/a/b;->g:Lcom/stagnationlab/sk/models/HybridInitData;

    invoke-virtual {p2, p1}, Lcom/stagnationlab/sk/models/HybridInitData;->a(Ljava/util/Map;)V

    .line 176
    invoke-virtual {p0}, Lcom/stagnationlab/sk/a/b;->a()Z

    move-result p2

    if-eqz p2, :cond_0

    .line 177
    iget-object p2, p0, Lcom/stagnationlab/sk/a/b;->a:Lcom/stagnationlab/sk/h/d;

    invoke-virtual {p2, p1}, Lcom/stagnationlab/sk/h/d;->a(Ljava/util/Map;)V

    :cond_0
    return-void
.end method

.method static synthetic b(Lcom/stagnationlab/sk/a/b;)Lcom/stagnationlab/sk/models/HybridInitData;
    .locals 0

    .line 22
    iget-object p0, p0, Lcom/stagnationlab/sk/a/b;->g:Lcom/stagnationlab/sk/models/HybridInitData;

    return-object p0
.end method

.method private b()V
    .locals 2

    .line 72
    sget-object v0, Lcom/stagnationlab/sk/a/b$a;->b:Lcom/stagnationlab/sk/a/b$a;

    iput-object v0, p0, Lcom/stagnationlab/sk/a/b;->d:Lcom/stagnationlab/sk/a/b$a;

    .line 74
    iget-object v0, p0, Lcom/stagnationlab/sk/a/b;->b:Lcom/stagnationlab/sk/a/a;

    new-instance v1, Lcom/stagnationlab/sk/a/b$1;

    invoke-direct {v1, p0}, Lcom/stagnationlab/sk/a/b$1;-><init>(Lcom/stagnationlab/sk/a/b;)V

    invoke-virtual {v0, v1}, Lcom/stagnationlab/sk/a/a;->a(Lcom/stagnationlab/sk/a/a/a;)V

    return-void
.end method

.method static synthetic c(Lcom/stagnationlab/sk/a/b;)Lcom/stagnationlab/sk/h/d;
    .locals 0

    .line 22
    iget-object p0, p0, Lcom/stagnationlab/sk/a/b;->a:Lcom/stagnationlab/sk/h/d;

    return-object p0
.end method

.method private c()V
    .locals 3

    .line 192
    iget-object v0, p0, Lcom/stagnationlab/sk/a/b;->f:Lcom/stagnationlab/sk/c/a;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    .line 193
    iget-object v0, p0, Lcom/stagnationlab/sk/a/b;->e:Lcom/stagnationlab/sk/a/a/u;

    iget-object v2, p0, Lcom/stagnationlab/sk/a/b;->g:Lcom/stagnationlab/sk/models/HybridInitData;

    invoke-interface {v0, v2}, Lcom/stagnationlab/sk/a/a/u;->a(Lcom/stagnationlab/sk/models/HybridInitData;)V

    .line 194
    sget-object v0, Lcom/stagnationlab/sk/a/b$a;->d:Lcom/stagnationlab/sk/a/b$a;

    iput-object v0, p0, Lcom/stagnationlab/sk/a/b;->d:Lcom/stagnationlab/sk/a/b$a;

    goto :goto_0

    .line 196
    :cond_0
    iget-object v2, p0, Lcom/stagnationlab/sk/a/b;->e:Lcom/stagnationlab/sk/a/a/u;

    invoke-interface {v2, v0}, Lcom/stagnationlab/sk/a/a/u;->a(Lcom/stagnationlab/sk/c/a;)V

    .line 197
    iput-object v1, p0, Lcom/stagnationlab/sk/a/b;->f:Lcom/stagnationlab/sk/c/a;

    .line 198
    sget-object v0, Lcom/stagnationlab/sk/a/b$a;->a:Lcom/stagnationlab/sk/a/b$a;

    iput-object v0, p0, Lcom/stagnationlab/sk/a/b;->d:Lcom/stagnationlab/sk/a/b$a;

    .line 200
    :goto_0
    iput-object v1, p0, Lcom/stagnationlab/sk/a/b;->e:Lcom/stagnationlab/sk/a/a/u;

    return-void
.end method


# virtual methods
.method public a(Lcom/stagnationlab/sk/a/a/u;)V
    .locals 1

    .line 62
    iput-object p1, p0, Lcom/stagnationlab/sk/a/b;->e:Lcom/stagnationlab/sk/a/a/u;

    .line 64
    iget-object p1, p0, Lcom/stagnationlab/sk/a/b;->d:Lcom/stagnationlab/sk/a/b$a;

    sget-object v0, Lcom/stagnationlab/sk/a/b$a;->a:Lcom/stagnationlab/sk/a/b$a;

    if-ne p1, v0, :cond_0

    .line 65
    invoke-direct {p0}, Lcom/stagnationlab/sk/a/b;->b()V

    goto :goto_0

    .line 66
    :cond_0
    iget-object p1, p0, Lcom/stagnationlab/sk/a/b;->d:Lcom/stagnationlab/sk/a/b$a;

    sget-object v0, Lcom/stagnationlab/sk/a/b$a;->c:Lcom/stagnationlab/sk/a/b$a;

    if-eq p1, v0, :cond_1

    iget-object p1, p0, Lcom/stagnationlab/sk/a/b;->d:Lcom/stagnationlab/sk/a/b$a;

    sget-object v0, Lcom/stagnationlab/sk/a/b$a;->d:Lcom/stagnationlab/sk/a/b$a;

    if-ne p1, v0, :cond_2

    .line 67
    :cond_1
    invoke-direct {p0}, Lcom/stagnationlab/sk/a/b;->c()V

    :cond_2
    :goto_0
    return-void
.end method

.method public a(Lcom/stagnationlab/sk/a/a;)V
    .locals 1

    .line 54
    iput-object p1, p0, Lcom/stagnationlab/sk/a/b;->b:Lcom/stagnationlab/sk/a/a;

    .line 56
    iget-object p1, p0, Lcom/stagnationlab/sk/a/b;->d:Lcom/stagnationlab/sk/a/b$a;

    sget-object v0, Lcom/stagnationlab/sk/a/b$a;->a:Lcom/stagnationlab/sk/a/b$a;

    if-ne p1, v0, :cond_0

    .line 57
    invoke-direct {p0}, Lcom/stagnationlab/sk/a/b;->b()V

    :cond_0
    return-void
.end method

.method public a()Z
    .locals 2

    .line 204
    iget-object v0, p0, Lcom/stagnationlab/sk/a/b;->d:Lcom/stagnationlab/sk/a/b$a;

    sget-object v1, Lcom/stagnationlab/sk/a/b$a;->d:Lcom/stagnationlab/sk/a/b$a;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method
