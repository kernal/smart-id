.class Lcom/stagnationlab/sk/a/a$26;
.super Ljava/lang/Object;
.source "Api.java"

# interfaces
.implements Lee/cyber/smartid/inter/GetPushNotificationDataListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/stagnationlab/sk/a/a;->a(Lcom/stagnationlab/sk/a/a/q;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/stagnationlab/sk/a/a/q;

.field final synthetic b:Lcom/stagnationlab/sk/a/a;


# direct methods
.method constructor <init>(Lcom/stagnationlab/sk/a/a;Lcom/stagnationlab/sk/a/a/q;)V
    .locals 0

    .line 1284
    iput-object p1, p0, Lcom/stagnationlab/sk/a/a$26;->b:Lcom/stagnationlab/sk/a/a;

    iput-object p2, p0, Lcom/stagnationlab/sk/a/a$26;->a:Lcom/stagnationlab/sk/a/a/q;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onGetPushNotificationDataFailed(Ljava/lang/String;Lee/cyber/smartid/dto/SmartIdError;)V
    .locals 2

    .line 1294
    new-instance p1, Lcom/stagnationlab/sk/d/b;

    invoke-direct {p1, p2}, Lcom/stagnationlab/sk/d/b;-><init>(Lee/cyber/smartid/dto/SmartIdError;)V

    const-string v0, "Api"

    const-string v1, "Get push notification data failed"

    invoke-static {v0, v1, p1}, Lcom/stagnationlab/sk/f;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    const-string p1, "getPushNotificationData"

    .line 1295
    invoke-static {p2, p1}, Lcom/stagnationlab/sk/c/a;->a(Lee/cyber/smartid/dto/SmartIdError;Ljava/lang/String;)V

    .line 1297
    iget-object p1, p0, Lcom/stagnationlab/sk/a/a$26;->a:Lcom/stagnationlab/sk/a/a/q;

    const/4 p2, 0x0

    invoke-interface {p1, p2}, Lcom/stagnationlab/sk/a/a/q;->a(Ljava/lang/String;)V

    return-void
.end method

.method public onGetPushNotificationDataSuccess(Ljava/lang/String;Lee/cyber/smartid/dto/jsonrpc/resp/GetPushNotificationResp;)V
    .locals 0

    .line 1289
    iget-object p1, p0, Lcom/stagnationlab/sk/a/a$26;->a:Lcom/stagnationlab/sk/a/a/q;

    invoke-virtual {p2}, Lee/cyber/smartid/dto/jsonrpc/resp/GetPushNotificationResp;->getToken()Ljava/lang/String;

    move-result-object p2

    invoke-interface {p1, p2}, Lcom/stagnationlab/sk/a/a/q;->a(Ljava/lang/String;)V

    return-void
.end method
