.class Lcom/stagnationlab/sk/a/a$10;
.super Ljava/lang/Object;
.source "Api.java"

# interfaces
.implements Lee/cyber/smartid/inter/CheckLocalPendingStateListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/stagnationlab/sk/a/a;->a(Ljava/lang/String;Lcom/stagnationlab/sk/a/a/e;Ljava/lang/String;)Lee/cyber/smartid/inter/CheckLocalPendingStateListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:Lcom/stagnationlab/sk/a/a/e;

.field final synthetic c:Ljava/lang/String;

.field final synthetic d:Lcom/stagnationlab/sk/a/a;


# direct methods
.method constructor <init>(Lcom/stagnationlab/sk/a/a;Ljava/lang/String;Lcom/stagnationlab/sk/a/a/e;Ljava/lang/String;)V
    .locals 0

    .line 806
    iput-object p1, p0, Lcom/stagnationlab/sk/a/a$10;->d:Lcom/stagnationlab/sk/a/a;

    iput-object p2, p0, Lcom/stagnationlab/sk/a/a$10;->a:Ljava/lang/String;

    iput-object p3, p0, Lcom/stagnationlab/sk/a/a$10;->b:Lcom/stagnationlab/sk/a/a/e;

    iput-object p4, p0, Lcom/stagnationlab/sk/a/a$10;->c:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCheckLocalPendingStateFailed(Ljava/lang/String;Lee/cyber/smartid/dto/SmartIdError;)V
    .locals 2

    .line 822
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "checkLocalPendingState("

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/stagnationlab/sk/a/a$10;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, ") failed"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    new-instance v0, Lcom/stagnationlab/sk/d/b;

    invoke-direct {v0, p2}, Lcom/stagnationlab/sk/d/b;-><init>(Lee/cyber/smartid/dto/SmartIdError;)V

    const-string v1, "Api"

    invoke-static {v1, p1, v0}, Lcom/stagnationlab/sk/f;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 828
    iget-object p1, p0, Lcom/stagnationlab/sk/a/a$10;->b:Lcom/stagnationlab/sk/a/a/e;

    new-instance v0, Lcom/stagnationlab/sk/c/a;

    iget-object v1, p0, Lcom/stagnationlab/sk/a/a$10;->c:Ljava/lang/String;

    invoke-direct {v0, p2, v1}, Lcom/stagnationlab/sk/c/a;-><init>(Lee/cyber/smartid/dto/SmartIdError;Ljava/lang/String;)V

    invoke-interface {p1, v0}, Lcom/stagnationlab/sk/a/a/e;->a(Lcom/stagnationlab/sk/c/a;)V

    return-void
.end method

.method public onCheckLocalPendingStateSuccess(Ljava/lang/String;Lee/cyber/smartid/dto/jsonrpc/resp/CheckLocalPendingStateResp;)V
    .locals 1

    .line 809
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "checkLocalPendingState("

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/stagnationlab/sk/a/a$10;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, ") success: "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string v0, "Api"

    invoke-static {v0, p1}, Lcom/stagnationlab/sk/f;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 811
    iget-object p1, p0, Lcom/stagnationlab/sk/a/a$10;->a:Ljava/lang/String;

    invoke-virtual {p2}, Lee/cyber/smartid/dto/jsonrpc/resp/CheckLocalPendingStateResp;->getLocalPendingStateType()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 812
    iget-object p1, p0, Lcom/stagnationlab/sk/a/a$10;->b:Lcom/stagnationlab/sk/a/a/e;

    invoke-interface {p1, p2}, Lcom/stagnationlab/sk/a/a/e;->a(Lee/cyber/smartid/dto/jsonrpc/resp/CheckLocalPendingStateResp;)V

    return-void

    .line 817
    :cond_0
    iget-object p1, p0, Lcom/stagnationlab/sk/a/a$10;->b:Lcom/stagnationlab/sk/a/a/e;

    invoke-interface {p1}, Lcom/stagnationlab/sk/a/a/e;->a()V

    return-void
.end method
