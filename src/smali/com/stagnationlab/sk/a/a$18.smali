.class Lcom/stagnationlab/sk/a/a$18;
.super Ljava/lang/Object;
.source "Api.java"

# interfaces
.implements Lee/cyber/smartid/inter/GetSecureRandomListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/stagnationlab/sk/a/a;->a(Lcom/stagnationlab/sk/a/a/s;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/stagnationlab/sk/a/a/s;

.field final synthetic b:Lcom/stagnationlab/sk/a/a;


# direct methods
.method constructor <init>(Lcom/stagnationlab/sk/a/a;Lcom/stagnationlab/sk/a/a/s;)V
    .locals 0

    .line 1003
    iput-object p1, p0, Lcom/stagnationlab/sk/a/a$18;->b:Lcom/stagnationlab/sk/a/a;

    iput-object p2, p0, Lcom/stagnationlab/sk/a/a$18;->a:Lcom/stagnationlab/sk/a/a/s;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onGetSecureRandomFailed(Ljava/lang/String;Lee/cyber/smartid/dto/SmartIdError;)V
    .locals 2

    .line 1011
    new-instance p1, Lcom/stagnationlab/sk/d/b;

    invoke-direct {p1, p2}, Lcom/stagnationlab/sk/d/b;-><init>(Lee/cyber/smartid/dto/SmartIdError;)V

    const-string v0, "Api"

    const-string v1, "get secure random failed"

    invoke-static {v0, v1, p1}, Lcom/stagnationlab/sk/f;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    const-string p1, "getSecureRandom"

    .line 1012
    invoke-static {p2, p1}, Lcom/stagnationlab/sk/c/a;->a(Lee/cyber/smartid/dto/SmartIdError;Ljava/lang/String;)V

    .line 1013
    iget-object p1, p0, Lcom/stagnationlab/sk/a/a$18;->a:Lcom/stagnationlab/sk/a/a/s;

    new-instance p2, Ljava/security/SecureRandom;

    invoke-direct {p2}, Ljava/security/SecureRandom;-><init>()V

    invoke-interface {p1, p2}, Lcom/stagnationlab/sk/a/a/s;->a(Ljava/security/SecureRandom;)V

    return-void
.end method

.method public onGetSecureRandomSuccess(Ljava/lang/String;Lee/cyber/smartid/dto/jsonrpc/resp/GetSecureRandomResp;)V
    .locals 0

    .line 1006
    iget-object p1, p0, Lcom/stagnationlab/sk/a/a$18;->a:Lcom/stagnationlab/sk/a/a/s;

    invoke-virtual {p2}, Lee/cyber/smartid/dto/jsonrpc/resp/GetSecureRandomResp;->getSecureRandom()Ljava/security/SecureRandom;

    move-result-object p2

    invoke-interface {p1, p2}, Lcom/stagnationlab/sk/a/a/s;->a(Ljava/security/SecureRandom;)V

    return-void
.end method
