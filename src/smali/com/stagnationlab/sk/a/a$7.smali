.class Lcom/stagnationlab/sk/a/a$7;
.super Ljava/lang/Object;
.source "Api.java"

# interfaces
.implements Lee/cyber/smartid/inter/GetRegistrationTokenListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/stagnationlab/sk/a/a;->a(Ljava/lang/String;Ljava/lang/String;Lcom/stagnationlab/sk/a/a/r;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/stagnationlab/sk/a/a/r;

.field final synthetic b:Lcom/stagnationlab/sk/a/a;


# direct methods
.method constructor <init>(Lcom/stagnationlab/sk/a/a;Lcom/stagnationlab/sk/a/a/r;)V
    .locals 0

    .line 610
    iput-object p1, p0, Lcom/stagnationlab/sk/a/a$7;->b:Lcom/stagnationlab/sk/a/a;

    iput-object p2, p0, Lcom/stagnationlab/sk/a/a$7;->a:Lcom/stagnationlab/sk/a/a/r;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onGetRegistrationTokenFailed(Ljava/lang/String;Lee/cyber/smartid/dto/SmartIdError;)V
    .locals 2

    .line 624
    new-instance p1, Lcom/stagnationlab/sk/d/b;

    invoke-direct {p1, p2}, Lcom/stagnationlab/sk/d/b;-><init>(Lee/cyber/smartid/dto/SmartIdError;)V

    const-string v0, "Api"

    const-string v1, "getRegistrationToken failed"

    invoke-static {v0, v1, p1}, Lcom/stagnationlab/sk/f;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 629
    iget-object p1, p0, Lcom/stagnationlab/sk/a/a$7;->a:Lcom/stagnationlab/sk/a/a/r;

    new-instance v0, Lcom/stagnationlab/sk/c/a;

    const-string v1, "getRegistrationToken"

    invoke-direct {v0, p2, v1}, Lcom/stagnationlab/sk/c/a;-><init>(Lee/cyber/smartid/dto/SmartIdError;Ljava/lang/String;)V

    invoke-interface {p1, v0}, Lcom/stagnationlab/sk/a/a/r;->a(Lcom/stagnationlab/sk/c/a;)V

    return-void
.end method

.method public onGetRegistrationTokenSuccess(Ljava/lang/String;Lee/cyber/smartid/dto/jsonrpc/resp/GetRegistrationTokenResp;)V
    .locals 5

    const-string p1, "Api"

    const-string v0, "getRegistrationToken success"

    .line 613
    invoke-static {p1, v0}, Lcom/stagnationlab/sk/f;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 615
    iget-object p1, p0, Lcom/stagnationlab/sk/a/a$7;->a:Lcom/stagnationlab/sk/a/a/r;

    .line 616
    invoke-virtual {p2}, Lee/cyber/smartid/dto/jsonrpc/resp/GetRegistrationTokenResp;->getToken()Ljava/lang/String;

    move-result-object v0

    .line 617
    invoke-virtual {p2}, Lee/cyber/smartid/dto/jsonrpc/resp/GetRegistrationTokenResp;->getNonce()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lorg/b/a/b;

    invoke-direct {v2}, Lorg/b/a/b;-><init>()V

    .line 618
    invoke-virtual {p2}, Lee/cyber/smartid/dto/jsonrpc/resp/GetRegistrationTokenResp;->getTimeToLiveSec()J

    move-result-wide v3

    long-to-int p2, v3

    invoke-virtual {v2, p2}, Lorg/b/a/b;->a(I)Lorg/b/a/b;

    move-result-object p2

    .line 615
    invoke-interface {p1, v0, v1, p2}, Lcom/stagnationlab/sk/a/a/r;->a(Ljava/lang/String;Ljava/lang/String;Lorg/b/a/b;)V

    return-void
.end method
