.class Lcom/stagnationlab/sk/a/a$4;
.super Ljava/lang/Object;
.source "Api.java"

# interfaces
.implements Lee/cyber/smartid/inter/ConfirmTransactionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/stagnationlab/sk/a/a;->a(Ljava/lang/String;Lcom/stagnationlab/sk/a/a/v;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/stagnationlab/sk/a/a/v;

.field final synthetic b:Lcom/stagnationlab/sk/a/a;


# direct methods
.method constructor <init>(Lcom/stagnationlab/sk/a/a;Lcom/stagnationlab/sk/a/a/v;)V
    .locals 0

    .line 495
    iput-object p1, p0, Lcom/stagnationlab/sk/a/a$4;->b:Lcom/stagnationlab/sk/a/a;

    iput-object p2, p0, Lcom/stagnationlab/sk/a/a$4;->a:Lcom/stagnationlab/sk/a/a/v;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onConfirmTransactionFailed(Ljava/lang/String;Lee/cyber/smartid/dto/SmartIdError;)V
    .locals 4

    .line 507
    new-instance p1, Lcom/stagnationlab/sk/d/b;

    invoke-direct {p1, p2}, Lcom/stagnationlab/sk/d/b;-><init>(Lee/cyber/smartid/dto/SmartIdError;)V

    const-string v0, "Api"

    const-string v1, "retryLocalPendingState failed"

    invoke-static {v0, v1, p1}, Lcom/stagnationlab/sk/f;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 513
    invoke-virtual {p2}, Lee/cyber/smartid/dto/SmartIdError;->getCode()J

    move-result-wide v0

    const-wide/16 v2, 0x406

    cmp-long p1, v0, v2

    if-nez p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    if-eqz p1, :cond_1

    .line 517
    iget-object p1, p0, Lcom/stagnationlab/sk/a/a$4;->a:Lcom/stagnationlab/sk/a/a/v;

    invoke-interface {p1}, Lcom/stagnationlab/sk/a/a/v;->b()V

    goto :goto_1

    .line 519
    :cond_1
    iget-object p1, p0, Lcom/stagnationlab/sk/a/a$4;->a:Lcom/stagnationlab/sk/a/a/v;

    new-instance v0, Lcom/stagnationlab/sk/c/a;

    const-string v1, "retryLocalPendingState"

    invoke-direct {v0, p2, v1}, Lcom/stagnationlab/sk/c/a;-><init>(Lee/cyber/smartid/dto/SmartIdError;Ljava/lang/String;)V

    invoke-interface {p1, v0}, Lcom/stagnationlab/sk/a/a/v;->a(Lcom/stagnationlab/sk/c/a;)V

    :goto_1
    return-void
.end method

.method public onConfirmTransactionSuccess(Ljava/lang/String;Lee/cyber/smartid/tse/dto/jsonrpc/resp/ConfirmTransactionResp;)V
    .locals 0

    const-string p1, "Api"

    const-string p2, "retryLocalPendingState success"

    .line 500
    invoke-static {p1, p2}, Lcom/stagnationlab/sk/f;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 502
    iget-object p1, p0, Lcom/stagnationlab/sk/a/a$4;->a:Lcom/stagnationlab/sk/a/a/v;

    invoke-interface {p1}, Lcom/stagnationlab/sk/a/a/v;->a()V

    return-void
.end method
