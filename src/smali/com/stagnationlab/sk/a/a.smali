.class public Lcom/stagnationlab/sk/a/a;
.super Ljava/lang/Object;
.source "Api.java"


# static fields
.field private static i:Lcom/stagnationlab/sk/a/a;


# instance fields
.field private a:I

.field private b:Lee/cyber/smartid/SmartIdService;

.field private c:Ljava/lang/String;

.field private d:Z

.field private e:Z

.field private f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/stagnationlab/sk/a/a/a;",
            ">;"
        }
    .end annotation
.end field

.field private g:Lcom/stagnationlab/sk/models/Account;

.field private h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/stagnationlab/sk/models/Account;",
            ">;"
        }
    .end annotation
.end field

.field private j:Lcom/stagnationlab/sk/h/d;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 135
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    .line 98
    iput v0, p0, Lcom/stagnationlab/sk/a/a;->a:I

    .line 109
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/stagnationlab/sk/a/a;->f:Ljava/util/List;

    const/4 v0, 0x0

    .line 111
    iput-object v0, p0, Lcom/stagnationlab/sk/a/a;->g:Lcom/stagnationlab/sk/models/Account;

    .line 112
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/stagnationlab/sk/a/a;->h:Ljava/util/List;

    return-void
.end method

.method public static a()Lcom/stagnationlab/sk/a/a;
    .locals 1

    .line 118
    sget-object v0, Lcom/stagnationlab/sk/a/a;->i:Lcom/stagnationlab/sk/a/a;

    if-nez v0, :cond_0

    .line 119
    new-instance v0, Lcom/stagnationlab/sk/a/a;

    invoke-direct {v0}, Lcom/stagnationlab/sk/a/a;-><init>()V

    sput-object v0, Lcom/stagnationlab/sk/a/a;->i:Lcom/stagnationlab/sk/a/a;

    .line 122
    :cond_0
    sget-object v0, Lcom/stagnationlab/sk/a/a;->i:Lcom/stagnationlab/sk/a/a;

    return-object v0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;)Lcom/stagnationlab/sk/a/a;
    .locals 1

    .line 126
    sget-object v0, Lcom/stagnationlab/sk/a/a;->i:Lcom/stagnationlab/sk/a/a;

    if-nez v0, :cond_0

    .line 127
    new-instance v0, Lcom/stagnationlab/sk/a/a;

    invoke-direct {v0}, Lcom/stagnationlab/sk/a/a;-><init>()V

    sput-object v0, Lcom/stagnationlab/sk/a/a;->i:Lcom/stagnationlab/sk/a/a;

    .line 130
    :cond_0
    sget-object v0, Lcom/stagnationlab/sk/a/a;->i:Lcom/stagnationlab/sk/a/a;

    invoke-direct {v0, p0, p1}, Lcom/stagnationlab/sk/a/a;->b(Landroid/content/Context;Ljava/lang/String;)V

    .line 132
    sget-object p0, Lcom/stagnationlab/sk/a/a;->i:Lcom/stagnationlab/sk/a/a;

    return-object p0
.end method

.method static synthetic a(Lcom/stagnationlab/sk/a/a;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/stagnationlab/sk/a/a/i;Ljava/lang/String;)Lee/cyber/smartid/inter/AddAccountListener;
    .locals 0

    .line 95
    invoke-direct/range {p0 .. p5}, Lcom/stagnationlab/sk/a/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/stagnationlab/sk/a/a/i;Ljava/lang/String;)Lee/cyber/smartid/inter/AddAccountListener;

    move-result-object p0

    return-object p0
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/stagnationlab/sk/a/a/i;Ljava/lang/String;)Lee/cyber/smartid/inter/AddAccountListener;
    .locals 8

    .line 713
    new-instance v7, Lcom/stagnationlab/sk/a/a$9;

    move-object v0, v7

    move-object v1, p0

    move-object v2, p4

    move-object v3, p5

    move-object v4, p1

    move-object v5, p2

    move-object v6, p3

    invoke-direct/range {v0 .. v6}, Lcom/stagnationlab/sk/a/a$9;-><init>(Lcom/stagnationlab/sk/a/a;Lcom/stagnationlab/sk/a/a/i;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v7
.end method

.method private a(Ljava/lang/String;Lcom/stagnationlab/sk/a/a/e;Ljava/lang/String;)Lee/cyber/smartid/inter/CheckLocalPendingStateListener;
    .locals 1

    .line 806
    new-instance v0, Lcom/stagnationlab/sk/a/a$10;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/stagnationlab/sk/a/a$10;-><init>(Lcom/stagnationlab/sk/a/a;Ljava/lang/String;Lcom/stagnationlab/sk/a/a/e;Ljava/lang/String;)V

    return-object v0
.end method

.method private a(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .line 1131
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/stagnationlab/sk/a/a;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private a(Lcom/stagnationlab/sk/a/a/k;)V
    .locals 6

    .line 250
    iget-object v0, p0, Lcom/stagnationlab/sk/a/a;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_0

    .line 251
    invoke-static {}, Lcom/stagnationlab/sk/a/d;->h()V

    .line 252
    invoke-interface {p1}, Lcom/stagnationlab/sk/a/a/k;->a()V

    return-void

    .line 257
    :cond_0
    iget-object v0, p0, Lcom/stagnationlab/sk/a/a;->h:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/stagnationlab/sk/models/Account;

    .line 259
    iget-object v1, p0, Lcom/stagnationlab/sk/a/a;->b:Lee/cyber/smartid/SmartIdService;

    .line 260
    invoke-direct {p0}, Lcom/stagnationlab/sk/a/a;->m()Ljava/lang/String;

    move-result-object v2

    .line 261
    invoke-virtual {v0}, Lcom/stagnationlab/sk/models/Account;->a()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    new-instance v5, Lcom/stagnationlab/sk/a/a$31;

    invoke-direct {v5, p0, v0, p1}, Lcom/stagnationlab/sk/a/a$31;-><init>(Lcom/stagnationlab/sk/a/a;Lcom/stagnationlab/sk/models/Account;Lcom/stagnationlab/sk/a/a/k;)V

    .line 259
    invoke-virtual {v1, v2, v3, v4, v5}, Lee/cyber/smartid/SmartIdService;->deleteAccount(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/inter/DeleteAccountListener;)V

    return-void
.end method

.method static synthetic a(Lcom/stagnationlab/sk/a/a;)V
    .locals 0

    .line 95
    invoke-direct {p0}, Lcom/stagnationlab/sk/a/a;->k()V

    return-void
.end method

.method static synthetic a(Lcom/stagnationlab/sk/a/a;Lcom/stagnationlab/sk/a/a/k;)V
    .locals 0

    .line 95
    invoke-direct {p0, p1}, Lcom/stagnationlab/sk/a/a;->a(Lcom/stagnationlab/sk/a/a/k;)V

    return-void
.end method

.method static synthetic a(Lcom/stagnationlab/sk/a/a;Lcom/stagnationlab/sk/c/a;)V
    .locals 0

    .line 95
    invoke-direct {p0, p1}, Lcom/stagnationlab/sk/a/a;->a(Lcom/stagnationlab/sk/c/a;)V

    return-void
.end method

.method static synthetic a(Lcom/stagnationlab/sk/a/a;Lcom/stagnationlab/sk/models/Transaction;Lcom/stagnationlab/sk/a/a/x;Lcom/stagnationlab/sk/c/a;Ljava/lang/String;)V
    .locals 0

    .line 95
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/stagnationlab/sk/a/a;->a(Lcom/stagnationlab/sk/models/Transaction;Lcom/stagnationlab/sk/a/a/x;Lcom/stagnationlab/sk/c/a;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic a(Lcom/stagnationlab/sk/a/a;Ljava/lang/String;Lcom/stagnationlab/sk/a/a/v;)V
    .locals 0

    .line 95
    invoke-direct {p0, p1, p2}, Lcom/stagnationlab/sk/a/a;->a(Ljava/lang/String;Lcom/stagnationlab/sk/a/a/v;)V

    return-void
.end method

.method static synthetic a(Lcom/stagnationlab/sk/a/a;Ljava/lang/String;Lee/cyber/smartid/dto/SmartIdError;)V
    .locals 0

    .line 95
    invoke-direct {p0, p1, p2}, Lcom/stagnationlab/sk/a/a;->a(Ljava/lang/String;Lee/cyber/smartid/dto/SmartIdError;)V

    return-void
.end method

.method static synthetic a(Lcom/stagnationlab/sk/a/a;Ljava/lang/String;Ljava/lang/String;Lcom/stagnationlab/sk/a/a/e;)V
    .locals 0

    .line 95
    invoke-direct {p0, p1, p2, p3}, Lcom/stagnationlab/sk/a/a;->a(Ljava/lang/String;Ljava/lang/String;Lcom/stagnationlab/sk/a/a/e;)V

    return-void
.end method

.method static synthetic a(Lcom/stagnationlab/sk/a/a;Ljava/util/List;)V
    .locals 0

    .line 95
    invoke-direct {p0, p1}, Lcom/stagnationlab/sk/a/a;->a(Ljava/util/List;)V

    return-void
.end method

.method static synthetic a(Lcom/stagnationlab/sk/a/a;Z)V
    .locals 0

    .line 95
    invoke-direct {p0, p1}, Lcom/stagnationlab/sk/a/a;->a(Z)V

    return-void
.end method

.method private a(Lcom/stagnationlab/sk/c/a;)V
    .locals 4

    const/4 v0, 0x0

    .line 1061
    iput-boolean v0, p0, Lcom/stagnationlab/sk/a/a;->d:Z

    const/4 v1, 0x1

    if-nez p1, :cond_0

    const/4 v0, 0x1

    .line 1062
    :cond_0
    iput-boolean v0, p0, Lcom/stagnationlab/sk/a/a;->e:Z

    .line 1064
    iget-object v0, p0, Lcom/stagnationlab/sk/a/a;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/stagnationlab/sk/a/a/a;

    if-nez p1, :cond_1

    .line 1066
    invoke-interface {v2, p0, v1}, Lcom/stagnationlab/sk/a/a/a;->a(Lcom/stagnationlab/sk/a/a;Z)V

    goto :goto_0

    .line 1067
    :cond_1
    invoke-virtual {p1}, Lcom/stagnationlab/sk/c/a;->n()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1068
    invoke-interface {v2, p1}, Lcom/stagnationlab/sk/a/a/a;->a(Lcom/stagnationlab/sk/c/a;)V

    goto :goto_0

    .line 1070
    :cond_2
    invoke-interface {v2, p1}, Lcom/stagnationlab/sk/a/a/a;->b(Lcom/stagnationlab/sk/c/a;)V

    goto :goto_0

    .line 1074
    :cond_3
    iget-object p1, p0, Lcom/stagnationlab/sk/a/a;->f:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->clear()V

    return-void
.end method

.method private a(Lcom/stagnationlab/sk/models/Transaction;Lcom/stagnationlab/sk/a/a/e;)V
    .locals 5

    const-string v0, "Api"

    const-string v1, "checkLocalPendingState start"

    .line 473
    invoke-static {v0, v1}, Lcom/stagnationlab/sk/f;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 475
    iget-object v0, p0, Lcom/stagnationlab/sk/a/a;->b:Lee/cyber/smartid/SmartIdService;

    .line 476
    invoke-direct {p0}, Lcom/stagnationlab/sk/a/a;->m()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/stagnationlab/sk/a/a;->g:Lcom/stagnationlab/sk/models/Account;

    .line 477
    invoke-virtual {v2}, Lcom/stagnationlab/sk/models/Account;->a()Ljava/lang/String;

    move-result-object v2

    .line 478
    invoke-virtual {p1}, Lcom/stagnationlab/sk/models/Transaction;->h()Lcom/stagnationlab/sk/models/Transaction$Type;

    move-result-object p1

    sget-object v3, Lcom/stagnationlab/sk/models/Transaction$Type;->AUTHENTICATION:Lcom/stagnationlab/sk/models/Transaction$Type;

    if-ne p1, v3, :cond_0

    const-string p1, "AUTHENTICATION"

    goto :goto_0

    :cond_0
    const-string p1, "SIGNATURE"

    :goto_0
    const-string v3, "confirmTransaction"

    const-string v4, "checkLocalPendingStateForKey"

    .line 481
    invoke-direct {p0, v3, p2, v4}, Lcom/stagnationlab/sk/a/a;->a(Ljava/lang/String;Lcom/stagnationlab/sk/a/a/e;Ljava/lang/String;)Lee/cyber/smartid/inter/CheckLocalPendingStateListener;

    move-result-object p2

    .line 475
    invoke-virtual {v0, v1, v2, p1, p2}, Lee/cyber/smartid/SmartIdService;->checkLocalPendingStateForKey(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/inter/CheckLocalPendingStateListener;)V

    return-void
.end method

.method private a(Lcom/stagnationlab/sk/models/Transaction;Lcom/stagnationlab/sk/a/a/x;Lcom/stagnationlab/sk/c/a;Ljava/lang/String;)V
    .locals 3

    if-eqz p4, :cond_0

    .line 887
    iget-object v0, p0, Lcom/stagnationlab/sk/a/a;->c:Ljava/lang/String;

    new-instance v1, Lcom/stagnationlab/sk/d/b;

    invoke-virtual {p3}, Lcom/stagnationlab/sk/c/a;->j()Lee/cyber/smartid/dto/SmartIdError;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/stagnationlab/sk/d/b;-><init>(Lee/cyber/smartid/dto/SmartIdError;)V

    invoke-static {v0, p4, v1}, Lcom/stagnationlab/sk/f;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 890
    :cond_0
    new-instance p4, Lcom/stagnationlab/sk/a/a$14;

    invoke-direct {p4, p0, p3, p2}, Lcom/stagnationlab/sk/a/a$14;-><init>(Lcom/stagnationlab/sk/a/a;Lcom/stagnationlab/sk/c/a;Lcom/stagnationlab/sk/a/a/x;)V

    invoke-direct {p0, p1, p4}, Lcom/stagnationlab/sk/a/a;->a(Lcom/stagnationlab/sk/models/Transaction;Lcom/stagnationlab/sk/a/a/e;)V

    return-void
.end method

.method private a(Ljava/lang/String;Lcom/stagnationlab/sk/a/a/v;)V
    .locals 3

    const-string v0, "Api"

    const-string v1, "retryLocalPendingState start"

    .line 490
    invoke-static {v0, v1}, Lcom/stagnationlab/sk/f;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 492
    iget-object v0, p0, Lcom/stagnationlab/sk/a/a;->b:Lee/cyber/smartid/SmartIdService;

    .line 493
    invoke-direct {p0}, Lcom/stagnationlab/sk/a/a;->m()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/stagnationlab/sk/a/a$4;

    invoke-direct {v2, p0, p2}, Lcom/stagnationlab/sk/a/a$4;-><init>(Lcom/stagnationlab/sk/a/a;Lcom/stagnationlab/sk/a/a/v;)V

    .line 492
    invoke-virtual {v0, v1, p1, v2}, Lee/cyber/smartid/SmartIdService;->retryLocalPendingState(Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/inter/ServiceListener;)V

    return-void
.end method

.method private a(Ljava/lang/String;Lee/cyber/smartid/dto/SmartIdError;)V
    .locals 1

    .line 1396
    new-instance v0, Lcom/stagnationlab/sk/d/b;

    invoke-direct {v0, p2}, Lcom/stagnationlab/sk/d/b;-><init>(Lee/cyber/smartid/dto/SmartIdError;)V

    const-string p2, "Api"

    invoke-static {p2, p1, v0}, Lcom/stagnationlab/sk/f;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Lcom/stagnationlab/sk/a/a/e;)V
    .locals 4

    const-string v0, "Api"

    const-string v1, "checkLocalPendingStateForAccountRegistration start"

    .line 791
    invoke-static {v0, v1}, Lcom/stagnationlab/sk/f;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 793
    iget-object v0, p0, Lcom/stagnationlab/sk/a/a;->b:Lee/cyber/smartid/SmartIdService;

    .line 794
    invoke-direct {p0}, Lcom/stagnationlab/sk/a/a;->m()Ljava/lang/String;

    move-result-object v1

    const-string v2, "addAccount"

    const-string v3, "checkLocalPendingStateForAccountRegistration"

    .line 797
    invoke-direct {p0, v2, p3, v3}, Lcom/stagnationlab/sk/a/a;->a(Ljava/lang/String;Lcom/stagnationlab/sk/a/a/e;Ljava/lang/String;)Lee/cyber/smartid/inter/CheckLocalPendingStateListener;

    move-result-object p3

    .line 793
    invoke-virtual {v0, v1, p1, p2, p3}, Lee/cyber/smartid/SmartIdService;->checkLocalPendingStateForAccountRegistration(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/inter/CheckLocalPendingStateListener;)V

    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/stagnationlab/sk/a/a/h;)V
    .locals 6

    .line 419
    iget-boolean v0, p0, Lcom/stagnationlab/sk/a/a;->e:Z

    if-nez v0, :cond_0

    .line 420
    new-instance p1, Lcom/stagnationlab/sk/c/a;

    sget-object p2, Lcom/stagnationlab/sk/c/b;->I:Lcom/stagnationlab/sk/c/b;

    invoke-direct {p1, p2}, Lcom/stagnationlab/sk/c/a;-><init>(Lcom/stagnationlab/sk/c/b;)V

    invoke-interface {p4, p1}, Lcom/stagnationlab/sk/a/a/h;->a(Lcom/stagnationlab/sk/c/a;)V

    return-void

    .line 424
    :cond_0
    iget-object v0, p0, Lcom/stagnationlab/sk/a/a;->b:Lee/cyber/smartid/SmartIdService;

    .line 425
    invoke-direct {p0}, Lcom/stagnationlab/sk/a/a;->m()Ljava/lang/String;

    move-result-object v1

    new-instance v5, Lcom/stagnationlab/sk/a/a$2;

    invoke-direct {v5, p0, p4}, Lcom/stagnationlab/sk/a/a$2;-><init>(Lcom/stagnationlab/sk/a/a;Lcom/stagnationlab/sk/a/a/h;)V

    move-object v2, p2

    move-object v3, p1

    move-object v4, p3

    .line 424
    invoke-virtual/range {v0 .. v5}, Lee/cyber/smartid/SmartIdService;->confirmTransaction(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/inter/ConfirmTransactionListener;)V

    return-void
.end method

.method private a(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lee/cyber/smartid/dto/AccountWrapper;",
            ">;)V"
        }
    .end annotation

    .line 1045
    iget-object v0, p0, Lcom/stagnationlab/sk/a/a;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    if-eqz p1, :cond_0

    .line 1048
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lee/cyber/smartid/dto/AccountWrapper;

    .line 1049
    iget-object v1, p0, Lcom/stagnationlab/sk/a/a;->h:Ljava/util/List;

    new-instance v2, Lcom/stagnationlab/sk/models/Account;

    invoke-direct {v2, v0}, Lcom/stagnationlab/sk/models/Account;-><init>(Lee/cyber/smartid/dto/AccountWrapper;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1053
    :cond_0
    iget-object p1, p0, Lcom/stagnationlab/sk/a/a;->h:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p1

    if-eqz p1, :cond_1

    .line 1054
    iget-object p1, p0, Lcom/stagnationlab/sk/a/a;->h:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/stagnationlab/sk/models/Account;

    iput-object p1, p0, Lcom/stagnationlab/sk/a/a;->g:Lcom/stagnationlab/sk/models/Account;

    goto :goto_1

    :cond_1
    const/4 p1, 0x0

    .line 1056
    iput-object p1, p0, Lcom/stagnationlab/sk/a/a;->g:Lcom/stagnationlab/sk/models/Account;

    :goto_1
    return-void
.end method

.method private a(Z)V
    .locals 0

    .line 1246
    invoke-static {p1}, Lcom/stagnationlab/sk/a/d;->d(Z)V

    return-void
.end method

.method private b(Landroid/content/Context;Ljava/lang/String;)V
    .locals 0

    .line 138
    iput-object p2, p0, Lcom/stagnationlab/sk/a/a;->c:Ljava/lang/String;

    const/4 p2, 0x0

    .line 139
    iput-boolean p2, p0, Lcom/stagnationlab/sk/a/a;->e:Z

    .line 141
    invoke-static {p1}, Lee/cyber/smartid/SmartIdService;->getInstance(Landroid/content/Context;)Lee/cyber/smartid/SmartIdService;

    move-result-object p1

    iput-object p1, p0, Lcom/stagnationlab/sk/a/a;->b:Lee/cyber/smartid/SmartIdService;

    return-void
.end method

.method static synthetic b(Lcom/stagnationlab/sk/a/a;)V
    .locals 0

    .line 95
    invoke-direct {p0}, Lcom/stagnationlab/sk/a/a;->l()V

    return-void
.end method

.method static synthetic b(Lcom/stagnationlab/sk/a/a;Lcom/stagnationlab/sk/c/a;)V
    .locals 0

    .line 95
    invoke-direct {p0, p1}, Lcom/stagnationlab/sk/a/a;->b(Lcom/stagnationlab/sk/c/a;)V

    return-void
.end method

.method private b(Lcom/stagnationlab/sk/c/a;)V
    .locals 1

    .line 1224
    iget-object v0, p0, Lcom/stagnationlab/sk/a/a;->g:Lcom/stagnationlab/sk/models/Account;

    if-eqz v0, :cond_0

    .line 1225
    invoke-virtual {v0, p1}, Lcom/stagnationlab/sk/models/Account;->a(Lcom/stagnationlab/sk/c/a;)V

    :cond_0
    return-void
.end method

.method static synthetic c(Lcom/stagnationlab/sk/a/a;)Lcom/stagnationlab/sk/h/d;
    .locals 0

    .line 95
    iget-object p0, p0, Lcom/stagnationlab/sk/a/a;->j:Lcom/stagnationlab/sk/h/d;

    return-object p0
.end method

.method static synthetic d(Lcom/stagnationlab/sk/a/a;)Lcom/stagnationlab/sk/models/Account;
    .locals 0

    .line 95
    iget-object p0, p0, Lcom/stagnationlab/sk/a/a;->g:Lcom/stagnationlab/sk/models/Account;

    return-object p0
.end method

.method static synthetic e(Lcom/stagnationlab/sk/a/a;)Ljava/lang/String;
    .locals 0

    .line 95
    invoke-direct {p0}, Lcom/stagnationlab/sk/a/a;->m()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method static synthetic f(Lcom/stagnationlab/sk/a/a;)Lee/cyber/smartid/SmartIdService;
    .locals 0

    .line 95
    iget-object p0, p0, Lcom/stagnationlab/sk/a/a;->b:Lee/cyber/smartid/SmartIdService;

    return-object p0
.end method

.method static synthetic g(Lcom/stagnationlab/sk/a/a;)Ljava/util/List;
    .locals 0

    .line 95
    iget-object p0, p0, Lcom/stagnationlab/sk/a/a;->h:Ljava/util/List;

    return-object p0
.end method

.method private j()V
    .locals 2

    .line 145
    iget-boolean v0, p0, Lcom/stagnationlab/sk/a/a;->d:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x0

    .line 149
    iput-boolean v0, p0, Lcom/stagnationlab/sk/a/a;->e:Z

    const/4 v0, 0x1

    .line 150
    iput-boolean v0, p0, Lcom/stagnationlab/sk/a/a;->d:Z

    .line 152
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Initializing Smart-ID API. Lib: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/stagnationlab/sk/a/a;->b:Lee/cyber/smartid/SmartIdService;

    .line 153
    invoke-virtual {v1}, Lee/cyber/smartid/SmartIdService;->getVersion()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "; TSE: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/stagnationlab/sk/a/a;->b:Lee/cyber/smartid/SmartIdService;

    .line 154
    invoke-virtual {v1}, Lee/cyber/smartid/SmartIdService;->getVersionSmartIdTSE()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "; Crypto: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/stagnationlab/sk/a/a;->b:Lee/cyber/smartid/SmartIdService;

    .line 155
    invoke-virtual {v1}, Lee/cyber/smartid/SmartIdService;->getVersionCryptoLib()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Api"

    .line 152
    invoke-static {v1, v0}, Lcom/stagnationlab/sk/f;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 158
    new-instance v0, Lcom/stagnationlab/sk/a/a$1;

    invoke-direct {v0, p0}, Lcom/stagnationlab/sk/a/a$1;-><init>(Lcom/stagnationlab/sk/a/a;)V

    invoke-virtual {p0, v0}, Lcom/stagnationlab/sk/a/a;->a(Lcom/stagnationlab/sk/a/a/n;)V

    return-void
.end method

.method private k()V
    .locals 3

    .line 168
    iget-object v0, p0, Lcom/stagnationlab/sk/a/a;->b:Lee/cyber/smartid/SmartIdService;

    invoke-direct {p0}, Lcom/stagnationlab/sk/a/a;->m()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/stagnationlab/sk/a/a$12;

    invoke-direct {v2, p0}, Lcom/stagnationlab/sk/a/a$12;-><init>(Lcom/stagnationlab/sk/a/a;)V

    invoke-virtual {v0, v1, v2}, Lee/cyber/smartid/SmartIdService;->initService(Ljava/lang/String;Lee/cyber/smartid/inter/InitServiceListener;)V

    return-void
.end method

.method private l()V
    .locals 3

    .line 209
    iget-object v0, p0, Lcom/stagnationlab/sk/a/a;->b:Lee/cyber/smartid/SmartIdService;

    invoke-direct {p0}, Lcom/stagnationlab/sk/a/a;->m()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/stagnationlab/sk/a/a$23;

    invoke-direct {v2, p0}, Lcom/stagnationlab/sk/a/a$23;-><init>(Lcom/stagnationlab/sk/a/a;)V

    invoke-virtual {v0, v1, v2}, Lee/cyber/smartid/SmartIdService;->getAccounts(Ljava/lang/String;Lee/cyber/smartid/inter/GetAccountsListener;)V

    return-void
.end method

.method private m()Ljava/lang/String;
    .locals 2

    .line 1127
    iget v0, p0, Lcom/stagnationlab/sk/a/a;->a:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/stagnationlab/sk/a/a;->a:I

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/stagnationlab/sk/a/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a(Landroid/app/Activity;)V
    .locals 2

    .line 1254
    iget-object v0, p0, Lcom/stagnationlab/sk/a/a;->b:Lee/cyber/smartid/SmartIdService;

    new-instance v1, Lcom/stagnationlab/sk/a/a$25;

    invoke-direct {v1, p0, p1}, Lcom/stagnationlab/sk/a/a$25;-><init>(Lcom/stagnationlab/sk/a/a;Landroid/app/Activity;)V

    const-string p1, "SYSTEM_EVENT_LISTENER"

    invoke-virtual {v0, p1, v1}, Lee/cyber/smartid/SmartIdService;->addSystemEventListener(Ljava/lang/String;Lee/cyber/smartid/inter/SystemEventListener;)V

    return-void
.end method

.method public a(Landroid/app/Activity;ZLcom/stagnationlab/sk/a/a/y;)V
    .locals 1

    .line 1220
    new-instance v0, Lcom/stagnationlab/sk/a/c;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/stagnationlab/sk/a/c;-><init>(Lcom/stagnationlab/sk/a/a;Landroid/app/Activity;ZLcom/stagnationlab/sk/a/a/y;)V

    return-void
.end method

.method public a(Lcom/stagnationlab/sk/a/a/a;)V
    .locals 1

    .line 283
    iget-boolean v0, p0, Lcom/stagnationlab/sk/a/a;->e:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    .line 284
    invoke-interface {p1, p0, v0}, Lcom/stagnationlab/sk/a/a/a;->a(Lcom/stagnationlab/sk/a/a;Z)V

    goto :goto_0

    .line 286
    :cond_0
    iget-object v0, p0, Lcom/stagnationlab/sk/a/a;->f:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 288
    invoke-direct {p0}, Lcom/stagnationlab/sk/a/a;->j()V

    :goto_0
    return-void
.end method

.method public a(Lcom/stagnationlab/sk/a/a/b;)V
    .locals 3

    .line 1334
    iget-object v0, p0, Lcom/stagnationlab/sk/a/a;->b:Lee/cyber/smartid/SmartIdService;

    .line 1335
    invoke-direct {p0}, Lcom/stagnationlab/sk/a/a;->m()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/stagnationlab/sk/a/a$28;

    invoke-direct {v2, p0, p1}, Lcom/stagnationlab/sk/a/a$28;-><init>(Lcom/stagnationlab/sk/a/a;Lcom/stagnationlab/sk/a/a/b;)V

    .line 1334
    invoke-virtual {v0, v1, v2}, Lee/cyber/smartid/SmartIdService;->cancelInteractiveUpgrade(Ljava/lang/String;Lee/cyber/smartid/inter/CancelInteractiveUpgradeListener;)V

    return-void
.end method

.method public a(Lcom/stagnationlab/sk/a/a/m;)V
    .locals 3

    const-string v0, "Api"

    const-string v1, "Starting key generation"

    .line 564
    invoke-static {v0, v1}, Lcom/stagnationlab/sk/f;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 565
    iget-object v0, p0, Lcom/stagnationlab/sk/a/a;->b:Lee/cyber/smartid/SmartIdService;

    .line 566
    invoke-direct {p0}, Lcom/stagnationlab/sk/a/a;->m()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/stagnationlab/sk/a/a$6;

    invoke-direct {v2, p0, p1}, Lcom/stagnationlab/sk/a/a$6;-><init>(Lcom/stagnationlab/sk/a/a;Lcom/stagnationlab/sk/a/a/m;)V

    const/4 p1, 0x2

    .line 565
    invoke-virtual {v0, v1, p1, v2}, Lee/cyber/smartid/SmartIdService;->generateKeys(Ljava/lang/String;ILee/cyber/smartid/inter/GenerateKeysListener;)V

    return-void
.end method

.method public a(Lcom/stagnationlab/sk/a/a/n;)V
    .locals 3

    .line 1381
    iget-object v0, p0, Lcom/stagnationlab/sk/a/a;->b:Lee/cyber/smartid/SmartIdService;

    invoke-direct {p0}, Lcom/stagnationlab/sk/a/a;->m()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/stagnationlab/sk/a/a$30;

    invoke-direct {v2, p0, p1}, Lcom/stagnationlab/sk/a/a$30;-><init>(Lcom/stagnationlab/sk/a/a;Lcom/stagnationlab/sk/a/a/n;)V

    invoke-virtual {v0, v1, v2}, Lee/cyber/smartid/SmartIdService;->getDeviceFingerprint(Ljava/lang/String;Lee/cyber/smartid/inter/GetDeviceFingerprintListener;)V

    return-void
.end method

.method public a(Lcom/stagnationlab/sk/a/a/o;)V
    .locals 4

    .line 307
    iget-object v0, p0, Lcom/stagnationlab/sk/a/a;->b:Lee/cyber/smartid/SmartIdService;

    .line 308
    invoke-direct {p0}, Lcom/stagnationlab/sk/a/a;->m()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/stagnationlab/sk/a/a;->g:Lcom/stagnationlab/sk/models/Account;

    .line 309
    invoke-virtual {v2}, Lcom/stagnationlab/sk/models/Account;->a()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/stagnationlab/sk/a/a$33;

    invoke-direct {v3, p0, p1}, Lcom/stagnationlab/sk/a/a$33;-><init>(Lcom/stagnationlab/sk/a/a;Lcom/stagnationlab/sk/a/a/o;)V

    .line 307
    invoke-virtual {v0, v1, v2, v3}, Lee/cyber/smartid/SmartIdService;->getPendingOperation(Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/inter/GetPendingOperationListener;)V

    return-void
.end method

.method public a(Lcom/stagnationlab/sk/a/a/q;)V
    .locals 3

    .line 1276
    iget-object v0, p0, Lcom/stagnationlab/sk/a/a;->g:Lcom/stagnationlab/sk/models/Account;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 1277
    invoke-interface {p1, v0}, Lcom/stagnationlab/sk/a/a/q;->a(Ljava/lang/String;)V

    return-void

    .line 1282
    :cond_0
    iget-object v0, p0, Lcom/stagnationlab/sk/a/a;->b:Lee/cyber/smartid/SmartIdService;

    .line 1283
    invoke-direct {p0}, Lcom/stagnationlab/sk/a/a;->m()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/stagnationlab/sk/a/a$26;

    invoke-direct {v2, p0, p1}, Lcom/stagnationlab/sk/a/a$26;-><init>(Lcom/stagnationlab/sk/a/a;Lcom/stagnationlab/sk/a/a/q;)V

    .line 1282
    invoke-virtual {v0, v1, v2}, Lee/cyber/smartid/SmartIdService;->getPushNotificationData(Ljava/lang/String;Lee/cyber/smartid/inter/GetPushNotificationDataListener;)V

    return-void
.end method

.method public a(Lcom/stagnationlab/sk/a/a/s;)V
    .locals 3

    .line 1003
    iget-object v0, p0, Lcom/stagnationlab/sk/a/a;->b:Lee/cyber/smartid/SmartIdService;

    invoke-direct {p0}, Lcom/stagnationlab/sk/a/a;->m()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/stagnationlab/sk/a/a$18;

    invoke-direct {v2, p0, p1}, Lcom/stagnationlab/sk/a/a$18;-><init>(Lcom/stagnationlab/sk/a/a;Lcom/stagnationlab/sk/a/a/s;)V

    invoke-virtual {v0, v1, v2}, Lee/cyber/smartid/SmartIdService;->getSecureRandom(Ljava/lang/String;Lee/cyber/smartid/inter/GetSecureRandomListener;)V

    return-void
.end method

.method public a(Lcom/stagnationlab/sk/h/d;)V
    .locals 0

    .line 1304
    iput-object p1, p0, Lcom/stagnationlab/sk/a/a;->j:Lcom/stagnationlab/sk/h/d;

    return-void
.end method

.method public a(Lcom/stagnationlab/sk/models/RpRequest;Lcom/stagnationlab/sk/a/a/c;)V
    .locals 4

    .line 1135
    iget-object v0, p0, Lcom/stagnationlab/sk/a/a;->b:Lee/cyber/smartid/SmartIdService;

    .line 1136
    invoke-direct {p0}, Lcom/stagnationlab/sk/a/a;->m()Ljava/lang/String;

    move-result-object v1

    .line 1137
    invoke-virtual {p1}, Lcom/stagnationlab/sk/models/RpRequest;->h()Ljava/lang/String;

    move-result-object v2

    .line 1138
    invoke-virtual {p1}, Lcom/stagnationlab/sk/models/RpRequest;->i()Ljava/lang/String;

    move-result-object p1

    new-instance v3, Lcom/stagnationlab/sk/a/a$21;

    invoke-direct {v3, p0, p2}, Lcom/stagnationlab/sk/a/a$21;-><init>(Lcom/stagnationlab/sk/a/a;Lcom/stagnationlab/sk/a/a/c;)V

    .line 1135
    invoke-virtual {v0, v1, v2, p1, v3}, Lee/cyber/smartid/SmartIdService;->cancelRPRequest(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/inter/CancelRPRequestListener;)V

    return-void
.end method

.method public a(Lcom/stagnationlab/sk/models/RpRequest;Lcom/stagnationlab/sk/a/a/g;)V
    .locals 4

    .line 1158
    iget-object v0, p0, Lcom/stagnationlab/sk/a/a;->b:Lee/cyber/smartid/SmartIdService;

    .line 1159
    invoke-direct {p0}, Lcom/stagnationlab/sk/a/a;->m()Ljava/lang/String;

    move-result-object v1

    .line 1160
    invoke-virtual {p1}, Lcom/stagnationlab/sk/models/RpRequest;->h()Ljava/lang/String;

    move-result-object v2

    .line 1161
    invoke-virtual {p1}, Lcom/stagnationlab/sk/models/RpRequest;->i()Ljava/lang/String;

    move-result-object p1

    new-instance v3, Lcom/stagnationlab/sk/a/a$22;

    invoke-direct {v3, p0, p2}, Lcom/stagnationlab/sk/a/a$22;-><init>(Lcom/stagnationlab/sk/a/a;Lcom/stagnationlab/sk/a/a/g;)V

    .line 1158
    invoke-virtual {v0, v1, v2, p1, v3}, Lee/cyber/smartid/SmartIdService;->confirmRPRequest(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/inter/ConfirmRPRequestListener;)V

    return-void
.end method

.method public a(Lcom/stagnationlab/sk/models/RpRequest;Lcom/stagnationlab/sk/a/a/t;)V
    .locals 4

    .line 393
    iget-object v0, p0, Lcom/stagnationlab/sk/a/a;->b:Lee/cyber/smartid/SmartIdService;

    .line 394
    invoke-direct {p0}, Lcom/stagnationlab/sk/a/a;->m()Ljava/lang/String;

    move-result-object v1

    .line 395
    invoke-virtual {p1}, Lcom/stagnationlab/sk/models/RpRequest;->i()Ljava/lang/String;

    move-result-object v2

    .line 396
    invoke-virtual {p1}, Lcom/stagnationlab/sk/models/RpRequest;->h()Ljava/lang/String;

    move-result-object p1

    new-instance v3, Lcom/stagnationlab/sk/a/a$36;

    invoke-direct {v3, p0, p2}, Lcom/stagnationlab/sk/a/a$36;-><init>(Lcom/stagnationlab/sk/a/a;Lcom/stagnationlab/sk/a/a/t;)V

    .line 393
    invoke-virtual {v0, v1, v2, p1, v3}, Lee/cyber/smartid/SmartIdService;->createTransactionForRPRequest(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/inter/CreateTransactionForRPRequestListener;)V

    return-void
.end method

.method public a(Lcom/stagnationlab/sk/models/Transaction$Type;Lcom/stagnationlab/sk/a/a/p;)V
    .locals 7

    .line 1082
    iget-object v0, p0, Lcom/stagnationlab/sk/a/a;->g:Lcom/stagnationlab/sk/models/Account;

    if-nez v0, :cond_0

    .line 1084
    new-instance p1, Lcom/stagnationlab/sk/c/a;

    sget-object v0, Lcom/stagnationlab/sk/c/b;->H:Lcom/stagnationlab/sk/c/b;

    invoke-direct {p1, v0}, Lcom/stagnationlab/sk/c/a;-><init>(Lcom/stagnationlab/sk/c/b;)V

    invoke-interface {p2, p1}, Lcom/stagnationlab/sk/a/a/p;->a(Lcom/stagnationlab/sk/c/a;)V

    return-void

    .line 1089
    :cond_0
    iget-object v1, p0, Lcom/stagnationlab/sk/a/a;->b:Lee/cyber/smartid/SmartIdService;

    .line 1090
    invoke-direct {p0}, Lcom/stagnationlab/sk/a/a;->m()Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lcom/stagnationlab/sk/a/a;->g:Lcom/stagnationlab/sk/models/Account;

    .line 1091
    invoke-virtual {v0}, Lcom/stagnationlab/sk/models/Account;->a()Ljava/lang/String;

    move-result-object v3

    sget-object v0, Lcom/stagnationlab/sk/models/Transaction$Type;->AUTHENTICATION:Lcom/stagnationlab/sk/models/Transaction$Type;

    if-ne p1, v0, :cond_1

    const-string p1, "AUTHENTICATION"

    goto :goto_0

    :cond_1
    const-string p1, "SIGNATURE"

    :goto_0
    move-object v4, p1

    const/4 v5, 0x0

    new-instance v6, Lcom/stagnationlab/sk/a/a$20;

    invoke-direct {v6, p0, p2}, Lcom/stagnationlab/sk/a/a$20;-><init>(Lcom/stagnationlab/sk/a/a;Lcom/stagnationlab/sk/a/a/p;)V

    .line 1089
    invoke-virtual/range {v1 .. v6}, Lee/cyber/smartid/SmartIdService;->getKeyPinLength(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Lee/cyber/smartid/inter/GetKeyPinLengthListener;)V

    return-void
.end method

.method public a(Lcom/stagnationlab/sk/models/Transaction;Lcom/stagnationlab/sk/a/a/aa;)V
    .locals 4

    .line 1354
    iget-object v0, p0, Lcom/stagnationlab/sk/a/a;->b:Lee/cyber/smartid/SmartIdService;

    .line 1355
    invoke-direct {p0}, Lcom/stagnationlab/sk/a/a;->m()Ljava/lang/String;

    move-result-object v1

    .line 1356
    invoke-virtual {p1}, Lcom/stagnationlab/sk/models/Transaction;->f()Ljava/lang/String;

    move-result-object v2

    .line 1357
    invoke-virtual {p1}, Lcom/stagnationlab/sk/models/Transaction;->j()Ljava/lang/String;

    move-result-object p1

    new-instance v3, Lcom/stagnationlab/sk/a/a$29;

    invoke-direct {v3, p0, p2}, Lcom/stagnationlab/sk/a/a$29;-><init>(Lcom/stagnationlab/sk/a/a;Lcom/stagnationlab/sk/a/a/aa;)V

    .line 1354
    invoke-virtual {v0, v1, v2, p1, v3}, Lee/cyber/smartid/SmartIdService;->verifyTransactionVerificationCode(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/inter/VerifyTransactionVerificationCodeListener;)V

    return-void
.end method

.method public a(Lcom/stagnationlab/sk/models/Transaction;Lcom/stagnationlab/sk/a/a/d;)V
    .locals 3

    .line 536
    iget-boolean v0, p0, Lcom/stagnationlab/sk/a/a;->e:Z

    if-nez v0, :cond_0

    .line 537
    new-instance p1, Lcom/stagnationlab/sk/c/a;

    sget-object v0, Lcom/stagnationlab/sk/c/b;->I:Lcom/stagnationlab/sk/c/b;

    invoke-direct {p1, v0}, Lcom/stagnationlab/sk/c/a;-><init>(Lcom/stagnationlab/sk/c/b;)V

    invoke-interface {p2, p1}, Lcom/stagnationlab/sk/a/a/d;->a(Lcom/stagnationlab/sk/c/a;)V

    return-void

    .line 541
    :cond_0
    iget-object v0, p0, Lcom/stagnationlab/sk/a/a;->b:Lee/cyber/smartid/SmartIdService;

    .line 542
    invoke-direct {p0}, Lcom/stagnationlab/sk/a/a;->m()Ljava/lang/String;

    move-result-object v1

    .line 543
    invoke-virtual {p1}, Lcom/stagnationlab/sk/models/Transaction;->f()Ljava/lang/String;

    move-result-object p1

    new-instance v2, Lcom/stagnationlab/sk/a/a$5;

    invoke-direct {v2, p0, p2}, Lcom/stagnationlab/sk/a/a$5;-><init>(Lcom/stagnationlab/sk/a/a;Lcom/stagnationlab/sk/a/a/d;)V

    .line 541
    invoke-virtual {v0, v1, p1, v2}, Lee/cyber/smartid/SmartIdService;->cancelTransaction(Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/inter/CancelTransactionListener;)V

    return-void
.end method

.method public a(Lcom/stagnationlab/sk/models/Transaction;Lcom/stagnationlab/sk/a/a/v;)V
    .locals 1

    .line 452
    new-instance v0, Lcom/stagnationlab/sk/a/a$3;

    invoke-direct {v0, p0, p2}, Lcom/stagnationlab/sk/a/a$3;-><init>(Lcom/stagnationlab/sk/a/a;Lcom/stagnationlab/sk/a/a/v;)V

    invoke-direct {p0, p1, v0}, Lcom/stagnationlab/sk/a/a;->a(Lcom/stagnationlab/sk/models/Transaction;Lcom/stagnationlab/sk/a/a/e;)V

    return-void
.end method

.method public a(Lcom/stagnationlab/sk/models/Transaction;Ljava/lang/String;Lcom/stagnationlab/sk/a/a/h;)V
    .locals 1

    .line 529
    invoke-virtual {p1}, Lcom/stagnationlab/sk/models/Transaction;->f()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/stagnationlab/sk/models/Transaction;->j()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, v0, p2, p1, p3}, Lcom/stagnationlab/sk/a/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/stagnationlab/sk/a/a/h;)V

    return-void
.end method

.method public a(Lcom/stagnationlab/sk/models/Transaction;Ljava/lang/String;Lcom/stagnationlab/sk/a/a/x;)V
    .locals 3

    if-nez p2, :cond_0

    .line 838
    new-instance p2, Lcom/stagnationlab/sk/a/a$11;

    invoke-direct {p2, p0, p3, p1}, Lcom/stagnationlab/sk/a/a$11;-><init>(Lcom/stagnationlab/sk/a/a;Lcom/stagnationlab/sk/a/a/x;Lcom/stagnationlab/sk/models/Transaction;)V

    invoke-virtual {p0, p1, p2}, Lcom/stagnationlab/sk/a/a;->a(Lcom/stagnationlab/sk/models/Transaction;Lcom/stagnationlab/sk/a/a/v;)V

    return-void

    .line 870
    :cond_0
    invoke-virtual {p1}, Lcom/stagnationlab/sk/models/Transaction;->f()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/stagnationlab/sk/models/Transaction;->j()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/stagnationlab/sk/a/a$13;

    invoke-direct {v2, p0, p3, p1}, Lcom/stagnationlab/sk/a/a$13;-><init>(Lcom/stagnationlab/sk/a/a;Lcom/stagnationlab/sk/a/a/x;Lcom/stagnationlab/sk/models/Transaction;)V

    invoke-direct {p0, v0, p2, v1, v2}, Lcom/stagnationlab/sk/a/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/stagnationlab/sk/a/a/h;)V

    return-void
.end method

.method public a(Ljava/lang/String;Lcom/stagnationlab/sk/a/a/j;)V
    .locals 1

    .line 293
    new-instance v0, Lcom/stagnationlab/sk/a/a$32;

    invoke-direct {v0, p0, p2, p1}, Lcom/stagnationlab/sk/a/a$32;-><init>(Lcom/stagnationlab/sk/a/a;Lcom/stagnationlab/sk/a/a/j;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/stagnationlab/sk/a/a;->a(Lcom/stagnationlab/sk/a/a/k;)V

    return-void
.end method

.method public a(Ljava/lang/String;Lcom/stagnationlab/sk/a/a/t;)V
    .locals 3

    .line 912
    iget-object v0, p0, Lcom/stagnationlab/sk/a/a;->b:Lee/cyber/smartid/SmartIdService;

    .line 913
    invoke-direct {p0}, Lcom/stagnationlab/sk/a/a;->m()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/stagnationlab/sk/a/a$15;

    invoke-direct {v2, p0, p2}, Lcom/stagnationlab/sk/a/a$15;-><init>(Lcom/stagnationlab/sk/a/a;Lcom/stagnationlab/sk/a/a/t;)V

    .line 912
    invoke-virtual {v0, v1, p1, v2}, Lee/cyber/smartid/SmartIdService;->getTransaction(Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/inter/GetTransactionListener;)V

    return-void
.end method

.method public a(Ljava/lang/String;Lcom/stagnationlab/sk/a/a/w;)V
    .locals 3

    .line 937
    iget-object v0, p0, Lcom/stagnationlab/sk/a/a;->b:Lee/cyber/smartid/SmartIdService;

    .line 938
    invoke-direct {p0}, Lcom/stagnationlab/sk/a/a;->m()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/stagnationlab/sk/a/a$16;

    invoke-direct {v2, p0, p2}, Lcom/stagnationlab/sk/a/a$16;-><init>(Lcom/stagnationlab/sk/a/a;Lcom/stagnationlab/sk/a/a/w;)V

    .line 937
    invoke-virtual {v0, v1, p1, v2}, Lee/cyber/smartid/SmartIdService;->storePushNotificationData(Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/inter/StorePushNotificationDataListener;)V

    return-void
.end method

.method public a(Ljava/lang/String;Lcom/stagnationlab/sk/a/a/z;)V
    .locals 3

    .line 1019
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1020
    new-instance v1, Lee/cyber/smartid/tse/dto/PinInfo;

    const-string v2, "pin"

    invoke-direct {v1, v2, p1}, Lee/cyber/smartid/tse/dto/PinInfo;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1022
    iget-object p1, p0, Lcom/stagnationlab/sk/a/a;->b:Lee/cyber/smartid/SmartIdService;

    .line 1023
    invoke-direct {p0}, Lcom/stagnationlab/sk/a/a;->m()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/stagnationlab/sk/a/a$19;

    invoke-direct {v2, p0, p2}, Lcom/stagnationlab/sk/a/a$19;-><init>(Lcom/stagnationlab/sk/a/a;Lcom/stagnationlab/sk/a/a/z;)V

    .line 1022
    invoke-virtual {p1, v1, v0, v2}, Lee/cyber/smartid/SmartIdService;->validatePins(Ljava/lang/String;Ljava/util/ArrayList;Lee/cyber/smartid/inter/ValidatePinListener;)V

    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Lcom/stagnationlab/sk/a/a/r;)V
    .locals 9

    const-string v0, "Api"

    const-string v1, "Starting getRegistrationToken"

    .line 602
    invoke-static {v0, v1}, Lcom/stagnationlab/sk/f;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 604
    iget-object v2, p0, Lcom/stagnationlab/sk/a/a;->b:Lee/cyber/smartid/SmartIdService;

    .line 605
    invoke-direct {p0}, Lcom/stagnationlab/sk/a/a;->m()Ljava/lang/String;

    move-result-object v3

    new-instance v8, Lcom/stagnationlab/sk/a/a$7;

    invoke-direct {v8, p0, p3}, Lcom/stagnationlab/sk/a/a$7;-><init>(Lcom/stagnationlab/sk/a/a;Lcom/stagnationlab/sk/a/a/r;)V

    const-string v4, "KEYTOKEN"

    const-string v5, "MODULI-WITH-NONCE-SHA256"

    move-object v6, p1

    move-object v7, p2

    .line 604
    invoke-virtual/range {v2 .. v8}, Lee/cyber/smartid/SmartIdService;->getRegistrationToken(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/inter/GetRegistrationTokenListener;)V

    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/stagnationlab/sk/a/a/o;)V
    .locals 2

    if-eqz p1, :cond_0

    .line 350
    new-instance p2, Lcom/stagnationlab/sk/a/a$34;

    invoke-direct {p2, p0, p4}, Lcom/stagnationlab/sk/a/a$34;-><init>(Lcom/stagnationlab/sk/a/a;Lcom/stagnationlab/sk/a/a/o;)V

    invoke-virtual {p0, p1, p2}, Lcom/stagnationlab/sk/a/a;->a(Ljava/lang/String;Lcom/stagnationlab/sk/a/a/t;)V

    return-void

    .line 366
    :cond_0
    iget-object p1, p0, Lcom/stagnationlab/sk/a/a;->b:Lee/cyber/smartid/SmartIdService;

    .line 367
    invoke-direct {p0}, Lcom/stagnationlab/sk/a/a;->m()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lcom/stagnationlab/sk/a/a$35;

    invoke-direct {v1, p0, p4}, Lcom/stagnationlab/sk/a/a$35;-><init>(Lcom/stagnationlab/sk/a/a;Lcom/stagnationlab/sk/a/a/o;)V

    .line 366
    invoke-virtual {p1, v0, p2, p3, v1}, Lee/cyber/smartid/SmartIdService;->getRPRequest(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/inter/GetRPRequestListener;)V

    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/stagnationlab/sk/a/a/l;)V
    .locals 7

    .line 977
    iget-object v0, p0, Lcom/stagnationlab/sk/a/a;->b:Lee/cyber/smartid/SmartIdService;

    .line 978
    invoke-direct {p0}, Lcom/stagnationlab/sk/a/a;->m()Ljava/lang/String;

    move-result-object v1

    new-instance v6, Lcom/stagnationlab/sk/a/a$17;

    invoke-direct {v6, p0, p5}, Lcom/stagnationlab/sk/a/a$17;-><init>(Lcom/stagnationlab/sk/a/a;Lcom/stagnationlab/sk/a/a/l;)V

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    .line 977
    invoke-virtual/range {v0 .. v6}, Lee/cyber/smartid/SmartIdService;->encryptKeys(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/inter/EncryptKeysListener;)V

    return-void
.end method

.method public a(ZLcom/stagnationlab/sk/a/a/f;)V
    .locals 3

    .line 1308
    iget-object v0, p0, Lcom/stagnationlab/sk/a/a;->b:Lee/cyber/smartid/SmartIdService;

    .line 1309
    invoke-direct {p0}, Lcom/stagnationlab/sk/a/a;->m()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/stagnationlab/sk/a/a$27;

    invoke-direct {v2, p0, p2}, Lcom/stagnationlab/sk/a/a$27;-><init>(Lcom/stagnationlab/sk/a/a;Lcom/stagnationlab/sk/a/a/f;)V

    .line 1308
    invoke-virtual {v0, v1, p1, v2}, Lee/cyber/smartid/SmartIdService;->getSafetyNetAttestation(Ljava/lang/String;ZLee/cyber/smartid/inter/GetSafetyNetAttestationListener;)V

    return-void
.end method

.method public a(ZLcom/stagnationlab/sk/a/a/y;)V
    .locals 3

    .line 1182
    iget-object p1, p0, Lcom/stagnationlab/sk/a/a;->g:Lcom/stagnationlab/sk/models/Account;

    if-nez p1, :cond_0

    .line 1183
    new-instance p1, Lcom/stagnationlab/sk/c/a;

    sget-object v0, Lcom/stagnationlab/sk/c/b;->f:Lcom/stagnationlab/sk/c/b;

    invoke-direct {p1, v0}, Lcom/stagnationlab/sk/c/a;-><init>(Lcom/stagnationlab/sk/c/b;)V

    invoke-interface {p2, p1}, Lcom/stagnationlab/sk/a/a/y;->a(Lcom/stagnationlab/sk/c/a;)V

    return-void

    .line 1188
    :cond_0
    iget-object p1, p0, Lcom/stagnationlab/sk/a/a;->b:Lee/cyber/smartid/SmartIdService;

    .line 1189
    invoke-direct {p0}, Lcom/stagnationlab/sk/a/a;->m()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/stagnationlab/sk/a/a;->g:Lcom/stagnationlab/sk/models/Account;

    .line 1190
    invoke-virtual {v1}, Lcom/stagnationlab/sk/models/Account;->a()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/stagnationlab/sk/a/a$24;

    invoke-direct {v2, p0, p2}, Lcom/stagnationlab/sk/a/a$24;-><init>(Lcom/stagnationlab/sk/a/a;Lcom/stagnationlab/sk/a/a/y;)V

    .line 1188
    invoke-virtual {p1, v0, v1, v2}, Lee/cyber/smartid/SmartIdService;->getAccountStatus(Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/inter/GetAccountStatusListener;)V

    return-void
.end method

.method public a(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/stagnationlab/sk/a/a/i;)V
    .locals 18

    move-object/from16 v8, p0

    const/4 v0, 0x0

    if-eqz p4, :cond_0

    const-string v1, "IDENTITYTOKEN"

    :goto_0
    move-object v11, v1

    goto :goto_1

    :cond_0
    if-eqz p5, :cond_1

    const-string v1, "KEYTOKEN"

    goto :goto_0

    :cond_1
    move-object v11, v0

    :goto_1
    if-eqz p1, :cond_2

    .line 654
    new-instance v9, Lcom/stagnationlab/sk/a/a$8;

    move-object v0, v9

    move-object/from16 v1, p0

    move-object v2, v11

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p6

    move-object/from16 v6, p4

    move-object/from16 v7, p5

    invoke-direct/range {v0 .. v7}, Lcom/stagnationlab/sk/a/a$8;-><init>(Lcom/stagnationlab/sk/a/a;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/stagnationlab/sk/a/a/i;Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v6, p2

    move-object/from16 v7, p3

    invoke-direct {v8, v6, v7, v9}, Lcom/stagnationlab/sk/a/a;->a(Ljava/lang/String;Ljava/lang/String;Lcom/stagnationlab/sk/a/a/e;)V

    return-void

    :cond_2
    move-object/from16 v6, p2

    move-object/from16 v7, p3

    .line 691
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Starting add account with type "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Api"

    invoke-static {v2, v1}, Lcom/stagnationlab/sk/f;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 693
    iget-object v9, v8, Lcom/stagnationlab/sk/a/a;->b:Lee/cyber/smartid/SmartIdService;

    if-nez p5, :cond_3

    goto :goto_2

    :cond_3
    const-string v0, "MODULI-WITH-NONCE-SHA256"

    :goto_2
    move-object v12, v0

    const-string v5, "addAccount"

    move-object/from16 v0, p0

    move-object v1, v11

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p6

    .line 702
    invoke-direct/range {v0 .. v5}, Lcom/stagnationlab/sk/a/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/stagnationlab/sk/a/a/i;Ljava/lang/String;)Lee/cyber/smartid/inter/AddAccountListener;

    move-result-object v17

    const-string v10, "TAG_ADD_ACCOUNT"

    move-object/from16 v13, p5

    move-object/from16 v14, p4

    move-object/from16 v15, p2

    move-object/from16 v16, p3

    .line 693
    invoke-virtual/range {v9 .. v17}, Lee/cyber/smartid/SmartIdService;->addAccount(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/inter/AddAccountListener;)V

    return-void
.end method

.method public b()Lcom/stagnationlab/sk/models/Account;
    .locals 1

    .line 1078
    iget-object v0, p0, Lcom/stagnationlab/sk/a/a;->g:Lcom/stagnationlab/sk/models/Account;

    return-object v0
.end method

.method public c()Ljava/lang/String;
    .locals 1

    .line 1115
    iget-object v0, p0, Lcom/stagnationlab/sk/a/a;->b:Lee/cyber/smartid/SmartIdService;

    invoke-virtual {v0}, Lee/cyber/smartid/SmartIdService;->getVersion()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public d()Ljava/lang/String;
    .locals 1

    .line 1119
    iget-object v0, p0, Lcom/stagnationlab/sk/a/a;->b:Lee/cyber/smartid/SmartIdService;

    invoke-virtual {v0}, Lee/cyber/smartid/SmartIdService;->getVersionSmartIdTSE()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public e()Ljava/lang/String;
    .locals 1

    .line 1123
    iget-object v0, p0, Lcom/stagnationlab/sk/a/a;->b:Lee/cyber/smartid/SmartIdService;

    invoke-virtual {v0}, Lee/cyber/smartid/SmartIdService;->getVersionCryptoLib()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public f()Z
    .locals 1

    .line 1230
    iget-object v0, p0, Lcom/stagnationlab/sk/a/a;->g:Lcom/stagnationlab/sk/models/Account;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/stagnationlab/sk/models/Account;->c()Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    .line 1235
    :cond_0
    iget-object v0, p0, Lcom/stagnationlab/sk/a/a;->g:Lcom/stagnationlab/sk/models/Account;

    invoke-virtual {v0}, Lcom/stagnationlab/sk/models/Account;->b()Z

    move-result v0

    return v0

    :cond_1
    :goto_0
    const/4 v0, 0x1

    return v0
.end method

.method public g()V
    .locals 2

    .line 1239
    iget-object v0, p0, Lcom/stagnationlab/sk/a/a;->g:Lcom/stagnationlab/sk/models/Account;

    if-eqz v0, :cond_0

    const/4 v1, 0x1

    .line 1240
    invoke-virtual {v0, v1}, Lcom/stagnationlab/sk/models/Account;->c(Z)V

    .line 1241
    iget-object v0, p0, Lcom/stagnationlab/sk/a/a;->g:Lcom/stagnationlab/sk/models/Account;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/stagnationlab/sk/models/Account;->a(Ljava/util/List;)V

    :cond_0
    return-void
.end method

.method public h()Z
    .locals 1

    .line 1250
    iget-object v0, p0, Lcom/stagnationlab/sk/a/a;->g:Lcom/stagnationlab/sk/models/Account;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/stagnationlab/sk/models/Account;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public i()V
    .locals 2

    .line 1272
    iget-object v0, p0, Lcom/stagnationlab/sk/a/a;->b:Lee/cyber/smartid/SmartIdService;

    const-string v1, "SYSTEM_EVENT_LISTENER"

    invoke-virtual {v0, v1}, Lee/cyber/smartid/SmartIdService;->removeSystemEventListener(Ljava/lang/String;)V

    return-void
.end method
