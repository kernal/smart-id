.class Lcom/stagnationlab/sk/a/a$9$1;
.super Ljava/lang/Object;
.source "Api.java"

# interfaces
.implements Lcom/stagnationlab/sk/a/a/y;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/stagnationlab/sk/a/a$9;->onAddAccountSuccess(Ljava/lang/String;Lee/cyber/smartid/dto/jsonrpc/resp/AddAccountResp;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lee/cyber/smartid/dto/jsonrpc/resp/AddAccountResp;

.field final synthetic b:Lcom/stagnationlab/sk/a/a$9;


# direct methods
.method constructor <init>(Lcom/stagnationlab/sk/a/a$9;Lee/cyber/smartid/dto/jsonrpc/resp/AddAccountResp;)V
    .locals 0

    .line 726
    iput-object p1, p0, Lcom/stagnationlab/sk/a/a$9$1;->b:Lcom/stagnationlab/sk/a/a$9;

    iput-object p2, p0, Lcom/stagnationlab/sk/a/a$9$1;->a:Lee/cyber/smartid/dto/jsonrpc/resp/AddAccountResp;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 4

    .line 729
    iget-object v0, p0, Lcom/stagnationlab/sk/a/a$9$1;->b:Lcom/stagnationlab/sk/a/a$9;

    iget-object v0, v0, Lcom/stagnationlab/sk/a/a$9;->a:Lcom/stagnationlab/sk/a/a/i;

    iget-object v1, p0, Lcom/stagnationlab/sk/a/a$9$1;->b:Lcom/stagnationlab/sk/a/a$9;

    iget-object v1, v1, Lcom/stagnationlab/sk/a/a$9;->f:Lcom/stagnationlab/sk/a/a;

    .line 730
    invoke-static {v1}, Lcom/stagnationlab/sk/a/a;->d(Lcom/stagnationlab/sk/a/a;)Lcom/stagnationlab/sk/models/Account;

    move-result-object v1

    iget-object v2, p0, Lcom/stagnationlab/sk/a/a$9$1;->a:Lee/cyber/smartid/dto/jsonrpc/resp/AddAccountResp;

    .line 731
    invoke-virtual {v2}, Lee/cyber/smartid/dto/jsonrpc/resp/AddAccountResp;->getAuthCsrTransactionUUID()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/stagnationlab/sk/a/a$9$1;->a:Lee/cyber/smartid/dto/jsonrpc/resp/AddAccountResp;

    .line 732
    invoke-virtual {v3}, Lee/cyber/smartid/dto/jsonrpc/resp/AddAccountResp;->getSignCsrTransactionUUID()Ljava/lang/String;

    move-result-object v3

    .line 729
    invoke-interface {v0, v1, v2, v3}, Lcom/stagnationlab/sk/a/a/i;->a(Lcom/stagnationlab/sk/models/Account;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public a(Lcom/stagnationlab/sk/c/a;)V
    .locals 1

    .line 738
    iget-object v0, p0, Lcom/stagnationlab/sk/a/a$9$1;->b:Lcom/stagnationlab/sk/a/a$9;

    iget-object v0, v0, Lcom/stagnationlab/sk/a/a$9;->a:Lcom/stagnationlab/sk/a/a/i;

    invoke-interface {v0, p1}, Lcom/stagnationlab/sk/a/a/i;->a(Lcom/stagnationlab/sk/c/a;)V

    return-void
.end method
