.class Lcom/stagnationlab/sk/a/a$29;
.super Ljava/lang/Object;
.source "Api.java"

# interfaces
.implements Lee/cyber/smartid/inter/VerifyTransactionVerificationCodeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/stagnationlab/sk/a/a;->a(Lcom/stagnationlab/sk/models/Transaction;Lcom/stagnationlab/sk/a/a/aa;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/stagnationlab/sk/a/a/aa;

.field final synthetic b:Lcom/stagnationlab/sk/a/a;


# direct methods
.method constructor <init>(Lcom/stagnationlab/sk/a/a;Lcom/stagnationlab/sk/a/a/aa;)V
    .locals 0

    .line 1358
    iput-object p1, p0, Lcom/stagnationlab/sk/a/a$29;->b:Lcom/stagnationlab/sk/a/a;

    iput-object p2, p0, Lcom/stagnationlab/sk/a/a$29;->a:Lcom/stagnationlab/sk/a/a/aa;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onVerifyTransactionVerificationCodeFailed(Ljava/lang/String;Lee/cyber/smartid/dto/SmartIdError;)V
    .locals 2

    .line 1372
    new-instance p1, Lcom/stagnationlab/sk/d/b;

    invoke-direct {p1, p2}, Lcom/stagnationlab/sk/d/b;-><init>(Lee/cyber/smartid/dto/SmartIdError;)V

    const-string v0, "Api"

    const-string v1, "Verify Transaction Verification Code failed"

    invoke-static {v0, v1, p1}, Lcom/stagnationlab/sk/f;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1374
    iget-object p1, p0, Lcom/stagnationlab/sk/a/a$29;->a:Lcom/stagnationlab/sk/a/a/aa;

    new-instance v0, Lcom/stagnationlab/sk/c/a;

    const-string v1, "verifyTransactionVerificationCode"

    invoke-direct {v0, p2, v1}, Lcom/stagnationlab/sk/c/a;-><init>(Lee/cyber/smartid/dto/SmartIdError;Ljava/lang/String;)V

    invoke-interface {p1, v0}, Lcom/stagnationlab/sk/a/a/aa;->a(Lcom/stagnationlab/sk/c/a;)V

    return-void
.end method

.method public onVerifyTransactionVerificationCodeSuccess(Ljava/lang/String;Lee/cyber/smartid/dto/jsonrpc/resp/VerifyTransactionVerificationCodeResp;)V
    .locals 0

    .line 1363
    invoke-virtual {p2}, Lee/cyber/smartid/dto/jsonrpc/resp/VerifyTransactionVerificationCodeResp;->isCorrectVerificationCode()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 1364
    iget-object p1, p0, Lcom/stagnationlab/sk/a/a$29;->a:Lcom/stagnationlab/sk/a/a/aa;

    invoke-interface {p1}, Lcom/stagnationlab/sk/a/a/aa;->a()V

    goto :goto_0

    .line 1366
    :cond_0
    iget-object p1, p0, Lcom/stagnationlab/sk/a/a$29;->a:Lcom/stagnationlab/sk/a/a/aa;

    invoke-interface {p1}, Lcom/stagnationlab/sk/a/a/aa;->b()V

    :goto_0
    return-void
.end method
