.class Lcom/stagnationlab/sk/a/a$9;
.super Ljava/lang/Object;
.source "Api.java"

# interfaces
.implements Lee/cyber/smartid/inter/AddAccountListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/stagnationlab/sk/a/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/stagnationlab/sk/a/a/i;Ljava/lang/String;)Lee/cyber/smartid/inter/AddAccountListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/stagnationlab/sk/a/a/i;

.field final synthetic b:Ljava/lang/String;

.field final synthetic c:Ljava/lang/String;

.field final synthetic d:Ljava/lang/String;

.field final synthetic e:Ljava/lang/String;

.field final synthetic f:Lcom/stagnationlab/sk/a/a;


# direct methods
.method constructor <init>(Lcom/stagnationlab/sk/a/a;Lcom/stagnationlab/sk/a/a/i;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 713
    iput-object p1, p0, Lcom/stagnationlab/sk/a/a$9;->f:Lcom/stagnationlab/sk/a/a;

    iput-object p2, p0, Lcom/stagnationlab/sk/a/a$9;->a:Lcom/stagnationlab/sk/a/a/i;

    iput-object p3, p0, Lcom/stagnationlab/sk/a/a$9;->b:Ljava/lang/String;

    iput-object p4, p0, Lcom/stagnationlab/sk/a/a$9;->c:Ljava/lang/String;

    iput-object p5, p0, Lcom/stagnationlab/sk/a/a$9;->d:Ljava/lang/String;

    iput-object p6, p0, Lcom/stagnationlab/sk/a/a$9;->e:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAddAccountFailed(Ljava/lang/String;Lee/cyber/smartid/dto/SmartIdError;)V
    .locals 3

    .line 747
    new-instance p1, Lcom/stagnationlab/sk/c/a;

    iget-object v0, p0, Lcom/stagnationlab/sk/a/a$9;->b:Ljava/lang/String;

    invoke-direct {p1, p2, v0}, Lcom/stagnationlab/sk/c/a;-><init>(Lee/cyber/smartid/dto/SmartIdError;Ljava/lang/String;)V

    .line 750
    invoke-virtual {p1}, Lcom/stagnationlab/sk/c/a;->a()Lcom/stagnationlab/sk/c/b;

    move-result-object v0

    sget-object v1, Lcom/stagnationlab/sk/c/b;->k:Lcom/stagnationlab/sk/c/b;

    const-string v2, "Api"

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/stagnationlab/sk/a/a$9;->c:Ljava/lang/String;

    const-string v1, "KEYTOKEN"

    .line 751
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string p2, "Add account failed with INVALID_TOKEN error during KEYTOKEN registration"

    .line 755
    invoke-static {v2, p2}, Lcom/stagnationlab/sk/f;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 757
    iget-object p2, p0, Lcom/stagnationlab/sk/a/a$9;->a:Lcom/stagnationlab/sk/a/a/i;

    invoke-interface {p2, p1}, Lcom/stagnationlab/sk/a/a/i;->a(Lcom/stagnationlab/sk/c/a;)V

    return-void

    .line 761
    :cond_0
    new-instance v0, Lcom/stagnationlab/sk/d/b;

    invoke-direct {v0, p2}, Lcom/stagnationlab/sk/d/b;-><init>(Lee/cyber/smartid/dto/SmartIdError;)V

    const-string p2, "Add account failed"

    invoke-static {v2, p2, v0}, Lcom/stagnationlab/sk/f;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 767
    iget-object p2, p0, Lcom/stagnationlab/sk/a/a$9;->f:Lcom/stagnationlab/sk/a/a;

    iget-object v0, p0, Lcom/stagnationlab/sk/a/a$9;->d:Ljava/lang/String;

    iget-object v1, p0, Lcom/stagnationlab/sk/a/a$9;->e:Ljava/lang/String;

    new-instance v2, Lcom/stagnationlab/sk/a/a$9$2;

    invoke-direct {v2, p0, p1}, Lcom/stagnationlab/sk/a/a$9$2;-><init>(Lcom/stagnationlab/sk/a/a$9;Lcom/stagnationlab/sk/c/a;)V

    invoke-static {p2, v0, v1, v2}, Lcom/stagnationlab/sk/a/a;->a(Lcom/stagnationlab/sk/a/a;Ljava/lang/String;Ljava/lang/String;Lcom/stagnationlab/sk/a/a/e;)V

    return-void
.end method

.method public onAddAccountSuccess(Ljava/lang/String;Lee/cyber/smartid/dto/jsonrpc/resp/AddAccountResp;)V
    .locals 2

    .line 716
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "Add account succeeded with accounts: "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/stagnationlab/sk/a/a$9;->f:Lcom/stagnationlab/sk/a/a;

    invoke-static {v0}, Lcom/stagnationlab/sk/a/a;->g(Lcom/stagnationlab/sk/a/a;)Ljava/util/List;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string v0, "Api"

    invoke-static {v0, p1}, Lcom/stagnationlab/sk/f;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 718
    invoke-virtual {p2}, Lee/cyber/smartid/dto/jsonrpc/resp/AddAccountResp;->getAccounts()Ljava/util/ArrayList;

    move-result-object p1

    .line 719
    iget-object v0, p0, Lcom/stagnationlab/sk/a/a$9;->f:Lcom/stagnationlab/sk/a/a;

    invoke-static {v0, p1}, Lcom/stagnationlab/sk/a/a;->a(Lcom/stagnationlab/sk/a/a;Ljava/util/List;)V

    .line 721
    iget-object p1, p0, Lcom/stagnationlab/sk/a/a$9;->f:Lcom/stagnationlab/sk/a/a;

    invoke-static {p1}, Lcom/stagnationlab/sk/a/a;->d(Lcom/stagnationlab/sk/a/a;)Lcom/stagnationlab/sk/models/Account;

    move-result-object p1

    if-nez p1, :cond_0

    .line 722
    iget-object p1, p0, Lcom/stagnationlab/sk/a/a$9;->a:Lcom/stagnationlab/sk/a/a/i;

    new-instance p2, Lcom/stagnationlab/sk/c/a;

    sget-object v0, Lcom/stagnationlab/sk/c/b;->P:Lcom/stagnationlab/sk/c/b;

    invoke-direct {p2, v0}, Lcom/stagnationlab/sk/c/a;-><init>(Lcom/stagnationlab/sk/c/b;)V

    invoke-interface {p1, p2}, Lcom/stagnationlab/sk/a/a/i;->a(Lcom/stagnationlab/sk/c/a;)V

    goto :goto_0

    .line 724
    :cond_0
    iget-object p1, p0, Lcom/stagnationlab/sk/a/a$9;->f:Lcom/stagnationlab/sk/a/a;

    invoke-static {p1}, Lcom/stagnationlab/sk/a/a;->d(Lcom/stagnationlab/sk/a/a;)Lcom/stagnationlab/sk/models/Account;

    move-result-object p1

    invoke-virtual {p2}, Lee/cyber/smartid/dto/jsonrpc/resp/AddAccountResp;->getIdentityData()Lee/cyber/smartid/dto/jsonrpc/IdentityData;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/stagnationlab/sk/models/Account;->a(Lee/cyber/smartid/dto/jsonrpc/IdentityData;)V

    .line 725
    iget-object p1, p0, Lcom/stagnationlab/sk/a/a$9;->f:Lcom/stagnationlab/sk/a/a;

    invoke-static {p1}, Lcom/stagnationlab/sk/a/a;->d(Lcom/stagnationlab/sk/a/a;)Lcom/stagnationlab/sk/models/Account;

    move-result-object p1

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/stagnationlab/sk/models/Account;->c(Z)V

    .line 726
    iget-object p1, p0, Lcom/stagnationlab/sk/a/a$9;->f:Lcom/stagnationlab/sk/a/a;

    new-instance v1, Lcom/stagnationlab/sk/a/a$9$1;

    invoke-direct {v1, p0, p2}, Lcom/stagnationlab/sk/a/a$9$1;-><init>(Lcom/stagnationlab/sk/a/a$9;Lee/cyber/smartid/dto/jsonrpc/resp/AddAccountResp;)V

    invoke-virtual {p1, v0, v1}, Lcom/stagnationlab/sk/a/a;->a(ZLcom/stagnationlab/sk/a/a/y;)V

    :goto_0
    return-void
.end method
