.class Lcom/stagnationlab/sk/a/a$23;
.super Ljava/lang/Object;
.source "Api.java"

# interfaces
.implements Lee/cyber/smartid/inter/GetAccountsListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/stagnationlab/sk/a/a;->l()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/stagnationlab/sk/a/a;


# direct methods
.method constructor <init>(Lcom/stagnationlab/sk/a/a;)V
    .locals 0

    .line 209
    iput-object p1, p0, Lcom/stagnationlab/sk/a/a$23;->a:Lcom/stagnationlab/sk/a/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onGetAccountsFailed(Ljava/lang/String;Lee/cyber/smartid/dto/SmartIdError;)V
    .locals 2

    .line 242
    new-instance p1, Lcom/stagnationlab/sk/d/b;

    invoke-direct {p1, p2}, Lcom/stagnationlab/sk/d/b;-><init>(Lee/cyber/smartid/dto/SmartIdError;)V

    const-string v0, "Api"

    const-string v1, "Get accounts failed"

    invoke-static {v0, v1, p1}, Lcom/stagnationlab/sk/f;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 244
    iget-object p1, p0, Lcom/stagnationlab/sk/a/a$23;->a:Lcom/stagnationlab/sk/a/a;

    new-instance v0, Lcom/stagnationlab/sk/c/a;

    const-string v1, "getAccounts"

    invoke-direct {v0, p2, v1}, Lcom/stagnationlab/sk/c/a;-><init>(Lee/cyber/smartid/dto/SmartIdError;Ljava/lang/String;)V

    invoke-static {p1, v0}, Lcom/stagnationlab/sk/a/a;->a(Lcom/stagnationlab/sk/a/a;Lcom/stagnationlab/sk/c/a;)V

    return-void
.end method

.method public onGetAccountsSuccess(Ljava/lang/String;Lee/cyber/smartid/dto/jsonrpc/resp/GetAccountsResp;)V
    .locals 1

    .line 212
    invoke-virtual {p2}, Lee/cyber/smartid/dto/jsonrpc/resp/GetAccountsResp;->getAccounts()Ljava/util/ArrayList;

    move-result-object p1

    .line 214
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "Got accounts: "

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    const-string v0, "Api"

    invoke-static {v0, p2}, Lcom/stagnationlab/sk/f;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 215
    iget-object p2, p0, Lcom/stagnationlab/sk/a/a$23;->a:Lcom/stagnationlab/sk/a/a;

    invoke-static {p2, p1}, Lcom/stagnationlab/sk/a/a;->a(Lcom/stagnationlab/sk/a/a;Ljava/util/List;)V

    .line 217
    iget-object p1, p0, Lcom/stagnationlab/sk/a/a$23;->a:Lcom/stagnationlab/sk/a/a;

    invoke-static {p1}, Lcom/stagnationlab/sk/a/a;->d(Lcom/stagnationlab/sk/a/a;)Lcom/stagnationlab/sk/models/Account;

    move-result-object p1

    if-nez p1, :cond_0

    .line 218
    iget-object p1, p0, Lcom/stagnationlab/sk/a/a$23;->a:Lcom/stagnationlab/sk/a/a;

    const/4 p2, 0x0

    invoke-static {p1, p2}, Lcom/stagnationlab/sk/a/a;->a(Lcom/stagnationlab/sk/a/a;Lcom/stagnationlab/sk/c/a;)V

    return-void

    .line 223
    :cond_0
    iget-object p1, p0, Lcom/stagnationlab/sk/a/a$23;->a:Lcom/stagnationlab/sk/a/a;

    const/4 p2, 0x1

    new-instance v0, Lcom/stagnationlab/sk/a/a$23$1;

    invoke-direct {v0, p0}, Lcom/stagnationlab/sk/a/a$23$1;-><init>(Lcom/stagnationlab/sk/a/a$23;)V

    invoke-virtual {p1, p2, v0}, Lcom/stagnationlab/sk/a/a;->a(ZLcom/stagnationlab/sk/a/a/y;)V

    return-void
.end method
