.class Lcom/stagnationlab/sk/a/a$30;
.super Ljava/lang/Object;
.source "Api.java"

# interfaces
.implements Lee/cyber/smartid/inter/GetDeviceFingerprintListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/stagnationlab/sk/a/a;->a(Lcom/stagnationlab/sk/a/a/n;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/stagnationlab/sk/a/a/n;

.field final synthetic b:Lcom/stagnationlab/sk/a/a;


# direct methods
.method constructor <init>(Lcom/stagnationlab/sk/a/a;Lcom/stagnationlab/sk/a/a/n;)V
    .locals 0

    .line 1381
    iput-object p1, p0, Lcom/stagnationlab/sk/a/a$30;->b:Lcom/stagnationlab/sk/a/a;

    iput-object p2, p0, Lcom/stagnationlab/sk/a/a$30;->a:Lcom/stagnationlab/sk/a/a/n;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onGetDeviceFingerprintFailed(Ljava/lang/String;Lee/cyber/smartid/dto/SmartIdError;)V
    .locals 1

    .line 1389
    new-instance p1, Lcom/stagnationlab/sk/d/b;

    invoke-direct {p1, p2}, Lcom/stagnationlab/sk/d/b;-><init>(Lee/cyber/smartid/dto/SmartIdError;)V

    const-string p2, "Api"

    const-string v0, "Get device fingerprint failed"

    invoke-static {p2, v0, p1}, Lcom/stagnationlab/sk/f;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1390
    iget-object p1, p0, Lcom/stagnationlab/sk/a/a$30;->a:Lcom/stagnationlab/sk/a/a/n;

    const/4 p2, 0x0

    invoke-interface {p1, p2}, Lcom/stagnationlab/sk/a/a/n;->a(Ljava/lang/String;)V

    return-void
.end method

.method public onGetDeviceFingerprintSuccess(Ljava/lang/String;Lee/cyber/smartid/dto/jsonrpc/resp/GetDeviceFingerprintResp;)V
    .locals 0

    .line 1384
    iget-object p1, p0, Lcom/stagnationlab/sk/a/a$30;->a:Lcom/stagnationlab/sk/a/a/n;

    invoke-virtual {p2}, Lee/cyber/smartid/dto/jsonrpc/resp/GetDeviceFingerprintResp;->getDeviceFingerprint()Ljava/lang/String;

    move-result-object p2

    invoke-interface {p1, p2}, Lcom/stagnationlab/sk/a/a/n;->a(Ljava/lang/String;)V

    return-void
.end method
