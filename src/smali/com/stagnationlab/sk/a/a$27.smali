.class Lcom/stagnationlab/sk/a/a$27;
.super Ljava/lang/Object;
.source "Api.java"

# interfaces
.implements Lee/cyber/smartid/inter/GetSafetyNetAttestationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/stagnationlab/sk/a/a;->a(ZLcom/stagnationlab/sk/a/a/f;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/stagnationlab/sk/a/a/f;

.field final synthetic b:Lcom/stagnationlab/sk/a/a;


# direct methods
.method constructor <init>(Lcom/stagnationlab/sk/a/a;Lcom/stagnationlab/sk/a/a/f;)V
    .locals 0

    .line 1311
    iput-object p1, p0, Lcom/stagnationlab/sk/a/a$27;->b:Lcom/stagnationlab/sk/a/a;

    iput-object p2, p0, Lcom/stagnationlab/sk/a/a$27;->a:Lcom/stagnationlab/sk/a/a/f;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onGetSafetyNetAttestationFailed(Ljava/lang/String;Lee/cyber/smartid/dto/SmartIdError;)V
    .locals 2

    .line 1325
    iget-object p1, p0, Lcom/stagnationlab/sk/a/a$27;->b:Lcom/stagnationlab/sk/a/a;

    const-string v0, "SafetyNet attestation failed"

    invoke-static {p1, v0, p2}, Lcom/stagnationlab/sk/a/a;->a(Lcom/stagnationlab/sk/a/a;Ljava/lang/String;Lee/cyber/smartid/dto/SmartIdError;)V

    .line 1327
    iget-object p1, p0, Lcom/stagnationlab/sk/a/a$27;->a:Lcom/stagnationlab/sk/a/a/f;

    new-instance v0, Lcom/stagnationlab/sk/c/a;

    const-string v1, "getSafetyNetAttestation"

    invoke-direct {v0, p2, v1}, Lcom/stagnationlab/sk/c/a;-><init>(Lee/cyber/smartid/dto/SmartIdError;Ljava/lang/String;)V

    invoke-interface {p1, v0}, Lcom/stagnationlab/sk/a/a/f;->a(Lcom/stagnationlab/sk/c/a;)V

    return-void
.end method

.method public onGetSafetyNetAttestationSuccess(Ljava/lang/String;Lee/cyber/smartid/dto/jsonrpc/resp/GetSafetyNetAttestationResp;)V
    .locals 1

    .line 1314
    invoke-virtual {p2}, Lee/cyber/smartid/dto/jsonrpc/resp/GetSafetyNetAttestationResp;->getAttestation()Lee/cyber/smartid/dto/SafetyNetAttestation;

    move-result-object p1

    invoke-virtual {p1}, Lee/cyber/smartid/dto/SafetyNetAttestation;->getPayloadSummary()Lee/cyber/smartid/dto/jsonrpc/resp/SafetyNetAttestationSummary;

    move-result-object p1

    if-eqz p1, :cond_1

    .line 1316
    invoke-virtual {p1}, Lee/cyber/smartid/dto/jsonrpc/resp/SafetyNetAttestationSummary;->isAttestationDataPresent()Z

    move-result p2

    if-nez p2, :cond_0

    goto :goto_0

    .line 1319
    :cond_0
    iget-object p2, p0, Lcom/stagnationlab/sk/a/a$27;->a:Lcom/stagnationlab/sk/a/a/f;

    invoke-virtual {p1}, Lee/cyber/smartid/dto/jsonrpc/resp/SafetyNetAttestationSummary;->isBasicIntegrityVerified()Z

    move-result v0

    invoke-virtual {p1}, Lee/cyber/smartid/dto/jsonrpc/resp/SafetyNetAttestationSummary;->isCtsProfileMatch()Z

    move-result p1

    invoke-interface {p2, v0, p1}, Lcom/stagnationlab/sk/a/a/f;->a(ZZ)V

    goto :goto_1

    .line 1317
    :cond_1
    :goto_0
    iget-object p1, p0, Lcom/stagnationlab/sk/a/a$27;->a:Lcom/stagnationlab/sk/a/a/f;

    new-instance p2, Lcom/stagnationlab/sk/c/a;

    sget-object v0, Lcom/stagnationlab/sk/c/b;->d:Lcom/stagnationlab/sk/c/b;

    invoke-direct {p2, v0}, Lcom/stagnationlab/sk/c/a;-><init>(Lcom/stagnationlab/sk/c/b;)V

    invoke-interface {p1, p2}, Lcom/stagnationlab/sk/a/a/f;->a(Lcom/stagnationlab/sk/c/a;)V

    :goto_1
    return-void
.end method
