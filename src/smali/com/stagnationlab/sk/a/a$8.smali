.class Lcom/stagnationlab/sk/a/a$8;
.super Ljava/lang/Object;
.source "Api.java"

# interfaces
.implements Lcom/stagnationlab/sk/a/a/e;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/stagnationlab/sk/a/a;->a(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/stagnationlab/sk/a/a/i;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:Ljava/lang/String;

.field final synthetic c:Ljava/lang/String;

.field final synthetic d:Lcom/stagnationlab/sk/a/a/i;

.field final synthetic e:Ljava/lang/String;

.field final synthetic f:Ljava/lang/String;

.field final synthetic g:Lcom/stagnationlab/sk/a/a;


# direct methods
.method constructor <init>(Lcom/stagnationlab/sk/a/a;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/stagnationlab/sk/a/a/i;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 657
    iput-object p1, p0, Lcom/stagnationlab/sk/a/a$8;->g:Lcom/stagnationlab/sk/a/a;

    iput-object p2, p0, Lcom/stagnationlab/sk/a/a$8;->a:Ljava/lang/String;

    iput-object p3, p0, Lcom/stagnationlab/sk/a/a$8;->b:Ljava/lang/String;

    iput-object p4, p0, Lcom/stagnationlab/sk/a/a$8;->c:Ljava/lang/String;

    iput-object p5, p0, Lcom/stagnationlab/sk/a/a$8;->d:Lcom/stagnationlab/sk/a/a/i;

    iput-object p6, p0, Lcom/stagnationlab/sk/a/a$8;->e:Ljava/lang/String;

    iput-object p7, p0, Lcom/stagnationlab/sk/a/a$8;->f:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 7

    .line 671
    iget-object v0, p0, Lcom/stagnationlab/sk/a/a$8;->g:Lcom/stagnationlab/sk/a/a;

    iget-object v2, p0, Lcom/stagnationlab/sk/a/a$8;->b:Ljava/lang/String;

    iget-object v3, p0, Lcom/stagnationlab/sk/a/a$8;->c:Ljava/lang/String;

    iget-object v4, p0, Lcom/stagnationlab/sk/a/a$8;->e:Ljava/lang/String;

    iget-object v5, p0, Lcom/stagnationlab/sk/a/a$8;->f:Ljava/lang/String;

    iget-object v6, p0, Lcom/stagnationlab/sk/a/a$8;->d:Lcom/stagnationlab/sk/a/a/i;

    const/4 v1, 0x0

    invoke-virtual/range {v0 .. v6}, Lcom/stagnationlab/sk/a/a;->a(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/stagnationlab/sk/a/a/i;)V

    return-void
.end method

.method public a(Lcom/stagnationlab/sk/c/a;)V
    .locals 1

    .line 683
    iget-object v0, p0, Lcom/stagnationlab/sk/a/a$8;->d:Lcom/stagnationlab/sk/a/a/i;

    invoke-interface {v0, p1}, Lcom/stagnationlab/sk/a/a/i;->a(Lcom/stagnationlab/sk/c/a;)V

    return-void
.end method

.method public a(Lee/cyber/smartid/dto/jsonrpc/resp/CheckLocalPendingStateResp;)V
    .locals 8

    .line 660
    iget-object v0, p0, Lcom/stagnationlab/sk/a/a$8;->g:Lcom/stagnationlab/sk/a/a;

    invoke-static {v0}, Lcom/stagnationlab/sk/a/a;->f(Lcom/stagnationlab/sk/a/a;)Lee/cyber/smartid/SmartIdService;

    move-result-object v0

    iget-object v1, p0, Lcom/stagnationlab/sk/a/a$8;->g:Lcom/stagnationlab/sk/a/a;

    .line 661
    invoke-static {v1}, Lcom/stagnationlab/sk/a/a;->e(Lcom/stagnationlab/sk/a/a;)Ljava/lang/String;

    move-result-object v1

    .line 662
    invoke-virtual {p1}, Lee/cyber/smartid/dto/jsonrpc/resp/CheckLocalPendingStateResp;->getLocalPendingStateReferenceId()Ljava/lang/String;

    move-result-object p1

    iget-object v2, p0, Lcom/stagnationlab/sk/a/a$8;->g:Lcom/stagnationlab/sk/a/a;

    iget-object v3, p0, Lcom/stagnationlab/sk/a/a$8;->a:Ljava/lang/String;

    iget-object v4, p0, Lcom/stagnationlab/sk/a/a$8;->b:Ljava/lang/String;

    iget-object v5, p0, Lcom/stagnationlab/sk/a/a$8;->c:Ljava/lang/String;

    iget-object v6, p0, Lcom/stagnationlab/sk/a/a$8;->d:Lcom/stagnationlab/sk/a/a/i;

    const-string v7, "retryLocalPendingState"

    .line 663
    invoke-static/range {v2 .. v7}, Lcom/stagnationlab/sk/a/a;->a(Lcom/stagnationlab/sk/a/a;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/stagnationlab/sk/a/a/i;Ljava/lang/String;)Lee/cyber/smartid/inter/AddAccountListener;

    move-result-object v2

    .line 660
    invoke-virtual {v0, v1, p1, v2}, Lee/cyber/smartid/SmartIdService;->retryLocalPendingState(Ljava/lang/String;Ljava/lang/String;Lee/cyber/smartid/inter/ServiceListener;)V

    return-void
.end method
