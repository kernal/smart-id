.class Lcom/stagnationlab/sk/a/a$33;
.super Ljava/lang/Object;
.source "Api.java"

# interfaces
.implements Lee/cyber/smartid/inter/GetPendingOperationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/stagnationlab/sk/a/a;->a(Lcom/stagnationlab/sk/a/a/o;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/stagnationlab/sk/a/a/o;

.field final synthetic b:Lcom/stagnationlab/sk/a/a;


# direct methods
.method constructor <init>(Lcom/stagnationlab/sk/a/a;Lcom/stagnationlab/sk/a/a/o;)V
    .locals 0

    .line 310
    iput-object p1, p0, Lcom/stagnationlab/sk/a/a$33;->b:Lcom/stagnationlab/sk/a/a;

    iput-object p2, p0, Lcom/stagnationlab/sk/a/a$33;->a:Lcom/stagnationlab/sk/a/a/o;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onGetPendingOperationFailed(Ljava/lang/String;Lee/cyber/smartid/dto/SmartIdError;)V
    .locals 2

    .line 334
    new-instance p1, Lcom/stagnationlab/sk/d/b;

    invoke-direct {p1, p2}, Lcom/stagnationlab/sk/d/b;-><init>(Lee/cyber/smartid/dto/SmartIdError;)V

    const-string v0, "Api"

    const-string v1, "Get Pending operation failed"

    invoke-static {v0, v1, p1}, Lcom/stagnationlab/sk/f;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 335
    new-instance p1, Lcom/stagnationlab/sk/c/a;

    const-string v0, "getPendingOperation"

    invoke-direct {p1, p2, v0}, Lcom/stagnationlab/sk/c/a;-><init>(Lee/cyber/smartid/dto/SmartIdError;Ljava/lang/String;)V

    .line 336
    iget-object p2, p0, Lcom/stagnationlab/sk/a/a$33;->b:Lcom/stagnationlab/sk/a/a;

    invoke-static {p2, p1}, Lcom/stagnationlab/sk/a/a;->b(Lcom/stagnationlab/sk/a/a;Lcom/stagnationlab/sk/c/a;)V

    .line 337
    iget-object p2, p0, Lcom/stagnationlab/sk/a/a$33;->a:Lcom/stagnationlab/sk/a/a/o;

    invoke-interface {p2, p1}, Lcom/stagnationlab/sk/a/a/o;->a(Lcom/stagnationlab/sk/c/a;)V

    return-void
.end method

.method public onGetPendingOperationSuccess(Ljava/lang/String;Lee/cyber/smartid/dto/jsonrpc/resp/GetPendingOperationResp;)V
    .locals 2

    .line 315
    invoke-virtual {p2}, Lee/cyber/smartid/dto/jsonrpc/resp/GetPendingOperationResp;->isTransaction()Z

    move-result p1

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    .line 316
    iget-object p1, p0, Lcom/stagnationlab/sk/a/a$33;->a:Lcom/stagnationlab/sk/a/a/o;

    new-instance v1, Lcom/stagnationlab/sk/models/Transaction;

    .line 317
    invoke-virtual {p2}, Lee/cyber/smartid/dto/jsonrpc/resp/GetPendingOperationResp;->getTransactionResp()Lee/cyber/smartid/dto/jsonrpc/resp/GetTransactionResp;

    move-result-object p2

    invoke-direct {v1, p2}, Lcom/stagnationlab/sk/models/Transaction;-><init>(Lee/cyber/smartid/dto/jsonrpc/resp/GetTransactionResp;)V

    .line 316
    invoke-interface {p1, v1, v0}, Lcom/stagnationlab/sk/a/a/o;->a(Lcom/stagnationlab/sk/models/Transaction;Lcom/stagnationlab/sk/models/RpRequest;)V

    goto :goto_0

    .line 320
    :cond_0
    invoke-virtual {p2}, Lee/cyber/smartid/dto/jsonrpc/resp/GetPendingOperationResp;->isRPRequest()Z

    move-result p1

    if-eqz p1, :cond_1

    .line 321
    iget-object p1, p0, Lcom/stagnationlab/sk/a/a$33;->a:Lcom/stagnationlab/sk/a/a/o;

    new-instance v1, Lcom/stagnationlab/sk/models/RpRequest;

    .line 323
    invoke-virtual {p2}, Lee/cyber/smartid/dto/jsonrpc/resp/GetPendingOperationResp;->getRpRequestResp()Lee/cyber/smartid/dto/jsonrpc/resp/GetRPRequestResp;

    move-result-object p2

    invoke-direct {v1, p2}, Lcom/stagnationlab/sk/models/RpRequest;-><init>(Lee/cyber/smartid/dto/jsonrpc/resp/GetRPRequestResp;)V

    .line 321
    invoke-interface {p1, v0, v1}, Lcom/stagnationlab/sk/a/a/o;->a(Lcom/stagnationlab/sk/models/Transaction;Lcom/stagnationlab/sk/models/RpRequest;)V

    goto :goto_0

    .line 326
    :cond_1
    iget-object p1, p0, Lcom/stagnationlab/sk/a/a$33;->a:Lcom/stagnationlab/sk/a/a/o;

    new-instance p2, Lcom/stagnationlab/sk/c/a;

    sget-object v0, Lcom/stagnationlab/sk/c/b;->N:Lcom/stagnationlab/sk/c/b;

    invoke-direct {p2, v0}, Lcom/stagnationlab/sk/c/a;-><init>(Lcom/stagnationlab/sk/c/b;)V

    invoke-interface {p1, p2}, Lcom/stagnationlab/sk/a/a/o;->a(Lcom/stagnationlab/sk/c/a;)V

    :goto_0
    return-void
.end method
