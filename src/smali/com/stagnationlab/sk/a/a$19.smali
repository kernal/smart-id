.class Lcom/stagnationlab/sk/a/a$19;
.super Ljava/lang/Object;
.source "Api.java"

# interfaces
.implements Lee/cyber/smartid/inter/ValidatePinListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/stagnationlab/sk/a/a;->a(Ljava/lang/String;Lcom/stagnationlab/sk/a/a/z;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/stagnationlab/sk/a/a/z;

.field final synthetic b:Lcom/stagnationlab/sk/a/a;


# direct methods
.method constructor <init>(Lcom/stagnationlab/sk/a/a;Lcom/stagnationlab/sk/a/a/z;)V
    .locals 0

    .line 1025
    iput-object p1, p0, Lcom/stagnationlab/sk/a/a$19;->b:Lcom/stagnationlab/sk/a/a;

    iput-object p2, p0, Lcom/stagnationlab/sk/a/a$19;->a:Lcom/stagnationlab/sk/a/a/z;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onValidatePinFailed(Ljava/lang/String;Lee/cyber/smartid/dto/SmartIdError;)V
    .locals 2

    .line 1037
    new-instance p1, Lcom/stagnationlab/sk/d/b;

    invoke-direct {p1, p2}, Lcom/stagnationlab/sk/d/b;-><init>(Lee/cyber/smartid/dto/SmartIdError;)V

    const-string v0, "Api"

    const-string v1, "Validate pin failed"

    invoke-static {v0, v1, p1}, Lcom/stagnationlab/sk/f;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1038
    iget-object p1, p0, Lcom/stagnationlab/sk/a/a$19;->a:Lcom/stagnationlab/sk/a/a/z;

    new-instance v0, Lcom/stagnationlab/sk/c/a;

    const-string v1, "validatePins"

    invoke-direct {v0, p2, v1}, Lcom/stagnationlab/sk/c/a;-><init>(Lee/cyber/smartid/dto/SmartIdError;Ljava/lang/String;)V

    invoke-interface {p1, v0}, Lcom/stagnationlab/sk/a/a/z;->a(Lcom/stagnationlab/sk/c/a;)V

    return-void
.end method

.method public onValidatePinSuccess(Ljava/lang/String;Lee/cyber/smartid/tse/dto/jsonrpc/resp/ValidatePinResp;)V
    .locals 1

    .line 1028
    invoke-virtual {p2}, Lee/cyber/smartid/tse/dto/jsonrpc/resp/ValidatePinResp;->isValid()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 1029
    iget-object p1, p0, Lcom/stagnationlab/sk/a/a$19;->a:Lcom/stagnationlab/sk/a/a/z;

    invoke-interface {p1}, Lcom/stagnationlab/sk/a/a/z;->a()V

    goto :goto_0

    .line 1031
    :cond_0
    iget-object p1, p0, Lcom/stagnationlab/sk/a/a$19;->a:Lcom/stagnationlab/sk/a/a/z;

    new-instance p2, Lcom/stagnationlab/sk/c/a;

    sget-object v0, Lcom/stagnationlab/sk/c/b;->s:Lcom/stagnationlab/sk/c/b;

    invoke-direct {p2, v0}, Lcom/stagnationlab/sk/c/a;-><init>(Lcom/stagnationlab/sk/c/b;)V

    invoke-interface {p1, p2}, Lcom/stagnationlab/sk/a/a/z;->a(Lcom/stagnationlab/sk/c/a;)V

    :goto_0
    return-void
.end method
