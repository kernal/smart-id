.class Lcom/stagnationlab/sk/a/a$6;
.super Ljava/lang/Object;
.source "Api.java"

# interfaces
.implements Lee/cyber/smartid/inter/GenerateKeysListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/stagnationlab/sk/a/a;->a(Lcom/stagnationlab/sk/a/a/m;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/stagnationlab/sk/a/a/m;

.field final synthetic b:Lcom/stagnationlab/sk/a/a;


# direct methods
.method constructor <init>(Lcom/stagnationlab/sk/a/a;Lcom/stagnationlab/sk/a/a/m;)V
    .locals 0

    .line 568
    iput-object p1, p0, Lcom/stagnationlab/sk/a/a$6;->b:Lcom/stagnationlab/sk/a/a;

    iput-object p2, p0, Lcom/stagnationlab/sk/a/a$6;->a:Lcom/stagnationlab/sk/a/a/m;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onGenerateKeysFailed(Ljava/lang/String;Lee/cyber/smartid/dto/SmartIdError;)V
    .locals 2

    .line 580
    new-instance p1, Lcom/stagnationlab/sk/c/a;

    const-string v0, "generateKeys"

    invoke-direct {p1, p2, v0}, Lcom/stagnationlab/sk/c/a;-><init>(Lee/cyber/smartid/dto/SmartIdError;Ljava/lang/String;)V

    .line 581
    new-instance v0, Lcom/stagnationlab/sk/d/b;

    invoke-direct {v0, p2}, Lcom/stagnationlab/sk/d/b;-><init>(Lee/cyber/smartid/dto/SmartIdError;)V

    const-string p2, "Api"

    const-string v1, "Key generation failed"

    invoke-static {p2, v1, v0}, Lcom/stagnationlab/sk/f;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 586
    iget-object p2, p0, Lcom/stagnationlab/sk/a/a$6;->a:Lcom/stagnationlab/sk/a/a/m;

    invoke-interface {p2, p1}, Lcom/stagnationlab/sk/a/a/m;->a(Lcom/stagnationlab/sk/c/a;)V

    return-void
.end method

.method public onGenerateKeysProgress(Ljava/lang/String;II)V
    .locals 0

    const-string p1, "Api"

    const-string p2, "key generation progress"

    .line 591
    invoke-static {p1, p2}, Lcom/stagnationlab/sk/f;->b(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public onGenerateKeysSuccess(Ljava/lang/String;Lee/cyber/smartid/tse/dto/jsonrpc/resp/GenerateKeysResp;)V
    .locals 3

    const-string p1, "Api"

    const-string v0, "key generation success"

    .line 571
    invoke-static {p1, v0}, Lcom/stagnationlab/sk/f;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 572
    iget-object p1, p0, Lcom/stagnationlab/sk/a/a$6;->a:Lcom/stagnationlab/sk/a/a/m;

    .line 573
    invoke-virtual {p2}, Lee/cyber/smartid/tse/dto/jsonrpc/resp/GenerateKeysResp;->getKeyRefs()Ljava/util/ArrayList;

    move-result-object v0

    .line 574
    invoke-virtual {p2}, Lee/cyber/smartid/tse/dto/jsonrpc/resp/GenerateKeysResp;->getTimeTakenMS()J

    move-result-wide v1

    .line 572
    invoke-interface {p1, v0, v1, v2}, Lcom/stagnationlab/sk/a/a/m;->a(Ljava/util/List;J)V

    return-void
.end method
