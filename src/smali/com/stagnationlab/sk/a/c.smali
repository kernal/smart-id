.class Lcom/stagnationlab/sk/a/c;
.super Ljava/lang/Object;
.source "InitialUpdater.java"


# instance fields
.field private a:Lcom/stagnationlab/sk/models/Account;

.field private b:Z

.field private c:Z

.field private d:Lcom/stagnationlab/sk/c/a;

.field private e:Lcom/stagnationlab/sk/a/a/y;


# direct methods
.method constructor <init>(Lcom/stagnationlab/sk/a/a;Landroid/app/Activity;ZLcom/stagnationlab/sk/a/a/y;)V
    .locals 1

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 22
    iput-boolean v0, p0, Lcom/stagnationlab/sk/a/c;->b:Z

    .line 23
    iput-boolean v0, p0, Lcom/stagnationlab/sk/a/c;->c:Z

    .line 24
    iput-object p4, p0, Lcom/stagnationlab/sk/a/c;->e:Lcom/stagnationlab/sk/a/a/y;

    .line 25
    invoke-virtual {p1}, Lcom/stagnationlab/sk/a/a;->b()Lcom/stagnationlab/sk/models/Account;

    move-result-object v0

    iput-object v0, p0, Lcom/stagnationlab/sk/a/c;->a:Lcom/stagnationlab/sk/models/Account;

    .line 27
    iget-object v0, p0, Lcom/stagnationlab/sk/a/c;->a:Lcom/stagnationlab/sk/models/Account;

    if-nez v0, :cond_0

    .line 28
    new-instance p1, Lcom/stagnationlab/sk/c/a;

    sget-object p2, Lcom/stagnationlab/sk/c/b;->f:Lcom/stagnationlab/sk/c/b;

    invoke-direct {p1, p2}, Lcom/stagnationlab/sk/c/a;-><init>(Lcom/stagnationlab/sk/c/b;)V

    invoke-interface {p4, p1}, Lcom/stagnationlab/sk/a/a/y;->a(Lcom/stagnationlab/sk/c/a;)V

    return-void

    :cond_0
    if-eqz p3, :cond_1

    const/4 p1, 0x0

    .line 37
    invoke-direct {p0, p1}, Lcom/stagnationlab/sk/a/c;->a(Lcom/stagnationlab/sk/c/a;)V

    goto :goto_0

    :cond_1
    const/4 p3, 0x1

    .line 39
    new-instance p4, Lcom/stagnationlab/sk/a/c$1;

    invoke-direct {p4, p0}, Lcom/stagnationlab/sk/a/c$1;-><init>(Lcom/stagnationlab/sk/a/c;)V

    invoke-virtual {p1, p3, p4}, Lcom/stagnationlab/sk/a/a;->a(ZLcom/stagnationlab/sk/a/a/y;)V

    .line 52
    :goto_0
    new-instance p1, Lcom/stagnationlab/sk/a/c$2;

    invoke-direct {p1, p0}, Lcom/stagnationlab/sk/a/c$2;-><init>(Lcom/stagnationlab/sk/a/c;)V

    invoke-static {p2, p1}, Lcom/stagnationlab/sk/fcm/b;->a(Landroid/app/Activity;Lcom/stagnationlab/sk/fcm/d;)V

    return-void
.end method

.method private a()V
    .locals 2

    .line 75
    iget-boolean v0, p0, Lcom/stagnationlab/sk/a/c;->b:Z

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/stagnationlab/sk/a/c;->c:Z

    if-nez v0, :cond_0

    goto :goto_0

    .line 79
    :cond_0
    iget-object v0, p0, Lcom/stagnationlab/sk/a/c;->d:Lcom/stagnationlab/sk/c/a;

    if-eqz v0, :cond_1

    .line 80
    iget-object v1, p0, Lcom/stagnationlab/sk/a/c;->e:Lcom/stagnationlab/sk/a/a/y;

    invoke-interface {v1, v0}, Lcom/stagnationlab/sk/a/a/y;->a(Lcom/stagnationlab/sk/c/a;)V

    goto :goto_0

    .line 82
    :cond_1
    iget-object v0, p0, Lcom/stagnationlab/sk/a/c;->e:Lcom/stagnationlab/sk/a/a/y;

    invoke-interface {v0}, Lcom/stagnationlab/sk/a/a/y;->a()V

    :cond_2
    :goto_0
    return-void
.end method

.method static synthetic a(Lcom/stagnationlab/sk/a/c;Lcom/stagnationlab/sk/c/a;)V
    .locals 0

    .line 12
    invoke-direct {p0, p1}, Lcom/stagnationlab/sk/a/c;->a(Lcom/stagnationlab/sk/c/a;)V

    return-void
.end method

.method static synthetic a(Lcom/stagnationlab/sk/a/c;Z)V
    .locals 0

    .line 12
    invoke-direct {p0, p1}, Lcom/stagnationlab/sk/a/c;->a(Z)V

    return-void
.end method

.method private a(Lcom/stagnationlab/sk/c/a;)V
    .locals 0

    .line 68
    iput-object p1, p0, Lcom/stagnationlab/sk/a/c;->d:Lcom/stagnationlab/sk/c/a;

    const/4 p1, 0x1

    .line 69
    iput-boolean p1, p0, Lcom/stagnationlab/sk/a/c;->b:Z

    .line 71
    invoke-direct {p0}, Lcom/stagnationlab/sk/a/c;->a()V

    return-void
.end method

.method private a(Z)V
    .locals 1

    .line 61
    iget-object v0, p0, Lcom/stagnationlab/sk/a/c;->a:Lcom/stagnationlab/sk/models/Account;

    invoke-virtual {v0, p1}, Lcom/stagnationlab/sk/models/Account;->b(Z)V

    const/4 p1, 0x1

    .line 62
    iput-boolean p1, p0, Lcom/stagnationlab/sk/a/c;->c:Z

    .line 64
    invoke-direct {p0}, Lcom/stagnationlab/sk/a/c;->a()V

    return-void
.end method
