.class Lcom/stagnationlab/sk/a/a$16;
.super Ljava/lang/Object;
.source "Api.java"

# interfaces
.implements Lee/cyber/smartid/inter/StorePushNotificationDataListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/stagnationlab/sk/a/a;->a(Ljava/lang/String;Lcom/stagnationlab/sk/a/a/w;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/stagnationlab/sk/a/a/w;

.field final synthetic b:Lcom/stagnationlab/sk/a/a;


# direct methods
.method constructor <init>(Lcom/stagnationlab/sk/a/a;Lcom/stagnationlab/sk/a/a/w;)V
    .locals 0

    .line 940
    iput-object p1, p0, Lcom/stagnationlab/sk/a/a$16;->b:Lcom/stagnationlab/sk/a/a;

    iput-object p2, p0, Lcom/stagnationlab/sk/a/a$16;->a:Lcom/stagnationlab/sk/a/a/w;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onStorePushNotificationDataFailed(Ljava/lang/String;Lee/cyber/smartid/dto/SmartIdError;)V
    .locals 2

    .line 960
    new-instance p1, Lcom/stagnationlab/sk/d/b;

    invoke-direct {p1, p2}, Lcom/stagnationlab/sk/d/b;-><init>(Lee/cyber/smartid/dto/SmartIdError;)V

    const-string v0, "Api"

    const-string v1, "Store push notification data failed"

    invoke-static {v0, v1, p1}, Lcom/stagnationlab/sk/f;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 962
    iget-object p1, p0, Lcom/stagnationlab/sk/a/a$16;->b:Lcom/stagnationlab/sk/a/a;

    const/4 v0, 0x0

    invoke-static {p1, v0}, Lcom/stagnationlab/sk/a/a;->a(Lcom/stagnationlab/sk/a/a;Z)V

    .line 964
    iget-object p1, p0, Lcom/stagnationlab/sk/a/a$16;->a:Lcom/stagnationlab/sk/a/a/w;

    new-instance v0, Lcom/stagnationlab/sk/c/a;

    const-string v1, "storePushNotificationData"

    invoke-direct {v0, p2, v1}, Lcom/stagnationlab/sk/c/a;-><init>(Lee/cyber/smartid/dto/SmartIdError;Ljava/lang/String;)V

    invoke-interface {p1, v0}, Lcom/stagnationlab/sk/a/a/w;->a(Lcom/stagnationlab/sk/c/a;)V

    return-void
.end method

.method public onStorePushNotificationDataSuccess(Ljava/lang/String;Lee/cyber/smartid/dto/jsonrpc/resp/StorePushNotificationResp;)V
    .locals 1

    const-string p1, "Api"

    const-string v0, "Push data stored"

    .line 945
    invoke-static {p1, v0}, Lcom/stagnationlab/sk/f;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 947
    iget-object p1, p0, Lcom/stagnationlab/sk/a/a$16;->b:Lcom/stagnationlab/sk/a/a;

    invoke-static {p1}, Lcom/stagnationlab/sk/a/a;->c(Lcom/stagnationlab/sk/a/a;)Lcom/stagnationlab/sk/h/d;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 948
    iget-object p1, p0, Lcom/stagnationlab/sk/a/a$16;->b:Lcom/stagnationlab/sk/a/a;

    invoke-static {p1}, Lcom/stagnationlab/sk/a/a;->c(Lcom/stagnationlab/sk/a/a;)Lcom/stagnationlab/sk/h/d;

    move-result-object p1

    invoke-virtual {p2}, Lee/cyber/smartid/dto/jsonrpc/resp/StorePushNotificationResp;->getToken()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/stagnationlab/sk/h/d;->b(Ljava/lang/String;)V

    .line 951
    :cond_0
    iget-object p1, p0, Lcom/stagnationlab/sk/a/a$16;->b:Lcom/stagnationlab/sk/a/a;

    invoke-static {p1}, Lcom/stagnationlab/sk/a/a;->d(Lcom/stagnationlab/sk/a/a;)Lcom/stagnationlab/sk/models/Account;

    move-result-object p1

    if-eqz p1, :cond_1

    .line 952
    iget-object p1, p0, Lcom/stagnationlab/sk/a/a$16;->b:Lcom/stagnationlab/sk/a/a;

    invoke-static {p1}, Lcom/stagnationlab/sk/a/a;->d(Lcom/stagnationlab/sk/a/a;)Lcom/stagnationlab/sk/models/Account;

    move-result-object p1

    const/4 p2, 0x1

    invoke-virtual {p1, p2}, Lcom/stagnationlab/sk/models/Account;->b(Z)V

    .line 955
    :cond_1
    iget-object p1, p0, Lcom/stagnationlab/sk/a/a$16;->a:Lcom/stagnationlab/sk/a/a/w;

    invoke-interface {p1}, Lcom/stagnationlab/sk/a/a/w;->a()V

    return-void
.end method
