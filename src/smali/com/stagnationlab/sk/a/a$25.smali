.class Lcom/stagnationlab/sk/a/a$25;
.super Ljava/lang/Object;
.source "Api.java"

# interfaces
.implements Lee/cyber/smartid/inter/SystemEventListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/stagnationlab/sk/a/a;->a(Landroid/app/Activity;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Landroid/app/Activity;

.field final synthetic b:Lcom/stagnationlab/sk/a/a;


# direct methods
.method constructor <init>(Lcom/stagnationlab/sk/a/a;Landroid/app/Activity;)V
    .locals 0

    .line 1254
    iput-object p1, p0, Lcom/stagnationlab/sk/a/a$25;->b:Lcom/stagnationlab/sk/a/a;

    iput-object p2, p0, Lcom/stagnationlab/sk/a/a$25;->a:Landroid/app/Activity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onSystemErrorEvent(Ljava/lang/String;Lee/cyber/smartid/dto/SmartIdError;)V
    .locals 2

    .line 1257
    new-instance p1, Lcom/stagnationlab/sk/d/b;

    invoke-direct {p1, p2}, Lcom/stagnationlab/sk/d/b;-><init>(Lee/cyber/smartid/dto/SmartIdError;)V

    const-string v0, "Api"

    const-string v1, "Received system error"

    invoke-static {v0, v1, p1}, Lcom/stagnationlab/sk/f;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1258
    new-instance p1, Lcom/stagnationlab/sk/c/a;

    const-string v0, "addSystemEventListener"

    invoke-direct {p1, p2, v0}, Lcom/stagnationlab/sk/c/a;-><init>(Lee/cyber/smartid/dto/SmartIdError;Ljava/lang/String;)V

    .line 1260
    invoke-virtual {p1}, Lcom/stagnationlab/sk/c/a;->a()Lcom/stagnationlab/sk/c/b;

    move-result-object p2

    sget-object v0, Lcom/stagnationlab/sk/c/b;->q:Lcom/stagnationlab/sk/c/b;

    if-ne p2, v0, :cond_0

    .line 1261
    new-instance p2, Landroid/content/Intent;

    iget-object v0, p0, Lcom/stagnationlab/sk/a/a$25;->a:Landroid/app/Activity;

    const-class v1, Lcom/stagnationlab/sk/ErrorActivity;

    invoke-direct {p2, v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v0, "error"

    .line 1262
    invoke-virtual {p2, v0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1263
    iget-object p1, p0, Lcom/stagnationlab/sk/a/a$25;->a:Landroid/app/Activity;

    invoke-virtual {p1, p2}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 1264
    iget-object p1, p0, Lcom/stagnationlab/sk/a/a$25;->a:Landroid/app/Activity;

    invoke-static {p1}, Lcom/stagnationlab/sk/util/n;->a(Landroid/app/Activity;)V

    .line 1265
    iget-object p1, p0, Lcom/stagnationlab/sk/a/a$25;->a:Landroid/app/Activity;

    invoke-virtual {p1}, Landroid/app/Activity;->finish()V

    :cond_0
    return-void
.end method
