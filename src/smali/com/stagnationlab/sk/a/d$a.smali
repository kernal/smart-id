.class final enum Lcom/stagnationlab/sk/a/d$a;
.super Ljava/lang/Enum;
.source "Storage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/stagnationlab/sk/a/d;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/stagnationlab/sk/a/d$a;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/stagnationlab/sk/a/d$a;

.field public static final enum b:Lcom/stagnationlab/sk/a/d$a;

.field public static final enum c:Lcom/stagnationlab/sk/a/d$a;

.field public static final enum d:Lcom/stagnationlab/sk/a/d$a;

.field public static final enum e:Lcom/stagnationlab/sk/a/d$a;

.field public static final enum f:Lcom/stagnationlab/sk/a/d$a;

.field public static final enum g:Lcom/stagnationlab/sk/a/d$a;

.field public static final enum h:Lcom/stagnationlab/sk/a/d$a;

.field public static final enum i:Lcom/stagnationlab/sk/a/d$a;

.field private static final synthetic j:[Lcom/stagnationlab/sk/a/d$a;


# direct methods
.method static constructor <clinit>()V
    .locals 11

    .line 30
    new-instance v0, Lcom/stagnationlab/sk/a/d$a;

    const/4 v1, 0x0

    const-string v2, "accountHasPushToken"

    invoke-direct {v0, v2, v1}, Lcom/stagnationlab/sk/a/d$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/stagnationlab/sk/a/d$a;->a:Lcom/stagnationlab/sk/a/d$a;

    .line 31
    new-instance v0, Lcom/stagnationlab/sk/a/d$a;

    const/4 v2, 0x1

    const-string v3, "hasSeenSuccess"

    invoke-direct {v0, v3, v2}, Lcom/stagnationlab/sk/a/d$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/stagnationlab/sk/a/d$a;->b:Lcom/stagnationlab/sk/a/d$a;

    .line 32
    new-instance v0, Lcom/stagnationlab/sk/a/d$a;

    const/4 v3, 0x2

    const-string v4, "showNotificationInfoAfterManualTransaction"

    invoke-direct {v0, v4, v3}, Lcom/stagnationlab/sk/a/d$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/stagnationlab/sk/a/d$a;->c:Lcom/stagnationlab/sk/a/d$a;

    .line 33
    new-instance v0, Lcom/stagnationlab/sk/a/d$a;

    const/4 v4, 0x3

    const-string v5, "locale"

    invoke-direct {v0, v5, v4}, Lcom/stagnationlab/sk/a/d$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/stagnationlab/sk/a/d$a;->d:Lcom/stagnationlab/sk/a/d$a;

    .line 34
    new-instance v0, Lcom/stagnationlab/sk/a/d$a;

    const/4 v5, 0x4

    const-string v6, "legalGuardians"

    invoke-direct {v0, v6, v5}, Lcom/stagnationlab/sk/a/d$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/stagnationlab/sk/a/d$a;->e:Lcom/stagnationlab/sk/a/d$a;

    .line 35
    new-instance v0, Lcom/stagnationlab/sk/a/d$a;

    const/4 v6, 0x5

    const-string v7, "pushBehaviour"

    invoke-direct {v0, v7, v6}, Lcom/stagnationlab/sk/a/d$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/stagnationlab/sk/a/d$a;->f:Lcom/stagnationlab/sk/a/d$a;

    .line 36
    new-instance v0, Lcom/stagnationlab/sk/a/d$a;

    const/4 v7, 0x6

    const-string v8, "notificationSound"

    invoke-direct {v0, v8, v7}, Lcom/stagnationlab/sk/a/d$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/stagnationlab/sk/a/d$a;->g:Lcom/stagnationlab/sk/a/d$a;

    .line 37
    new-instance v0, Lcom/stagnationlab/sk/a/d$a;

    const/4 v8, 0x7

    const-string v9, "isRegistrationInProgress"

    invoke-direct {v0, v9, v8}, Lcom/stagnationlab/sk/a/d$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/stagnationlab/sk/a/d$a;->h:Lcom/stagnationlab/sk/a/d$a;

    .line 38
    new-instance v0, Lcom/stagnationlab/sk/a/d$a;

    const/16 v9, 0x8

    const-string v10, "allowScreenshots"

    invoke-direct {v0, v10, v9}, Lcom/stagnationlab/sk/a/d$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/stagnationlab/sk/a/d$a;->i:Lcom/stagnationlab/sk/a/d$a;

    const/16 v0, 0x9

    .line 29
    new-array v0, v0, [Lcom/stagnationlab/sk/a/d$a;

    sget-object v10, Lcom/stagnationlab/sk/a/d$a;->a:Lcom/stagnationlab/sk/a/d$a;

    aput-object v10, v0, v1

    sget-object v1, Lcom/stagnationlab/sk/a/d$a;->b:Lcom/stagnationlab/sk/a/d$a;

    aput-object v1, v0, v2

    sget-object v1, Lcom/stagnationlab/sk/a/d$a;->c:Lcom/stagnationlab/sk/a/d$a;

    aput-object v1, v0, v3

    sget-object v1, Lcom/stagnationlab/sk/a/d$a;->d:Lcom/stagnationlab/sk/a/d$a;

    aput-object v1, v0, v4

    sget-object v1, Lcom/stagnationlab/sk/a/d$a;->e:Lcom/stagnationlab/sk/a/d$a;

    aput-object v1, v0, v5

    sget-object v1, Lcom/stagnationlab/sk/a/d$a;->f:Lcom/stagnationlab/sk/a/d$a;

    aput-object v1, v0, v6

    sget-object v1, Lcom/stagnationlab/sk/a/d$a;->g:Lcom/stagnationlab/sk/a/d$a;

    aput-object v1, v0, v7

    sget-object v1, Lcom/stagnationlab/sk/a/d$a;->h:Lcom/stagnationlab/sk/a/d$a;

    aput-object v1, v0, v8

    sget-object v1, Lcom/stagnationlab/sk/a/d$a;->i:Lcom/stagnationlab/sk/a/d$a;

    aput-object v1, v0, v9

    sput-object v0, Lcom/stagnationlab/sk/a/d$a;->j:[Lcom/stagnationlab/sk/a/d$a;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 29
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/stagnationlab/sk/a/d$a;
    .locals 1

    .line 29
    const-class v0, Lcom/stagnationlab/sk/a/d$a;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/stagnationlab/sk/a/d$a;

    return-object p0
.end method

.method public static values()[Lcom/stagnationlab/sk/a/d$a;
    .locals 1

    .line 29
    sget-object v0, Lcom/stagnationlab/sk/a/d$a;->j:[Lcom/stagnationlab/sk/a/d$a;

    invoke-virtual {v0}, [Lcom/stagnationlab/sk/a/d$a;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/stagnationlab/sk/a/d$a;

    return-object v0
.end method
