.class Lcom/stagnationlab/sk/a/a$31;
.super Ljava/lang/Object;
.source "Api.java"

# interfaces
.implements Lee/cyber/smartid/inter/DeleteAccountListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/stagnationlab/sk/a/a;->a(Lcom/stagnationlab/sk/a/a/k;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/stagnationlab/sk/models/Account;

.field final synthetic b:Lcom/stagnationlab/sk/a/a/k;

.field final synthetic c:Lcom/stagnationlab/sk/a/a;


# direct methods
.method constructor <init>(Lcom/stagnationlab/sk/a/a;Lcom/stagnationlab/sk/models/Account;Lcom/stagnationlab/sk/a/a/k;)V
    .locals 0

    .line 263
    iput-object p1, p0, Lcom/stagnationlab/sk/a/a$31;->c:Lcom/stagnationlab/sk/a/a;

    iput-object p2, p0, Lcom/stagnationlab/sk/a/a$31;->a:Lcom/stagnationlab/sk/models/Account;

    iput-object p3, p0, Lcom/stagnationlab/sk/a/a$31;->b:Lcom/stagnationlab/sk/a/a/k;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDeleteAccountFailed(Ljava/lang/String;Lee/cyber/smartid/dto/SmartIdError;)V
    .locals 2

    .line 275
    new-instance p1, Lcom/stagnationlab/sk/d/b;

    invoke-direct {p1, p2}, Lcom/stagnationlab/sk/d/b;-><init>(Lee/cyber/smartid/dto/SmartIdError;)V

    const-string v0, "Api"

    const-string v1, "Delete account failed"

    invoke-static {v0, v1, p1}, Lcom/stagnationlab/sk/f;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 276
    iget-object p1, p0, Lcom/stagnationlab/sk/a/a$31;->b:Lcom/stagnationlab/sk/a/a/k;

    new-instance v0, Lcom/stagnationlab/sk/c/a;

    const-string v1, "deleteAccount"

    invoke-direct {v0, p2, v1}, Lcom/stagnationlab/sk/c/a;-><init>(Lee/cyber/smartid/dto/SmartIdError;Ljava/lang/String;)V

    invoke-interface {p1, v0}, Lcom/stagnationlab/sk/a/a/k;->a(Lcom/stagnationlab/sk/c/a;)V

    return-void
.end method

.method public onDeleteAccountSuccess(Ljava/lang/String;Lee/cyber/smartid/dto/jsonrpc/resp/DeleteAccountResp;)V
    .locals 0

    .line 266
    iget-object p1, p0, Lcom/stagnationlab/sk/a/a$31;->a:Lcom/stagnationlab/sk/models/Account;

    invoke-virtual {p1}, Lcom/stagnationlab/sk/models/Account;->d()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 267
    sget-object p1, Lcom/stagnationlab/sk/util/i$b;->b:Lcom/stagnationlab/sk/util/i$b;

    invoke-static {p1}, Lcom/stagnationlab/sk/util/i;->a(Lcom/stagnationlab/sk/util/i$b;)V

    .line 269
    :cond_0
    iget-object p1, p0, Lcom/stagnationlab/sk/a/a$31;->c:Lcom/stagnationlab/sk/a/a;

    invoke-virtual {p2}, Lee/cyber/smartid/dto/jsonrpc/resp/DeleteAccountResp;->getAccounts()Ljava/util/ArrayList;

    move-result-object p2

    invoke-static {p1, p2}, Lcom/stagnationlab/sk/a/a;->a(Lcom/stagnationlab/sk/a/a;Ljava/util/List;)V

    .line 270
    iget-object p1, p0, Lcom/stagnationlab/sk/a/a$31;->c:Lcom/stagnationlab/sk/a/a;

    iget-object p2, p0, Lcom/stagnationlab/sk/a/a$31;->b:Lcom/stagnationlab/sk/a/a/k;

    invoke-static {p1, p2}, Lcom/stagnationlab/sk/a/a;->a(Lcom/stagnationlab/sk/a/a;Lcom/stagnationlab/sk/a/a/k;)V

    return-void
.end method
