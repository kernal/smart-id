.class Lcom/stagnationlab/sk/a/a$12;
.super Ljava/lang/Object;
.source "Api.java"

# interfaces
.implements Lee/cyber/smartid/inter/InitServiceListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/stagnationlab/sk/a/a;->k()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/stagnationlab/sk/a/a;


# direct methods
.method constructor <init>(Lcom/stagnationlab/sk/a/a;)V
    .locals 0

    .line 168
    iput-object p1, p0, Lcom/stagnationlab/sk/a/a$12;->a:Lcom/stagnationlab/sk/a/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onInitServiceFailed(Ljava/lang/String;Lee/cyber/smartid/dto/SmartIdError;)V
    .locals 2

    .line 201
    new-instance p1, Lcom/stagnationlab/sk/d/b;

    invoke-direct {p1, p2}, Lcom/stagnationlab/sk/d/b;-><init>(Lee/cyber/smartid/dto/SmartIdError;)V

    const-string v0, "Api"

    const-string v1, "Api initialization failed"

    invoke-static {v0, v1, p1}, Lcom/stagnationlab/sk/f;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 203
    iget-object p1, p0, Lcom/stagnationlab/sk/a/a$12;->a:Lcom/stagnationlab/sk/a/a;

    new-instance v0, Lcom/stagnationlab/sk/c/a;

    const-string v1, "initService"

    invoke-direct {v0, p2, v1}, Lcom/stagnationlab/sk/c/a;-><init>(Lee/cyber/smartid/dto/SmartIdError;Ljava/lang/String;)V

    invoke-static {p1, v0}, Lcom/stagnationlab/sk/a/a;->a(Lcom/stagnationlab/sk/a/a;Lcom/stagnationlab/sk/c/a;)V

    return-void
.end method

.method public onInitServiceInteractiveUpgradeFailed(Ljava/lang/String;ILee/cyber/smartid/dto/SmartIdError;)V
    .locals 1

    .line 191
    new-instance p1, Lcom/stagnationlab/sk/d/b;

    invoke-direct {p1, p3}, Lcom/stagnationlab/sk/d/b;-><init>(Lee/cyber/smartid/dto/SmartIdError;)V

    const-string p2, "Api"

    const-string v0, "Api initialization failed: interactive upgrade failed"

    invoke-static {p2, v0, p1}, Lcom/stagnationlab/sk/f;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 193
    new-instance p1, Lcom/stagnationlab/sk/c/a;

    const-string p2, "initService"

    invoke-direct {p1, p3, p2}, Lcom/stagnationlab/sk/c/a;-><init>(Lee/cyber/smartid/dto/SmartIdError;Ljava/lang/String;)V

    .line 194
    invoke-virtual {p1}, Lcom/stagnationlab/sk/c/a;->m()V

    .line 196
    iget-object p2, p0, Lcom/stagnationlab/sk/a/a$12;->a:Lcom/stagnationlab/sk/a/a;

    invoke-static {p2, p1}, Lcom/stagnationlab/sk/a/a;->a(Lcom/stagnationlab/sk/a/a;Lcom/stagnationlab/sk/c/a;)V

    return-void
.end method

.method public onInitServiceInteractiveUpgradeStarted(Ljava/lang/String;I)V
    .locals 0

    const-string p1, "Api"

    const-string p2, "Interactive upgrade started"

    .line 182
    invoke-static {p1, p2}, Lcom/stagnationlab/sk/f;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 184
    iget-object p1, p0, Lcom/stagnationlab/sk/a/a$12;->a:Lcom/stagnationlab/sk/a/a;

    invoke-static {p1}, Lcom/stagnationlab/sk/a/a;->c(Lcom/stagnationlab/sk/a/a;)Lcom/stagnationlab/sk/h/d;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 185
    iget-object p1, p0, Lcom/stagnationlab/sk/a/a$12;->a:Lcom/stagnationlab/sk/a/a;

    invoke-static {p1}, Lcom/stagnationlab/sk/a/a;->c(Lcom/stagnationlab/sk/a/a;)Lcom/stagnationlab/sk/h/d;

    move-result-object p1

    invoke-virtual {p1}, Lcom/stagnationlab/sk/h/d;->h()V

    :cond_0
    return-void
.end method

.method public onInitServiceSuccess(Ljava/lang/String;[Lee/cyber/smartid/dto/SmartIdError;)V
    .locals 1

    .line 171
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "API initialized. Warnings: "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    if-nez p2, :cond_0

    const-string p2, "none"

    goto :goto_0

    .line 174
    :cond_0
    invoke-static {p2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p2

    :goto_0
    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string p2, "Api"

    .line 171
    invoke-static {p2, p1}, Lcom/stagnationlab/sk/f;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 177
    iget-object p1, p0, Lcom/stagnationlab/sk/a/a$12;->a:Lcom/stagnationlab/sk/a/a;

    invoke-static {p1}, Lcom/stagnationlab/sk/a/a;->b(Lcom/stagnationlab/sk/a/a;)V

    return-void
.end method
