.class public final enum Lcom/stagnationlab/sk/b/b;
.super Ljava/lang/Enum;
.source "PushBehaviour.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/stagnationlab/sk/b/b;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/stagnationlab/sk/b/b;

.field public static final enum b:Lcom/stagnationlab/sk/b/b;

.field public static final enum c:Lcom/stagnationlab/sk/b/b;

.field private static final d:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray<",
            "Lcom/stagnationlab/sk/b/b;",
            ">;"
        }
    .end annotation
.end field

.field private static final synthetic f:[Lcom/stagnationlab/sk/b/b;


# instance fields
.field private final e:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .line 6
    new-instance v0, Lcom/stagnationlab/sk/b/b;

    const/4 v1, 0x0

    const-string v2, "DISABLED"

    invoke-direct {v0, v2, v1, v1}, Lcom/stagnationlab/sk/b/b;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/stagnationlab/sk/b/b;->a:Lcom/stagnationlab/sk/b/b;

    .line 7
    new-instance v0, Lcom/stagnationlab/sk/b/b;

    const/4 v2, 0x1

    const-string v3, "NOTIFICATION"

    invoke-direct {v0, v3, v2, v2}, Lcom/stagnationlab/sk/b/b;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/stagnationlab/sk/b/b;->b:Lcom/stagnationlab/sk/b/b;

    .line 8
    new-instance v0, Lcom/stagnationlab/sk/b/b;

    const/4 v3, 0x2

    const-string v4, "OPEN_APP"

    invoke-direct {v0, v4, v3, v3}, Lcom/stagnationlab/sk/b/b;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/stagnationlab/sk/b/b;->c:Lcom/stagnationlab/sk/b/b;

    const/4 v0, 0x3

    .line 5
    new-array v0, v0, [Lcom/stagnationlab/sk/b/b;

    sget-object v4, Lcom/stagnationlab/sk/b/b;->a:Lcom/stagnationlab/sk/b/b;

    aput-object v4, v0, v1

    sget-object v4, Lcom/stagnationlab/sk/b/b;->b:Lcom/stagnationlab/sk/b/b;

    aput-object v4, v0, v2

    sget-object v2, Lcom/stagnationlab/sk/b/b;->c:Lcom/stagnationlab/sk/b/b;

    aput-object v2, v0, v3

    sput-object v0, Lcom/stagnationlab/sk/b/b;->f:[Lcom/stagnationlab/sk/b/b;

    .line 10
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    sput-object v0, Lcom/stagnationlab/sk/b/b;->d:Landroid/util/SparseArray;

    .line 14
    invoke-static {}, Lcom/stagnationlab/sk/b/b;->values()[Lcom/stagnationlab/sk/b/b;

    move-result-object v0

    array-length v2, v0

    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    .line 15
    sget-object v4, Lcom/stagnationlab/sk/b/b;->d:Landroid/util/SparseArray;

    iget v5, v3, Lcom/stagnationlab/sk/b/b;->e:I

    invoke-virtual {v4, v5, v3}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 19
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 20
    iput p3, p0, Lcom/stagnationlab/sk/b/b;->e:I

    return-void
.end method

.method public static a(I)Lcom/stagnationlab/sk/b/b;
    .locals 2

    .line 28
    sget-object v0, Lcom/stagnationlab/sk/b/b;->d:Landroid/util/SparseArray;

    sget-object v1, Lcom/stagnationlab/sk/b/b;->c:Lcom/stagnationlab/sk/b/b;

    invoke-virtual {v0, p0, v1}, Landroid/util/SparseArray;->get(ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/stagnationlab/sk/b/b;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/stagnationlab/sk/b/b;
    .locals 1

    .line 5
    const-class v0, Lcom/stagnationlab/sk/b/b;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/stagnationlab/sk/b/b;

    return-object p0
.end method

.method public static values()[Lcom/stagnationlab/sk/b/b;
    .locals 1

    .line 5
    sget-object v0, Lcom/stagnationlab/sk/b/b;->f:[Lcom/stagnationlab/sk/b/b;

    invoke-virtual {v0}, [Lcom/stagnationlab/sk/b/b;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/stagnationlab/sk/b/b;

    return-object v0
.end method


# virtual methods
.method public a()I
    .locals 1

    .line 24
    iget v0, p0, Lcom/stagnationlab/sk/b/b;->e:I

    return v0
.end method
