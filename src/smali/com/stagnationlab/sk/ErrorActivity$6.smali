.class Lcom/stagnationlab/sk/ErrorActivity$6;
.super Ljava/lang/Object;
.source "ErrorActivity.java"

# interfaces
.implements Lcom/stagnationlab/sk/a/a/a;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/stagnationlab/sk/ErrorActivity;->l()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/stagnationlab/sk/ErrorActivity;


# direct methods
.method constructor <init>(Lcom/stagnationlab/sk/ErrorActivity;)V
    .locals 0

    .line 346
    iput-object p1, p0, Lcom/stagnationlab/sk/ErrorActivity$6;->a:Lcom/stagnationlab/sk/ErrorActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/stagnationlab/sk/a/a;Z)V
    .locals 1

    .line 349
    iget-object p2, p0, Lcom/stagnationlab/sk/ErrorActivity$6;->a:Lcom/stagnationlab/sk/ErrorActivity;

    invoke-static {p2}, Lcom/stagnationlab/sk/ErrorActivity;->c(Lcom/stagnationlab/sk/ErrorActivity;)Lcom/stagnationlab/sk/models/Transaction;

    move-result-object p2

    new-instance v0, Lcom/stagnationlab/sk/ErrorActivity$6$1;

    invoke-direct {v0, p0}, Lcom/stagnationlab/sk/ErrorActivity$6$1;-><init>(Lcom/stagnationlab/sk/ErrorActivity$6;)V

    invoke-virtual {p1, p2, v0}, Lcom/stagnationlab/sk/a/a;->a(Lcom/stagnationlab/sk/models/Transaction;Lcom/stagnationlab/sk/a/a/d;)V

    return-void
.end method

.method public a(Lcom/stagnationlab/sk/c/a;)V
    .locals 2

    .line 366
    new-instance p1, Landroid/content/Intent;

    iget-object v0, p0, Lcom/stagnationlab/sk/ErrorActivity$6;->a:Lcom/stagnationlab/sk/ErrorActivity;

    const-class v1, Lcom/stagnationlab/sk/MainActivity;

    invoke-direct {p1, v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 368
    iget-object v0, p0, Lcom/stagnationlab/sk/ErrorActivity$6;->a:Lcom/stagnationlab/sk/ErrorActivity;

    invoke-virtual {v0, p1}, Lcom/stagnationlab/sk/ErrorActivity;->startActivity(Landroid/content/Intent;)V

    .line 369
    iget-object p1, p0, Lcom/stagnationlab/sk/ErrorActivity$6;->a:Lcom/stagnationlab/sk/ErrorActivity;

    invoke-virtual {p1}, Lcom/stagnationlab/sk/ErrorActivity;->finish()V

    return-void
.end method

.method public b(Lcom/stagnationlab/sk/c/a;)V
    .locals 2

    .line 374
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Failed to cancel transaction: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/stagnationlab/sk/c/a;->b()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string v0, "ErrorActivity"

    invoke-static {v0, p1}, Lcom/stagnationlab/sk/f;->e(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method
