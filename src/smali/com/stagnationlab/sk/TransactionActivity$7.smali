.class Lcom/stagnationlab/sk/TransactionActivity$7;
.super Ljava/lang/Object;
.source "TransactionActivity.java"

# interfaces
.implements Lcom/stagnationlab/sk/a/a/o;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/stagnationlab/sk/TransactionActivity;->a(Lcom/stagnationlab/sk/util/m;Lcom/stagnationlab/sk/TransactionActivity$a;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/stagnationlab/sk/TransactionActivity$a;

.field final synthetic b:Lcom/stagnationlab/sk/TransactionActivity;


# direct methods
.method constructor <init>(Lcom/stagnationlab/sk/TransactionActivity;Lcom/stagnationlab/sk/TransactionActivity$a;)V
    .locals 0

    .line 820
    iput-object p1, p0, Lcom/stagnationlab/sk/TransactionActivity$7;->b:Lcom/stagnationlab/sk/TransactionActivity;

    iput-object p2, p0, Lcom/stagnationlab/sk/TransactionActivity$7;->a:Lcom/stagnationlab/sk/TransactionActivity$a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/stagnationlab/sk/c/a;)V
    .locals 2

    .line 835
    iget-object p1, p0, Lcom/stagnationlab/sk/TransactionActivity$7;->a:Lcom/stagnationlab/sk/TransactionActivity$a;

    sget-object v0, Lcom/stagnationlab/sk/TransactionActivity$a;->c:Lcom/stagnationlab/sk/TransactionActivity$a;

    if-ne p1, v0, :cond_0

    .line 836
    iget-object p1, p0, Lcom/stagnationlab/sk/TransactionActivity$7;->b:Lcom/stagnationlab/sk/TransactionActivity;

    new-instance v0, Lcom/stagnationlab/sk/c/a;

    sget-object v1, Lcom/stagnationlab/sk/c/b;->i:Lcom/stagnationlab/sk/c/b;

    invoke-direct {v0, v1}, Lcom/stagnationlab/sk/c/a;-><init>(Lcom/stagnationlab/sk/c/b;)V

    invoke-static {p1, v0}, Lcom/stagnationlab/sk/TransactionActivity;->a(Lcom/stagnationlab/sk/TransactionActivity;Lcom/stagnationlab/sk/c/a;)Z

    return-void

    .line 841
    :cond_0
    iget-object p1, p0, Lcom/stagnationlab/sk/TransactionActivity$7;->a:Lcom/stagnationlab/sk/TransactionActivity$a;

    sget-object v0, Lcom/stagnationlab/sk/TransactionActivity$a;->b:Lcom/stagnationlab/sk/TransactionActivity$a;

    if-ne p1, v0, :cond_1

    iget-object p1, p0, Lcom/stagnationlab/sk/TransactionActivity$7;->b:Lcom/stagnationlab/sk/TransactionActivity;

    .line 843
    invoke-static {p1}, Lcom/stagnationlab/sk/TransactionActivity;->o(Lcom/stagnationlab/sk/TransactionActivity;)Z

    move-result p1

    if-eqz p1, :cond_1

    .line 844
    invoke-static {}, Lcom/stagnationlab/sk/a/d;->a()Z

    move-result p1

    if-eqz p1, :cond_1

    .line 846
    new-instance p1, Landroid/content/Intent;

    iget-object v0, p0, Lcom/stagnationlab/sk/TransactionActivity$7;->b:Lcom/stagnationlab/sk/TransactionActivity;

    const-class v1, Lcom/stagnationlab/sk/MainActivity;

    invoke-direct {p1, v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/4 v0, 0x1

    const-string v1, "showNotificationInfo"

    .line 847
    invoke-virtual {p1, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 848
    iget-object v0, p0, Lcom/stagnationlab/sk/TransactionActivity$7;->b:Lcom/stagnationlab/sk/TransactionActivity;

    invoke-virtual {v0, p1}, Lcom/stagnationlab/sk/TransactionActivity;->startActivity(Landroid/content/Intent;)V

    .line 849
    iget-object p1, p0, Lcom/stagnationlab/sk/TransactionActivity$7;->b:Lcom/stagnationlab/sk/TransactionActivity;

    invoke-static {p1}, Lcom/stagnationlab/sk/util/n;->c(Landroid/app/Activity;)V

    .line 850
    iget-object p1, p0, Lcom/stagnationlab/sk/TransactionActivity$7;->b:Lcom/stagnationlab/sk/TransactionActivity;

    invoke-virtual {p1}, Lcom/stagnationlab/sk/TransactionActivity;->finish()V

    return-void

    .line 855
    :cond_1
    iget-object p1, p0, Lcom/stagnationlab/sk/TransactionActivity$7;->b:Lcom/stagnationlab/sk/TransactionActivity;

    invoke-static {p1}, Lcom/stagnationlab/sk/TransactionActivity;->e(Lcom/stagnationlab/sk/TransactionActivity;)V

    return-void
.end method

.method public a(Lcom/stagnationlab/sk/models/Transaction;Lcom/stagnationlab/sk/models/RpRequest;)V
    .locals 3

    .line 823
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/stagnationlab/sk/TransactionActivity$7;->b:Lcom/stagnationlab/sk/TransactionActivity;

    const-class v2, Lcom/stagnationlab/sk/TransactionActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "transaction"

    .line 824
    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string p1, "rpRequest"

    .line 825
    invoke-virtual {v0, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 826
    iget-object p1, p0, Lcom/stagnationlab/sk/TransactionActivity$7;->b:Lcom/stagnationlab/sk/TransactionActivity;

    invoke-static {p1}, Lcom/stagnationlab/sk/TransactionActivity;->o(Lcom/stagnationlab/sk/TransactionActivity;)Z

    move-result p1

    const-string p2, "manual"

    invoke-virtual {v0, p2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string p1, "reloadActivity"

    const/4 p2, 0x1

    .line 827
    invoke-virtual {v0, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 828
    iget-object p1, p0, Lcom/stagnationlab/sk/TransactionActivity$7;->b:Lcom/stagnationlab/sk/TransactionActivity;

    invoke-virtual {p1, v0}, Lcom/stagnationlab/sk/TransactionActivity;->startActivity(Landroid/content/Intent;)V

    .line 829
    iget-object p1, p0, Lcom/stagnationlab/sk/TransactionActivity$7;->b:Lcom/stagnationlab/sk/TransactionActivity;

    invoke-static {p1}, Lcom/stagnationlab/sk/util/n;->c(Landroid/app/Activity;)V

    .line 830
    iget-object p1, p0, Lcom/stagnationlab/sk/TransactionActivity$7;->b:Lcom/stagnationlab/sk/TransactionActivity;

    invoke-virtual {p1}, Lcom/stagnationlab/sk/TransactionActivity;->finish()V

    return-void
.end method
