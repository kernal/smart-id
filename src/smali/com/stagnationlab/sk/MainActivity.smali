.class public Lcom/stagnationlab/sk/MainActivity;
.super Lcom/stagnationlab/sk/c;
.source "MainActivity.java"


# instance fields
.field private c:Lcom/stagnationlab/sk/h/e;

.field private d:Lcom/stagnationlab/sk/a/a;

.field private e:Lcom/stagnationlab/sk/h/d;

.field private f:Lcom/stagnationlab/sk/MyApplication;

.field private g:Lcom/stagnationlab/sk/g/e;

.field private h:Lcom/stagnationlab/sk/a/b;

.field private i:Lcom/stagnationlab/sk/h/c;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 33
    invoke-direct {p0}, Lcom/stagnationlab/sk/c;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/stagnationlab/sk/MainActivity;Ljava/lang/String;)Landroid/content/Context;
    .locals 0

    .line 33
    invoke-direct {p0, p1}, Lcom/stagnationlab/sk/MainActivity;->c(Ljava/lang/String;)Landroid/content/Context;

    move-result-object p0

    return-object p0
.end method

.method static synthetic a(Lcom/stagnationlab/sk/MainActivity;Lcom/stagnationlab/sk/a/a;)Lcom/stagnationlab/sk/a/a;
    .locals 0

    .line 33
    iput-object p1, p0, Lcom/stagnationlab/sk/MainActivity;->d:Lcom/stagnationlab/sk/a/a;

    return-object p1
.end method

.method static synthetic a(Lcom/stagnationlab/sk/MainActivity;)Lcom/stagnationlab/sk/h/e;
    .locals 0

    .line 33
    iget-object p0, p0, Lcom/stagnationlab/sk/MainActivity;->c:Lcom/stagnationlab/sk/h/e;

    return-object p0
.end method

.method static synthetic b(Lcom/stagnationlab/sk/MainActivity;)Lcom/stagnationlab/sk/MyApplication;
    .locals 0

    .line 33
    iget-object p0, p0, Lcom/stagnationlab/sk/MainActivity;->f:Lcom/stagnationlab/sk/MyApplication;

    return-object p0
.end method

.method private c(Ljava/lang/String;)Landroid/content/Context;
    .locals 1

    .line 222
    iget-object v0, p0, Lcom/stagnationlab/sk/MainActivity;->b:Landroid/content/Context;

    invoke-virtual {p0, v0, p1}, Lcom/stagnationlab/sk/MainActivity;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Context;

    move-result-object p1

    return-object p1
.end method

.method static synthetic c(Lcom/stagnationlab/sk/MainActivity;)Lcom/stagnationlab/sk/a/a;
    .locals 0

    .line 33
    iget-object p0, p0, Lcom/stagnationlab/sk/MainActivity;->d:Lcom/stagnationlab/sk/a/a;

    return-object p0
.end method

.method static synthetic d(Lcom/stagnationlab/sk/MainActivity;)Lcom/stagnationlab/sk/h/d;
    .locals 0

    .line 33
    iget-object p0, p0, Lcom/stagnationlab/sk/MainActivity;->e:Lcom/stagnationlab/sk/h/d;

    return-object p0
.end method

.method static synthetic e(Lcom/stagnationlab/sk/MainActivity;)Lcom/stagnationlab/sk/a/b;
    .locals 0

    .line 33
    iget-object p0, p0, Lcom/stagnationlab/sk/MainActivity;->h:Lcom/stagnationlab/sk/a/b;

    return-object p0
.end method

.method private m()V
    .locals 3

    .line 153
    iget-object v0, p0, Lcom/stagnationlab/sk/MainActivity;->d:Lcom/stagnationlab/sk/a/a;

    invoke-virtual {v0}, Lcom/stagnationlab/sk/a/a;->h()Z

    move-result v0

    if-nez v0, :cond_0

    .line 154
    iget-object v0, p0, Lcom/stagnationlab/sk/MainActivity;->e:Lcom/stagnationlab/sk/h/d;

    iget-object v1, p0, Lcom/stagnationlab/sk/MainActivity;->d:Lcom/stagnationlab/sk/a/a;

    invoke-virtual {v1}, Lcom/stagnationlab/sk/a/a;->b()Lcom/stagnationlab/sk/models/Account;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/stagnationlab/sk/h/d;->a(Lcom/stagnationlab/sk/models/Account;)V

    return-void

    .line 159
    :cond_0
    iget-object v0, p0, Lcom/stagnationlab/sk/MainActivity;->e:Lcom/stagnationlab/sk/h/d;

    invoke-virtual {v0}, Lcom/stagnationlab/sk/h/d;->f()V

    .line 161
    iget-object v0, p0, Lcom/stagnationlab/sk/MainActivity;->d:Lcom/stagnationlab/sk/a/a;

    const/4 v1, 0x0

    new-instance v2, Lcom/stagnationlab/sk/MainActivity$2;

    invoke-direct {v2, p0}, Lcom/stagnationlab/sk/MainActivity$2;-><init>(Lcom/stagnationlab/sk/MainActivity;)V

    invoke-virtual {v0, v1, v2}, Lcom/stagnationlab/sk/a/a;->a(ZLcom/stagnationlab/sk/a/a/y;)V

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .line 211
    iget-object v0, p0, Lcom/stagnationlab/sk/MainActivity;->d:Lcom/stagnationlab/sk/a/a;

    invoke-virtual {v0}, Lcom/stagnationlab/sk/a/a;->h()Z

    move-result v0

    invoke-virtual {p0, p1, v0, p2}, Lcom/stagnationlab/sk/MainActivity;->a(Ljava/lang/String;ZLjava/lang/String;)V

    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Lcom/stagnationlab/sk/g/f;)V
    .locals 2

    .line 186
    iget-object v0, p0, Lcom/stagnationlab/sk/MainActivity;->g:Lcom/stagnationlab/sk/g/e;

    invoke-virtual {v0}, Lcom/stagnationlab/sk/g/e;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 189
    iget-object v0, p0, Lcom/stagnationlab/sk/MainActivity;->g:Lcom/stagnationlab/sk/g/e;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/stagnationlab/sk/g/e;->a(Z)V

    .line 192
    :cond_0
    iget-object v0, p0, Lcom/stagnationlab/sk/MainActivity;->g:Lcom/stagnationlab/sk/g/e;

    invoke-virtual {v0, p2, p3}, Lcom/stagnationlab/sk/g/e;->a(Ljava/lang/String;Lcom/stagnationlab/sk/g/f;)V

    const p2, 0x7f080022

    .line 195
    invoke-virtual {p0, p2}, Lcom/stagnationlab/sk/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Lcom/stagnationlab/sk/elements/TextField;

    iget-object p3, p0, Lcom/stagnationlab/sk/MainActivity;->b:Landroid/content/Context;

    invoke-virtual {p3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p3

    const v0, 0x7f0e002a

    invoke-virtual {p3, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p2, p3}, Lcom/stagnationlab/sk/elements/TextField;->setText(Ljava/lang/CharSequence;)V

    .line 198
    iget-object p2, p0, Lcom/stagnationlab/sk/MainActivity;->f:Lcom/stagnationlab/sk/MyApplication;

    invoke-virtual {p2}, Lcom/stagnationlab/sk/MyApplication;->e()Lcom/stagnationlab/sk/a;

    move-result-object p2

    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "Banklink - "

    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2, p1}, Lcom/stagnationlab/sk/a;->a(Ljava/lang/String;)V

    return-void
.end method

.method public a(ZZLcom/stagnationlab/sk/h/c;)V
    .locals 7

    .line 93
    iget-object v0, p0, Lcom/stagnationlab/sk/MainActivity;->i:Lcom/stagnationlab/sk/h/c;

    if-eqz v0, :cond_0

    .line 95
    new-instance p1, Lcom/stagnationlab/sk/c/a;

    sget-object p2, Lcom/stagnationlab/sk/c/b;->V:Lcom/stagnationlab/sk/c/b;

    invoke-direct {p1, p2}, Lcom/stagnationlab/sk/c/a;-><init>(Lcom/stagnationlab/sk/c/b;)V

    invoke-static {p1}, Lcom/stagnationlab/sk/util/f;->a(Lcom/stagnationlab/sk/c/a;)Lcom/stagnationlab/sk/util/f;

    move-result-object p1

    invoke-interface {p3, p1}, Lcom/stagnationlab/sk/h/c;->b(Lcom/stagnationlab/sk/util/f;)V

    return-void

    .line 100
    :cond_0
    new-instance v0, Lcom/google/android/gms/auth/api/credentials/CredentialPickerConfig$a;

    invoke-direct {v0}, Lcom/google/android/gms/auth/api/credentials/CredentialPickerConfig$a;-><init>()V

    const/4 v1, 0x0

    .line 101
    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/api/credentials/CredentialPickerConfig$a;->a(Z)Lcom/google/android/gms/auth/api/credentials/CredentialPickerConfig$a;

    move-result-object v0

    const/4 v1, 0x1

    .line 102
    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/api/credentials/CredentialPickerConfig$a;->b(Z)Lcom/google/android/gms/auth/api/credentials/CredentialPickerConfig$a;

    move-result-object v0

    .line 103
    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/api/credentials/CredentialPickerConfig$a;->a(I)Lcom/google/android/gms/auth/api/credentials/CredentialPickerConfig$a;

    move-result-object v0

    .line 104
    invoke-virtual {v0}, Lcom/google/android/gms/auth/api/credentials/CredentialPickerConfig$a;->a()Lcom/google/android/gms/auth/api/credentials/CredentialPickerConfig;

    move-result-object v0

    .line 106
    new-instance v1, Lcom/google/android/gms/auth/api/credentials/HintRequest$a;

    invoke-direct {v1}, Lcom/google/android/gms/auth/api/credentials/HintRequest$a;-><init>()V

    .line 107
    invoke-virtual {v1, v0}, Lcom/google/android/gms/auth/api/credentials/HintRequest$a;->a(Lcom/google/android/gms/auth/api/credentials/CredentialPickerConfig;)Lcom/google/android/gms/auth/api/credentials/HintRequest$a;

    move-result-object v0

    .line 108
    invoke-virtual {v0, p2}, Lcom/google/android/gms/auth/api/credentials/HintRequest$a;->b(Z)Lcom/google/android/gms/auth/api/credentials/HintRequest$a;

    move-result-object p2

    .line 109
    invoke-virtual {p2, p1}, Lcom/google/android/gms/auth/api/credentials/HintRequest$a;->a(Z)Lcom/google/android/gms/auth/api/credentials/HintRequest$a;

    move-result-object p1

    .line 110
    invoke-virtual {p1}, Lcom/google/android/gms/auth/api/credentials/HintRequest$a;->a()Lcom/google/android/gms/auth/api/credentials/HintRequest;

    move-result-object p1

    .line 112
    invoke-static {p0}, Lcom/google/android/gms/auth/api/credentials/a;->a(Landroid/app/Activity;)Lcom/google/android/gms/auth/api/credentials/c;

    move-result-object p2

    .line 114
    invoke-virtual {p2, p1}, Lcom/google/android/gms/auth/api/credentials/c;->a(Lcom/google/android/gms/auth/api/credentials/HintRequest;)Landroid/app/PendingIntent;

    move-result-object p1

    .line 117
    :try_start_0
    invoke-virtual {p1}, Landroid/app/PendingIntent;->getIntentSender()Landroid/content/IntentSender;

    move-result-object v1

    const/4 v2, 0x1

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object v0, p0

    invoke-virtual/range {v0 .. v6}, Lcom/stagnationlab/sk/MainActivity;->startIntentSenderForResult(Landroid/content/IntentSender;ILandroid/content/Intent;III)V

    .line 118
    iput-object p3, p0, Lcom/stagnationlab/sk/MainActivity;->i:Lcom/stagnationlab/sk/h/c;
    :try_end_0
    .catch Landroid/content/IntentSender$SendIntentException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    const-string p2, "MainActivity"

    const-string v0, "Could not start hint picker Intent"

    .line 120
    invoke-static {p2, v0, p1}, Lcom/stagnationlab/sk/f;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 122
    new-instance p1, Lcom/stagnationlab/sk/c/a;

    sget-object p2, Lcom/stagnationlab/sk/c/b;->U:Lcom/stagnationlab/sk/c/b;

    invoke-direct {p1, p2}, Lcom/stagnationlab/sk/c/a;-><init>(Lcom/stagnationlab/sk/c/b;)V

    invoke-static {p1}, Lcom/stagnationlab/sk/util/f;->a(Lcom/stagnationlab/sk/c/a;)Lcom/stagnationlab/sk/util/f;

    move-result-object p1

    invoke-interface {p3, p1}, Lcom/stagnationlab/sk/h/c;->b(Lcom/stagnationlab/sk/util/f;)V

    :goto_0
    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 1

    .line 215
    iget-object v0, p0, Lcom/stagnationlab/sk/MainActivity;->f:Lcom/stagnationlab/sk/MyApplication;

    invoke-virtual {v0, p1}, Lcom/stagnationlab/sk/MyApplication;->a(Ljava/lang/String;)V

    .line 217
    invoke-direct {p0, p1}, Lcom/stagnationlab/sk/MainActivity;->c(Ljava/lang/String;)Landroid/content/Context;

    move-result-object p1

    .line 218
    invoke-static {p1}, Lcom/stagnationlab/sk/fcm/c;->d(Landroid/content/Context;)V

    return-void
.end method

.method public closeSso(Landroid/view/View;)V
    .locals 0

    .line 202
    iget-object p1, p0, Lcom/stagnationlab/sk/MainActivity;->g:Lcom/stagnationlab/sk/g/e;

    invoke-virtual {p1}, Lcom/stagnationlab/sk/g/e;->a()V

    return-void
.end method

.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 2

    .line 235
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_1

    .line 236
    iget-object p1, p0, Lcom/stagnationlab/sk/MainActivity;->g:Lcom/stagnationlab/sk/g/e;

    invoke-virtual {p1}, Lcom/stagnationlab/sk/g/e;->b()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 237
    iget-object p1, p0, Lcom/stagnationlab/sk/MainActivity;->g:Lcom/stagnationlab/sk/g/e;

    invoke-virtual {p1}, Lcom/stagnationlab/sk/g/e;->a()V

    goto :goto_0

    .line 239
    :cond_0
    iget-object p1, p0, Lcom/stagnationlab/sk/MainActivity;->e:Lcom/stagnationlab/sk/h/d;

    invoke-virtual {p1}, Lcom/stagnationlab/sk/h/d;->a()V

    :goto_0
    const/4 p1, 0x1

    return p1

    .line 245
    :cond_1
    invoke-super {p0, p1}, Lcom/stagnationlab/sk/c;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result p1

    return p1
.end method

.method public j()V
    .locals 1

    .line 207
    iget-object v0, p0, Lcom/stagnationlab/sk/MainActivity;->e:Lcom/stagnationlab/sk/h/d;

    invoke-virtual {v0}, Lcom/stagnationlab/sk/h/d;->g()V

    return-void
.end method

.method public k()V
    .locals 1

    .line 226
    iget-object v0, p0, Lcom/stagnationlab/sk/MainActivity;->c:Lcom/stagnationlab/sk/h/e;

    invoke-virtual {v0}, Lcom/stagnationlab/sk/h/e;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 227
    iget-object v0, p0, Lcom/stagnationlab/sk/MainActivity;->c:Lcom/stagnationlab/sk/h/e;

    invoke-virtual {v0}, Lcom/stagnationlab/sk/h/e;->e()V

    goto :goto_0

    .line 229
    :cond_0
    invoke-virtual {p0}, Lcom/stagnationlab/sk/MainActivity;->finish()V

    :goto_0
    return-void
.end method

.method public l()Lcom/stagnationlab/sk/h/d;
    .locals 1

    .line 296
    iget-object v0, p0, Lcom/stagnationlab/sk/MainActivity;->e:Lcom/stagnationlab/sk/h/d;

    return-object v0
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .line 128
    invoke-super {p0, p1, p2, p3}, Lcom/stagnationlab/sk/c;->onActivityResult(IILandroid/content/Intent;)V

    const/4 v0, 0x1

    if-ne p1, v0, :cond_3

    .line 131
    iget-object p1, p0, Lcom/stagnationlab/sk/MainActivity;->i:Lcom/stagnationlab/sk/h/c;

    if-nez p1, :cond_0

    return-void

    :cond_0
    const/4 p1, -0x1

    if-ne p2, p1, :cond_1

    const-string p1, "com.google.android.gms.credentials.Credential"

    .line 136
    invoke-virtual {p3, p1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object p1

    check-cast p1, Lcom/google/android/gms/auth/api/credentials/Credential;

    .line 138
    iget-object p2, p0, Lcom/stagnationlab/sk/MainActivity;->i:Lcom/stagnationlab/sk/h/c;

    new-instance p3, Lcom/stagnationlab/sk/util/f;

    invoke-direct {p3}, Lcom/stagnationlab/sk/util/f;-><init>()V

    invoke-virtual {p1}, Lcom/google/android/gms/auth/api/credentials/Credential;->a()Ljava/lang/String;

    move-result-object p1

    const-string v0, "value"

    invoke-virtual {p3, v0, p1}, Lcom/stagnationlab/sk/util/f;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/stagnationlab/sk/util/f;

    move-result-object p1

    invoke-interface {p2, p1}, Lcom/stagnationlab/sk/h/c;->a(Lcom/stagnationlab/sk/util/f;)V

    goto :goto_1

    .line 140
    :cond_1
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string p3, "Hint Read: NOT OK. Code: "

    invoke-virtual {p1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string p3, "MainActivity"

    invoke-static {p3, p1}, Lcom/stagnationlab/sk/f;->c(Ljava/lang/String;Ljava/lang/String;)V

    const/16 p1, 0x3ea

    if-ne p2, p1, :cond_2

    .line 142
    sget-object p1, Lcom/stagnationlab/sk/c/b;->W:Lcom/stagnationlab/sk/c/b;

    goto :goto_0

    :cond_2
    sget-object p1, Lcom/stagnationlab/sk/c/b;->U:Lcom/stagnationlab/sk/c/b;

    .line 145
    :goto_0
    iget-object p2, p0, Lcom/stagnationlab/sk/MainActivity;->i:Lcom/stagnationlab/sk/h/c;

    new-instance p3, Lcom/stagnationlab/sk/c/a;

    invoke-direct {p3, p1}, Lcom/stagnationlab/sk/c/a;-><init>(Lcom/stagnationlab/sk/c/b;)V

    invoke-static {p3}, Lcom/stagnationlab/sk/util/f;->a(Lcom/stagnationlab/sk/c/a;)Lcom/stagnationlab/sk/util/f;

    move-result-object p1

    invoke-interface {p2, p1}, Lcom/stagnationlab/sk/h/c;->b(Lcom/stagnationlab/sk/util/f;)V

    :goto_1
    const/4 p1, 0x0

    .line 148
    iput-object p1, p0, Lcom/stagnationlab/sk/MainActivity;->i:Lcom/stagnationlab/sk/h/c;

    :cond_3
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2

    const-string v0, "MainActivity"

    const-string v1, "application started"

    .line 47
    invoke-static {v0, v1}, Lcom/stagnationlab/sk/f;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 49
    invoke-super {p0, p1}, Lcom/stagnationlab/sk/c;->onCreate(Landroid/os/Bundle;)V

    .line 51
    invoke-virtual {p0}, Lcom/stagnationlab/sk/MainActivity;->isTaskRoot()Z

    move-result p1

    if-nez p1, :cond_0

    invoke-virtual {p0}, Lcom/stagnationlab/sk/MainActivity;->getIntent()Landroid/content/Intent;

    move-result-object p1

    const-string v0, "android.intent.category.LAUNCHER"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->hasCategory(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lcom/stagnationlab/sk/MainActivity;->getIntent()Landroid/content/Intent;

    move-result-object p1

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 52
    invoke-virtual {p0}, Lcom/stagnationlab/sk/MainActivity;->getIntent()Landroid/content/Intent;

    move-result-object p1

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object p1

    const-string v0, "android.intent.action.MAIN"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 56
    invoke-virtual {p0}, Lcom/stagnationlab/sk/MainActivity;->finish()V

    return-void

    :cond_0
    const p1, 0x7f0a001e

    .line 61
    invoke-virtual {p0, p1}, Lcom/stagnationlab/sk/MainActivity;->setContentView(I)V

    .line 63
    invoke-virtual {p0}, Lcom/stagnationlab/sk/MainActivity;->getApplication()Landroid/app/Application;

    move-result-object p1

    check-cast p1, Lcom/stagnationlab/sk/MyApplication;

    iput-object p1, p0, Lcom/stagnationlab/sk/MainActivity;->f:Lcom/stagnationlab/sk/MyApplication;

    const p1, 0x7f0800df

    .line 65
    invoke-virtual {p0, p1}, Lcom/stagnationlab/sk/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    .line 67
    new-instance v0, Lcom/stagnationlab/sk/h/e;

    move-object v1, p1

    check-cast v1, Landroid/widget/LinearLayout;

    invoke-direct {v0, p0, v1}, Lcom/stagnationlab/sk/h/e;-><init>(Landroid/content/Context;Landroid/widget/LinearLayout;)V

    iput-object v0, p0, Lcom/stagnationlab/sk/MainActivity;->c:Lcom/stagnationlab/sk/h/e;

    .line 68
    new-instance v0, Lcom/stagnationlab/sk/h/d;

    iget-object v1, p0, Lcom/stagnationlab/sk/MainActivity;->c:Lcom/stagnationlab/sk/h/e;

    invoke-direct {v0, p0, v1}, Lcom/stagnationlab/sk/h/d;-><init>(Lcom/stagnationlab/sk/MainActivity;Lcom/stagnationlab/sk/h/e;)V

    iput-object v0, p0, Lcom/stagnationlab/sk/MainActivity;->e:Lcom/stagnationlab/sk/h/d;

    .line 69
    new-instance v0, Lcom/stagnationlab/sk/a/b;

    iget-object v1, p0, Lcom/stagnationlab/sk/MainActivity;->e:Lcom/stagnationlab/sk/h/d;

    invoke-direct {v0, p0, v1}, Lcom/stagnationlab/sk/a/b;-><init>(Lcom/stagnationlab/sk/MainActivity;Lcom/stagnationlab/sk/h/d;)V

    iput-object v0, p0, Lcom/stagnationlab/sk/MainActivity;->h:Lcom/stagnationlab/sk/a/b;

    .line 70
    new-instance v0, Lcom/stagnationlab/sk/g/e;

    invoke-direct {v0, p0, p1}, Lcom/stagnationlab/sk/g/e;-><init>(Landroid/app/Activity;Landroid/view/View;)V

    iput-object v0, p0, Lcom/stagnationlab/sk/MainActivity;->g:Lcom/stagnationlab/sk/g/e;

    .line 72
    iget-object p1, p0, Lcom/stagnationlab/sk/MainActivity;->c:Lcom/stagnationlab/sk/h/e;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/stagnationlab/sk/h/e;->a(Z)V

    .line 73
    iget-object p1, p0, Lcom/stagnationlab/sk/MainActivity;->c:Lcom/stagnationlab/sk/h/e;

    iget-object v0, p0, Lcom/stagnationlab/sk/MainActivity;->e:Lcom/stagnationlab/sk/h/d;

    const-string v1, "NativeBridge"

    invoke-virtual {p1, v0, v1}, Lcom/stagnationlab/sk/h/e;->a(Lcom/stagnationlab/sk/h/d;Ljava/lang/String;)V

    .line 75
    iget-object p1, p0, Lcom/stagnationlab/sk/MainActivity;->f:Lcom/stagnationlab/sk/MyApplication;

    new-instance v0, Lcom/stagnationlab/sk/MainActivity$1;

    invoke-direct {v0, p0}, Lcom/stagnationlab/sk/MainActivity$1;-><init>(Lcom/stagnationlab/sk/MainActivity;)V

    invoke-virtual {p1, v0}, Lcom/stagnationlab/sk/MyApplication;->a(Lcom/stagnationlab/sk/e/a;)V

    return-void
.end method

.method protected onDestroy()V
    .locals 1

    .line 284
    invoke-super {p0}, Lcom/stagnationlab/sk/c;->onDestroy()V

    .line 286
    iget-object v0, p0, Lcom/stagnationlab/sk/MainActivity;->c:Lcom/stagnationlab/sk/h/e;

    if-eqz v0, :cond_0

    .line 287
    invoke-virtual {v0}, Lcom/stagnationlab/sk/h/e;->h()V

    .line 290
    :cond_0
    iget-object v0, p0, Lcom/stagnationlab/sk/MainActivity;->g:Lcom/stagnationlab/sk/g/e;

    if-eqz v0, :cond_1

    .line 291
    invoke-virtual {v0}, Lcom/stagnationlab/sk/g/e;->e()V

    :cond_1
    return-void
.end method

.method protected onPause()V
    .locals 1

    .line 250
    invoke-super {p0}, Lcom/stagnationlab/sk/c;->onPause()V

    .line 252
    iget-object v0, p0, Lcom/stagnationlab/sk/MainActivity;->c:Lcom/stagnationlab/sk/h/e;

    if-eqz v0, :cond_0

    .line 253
    invoke-virtual {v0}, Lcom/stagnationlab/sk/h/e;->f()V

    .line 256
    :cond_0
    iget-object v0, p0, Lcom/stagnationlab/sk/MainActivity;->g:Lcom/stagnationlab/sk/g/e;

    invoke-virtual {v0}, Lcom/stagnationlab/sk/g/e;->c()V

    .line 258
    iget-object v0, p0, Lcom/stagnationlab/sk/MainActivity;->d:Lcom/stagnationlab/sk/a/a;

    if-eqz v0, :cond_1

    .line 259
    invoke-virtual {v0}, Lcom/stagnationlab/sk/a/a;->i()V

    :cond_1
    return-void
.end method

.method protected onResume()V
    .locals 1

    .line 265
    invoke-super {p0}, Lcom/stagnationlab/sk/c;->onResume()V

    .line 267
    iget-object v0, p0, Lcom/stagnationlab/sk/MainActivity;->c:Lcom/stagnationlab/sk/h/e;

    if-eqz v0, :cond_0

    .line 268
    invoke-virtual {v0}, Lcom/stagnationlab/sk/h/e;->g()V

    .line 270
    iget-object v0, p0, Lcom/stagnationlab/sk/MainActivity;->h:Lcom/stagnationlab/sk/a/b;

    invoke-virtual {v0}, Lcom/stagnationlab/sk/a/b;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 271
    invoke-direct {p0}, Lcom/stagnationlab/sk/MainActivity;->m()V

    .line 275
    :cond_0
    iget-object v0, p0, Lcom/stagnationlab/sk/MainActivity;->g:Lcom/stagnationlab/sk/g/e;

    invoke-virtual {v0}, Lcom/stagnationlab/sk/g/e;->d()V

    .line 277
    iget-object v0, p0, Lcom/stagnationlab/sk/MainActivity;->d:Lcom/stagnationlab/sk/a/a;

    if-eqz v0, :cond_1

    .line 278
    invoke-virtual {v0, p0}, Lcom/stagnationlab/sk/a/a;->a(Landroid/app/Activity;)V

    :cond_1
    return-void
.end method
