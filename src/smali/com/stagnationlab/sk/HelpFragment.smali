.class public Lcom/stagnationlab/sk/HelpFragment;
.super Landroid/app/Fragment;
.source "HelpFragment.java"


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Ljava/lang/String;

.field private final c:Ljava/lang/String;

.field private final d:Ljava/lang/String;

.field private final e:Ljava/lang/String;

.field private final f:Ljava/lang/String;

.field private final g:Ljava/lang/String;

.field private final h:Ljava/lang/String;

.field private final i:Ljava/lang/String;

.field private final j:Ljava/lang/String;

.field private final k:Ljava/lang/String;

.field private final l:Ljava/lang/String;

.field private final m:Ljava/lang/String;

.field private n:Lcom/stagnationlab/sk/c;

.field private o:Landroid/widget/TableLayout;

.field private p:Landroid/view/LayoutInflater;

.field private q:Landroid/view/View;

.field private r:I

.field private s:Landroid/content/Context;

.field private t:Landroid/widget/RelativeLayout;

.field private u:Landroid/view/View;

.field private v:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 21
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    const-string v0, "+372 715 1606"

    .line 22
    iput-object v0, p0, Lcom/stagnationlab/sk/HelpFragment;->a:Ljava/lang/String;

    const-string v0, "1807"

    .line 23
    iput-object v0, p0, Lcom/stagnationlab/sk/HelpFragment;->b:Ljava/lang/String;

    const-string v1, "+371 6766 5001"

    .line 24
    iput-object v1, p0, Lcom/stagnationlab/sk/HelpFragment;->c:Ljava/lang/String;

    .line 25
    iput-object v0, p0, Lcom/stagnationlab/sk/HelpFragment;->d:Ljava/lang/String;

    const-string v0, "+370 6704 1807"

    .line 26
    iput-object v0, p0, Lcom/stagnationlab/sk/HelpFragment;->e:Ljava/lang/String;

    const-string v0, "help.ee@smart-id.com"

    .line 27
    iput-object v0, p0, Lcom/stagnationlab/sk/HelpFragment;->f:Ljava/lang/String;

    const-string v0, "help.lv@smart-id.com"

    .line 28
    iput-object v0, p0, Lcom/stagnationlab/sk/HelpFragment;->g:Ljava/lang/String;

    const-string v0, "help.lt@smart-id.com"

    .line 29
    iput-object v0, p0, Lcom/stagnationlab/sk/HelpFragment;->h:Ljava/lang/String;

    const-string v0, "https://www.smart-id.com/help/faq/"

    .line 31
    iput-object v0, p0, Lcom/stagnationlab/sk/HelpFragment;->i:Ljava/lang/String;

    const-string v0, "https://www.smart-id.com/et/abi/kkk/"

    .line 32
    iput-object v0, p0, Lcom/stagnationlab/sk/HelpFragment;->j:Ljava/lang/String;

    const-string v0, "https://www.smart-id.com/lv/palidziba/buj/"

    .line 33
    iput-object v0, p0, Lcom/stagnationlab/sk/HelpFragment;->k:Ljava/lang/String;

    const-string v0, "https://www.smart-id.com/lt/pagalba/duk/"

    .line 34
    iput-object v0, p0, Lcom/stagnationlab/sk/HelpFragment;->l:Ljava/lang/String;

    const-string v0, "https://www.smart-id.com/ru/spravka/voprosy-otvety/"

    .line 35
    iput-object v0, p0, Lcom/stagnationlab/sk/HelpFragment;->m:Ljava/lang/String;

    const/4 v0, -0x1

    .line 41
    iput v0, p0, Lcom/stagnationlab/sk/HelpFragment;->r:I

    return-void
.end method

.method private a(Landroid/view/View;Landroid/widget/TextView;Landroid/widget/TextView;)I
    .locals 3

    .line 219
    invoke-virtual {p0}, Lcom/stagnationlab/sk/HelpFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f06005c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 220
    invoke-virtual {p1}, Landroid/view/View;->getPaddingRight()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-virtual {p1}, Landroid/view/View;->getPaddingLeft()I

    move-result p1

    sub-int/2addr v0, p1

    .line 222
    invoke-virtual {p2}, Landroid/widget/TextView;->getPaddingRight()I

    move-result p1

    sub-int/2addr v0, p1

    .line 223
    invoke-virtual {p2}, Landroid/widget/TextView;->getPaddingLeft()I

    move-result p1

    sub-int/2addr v0, p1

    .line 224
    invoke-virtual {p3}, Landroid/widget/TextView;->getPaddingRight()I

    move-result p1

    sub-int/2addr v0, p1

    .line 225
    invoke-virtual {p3}, Landroid/widget/TextView;->getPaddingLeft()I

    move-result p1

    sub-int/2addr v0, p1

    .line 227
    invoke-virtual {p2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object p1

    int-to-float v0, v0

    .line 228
    invoke-virtual {p2}, Landroid/widget/TextView;->getPaint()Landroid/text/TextPaint;

    move-result-object p2

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v1

    const/4 v2, 0x0

    invoke-virtual {p2, p1, v2, v1}, Landroid/text/TextPaint;->measureText(Ljava/lang/CharSequence;II)F

    move-result p1

    sub-float/2addr v0, p1

    float-to-int p1, v0

    .line 231
    invoke-virtual {p3, v2, v2}, Landroid/widget/TextView;->measure(II)V

    .line 232
    invoke-virtual {p3}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result p2

    sub-int/2addr p1, p2

    return p1
.end method

.method private a(ZLjava/lang/String;Ljava/lang/String;)Landroid/widget/TableRow;
    .locals 8

    .line 193
    iget-object v0, p0, Lcom/stagnationlab/sk/HelpFragment;->p:Landroid/view/LayoutInflater;

    iget-object v1, p0, Lcom/stagnationlab/sk/HelpFragment;->o:Landroid/widget/TableLayout;

    const/4 v2, 0x0

    const v3, 0x7f0a0022

    invoke-virtual {v0, v3, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TableRow;

    .line 194
    invoke-virtual {v0, v2}, Landroid/widget/TableRow;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    const/4 v3, 0x1

    .line 195
    invoke-virtual {v0, v3}, Landroid/widget/TableRow;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    const v5, 0x7f0e0044

    const v6, 0x7f0e00fa

    if-eqz p1, :cond_0

    const v7, 0x7f0e0044

    goto :goto_0

    :cond_0
    const v7, 0x7f0e00fa

    .line 197
    :goto_0
    invoke-direct {p0, v7}, Lcom/stagnationlab/sk/HelpFragment;->c(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    if-eqz p1, :cond_1

    .line 200
    invoke-virtual {v4, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 201
    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setTextIsSelectable(Z)V

    goto :goto_1

    .line 203
    :cond_1
    invoke-direct {p0, v4, p2, p3}, Lcom/stagnationlab/sk/HelpFragment;->a(Landroid/widget/TextView;Ljava/lang/String;Ljava/lang/String;)V

    .line 206
    :goto_1
    iget p1, p0, Lcom/stagnationlab/sk/HelpFragment;->r:I

    const/4 p2, -0x1

    if-ne p1, p2, :cond_3

    invoke-direct {p0, v0, v1, v4}, Lcom/stagnationlab/sk/HelpFragment;->a(Landroid/view/View;Landroid/widget/TextView;Landroid/widget/TextView;)I

    move-result p1

    if-gez p1, :cond_3

    .line 208
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-direct {p0, v5}, Lcom/stagnationlab/sk/HelpFragment;->c(I)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p2, " "

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-direct {p0, v6}, Lcom/stagnationlab/sk/HelpFragment;->c(I)Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 209
    invoke-virtual {p1, p2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object p1

    array-length p3, p1

    :goto_2
    if-ge v2, p3, :cond_2

    aget-object v3, p1, v2

    .line 210
    iget v4, p0, Lcom/stagnationlab/sk/HelpFragment;->r:I

    int-to-float v4, v4

    invoke-virtual {v1}, Landroid/widget/TextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v5, v3}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    move-result v3

    invoke-static {v4, v3}, Ljava/lang/Math;->max(FF)F

    move-result v3

    float-to-int v3, v3

    iput v3, p0, Lcom/stagnationlab/sk/HelpFragment;->r:I

    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 212
    :cond_2
    iget p1, p0, Lcom/stagnationlab/sk/HelpFragment;->r:I

    invoke-virtual {v1}, Landroid/widget/TextView;->getPaddingRight()I

    move-result p2

    invoke-virtual {v1}, Landroid/widget/TextView;->getPaddingLeft()I

    move-result p3

    add-int/2addr p2, p3

    add-int/2addr p1, p2

    iput p1, p0, Lcom/stagnationlab/sk/HelpFragment;->r:I

    :cond_3
    return-object v0
.end method

.method static synthetic a(Lcom/stagnationlab/sk/HelpFragment;)Lcom/stagnationlab/sk/c;
    .locals 0

    .line 21
    iget-object p0, p0, Lcom/stagnationlab/sk/HelpFragment;->n:Lcom/stagnationlab/sk/c;

    return-object p0
.end method

.method private a(II)V
    .locals 0

    .line 305
    invoke-direct {p0, p2}, Lcom/stagnationlab/sk/HelpFragment;->c(I)Ljava/lang/String;

    move-result-object p2

    invoke-direct {p0, p1, p2}, Lcom/stagnationlab/sk/HelpFragment;->a(ILjava/lang/String;)V

    return-void
.end method

.method private a(ILjava/lang/String;)V
    .locals 0

    .line 309
    invoke-direct {p0, p1}, Lcom/stagnationlab/sk/HelpFragment;->d(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method private a(Landroid/widget/TextView;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .line 238
    invoke-direct {p0, p2}, Lcom/stagnationlab/sk/HelpFragment;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    .line 239
    invoke-direct {p0, p3}, Lcom/stagnationlab/sk/HelpFragment;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p3

    .line 241
    invoke-virtual {p2}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p3}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 242
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p2, " "

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const v1, 0x7f0e00f9

    invoke-direct {p0, v1}, Lcom/stagnationlab/sk/HelpFragment;->c(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    .line 245
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    .line 247
    iget-object p3, p0, Lcom/stagnationlab/sk/HelpFragment;->s:Landroid/content/Context;

    invoke-static {p3, p1, p2}, Lcom/stagnationlab/sk/util/k;->a(Landroid/content/Context;Landroid/widget/TextView;Ljava/lang/String;)V

    return-void
.end method

.method private a(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .line 163
    iget-object v0, p0, Lcom/stagnationlab/sk/HelpFragment;->o:Landroid/widget/TableLayout;

    invoke-direct {p0, p2}, Lcom/stagnationlab/sk/HelpFragment;->b(I)Landroid/widget/TableRow;

    move-result-object p2

    invoke-virtual {v0, p2}, Landroid/widget/TableLayout;->addView(Landroid/view/View;)V

    .line 164
    iget-object p2, p0, Lcom/stagnationlab/sk/HelpFragment;->o:Landroid/widget/TableLayout;

    invoke-direct {p0, p1}, Lcom/stagnationlab/sk/HelpFragment;->b(Ljava/lang/String;)Landroid/widget/TableRow;

    move-result-object p1

    invoke-virtual {p2, p1}, Landroid/widget/TableLayout;->addView(Landroid/view/View;)V

    .line 165
    iget-object p1, p0, Lcom/stagnationlab/sk/HelpFragment;->o:Landroid/widget/TableLayout;

    const/4 p2, 0x1

    const/4 v0, 0x0

    invoke-direct {p0, p2, p3, v0}, Lcom/stagnationlab/sk/HelpFragment;->a(ZLjava/lang/String;Ljava/lang/String;)Landroid/widget/TableRow;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/widget/TableLayout;->addView(Landroid/view/View;)V

    .line 166
    iget-object p1, p0, Lcom/stagnationlab/sk/HelpFragment;->o:Landroid/widget/TableLayout;

    const/4 p2, 0x0

    invoke-direct {p0, p2, p4, p5}, Lcom/stagnationlab/sk/HelpFragment;->a(ZLjava/lang/String;Ljava/lang/String;)Landroid/widget/TableRow;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/widget/TableLayout;->addView(Landroid/view/View;)V

    .line 167
    iget-object p1, p0, Lcom/stagnationlab/sk/HelpFragment;->o:Landroid/widget/TableLayout;

    invoke-direct {p0}, Lcom/stagnationlab/sk/HelpFragment;->g()Landroid/widget/TableRow;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/widget/TableLayout;->addView(Landroid/view/View;)V

    return-void
.end method

.method private b(I)Landroid/widget/TableRow;
    .locals 4

    .line 171
    iget-object v0, p0, Lcom/stagnationlab/sk/HelpFragment;->p:Landroid/view/LayoutInflater;

    iget-object v1, p0, Lcom/stagnationlab/sk/HelpFragment;->o:Landroid/widget/TableLayout;

    const/4 v2, 0x0

    const v3, 0x7f0a0024

    invoke-virtual {v0, v3, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TableRow;

    .line 172
    invoke-virtual {v0, v2}, Landroid/widget/TableRow;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-direct {p0, p1}, Lcom/stagnationlab/sk/HelpFragment;->c(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-object v0
.end method

.method private b(Ljava/lang/String;)Landroid/widget/TableRow;
    .locals 5

    .line 178
    iget-object v0, p0, Lcom/stagnationlab/sk/HelpFragment;->p:Landroid/view/LayoutInflater;

    iget-object v1, p0, Lcom/stagnationlab/sk/HelpFragment;->o:Landroid/widget/TableLayout;

    const/4 v2, 0x0

    const v3, 0x7f0a0021

    invoke-virtual {v0, v3, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TableRow;

    .line 179
    iget-object v1, p0, Lcom/stagnationlab/sk/HelpFragment;->s:Landroid/content/Context;

    .line 181
    invoke-virtual {v0, v2}, Landroid/widget/TableRow;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    const v3, 0x7f0e0042

    .line 182
    invoke-direct {p0, v3}, Lcom/stagnationlab/sk/HelpFragment;->c(I)Ljava/lang/String;

    move-result-object v3

    const-string v4, "{country}"

    invoke-virtual {v3, v4, p1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p1

    .line 179
    invoke-static {v1, v2, p1}, Lcom/stagnationlab/sk/util/k;->a(Landroid/content/Context;Landroid/widget/TextView;Ljava/lang/String;)V

    return-object v0
.end method

.method static synthetic b(Lcom/stagnationlab/sk/HelpFragment;)Ljava/lang/String;
    .locals 0

    .line 21
    invoke-direct {p0}, Lcom/stagnationlab/sk/HelpFragment;->h()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method private b()V
    .locals 2

    const v0, 0x7f080036

    .line 68
    invoke-direct {p0, v0}, Lcom/stagnationlab/sk/HelpFragment;->d(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/stagnationlab/sk/HelpFragment$1;

    invoke-direct {v1, p0}, Lcom/stagnationlab/sk/HelpFragment$1;-><init>(Lcom/stagnationlab/sk/HelpFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f080062

    .line 74
    invoke-direct {p0, v0}, Lcom/stagnationlab/sk/HelpFragment;->d(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/stagnationlab/sk/HelpFragment$2;

    invoke-direct {v1, p0}, Lcom/stagnationlab/sk/HelpFragment$2;-><init>(Lcom/stagnationlab/sk/HelpFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f080057

    .line 81
    invoke-direct {p0, v0}, Lcom/stagnationlab/sk/HelpFragment;->d(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/stagnationlab/sk/HelpFragment$3;

    invoke-direct {v1, p0}, Lcom/stagnationlab/sk/HelpFragment$3;-><init>(Lcom/stagnationlab/sk/HelpFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f08008b

    .line 87
    invoke-direct {p0, v0}, Lcom/stagnationlab/sk/HelpFragment;->d(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/stagnationlab/sk/HelpFragment$4;

    invoke-direct {v1, p0}, Lcom/stagnationlab/sk/HelpFragment$4;-><init>(Lcom/stagnationlab/sk/HelpFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f0800b2

    .line 93
    invoke-direct {p0, v0}, Lcom/stagnationlab/sk/HelpFragment;->d(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/stagnationlab/sk/HelpFragment$5;

    invoke-direct {v1, p0}, Lcom/stagnationlab/sk/HelpFragment$5;-><init>(Lcom/stagnationlab/sk/HelpFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private c(I)Ljava/lang/String;
    .locals 1

    .line 292
    iget-object v0, p0, Lcom/stagnationlab/sk/HelpFragment;->n:Lcom/stagnationlab/sk/c;

    invoke-virtual {v0}, Lcom/stagnationlab/sk/c;->g()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method static synthetic c(Lcom/stagnationlab/sk/HelpFragment;)Ljava/lang/String;
    .locals 0

    .line 21
    iget-object p0, p0, Lcom/stagnationlab/sk/HelpFragment;->v:Ljava/lang/String;

    return-object p0
.end method

.method private c(Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    if-nez p1, :cond_0

    const-string p1, ""

    return-object p1

    :cond_0
    const/4 v0, 0x2

    .line 255
    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const/4 v1, 0x1

    const-string v2, " "

    const-string v3, "\u00a0"

    .line 258
    invoke-virtual {p1, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p1

    aput-object p1, v0, v1

    const-string p1, "<a href=\"tel:%1$s\">%2$s</a>"

    .line 255
    invoke-static {p1, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private c()V
    .locals 8

    const v0, 0x7f0800d0

    const v1, 0x7f0e00c3

    .line 102
    invoke-direct {p0, v0, v1}, Lcom/stagnationlab/sk/HelpFragment;->a(II)V

    const v0, 0x7f080062

    const v1, 0x7f0e00c4

    .line 103
    invoke-direct {p0, v0, v1}, Lcom/stagnationlab/sk/HelpFragment;->a(II)V

    const v0, 0x7f080057

    const v1, 0x7f0e00be

    .line 104
    invoke-direct {p0, v0, v1}, Lcom/stagnationlab/sk/HelpFragment;->a(II)V

    const v0, 0x7f08008b

    const v1, 0x7f0e0113

    .line 105
    invoke-direct {p0, v0, v1}, Lcom/stagnationlab/sk/HelpFragment;->a(II)V

    const/4 v0, 0x3

    .line 106
    new-array v1, v0, [Ljava/lang/Object;

    .line 109
    invoke-static {}, Lcom/stagnationlab/sk/util/b;->a()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const/4 v2, 0x1

    const-string v4, ""

    aput-object v4, v1, v2

    .line 111
    invoke-static {}, Lcom/stagnationlab/sk/util/b;->b()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x2

    aput-object v4, v1, v5

    const-string v4, "App %s%s / Lib %s"

    .line 107
    invoke-static {v4, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const v4, 0x7f0800de

    .line 106
    invoke-direct {p0, v4, v1}, Lcom/stagnationlab/sk/HelpFragment;->a(ILjava/lang/String;)V

    const/4 v1, -0x1

    .line 115
    iput v1, p0, Lcom/stagnationlab/sk/HelpFragment;->r:I

    .line 118
    invoke-static {}, Lcom/stagnationlab/sk/MyApplication;->c()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->hashCode()I

    move-result v6

    const/16 v7, 0xcaf

    if-eq v6, v7, :cond_2

    const/16 v7, 0xd88

    if-eq v6, v7, :cond_1

    const/16 v7, 0xd8a

    if-eq v6, v7, :cond_0

    goto :goto_0

    :cond_0
    const-string v6, "lv"

    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    const/4 v4, 0x1

    goto :goto_1

    :cond_1
    const-string v6, "lt"

    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    const/4 v4, 0x2

    goto :goto_1

    :cond_2
    const-string v6, "et"

    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    const/4 v4, 0x0

    goto :goto_1

    :cond_3
    :goto_0
    const/4 v4, -0x1

    :goto_1
    if-eqz v4, :cond_6

    if-eq v4, v2, :cond_5

    if-eq v4, v5, :cond_4

    .line 132
    invoke-direct {p0}, Lcom/stagnationlab/sk/HelpFragment;->d()V

    .line 133
    invoke-direct {p0}, Lcom/stagnationlab/sk/HelpFragment;->e()V

    .line 134
    invoke-direct {p0}, Lcom/stagnationlab/sk/HelpFragment;->f()V

    goto :goto_3

    .line 128
    :cond_4
    invoke-direct {p0}, Lcom/stagnationlab/sk/HelpFragment;->f()V

    goto :goto_2

    .line 124
    :cond_5
    invoke-direct {p0}, Lcom/stagnationlab/sk/HelpFragment;->e()V

    goto :goto_2

    .line 120
    :cond_6
    invoke-direct {p0}, Lcom/stagnationlab/sk/HelpFragment;->d()V

    :goto_2
    const/4 v0, 0x1

    .line 139
    :goto_3
    iget-object v2, p0, Lcom/stagnationlab/sk/HelpFragment;->o:Landroid/widget/TableLayout;

    invoke-virtual {v2}, Landroid/widget/TableLayout;->getChildCount()I

    move-result v2

    div-int/2addr v2, v0

    .line 141
    iget v4, p0, Lcom/stagnationlab/sk/HelpFragment;->r:I

    if-eq v4, v1, :cond_7

    :goto_4
    if-ge v3, v0, :cond_7

    mul-int v1, v3, v2

    add-int/lit8 v4, v1, 0x1

    .line 143
    invoke-virtual {p0, v4}, Lcom/stagnationlab/sk/HelpFragment;->a(I)V

    add-int/2addr v1, v5

    .line 144
    invoke-virtual {p0, v1}, Lcom/stagnationlab/sk/HelpFragment;->a(I)V

    add-int/lit8 v3, v3, 0x1

    goto :goto_4

    :cond_7
    return-void
.end method

.method private d(I)Landroid/view/View;
    .locals 1

    .line 301
    iget-object v0, p0, Lcom/stagnationlab/sk/HelpFragment;->q:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    return-object p1
.end method

.method private d()V
    .locals 6

    const-string v1, "Estonia"

    const v2, 0x7f0e00bd

    const-string v3, "help.ee@smart-id.com"

    const/4 v4, 0x0

    const-string v5, "+372 715 1606"

    move-object v0, p0

    .line 150
    invoke-direct/range {v0 .. v5}, Lcom/stagnationlab/sk/HelpFragment;->a(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private e()V
    .locals 6

    const-string v1, "Latvia"

    const v2, 0x7f0e00f3

    const-string v3, "help.lv@smart-id.com"

    const-string v4, "1807"

    const-string v5, "+371 6766 5001"

    move-object v0, p0

    .line 154
    invoke-direct/range {v0 .. v5}, Lcom/stagnationlab/sk/HelpFragment;->a(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private f()V
    .locals 6

    const-string v1, "Lithuania"

    const v2, 0x7f0e00f4

    const-string v3, "help.lt@smart-id.com"

    const-string v4, "1807"

    const-string v5, "+370 6704 1807"

    move-object v0, p0

    .line 159
    invoke-direct/range {v0 .. v5}, Lcom/stagnationlab/sk/HelpFragment;->a(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private g()Landroid/widget/TableRow;
    .locals 4

    .line 189
    iget-object v0, p0, Lcom/stagnationlab/sk/HelpFragment;->p:Landroid/view/LayoutInflater;

    iget-object v1, p0, Lcom/stagnationlab/sk/HelpFragment;->o:Landroid/widget/TableLayout;

    const v2, 0x7f0a0023

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TableRow;

    return-object v0
.end method

.method private h()Ljava/lang/String;
    .locals 6

    .line 263
    invoke-static {}, Lcom/stagnationlab/sk/MyApplication;->c()Ljava/lang/String;

    move-result-object v0

    .line 265
    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v1

    const/16 v2, 0xcaf

    const/4 v3, 0x3

    const/4 v4, 0x2

    const/4 v5, 0x1

    if-eq v1, v2, :cond_3

    const/16 v2, 0xd88

    if-eq v1, v2, :cond_2

    const/16 v2, 0xd8a

    if-eq v1, v2, :cond_1

    const/16 v2, 0xe43

    if-eq v1, v2, :cond_0

    goto :goto_0

    :cond_0
    const-string v1, "ru"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x3

    goto :goto_1

    :cond_1
    const-string v1, "lv"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x1

    goto :goto_1

    :cond_2
    const-string v1, "lt"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x2

    goto :goto_1

    :cond_3
    const-string v1, "et"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x0

    goto :goto_1

    :cond_4
    :goto_0
    const/4 v0, -0x1

    :goto_1
    if-eqz v0, :cond_8

    if-eq v0, v5, :cond_7

    if-eq v0, v4, :cond_6

    if-eq v0, v3, :cond_5

    const-string v0, "https://www.smart-id.com/help/faq/"

    return-object v0

    :cond_5
    const-string v0, "https://www.smart-id.com/ru/spravka/voprosy-otvety/"

    return-object v0

    :cond_6
    const-string v0, "https://www.smart-id.com/lt/pagalba/duk/"

    return-object v0

    :cond_7
    const-string v0, "https://www.smart-id.com/lv/palidziba/buj/"

    return-object v0

    :cond_8
    const-string v0, "https://www.smart-id.com/et/abi/kkk/"

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 1

    .line 296
    iget-object v0, p0, Lcom/stagnationlab/sk/HelpFragment;->o:Landroid/widget/TableLayout;

    invoke-virtual {v0}, Landroid/widget/TableLayout;->removeAllViews()V

    .line 297
    invoke-direct {p0}, Lcom/stagnationlab/sk/HelpFragment;->c()V

    return-void
.end method

.method public a(I)V
    .locals 1

    .line 287
    iget-object v0, p0, Lcom/stagnationlab/sk/HelpFragment;->o:Landroid/widget/TableLayout;

    invoke-virtual {v0, p1}, Landroid/widget/TableLayout;->getChildAt(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TableRow;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/widget/TableRow;->getChildAt(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    .line 288
    iget v0, p0, Lcom/stagnationlab/sk/HelpFragment;->r:I

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setWidth(I)V

    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 0

    return-void
.end method

.method public a(Z)V
    .locals 1

    if-eqz p1, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    :cond_0
    const/16 p1, 0x8

    .line 282
    :goto_0
    iget-object v0, p0, Lcom/stagnationlab/sk/HelpFragment;->t:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, p1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 283
    iget-object v0, p0, Lcom/stagnationlab/sk/HelpFragment;->u:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .line 50
    invoke-super {p0, p3}, Landroid/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 52
    invoke-virtual {p0}, Lcom/stagnationlab/sk/HelpFragment;->getActivity()Landroid/app/Activity;

    move-result-object p3

    check-cast p3, Lcom/stagnationlab/sk/c;

    iput-object p3, p0, Lcom/stagnationlab/sk/HelpFragment;->n:Lcom/stagnationlab/sk/c;

    .line 54
    new-instance p3, Landroid/view/ContextThemeWrapper;

    invoke-virtual {p0}, Lcom/stagnationlab/sk/HelpFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const v1, 0x7f0f0007

    invoke-direct {p3, v0, v1}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    iput-object p3, p0, Lcom/stagnationlab/sk/HelpFragment;->s:Landroid/content/Context;

    .line 55
    iget-object p3, p0, Lcom/stagnationlab/sk/HelpFragment;->s:Landroid/content/Context;

    invoke-virtual {p1, p3}, Landroid/view/LayoutInflater;->cloneInContext(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object p1

    iput-object p1, p0, Lcom/stagnationlab/sk/HelpFragment;->p:Landroid/view/LayoutInflater;

    .line 56
    iget-object p1, p0, Lcom/stagnationlab/sk/HelpFragment;->p:Landroid/view/LayoutInflater;

    const p3, 0x7f0a0025

    const/4 v0, 0x0

    invoke-virtual {p1, p3, p2, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lcom/stagnationlab/sk/HelpFragment;->q:Landroid/view/View;

    const p1, 0x7f0800c7

    .line 57
    invoke-direct {p0, p1}, Lcom/stagnationlab/sk/HelpFragment;->d(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TableLayout;

    iput-object p1, p0, Lcom/stagnationlab/sk/HelpFragment;->o:Landroid/widget/TableLayout;

    const p1, 0x7f080063

    .line 58
    invoke-direct {p0, p1}, Lcom/stagnationlab/sk/HelpFragment;->d(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/RelativeLayout;

    iput-object p1, p0, Lcom/stagnationlab/sk/HelpFragment;->t:Landroid/widget/RelativeLayout;

    const p1, 0x7f080064

    .line 59
    invoke-direct {p0, p1}, Lcom/stagnationlab/sk/HelpFragment;->d(I)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lcom/stagnationlab/sk/HelpFragment;->u:Landroid/view/View;

    .line 61
    invoke-direct {p0}, Lcom/stagnationlab/sk/HelpFragment;->b()V

    .line 62
    invoke-direct {p0}, Lcom/stagnationlab/sk/HelpFragment;->c()V

    .line 64
    iget-object p1, p0, Lcom/stagnationlab/sk/HelpFragment;->q:Landroid/view/View;

    return-object p1
.end method
