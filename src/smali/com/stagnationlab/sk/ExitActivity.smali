.class public Lcom/stagnationlab/sk/ExitActivity;
.super Landroid/app/Activity;
.source "ExitActivity.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 8
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method

.method public static a(Landroid/app/Activity;)V
    .locals 2

    .line 21
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/stagnationlab/sk/ExitActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 22
    invoke-static {v0}, Lcom/stagnationlab/sk/ExitActivity;->a(Landroid/content/Intent;)V

    .line 23
    invoke-virtual {p0, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method public static a(Landroid/content/Intent;)V
    .locals 1

    const v0, 0x10818000

    .line 27
    invoke-virtual {p0, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1

    .line 11
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 13
    sget p1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v0, 0x15

    if-lt p1, v0, :cond_0

    .line 14
    invoke-virtual {p0}, Lcom/stagnationlab/sk/ExitActivity;->finishAndRemoveTask()V

    goto :goto_0

    .line 16
    :cond_0
    invoke-virtual {p0}, Lcom/stagnationlab/sk/ExitActivity;->finish()V

    :goto_0
    return-void
.end method
