.class public Lcom/stagnationlab/sk/c;
.super Lcom/stagnationlab/sk/b;
.source "BaseHelpActivity.java"


# instance fields
.field private c:Landroid/support/v4/widget/DrawerLayout;

.field private d:Lcom/stagnationlab/sk/HelpFragment;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 10
    invoke-direct {p0}, Lcom/stagnationlab/sk/b;-><init>()V

    return-void
.end method


# virtual methods
.method protected a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Context;
    .locals 0

    .line 35
    invoke-super {p0, p1, p2}, Lcom/stagnationlab/sk/b;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Context;

    move-result-object p1

    .line 37
    iget-object p2, p0, Lcom/stagnationlab/sk/c;->d:Lcom/stagnationlab/sk/HelpFragment;

    if-eqz p2, :cond_0

    .line 38
    invoke-virtual {p2}, Lcom/stagnationlab/sk/HelpFragment;->a()V

    :cond_0
    return-object p1
.end method

.method public a(Ljava/lang/String;)V
    .locals 2

    .line 53
    invoke-static {p0}, Lcom/stagnationlab/sk/i;->c(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "Q"

    goto :goto_0

    :cond_0
    const-string v0, "NQ"

    :goto_0
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1, p1}, Lcom/stagnationlab/sk/c;->a(Ljava/lang/String;ZLjava/lang/String;)V

    return-void
.end method

.method public a(Ljava/lang/String;ZLjava/lang/String;)V
    .locals 1

    .line 59
    iget-object v0, p0, Lcom/stagnationlab/sk/c;->d:Lcom/stagnationlab/sk/HelpFragment;

    invoke-virtual {v0, p2}, Lcom/stagnationlab/sk/HelpFragment;->a(Z)V

    .line 60
    iget-object p2, p0, Lcom/stagnationlab/sk/c;->d:Lcom/stagnationlab/sk/HelpFragment;

    invoke-virtual {p2, p3}, Lcom/stagnationlab/sk/HelpFragment;->a(Ljava/lang/String;)V

    .line 62
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result p2

    const/16 p3, 0x9c3

    const/4 v0, 0x1

    if-eq p2, p3, :cond_1

    const p3, 0x27afc5f7

    if-eq p2, p3, :cond_0

    goto :goto_0

    :cond_0
    const-string p2, "LANDING"

    invoke-virtual {p1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    const/4 p1, 0x0

    goto :goto_1

    :cond_1
    const-string p2, "NQ"

    invoke-virtual {p1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    const/4 p1, 0x1

    goto :goto_1

    :cond_2
    :goto_0
    const/4 p1, -0x1

    :goto_1
    if-eqz p1, :cond_4

    if-eq p1, v0, :cond_3

    const p1, 0x7f05002e

    goto :goto_2

    :cond_3
    const p1, 0x7f05002d

    goto :goto_2

    :cond_4
    const p1, 0x7f05002c

    .line 72
    :goto_2
    iget-object p2, p0, Lcom/stagnationlab/sk/c;->c:Landroid/support/v4/widget/DrawerLayout;

    invoke-virtual {p0}, Lcom/stagnationlab/sk/c;->getResources()Landroid/content/res/Resources;

    move-result-object p3

    const/4 v0, 0x0

    invoke-static {p3, p1, v0}, Landroid/support/v4/content/a/f;->b(Landroid/content/res/Resources;ILandroid/content/res/Resources$Theme;)I

    move-result p1

    invoke-virtual {p2, p1}, Landroid/support/v4/widget/DrawerLayout;->setScrimColor(I)V

    .line 73
    iget-object p1, p0, Lcom/stagnationlab/sk/c;->c:Landroid/support/v4/widget/DrawerLayout;

    const/4 p2, 0x5

    invoke-virtual {p1, p2}, Landroid/support/v4/widget/DrawerLayout;->e(I)V

    return-void
.end method

.method public g()Landroid/content/Context;
    .locals 1

    .line 45
    iget-object v0, p0, Lcom/stagnationlab/sk/c;->b:Landroid/content/Context;

    return-object v0
.end method

.method public h()V
    .locals 1

    const/4 v0, 0x0

    .line 49
    invoke-virtual {p0, v0}, Lcom/stagnationlab/sk/c;->a(Ljava/lang/String;)V

    return-void
.end method

.method public i()V
    .locals 2

    .line 77
    iget-object v0, p0, Lcom/stagnationlab/sk/c;->c:Landroid/support/v4/widget/DrawerLayout;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/DrawerLayout;->f(I)V

    return-void
.end method

.method public j()V
    .locals 0

    return-void
.end method

.method public setContentView(I)V
    .locals 2

    const v0, 0x7f0a001c

    .line 20
    invoke-super {p0, v0}, Lcom/stagnationlab/sk/b;->setContentView(I)V

    const v0, 0x7f08003d

    .line 22
    invoke-virtual {p0, v0}, Lcom/stagnationlab/sk/c;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-static {p0, p1, v0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    const p1, 0x7f08004b

    .line 24
    invoke-virtual {p0, p1}, Lcom/stagnationlab/sk/c;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/support/v4/widget/DrawerLayout;

    iput-object p1, p0, Lcom/stagnationlab/sk/c;->c:Landroid/support/v4/widget/DrawerLayout;

    .line 25
    iget-object p1, p0, Lcom/stagnationlab/sk/c;->c:Landroid/support/v4/widget/DrawerLayout;

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/support/v4/widget/DrawerLayout;->setDrawerLockMode(I)V

    .line 28
    iget-object p1, p0, Lcom/stagnationlab/sk/c;->c:Landroid/support/v4/widget/DrawerLayout;

    const v0, 0x7f070074

    const/4 v1, 0x5

    invoke-virtual {p1, v0, v1}, Landroid/support/v4/widget/DrawerLayout;->a(II)V

    .line 30
    invoke-virtual {p0}, Lcom/stagnationlab/sk/c;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object p1

    const v0, 0x7f08005e

    invoke-virtual {p1, v0}, Landroid/app/FragmentManager;->findFragmentById(I)Landroid/app/Fragment;

    move-result-object p1

    check-cast p1, Lcom/stagnationlab/sk/HelpFragment;

    iput-object p1, p0, Lcom/stagnationlab/sk/c;->d:Lcom/stagnationlab/sk/HelpFragment;

    return-void
.end method
