.class final enum Lcom/stagnationlab/sk/models/AccountKey$Status;
.super Ljava/lang/Enum;
.source "AccountKey.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/stagnationlab/sk/models/AccountKey;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "Status"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/stagnationlab/sk/models/AccountKey$Status;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/stagnationlab/sk/models/AccountKey$Status;

.field public static final enum EXPIRED:Lcom/stagnationlab/sk/models/AccountKey$Status;

.field public static final enum IN_PREPARATION:Lcom/stagnationlab/sk/models/AccountKey$Status;

.field public static final enum LOCKED:Lcom/stagnationlab/sk/models/AccountKey$Status;

.field public static final enum OK:Lcom/stagnationlab/sk/models/AccountKey$Status;

.field public static final enum REVOKED:Lcom/stagnationlab/sk/models/AccountKey$Status;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .line 77
    new-instance v0, Lcom/stagnationlab/sk/models/AccountKey$Status;

    const/4 v1, 0x0

    const-string v2, "OK"

    invoke-direct {v0, v2, v1}, Lcom/stagnationlab/sk/models/AccountKey$Status;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/stagnationlab/sk/models/AccountKey$Status;->OK:Lcom/stagnationlab/sk/models/AccountKey$Status;

    new-instance v0, Lcom/stagnationlab/sk/models/AccountKey$Status;

    const/4 v2, 0x1

    const-string v3, "IN_PREPARATION"

    invoke-direct {v0, v3, v2}, Lcom/stagnationlab/sk/models/AccountKey$Status;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/stagnationlab/sk/models/AccountKey$Status;->IN_PREPARATION:Lcom/stagnationlab/sk/models/AccountKey$Status;

    new-instance v0, Lcom/stagnationlab/sk/models/AccountKey$Status;

    const/4 v3, 0x2

    const-string v4, "LOCKED"

    invoke-direct {v0, v4, v3}, Lcom/stagnationlab/sk/models/AccountKey$Status;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/stagnationlab/sk/models/AccountKey$Status;->LOCKED:Lcom/stagnationlab/sk/models/AccountKey$Status;

    new-instance v0, Lcom/stagnationlab/sk/models/AccountKey$Status;

    const/4 v4, 0x3

    const-string v5, "EXPIRED"

    invoke-direct {v0, v5, v4}, Lcom/stagnationlab/sk/models/AccountKey$Status;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/stagnationlab/sk/models/AccountKey$Status;->EXPIRED:Lcom/stagnationlab/sk/models/AccountKey$Status;

    new-instance v0, Lcom/stagnationlab/sk/models/AccountKey$Status;

    const/4 v5, 0x4

    const-string v6, "REVOKED"

    invoke-direct {v0, v6, v5}, Lcom/stagnationlab/sk/models/AccountKey$Status;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/stagnationlab/sk/models/AccountKey$Status;->REVOKED:Lcom/stagnationlab/sk/models/AccountKey$Status;

    const/4 v0, 0x5

    .line 76
    new-array v0, v0, [Lcom/stagnationlab/sk/models/AccountKey$Status;

    sget-object v6, Lcom/stagnationlab/sk/models/AccountKey$Status;->OK:Lcom/stagnationlab/sk/models/AccountKey$Status;

    aput-object v6, v0, v1

    sget-object v1, Lcom/stagnationlab/sk/models/AccountKey$Status;->IN_PREPARATION:Lcom/stagnationlab/sk/models/AccountKey$Status;

    aput-object v1, v0, v2

    sget-object v1, Lcom/stagnationlab/sk/models/AccountKey$Status;->LOCKED:Lcom/stagnationlab/sk/models/AccountKey$Status;

    aput-object v1, v0, v3

    sget-object v1, Lcom/stagnationlab/sk/models/AccountKey$Status;->EXPIRED:Lcom/stagnationlab/sk/models/AccountKey$Status;

    aput-object v1, v0, v4

    sget-object v1, Lcom/stagnationlab/sk/models/AccountKey$Status;->REVOKED:Lcom/stagnationlab/sk/models/AccountKey$Status;

    aput-object v1, v0, v5

    sput-object v0, Lcom/stagnationlab/sk/models/AccountKey$Status;->$VALUES:[Lcom/stagnationlab/sk/models/AccountKey$Status;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 76
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/stagnationlab/sk/models/AccountKey$Status;
    .locals 1

    .line 76
    const-class v0, Lcom/stagnationlab/sk/models/AccountKey$Status;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/stagnationlab/sk/models/AccountKey$Status;

    return-object p0
.end method

.method public static values()[Lcom/stagnationlab/sk/models/AccountKey$Status;
    .locals 1

    .line 76
    sget-object v0, Lcom/stagnationlab/sk/models/AccountKey$Status;->$VALUES:[Lcom/stagnationlab/sk/models/AccountKey$Status;

    invoke-virtual {v0}, [Lcom/stagnationlab/sk/models/AccountKey$Status;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/stagnationlab/sk/models/AccountKey$Status;

    return-object v0
.end method
