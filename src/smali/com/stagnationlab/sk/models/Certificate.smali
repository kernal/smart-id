.class public Lcom/stagnationlab/sk/models/Certificate;
.super Ljava/lang/Object;
.source "Certificate.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/stagnationlab/sk/models/Certificate$Type;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/stagnationlab/sk/models/Certificate;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private isQscd:Z

.field private subject:Ljava/util/Map;

.field private type:Lcom/stagnationlab/sk/models/Certificate$Type;

.field private validSince:Lorg/b/a/b;

.field private validUntil:Lorg/b/a/b;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 29
    new-instance v0, Lcom/stagnationlab/sk/models/Certificate$1;

    invoke-direct {v0}, Lcom/stagnationlab/sk/models/Certificate$1;-><init>()V

    sput-object v0, Lcom/stagnationlab/sk/models/Certificate;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    invoke-static {p1}, Lcom/stagnationlab/sk/i;->a(Landroid/os/Parcel;)Lorg/b/a/b;

    move-result-object v0

    iput-object v0, p0, Lcom/stagnationlab/sk/models/Certificate;->validSince:Lorg/b/a/b;

    .line 48
    invoke-static {p1}, Lcom/stagnationlab/sk/i;->a(Landroid/os/Parcel;)Lorg/b/a/b;

    move-result-object v0

    iput-object v0, p0, Lcom/stagnationlab/sk/models/Certificate;->validUntil:Lorg/b/a/b;

    .line 49
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/stagnationlab/sk/models/Certificate$Type;->valueOf(Ljava/lang/String;)Lcom/stagnationlab/sk/models/Certificate$Type;

    move-result-object v0

    iput-object v0, p0, Lcom/stagnationlab/sk/models/Certificate;->type:Lcom/stagnationlab/sk/models/Certificate$Type;

    .line 50
    invoke-static {p1}, Lcom/stagnationlab/sk/i;->b(Landroid/os/Parcel;)Z

    move-result p1

    iput-boolean p1, p0, Lcom/stagnationlab/sk/models/Certificate;->isQscd:Z

    return-void
.end method

.method public constructor <init>(Lee/cyber/smartid/dto/jsonrpc/ServiceAccountKeyCertificate;)V
    .locals 2

    .line 65
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 66
    invoke-virtual {p1}, Lee/cyber/smartid/dto/jsonrpc/ServiceAccountKeyCertificate;->getType()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ADVANCED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/stagnationlab/sk/models/Certificate$Type;->ADVANCED:Lcom/stagnationlab/sk/models/Certificate$Type;

    goto :goto_0

    :cond_0
    sget-object v0, Lcom/stagnationlab/sk/models/Certificate$Type;->QUALIFIED:Lcom/stagnationlab/sk/models/Certificate$Type;

    :goto_0
    iput-object v0, p0, Lcom/stagnationlab/sk/models/Certificate;->type:Lcom/stagnationlab/sk/models/Certificate$Type;

    .line 68
    new-instance v0, Lorg/b/a/b;

    invoke-virtual {p1}, Lee/cyber/smartid/dto/jsonrpc/ServiceAccountKeyCertificate;->getValidSince()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/b/a/b;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/stagnationlab/sk/models/Certificate;->validSince:Lorg/b/a/b;

    .line 69
    new-instance v0, Lorg/b/a/b;

    invoke-virtual {p1}, Lee/cyber/smartid/dto/jsonrpc/ServiceAccountKeyCertificate;->getValidUntil()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/b/a/b;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/stagnationlab/sk/models/Certificate;->validUntil:Lorg/b/a/b;

    .line 70
    invoke-virtual {p1}, Lee/cyber/smartid/dto/jsonrpc/ServiceAccountKeyCertificate;->isQSCDKeyPair()Z

    move-result v0

    iput-boolean v0, p0, Lcom/stagnationlab/sk/models/Certificate;->isQscd:Z

    .line 71
    new-instance v0, Lcom/google/gson/f;

    invoke-direct {v0}, Lcom/google/gson/f;-><init>()V

    invoke-virtual {p1}, Lee/cyber/smartid/dto/jsonrpc/ServiceAccountKeyCertificate;->getSubject()Ljava/lang/String;

    move-result-object p1

    const-class v1, Ljava/util/Map;

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/f;->a(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/Map;

    iput-object p1, p0, Lcom/stagnationlab/sk/models/Certificate;->subject:Ljava/util/Map;

    return-void
.end method


# virtual methods
.method a()Ljava/util/Map;
    .locals 1

    .line 26
    iget-object v0, p0, Lcom/stagnationlab/sk/models/Certificate;->subject:Ljava/util/Map;

    return-object v0
.end method

.method b()Z
    .locals 2

    .line 75
    iget-object v0, p0, Lcom/stagnationlab/sk/models/Certificate;->type:Lcom/stagnationlab/sk/models/Certificate$Type;

    sget-object v1, Lcom/stagnationlab/sk/models/Certificate$Type;->QUALIFIED:Lcom/stagnationlab/sk/models/Certificate$Type;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .line 55
    iget-object p2, p0, Lcom/stagnationlab/sk/models/Certificate;->validSince:Lorg/b/a/b;

    invoke-static {p1, p2}, Lcom/stagnationlab/sk/i;->a(Landroid/os/Parcel;Lorg/b/a/b;)V

    .line 56
    iget-object p2, p0, Lcom/stagnationlab/sk/models/Certificate;->validUntil:Lorg/b/a/b;

    invoke-static {p1, p2}, Lcom/stagnationlab/sk/i;->a(Landroid/os/Parcel;Lorg/b/a/b;)V

    .line 57
    iget-object p2, p0, Lcom/stagnationlab/sk/models/Certificate;->type:Lcom/stagnationlab/sk/models/Certificate$Type;

    invoke-virtual {p2}, Lcom/stagnationlab/sk/models/Certificate$Type;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 58
    iget-boolean p2, p0, Lcom/stagnationlab/sk/models/Certificate;->isQscd:Z

    invoke-static {p1, p2}, Lcom/stagnationlab/sk/i;->a(Landroid/os/Parcel;Z)V

    return-void
.end method
