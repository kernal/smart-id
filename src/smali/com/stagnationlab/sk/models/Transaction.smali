.class public Lcom/stagnationlab/sk/models/Transaction;
.super Lcom/stagnationlab/sk/models/AbstractTransaction;
.source "Transaction.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/stagnationlab/sk/models/Transaction$ResultCode;,
        Lcom/stagnationlab/sk/models/Transaction$State;,
        Lcom/stagnationlab/sk/models/Transaction$Type;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/stagnationlab/sk/models/Transaction;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private displayText:Ljava/lang/String;

.field private fake:Z

.field private relayingPartyName:Ljava/lang/String;

.field private resultCode:Lcom/stagnationlab/sk/models/Transaction$ResultCode;

.field private state:Lcom/stagnationlab/sk/models/Transaction$State;

.field private type:Lcom/stagnationlab/sk/models/Transaction$Type;

.field private useMultiCodeVerification:Z

.field private uuid:Ljava/lang/String;

.field private verificationCode:Ljava/lang/String;

.field private verificationCodes:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 23
    new-instance v0, Lcom/stagnationlab/sk/models/Transaction$1;

    invoke-direct {v0}, Lcom/stagnationlab/sk/models/Transaction$1;-><init>()V

    sput-object v0, Lcom/stagnationlab/sk/models/Transaction;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    const-wide/16 v0, 0x190

    .line 108
    invoke-direct {p0, v0, v1}, Lcom/stagnationlab/sk/models/AbstractTransaction;-><init>(J)V

    const-string v0, "07f49281-3ff1-4637-83d0-f0aba7ae71d1"

    .line 109
    iput-object v0, p0, Lcom/stagnationlab/sk/models/Transaction;->uuid:Ljava/lang/String;

    .line 110
    sget-object v0, Lcom/stagnationlab/sk/models/Transaction$Type;->AUTHENTICATION:Lcom/stagnationlab/sk/models/Transaction$Type;

    iput-object v0, p0, Lcom/stagnationlab/sk/models/Transaction;->type:Lcom/stagnationlab/sk/models/Transaction$Type;

    .line 111
    sget-object v0, Lcom/stagnationlab/sk/models/Transaction$State;->RUNNING:Lcom/stagnationlab/sk/models/Transaction$State;

    iput-object v0, p0, Lcom/stagnationlab/sk/models/Transaction;->state:Lcom/stagnationlab/sk/models/Transaction$State;

    const/4 v0, 0x0

    .line 112
    iput-object v0, p0, Lcom/stagnationlab/sk/models/Transaction;->resultCode:Lcom/stagnationlab/sk/models/Transaction$ResultCode;

    const-string v1, "Fake transaction"

    .line 113
    iput-object v1, p0, Lcom/stagnationlab/sk/models/Transaction;->relayingPartyName:Ljava/lang/String;

    const-string v1, "Lorem impsum dolor sit amet, lotral om se consectetur adipiscing elit."

    .line 114
    iput-object v1, p0, Lcom/stagnationlab/sk/models/Transaction;->displayText:Ljava/lang/String;

    .line 115
    iput-object v0, p0, Lcom/stagnationlab/sk/models/Transaction;->verificationCode:Ljava/lang/String;

    const/4 v0, 0x1

    .line 116
    iput-boolean v0, p0, Lcom/stagnationlab/sk/models/Transaction;->fake:Z

    const/4 v0, 0x0

    .line 117
    iput-boolean v0, p0, Lcom/stagnationlab/sk/models/Transaction;->useMultiCodeVerification:Z

    const-string v0, "4444"

    .line 118
    filled-new-array {v0}, [Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/stagnationlab/sk/models/Transaction;->verificationCodes:[Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .line 34
    invoke-direct {p0, p1}, Lcom/stagnationlab/sk/models/AbstractTransaction;-><init>(Landroid/os/Parcel;)V

    .line 35
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/stagnationlab/sk/models/Transaction;->uuid:Ljava/lang/String;

    .line 36
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/stagnationlab/sk/models/Transaction;->relayingPartyName:Ljava/lang/String;

    .line 37
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/stagnationlab/sk/models/Transaction$Type;->valueOf(Ljava/lang/String;)Lcom/stagnationlab/sk/models/Transaction$Type;

    move-result-object v0

    iput-object v0, p0, Lcom/stagnationlab/sk/models/Transaction;->type:Lcom/stagnationlab/sk/models/Transaction$Type;

    .line 38
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/stagnationlab/sk/models/Transaction$State;->valueOf(Ljava/lang/String;)Lcom/stagnationlab/sk/models/Transaction$State;

    move-result-object v0

    iput-object v0, p0, Lcom/stagnationlab/sk/models/Transaction;->state:Lcom/stagnationlab/sk/models/Transaction$State;

    .line 39
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/stagnationlab/sk/models/Transaction;->b(Ljava/lang/String;)Lcom/stagnationlab/sk/models/Transaction$ResultCode;

    move-result-object v0

    iput-object v0, p0, Lcom/stagnationlab/sk/models/Transaction;->resultCode:Lcom/stagnationlab/sk/models/Transaction$ResultCode;

    .line 40
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/stagnationlab/sk/models/Transaction;->displayText:Ljava/lang/String;

    .line 41
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/stagnationlab/sk/models/Transaction;->verificationCode:Ljava/lang/String;

    .line 42
    invoke-static {p1}, Lcom/stagnationlab/sk/i;->b(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/stagnationlab/sk/models/Transaction;->fake:Z

    .line 43
    invoke-static {p1}, Lcom/stagnationlab/sk/i;->b(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/stagnationlab/sk/models/Transaction;->useMultiCodeVerification:Z

    .line 45
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 46
    new-array v0, v0, [Ljava/lang/String;

    iput-object v0, p0, Lcom/stagnationlab/sk/models/Transaction;->verificationCodes:[Ljava/lang/String;

    .line 47
    iget-object v0, p0, Lcom/stagnationlab/sk/models/Transaction;->verificationCodes:[Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readStringArray([Ljava/lang/String;)V

    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/stagnationlab/sk/models/Transaction$1;)V
    .locals 0

    .line 11
    invoke-direct {p0, p1}, Lcom/stagnationlab/sk/models/Transaction;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public constructor <init>(Lee/cyber/smartid/dto/jsonrpc/resp/GetTransactionResp;)V
    .locals 2

    .line 93
    invoke-virtual {p1}, Lee/cyber/smartid/dto/jsonrpc/resp/GetTransactionResp;->getTtlSec()J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lcom/stagnationlab/sk/models/AbstractTransaction;-><init>(J)V

    .line 94
    invoke-virtual {p1}, Lee/cyber/smartid/dto/jsonrpc/resp/GetTransactionResp;->getTransactionUUID()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/stagnationlab/sk/models/Transaction;->uuid:Ljava/lang/String;

    .line 95
    invoke-virtual {p1}, Lee/cyber/smartid/dto/jsonrpc/resp/GetTransactionResp;->getRpName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/stagnationlab/sk/models/Transaction;->relayingPartyName:Ljava/lang/String;

    .line 96
    invoke-virtual {p1}, Lee/cyber/smartid/dto/jsonrpc/resp/GetTransactionResp;->getTransactionType()Ljava/lang/String;

    move-result-object v0

    const-string v1, "AUTHENTICATION"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/stagnationlab/sk/models/Transaction$Type;->AUTHENTICATION:Lcom/stagnationlab/sk/models/Transaction$Type;

    goto :goto_0

    :cond_0
    sget-object v0, Lcom/stagnationlab/sk/models/Transaction$Type;->SIGNATURE:Lcom/stagnationlab/sk/models/Transaction$Type;

    :goto_0
    iput-object v0, p0, Lcom/stagnationlab/sk/models/Transaction;->type:Lcom/stagnationlab/sk/models/Transaction$Type;

    .line 99
    invoke-virtual {p1}, Lee/cyber/smartid/dto/jsonrpc/resp/GetTransactionResp;->getState()Ljava/lang/String;

    move-result-object v0

    const-string v1, "COMPLETE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lcom/stagnationlab/sk/models/Transaction$State;->COMPLETE:Lcom/stagnationlab/sk/models/Transaction$State;

    goto :goto_1

    :cond_1
    sget-object v0, Lcom/stagnationlab/sk/models/Transaction$State;->RUNNING:Lcom/stagnationlab/sk/models/Transaction$State;

    :goto_1
    iput-object v0, p0, Lcom/stagnationlab/sk/models/Transaction;->state:Lcom/stagnationlab/sk/models/Transaction$State;

    .line 100
    invoke-virtual {p1}, Lee/cyber/smartid/dto/jsonrpc/resp/GetTransactionResp;->getResultCode()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/stagnationlab/sk/models/Transaction;->b(Ljava/lang/String;)Lcom/stagnationlab/sk/models/Transaction$ResultCode;

    move-result-object v0

    iput-object v0, p0, Lcom/stagnationlab/sk/models/Transaction;->resultCode:Lcom/stagnationlab/sk/models/Transaction$ResultCode;

    .line 101
    invoke-virtual {p1}, Lee/cyber/smartid/dto/jsonrpc/resp/GetTransactionResp;->getDisplayText()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/stagnationlab/sk/models/Transaction;->displayText:Ljava/lang/String;

    .line 102
    invoke-virtual {p1}, Lee/cyber/smartid/dto/jsonrpc/resp/GetTransactionResp;->getVerificationCodes()[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/stagnationlab/sk/models/Transaction;->verificationCodes:[Ljava/lang/String;

    .line 103
    invoke-virtual {p1}, Lee/cyber/smartid/dto/jsonrpc/resp/GetTransactionResp;->isMultiCodeVerification()Z

    move-result p1

    iput-boolean p1, p0, Lcom/stagnationlab/sk/models/Transaction;->useMultiCodeVerification:Z

    .line 104
    iget-boolean p1, p0, Lcom/stagnationlab/sk/models/Transaction;->useMultiCodeVerification:Z

    if-eqz p1, :cond_2

    const/4 p1, 0x0

    goto :goto_2

    :cond_2
    iget-object p1, p0, Lcom/stagnationlab/sk/models/Transaction;->verificationCodes:[Ljava/lang/String;

    const/4 v0, 0x0

    aget-object p1, p1, v0

    :goto_2
    iput-object p1, p0, Lcom/stagnationlab/sk/models/Transaction;->verificationCode:Ljava/lang/String;

    return-void
.end method

.method private static b(Ljava/lang/String;)Lcom/stagnationlab/sk/models/Transaction$ResultCode;
    .locals 0

    .line 167
    :try_start_0
    invoke-static {p0}, Lcom/stagnationlab/sk/models/Transaction$ResultCode;->valueOf(Ljava/lang/String;)Lcom/stagnationlab/sk/models/Transaction$ResultCode;

    move-result-object p0
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p0

    :catch_0
    const/4 p0, 0x0

    return-object p0

    .line 169
    :catch_1
    sget-object p0, Lcom/stagnationlab/sk/models/Transaction$ResultCode;->SERVER_CANCELLED:Lcom/stagnationlab/sk/models/Transaction$ResultCode;

    return-object p0
.end method


# virtual methods
.method public a(Ljava/lang/String;)V
    .locals 0

    .line 73
    iput-object p1, p0, Lcom/stagnationlab/sk/models/Transaction;->verificationCode:Ljava/lang/String;

    return-void
.end method

.method public a(Lcom/stagnationlab/sk/models/Transaction;)Z
    .locals 1

    if-eqz p1, :cond_0

    .line 146
    invoke-virtual {p1}, Lcom/stagnationlab/sk/models/Transaction;->f()Ljava/lang/String;

    move-result-object p1

    iget-object v0, p0, Lcom/stagnationlab/sk/models/Transaction;->uuid:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public e()Z
    .locals 1

    .line 77
    iget-boolean v0, p0, Lcom/stagnationlab/sk/models/Transaction;->useMultiCodeVerification:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/stagnationlab/sk/models/Transaction;->verificationCode:Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public f()Ljava/lang/String;
    .locals 1

    .line 122
    iget-object v0, p0, Lcom/stagnationlab/sk/models/Transaction;->uuid:Ljava/lang/String;

    return-object v0
.end method

.method public g()Ljava/lang/String;
    .locals 1

    .line 126
    iget-object v0, p0, Lcom/stagnationlab/sk/models/Transaction;->relayingPartyName:Ljava/lang/String;

    return-object v0
.end method

.method public h()Lcom/stagnationlab/sk/models/Transaction$Type;
    .locals 1

    .line 130
    iget-object v0, p0, Lcom/stagnationlab/sk/models/Transaction;->type:Lcom/stagnationlab/sk/models/Transaction$Type;

    return-object v0
.end method

.method public i()Ljava/lang/String;
    .locals 1

    .line 134
    iget-object v0, p0, Lcom/stagnationlab/sk/models/Transaction;->displayText:Ljava/lang/String;

    return-object v0
.end method

.method public j()Ljava/lang/String;
    .locals 1

    .line 138
    iget-object v0, p0, Lcom/stagnationlab/sk/models/Transaction;->verificationCode:Ljava/lang/String;

    return-object v0
.end method

.method public k()Z
    .locals 1

    .line 142
    iget-boolean v0, p0, Lcom/stagnationlab/sk/models/Transaction;->fake:Z

    return v0
.end method

.method public l()Z
    .locals 2

    .line 150
    iget-object v0, p0, Lcom/stagnationlab/sk/models/Transaction;->type:Lcom/stagnationlab/sk/models/Transaction$Type;

    sget-object v1, Lcom/stagnationlab/sk/models/Transaction$Type;->AUTHENTICATION:Lcom/stagnationlab/sk/models/Transaction$Type;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public m()Z
    .locals 2

    .line 158
    iget-object v0, p0, Lcom/stagnationlab/sk/models/Transaction;->state:Lcom/stagnationlab/sk/models/Transaction$State;

    sget-object v1, Lcom/stagnationlab/sk/models/Transaction$State;->COMPLETE:Lcom/stagnationlab/sk/models/Transaction$State;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public n()[Ljava/lang/String;
    .locals 1

    .line 162
    iget-object v0, p0, Lcom/stagnationlab/sk/models/Transaction;->verificationCodes:[Ljava/lang/String;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 176
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Transaction{uuid=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/stagnationlab/sk/models/Transaction;->uuid:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\', relayingPartyName=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/stagnationlab/sk/models/Transaction;->relayingPartyName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\', type=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/stagnationlab/sk/models/Transaction;->type:Lcom/stagnationlab/sk/models/Transaction$Type;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, "\', state=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/stagnationlab/sk/models/Transaction;->state:Lcom/stagnationlab/sk/models/Transaction$State;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, "\', resultCode=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/stagnationlab/sk/models/Transaction;->resultCode:Lcom/stagnationlab/sk/models/Transaction$ResultCode;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, "\', displayText=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/stagnationlab/sk/models/Transaction;->displayText:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\', verificationCode=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/stagnationlab/sk/models/Transaction;->verificationCode:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\'}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .line 52
    invoke-super {p0, p1, p2}, Lcom/stagnationlab/sk/models/AbstractTransaction;->writeToParcel(Landroid/os/Parcel;I)V

    .line 53
    iget-object p2, p0, Lcom/stagnationlab/sk/models/Transaction;->uuid:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 54
    iget-object p2, p0, Lcom/stagnationlab/sk/models/Transaction;->relayingPartyName:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 55
    iget-object p2, p0, Lcom/stagnationlab/sk/models/Transaction;->type:Lcom/stagnationlab/sk/models/Transaction$Type;

    invoke-virtual {p2}, Lcom/stagnationlab/sk/models/Transaction$Type;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 56
    iget-object p2, p0, Lcom/stagnationlab/sk/models/Transaction;->state:Lcom/stagnationlab/sk/models/Transaction$State;

    invoke-virtual {p2}, Lcom/stagnationlab/sk/models/Transaction$State;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 57
    iget-object p2, p0, Lcom/stagnationlab/sk/models/Transaction;->resultCode:Lcom/stagnationlab/sk/models/Transaction$ResultCode;

    if-nez p2, :cond_0

    const/4 p2, 0x0

    goto :goto_0

    :cond_0
    invoke-virtual {p2}, Lcom/stagnationlab/sk/models/Transaction$ResultCode;->toString()Ljava/lang/String;

    move-result-object p2

    :goto_0
    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 58
    iget-object p2, p0, Lcom/stagnationlab/sk/models/Transaction;->displayText:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 59
    iget-object p2, p0, Lcom/stagnationlab/sk/models/Transaction;->verificationCode:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 60
    iget-boolean p2, p0, Lcom/stagnationlab/sk/models/Transaction;->fake:Z

    invoke-static {p1, p2}, Lcom/stagnationlab/sk/i;->a(Landroid/os/Parcel;Z)V

    .line 61
    iget-boolean p2, p0, Lcom/stagnationlab/sk/models/Transaction;->useMultiCodeVerification:Z

    invoke-static {p1, p2}, Lcom/stagnationlab/sk/i;->a(Landroid/os/Parcel;Z)V

    .line 63
    iget-object p2, p0, Lcom/stagnationlab/sk/models/Transaction;->verificationCodes:[Ljava/lang/String;

    array-length p2, p2

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 64
    iget-object p2, p0, Lcom/stagnationlab/sk/models/Transaction;->verificationCodes:[Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeStringArray([Ljava/lang/String;)V

    return-void
.end method
