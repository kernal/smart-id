.class public abstract Lcom/stagnationlab/sk/models/AbstractTransaction;
.super Ljava/lang/Object;
.source "AbstractTransaction.java"

# interfaces
.implements Landroid/os/Parcelable;


# instance fields
.field private validUntil:Lorg/b/a/b;


# direct methods
.method constructor <init>(J)V
    .locals 1

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    new-instance v0, Lorg/b/a/b;

    invoke-direct {v0}, Lorg/b/a/b;-><init>()V

    long-to-int p2, p1

    invoke-virtual {v0, p2}, Lorg/b/a/b;->a(I)Lorg/b/a/b;

    move-result-object p1

    iput-object p1, p0, Lcom/stagnationlab/sk/models/AbstractTransaction;->validUntil:Lorg/b/a/b;

    return-void
.end method

.method constructor <init>(Landroid/os/Parcel;)V
    .locals 0

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    invoke-static {p1}, Lcom/stagnationlab/sk/i;->a(Landroid/os/Parcel;)Lorg/b/a/b;

    move-result-object p1

    iput-object p1, p0, Lcom/stagnationlab/sk/models/AbstractTransaction;->validUntil:Lorg/b/a/b;

    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .line 27
    new-instance v0, Lorg/b/a/g;

    new-instance v1, Lorg/b/a/b;

    invoke-direct {v1}, Lorg/b/a/b;-><init>()V

    iget-object v2, p0, Lcom/stagnationlab/sk/models/AbstractTransaction;->validUntil:Lorg/b/a/b;

    invoke-direct {v0, v1, v2}, Lorg/b/a/g;-><init>(Lorg/b/a/t;Lorg/b/a/t;)V

    invoke-virtual {v0}, Lorg/b/a/g;->a()J

    move-result-wide v0

    long-to-int v1, v0

    return v1
.end method

.method public b()J
    .locals 3

    .line 31
    new-instance v0, Lorg/b/a/g;

    new-instance v1, Lorg/b/a/b;

    invoke-direct {v1}, Lorg/b/a/b;-><init>()V

    iget-object v2, p0, Lcom/stagnationlab/sk/models/AbstractTransaction;->validUntil:Lorg/b/a/b;

    invoke-direct {v0, v1, v2}, Lorg/b/a/g;-><init>(Lorg/b/a/t;Lorg/b/a/t;)V

    invoke-virtual {v0}, Lorg/b/a/g;->b()J

    move-result-wide v0

    return-wide v0
.end method

.method public c()Lorg/b/a/b;
    .locals 1

    .line 35
    iget-object v0, p0, Lcom/stagnationlab/sk/models/AbstractTransaction;->validUntil:Lorg/b/a/b;

    return-object v0
.end method

.method public d()Z
    .locals 1

    .line 39
    iget-object v0, p0, Lcom/stagnationlab/sk/models/AbstractTransaction;->validUntil:Lorg/b/a/b;

    invoke-virtual {v0}, Lorg/b/a/b;->i()Z

    move-result v0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .line 23
    iget-object p2, p0, Lcom/stagnationlab/sk/models/AbstractTransaction;->validUntil:Lorg/b/a/b;

    invoke-static {p1, p2}, Lcom/stagnationlab/sk/i;->a(Landroid/os/Parcel;Lorg/b/a/b;)V

    return-void
.end method
