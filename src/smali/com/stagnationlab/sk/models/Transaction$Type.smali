.class public final enum Lcom/stagnationlab/sk/models/Transaction$Type;
.super Ljava/lang/Enum;
.source "Transaction.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/stagnationlab/sk/models/Transaction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Type"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/stagnationlab/sk/models/Transaction$Type;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/stagnationlab/sk/models/Transaction$Type;

.field public static final enum AUTHENTICATION:Lcom/stagnationlab/sk/models/Transaction$Type;

.field public static final enum SIGNATURE:Lcom/stagnationlab/sk/models/Transaction$Type;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .line 81
    new-instance v0, Lcom/stagnationlab/sk/models/Transaction$Type;

    const/4 v1, 0x0

    const-string v2, "AUTHENTICATION"

    invoke-direct {v0, v2, v1}, Lcom/stagnationlab/sk/models/Transaction$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/stagnationlab/sk/models/Transaction$Type;->AUTHENTICATION:Lcom/stagnationlab/sk/models/Transaction$Type;

    new-instance v0, Lcom/stagnationlab/sk/models/Transaction$Type;

    const/4 v2, 0x1

    const-string v3, "SIGNATURE"

    invoke-direct {v0, v3, v2}, Lcom/stagnationlab/sk/models/Transaction$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/stagnationlab/sk/models/Transaction$Type;->SIGNATURE:Lcom/stagnationlab/sk/models/Transaction$Type;

    const/4 v0, 0x2

    .line 80
    new-array v0, v0, [Lcom/stagnationlab/sk/models/Transaction$Type;

    sget-object v3, Lcom/stagnationlab/sk/models/Transaction$Type;->AUTHENTICATION:Lcom/stagnationlab/sk/models/Transaction$Type;

    aput-object v3, v0, v1

    sget-object v1, Lcom/stagnationlab/sk/models/Transaction$Type;->SIGNATURE:Lcom/stagnationlab/sk/models/Transaction$Type;

    aput-object v1, v0, v2

    sput-object v0, Lcom/stagnationlab/sk/models/Transaction$Type;->$VALUES:[Lcom/stagnationlab/sk/models/Transaction$Type;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 80
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/stagnationlab/sk/models/Transaction$Type;
    .locals 1

    .line 80
    const-class v0, Lcom/stagnationlab/sk/models/Transaction$Type;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/stagnationlab/sk/models/Transaction$Type;

    return-object p0
.end method

.method public static values()[Lcom/stagnationlab/sk/models/Transaction$Type;
    .locals 1

    .line 80
    sget-object v0, Lcom/stagnationlab/sk/models/Transaction$Type;->$VALUES:[Lcom/stagnationlab/sk/models/Transaction$Type;

    invoke-virtual {v0}, [Lcom/stagnationlab/sk/models/Transaction$Type;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/stagnationlab/sk/models/Transaction$Type;

    return-object v0
.end method
