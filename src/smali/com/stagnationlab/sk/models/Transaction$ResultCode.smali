.class public final enum Lcom/stagnationlab/sk/models/Transaction$ResultCode;
.super Ljava/lang/Enum;
.source "Transaction.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/stagnationlab/sk/models/Transaction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ResultCode"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/stagnationlab/sk/models/Transaction$ResultCode;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/stagnationlab/sk/models/Transaction$ResultCode;

.field public static final enum OK:Lcom/stagnationlab/sk/models/Transaction$ResultCode;

.field public static final enum SERVER_CANCELLED:Lcom/stagnationlab/sk/models/Transaction$ResultCode;

.field public static final enum TIMEOUT:Lcom/stagnationlab/sk/models/Transaction$ResultCode;

.field public static final enum USER_REFUSED:Lcom/stagnationlab/sk/models/Transaction$ResultCode;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .line 89
    new-instance v0, Lcom/stagnationlab/sk/models/Transaction$ResultCode;

    const/4 v1, 0x0

    const-string v2, "OK"

    invoke-direct {v0, v2, v1}, Lcom/stagnationlab/sk/models/Transaction$ResultCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/stagnationlab/sk/models/Transaction$ResultCode;->OK:Lcom/stagnationlab/sk/models/Transaction$ResultCode;

    new-instance v0, Lcom/stagnationlab/sk/models/Transaction$ResultCode;

    const/4 v2, 0x1

    const-string v3, "TIMEOUT"

    invoke-direct {v0, v3, v2}, Lcom/stagnationlab/sk/models/Transaction$ResultCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/stagnationlab/sk/models/Transaction$ResultCode;->TIMEOUT:Lcom/stagnationlab/sk/models/Transaction$ResultCode;

    new-instance v0, Lcom/stagnationlab/sk/models/Transaction$ResultCode;

    const/4 v3, 0x2

    const-string v4, "USER_REFUSED"

    invoke-direct {v0, v4, v3}, Lcom/stagnationlab/sk/models/Transaction$ResultCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/stagnationlab/sk/models/Transaction$ResultCode;->USER_REFUSED:Lcom/stagnationlab/sk/models/Transaction$ResultCode;

    new-instance v0, Lcom/stagnationlab/sk/models/Transaction$ResultCode;

    const/4 v4, 0x3

    const-string v5, "SERVER_CANCELLED"

    invoke-direct {v0, v5, v4}, Lcom/stagnationlab/sk/models/Transaction$ResultCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/stagnationlab/sk/models/Transaction$ResultCode;->SERVER_CANCELLED:Lcom/stagnationlab/sk/models/Transaction$ResultCode;

    const/4 v0, 0x4

    .line 88
    new-array v0, v0, [Lcom/stagnationlab/sk/models/Transaction$ResultCode;

    sget-object v5, Lcom/stagnationlab/sk/models/Transaction$ResultCode;->OK:Lcom/stagnationlab/sk/models/Transaction$ResultCode;

    aput-object v5, v0, v1

    sget-object v1, Lcom/stagnationlab/sk/models/Transaction$ResultCode;->TIMEOUT:Lcom/stagnationlab/sk/models/Transaction$ResultCode;

    aput-object v1, v0, v2

    sget-object v1, Lcom/stagnationlab/sk/models/Transaction$ResultCode;->USER_REFUSED:Lcom/stagnationlab/sk/models/Transaction$ResultCode;

    aput-object v1, v0, v3

    sget-object v1, Lcom/stagnationlab/sk/models/Transaction$ResultCode;->SERVER_CANCELLED:Lcom/stagnationlab/sk/models/Transaction$ResultCode;

    aput-object v1, v0, v4

    sput-object v0, Lcom/stagnationlab/sk/models/Transaction$ResultCode;->$VALUES:[Lcom/stagnationlab/sk/models/Transaction$ResultCode;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 88
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/stagnationlab/sk/models/Transaction$ResultCode;
    .locals 1

    .line 88
    const-class v0, Lcom/stagnationlab/sk/models/Transaction$ResultCode;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/stagnationlab/sk/models/Transaction$ResultCode;

    return-object p0
.end method

.method public static values()[Lcom/stagnationlab/sk/models/Transaction$ResultCode;
    .locals 1

    .line 88
    sget-object v0, Lcom/stagnationlab/sk/models/Transaction$ResultCode;->$VALUES:[Lcom/stagnationlab/sk/models/Transaction$ResultCode;

    invoke-virtual {v0}, [Lcom/stagnationlab/sk/models/Transaction$ResultCode;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/stagnationlab/sk/models/Transaction$ResultCode;

    return-object v0
.end method
