.class public Lcom/stagnationlab/sk/models/PushMessage;
.super Ljava/lang/Object;
.source "PushMessage.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/stagnationlab/sk/models/PushMessage;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private accountUuid:Ljava/lang/String;

.field private rpRequestUuid:Ljava/lang/String;

.field private transactionUuid:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 35
    new-instance v0, Lcom/stagnationlab/sk/models/PushMessage$1;

    invoke-direct {v0}, Lcom/stagnationlab/sk/models/PushMessage$1;-><init>()V

    sput-object v0, Lcom/stagnationlab/sk/models/PushMessage;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/stagnationlab/sk/models/PushMessage;->transactionUuid:Ljava/lang/String;

    .line 54
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/stagnationlab/sk/models/PushMessage;->rpRequestUuid:Ljava/lang/String;

    .line 55
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/stagnationlab/sk/models/PushMessage;->accountUuid:Ljava/lang/String;

    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/stagnationlab/sk/models/PushMessage$1;)V
    .locals 0

    .line 8
    invoke-direct {p0, p1}, Lcom/stagnationlab/sk/models/PushMessage;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public constructor <init>(Ljava/util/Map;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "transactionUUID"

    .line 14
    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/stagnationlab/sk/models/PushMessage;->transactionUuid:Ljava/lang/String;

    const-string v0, "rpRequestUUID"

    .line 15
    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/stagnationlab/sk/models/PushMessage;->rpRequestUuid:Ljava/lang/String;

    const-string v0, "accountUUID"

    .line 16
    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    iput-object p1, p0, Lcom/stagnationlab/sk/models/PushMessage;->accountUuid:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .line 20
    iget-object v0, p0, Lcom/stagnationlab/sk/models/PushMessage;->transactionUuid:Ljava/lang/String;

    return-object v0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .line 24
    iget-object v0, p0, Lcom/stagnationlab/sk/models/PushMessage;->accountUuid:Ljava/lang/String;

    return-object v0
.end method

.method public c()Ljava/lang/String;
    .locals 1

    .line 28
    iget-object v0, p0, Lcom/stagnationlab/sk/models/PushMessage;->rpRequestUuid:Ljava/lang/String;

    return-object v0
.end method

.method public d()Z
    .locals 1

    .line 32
    iget-object v0, p0, Lcom/stagnationlab/sk/models/PushMessage;->transactionUuid:Ljava/lang/String;

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/stagnationlab/sk/models/PushMessage;->rpRequestUuid:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/stagnationlab/sk/models/PushMessage;->accountUuid:Ljava/lang/String;

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .line 60
    iget-object p2, p0, Lcom/stagnationlab/sk/models/PushMessage;->transactionUuid:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 61
    iget-object p2, p0, Lcom/stagnationlab/sk/models/PushMessage;->rpRequestUuid:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 62
    iget-object p2, p0, Lcom/stagnationlab/sk/models/PushMessage;->accountUuid:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    return-void
.end method
