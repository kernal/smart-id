.class public Lcom/stagnationlab/sk/models/RpRequest;
.super Lcom/stagnationlab/sk/models/AbstractTransaction;
.source "RpRequest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/stagnationlab/sk/models/RpRequest$Type;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/stagnationlab/sk/models/RpRequest;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private accountUuid:Ljava/lang/String;

.field private fake:Z

.field private relayingPartyName:Ljava/lang/String;

.field private type:Lcom/stagnationlab/sk/models/RpRequest$Type;

.field private uuid:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 39
    new-instance v0, Lcom/stagnationlab/sk/models/RpRequest$1;

    invoke-direct {v0}, Lcom/stagnationlab/sk/models/RpRequest$1;-><init>()V

    sput-object v0, Lcom/stagnationlab/sk/models/RpRequest;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    const-wide/16 v0, 0x384

    .line 78
    invoke-direct {p0, v0, v1}, Lcom/stagnationlab/sk/models/AbstractTransaction;-><init>(J)V

    const/4 v0, 0x0

    .line 16
    iput-boolean v0, p0, Lcom/stagnationlab/sk/models/RpRequest;->fake:Z

    .line 79
    sget-object v0, Lcom/stagnationlab/sk/models/RpRequest$Type;->AUTHENTICATION:Lcom/stagnationlab/sk/models/RpRequest$Type;

    iput-object v0, p0, Lcom/stagnationlab/sk/models/RpRequest;->type:Lcom/stagnationlab/sk/models/RpRequest$Type;

    .line 80
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/stagnationlab/sk/models/RpRequest;->uuid:Ljava/lang/String;

    const-string v0, "5545"

    .line 81
    iput-object v0, p0, Lcom/stagnationlab/sk/models/RpRequest;->accountUuid:Ljava/lang/String;

    const-string v0, "test"

    .line 82
    iput-object v0, p0, Lcom/stagnationlab/sk/models/RpRequest;->relayingPartyName:Ljava/lang/String;

    const/4 v0, 0x1

    .line 83
    iput-boolean v0, p0, Lcom/stagnationlab/sk/models/RpRequest;->fake:Z

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .line 50
    invoke-direct {p0, p1}, Lcom/stagnationlab/sk/models/AbstractTransaction;-><init>(Landroid/os/Parcel;)V

    const/4 v0, 0x0

    .line 16
    iput-boolean v0, p0, Lcom/stagnationlab/sk/models/RpRequest;->fake:Z

    .line 51
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/stagnationlab/sk/models/RpRequest;->uuid:Ljava/lang/String;

    .line 52
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/stagnationlab/sk/models/RpRequest$Type;->valueOf(Ljava/lang/String;)Lcom/stagnationlab/sk/models/RpRequest$Type;

    move-result-object v0

    iput-object v0, p0, Lcom/stagnationlab/sk/models/RpRequest;->type:Lcom/stagnationlab/sk/models/RpRequest$Type;

    .line 53
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/stagnationlab/sk/models/RpRequest;->accountUuid:Ljava/lang/String;

    .line 54
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/stagnationlab/sk/models/RpRequest;->relayingPartyName:Ljava/lang/String;

    .line 55
    invoke-static {p1}, Lcom/stagnationlab/sk/i;->b(Landroid/os/Parcel;)Z

    move-result p1

    iput-boolean p1, p0, Lcom/stagnationlab/sk/models/RpRequest;->fake:Z

    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/stagnationlab/sk/models/RpRequest$1;)V
    .locals 0

    .line 11
    invoke-direct {p0, p1}, Lcom/stagnationlab/sk/models/RpRequest;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public constructor <init>(Lee/cyber/smartid/dto/jsonrpc/resp/GetRPRequestResp;)V
    .locals 2

    .line 87
    invoke-virtual {p1}, Lee/cyber/smartid/dto/jsonrpc/resp/GetRPRequestResp;->getTtlSec()J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lcom/stagnationlab/sk/models/AbstractTransaction;-><init>(J)V

    const/4 v0, 0x0

    .line 16
    iput-boolean v0, p0, Lcom/stagnationlab/sk/models/RpRequest;->fake:Z

    .line 88
    invoke-virtual {p1}, Lee/cyber/smartid/dto/jsonrpc/resp/GetRPRequestResp;->getRequestType()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/stagnationlab/sk/models/RpRequest;->a(Ljava/lang/String;)Lcom/stagnationlab/sk/models/RpRequest$Type;

    move-result-object v0

    iput-object v0, p0, Lcom/stagnationlab/sk/models/RpRequest;->type:Lcom/stagnationlab/sk/models/RpRequest$Type;

    .line 89
    invoke-virtual {p1}, Lee/cyber/smartid/dto/jsonrpc/resp/GetRPRequestResp;->getRpRequestUUID()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/stagnationlab/sk/models/RpRequest;->uuid:Ljava/lang/String;

    .line 90
    invoke-virtual {p1}, Lee/cyber/smartid/dto/jsonrpc/resp/GetRPRequestResp;->getAccountUUID()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/stagnationlab/sk/models/RpRequest;->accountUuid:Ljava/lang/String;

    .line 91
    invoke-virtual {p1}, Lee/cyber/smartid/dto/jsonrpc/resp/GetRPRequestResp;->getRpName()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/stagnationlab/sk/models/RpRequest;->relayingPartyName:Ljava/lang/String;

    return-void
.end method

.method private static a(Ljava/lang/String;)Lcom/stagnationlab/sk/models/RpRequest$Type;
    .locals 3

    .line 111
    invoke-virtual {p0}, Ljava/lang/String;->hashCode()I

    move-result v0

    const v1, -0x58bbef1b

    const/4 v2, 0x1

    if-eq v0, v1, :cond_1

    const v1, 0x298bfe94

    if-eq v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const-string v0, "GET_SIGNING_CERTIFICATE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_2

    const/4 p0, 0x0

    goto :goto_1

    :cond_1
    const-string v0, "SIGNING"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_2

    const/4 p0, 0x1

    goto :goto_1

    :cond_2
    :goto_0
    const/4 p0, -0x1

    :goto_1
    if-eqz p0, :cond_4

    if-eq p0, v2, :cond_3

    .line 120
    sget-object p0, Lcom/stagnationlab/sk/models/RpRequest$Type;->AUTHENTICATION:Lcom/stagnationlab/sk/models/RpRequest$Type;

    return-object p0

    .line 116
    :cond_3
    sget-object p0, Lcom/stagnationlab/sk/models/RpRequest$Type;->SIGNING:Lcom/stagnationlab/sk/models/RpRequest$Type;

    return-object p0

    .line 113
    :cond_4
    sget-object p0, Lcom/stagnationlab/sk/models/RpRequest$Type;->GET_SIGNING_CERTIFICATE:Lcom/stagnationlab/sk/models/RpRequest$Type;

    return-object p0
.end method


# virtual methods
.method public a(Lcom/stagnationlab/sk/models/RpRequest;)Z
    .locals 1

    if-eqz p1, :cond_0

    .line 95
    invoke-virtual {p1}, Lcom/stagnationlab/sk/models/RpRequest;->i()Ljava/lang/String;

    move-result-object p1

    iget-object v0, p0, Lcom/stagnationlab/sk/models/RpRequest;->uuid:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public e()Z
    .locals 2

    .line 19
    iget-object v0, p0, Lcom/stagnationlab/sk/models/RpRequest;->type:Lcom/stagnationlab/sk/models/RpRequest$Type;

    sget-object v1, Lcom/stagnationlab/sk/models/RpRequest$Type;->AUTHENTICATION:Lcom/stagnationlab/sk/models/RpRequest$Type;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public f()Z
    .locals 2

    .line 23
    iget-object v0, p0, Lcom/stagnationlab/sk/models/RpRequest;->type:Lcom/stagnationlab/sk/models/RpRequest$Type;

    sget-object v1, Lcom/stagnationlab/sk/models/RpRequest$Type;->GET_SIGNING_CERTIFICATE:Lcom/stagnationlab/sk/models/RpRequest$Type;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public g()Ljava/lang/String;
    .locals 1

    .line 27
    iget-object v0, p0, Lcom/stagnationlab/sk/models/RpRequest;->relayingPartyName:Ljava/lang/String;

    return-object v0
.end method

.method public h()Ljava/lang/String;
    .locals 1

    .line 31
    iget-object v0, p0, Lcom/stagnationlab/sk/models/RpRequest;->accountUuid:Ljava/lang/String;

    return-object v0
.end method

.method public i()Ljava/lang/String;
    .locals 1

    .line 35
    iget-object v0, p0, Lcom/stagnationlab/sk/models/RpRequest;->uuid:Ljava/lang/String;

    return-object v0
.end method

.method public j()Z
    .locals 1

    .line 107
    iget-boolean v0, p0, Lcom/stagnationlab/sk/models/RpRequest;->fake:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 99
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "RpRequest{uuid=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/stagnationlab/sk/models/RpRequest;->uuid:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\', type=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/stagnationlab/sk/models/RpRequest;->type:Lcom/stagnationlab/sk/models/RpRequest$Type;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, "\', relayingPartyName=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/stagnationlab/sk/models/RpRequest;->relayingPartyName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\', accountUuid=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/stagnationlab/sk/models/RpRequest;->accountUuid:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\'}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .line 60
    invoke-super {p0, p1, p2}, Lcom/stagnationlab/sk/models/AbstractTransaction;->writeToParcel(Landroid/os/Parcel;I)V

    .line 61
    iget-object p2, p0, Lcom/stagnationlab/sk/models/RpRequest;->uuid:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 62
    iget-object p2, p0, Lcom/stagnationlab/sk/models/RpRequest;->type:Lcom/stagnationlab/sk/models/RpRequest$Type;

    invoke-virtual {p2}, Lcom/stagnationlab/sk/models/RpRequest$Type;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 63
    iget-object p2, p0, Lcom/stagnationlab/sk/models/RpRequest;->accountUuid:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 64
    iget-object p2, p0, Lcom/stagnationlab/sk/models/RpRequest;->relayingPartyName:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 65
    iget-boolean p2, p0, Lcom/stagnationlab/sk/models/RpRequest;->fake:Z

    invoke-static {p1, p2}, Lcom/stagnationlab/sk/i;->a(Landroid/os/Parcel;Z)V

    return-void
.end method
