.class public final enum Lcom/stagnationlab/sk/models/Certificate$Type;
.super Ljava/lang/Enum;
.source "Certificate.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/stagnationlab/sk/models/Certificate;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Type"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/stagnationlab/sk/models/Certificate$Type;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/stagnationlab/sk/models/Certificate$Type;

.field public static final enum ADVANCED:Lcom/stagnationlab/sk/models/Certificate$Type;

.field public static final enum QUALIFIED:Lcom/stagnationlab/sk/models/Certificate$Type;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .line 62
    new-instance v0, Lcom/stagnationlab/sk/models/Certificate$Type;

    const/4 v1, 0x0

    const-string v2, "QUALIFIED"

    invoke-direct {v0, v2, v1}, Lcom/stagnationlab/sk/models/Certificate$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/stagnationlab/sk/models/Certificate$Type;->QUALIFIED:Lcom/stagnationlab/sk/models/Certificate$Type;

    new-instance v0, Lcom/stagnationlab/sk/models/Certificate$Type;

    const/4 v2, 0x1

    const-string v3, "ADVANCED"

    invoke-direct {v0, v3, v2}, Lcom/stagnationlab/sk/models/Certificate$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/stagnationlab/sk/models/Certificate$Type;->ADVANCED:Lcom/stagnationlab/sk/models/Certificate$Type;

    const/4 v0, 0x2

    .line 61
    new-array v0, v0, [Lcom/stagnationlab/sk/models/Certificate$Type;

    sget-object v3, Lcom/stagnationlab/sk/models/Certificate$Type;->QUALIFIED:Lcom/stagnationlab/sk/models/Certificate$Type;

    aput-object v3, v0, v1

    sget-object v1, Lcom/stagnationlab/sk/models/Certificate$Type;->ADVANCED:Lcom/stagnationlab/sk/models/Certificate$Type;

    aput-object v1, v0, v2

    sput-object v0, Lcom/stagnationlab/sk/models/Certificate$Type;->$VALUES:[Lcom/stagnationlab/sk/models/Certificate$Type;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 61
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/stagnationlab/sk/models/Certificate$Type;
    .locals 1

    .line 61
    const-class v0, Lcom/stagnationlab/sk/models/Certificate$Type;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/stagnationlab/sk/models/Certificate$Type;

    return-object p0
.end method

.method public static values()[Lcom/stagnationlab/sk/models/Certificate$Type;
    .locals 1

    .line 61
    sget-object v0, Lcom/stagnationlab/sk/models/Certificate$Type;->$VALUES:[Lcom/stagnationlab/sk/models/Certificate$Type;

    invoke-virtual {v0}, [Lcom/stagnationlab/sk/models/Certificate$Type;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/stagnationlab/sk/models/Certificate$Type;

    return-object v0
.end method
