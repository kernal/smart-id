.class public Lcom/stagnationlab/sk/models/ErrorData;
.super Ljava/lang/Object;
.source "ErrorData.java"


# instance fields
.field private canTryAgain:Z

.field private description:Ljava/lang/String;

.field private icon:I

.field private isClientTooOldError:Z

.field private secondaryDescription:Ljava/lang/String;

.field private showErrorCode:Z

.field private showMainActivityAfterClosed:Z

.field private title:Ljava/lang/String;

.field private useTimeout:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    const v0, 0x7f070082

    const/4 v1, 0x0

    .line 28
    invoke-direct {p0, p1, p2, v0, v1}, Lcom/stagnationlab/sk/models/ErrorData;-><init>(Ljava/lang/String;Ljava/lang/String;IZ)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 1

    const/4 v0, 0x0

    .line 36
    invoke-direct {p0, p1, p2, p3, v0}, Lcom/stagnationlab/sk/models/ErrorData;-><init>(Ljava/lang/String;Ljava/lang/String;IZ)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;IZ)V
    .locals 0

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    iput-object p1, p0, Lcom/stagnationlab/sk/models/ErrorData;->title:Ljava/lang/String;

    .line 18
    iput-object p2, p0, Lcom/stagnationlab/sk/models/ErrorData;->description:Ljava/lang/String;

    .line 19
    iput p3, p0, Lcom/stagnationlab/sk/models/ErrorData;->icon:I

    .line 20
    iput-boolean p4, p0, Lcom/stagnationlab/sk/models/ErrorData;->canTryAgain:Z

    const/4 p1, 0x0

    .line 21
    iput-boolean p1, p0, Lcom/stagnationlab/sk/models/ErrorData;->showErrorCode:Z

    .line 22
    iput-boolean p1, p0, Lcom/stagnationlab/sk/models/ErrorData;->isClientTooOldError:Z

    .line 23
    iput-boolean p1, p0, Lcom/stagnationlab/sk/models/ErrorData;->showMainActivityAfterClosed:Z

    .line 24
    iput-boolean p1, p0, Lcom/stagnationlab/sk/models/ErrorData;->useTimeout:Z

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 1

    const v0, 0x7f070082

    .line 32
    invoke-direct {p0, p1, p2, v0, p3}, Lcom/stagnationlab/sk/models/ErrorData;-><init>(Ljava/lang/String;Ljava/lang/String;IZ)V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    .line 40
    iget v0, p0, Lcom/stagnationlab/sk/models/ErrorData;->icon:I

    return v0
.end method

.method public a(Ljava/lang/String;)Lcom/stagnationlab/sk/models/ErrorData;
    .locals 0

    .line 100
    iput-object p1, p0, Lcom/stagnationlab/sk/models/ErrorData;->secondaryDescription:Ljava/lang/String;

    return-object p0
.end method

.method public a(Z)Lcom/stagnationlab/sk/models/ErrorData;
    .locals 0

    .line 60
    iput-boolean p1, p0, Lcom/stagnationlab/sk/models/ErrorData;->showErrorCode:Z

    return-object p0
.end method

.method public b(Z)Lcom/stagnationlab/sk/models/ErrorData;
    .locals 0

    .line 70
    iput-boolean p1, p0, Lcom/stagnationlab/sk/models/ErrorData;->isClientTooOldError:Z

    return-object p0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .line 44
    iget-object v0, p0, Lcom/stagnationlab/sk/models/ErrorData;->title:Ljava/lang/String;

    return-object v0
.end method

.method public c(Z)Lcom/stagnationlab/sk/models/ErrorData;
    .locals 0

    .line 80
    iput-boolean p1, p0, Lcom/stagnationlab/sk/models/ErrorData;->showMainActivityAfterClosed:Z

    return-object p0
.end method

.method public c()Ljava/lang/String;
    .locals 1

    .line 48
    iget-object v0, p0, Lcom/stagnationlab/sk/models/ErrorData;->description:Ljava/lang/String;

    return-object v0
.end method

.method public d(Z)Lcom/stagnationlab/sk/models/ErrorData;
    .locals 0

    .line 90
    iput-boolean p1, p0, Lcom/stagnationlab/sk/models/ErrorData;->useTimeout:Z

    return-object p0
.end method

.method public d()Z
    .locals 1

    .line 52
    iget-boolean v0, p0, Lcom/stagnationlab/sk/models/ErrorData;->canTryAgain:Z

    return v0
.end method

.method public e()Z
    .locals 1

    .line 56
    iget-boolean v0, p0, Lcom/stagnationlab/sk/models/ErrorData;->showErrorCode:Z

    return v0
.end method

.method public f()Z
    .locals 1

    .line 66
    iget-boolean v0, p0, Lcom/stagnationlab/sk/models/ErrorData;->isClientTooOldError:Z

    return v0
.end method

.method public g()Z
    .locals 1

    .line 76
    iget-boolean v0, p0, Lcom/stagnationlab/sk/models/ErrorData;->showMainActivityAfterClosed:Z

    return v0
.end method

.method public h()Z
    .locals 1

    .line 86
    iget-boolean v0, p0, Lcom/stagnationlab/sk/models/ErrorData;->useTimeout:Z

    return v0
.end method

.method public i()Ljava/lang/String;
    .locals 1

    .line 96
    iget-object v0, p0, Lcom/stagnationlab/sk/models/ErrorData;->secondaryDescription:Ljava/lang/String;

    return-object v0
.end method
