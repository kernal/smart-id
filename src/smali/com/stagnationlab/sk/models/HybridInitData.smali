.class public Lcom/stagnationlab/sk/models/HybridInitData;
.super Ljava/lang/Object;
.source "HybridInitData.java"


# instance fields
.field private error:Lcom/stagnationlab/sk/c/a;

.field private pushToken:Ljava/lang/String;

.field private rootData:Ljava/util/Map;

.field private showNotificationInfo:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 10
    invoke-static {}, Lcom/stagnationlab/sk/util/j;->a()Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/stagnationlab/sk/models/HybridInitData;->rootData:Ljava/util/Map;

    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .line 15
    iget-object v0, p0, Lcom/stagnationlab/sk/models/HybridInitData;->pushToken:Ljava/lang/String;

    return-object v0
.end method

.method public a(Lcom/stagnationlab/sk/c/a;)V
    .locals 0

    .line 35
    iput-object p1, p0, Lcom/stagnationlab/sk/models/HybridInitData;->error:Lcom/stagnationlab/sk/c/a;

    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 0

    .line 19
    iput-object p1, p0, Lcom/stagnationlab/sk/models/HybridInitData;->pushToken:Ljava/lang/String;

    return-void
.end method

.method public a(Ljava/util/Map;)V
    .locals 0

    .line 27
    iput-object p1, p0, Lcom/stagnationlab/sk/models/HybridInitData;->rootData:Ljava/util/Map;

    return-void
.end method

.method public a(Z)V
    .locals 0

    .line 43
    iput-boolean p1, p0, Lcom/stagnationlab/sk/models/HybridInitData;->showNotificationInfo:Z

    return-void
.end method

.method public b()Ljava/util/Map;
    .locals 1

    .line 23
    iget-object v0, p0, Lcom/stagnationlab/sk/models/HybridInitData;->rootData:Ljava/util/Map;

    return-object v0
.end method

.method public c()Lcom/stagnationlab/sk/c/a;
    .locals 1

    .line 31
    iget-object v0, p0, Lcom/stagnationlab/sk/models/HybridInitData;->error:Lcom/stagnationlab/sk/c/a;

    return-object v0
.end method

.method public d()Z
    .locals 1

    .line 39
    iget-boolean v0, p0, Lcom/stagnationlab/sk/models/HybridInitData;->showNotificationInfo:Z

    return v0
.end method
