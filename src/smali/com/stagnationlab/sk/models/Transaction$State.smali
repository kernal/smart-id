.class public final enum Lcom/stagnationlab/sk/models/Transaction$State;
.super Ljava/lang/Enum;
.source "Transaction.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/stagnationlab/sk/models/Transaction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "State"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/stagnationlab/sk/models/Transaction$State;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/stagnationlab/sk/models/Transaction$State;

.field public static final enum COMPLETE:Lcom/stagnationlab/sk/models/Transaction$State;

.field public static final enum RUNNING:Lcom/stagnationlab/sk/models/Transaction$State;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .line 85
    new-instance v0, Lcom/stagnationlab/sk/models/Transaction$State;

    const/4 v1, 0x0

    const-string v2, "RUNNING"

    invoke-direct {v0, v2, v1}, Lcom/stagnationlab/sk/models/Transaction$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/stagnationlab/sk/models/Transaction$State;->RUNNING:Lcom/stagnationlab/sk/models/Transaction$State;

    new-instance v0, Lcom/stagnationlab/sk/models/Transaction$State;

    const/4 v2, 0x1

    const-string v3, "COMPLETE"

    invoke-direct {v0, v3, v2}, Lcom/stagnationlab/sk/models/Transaction$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/stagnationlab/sk/models/Transaction$State;->COMPLETE:Lcom/stagnationlab/sk/models/Transaction$State;

    const/4 v0, 0x2

    .line 84
    new-array v0, v0, [Lcom/stagnationlab/sk/models/Transaction$State;

    sget-object v3, Lcom/stagnationlab/sk/models/Transaction$State;->RUNNING:Lcom/stagnationlab/sk/models/Transaction$State;

    aput-object v3, v0, v1

    sget-object v1, Lcom/stagnationlab/sk/models/Transaction$State;->COMPLETE:Lcom/stagnationlab/sk/models/Transaction$State;

    aput-object v1, v0, v2

    sput-object v0, Lcom/stagnationlab/sk/models/Transaction$State;->$VALUES:[Lcom/stagnationlab/sk/models/Transaction$State;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 84
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/stagnationlab/sk/models/Transaction$State;
    .locals 1

    .line 84
    const-class v0, Lcom/stagnationlab/sk/models/Transaction$State;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/stagnationlab/sk/models/Transaction$State;

    return-object p0
.end method

.method public static values()[Lcom/stagnationlab/sk/models/Transaction$State;
    .locals 1

    .line 84
    sget-object v0, Lcom/stagnationlab/sk/models/Transaction$State;->$VALUES:[Lcom/stagnationlab/sk/models/Transaction$State;

    invoke-virtual {v0}, [Lcom/stagnationlab/sk/models/Transaction$State;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/stagnationlab/sk/models/Transaction$State;

    return-object v0
.end method
