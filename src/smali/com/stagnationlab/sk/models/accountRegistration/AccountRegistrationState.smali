.class public final enum Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationState;
.super Ljava/lang/Enum;
.source "AccountRegistrationState.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationState;

.field public static final enum CERTIFICATION:Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationState;

.field public static final enum COMPLETE:Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationState;

.field public static final enum DOCUMENT_CREATED:Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationState;

.field public static final enum WAITING_APPROVAL:Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationState;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .line 4
    new-instance v0, Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationState;

    const/4 v1, 0x0

    const-string v2, "DOCUMENT_CREATED"

    invoke-direct {v0, v2, v1}, Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationState;->DOCUMENT_CREATED:Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationState;

    .line 5
    new-instance v0, Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationState;

    const/4 v2, 0x1

    const-string v3, "WAITING_APPROVAL"

    invoke-direct {v0, v3, v2}, Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationState;->WAITING_APPROVAL:Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationState;

    .line 6
    new-instance v0, Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationState;

    const/4 v3, 0x2

    const-string v4, "CERTIFICATION"

    invoke-direct {v0, v4, v3}, Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationState;->CERTIFICATION:Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationState;

    .line 7
    new-instance v0, Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationState;

    const/4 v4, 0x3

    const-string v5, "COMPLETE"

    invoke-direct {v0, v5, v4}, Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationState;->COMPLETE:Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationState;

    const/4 v0, 0x4

    .line 3
    new-array v0, v0, [Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationState;

    sget-object v5, Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationState;->DOCUMENT_CREATED:Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationState;

    aput-object v5, v0, v1

    sget-object v1, Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationState;->WAITING_APPROVAL:Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationState;

    aput-object v1, v0, v2

    sget-object v1, Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationState;->CERTIFICATION:Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationState;

    aput-object v1, v0, v3

    sget-object v1, Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationState;->COMPLETE:Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationState;

    aput-object v1, v0, v4

    sput-object v0, Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationState;->$VALUES:[Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 3
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationState;
    .locals 1

    .line 3
    const-class v0, Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationState;

    return-object p0
.end method

.method public static values()[Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationState;
    .locals 1

    .line 3
    sget-object v0, Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationState;->$VALUES:[Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationState;

    invoke-virtual {v0}, [Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationState;

    return-object v0
.end method
