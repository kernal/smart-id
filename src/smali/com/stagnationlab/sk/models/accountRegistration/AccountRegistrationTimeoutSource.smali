.class public final enum Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationTimeoutSource;
.super Ljava/lang/Enum;
.source "AccountRegistrationTimeoutSource.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationTimeoutSource;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationTimeoutSource;

.field public static final enum APPROVAL:Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationTimeoutSource;

.field public static final enum CA:Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationTimeoutSource;

.field public static final enum CSR:Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationTimeoutSource;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 4
    new-instance v0, Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationTimeoutSource;

    const/4 v1, 0x0

    const-string v2, "CA"

    invoke-direct {v0, v2, v1}, Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationTimeoutSource;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationTimeoutSource;->CA:Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationTimeoutSource;

    .line 5
    new-instance v0, Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationTimeoutSource;

    const/4 v2, 0x1

    const-string v3, "CSR"

    invoke-direct {v0, v3, v2}, Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationTimeoutSource;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationTimeoutSource;->CSR:Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationTimeoutSource;

    .line 6
    new-instance v0, Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationTimeoutSource;

    const/4 v3, 0x2

    const-string v4, "APPROVAL"

    invoke-direct {v0, v4, v3}, Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationTimeoutSource;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationTimeoutSource;->APPROVAL:Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationTimeoutSource;

    const/4 v0, 0x3

    .line 3
    new-array v0, v0, [Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationTimeoutSource;

    sget-object v4, Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationTimeoutSource;->CA:Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationTimeoutSource;

    aput-object v4, v0, v1

    sget-object v1, Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationTimeoutSource;->CSR:Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationTimeoutSource;

    aput-object v1, v0, v2

    sget-object v1, Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationTimeoutSource;->APPROVAL:Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationTimeoutSource;

    aput-object v1, v0, v3

    sput-object v0, Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationTimeoutSource;->$VALUES:[Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationTimeoutSource;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 3
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationTimeoutSource;
    .locals 1

    .line 3
    const-class v0, Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationTimeoutSource;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationTimeoutSource;

    return-object p0
.end method

.method public static values()[Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationTimeoutSource;
    .locals 1

    .line 3
    sget-object v0, Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationTimeoutSource;->$VALUES:[Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationTimeoutSource;

    invoke-virtual {v0}, [Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationTimeoutSource;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationTimeoutSource;

    return-object v0
.end method
