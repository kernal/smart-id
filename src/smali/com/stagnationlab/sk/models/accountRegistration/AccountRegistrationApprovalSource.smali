.class public final enum Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationApprovalSource;
.super Ljava/lang/Enum;
.source "AccountRegistrationApprovalSource.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationApprovalSource;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationApprovalSource;

.field public static final enum KEY_VERIFIER:Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationApprovalSource;

.field public static final enum LEGAL_GUARDIAN:Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationApprovalSource;

.field public static final enum RA:Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationApprovalSource;

.field public static final enum RA_BANK:Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationApprovalSource;

.field public static final enum RA_PORTAL:Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationApprovalSource;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .line 4
    new-instance v0, Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationApprovalSource;

    const/4 v1, 0x0

    const-string v2, "RA"

    invoke-direct {v0, v2, v1}, Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationApprovalSource;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationApprovalSource;->RA:Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationApprovalSource;

    .line 5
    new-instance v0, Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationApprovalSource;

    const/4 v2, 0x1

    const-string v3, "RA_PORTAL"

    invoke-direct {v0, v3, v2}, Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationApprovalSource;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationApprovalSource;->RA_PORTAL:Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationApprovalSource;

    .line 6
    new-instance v0, Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationApprovalSource;

    const/4 v3, 0x2

    const-string v4, "RA_BANK"

    invoke-direct {v0, v4, v3}, Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationApprovalSource;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationApprovalSource;->RA_BANK:Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationApprovalSource;

    .line 7
    new-instance v0, Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationApprovalSource;

    const/4 v4, 0x3

    const-string v5, "LEGAL_GUARDIAN"

    invoke-direct {v0, v5, v4}, Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationApprovalSource;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationApprovalSource;->LEGAL_GUARDIAN:Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationApprovalSource;

    .line 8
    new-instance v0, Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationApprovalSource;

    const/4 v5, 0x4

    const-string v6, "KEY_VERIFIER"

    invoke-direct {v0, v6, v5}, Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationApprovalSource;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationApprovalSource;->KEY_VERIFIER:Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationApprovalSource;

    const/4 v0, 0x5

    .line 3
    new-array v0, v0, [Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationApprovalSource;

    sget-object v6, Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationApprovalSource;->RA:Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationApprovalSource;

    aput-object v6, v0, v1

    sget-object v1, Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationApprovalSource;->RA_PORTAL:Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationApprovalSource;

    aput-object v1, v0, v2

    sget-object v1, Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationApprovalSource;->RA_BANK:Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationApprovalSource;

    aput-object v1, v0, v3

    sget-object v1, Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationApprovalSource;->LEGAL_GUARDIAN:Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationApprovalSource;

    aput-object v1, v0, v4

    sget-object v1, Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationApprovalSource;->KEY_VERIFIER:Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationApprovalSource;

    aput-object v1, v0, v5

    sput-object v0, Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationApprovalSource;->$VALUES:[Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationApprovalSource;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 3
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationApprovalSource;
    .locals 1

    .line 3
    const-class v0, Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationApprovalSource;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationApprovalSource;

    return-object p0
.end method

.method public static values()[Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationApprovalSource;
    .locals 1

    .line 3
    sget-object v0, Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationApprovalSource;->$VALUES:[Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationApprovalSource;

    invoke-virtual {v0}, [Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationApprovalSource;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationApprovalSource;

    return-object v0
.end method
