.class public Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistration;
.super Ljava/lang/Object;
.source "AccountRegistration.java"


# instance fields
.field private documentNumber:Ljava/lang/String;

.field private missingApproval:Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationApprovalSource;

.field private resultCode:Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationResultCode;

.field private state:Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationState;

.field private timeoutSource:Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationTimeoutSource;


# direct methods
.method public constructor <init>(Lee/cyber/smartid/dto/jsonrpc/GetAccountStatusRegistration;)V
    .locals 1

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    invoke-virtual {p1}, Lee/cyber/smartid/dto/jsonrpc/GetAccountStatusRegistration;->getDocumentNumber()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistration;->documentNumber:Ljava/lang/String;

    .line 16
    invoke-virtual {p1}, Lee/cyber/smartid/dto/jsonrpc/GetAccountStatusRegistration;->getState()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistration;->a(Ljava/lang/String;)Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationState;

    move-result-object v0

    iput-object v0, p0, Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistration;->state:Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationState;

    .line 17
    invoke-virtual {p1}, Lee/cyber/smartid/dto/jsonrpc/GetAccountStatusRegistration;->getResultCode()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistration;->b(Ljava/lang/String;)Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationResultCode;

    move-result-object v0

    iput-object v0, p0, Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistration;->resultCode:Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationResultCode;

    .line 18
    invoke-virtual {p1}, Lee/cyber/smartid/dto/jsonrpc/GetAccountStatusRegistration;->getTimeoutSource()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistration;->c(Ljava/lang/String;)Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationTimeoutSource;

    move-result-object v0

    iput-object v0, p0, Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistration;->timeoutSource:Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationTimeoutSource;

    .line 19
    invoke-virtual {p1}, Lee/cyber/smartid/dto/jsonrpc/GetAccountStatusRegistration;->getMissingApproval()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistration;->d(Ljava/lang/String;)Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationApprovalSource;

    move-result-object p1

    iput-object p1, p0, Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistration;->missingApproval:Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationApprovalSource;

    return-void
.end method

.method private static a(Ljava/lang/String;)Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationState;
    .locals 5

    if-nez p0, :cond_0

    .line 24
    sget-object p0, Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationState;->CERTIFICATION:Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationState;

    return-object p0

    :cond_0
    const/4 v0, -0x1

    .line 27
    invoke-virtual {p0}, Ljava/lang/String;->hashCode()I

    move-result v1

    const/4 v2, 0x3

    const/4 v3, 0x2

    const/4 v4, 0x1

    sparse-switch v1, :sswitch_data_0

    goto :goto_0

    :sswitch_0
    const-string v1, "DOCUMENT_CREATED"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    :sswitch_1
    const-string v1, "COMPLETE"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_1

    const/4 v0, 0x3

    goto :goto_0

    :sswitch_2
    const-string v1, "WAITING_APPROVAL"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :sswitch_3
    const-string v1, "CERTIFICATION"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_1

    const/4 v0, 0x2

    :cond_1
    :goto_0
    if-eqz v0, :cond_5

    if-eq v0, v4, :cond_4

    if-eq v0, v3, :cond_3

    if-eq v0, v2, :cond_2

    .line 41
    sget-object p0, Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationState;->CERTIFICATION:Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationState;

    return-object p0

    .line 38
    :cond_2
    sget-object p0, Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationState;->COMPLETE:Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationState;

    return-object p0

    .line 35
    :cond_3
    sget-object p0, Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationState;->CERTIFICATION:Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationState;

    return-object p0

    .line 32
    :cond_4
    sget-object p0, Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationState;->WAITING_APPROVAL:Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationState;

    return-object p0

    .line 29
    :cond_5
    sget-object p0, Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationState;->DOCUMENT_CREATED:Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationState;

    return-object p0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x6cac9366 -> :sswitch_3
        -0x111577eb -> :sswitch_2
        0xaeb2139 -> :sswitch_1
        0x6f9327a4 -> :sswitch_0
    .end sparse-switch
.end method

.method private static b(Ljava/lang/String;)Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationResultCode;
    .locals 6

    if-nez p0, :cond_0

    .line 47
    sget-object p0, Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationResultCode;->CA_REFUSED:Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationResultCode;

    return-object p0

    :cond_0
    const/4 v0, -0x1

    .line 50
    invoke-virtual {p0}, Ljava/lang/String;->hashCode()I

    move-result v1

    const v2, -0x7357e031

    const/4 v3, 0x3

    const/4 v4, 0x2

    const/4 v5, 0x1

    if-eq v1, v2, :cond_4

    const v2, -0x238526bf

    if-eq v1, v2, :cond_3

    const/16 v2, 0x9dc

    if-eq v1, v2, :cond_2

    const v2, 0x54e900a2

    if-eq v1, v2, :cond_1

    goto :goto_0

    :cond_1
    const-string v1, "POOR_KEY"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_5

    const/4 v0, 0x3

    goto :goto_0

    :cond_2
    const-string v1, "OK"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_5

    const/4 v0, 0x0

    goto :goto_0

    :cond_3
    const-string v1, "TIMEOUT"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_5

    const/4 v0, 0x1

    goto :goto_0

    :cond_4
    const-string v1, "CA_REFUSED"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_5

    const/4 v0, 0x2

    :cond_5
    :goto_0
    if-eqz v0, :cond_9

    if-eq v0, v5, :cond_8

    if-eq v0, v4, :cond_7

    if-eq v0, v3, :cond_6

    .line 65
    sget-object p0, Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationResultCode;->CA_REFUSED:Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationResultCode;

    return-object p0

    .line 62
    :cond_6
    sget-object p0, Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationResultCode;->POOR_KEY:Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationResultCode;

    return-object p0

    .line 58
    :cond_7
    sget-object p0, Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationResultCode;->CA_REFUSED:Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationResultCode;

    return-object p0

    .line 55
    :cond_8
    sget-object p0, Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationResultCode;->TIMEOUT:Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationResultCode;

    return-object p0

    .line 52
    :cond_9
    sget-object p0, Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationResultCode;->OK:Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationResultCode;

    return-object p0
.end method

.method private static c(Ljava/lang/String;)Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationTimeoutSource;
    .locals 5

    if-nez p0, :cond_0

    .line 71
    sget-object p0, Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationTimeoutSource;->CA:Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationTimeoutSource;

    return-object p0

    :cond_0
    const/4 v0, -0x1

    .line 74
    invoke-virtual {p0}, Ljava/lang/String;->hashCode()I

    move-result v1

    const/16 v2, 0x85e

    const/4 v3, 0x2

    const/4 v4, 0x1

    if-eq v1, v2, :cond_3

    const v2, 0x105e2

    if-eq v1, v2, :cond_2

    const v2, 0x754b5643

    if-eq v1, v2, :cond_1

    goto :goto_0

    :cond_1
    const-string v1, "APPROVAL"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_4

    const/4 v0, 0x2

    goto :goto_0

    :cond_2
    const-string v1, "CSR"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_4

    const/4 v0, 0x1

    goto :goto_0

    :cond_3
    const-string v1, "CA"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_4

    const/4 v0, 0x0

    :cond_4
    :goto_0
    if-eqz v0, :cond_7

    if-eq v0, v4, :cond_6

    if-eq v0, v3, :cond_5

    .line 85
    sget-object p0, Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationTimeoutSource;->CA:Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationTimeoutSource;

    return-object p0

    .line 82
    :cond_5
    sget-object p0, Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationTimeoutSource;->APPROVAL:Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationTimeoutSource;

    return-object p0

    .line 79
    :cond_6
    sget-object p0, Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationTimeoutSource;->CSR:Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationTimeoutSource;

    return-object p0

    .line 76
    :cond_7
    sget-object p0, Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationTimeoutSource;->CA:Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationTimeoutSource;

    return-object p0
.end method

.method private static d(Ljava/lang/String;)Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationApprovalSource;
    .locals 6

    if-nez p0, :cond_0

    .line 93
    sget-object p0, Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationApprovalSource;->RA:Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationApprovalSource;

    return-object p0

    :cond_0
    const/4 v0, -0x1

    .line 96
    invoke-virtual {p0}, Ljava/lang/String;->hashCode()I

    move-result v1

    const/4 v2, 0x4

    const/4 v3, 0x3

    const/4 v4, 0x2

    const/4 v5, 0x1

    sparse-switch v1, :sswitch_data_0

    goto :goto_0

    :sswitch_0
    const-string v1, "RA_BANK"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_1

    const/4 v0, 0x2

    goto :goto_0

    :sswitch_1
    const-string v1, "KEY_VERIFIER"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_1

    const/4 v0, 0x4

    goto :goto_0

    :sswitch_2
    const-string v1, "RA_PORTAL"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :sswitch_3
    const-string v1, "RA"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    :sswitch_4
    const-string v1, "LEGAL_GUARDIAN"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_1

    const/4 v0, 0x3

    :cond_1
    :goto_0
    if-eqz v0, :cond_6

    if-eq v0, v5, :cond_5

    if-eq v0, v4, :cond_4

    if-eq v0, v3, :cond_3

    if-eq v0, v2, :cond_2

    .line 113
    sget-object p0, Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationApprovalSource;->RA:Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationApprovalSource;

    return-object p0

    .line 110
    :cond_2
    sget-object p0, Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationApprovalSource;->KEY_VERIFIER:Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationApprovalSource;

    return-object p0

    .line 107
    :cond_3
    sget-object p0, Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationApprovalSource;->LEGAL_GUARDIAN:Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationApprovalSource;

    return-object p0

    .line 104
    :cond_4
    sget-object p0, Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationApprovalSource;->RA_BANK:Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationApprovalSource;

    return-object p0

    .line 101
    :cond_5
    sget-object p0, Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationApprovalSource;->RA_PORTAL:Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationApprovalSource;

    return-object p0

    .line 98
    :cond_6
    sget-object p0, Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationApprovalSource;->RA:Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationApprovalSource;

    return-object p0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x59dab389 -> :sswitch_4
        0xa2f -> :sswitch_3
        0xc8b6c7c -> :sswitch_2
        0xd6334d6 -> :sswitch_1
        0x6603b24c -> :sswitch_0
    .end sparse-switch
.end method


# virtual methods
.method public a()Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationState;
    .locals 1

    .line 118
    iget-object v0, p0, Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistration;->state:Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationState;

    return-object v0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .line 122
    iget-object v0, p0, Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistration;->documentNumber:Ljava/lang/String;

    return-object v0
.end method

.method public c()Z
    .locals 2

    .line 126
    iget-object v0, p0, Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistration;->state:Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationState;

    sget-object v1, Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationState;->COMPLETE:Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationState;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistration;->resultCode:Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationResultCode;

    sget-object v1, Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationResultCode;->OK:Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationResultCode;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method
