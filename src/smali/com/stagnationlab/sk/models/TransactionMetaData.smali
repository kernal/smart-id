.class public Lcom/stagnationlab/sk/models/TransactionMetaData;
.super Ljava/lang/Object;
.source "TransactionMetaData.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/stagnationlab/sk/models/TransactionMetaData;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private isRetry:Z

.field private pinLength:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 26
    new-instance v0, Lcom/stagnationlab/sk/models/TransactionMetaData$1;

    invoke-direct {v0}, Lcom/stagnationlab/sk/models/TransactionMetaData$1;-><init>()V

    sput-object v0, Lcom/stagnationlab/sk/models/TransactionMetaData;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/stagnationlab/sk/models/TransactionMetaData;->pinLength:I

    .line 45
    invoke-static {p1}, Lcom/stagnationlab/sk/i;->b(Landroid/os/Parcel;)Z

    move-result p1

    iput-boolean p1, p0, Lcom/stagnationlab/sk/models/TransactionMetaData;->isRetry:Z

    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/stagnationlab/sk/models/TransactionMetaData$1;)V
    .locals 0

    .line 8
    invoke-direct {p0, p1}, Lcom/stagnationlab/sk/models/TransactionMetaData;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public constructor <init>(ZI)V
    .locals 0

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    iput p2, p0, Lcom/stagnationlab/sk/models/TransactionMetaData;->pinLength:I

    .line 14
    iput-boolean p1, p0, Lcom/stagnationlab/sk/models/TransactionMetaData;->isRetry:Z

    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    .line 19
    iget v0, p0, Lcom/stagnationlab/sk/models/TransactionMetaData;->pinLength:I

    return v0
.end method

.method public b()Z
    .locals 1

    .line 23
    iget-boolean v0, p0, Lcom/stagnationlab/sk/models/TransactionMetaData;->isRetry:Z

    return v0
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .line 50
    iget p2, p0, Lcom/stagnationlab/sk/models/TransactionMetaData;->pinLength:I

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 51
    iget-boolean p2, p0, Lcom/stagnationlab/sk/models/TransactionMetaData;->isRetry:Z

    invoke-static {p1, p2}, Lcom/stagnationlab/sk/i;->a(Landroid/os/Parcel;Z)V

    return-void
.end method
