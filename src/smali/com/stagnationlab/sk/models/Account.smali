.class public Lcom/stagnationlab/sk/models/Account;
.super Ljava/lang/Object;
.source "Account.java"


# instance fields
.field private authKey:Lcom/stagnationlab/sk/models/AccountKey;

.field private country:Ljava/lang/String;

.field private documentNumber:Ljava/lang/String;

.field private givenName:Ljava/lang/String;

.field private hasPushToken:Z

.field private hasSeenSuccess:Z

.field private idCode:Ljava/lang/String;

.field private isEnabled:Z

.field private legalGuardians:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/stagnationlab/sk/models/LegalGuardian;",
            ">;"
        }
    .end annotation
.end field

.field private numberOfAccounts:J

.field private registration:Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistration;

.field private signKey:Lcom/stagnationlab/sk/models/AccountKey;

.field private surname:Ljava/lang/String;

.field private uuid:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lee/cyber/smartid/dto/AccountWrapper;)V
    .locals 2

    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    invoke-virtual {p1}, Lee/cyber/smartid/dto/AccountWrapper;->getAccountUUID()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/stagnationlab/sk/models/Account;->uuid:Ljava/lang/String;

    const/4 p1, 0x0

    .line 48
    iput-boolean p1, p0, Lcom/stagnationlab/sk/models/Account;->isEnabled:Z

    const/4 p1, 0x0

    .line 49
    iput-object p1, p0, Lcom/stagnationlab/sk/models/Account;->authKey:Lcom/stagnationlab/sk/models/AccountKey;

    .line 50
    iput-object p1, p0, Lcom/stagnationlab/sk/models/Account;->signKey:Lcom/stagnationlab/sk/models/AccountKey;

    .line 51
    iput-object p1, p0, Lcom/stagnationlab/sk/models/Account;->registration:Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistration;

    .line 52
    iput-object p1, p0, Lcom/stagnationlab/sk/models/Account;->documentNumber:Ljava/lang/String;

    const-wide/16 v0, 0x0

    .line 53
    iput-wide v0, p0, Lcom/stagnationlab/sk/models/Account;->numberOfAccounts:J

    .line 55
    invoke-static {}, Lcom/stagnationlab/sk/a/d;->d()Z

    move-result p1

    iput-boolean p1, p0, Lcom/stagnationlab/sk/models/Account;->hasPushToken:Z

    .line 56
    invoke-static {}, Lcom/stagnationlab/sk/a/d;->e()Z

    move-result p1

    iput-boolean p1, p0, Lcom/stagnationlab/sk/models/Account;->hasSeenSuccess:Z

    .line 57
    invoke-static {}, Lcom/stagnationlab/sk/a/d;->j()Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/stagnationlab/sk/models/Account;->legalGuardians:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .line 61
    iget-object v0, p0, Lcom/stagnationlab/sk/models/Account;->uuid:Ljava/lang/String;

    return-object v0
.end method

.method public a(Lcom/stagnationlab/sk/c/a;)V
    .locals 1

    .line 96
    invoke-virtual {p1}, Lcom/stagnationlab/sk/c/a;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 p1, 0x0

    .line 97
    iput-boolean p1, p0, Lcom/stagnationlab/sk/models/Account;->isEnabled:Z

    return-void

    .line 102
    :cond_0
    invoke-virtual {p1}, Lcom/stagnationlab/sk/c/a;->g()Z

    move-result v0

    if-nez v0, :cond_1

    return-void

    .line 106
    :cond_1
    invoke-virtual {p1}, Lcom/stagnationlab/sk/c/a;->f()Lcom/stagnationlab/sk/models/AccountKey;

    move-result-object p1

    .line 107
    invoke-virtual {p1}, Lcom/stagnationlab/sk/models/AccountKey;->d()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/stagnationlab/sk/models/Account;->authKey:Lcom/stagnationlab/sk/models/AccountKey;

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/stagnationlab/sk/models/Account;->signKey:Lcom/stagnationlab/sk/models/AccountKey;

    :goto_0
    if-nez v0, :cond_4

    .line 110
    invoke-virtual {p1}, Lcom/stagnationlab/sk/models/AccountKey;->d()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 111
    iput-object p1, p0, Lcom/stagnationlab/sk/models/Account;->authKey:Lcom/stagnationlab/sk/models/AccountKey;

    goto :goto_1

    .line 113
    :cond_3
    iput-object p1, p0, Lcom/stagnationlab/sk/models/Account;->signKey:Lcom/stagnationlab/sk/models/AccountKey;

    goto :goto_1

    .line 116
    :cond_4
    invoke-virtual {v0, p1}, Lcom/stagnationlab/sk/models/AccountKey;->a(Lcom/stagnationlab/sk/models/AccountKey;)V

    :goto_1
    return-void
.end method

.method public a(Lee/cyber/smartid/dto/jsonrpc/IdentityData;)V
    .locals 2

    .line 125
    invoke-virtual {p1}, Lee/cyber/smartid/dto/jsonrpc/IdentityData;->getId()Lee/cyber/smartid/dto/jsonrpc/PersonalIdentifier;

    move-result-object v0

    .line 127
    invoke-virtual {p1}, Lee/cyber/smartid/dto/jsonrpc/IdentityData;->getGivenName()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/stagnationlab/sk/models/Account;->givenName:Ljava/lang/String;

    .line 128
    invoke-virtual {p1}, Lee/cyber/smartid/dto/jsonrpc/IdentityData;->getSurname()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/stagnationlab/sk/models/Account;->surname:Ljava/lang/String;

    .line 129
    invoke-virtual {v0}, Lee/cyber/smartid/dto/jsonrpc/PersonalIdentifier;->getIssuer()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/stagnationlab/sk/models/Account;->country:Ljava/lang/String;

    .line 130
    invoke-virtual {v0}, Lee/cyber/smartid/dto/jsonrpc/PersonalIdentifier;->getValue()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/stagnationlab/sk/models/Account;->idCode:Ljava/lang/String;

    return-void
.end method

.method public a(Lee/cyber/smartid/dto/jsonrpc/resp/GetAccountStatusResp;)V
    .locals 3

    .line 65
    invoke-virtual {p1}, Lee/cyber/smartid/dto/jsonrpc/resp/GetAccountStatusResp;->getRegistration()Lee/cyber/smartid/dto/jsonrpc/GetAccountStatusRegistration;

    move-result-object v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    move-object v2, v1

    goto :goto_0

    .line 66
    :cond_0
    new-instance v2, Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistration;

    invoke-direct {v2, v0}, Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistration;-><init>(Lee/cyber/smartid/dto/jsonrpc/GetAccountStatusRegistration;)V

    :goto_0
    iput-object v2, p0, Lcom/stagnationlab/sk/models/Account;->registration:Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistration;

    if-nez v0, :cond_1

    goto :goto_1

    .line 67
    :cond_1
    iget-object v0, p0, Lcom/stagnationlab/sk/models/Account;->registration:Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistration;

    invoke-virtual {v0}, Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistration;->b()Ljava/lang/String;

    move-result-object v1

    :goto_1
    iput-object v1, p0, Lcom/stagnationlab/sk/models/Account;->documentNumber:Ljava/lang/String;

    .line 68
    invoke-virtual {p1}, Lee/cyber/smartid/dto/jsonrpc/resp/GetAccountStatusResp;->getNumberOfAccounts()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/stagnationlab/sk/models/Account;->numberOfAccounts:J

    .line 69
    iget-object v0, p0, Lcom/stagnationlab/sk/models/Account;->registration:Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistration;

    if-eqz v0, :cond_2

    .line 70
    invoke-virtual {p1}, Lee/cyber/smartid/dto/jsonrpc/resp/GetAccountStatusResp;->getStatus()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ENABLED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    goto :goto_2

    :cond_2
    const/4 v0, 0x0

    :goto_2
    iput-boolean v0, p0, Lcom/stagnationlab/sk/models/Account;->isEnabled:Z

    .line 72
    invoke-virtual {p1}, Lee/cyber/smartid/dto/jsonrpc/resp/GetAccountStatusResp;->getKeys()Ljava/util/ArrayList;

    move-result-object p1

    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_3
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lee/cyber/smartid/dto/jsonrpc/ServiceAccountKeyStatus;

    .line 73
    new-instance v1, Lcom/stagnationlab/sk/models/AccountKey;

    invoke-direct {v1, v0}, Lcom/stagnationlab/sk/models/AccountKey;-><init>(Lee/cyber/smartid/dto/jsonrpc/ServiceAccountKeyStatus;)V

    .line 75
    invoke-virtual {v1}, Lcom/stagnationlab/sk/models/AccountKey;->b()Lcom/stagnationlab/sk/models/AccountKey$Type;

    move-result-object v0

    sget-object v2, Lcom/stagnationlab/sk/models/AccountKey$Type;->AUTHENTICATION:Lcom/stagnationlab/sk/models/AccountKey$Type;

    if-ne v0, v2, :cond_3

    .line 76
    iput-object v1, p0, Lcom/stagnationlab/sk/models/Account;->authKey:Lcom/stagnationlab/sk/models/AccountKey;

    goto :goto_3

    .line 78
    :cond_3
    iput-object v1, p0, Lcom/stagnationlab/sk/models/Account;->signKey:Lcom/stagnationlab/sk/models/AccountKey;

    goto :goto_3

    .line 82
    :cond_4
    iget-object p1, p0, Lcom/stagnationlab/sk/models/Account;->authKey:Lcom/stagnationlab/sk/models/AccountKey;

    if-eqz p1, :cond_5

    .line 83
    invoke-virtual {p1}, Lcom/stagnationlab/sk/models/AccountKey;->a()Lcom/stagnationlab/sk/models/Certificate;

    move-result-object p1

    if-eqz p1, :cond_5

    .line 86
    invoke-virtual {p1}, Lcom/stagnationlab/sk/models/Certificate;->a()Ljava/util/Map;

    move-result-object p1

    const-string v0, "GN"

    .line 87
    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/stagnationlab/sk/models/Account;->givenName:Ljava/lang/String;

    const-string v0, "SN"

    .line 88
    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/stagnationlab/sk/models/Account;->surname:Ljava/lang/String;

    const-string v0, "C"

    .line 89
    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/stagnationlab/sk/models/Account;->country:Ljava/lang/String;

    const-string v0, "serialNumber"

    .line 90
    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/stagnationlab/sk/models/Account;->idCode:Ljava/lang/String;

    :cond_5
    return-void
.end method

.method public a(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/stagnationlab/sk/models/LegalGuardian;",
            ">;)V"
        }
    .end annotation

    .line 153
    iput-object p1, p0, Lcom/stagnationlab/sk/models/Account;->legalGuardians:Ljava/util/List;

    .line 154
    invoke-static {p1}, Lcom/stagnationlab/sk/a/d;->a(Ljava/util/List;)V

    return-void
.end method

.method public a(Z)V
    .locals 0

    .line 121
    iput-boolean p1, p0, Lcom/stagnationlab/sk/models/Account;->isEnabled:Z

    return-void
.end method

.method public b(Z)V
    .locals 0

    .line 138
    iput-boolean p1, p0, Lcom/stagnationlab/sk/models/Account;->hasPushToken:Z

    .line 140
    invoke-static {p1}, Lcom/stagnationlab/sk/a/d;->d(Z)V

    return-void
.end method

.method public b()Z
    .locals 1

    .line 134
    iget-object v0, p0, Lcom/stagnationlab/sk/models/Account;->authKey:Lcom/stagnationlab/sk/models/AccountKey;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/stagnationlab/sk/models/AccountKey;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public c(Z)V
    .locals 0

    .line 148
    iput-boolean p1, p0, Lcom/stagnationlab/sk/models/Account;->hasSeenSuccess:Z

    .line 149
    invoke-static {p1}, Lcom/stagnationlab/sk/a/d;->e(Z)V

    return-void
.end method

.method public c()Z
    .locals 1

    .line 144
    iget-object v0, p0, Lcom/stagnationlab/sk/models/Account;->authKey:Lcom/stagnationlab/sk/models/AccountKey;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/stagnationlab/sk/models/AccountKey;->a()Lcom/stagnationlab/sk/models/Certificate;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public d()Z
    .locals 2

    .line 158
    iget-boolean v0, p0, Lcom/stagnationlab/sk/models/Account;->isEnabled:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/stagnationlab/sk/models/Account;->registration:Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistration;

    invoke-virtual {v0}, Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistration;->a()Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationState;

    move-result-object v0

    sget-object v1, Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationState;->DOCUMENT_CREATED:Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistrationState;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public e()Z
    .locals 1

    .line 162
    iget-boolean v0, p0, Lcom/stagnationlab/sk/models/Account;->isEnabled:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/stagnationlab/sk/models/Account;->registration:Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistration;

    .line 163
    invoke-virtual {v0}, Lcom/stagnationlab/sk/models/accountRegistration/AccountRegistration;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/stagnationlab/sk/models/Account;->authKey:Lcom/stagnationlab/sk/models/AccountKey;

    if-eqz v0, :cond_0

    .line 165
    invoke-virtual {v0}, Lcom/stagnationlab/sk/models/AccountKey;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method
