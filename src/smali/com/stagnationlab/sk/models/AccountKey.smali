.class public Lcom/stagnationlab/sk/models/AccountKey;
.super Ljava/lang/Object;
.source "AccountKey.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/stagnationlab/sk/models/AccountKey$Status;,
        Lcom/stagnationlab/sk/models/AccountKey$Type;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/stagnationlab/sk/models/AccountKey;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private attemptsLeft:I

.field private certificate:Lcom/stagnationlab/sk/models/Certificate;

.field private lockedUntil:Lorg/b/a/b;

.field private status:Lcom/stagnationlab/sk/models/AccountKey$Status;

.field private type:Lcom/stagnationlab/sk/models/AccountKey$Type;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 38
    new-instance v0, Lcom/stagnationlab/sk/models/AccountKey$1;

    invoke-direct {v0}, Lcom/stagnationlab/sk/models/AccountKey$1;-><init>()V

    sput-object v0, Lcom/stagnationlab/sk/models/AccountKey;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/stagnationlab/sk/models/AccountKey$Status;->valueOf(Ljava/lang/String;)Lcom/stagnationlab/sk/models/AccountKey$Status;

    move-result-object v0

    iput-object v0, p0, Lcom/stagnationlab/sk/models/AccountKey;->status:Lcom/stagnationlab/sk/models/AccountKey$Status;

    .line 57
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/stagnationlab/sk/models/AccountKey$Type;->valueOf(Ljava/lang/String;)Lcom/stagnationlab/sk/models/AccountKey$Type;

    move-result-object v0

    iput-object v0, p0, Lcom/stagnationlab/sk/models/AccountKey;->type:Lcom/stagnationlab/sk/models/AccountKey$Type;

    .line 58
    const-class v0, Lcom/stagnationlab/sk/models/Certificate;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/stagnationlab/sk/models/Certificate;

    iput-object v0, p0, Lcom/stagnationlab/sk/models/AccountKey;->certificate:Lcom/stagnationlab/sk/models/Certificate;

    .line 59
    invoke-static {p1}, Lcom/stagnationlab/sk/i;->a(Landroid/os/Parcel;)Lorg/b/a/b;

    move-result-object v0

    iput-object v0, p0, Lcom/stagnationlab/sk/models/AccountKey;->lockedUntil:Lorg/b/a/b;

    .line 60
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result p1

    iput p1, p0, Lcom/stagnationlab/sk/models/AccountKey;->attemptsLeft:I

    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/stagnationlab/sk/models/AccountKey$1;)V
    .locals 0

    .line 15
    invoke-direct {p0, p1}, Lcom/stagnationlab/sk/models/AccountKey;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public constructor <init>(Lee/cyber/smartid/dto/jsonrpc/ServiceAccountKeyStatus;)V
    .locals 2

    .line 80
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 81
    invoke-virtual {p1}, Lee/cyber/smartid/dto/jsonrpc/ServiceAccountKeyStatus;->getKeyType()Ljava/lang/String;

    move-result-object v0

    const-string v1, "SIGNATURE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/stagnationlab/sk/models/AccountKey$Type;->SIGNATURE:Lcom/stagnationlab/sk/models/AccountKey$Type;

    goto :goto_0

    :cond_0
    sget-object v0, Lcom/stagnationlab/sk/models/AccountKey$Type;->AUTHENTICATION:Lcom/stagnationlab/sk/models/AccountKey$Type;

    :goto_0
    iput-object v0, p0, Lcom/stagnationlab/sk/models/AccountKey;->type:Lcom/stagnationlab/sk/models/AccountKey$Type;

    .line 83
    invoke-virtual {p1}, Lee/cyber/smartid/dto/jsonrpc/ServiceAccountKeyStatus;->getStatus()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/stagnationlab/sk/models/AccountKey;->a(Ljava/lang/String;)Lcom/stagnationlab/sk/models/AccountKey$Status;

    move-result-object v0

    iput-object v0, p0, Lcom/stagnationlab/sk/models/AccountKey;->status:Lcom/stagnationlab/sk/models/AccountKey$Status;

    .line 85
    invoke-virtual {p1}, Lee/cyber/smartid/dto/jsonrpc/ServiceAccountKeyStatus;->getCertificate()Lee/cyber/smartid/dto/jsonrpc/ServiceAccountKeyCertificate;

    move-result-object v0

    if-nez v0, :cond_1

    const/4 v0, 0x0

    goto :goto_1

    .line 86
    :cond_1
    new-instance v1, Lcom/stagnationlab/sk/models/Certificate;

    invoke-direct {v1, v0}, Lcom/stagnationlab/sk/models/Certificate;-><init>(Lee/cyber/smartid/dto/jsonrpc/ServiceAccountKeyCertificate;)V

    move-object v0, v1

    :goto_1
    iput-object v0, p0, Lcom/stagnationlab/sk/models/AccountKey;->certificate:Lcom/stagnationlab/sk/models/Certificate;

    .line 88
    invoke-virtual {p1}, Lee/cyber/smartid/dto/jsonrpc/ServiceAccountKeyStatus;->getLockInfo()Lee/cyber/smartid/dto/jsonrpc/ServiceAccountKeyLockInfo;

    move-result-object p1

    .line 89
    invoke-virtual {p1}, Lee/cyber/smartid/dto/jsonrpc/ServiceAccountKeyLockInfo;->getPinAttemptsLeft()I

    move-result v0

    iput v0, p0, Lcom/stagnationlab/sk/models/AccountKey;->attemptsLeft:I

    .line 90
    new-instance v0, Lorg/b/a/b;

    invoke-direct {v0}, Lorg/b/a/b;-><init>()V

    invoke-virtual {p1}, Lee/cyber/smartid/dto/jsonrpc/ServiceAccountKeyLockInfo;->getLockDurationSec()I

    move-result p1

    invoke-virtual {v0, p1}, Lorg/b/a/b;->a(I)Lorg/b/a/b;

    move-result-object p1

    iput-object p1, p0, Lcom/stagnationlab/sk/models/AccountKey;->lockedUntil:Lorg/b/a/b;

    return-void
.end method

.method private a(Ljava/lang/String;)Lcom/stagnationlab/sk/models/AccountKey$Status;
    .locals 5

    .line 94
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    const/4 v1, 0x4

    const/4 v2, 0x3

    const/4 v3, 0x2

    const/4 v4, 0x1

    sparse-switch v0, :sswitch_data_0

    goto :goto_0

    :sswitch_0
    const-string v0, "REVOKED"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x4

    goto :goto_1

    :sswitch_1
    const-string v0, "IN_PREPARATION"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_1

    :sswitch_2
    const-string v0, "OK"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x3

    goto :goto_1

    :sswitch_3
    const-string v0, "EXPIRED"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x0

    goto :goto_1

    :sswitch_4
    const-string v0, "LOCKED"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x2

    goto :goto_1

    :cond_0
    :goto_0
    const/4 p1, -0x1

    :goto_1
    if-eqz p1, :cond_5

    if-eq p1, v4, :cond_4

    if-eq p1, v3, :cond_3

    if-eq p1, v2, :cond_2

    if-eq p1, v1, :cond_1

    .line 111
    sget-object p1, Lcom/stagnationlab/sk/models/AccountKey$Status;->IN_PREPARATION:Lcom/stagnationlab/sk/models/AccountKey$Status;

    return-object p1

    .line 108
    :cond_1
    sget-object p1, Lcom/stagnationlab/sk/models/AccountKey$Status;->REVOKED:Lcom/stagnationlab/sk/models/AccountKey$Status;

    return-object p1

    .line 105
    :cond_2
    sget-object p1, Lcom/stagnationlab/sk/models/AccountKey$Status;->OK:Lcom/stagnationlab/sk/models/AccountKey$Status;

    return-object p1

    .line 102
    :cond_3
    sget-object p1, Lcom/stagnationlab/sk/models/AccountKey$Status;->LOCKED:Lcom/stagnationlab/sk/models/AccountKey$Status;

    return-object p1

    .line 99
    :cond_4
    sget-object p1, Lcom/stagnationlab/sk/models/AccountKey$Status;->IN_PREPARATION:Lcom/stagnationlab/sk/models/AccountKey$Status;

    return-object p1

    .line 96
    :cond_5
    sget-object p1, Lcom/stagnationlab/sk/models/AccountKey$Status;->EXPIRED:Lcom/stagnationlab/sk/models/AccountKey$Status;

    return-object p1

    nop

    :sswitch_data_0
    .sparse-switch
        -0x79d6d8f6 -> :sswitch_4
        -0x233dccfb -> :sswitch_3
        0x9dc -> :sswitch_2
        0x2102925d -> :sswitch_1
        0x6c5e4e7e -> :sswitch_0
    .end sparse-switch
.end method


# virtual methods
.method a()Lcom/stagnationlab/sk/models/Certificate;
    .locals 1

    .line 35
    iget-object v0, p0, Lcom/stagnationlab/sk/models/AccountKey;->certificate:Lcom/stagnationlab/sk/models/Certificate;

    return-object v0
.end method

.method a(Lcom/stagnationlab/sk/models/AccountKey;)V
    .locals 1

    .line 24
    iget-object v0, p1, Lcom/stagnationlab/sk/models/AccountKey;->type:Lcom/stagnationlab/sk/models/AccountKey$Type;

    iput-object v0, p0, Lcom/stagnationlab/sk/models/AccountKey;->type:Lcom/stagnationlab/sk/models/AccountKey$Type;

    .line 25
    iget-object v0, p1, Lcom/stagnationlab/sk/models/AccountKey;->status:Lcom/stagnationlab/sk/models/AccountKey$Status;

    iput-object v0, p0, Lcom/stagnationlab/sk/models/AccountKey;->status:Lcom/stagnationlab/sk/models/AccountKey$Status;

    .line 26
    iget-object v0, p1, Lcom/stagnationlab/sk/models/AccountKey;->lockedUntil:Lorg/b/a/b;

    iput-object v0, p0, Lcom/stagnationlab/sk/models/AccountKey;->lockedUntil:Lorg/b/a/b;

    .line 27
    iget v0, p1, Lcom/stagnationlab/sk/models/AccountKey;->attemptsLeft:I

    iput v0, p0, Lcom/stagnationlab/sk/models/AccountKey;->attemptsLeft:I

    .line 29
    iget-object p1, p1, Lcom/stagnationlab/sk/models/AccountKey;->certificate:Lcom/stagnationlab/sk/models/Certificate;

    if-eqz p1, :cond_0

    .line 30
    iput-object p1, p0, Lcom/stagnationlab/sk/models/AccountKey;->certificate:Lcom/stagnationlab/sk/models/Certificate;

    :cond_0
    return-void
.end method

.method public b()Lcom/stagnationlab/sk/models/AccountKey$Type;
    .locals 1

    .line 115
    iget-object v0, p0, Lcom/stagnationlab/sk/models/AccountKey;->type:Lcom/stagnationlab/sk/models/AccountKey$Type;

    return-object v0
.end method

.method public c()I
    .locals 1

    .line 119
    iget v0, p0, Lcom/stagnationlab/sk/models/AccountKey;->attemptsLeft:I

    return v0
.end method

.method public d()Z
    .locals 2

    .line 123
    iget-object v0, p0, Lcom/stagnationlab/sk/models/AccountKey;->type:Lcom/stagnationlab/sk/models/AccountKey$Type;

    sget-object v1, Lcom/stagnationlab/sk/models/AccountKey$Type;->AUTHENTICATION:Lcom/stagnationlab/sk/models/AccountKey$Type;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public e()Lorg/b/a/b;
    .locals 1

    .line 127
    iget-object v0, p0, Lcom/stagnationlab/sk/models/AccountKey;->lockedUntil:Lorg/b/a/b;

    return-object v0
.end method

.method f()Z
    .locals 1

    .line 131
    iget-object v0, p0, Lcom/stagnationlab/sk/models/AccountKey;->certificate:Lcom/stagnationlab/sk/models/Certificate;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/stagnationlab/sk/models/Certificate;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method g()Z
    .locals 2

    .line 135
    iget-object v0, p0, Lcom/stagnationlab/sk/models/AccountKey;->status:Lcom/stagnationlab/sk/models/AccountKey$Status;

    sget-object v1, Lcom/stagnationlab/sk/models/AccountKey$Status;->LOCKED:Lcom/stagnationlab/sk/models/AccountKey$Status;

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lcom/stagnationlab/sk/models/AccountKey;->status:Lcom/stagnationlab/sk/models/AccountKey$Status;

    sget-object v1, Lcom/stagnationlab/sk/models/AccountKey$Status;->OK:Lcom/stagnationlab/sk/models/AccountKey$Status;

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .line 65
    iget-object v0, p0, Lcom/stagnationlab/sk/models/AccountKey;->status:Lcom/stagnationlab/sk/models/AccountKey$Status;

    invoke-virtual {v0}, Lcom/stagnationlab/sk/models/AccountKey$Status;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 66
    iget-object v0, p0, Lcom/stagnationlab/sk/models/AccountKey;->type:Lcom/stagnationlab/sk/models/AccountKey$Type;

    invoke-virtual {v0}, Lcom/stagnationlab/sk/models/AccountKey$Type;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 67
    iget-object v0, p0, Lcom/stagnationlab/sk/models/AccountKey;->certificate:Lcom/stagnationlab/sk/models/Certificate;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 68
    iget-object p2, p0, Lcom/stagnationlab/sk/models/AccountKey;->lockedUntil:Lorg/b/a/b;

    invoke-static {p1, p2}, Lcom/stagnationlab/sk/i;->a(Landroid/os/Parcel;Lorg/b/a/b;)V

    .line 69
    iget p2, p0, Lcom/stagnationlab/sk/models/AccountKey;->attemptsLeft:I

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    return-void
.end method
