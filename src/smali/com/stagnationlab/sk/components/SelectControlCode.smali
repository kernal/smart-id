.class public Lcom/stagnationlab/sk/components/SelectControlCode;
.super Landroid/widget/RelativeLayout;
.source "SelectControlCode.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/stagnationlab/sk/components/SelectControlCode$a;
    }
.end annotation


# instance fields
.field private a:Lcom/stagnationlab/sk/components/SelectControlCode$a;

.field private b:[Ljava/lang/String;

.field private c:Z

.field private d:Lcom/stagnationlab/sk/elements/Modal;

.field private e:Landroid/view/View;

.field private f:Landroid/view/View;

.field private g:Landroid/widget/Button;

.field private h:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .line 71
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 34
    new-instance p2, Lcom/stagnationlab/sk/components/SelectControlCode$1;

    invoke-direct {p2, p0}, Lcom/stagnationlab/sk/components/SelectControlCode$1;-><init>(Lcom/stagnationlab/sk/components/SelectControlCode;)V

    iput-object p2, p0, Lcom/stagnationlab/sk/components/SelectControlCode;->h:Landroid/view/View$OnClickListener;

    .line 73
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object p1

    const p2, 0x7f0a0020

    const/4 v0, 0x1

    invoke-virtual {p1, p2, p0, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    const p1, 0x7f08007a

    .line 75
    invoke-virtual {p0, p1}, Lcom/stagnationlab/sk/components/SelectControlCode;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/stagnationlab/sk/elements/Modal;

    iput-object p1, p0, Lcom/stagnationlab/sk/components/SelectControlCode;->d:Lcom/stagnationlab/sk/elements/Modal;

    const p1, 0x7f080048

    .line 76
    invoke-virtual {p0, p1}, Lcom/stagnationlab/sk/components/SelectControlCode;->findViewById(I)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lcom/stagnationlab/sk/components/SelectControlCode;->e:Landroid/view/View;

    const p1, 0x7f080050

    .line 77
    invoke-virtual {p0, p1}, Lcom/stagnationlab/sk/components/SelectControlCode;->findViewById(I)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lcom/stagnationlab/sk/components/SelectControlCode;->f:Landroid/view/View;

    return-void
.end method

.method static synthetic a(Lcom/stagnationlab/sk/components/SelectControlCode;Landroid/widget/Button;)Landroid/widget/Button;
    .locals 0

    .line 16
    iput-object p1, p0, Lcom/stagnationlab/sk/components/SelectControlCode;->g:Landroid/widget/Button;

    return-object p1
.end method

.method static synthetic a(Lcom/stagnationlab/sk/components/SelectControlCode;)Z
    .locals 0

    .line 16
    iget-boolean p0, p0, Lcom/stagnationlab/sk/components/SelectControlCode;->c:Z

    return p0
.end method

.method static synthetic a(Lcom/stagnationlab/sk/components/SelectControlCode;Z)Z
    .locals 0

    .line 16
    iput-boolean p1, p0, Lcom/stagnationlab/sk/components/SelectControlCode;->c:Z

    return p1
.end method

.method static synthetic b(Lcom/stagnationlab/sk/components/SelectControlCode;)Landroid/widget/Button;
    .locals 0

    .line 16
    iget-object p0, p0, Lcom/stagnationlab/sk/components/SelectControlCode;->g:Landroid/widget/Button;

    return-object p0
.end method

.method private b()V
    .locals 9

    const/4 v0, 0x3

    .line 107
    new-array v0, v0, [I

    fill-array-data v0, :array_0

    .line 113
    invoke-virtual {p0}, Lcom/stagnationlab/sk/components/SelectControlCode;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f050027

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    .line 114
    invoke-virtual {p0}, Lcom/stagnationlab/sk/components/SelectControlCode;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f07005b

    invoke-static {v2, v3}, Landroid/support/v4/content/b;->a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 118
    array-length v3, v0

    const/4 v4, 0x0

    const/4 v5, 0x0

    :goto_0
    if-ge v4, v3, :cond_0

    aget v6, v0, v4

    .line 119
    invoke-virtual {p0, v6}, Lcom/stagnationlab/sk/components/SelectControlCode;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/Button;

    .line 120
    iget-object v7, p0, Lcom/stagnationlab/sk/components/SelectControlCode;->b:[Ljava/lang/String;

    add-int/lit8 v8, v5, 0x1

    aget-object v5, v7, v5

    .line 122
    invoke-virtual {v6, v5}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 123
    invoke-virtual {v6, v1}, Landroid/widget/Button;->setTextColor(I)V

    .line 124
    invoke-virtual {v6, v2}, Landroid/widget/Button;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 125
    iget-object v5, p0, Lcom/stagnationlab/sk/components/SelectControlCode;->h:Landroid/view/View$OnClickListener;

    invoke-virtual {v6, v5}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    add-int/lit8 v4, v4, 0x1

    move v5, v8

    goto :goto_0

    :cond_0
    return-void

    :array_0
    .array-data 4
        0x7f080038
        0x7f080039
        0x7f08003a
    .end array-data
.end method

.method static synthetic c(Lcom/stagnationlab/sk/components/SelectControlCode;)Lcom/stagnationlab/sk/components/SelectControlCode$a;
    .locals 0

    .line 16
    iget-object p0, p0, Lcom/stagnationlab/sk/components/SelectControlCode;->a:Lcom/stagnationlab/sk/components/SelectControlCode$a;

    return-object p0
.end method


# virtual methods
.method public a()V
    .locals 5

    .line 49
    iget-object v0, p0, Lcom/stagnationlab/sk/components/SelectControlCode;->f:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    int-to-float v0, v0

    .line 52
    iget-object v1, p0, Lcom/stagnationlab/sk/components/SelectControlCode;->g:Landroid/widget/Button;

    if-eqz v1, :cond_0

    .line 54
    invoke-virtual {p0}, Lcom/stagnationlab/sk/components/SelectControlCode;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f07005e

    invoke-static {v2, v3}, Landroid/support/v4/content/b;->a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 53
    invoke-virtual {v1, v2}, Landroid/widget/Button;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 56
    iget-object v1, p0, Lcom/stagnationlab/sk/components/SelectControlCode;->g:Landroid/widget/Button;

    invoke-virtual {p0}, Lcom/stagnationlab/sk/components/SelectControlCode;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050029

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setTextColor(I)V

    .line 59
    :cond_0
    iget-object v1, p0, Lcom/stagnationlab/sk/components/SelectControlCode;->e:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    const/16 v2, 0x190

    int-to-long v2, v2

    invoke-virtual {v1, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    const/4 v4, 0x0

    invoke-virtual {v1, v4}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    .line 60
    iget-object v1, p0, Lcom/stagnationlab/sk/components/SelectControlCode;->f:Landroid/view/View;

    neg-float v0, v0

    invoke-virtual {v1, v0}, Landroid/view/View;->setTranslationY(F)V

    .line 61
    iget-object v0, p0, Lcom/stagnationlab/sk/components/SelectControlCode;->f:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    .line 63
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lcom/stagnationlab/sk/components/SelectControlCode$2;

    invoke-direct {v1, p0}, Lcom/stagnationlab/sk/components/SelectControlCode$2;-><init>(Lcom/stagnationlab/sk/components/SelectControlCode;)V

    const-wide/16 v2, 0x7d0

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method public a([Ljava/lang/String;Lcom/stagnationlab/sk/components/SelectControlCode$a;)V
    .locals 2

    .line 81
    iput-object p1, p0, Lcom/stagnationlab/sk/components/SelectControlCode;->b:[Ljava/lang/String;

    .line 82
    iput-object p2, p0, Lcom/stagnationlab/sk/components/SelectControlCode;->a:Lcom/stagnationlab/sk/components/SelectControlCode$a;

    const/4 p1, 0x1

    .line 83
    iput-boolean p1, p0, Lcom/stagnationlab/sk/components/SelectControlCode;->c:Z

    .line 85
    iget-object p2, p0, Lcom/stagnationlab/sk/components/SelectControlCode;->f:Landroid/view/View;

    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Landroid/view/View;->setAlpha(F)V

    .line 86
    iget-object p2, p0, Lcom/stagnationlab/sk/components/SelectControlCode;->e:Landroid/view/View;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {p2, v1}, Landroid/view/View;->setAlpha(F)V

    .line 87
    iget-object p2, p0, Lcom/stagnationlab/sk/components/SelectControlCode;->e:Landroid/view/View;

    invoke-virtual {p2, v0}, Landroid/view/View;->setTranslationY(F)V

    .line 89
    invoke-direct {p0}, Lcom/stagnationlab/sk/components/SelectControlCode;->b()V

    const p2, 0x7f080028

    .line 91
    invoke-virtual {p0, p2}, Lcom/stagnationlab/sk/components/SelectControlCode;->findViewById(I)Landroid/view/View;

    move-result-object p2

    new-instance v0, Lcom/stagnationlab/sk/components/SelectControlCode$3;

    invoke-direct {v0, p0}, Lcom/stagnationlab/sk/components/SelectControlCode$3;-><init>(Lcom/stagnationlab/sk/components/SelectControlCode;)V

    invoke-virtual {p2, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 103
    iget-object p2, p0, Lcom/stagnationlab/sk/components/SelectControlCode;->d:Lcom/stagnationlab/sk/elements/Modal;

    const/4 v0, 0x0

    invoke-virtual {p2, p1, v0}, Lcom/stagnationlab/sk/elements/Modal;->a(ZZ)V

    return-void
.end method

.method public setVisible(Z)V
    .locals 1

    .line 130
    iget-object v0, p0, Lcom/stagnationlab/sk/components/SelectControlCode;->d:Lcom/stagnationlab/sk/elements/Modal;

    invoke-virtual {v0, p1}, Lcom/stagnationlab/sk/elements/Modal;->setVisible(Z)V

    return-void
.end method
