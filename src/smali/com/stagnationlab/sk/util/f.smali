.class public Lcom/stagnationlab/sk/util/f;
.super Ljava/lang/Object;
.source "JsonOutput.java"


# static fields
.field private static b:Lcom/google/gson/f;


# instance fields
.field private a:Lcom/google/gson/o;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 18
    new-instance v0, Lcom/google/gson/g;

    invoke-direct {v0}, Lcom/google/gson/g;-><init>()V

    const-class v1, Lorg/b/a/b;

    new-instance v2, Lcom/stagnationlab/sk/util/JsonOutput$2;

    invoke-direct {v2}, Lcom/stagnationlab/sk/util/JsonOutput$2;-><init>()V

    .line 19
    invoke-virtual {v0, v1, v2}, Lcom/google/gson/g;->a(Ljava/lang/reflect/Type;Ljava/lang/Object;)Lcom/google/gson/g;

    move-result-object v0

    const-class v1, Lcom/stagnationlab/sk/c/a;

    new-instance v2, Lcom/stagnationlab/sk/util/JsonOutput$1;

    invoke-direct {v2}, Lcom/stagnationlab/sk/util/JsonOutput$1;-><init>()V

    .line 25
    invoke-virtual {v0, v1, v2}, Lcom/google/gson/g;->a(Ljava/lang/reflect/Type;Ljava/lang/Object;)Lcom/google/gson/g;

    move-result-object v0

    .line 41
    invoke-virtual {v0}, Lcom/google/gson/g;->a()Lcom/google/gson/f;

    move-result-object v0

    sput-object v0, Lcom/stagnationlab/sk/util/f;->b:Lcom/google/gson/f;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    new-instance v0, Lcom/google/gson/o;

    invoke-direct {v0}, Lcom/google/gson/o;-><init>()V

    iput-object v0, p0, Lcom/stagnationlab/sk/util/f;->a:Lcom/google/gson/o;

    return-void
.end method

.method constructor <init>(Lcom/google/gson/o;)V
    .locals 0

    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    iput-object p1, p0, Lcom/stagnationlab/sk/util/f;->a:Lcom/google/gson/o;

    return-void
.end method

.method static synthetic a(Lcom/stagnationlab/sk/util/f;)Lcom/google/gson/o;
    .locals 0

    .line 16
    iget-object p0, p0, Lcom/stagnationlab/sk/util/f;->a:Lcom/google/gson/o;

    return-object p0
.end method

.method public static a(Lcom/stagnationlab/sk/c/a;)Lcom/stagnationlab/sk/util/f;
    .locals 2

    if-nez p0, :cond_0

    const/4 p0, 0x0

    goto :goto_0

    .line 91
    :cond_0
    new-instance v0, Lcom/stagnationlab/sk/util/f;

    sget-object v1, Lcom/stagnationlab/sk/util/f;->b:Lcom/google/gson/f;

    invoke-virtual {v1, p0}, Lcom/google/gson/f;->a(Ljava/lang/Object;)Lcom/google/gson/l;

    move-result-object p0

    check-cast p0, Lcom/google/gson/o;

    invoke-direct {v0, p0}, Lcom/stagnationlab/sk/util/f;-><init>(Lcom/google/gson/o;)V

    move-object p0, v0

    :goto_0
    return-object p0
.end method


# virtual methods
.method public a(Ljava/lang/String;I)Lcom/stagnationlab/sk/util/f;
    .locals 1

    .line 58
    iget-object v0, p0, Lcom/stagnationlab/sk/util/f;->a:Lcom/google/gson/o;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    invoke-virtual {v0, p1, p2}, Lcom/google/gson/o;->a(Ljava/lang/String;Ljava/lang/Number;)V

    return-object p0
.end method

.method public a(Ljava/lang/String;Lcom/stagnationlab/sk/util/f;)Lcom/stagnationlab/sk/util/f;
    .locals 1

    .line 76
    iget-object v0, p0, Lcom/stagnationlab/sk/util/f;->a:Lcom/google/gson/o;

    if-nez p2, :cond_0

    const/4 p2, 0x0

    goto :goto_0

    :cond_0
    iget-object p2, p2, Lcom/stagnationlab/sk/util/f;->a:Lcom/google/gson/o;

    :goto_0
    invoke-virtual {v0, p1, p2}, Lcom/google/gson/o;->a(Ljava/lang/String;Lcom/google/gson/l;)V

    return-object p0
.end method

.method public a(Ljava/lang/String;Ljava/lang/Object;)Lcom/stagnationlab/sk/util/f;
    .locals 2

    .line 52
    iget-object v0, p0, Lcom/stagnationlab/sk/util/f;->a:Lcom/google/gson/o;

    sget-object v1, Lcom/stagnationlab/sk/util/f;->b:Lcom/google/gson/f;

    invoke-virtual {v1, p2}, Lcom/google/gson/f;->a(Ljava/lang/Object;)Lcom/google/gson/l;

    move-result-object p2

    invoke-virtual {v0, p1, p2}, Lcom/google/gson/o;->a(Ljava/lang/String;Lcom/google/gson/l;)V

    return-object p0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)Lcom/stagnationlab/sk/util/f;
    .locals 1

    .line 64
    iget-object v0, p0, Lcom/stagnationlab/sk/util/f;->a:Lcom/google/gson/o;

    invoke-virtual {v0, p1, p2}, Lcom/google/gson/o;->a(Ljava/lang/String;Ljava/lang/String;)V

    return-object p0
.end method

.method public a(Ljava/lang/String;Z)Lcom/stagnationlab/sk/util/f;
    .locals 1

    .line 70
    iget-object v0, p0, Lcom/stagnationlab/sk/util/f;->a:Lcom/google/gson/o;

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p2

    invoke-virtual {v0, p1, p2}, Lcom/google/gson/o;->a(Ljava/lang/String;Ljava/lang/Boolean;)V

    return-object p0
.end method

.method public a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .line 82
    iget-object v0, p0, Lcom/stagnationlab/sk/util/f;->a:Lcom/google/gson/o;

    invoke-virtual {v0, p1}, Lcom/google/gson/o;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/stagnationlab/sk/util/f;->a:Lcom/google/gson/o;

    invoke-virtual {v0, p1}, Lcom/google/gson/o;->b(Ljava/lang/String;)Lcom/google/gson/l;

    move-result-object p1

    invoke-virtual {p1}, Lcom/google/gson/l;->b()Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return-object p1
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 87
    sget-object v0, Lcom/stagnationlab/sk/util/f;->b:Lcom/google/gson/f;

    iget-object v1, p0, Lcom/stagnationlab/sk/util/f;->a:Lcom/google/gson/o;

    invoke-virtual {v0, v1}, Lcom/google/gson/f;->a(Lcom/google/gson/l;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
