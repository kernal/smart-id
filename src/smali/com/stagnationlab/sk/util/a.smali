.class Lcom/stagnationlab/sk/util/a;
.super Landroid/text/method/BaseMovementMethod;
.source "ClickableMovementMethod.java"


# static fields
.field private static a:Lcom/stagnationlab/sk/util/a;


# direct methods
.method constructor <init>()V
    .locals 0

    .line 11
    invoke-direct {p0}, Landroid/text/method/BaseMovementMethod;-><init>()V

    return-void
.end method

.method public static a()Lcom/stagnationlab/sk/util/a;
    .locals 1

    .line 17
    sget-object v0, Lcom/stagnationlab/sk/util/a;->a:Lcom/stagnationlab/sk/util/a;

    if-nez v0, :cond_0

    .line 18
    new-instance v0, Lcom/stagnationlab/sk/util/a;

    invoke-direct {v0}, Lcom/stagnationlab/sk/util/a;-><init>()V

    sput-object v0, Lcom/stagnationlab/sk/util/a;->a:Lcom/stagnationlab/sk/util/a;

    .line 20
    :cond_0
    sget-object v0, Lcom/stagnationlab/sk/util/a;->a:Lcom/stagnationlab/sk/util/a;

    return-object v0
.end method

.method private a(Landroid/text/Layout;Landroid/text/Spannable;II)[Landroid/text/style/ClickableSpan;
    .locals 3

    .line 61
    invoke-virtual {p1, p4}, Landroid/text/Layout;->getLineForVertical(I)I

    move-result v0

    .line 63
    invoke-direct {p0, p1, p2, p3, v0}, Lcom/stagnationlab/sk/util/a;->b(Landroid/text/Layout;Landroid/text/Spannable;II)[Landroid/text/style/ClickableSpan;

    move-result-object v1

    .line 65
    array-length v2, v1

    if-nez v2, :cond_1

    .line 67
    invoke-virtual {p1, v0}, Landroid/text/Layout;->getLineTop(I)I

    move-result v1

    sub-int v1, p4, v1

    .line 68
    invoke-virtual {p1, v0}, Landroid/text/Layout;->getLineBottom(I)I

    move-result v2

    sub-int/2addr v2, p4

    if-le v1, v2, :cond_0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    add-int/lit8 v0, v0, -0x1

    .line 71
    :goto_0
    invoke-direct {p0, p1, p2, p3, v0}, Lcom/stagnationlab/sk/util/a;->b(Landroid/text/Layout;Landroid/text/Spannable;II)[Landroid/text/style/ClickableSpan;

    move-result-object v1

    :cond_1
    return-object v1
.end method

.method private b(Landroid/text/Layout;Landroid/text/Spannable;II)[Landroid/text/style/ClickableSpan;
    .locals 0

    int-to-float p3, p3

    .line 81
    :try_start_0
    invoke-virtual {p1, p4, p3}, Landroid/text/Layout;->getOffsetForHorizontal(IF)I

    move-result p1
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    add-int/lit8 p3, p1, -0x2

    add-int/lit8 p1, p1, 0x2

    .line 86
    const-class p4, Landroid/text/style/ClickableSpan;

    invoke-interface {p2, p3, p1, p4}, Landroid/text/Spannable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object p1

    check-cast p1, [Landroid/text/style/ClickableSpan;

    return-object p1

    :catch_0
    const/4 p1, 0x0

    .line 83
    new-array p1, p1, [Landroid/text/style/ClickableSpan;

    return-object p1
.end method


# virtual methods
.method public canSelectArbitrarily()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public onTouchEvent(Landroid/widget/TextView;Landroid/text/Spannable;Landroid/view/MotionEvent;)Z
    .locals 5

    .line 31
    invoke-virtual {p3}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    if-nez v0, :cond_3

    .line 33
    :cond_0
    invoke-virtual {p3}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    float-to-int v2, v2

    .line 34
    invoke-virtual {p3}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    float-to-int v3, v3

    .line 36
    invoke-virtual {p1}, Landroid/widget/TextView;->getTotalPaddingLeft()I

    move-result v4

    sub-int/2addr v2, v4

    .line 37
    invoke-virtual {p1}, Landroid/widget/TextView;->getTotalPaddingTop()I

    move-result v4

    sub-int/2addr v3, v4

    .line 39
    invoke-virtual {p1}, Landroid/widget/TextView;->getScrollX()I

    move-result v4

    add-int/2addr v2, v4

    .line 40
    invoke-virtual {p1}, Landroid/widget/TextView;->getScrollY()I

    move-result v4

    add-int/2addr v3, v4

    .line 42
    invoke-virtual {p1}, Landroid/widget/TextView;->getLayout()Landroid/text/Layout;

    move-result-object v4

    invoke-direct {p0, v4, p2, v2, v3}, Lcom/stagnationlab/sk/util/a;->a(Landroid/text/Layout;Landroid/text/Spannable;II)[Landroid/text/style/ClickableSpan;

    move-result-object v2

    .line 44
    array-length v3, v2

    if-eqz v3, :cond_2

    const/4 p3, 0x0

    .line 45
    aget-object p3, v2, p3

    if-ne v0, v1, :cond_1

    .line 48
    invoke-virtual {p3, p1}, Landroid/text/style/ClickableSpan;->onClick(Landroid/view/View;)V

    goto :goto_0

    .line 50
    :cond_1
    invoke-interface {p2, p3}, Landroid/text/Spannable;->getSpanStart(Ljava/lang/Object;)I

    move-result p1

    invoke-interface {p2, p3}, Landroid/text/Spannable;->getSpanEnd(Ljava/lang/Object;)I

    move-result p3

    invoke-static {p2, p1, p3}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;II)V

    :goto_0
    return v1

    .line 54
    :cond_2
    invoke-static {p2}, Landroid/text/Selection;->removeSelection(Landroid/text/Spannable;)V

    .line 57
    :cond_3
    invoke-super {p0, p1, p2, p3}, Landroid/text/method/BaseMovementMethod;->onTouchEvent(Landroid/widget/TextView;Landroid/text/Spannable;Landroid/view/MotionEvent;)Z

    move-result p1

    return p1
.end method
