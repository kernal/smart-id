.class public Lcom/stagnationlab/sk/util/m;
.super Ljava/lang/Object;
.source "TransactionLoader.java"


# instance fields
.field private a:Lcom/stagnationlab/sk/a/a/o;

.field private b:Lcom/stagnationlab/sk/models/Transaction;

.field private c:Lcom/stagnationlab/sk/models/RpRequest;

.field private d:Lcom/stagnationlab/sk/c/a;

.field private e:Z

.field private f:Z


# direct methods
.method public constructor <init>(Lcom/stagnationlab/sk/a/a;Z)V
    .locals 1

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 18
    iput-boolean v0, p0, Lcom/stagnationlab/sk/util/m;->e:Z

    .line 19
    iput-boolean v0, p0, Lcom/stagnationlab/sk/util/m;->f:Z

    .line 22
    invoke-direct {p0, p1, p2}, Lcom/stagnationlab/sk/util/m;->a(Lcom/stagnationlab/sk/a/a;Z)V

    return-void
.end method

.method static synthetic a(Lcom/stagnationlab/sk/util/m;Lcom/stagnationlab/sk/c/a;)Lcom/stagnationlab/sk/c/a;
    .locals 0

    .line 11
    iput-object p1, p0, Lcom/stagnationlab/sk/util/m;->d:Lcom/stagnationlab/sk/c/a;

    return-object p1
.end method

.method static synthetic a(Lcom/stagnationlab/sk/util/m;Lcom/stagnationlab/sk/models/RpRequest;)Lcom/stagnationlab/sk/models/RpRequest;
    .locals 0

    .line 11
    iput-object p1, p0, Lcom/stagnationlab/sk/util/m;->c:Lcom/stagnationlab/sk/models/RpRequest;

    return-object p1
.end method

.method static synthetic a(Lcom/stagnationlab/sk/util/m;Lcom/stagnationlab/sk/models/Transaction;)Lcom/stagnationlab/sk/models/Transaction;
    .locals 0

    .line 11
    iput-object p1, p0, Lcom/stagnationlab/sk/util/m;->b:Lcom/stagnationlab/sk/models/Transaction;

    return-object p1
.end method

.method private a(Lcom/stagnationlab/sk/a/a;Z)V
    .locals 2

    if-eqz p2, :cond_0

    .line 35
    new-instance p1, Landroid/os/Handler;

    invoke-direct {p1}, Landroid/os/Handler;-><init>()V

    new-instance p2, Lcom/stagnationlab/sk/util/m$1;

    invoke-direct {p2, p0}, Lcom/stagnationlab/sk/util/m$1;-><init>(Lcom/stagnationlab/sk/util/m;)V

    const-wide/16 v0, 0x1f4

    invoke-virtual {p1, p2, v0, v1}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void

    .line 47
    :cond_0
    new-instance p2, Lcom/stagnationlab/sk/util/m$2;

    invoke-direct {p2, p0}, Lcom/stagnationlab/sk/util/m$2;-><init>(Lcom/stagnationlab/sk/util/m;)V

    invoke-virtual {p1, p2}, Lcom/stagnationlab/sk/a/a;->a(Lcom/stagnationlab/sk/a/a/o;)V

    return-void
.end method

.method static synthetic a(Lcom/stagnationlab/sk/util/m;)V
    .locals 0

    .line 11
    invoke-direct {p0}, Lcom/stagnationlab/sk/util/m;->c()V

    return-void
.end method

.method private b()V
    .locals 3

    .line 26
    iget-object v0, p0, Lcom/stagnationlab/sk/util/m;->d:Lcom/stagnationlab/sk/c/a;

    if-eqz v0, :cond_0

    .line 27
    iget-object v1, p0, Lcom/stagnationlab/sk/util/m;->a:Lcom/stagnationlab/sk/a/a/o;

    invoke-interface {v1, v0}, Lcom/stagnationlab/sk/a/a/o;->a(Lcom/stagnationlab/sk/c/a;)V

    goto :goto_0

    .line 29
    :cond_0
    iget-object v0, p0, Lcom/stagnationlab/sk/util/m;->a:Lcom/stagnationlab/sk/a/a/o;

    iget-object v1, p0, Lcom/stagnationlab/sk/util/m;->b:Lcom/stagnationlab/sk/models/Transaction;

    iget-object v2, p0, Lcom/stagnationlab/sk/util/m;->c:Lcom/stagnationlab/sk/models/RpRequest;

    invoke-interface {v0, v1, v2}, Lcom/stagnationlab/sk/a/a/o;->a(Lcom/stagnationlab/sk/models/Transaction;Lcom/stagnationlab/sk/models/RpRequest;)V

    :goto_0
    return-void
.end method

.method private c()V
    .locals 1

    const/4 v0, 0x1

    .line 64
    iput-boolean v0, p0, Lcom/stagnationlab/sk/util/m;->e:Z

    .line 66
    iget-boolean v0, p0, Lcom/stagnationlab/sk/util/m;->f:Z

    if-eqz v0, :cond_0

    .line 67
    invoke-direct {p0}, Lcom/stagnationlab/sk/util/m;->b()V

    :cond_0
    return-void
.end method


# virtual methods
.method public a(Lcom/stagnationlab/sk/a/a/o;)V
    .locals 0

    .line 76
    iput-object p1, p0, Lcom/stagnationlab/sk/util/m;->a:Lcom/stagnationlab/sk/a/a/o;

    .line 78
    iget-boolean p1, p0, Lcom/stagnationlab/sk/util/m;->e:Z

    if-eqz p1, :cond_0

    .line 79
    invoke-direct {p0}, Lcom/stagnationlab/sk/util/m;->b()V

    goto :goto_0

    :cond_0
    const/4 p1, 0x1

    .line 81
    iput-boolean p1, p0, Lcom/stagnationlab/sk/util/m;->f:Z

    :goto_0
    return-void
.end method

.method public a()Z
    .locals 1

    .line 72
    iget-boolean v0, p0, Lcom/stagnationlab/sk/util/m;->e:Z

    return v0
.end method
