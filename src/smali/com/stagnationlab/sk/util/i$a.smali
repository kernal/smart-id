.class public final enum Lcom/stagnationlab/sk/util/i$a;
.super Ljava/lang/Enum;
.source "PortalLogger.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/stagnationlab/sk/util/i;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/stagnationlab/sk/util/i$a;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/stagnationlab/sk/util/i$a;

.field private static final synthetic b:[Lcom/stagnationlab/sk/util/i$a;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 20
    new-instance v0, Lcom/stagnationlab/sk/util/i$a;

    const/4 v1, 0x0

    const-string v2, "APP_SSL_EXCEPTION"

    invoke-direct {v0, v2, v1}, Lcom/stagnationlab/sk/util/i$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/stagnationlab/sk/util/i$a;->a:Lcom/stagnationlab/sk/util/i$a;

    const/4 v0, 0x1

    .line 19
    new-array v0, v0, [Lcom/stagnationlab/sk/util/i$a;

    sget-object v2, Lcom/stagnationlab/sk/util/i$a;->a:Lcom/stagnationlab/sk/util/i$a;

    aput-object v2, v0, v1

    sput-object v0, Lcom/stagnationlab/sk/util/i$a;->b:[Lcom/stagnationlab/sk/util/i$a;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 19
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/stagnationlab/sk/util/i$a;
    .locals 1

    .line 19
    const-class v0, Lcom/stagnationlab/sk/util/i$a;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/stagnationlab/sk/util/i$a;

    return-object p0
.end method

.method public static values()[Lcom/stagnationlab/sk/util/i$a;
    .locals 1

    .line 19
    sget-object v0, Lcom/stagnationlab/sk/util/i$a;->b:[Lcom/stagnationlab/sk/util/i$a;

    invoke-virtual {v0}, [Lcom/stagnationlab/sk/util/i$a;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/stagnationlab/sk/util/i$a;

    return-object v0
.end method
