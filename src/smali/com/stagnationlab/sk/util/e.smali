.class public Lcom/stagnationlab/sk/util/e;
.super Ljava/lang/Object;
.source "JsonInput.java"


# static fields
.field private static b:Lcom/google/gson/f;


# instance fields
.field private a:Lcom/google/gson/o;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 14
    new-instance v0, Lcom/google/gson/f;

    invoke-direct {v0}, Lcom/google/gson/f;-><init>()V

    sput-object v0, Lcom/stagnationlab/sk/util/e;->b:Lcom/google/gson/f;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    if-eqz p1, :cond_0

    .line 18
    new-instance v0, Lcom/google/gson/q;

    invoke-direct {v0}, Lcom/google/gson/q;-><init>()V

    invoke-virtual {v0, p1}, Lcom/google/gson/q;->a(Ljava/lang/String;)Lcom/google/gson/l;

    move-result-object p1

    invoke-virtual {p1}, Lcom/google/gson/l;->k()Lcom/google/gson/o;

    move-result-object p1

    iput-object p1, p0, Lcom/stagnationlab/sk/util/e;->a:Lcom/google/gson/o;

    :cond_0
    return-void
.end method

.method private c(Ljava/lang/String;)Lcom/google/gson/l;
    .locals 2

    .line 45
    iget-object v0, p0, Lcom/stagnationlab/sk/util/e;->a:Lcom/google/gson/o;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    move-object p1, v1

    goto :goto_0

    :cond_0
    invoke-virtual {v0, p1}, Lcom/google/gson/o;->b(Ljava/lang/String;)Lcom/google/gson/l;

    move-result-object p1

    :goto_0
    if-eqz p1, :cond_1

    .line 47
    invoke-virtual {p1}, Lcom/google/gson/l;->j()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    move-object p1, v1

    :cond_2
    return-object p1
.end method


# virtual methods
.method public a(Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .line 23
    invoke-direct {p0, p1}, Lcom/stagnationlab/sk/util/e;->c(Ljava/lang/String;)Lcom/google/gson/l;

    move-result-object p1

    if-nez p1, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    .line 25
    :cond_0
    invoke-virtual {p1}, Lcom/google/gson/l;->b()Ljava/lang/String;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method public a(Ljava/lang/String;Ljava/lang/Class;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/String;",
            "Ljava/lang/Class<",
            "[TT;>;)",
            "Ljava/util/List<",
            "TT;>;"
        }
    .end annotation

    .line 35
    sget-object v0, Lcom/stagnationlab/sk/util/e;->b:Lcom/google/gson/f;

    iget-object v1, p0, Lcom/stagnationlab/sk/util/e;->a:Lcom/google/gson/o;

    invoke-virtual {v1, p1}, Lcom/google/gson/o;->b(Ljava/lang/String;)Lcom/google/gson/l;

    move-result-object p1

    invoke-virtual {v0, p1, p2}, Lcom/google/gson/f;->a(Lcom/google/gson/l;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, [Ljava/lang/Object;

    .line 37
    invoke-static {p1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    return-object p1
.end method

.method public b(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/String;",
            "Ljava/lang/Class<",
            "TT;>;)TT;"
        }
    .end annotation

    .line 41
    sget-object v0, Lcom/stagnationlab/sk/util/e;->b:Lcom/google/gson/f;

    iget-object v1, p0, Lcom/stagnationlab/sk/util/e;->a:Lcom/google/gson/o;

    invoke-virtual {v1, p1}, Lcom/google/gson/o;->b(Ljava/lang/String;)Lcom/google/gson/l;

    move-result-object p1

    invoke-virtual {v0, p1, p2}, Lcom/google/gson/f;->a(Lcom/google/gson/l;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public b(Ljava/lang/String;)Z
    .locals 0

    .line 29
    invoke-direct {p0, p1}, Lcom/stagnationlab/sk/util/e;->c(Ljava/lang/String;)Lcom/google/gson/l;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 31
    invoke-virtual {p1}, Lcom/google/gson/l;->f()Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method
