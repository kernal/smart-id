.class final Lcom/stagnationlab/sk/util/JsonOutput$1;
.super Ljava/lang/Object;
.source "JsonOutput.java"

# interfaces
.implements Lcom/google/gson/t;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/stagnationlab/sk/util/f;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/gson/t<",
        "Lcom/stagnationlab/sk/c/a;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/stagnationlab/sk/c/a;Ljava/lang/reflect/Type;Lcom/google/gson/s;)Lcom/google/gson/l;
    .locals 1

    .line 28
    new-instance p2, Lcom/google/gson/o;

    invoke-direct {p2}, Lcom/google/gson/o;-><init>()V

    .line 30
    invoke-virtual {p1}, Lcom/stagnationlab/sk/c/a;->k()Lcom/stagnationlab/sk/c/c;

    move-result-object p3

    invoke-virtual {p3}, Lcom/stagnationlab/sk/c/c;->toString()Ljava/lang/String;

    move-result-object p3

    const-string v0, "type"

    invoke-virtual {p2, v0, p3}, Lcom/google/gson/o;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 31
    invoke-virtual {p1}, Lcom/stagnationlab/sk/c/a;->l()Ljava/lang/String;

    move-result-object p3

    const-string v0, "code"

    invoke-virtual {p2, v0, p3}, Lcom/google/gson/o;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 32
    invoke-virtual {p1}, Lcom/stagnationlab/sk/c/a;->b()Ljava/lang/String;

    move-result-object p3

    const-string v0, "message"

    invoke-virtual {p2, v0, p3}, Lcom/google/gson/o;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 34
    invoke-virtual {p1}, Lcom/stagnationlab/sk/c/a;->d()Z

    move-result p3

    if-eqz p3, :cond_0

    .line 35
    invoke-virtual {p1}, Lcom/stagnationlab/sk/c/a;->e()Lcom/stagnationlab/sk/util/f;

    move-result-object p1

    invoke-static {p1}, Lcom/stagnationlab/sk/util/f;->a(Lcom/stagnationlab/sk/util/f;)Lcom/google/gson/o;

    move-result-object p1

    const-string p3, "extra"

    invoke-virtual {p2, p3, p1}, Lcom/google/gson/o;->a(Ljava/lang/String;Lcom/google/gson/l;)V

    :cond_0
    return-object p2
.end method

.method public bridge synthetic a(Ljava/lang/Object;Ljava/lang/reflect/Type;Lcom/google/gson/s;)Lcom/google/gson/l;
    .locals 0

    .line 25
    check-cast p1, Lcom/stagnationlab/sk/c/a;

    invoke-virtual {p0, p1, p2, p3}, Lcom/stagnationlab/sk/util/JsonOutput$1;->a(Lcom/stagnationlab/sk/c/a;Ljava/lang/reflect/Type;Lcom/google/gson/s;)Lcom/google/gson/l;

    move-result-object p1

    return-object p1
.end method
