.class public final enum Lcom/stagnationlab/sk/util/i$b;
.super Ljava/lang/Enum;
.source "PortalLogger.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/stagnationlab/sk/util/i;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/stagnationlab/sk/util/i$b;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/stagnationlab/sk/util/i$b;

.field public static final enum b:Lcom/stagnationlab/sk/util/i$b;

.field private static final synthetic c:[Lcom/stagnationlab/sk/util/i$b;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .line 16
    new-instance v0, Lcom/stagnationlab/sk/util/i$b;

    const/4 v1, 0x0

    const-string v2, "REGISTRATION_ABANDONED"

    invoke-direct {v0, v2, v1}, Lcom/stagnationlab/sk/util/i$b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/stagnationlab/sk/util/i$b;->a:Lcom/stagnationlab/sk/util/i$b;

    new-instance v0, Lcom/stagnationlab/sk/util/i$b;

    const/4 v2, 0x1

    const-string v3, "ACCOUNT_DELETED_IN_ABANDONED_STATE"

    invoke-direct {v0, v3, v2}, Lcom/stagnationlab/sk/util/i$b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/stagnationlab/sk/util/i$b;->b:Lcom/stagnationlab/sk/util/i$b;

    const/4 v0, 0x2

    .line 15
    new-array v0, v0, [Lcom/stagnationlab/sk/util/i$b;

    sget-object v3, Lcom/stagnationlab/sk/util/i$b;->a:Lcom/stagnationlab/sk/util/i$b;

    aput-object v3, v0, v1

    sget-object v1, Lcom/stagnationlab/sk/util/i$b;->b:Lcom/stagnationlab/sk/util/i$b;

    aput-object v1, v0, v2

    sput-object v0, Lcom/stagnationlab/sk/util/i$b;->c:[Lcom/stagnationlab/sk/util/i$b;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 15
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/stagnationlab/sk/util/i$b;
    .locals 1

    .line 15
    const-class v0, Lcom/stagnationlab/sk/util/i$b;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/stagnationlab/sk/util/i$b;

    return-object p0
.end method

.method public static values()[Lcom/stagnationlab/sk/util/i$b;
    .locals 1

    .line 15
    sget-object v0, Lcom/stagnationlab/sk/util/i$b;->c:[Lcom/stagnationlab/sk/util/i$b;

    invoke-virtual {v0}, [Lcom/stagnationlab/sk/util/i$b;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/stagnationlab/sk/util/i$b;

    return-object v0
.end method
