.class public Lcom/stagnationlab/sk/util/c;
.super Ljava/lang/Object;
.source "Http.java"


# static fields
.field private static a:Lokhttp3/m;

.field private static b:Lokhttp3/w;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 42
    new-instance v0, Lcom/stagnationlab/sk/util/c$1;

    invoke-direct {v0}, Lcom/stagnationlab/sk/util/c$1;-><init>()V

    sput-object v0, Lcom/stagnationlab/sk/util/c;->a:Lokhttp3/m;

    .line 56
    invoke-static {}, Lcom/stagnationlab/sk/util/c;->a()Lokhttp3/w;

    move-result-object v0

    sput-object v0, Lcom/stagnationlab/sk/util/c;->b:Lokhttp3/w;

    return-void
.end method

.method public static a(Landroid/net/Uri;)Landroid/net/Uri;
    .locals 2

    .line 233
    invoke-virtual {p0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object p0

    .line 234
    invoke-static {}, Lcom/stagnationlab/sk/MyApplication;->c()Ljava/lang/String;

    move-result-object v0

    const-string v1, "lang"

    invoke-virtual {p0, v1, v0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object p0

    .line 235
    invoke-virtual {p0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object p0

    return-object p0
.end method

.method public static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .line 221
    invoke-static {p0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p0

    .line 222
    invoke-virtual {p0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object p0

    .line 223
    invoke-static {}, Lcom/stagnationlab/sk/util/b;->a()Ljava/lang/String;

    move-result-object v0

    const-string v1, "clientApp"

    invoke-virtual {p0, v1, v0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object p0

    .line 224
    invoke-static {}, Lcom/stagnationlab/sk/util/b;->b()Ljava/lang/String;

    move-result-object v0

    const-string v1, "clientLib"

    invoke-virtual {p0, v1, v0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object p0

    .line 225
    invoke-static {}, Lcom/stagnationlab/sk/util/b;->f()Ljava/lang/String;

    move-result-object v0

    const-string v1, "clientDevice"

    invoke-virtual {p0, v1, v0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object p0

    .line 226
    invoke-static {}, Lcom/stagnationlab/sk/util/b;->e()Ljava/lang/String;

    move-result-object v0

    const-string v1, "clientPlatform"

    invoke-virtual {p0, v1, v0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object p0

    .line 227
    invoke-virtual {p0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object p0

    .line 228
    invoke-virtual {p0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method private static a(Ljava/util/Map;)Ljava/lang/String;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/stagnationlab/sk/d/a;
        }
    .end annotation

    .line 197
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 199
    invoke-interface {p0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object p0

    invoke-interface {p0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :cond_0
    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 200
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 203
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/stagnationlab/sk/util/c;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "&"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 207
    :cond_1
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static a(Ljava/lang/String;Ljava/util/Map;Lcom/stagnationlab/sk/util/d;)Lokhttp3/e;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;",
            "Lcom/stagnationlab/sk/util/d;",
            ")",
            "Lokhttp3/e;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/stagnationlab/sk/d/a;
        }
    .end annotation

    .line 124
    invoke-static {p0}, Lcom/stagnationlab/sk/util/c;->c(Ljava/lang/String;)V

    const-string v0, "application/x-www-form-urlencoded; charset=utf-8"

    .line 127
    invoke-static {v0}, Lokhttp3/u;->b(Ljava/lang/String;)Lokhttp3/u;

    move-result-object v0

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    new-instance p1, Ljava/util/HashMap;

    invoke-direct {p1}, Ljava/util/HashMap;-><init>()V

    .line 128
    :goto_0
    invoke-static {p1}, Lcom/stagnationlab/sk/util/c;->a(Ljava/util/Map;)Ljava/lang/String;

    move-result-object p1

    .line 126
    invoke-static {v0, p1}, Lokhttp3/aa;->a(Lokhttp3/u;Ljava/lang/String;)Lokhttp3/aa;

    move-result-object p1

    .line 131
    new-instance v0, Lokhttp3/z$a;

    invoke-direct {v0}, Lokhttp3/z$a;-><init>()V

    const-string v1, "POST"

    .line 132
    invoke-virtual {v0, v1, p1}, Lokhttp3/z$a;->a(Ljava/lang/String;Lokhttp3/aa;)Lokhttp3/z$a;

    move-result-object p1

    .line 133
    invoke-static {p0}, Lcom/stagnationlab/sk/util/c;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {p1, p0}, Lokhttp3/z$a;->a(Ljava/lang/String;)Lokhttp3/z$a;

    move-result-object p0

    .line 134
    invoke-virtual {p0}, Lokhttp3/z$a;->a()Lokhttp3/z;

    move-result-object p0

    .line 136
    invoke-static {p0, p2}, Lcom/stagnationlab/sk/util/c;->a(Lokhttp3/z;Lcom/stagnationlab/sk/util/d;)Lokhttp3/e;

    move-result-object p0

    return-object p0
.end method

.method public static a(Ljava/lang/String;ZLcom/stagnationlab/sk/util/d;)Lokhttp3/e;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/stagnationlab/sk/d/a;
        }
    .end annotation

    .line 109
    invoke-static {p0}, Lcom/stagnationlab/sk/util/c;->c(Ljava/lang/String;)V

    .line 111
    new-instance v0, Lokhttp3/z$a;

    invoke-direct {v0}, Lokhttp3/z$a;-><init>()V

    .line 112
    invoke-static {p0}, Lcom/stagnationlab/sk/util/c;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v0, p0}, Lokhttp3/z$a;->a(Ljava/lang/String;)Lokhttp3/z$a;

    move-result-object p0

    if-eqz p1, :cond_0

    .line 115
    invoke-static {}, Lcom/stagnationlab/sk/util/b;->h()Ljava/lang/String;

    move-result-object p1

    const-string v0, "X-Smart-ID-Device-Fingerprint"

    invoke-virtual {p0, v0, p1}, Lokhttp3/z$a;->a(Ljava/lang/String;Ljava/lang/String;)Lokhttp3/z$a;

    .line 118
    :cond_0
    invoke-virtual {p0}, Lokhttp3/z$a;->a()Lokhttp3/z;

    move-result-object p0

    invoke-static {p0, p2}, Lcom/stagnationlab/sk/util/c;->a(Lokhttp3/z;Lcom/stagnationlab/sk/util/d;)Lokhttp3/e;

    move-result-object p0

    return-object p0
.end method

.method private static a(Lokhttp3/z;Lcom/stagnationlab/sk/util/d;)Lokhttp3/e;
    .locals 1

    .line 148
    sget-object v0, Lcom/stagnationlab/sk/util/c;->b:Lokhttp3/w;

    invoke-virtual {v0, p0}, Lokhttp3/w;->a(Lokhttp3/z;)Lokhttp3/e;

    move-result-object p0

    .line 151
    :try_start_0
    new-instance v0, Lcom/stagnationlab/sk/util/c$2;

    invoke-direct {v0, p1}, Lcom/stagnationlab/sk/util/c$2;-><init>(Lcom/stagnationlab/sk/util/d;)V

    invoke-interface {p0, v0}, Lokhttp3/e;->a(Lokhttp3/f;)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p0

    :catch_0
    move-exception p0

    const-string p1, "Http"

    const-string v0, "Call already executed"

    .line 189
    invoke-static {p1, v0, p0}, Lcom/stagnationlab/sk/f;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 190
    new-instance p1, Ljava/lang/IllegalStateException;

    invoke-direct {p1, p0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw p1
.end method

.method private static a(Ljava/lang/String;Ljava/lang/String;)Lokhttp3/g;
    .locals 10

    const-string v0, "Http"

    const/4 v1, 0x0

    .line 82
    :try_start_0
    invoke-static {p0}, Lcom/stagnationlab/sk/util/c;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    const-string v2, ","

    .line 83
    invoke-virtual {p1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 85
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p1

    if-nez p1, :cond_1

    .line 86
    new-instance p1, Lokhttp3/g$a;

    invoke-direct {p1}, Lokhttp3/g$a;-><init>()V

    .line 88
    array-length v3, v2

    const/4 v4, 0x0

    const/4 v5, 0x0

    :goto_0
    if-ge v5, v3, :cond_0

    aget-object v6, v2, v5

    .line 89
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Adding pin: \""

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v8, 0x22

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v9, "to host: "

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v0, v7}, Lcom/stagnationlab/sk/f;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 90
    invoke-virtual {p0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/String;

    invoke-virtual {v6}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v8, v4

    invoke-virtual {p1, v7, v8}, Lokhttp3/g$a;->a(Ljava/lang/String;[Ljava/lang/String;)Lokhttp3/g$a;

    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 93
    :cond_0
    invoke-virtual {p1}, Lokhttp3/g$a;->a()Lokhttp3/g;

    move-result-object v1
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception p0

    .line 96
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid url: "

    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/net/MalformedURLException;->getMessage()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {p1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-static {v0, p0}, Lcom/stagnationlab/sk/f;->c(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    :goto_1
    return-object v1
.end method

.method private static a()Lokhttp3/w;
    .locals 5

    .line 59
    new-instance v0, Lokhttp3/w$a;

    invoke-direct {v0}, Lokhttp3/w$a;-><init>()V

    const-string v1, "https://portal.smart-id.com"

    const-string v2, "sha256/loUeIWZlKN66wo2lcTdABJjwjL7sqK3esJ/kVOZXJ/U=,sha256/gx9XGTLst426ccdoH81TLyzcPYornvqOLYvMkRmsSj0=,sha256/f04DqeqACA4E+U7wl2jLeAYkqXCgXlwRiBMIvPLpvrE=,sha256/tmasPKcvYrEeycngvWWucrvZZRCoqx/tCwacy12xM2I="

    .line 60
    invoke-static {v1, v2}, Lcom/stagnationlab/sk/util/c;->a(Ljava/lang/String;Ljava/lang/String;)Lokhttp3/g;

    move-result-object v1

    .line 62
    sget-object v2, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v3, 0x1e

    invoke-virtual {v0, v3, v4, v2}, Lokhttp3/w$a;->a(JLjava/util/concurrent/TimeUnit;)Lokhttp3/w$a;

    .line 63
    sget-object v2, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v3, v4, v2}, Lokhttp3/w$a;->b(JLjava/util/concurrent/TimeUnit;)Lokhttp3/w$a;

    .line 64
    sget-object v2, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v3, v4, v2}, Lokhttp3/w$a;->c(JLjava/util/concurrent/TimeUnit;)Lokhttp3/w$a;

    .line 65
    sget-object v2, Lcom/stagnationlab/sk/util/c;->a:Lokhttp3/m;

    invoke-virtual {v0, v2}, Lokhttp3/w$a;->a(Lokhttp3/m;)Lokhttp3/w$a;

    if-eqz v1, :cond_0

    .line 68
    invoke-virtual {v0, v1}, Lokhttp3/w$a;->a(Lokhttp3/g;)Lokhttp3/w$a;

    .line 71
    :cond_0
    invoke-virtual {v0}, Lokhttp3/w$a;->a()Lokhttp3/w;

    move-result-object v0

    return-object v0
.end method

.method private static b(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/net/MalformedURLException;
        }
    .end annotation

    .line 103
    new-instance v0, Ljava/net/URL;

    invoke-direct {v0, p0}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/net/URL;->getHost()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method private static c(Ljava/lang/String;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/stagnationlab/sk/d/a;
        }
    .end annotation

    const-string v0, "https://portal.smart-id.com"

    .line 140
    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 141
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Request URL not white-listed: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    const-string v0, "Http"

    .line 142
    invoke-static {v0, p0}, Lcom/stagnationlab/sk/f;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 143
    new-instance v0, Lcom/stagnationlab/sk/d/a;

    sget-object v1, Lcom/stagnationlab/sk/c/b;->X:Lcom/stagnationlab/sk/c/b;

    invoke-direct {v0, p0, v1}, Lcom/stagnationlab/sk/d/a;-><init>(Ljava/lang/String;Lcom/stagnationlab/sk/c/b;)V

    throw v0
.end method

.method private static d(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/stagnationlab/sk/d/a;
        }
    .end annotation

    :try_start_0
    const-string v0, "utf-8"

    .line 212
    invoke-static {p0, v0}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p0

    :catch_0
    move-exception v0

    .line 214
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Failed to URLEncode \'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p0, "\' to UTF-8"

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    const-string v1, "Http"

    invoke-static {v1, p0, v0}, Lcom/stagnationlab/sk/f;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 216
    new-instance p0, Lcom/stagnationlab/sk/d/a;

    invoke-direct {p0, v0}, Lcom/stagnationlab/sk/d/a;-><init>(Ljava/lang/Exception;)V

    throw p0
.end method
