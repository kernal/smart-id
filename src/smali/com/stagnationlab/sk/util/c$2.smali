.class final Lcom/stagnationlab/sk/util/c$2;
.super Ljava/lang/Object;
.source "Http.java"

# interfaces
.implements Lokhttp3/f;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/stagnationlab/sk/util/c;->a(Lokhttp3/z;Lcom/stagnationlab/sk/util/d;)Lokhttp3/e;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/stagnationlab/sk/util/d;


# direct methods
.method constructor <init>(Lcom/stagnationlab/sk/util/d;)V
    .locals 0

    .line 151
    iput-object p1, p0, Lcom/stagnationlab/sk/util/c$2;->a:Lcom/stagnationlab/sk/util/d;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lokhttp3/e;Ljava/io/IOException;)V
    .locals 3

    const-string p1, "Http"

    const-string v0, "Network request failed"

    .line 154
    invoke-static {p1, v0, p2}, Lcom/stagnationlab/sk/f;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 156
    instance-of p1, p2, Ljavax/net/ssl/SSLException;

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    .line 157
    iget-object p1, p0, Lcom/stagnationlab/sk/util/c$2;->a:Lcom/stagnationlab/sk/util/d;

    new-instance v1, Lcom/stagnationlab/sk/d/a;

    sget-object v2, Lcom/stagnationlab/sk/c/b;->R:Lcom/stagnationlab/sk/c/b;

    invoke-direct {v1, p2, v2}, Lcom/stagnationlab/sk/d/a;-><init>(Ljava/lang/Exception;Lcom/stagnationlab/sk/c/b;)V

    invoke-interface {p1, v0, v1}, Lcom/stagnationlab/sk/util/d;->a(Lokhttp3/ab;Lcom/stagnationlab/sk/d/a;)V

    return-void

    .line 161
    :cond_0
    iget-object p1, p0, Lcom/stagnationlab/sk/util/c$2;->a:Lcom/stagnationlab/sk/util/d;

    new-instance v1, Lcom/stagnationlab/sk/d/a;

    invoke-direct {v1, p2}, Lcom/stagnationlab/sk/d/a;-><init>(Ljava/lang/Exception;)V

    invoke-interface {p1, v0, v1}, Lcom/stagnationlab/sk/util/d;->a(Lokhttp3/ab;Lcom/stagnationlab/sk/d/a;)V

    return-void
.end method

.method public a(Lokhttp3/e;Lokhttp3/ab;)V
    .locals 3

    .line 166
    invoke-virtual {p2}, Lokhttp3/ab;->c()Z

    move-result p1

    const-string v0, "Http"

    if-nez p1, :cond_0

    .line 167
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Unexpected status code "

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {v0, p1}, Lcom/stagnationlab/sk/f;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 168
    iget-object p1, p0, Lcom/stagnationlab/sk/util/c$2;->a:Lcom/stagnationlab/sk/util/d;

    new-instance v0, Lcom/stagnationlab/sk/d/a;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/stagnationlab/sk/c/b;->z:Lcom/stagnationlab/sk/c/b;

    invoke-direct {v0, v1, v2}, Lcom/stagnationlab/sk/d/a;-><init>(Ljava/lang/String;Lcom/stagnationlab/sk/c/b;)V

    invoke-interface {p1, p2, v0}, Lcom/stagnationlab/sk/util/d;->a(Lokhttp3/ab;Lcom/stagnationlab/sk/d/a;)V

    return-void

    .line 178
    :cond_0
    :try_start_0
    new-instance p1, Lcom/stagnationlab/sk/util/f;

    new-instance v1, Lcom/google/gson/q;

    invoke-direct {v1}, Lcom/google/gson/q;-><init>()V

    .line 179
    invoke-virtual {p2}, Lokhttp3/ab;->g()Lokhttp3/ac;

    move-result-object v2

    invoke-virtual {v2}, Lokhttp3/ac;->d()Ljava/io/Reader;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/gson/q;->a(Ljava/io/Reader;)Lcom/google/gson/l;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/gson/l;->k()Lcom/google/gson/o;

    move-result-object v1

    invoke-direct {p1, v1}, Lcom/stagnationlab/sk/util/f;-><init>(Lcom/google/gson/o;)V

    .line 181
    iget-object v1, p0, Lcom/stagnationlab/sk/util/c$2;->a:Lcom/stagnationlab/sk/util/d;

    invoke-interface {v1, p1}, Lcom/stagnationlab/sk/util/d;->a(Lcom/stagnationlab/sk/util/f;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    const-string v1, "Failed to parse response to JSON"

    .line 183
    invoke-static {v0, v1, p1}, Lcom/stagnationlab/sk/f;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 184
    iget-object v0, p0, Lcom/stagnationlab/sk/util/c$2;->a:Lcom/stagnationlab/sk/util/d;

    new-instance v1, Lcom/stagnationlab/sk/d/a;

    invoke-direct {v1, p1}, Lcom/stagnationlab/sk/d/a;-><init>(Ljava/lang/Exception;)V

    invoke-interface {v0, p2, v1}, Lcom/stagnationlab/sk/util/d;->a(Lokhttp3/ab;Lcom/stagnationlab/sk/d/a;)V

    :goto_0
    return-void
.end method
