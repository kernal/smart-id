.class public Lcom/stagnationlab/sk/util/k;
.super Ljava/lang/Object;
.source "TextViewFormatter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/stagnationlab/sk/util/k$a;
    }
.end annotation


# instance fields
.field private a:Landroid/graphics/Typeface;

.field private b:Landroid/content/Context;

.field private c:Ljava/lang/String;

.field private d:Z

.field private e:I

.field private f:Landroid/text/SpannableStringBuilder;

.field private g:Z


# direct methods
.method private constructor <init>(Landroid/content/Context;Ljava/lang/String;Z)V
    .locals 1

    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 30
    iput-boolean v0, p0, Lcom/stagnationlab/sk/util/k;->d:Z

    const/4 v0, -0x1

    .line 31
    iput v0, p0, Lcom/stagnationlab/sk/util/k;->e:I

    .line 40
    iput-object p1, p0, Lcom/stagnationlab/sk/util/k;->b:Landroid/content/Context;

    .line 41
    iput-object p2, p0, Lcom/stagnationlab/sk/util/k;->c:Ljava/lang/String;

    .line 42
    iput-boolean p3, p0, Lcom/stagnationlab/sk/util/k;->g:Z

    return-void
.end method

.method static synthetic a(Lcom/stagnationlab/sk/util/k;)Landroid/content/Context;
    .locals 0

    .line 23
    iget-object p0, p0, Lcom/stagnationlab/sk/util/k;->b:Landroid/content/Context;

    return-object p0
.end method

.method private a(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .line 149
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 151
    invoke-virtual {v0}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v1

    const-string v2, "portal.smart-id.com"

    invoke-static {v1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 152
    invoke-static {v0}, Lcom/stagnationlab/sk/util/c;->a(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object p1

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object p1

    :cond_0
    return-object p1
.end method

.method public static a(Landroid/content/Context;Landroid/widget/TextView;Ljava/lang/String;)V
    .locals 1

    const/4 v0, 0x0

    .line 46
    invoke-static {p0, p1, p2, v0}, Lcom/stagnationlab/sk/util/k;->a(Landroid/content/Context;Landroid/widget/TextView;Ljava/lang/String;Z)V

    return-void
.end method

.method public static a(Landroid/content/Context;Landroid/widget/TextView;Ljava/lang/String;Z)V
    .locals 1

    .line 50
    new-instance v0, Lcom/stagnationlab/sk/util/k;

    invoke-direct {v0, p0, p2, p3}, Lcom/stagnationlab/sk/util/k;-><init>(Landroid/content/Context;Ljava/lang/String;Z)V

    .line 51
    invoke-direct {v0}, Lcom/stagnationlab/sk/util/k;->b()Ljava/lang/CharSequence;

    move-result-object p0

    .line 53
    invoke-direct {v0}, Lcom/stagnationlab/sk/util/k;->a()Z

    move-result p2

    if-eqz p2, :cond_0

    .line 54
    invoke-static {}, Lcom/stagnationlab/sk/util/a;->a()Lcom/stagnationlab/sk/util/a;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 57
    :cond_0
    invoke-virtual {p1, p0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method private a(Landroid/text/SpannableStringBuilder;)V
    .locals 7

    .line 125
    invoke-virtual {p1}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v0

    const-class v1, Landroid/text/style/StyleSpan;

    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0, v1}, Landroid/text/SpannableStringBuilder;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/text/style/StyleSpan;

    .line 127
    array-length v1, v0

    :goto_0
    if-ge v2, v1, :cond_1

    aget-object v3, v0, v2

    .line 128
    invoke-virtual {v3}, Landroid/text/style/StyleSpan;->getStyle()I

    move-result v4

    const/4 v5, 0x1

    if-ne v4, v5, :cond_0

    .line 129
    new-instance v4, Lcom/stagnationlab/sk/elements/a;

    invoke-direct {p0}, Lcom/stagnationlab/sk/util/k;->f()Landroid/graphics/Typeface;

    move-result-object v5

    const-string v6, ""

    invoke-direct {v4, v6, v5}, Lcom/stagnationlab/sk/elements/a;-><init>(Ljava/lang/String;Landroid/graphics/Typeface;)V

    invoke-direct {p0, p1, v3, v4}, Lcom/stagnationlab/sk/util/k;->a(Landroid/text/SpannableStringBuilder;Ljava/lang/Object;Ljava/lang/Object;)V

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method private a(Landroid/text/SpannableStringBuilder;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 3

    .line 159
    invoke-virtual {p1, p2}, Landroid/text/SpannableStringBuilder;->getSpanStart(Ljava/lang/Object;)I

    move-result v0

    .line 160
    invoke-virtual {p1, p2}, Landroid/text/SpannableStringBuilder;->getSpanEnd(Ljava/lang/Object;)I

    move-result v1

    .line 161
    invoke-virtual {p1, p2}, Landroid/text/SpannableStringBuilder;->getSpanFlags(Ljava/lang/Object;)I

    move-result v2

    .line 163
    invoke-virtual {p1, p2}, Landroid/text/SpannableStringBuilder;->removeSpan(Ljava/lang/Object;)V

    .line 164
    invoke-virtual {p1, p3, v0, v1, v2}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    return-void
.end method

.method private a(Landroid/text/TextPaint;)V
    .locals 1

    .line 168
    invoke-direct {p0}, Lcom/stagnationlab/sk/util/k;->f()Landroid/graphics/Typeface;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 169
    invoke-direct {p0}, Lcom/stagnationlab/sk/util/k;->e()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setColor(I)V

    .line 170
    invoke-direct {p0}, Lcom/stagnationlab/sk/util/k;->d()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setUnderlineText(Z)V

    return-void
.end method

.method static synthetic a(Lcom/stagnationlab/sk/util/k;Landroid/text/TextPaint;)V
    .locals 0

    .line 23
    invoke-direct {p0, p1}, Lcom/stagnationlab/sk/util/k;->a(Landroid/text/TextPaint;)V

    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Lcom/stagnationlab/sk/util/k$a;)V
    .locals 2

    .line 85
    iget-object v0, p0, Lcom/stagnationlab/sk/util/k;->c:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    return-void

    :cond_0
    const/4 v1, 0x1

    .line 91
    iput-boolean v1, p0, Lcom/stagnationlab/sk/util/k;->d:Z

    .line 93
    iget-object v1, p0, Lcom/stagnationlab/sk/util/k;->f:Landroid/text/SpannableStringBuilder;

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result p1

    add-int/2addr p1, v0

    invoke-virtual {v1, v0, p1, p2}, Landroid/text/SpannableStringBuilder;->replace(IILjava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 94
    iget-object p1, p0, Lcom/stagnationlab/sk/util/k;->f:Landroid/text/SpannableStringBuilder;

    new-instance v1, Lcom/stagnationlab/sk/util/k$3;

    invoke-direct {v1, p0, p3}, Lcom/stagnationlab/sk/util/k$3;-><init>(Lcom/stagnationlab/sk/util/k;Lcom/stagnationlab/sk/util/k$a;)V

    .line 107
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result p2

    add-int/2addr p2, v0

    const/16 p3, 0x21

    .line 94
    invoke-virtual {p1, v1, v0, p2, p3}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 111
    iget-object p1, p0, Lcom/stagnationlab/sk/util/k;->f:Landroid/text/SpannableStringBuilder;

    invoke-virtual {p1}, Landroid/text/SpannableStringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/stagnationlab/sk/util/k;->c:Ljava/lang/String;

    return-void
.end method

.method private a()Z
    .locals 1

    .line 61
    iget-boolean v0, p0, Lcom/stagnationlab/sk/util/k;->d:Z

    return v0
.end method

.method private static b(Ljava/lang/String;)Landroid/text/Spanned;
    .locals 2

    .line 200
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x18

    if-lt v0, v1, :cond_0

    const/4 v0, 0x0

    .line 201
    invoke-static {p0, v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;I)Landroid/text/Spanned;

    move-result-object p0

    return-object p0

    .line 204
    :cond_0
    invoke-static {p0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object p0

    return-object p0
.end method

.method private b()Ljava/lang/CharSequence;
    .locals 3

    .line 65
    iget-object v0, p0, Lcom/stagnationlab/sk/util/k;->c:Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return-object v0

    .line 69
    :cond_0
    invoke-direct {p0}, Lcom/stagnationlab/sk/util/k;->c()Landroid/text/SpannableStringBuilder;

    move-result-object v0

    iput-object v0, p0, Lcom/stagnationlab/sk/util/k;->f:Landroid/text/SpannableStringBuilder;

    .line 70
    iget-object v0, p0, Lcom/stagnationlab/sk/util/k;->b:Landroid/content/Context;

    const v1, 0x7f0e0040

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lcom/stagnationlab/sk/util/k$1;

    invoke-direct {v1, p0}, Lcom/stagnationlab/sk/util/k$1;-><init>(Lcom/stagnationlab/sk/util/k;)V

    const-string v2, "{contactUs}"

    invoke-direct {p0, v2, v0, v1}, Lcom/stagnationlab/sk/util/k;->a(Ljava/lang/String;Ljava/lang/String;Lcom/stagnationlab/sk/util/k$a;)V

    .line 75
    iget-object v0, p0, Lcom/stagnationlab/sk/util/k;->b:Landroid/content/Context;

    const v1, 0x7f0e00c1

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lcom/stagnationlab/sk/util/k$2;

    invoke-direct {v1, p0}, Lcom/stagnationlab/sk/util/k$2;-><init>(Lcom/stagnationlab/sk/util/k;)V

    const-string v2, "{store}"

    invoke-direct {p0, v2, v0, v1}, Lcom/stagnationlab/sk/util/k;->a(Ljava/lang/String;Ljava/lang/String;Lcom/stagnationlab/sk/util/k$a;)V

    .line 81
    iget-object v0, p0, Lcom/stagnationlab/sk/util/k;->f:Landroid/text/SpannableStringBuilder;

    return-object v0
.end method

.method private b(Landroid/text/SpannableStringBuilder;)V
    .locals 6

    .line 135
    invoke-virtual {p1}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v0

    const-class v1, Landroid/text/style/URLSpan;

    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0, v1}, Landroid/text/SpannableStringBuilder;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/text/style/URLSpan;

    .line 136
    array-length v1, v0

    :goto_0
    if-ge v2, v1, :cond_0

    aget-object v3, v0, v2

    const/4 v4, 0x1

    .line 137
    iput-boolean v4, p0, Lcom/stagnationlab/sk/util/k;->d:Z

    .line 139
    new-instance v4, Lcom/stagnationlab/sk/util/k$4;

    invoke-virtual {v3}, Landroid/text/style/URLSpan;->getURL()Ljava/lang/String;

    move-result-object v5

    invoke-direct {p0, v5}, Lcom/stagnationlab/sk/util/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, p0, v5}, Lcom/stagnationlab/sk/util/k$4;-><init>(Lcom/stagnationlab/sk/util/k;Ljava/lang/String;)V

    invoke-direct {p0, p1, v3, v4}, Lcom/stagnationlab/sk/util/k;->a(Landroid/text/SpannableStringBuilder;Ljava/lang/Object;Ljava/lang/Object;)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method private c()Landroid/text/SpannableStringBuilder;
    .locals 4

    .line 115
    new-instance v0, Landroid/text/SpannableStringBuilder;

    iget-object v1, p0, Lcom/stagnationlab/sk/util/k;->c:Ljava/lang/String;

    const-string v2, "\n\n"

    const-string v3, "<br><br>"

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/stagnationlab/sk/util/k;->b(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 116
    invoke-direct {p0, v0}, Lcom/stagnationlab/sk/util/k;->a(Landroid/text/SpannableStringBuilder;)V

    .line 117
    invoke-direct {p0, v0}, Lcom/stagnationlab/sk/util/k;->b(Landroid/text/SpannableStringBuilder;)V

    .line 119
    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/stagnationlab/sk/util/k;->c:Ljava/lang/String;

    return-object v0
.end method

.method private d()Z
    .locals 3

    .line 174
    iget-boolean v0, p0, Lcom/stagnationlab/sk/util/k;->g:Z

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    return v1

    .line 178
    :cond_0
    iget v0, p0, Lcom/stagnationlab/sk/util/k;->e:I

    const/4 v2, -0x1

    if-ne v0, v2, :cond_1

    .line 179
    iget-object v0, p0, Lcom/stagnationlab/sk/util/k;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/stagnationlab/sk/i;->d(Landroid/content/Context;)I

    move-result v0

    iput v0, p0, Lcom/stagnationlab/sk/util/k;->e:I

    .line 182
    :cond_1
    iget v0, p0, Lcom/stagnationlab/sk/util/k;->e:I

    const v2, 0x7f0f0007

    if-ne v0, v2, :cond_2

    goto :goto_0

    :cond_2
    const/4 v1, 0x0

    :goto_0
    return v1
.end method

.method private e()I
    .locals 4

    .line 186
    iget-boolean v0, p0, Lcom/stagnationlab/sk/util/k;->g:Z

    if-eqz v0, :cond_0

    .line 187
    iget-object v0, p0, Lcom/stagnationlab/sk/util/k;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f050027

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    return v0

    .line 190
    :cond_0
    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    .line 191
    iget-object v1, p0, Lcom/stagnationlab/sk/util/k;->b:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v1

    const v2, 0x7f030045

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v0, v3}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 192
    iget v0, v0, Landroid/util/TypedValue;->data:I

    return v0

    :cond_1
    const/4 v0, -0x1

    return v0
.end method

.method private f()Landroid/graphics/Typeface;
    .locals 2

    .line 208
    iget-object v0, p0, Lcom/stagnationlab/sk/util/k;->a:Landroid/graphics/Typeface;

    if-nez v0, :cond_0

    .line 209
    iget-object v0, p0, Lcom/stagnationlab/sk/util/k;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v0

    const-string v1, "fonts/Gilroy-ExtraBold.ttf"

    invoke-static {v0, v1}, Landroid/graphics/Typeface;->createFromAsset(Landroid/content/res/AssetManager;Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v0

    iput-object v0, p0, Lcom/stagnationlab/sk/util/k;->a:Landroid/graphics/Typeface;

    .line 212
    :cond_0
    iget-object v0, p0, Lcom/stagnationlab/sk/util/k;->a:Landroid/graphics/Typeface;

    return-object v0
.end method
