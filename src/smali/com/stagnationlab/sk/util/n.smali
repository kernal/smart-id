.class public Lcom/stagnationlab/sk/util/n;
.super Ljava/lang/Object;
.source "ViewAnimation.java"


# instance fields
.field private a:Landroid/content/Context;

.field private b:Landroid/view/View;

.field private c:I

.field private d:Z

.field private e:Z

.field private f:Lcom/stagnationlab/sk/e/b;

.field private g:Landroid/view/animation/Animation;

.field private h:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/view/View;)V
    .locals 1

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 17
    iput v0, p0, Lcom/stagnationlab/sk/util/n;->c:I

    .line 18
    iput-boolean v0, p0, Lcom/stagnationlab/sk/util/n;->d:Z

    .line 19
    iput-boolean v0, p0, Lcom/stagnationlab/sk/util/n;->e:Z

    .line 24
    iput v0, p0, Lcom/stagnationlab/sk/util/n;->h:I

    .line 28
    iput-object p1, p0, Lcom/stagnationlab/sk/util/n;->a:Landroid/content/Context;

    .line 29
    iput-object p2, p0, Lcom/stagnationlab/sk/util/n;->b:Landroid/view/View;

    return-void
.end method

.method static synthetic a(Lcom/stagnationlab/sk/util/n;)Lcom/stagnationlab/sk/e/b;
    .locals 0

    .line 13
    iget-object p0, p0, Lcom/stagnationlab/sk/util/n;->f:Lcom/stagnationlab/sk/e/b;

    return-object p0
.end method

.method public static a(Landroid/app/Activity;)V
    .locals 2

    const v0, 0x7f010010

    const v1, 0x7f010011

    .line 121
    invoke-virtual {p0, v0, v1}, Landroid/app/Activity;->overridePendingTransition(II)V

    return-void
.end method

.method private b(I)Lcom/stagnationlab/sk/util/n;
    .locals 1

    const/4 v0, 0x1

    .line 114
    iput-boolean v0, p0, Lcom/stagnationlab/sk/util/n;->d:Z

    .line 115
    iput p1, p0, Lcom/stagnationlab/sk/util/n;->c:I

    return-object p0
.end method

.method public static b(Landroid/app/Activity;)V
    .locals 2

    const v0, 0x7f010011

    const v1, 0x7f010012

    .line 125
    invoke-virtual {p0, v0, v1}, Landroid/app/Activity;->overridePendingTransition(II)V

    return-void
.end method

.method public static c(Landroid/app/Activity;)V
    .locals 2

    const v0, 0x7f01000e

    const v1, 0x7f01000d

    .line 129
    invoke-virtual {p0, v0, v1}, Landroid/app/Activity;->overridePendingTransition(II)V

    return-void
.end method

.method private f()V
    .locals 2

    .line 71
    iget-object v0, p0, Lcom/stagnationlab/sk/util/n;->a:Landroid/content/Context;

    iget v1, p0, Lcom/stagnationlab/sk/util/n;->c:I

    invoke-static {v0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/stagnationlab/sk/util/n;->g:Landroid/view/animation/Animation;

    return-void
.end method

.method private g()V
    .locals 3

    .line 75
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    const/high16 v1, 0x3f800000    # 1.0f

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    iput-object v0, p0, Lcom/stagnationlab/sk/util/n;->g:Landroid/view/animation/Animation;

    .line 76
    iget-object v0, p0, Lcom/stagnationlab/sk/util/n;->g:Landroid/view/animation/Animation;

    iget-object v1, p0, Lcom/stagnationlab/sk/util/n;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090002

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    int-to-long v1, v1

    invoke-virtual {v0, v1, v2}, Landroid/view/animation/Animation;->setDuration(J)V

    return-void
.end method


# virtual methods
.method public a(I)Lcom/stagnationlab/sk/util/n;
    .locals 0

    .line 108
    iput p1, p0, Lcom/stagnationlab/sk/util/n;->h:I

    return-object p0
.end method

.method public a(Lcom/stagnationlab/sk/e/b;)Lcom/stagnationlab/sk/util/n;
    .locals 0

    .line 86
    iput-object p1, p0, Lcom/stagnationlab/sk/util/n;->f:Lcom/stagnationlab/sk/e/b;

    return-object p0
.end method

.method public a()V
    .locals 3

    .line 33
    iget-boolean v0, p0, Lcom/stagnationlab/sk/util/n;->e:Z

    if-eqz v0, :cond_0

    .line 34
    invoke-direct {p0}, Lcom/stagnationlab/sk/util/n;->g()V

    goto :goto_0

    .line 35
    :cond_0
    iget-boolean v0, p0, Lcom/stagnationlab/sk/util/n;->d:Z

    if-eqz v0, :cond_1

    .line 36
    invoke-direct {p0}, Lcom/stagnationlab/sk/util/n;->f()V

    .line 39
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/stagnationlab/sk/util/n;->g:Landroid/view/animation/Animation;

    if-nez v0, :cond_3

    .line 40
    iget-object v0, p0, Lcom/stagnationlab/sk/util/n;->f:Lcom/stagnationlab/sk/e/b;

    if-eqz v0, :cond_2

    .line 41
    invoke-interface {v0}, Lcom/stagnationlab/sk/e/b;->a()V

    :cond_2
    return-void

    .line 45
    :cond_3
    iget v1, p0, Lcom/stagnationlab/sk/util/n;->h:I

    int-to-long v1, v1

    invoke-virtual {v0, v1, v2}, Landroid/view/animation/Animation;->setStartOffset(J)V

    .line 48
    iget-object v0, p0, Lcom/stagnationlab/sk/util/n;->f:Lcom/stagnationlab/sk/e/b;

    if-eqz v0, :cond_4

    .line 49
    iget-object v0, p0, Lcom/stagnationlab/sk/util/n;->g:Landroid/view/animation/Animation;

    new-instance v1, Lcom/stagnationlab/sk/util/n$1;

    invoke-direct {v1, p0}, Lcom/stagnationlab/sk/util/n$1;-><init>(Lcom/stagnationlab/sk/util/n;)V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 67
    :cond_4
    iget-object v0, p0, Lcom/stagnationlab/sk/util/n;->b:Landroid/view/View;

    iget-object v1, p0, Lcom/stagnationlab/sk/util/n;->g:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    return-void
.end method

.method public b()Lcom/stagnationlab/sk/util/n;
    .locals 1

    const v0, 0x7f01000d

    .line 92
    invoke-direct {p0, v0}, Lcom/stagnationlab/sk/util/n;->b(I)Lcom/stagnationlab/sk/util/n;

    move-result-object v0

    return-object v0
.end method

.method public c()Lcom/stagnationlab/sk/util/n;
    .locals 1

    const v0, 0x7f01000f

    .line 96
    invoke-direct {p0, v0}, Lcom/stagnationlab/sk/util/n;->b(I)Lcom/stagnationlab/sk/util/n;

    move-result-object v0

    return-object v0
.end method

.method public d()Lcom/stagnationlab/sk/util/n;
    .locals 1

    const v0, 0x7f01000c

    .line 100
    invoke-direct {p0, v0}, Lcom/stagnationlab/sk/util/n;->b(I)Lcom/stagnationlab/sk/util/n;

    move-result-object v0

    return-object v0
.end method

.method public e()Lcom/stagnationlab/sk/util/n;
    .locals 1

    const v0, 0x7f01000e

    .line 104
    invoke-direct {p0, v0}, Lcom/stagnationlab/sk/util/n;->b(I)Lcom/stagnationlab/sk/util/n;

    move-result-object v0

    return-object v0
.end method
