.class public Lcom/stagnationlab/sk/PinFragment;
.super Landroid/app/Fragment;
.source "PinFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/stagnationlab/sk/PinFragment$a;
    }
.end annotation


# instance fields
.field private a:Landroid/os/Vibrator;

.field private b:Landroid/view/LayoutInflater;

.field private c:Z

.field private d:Landroid/view/View;

.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private f:Landroid/widget/Button;

.field private g:Landroid/os/Handler;

.field private h:I

.field private i:Ljava/lang/String;

.field private j:I

.field private k:I

.field private l:Lcom/stagnationlab/sk/PinFragment$a;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 21
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    const/4 v0, 0x1

    .line 27
    iput-boolean v0, p0, Lcom/stagnationlab/sk/PinFragment;->c:Z

    const/4 v0, 0x0

    .line 30
    iput-object v0, p0, Lcom/stagnationlab/sk/PinFragment;->f:Landroid/widget/Button;

    .line 32
    iput-object v0, p0, Lcom/stagnationlab/sk/PinFragment;->g:Landroid/os/Handler;

    const/4 v0, 0x0

    .line 33
    iput v0, p0, Lcom/stagnationlab/sk/PinFragment;->h:I

    const/16 v0, 0xc

    .line 35
    iput v0, p0, Lcom/stagnationlab/sk/PinFragment;->j:I

    const/4 v0, 0x4

    .line 36
    iput v0, p0, Lcom/stagnationlab/sk/PinFragment;->k:I

    return-void
.end method

.method private a(Landroid/view/ViewGroup;IILandroid/view/View$OnClickListener;)Landroid/view/View;
    .locals 2

    .line 155
    iget-object v0, p0, Lcom/stagnationlab/sk/PinFragment;->b:Landroid/view/LayoutInflater;

    const/4 v1, 0x0

    invoke-virtual {v0, p3, p1, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p3

    if-eqz p4, :cond_0

    .line 158
    invoke-virtual {p3, p4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 160
    new-instance p4, Lcom/stagnationlab/sk/PinFragment$4;

    invoke-direct {p4, p0}, Lcom/stagnationlab/sk/PinFragment$4;-><init>(Lcom/stagnationlab/sk/PinFragment;)V

    invoke-virtual {p3, p4}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 174
    :cond_0
    invoke-virtual {p1, p3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    if-nez p2, :cond_1

    const/16 p1, 0x9

    goto :goto_0

    :cond_1
    const/4 p1, 0x2

    if-ne p2, p1, :cond_2

    const/16 p1, 0xb

    goto :goto_0

    :cond_2
    const/16 p1, 0xe

    .line 185
    :goto_0
    invoke-virtual {p3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object p2

    check-cast p2, Landroid/widget/RelativeLayout$LayoutParams;

    .line 186
    invoke-virtual {p2, p1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    const/16 p1, 0xf

    .line 187
    invoke-virtual {p2, p1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 188
    invoke-virtual {p3, p2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    return-object p3
.end method

.method static synthetic a(Lcom/stagnationlab/sk/PinFragment;Ljava/lang/String;Z)V
    .locals 0

    .line 21
    invoke-direct {p0, p1, p2}, Lcom/stagnationlab/sk/PinFragment;->a(Ljava/lang/String;Z)V

    return-void
.end method

.method private a(Ljava/lang/String;Z)V
    .locals 4

    .line 194
    iput-object p1, p0, Lcom/stagnationlab/sk/PinFragment;->i:Ljava/lang/String;

    .line 196
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    .line 197
    iget v1, p0, Lcom/stagnationlab/sk/PinFragment;->k:I

    if-lt v0, v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    .line 199
    :goto_0
    invoke-direct {p0}, Lcom/stagnationlab/sk/PinFragment;->j()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 200
    iget-object v2, p0, Lcom/stagnationlab/sk/PinFragment;->f:Landroid/widget/Button;

    if-eqz v1, :cond_1

    const/high16 v3, 0x3f800000    # 1.0f

    goto :goto_1

    :cond_1
    const v3, 0x3e99999a    # 0.3f

    :goto_1
    invoke-virtual {v2, v3}, Landroid/widget/Button;->setAlpha(F)V

    :cond_2
    if-eqz p2, :cond_3

    .line 204
    iget-object p2, p0, Lcom/stagnationlab/sk/PinFragment;->l:Lcom/stagnationlab/sk/PinFragment$a;

    invoke-interface {p2, v0}, Lcom/stagnationlab/sk/PinFragment$a;->a(I)V

    .line 205
    invoke-direct {p0}, Lcom/stagnationlab/sk/PinFragment;->j()Z

    move-result p2

    if-nez p2, :cond_3

    if-eqz v1, :cond_3

    .line 206
    iget-object p2, p0, Lcom/stagnationlab/sk/PinFragment;->l:Lcom/stagnationlab/sk/PinFragment$a;

    invoke-interface {p2, p1}, Lcom/stagnationlab/sk/PinFragment$a;->a(Ljava/lang/String;)V

    :cond_3
    return-void
.end method

.method static synthetic a(Lcom/stagnationlab/sk/PinFragment;)Z
    .locals 0

    .line 21
    invoke-direct {p0}, Lcom/stagnationlab/sk/PinFragment;->f()Z

    move-result p0

    return p0
.end method

.method static synthetic b(Lcom/stagnationlab/sk/PinFragment;)Ljava/lang/String;
    .locals 0

    .line 21
    iget-object p0, p0, Lcom/stagnationlab/sk/PinFragment;->i:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic c(Lcom/stagnationlab/sk/PinFragment;)I
    .locals 0

    .line 21
    iget p0, p0, Lcom/stagnationlab/sk/PinFragment;->j:I

    return p0
.end method

.method static synthetic d(Lcom/stagnationlab/sk/PinFragment;)Lcom/stagnationlab/sk/PinFragment$a;
    .locals 0

    .line 21
    iget-object p0, p0, Lcom/stagnationlab/sk/PinFragment;->l:Lcom/stagnationlab/sk/PinFragment$a;

    return-object p0
.end method

.method static synthetic e(Lcom/stagnationlab/sk/PinFragment;)I
    .locals 0

    .line 21
    iget p0, p0, Lcom/stagnationlab/sk/PinFragment;->k:I

    return p0
.end method

.method private f()Z
    .locals 1

    .line 212
    iget-boolean v0, p0, Lcom/stagnationlab/sk/PinFragment;->c:Z

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return v0

    :cond_0
    const/4 v0, 0x0

    .line 216
    invoke-virtual {p0, v0}, Lcom/stagnationlab/sk/PinFragment;->a(Ljava/lang/String;)V

    const/4 v0, 0x1

    return v0
.end method

.method static synthetic f(Lcom/stagnationlab/sk/PinFragment;)Z
    .locals 0

    .line 21
    iget-boolean p0, p0, Lcom/stagnationlab/sk/PinFragment;->c:Z

    return p0
.end method

.method private g()V
    .locals 1

    .line 222
    iget-object v0, p0, Lcom/stagnationlab/sk/PinFragment;->l:Lcom/stagnationlab/sk/PinFragment$a;

    invoke-interface {v0}, Lcom/stagnationlab/sk/PinFragment$a;->a()V

    return-void
.end method

.method static synthetic g(Lcom/stagnationlab/sk/PinFragment;)V
    .locals 0

    .line 21
    invoke-direct {p0}, Lcom/stagnationlab/sk/PinFragment;->h()V

    return-void
.end method

.method static synthetic h(Lcom/stagnationlab/sk/PinFragment;)I
    .locals 0

    .line 21
    iget p0, p0, Lcom/stagnationlab/sk/PinFragment;->h:I

    return p0
.end method

.method private h()V
    .locals 3

    .line 230
    iget-object v0, p0, Lcom/stagnationlab/sk/PinFragment;->a:Landroid/os/Vibrator;

    const-wide/16 v1, 0x14

    invoke-virtual {v0, v1, v2}, Landroid/os/Vibrator;->vibrate(J)V

    return-void
.end method

.method static synthetic i(Lcom/stagnationlab/sk/PinFragment;)I
    .locals 2

    .line 21
    iget v0, p0, Lcom/stagnationlab/sk/PinFragment;->h:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/stagnationlab/sk/PinFragment;->h:I

    return v0
.end method

.method private i()V
    .locals 4

    .line 254
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/stagnationlab/sk/PinFragment;->g:Landroid/os/Handler;

    .line 255
    iget-object v0, p0, Lcom/stagnationlab/sk/PinFragment;->g:Landroid/os/Handler;

    new-instance v1, Lcom/stagnationlab/sk/PinFragment$5;

    invoke-direct {v1, p0}, Lcom/stagnationlab/sk/PinFragment$5;-><init>(Lcom/stagnationlab/sk/PinFragment;)V

    const-wide/16 v2, 0x12c

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method static synthetic j(Lcom/stagnationlab/sk/PinFragment;)V
    .locals 0

    .line 21
    invoke-direct {p0}, Lcom/stagnationlab/sk/PinFragment;->i()V

    return-void
.end method

.method private j()Z
    .locals 2

    .line 301
    iget v0, p0, Lcom/stagnationlab/sk/PinFragment;->k:I

    iget v1, p0, Lcom/stagnationlab/sk/PinFragment;->j:I

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method


# virtual methods
.method public a()V
    .locals 2

    const-string v0, ""

    const/4 v1, 0x1

    .line 41
    invoke-direct {p0, v0, v1}, Lcom/stagnationlab/sk/PinFragment;->a(Ljava/lang/String;Z)V

    return-void
.end method

.method public a(II)V
    .locals 1

    .line 45
    iput p1, p0, Lcom/stagnationlab/sk/PinFragment;->k:I

    .line 46
    iput p2, p0, Lcom/stagnationlab/sk/PinFragment;->j:I

    const/4 p1, 0x0

    .line 47
    iput p1, p0, Lcom/stagnationlab/sk/PinFragment;->h:I

    .line 49
    iget-object p2, p0, Lcom/stagnationlab/sk/PinFragment;->g:Landroid/os/Handler;

    if-eqz p2, :cond_0

    const/4 v0, 0x0

    .line 50
    invoke-virtual {p2, v0}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 53
    :cond_0
    iget-object p2, p0, Lcom/stagnationlab/sk/PinFragment;->f:Landroid/widget/Button;

    invoke-direct {p0}, Lcom/stagnationlab/sk/PinFragment;->j()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    const/4 v0, 0x4

    :goto_0
    invoke-virtual {p2, v0}, Landroid/widget/Button;->setVisibility(I)V

    .line 54
    iget-object p2, p0, Lcom/stagnationlab/sk/PinFragment;->f:Landroid/widget/Button;

    invoke-direct {p0}, Lcom/stagnationlab/sk/PinFragment;->j()Z

    move-result v0

    invoke-virtual {p2, v0}, Landroid/widget/Button;->setEnabled(Z)V

    const-string p2, ""

    .line 56
    invoke-direct {p0, p2, p1}, Lcom/stagnationlab/sk/PinFragment;->a(Ljava/lang/String;Z)V

    return-void
.end method

.method public a(Lcom/stagnationlab/sk/PinFragment$a;)V
    .locals 0

    .line 234
    iput-object p1, p0, Lcom/stagnationlab/sk/PinFragment;->l:Lcom/stagnationlab/sk/PinFragment$a;

    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 1

    .line 226
    iget-object v0, p0, Lcom/stagnationlab/sk/PinFragment;->l:Lcom/stagnationlab/sk/PinFragment$a;

    invoke-interface {v0, p1}, Lcom/stagnationlab/sk/PinFragment$a;->b(Ljava/lang/String;)V

    return-void
.end method

.method public a(Z)V
    .locals 2

    .line 238
    iget-object v0, p0, Lcom/stagnationlab/sk/PinFragment;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    .line 239
    invoke-virtual {v1, p1}, Landroid/view/View;->setEnabled(Z)V

    goto :goto_0

    .line 241
    :cond_0
    iget-object v0, p0, Lcom/stagnationlab/sk/PinFragment;->f:Landroid/widget/Button;

    if-eqz p1, :cond_1

    invoke-direct {p0}, Lcom/stagnationlab/sk/PinFragment;->j()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 242
    iput-boolean p1, p0, Lcom/stagnationlab/sk/PinFragment;->c:Z

    return-void
.end method

.method public b()V
    .locals 0

    .line 246
    invoke-direct {p0}, Lcom/stagnationlab/sk/PinFragment;->g()V

    return-void
.end method

.method public c()V
    .locals 1

    .line 282
    iget-boolean v0, p0, Lcom/stagnationlab/sk/PinFragment;->c:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    .line 283
    iput v0, p0, Lcom/stagnationlab/sk/PinFragment;->h:I

    .line 284
    invoke-direct {p0}, Lcom/stagnationlab/sk/PinFragment;->i()V

    :cond_0
    return-void
.end method

.method public d()V
    .locals 5

    .line 289
    iget v0, p0, Lcom/stagnationlab/sk/PinFragment;->h:I

    const/4 v1, 0x0

    if-nez v0, :cond_0

    .line 290
    invoke-virtual {p0, v1}, Lcom/stagnationlab/sk/PinFragment;->a(Ljava/lang/String;)V

    .line 292
    iget-object v0, p0, Lcom/stagnationlab/sk/PinFragment;->i:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_0

    .line 293
    iget-object v0, p0, Lcom/stagnationlab/sk/PinFragment;->i:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    const/4 v4, 0x1

    sub-int/2addr v3, v4

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, v4}, Lcom/stagnationlab/sk/PinFragment;->a(Ljava/lang/String;Z)V

    .line 297
    :cond_0
    iget-object v0, p0, Lcom/stagnationlab/sk/PinFragment;->g:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    return-void
.end method

.method public e()Z
    .locals 1

    .line 305
    iget-boolean v0, p0, Lcom/stagnationlab/sk/PinFragment;->c:Z

    return v0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 10

    .line 71
    iput-object p1, p0, Lcom/stagnationlab/sk/PinFragment;->b:Landroid/view/LayoutInflater;

    const/4 p3, 0x0

    const v0, 0x7f0a0026

    .line 73
    invoke-virtual {p1, v0, p2, p3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    iput-object p2, p0, Lcom/stagnationlab/sk/PinFragment;->d:Landroid/view/View;

    .line 74
    invoke-virtual {p0}, Lcom/stagnationlab/sk/PinFragment;->getActivity()Landroid/app/Activity;

    move-result-object p2

    const-string v0, "vibrator"

    invoke-virtual {p2, v0}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Landroid/os/Vibrator;

    iput-object p2, p0, Lcom/stagnationlab/sk/PinFragment;->a:Landroid/os/Vibrator;

    .line 76
    new-instance p2, Lcom/stagnationlab/sk/PinFragment$1;

    invoke-direct {p2, p0}, Lcom/stagnationlab/sk/PinFragment$1;-><init>(Lcom/stagnationlab/sk/PinFragment;)V

    .line 92
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/stagnationlab/sk/PinFragment;->e:Ljava/util/List;

    .line 94
    iget-object v0, p0, Lcom/stagnationlab/sk/PinFragment;->d:Landroid/view/View;

    const v1, 0x7f080089

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    :goto_0
    const v2, 0x7f0a0038

    const v3, 0x7f0a003b

    const/4 v4, 0x3

    const/4 v5, 0x1

    if-ge v1, v4, :cond_1

    .line 97
    invoke-virtual {p1, v3, v0, p3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/RelativeLayout;

    const/4 v6, 0x0

    :goto_1
    if-ge v6, v4, :cond_0

    mul-int/lit8 v7, v1, 0x3

    add-int/2addr v7, v6

    add-int/2addr v7, v5

    .line 100
    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    .line 101
    invoke-direct {p0, v3, v6, v2, p2}, Lcom/stagnationlab/sk/PinFragment;->a(Landroid/view/ViewGroup;IILandroid/view/View$OnClickListener;)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/Button;

    .line 102
    iget-object v9, p0, Lcom/stagnationlab/sk/PinFragment;->e:Ljava/util/List;

    invoke-interface {v9, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 104
    invoke-virtual {v8, v7}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    .line 106
    :cond_0
    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 109
    :cond_1
    invoke-virtual {p1, v3, v0, p3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/RelativeLayout;

    const v1, 0x7f0a0039

    .line 111
    new-instance v3, Lcom/stagnationlab/sk/PinFragment$2;

    invoke-direct {v3, p0}, Lcom/stagnationlab/sk/PinFragment$2;-><init>(Lcom/stagnationlab/sk/PinFragment;)V

    invoke-direct {p0, p1, p3, v1, v3}, Lcom/stagnationlab/sk/PinFragment;->a(Landroid/view/ViewGroup;IILandroid/view/View$OnClickListener;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    .line 123
    invoke-direct {p0, p1, v5, v2, p2}, Lcom/stagnationlab/sk/PinFragment;->a(Landroid/view/ViewGroup;IILandroid/view/View$OnClickListener;)Landroid/view/View;

    move-result-object p2

    check-cast p2, Landroid/widget/Button;

    const-string v2, "0"

    .line 124
    invoke-virtual {p2, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 126
    iget-object v2, p0, Lcom/stagnationlab/sk/PinFragment;->e:Ljava/util/List;

    invoke-interface {v2, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 127
    iget-object p2, p0, Lcom/stagnationlab/sk/PinFragment;->e:Ljava/util/List;

    invoke-interface {p2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const/4 p2, 0x2

    const v1, 0x7f0a003a

    .line 130
    new-instance v2, Lcom/stagnationlab/sk/PinFragment$3;

    invoke-direct {v2, p0}, Lcom/stagnationlab/sk/PinFragment$3;-><init>(Lcom/stagnationlab/sk/PinFragment;)V

    invoke-direct {p0, p1, p2, v1, v2}, Lcom/stagnationlab/sk/PinFragment;->a(Landroid/view/ViewGroup;IILandroid/view/View$OnClickListener;)Landroid/view/View;

    move-result-object p2

    check-cast p2, Landroid/widget/Button;

    iput-object p2, p0, Lcom/stagnationlab/sk/PinFragment;->f:Landroid/widget/Button;

    .line 144
    iget-object p2, p0, Lcom/stagnationlab/sk/PinFragment;->f:Landroid/widget/Button;

    invoke-virtual {p2, p3}, Landroid/widget/Button;->setEnabled(Z)V

    .line 145
    iget-object p2, p0, Lcom/stagnationlab/sk/PinFragment;->f:Landroid/widget/Button;

    const/4 v1, 0x4

    invoke-virtual {p2, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 147
    invoke-virtual {v0, p1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    const-string p1, ""

    .line 149
    invoke-direct {p0, p1, p3}, Lcom/stagnationlab/sk/PinFragment;->a(Ljava/lang/String;Z)V

    .line 151
    iget-object p1, p0, Lcom/stagnationlab/sk/PinFragment;->d:Landroid/view/View;

    return-object p1
.end method
