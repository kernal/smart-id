.class public Lcom/stagnationlab/sk/c/a;
.super Ljava/lang/Object;
.source "AppError.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/stagnationlab/sk/c/a;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private a:Lcom/stagnationlab/sk/c/c;

.field private b:Ljava/lang/String;

.field private c:Lcom/stagnationlab/sk/c/b;

.field private d:Lcom/stagnationlab/sk/models/AccountKey;

.field private e:Ljava/lang/String;

.field private f:Z

.field private g:Z

.field private transient h:Lee/cyber/smartid/dto/SmartIdError;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 304
    new-instance v0, Lcom/stagnationlab/sk/c/a$1;

    invoke-direct {v0}, Lcom/stagnationlab/sk/c/a$1;-><init>()V

    sput-object v0, Lcom/stagnationlab/sk/c/a;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .line 321
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 24
    iput-boolean v0, p0, Lcom/stagnationlab/sk/c/a;->f:Z

    .line 25
    iput-boolean v0, p0, Lcom/stagnationlab/sk/c/a;->g:Z

    .line 322
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/stagnationlab/sk/c/c;->valueOf(Ljava/lang/String;)Lcom/stagnationlab/sk/c/c;

    move-result-object v0

    iput-object v0, p0, Lcom/stagnationlab/sk/c/a;->a:Lcom/stagnationlab/sk/c/c;

    .line 323
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/stagnationlab/sk/c/a;->b:Ljava/lang/String;

    .line 324
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/stagnationlab/sk/c/b;->valueOf(Ljava/lang/String;)Lcom/stagnationlab/sk/c/b;

    move-result-object v0

    iput-object v0, p0, Lcom/stagnationlab/sk/c/a;->c:Lcom/stagnationlab/sk/c/b;

    .line 325
    const-class v0, Lcom/stagnationlab/sk/models/AccountKey;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/stagnationlab/sk/models/AccountKey;

    iput-object v0, p0, Lcom/stagnationlab/sk/c/a;->d:Lcom/stagnationlab/sk/models/AccountKey;

    .line 326
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/stagnationlab/sk/c/a;->e:Ljava/lang/String;

    .line 327
    invoke-static {p1}, Lcom/stagnationlab/sk/i;->b(Landroid/os/Parcel;)Z

    move-result p1

    iput-boolean p1, p0, Lcom/stagnationlab/sk/c/a;->f:Z

    return-void
.end method

.method public constructor <init>(Lcom/stagnationlab/sk/c/b;)V
    .locals 2

    .line 52
    invoke-static {p1}, Lcom/stagnationlab/sk/c/a;->a(Lcom/stagnationlab/sk/c/b;)Lcom/stagnationlab/sk/c/c;

    move-result-object v0

    invoke-virtual {p1}, Lcom/stagnationlab/sk/c/b;->a()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, p1, v1}, Lcom/stagnationlab/sk/c/a;-><init>(Lcom/stagnationlab/sk/c/c;Lcom/stagnationlab/sk/c/b;Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Lcom/stagnationlab/sk/c/c;Lcom/stagnationlab/sk/c/b;Ljava/lang/String;)V
    .locals 1

    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 24
    iput-boolean v0, p0, Lcom/stagnationlab/sk/c/a;->f:Z

    .line 25
    iput-boolean v0, p0, Lcom/stagnationlab/sk/c/a;->g:Z

    .line 56
    iput-object p1, p0, Lcom/stagnationlab/sk/c/a;->a:Lcom/stagnationlab/sk/c/c;

    .line 57
    iput-object p2, p0, Lcom/stagnationlab/sk/c/a;->c:Lcom/stagnationlab/sk/c/b;

    .line 58
    iput-object p3, p0, Lcom/stagnationlab/sk/c/a;->e:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lee/cyber/smartid/dto/SmartIdError;Ljava/lang/String;)V
    .locals 3

    .line 29
    sget-object v0, Lcom/stagnationlab/sk/c/c;->a:Lcom/stagnationlab/sk/c/c;

    invoke-virtual {p1}, Lee/cyber/smartid/dto/SmartIdError;->getCode()J

    move-result-wide v1

    invoke-static {v1, v2, p1}, Lcom/stagnationlab/sk/c/a;->a(JLee/cyber/smartid/dto/SmartIdError;)Lcom/stagnationlab/sk/c/b;

    move-result-object v1

    invoke-virtual {p1}, Lee/cyber/smartid/dto/SmartIdError;->getHumanReadableErrorCodeName()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v0, v1, v2}, Lcom/stagnationlab/sk/c/a;-><init>(Lcom/stagnationlab/sk/c/c;Lcom/stagnationlab/sk/c/b;Ljava/lang/String;)V

    .line 30
    iput-object p1, p0, Lcom/stagnationlab/sk/c/a;->h:Lee/cyber/smartid/dto/SmartIdError;

    .line 31
    invoke-virtual {p1}, Lee/cyber/smartid/dto/SmartIdError;->getMessage()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/stagnationlab/sk/c/a;->b:Ljava/lang/String;

    const/4 v0, 0x0

    .line 34
    :try_start_0
    invoke-virtual {p1}, Lee/cyber/smartid/dto/SmartIdError;->getServiceAccountKeyStatus()Lee/cyber/smartid/dto/jsonrpc/ServiceAccountKeyStatus;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/stagnationlab/sk/c/a;->a(Lee/cyber/smartid/dto/jsonrpc/ServiceAccountKeyStatus;)Lcom/stagnationlab/sk/models/AccountKey;

    move-result-object p1

    iput-object p1, p0, Lcom/stagnationlab/sk/c/a;->d:Lcom/stagnationlab/sk/models/AccountKey;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 36
    :catch_0
    iput-object v0, p0, Lcom/stagnationlab/sk/c/a;->d:Lcom/stagnationlab/sk/models/AccountKey;

    .line 39
    :goto_0
    invoke-virtual {p0}, Lcom/stagnationlab/sk/c/a;->g()Z

    move-result p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/stagnationlab/sk/c/a;->d:Lcom/stagnationlab/sk/models/AccountKey;

    if-nez p1, :cond_0

    .line 41
    sget-object p1, Lcom/stagnationlab/sk/c/c;->b:Lcom/stagnationlab/sk/c/c;

    iput-object p1, p0, Lcom/stagnationlab/sk/c/a;->a:Lcom/stagnationlab/sk/c/c;

    .line 42
    sget-object p1, Lcom/stagnationlab/sk/c/b;->T:Lcom/stagnationlab/sk/c/b;

    iput-object p1, p0, Lcom/stagnationlab/sk/c/a;->c:Lcom/stagnationlab/sk/c/b;

    .line 43
    sget-object p1, Lcom/stagnationlab/sk/c/b;->T:Lcom/stagnationlab/sk/c/b;

    invoke-virtual {p1}, Lcom/stagnationlab/sk/c/b;->a()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/stagnationlab/sk/c/a;->e:Ljava/lang/String;

    .line 44
    iput-object v0, p0, Lcom/stagnationlab/sk/c/a;->h:Lee/cyber/smartid/dto/SmartIdError;

    .line 45
    iput-object v0, p0, Lcom/stagnationlab/sk/c/a;->b:Ljava/lang/String;

    goto :goto_1

    .line 47
    :cond_0
    invoke-direct {p0, p2}, Lcom/stagnationlab/sk/c/a;->b(Ljava/lang/String;)V

    :goto_1
    return-void
.end method

.method private static a(JLee/cyber/smartid/dto/SmartIdError;)Lcom/stagnationlab/sk/c/b;
    .locals 3

    const-wide/16 v0, -0x791a

    cmp-long v2, p0, v0

    if-nez v2, :cond_0

    .line 64
    sget-object p0, Lcom/stagnationlab/sk/c/b;->f:Lcom/stagnationlab/sk/c/b;

    return-object p0

    :cond_0
    const-wide/16 v0, -0x7922

    cmp-long v2, p0, v0

    if-nez v2, :cond_1

    .line 68
    sget-object p0, Lcom/stagnationlab/sk/c/b;->j:Lcom/stagnationlab/sk/c/b;

    return-object p0

    :cond_1
    const-wide/16 v0, -0x7923

    cmp-long v2, p0, v0

    if-nez v2, :cond_2

    .line 72
    sget-object p0, Lcom/stagnationlab/sk/c/b;->i:Lcom/stagnationlab/sk/c/b;

    return-object p0

    :cond_2
    const-wide/16 v0, -0x791f

    cmp-long v2, p0, v0

    if-nez v2, :cond_4

    .line 76
    invoke-virtual {p2}, Lee/cyber/smartid/dto/SmartIdError;->getRawErrorData()Ljava/lang/String;

    move-result-object p0

    invoke-static {p0}, Lcom/stagnationlab/sk/c/a;->a(Ljava/lang/String;)Ljava/util/Map;

    move-result-object p0

    if-eqz p0, :cond_3

    const-string p1, "resultCode"

    .line 78
    invoke-interface {p0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    const-string p1, "TIMEOUT"

    invoke-virtual {p1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_3

    .line 79
    sget-object p0, Lcom/stagnationlab/sk/c/b;->i:Lcom/stagnationlab/sk/c/b;

    return-object p0

    .line 81
    :cond_3
    sget-object p0, Lcom/stagnationlab/sk/c/b;->h:Lcom/stagnationlab/sk/c/b;

    return-object p0

    :cond_4
    const-wide/16 v0, -0x792b

    cmp-long v2, p0, v0

    if-nez v2, :cond_5

    .line 85
    sget-object p0, Lcom/stagnationlab/sk/c/b;->o:Lcom/stagnationlab/sk/c/b;

    return-object p0

    :cond_5
    const-wide/16 v0, -0x7921

    cmp-long v2, p0, v0

    if-nez v2, :cond_6

    .line 89
    sget-object p0, Lcom/stagnationlab/sk/c/b;->e:Lcom/stagnationlab/sk/c/b;

    return-object p0

    :cond_6
    const-wide/16 v0, 0x3e8

    cmp-long v2, p0, v0

    if-eqz v2, :cond_16

    const-wide/16 v0, 0x40e

    cmp-long v2, p0, v0

    if-nez v2, :cond_7

    goto/16 :goto_1

    :cond_7
    const-wide/16 v0, 0x3e9

    cmp-long v2, p0, v0

    if-nez v2, :cond_8

    .line 100
    sget-object p0, Lcom/stagnationlab/sk/c/b;->b:Lcom/stagnationlab/sk/c/b;

    return-object p0

    :cond_8
    const-wide/16 v0, -0x7919

    cmp-long v2, p0, v0

    if-eqz v2, :cond_15

    const-wide/16 v0, 0x3ee

    cmp-long v2, p0, v0

    if-nez v2, :cond_9

    goto :goto_0

    :cond_9
    const-wide/16 v0, -0x7920

    cmp-long v2, p0, v0

    if-nez v2, :cond_a

    .line 112
    sget-object p0, Lcom/stagnationlab/sk/c/b;->l:Lcom/stagnationlab/sk/c/b;

    return-object p0

    :cond_a
    const-wide/16 v0, -0x7918

    cmp-long v2, p0, v0

    if-nez v2, :cond_b

    .line 116
    sget-object p0, Lcom/stagnationlab/sk/c/b;->k:Lcom/stagnationlab/sk/c/b;

    return-object p0

    :cond_b
    const-wide/16 v0, 0x3fe

    cmp-long v2, p0, v0

    if-nez v2, :cond_c

    .line 121
    sget-object p0, Lcom/stagnationlab/sk/c/b;->s:Lcom/stagnationlab/sk/c/b;

    return-object p0

    :cond_c
    const-wide/16 v0, 0x44d

    cmp-long v2, p0, v0

    if-nez v2, :cond_d

    .line 125
    sget-object p0, Lcom/stagnationlab/sk/c/b;->p:Lcom/stagnationlab/sk/c/b;

    return-object p0

    :cond_d
    const-wide/16 v0, 0x44c

    cmp-long v2, p0, v0

    if-nez v2, :cond_e

    .line 129
    sget-object p0, Lcom/stagnationlab/sk/c/b;->q:Lcom/stagnationlab/sk/c/b;

    return-object p0

    :cond_e
    const-wide/16 v0, -0x7f5b

    cmp-long v2, p0, v0

    if-nez v2, :cond_f

    .line 133
    sget-object p0, Lcom/stagnationlab/sk/c/b;->r:Lcom/stagnationlab/sk/c/b;

    return-object p0

    :cond_f
    const-wide/16 v0, 0x3f4    # 5.0E-321

    cmp-long v2, p0, v0

    if-nez v2, :cond_11

    if-eqz p2, :cond_10

    .line 139
    invoke-virtual {p2}, Lee/cyber/smartid/dto/SmartIdError;->getInvalidTransactionStateSource()I

    move-result p0

    const/4 p1, 0x1

    if-ne p0, p1, :cond_10

    .line 141
    sget-object p0, Lcom/stagnationlab/sk/c/b;->a:Lcom/stagnationlab/sk/c/b;

    return-object p0

    .line 144
    :cond_10
    sget-object p0, Lcom/stagnationlab/sk/c/b;->u:Lcom/stagnationlab/sk/c/b;

    return-object p0

    :cond_11
    const-wide/16 v0, 0x3f9

    cmp-long p2, p0, v0

    if-nez p2, :cond_12

    .line 148
    sget-object p0, Lcom/stagnationlab/sk/c/b;->c:Lcom/stagnationlab/sk/c/b;

    return-object p0

    :cond_12
    const-wide/16 v0, -0x792f

    cmp-long p2, p0, v0

    if-nez p2, :cond_13

    .line 152
    sget-object p0, Lcom/stagnationlab/sk/c/b;->m:Lcom/stagnationlab/sk/c/b;

    return-object p0

    :cond_13
    const-wide/32 v0, 0xc352

    cmp-long p2, p0, v0

    if-nez p2, :cond_14

    .line 156
    sget-object p0, Lcom/stagnationlab/sk/c/b;->t:Lcom/stagnationlab/sk/c/b;

    return-object p0

    .line 159
    :cond_14
    sget-object p0, Lcom/stagnationlab/sk/c/b;->v:Lcom/stagnationlab/sk/c/b;

    return-object p0

    .line 107
    :cond_15
    :goto_0
    sget-object p0, Lcom/stagnationlab/sk/c/b;->g:Lcom/stagnationlab/sk/c/b;

    return-object p0

    .line 96
    :cond_16
    :goto_1
    sget-object p0, Lcom/stagnationlab/sk/c/b;->a:Lcom/stagnationlab/sk/c/b;

    return-object p0
.end method

.method private static a(Lcom/stagnationlab/sk/c/b;)Lcom/stagnationlab/sk/c/c;
    .locals 2

    .line 262
    :try_start_0
    invoke-virtual {p0}, Lcom/stagnationlab/sk/c/b;->toString()Ljava/lang/String;

    move-result-object p0

    const/4 v0, 0x0

    const-string v1, "_"

    .line 264
    invoke-virtual {p0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p0

    invoke-static {p0}, Lcom/stagnationlab/sk/c/c;->valueOf(Ljava/lang/String;)Lcom/stagnationlab/sk/c/c;

    move-result-object p0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object p0

    .line 266
    :catch_0
    sget-object p0, Lcom/stagnationlab/sk/c/c;->b:Lcom/stagnationlab/sk/c/c;

    return-object p0
.end method

.method private a(Lee/cyber/smartid/dto/jsonrpc/ServiceAccountKeyStatus;)Lcom/stagnationlab/sk/models/AccountKey;
    .locals 1

    if-nez p1, :cond_0

    const/4 p1, 0x0

    return-object p1

    .line 179
    :cond_0
    new-instance v0, Lcom/stagnationlab/sk/models/AccountKey;

    invoke-direct {v0, p1}, Lcom/stagnationlab/sk/models/AccountKey;-><init>(Lee/cyber/smartid/dto/jsonrpc/ServiceAccountKeyStatus;)V

    return-object v0
.end method

.method private static a(Ljava/lang/String;)Ljava/util/Map;
    .locals 3

    const/4 v0, 0x0

    if-nez p0, :cond_0

    return-object v0

    .line 168
    :cond_0
    :try_start_0
    new-instance v1, Lcom/google/gson/f;

    invoke-direct {v1}, Lcom/google/gson/f;-><init>()V

    const-class v2, Ljava/util/Map;

    invoke-virtual {v1, p0, v2}, Lcom/google/gson/f;->a(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/util/Map;
    :try_end_0
    .catch Lcom/google/gson/u; {:try_start_0 .. :try_end_0} :catch_0

    return-object p0

    :catch_0
    return-object v0
.end method

.method private static a(Lcom/stagnationlab/sk/c/b;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .line 291
    sget-object v0, Lcom/stagnationlab/sk/c/b;->v:Lcom/stagnationlab/sk/c/b;

    if-eq p0, v0, :cond_0

    sget-object v0, Lcom/stagnationlab/sk/c/b;->c:Lcom/stagnationlab/sk/c/b;

    if-eq p0, v0, :cond_0

    sget-object v0, Lcom/stagnationlab/sk/c/b;->b:Lcom/stagnationlab/sk/c/b;

    if-eq p0, v0, :cond_0

    sget-object v0, Lcom/stagnationlab/sk/c/b;->r:Lcom/stagnationlab/sk/c/b;

    if-ne p0, v0, :cond_1

    .line 296
    :cond_0
    new-instance p0, Ljava/lang/StringBuilder;

    invoke-direct {p0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "SmartIdError."

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-static {p0, p2}, Lcom/stagnationlab/sk/util/i;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    return-void
.end method

.method public static a(Lee/cyber/smartid/dto/SmartIdError;Ljava/lang/String;)V
    .locals 2

    .line 301
    invoke-virtual {p0}, Lee/cyber/smartid/dto/SmartIdError;->getCode()J

    move-result-wide v0

    invoke-static {v0, v1, p0}, Lcom/stagnationlab/sk/c/a;->a(JLee/cyber/smartid/dto/SmartIdError;)Lcom/stagnationlab/sk/c/b;

    move-result-object v0

    invoke-virtual {p0}, Lee/cyber/smartid/dto/SmartIdError;->getHumanReadableErrorCodeName()Ljava/lang/String;

    move-result-object p0

    invoke-static {v0, p0, p1}, Lcom/stagnationlab/sk/c/a;->a(Lcom/stagnationlab/sk/c/b;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private b(Ljava/lang/String;)V
    .locals 2

    .line 287
    iget-object v0, p0, Lcom/stagnationlab/sk/c/a;->c:Lcom/stagnationlab/sk/c/b;

    iget-object v1, p0, Lcom/stagnationlab/sk/c/a;->e:Ljava/lang/String;

    invoke-static {v0, v1, p1}, Lcom/stagnationlab/sk/c/a;->a(Lcom/stagnationlab/sk/c/b;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public a()Lcom/stagnationlab/sk/c/b;
    .locals 1

    .line 183
    iget-object v0, p0, Lcom/stagnationlab/sk/c/a;->c:Lcom/stagnationlab/sk/c/b;

    return-object v0
.end method

.method public a(Z)V
    .locals 0

    .line 257
    iput-boolean p1, p0, Lcom/stagnationlab/sk/c/a;->f:Z

    return-void
.end method

.method public b()Ljava/lang/String;
    .locals 2

    .line 187
    iget-object v0, p0, Lcom/stagnationlab/sk/c/a;->c:Lcom/stagnationlab/sk/c/b;

    sget-object v1, Lcom/stagnationlab/sk/c/b;->l:Lcom/stagnationlab/sk/c/b;

    if-ne v0, v1, :cond_0

    .line 188
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/stagnationlab/sk/c/a;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ". Attempts left: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/stagnationlab/sk/c/a;->c()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 191
    :cond_0
    iget-object v0, p0, Lcom/stagnationlab/sk/c/a;->b:Ljava/lang/String;

    return-object v0
.end method

.method public c()I
    .locals 1

    .line 199
    iget-object v0, p0, Lcom/stagnationlab/sk/c/a;->d:Lcom/stagnationlab/sk/models/AccountKey;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return v0

    .line 202
    :cond_0
    invoke-virtual {v0}, Lcom/stagnationlab/sk/models/AccountKey;->c()I

    move-result v0

    return v0
.end method

.method public d()Z
    .locals 1

    .line 206
    iget-object v0, p0, Lcom/stagnationlab/sk/c/a;->d:Lcom/stagnationlab/sk/models/AccountKey;

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/stagnationlab/sk/c/a;->f:Z

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/stagnationlab/sk/c/a;->g:Z

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public e()Lcom/stagnationlab/sk/util/f;
    .locals 3

    .line 210
    new-instance v0, Lcom/stagnationlab/sk/util/f;

    invoke-direct {v0}, Lcom/stagnationlab/sk/util/f;-><init>()V

    .line 212
    iget-object v1, p0, Lcom/stagnationlab/sk/c/a;->c:Lcom/stagnationlab/sk/c/b;

    sget-object v2, Lcom/stagnationlab/sk/c/b;->l:Lcom/stagnationlab/sk/c/b;

    if-ne v1, v2, :cond_0

    .line 213
    invoke-virtual {p0}, Lcom/stagnationlab/sk/c/a;->c()I

    move-result v1

    const-string v2, "pinAttemptsLeft"

    invoke-virtual {v0, v2, v1}, Lcom/stagnationlab/sk/util/f;->a(Ljava/lang/String;I)Lcom/stagnationlab/sk/util/f;

    .line 216
    :cond_0
    iget-object v1, p0, Lcom/stagnationlab/sk/c/a;->c:Lcom/stagnationlab/sk/c/b;

    sget-object v2, Lcom/stagnationlab/sk/c/b;->o:Lcom/stagnationlab/sk/c/b;

    if-ne v1, v2, :cond_1

    .line 217
    iget-object v1, p0, Lcom/stagnationlab/sk/c/a;->d:Lcom/stagnationlab/sk/models/AccountKey;

    const-string v2, "accountKey"

    invoke-virtual {v0, v2, v1}, Lcom/stagnationlab/sk/util/f;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/stagnationlab/sk/util/f;

    .line 220
    :cond_1
    iget-boolean v1, p0, Lcom/stagnationlab/sk/c/a;->g:Z

    const/4 v2, 0x1

    if-eqz v1, :cond_2

    const-string v1, "isUpgradeError"

    .line 221
    invoke-virtual {v0, v1, v2}, Lcom/stagnationlab/sk/util/f;->a(Ljava/lang/String;Z)Lcom/stagnationlab/sk/util/f;

    .line 224
    :cond_2
    iget-boolean v1, p0, Lcom/stagnationlab/sk/c/a;->f:Z

    if-eqz v1, :cond_3

    const-string v1, "canRetry"

    .line 225
    invoke-virtual {v0, v1, v2}, Lcom/stagnationlab/sk/util/f;->a(Ljava/lang/String;Z)Lcom/stagnationlab/sk/util/f;

    :cond_3
    return-object v0
.end method

.method public f()Lcom/stagnationlab/sk/models/AccountKey;
    .locals 1

    .line 232
    iget-object v0, p0, Lcom/stagnationlab/sk/c/a;->d:Lcom/stagnationlab/sk/models/AccountKey;

    return-object v0
.end method

.method public g()Z
    .locals 2

    .line 241
    iget-object v0, p0, Lcom/stagnationlab/sk/c/a;->c:Lcom/stagnationlab/sk/c/b;

    sget-object v1, Lcom/stagnationlab/sk/c/b;->o:Lcom/stagnationlab/sk/c/b;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public h()Z
    .locals 2

    .line 245
    iget-object v0, p0, Lcom/stagnationlab/sk/c/a;->c:Lcom/stagnationlab/sk/c/b;

    sget-object v1, Lcom/stagnationlab/sk/c/b;->f:Lcom/stagnationlab/sk/c/b;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public i()Z
    .locals 2

    .line 249
    iget-object v0, p0, Lcom/stagnationlab/sk/c/a;->c:Lcom/stagnationlab/sk/c/b;

    sget-object v1, Lcom/stagnationlab/sk/c/b;->e:Lcom/stagnationlab/sk/c/b;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public j()Lee/cyber/smartid/dto/SmartIdError;
    .locals 1

    .line 253
    iget-object v0, p0, Lcom/stagnationlab/sk/c/a;->h:Lee/cyber/smartid/dto/SmartIdError;

    return-object v0
.end method

.method public k()Lcom/stagnationlab/sk/c/c;
    .locals 1

    .line 271
    iget-object v0, p0, Lcom/stagnationlab/sk/c/a;->a:Lcom/stagnationlab/sk/c/c;

    return-object v0
.end method

.method public l()Ljava/lang/String;
    .locals 1

    .line 275
    iget-object v0, p0, Lcom/stagnationlab/sk/c/a;->e:Ljava/lang/String;

    return-object v0
.end method

.method public m()V
    .locals 1

    const/4 v0, 0x1

    .line 279
    iput-boolean v0, p0, Lcom/stagnationlab/sk/c/a;->g:Z

    return-void
.end method

.method public n()Z
    .locals 1

    .line 283
    iget-boolean v0, p0, Lcom/stagnationlab/sk/c/a;->g:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    const/4 v0, 0x2

    .line 195
    new-array v0, v0, [Ljava/lang/Object;

    iget-object v1, p0, Lcom/stagnationlab/sk/c/a;->c:Lcom/stagnationlab/sk/c/b;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    invoke-virtual {p0}, Lcom/stagnationlab/sk/c/a;->b()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    aput-object v1, v0, v2

    const-string v1, "AppError{%s, %s}"

    invoke-static {v1, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .line 332
    iget-object v0, p0, Lcom/stagnationlab/sk/c/a;->a:Lcom/stagnationlab/sk/c/c;

    invoke-virtual {v0}, Lcom/stagnationlab/sk/c/c;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 333
    iget-object v0, p0, Lcom/stagnationlab/sk/c/a;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 334
    iget-object v0, p0, Lcom/stagnationlab/sk/c/a;->c:Lcom/stagnationlab/sk/c/b;

    invoke-virtual {v0}, Lcom/stagnationlab/sk/c/b;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 335
    iget-object v0, p0, Lcom/stagnationlab/sk/c/a;->d:Lcom/stagnationlab/sk/models/AccountKey;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 336
    iget-object p2, p0, Lcom/stagnationlab/sk/c/a;->e:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 337
    iget-boolean p2, p0, Lcom/stagnationlab/sk/c/a;->f:Z

    invoke-static {p1, p2}, Lcom/stagnationlab/sk/i;->a(Landroid/os/Parcel;Z)V

    return-void
.end method
