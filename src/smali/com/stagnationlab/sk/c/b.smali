.class public final enum Lcom/stagnationlab/sk/c/b;
.super Ljava/lang/Enum;
.source "ErrorCode.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/stagnationlab/sk/c/b;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum A:Lcom/stagnationlab/sk/c/b;

.field public static final enum B:Lcom/stagnationlab/sk/c/b;

.field public static final enum C:Lcom/stagnationlab/sk/c/b;

.field public static final enum D:Lcom/stagnationlab/sk/c/b;

.field public static final enum E:Lcom/stagnationlab/sk/c/b;

.field public static final enum F:Lcom/stagnationlab/sk/c/b;

.field public static final enum G:Lcom/stagnationlab/sk/c/b;

.field public static final enum H:Lcom/stagnationlab/sk/c/b;

.field public static final enum I:Lcom/stagnationlab/sk/c/b;

.field public static final enum J:Lcom/stagnationlab/sk/c/b;

.field public static final enum K:Lcom/stagnationlab/sk/c/b;

.field public static final enum L:Lcom/stagnationlab/sk/c/b;

.field public static final enum M:Lcom/stagnationlab/sk/c/b;

.field public static final enum N:Lcom/stagnationlab/sk/c/b;

.field public static final enum O:Lcom/stagnationlab/sk/c/b;

.field public static final enum P:Lcom/stagnationlab/sk/c/b;

.field public static final enum Q:Lcom/stagnationlab/sk/c/b;

.field public static final enum R:Lcom/stagnationlab/sk/c/b;

.field public static final enum S:Lcom/stagnationlab/sk/c/b;

.field public static final enum T:Lcom/stagnationlab/sk/c/b;

.field public static final enum U:Lcom/stagnationlab/sk/c/b;

.field public static final enum V:Lcom/stagnationlab/sk/c/b;

.field public static final enum W:Lcom/stagnationlab/sk/c/b;

.field public static final enum X:Lcom/stagnationlab/sk/c/b;

.field public static final enum Y:Lcom/stagnationlab/sk/c/b;

.field public static final enum a:Lcom/stagnationlab/sk/c/b;

.field private static final synthetic aa:[Lcom/stagnationlab/sk/c/b;

.field public static final enum b:Lcom/stagnationlab/sk/c/b;

.field public static final enum c:Lcom/stagnationlab/sk/c/b;

.field public static final enum d:Lcom/stagnationlab/sk/c/b;

.field public static final enum e:Lcom/stagnationlab/sk/c/b;

.field public static final enum f:Lcom/stagnationlab/sk/c/b;

.field public static final enum g:Lcom/stagnationlab/sk/c/b;

.field public static final enum h:Lcom/stagnationlab/sk/c/b;

.field public static final enum i:Lcom/stagnationlab/sk/c/b;

.field public static final enum j:Lcom/stagnationlab/sk/c/b;

.field public static final enum k:Lcom/stagnationlab/sk/c/b;

.field public static final enum l:Lcom/stagnationlab/sk/c/b;

.field public static final enum m:Lcom/stagnationlab/sk/c/b;

.field public static final enum n:Lcom/stagnationlab/sk/c/b;

.field public static final enum o:Lcom/stagnationlab/sk/c/b;

.field public static final enum p:Lcom/stagnationlab/sk/c/b;

.field public static final enum q:Lcom/stagnationlab/sk/c/b;

.field public static final enum r:Lcom/stagnationlab/sk/c/b;

.field public static final enum s:Lcom/stagnationlab/sk/c/b;

.field public static final enum t:Lcom/stagnationlab/sk/c/b;

.field public static final enum u:Lcom/stagnationlab/sk/c/b;

.field public static final enum v:Lcom/stagnationlab/sk/c/b;

.field public static final enum w:Lcom/stagnationlab/sk/c/b;

.field public static final enum x:Lcom/stagnationlab/sk/c/b;

.field public static final enum y:Lcom/stagnationlab/sk/c/b;

.field public static final enum z:Lcom/stagnationlab/sk/c/b;


# instance fields
.field private Z:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 16

    .line 5
    new-instance v0, Lcom/stagnationlab/sk/c/b;

    const/4 v1, 0x0

    const-string v2, "LIB_NETWORK_FAILURE"

    invoke-direct {v0, v2, v1}, Lcom/stagnationlab/sk/c/b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/stagnationlab/sk/c/b;->a:Lcom/stagnationlab/sk/c/b;

    .line 6
    new-instance v0, Lcom/stagnationlab/sk/c/b;

    const/4 v2, 0x1

    const-string v3, "LIB_RESPONSE_TIMEOUT"

    invoke-direct {v0, v3, v2}, Lcom/stagnationlab/sk/c/b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/stagnationlab/sk/c/b;->b:Lcom/stagnationlab/sk/c/b;

    .line 7
    new-instance v0, Lcom/stagnationlab/sk/c/b;

    const/4 v3, 0x2

    const-string v4, "LIB_SSL_EXCEPTION"

    invoke-direct {v0, v4, v3}, Lcom/stagnationlab/sk/c/b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/stagnationlab/sk/c/b;->c:Lcom/stagnationlab/sk/c/b;

    .line 8
    new-instance v0, Lcom/stagnationlab/sk/c/b;

    const/4 v4, 0x3

    const-string v5, "LIB_SAFETY_NET_FAILED"

    invoke-direct {v0, v5, v4}, Lcom/stagnationlab/sk/c/b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/stagnationlab/sk/c/b;->d:Lcom/stagnationlab/sk/c/b;

    .line 9
    new-instance v0, Lcom/stagnationlab/sk/c/b;

    const/4 v5, 0x4

    const-string v6, "LIB_SERVER_ACCOUNT_LOCKED"

    invoke-direct {v0, v6, v5}, Lcom/stagnationlab/sk/c/b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/stagnationlab/sk/c/b;->e:Lcom/stagnationlab/sk/c/b;

    .line 10
    new-instance v0, Lcom/stagnationlab/sk/c/b;

    const/4 v6, 0x5

    const-string v7, "LIB_SERVER_ACCOUNT_NOT_FOUND"

    invoke-direct {v0, v7, v6}, Lcom/stagnationlab/sk/c/b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/stagnationlab/sk/c/b;->f:Lcom/stagnationlab/sk/c/b;

    .line 11
    new-instance v0, Lcom/stagnationlab/sk/c/b;

    const/4 v7, 0x6

    const-string v8, "LIB_SERVER_TRANSACTION_NOT_FOUND"

    invoke-direct {v0, v8, v7}, Lcom/stagnationlab/sk/c/b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/stagnationlab/sk/c/b;->g:Lcom/stagnationlab/sk/c/b;

    .line 12
    new-instance v0, Lcom/stagnationlab/sk/c/b;

    const/4 v8, 0x7

    const-string v9, "LIB_SERVER_TRANSACTION_ALREADY_COMPLETED"

    invoke-direct {v0, v9, v8}, Lcom/stagnationlab/sk/c/b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/stagnationlab/sk/c/b;->h:Lcom/stagnationlab/sk/c/b;

    .line 13
    new-instance v0, Lcom/stagnationlab/sk/c/b;

    const/16 v9, 0x8

    const-string v10, "LIB_SERVER_TRANSACTION_EXPIRED"

    invoke-direct {v0, v10, v9}, Lcom/stagnationlab/sk/c/b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/stagnationlab/sk/c/b;->i:Lcom/stagnationlab/sk/c/b;

    .line 14
    new-instance v0, Lcom/stagnationlab/sk/c/b;

    const/16 v10, 0x9

    const-string v11, "LIB_SERVER_RP_REQUEST_NOT_FOUND"

    invoke-direct {v0, v11, v10}, Lcom/stagnationlab/sk/c/b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/stagnationlab/sk/c/b;->j:Lcom/stagnationlab/sk/c/b;

    .line 15
    new-instance v0, Lcom/stagnationlab/sk/c/b;

    const/16 v11, 0xa

    const-string v12, "LIB_SERVER_REGISTRATION_TOKEN_INVALID"

    invoke-direct {v0, v12, v11}, Lcom/stagnationlab/sk/c/b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/stagnationlab/sk/c/b;->k:Lcom/stagnationlab/sk/c/b;

    .line 16
    new-instance v0, Lcom/stagnationlab/sk/c/b;

    const/16 v12, 0xb

    const-string v13, "LIB_SERVER_WRONG_PIN"

    invoke-direct {v0, v13, v12}, Lcom/stagnationlab/sk/c/b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/stagnationlab/sk/c/b;->l:Lcom/stagnationlab/sk/c/b;

    .line 17
    new-instance v0, Lcom/stagnationlab/sk/c/b;

    const/16 v13, 0xc

    const-string v14, "LIB_SERVER_CLONE_DETECTED"

    invoke-direct {v0, v14, v13}, Lcom/stagnationlab/sk/c/b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/stagnationlab/sk/c/b;->m:Lcom/stagnationlab/sk/c/b;

    .line 18
    new-instance v0, Lcom/stagnationlab/sk/c/b;

    const/16 v14, 0xd

    const-string v15, "LIB_LOCAL_PENDING_STATE_NOT_FOUND"

    invoke-direct {v0, v15, v14}, Lcom/stagnationlab/sk/c/b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/stagnationlab/sk/c/b;->n:Lcom/stagnationlab/sk/c/b;

    .line 19
    new-instance v0, Lcom/stagnationlab/sk/c/b;

    const/16 v15, 0xe

    const-string v14, "LIB_SERVER_KEY_UNUSABLE"

    invoke-direct {v0, v14, v15}, Lcom/stagnationlab/sk/c/b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/stagnationlab/sk/c/b;->o:Lcom/stagnationlab/sk/c/b;

    .line 20
    new-instance v0, Lcom/stagnationlab/sk/c/b;

    const-string v14, "LIB_SYSTEM_UNDER_MAINTENANCE"

    const/16 v15, 0xf

    invoke-direct {v0, v14, v15}, Lcom/stagnationlab/sk/c/b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/stagnationlab/sk/c/b;->p:Lcom/stagnationlab/sk/c/b;

    .line 21
    new-instance v0, Lcom/stagnationlab/sk/c/b;

    const-string v14, "LIB_CLIENT_TOO_OLD"

    const/16 v15, 0x10

    invoke-direct {v0, v14, v15}, Lcom/stagnationlab/sk/c/b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/stagnationlab/sk/c/b;->q:Lcom/stagnationlab/sk/c/b;

    .line 22
    new-instance v0, Lcom/stagnationlab/sk/c/b;

    const-string v14, "LIB_INTERNAL_ERROR"

    const/16 v15, 0x11

    invoke-direct {v0, v14, v15}, Lcom/stagnationlab/sk/c/b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/stagnationlab/sk/c/b;->r:Lcom/stagnationlab/sk/c/b;

    .line 23
    new-instance v0, Lcom/stagnationlab/sk/c/b;

    const-string v14, "LIB_INVALID_PINS"

    const/16 v15, 0x12

    invoke-direct {v0, v14, v15}, Lcom/stagnationlab/sk/c/b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/stagnationlab/sk/c/b;->s:Lcom/stagnationlab/sk/c/b;

    .line 24
    new-instance v0, Lcom/stagnationlab/sk/c/b;

    const-string v14, "LIB_WRONG_VERIFICATION_CODE"

    const/16 v15, 0x13

    invoke-direct {v0, v14, v15}, Lcom/stagnationlab/sk/c/b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/stagnationlab/sk/c/b;->t:Lcom/stagnationlab/sk/c/b;

    .line 25
    new-instance v0, Lcom/stagnationlab/sk/c/b;

    const-string v14, "LIB_INVALID_TRANSACTION_STATE"

    const/16 v15, 0x14

    invoke-direct {v0, v14, v15}, Lcom/stagnationlab/sk/c/b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/stagnationlab/sk/c/b;->u:Lcom/stagnationlab/sk/c/b;

    .line 26
    new-instance v0, Lcom/stagnationlab/sk/c/b;

    const-string v14, "LIB_DEFAULT"

    const/16 v15, 0x15

    invoke-direct {v0, v14, v15}, Lcom/stagnationlab/sk/c/b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/stagnationlab/sk/c/b;->v:Lcom/stagnationlab/sk/c/b;

    .line 28
    new-instance v0, Lcom/stagnationlab/sk/c/b;

    const-string v14, "PORTAL_NETWORK_FAILURE"

    const/16 v15, 0x16

    const-string v13, "NETWORK_FAILURE"

    invoke-direct {v0, v14, v15, v13}, Lcom/stagnationlab/sk/c/b;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/stagnationlab/sk/c/b;->w:Lcom/stagnationlab/sk/c/b;

    .line 29
    new-instance v0, Lcom/stagnationlab/sk/c/b;

    const-string v13, "PORTAL_RESPONSE_TIMEOUT"

    const/16 v14, 0x17

    const-string v15, "RESPONSE_TIMEOUT"

    invoke-direct {v0, v13, v14, v15}, Lcom/stagnationlab/sk/c/b;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/stagnationlab/sk/c/b;->x:Lcom/stagnationlab/sk/c/b;

    .line 30
    new-instance v0, Lcom/stagnationlab/sk/c/b;

    const-string v13, "PORTAL_INVALID_APP_VERSION"

    const/16 v14, 0x18

    const-string v15, "INVALID_APP_VERSION"

    invoke-direct {v0, v13, v14, v15}, Lcom/stagnationlab/sk/c/b;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/stagnationlab/sk/c/b;->y:Lcom/stagnationlab/sk/c/b;

    .line 31
    new-instance v0, Lcom/stagnationlab/sk/c/b;

    const-string v13, "PORTAL_UNEXPECTED_STATUS_CODE"

    const/16 v14, 0x19

    invoke-direct {v0, v13, v14}, Lcom/stagnationlab/sk/c/b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/stagnationlab/sk/c/b;->z:Lcom/stagnationlab/sk/c/b;

    .line 32
    new-instance v0, Lcom/stagnationlab/sk/c/b;

    const-string v13, "PORTAL_DEFAULT"

    const/16 v14, 0x1a

    const-string v15, "DEFAULT"

    invoke-direct {v0, v13, v14, v15}, Lcom/stagnationlab/sk/c/b;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/stagnationlab/sk/c/b;->A:Lcom/stagnationlab/sk/c/b;

    .line 34
    new-instance v0, Lcom/stagnationlab/sk/c/b;

    const-string v13, "APP_OTP_RESPONSE_INVALID"

    const/16 v14, 0x1b

    invoke-direct {v0, v13, v14}, Lcom/stagnationlab/sk/c/b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/stagnationlab/sk/c/b;->B:Lcom/stagnationlab/sk/c/b;

    .line 35
    new-instance v0, Lcom/stagnationlab/sk/c/b;

    const-string v13, "APP_OTP_START_SMS_RETRIEVER_FAILED"

    const/16 v14, 0x1c

    invoke-direct {v0, v13, v14}, Lcom/stagnationlab/sk/c/b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/stagnationlab/sk/c/b;->C:Lcom/stagnationlab/sk/c/b;

    .line 36
    new-instance v0, Lcom/stagnationlab/sk/c/b;

    const-string v13, "APP_OTP_TIMEOUT"

    const/16 v14, 0x1d

    invoke-direct {v0, v13, v14}, Lcom/stagnationlab/sk/c/b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/stagnationlab/sk/c/b;->D:Lcom/stagnationlab/sk/c/b;

    .line 37
    new-instance v0, Lcom/stagnationlab/sk/c/b;

    const-string v13, "APP_OTP_UNKNOWN_STATUS_CODE"

    const/16 v14, 0x1e

    invoke-direct {v0, v13, v14}, Lcom/stagnationlab/sk/c/b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/stagnationlab/sk/c/b;->E:Lcom/stagnationlab/sk/c/b;

    .line 39
    new-instance v0, Lcom/stagnationlab/sk/c/b;

    const-string v13, "APP_GOOGLE_PLAY_SERVICES_NOT_AVAILABLE"

    const/16 v14, 0x1f

    invoke-direct {v0, v13, v14}, Lcom/stagnationlab/sk/c/b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/stagnationlab/sk/c/b;->F:Lcom/stagnationlab/sk/c/b;

    .line 40
    new-instance v0, Lcom/stagnationlab/sk/c/b;

    const-string v13, "APP_FAILED_TO_GET_GCM_TOKEN"

    const/16 v14, 0x20

    invoke-direct {v0, v13, v14}, Lcom/stagnationlab/sk/c/b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/stagnationlab/sk/c/b;->G:Lcom/stagnationlab/sk/c/b;

    .line 41
    new-instance v0, Lcom/stagnationlab/sk/c/b;

    const-string v13, "APP_ACCOUNT_NOT_FOUND"

    const/16 v14, 0x21

    invoke-direct {v0, v13, v14}, Lcom/stagnationlab/sk/c/b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/stagnationlab/sk/c/b;->H:Lcom/stagnationlab/sk/c/b;

    .line 42
    new-instance v0, Lcom/stagnationlab/sk/c/b;

    const-string v13, "APP_LIB_NOT_READY"

    const/16 v14, 0x22

    invoke-direct {v0, v13, v14}, Lcom/stagnationlab/sk/c/b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/stagnationlab/sk/c/b;->I:Lcom/stagnationlab/sk/c/b;

    .line 43
    new-instance v0, Lcom/stagnationlab/sk/c/b;

    const-string v13, "APP_REGISTRATION_CANCELLED"

    const/16 v14, 0x23

    invoke-direct {v0, v13, v14}, Lcom/stagnationlab/sk/c/b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/stagnationlab/sk/c/b;->J:Lcom/stagnationlab/sk/c/b;

    .line 44
    new-instance v0, Lcom/stagnationlab/sk/c/b;

    const-string v13, "APP_REGISTRATION_INVALID_STATE"

    const/16 v14, 0x24

    invoke-direct {v0, v13, v14}, Lcom/stagnationlab/sk/c/b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/stagnationlab/sk/c/b;->K:Lcom/stagnationlab/sk/c/b;

    .line 45
    new-instance v0, Lcom/stagnationlab/sk/c/b;

    const-string v13, "APP_REGISTRATION_REQUEST_IN_PROGRESS"

    const/16 v14, 0x25

    invoke-direct {v0, v13, v14}, Lcom/stagnationlab/sk/c/b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/stagnationlab/sk/c/b;->L:Lcom/stagnationlab/sk/c/b;

    .line 46
    new-instance v0, Lcom/stagnationlab/sk/c/b;

    const-string v13, "APP_REGISTRATION_NOT_INITIALIZED"

    const/16 v14, 0x26

    invoke-direct {v0, v13, v14}, Lcom/stagnationlab/sk/c/b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/stagnationlab/sk/c/b;->M:Lcom/stagnationlab/sk/c/b;

    .line 47
    new-instance v0, Lcom/stagnationlab/sk/c/b;

    const-string v13, "APP_UNKNOWN_PENDING_OPERATION_TYPE"

    const/16 v14, 0x27

    invoke-direct {v0, v13, v14}, Lcom/stagnationlab/sk/c/b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/stagnationlab/sk/c/b;->N:Lcom/stagnationlab/sk/c/b;

    .line 48
    new-instance v0, Lcom/stagnationlab/sk/c/b;

    const-string v13, "APP_UNKNOWN_LOCAL_PENDING_STATE_TYPE"

    const/16 v14, 0x28

    invoke-direct {v0, v13, v14}, Lcom/stagnationlab/sk/c/b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/stagnationlab/sk/c/b;->O:Lcom/stagnationlab/sk/c/b;

    .line 49
    new-instance v0, Lcom/stagnationlab/sk/c/b;

    const-string v13, "APP_CREATE_ACCOUNT_FAILED"

    const/16 v14, 0x29

    invoke-direct {v0, v13, v14}, Lcom/stagnationlab/sk/c/b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/stagnationlab/sk/c/b;->P:Lcom/stagnationlab/sk/c/b;

    .line 50
    new-instance v0, Lcom/stagnationlab/sk/c/b;

    const-string v13, "APP_NETWORK_FAILURE"

    const/16 v14, 0x2a

    const-string v15, "ERROR_CODE_NETWORK_FAILURE"

    invoke-direct {v0, v13, v14, v15}, Lcom/stagnationlab/sk/c/b;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/stagnationlab/sk/c/b;->Q:Lcom/stagnationlab/sk/c/b;

    .line 51
    new-instance v0, Lcom/stagnationlab/sk/c/b;

    const-string v13, "APP_INVALID_SSL_PINS"

    const/16 v14, 0x2b

    invoke-direct {v0, v13, v14}, Lcom/stagnationlab/sk/c/b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/stagnationlab/sk/c/b;->R:Lcom/stagnationlab/sk/c/b;

    .line 52
    new-instance v0, Lcom/stagnationlab/sk/c/b;

    const-string v13, "APP_PUSH_ACCOUNT_NOT_FOUND"

    const/16 v14, 0x2c

    invoke-direct {v0, v13, v14}, Lcom/stagnationlab/sk/c/b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/stagnationlab/sk/c/b;->S:Lcom/stagnationlab/sk/c/b;

    .line 53
    new-instance v0, Lcom/stagnationlab/sk/c/b;

    const-string v13, "APP_INVALID_LIB_RESPONSE"

    const/16 v14, 0x2d

    invoke-direct {v0, v13, v14}, Lcom/stagnationlab/sk/c/b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/stagnationlab/sk/c/b;->T:Lcom/stagnationlab/sk/c/b;

    .line 54
    new-instance v0, Lcom/stagnationlab/sk/c/b;

    const-string v13, "APP_REQUEST_HINT_FAILED"

    const/16 v14, 0x2e

    invoke-direct {v0, v13, v14}, Lcom/stagnationlab/sk/c/b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/stagnationlab/sk/c/b;->U:Lcom/stagnationlab/sk/c/b;

    .line 55
    new-instance v0, Lcom/stagnationlab/sk/c/b;

    const-string v13, "APP_REQUEST_HINT_CANCELLED"

    const/16 v14, 0x2f

    invoke-direct {v0, v13, v14}, Lcom/stagnationlab/sk/c/b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/stagnationlab/sk/c/b;->V:Lcom/stagnationlab/sk/c/b;

    .line 56
    new-instance v0, Lcom/stagnationlab/sk/c/b;

    const-string v13, "APP_REQUEST_HINT_NO_HINTS_AVAILABLE"

    const/16 v14, 0x30

    invoke-direct {v0, v13, v14}, Lcom/stagnationlab/sk/c/b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/stagnationlab/sk/c/b;->W:Lcom/stagnationlab/sk/c/b;

    .line 57
    new-instance v0, Lcom/stagnationlab/sk/c/b;

    const-string v13, "APP_URL_NOT_WHITELISTED"

    const/16 v14, 0x31

    invoke-direct {v0, v13, v14}, Lcom/stagnationlab/sk/c/b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/stagnationlab/sk/c/b;->X:Lcom/stagnationlab/sk/c/b;

    .line 58
    new-instance v0, Lcom/stagnationlab/sk/c/b;

    const-string v13, "APP_DEFAULT"

    const/16 v14, 0x32

    const-string v15, "DEFAULT"

    invoke-direct {v0, v13, v14, v15}, Lcom/stagnationlab/sk/c/b;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/stagnationlab/sk/c/b;->Y:Lcom/stagnationlab/sk/c/b;

    const/16 v0, 0x33

    .line 3
    new-array v0, v0, [Lcom/stagnationlab/sk/c/b;

    sget-object v13, Lcom/stagnationlab/sk/c/b;->a:Lcom/stagnationlab/sk/c/b;

    aput-object v13, v0, v1

    sget-object v1, Lcom/stagnationlab/sk/c/b;->b:Lcom/stagnationlab/sk/c/b;

    aput-object v1, v0, v2

    sget-object v1, Lcom/stagnationlab/sk/c/b;->c:Lcom/stagnationlab/sk/c/b;

    aput-object v1, v0, v3

    sget-object v1, Lcom/stagnationlab/sk/c/b;->d:Lcom/stagnationlab/sk/c/b;

    aput-object v1, v0, v4

    sget-object v1, Lcom/stagnationlab/sk/c/b;->e:Lcom/stagnationlab/sk/c/b;

    aput-object v1, v0, v5

    sget-object v1, Lcom/stagnationlab/sk/c/b;->f:Lcom/stagnationlab/sk/c/b;

    aput-object v1, v0, v6

    sget-object v1, Lcom/stagnationlab/sk/c/b;->g:Lcom/stagnationlab/sk/c/b;

    aput-object v1, v0, v7

    sget-object v1, Lcom/stagnationlab/sk/c/b;->h:Lcom/stagnationlab/sk/c/b;

    aput-object v1, v0, v8

    sget-object v1, Lcom/stagnationlab/sk/c/b;->i:Lcom/stagnationlab/sk/c/b;

    aput-object v1, v0, v9

    sget-object v1, Lcom/stagnationlab/sk/c/b;->j:Lcom/stagnationlab/sk/c/b;

    aput-object v1, v0, v10

    sget-object v1, Lcom/stagnationlab/sk/c/b;->k:Lcom/stagnationlab/sk/c/b;

    aput-object v1, v0, v11

    sget-object v1, Lcom/stagnationlab/sk/c/b;->l:Lcom/stagnationlab/sk/c/b;

    aput-object v1, v0, v12

    sget-object v1, Lcom/stagnationlab/sk/c/b;->m:Lcom/stagnationlab/sk/c/b;

    const/16 v2, 0xc

    aput-object v1, v0, v2

    sget-object v1, Lcom/stagnationlab/sk/c/b;->n:Lcom/stagnationlab/sk/c/b;

    const/16 v2, 0xd

    aput-object v1, v0, v2

    sget-object v1, Lcom/stagnationlab/sk/c/b;->o:Lcom/stagnationlab/sk/c/b;

    const/16 v2, 0xe

    aput-object v1, v0, v2

    sget-object v1, Lcom/stagnationlab/sk/c/b;->p:Lcom/stagnationlab/sk/c/b;

    const/16 v2, 0xf

    aput-object v1, v0, v2

    sget-object v1, Lcom/stagnationlab/sk/c/b;->q:Lcom/stagnationlab/sk/c/b;

    const/16 v2, 0x10

    aput-object v1, v0, v2

    sget-object v1, Lcom/stagnationlab/sk/c/b;->r:Lcom/stagnationlab/sk/c/b;

    const/16 v2, 0x11

    aput-object v1, v0, v2

    sget-object v1, Lcom/stagnationlab/sk/c/b;->s:Lcom/stagnationlab/sk/c/b;

    const/16 v2, 0x12

    aput-object v1, v0, v2

    sget-object v1, Lcom/stagnationlab/sk/c/b;->t:Lcom/stagnationlab/sk/c/b;

    const/16 v2, 0x13

    aput-object v1, v0, v2

    sget-object v1, Lcom/stagnationlab/sk/c/b;->u:Lcom/stagnationlab/sk/c/b;

    const/16 v2, 0x14

    aput-object v1, v0, v2

    sget-object v1, Lcom/stagnationlab/sk/c/b;->v:Lcom/stagnationlab/sk/c/b;

    const/16 v2, 0x15

    aput-object v1, v0, v2

    sget-object v1, Lcom/stagnationlab/sk/c/b;->w:Lcom/stagnationlab/sk/c/b;

    const/16 v2, 0x16

    aput-object v1, v0, v2

    sget-object v1, Lcom/stagnationlab/sk/c/b;->x:Lcom/stagnationlab/sk/c/b;

    const/16 v2, 0x17

    aput-object v1, v0, v2

    sget-object v1, Lcom/stagnationlab/sk/c/b;->y:Lcom/stagnationlab/sk/c/b;

    const/16 v2, 0x18

    aput-object v1, v0, v2

    sget-object v1, Lcom/stagnationlab/sk/c/b;->z:Lcom/stagnationlab/sk/c/b;

    const/16 v2, 0x19

    aput-object v1, v0, v2

    sget-object v1, Lcom/stagnationlab/sk/c/b;->A:Lcom/stagnationlab/sk/c/b;

    const/16 v2, 0x1a

    aput-object v1, v0, v2

    sget-object v1, Lcom/stagnationlab/sk/c/b;->B:Lcom/stagnationlab/sk/c/b;

    const/16 v2, 0x1b

    aput-object v1, v0, v2

    sget-object v1, Lcom/stagnationlab/sk/c/b;->C:Lcom/stagnationlab/sk/c/b;

    const/16 v2, 0x1c

    aput-object v1, v0, v2

    sget-object v1, Lcom/stagnationlab/sk/c/b;->D:Lcom/stagnationlab/sk/c/b;

    const/16 v2, 0x1d

    aput-object v1, v0, v2

    sget-object v1, Lcom/stagnationlab/sk/c/b;->E:Lcom/stagnationlab/sk/c/b;

    const/16 v2, 0x1e

    aput-object v1, v0, v2

    sget-object v1, Lcom/stagnationlab/sk/c/b;->F:Lcom/stagnationlab/sk/c/b;

    const/16 v2, 0x1f

    aput-object v1, v0, v2

    sget-object v1, Lcom/stagnationlab/sk/c/b;->G:Lcom/stagnationlab/sk/c/b;

    const/16 v2, 0x20

    aput-object v1, v0, v2

    sget-object v1, Lcom/stagnationlab/sk/c/b;->H:Lcom/stagnationlab/sk/c/b;

    const/16 v2, 0x21

    aput-object v1, v0, v2

    sget-object v1, Lcom/stagnationlab/sk/c/b;->I:Lcom/stagnationlab/sk/c/b;

    const/16 v2, 0x22

    aput-object v1, v0, v2

    sget-object v1, Lcom/stagnationlab/sk/c/b;->J:Lcom/stagnationlab/sk/c/b;

    const/16 v2, 0x23

    aput-object v1, v0, v2

    sget-object v1, Lcom/stagnationlab/sk/c/b;->K:Lcom/stagnationlab/sk/c/b;

    const/16 v2, 0x24

    aput-object v1, v0, v2

    sget-object v1, Lcom/stagnationlab/sk/c/b;->L:Lcom/stagnationlab/sk/c/b;

    const/16 v2, 0x25

    aput-object v1, v0, v2

    sget-object v1, Lcom/stagnationlab/sk/c/b;->M:Lcom/stagnationlab/sk/c/b;

    const/16 v2, 0x26

    aput-object v1, v0, v2

    sget-object v1, Lcom/stagnationlab/sk/c/b;->N:Lcom/stagnationlab/sk/c/b;

    const/16 v2, 0x27

    aput-object v1, v0, v2

    sget-object v1, Lcom/stagnationlab/sk/c/b;->O:Lcom/stagnationlab/sk/c/b;

    const/16 v2, 0x28

    aput-object v1, v0, v2

    sget-object v1, Lcom/stagnationlab/sk/c/b;->P:Lcom/stagnationlab/sk/c/b;

    const/16 v2, 0x29

    aput-object v1, v0, v2

    sget-object v1, Lcom/stagnationlab/sk/c/b;->Q:Lcom/stagnationlab/sk/c/b;

    const/16 v2, 0x2a

    aput-object v1, v0, v2

    sget-object v1, Lcom/stagnationlab/sk/c/b;->R:Lcom/stagnationlab/sk/c/b;

    const/16 v2, 0x2b

    aput-object v1, v0, v2

    sget-object v1, Lcom/stagnationlab/sk/c/b;->S:Lcom/stagnationlab/sk/c/b;

    const/16 v2, 0x2c

    aput-object v1, v0, v2

    sget-object v1, Lcom/stagnationlab/sk/c/b;->T:Lcom/stagnationlab/sk/c/b;

    const/16 v2, 0x2d

    aput-object v1, v0, v2

    sget-object v1, Lcom/stagnationlab/sk/c/b;->U:Lcom/stagnationlab/sk/c/b;

    const/16 v2, 0x2e

    aput-object v1, v0, v2

    sget-object v1, Lcom/stagnationlab/sk/c/b;->V:Lcom/stagnationlab/sk/c/b;

    const/16 v2, 0x2f

    aput-object v1, v0, v2

    sget-object v1, Lcom/stagnationlab/sk/c/b;->W:Lcom/stagnationlab/sk/c/b;

    const/16 v2, 0x30

    aput-object v1, v0, v2

    sget-object v1, Lcom/stagnationlab/sk/c/b;->X:Lcom/stagnationlab/sk/c/b;

    const/16 v2, 0x31

    aput-object v1, v0, v2

    sget-object v1, Lcom/stagnationlab/sk/c/b;->Y:Lcom/stagnationlab/sk/c/b;

    const/16 v2, 0x32

    aput-object v1, v0, v2

    sput-object v0, Lcom/stagnationlab/sk/c/b;->aa:[Lcom/stagnationlab/sk/c/b;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 66
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 67
    invoke-virtual {p0}, Lcom/stagnationlab/sk/c/b;->toString()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/stagnationlab/sk/c/b;->Z:Ljava/lang/String;

    .line 69
    iget-object p1, p0, Lcom/stagnationlab/sk/c/b;->Z:Ljava/lang/String;

    const-string p2, "LIB_"

    invoke-virtual {p1, p2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 70
    iget-object p1, p0, Lcom/stagnationlab/sk/c/b;->Z:Ljava/lang/String;

    const-string v0, "ERROR_CODE_"

    invoke-virtual {p1, p2, v0}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/stagnationlab/sk/c/b;->Z:Ljava/lang/String;

    :cond_0
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 62
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 63
    iput-object p3, p0, Lcom/stagnationlab/sk/c/b;->Z:Ljava/lang/String;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/stagnationlab/sk/c/b;
    .locals 1

    .line 3
    const-class v0, Lcom/stagnationlab/sk/c/b;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/stagnationlab/sk/c/b;

    return-object p0
.end method

.method public static values()[Lcom/stagnationlab/sk/c/b;
    .locals 1

    .line 3
    sget-object v0, Lcom/stagnationlab/sk/c/b;->aa:[Lcom/stagnationlab/sk/c/b;

    invoke-virtual {v0}, [Lcom/stagnationlab/sk/c/b;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/stagnationlab/sk/c/b;

    return-object v0
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .line 75
    iget-object v0, p0, Lcom/stagnationlab/sk/c/b;->Z:Ljava/lang/String;

    return-object v0
.end method
