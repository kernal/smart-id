.class public Lcom/stagnationlab/sk/ErrorActivity;
.super Lcom/stagnationlab/sk/c;
.source "ErrorActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/stagnationlab/sk/ErrorActivity$a;
    }
.end annotation


# instance fields
.field private c:Lcom/stagnationlab/sk/models/Transaction;

.field private d:Lcom/stagnationlab/sk/models/RpRequest;

.field private e:Lcom/stagnationlab/sk/c/a;

.field private f:Landroid/os/CountDownTimer;

.field private g:Lcom/stagnationlab/sk/models/ErrorData;

.field private h:Lcom/stagnationlab/sk/models/TransactionMetaData;

.field private i:Landroid/widget/TextView;

.field private j:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 31
    invoke-direct {p0}, Lcom/stagnationlab/sk/c;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/stagnationlab/sk/ErrorActivity;)Lcom/stagnationlab/sk/c/a;
    .locals 0

    .line 31
    iget-object p0, p0, Lcom/stagnationlab/sk/ErrorActivity;->e:Lcom/stagnationlab/sk/c/a;

    return-object p0
.end method

.method private a(Lcom/stagnationlab/sk/c/a;)Lcom/stagnationlab/sk/models/ErrorData;
    .locals 6

    .line 199
    sget-object v0, Lcom/stagnationlab/sk/ErrorActivity$7;->a:[I

    invoke-virtual {p1}, Lcom/stagnationlab/sk/c/a;->a()Lcom/stagnationlab/sk/c/b;

    move-result-object v1

    invoke-virtual {v1}, Lcom/stagnationlab/sk/c/b;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const v1, 0x7f0e00ae

    const v2, 0x7f07007f

    const v3, 0x7f0e00b7

    const v4, 0x7f070077

    const/4 v5, 0x1

    packed-switch v0, :pswitch_data_0

    .line 309
    new-instance p1, Lcom/stagnationlab/sk/models/ErrorData;

    .line 310
    invoke-virtual {p0, v3}, Lcom/stagnationlab/sk/ErrorActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f0e00b6

    .line 311
    invoke-virtual {p0, v1}, Lcom/stagnationlab/sk/ErrorActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p1, v0, v1}, Lcom/stagnationlab/sk/models/ErrorData;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 312
    invoke-virtual {p1, v5}, Lcom/stagnationlab/sk/models/ErrorData;->a(Z)Lcom/stagnationlab/sk/models/ErrorData;

    move-result-object p1

    return-object p1

    .line 303
    :pswitch_0
    new-instance p1, Lcom/stagnationlab/sk/models/ErrorData;

    .line 304
    invoke-virtual {p0, v3}, Lcom/stagnationlab/sk/ErrorActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f0e00a7

    .line 305
    invoke-virtual {p0, v1}, Lcom/stagnationlab/sk/ErrorActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p1, v0, v1}, Lcom/stagnationlab/sk/models/ErrorData;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 306
    invoke-virtual {p1, v5}, Lcom/stagnationlab/sk/models/ErrorData;->a(Z)Lcom/stagnationlab/sk/models/ErrorData;

    move-result-object p1

    return-object p1

    .line 297
    :pswitch_1
    new-instance p1, Lcom/stagnationlab/sk/models/ErrorData;

    .line 298
    invoke-virtual {p0, v3}, Lcom/stagnationlab/sk/ErrorActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f0e0111

    .line 299
    invoke-virtual {p0, v1}, Lcom/stagnationlab/sk/ErrorActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p1, v0, v1}, Lcom/stagnationlab/sk/models/ErrorData;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object p1

    .line 290
    :pswitch_2
    new-instance p1, Lcom/stagnationlab/sk/models/ErrorData;

    const v0, 0x7f0e00a6

    .line 291
    invoke-virtual {p0, v0}, Lcom/stagnationlab/sk/ErrorActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f0e00a5

    .line 292
    invoke-virtual {p0, v1}, Lcom/stagnationlab/sk/ErrorActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p1, v0, v1, v4}, Lcom/stagnationlab/sk/models/ErrorData;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    return-object p1

    .line 283
    :pswitch_3
    new-instance p1, Lcom/stagnationlab/sk/models/ErrorData;

    const v0, 0x7f0e00a1

    .line 284
    invoke-virtual {p0, v0}, Lcom/stagnationlab/sk/ErrorActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f0e00a0

    .line 285
    invoke-virtual {p0, v1}, Lcom/stagnationlab/sk/ErrorActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p1, v0, v1, v4}, Lcom/stagnationlab/sk/models/ErrorData;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    .line 287
    invoke-virtual {p1, v5}, Lcom/stagnationlab/sk/models/ErrorData;->c(Z)Lcom/stagnationlab/sk/models/ErrorData;

    move-result-object p1

    return-object p1

    .line 276
    :pswitch_4
    new-instance p1, Lcom/stagnationlab/sk/models/ErrorData;

    const v0, 0x7f0e00b5

    .line 277
    invoke-virtual {p0, v0}, Lcom/stagnationlab/sk/ErrorActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f0e00b4

    .line 278
    invoke-virtual {p0, v1}, Lcom/stagnationlab/sk/ErrorActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p1, v0, v1, v4}, Lcom/stagnationlab/sk/models/ErrorData;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    return-object p1

    .line 268
    :pswitch_5
    new-instance p1, Lcom/stagnationlab/sk/models/ErrorData;

    const v0, 0x7f0e00a4

    .line 269
    invoke-virtual {p0, v0}, Lcom/stagnationlab/sk/ErrorActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f0e00a3

    .line 270
    invoke-virtual {p0, v1}, Lcom/stagnationlab/sk/ErrorActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p1, v0, v1, v4}, Lcom/stagnationlab/sk/models/ErrorData;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    .line 272
    invoke-virtual {p1, v5}, Lcom/stagnationlab/sk/models/ErrorData;->b(Z)Lcom/stagnationlab/sk/models/ErrorData;

    move-result-object p1

    return-object p1

    .line 261
    :pswitch_6
    new-instance p1, Lcom/stagnationlab/sk/models/ErrorData;

    const v0, 0x7f0e00a9

    .line 262
    invoke-virtual {p0, v0}, Lcom/stagnationlab/sk/ErrorActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f0e00a8

    .line 263
    invoke-virtual {p0, v1}, Lcom/stagnationlab/sk/ErrorActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p1, v0, v1, v5}, Lcom/stagnationlab/sk/models/ErrorData;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    return-object p1

    .line 255
    :pswitch_7
    new-instance p1, Lcom/stagnationlab/sk/models/ErrorData;

    const v0, 0x7f0e00af

    .line 256
    invoke-virtual {p0, v0}, Lcom/stagnationlab/sk/ErrorActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 257
    invoke-virtual {p0, v1}, Lcom/stagnationlab/sk/ErrorActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p1, v0, v1}, Lcom/stagnationlab/sk/models/ErrorData;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 258
    invoke-virtual {p1, v5}, Lcom/stagnationlab/sk/models/ErrorData;->d(Z)Lcom/stagnationlab/sk/models/ErrorData;

    move-result-object p1

    return-object p1

    .line 243
    :pswitch_8
    invoke-virtual {p1}, Lcom/stagnationlab/sk/c/a;->f()Lcom/stagnationlab/sk/models/AccountKey;

    move-result-object v0

    .line 245
    new-instance v1, Lcom/stagnationlab/sk/models/ErrorData;

    .line 247
    invoke-virtual {p1}, Lcom/stagnationlab/sk/c/a;->f()Lcom/stagnationlab/sk/models/AccountKey;

    move-result-object p1

    invoke-virtual {p1}, Lcom/stagnationlab/sk/models/AccountKey;->d()Z

    move-result p1

    if-eqz p1, :cond_0

    const p1, 0x7f0e00fb

    goto :goto_0

    :cond_0
    const p1, 0x7f0e00fd

    .line 246
    :goto_0
    invoke-virtual {p0, p1}, Lcom/stagnationlab/sk/ErrorActivity;->getString(I)Ljava/lang/String;

    move-result-object p1

    .line 249
    invoke-virtual {v0}, Lcom/stagnationlab/sk/models/AccountKey;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    const v0, 0x7f0e00fc

    goto :goto_1

    :cond_1
    const v0, 0x7f0e00fe

    :goto_1
    invoke-virtual {p0, v0}, Lcom/stagnationlab/sk/ErrorActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    const v2, 0x7f07007e

    invoke-direct {v1, p1, v0, v2}, Lcom/stagnationlab/sk/models/ErrorData;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    const p1, 0x7f0e00ff

    .line 251
    invoke-virtual {p0, p1}, Lcom/stagnationlab/sk/ErrorActivity;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Lcom/stagnationlab/sk/models/ErrorData;->a(Ljava/lang/String;)Lcom/stagnationlab/sk/models/ErrorData;

    move-result-object p1

    return-object p1

    .line 237
    :pswitch_9
    new-instance p1, Lcom/stagnationlab/sk/models/ErrorData;

    const v0, 0x7f0e00bb

    .line 238
    invoke-virtual {p0, v0}, Lcom/stagnationlab/sk/ErrorActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 239
    invoke-virtual {p0, v1}, Lcom/stagnationlab/sk/ErrorActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p1, v0, v1}, Lcom/stagnationlab/sk/models/ErrorData;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 240
    invoke-virtual {p1, v5}, Lcom/stagnationlab/sk/models/ErrorData;->d(Z)Lcom/stagnationlab/sk/models/ErrorData;

    move-result-object p1

    return-object p1

    .line 231
    :pswitch_a
    new-instance p1, Lcom/stagnationlab/sk/models/ErrorData;

    const v0, 0x7f0e00ba

    .line 232
    invoke-virtual {p0, v0}, Lcom/stagnationlab/sk/ErrorActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-direct {p1, v0, v1}, Lcom/stagnationlab/sk/models/ErrorData;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 234
    invoke-virtual {p1, v5}, Lcom/stagnationlab/sk/models/ErrorData;->d(Z)Lcom/stagnationlab/sk/models/ErrorData;

    move-result-object p1

    return-object p1

    .line 225
    :pswitch_b
    new-instance p1, Lcom/stagnationlab/sk/models/ErrorData;

    const v0, 0x7f0e00b3

    .line 226
    invoke-virtual {p0, v0}, Lcom/stagnationlab/sk/ErrorActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f0e00b2

    .line 227
    invoke-virtual {p0, v1}, Lcom/stagnationlab/sk/ErrorActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p1, v0, v1}, Lcom/stagnationlab/sk/models/ErrorData;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object p1

    .line 217
    :pswitch_c
    new-instance p1, Lcom/stagnationlab/sk/models/ErrorData;

    const v0, 0x7f0e00b1

    .line 218
    invoke-virtual {p0, v0}, Lcom/stagnationlab/sk/ErrorActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f0e00b0

    .line 219
    invoke-virtual {p0, v1}, Lcom/stagnationlab/sk/ErrorActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p1, v0, v1, v2, v5}, Lcom/stagnationlab/sk/models/ErrorData;-><init>(Ljava/lang/String;Ljava/lang/String;IZ)V

    .line 222
    invoke-virtual {p1, v5}, Lcom/stagnationlab/sk/models/ErrorData;->a(Z)Lcom/stagnationlab/sk/models/ErrorData;

    move-result-object p1

    return-object p1

    .line 209
    :pswitch_d
    new-instance p1, Lcom/stagnationlab/sk/models/ErrorData;

    const v0, 0x7f0e00ad

    .line 210
    invoke-virtual {p0, v0}, Lcom/stagnationlab/sk/ErrorActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f0e00ac

    .line 211
    invoke-virtual {p0, v1}, Lcom/stagnationlab/sk/ErrorActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p1, v0, v1, v2, v5}, Lcom/stagnationlab/sk/models/ErrorData;-><init>(Ljava/lang/String;Ljava/lang/String;IZ)V

    return-object p1

    .line 201
    :pswitch_e
    new-instance p1, Lcom/stagnationlab/sk/models/ErrorData;

    const v0, 0x7f0e009f

    .line 202
    invoke-virtual {p0, v0}, Lcom/stagnationlab/sk/ErrorActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f0e009e

    .line 203
    invoke-virtual {p0, v1}, Lcom/stagnationlab/sk/ErrorActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p1, v0, v1, v4}, Lcom/stagnationlab/sk/models/ErrorData;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    return-object p1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_e
        :pswitch_d
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method static synthetic a(Lcom/stagnationlab/sk/ErrorActivity;Lcom/stagnationlab/sk/c/a;)Ljava/lang/String;
    .locals 0

    .line 31
    invoke-direct {p0, p1}, Lcom/stagnationlab/sk/ErrorActivity;->b(Lcom/stagnationlab/sk/c/a;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method private a(JZLcom/stagnationlab/sk/ErrorActivity$a;)V
    .locals 10

    .line 179
    iget-object v0, p0, Lcom/stagnationlab/sk/ErrorActivity;->i:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    const-wide/16 v0, 0x0

    cmp-long v2, p1, v0

    if-gez v2, :cond_0

    goto :goto_0

    :cond_0
    const-wide/16 v0, 0x1

    add-long/2addr v0, p1

    .line 183
    :goto_0
    new-instance p1, Lcom/stagnationlab/sk/ErrorActivity$5;

    const-wide/16 v2, 0x3e8

    mul-long v4, v0, v2

    const-wide/16 v6, 0x1f4

    move-object v2, p1

    move-object v3, p0

    move v8, p3

    move-object v9, p4

    invoke-direct/range {v2 .. v9}, Lcom/stagnationlab/sk/ErrorActivity$5;-><init>(Lcom/stagnationlab/sk/ErrorActivity;JJZLcom/stagnationlab/sk/ErrorActivity$a;)V

    .line 195
    invoke-virtual {p1}, Lcom/stagnationlab/sk/ErrorActivity$5;->start()Landroid/os/CountDownTimer;

    move-result-object p1

    iput-object p1, p0, Lcom/stagnationlab/sk/ErrorActivity;->f:Landroid/os/CountDownTimer;

    return-void
.end method

.method static synthetic b(Lcom/stagnationlab/sk/ErrorActivity;)Landroid/widget/TextView;
    .locals 0

    .line 31
    iget-object p0, p0, Lcom/stagnationlab/sk/ErrorActivity;->i:Landroid/widget/TextView;

    return-object p0
.end method

.method private b(Lcom/stagnationlab/sk/c/a;)Ljava/lang/String;
    .locals 2

    .line 396
    invoke-virtual {p1}, Lcom/stagnationlab/sk/c/a;->k()Lcom/stagnationlab/sk/c/c;

    move-result-object v0

    sget-object v1, Lcom/stagnationlab/sk/c/c;->b:Lcom/stagnationlab/sk/c/c;

    if-ne v0, v1, :cond_0

    const-string p1, "AppError"

    return-object p1

    .line 398
    :cond_0
    invoke-virtual {p1}, Lcom/stagnationlab/sk/c/a;->k()Lcom/stagnationlab/sk/c/c;

    move-result-object p1

    sget-object v0, Lcom/stagnationlab/sk/c/c;->a:Lcom/stagnationlab/sk/c/c;

    if-ne p1, v0, :cond_1

    const-string p1, "SmartIdError"

    return-object p1

    :cond_1
    const/4 p1, 0x0

    return-object p1
.end method

.method static synthetic c(Lcom/stagnationlab/sk/ErrorActivity;)Lcom/stagnationlab/sk/models/Transaction;
    .locals 0

    .line 31
    iget-object p0, p0, Lcom/stagnationlab/sk/ErrorActivity;->c:Lcom/stagnationlab/sk/models/Transaction;

    return-object p0
.end method

.method private n()V
    .locals 4

    .line 154
    iget-object v0, p0, Lcom/stagnationlab/sk/ErrorActivity;->e:Lcom/stagnationlab/sk/c/a;

    invoke-virtual {v0}, Lcom/stagnationlab/sk/c/a;->a()Lcom/stagnationlab/sk/c/b;

    move-result-object v0

    sget-object v1, Lcom/stagnationlab/sk/c/b;->o:Lcom/stagnationlab/sk/c/b;

    if-ne v0, v1, :cond_0

    .line 155
    iget-object v0, p0, Lcom/stagnationlab/sk/ErrorActivity;->e:Lcom/stagnationlab/sk/c/a;

    .line 156
    invoke-virtual {v0}, Lcom/stagnationlab/sk/c/a;->f()Lcom/stagnationlab/sk/models/AccountKey;

    move-result-object v0

    invoke-virtual {v0}, Lcom/stagnationlab/sk/models/AccountKey;->e()Lorg/b/a/b;

    move-result-object v0

    invoke-virtual {v0}, Lorg/b/a/b;->c()J

    move-result-wide v0

    invoke-static {}, Lorg/b/a/b;->a()Lorg/b/a/b;

    move-result-object v2

    invoke-virtual {v2}, Lorg/b/a/b;->c()J

    move-result-wide v2

    sub-long/2addr v0, v2

    const-wide/16 v2, 0x3e8

    div-long/2addr v0, v2

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 155
    invoke-direct {p0, v0, v1, v2, v3}, Lcom/stagnationlab/sk/ErrorActivity;->a(JZLcom/stagnationlab/sk/ErrorActivity$a;)V

    return-void

    .line 164
    :cond_0
    iget-object v0, p0, Lcom/stagnationlab/sk/ErrorActivity;->g:Lcom/stagnationlab/sk/models/ErrorData;

    invoke-virtual {v0}, Lcom/stagnationlab/sk/models/ErrorData;->h()Z

    move-result v0

    if-eqz v0, :cond_1

    const-wide/16 v0, 0xa

    const/4 v2, 0x0

    .line 165
    new-instance v3, Lcom/stagnationlab/sk/ErrorActivity$4;

    invoke-direct {v3, p0}, Lcom/stagnationlab/sk/ErrorActivity$4;-><init>(Lcom/stagnationlab/sk/ErrorActivity;)V

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/stagnationlab/sk/ErrorActivity;->a(JZLcom/stagnationlab/sk/ErrorActivity$a;)V

    :cond_1
    return-void
.end method


# virtual methods
.method public k()V
    .locals 3

    .line 317
    iget-object v0, p0, Lcom/stagnationlab/sk/ErrorActivity;->g:Lcom/stagnationlab/sk/models/ErrorData;

    invoke-virtual {v0}, Lcom/stagnationlab/sk/models/ErrorData;->d()Z

    move-result v0

    if-nez v0, :cond_2

    .line 318
    iget-object v0, p0, Lcom/stagnationlab/sk/ErrorActivity;->g:Lcom/stagnationlab/sk/models/ErrorData;

    invoke-virtual {v0}, Lcom/stagnationlab/sk/models/ErrorData;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 319
    invoke-static {p0}, Lcom/stagnationlab/sk/util/h;->b(Landroid/content/Context;)V

    goto :goto_0

    .line 320
    :cond_0
    iget-object v0, p0, Lcom/stagnationlab/sk/ErrorActivity;->g:Lcom/stagnationlab/sk/models/ErrorData;

    invoke-virtual {v0}, Lcom/stagnationlab/sk/models/ErrorData;->g()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 321
    invoke-static {p0}, Lcom/stagnationlab/sk/util/h;->a(Landroid/app/Activity;)V

    goto :goto_0

    .line 323
    :cond_1
    invoke-virtual {p0}, Lcom/stagnationlab/sk/ErrorActivity;->m()V

    :goto_0
    return-void

    .line 329
    :cond_2
    iget-object v0, p0, Lcom/stagnationlab/sk/ErrorActivity;->c:Lcom/stagnationlab/sk/models/Transaction;

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/stagnationlab/sk/ErrorActivity;->d:Lcom/stagnationlab/sk/models/RpRequest;

    if-nez v0, :cond_3

    .line 330
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/stagnationlab/sk/MainActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 331
    invoke-virtual {p0, v0}, Lcom/stagnationlab/sk/ErrorActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_1

    .line 333
    :cond_3
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/stagnationlab/sk/TransactionActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 334
    iget-object v1, p0, Lcom/stagnationlab/sk/ErrorActivity;->c:Lcom/stagnationlab/sk/models/Transaction;

    const-string v2, "transaction"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 335
    iget-object v1, p0, Lcom/stagnationlab/sk/ErrorActivity;->d:Lcom/stagnationlab/sk/models/RpRequest;

    const-string v2, "rpRequest"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 336
    iget-object v1, p0, Lcom/stagnationlab/sk/ErrorActivity;->h:Lcom/stagnationlab/sk/models/TransactionMetaData;

    const-string v2, "metaData"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 337
    invoke-virtual {p0, v0}, Lcom/stagnationlab/sk/ErrorActivity;->startActivity(Landroid/content/Intent;)V

    .line 339
    :goto_1
    invoke-static {p0}, Lcom/stagnationlab/sk/util/n;->b(Landroid/app/Activity;)V

    .line 341
    invoke-super {p0}, Lcom/stagnationlab/sk/c;->finish()V

    return-void
.end method

.method public l()V
    .locals 2

    .line 345
    iget-object v0, p0, Lcom/stagnationlab/sk/ErrorActivity;->c:Lcom/stagnationlab/sk/models/Transaction;

    if-eqz v0, :cond_0

    .line 346
    invoke-virtual {p0}, Lcom/stagnationlab/sk/ErrorActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/stagnationlab/sk/MyApplication;

    new-instance v1, Lcom/stagnationlab/sk/ErrorActivity$6;

    invoke-direct {v1, p0}, Lcom/stagnationlab/sk/ErrorActivity$6;-><init>(Lcom/stagnationlab/sk/ErrorActivity;)V

    invoke-virtual {v0, v1}, Lcom/stagnationlab/sk/MyApplication;->a(Lcom/stagnationlab/sk/a/a/a;)V

    .line 379
    :cond_0
    invoke-virtual {p0}, Lcom/stagnationlab/sk/ErrorActivity;->m()V

    return-void
.end method

.method public m()V
    .locals 0

    .line 388
    invoke-static {p0}, Lcom/stagnationlab/sk/ExitActivity;->a(Landroid/app/Activity;)V

    return-void
.end method

.method public onBackPressed()V
    .locals 0

    .line 384
    invoke-virtual {p0}, Lcom/stagnationlab/sk/ErrorActivity;->m()V

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 7

    .line 45
    invoke-super {p0, p1}, Lcom/stagnationlab/sk/c;->onCreate(Landroid/os/Bundle;)V

    const p1, 0x7f0a001d

    .line 46
    invoke-virtual {p0, p1}, Lcom/stagnationlab/sk/ErrorActivity;->setContentView(I)V

    .line 48
    invoke-virtual {p0}, Lcom/stagnationlab/sk/ErrorActivity;->getIntent()Landroid/content/Intent;

    move-result-object p1

    const v0, 0x7f080040

    .line 49
    invoke-virtual {p0, v0}, Lcom/stagnationlab/sk/ErrorActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/stagnationlab/sk/ErrorActivity;->i:Landroid/widget/TextView;

    const-string v0, "transaction"

    .line 50
    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/stagnationlab/sk/models/Transaction;

    iput-object v0, p0, Lcom/stagnationlab/sk/ErrorActivity;->c:Lcom/stagnationlab/sk/models/Transaction;

    const v0, 0x7f0800af

    .line 51
    invoke-virtual {p0, v0}, Lcom/stagnationlab/sk/ErrorActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/stagnationlab/sk/ErrorActivity;->j:Landroid/widget/TextView;

    const-string v0, "rpRequest"

    .line 52
    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/stagnationlab/sk/models/RpRequest;

    iput-object v0, p0, Lcom/stagnationlab/sk/ErrorActivity;->d:Lcom/stagnationlab/sk/models/RpRequest;

    const-string v0, "error"

    .line 53
    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/stagnationlab/sk/c/a;

    iput-object v0, p0, Lcom/stagnationlab/sk/ErrorActivity;->e:Lcom/stagnationlab/sk/c/a;

    const-string v0, "metaData"

    .line 54
    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/stagnationlab/sk/models/TransactionMetaData;

    iput-object v0, p0, Lcom/stagnationlab/sk/ErrorActivity;->h:Lcom/stagnationlab/sk/models/TransactionMetaData;

    const v0, 0x7f080065

    .line 56
    invoke-virtual {p0, v0}, Lcom/stagnationlab/sk/ErrorActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    const v1, 0x7f080053

    .line 57
    invoke-virtual {p0, v1}, Lcom/stagnationlab/sk/ErrorActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    const v2, 0x7f080052

    .line 58
    invoke-virtual {p0, v2}, Lcom/stagnationlab/sk/ErrorActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    const v3, 0x7f0800d8

    .line 59
    invoke-virtual {p0, v3}, Lcom/stagnationlab/sk/ErrorActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    const v4, 0x7f080036

    .line 60
    invoke-virtual {p0, v4}, Lcom/stagnationlab/sk/ErrorActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/support/v7/widget/AppCompatImageButton;

    const v5, 0x7f080028

    .line 61
    invoke-virtual {p0, v5}, Lcom/stagnationlab/sk/ErrorActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/Button;

    .line 63
    iget-object v6, p0, Lcom/stagnationlab/sk/ErrorActivity;->e:Lcom/stagnationlab/sk/c/a;

    invoke-direct {p0, v6}, Lcom/stagnationlab/sk/ErrorActivity;->a(Lcom/stagnationlab/sk/c/a;)Lcom/stagnationlab/sk/models/ErrorData;

    move-result-object v6

    iput-object v6, p0, Lcom/stagnationlab/sk/ErrorActivity;->g:Lcom/stagnationlab/sk/models/ErrorData;

    .line 66
    new-instance v6, Lcom/stagnationlab/sk/ErrorActivity$1;

    invoke-direct {v6, p0}, Lcom/stagnationlab/sk/ErrorActivity$1;-><init>(Lcom/stagnationlab/sk/ErrorActivity;)V

    invoke-virtual {v3, v6}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 72
    new-instance v6, Lcom/stagnationlab/sk/ErrorActivity$2;

    invoke-direct {v6, p0}, Lcom/stagnationlab/sk/ErrorActivity$2;-><init>(Lcom/stagnationlab/sk/ErrorActivity;)V

    invoke-virtual {v5, v6}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 78
    new-instance v6, Lcom/stagnationlab/sk/ErrorActivity$3;

    invoke-direct {v6, p0}, Lcom/stagnationlab/sk/ErrorActivity$3;-><init>(Lcom/stagnationlab/sk/ErrorActivity;)V

    invoke-virtual {v4, v6}, Landroid/support/v7/widget/AppCompatImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 91
    iget-object v4, p0, Lcom/stagnationlab/sk/ErrorActivity;->g:Lcom/stagnationlab/sk/models/ErrorData;

    invoke-virtual {v4}, Lcom/stagnationlab/sk/models/ErrorData;->a()I

    move-result v4

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 92
    iget-object v0, p0, Lcom/stagnationlab/sk/ErrorActivity;->g:Lcom/stagnationlab/sk/models/ErrorData;

    invoke-virtual {v0}, Lcom/stagnationlab/sk/models/ErrorData;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 93
    iget-object v0, p0, Lcom/stagnationlab/sk/ErrorActivity;->g:Lcom/stagnationlab/sk/models/ErrorData;

    invoke-virtual {v0}, Lcom/stagnationlab/sk/models/ErrorData;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v2, v0}, Lcom/stagnationlab/sk/util/k;->a(Landroid/content/Context;Landroid/widget/TextView;Ljava/lang/String;)V

    .line 95
    iget-object v0, p0, Lcom/stagnationlab/sk/ErrorActivity;->g:Lcom/stagnationlab/sk/models/ErrorData;

    invoke-virtual {v0}, Lcom/stagnationlab/sk/models/ErrorData;->i()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 98
    iget-object v2, p0, Lcom/stagnationlab/sk/ErrorActivity;->j:Landroid/widget/TextView;

    invoke-static {p0, v2, v0}, Lcom/stagnationlab/sk/util/k;->a(Landroid/content/Context;Landroid/widget/TextView;Ljava/lang/String;)V

    .line 101
    iget-object v0, p0, Lcom/stagnationlab/sk/ErrorActivity;->j:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 104
    :cond_0
    iget-object v0, p0, Lcom/stagnationlab/sk/ErrorActivity;->g:Lcom/stagnationlab/sk/models/ErrorData;

    invoke-virtual {v0}, Lcom/stagnationlab/sk/models/ErrorData;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 105
    iget-object v0, p0, Lcom/stagnationlab/sk/ErrorActivity;->e:Lcom/stagnationlab/sk/c/a;

    invoke-direct {p0, v0}, Lcom/stagnationlab/sk/ErrorActivity;->b(Lcom/stagnationlab/sk/c/a;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    const v2, 0x7f080051

    .line 108
    invoke-virtual {p0, v2}, Lcom/stagnationlab/sk/ErrorActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 110
    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 111
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, ".\u200b"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/stagnationlab/sk/ErrorActivity;->e:Lcom/stagnationlab/sk/c/a;

    invoke-virtual {v0}, Lcom/stagnationlab/sk/c/a;->l()Ljava/lang/String;

    move-result-object v0

    const-string v4, "_"

    const-string v6, "_\u200b"

    invoke-virtual {v0, v4, v6}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 115
    :cond_1
    invoke-virtual {p0}, Lcom/stagnationlab/sk/ErrorActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/stagnationlab/sk/MyApplication;

    invoke-virtual {v0}, Lcom/stagnationlab/sk/MyApplication;->e()Lcom/stagnationlab/sk/a;

    move-result-object v0

    const-string v1, "Error"

    .line 116
    invoke-virtual {v0, v1}, Lcom/stagnationlab/sk/a;->a(Ljava/lang/String;)V

    .line 118
    iget-object v0, p0, Lcom/stagnationlab/sk/ErrorActivity;->g:Lcom/stagnationlab/sk/models/ErrorData;

    invoke-virtual {v0}, Lcom/stagnationlab/sk/models/ErrorData;->d()Z

    move-result v0

    if-nez v0, :cond_3

    const/16 v0, 0x8

    .line 119
    invoke-virtual {v5, v0}, Landroid/widget/Button;->setVisibility(I)V

    const v0, 0x7f0e00f8

    .line 123
    iget-object v1, p0, Lcom/stagnationlab/sk/ErrorActivity;->g:Lcom/stagnationlab/sk/models/ErrorData;

    invoke-virtual {v1}, Lcom/stagnationlab/sk/models/ErrorData;->f()Z

    move-result v1

    if-eqz v1, :cond_2

    const v0, 0x7f0e0121

    .line 127
    :cond_2
    invoke-virtual {p0, v0}, Lcom/stagnationlab/sk/ErrorActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 130
    :cond_3
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Error: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/stagnationlab/sk/ErrorActivity;->e:Lcom/stagnationlab/sk/c/a;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ErrorActivity"

    invoke-static {v1, v0}, Lcom/stagnationlab/sk/f;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 132
    invoke-virtual {p0}, Lcom/stagnationlab/sk/ErrorActivity;->d()V

    .line 133
    invoke-static {p0, p1}, Lcom/stagnationlab/sk/fcm/ClearNotificationReceiver;->a(Landroid/content/Context;Landroid/content/Intent;)V

    return-void
.end method

.method protected onPause()V
    .locals 1

    .line 145
    iget-object v0, p0, Lcom/stagnationlab/sk/ErrorActivity;->f:Landroid/os/CountDownTimer;

    if-eqz v0, :cond_0

    .line 146
    invoke-virtual {v0}, Landroid/os/CountDownTimer;->cancel()V

    const/4 v0, 0x0

    .line 147
    iput-object v0, p0, Lcom/stagnationlab/sk/ErrorActivity;->f:Landroid/os/CountDownTimer;

    .line 150
    :cond_0
    invoke-super {p0}, Lcom/stagnationlab/sk/c;->onPause()V

    return-void
.end method

.method protected onResume()V
    .locals 0

    .line 138
    invoke-super {p0}, Lcom/stagnationlab/sk/c;->onResume()V

    .line 140
    invoke-direct {p0}, Lcom/stagnationlab/sk/ErrorActivity;->n()V

    return-void
.end method
