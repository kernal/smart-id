.class public Lcom/stagnationlab/sk/f;
.super Ljava/lang/Object;
.source "Log.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/stagnationlab/sk/f$a;
    }
.end annotation


# static fields
.field private static a:Lcom/stagnationlab/sk/f$a;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 12
    sget-object v0, Lcom/stagnationlab/sk/f$a;->a:Lcom/stagnationlab/sk/f$a;

    sput-object v0, Lcom/stagnationlab/sk/f;->a:Lcom/stagnationlab/sk/f$a;

    return-void
.end method

.method static a(Lcom/stagnationlab/sk/f$a;)V
    .locals 1

    .line 53
    sput-object p0, Lcom/stagnationlab/sk/f;->a:Lcom/stagnationlab/sk/f$a;

    .line 55
    sget-object v0, Lcom/stagnationlab/sk/f$a;->a:Lcom/stagnationlab/sk/f$a;

    if-ne p0, v0, :cond_0

    .line 56
    sget-object p0, Lee/cyber/smartid/util/Log;->LOG_LEVEL_RELEASE_SILENT:Ljava/lang/String;

    invoke-static {p0}, Lee/cyber/smartid/util/Log;->setLogLevel(Ljava/lang/String;)V

    goto :goto_0

    .line 58
    :cond_0
    sget-object p0, Lee/cyber/smartid/util/Log;->LOG_LEVEL_DEBUG:Ljava/lang/String;

    invoke-static {p0}, Lee/cyber/smartid/util/Log;->setLogLevel(Ljava/lang/String;)V

    :goto_0
    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .line 15
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Smart-ID "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ""

    .line 16
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 18
    invoke-static {p0, p1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 19
    invoke-static {p0, p1}, Lcom/stagnationlab/sk/e;->a(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 0

    .line 33
    invoke-static {p0}, Ld/a/a;->a(Ljava/lang/String;)Ld/a/a$b;

    const/4 p0, 0x0

    .line 34
    new-array p0, p0, [Ljava/lang/Object;

    invoke-static {p2, p1, p0}, Ld/a/a;->d(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public static a()Z
    .locals 2

    .line 63
    sget-object v0, Lcom/stagnationlab/sk/f;->a:Lcom/stagnationlab/sk/f$a;

    sget-object v1, Lcom/stagnationlab/sk/f$a;->b:Lcom/stagnationlab/sk/f$a;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public static b(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 23
    invoke-static {p0}, Ld/a/a;->a(Ljava/lang/String;)Ld/a/a$b;

    const/4 p0, 0x0

    .line 24
    new-array p0, p0, [Ljava/lang/Object;

    invoke-static {p1, p0}, Ld/a/a;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public static c(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 28
    invoke-static {p0}, Ld/a/a;->a(Ljava/lang/String;)Ld/a/a$b;

    const/4 p0, 0x0

    .line 29
    new-array p0, p0, [Ljava/lang/Object;

    invoke-static {p1, p0}, Ld/a/a;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public static d(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 38
    invoke-static {p0}, Ld/a/a;->a(Ljava/lang/String;)Ld/a/a$b;

    const/4 p0, 0x0

    .line 39
    new-array p0, p0, [Ljava/lang/Object;

    invoke-static {p1, p0}, Ld/a/a;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public static e(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 48
    invoke-static {p0}, Ld/a/a;->a(Ljava/lang/String;)Ld/a/a$b;

    const/4 p0, 0x0

    .line 49
    new-array p0, p0, [Ljava/lang/Object;

    invoke-static {p1, p0}, Ld/a/a;->c(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method
