.class Lcom/stagnationlab/sk/g;
.super Ljava/lang/Object;
.source "PinBullet.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/stagnationlab/sk/g$a;
    }
.end annotation


# static fields
.field static a:I = 0x12c


# instance fields
.field private b:Lcom/stagnationlab/sk/g$a;

.field private c:Landroid/view/View;

.field private d:Landroid/view/View;

.field private e:Landroid/graphics/drawable/TransitionDrawable;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method constructor <init>(Landroid/view/View;)V
    .locals 1

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 10
    sget-object v0, Lcom/stagnationlab/sk/g$a;->a:Lcom/stagnationlab/sk/g$a;

    iput-object v0, p0, Lcom/stagnationlab/sk/g;->b:Lcom/stagnationlab/sk/g$a;

    .line 16
    iput-object p1, p0, Lcom/stagnationlab/sk/g;->d:Landroid/view/View;

    const v0, 0x7f080085

    .line 17
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/stagnationlab/sk/g;->c:Landroid/view/View;

    const v0, 0x7f080087

    .line 18
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    invoke-virtual {p1}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object p1

    check-cast p1, Landroid/graphics/drawable/TransitionDrawable;

    iput-object p1, p0, Lcom/stagnationlab/sk/g;->e:Landroid/graphics/drawable/TransitionDrawable;

    return-void
.end method


# virtual methods
.method public a()Landroid/view/View;
    .locals 1

    .line 22
    iget-object v0, p0, Lcom/stagnationlab/sk/g;->d:Landroid/view/View;

    return-object v0
.end method

.method a(Lcom/stagnationlab/sk/g$a;)V
    .locals 3

    .line 26
    iget-object v0, p0, Lcom/stagnationlab/sk/g;->b:Lcom/stagnationlab/sk/g$a;

    if-ne v0, p1, :cond_0

    return-void

    .line 29
    :cond_0
    sget-object v0, Lcom/stagnationlab/sk/g$a;->b:Lcom/stagnationlab/sk/g$a;

    if-ne p1, v0, :cond_1

    .line 30
    iget-object v0, p0, Lcom/stagnationlab/sk/g;->c:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    sget v1, Lcom/stagnationlab/sk/g;->a:I

    int-to-long v1, v1

    invoke-virtual {v0, v1, v2}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/16 v1, 0x8

    invoke-static {v1}, Lcom/stagnationlab/sk/i;->a(I)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->y(F)Landroid/view/ViewPropertyAnimator;

    goto :goto_0

    .line 31
    :cond_1
    iget-object v0, p0, Lcom/stagnationlab/sk/g;->b:Lcom/stagnationlab/sk/g$a;

    sget-object v1, Lcom/stagnationlab/sk/g$a;->b:Lcom/stagnationlab/sk/g$a;

    if-ne v0, v1, :cond_2

    .line 32
    iget-object v0, p0, Lcom/stagnationlab/sk/g;->c:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->y(F)Landroid/view/ViewPropertyAnimator;

    .line 35
    :cond_2
    :goto_0
    sget-object v0, Lcom/stagnationlab/sk/g$a;->c:Lcom/stagnationlab/sk/g$a;

    if-ne p1, v0, :cond_3

    .line 36
    iget-object v0, p0, Lcom/stagnationlab/sk/g;->e:Landroid/graphics/drawable/TransitionDrawable;

    sget v1, Lcom/stagnationlab/sk/g;->a:I

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/TransitionDrawable;->startTransition(I)V

    goto :goto_1

    .line 37
    :cond_3
    iget-object v0, p0, Lcom/stagnationlab/sk/g;->b:Lcom/stagnationlab/sk/g$a;

    sget-object v1, Lcom/stagnationlab/sk/g$a;->c:Lcom/stagnationlab/sk/g$a;

    if-ne v0, v1, :cond_4

    .line 38
    iget-object v0, p0, Lcom/stagnationlab/sk/g;->e:Landroid/graphics/drawable/TransitionDrawable;

    sget v1, Lcom/stagnationlab/sk/g;->a:I

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/TransitionDrawable;->reverseTransition(I)V

    .line 41
    :cond_4
    :goto_1
    iput-object p1, p0, Lcom/stagnationlab/sk/g;->b:Lcom/stagnationlab/sk/g$a;

    return-void
.end method
