.class public Lcom/stagnationlab/sk/d/a;
.super Ljava/lang/Exception;
.source "HttpException.java"


# instance fields
.field private a:Lcom/stagnationlab/sk/c/b;

.field private b:Ljava/lang/Exception;


# direct methods
.method public constructor <init>(Ljava/lang/Exception;)V
    .locals 1

    const/4 v0, 0x0

    .line 10
    invoke-direct {p0, p1, v0}, Lcom/stagnationlab/sk/d/a;-><init>(Ljava/lang/Exception;Lcom/stagnationlab/sk/c/b;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/Exception;Lcom/stagnationlab/sk/c/b;)V
    .locals 2

    .line 14
    invoke-virtual {p1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Exception;->getCause()Ljava/lang/Throwable;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 15
    iput-object p1, p0, Lcom/stagnationlab/sk/d/a;->b:Ljava/lang/Exception;

    .line 16
    iput-object p2, p0, Lcom/stagnationlab/sk/d/a;->a:Lcom/stagnationlab/sk/c/b;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/stagnationlab/sk/c/b;)V
    .locals 0

    .line 20
    invoke-direct {p0, p1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    .line 21
    iput-object p2, p0, Lcom/stagnationlab/sk/d/a;->a:Lcom/stagnationlab/sk/c/b;

    return-void
.end method


# virtual methods
.method public a()Lcom/stagnationlab/sk/c/b;
    .locals 1

    .line 25
    iget-object v0, p0, Lcom/stagnationlab/sk/d/a;->a:Lcom/stagnationlab/sk/c/b;

    return-object v0
.end method

.method public b()Ljava/lang/Exception;
    .locals 1

    .line 29
    iget-object v0, p0, Lcom/stagnationlab/sk/d/a;->b:Ljava/lang/Exception;

    return-object v0
.end method
