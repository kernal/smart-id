.class Lcom/stagnationlab/sk/g/e$3;
.super Landroid/webkit/WebViewClient;
.source "SsoManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/stagnationlab/sk/g/e;->a(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Ljava/util/List;

.field final synthetic b:Lcom/stagnationlab/sk/g/e;


# direct methods
.method constructor <init>(Lcom/stagnationlab/sk/g/e;Ljava/util/List;)V
    .locals 0

    .line 177
    iput-object p1, p0, Lcom/stagnationlab/sk/g/e$3;->b:Lcom/stagnationlab/sk/g/e;

    iput-object p2, p0, Lcom/stagnationlab/sk/g/e$3;->a:Ljava/util/List;

    invoke-direct {p0}, Landroid/webkit/WebViewClient;-><init>()V

    return-void
.end method


# virtual methods
.method public onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V
    .locals 2

    .line 232
    invoke-super {p0, p1, p2}, Landroid/webkit/WebViewClient;->onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V

    .line 234
    iget-object p1, p0, Lcom/stagnationlab/sk/g/e$3;->b:Lcom/stagnationlab/sk/g/e;

    invoke-static {p1}, Lcom/stagnationlab/sk/g/e;->e(Lcom/stagnationlab/sk/g/e;)Lcom/stagnationlab/sk/g/a;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 235
    iget-object p1, p0, Lcom/stagnationlab/sk/g/e$3;->b:Lcom/stagnationlab/sk/g/e;

    invoke-static {p1}, Lcom/stagnationlab/sk/g/e;->e(Lcom/stagnationlab/sk/g/e;)Lcom/stagnationlab/sk/g/a;

    move-result-object p1

    invoke-virtual {p1}, Lcom/stagnationlab/sk/g/a;->b()V

    .line 238
    :cond_0
    iget-object p1, p0, Lcom/stagnationlab/sk/g/e$3;->b:Lcom/stagnationlab/sk/g/e;

    invoke-static {p1}, Lcom/stagnationlab/sk/g/e;->f(Lcom/stagnationlab/sk/g/e;)Z

    move-result p1

    if-nez p1, :cond_1

    .line 239
    iget-object p1, p0, Lcom/stagnationlab/sk/g/e$3;->b:Lcom/stagnationlab/sk/g/e;

    const/4 p2, 0x1

    invoke-static {p1, p2}, Lcom/stagnationlab/sk/g/e;->a(Lcom/stagnationlab/sk/g/e;Z)Z

    .line 240
    iget-object p1, p0, Lcom/stagnationlab/sk/g/e$3;->b:Lcom/stagnationlab/sk/g/e;

    invoke-static {p1}, Lcom/stagnationlab/sk/g/e;->g(Lcom/stagnationlab/sk/g/e;)Landroid/view/View;

    move-result-object p1

    const/4 p2, 0x0

    invoke-virtual {p1, p2}, Landroid/view/View;->setVisibility(I)V

    const/16 p1, 0xc8

    .line 243
    new-instance p2, Lcom/stagnationlab/sk/util/n;

    iget-object v0, p0, Lcom/stagnationlab/sk/g/e$3;->b:Lcom/stagnationlab/sk/g/e;

    invoke-static {v0}, Lcom/stagnationlab/sk/g/e;->d(Lcom/stagnationlab/sk/g/e;)Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lcom/stagnationlab/sk/g/e$3;->b:Lcom/stagnationlab/sk/g/e;

    invoke-static {v1}, Lcom/stagnationlab/sk/g/e;->h(Lcom/stagnationlab/sk/g/e;)Landroid/view/View;

    move-result-object v1

    invoke-direct {p2, v0, v1}, Lcom/stagnationlab/sk/util/n;-><init>(Landroid/content/Context;Landroid/view/View;)V

    invoke-virtual {p2}, Lcom/stagnationlab/sk/util/n;->b()Lcom/stagnationlab/sk/util/n;

    move-result-object p2

    invoke-virtual {p2, p1}, Lcom/stagnationlab/sk/util/n;->a(I)Lcom/stagnationlab/sk/util/n;

    move-result-object p2

    invoke-virtual {p2}, Lcom/stagnationlab/sk/util/n;->a()V

    .line 244
    new-instance p2, Lcom/stagnationlab/sk/util/n;

    iget-object v0, p0, Lcom/stagnationlab/sk/g/e$3;->b:Lcom/stagnationlab/sk/g/e;

    invoke-static {v0}, Lcom/stagnationlab/sk/g/e;->d(Lcom/stagnationlab/sk/g/e;)Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lcom/stagnationlab/sk/g/e$3;->b:Lcom/stagnationlab/sk/g/e;

    invoke-static {v1}, Lcom/stagnationlab/sk/g/e;->g(Lcom/stagnationlab/sk/g/e;)Landroid/view/View;

    move-result-object v1

    invoke-direct {p2, v0, v1}, Lcom/stagnationlab/sk/util/n;-><init>(Landroid/content/Context;Landroid/view/View;)V

    invoke-virtual {p2}, Lcom/stagnationlab/sk/util/n;->e()Lcom/stagnationlab/sk/util/n;

    move-result-object p2

    invoke-virtual {p2, p1}, Lcom/stagnationlab/sk/util/n;->a(I)Lcom/stagnationlab/sk/util/n;

    move-result-object p1

    new-instance p2, Lcom/stagnationlab/sk/g/e$3$1;

    invoke-direct {p2, p0}, Lcom/stagnationlab/sk/g/e$3$1;-><init>(Lcom/stagnationlab/sk/g/e$3;)V

    invoke-virtual {p1, p2}, Lcom/stagnationlab/sk/util/n;->a(Lcom/stagnationlab/sk/e/b;)Lcom/stagnationlab/sk/util/n;

    move-result-object p1

    .line 252
    invoke-virtual {p1}, Lcom/stagnationlab/sk/util/n;->a()V

    goto :goto_0

    .line 254
    :cond_1
    iget-object p1, p0, Lcom/stagnationlab/sk/g/e$3;->b:Lcom/stagnationlab/sk/g/e;

    invoke-static {p1}, Lcom/stagnationlab/sk/g/e;->b(Lcom/stagnationlab/sk/g/e;)Lcom/stagnationlab/sk/g/c;

    move-result-object p1

    iget-object p2, p0, Lcom/stagnationlab/sk/g/e$3;->b:Lcom/stagnationlab/sk/g/e;

    invoke-static {p2}, Lcom/stagnationlab/sk/g/e;->a(Lcom/stagnationlab/sk/g/e;)Lcom/stagnationlab/sk/g/b;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/stagnationlab/sk/g/c;->a(Lcom/stagnationlab/sk/g/b;)V

    :goto_0
    return-void
.end method

.method public onPageStarted(Landroid/webkit/WebView;Ljava/lang/String;Landroid/graphics/Bitmap;)V
    .locals 0

    .line 260
    invoke-super {p0, p1, p2, p3}, Landroid/webkit/WebViewClient;->onPageStarted(Landroid/webkit/WebView;Ljava/lang/String;Landroid/graphics/Bitmap;)V

    .line 262
    iget-object p1, p0, Lcom/stagnationlab/sk/g/e$3;->b:Lcom/stagnationlab/sk/g/e;

    invoke-static {p1}, Lcom/stagnationlab/sk/g/e;->f(Lcom/stagnationlab/sk/g/e;)Z

    move-result p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/stagnationlab/sk/g/e$3;->b:Lcom/stagnationlab/sk/g/e;

    invoke-static {p1}, Lcom/stagnationlab/sk/g/e;->e(Lcom/stagnationlab/sk/g/e;)Lcom/stagnationlab/sk/g/a;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 263
    iget-object p1, p0, Lcom/stagnationlab/sk/g/e$3;->b:Lcom/stagnationlab/sk/g/e;

    invoke-static {p1}, Lcom/stagnationlab/sk/g/e;->e(Lcom/stagnationlab/sk/g/e;)Lcom/stagnationlab/sk/g/a;

    move-result-object p1

    invoke-virtual {p1}, Lcom/stagnationlab/sk/g/a;->a()V

    :cond_0
    return-void
.end method

.method public onReceivedError(Landroid/webkit/WebView;ILjava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 205
    invoke-super {p0, p1, p2, p3, p4}, Landroid/webkit/WebViewClient;->onReceivedError(Landroid/webkit/WebView;ILjava/lang/String;Ljava/lang/String;)V

    .line 207
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string p2, "Failed to load URL "

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p2, ", error: "

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string p2, "SsoManager"

    invoke-static {p2, p1}, Lcom/stagnationlab/sk/f;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 209
    iget-object p1, p0, Lcom/stagnationlab/sk/g/e$3;->b:Lcom/stagnationlab/sk/g/e;

    invoke-static {p1}, Lcom/stagnationlab/sk/g/e;->c(Lcom/stagnationlab/sk/g/e;)V

    return-void
.end method

.method public onReceivedError(Landroid/webkit/WebView;Landroid/webkit/WebResourceRequest;Landroid/webkit/WebResourceError;)V
    .locals 1
    .annotation build Landroid/annotation/TargetApi;
        value = 0x17
    .end annotation

    .line 193
    invoke-super {p0, p1, p2, p3}, Landroid/webkit/WebViewClient;->onReceivedError(Landroid/webkit/WebView;Landroid/webkit/WebResourceRequest;Landroid/webkit/WebResourceError;)V

    .line 195
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "Failed to load URL "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-interface {p2}, Landroid/webkit/WebResourceRequest;->getUrl()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v0, ", error: "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Landroid/webkit/WebResourceError;->getDescription()Ljava/lang/CharSequence;

    move-result-object p3

    invoke-virtual {p1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string p3, ", isMainFrame: "

    invoke-virtual {p1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-interface {p2}, Landroid/webkit/WebResourceRequest;->isForMainFrame()Z

    move-result p3

    invoke-virtual {p1, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string p3, "SsoManager"

    invoke-static {p3, p1}, Lcom/stagnationlab/sk/f;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 197
    invoke-interface {p2}, Landroid/webkit/WebResourceRequest;->isForMainFrame()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 198
    iget-object p1, p0, Lcom/stagnationlab/sk/g/e$3;->b:Lcom/stagnationlab/sk/g/e;

    invoke-static {p1}, Lcom/stagnationlab/sk/g/e;->c(Lcom/stagnationlab/sk/g/e;)V

    :cond_0
    return-void
.end method

.method public onReceivedSslError(Landroid/webkit/WebView;Landroid/webkit/SslErrorHandler;Landroid/net/http/SslError;)V
    .locals 1

    .line 214
    invoke-virtual {p3}, Landroid/net/http/SslError;->getUrl()Ljava/lang/String;

    move-result-object p1

    const-string v0, "https://portal.smart-id.com"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 215
    sget-object p1, Lcom/stagnationlab/sk/util/i$a;->a:Lcom/stagnationlab/sk/util/i$a;

    invoke-static {p1}, Lcom/stagnationlab/sk/util/i;->a(Lcom/stagnationlab/sk/util/i$a;)V

    .line 216
    iget-object p1, p0, Lcom/stagnationlab/sk/g/e$3;->b:Lcom/stagnationlab/sk/g/e;

    invoke-static {p1}, Lcom/stagnationlab/sk/g/e;->d(Lcom/stagnationlab/sk/g/e;)Landroid/app/Activity;

    move-result-object p1

    sget-object v0, Lcom/stagnationlab/sk/c/b;->R:Lcom/stagnationlab/sk/c/b;

    invoke-static {p1, v0}, Lcom/stagnationlab/sk/util/h;->a(Landroid/app/Activity;Lcom/stagnationlab/sk/c/b;)V

    .line 219
    :cond_0
    invoke-virtual {p3}, Landroid/net/http/SslError;->getUrl()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p1

    invoke-virtual {p1}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object p1

    .line 221
    iget-object p3, p0, Lcom/stagnationlab/sk/g/e$3;->a:Ljava/util/List;

    invoke-interface {p3, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    .line 222
    invoke-virtual {p2}, Landroid/webkit/SslErrorHandler;->proceed()V

    return-void

    .line 227
    :cond_1
    invoke-virtual {p2}, Landroid/webkit/SslErrorHandler;->cancel()V

    return-void
.end method

.method public shouldOverrideUrlLoading(Landroid/webkit/WebView;Landroid/webkit/WebResourceRequest;)Z
    .locals 0
    .annotation build Landroid/annotation/TargetApi;
        value = 0x18
    .end annotation

    .line 187
    iget-object p1, p0, Lcom/stagnationlab/sk/g/e$3;->b:Lcom/stagnationlab/sk/g/e;

    invoke-interface {p2}, Landroid/webkit/WebResourceRequest;->getUrl()Landroid/net/Uri;

    move-result-object p2

    invoke-static {p1, p2}, Lcom/stagnationlab/sk/g/e;->a(Lcom/stagnationlab/sk/g/e;Landroid/net/Uri;)Z

    move-result p1

    return p1
.end method

.method public shouldOverrideUrlLoading(Landroid/webkit/WebView;Ljava/lang/String;)Z
    .locals 0

    .line 181
    iget-object p1, p0, Lcom/stagnationlab/sk/g/e$3;->b:Lcom/stagnationlab/sk/g/e;

    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p2

    invoke-static {p1, p2}, Lcom/stagnationlab/sk/g/e;->a(Lcom/stagnationlab/sk/g/e;Landroid/net/Uri;)Z

    move-result p1

    return p1
.end method
