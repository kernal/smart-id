.class public Lcom/stagnationlab/sk/g/b;
.super Landroid/webkit/WebView;
.source "ObservableWebView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/stagnationlab/sk/g/b$a;
    }
.end annotation


# instance fields
.field private a:Lcom/stagnationlab/sk/g/b$a;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .line 11
    invoke-direct {p0, p1}, Landroid/webkit/WebView;-><init>(Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method public getScrollHeight()I
    .locals 1

    .line 36
    invoke-virtual {p0}, Lcom/stagnationlab/sk/g/b;->computeVerticalScrollRange()I

    move-result v0

    return v0
.end method

.method protected onScrollChanged(IIII)V
    .locals 6

    .line 24
    invoke-super {p0, p1, p2, p3, p4}, Landroid/webkit/WebView;->onScrollChanged(IIII)V

    .line 26
    iget-object v0, p0, Lcom/stagnationlab/sk/g/b;->a:Lcom/stagnationlab/sk/g/b$a;

    if-eqz v0, :cond_0

    move-object v1, p0

    move v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    .line 27
    invoke-interface/range {v0 .. v5}, Lcom/stagnationlab/sk/g/b$a;->a(Landroid/webkit/WebView;IIII)V

    :cond_0
    return-void
.end method

.method public setOnScrollChangeListener(Lcom/stagnationlab/sk/g/b$a;)V
    .locals 0

    .line 32
    iput-object p1, p0, Lcom/stagnationlab/sk/g/b;->a:Lcom/stagnationlab/sk/g/b$a;

    return-void
.end method
