.class Lcom/stagnationlab/sk/g/a;
.super Ljava/lang/Object;
.source "LoaderAnimator.java"


# instance fields
.field private a:Landroid/view/View;

.field private b:Z

.field private c:Landroid/view/animation/TranslateAnimation;


# direct methods
.method constructor <init>(Landroid/view/View;)V
    .locals 10

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 9
    iput-boolean v0, p0, Lcom/stagnationlab/sk/g/a;->b:Z

    .line 13
    iput-object p1, p0, Lcom/stagnationlab/sk/g/a;->a:Landroid/view/View;

    .line 15
    new-instance v0, Landroid/view/animation/TranslateAnimation;

    const/4 v2, 0x1

    const/high16 v3, -0x40800000    # -1.0f

    const/4 v4, 0x2

    const/high16 v5, 0x3f800000    # 1.0f

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object v1, v0

    invoke-direct/range {v1 .. v9}, Landroid/view/animation/TranslateAnimation;-><init>(IFIFIFIF)V

    iput-object v0, p0, Lcom/stagnationlab/sk/g/a;->c:Landroid/view/animation/TranslateAnimation;

    .line 22
    iget-object v0, p0, Lcom/stagnationlab/sk/g/a;->c:Landroid/view/animation/TranslateAnimation;

    const-wide/16 v1, 0x3e8

    invoke-virtual {v0, v1, v2}, Landroid/view/animation/TranslateAnimation;->setDuration(J)V

    .line 23
    iget-object v0, p0, Lcom/stagnationlab/sk/g/a;->c:Landroid/view/animation/TranslateAnimation;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/animation/TranslateAnimation;->setFillAfter(Z)V

    .line 24
    iget-object v0, p0, Lcom/stagnationlab/sk/g/a;->c:Landroid/view/animation/TranslateAnimation;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/view/animation/TranslateAnimation;->setRepeatCount(I)V

    .line 25
    iget-object v0, p0, Lcom/stagnationlab/sk/g/a;->c:Landroid/view/animation/TranslateAnimation;

    invoke-virtual {v0, v1}, Landroid/view/animation/TranslateAnimation;->setRepeatMode(I)V

    const/4 v0, 0x4

    .line 26
    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method


# virtual methods
.method a()V
    .locals 2

    .line 30
    iget-boolean v0, p0, Lcom/stagnationlab/sk/g/a;->b:Z

    if-eqz v0, :cond_0

    return-void

    .line 34
    :cond_0
    iget-object v0, p0, Lcom/stagnationlab/sk/g/a;->a:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 35
    iget-object v0, p0, Lcom/stagnationlab/sk/g/a;->c:Landroid/view/animation/TranslateAnimation;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/view/animation/TranslateAnimation;->setRepeatCount(I)V

    .line 37
    iget-object v0, p0, Lcom/stagnationlab/sk/g/a;->a:Landroid/view/View;

    iget-object v1, p0, Lcom/stagnationlab/sk/g/a;->c:Landroid/view/animation/TranslateAnimation;

    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    const/4 v0, 0x1

    .line 38
    iput-boolean v0, p0, Lcom/stagnationlab/sk/g/a;->b:Z

    return-void
.end method

.method b()V
    .locals 2

    .line 42
    iget-object v0, p0, Lcom/stagnationlab/sk/g/a;->c:Landroid/view/animation/TranslateAnimation;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/animation/TranslateAnimation;->setRepeatCount(I)V

    .line 43
    iput-boolean v1, p0, Lcom/stagnationlab/sk/g/a;->b:Z

    return-void
.end method
