.class Lcom/stagnationlab/sk/g/c$2;
.super Ljava/lang/Object;
.source "ScrollIndicator.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/stagnationlab/sk/g/c;->a(Lcom/stagnationlab/sk/g/b;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/stagnationlab/sk/g/b;

.field final synthetic b:Lcom/stagnationlab/sk/g/c;


# direct methods
.method constructor <init>(Lcom/stagnationlab/sk/g/c;Lcom/stagnationlab/sk/g/b;)V
    .locals 0

    .line 49
    iput-object p1, p0, Lcom/stagnationlab/sk/g/c$2;->b:Lcom/stagnationlab/sk/g/c;

    iput-object p2, p0, Lcom/stagnationlab/sk/g/c$2;->a:Lcom/stagnationlab/sk/g/b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 10

    .line 52
    iget-object v0, p0, Lcom/stagnationlab/sk/g/c$2;->b:Lcom/stagnationlab/sk/g/c;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/stagnationlab/sk/g/c;->a(Lcom/stagnationlab/sk/g/c;Z)Z

    .line 54
    iget-object v0, p0, Lcom/stagnationlab/sk/g/c$2;->a:Lcom/stagnationlab/sk/g/b;

    invoke-virtual {v0}, Lcom/stagnationlab/sk/g/b;->getScrollHeight()I

    move-result v0

    iget-object v2, p0, Lcom/stagnationlab/sk/g/c$2;->a:Lcom/stagnationlab/sk/g/b;

    invoke-virtual {v2}, Lcom/stagnationlab/sk/g/b;->getHeight()I

    move-result v2

    sub-int/2addr v0, v2

    invoke-static {v0}, Lcom/stagnationlab/sk/i;->b(I)I

    move-result v0

    const/16 v2, 0x14

    if-ge v0, v2, :cond_0

    return-void

    .line 60
    :cond_0
    iget-object v0, p0, Lcom/stagnationlab/sk/g/c$2;->b:Lcom/stagnationlab/sk/g/c;

    const/4 v2, 0x1

    invoke-static {v0, v2}, Lcom/stagnationlab/sk/g/c;->b(Lcom/stagnationlab/sk/g/c;Z)Z

    .line 62
    iget-object v0, p0, Lcom/stagnationlab/sk/g/c$2;->b:Lcom/stagnationlab/sk/g/c;

    invoke-static {v0}, Lcom/stagnationlab/sk/g/c;->a(Lcom/stagnationlab/sk/g/c;)Landroid/view/View;

    move-result-object v0

    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Landroid/view/View;->setAlpha(F)V

    .line 63
    iget-object v0, p0, Lcom/stagnationlab/sk/g/c$2;->b:Lcom/stagnationlab/sk/g/c;

    invoke-static {v0}, Lcom/stagnationlab/sk/g/c;->a(Lcom/stagnationlab/sk/g/c;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 65
    iget-object v0, p0, Lcom/stagnationlab/sk/g/c$2;->b:Lcom/stagnationlab/sk/g/c;

    invoke-static {v0}, Lcom/stagnationlab/sk/g/c;->a(Lcom/stagnationlab/sk/g/c;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v4, 0x3e8

    invoke-virtual {v0, v4, v5}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/high16 v4, 0x3f800000    # 1.0f

    invoke-virtual {v0, v4}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    .line 67
    sget-object v0, Landroid/view/View;->TRANSLATION_Y:Landroid/util/Property;

    const/4 v5, 0x7

    new-array v6, v5, [Landroid/animation/Keyframe;

    .line 69
    invoke-static {v3, v3}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v7

    aput-object v7, v6, v1

    const v7, 0x3e4ccccd    # 0.2f

    .line 70
    invoke-static {v7, v3}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v7

    aput-object v7, v6, v2

    const/4 v7, 0x2

    const v8, 0x3ecccccd    # 0.4f

    const/16 v9, 0xe

    .line 71
    invoke-static {v9}, Lcom/stagnationlab/sk/i;->a(I)I

    move-result v9

    int-to-float v9, v9

    invoke-static {v8, v9}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x3

    const/high16 v8, 0x3f000000    # 0.5f

    .line 72
    invoke-static {v8, v3}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x4

    const v8, 0x3f19999a    # 0.6f

    .line 73
    invoke-static {v5}, Lcom/stagnationlab/sk/i;->a(I)I

    move-result v5

    int-to-float v5, v5

    invoke-static {v8, v5}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v5

    aput-object v5, v6, v7

    const/4 v5, 0x5

    const v7, 0x3f4ccccd    # 0.8f

    .line 74
    invoke-static {v7, v3}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v7

    aput-object v7, v6, v5

    const/4 v5, 0x6

    .line 75
    invoke-static {v4, v3}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v3

    aput-object v3, v6, v5

    .line 67
    invoke-static {v0, v6}, Landroid/animation/PropertyValuesHolder;->ofKeyframe(Landroid/util/Property;[Landroid/animation/Keyframe;)Landroid/animation/PropertyValuesHolder;

    move-result-object v0

    .line 77
    iget-object v3, p0, Lcom/stagnationlab/sk/g/c$2;->b:Lcom/stagnationlab/sk/g/c;

    invoke-static {v3}, Lcom/stagnationlab/sk/g/c;->b(Lcom/stagnationlab/sk/g/c;)Landroid/view/View;

    move-result-object v3

    new-array v2, v2, [Landroid/animation/PropertyValuesHolder;

    aput-object v0, v2, v1

    invoke-static {v3, v2}, Landroid/animation/ObjectAnimator;->ofPropertyValuesHolder(Ljava/lang/Object;[Landroid/animation/PropertyValuesHolder;)Landroid/animation/ObjectAnimator;

    move-result-object v0

    const-wide/16 v1, 0x7d0

    .line 78
    invoke-virtual {v0, v1, v2}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 79
    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    return-void
.end method
