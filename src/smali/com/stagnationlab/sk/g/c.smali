.class Lcom/stagnationlab/sk/g/c;
.super Ljava/lang/Object;
.source "ScrollIndicator.java"


# instance fields
.field private a:Landroid/os/Handler;

.field private b:Landroid/view/View;

.field private c:Landroid/view/View;

.field private d:Z

.field private e:Z


# direct methods
.method constructor <init>(Landroid/view/View;Landroid/view/View$OnClickListener;)V
    .locals 2

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object p1, p0, Lcom/stagnationlab/sk/g/c;->b:Landroid/view/View;

    const v0, 0x7f08009f

    .line 22
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/stagnationlab/sk/g/c;->c:Landroid/view/View;

    .line 23
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/stagnationlab/sk/g/c;->a:Landroid/os/Handler;

    const/4 v0, 0x0

    .line 25
    iput-boolean v0, p0, Lcom/stagnationlab/sk/g/c;->d:Z

    .line 26
    iput-boolean v0, p0, Lcom/stagnationlab/sk/g/c;->e:Z

    .line 28
    iget-object v1, p0, Lcom/stagnationlab/sk/g/c;->b:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setEnabled(Z)V

    .line 29
    iget-object v1, p0, Lcom/stagnationlab/sk/g/c;->b:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setClickable(Z)V

    const v0, 0x7f0800a0

    .line 31
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    new-instance v0, Lcom/stagnationlab/sk/g/c$1;

    invoke-direct {v0, p0, p2}, Lcom/stagnationlab/sk/g/c$1;-><init>(Lcom/stagnationlab/sk/g/c;Landroid/view/View$OnClickListener;)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method static synthetic a(Lcom/stagnationlab/sk/g/c;)Landroid/view/View;
    .locals 0

    .line 12
    iget-object p0, p0, Lcom/stagnationlab/sk/g/c;->b:Landroid/view/View;

    return-object p0
.end method

.method static synthetic a(Lcom/stagnationlab/sk/g/c;Z)Z
    .locals 0

    .line 12
    iput-boolean p1, p0, Lcom/stagnationlab/sk/g/c;->e:Z

    return p1
.end method

.method static synthetic b(Lcom/stagnationlab/sk/g/c;)Landroid/view/View;
    .locals 0

    .line 12
    iget-object p0, p0, Lcom/stagnationlab/sk/g/c;->c:Landroid/view/View;

    return-object p0
.end method

.method static synthetic b(Lcom/stagnationlab/sk/g/c;Z)Z
    .locals 0

    .line 12
    iput-boolean p1, p0, Lcom/stagnationlab/sk/g/c;->d:Z

    return p1
.end method


# virtual methods
.method a()V
    .locals 4

    .line 85
    iget-boolean v0, p0, Lcom/stagnationlab/sk/g/c;->d:Z

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 86
    iget-object v0, p0, Lcom/stagnationlab/sk/g/c;->b:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setStartDelay(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v2, 0x12c

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    new-instance v2, Lcom/stagnationlab/sk/g/c$3;

    invoke-direct {v2, p0}, Lcom/stagnationlab/sk/g/c$3;-><init>(Lcom/stagnationlab/sk/g/c;)V

    invoke-virtual {v0, v2}, Landroid/view/ViewPropertyAnimator;->withEndAction(Ljava/lang/Runnable;)Landroid/view/ViewPropertyAnimator;

    .line 93
    iput-boolean v1, p0, Lcom/stagnationlab/sk/g/c;->d:Z

    .line 96
    :cond_0
    iget-boolean v0, p0, Lcom/stagnationlab/sk/g/c;->e:Z

    if-eqz v0, :cond_1

    .line 97
    iget-object v0, p0, Lcom/stagnationlab/sk/g/c;->a:Landroid/os/Handler;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 98
    iput-boolean v1, p0, Lcom/stagnationlab/sk/g/c;->e:Z

    :cond_1
    return-void
.end method

.method a(Lcom/stagnationlab/sk/g/b;)V
    .locals 4

    .line 42
    iget-boolean v0, p0, Lcom/stagnationlab/sk/g/c;->d:Z

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/stagnationlab/sk/g/c;->e:Z

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x1

    .line 46
    iput-boolean v0, p0, Lcom/stagnationlab/sk/g/c;->e:Z

    .line 47
    iget-object v0, p0, Lcom/stagnationlab/sk/g/c;->a:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 49
    iget-object v0, p0, Lcom/stagnationlab/sk/g/c;->a:Landroid/os/Handler;

    new-instance v1, Lcom/stagnationlab/sk/g/c$2;

    invoke-direct {v1, p0, p1}, Lcom/stagnationlab/sk/g/c$2;-><init>(Lcom/stagnationlab/sk/g/c;Lcom/stagnationlab/sk/g/b;)V

    const-wide/16 v2, 0xbb8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    :cond_1
    :goto_0
    return-void
.end method
