.class public Lcom/stagnationlab/sk/g/e;
.super Ljava/lang/Object;
.source "SsoManager.java"


# instance fields
.field private a:Landroid/app/Activity;

.field private b:Landroid/view/View;

.field private c:Landroid/view/View;

.field private d:Landroid/widget/LinearLayout;

.field private e:Lcom/stagnationlab/sk/g/f;

.field private f:Lcom/stagnationlab/sk/g/a;

.field private g:Lcom/stagnationlab/sk/g/b;

.field private h:Lcom/stagnationlab/sk/g/c;

.field private i:Z

.field private j:Z

.field private k:Z


# direct methods
.method public constructor <init>(Landroid/app/Activity;Landroid/view/View;)V
    .locals 1

    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 54
    iput-boolean v0, p0, Lcom/stagnationlab/sk/g/e;->i:Z

    .line 55
    iput-boolean v0, p0, Lcom/stagnationlab/sk/g/e;->j:Z

    .line 56
    iput-boolean v0, p0, Lcom/stagnationlab/sk/g/e;->k:Z

    .line 59
    iput-object p1, p0, Lcom/stagnationlab/sk/g/e;->a:Landroid/app/Activity;

    .line 60
    iput-object p2, p0, Lcom/stagnationlab/sk/g/e;->b:Landroid/view/View;

    const p2, 0x7f0800bc

    .line 62
    invoke-virtual {p1, p2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object p2

    iput-object p2, p0, Lcom/stagnationlab/sk/g/e;->c:Landroid/view/View;

    const p2, 0x7f0800be

    .line 63
    invoke-virtual {p1, p2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/LinearLayout;

    iput-object p1, p0, Lcom/stagnationlab/sk/g/e;->d:Landroid/widget/LinearLayout;

    return-void
.end method

.method static synthetic a(Lcom/stagnationlab/sk/g/e;)Lcom/stagnationlab/sk/g/b;
    .locals 0

    .line 38
    iget-object p0, p0, Lcom/stagnationlab/sk/g/e;->g:Lcom/stagnationlab/sk/g/b;

    return-object p0
.end method

.method private a(Ljava/lang/String;)V
    .locals 3
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "SetJavaScriptEnabled"
        }
    .end annotation

    .line 141
    new-instance v0, Lcom/stagnationlab/sk/g/b;

    iget-object v1, p0, Lcom/stagnationlab/sk/g/e;->a:Landroid/app/Activity;

    invoke-direct {v0, v1}, Lcom/stagnationlab/sk/g/b;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/stagnationlab/sk/g/e;->g:Lcom/stagnationlab/sk/g/b;

    .line 142
    iget-object v0, p0, Lcom/stagnationlab/sk/g/e;->g:Lcom/stagnationlab/sk/g/b;

    new-instance v1, Landroid/view/ViewGroup$LayoutParams;

    const/4 v2, -0x1

    invoke-direct {v1, v2, v2}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Lcom/stagnationlab/sk/g/b;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 145
    iget-object v0, p0, Lcom/stagnationlab/sk/g/e;->g:Lcom/stagnationlab/sk/g/b;

    new-instance v1, Lcom/stagnationlab/sk/g/e$2;

    invoke-direct {v1, p0}, Lcom/stagnationlab/sk/g/e$2;-><init>(Lcom/stagnationlab/sk/g/e;)V

    invoke-virtual {v0, v1}, Lcom/stagnationlab/sk/g/b;->setOnScrollChangeListener(Lcom/stagnationlab/sk/g/b$a;)V

    .line 160
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 163
    iget-object v1, p0, Lcom/stagnationlab/sk/g/e;->g:Lcom/stagnationlab/sk/g/b;

    invoke-virtual {v1}, Lcom/stagnationlab/sk/g/b;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v1

    const/4 v2, 0x1

    .line 164
    invoke-virtual {v1, v2}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    const/4 v2, 0x0

    .line 169
    invoke-virtual {v1, v2}, Landroid/webkit/WebSettings;->setAllowFileAccess(Z)V

    .line 171
    invoke-virtual {v1, v2}, Landroid/webkit/WebSettings;->setAllowFileAccessFromFileURLs(Z)V

    .line 173
    invoke-virtual {v1, v2}, Landroid/webkit/WebSettings;->setAllowUniversalAccessFromFileURLs(Z)V

    .line 175
    invoke-virtual {v1, v2}, Landroid/webkit/WebSettings;->setAllowContentAccess(Z)V

    .line 177
    iget-object v1, p0, Lcom/stagnationlab/sk/g/e;->g:Lcom/stagnationlab/sk/g/b;

    new-instance v2, Lcom/stagnationlab/sk/g/e$3;

    invoke-direct {v2, p0, v0}, Lcom/stagnationlab/sk/g/e$3;-><init>(Lcom/stagnationlab/sk/g/e;Ljava/util/List;)V

    invoke-virtual {v1, v2}, Lcom/stagnationlab/sk/g/b;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 268
    iget-object v0, p0, Lcom/stagnationlab/sk/g/e;->g:Lcom/stagnationlab/sk/g/b;

    new-instance v1, Lcom/stagnationlab/sk/g/e$4;

    invoke-direct {v1, p0}, Lcom/stagnationlab/sk/g/e$4;-><init>(Lcom/stagnationlab/sk/g/e;)V

    invoke-virtual {v0, v1}, Lcom/stagnationlab/sk/g/b;->setWebChromeClient(Landroid/webkit/WebChromeClient;)V

    .line 275
    iget-object v0, p0, Lcom/stagnationlab/sk/g/e;->d:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/stagnationlab/sk/g/e;->g:Lcom/stagnationlab/sk/g/b;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 276
    iget-object v0, p0, Lcom/stagnationlab/sk/g/e;->g:Lcom/stagnationlab/sk/g/b;

    invoke-static {p1}, Lcom/stagnationlab/sk/util/c;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/stagnationlab/sk/g/b;->loadUrl(Ljava/lang/String;)V

    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "ObsoleteSdkInt"
        }
    .end annotation

    .line 318
    invoke-direct {p0}, Lcom/stagnationlab/sk/g/e;->g()V

    .line 319
    iget-object v0, p0, Lcom/stagnationlab/sk/g/e;->b:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 321
    iget-object v0, p0, Lcom/stagnationlab/sk/g/e;->h:Lcom/stagnationlab/sk/g/c;

    if-eqz v0, :cond_0

    .line 322
    invoke-virtual {v0}, Lcom/stagnationlab/sk/g/c;->a()V

    .line 325
    :cond_0
    iget-object v0, p0, Lcom/stagnationlab/sk/g/e;->e:Lcom/stagnationlab/sk/g/f;

    if-eqz v0, :cond_2

    if-eqz p2, :cond_1

    .line 327
    invoke-interface {v0, p2, p3}, Lcom/stagnationlab/sk/g/f;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 329
    :cond_1
    invoke-interface {v0, p1}, Lcom/stagnationlab/sk/g/f;->a(Ljava/lang/String;)V

    :goto_0
    const/4 p1, 0x0

    .line 332
    iput-object p1, p0, Lcom/stagnationlab/sk/g/e;->e:Lcom/stagnationlab/sk/g/f;

    .line 335
    :cond_2
    iput-boolean v1, p0, Lcom/stagnationlab/sk/g/e;->k:Z

    .line 337
    iget-boolean p1, p0, Lcom/stagnationlab/sk/g/e;->j:Z

    if-nez p1, :cond_3

    .line 339
    invoke-virtual {p0}, Lcom/stagnationlab/sk/g/e;->e()V

    return-void

    .line 346
    :cond_3
    new-instance p1, Lcom/stagnationlab/sk/util/n;

    iget-object p3, p0, Lcom/stagnationlab/sk/g/e;->a:Landroid/app/Activity;

    iget-object v0, p0, Lcom/stagnationlab/sk/g/e;->c:Landroid/view/View;

    invoke-direct {p1, p3, v0}, Lcom/stagnationlab/sk/util/n;-><init>(Landroid/content/Context;Landroid/view/View;)V

    if-nez p2, :cond_4

    const/16 p2, 0xc8

    .line 351
    invoke-virtual {p1}, Lcom/stagnationlab/sk/util/n;->b()Lcom/stagnationlab/sk/util/n;

    move-result-object p1

    invoke-virtual {p1, p2}, Lcom/stagnationlab/sk/util/n;->a(I)Lcom/stagnationlab/sk/util/n;

    move-result-object p1

    .line 352
    new-instance p3, Lcom/stagnationlab/sk/util/n;

    iget-object v0, p0, Lcom/stagnationlab/sk/g/e;->a:Landroid/app/Activity;

    iget-object v1, p0, Lcom/stagnationlab/sk/g/e;->b:Landroid/view/View;

    invoke-direct {p3, v0, v1}, Lcom/stagnationlab/sk/util/n;-><init>(Landroid/content/Context;Landroid/view/View;)V

    invoke-virtual {p3}, Lcom/stagnationlab/sk/util/n;->e()Lcom/stagnationlab/sk/util/n;

    move-result-object p3

    invoke-virtual {p3, p2}, Lcom/stagnationlab/sk/util/n;->a(I)Lcom/stagnationlab/sk/util/n;

    move-result-object p2

    goto :goto_1

    .line 355
    :cond_4
    iget-object p2, p0, Lcom/stagnationlab/sk/g/e;->c:Landroid/view/View;

    invoke-virtual {p2}, Landroid/view/View;->clearAnimation()V

    .line 356
    invoke-virtual {p1}, Lcom/stagnationlab/sk/util/n;->c()Lcom/stagnationlab/sk/util/n;

    move-result-object p1

    .line 357
    new-instance p2, Lcom/stagnationlab/sk/util/n;

    iget-object p3, p0, Lcom/stagnationlab/sk/g/e;->a:Landroid/app/Activity;

    iget-object v0, p0, Lcom/stagnationlab/sk/g/e;->b:Landroid/view/View;

    invoke-direct {p2, p3, v0}, Lcom/stagnationlab/sk/util/n;-><init>(Landroid/content/Context;Landroid/view/View;)V

    invoke-virtual {p2}, Lcom/stagnationlab/sk/util/n;->d()Lcom/stagnationlab/sk/util/n;

    move-result-object p2

    .line 360
    :goto_1
    new-instance p3, Lcom/stagnationlab/sk/g/e$5;

    invoke-direct {p3, p0}, Lcom/stagnationlab/sk/g/e$5;-><init>(Lcom/stagnationlab/sk/g/e;)V

    invoke-virtual {p1, p3}, Lcom/stagnationlab/sk/util/n;->a(Lcom/stagnationlab/sk/e/b;)Lcom/stagnationlab/sk/util/n;

    move-result-object p1

    .line 365
    invoke-virtual {p1}, Lcom/stagnationlab/sk/util/n;->a()V

    if-eqz p2, :cond_5

    .line 368
    sget p1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 p3, 0x15

    if-lt p1, p3, :cond_5

    .line 369
    invoke-virtual {p2}, Lcom/stagnationlab/sk/util/n;->a()V

    :cond_5
    return-void
.end method

.method private a(Landroid/net/Uri;)Z
    .locals 4

    .line 284
    invoke-virtual {p1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v0

    const-string v1, "sid-registration"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 289
    invoke-virtual {p1}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v0

    const-string v1, "success"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    const-string v0, "identityToken"

    .line 290
    invoke-virtual {p1, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    move-object v0, v1

    goto :goto_0

    :cond_0
    const-string v0, "errorCode"

    .line 292
    invoke-virtual {p1, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v2, "id"

    .line 293
    invoke-virtual {p1, v2}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    move-object v3, v1

    move-object v1, p1

    move-object p1, v3

    .line 296
    :goto_0
    invoke-direct {p0, p1, v0, v1}, Lcom/stagnationlab/sk/g/e;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    const/4 p1, 0x1

    return p1

    :cond_1
    const/4 p1, 0x0

    return p1
.end method

.method static synthetic a(Lcom/stagnationlab/sk/g/e;Landroid/net/Uri;)Z
    .locals 0

    .line 38
    invoke-direct {p0, p1}, Lcom/stagnationlab/sk/g/e;->a(Landroid/net/Uri;)Z

    move-result p0

    return p0
.end method

.method static synthetic a(Lcom/stagnationlab/sk/g/e;Z)Z
    .locals 0

    .line 38
    iput-boolean p1, p0, Lcom/stagnationlab/sk/g/e;->j:Z

    return p1
.end method

.method static synthetic b(Lcom/stagnationlab/sk/g/e;)Lcom/stagnationlab/sk/g/c;
    .locals 0

    .line 38
    iget-object p0, p0, Lcom/stagnationlab/sk/g/e;->h:Lcom/stagnationlab/sk/g/c;

    return-object p0
.end method

.method static synthetic c(Lcom/stagnationlab/sk/g/e;)V
    .locals 0

    .line 38
    invoke-direct {p0}, Lcom/stagnationlab/sk/g/e;->f()V

    return-void
.end method

.method static synthetic d(Lcom/stagnationlab/sk/g/e;)Landroid/app/Activity;
    .locals 0

    .line 38
    iget-object p0, p0, Lcom/stagnationlab/sk/g/e;->a:Landroid/app/Activity;

    return-object p0
.end method

.method static synthetic e(Lcom/stagnationlab/sk/g/e;)Lcom/stagnationlab/sk/g/a;
    .locals 0

    .line 38
    iget-object p0, p0, Lcom/stagnationlab/sk/g/e;->f:Lcom/stagnationlab/sk/g/a;

    return-object p0
.end method

.method private f()V
    .locals 2

    const/4 v0, 0x0

    const-string v1, "NETWORK_FAILURE"

    .line 280
    invoke-direct {p0, v0, v1, v0}, Lcom/stagnationlab/sk/g/e;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic f(Lcom/stagnationlab/sk/g/e;)Z
    .locals 0

    .line 38
    iget-boolean p0, p0, Lcom/stagnationlab/sk/g/e;->j:Z

    return p0
.end method

.method static synthetic g(Lcom/stagnationlab/sk/g/e;)Landroid/view/View;
    .locals 0

    .line 38
    iget-object p0, p0, Lcom/stagnationlab/sk/g/e;->c:Landroid/view/View;

    return-object p0
.end method

.method private g()V
    .locals 3

    .line 305
    iget-object v0, p0, Lcom/stagnationlab/sk/g/e;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getCurrentFocus()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 308
    iget-object v1, p0, Lcom/stagnationlab/sk/g/e;->a:Landroid/app/Activity;

    const-string v2, "input_method"

    invoke-virtual {v1, v2}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/inputmethod/InputMethodManager;

    if-eqz v1, :cond_0

    .line 311
    invoke-virtual {v0}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    :cond_0
    return-void
.end method

.method static synthetic h(Lcom/stagnationlab/sk/g/e;)Landroid/view/View;
    .locals 0

    .line 38
    iget-object p0, p0, Lcom/stagnationlab/sk/g/e;->b:Landroid/view/View;

    return-object p0
.end method


# virtual methods
.method public a()V
    .locals 1

    const/4 v0, 0x0

    .line 93
    invoke-virtual {p0, v0}, Lcom/stagnationlab/sk/g/e;->a(Z)V

    return-void
.end method

.method public a(Ljava/lang/String;Lcom/stagnationlab/sk/g/f;)V
    .locals 4

    .line 67
    iget-boolean v0, p0, Lcom/stagnationlab/sk/g/e;->i:Z

    const/4 v1, 0x1

    if-nez v0, :cond_0

    .line 68
    new-instance v0, Lcom/stagnationlab/sk/g/c;

    iget-object v2, p0, Lcom/stagnationlab/sk/g/e;->a:Landroid/app/Activity;

    const v3, 0x7f0800a3

    .line 69
    invoke-virtual {v2, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    new-instance v3, Lcom/stagnationlab/sk/g/e$1;

    invoke-direct {v3, p0}, Lcom/stagnationlab/sk/g/e$1;-><init>(Lcom/stagnationlab/sk/g/e;)V

    invoke-direct {v0, v2, v3}, Lcom/stagnationlab/sk/g/c;-><init>(Landroid/view/View;Landroid/view/View$OnClickListener;)V

    iput-object v0, p0, Lcom/stagnationlab/sk/g/e;->h:Lcom/stagnationlab/sk/g/c;

    .line 80
    iput-boolean v1, p0, Lcom/stagnationlab/sk/g/e;->i:Z

    .line 83
    :cond_0
    new-instance v0, Lcom/stagnationlab/sk/g/a;

    iget-object v2, p0, Lcom/stagnationlab/sk/g/e;->a:Landroid/app/Activity;

    const v3, 0x7f080075

    invoke-virtual {v2, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/stagnationlab/sk/g/a;-><init>(Landroid/view/View;)V

    iput-object v0, p0, Lcom/stagnationlab/sk/g/e;->f:Lcom/stagnationlab/sk/g/a;

    .line 84
    iput-object p2, p0, Lcom/stagnationlab/sk/g/e;->e:Lcom/stagnationlab/sk/g/f;

    .line 86
    iput-boolean v1, p0, Lcom/stagnationlab/sk/g/e;->k:Z

    .line 88
    invoke-direct {p0}, Lcom/stagnationlab/sk/g/e;->g()V

    .line 89
    invoke-direct {p0, p1}, Lcom/stagnationlab/sk/g/e;->a(Ljava/lang/String;)V

    return-void
.end method

.method public a(Z)V
    .locals 1

    if-eqz p1, :cond_0

    const/4 p1, 0x0

    .line 99
    iput-boolean p1, p0, Lcom/stagnationlab/sk/g/e;->j:Z

    :cond_0
    const/4 p1, 0x0

    const-string v0, "USER_ABORTED"

    .line 102
    invoke-direct {p0, p1, v0, p1}, Lcom/stagnationlab/sk/g/e;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public b()Z
    .locals 1

    .line 106
    iget-boolean v0, p0, Lcom/stagnationlab/sk/g/e;->k:Z

    return v0
.end method

.method public c()V
    .locals 1

    .line 110
    iget-object v0, p0, Lcom/stagnationlab/sk/g/e;->g:Lcom/stagnationlab/sk/g/b;

    if-eqz v0, :cond_0

    .line 111
    invoke-virtual {v0}, Lcom/stagnationlab/sk/g/b;->pauseTimers()V

    :cond_0
    return-void
.end method

.method public d()V
    .locals 1

    .line 116
    iget-object v0, p0, Lcom/stagnationlab/sk/g/e;->g:Lcom/stagnationlab/sk/g/b;

    if-eqz v0, :cond_0

    .line 117
    invoke-virtual {v0}, Lcom/stagnationlab/sk/g/b;->resumeTimers()V

    :cond_0
    return-void
.end method

.method public e()V
    .locals 3

    .line 122
    iget-object v0, p0, Lcom/stagnationlab/sk/g/e;->g:Lcom/stagnationlab/sk/g/b;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 123
    iget-object v2, p0, Lcom/stagnationlab/sk/g/e;->d:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v0}, Landroid/widget/LinearLayout;->removeView(Landroid/view/View;)V

    .line 124
    iget-object v0, p0, Lcom/stagnationlab/sk/g/e;->c:Landroid/view/View;

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 125
    iget-object v0, p0, Lcom/stagnationlab/sk/g/e;->g:Lcom/stagnationlab/sk/g/b;

    invoke-virtual {v0}, Lcom/stagnationlab/sk/g/b;->destroy()V

    .line 126
    iput-object v1, p0, Lcom/stagnationlab/sk/g/e;->g:Lcom/stagnationlab/sk/g/b;

    .line 129
    :cond_0
    iget-object v0, p0, Lcom/stagnationlab/sk/g/e;->f:Lcom/stagnationlab/sk/g/a;

    if-eqz v0, :cond_1

    .line 130
    invoke-virtual {v0}, Lcom/stagnationlab/sk/g/a;->b()V

    .line 131
    iput-object v1, p0, Lcom/stagnationlab/sk/g/e;->f:Lcom/stagnationlab/sk/g/a;

    .line 134
    :cond_1
    iput-object v1, p0, Lcom/stagnationlab/sk/g/e;->e:Lcom/stagnationlab/sk/g/f;

    const/4 v0, 0x0

    .line 135
    iput-boolean v0, p0, Lcom/stagnationlab/sk/g/e;->j:Z

    .line 136
    iput-boolean v0, p0, Lcom/stagnationlab/sk/g/e;->k:Z

    return-void
.end method
