.class public Lcom/stagnationlab/sk/MyApplication;
.super Landroid/app/Application;
.source "MyApplication.java"


# static fields
.field private static c:Ljava/lang/String;


# instance fields
.field private a:Lcom/stagnationlab/sk/a/a;

.field private b:Lcom/stagnationlab/sk/a;

.field private d:Z

.field private e:Z

.field private f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/stagnationlab/sk/e/a;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 22
    invoke-direct {p0}, Landroid/app/Application;-><init>()V

    return-void
.end method

.method public static a()Ljava/lang/String;
    .locals 1

    .line 33
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/stagnationlab/sk/MyApplication;)Ljava/util/List;
    .locals 0

    .line 22
    iget-object p0, p0, Lcom/stagnationlab/sk/MyApplication;->f:Ljava/util/List;

    return-object p0
.end method

.method static synthetic a(Lcom/stagnationlab/sk/MyApplication;Z)Z
    .locals 0

    .line 22
    iput-boolean p1, p0, Lcom/stagnationlab/sk/MyApplication;->d:Z

    return p1
.end method

.method static synthetic b(Lcom/stagnationlab/sk/MyApplication;)Lcom/stagnationlab/sk/a/a;
    .locals 0

    .line 22
    iget-object p0, p0, Lcom/stagnationlab/sk/MyApplication;->a:Lcom/stagnationlab/sk/a/a;

    return-object p0
.end method

.method static synthetic b(Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .line 22
    sput-object p0, Lcom/stagnationlab/sk/MyApplication;->c:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic b(Lcom/stagnationlab/sk/MyApplication;Z)Z
    .locals 0

    .line 22
    iput-boolean p1, p0, Lcom/stagnationlab/sk/MyApplication;->e:Z

    return p1
.end method

.method public static c()Ljava/lang/String;
    .locals 1

    .line 110
    sget-object v0, Lcom/stagnationlab/sk/MyApplication;->c:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic f()Ljava/lang/String;
    .locals 1

    .line 22
    sget-object v0, Lcom/stagnationlab/sk/MyApplication;->c:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public a(Lcom/stagnationlab/sk/a/a/a;)V
    .locals 1

    .line 101
    new-instance v0, Lcom/stagnationlab/sk/MyApplication$2;

    invoke-direct {v0, p0, p1}, Lcom/stagnationlab/sk/MyApplication$2;-><init>(Lcom/stagnationlab/sk/MyApplication;Lcom/stagnationlab/sk/a/a/a;)V

    invoke-virtual {p0, v0}, Lcom/stagnationlab/sk/MyApplication;->a(Lcom/stagnationlab/sk/e/a;)V

    return-void
.end method

.method public a(Lcom/stagnationlab/sk/e/a;)V
    .locals 1

    .line 65
    iget-boolean v0, p0, Lcom/stagnationlab/sk/MyApplication;->d:Z

    if-eqz v0, :cond_0

    .line 66
    invoke-interface {p1}, Lcom/stagnationlab/sk/e/a;->a()V

    return-void

    .line 71
    :cond_0
    iget-object v0, p0, Lcom/stagnationlab/sk/MyApplication;->f:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 73
    iget-boolean p1, p0, Lcom/stagnationlab/sk/MyApplication;->e:Z

    if-eqz p1, :cond_1

    return-void

    :cond_1
    const/4 p1, 0x1

    .line 77
    iput-boolean p1, p0, Lcom/stagnationlab/sk/MyApplication;->e:Z

    .line 79
    new-instance p1, Lcom/stagnationlab/sk/MyApplication$1;

    invoke-direct {p1, p0}, Lcom/stagnationlab/sk/MyApplication$1;-><init>(Lcom/stagnationlab/sk/MyApplication;)V

    invoke-static {p0, p1}, Lcom/stagnationlab/sk/a/d;->a(Landroid/content/Context;Lcom/stagnationlab/sk/e/a;)V

    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 0

    .line 114
    sput-object p1, Lcom/stagnationlab/sk/MyApplication;->c:Ljava/lang/String;

    .line 115
    invoke-static {p1}, Lcom/stagnationlab/sk/a/d;->a(Ljava/lang/String;)V

    return-void
.end method

.method public b()Lcom/stagnationlab/sk/a/a;
    .locals 3

    .line 56
    iget-object v0, p0, Lcom/stagnationlab/sk/MyApplication;->a:Lcom/stagnationlab/sk/a/a;

    if-nez v0, :cond_0

    const-string v0, "TAG_"

    .line 57
    invoke-static {p0, v0}, Lcom/stagnationlab/sk/a/a;->a(Landroid/content/Context;Ljava/lang/String;)Lcom/stagnationlab/sk/a/a;

    move-result-object v0

    iput-object v0, p0, Lcom/stagnationlab/sk/MyApplication;->a:Lcom/stagnationlab/sk/a/a;

    .line 58
    iget-object v0, p0, Lcom/stagnationlab/sk/MyApplication;->a:Lcom/stagnationlab/sk/a/a;

    invoke-virtual {v0}, Lcom/stagnationlab/sk/a/a;->c()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/stagnationlab/sk/MyApplication;->a:Lcom/stagnationlab/sk/a/a;

    invoke-virtual {v1}, Lcom/stagnationlab/sk/a/a;->d()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/stagnationlab/sk/MyApplication;->a:Lcom/stagnationlab/sk/a/a;

    invoke-virtual {v2}, Lcom/stagnationlab/sk/a/a;->e()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/stagnationlab/sk/util/b;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 61
    :cond_0
    iget-object v0, p0, Lcom/stagnationlab/sk/MyApplication;->a:Lcom/stagnationlab/sk/a/a;

    return-object v0
.end method

.method public d()Lcom/stagnationlab/sk/a/a;
    .locals 1

    .line 119
    iget-object v0, p0, Lcom/stagnationlab/sk/MyApplication;->a:Lcom/stagnationlab/sk/a/a;

    return-object v0
.end method

.method public declared-synchronized e()Lcom/stagnationlab/sk/a;
    .locals 1

    monitor-enter p0

    .line 128
    :try_start_0
    iget-object v0, p0, Lcom/stagnationlab/sk/MyApplication;->b:Lcom/stagnationlab/sk/a;

    if-nez v0, :cond_0

    .line 129
    new-instance v0, Lcom/stagnationlab/sk/a;

    invoke-direct {v0, p0}, Lcom/stagnationlab/sk/a;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/stagnationlab/sk/MyApplication;->b:Lcom/stagnationlab/sk/a;

    .line 132
    :cond_0
    iget-object v0, p0, Lcom/stagnationlab/sk/MyApplication;->b:Lcom/stagnationlab/sk/a;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public onCreate()V
    .locals 2

    .line 38
    invoke-super {p0}, Landroid/app/Application;->onCreate()V

    .line 44
    sget-object v0, Lcom/stagnationlab/sk/f$a;->a:Lcom/stagnationlab/sk/f$a;

    invoke-static {v0}, Lcom/stagnationlab/sk/f;->a(Lcom/stagnationlab/sk/f$a;)V

    .line 47
    invoke-static {p0}, Lcom/stagnationlab/sk/e;->a(Landroid/content/Context;)V

    const-string v0, "MyApplication"

    const-string v1, "process started"

    .line 48
    invoke-static {v0, v1}, Lcom/stagnationlab/sk/f;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 49
    invoke-static {}, Lcom/stagnationlab/sk/MyApplication;->a()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/stagnationlab/sk/MyApplication;->c:Ljava/lang/String;

    const/4 v0, 0x0

    .line 50
    iput-boolean v0, p0, Lcom/stagnationlab/sk/MyApplication;->d:Z

    .line 51
    iput-boolean v0, p0, Lcom/stagnationlab/sk/MyApplication;->e:Z

    .line 52
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/stagnationlab/sk/MyApplication;->f:Ljava/util/List;

    return-void
.end method
