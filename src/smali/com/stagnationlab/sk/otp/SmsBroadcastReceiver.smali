.class public Lcom/stagnationlab/sk/otp/SmsBroadcastReceiver;
.super Landroid/content/BroadcastReceiver;
.source "SmsBroadcastReceiver.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 19
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method private a(Lcom/stagnationlab/sk/h/d;Lcom/stagnationlab/sk/c/b;)V
    .locals 1

    .line 93
    new-instance v0, Lcom/stagnationlab/sk/c/a;

    invoke-direct {v0, p2}, Lcom/stagnationlab/sk/c/a;-><init>(Lcom/stagnationlab/sk/c/b;)V

    const/4 p2, 0x0

    invoke-virtual {p1, v0, p2}, Lcom/stagnationlab/sk/h/d;->a(Lcom/stagnationlab/sk/c/a;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4

    .line 24
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object p1

    const-string v0, "com.google.android.gms.auth.api.phone.SMS_RETRIEVED"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_0

    return-void

    .line 28
    :cond_0
    invoke-static {}, Lcom/stagnationlab/sk/fcm/a;->a()Landroid/app/Activity;

    move-result-object p1

    .line 31
    instance-of v1, p1, Lcom/stagnationlab/sk/MainActivity;

    const/4 v2, 0x0

    if-eqz v1, :cond_1

    .line 32
    check-cast p1, Lcom/stagnationlab/sk/MainActivity;

    invoke-virtual {p1}, Lcom/stagnationlab/sk/MainActivity;->l()Lcom/stagnationlab/sk/h/d;

    move-result-object p1

    goto :goto_0

    :cond_1
    move-object p1, v2

    :goto_0
    const-string v1, "SmsBroadcastReceiver"

    if-nez p1, :cond_2

    const-string p1, "No active bridge to send event"

    .line 36
    invoke-static {v1, p1}, Lcom/stagnationlab/sk/f;->e(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    .line 41
    :cond_2
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 42
    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object p2

    if-nez p2, :cond_3

    const-string p2, "Missing extras"

    .line 45
    invoke-static {v1, p2}, Lcom/stagnationlab/sk/f;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 46
    sget-object p2, Lcom/stagnationlab/sk/c/b;->B:Lcom/stagnationlab/sk/c/b;

    invoke-direct {p0, p1, p2}, Lcom/stagnationlab/sk/otp/SmsBroadcastReceiver;->a(Lcom/stagnationlab/sk/h/d;Lcom/stagnationlab/sk/c/b;)V

    return-void

    :cond_3
    const-string v0, "com.google.android.gms.auth.api.phone.EXTRA_STATUS"

    .line 51
    invoke-virtual {p2, v0}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/api/Status;

    if-nez v0, :cond_4

    const-string p2, "Missing status"

    .line 54
    invoke-static {v1, p2}, Lcom/stagnationlab/sk/f;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 55
    sget-object p2, Lcom/stagnationlab/sk/c/b;->B:Lcom/stagnationlab/sk/c/b;

    invoke-direct {p0, p1, p2}, Lcom/stagnationlab/sk/otp/SmsBroadcastReceiver;->a(Lcom/stagnationlab/sk/h/d;Lcom/stagnationlab/sk/c/b;)V

    return-void

    .line 60
    :cond_4
    invoke-virtual {v0}, Lcom/google/android/gms/common/api/Status;->e()I

    move-result v3

    if-eqz v3, :cond_6

    .line 63
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Failed to receive SMS. Code: "

    invoke-virtual {p2, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 65
    invoke-virtual {v0}, Lcom/google/android/gms/common/api/Status;->e()I

    move-result v2

    invoke-virtual {p2, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v2, "; message: "

    invoke-virtual {p2, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 66
    invoke-virtual {v0}, Lcom/google/android/gms/common/api/Status;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    .line 63
    invoke-static {v1, p2}, Lcom/stagnationlab/sk/f;->c(Ljava/lang/String;Ljava/lang/String;)V

    const/16 p2, 0xf

    if-ne v3, p2, :cond_5

    .line 69
    sget-object p2, Lcom/stagnationlab/sk/c/b;->D:Lcom/stagnationlab/sk/c/b;

    goto :goto_1

    :cond_5
    sget-object p2, Lcom/stagnationlab/sk/c/b;->E:Lcom/stagnationlab/sk/c/b;

    :goto_1
    invoke-direct {p0, p1, p2}, Lcom/stagnationlab/sk/otp/SmsBroadcastReceiver;->a(Lcom/stagnationlab/sk/h/d;Lcom/stagnationlab/sk/c/b;)V

    return-void

    :cond_6
    const-string v0, "com.google.android.gms.auth.api.phone.EXTRA_SMS_MESSAGE"

    .line 77
    invoke-virtual {p2, v0}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Ljava/lang/String;

    if-nez p2, :cond_7

    const-string p2, "Missing SMS message"

    .line 80
    invoke-static {v1, p2}, Lcom/stagnationlab/sk/f;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 81
    sget-object p2, Lcom/stagnationlab/sk/c/b;->B:Lcom/stagnationlab/sk/c/b;

    invoke-direct {p0, p1, p2}, Lcom/stagnationlab/sk/otp/SmsBroadcastReceiver;->a(Lcom/stagnationlab/sk/h/d;Lcom/stagnationlab/sk/c/b;)V

    return-void

    .line 86
    :cond_7
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Received SMS successfully: "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/stagnationlab/sk/f;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 88
    invoke-virtual {p1, v2, p2}, Lcom/stagnationlab/sk/h/d;->a(Lcom/stagnationlab/sk/c/a;Ljava/lang/String;)V

    :cond_8
    return-void
.end method
