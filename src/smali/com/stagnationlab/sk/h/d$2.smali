.class Lcom/stagnationlab/sk/h/d$2;
.super Ljava/lang/Object;
.source "JavascriptBridge.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/stagnationlab/sk/h/d;->a(Ljava/lang/String;Ljava/lang/Object;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:Ljava/lang/Object;

.field final synthetic c:Lcom/stagnationlab/sk/h/d;


# direct methods
.method constructor <init>(Lcom/stagnationlab/sk/h/d;Ljava/lang/String;Ljava/lang/Object;)V
    .locals 0

    .line 297
    iput-object p1, p0, Lcom/stagnationlab/sk/h/d$2;->c:Lcom/stagnationlab/sk/h/d;

    iput-object p2, p0, Lcom/stagnationlab/sk/h/d$2;->a:Ljava/lang/String;

    iput-object p3, p0, Lcom/stagnationlab/sk/h/d$2;->b:Ljava/lang/Object;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .line 299
    iget-object v0, p0, Lcom/stagnationlab/sk/h/d$2;->c:Lcom/stagnationlab/sk/h/d;

    invoke-static {v0}, Lcom/stagnationlab/sk/h/d;->a(Lcom/stagnationlab/sk/h/d;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "--> Sending bridge event \'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/stagnationlab/sk/h/d$2;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/stagnationlab/sk/f;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 301
    new-instance v0, Lcom/stagnationlab/sk/util/f;

    invoke-direct {v0}, Lcom/stagnationlab/sk/util/f;-><init>()V

    iget-object v1, p0, Lcom/stagnationlab/sk/h/d$2;->a:Ljava/lang/String;

    const-string v2, "name"

    .line 302
    invoke-virtual {v0, v2, v1}, Lcom/stagnationlab/sk/util/f;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/stagnationlab/sk/util/f;

    move-result-object v0

    iget-object v1, p0, Lcom/stagnationlab/sk/h/d$2;->b:Ljava/lang/Object;

    const-string v2, "data"

    .line 303
    invoke-virtual {v0, v2, v1}, Lcom/stagnationlab/sk/util/f;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/stagnationlab/sk/util/f;

    move-result-object v0

    .line 305
    iget-object v1, p0, Lcom/stagnationlab/sk/h/d$2;->c:Lcom/stagnationlab/sk/h/d;

    invoke-static {v1}, Lcom/stagnationlab/sk/h/d;->b(Lcom/stagnationlab/sk/h/d;)Lcom/stagnationlab/sk/h/e;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    .line 308
    invoke-virtual {v0}, Lcom/stagnationlab/sk/util/f;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v3, 0x0

    aput-object v0, v2, v3

    const-string v0, "NativeBridge.handleEvent ? NativeBridge.handleEvent(%s) : (NativeBridge.events = NativeBridge.events || [], NativeBridge.events.push(%<s));"

    .line 306
    invoke-static {v0, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x0

    .line 305
    invoke-virtual {v1, v0, v2}, Lcom/stagnationlab/sk/h/e;->a(Ljava/lang/String;Landroid/webkit/ValueCallback;)V

    return-void
.end method
