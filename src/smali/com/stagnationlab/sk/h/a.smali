.class abstract Lcom/stagnationlab/sk/h/a;
.super Ljava/lang/Object;
.source "AbstractWebViewWrapper.java"


# static fields
.field static a:I = 0x12c


# instance fields
.field b:Landroid/widget/LinearLayout;

.field private c:Landroid/content/Context;

.field private d:Z

.field private e:Z

.field private f:Ljava/lang/String;

.field private g:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method constructor <init>(Landroid/content/Context;Landroid/widget/LinearLayout;)V
    .locals 1

    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 25
    iput-boolean v0, p0, Lcom/stagnationlab/sk/h/a;->d:Z

    .line 26
    iput-boolean v0, p0, Lcom/stagnationlab/sk/h/a;->e:Z

    .line 31
    iput-object p1, p0, Lcom/stagnationlab/sk/h/a;->c:Landroid/content/Context;

    .line 32
    iput-object p2, p0, Lcom/stagnationlab/sk/h/a;->b:Landroid/widget/LinearLayout;

    return-void
.end method

.method static synthetic a(Lcom/stagnationlab/sk/h/a;)V
    .locals 0

    .line 18
    invoke-direct {p0}, Lcom/stagnationlab/sk/h/a;->e()V

    return-void
.end method

.method private d()V
    .locals 2

    .line 98
    iget-boolean v0, p0, Lcom/stagnationlab/sk/h/a;->d:Z

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/stagnationlab/sk/h/a;->e:Z

    if-nez v0, :cond_0

    goto :goto_0

    .line 102
    :cond_0
    invoke-virtual {p0}, Lcom/stagnationlab/sk/h/a;->c()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    if-nez v0, :cond_1

    .line 105
    invoke-direct {p0}, Lcom/stagnationlab/sk/h/a;->e()V

    goto :goto_0

    .line 107
    :cond_1
    new-instance v1, Lcom/stagnationlab/sk/h/a$1;

    invoke-direct {v1, p0}, Lcom/stagnationlab/sk/h/a$1;-><init>(Lcom/stagnationlab/sk/h/a;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->withEndAction(Ljava/lang/Runnable;)Landroid/view/ViewPropertyAnimator;

    :cond_2
    :goto_0
    return-void
.end method

.method private e()V
    .locals 2

    .line 118
    iget-object v0, p0, Lcom/stagnationlab/sk/h/a;->c:Landroid/content/Context;

    const v1, 0x7f0f0005

    invoke-virtual {v0, v1}, Landroid/content/Context;->setTheme(I)V

    .line 119
    iget-object v0, p0, Lcom/stagnationlab/sk/h/a;->b:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getRootView()Landroid/view/View;

    move-result-object v0

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    return-void
.end method


# virtual methods
.method a()V
    .locals 1

    .line 67
    iget-boolean v0, p0, Lcom/stagnationlab/sk/h/a;->e:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x1

    .line 71
    iput-boolean v0, p0, Lcom/stagnationlab/sk/h/a;->e:Z

    .line 73
    invoke-direct {p0}, Lcom/stagnationlab/sk/h/a;->d()V

    return-void
.end method

.method a(Ljava/lang/String;)V
    .locals 2

    .line 87
    iget-object v0, p0, Lcom/stagnationlab/sk/h/a;->g:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_0

    return-void

    .line 92
    :cond_0
    new-instance p1, Landroid/content/Intent;

    iget-object v0, p0, Lcom/stagnationlab/sk/h/a;->c:Landroid/content/Context;

    const-class v1, Lcom/stagnationlab/sk/ErrorActivity;

    invoke-direct {p1, v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 93
    new-instance v0, Lcom/stagnationlab/sk/c/a;

    sget-object v1, Lcom/stagnationlab/sk/c/b;->Q:Lcom/stagnationlab/sk/c/b;

    invoke-direct {v0, v1}, Lcom/stagnationlab/sk/c/a;-><init>(Lcom/stagnationlab/sk/c/b;)V

    const-string v1, "error"

    invoke-virtual {p1, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 94
    iget-object v0, p0, Lcom/stagnationlab/sk/h/a;->c:Landroid/content/Context;

    invoke-virtual {v0, p1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method a(Landroid/net/Uri;)Z
    .locals 6

    .line 36
    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/stagnationlab/sk/h/a;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    return v1

    .line 40
    :cond_0
    invoke-virtual {p1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v0

    const-string v2, "smart-id"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    const/4 v2, 0x1

    if-eqz v0, :cond_8

    .line 41
    invoke-virtual {p1}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object p1

    const/4 v0, -0x1

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v3

    const v4, -0x3a8f7794

    const/4 v5, 0x2

    if-eq v3, v4, :cond_3

    const v4, -0x312c713b

    if-eq v3, v4, :cond_2

    const v4, 0x68af8e1

    if-eq v3, v4, :cond_1

    goto :goto_0

    :cond_1
    const-string v3, "store"

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_4

    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    const-string v1, "notification-settings"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_4

    const/4 v0, 0x2

    goto :goto_0

    :cond_3
    const-string v1, "portal"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_4

    const/4 v0, 0x1

    :cond_4
    :goto_0
    if-eqz v0, :cond_7

    if-eq v0, v2, :cond_6

    if-eq v0, v5, :cond_5

    goto :goto_1

    .line 51
    :cond_5
    iget-object p1, p0, Lcom/stagnationlab/sk/h/a;->c:Landroid/content/Context;

    invoke-static {p1}, Lcom/stagnationlab/sk/util/h;->c(Landroid/content/Context;)V

    goto :goto_1

    .line 47
    :cond_6
    iget-object p1, p0, Lcom/stagnationlab/sk/h/a;->c:Landroid/content/Context;

    invoke-static {p1}, Lcom/stagnationlab/sk/util/h;->d(Landroid/content/Context;)V

    goto :goto_1

    .line 43
    :cond_7
    iget-object p1, p0, Lcom/stagnationlab/sk/h/a;->c:Landroid/content/Context;

    invoke-static {p1}, Lcom/stagnationlab/sk/util/h;->b(Landroid/content/Context;)V

    :goto_1
    return v2

    .line 59
    :cond_8
    invoke-static {p1}, Lcom/stagnationlab/sk/util/h;->a(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object p1

    .line 61
    iget-object v0, p0, Lcom/stagnationlab/sk/h/a;->c:Landroid/content/Context;

    invoke-static {v0, p1}, Lcom/stagnationlab/sk/util/h;->a(Landroid/content/Context;Landroid/net/Uri;)V

    return v2
.end method

.method b()V
    .locals 1

    .line 77
    iget-boolean v0, p0, Lcom/stagnationlab/sk/h/a;->d:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x1

    .line 81
    iput-boolean v0, p0, Lcom/stagnationlab/sk/h/a;->d:Z

    .line 83
    invoke-direct {p0}, Lcom/stagnationlab/sk/h/a;->d()V

    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 3

    .line 123
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 125
    iput-object p1, p0, Lcom/stagnationlab/sk/h/a;->g:Ljava/lang/String;

    .line 126
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "://"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "/"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/stagnationlab/sk/h/a;->f:Ljava/lang/String;

    .line 128
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Loading url \""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "AbstractWebViewWrapper"

    invoke-static {v1, v0}, Lcom/stagnationlab/sk/f;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 130
    invoke-virtual {p0, p1}, Lcom/stagnationlab/sk/h/a;->c(Ljava/lang/String;)V

    return-void
.end method

.method abstract c()Landroid/view/ViewPropertyAnimator;
.end method

.method abstract c(Ljava/lang/String;)V
.end method
