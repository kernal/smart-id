.class Lcom/stagnationlab/sk/h/b$15;
.super Ljava/lang/Object;
.source "JavascriptApi.java"

# interfaces
.implements Lcom/stagnationlab/sk/util/d;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/stagnationlab/sk/h/b;->a(Ljava/lang/String;Ljava/lang/String;Lcom/stagnationlab/sk/h/c;)Lcom/stagnationlab/sk/util/d;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:Ljava/lang/String;

.field final synthetic c:Lcom/stagnationlab/sk/h/c;

.field final synthetic d:Lcom/stagnationlab/sk/h/b;


# direct methods
.method constructor <init>(Lcom/stagnationlab/sk/h/b;Ljava/lang/String;Ljava/lang/String;Lcom/stagnationlab/sk/h/c;)V
    .locals 0

    .line 283
    iput-object p1, p0, Lcom/stagnationlab/sk/h/b$15;->d:Lcom/stagnationlab/sk/h/b;

    iput-object p2, p0, Lcom/stagnationlab/sk/h/b$15;->a:Ljava/lang/String;

    iput-object p3, p0, Lcom/stagnationlab/sk/h/b$15;->b:Ljava/lang/String;

    iput-object p4, p0, Lcom/stagnationlab/sk/h/b$15;->c:Lcom/stagnationlab/sk/h/c;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/stagnationlab/sk/util/f;)V
    .locals 2

    const-string v0, "status"

    .line 301
    invoke-virtual {p1, v0}, Lcom/stagnationlab/sk/util/f;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "ERROR"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 302
    sget-object v0, Lcom/stagnationlab/sk/c/b;->y:Lcom/stagnationlab/sk/c/b;

    invoke-virtual {v0}, Lcom/stagnationlab/sk/c/b;->a()Ljava/lang/String;

    move-result-object v0

    const-string v1, "error"

    invoke-virtual {p1, v1}, Lcom/stagnationlab/sk/util/f;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 303
    new-instance v0, Lcom/stagnationlab/sk/c/a;

    sget-object v1, Lcom/stagnationlab/sk/c/b;->q:Lcom/stagnationlab/sk/c/b;

    invoke-direct {v0, v1}, Lcom/stagnationlab/sk/c/a;-><init>(Lcom/stagnationlab/sk/c/b;)V

    .line 304
    iget-object v1, p0, Lcom/stagnationlab/sk/h/b$15;->d:Lcom/stagnationlab/sk/h/b;

    invoke-static {v1}, Lcom/stagnationlab/sk/h/b;->a(Lcom/stagnationlab/sk/h/b;)Lcom/stagnationlab/sk/h/d;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/stagnationlab/sk/h/d;->a(Lcom/stagnationlab/sk/c/a;)V

    .line 308
    :cond_0
    iget-object v0, p0, Lcom/stagnationlab/sk/h/b$15;->c:Lcom/stagnationlab/sk/h/c;

    invoke-interface {v0, p1}, Lcom/stagnationlab/sk/h/c;->a(Lcom/stagnationlab/sk/util/f;)V

    return-void
.end method

.method public a(Lokhttp3/ab;Lcom/stagnationlab/sk/d/a;)V
    .locals 1

    .line 286
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v0, p0, Lcom/stagnationlab/sk/h/b$15;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, " request to \'"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/stagnationlab/sk/h/b$15;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "\' failed"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string v0, "JavascriptApi"

    invoke-static {v0, p1, p2}, Lcom/stagnationlab/sk/f;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 288
    invoke-virtual {p2}, Lcom/stagnationlab/sk/d/a;->a()Lcom/stagnationlab/sk/c/b;

    move-result-object p1

    sget-object v0, Lcom/stagnationlab/sk/c/b;->R:Lcom/stagnationlab/sk/c/b;

    if-ne p1, v0, :cond_0

    .line 289
    sget-object p1, Lcom/stagnationlab/sk/util/i$a;->a:Lcom/stagnationlab/sk/util/i$a;

    invoke-static {p1}, Lcom/stagnationlab/sk/util/i;->a(Lcom/stagnationlab/sk/util/i$a;)V

    .line 290
    new-instance p1, Lcom/stagnationlab/sk/c/a;

    sget-object p2, Lcom/stagnationlab/sk/c/b;->R:Lcom/stagnationlab/sk/c/b;

    invoke-direct {p1, p2}, Lcom/stagnationlab/sk/c/a;-><init>(Lcom/stagnationlab/sk/c/b;)V

    .line 291
    iget-object p2, p0, Lcom/stagnationlab/sk/h/b$15;->d:Lcom/stagnationlab/sk/h/b;

    invoke-static {p2}, Lcom/stagnationlab/sk/h/b;->a(Lcom/stagnationlab/sk/h/b;)Lcom/stagnationlab/sk/h/d;

    move-result-object p2

    invoke-virtual {p2, p1}, Lcom/stagnationlab/sk/h/d;->a(Lcom/stagnationlab/sk/c/a;)V

    return-void

    .line 296
    :cond_0
    iget-object p1, p0, Lcom/stagnationlab/sk/h/b$15;->c:Lcom/stagnationlab/sk/h/c;

    iget-object v0, p0, Lcom/stagnationlab/sk/h/b$15;->d:Lcom/stagnationlab/sk/h/b;

    invoke-static {v0, p2}, Lcom/stagnationlab/sk/h/b;->a(Lcom/stagnationlab/sk/h/b;Lcom/stagnationlab/sk/d/a;)Lcom/stagnationlab/sk/util/f;

    move-result-object p2

    invoke-interface {p1, p2}, Lcom/stagnationlab/sk/h/c;->b(Lcom/stagnationlab/sk/util/f;)V

    return-void
.end method
