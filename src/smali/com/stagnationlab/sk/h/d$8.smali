.class Lcom/stagnationlab/sk/h/d$8;
.super Ljava/lang/Object;
.source "JavascriptBridge.java"

# interfaces
.implements Lcom/stagnationlab/sk/a/a/o;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/stagnationlab/sk/h/d;->a(Lcom/stagnationlab/sk/d;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/stagnationlab/sk/d;

.field final synthetic b:Lcom/stagnationlab/sk/h/d;


# direct methods
.method constructor <init>(Lcom/stagnationlab/sk/h/d;Lcom/stagnationlab/sk/d;)V
    .locals 0

    .line 155
    iput-object p1, p0, Lcom/stagnationlab/sk/h/d$8;->b:Lcom/stagnationlab/sk/h/d;

    iput-object p2, p0, Lcom/stagnationlab/sk/h/d$8;->a:Lcom/stagnationlab/sk/d;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/stagnationlab/sk/c/a;)V
    .locals 1

    .line 172
    iget-object v0, p0, Lcom/stagnationlab/sk/h/d$8;->a:Lcom/stagnationlab/sk/d;

    if-eqz v0, :cond_0

    .line 173
    invoke-interface {v0, p1}, Lcom/stagnationlab/sk/d;->a(Lcom/stagnationlab/sk/c/a;)V

    :cond_0
    return-void
.end method

.method public a(Lcom/stagnationlab/sk/models/Transaction;Lcom/stagnationlab/sk/models/RpRequest;)V
    .locals 3

    .line 160
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/stagnationlab/sk/h/d$8;->b:Lcom/stagnationlab/sk/h/d;

    invoke-static {v1}, Lcom/stagnationlab/sk/h/d;->c(Lcom/stagnationlab/sk/h/d;)Lcom/stagnationlab/sk/MainActivity;

    move-result-object v1

    const-class v2, Lcom/stagnationlab/sk/TransactionActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "transaction"

    .line 161
    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string p1, "rpRequest"

    .line 162
    invoke-virtual {v0, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string p1, "manual"

    const/4 p2, 0x1

    .line 163
    invoke-virtual {v0, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 164
    iget-object p1, p0, Lcom/stagnationlab/sk/h/d$8;->b:Lcom/stagnationlab/sk/h/d;

    invoke-static {p1}, Lcom/stagnationlab/sk/h/d;->c(Lcom/stagnationlab/sk/h/d;)Lcom/stagnationlab/sk/MainActivity;

    move-result-object p1

    invoke-virtual {p1, v0}, Lcom/stagnationlab/sk/MainActivity;->startActivity(Landroid/content/Intent;)V

    .line 165
    iget-object p1, p0, Lcom/stagnationlab/sk/h/d$8;->b:Lcom/stagnationlab/sk/h/d;

    invoke-static {p1}, Lcom/stagnationlab/sk/h/d;->c(Lcom/stagnationlab/sk/h/d;)Lcom/stagnationlab/sk/MainActivity;

    move-result-object p1

    invoke-virtual {p1}, Lcom/stagnationlab/sk/MainActivity;->finish()V

    .line 167
    iget-object p1, p0, Lcom/stagnationlab/sk/h/d$8;->b:Lcom/stagnationlab/sk/h/d;

    invoke-static {p1}, Lcom/stagnationlab/sk/h/d;->c(Lcom/stagnationlab/sk/h/d;)Lcom/stagnationlab/sk/MainActivity;

    move-result-object p1

    invoke-static {p1}, Lcom/stagnationlab/sk/util/n;->c(Landroid/app/Activity;)V

    return-void
.end method
