.class Lcom/stagnationlab/sk/h/b$4;
.super Ljava/lang/Object;
.source "JavascriptApi.java"

# interfaces
.implements Lcom/stagnationlab/sk/f/a;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/stagnationlab/sk/h/b;->createAccount(Lcom/stagnationlab/sk/util/e;Lcom/stagnationlab/sk/h/c;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/stagnationlab/sk/h/c;

.field final synthetic b:Lcom/stagnationlab/sk/h/b;


# direct methods
.method constructor <init>(Lcom/stagnationlab/sk/h/b;Lcom/stagnationlab/sk/h/c;)V
    .locals 0

    .line 432
    iput-object p1, p0, Lcom/stagnationlab/sk/h/b$4;->b:Lcom/stagnationlab/sk/h/b;

    iput-object p2, p0, Lcom/stagnationlab/sk/h/b$4;->a:Lcom/stagnationlab/sk/h/c;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/stagnationlab/sk/c/a;)V
    .locals 2

    .line 444
    iget-object v0, p0, Lcom/stagnationlab/sk/h/b$4;->b:Lcom/stagnationlab/sk/h/b;

    invoke-static {v0}, Lcom/stagnationlab/sk/h/b;->a(Lcom/stagnationlab/sk/h/b;)Lcom/stagnationlab/sk/h/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/stagnationlab/sk/h/d;->e()V

    .line 445
    iget-object v0, p0, Lcom/stagnationlab/sk/h/b$4;->a:Lcom/stagnationlab/sk/h/c;

    iget-object v1, p0, Lcom/stagnationlab/sk/h/b$4;->b:Lcom/stagnationlab/sk/h/b;

    invoke-static {v1, p1}, Lcom/stagnationlab/sk/h/b;->a(Lcom/stagnationlab/sk/h/b;Lcom/stagnationlab/sk/c/a;)Lcom/stagnationlab/sk/util/f;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/stagnationlab/sk/h/c;->b(Lcom/stagnationlab/sk/util/f;)V

    return-void
.end method

.method public a(Lcom/stagnationlab/sk/models/Account;Lcom/stagnationlab/sk/models/Transaction;)V
    .locals 3

    .line 435
    iget-object v0, p0, Lcom/stagnationlab/sk/h/b$4;->b:Lcom/stagnationlab/sk/h/b;

    invoke-static {v0}, Lcom/stagnationlab/sk/h/b;->a(Lcom/stagnationlab/sk/h/b;)Lcom/stagnationlab/sk/h/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/stagnationlab/sk/h/d;->e()V

    .line 436
    iget-object v0, p0, Lcom/stagnationlab/sk/h/b$4;->a:Lcom/stagnationlab/sk/h/c;

    new-instance v1, Lcom/stagnationlab/sk/util/f;

    invoke-direct {v1}, Lcom/stagnationlab/sk/util/f;-><init>()V

    const-string v2, "account"

    .line 437
    invoke-virtual {v1, v2, p1}, Lcom/stagnationlab/sk/util/f;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/stagnationlab/sk/util/f;

    move-result-object p1

    .line 438
    invoke-virtual {p2}, Lcom/stagnationlab/sk/models/Transaction;->c()Lorg/b/a/b;

    move-result-object p2

    invoke-virtual {p2}, Lorg/b/a/b;->toString()Ljava/lang/String;

    move-result-object p2

    const-string v1, "authCsrValidUntil"

    invoke-virtual {p1, v1, p2}, Lcom/stagnationlab/sk/util/f;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/stagnationlab/sk/util/f;

    move-result-object p1

    .line 436
    invoke-interface {v0, p1}, Lcom/stagnationlab/sk/h/c;->a(Lcom/stagnationlab/sk/util/f;)V

    return-void
.end method
