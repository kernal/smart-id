.class public Lcom/stagnationlab/sk/h/e;
.super Lcom/stagnationlab/sk/h/a;
.source "WebViewWrapper.java"


# instance fields
.field private c:Landroid/webkit/WebView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/widget/LinearLayout;)V
    .locals 2
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "SetJavaScriptEnabled"
        }
    .end annotation

    .line 27
    invoke-direct {p0, p1, p2}, Lcom/stagnationlab/sk/h/a;-><init>(Landroid/content/Context;Landroid/widget/LinearLayout;)V

    .line 28
    new-instance v0, Landroid/webkit/WebView;

    invoke-direct {v0, p1}, Landroid/webkit/WebView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/stagnationlab/sk/h/e;->c:Landroid/webkit/WebView;

    .line 30
    iget-object p1, p0, Lcom/stagnationlab/sk/h/e;->c:Landroid/webkit/WebView;

    new-instance v0, Landroid/view/ViewGroup$LayoutParams;

    const/4 v1, -0x1

    invoke-direct {v0, v1, v1}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {p1, v0}, Landroid/webkit/WebView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 34
    iget-object p1, p0, Lcom/stagnationlab/sk/h/e;->c:Landroid/webkit/WebView;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/webkit/WebView;->setBackgroundColor(I)V

    .line 36
    iget-object p1, p0, Lcom/stagnationlab/sk/h/e;->c:Landroid/webkit/WebView;

    invoke-virtual {p1}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object p1

    const/4 v1, 0x1

    .line 37
    invoke-virtual {p1, v1}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    .line 38
    invoke-virtual {p1, v0}, Landroid/webkit/WebSettings;->setMediaPlaybackRequiresUserGesture(Z)V

    .line 43
    invoke-virtual {p1, v0}, Landroid/webkit/WebSettings;->setAllowFileAccess(Z)V

    .line 45
    invoke-virtual {p1, v0}, Landroid/webkit/WebSettings;->setAllowFileAccessFromFileURLs(Z)V

    .line 47
    invoke-virtual {p1, v0}, Landroid/webkit/WebSettings;->setAllowUniversalAccessFromFileURLs(Z)V

    .line 49
    invoke-virtual {p1, v0}, Landroid/webkit/WebSettings;->setAllowContentAccess(Z)V

    .line 51
    iget-object p1, p0, Lcom/stagnationlab/sk/h/e;->c:Landroid/webkit/WebView;

    new-instance v0, Lcom/stagnationlab/sk/h/e$1;

    invoke-direct {v0, p0}, Lcom/stagnationlab/sk/h/e$1;-><init>(Lcom/stagnationlab/sk/h/e;)V

    invoke-virtual {p1, v0}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 70
    iget-object p1, p0, Lcom/stagnationlab/sk/h/e;->c:Landroid/webkit/WebView;

    new-instance v0, Lcom/stagnationlab/sk/h/e$2;

    invoke-direct {v0, p0}, Lcom/stagnationlab/sk/h/e$2;-><init>(Lcom/stagnationlab/sk/h/e;)V

    invoke-virtual {p1, v0}, Landroid/webkit/WebView;->setWebChromeClient(Landroid/webkit/WebChromeClient;)V

    .line 83
    iget-object p1, p0, Lcom/stagnationlab/sk/h/e;->c:Landroid/webkit/WebView;

    invoke-virtual {p2, p1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    return-void
.end method


# virtual methods
.method public a(Lcom/stagnationlab/sk/h/d;Ljava/lang/String;)V
    .locals 2

    .line 108
    iget-object v0, p0, Lcom/stagnationlab/sk/h/e;->c:Landroid/webkit/WebView;

    new-instance v1, Lcom/stagnationlab/sk/h/e$3;

    invoke-direct {v1, p0, p1}, Lcom/stagnationlab/sk/h/e$3;-><init>(Lcom/stagnationlab/sk/h/e;Lcom/stagnationlab/sk/h/d;)V

    invoke-virtual {v0, v1, p2}, Landroid/webkit/WebView;->addJavascriptInterface(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method

.method public a(Ljava/lang/String;Landroid/webkit/ValueCallback;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Landroid/webkit/ValueCallback<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 138
    iget-object v0, p0, Lcom/stagnationlab/sk/h/e;->c:Landroid/webkit/WebView;

    invoke-virtual {v0, p1, p2}, Landroid/webkit/WebView;->evaluateJavascript(Ljava/lang/String;Landroid/webkit/ValueCallback;)V

    return-void
.end method

.method public a(Z)V
    .locals 0

    .line 88
    invoke-static {p1}, Landroid/webkit/WebView;->setWebContentsDebuggingEnabled(Z)V

    return-void
.end method

.method public bridge synthetic b(Ljava/lang/String;)V
    .locals 0

    .line 22
    invoke-super {p0, p1}, Lcom/stagnationlab/sk/h/a;->b(Ljava/lang/String;)V

    return-void
.end method

.method c()Landroid/view/ViewPropertyAnimator;
    .locals 3

    .line 103
    iget-object v0, p0, Lcom/stagnationlab/sk/h/e;->b:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    sget v1, Lcom/stagnationlab/sk/h/e;->a:I

    int-to-long v1, v1

    invoke-virtual {v0, v1, v2}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    return-object v0
.end method

.method c(Ljava/lang/String;)V
    .locals 1

    .line 118
    iget-object v0, p0, Lcom/stagnationlab/sk/h/e;->c:Landroid/webkit/WebView;

    invoke-virtual {v0, p1}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    return-void
.end method

.method public d()Z
    .locals 1

    .line 93
    iget-object v0, p0, Lcom/stagnationlab/sk/h/e;->c:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->canGoBack()Z

    move-result v0

    return v0
.end method

.method public e()V
    .locals 1

    .line 98
    iget-object v0, p0, Lcom/stagnationlab/sk/h/e;->c:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->goBack()V

    return-void
.end method

.method public f()V
    .locals 1

    .line 123
    iget-object v0, p0, Lcom/stagnationlab/sk/h/e;->c:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->pauseTimers()V

    return-void
.end method

.method public g()V
    .locals 1

    .line 128
    iget-object v0, p0, Lcom/stagnationlab/sk/h/e;->c:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->resumeTimers()V

    return-void
.end method

.method public h()V
    .locals 1

    .line 133
    iget-object v0, p0, Lcom/stagnationlab/sk/h/e;->c:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->destroy()V

    return-void
.end method
