.class Lcom/stagnationlab/sk/h/b$6;
.super Ljava/lang/Object;
.source "JavascriptApi.java"

# interfaces
.implements Lcom/stagnationlab/sk/f/f;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/stagnationlab/sk/h/b;->submitCsr(Lcom/stagnationlab/sk/util/e;Lcom/stagnationlab/sk/h/c;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/stagnationlab/sk/h/c;

.field final synthetic b:Lcom/stagnationlab/sk/h/b;


# direct methods
.method constructor <init>(Lcom/stagnationlab/sk/h/b;Lcom/stagnationlab/sk/h/c;)V
    .locals 0

    .line 492
    iput-object p1, p0, Lcom/stagnationlab/sk/h/b$6;->b:Lcom/stagnationlab/sk/h/b;

    iput-object p2, p0, Lcom/stagnationlab/sk/h/b$6;->a:Lcom/stagnationlab/sk/h/c;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/stagnationlab/sk/c/a;)V
    .locals 2

    .line 508
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "CSR "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "JavascriptApi"

    invoke-static {v1, v0}, Lcom/stagnationlab/sk/f;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 510
    iget-object v0, p0, Lcom/stagnationlab/sk/h/b$6;->a:Lcom/stagnationlab/sk/h/c;

    iget-object v1, p0, Lcom/stagnationlab/sk/h/b$6;->b:Lcom/stagnationlab/sk/h/b;

    invoke-static {v1, p1}, Lcom/stagnationlab/sk/h/b;->a(Lcom/stagnationlab/sk/h/b;Lcom/stagnationlab/sk/c/a;)Lcom/stagnationlab/sk/util/f;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/stagnationlab/sk/h/c;->b(Lcom/stagnationlab/sk/util/f;)V

    return-void
.end method

.method public a(Ljava/lang/String;Lcom/stagnationlab/sk/models/Transaction;)V
    .locals 2

    .line 495
    new-instance v0, Lcom/stagnationlab/sk/util/f;

    invoke-direct {v0}, Lcom/stagnationlab/sk/util/f;-><init>()V

    const-string v1, "type"

    invoke-virtual {v0, v1, p1}, Lcom/stagnationlab/sk/util/f;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/stagnationlab/sk/util/f;

    move-result-object v0

    .line 497
    sget-object v1, Lcom/stagnationlab/sk/models/AccountKey$Type;->SIGNATURE:Lcom/stagnationlab/sk/models/AccountKey$Type;

    invoke-virtual {v1}, Lcom/stagnationlab/sk/models/AccountKey$Type;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 498
    iget-object p1, p0, Lcom/stagnationlab/sk/h/b$6;->b:Lcom/stagnationlab/sk/h/b;

    const/4 p2, 0x0

    invoke-static {p1, p2}, Lcom/stagnationlab/sk/h/b;->a(Lcom/stagnationlab/sk/h/b;Lcom/stagnationlab/sk/f/e;)Lcom/stagnationlab/sk/f/e;

    goto :goto_0

    :cond_0
    if-eqz p2, :cond_1

    .line 500
    invoke-virtual {p2}, Lcom/stagnationlab/sk/models/Transaction;->c()Lorg/b/a/b;

    move-result-object p1

    invoke-virtual {p1}, Lorg/b/a/b;->toString()Ljava/lang/String;

    move-result-object p1

    const-string p2, "signCsrValidUntil"

    invoke-virtual {v0, p2, p1}, Lcom/stagnationlab/sk/util/f;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/stagnationlab/sk/util/f;

    .line 503
    :cond_1
    :goto_0
    iget-object p1, p0, Lcom/stagnationlab/sk/h/b$6;->a:Lcom/stagnationlab/sk/h/c;

    invoke-interface {p1, v0}, Lcom/stagnationlab/sk/h/c;->a(Lcom/stagnationlab/sk/util/f;)V

    return-void
.end method
