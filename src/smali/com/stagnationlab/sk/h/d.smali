.class public Lcom/stagnationlab/sk/h/d;
.super Ljava/lang/Object;
.source "JavascriptBridge.java"


# static fields
.field public static final a:Ljava/lang/String; = "file:///android_asset/www/index.html"


# instance fields
.field private b:Lcom/stagnationlab/sk/MainActivity;

.field private c:Lcom/stagnationlab/sk/h/e;

.field private d:Lcom/stagnationlab/sk/a/a;

.field private e:Ljava/lang/String;

.field private f:Lcom/stagnationlab/sk/h/b;

.field private g:Z

.field private h:Landroid/os/PowerManager$WakeLock;

.field private i:Ljava/lang/String;

.field private j:Lcom/stagnationlab/sk/a/b;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>(Lcom/stagnationlab/sk/MainActivity;Lcom/stagnationlab/sk/h/e;)V
    .locals 1

    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "JavascriptBridge"

    .line 50
    iput-object v0, p0, Lcom/stagnationlab/sk/h/d;->e:Ljava/lang/String;

    const/4 v0, 0x0

    .line 52
    iput-boolean v0, p0, Lcom/stagnationlab/sk/h/d;->g:Z

    const/4 v0, 0x0

    .line 54
    iput-object v0, p0, Lcom/stagnationlab/sk/h/d;->i:Ljava/lang/String;

    .line 58
    iput-object p1, p0, Lcom/stagnationlab/sk/h/d;->b:Lcom/stagnationlab/sk/MainActivity;

    .line 59
    iput-object p2, p0, Lcom/stagnationlab/sk/h/d;->c:Lcom/stagnationlab/sk/h/e;

    .line 60
    invoke-virtual {p1}, Lcom/stagnationlab/sk/MainActivity;->getApplication()Landroid/app/Application;

    move-result-object p1

    check-cast p1, Lcom/stagnationlab/sk/MyApplication;

    .line 61
    new-instance p2, Lcom/stagnationlab/sk/h/b;

    .line 63
    invoke-virtual {p1}, Lcom/stagnationlab/sk/MyApplication;->e()Lcom/stagnationlab/sk/a;

    move-result-object p1

    invoke-direct {p2, p0, p1}, Lcom/stagnationlab/sk/h/b;-><init>(Lcom/stagnationlab/sk/h/d;Lcom/stagnationlab/sk/a;)V

    iput-object p2, p0, Lcom/stagnationlab/sk/h/d;->f:Lcom/stagnationlab/sk/h/b;

    return-void
.end method

.method static synthetic a(Lcom/stagnationlab/sk/h/d;)Ljava/lang/String;
    .locals 0

    .line 44
    iget-object p0, p0, Lcom/stagnationlab/sk/h/d;->e:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic a(Lcom/stagnationlab/sk/h/d;Ljava/lang/String;IZLcom/stagnationlab/sk/util/f;)V
    .locals 0

    .line 44
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/stagnationlab/sk/h/d;->a(Ljava/lang/String;IZLcom/stagnationlab/sk/util/f;)V

    return-void
.end method

.method private a(Ljava/lang/String;IZLcom/stagnationlab/sk/util/f;)V
    .locals 8

    .line 101
    iget-object v0, p0, Lcom/stagnationlab/sk/h/d;->b:Lcom/stagnationlab/sk/MainActivity;

    new-instance v7, Lcom/stagnationlab/sk/h/d$5;

    move-object v1, v7

    move-object v2, p0

    move-object v3, p1

    move v4, p2

    move v5, p3

    move-object v6, p4

    invoke-direct/range {v1 .. v6}, Lcom/stagnationlab/sk/h/d$5;-><init>(Lcom/stagnationlab/sk/h/d;Ljava/lang/String;IZLcom/stagnationlab/sk/util/f;)V

    invoke-virtual {v0, v7}, Lcom/stagnationlab/sk/MainActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 2

    .line 297
    iget-object v0, p0, Lcom/stagnationlab/sk/h/d;->b:Lcom/stagnationlab/sk/MainActivity;

    new-instance v1, Lcom/stagnationlab/sk/h/d$2;

    invoke-direct {v1, p0, p1, p2}, Lcom/stagnationlab/sk/h/d$2;-><init>(Lcom/stagnationlab/sk/h/d;Ljava/lang/String;Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Lcom/stagnationlab/sk/MainActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    return-void
.end method

.method static synthetic b(Lcom/stagnationlab/sk/h/d;)Lcom/stagnationlab/sk/h/e;
    .locals 0

    .line 44
    iget-object p0, p0, Lcom/stagnationlab/sk/h/d;->c:Lcom/stagnationlab/sk/h/e;

    return-object p0
.end method

.method static synthetic c(Lcom/stagnationlab/sk/h/d;)Lcom/stagnationlab/sk/MainActivity;
    .locals 0

    .line 44
    iget-object p0, p0, Lcom/stagnationlab/sk/h/d;->b:Lcom/stagnationlab/sk/MainActivity;

    return-object p0
.end method


# virtual methods
.method public a()V
    .locals 3

    .line 118
    iget-object v0, p0, Lcom/stagnationlab/sk/h/d;->c:Lcom/stagnationlab/sk/h/e;

    new-instance v1, Lcom/stagnationlab/sk/h/d$6;

    invoke-direct {v1, p0}, Lcom/stagnationlab/sk/h/d$6;-><init>(Lcom/stagnationlab/sk/h/d;)V

    const-string v2, "NativeBridge.handleBack();"

    invoke-virtual {v0, v2, v1}, Lcom/stagnationlab/sk/h/e;->a(Ljava/lang/String;Landroid/webkit/ValueCallback;)V

    return-void
.end method

.method a(Lcom/stagnationlab/sk/a/a/u;)V
    .locals 1

    .line 343
    iget-object v0, p0, Lcom/stagnationlab/sk/h/d;->j:Lcom/stagnationlab/sk/a/b;

    invoke-virtual {v0, p1}, Lcom/stagnationlab/sk/a/b;->a(Lcom/stagnationlab/sk/a/a/u;)V

    return-void
.end method

.method public a(Lcom/stagnationlab/sk/a/a;)V
    .locals 1

    .line 68
    iput-object p1, p0, Lcom/stagnationlab/sk/h/d;->d:Lcom/stagnationlab/sk/a/a;

    .line 69
    invoke-virtual {p1, p0}, Lcom/stagnationlab/sk/a/a;->a(Lcom/stagnationlab/sk/h/d;)V

    .line 70
    iget-object v0, p0, Lcom/stagnationlab/sk/h/d;->f:Lcom/stagnationlab/sk/h/b;

    invoke-virtual {v0, p1}, Lcom/stagnationlab/sk/h/b;->a(Lcom/stagnationlab/sk/a/a;)V

    return-void
.end method

.method public a(Lcom/stagnationlab/sk/a/b;)V
    .locals 0

    .line 347
    iput-object p1, p0, Lcom/stagnationlab/sk/h/d;->j:Lcom/stagnationlab/sk/a/b;

    return-void
.end method

.method public a(Lcom/stagnationlab/sk/b/b;)V
    .locals 0

    .line 339
    invoke-static {p1}, Lcom/stagnationlab/sk/a/d;->a(Lcom/stagnationlab/sk/b/b;)V

    return-void
.end method

.method a(Lcom/stagnationlab/sk/c/a;)V
    .locals 3

    .line 254
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/stagnationlab/sk/h/d;->b:Lcom/stagnationlab/sk/MainActivity;

    const-class v2, Lcom/stagnationlab/sk/ErrorActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "error"

    .line 255
    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 257
    iget-object p1, p0, Lcom/stagnationlab/sk/h/d;->b:Lcom/stagnationlab/sk/MainActivity;

    invoke-virtual {p1, v0}, Lcom/stagnationlab/sk/MainActivity;->startActivity(Landroid/content/Intent;)V

    .line 258
    iget-object p1, p0, Lcom/stagnationlab/sk/h/d;->b:Lcom/stagnationlab/sk/MainActivity;

    invoke-virtual {p1}, Lcom/stagnationlab/sk/MainActivity;->finish()V

    return-void
.end method

.method public a(Lcom/stagnationlab/sk/c/a;Ljava/lang/String;)V
    .locals 2

    .line 290
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    const-string v1, "error"

    .line 291
    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string p1, "message"

    .line 292
    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string p1, "otpResult"

    .line 293
    invoke-direct {p0, p1, v0}, Lcom/stagnationlab/sk/h/d;->a(Ljava/lang/String;Ljava/lang/Object;)V

    return-void
.end method

.method public a(Lcom/stagnationlab/sk/d;)V
    .locals 2

    .line 154
    iget-object v0, p0, Lcom/stagnationlab/sk/h/d;->d:Lcom/stagnationlab/sk/a/a;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/stagnationlab/sk/a/a;->h()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 155
    iget-object v0, p0, Lcom/stagnationlab/sk/h/d;->d:Lcom/stagnationlab/sk/a/a;

    new-instance v1, Lcom/stagnationlab/sk/h/d$8;

    invoke-direct {v1, p0, p1}, Lcom/stagnationlab/sk/h/d$8;-><init>(Lcom/stagnationlab/sk/h/d;Lcom/stagnationlab/sk/d;)V

    invoke-virtual {v0, v1}, Lcom/stagnationlab/sk/a/a;->a(Lcom/stagnationlab/sk/a/a/o;)V

    goto :goto_0

    :cond_0
    if-eqz p1, :cond_1

    const/4 v0, 0x0

    .line 178
    invoke-interface {p1, v0}, Lcom/stagnationlab/sk/d;->a(Lcom/stagnationlab/sk/c/a;)V

    :cond_1
    :goto_0
    return-void
.end method

.method a(Lcom/stagnationlab/sk/fcm/d;)V
    .locals 1

    .line 129
    iget-object v0, p0, Lcom/stagnationlab/sk/h/d;->b:Lcom/stagnationlab/sk/MainActivity;

    invoke-static {v0, p1}, Lcom/stagnationlab/sk/fcm/b;->a(Landroid/app/Activity;Lcom/stagnationlab/sk/fcm/d;)V

    return-void
.end method

.method a(Lcom/stagnationlab/sk/h/c;)V
    .locals 1

    .line 360
    iget-object v0, p0, Lcom/stagnationlab/sk/h/d;->b:Lcom/stagnationlab/sk/MainActivity;

    invoke-static {v0, p1}, Lcom/stagnationlab/sk/otp/a;->a(Landroid/content/Context;Lcom/stagnationlab/sk/h/c;)V

    return-void
.end method

.method public a(Lcom/stagnationlab/sk/models/Account;)V
    .locals 1

    const-string v0, "account"

    .line 282
    invoke-direct {p0, v0, p1}, Lcom/stagnationlab/sk/h/d;->a(Ljava/lang/String;Ljava/lang/Object;)V

    return-void
.end method

.method a(Ljava/lang/String;)V
    .locals 2

    .line 192
    iget-object v0, p0, Lcom/stagnationlab/sk/h/d;->b:Lcom/stagnationlab/sk/MainActivity;

    new-instance v1, Lcom/stagnationlab/sk/h/d$10;

    invoke-direct {v1, p0, p1}, Lcom/stagnationlab/sk/h/d$10;-><init>(Lcom/stagnationlab/sk/h/d;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/stagnationlab/sk/MainActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    return-void
.end method

.method a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .line 201
    iget-object v0, p0, Lcom/stagnationlab/sk/h/d;->b:Lcom/stagnationlab/sk/MainActivity;

    new-instance v1, Lcom/stagnationlab/sk/h/d$11;

    invoke-direct {v1, p0, p1, p2}, Lcom/stagnationlab/sk/h/d$11;-><init>(Lcom/stagnationlab/sk/h/d;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/stagnationlab/sk/MainActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    return-void
.end method

.method a(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 6

    .line 74
    iget-object v0, p0, Lcom/stagnationlab/sk/h/d;->e:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "<-- Received request for \'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "\' ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/stagnationlab/sk/f;->d(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 77
    :try_start_0
    new-instance v1, Lcom/stagnationlab/sk/util/e;

    invoke-direct {v1, p2}, Lcom/stagnationlab/sk/util/e;-><init>(Ljava/lang/String;)V

    .line 79
    const-class p2, Lcom/stagnationlab/sk/h/b;

    const/4 v2, 0x2

    new-array v3, v2, [Ljava/lang/Class;

    const-class v4, Lcom/stagnationlab/sk/util/e;

    aput-object v4, v3, v0

    const-class v4, Lcom/stagnationlab/sk/h/c;

    const/4 v5, 0x1

    aput-object v4, v3, v5

    invoke-virtual {p2, p1, v3}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object p2

    .line 80
    iget-object v3, p0, Lcom/stagnationlab/sk/h/d;->f:Lcom/stagnationlab/sk/h/b;

    new-array v2, v2, [Ljava/lang/Object;

    aput-object v1, v2, v0

    new-instance v1, Lcom/stagnationlab/sk/h/d$1;

    invoke-direct {v1, p0, p1, p3}, Lcom/stagnationlab/sk/h/d$1;-><init>(Lcom/stagnationlab/sk/h/d;Ljava/lang/String;I)V

    aput-object v1, v2, v5

    invoke-virtual {p2, v3, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p2

    .line 92
    iget-object v1, p0, Lcom/stagnationlab/sk/h/d;->e:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Failed to invoke method "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, p2}, Lcom/stagnationlab/sk/f;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 94
    new-instance p2, Lcom/stagnationlab/sk/util/f;

    invoke-direct {p2}, Lcom/stagnationlab/sk/util/f;-><init>()V

    const-string v1, "message"

    const-string v2, "Technical error"

    .line 95
    invoke-virtual {p2, v1, v2}, Lcom/stagnationlab/sk/util/f;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/stagnationlab/sk/util/f;

    move-result-object p2

    .line 94
    invoke-direct {p0, p1, p3, v0, p2}, Lcom/stagnationlab/sk/h/d;->a(Ljava/lang/String;IZLcom/stagnationlab/sk/util/f;)V

    :goto_0
    return-void
.end method

.method a(Ljava/lang/String;Ljava/lang/String;Lcom/stagnationlab/sk/g/d;)V
    .locals 2

    .line 135
    iget-object v0, p0, Lcom/stagnationlab/sk/h/d;->b:Lcom/stagnationlab/sk/MainActivity;

    new-instance v1, Lcom/stagnationlab/sk/h/d$7;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/stagnationlab/sk/h/d$7;-><init>(Lcom/stagnationlab/sk/h/d;Ljava/lang/String;Ljava/lang/String;Lcom/stagnationlab/sk/g/d;)V

    invoke-virtual {v0, v1}, Lcom/stagnationlab/sk/MainActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    return-void
.end method

.method public a(Ljava/util/Map;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    const-string v0, "rootData"

    .line 262
    invoke-direct {p0, v0, p1}, Lcom/stagnationlab/sk/h/d;->a(Ljava/lang/String;Ljava/lang/Object;)V

    return-void
.end method

.method a(ZZLcom/stagnationlab/sk/h/c;)V
    .locals 2

    .line 351
    iget-object v0, p0, Lcom/stagnationlab/sk/h/d;->b:Lcom/stagnationlab/sk/MainActivity;

    new-instance v1, Lcom/stagnationlab/sk/h/d$4;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/stagnationlab/sk/h/d$4;-><init>(Lcom/stagnationlab/sk/h/d;ZZLcom/stagnationlab/sk/h/c;)V

    invoke-virtual {v0, v1}, Lcom/stagnationlab/sk/MainActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    return-void
.end method

.method public b()V
    .locals 2

    .line 183
    iget-object v0, p0, Lcom/stagnationlab/sk/h/d;->b:Lcom/stagnationlab/sk/MainActivity;

    new-instance v1, Lcom/stagnationlab/sk/h/d$9;

    invoke-direct {v1, p0}, Lcom/stagnationlab/sk/h/d$9;-><init>(Lcom/stagnationlab/sk/h/d;)V

    invoke-virtual {v0, v1}, Lcom/stagnationlab/sk/MainActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    return-void
.end method

.method public b(Lcom/stagnationlab/sk/c/a;)V
    .locals 1

    const-string v0, "checkTransactionEnd"

    .line 278
    invoke-direct {p0, v0, p1}, Lcom/stagnationlab/sk/h/d;->a(Ljava/lang/String;Ljava/lang/Object;)V

    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 1

    .line 266
    invoke-static {p1}, Lcom/stagnationlab/sk/i;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    const-string v0, "pushToken"

    invoke-direct {p0, v0, p1}, Lcom/stagnationlab/sk/h/d;->a(Ljava/lang/String;Ljava/lang/Object;)V

    return-void
.end method

.method b(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .line 239
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "android.intent.action.SEND"

    .line 240
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "android.intent.extra.SUBJECT"

    .line 241
    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string p1, "android.intent.extra.TEXT"

    .line 242
    invoke-virtual {v0, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string p1, "text/plain"

    .line 243
    invoke-virtual {v0, p1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 244
    iget-object p1, p0, Lcom/stagnationlab/sk/h/d;->b:Lcom/stagnationlab/sk/MainActivity;

    const p2, 0x7f0e0115

    .line 245
    invoke-virtual {p1, p2}, Lcom/stagnationlab/sk/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object p2

    invoke-static {v0, p2}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object p2

    .line 244
    invoke-virtual {p1, p2}, Lcom/stagnationlab/sk/MainActivity;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method c()V
    .locals 2

    .line 210
    iget-object v0, p0, Lcom/stagnationlab/sk/h/d;->b:Lcom/stagnationlab/sk/MainActivity;

    new-instance v1, Lcom/stagnationlab/sk/h/d$12;

    invoke-direct {v1, p0}, Lcom/stagnationlab/sk/h/d$12;-><init>(Lcom/stagnationlab/sk/h/d;)V

    invoke-virtual {v0, v1}, Lcom/stagnationlab/sk/MainActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    return-void
.end method

.method c(Ljava/lang/String;)V
    .locals 0

    .line 317
    iput-object p1, p0, Lcom/stagnationlab/sk/h/d;->i:Ljava/lang/String;

    return-void
.end method

.method d()V
    .locals 3

    .line 222
    iget-object v0, p0, Lcom/stagnationlab/sk/h/d;->h:Landroid/os/PowerManager$WakeLock;

    if-eqz v0, :cond_0

    return-void

    .line 226
    :cond_0
    iget-object v0, p0, Lcom/stagnationlab/sk/h/d;->b:Lcom/stagnationlab/sk/MainActivity;

    const-string v1, "power"

    invoke-virtual {v0, v1}, Lcom/stagnationlab/sk/MainActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    const/4 v1, 0x1

    .line 227
    iget-object v2, p0, Lcom/stagnationlab/sk/h/d;->e:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    iput-object v0, p0, Lcom/stagnationlab/sk/h/d;->h:Landroid/os/PowerManager$WakeLock;

    .line 228
    iget-object v0, p0, Lcom/stagnationlab/sk/h/d;->h:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    return-void
.end method

.method e()V
    .locals 1

    .line 232
    iget-object v0, p0, Lcom/stagnationlab/sk/h/d;->h:Landroid/os/PowerManager$WakeLock;

    if-eqz v0, :cond_0

    .line 233
    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    const/4 v0, 0x0

    .line 234
    iput-object v0, p0, Lcom/stagnationlab/sk/h/d;->h:Landroid/os/PowerManager$WakeLock;

    :cond_0
    return-void
.end method

.method public f()V
    .locals 2

    const-string v0, "checkTransactionStart"

    const/4 v1, 0x0

    .line 270
    invoke-direct {p0, v0, v1}, Lcom/stagnationlab/sk/h/d;->a(Ljava/lang/String;Ljava/lang/Object;)V

    return-void
.end method

.method public g()V
    .locals 2

    const-string v0, "openHowToUse"

    const/4 v1, 0x0

    .line 274
    invoke-direct {p0, v0, v1}, Lcom/stagnationlab/sk/h/d;->a(Ljava/lang/String;Ljava/lang/Object;)V

    return-void
.end method

.method public h()V
    .locals 2

    const-string v0, "interactiveUpgradeStarted"

    const/4 v1, 0x0

    .line 286
    invoke-direct {p0, v0, v1}, Lcom/stagnationlab/sk/h/d;->a(Ljava/lang/String;Ljava/lang/Object;)V

    return-void
.end method

.method i()V
    .locals 3

    .line 321
    invoke-static {}, Lcom/stagnationlab/sk/a/d;->b()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/stagnationlab/sk/h/d;->i:Ljava/lang/String;

    if-eqz v0, :cond_1

    const-string v1, "PIN"

    .line 322
    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/stagnationlab/sk/h/d;->i:Ljava/lang/String;

    const-string v1, "Registration token"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    .line 324
    :goto_0
    iget-object v1, p0, Lcom/stagnationlab/sk/h/d;->b:Lcom/stagnationlab/sk/MainActivity;

    new-instance v2, Lcom/stagnationlab/sk/h/d$3;

    invoke-direct {v2, p0, v0}, Lcom/stagnationlab/sk/h/d$3;-><init>(Lcom/stagnationlab/sk/h/d;Z)V

    invoke-virtual {v1, v2}, Lcom/stagnationlab/sk/MainActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    return-void
.end method

.method j()Ljava/lang/String;
    .locals 1

    .line 364
    iget-object v0, p0, Lcom/stagnationlab/sk/h/d;->b:Lcom/stagnationlab/sk/MainActivity;

    invoke-virtual {v0}, Lcom/stagnationlab/sk/MainActivity;->getPackageName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
