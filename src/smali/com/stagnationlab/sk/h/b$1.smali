.class Lcom/stagnationlab/sk/h/b$1;
.super Ljava/lang/Object;
.source "JavascriptApi.java"

# interfaces
.implements Lcom/stagnationlab/sk/a/a/u;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/stagnationlab/sk/h/b;->init(Lcom/stagnationlab/sk/util/e;Lcom/stagnationlab/sk/h/c;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/stagnationlab/sk/h/c;

.field final synthetic b:Lcom/stagnationlab/sk/h/b;


# direct methods
.method constructor <init>(Lcom/stagnationlab/sk/h/b;Lcom/stagnationlab/sk/h/c;)V
    .locals 0

    .line 73
    iput-object p1, p0, Lcom/stagnationlab/sk/h/b$1;->b:Lcom/stagnationlab/sk/h/b;

    iput-object p2, p0, Lcom/stagnationlab/sk/h/b$1;->a:Lcom/stagnationlab/sk/h/c;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/stagnationlab/sk/c/a;)V
    .locals 2

    .line 107
    iget-object v0, p0, Lcom/stagnationlab/sk/h/b$1;->a:Lcom/stagnationlab/sk/h/c;

    iget-object v1, p0, Lcom/stagnationlab/sk/h/b$1;->b:Lcom/stagnationlab/sk/h/b;

    invoke-static {v1, p1}, Lcom/stagnationlab/sk/h/b;->a(Lcom/stagnationlab/sk/h/b;Lcom/stagnationlab/sk/c/a;)Lcom/stagnationlab/sk/util/f;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/stagnationlab/sk/h/c;->b(Lcom/stagnationlab/sk/util/f;)V

    return-void
.end method

.method public a(Lcom/stagnationlab/sk/models/HybridInitData;)V
    .locals 5

    .line 76
    iget-object v0, p0, Lcom/stagnationlab/sk/h/b$1;->a:Lcom/stagnationlab/sk/h/c;

    new-instance v1, Lcom/stagnationlab/sk/util/f;

    invoke-direct {v1}, Lcom/stagnationlab/sk/util/f;-><init>()V

    sget-object v2, Lcom/stagnationlab/sk/h/d;->a:Ljava/lang/String;

    const-string v3, "appUrl"

    .line 78
    invoke-virtual {v1, v3, v2}, Lcom/stagnationlab/sk/util/f;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/stagnationlab/sk/util/f;

    move-result-object v1

    sget-object v2, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v3, "device"

    .line 79
    invoke-virtual {v1, v3, v2}, Lcom/stagnationlab/sk/util/f;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/stagnationlab/sk/util/f;

    move-result-object v1

    const-string v2, "portalBaseUrl"

    const-string v3, "https://portal.smart-id.com"

    .line 80
    invoke-virtual {v1, v2, v3}, Lcom/stagnationlab/sk/util/f;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/stagnationlab/sk/util/f;

    move-result-object v1

    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const-string v3, "androidApiLevel"

    .line 81
    invoke-virtual {v1, v3, v2}, Lcom/stagnationlab/sk/util/f;->a(Ljava/lang/String;I)Lcom/stagnationlab/sk/util/f;

    move-result-object v1

    const-string v2, "channel"

    const-string v3, "PRODUCTION"

    .line 82
    invoke-virtual {v1, v2, v3}, Lcom/stagnationlab/sk/util/f;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/stagnationlab/sk/util/f;

    move-result-object v1

    const-string v2, "debug"

    const/4 v3, 0x0

    .line 83
    invoke-virtual {v1, v2, v3}, Lcom/stagnationlab/sk/util/f;->a(Ljava/lang/String;Z)Lcom/stagnationlab/sk/util/f;

    move-result-object v1

    const/4 v2, 0x1

    const-string v3, "enableLandingAnimation"

    .line 84
    invoke-virtual {v1, v3, v2}, Lcom/stagnationlab/sk/util/f;->a(Ljava/lang/String;Z)Lcom/stagnationlab/sk/util/f;

    move-result-object v1

    .line 85
    invoke-static {}, Lcom/stagnationlab/sk/util/b;->a()Ljava/lang/String;

    move-result-object v3

    const-string v4, "shellVersion"

    invoke-virtual {v1, v4, v3}, Lcom/stagnationlab/sk/util/f;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/stagnationlab/sk/util/f;

    move-result-object v1

    .line 86
    invoke-static {}, Lcom/stagnationlab/sk/util/b;->b()Ljava/lang/String;

    move-result-object v3

    const-string v4, "libVersion"

    invoke-virtual {v1, v4, v3}, Lcom/stagnationlab/sk/util/f;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/stagnationlab/sk/util/f;

    move-result-object v1

    .line 87
    invoke-static {}, Lcom/stagnationlab/sk/util/b;->c()Ljava/lang/String;

    move-result-object v3

    const-string v4, "tseVersion"

    invoke-virtual {v1, v4, v3}, Lcom/stagnationlab/sk/util/f;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/stagnationlab/sk/util/f;

    move-result-object v1

    .line 88
    invoke-static {}, Lcom/stagnationlab/sk/util/b;->d()Ljava/lang/String;

    move-result-object v3

    const-string v4, "cryptoLibVersion"

    invoke-virtual {v1, v4, v3}, Lcom/stagnationlab/sk/util/f;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/stagnationlab/sk/util/f;

    move-result-object v1

    .line 89
    invoke-static {}, Lcom/stagnationlab/sk/a/d;->f()Lcom/stagnationlab/sk/b/b;

    move-result-object v3

    invoke-virtual {v3}, Lcom/stagnationlab/sk/b/b;->name()Ljava/lang/String;

    move-result-object v3

    const-string v4, "pushBehaviour"

    invoke-virtual {v1, v4, v3}, Lcom/stagnationlab/sk/util/f;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/stagnationlab/sk/util/f;

    move-result-object v1

    .line 90
    invoke-static {}, Lcom/stagnationlab/sk/a/d;->g()Lcom/stagnationlab/sk/b/a;

    move-result-object v3

    invoke-virtual {v3}, Lcom/stagnationlab/sk/b/a;->name()Ljava/lang/String;

    move-result-object v3

    const-string v4, "notificationSound"

    invoke-virtual {v1, v4, v3}, Lcom/stagnationlab/sk/util/f;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/stagnationlab/sk/util/f;

    move-result-object v1

    .line 91
    invoke-static {}, Lcom/stagnationlab/sk/a/d;->c()Z

    move-result v3

    const-string v4, "isRegistrationInProgress"

    invoke-virtual {v1, v4, v3}, Lcom/stagnationlab/sk/util/f;->a(Ljava/lang/String;Z)Lcom/stagnationlab/sk/util/f;

    move-result-object v1

    .line 92
    invoke-static {}, Lcom/stagnationlab/sk/a/d;->b()Z

    move-result v3

    const-string v4, "allowScreenshot"

    invoke-virtual {v1, v4, v3}, Lcom/stagnationlab/sk/util/f;->a(Ljava/lang/String;Z)Lcom/stagnationlab/sk/util/f;

    move-result-object v1

    iget-object v3, p0, Lcom/stagnationlab/sk/h/b$1;->b:Lcom/stagnationlab/sk/h/b;

    .line 93
    invoke-virtual {p1}, Lcom/stagnationlab/sk/models/HybridInitData;->c()Lcom/stagnationlab/sk/c/a;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/stagnationlab/sk/h/b;->a(Lcom/stagnationlab/sk/h/b;Lcom/stagnationlab/sk/c/a;)Lcom/stagnationlab/sk/util/f;

    move-result-object v3

    const-string v4, "error"

    invoke-virtual {v1, v4, v3}, Lcom/stagnationlab/sk/util/f;->a(Ljava/lang/String;Lcom/stagnationlab/sk/util/f;)Lcom/stagnationlab/sk/util/f;

    move-result-object v1

    .line 94
    invoke-virtual {p1}, Lcom/stagnationlab/sk/models/HybridInitData;->d()Z

    move-result v3

    const-string v4, "showNotificationInfo"

    invoke-virtual {v1, v4, v3}, Lcom/stagnationlab/sk/util/f;->a(Ljava/lang/String;Z)Lcom/stagnationlab/sk/util/f;

    move-result-object v1

    .line 95
    invoke-virtual {p1}, Lcom/stagnationlab/sk/models/HybridInitData;->a()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/stagnationlab/sk/i;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "pushToken"

    invoke-virtual {v1, v4, v3}, Lcom/stagnationlab/sk/util/f;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/stagnationlab/sk/util/f;

    move-result-object v1

    .line 96
    invoke-virtual {p1}, Lcom/stagnationlab/sk/models/HybridInitData;->b()Ljava/util/Map;

    move-result-object p1

    const-string v3, "rootData"

    invoke-virtual {v1, v3, p1}, Lcom/stagnationlab/sk/util/f;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/stagnationlab/sk/util/f;

    move-result-object p1

    iget-object v1, p0, Lcom/stagnationlab/sk/h/b$1;->b:Lcom/stagnationlab/sk/h/b;

    .line 97
    invoke-static {v1}, Lcom/stagnationlab/sk/h/b;->b(Lcom/stagnationlab/sk/h/b;)Lcom/stagnationlab/sk/a/a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/stagnationlab/sk/a/a;->b()Lcom/stagnationlab/sk/models/Account;

    move-result-object v1

    const-string v3, "account"

    invoke-virtual {p1, v3, v1}, Lcom/stagnationlab/sk/util/f;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/stagnationlab/sk/util/f;

    move-result-object p1

    const-string v1, "isPushTokenResetEnabled"

    .line 98
    invoke-virtual {p1, v1, v2}, Lcom/stagnationlab/sk/util/f;->a(Ljava/lang/String;Z)Lcom/stagnationlab/sk/util/f;

    move-result-object p1

    const-string v1, "os"

    const-string v3, "ANDROID"

    .line 99
    invoke-virtual {p1, v1, v3}, Lcom/stagnationlab/sk/util/f;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/stagnationlab/sk/util/f;

    move-result-object p1

    const-string v1, "isUserInfoRequestSupported"

    .line 100
    invoke-virtual {p1, v1, v2}, Lcom/stagnationlab/sk/util/f;->a(Ljava/lang/String;Z)Lcom/stagnationlab/sk/util/f;

    move-result-object p1

    iget-object v1, p0, Lcom/stagnationlab/sk/h/b$1;->b:Lcom/stagnationlab/sk/h/b;

    .line 101
    invoke-static {v1}, Lcom/stagnationlab/sk/h/b;->a(Lcom/stagnationlab/sk/h/b;)Lcom/stagnationlab/sk/h/d;

    move-result-object v1

    invoke-virtual {v1}, Lcom/stagnationlab/sk/h/d;->j()Ljava/lang/String;

    move-result-object v1

    const-string v2, "packageName"

    invoke-virtual {p1, v2, v1}, Lcom/stagnationlab/sk/util/f;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/stagnationlab/sk/util/f;

    move-result-object p1

    .line 76
    invoke-interface {v0, p1}, Lcom/stagnationlab/sk/h/c;->a(Lcom/stagnationlab/sk/util/f;)V

    return-void
.end method
