.class Lcom/stagnationlab/sk/h/b;
.super Ljava/lang/Object;
.source "JavascriptApi.java"


# instance fields
.field private a:Lcom/stagnationlab/sk/f/e;

.field private b:Lcom/stagnationlab/sk/h/d;

.field private c:Lcom/stagnationlab/sk/a/a;

.field private d:Lcom/stagnationlab/sk/a;


# direct methods
.method constructor <init>(Lcom/stagnationlab/sk/h/d;Lcom/stagnationlab/sk/a;)V
    .locals 0

    .line 62
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 63
    iput-object p1, p0, Lcom/stagnationlab/sk/h/b;->b:Lcom/stagnationlab/sk/h/d;

    .line 64
    iput-object p2, p0, Lcom/stagnationlab/sk/h/b;->d:Lcom/stagnationlab/sk/a;

    return-void
.end method

.method static synthetic a(Lcom/stagnationlab/sk/h/b;Lcom/stagnationlab/sk/f/e;)Lcom/stagnationlab/sk/f/e;
    .locals 0

    .line 54
    iput-object p1, p0, Lcom/stagnationlab/sk/h/b;->a:Lcom/stagnationlab/sk/f/e;

    return-object p1
.end method

.method static synthetic a(Lcom/stagnationlab/sk/h/b;)Lcom/stagnationlab/sk/h/d;
    .locals 0

    .line 54
    iget-object p0, p0, Lcom/stagnationlab/sk/h/b;->b:Lcom/stagnationlab/sk/h/d;

    return-object p0
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Lcom/stagnationlab/sk/h/c;)Lcom/stagnationlab/sk/util/d;
    .locals 1

    .line 283
    new-instance v0, Lcom/stagnationlab/sk/h/b$15;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/stagnationlab/sk/h/b$15;-><init>(Lcom/stagnationlab/sk/h/b;Ljava/lang/String;Ljava/lang/String;Lcom/stagnationlab/sk/h/c;)V

    return-object v0
.end method

.method private a()Lcom/stagnationlab/sk/util/f;
    .locals 2

    .line 250
    sget-object v0, Lcom/stagnationlab/sk/c/c;->b:Lcom/stagnationlab/sk/c/c;

    sget-object v1, Lcom/stagnationlab/sk/c/b;->I:Lcom/stagnationlab/sk/c/b;

    invoke-direct {p0, v0, v1}, Lcom/stagnationlab/sk/h/b;->a(Lcom/stagnationlab/sk/c/c;Lcom/stagnationlab/sk/c/b;)Lcom/stagnationlab/sk/util/f;

    move-result-object v0

    return-object v0
.end method

.method private a(Lcom/stagnationlab/sk/c/a;)Lcom/stagnationlab/sk/util/f;
    .locals 0

    .line 617
    invoke-static {p1}, Lcom/stagnationlab/sk/util/f;->a(Lcom/stagnationlab/sk/c/a;)Lcom/stagnationlab/sk/util/f;

    move-result-object p1

    return-object p1
.end method

.method private a(Lcom/stagnationlab/sk/c/c;Lcom/stagnationlab/sk/c/b;)Lcom/stagnationlab/sk/util/f;
    .locals 2

    .line 591
    new-instance v0, Lcom/stagnationlab/sk/util/f;

    invoke-direct {v0}, Lcom/stagnationlab/sk/util/f;-><init>()V

    .line 592
    invoke-virtual {p1}, Lcom/stagnationlab/sk/c/c;->toString()Ljava/lang/String;

    move-result-object p1

    const-string v1, "type"

    invoke-virtual {v0, v1, p1}, Lcom/stagnationlab/sk/util/f;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/stagnationlab/sk/util/f;

    move-result-object p1

    .line 593
    invoke-virtual {p2}, Lcom/stagnationlab/sk/c/b;->a()Ljava/lang/String;

    move-result-object p2

    const-string v0, "code"

    invoke-virtual {p1, v0, p2}, Lcom/stagnationlab/sk/util/f;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/stagnationlab/sk/util/f;

    move-result-object p1

    return-object p1
.end method

.method private a(Lcom/stagnationlab/sk/d/a;)Lcom/stagnationlab/sk/util/f;
    .locals 2

    .line 598
    invoke-virtual {p1}, Lcom/stagnationlab/sk/d/a;->b()Ljava/lang/Exception;

    move-result-object v0

    .line 600
    instance-of v1, v0, Ljava/net/SocketTimeoutException;

    if-eqz v1, :cond_0

    .line 601
    sget-object p1, Lcom/stagnationlab/sk/c/b;->x:Lcom/stagnationlab/sk/c/b;

    goto :goto_0

    .line 602
    :cond_0
    instance-of v0, v0, Ljava/io/IOException;

    if-eqz v0, :cond_1

    .line 603
    sget-object p1, Lcom/stagnationlab/sk/c/b;->w:Lcom/stagnationlab/sk/c/b;

    goto :goto_0

    .line 605
    :cond_1
    invoke-virtual {p1}, Lcom/stagnationlab/sk/d/a;->a()Lcom/stagnationlab/sk/c/b;

    move-result-object p1

    if-nez p1, :cond_2

    .line 608
    sget-object p1, Lcom/stagnationlab/sk/c/b;->A:Lcom/stagnationlab/sk/c/b;

    .line 612
    :cond_2
    :goto_0
    new-instance v0, Lcom/stagnationlab/sk/c/a;

    invoke-direct {v0, p1}, Lcom/stagnationlab/sk/c/a;-><init>(Lcom/stagnationlab/sk/c/b;)V

    .line 613
    invoke-virtual {v0}, Lcom/stagnationlab/sk/c/a;->k()Lcom/stagnationlab/sk/c/c;

    move-result-object p1

    invoke-virtual {v0}, Lcom/stagnationlab/sk/c/a;->a()Lcom/stagnationlab/sk/c/b;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/stagnationlab/sk/h/b;->a(Lcom/stagnationlab/sk/c/c;Lcom/stagnationlab/sk/c/b;)Lcom/stagnationlab/sk/util/f;

    move-result-object p1

    return-object p1
.end method

.method static synthetic a(Lcom/stagnationlab/sk/h/b;Lcom/stagnationlab/sk/c/a;)Lcom/stagnationlab/sk/util/f;
    .locals 0

    .line 54
    invoke-direct {p0, p1}, Lcom/stagnationlab/sk/h/b;->a(Lcom/stagnationlab/sk/c/a;)Lcom/stagnationlab/sk/util/f;

    move-result-object p0

    return-object p0
.end method

.method static synthetic a(Lcom/stagnationlab/sk/h/b;Lcom/stagnationlab/sk/d/a;)Lcom/stagnationlab/sk/util/f;
    .locals 0

    .line 54
    invoke-direct {p0, p1}, Lcom/stagnationlab/sk/h/b;->a(Lcom/stagnationlab/sk/d/a;)Lcom/stagnationlab/sk/util/f;

    move-result-object p0

    return-object p0
.end method

.method static synthetic a(Lcom/stagnationlab/sk/h/b;Ljava/lang/String;Ljava/lang/String;)Lcom/stagnationlab/sk/util/f;
    .locals 0

    .line 54
    invoke-direct {p0, p1, p2}, Lcom/stagnationlab/sk/h/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/stagnationlab/sk/util/f;

    move-result-object p0

    return-object p0
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)Lcom/stagnationlab/sk/util/f;
    .locals 3

    .line 577
    new-instance v0, Lcom/stagnationlab/sk/util/f;

    invoke-direct {v0}, Lcom/stagnationlab/sk/util/f;-><init>()V

    sget-object v1, Lcom/stagnationlab/sk/c/c;->c:Lcom/stagnationlab/sk/c/c;

    .line 578
    invoke-virtual {v1}, Lcom/stagnationlab/sk/c/c;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "type"

    invoke-virtual {v0, v2, v1}, Lcom/stagnationlab/sk/util/f;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/stagnationlab/sk/util/f;

    move-result-object v0

    const-string v1, "code"

    .line 579
    invoke-virtual {v0, v1, p1}, Lcom/stagnationlab/sk/util/f;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/stagnationlab/sk/util/f;

    move-result-object p1

    if-eqz p2, :cond_0

    .line 582
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    const-string v1, "id"

    .line 583
    invoke-interface {v0, v1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string p2, "data"

    .line 584
    invoke-virtual {p1, p2, v0}, Lcom/stagnationlab/sk/util/f;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/stagnationlab/sk/util/f;

    :cond_0
    return-object p1
.end method

.method static synthetic a(Lcom/stagnationlab/sk/h/b;Lcom/stagnationlab/sk/h/c;)V
    .locals 0

    .line 54
    invoke-direct {p0, p1}, Lcom/stagnationlab/sk/h/b;->a(Lcom/stagnationlab/sk/h/c;)V

    return-void
.end method

.method static synthetic a(Lcom/stagnationlab/sk/h/b;Ljava/lang/String;Lcom/stagnationlab/sk/h/c;)V
    .locals 0

    .line 54
    invoke-direct {p0, p1, p2}, Lcom/stagnationlab/sk/h/b;->a(Ljava/lang/String;Lcom/stagnationlab/sk/h/c;)V

    return-void
.end method

.method private a(Lcom/stagnationlab/sk/h/c;)V
    .locals 3

    .line 205
    new-instance v0, Lcom/stagnationlab/sk/util/f;

    invoke-direct {v0}, Lcom/stagnationlab/sk/util/f;-><init>()V

    .line 206
    invoke-static {}, Lcom/stagnationlab/sk/util/j;->a()Ljava/util/Map;

    move-result-object v1

    const-string v2, "rootData"

    invoke-virtual {v0, v2, v1}, Lcom/stagnationlab/sk/util/f;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/stagnationlab/sk/util/f;

    .line 208
    invoke-interface {p1, v0}, Lcom/stagnationlab/sk/h/c;->a(Lcom/stagnationlab/sk/util/f;)V

    return-void
.end method

.method private a(Ljava/lang/String;Lcom/stagnationlab/sk/h/c;)V
    .locals 1

    .line 623
    iget-object v0, p0, Lcom/stagnationlab/sk/h/b;->a:Lcom/stagnationlab/sk/f/e;

    if-nez v0, :cond_0

    .line 624
    invoke-direct {p0}, Lcom/stagnationlab/sk/h/b;->b()Lcom/stagnationlab/sk/util/f;

    move-result-object p1

    invoke-interface {p2, p1}, Lcom/stagnationlab/sk/h/c;->b(Lcom/stagnationlab/sk/util/f;)V

    return-void

    .line 629
    :cond_0
    invoke-virtual {v0, p1}, Lcom/stagnationlab/sk/f/e;->a(Ljava/lang/String;)V

    .line 630
    iget-object p1, p0, Lcom/stagnationlab/sk/h/b;->a:Lcom/stagnationlab/sk/f/e;

    new-instance v0, Lcom/stagnationlab/sk/h/b$9;

    invoke-direct {v0, p0, p2}, Lcom/stagnationlab/sk/h/b$9;-><init>(Lcom/stagnationlab/sk/h/b;Lcom/stagnationlab/sk/h/c;)V

    invoke-virtual {p1, v0}, Lcom/stagnationlab/sk/f/e;->a(Lcom/stagnationlab/sk/f/c;)V

    return-void
.end method

.method static synthetic b(Lcom/stagnationlab/sk/h/b;)Lcom/stagnationlab/sk/a/a;
    .locals 0

    .line 54
    iget-object p0, p0, Lcom/stagnationlab/sk/h/b;->c:Lcom/stagnationlab/sk/a/a;

    return-object p0
.end method

.method private b()Lcom/stagnationlab/sk/util/f;
    .locals 2

    .line 254
    sget-object v0, Lcom/stagnationlab/sk/c/c;->b:Lcom/stagnationlab/sk/c/c;

    sget-object v1, Lcom/stagnationlab/sk/c/b;->M:Lcom/stagnationlab/sk/c/b;

    invoke-direct {p0, v0, v1}, Lcom/stagnationlab/sk/h/b;->a(Lcom/stagnationlab/sk/c/c;Lcom/stagnationlab/sk/c/b;)Lcom/stagnationlab/sk/util/f;

    move-result-object v0

    return-object v0
.end method

.method private c()V
    .locals 1

    .line 647
    iget-object v0, p0, Lcom/stagnationlab/sk/h/b;->a:Lcom/stagnationlab/sk/f/e;

    if-eqz v0, :cond_0

    .line 648
    invoke-virtual {v0}, Lcom/stagnationlab/sk/f/e;->a()V

    const/4 v0, 0x0

    .line 649
    iput-object v0, p0, Lcom/stagnationlab/sk/h/b;->a:Lcom/stagnationlab/sk/f/e;

    goto :goto_0

    .line 651
    :cond_0
    invoke-static {}, Lcom/stagnationlab/sk/f/e;->b()V

    .line 654
    :goto_0
    iget-object v0, p0, Lcom/stagnationlab/sk/h/b;->b:Lcom/stagnationlab/sk/h/d;

    invoke-virtual {v0}, Lcom/stagnationlab/sk/h/d;->e()V

    return-void
.end method

.method static synthetic c(Lcom/stagnationlab/sk/h/b;)V
    .locals 0

    .line 54
    invoke-direct {p0}, Lcom/stagnationlab/sk/h/b;->c()V

    return-void
.end method

.method static synthetic d(Lcom/stagnationlab/sk/h/b;)Lcom/stagnationlab/sk/f/e;
    .locals 0

    .line 54
    iget-object p0, p0, Lcom/stagnationlab/sk/h/b;->a:Lcom/stagnationlab/sk/f/e;

    return-object p0
.end method


# virtual methods
.method a(Lcom/stagnationlab/sk/a/a;)V
    .locals 0

    .line 68
    iput-object p1, p0, Lcom/stagnationlab/sk/h/b;->c:Lcom/stagnationlab/sk/a/a;

    return-void
.end method

.method public cancelInteractiveUpgrade(Lcom/stagnationlab/sk/util/e;Lcom/stagnationlab/sk/h/c;)V
    .locals 1

    .line 563
    iget-object p1, p0, Lcom/stagnationlab/sk/h/b;->c:Lcom/stagnationlab/sk/a/a;

    new-instance v0, Lcom/stagnationlab/sk/h/b$8;

    invoke-direct {v0, p0, p2}, Lcom/stagnationlab/sk/h/b$8;-><init>(Lcom/stagnationlab/sk/h/b;Lcom/stagnationlab/sk/h/c;)V

    invoke-virtual {p1, v0}, Lcom/stagnationlab/sk/a/a;->a(Lcom/stagnationlab/sk/a/a/b;)V

    return-void
.end method

.method public cancelRegistration(Lcom/stagnationlab/sk/util/e;Lcom/stagnationlab/sk/h/c;)V
    .locals 0

    const/4 p1, 0x0

    .line 517
    invoke-interface {p2, p1}, Lcom/stagnationlab/sk/h/c;->a(Lcom/stagnationlab/sk/util/f;)V

    .line 518
    invoke-direct {p0}, Lcom/stagnationlab/sk/h/b;->c()V

    return-void
.end method

.method public checkRoot(Lcom/stagnationlab/sk/util/e;Lcom/stagnationlab/sk/h/c;)V
    .locals 2

    .line 189
    iget-object v0, p0, Lcom/stagnationlab/sk/h/b;->c:Lcom/stagnationlab/sk/a/a;

    const-string v1, "force"

    invoke-virtual {p1, v1}, Lcom/stagnationlab/sk/util/e;->b(Ljava/lang/String;)Z

    move-result p1

    new-instance v1, Lcom/stagnationlab/sk/h/b$12;

    invoke-direct {v1, p0, p2}, Lcom/stagnationlab/sk/h/b$12;-><init>(Lcom/stagnationlab/sk/h/b;Lcom/stagnationlab/sk/h/c;)V

    invoke-virtual {v0, p1, v1}, Lcom/stagnationlab/sk/a/a;->a(ZLcom/stagnationlab/sk/a/a/f;)V

    return-void
.end method

.method public checkTransaction(Lcom/stagnationlab/sk/util/e;Lcom/stagnationlab/sk/h/c;)V
    .locals 1

    .line 149
    iget-object p1, p0, Lcom/stagnationlab/sk/h/b;->b:Lcom/stagnationlab/sk/h/d;

    new-instance v0, Lcom/stagnationlab/sk/h/b$10;

    invoke-direct {v0, p0, p2}, Lcom/stagnationlab/sk/h/b$10;-><init>(Lcom/stagnationlab/sk/h/b;Lcom/stagnationlab/sk/h/c;)V

    invoke-virtual {p1, v0}, Lcom/stagnationlab/sk/h/d;->a(Lcom/stagnationlab/sk/d;)V

    return-void
.end method

.method public createAccount(Lcom/stagnationlab/sk/util/e;Lcom/stagnationlab/sk/h/c;)V
    .locals 2

    .line 422
    iget-object v0, p0, Lcom/stagnationlab/sk/h/b;->a:Lcom/stagnationlab/sk/f/e;

    if-nez v0, :cond_0

    .line 423
    invoke-direct {p0}, Lcom/stagnationlab/sk/h/b;->b()Lcom/stagnationlab/sk/util/f;

    move-result-object p1

    invoke-interface {p2, p1}, Lcom/stagnationlab/sk/h/c;->b(Lcom/stagnationlab/sk/util/f;)V

    return-void

    .line 428
    :cond_0
    iget-object v0, p0, Lcom/stagnationlab/sk/h/b;->b:Lcom/stagnationlab/sk/h/d;

    invoke-virtual {v0}, Lcom/stagnationlab/sk/h/d;->d()V

    .line 430
    iget-object v0, p0, Lcom/stagnationlab/sk/h/b;->a:Lcom/stagnationlab/sk/f/e;

    const-string v1, "isRetry"

    .line 431
    invoke-virtual {p1, v1}, Lcom/stagnationlab/sk/util/e;->b(Ljava/lang/String;)Z

    move-result p1

    new-instance v1, Lcom/stagnationlab/sk/h/b$4;

    invoke-direct {v1, p0, p2}, Lcom/stagnationlab/sk/h/b$4;-><init>(Lcom/stagnationlab/sk/h/b;Lcom/stagnationlab/sk/h/c;)V

    .line 430
    invoke-virtual {v0, p1, v1}, Lcom/stagnationlab/sk/f/e;->a(ZLcom/stagnationlab/sk/f/a;)V

    .line 450
    iget-object p1, p0, Lcom/stagnationlab/sk/h/b;->a:Lcom/stagnationlab/sk/f/e;

    invoke-virtual {p1}, Lcom/stagnationlab/sk/f/e;->c()Z

    move-result p1

    if-nez p1, :cond_1

    .line 451
    iget-object p1, p0, Lcom/stagnationlab/sk/h/b;->b:Lcom/stagnationlab/sk/h/d;

    new-instance p2, Lcom/stagnationlab/sk/h/b$5;

    invoke-direct {p2, p0}, Lcom/stagnationlab/sk/h/b$5;-><init>(Lcom/stagnationlab/sk/h/b;)V

    invoke-virtual {p1, p2}, Lcom/stagnationlab/sk/h/d;->a(Lcom/stagnationlab/sk/fcm/d;)V

    :cond_1
    return-void
.end method

.method public createFakeTransaction(Lcom/stagnationlab/sk/util/e;Lcom/stagnationlab/sk/h/c;)V
    .locals 0

    .line 522
    iget-object p1, p0, Lcom/stagnationlab/sk/h/b;->b:Lcom/stagnationlab/sk/h/d;

    invoke-virtual {p1}, Lcom/stagnationlab/sk/h/d;->c()V

    return-void
.end method

.method public deleteAccount(Lcom/stagnationlab/sk/util/e;Lcom/stagnationlab/sk/h/c;)V
    .locals 2

    const-string v0, "accountUuid"

    .line 225
    invoke-virtual {p1, v0}, Lcom/stagnationlab/sk/util/e;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 226
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Deleting account "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "JavascriptApi"

    invoke-static {v1, v0}, Lcom/stagnationlab/sk/f;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 228
    iget-object v0, p0, Lcom/stagnationlab/sk/h/b;->c:Lcom/stagnationlab/sk/a/a;

    if-nez v0, :cond_0

    .line 229
    invoke-direct {p0}, Lcom/stagnationlab/sk/h/b;->a()Lcom/stagnationlab/sk/util/f;

    move-result-object p1

    invoke-interface {p2, p1}, Lcom/stagnationlab/sk/h/c;->b(Lcom/stagnationlab/sk/util/f;)V

    return-void

    .line 234
    :cond_0
    new-instance v1, Lcom/stagnationlab/sk/h/b$14;

    invoke-direct {v1, p0, p2}, Lcom/stagnationlab/sk/h/b$14;-><init>(Lcom/stagnationlab/sk/h/b;Lcom/stagnationlab/sk/h/c;)V

    invoke-virtual {v0, p1, v1}, Lcom/stagnationlab/sk/a/a;->a(Ljava/lang/String;Lcom/stagnationlab/sk/a/a/j;)V

    return-void
.end method

.method public encryptKeys(Lcom/stagnationlab/sk/util/e;Lcom/stagnationlab/sk/h/c;)V
    .locals 3

    .line 366
    iget-object v0, p0, Lcom/stagnationlab/sk/h/b;->a:Lcom/stagnationlab/sk/f/e;

    if-nez v0, :cond_0

    .line 367
    invoke-direct {p0}, Lcom/stagnationlab/sk/h/b;->b()Lcom/stagnationlab/sk/util/f;

    move-result-object p1

    invoke-interface {p2, p1}, Lcom/stagnationlab/sk/h/c;->b(Lcom/stagnationlab/sk/util/f;)V

    return-void

    :cond_0
    const-string v1, "authPin"

    .line 373
    invoke-virtual {p1, v1}, Lcom/stagnationlab/sk/util/e;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "signPin"

    .line 374
    invoke-virtual {p1, v2}, Lcom/stagnationlab/sk/util/e;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    new-instance v2, Lcom/stagnationlab/sk/h/b$2;

    invoke-direct {v2, p0, p2}, Lcom/stagnationlab/sk/h/b$2;-><init>(Lcom/stagnationlab/sk/h/b;Lcom/stagnationlab/sk/h/c;)V

    .line 372
    invoke-virtual {v0, v1, p1, v2}, Lcom/stagnationlab/sk/f/e;->a(Ljava/lang/String;Ljava/lang/String;Lcom/stagnationlab/sk/a/a/l;)V

    return-void
.end method

.method public getRegistrationToken(Lcom/stagnationlab/sk/util/e;Lcom/stagnationlab/sk/h/c;)V
    .locals 1

    .line 391
    iget-object p1, p0, Lcom/stagnationlab/sk/h/b;->a:Lcom/stagnationlab/sk/f/e;

    if-nez p1, :cond_0

    .line 392
    invoke-direct {p0}, Lcom/stagnationlab/sk/h/b;->b()Lcom/stagnationlab/sk/util/f;

    move-result-object p1

    invoke-interface {p2, p1}, Lcom/stagnationlab/sk/h/c;->b(Lcom/stagnationlab/sk/util/f;)V

    return-void

    .line 397
    :cond_0
    new-instance v0, Lcom/stagnationlab/sk/h/b$3;

    invoke-direct {v0, p0, p2}, Lcom/stagnationlab/sk/h/b$3;-><init>(Lcom/stagnationlab/sk/h/b;Lcom/stagnationlab/sk/h/c;)V

    invoke-virtual {p1, v0}, Lcom/stagnationlab/sk/f/e;->a(Lcom/stagnationlab/sk/a/a/r;)V

    return-void
.end method

.method public httpGet(Lcom/stagnationlab/sk/util/e;Lcom/stagnationlab/sk/h/c;)V
    .locals 3

    const-string v0, "url"

    .line 258
    invoke-virtual {p1, v0}, Lcom/stagnationlab/sk/util/e;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :try_start_0
    const-string v1, "sendDeviceFingerPrint"

    .line 261
    invoke-virtual {p1, v1}, Lcom/stagnationlab/sk/util/e;->b(Ljava/lang/String;)Z

    move-result p1

    const-string v1, "GET"

    invoke-direct {p0, v1, v0, p2}, Lcom/stagnationlab/sk/h/b;->a(Ljava/lang/String;Ljava/lang/String;Lcom/stagnationlab/sk/h/c;)Lcom/stagnationlab/sk/util/d;

    move-result-object v1

    invoke-static {v0, p1, v1}, Lcom/stagnationlab/sk/util/c;->a(Ljava/lang/String;ZLcom/stagnationlab/sk/util/d;)Lokhttp3/e;
    :try_end_0
    .catch Lcom/stagnationlab/sk/d/a; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 263
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "GET request to \'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "\' failed"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "JavascriptApi"

    invoke-static {v1, v0, p1}, Lcom/stagnationlab/sk/f;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 265
    invoke-direct {p0, p1}, Lcom/stagnationlab/sk/h/b;->a(Lcom/stagnationlab/sk/d/a;)Lcom/stagnationlab/sk/util/f;

    move-result-object p1

    invoke-interface {p2, p1}, Lcom/stagnationlab/sk/h/c;->b(Lcom/stagnationlab/sk/util/f;)V

    :goto_0
    return-void
.end method

.method public httpPost(Lcom/stagnationlab/sk/util/e;Lcom/stagnationlab/sk/h/c;)V
    .locals 3

    const-string v0, "url"

    .line 271
    invoke-virtual {p1, v0}, Lcom/stagnationlab/sk/util/e;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :try_start_0
    const-string v1, "params"

    .line 274
    const-class v2, Ljava/util/Map;

    invoke-virtual {p1, v1, v2}, Lcom/stagnationlab/sk/util/e;->b(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/Map;

    const-string v1, "POST"

    invoke-direct {p0, v1, v0, p2}, Lcom/stagnationlab/sk/h/b;->a(Ljava/lang/String;Ljava/lang/String;Lcom/stagnationlab/sk/h/c;)Lcom/stagnationlab/sk/util/d;

    move-result-object v1

    invoke-static {v0, p1, v1}, Lcom/stagnationlab/sk/util/c;->a(Ljava/lang/String;Ljava/util/Map;Lcom/stagnationlab/sk/util/d;)Lokhttp3/e;
    :try_end_0
    .catch Lcom/stagnationlab/sk/d/a; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 276
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "POST request to \'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "\' failed"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "JavascriptApi"

    invoke-static {v1, v0, p1}, Lcom/stagnationlab/sk/f;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 278
    invoke-direct {p0, p1}, Lcom/stagnationlab/sk/h/b;->a(Lcom/stagnationlab/sk/d/a;)Lcom/stagnationlab/sk/util/f;

    move-result-object p1

    invoke-interface {p2, p1}, Lcom/stagnationlab/sk/h/c;->b(Lcom/stagnationlab/sk/util/f;)V

    :goto_0
    return-void
.end method

.method public init(Lcom/stagnationlab/sk/util/e;Lcom/stagnationlab/sk/h/c;)V
    .locals 1

    .line 72
    iget-object p1, p0, Lcom/stagnationlab/sk/h/b;->b:Lcom/stagnationlab/sk/h/d;

    new-instance v0, Lcom/stagnationlab/sk/h/b$1;

    invoke-direct {v0, p0, p2}, Lcom/stagnationlab/sk/h/b$1;-><init>(Lcom/stagnationlab/sk/h/b;Lcom/stagnationlab/sk/h/c;)V

    invoke-virtual {p1, v0}, Lcom/stagnationlab/sk/h/d;->a(Lcom/stagnationlab/sk/a/a/u;)V

    return-void
.end method

.method public initRegistration(Lcom/stagnationlab/sk/util/e;Lcom/stagnationlab/sk/h/c;)V
    .locals 3

    const-string v0, "authenticationUrl"

    .line 314
    invoke-virtual {p1, v0}, Lcom/stagnationlab/sk/util/e;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "providerName"

    .line 315
    invoke-virtual {p1, v1}, Lcom/stagnationlab/sk/util/e;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "identityToken"

    .line 316
    invoke-virtual {p1, v2}, Lcom/stagnationlab/sk/util/e;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 318
    invoke-direct {p0}, Lcom/stagnationlab/sk/h/b;->c()V

    .line 320
    new-instance v2, Lcom/stagnationlab/sk/f/e;

    invoke-direct {v2}, Lcom/stagnationlab/sk/f/e;-><init>()V

    iput-object v2, p0, Lcom/stagnationlab/sk/h/b;->a:Lcom/stagnationlab/sk/f/e;

    if-eqz p1, :cond_0

    .line 323
    invoke-direct {p0, p1, p2}, Lcom/stagnationlab/sk/h/b;->a(Ljava/lang/String;Lcom/stagnationlab/sk/h/c;)V

    goto :goto_0

    :cond_0
    if-nez v0, :cond_1

    const/4 p1, 0x0

    .line 325
    invoke-direct {p0, p1, p2}, Lcom/stagnationlab/sk/h/b;->a(Ljava/lang/String;Lcom/stagnationlab/sk/h/c;)V

    goto :goto_0

    .line 327
    :cond_1
    iget-object p1, p0, Lcom/stagnationlab/sk/h/b;->b:Lcom/stagnationlab/sk/h/d;

    new-instance v2, Lcom/stagnationlab/sk/h/b$16;

    invoke-direct {v2, p0, p2}, Lcom/stagnationlab/sk/h/b$16;-><init>(Lcom/stagnationlab/sk/h/b;Lcom/stagnationlab/sk/h/c;)V

    invoke-virtual {p1, v1, v0, v2}, Lcom/stagnationlab/sk/h/d;->a(Ljava/lang/String;Ljava/lang/String;Lcom/stagnationlab/sk/g/d;)V

    :goto_0
    return-void
.end method

.method public launchUserVoice(Lcom/stagnationlab/sk/util/e;Lcom/stagnationlab/sk/h/c;)V
    .locals 3

    .line 526
    iget-object v0, p0, Lcom/stagnationlab/sk/h/b;->b:Lcom/stagnationlab/sk/h/d;

    const-string v1, "type"

    invoke-virtual {p1, v1}, Lcom/stagnationlab/sk/util/e;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "errorCode"

    invoke-virtual {p1, v2}, Lcom/stagnationlab/sk/util/e;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, v1, p1}, Lcom/stagnationlab/sk/h/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    const/4 p1, 0x0

    .line 528
    invoke-interface {p2, p1}, Lcom/stagnationlab/sk/h/c;->a(Lcom/stagnationlab/sk/util/f;)V

    return-void
.end method

.method public logEvent(Lcom/stagnationlab/sk/util/e;Lcom/stagnationlab/sk/h/c;)V
    .locals 1

    const-string v0, "event"

    .line 184
    invoke-virtual {p1, v0}, Lcom/stagnationlab/sk/util/e;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lcom/stagnationlab/sk/util/i;->a(Ljava/lang/String;)V

    const/4 p1, 0x0

    .line 185
    invoke-interface {p2, p1}, Lcom/stagnationlab/sk/h/c;->a(Lcom/stagnationlab/sk/util/f;)V

    return-void
.end method

.method public requestUserInfo(Lcom/stagnationlab/sk/util/e;Lcom/stagnationlab/sk/h/c;)V
    .locals 2

    const-string v0, "email"

    .line 465
    invoke-virtual {p1, v0}, Lcom/stagnationlab/sk/util/e;->b(Ljava/lang/String;)Z

    move-result v0

    const-string v1, "phone"

    .line 466
    invoke-virtual {p1, v1}, Lcom/stagnationlab/sk/util/e;->b(Ljava/lang/String;)Z

    move-result p1

    .line 468
    iget-object v1, p0, Lcom/stagnationlab/sk/h/b;->b:Lcom/stagnationlab/sk/h/d;

    invoke-virtual {v1, v0, p1, p2}, Lcom/stagnationlab/sk/h/d;->a(ZZLcom/stagnationlab/sk/h/c;)V

    return-void
.end method

.method public resetPushToken(Lcom/stagnationlab/sk/util/e;Lcom/stagnationlab/sk/h/c;)V
    .locals 1

    .line 212
    iget-object p1, p0, Lcom/stagnationlab/sk/h/b;->b:Lcom/stagnationlab/sk/h/d;

    new-instance v0, Lcom/stagnationlab/sk/h/b$13;

    invoke-direct {v0, p0, p2}, Lcom/stagnationlab/sk/h/b$13;-><init>(Lcom/stagnationlab/sk/h/b;Lcom/stagnationlab/sk/h/c;)V

    invoke-virtual {p1, v0}, Lcom/stagnationlab/sk/h/d;->a(Lcom/stagnationlab/sk/fcm/d;)V

    return-void
.end method

.method public saveLegalGuardianData(Lcom/stagnationlab/sk/util/e;Lcom/stagnationlab/sk/h/c;)V
    .locals 3

    .line 129
    iget-object v0, p0, Lcom/stagnationlab/sk/h/b;->a:Lcom/stagnationlab/sk/f/e;

    if-eqz v0, :cond_0

    .line 130
    const-class v1, [Lcom/stagnationlab/sk/models/LegalGuardian;

    const-string v2, "legalGuardians"

    invoke-virtual {p1, v2, v1}, Lcom/stagnationlab/sk/util/e;->a(Ljava/lang/String;Ljava/lang/Class;)Ljava/util/List;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/stagnationlab/sk/f/e;->a(Ljava/util/List;)V

    :cond_0
    const/4 p1, 0x0

    .line 133
    invoke-interface {p2, p1}, Lcom/stagnationlab/sk/h/c;->a(Lcom/stagnationlab/sk/util/f;)V

    return-void
.end method

.method public setHasSeenSuccess(Lcom/stagnationlab/sk/util/e;Lcom/stagnationlab/sk/h/c;)V
    .locals 0

    .line 472
    iget-object p1, p0, Lcom/stagnationlab/sk/h/b;->c:Lcom/stagnationlab/sk/a/a;

    if-nez p1, :cond_0

    .line 473
    invoke-direct {p0}, Lcom/stagnationlab/sk/h/b;->a()Lcom/stagnationlab/sk/util/f;

    move-result-object p1

    invoke-interface {p2, p1}, Lcom/stagnationlab/sk/h/c;->b(Lcom/stagnationlab/sk/util/f;)V

    return-void

    .line 478
    :cond_0
    invoke-virtual {p1}, Lcom/stagnationlab/sk/a/a;->g()V

    const/4 p1, 0x0

    .line 479
    invoke-interface {p2, p1}, Lcom/stagnationlab/sk/h/c;->a(Lcom/stagnationlab/sk/util/f;)V

    return-void
.end method

.method public setScreenName(Lcom/stagnationlab/sk/util/e;Lcom/stagnationlab/sk/h/c;)V
    .locals 2

    .line 541
    iget-object v0, p0, Lcom/stagnationlab/sk/h/b;->b:Lcom/stagnationlab/sk/h/d;

    const-string v1, "screenName"

    invoke-virtual {p1, v1}, Lcom/stagnationlab/sk/util/e;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/stagnationlab/sk/h/d;->c(Ljava/lang/String;)V

    .line 542
    iget-object p1, p0, Lcom/stagnationlab/sk/h/b;->b:Lcom/stagnationlab/sk/h/d;

    invoke-virtual {p1}, Lcom/stagnationlab/sk/h/d;->i()V

    const/4 p1, 0x0

    .line 543
    invoke-interface {p2, p1}, Lcom/stagnationlab/sk/h/c;->a(Lcom/stagnationlab/sk/util/f;)V

    return-void
.end method

.method public shareText(Lcom/stagnationlab/sk/util/e;Lcom/stagnationlab/sk/h/c;)V
    .locals 3

    .line 119
    iget-object v0, p0, Lcom/stagnationlab/sk/h/b;->b:Lcom/stagnationlab/sk/h/d;

    const-string v1, "subject"

    invoke-virtual {p1, v1}, Lcom/stagnationlab/sk/util/e;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "text"

    invoke-virtual {p1, v2}, Lcom/stagnationlab/sk/util/e;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, v1, p1}, Lcom/stagnationlab/sk/h/d;->b(Ljava/lang/String;Ljava/lang/String;)V

    const/4 p1, 0x0

    .line 120
    invoke-interface {p2, p1}, Lcom/stagnationlab/sk/h/c;->a(Lcom/stagnationlab/sk/util/f;)V

    return-void
.end method

.method public showWebView(Lcom/stagnationlab/sk/util/e;Lcom/stagnationlab/sk/h/c;)V
    .locals 0

    .line 114
    iget-object p1, p0, Lcom/stagnationlab/sk/h/b;->b:Lcom/stagnationlab/sk/h/d;

    invoke-virtual {p1}, Lcom/stagnationlab/sk/h/d;->b()V

    const/4 p1, 0x0

    .line 115
    invoke-interface {p2, p1}, Lcom/stagnationlab/sk/h/c;->a(Lcom/stagnationlab/sk/util/f;)V

    return-void
.end method

.method public startOtpRetriever(Lcom/stagnationlab/sk/util/e;Lcom/stagnationlab/sk/h/c;)V
    .locals 0

    .line 418
    iget-object p1, p0, Lcom/stagnationlab/sk/h/b;->b:Lcom/stagnationlab/sk/h/d;

    invoke-virtual {p1, p2}, Lcom/stagnationlab/sk/h/d;->a(Lcom/stagnationlab/sk/h/c;)V

    return-void
.end method

.method public submitCsr(Lcom/stagnationlab/sk/util/e;Lcom/stagnationlab/sk/h/c;)V
    .locals 3

    .line 483
    iget-object v0, p0, Lcom/stagnationlab/sk/h/b;->a:Lcom/stagnationlab/sk/f/e;

    if-nez v0, :cond_0

    .line 484
    invoke-direct {p0}, Lcom/stagnationlab/sk/h/b;->b()Lcom/stagnationlab/sk/util/f;

    move-result-object p1

    invoke-interface {p2, p1}, Lcom/stagnationlab/sk/h/c;->b(Lcom/stagnationlab/sk/util/f;)V

    return-void

    :cond_0
    const-string v1, "type"

    .line 490
    invoke-virtual {p1, v1}, Lcom/stagnationlab/sk/util/e;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "pin"

    .line 491
    invoke-virtual {p1, v2}, Lcom/stagnationlab/sk/util/e;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    new-instance v2, Lcom/stagnationlab/sk/h/b$6;

    invoke-direct {v2, p0, p2}, Lcom/stagnationlab/sk/h/b$6;-><init>(Lcom/stagnationlab/sk/h/b;Lcom/stagnationlab/sk/h/c;)V

    .line 489
    invoke-virtual {v0, v1, p1, v2}, Lcom/stagnationlab/sk/f/e;->a(Ljava/lang/String;Ljava/lang/String;Lcom/stagnationlab/sk/f/f;)V

    return-void
.end method

.method public track(Lcom/stagnationlab/sk/util/e;Lcom/stagnationlab/sk/h/c;)V
    .locals 2

    .line 137
    iget-object v0, p0, Lcom/stagnationlab/sk/h/b;->d:Lcom/stagnationlab/sk/a;

    if-eqz v0, :cond_0

    const-string v1, "screenName"

    .line 138
    invoke-virtual {p1, v1}, Lcom/stagnationlab/sk/util/e;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/stagnationlab/sk/a;->a(Ljava/lang/String;)V

    :cond_0
    const/4 p1, 0x0

    .line 140
    invoke-interface {p2, p1}, Lcom/stagnationlab/sk/h/c;->a(Lcom/stagnationlab/sk/util/f;)V

    return-void
.end method

.method public triggerCrash(Lcom/stagnationlab/sk/util/e;Lcom/stagnationlab/sk/h/c;)V
    .locals 1

    .line 532
    new-instance p1, Ljava/lang/Thread;

    new-instance v0, Lcom/stagnationlab/sk/h/b$7;

    invoke-direct {v0, p0}, Lcom/stagnationlab/sk/h/b$7;-><init>(Lcom/stagnationlab/sk/h/b;)V

    invoke-direct {p1, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 536
    invoke-virtual {p1}, Ljava/lang/Thread;->start()V

    const/4 p1, 0x0

    .line 537
    invoke-interface {p2, p1}, Lcom/stagnationlab/sk/h/c;->a(Lcom/stagnationlab/sk/util/f;)V

    return-void
.end method

.method public updateAccountStatus(Lcom/stagnationlab/sk/util/e;Lcom/stagnationlab/sk/h/c;)V
    .locals 2

    .line 162
    iget-object p1, p0, Lcom/stagnationlab/sk/h/b;->c:Lcom/stagnationlab/sk/a/a;

    if-nez p1, :cond_0

    .line 163
    invoke-direct {p0}, Lcom/stagnationlab/sk/h/b;->a()Lcom/stagnationlab/sk/util/f;

    move-result-object p1

    invoke-interface {p2, p1}, Lcom/stagnationlab/sk/h/c;->b(Lcom/stagnationlab/sk/util/f;)V

    return-void

    :cond_0
    const/4 v0, 0x0

    .line 168
    new-instance v1, Lcom/stagnationlab/sk/h/b$11;

    invoke-direct {v1, p0, p2}, Lcom/stagnationlab/sk/h/b$11;-><init>(Lcom/stagnationlab/sk/h/b;Lcom/stagnationlab/sk/h/c;)V

    invoke-virtual {p1, v0, v1}, Lcom/stagnationlab/sk/a/a;->a(ZLcom/stagnationlab/sk/a/a/y;)V

    return-void
.end method

.method public updateAllowScreenshot(Lcom/stagnationlab/sk/util/e;Lcom/stagnationlab/sk/h/c;)V
    .locals 1

    const-string v0, "allowScreenshot"

    .line 547
    invoke-virtual {p1, v0}, Lcom/stagnationlab/sk/util/e;->b(Ljava/lang/String;)Z

    move-result p1

    invoke-static {p1}, Lcom/stagnationlab/sk/a/d;->c(Z)V

    .line 548
    iget-object p1, p0, Lcom/stagnationlab/sk/h/b;->b:Lcom/stagnationlab/sk/h/d;

    invoke-virtual {p1}, Lcom/stagnationlab/sk/h/d;->i()V

    const/4 p1, 0x0

    .line 549
    invoke-interface {p2, p1}, Lcom/stagnationlab/sk/h/c;->a(Lcom/stagnationlab/sk/util/f;)V

    return-void
.end method

.method public updateLocale(Lcom/stagnationlab/sk/util/e;Lcom/stagnationlab/sk/h/c;)V
    .locals 2

    .line 144
    iget-object v0, p0, Lcom/stagnationlab/sk/h/b;->b:Lcom/stagnationlab/sk/h/d;

    const-string v1, "locale"

    invoke-virtual {p1, v1}, Lcom/stagnationlab/sk/util/e;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/stagnationlab/sk/h/d;->a(Ljava/lang/String;)V

    const/4 p1, 0x0

    .line 145
    invoke-interface {p2, p1}, Lcom/stagnationlab/sk/h/c;->a(Lcom/stagnationlab/sk/util/f;)V

    return-void
.end method

.method public updateNotificationSound(Lcom/stagnationlab/sk/util/e;Lcom/stagnationlab/sk/h/c;)V
    .locals 1

    const-string v0, "notificationSound"

    .line 558
    invoke-virtual {p1, v0}, Lcom/stagnationlab/sk/util/e;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lcom/stagnationlab/sk/b/a;->valueOf(Ljava/lang/String;)Lcom/stagnationlab/sk/b/a;

    move-result-object p1

    invoke-static {p1}, Lcom/stagnationlab/sk/a/d;->a(Lcom/stagnationlab/sk/b/a;)V

    const/4 p1, 0x0

    .line 559
    invoke-interface {p2, p1}, Lcom/stagnationlab/sk/h/c;->a(Lcom/stagnationlab/sk/util/f;)V

    return-void
.end method

.method public updatePushBehaviour(Lcom/stagnationlab/sk/util/e;Lcom/stagnationlab/sk/h/c;)V
    .locals 2

    .line 553
    iget-object v0, p0, Lcom/stagnationlab/sk/h/b;->b:Lcom/stagnationlab/sk/h/d;

    const-string v1, "pushBehaviour"

    invoke-virtual {p1, v1}, Lcom/stagnationlab/sk/util/e;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lcom/stagnationlab/sk/b/b;->valueOf(Ljava/lang/String;)Lcom/stagnationlab/sk/b/b;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/stagnationlab/sk/h/d;->a(Lcom/stagnationlab/sk/b/b;)V

    const/4 p1, 0x0

    .line 554
    invoke-interface {p2, p1}, Lcom/stagnationlab/sk/h/c;->a(Lcom/stagnationlab/sk/util/f;)V

    return-void
.end method

.method public updateShowNotificationInfo(Lcom/stagnationlab/sk/util/e;Lcom/stagnationlab/sk/h/c;)V
    .locals 1

    const-string v0, "show"

    .line 124
    invoke-virtual {p1, v0}, Lcom/stagnationlab/sk/util/e;->b(Ljava/lang/String;)Z

    move-result p1

    invoke-static {p1}, Lcom/stagnationlab/sk/a/d;->a(Z)V

    const/4 p1, 0x0

    .line 125
    invoke-interface {p2, p1}, Lcom/stagnationlab/sk/h/c;->a(Lcom/stagnationlab/sk/util/f;)V

    return-void
.end method

.method public validatePin(Lcom/stagnationlab/sk/util/e;Lcom/stagnationlab/sk/h/c;)V
    .locals 2

    .line 346
    iget-object v0, p0, Lcom/stagnationlab/sk/h/b;->c:Lcom/stagnationlab/sk/a/a;

    if-nez v0, :cond_0

    .line 347
    invoke-direct {p0}, Lcom/stagnationlab/sk/h/b;->a()Lcom/stagnationlab/sk/util/f;

    move-result-object p1

    invoke-interface {p2, p1}, Lcom/stagnationlab/sk/h/c;->b(Lcom/stagnationlab/sk/util/f;)V

    return-void

    :cond_0
    const-string v1, "pin"

    .line 352
    invoke-virtual {p1, v1}, Lcom/stagnationlab/sk/util/e;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    new-instance v1, Lcom/stagnationlab/sk/h/b$17;

    invoke-direct {v1, p0, p2}, Lcom/stagnationlab/sk/h/b$17;-><init>(Lcom/stagnationlab/sk/h/b;Lcom/stagnationlab/sk/h/c;)V

    invoke-virtual {v0, p1, v1}, Lcom/stagnationlab/sk/a/a;->a(Ljava/lang/String;Lcom/stagnationlab/sk/a/a/z;)V

    return-void
.end method
