.class Lcom/stagnationlab/sk/h/e$1;
.super Landroid/webkit/WebViewClient;
.source "WebViewWrapper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/stagnationlab/sk/h/e;-><init>(Landroid/content/Context;Landroid/widget/LinearLayout;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/stagnationlab/sk/h/e;


# direct methods
.method constructor <init>(Lcom/stagnationlab/sk/h/e;)V
    .locals 0

    .line 51
    iput-object p1, p0, Lcom/stagnationlab/sk/h/e$1;->a:Lcom/stagnationlab/sk/h/e;

    invoke-direct {p0}, Landroid/webkit/WebViewClient;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceivedError(Landroid/webkit/WebView;Landroid/webkit/WebResourceRequest;Landroid/webkit/WebResourceError;)V
    .locals 0

    .line 54
    invoke-super {p0, p1, p2, p3}, Landroid/webkit/WebViewClient;->onReceivedError(Landroid/webkit/WebView;Landroid/webkit/WebResourceRequest;Landroid/webkit/WebResourceError;)V

    .line 55
    iget-object p1, p0, Lcom/stagnationlab/sk/h/e$1;->a:Lcom/stagnationlab/sk/h/e;

    invoke-interface {p2}, Landroid/webkit/WebResourceRequest;->getUrl()Landroid/net/Uri;

    move-result-object p2

    invoke-virtual {p2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/stagnationlab/sk/h/e;->a(Ljava/lang/String;)V

    return-void
.end method

.method public shouldOverrideUrlLoading(Landroid/webkit/WebView;Landroid/webkit/WebResourceRequest;)Z
    .locals 0

    .line 60
    iget-object p1, p0, Lcom/stagnationlab/sk/h/e$1;->a:Lcom/stagnationlab/sk/h/e;

    invoke-interface {p2}, Landroid/webkit/WebResourceRequest;->getUrl()Landroid/net/Uri;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/stagnationlab/sk/h/e;->a(Landroid/net/Uri;)Z

    move-result p1

    return p1
.end method

.method public shouldOverrideUrlLoading(Landroid/webkit/WebView;Ljava/lang/String;)Z
    .locals 0

    .line 66
    iget-object p1, p0, Lcom/stagnationlab/sk/h/e$1;->a:Lcom/stagnationlab/sk/h/e;

    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/stagnationlab/sk/h/e;->a(Landroid/net/Uri;)Z

    move-result p1

    return p1
.end method
