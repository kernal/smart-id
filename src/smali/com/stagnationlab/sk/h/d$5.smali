.class Lcom/stagnationlab/sk/h/d$5;
.super Ljava/lang/Object;
.source "JavascriptBridge.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/stagnationlab/sk/h/d;->a(Ljava/lang/String;IZLcom/stagnationlab/sk/util/f;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:I

.field final synthetic c:Z

.field final synthetic d:Lcom/stagnationlab/sk/util/f;

.field final synthetic e:Lcom/stagnationlab/sk/h/d;


# direct methods
.method constructor <init>(Lcom/stagnationlab/sk/h/d;Ljava/lang/String;IZLcom/stagnationlab/sk/util/f;)V
    .locals 0

    .line 101
    iput-object p1, p0, Lcom/stagnationlab/sk/h/d$5;->e:Lcom/stagnationlab/sk/h/d;

    iput-object p2, p0, Lcom/stagnationlab/sk/h/d$5;->a:Ljava/lang/String;

    iput p3, p0, Lcom/stagnationlab/sk/h/d$5;->b:I

    iput-boolean p4, p0, Lcom/stagnationlab/sk/h/d$5;->c:Z

    iput-object p5, p0, Lcom/stagnationlab/sk/h/d$5;->d:Lcom/stagnationlab/sk/util/f;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .line 103
    iget-object v0, p0, Lcom/stagnationlab/sk/h/d$5;->e:Lcom/stagnationlab/sk/h/d;

    invoke-static {v0}, Lcom/stagnationlab/sk/h/d;->a(Lcom/stagnationlab/sk/h/d;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "--> Sending response for \'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/stagnationlab/sk/h/d$5;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "\' ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v2, p0, Lcom/stagnationlab/sk/h/d$5;->b:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v2, "): "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v2, p0, Lcom/stagnationlab/sk/h/d$5;->c:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/stagnationlab/sk/f;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 105
    iget-object v0, p0, Lcom/stagnationlab/sk/h/d$5;->e:Lcom/stagnationlab/sk/h/d;

    invoke-static {v0}, Lcom/stagnationlab/sk/h/d;->b(Lcom/stagnationlab/sk/h/d;)Lcom/stagnationlab/sk/h/e;

    move-result-object v0

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    iget v2, p0, Lcom/stagnationlab/sk/h/d$5;->b:I

    .line 108
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    iget-boolean v2, p0, Lcom/stagnationlab/sk/h/d$5;->c:Z

    if-eqz v2, :cond_0

    const-string v2, "true"

    goto :goto_0

    :cond_0
    const-string v2, "false"

    :goto_0
    const/4 v3, 0x1

    aput-object v2, v1, v3

    const/4 v2, 0x2

    iget-object v3, p0, Lcom/stagnationlab/sk/h/d$5;->d:Lcom/stagnationlab/sk/util/f;

    if-nez v3, :cond_1

    const-string v3, "null"

    goto :goto_1

    .line 109
    :cond_1
    invoke-virtual {v3}, Lcom/stagnationlab/sk/util/f;->toString()Ljava/lang/String;

    move-result-object v3

    :goto_1
    aput-object v3, v1, v2

    const-string v2, "NativeBridge.handleResult(%s, %s, %s);"

    .line 106
    invoke-static {v2, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    .line 105
    invoke-virtual {v0, v1, v2}, Lcom/stagnationlab/sk/h/e;->a(Ljava/lang/String;Landroid/webkit/ValueCallback;)V

    return-void
.end method
