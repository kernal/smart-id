.class final enum Lcom/stagnationlab/sk/TransactionActivity$a;
.super Ljava/lang/Enum;
.source "TransactionActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/stagnationlab/sk/TransactionActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/stagnationlab/sk/TransactionActivity$a;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/stagnationlab/sk/TransactionActivity$a;

.field public static final enum b:Lcom/stagnationlab/sk/TransactionActivity$a;

.field public static final enum c:Lcom/stagnationlab/sk/TransactionActivity$a;

.field private static final synthetic d:[Lcom/stagnationlab/sk/TransactionActivity$a;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 79
    new-instance v0, Lcom/stagnationlab/sk/TransactionActivity$a;

    const/4 v1, 0x0

    const-string v2, "EXIT"

    invoke-direct {v0, v2, v1}, Lcom/stagnationlab/sk/TransactionActivity$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/stagnationlab/sk/TransactionActivity$a;->a:Lcom/stagnationlab/sk/TransactionActivity$a;

    new-instance v0, Lcom/stagnationlab/sk/TransactionActivity$a;

    const/4 v2, 0x1

    const-string v3, "MANUAL_NOTIFICATION"

    invoke-direct {v0, v3, v2}, Lcom/stagnationlab/sk/TransactionActivity$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/stagnationlab/sk/TransactionActivity$a;->b:Lcom/stagnationlab/sk/TransactionActivity$a;

    new-instance v0, Lcom/stagnationlab/sk/TransactionActivity$a;

    const/4 v3, 0x2

    const-string v4, "TIMEOUT"

    invoke-direct {v0, v4, v3}, Lcom/stagnationlab/sk/TransactionActivity$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/stagnationlab/sk/TransactionActivity$a;->c:Lcom/stagnationlab/sk/TransactionActivity$a;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/stagnationlab/sk/TransactionActivity$a;

    sget-object v4, Lcom/stagnationlab/sk/TransactionActivity$a;->a:Lcom/stagnationlab/sk/TransactionActivity$a;

    aput-object v4, v0, v1

    sget-object v1, Lcom/stagnationlab/sk/TransactionActivity$a;->b:Lcom/stagnationlab/sk/TransactionActivity$a;

    aput-object v1, v0, v2

    sget-object v1, Lcom/stagnationlab/sk/TransactionActivity$a;->c:Lcom/stagnationlab/sk/TransactionActivity$a;

    aput-object v1, v0, v3

    sput-object v0, Lcom/stagnationlab/sk/TransactionActivity$a;->d:[Lcom/stagnationlab/sk/TransactionActivity$a;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 79
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/stagnationlab/sk/TransactionActivity$a;
    .locals 1

    .line 79
    const-class v0, Lcom/stagnationlab/sk/TransactionActivity$a;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/stagnationlab/sk/TransactionActivity$a;

    return-object p0
.end method

.method public static values()[Lcom/stagnationlab/sk/TransactionActivity$a;
    .locals 1

    .line 79
    sget-object v0, Lcom/stagnationlab/sk/TransactionActivity$a;->d:[Lcom/stagnationlab/sk/TransactionActivity$a;

    invoke-virtual {v0}, [Lcom/stagnationlab/sk/TransactionActivity$a;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/stagnationlab/sk/TransactionActivity$a;

    return-object v0
.end method
