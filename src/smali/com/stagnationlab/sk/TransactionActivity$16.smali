.class Lcom/stagnationlab/sk/TransactionActivity$16;
.super Ljava/lang/Object;
.source "TransactionActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/stagnationlab/sk/TransactionActivity;->t()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/stagnationlab/sk/TransactionActivity;


# direct methods
.method constructor <init>(Lcom/stagnationlab/sk/TransactionActivity;)V
    .locals 0

    .line 1164
    iput-object p1, p0, Lcom/stagnationlab/sk/TransactionActivity$16;->a:Lcom/stagnationlab/sk/TransactionActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 9

    .line 1168
    iget-object v0, p0, Lcom/stagnationlab/sk/TransactionActivity$16;->a:Lcom/stagnationlab/sk/TransactionActivity;

    invoke-static {v0}, Lcom/stagnationlab/sk/TransactionActivity;->p(Lcom/stagnationlab/sk/TransactionActivity;)Landroid/support/v7/widget/AppCompatImageView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/AppCompatImageView;->setAlpha(F)V

    .line 1171
    new-instance v0, Landroid/view/animation/RotateAnimation;

    const/4 v3, 0x0

    const/high16 v4, 0x43b40000    # 360.0f

    const/4 v5, 0x1

    const/high16 v6, 0x3f000000    # 0.5f

    const/4 v7, 0x1

    const/high16 v8, 0x3f000000    # 0.5f

    move-object v2, v0

    invoke-direct/range {v2 .. v8}, Landroid/view/animation/RotateAnimation;-><init>(FFIFIF)V

    .line 1179
    invoke-static {}, Lcom/stagnationlab/sk/TransactionActivity;->i()I

    move-result v1

    int-to-long v1, v1

    invoke-virtual {v0, v1, v2}, Landroid/view/animation/RotateAnimation;->setDuration(J)V

    .line 1180
    invoke-static {}, Lcom/stagnationlab/sk/TransactionActivity;->j()Landroid/view/animation/Interpolator;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/animation/RotateAnimation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 1181
    invoke-static {}, Lcom/stagnationlab/sk/TransactionActivity;->k()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/animation/RotateAnimation;->setRepeatCount(I)V

    .line 1183
    iget-object v1, p0, Lcom/stagnationlab/sk/TransactionActivity$16;->a:Lcom/stagnationlab/sk/TransactionActivity;

    invoke-static {v1}, Lcom/stagnationlab/sk/TransactionActivity;->q(Lcom/stagnationlab/sk/TransactionActivity;)Landroid/widget/ImageView;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    return-void
.end method
