.class Lcom/stagnationlab/sk/fcm/MyFirebaseMessagingService$2;
.super Ljava/lang/Object;
.source "MyFirebaseMessagingService.java"

# interfaces
.implements Lcom/stagnationlab/sk/a/a/a;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/stagnationlab/sk/fcm/MyFirebaseMessagingService;->a(Lcom/stagnationlab/sk/models/PushMessage;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/stagnationlab/sk/models/PushMessage;

.field final synthetic b:Lcom/stagnationlab/sk/fcm/MyFirebaseMessagingService;


# direct methods
.method constructor <init>(Lcom/stagnationlab/sk/fcm/MyFirebaseMessagingService;Lcom/stagnationlab/sk/models/PushMessage;)V
    .locals 0

    .line 101
    iput-object p1, p0, Lcom/stagnationlab/sk/fcm/MyFirebaseMessagingService$2;->b:Lcom/stagnationlab/sk/fcm/MyFirebaseMessagingService;

    iput-object p2, p0, Lcom/stagnationlab/sk/fcm/MyFirebaseMessagingService$2;->a:Lcom/stagnationlab/sk/models/PushMessage;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/stagnationlab/sk/a/a;Z)V
    .locals 3

    .line 104
    invoke-virtual {p1}, Lcom/stagnationlab/sk/a/a;->h()Z

    move-result p2

    if-nez p2, :cond_0

    .line 105
    iget-object p1, p0, Lcom/stagnationlab/sk/fcm/MyFirebaseMessagingService$2;->b:Lcom/stagnationlab/sk/fcm/MyFirebaseMessagingService;

    new-instance p2, Lcom/stagnationlab/sk/c/a;

    sget-object v0, Lcom/stagnationlab/sk/c/b;->S:Lcom/stagnationlab/sk/c/b;

    invoke-direct {p2, v0}, Lcom/stagnationlab/sk/c/a;-><init>(Lcom/stagnationlab/sk/c/b;)V

    iget-object v0, p0, Lcom/stagnationlab/sk/fcm/MyFirebaseMessagingService$2;->a:Lcom/stagnationlab/sk/models/PushMessage;

    invoke-static {p1, p2, v0}, Lcom/stagnationlab/sk/fcm/MyFirebaseMessagingService;->a(Lcom/stagnationlab/sk/fcm/MyFirebaseMessagingService;Lcom/stagnationlab/sk/c/a;Lcom/stagnationlab/sk/models/PushMessage;)V

    return-void

    .line 110
    :cond_0
    iget-object p2, p0, Lcom/stagnationlab/sk/fcm/MyFirebaseMessagingService$2;->a:Lcom/stagnationlab/sk/models/PushMessage;

    .line 111
    invoke-virtual {p2}, Lcom/stagnationlab/sk/models/PushMessage;->a()Ljava/lang/String;

    move-result-object p2

    iget-object v0, p0, Lcom/stagnationlab/sk/fcm/MyFirebaseMessagingService$2;->a:Lcom/stagnationlab/sk/models/PushMessage;

    .line 112
    invoke-virtual {v0}, Lcom/stagnationlab/sk/models/PushMessage;->c()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/stagnationlab/sk/fcm/MyFirebaseMessagingService$2;->a:Lcom/stagnationlab/sk/models/PushMessage;

    .line 113
    invoke-virtual {v1}, Lcom/stagnationlab/sk/models/PushMessage;->b()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/stagnationlab/sk/fcm/MyFirebaseMessagingService$2$1;

    invoke-direct {v2, p0}, Lcom/stagnationlab/sk/fcm/MyFirebaseMessagingService$2$1;-><init>(Lcom/stagnationlab/sk/fcm/MyFirebaseMessagingService$2;)V

    .line 110
    invoke-virtual {p1, p2, v0, v1, v2}, Lcom/stagnationlab/sk/a/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/stagnationlab/sk/a/a/o;)V

    return-void
.end method

.method public a(Lcom/stagnationlab/sk/c/a;)V
    .locals 2

    .line 140
    new-instance p1, Landroid/content/Intent;

    iget-object v0, p0, Lcom/stagnationlab/sk/fcm/MyFirebaseMessagingService$2;->b:Lcom/stagnationlab/sk/fcm/MyFirebaseMessagingService;

    const-class v1, Lcom/stagnationlab/sk/MainActivity;

    invoke-direct {p1, v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v0, "wakeUpDevice"

    const/4 v1, 0x1

    .line 141
    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const/high16 v0, 0x10000000

    .line 142
    invoke-virtual {p1, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 143
    iget-object v0, p0, Lcom/stagnationlab/sk/fcm/MyFirebaseMessagingService$2;->b:Lcom/stagnationlab/sk/fcm/MyFirebaseMessagingService;

    invoke-virtual {v0, p1}, Lcom/stagnationlab/sk/fcm/MyFirebaseMessagingService;->startActivity(Landroid/content/Intent;)V

    .line 144
    iget-object p1, p0, Lcom/stagnationlab/sk/fcm/MyFirebaseMessagingService$2;->b:Lcom/stagnationlab/sk/fcm/MyFirebaseMessagingService;

    invoke-static {p1}, Lcom/stagnationlab/sk/fcm/MyFirebaseMessagingService;->b(Lcom/stagnationlab/sk/fcm/MyFirebaseMessagingService;)V

    .line 145
    iget-object p1, p0, Lcom/stagnationlab/sk/fcm/MyFirebaseMessagingService$2;->b:Lcom/stagnationlab/sk/fcm/MyFirebaseMessagingService;

    invoke-static {p1}, Lcom/stagnationlab/sk/fcm/MyFirebaseMessagingService;->a(Lcom/stagnationlab/sk/fcm/MyFirebaseMessagingService;)V

    return-void
.end method

.method public b(Lcom/stagnationlab/sk/c/a;)V
    .locals 2

    .line 150
    iget-object v0, p0, Lcom/stagnationlab/sk/fcm/MyFirebaseMessagingService$2;->b:Lcom/stagnationlab/sk/fcm/MyFirebaseMessagingService;

    iget-object v1, p0, Lcom/stagnationlab/sk/fcm/MyFirebaseMessagingService$2;->a:Lcom/stagnationlab/sk/models/PushMessage;

    invoke-static {v0, p1, v1}, Lcom/stagnationlab/sk/fcm/MyFirebaseMessagingService;->a(Lcom/stagnationlab/sk/fcm/MyFirebaseMessagingService;Lcom/stagnationlab/sk/c/a;Lcom/stagnationlab/sk/models/PushMessage;)V

    return-void
.end method
