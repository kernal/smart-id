.class public Lcom/stagnationlab/sk/fcm/MyFirebaseMessagingService;
.super Lcom/google/firebase/messaging/FirebaseMessagingService;
.source "MyFirebaseMessagingService.java"


# instance fields
.field private b:Landroid/os/PowerManager$WakeLock;

.field private c:Landroid/media/MediaPlayer;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 44
    invoke-direct {p0}, Lcom/google/firebase/messaging/FirebaseMessagingService;-><init>()V

    return-void
.end method

.method private a(I)V
    .locals 4

    const/4 v0, -0x1

    if-ne p1, v0, :cond_0

    return-void

    :cond_0
    const-string v0, "alarm"

    .line 277
    invoke-virtual {p0, v0}, Lcom/stagnationlab/sk/fcm/MyFirebaseMessagingService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    if-nez v0, :cond_1

    return-void

    .line 283
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Setting alarm to clear notification in "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "MyFirebaseMessagingService"

    invoke-static {v2, v1}, Lcom/stagnationlab/sk/f;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 285
    new-instance v1, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/stagnationlab/sk/fcm/MyFirebaseMessagingService;->getBaseContext()Landroid/content/Context;

    move-result-object v2

    const-class v3, Lcom/stagnationlab/sk/fcm/ClearNotificationReceiver;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/4 v2, 0x1

    const-string v3, "notificationId"

    .line 286
    invoke-virtual {v1, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const/high16 v3, 0x8000000

    .line 289
    invoke-static {p0, v2, v1, v3}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 294
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v2

    const/16 v3, 0xd

    .line 295
    invoke-virtual {v2, v3, p1}, Ljava/util/Calendar;->add(II)V

    const/4 p1, 0x0

    .line 297
    invoke-virtual {v2}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    invoke-virtual {v0, p1, v2, v3, v1}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    return-void
.end method

.method private a(Landroid/content/Intent;I)V
    .locals 3

    .line 198
    invoke-direct {p0}, Lcom/stagnationlab/sk/fcm/MyFirebaseMessagingService;->e()V

    .line 200
    invoke-static {}, Lcom/stagnationlab/sk/fcm/a;->a()Landroid/app/Activity;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 203
    invoke-virtual {p0, p1}, Lcom/stagnationlab/sk/fcm/MyFirebaseMessagingService;->startActivity(Landroid/content/Intent;)V

    const-string p2, "error"

    .line 204
    invoke-virtual {p1, p2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 205
    invoke-static {v0}, Lcom/stagnationlab/sk/util/n;->a(Landroid/app/Activity;)V

    goto :goto_0

    .line 207
    :cond_0
    invoke-static {v0}, Lcom/stagnationlab/sk/util/n;->c(Landroid/app/Activity;)V

    .line 209
    :goto_0
    invoke-direct {p0}, Lcom/stagnationlab/sk/fcm/MyFirebaseMessagingService;->b()V

    return-void

    .line 214
    :cond_1
    invoke-static {}, Lcom/stagnationlab/sk/a/d;->f()Lcom/stagnationlab/sk/b/b;

    move-result-object v0

    .line 216
    sget-object v1, Lcom/stagnationlab/sk/b/b;->a:Lcom/stagnationlab/sk/b/b;

    if-ne v0, v1, :cond_2

    return-void

    .line 220
    :cond_2
    invoke-static {}, Lcom/stagnationlab/sk/util/b;->g()Z

    move-result v1

    .line 222
    sget-object v2, Lcom/stagnationlab/sk/b/b;->b:Lcom/stagnationlab/sk/b/b;

    if-eq v0, v2, :cond_4

    if-eqz v1, :cond_3

    goto :goto_1

    :cond_3
    const/4 p2, 0x1

    const-string v0, "wakeUpDevice"

    .line 228
    invoke-virtual {p1, v0, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 229
    invoke-virtual {p0, p1}, Lcom/stagnationlab/sk/fcm/MyFirebaseMessagingService;->startActivity(Landroid/content/Intent;)V

    .line 230
    invoke-direct {p0}, Lcom/stagnationlab/sk/fcm/MyFirebaseMessagingService;->b()V

    return-void

    .line 223
    :cond_4
    :goto_1
    invoke-direct {p0, p1, p2, v1}, Lcom/stagnationlab/sk/fcm/MyFirebaseMessagingService;->a(Landroid/content/Intent;IZ)V

    return-void
.end method

.method private a(Landroid/content/Intent;IZ)V
    .locals 6

    const-string v0, "notification"

    .line 234
    invoke-virtual {p0, v0}, Lcom/stagnationlab/sk/fcm/MyFirebaseMessagingService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    if-nez v0, :cond_0

    return-void

    .line 240
    :cond_0
    new-instance v1, Landroid/support/v4/app/w$d;

    const-string v2, "TRANSACTION"

    invoke-direct {v1, p0, v2}, Landroid/support/v4/app/w$d;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    const v2, 0x7f070075

    .line 242
    invoke-virtual {v1, v2}, Landroid/support/v4/app/w$d;->a(I)Landroid/support/v4/app/w$d;

    move-result-object v1

    const/4 v2, 0x1

    .line 243
    invoke-virtual {v1, v2}, Landroid/support/v4/app/w$d;->a(Z)Landroid/support/v4/app/w$d;

    move-result-object v1

    .line 244
    invoke-virtual {p0}, Lcom/stagnationlab/sk/fcm/MyFirebaseMessagingService;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f050027

    const/4 v5, 0x0

    invoke-static {v3, v4, v5}, Landroid/support/v4/content/a/f;->b(Landroid/content/res/Resources;ILandroid/content/res/Resources$Theme;)I

    move-result v3

    invoke-virtual {v1, v3}, Landroid/support/v4/app/w$d;->d(I)Landroid/support/v4/app/w$d;

    move-result-object v1

    .line 246
    invoke-static {}, Lcom/stagnationlab/sk/fcm/c;->a()Z

    move-result v3

    if-eqz v3, :cond_1

    const/4 v3, -0x1

    .line 248
    invoke-virtual {v1, v3}, Landroid/support/v4/app/w$d;->b(I)Landroid/support/v4/app/w$d;

    .line 251
    :cond_1
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x18

    const v5, 0x7f0e0100

    if-lt v3, v4, :cond_2

    .line 252
    invoke-virtual {p0, v5}, Lcom/stagnationlab/sk/fcm/MyFirebaseMessagingService;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/support/v4/app/w$d;->a(Ljava/lang/CharSequence;)Landroid/support/v4/app/w$d;

    goto :goto_0

    :cond_2
    const v3, 0x7f0e0028

    .line 255
    invoke-virtual {p0, v3}, Lcom/stagnationlab/sk/fcm/MyFirebaseMessagingService;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/support/v4/app/w$d;->a(Ljava/lang/CharSequence;)Landroid/support/v4/app/w$d;

    move-result-object v3

    .line 256
    invoke-virtual {p0, v5}, Lcom/stagnationlab/sk/fcm/MyFirebaseMessagingService;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/support/v4/app/w$d;->b(Ljava/lang/CharSequence;)Landroid/support/v4/app/w$d;

    :goto_0
    const-string v3, "notificationId"

    .line 259
    invoke-virtual {p1, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const/4 v3, 0x0

    const/high16 v4, 0x8000000

    .line 260
    invoke-static {p0, v3, p1, v4}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v3

    .line 261
    invoke-virtual {v1, v3}, Landroid/support/v4/app/w$d;->a(Landroid/app/PendingIntent;)Landroid/support/v4/app/w$d;

    if-eqz p3, :cond_3

    .line 264
    invoke-virtual {v1, v3, v2}, Landroid/support/v4/app/w$d;->a(Landroid/app/PendingIntent;Z)Landroid/support/v4/app/w$d;

    .line 267
    :cond_3
    invoke-virtual {v1}, Landroid/support/v4/app/w$d;->b()Landroid/app/Notification;

    move-result-object p3

    invoke-virtual {v0, v2, p3}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 268
    invoke-static {p1}, Lcom/stagnationlab/sk/fcm/ClearNotificationReceiver;->a(Landroid/content/Intent;)V

    .line 269
    invoke-direct {p0, p2}, Lcom/stagnationlab/sk/fcm/MyFirebaseMessagingService;->a(I)V

    return-void
.end method

.method private a(Lcom/stagnationlab/sk/c/a;Lcom/stagnationlab/sk/models/PushMessage;)V
    .locals 3

    .line 176
    invoke-virtual {p1}, Lcom/stagnationlab/sk/c/a;->a()Lcom/stagnationlab/sk/c/b;

    move-result-object v0

    sget-object v1, Lcom/stagnationlab/sk/c/b;->a:Lcom/stagnationlab/sk/c/b;

    const/high16 v2, 0x10000000

    if-eq v0, v1, :cond_1

    .line 177
    invoke-virtual {p1}, Lcom/stagnationlab/sk/c/a;->a()Lcom/stagnationlab/sk/c/b;

    move-result-object v0

    sget-object v1, Lcom/stagnationlab/sk/c/b;->b:Lcom/stagnationlab/sk/c/b;

    if-ne v0, v1, :cond_0

    goto :goto_0

    .line 185
    :cond_0
    new-instance p2, Landroid/content/Intent;

    const-class v0, Lcom/stagnationlab/sk/ErrorActivity;

    invoke-direct {p2, p0, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v0, "error"

    .line 186
    invoke-virtual {p2, v0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 187
    invoke-virtual {p2, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    move-object p1, p2

    goto :goto_1

    .line 181
    :cond_1
    :goto_0
    new-instance p1, Landroid/content/Intent;

    const-class v0, Lcom/stagnationlab/sk/TransactionActivity;

    invoke-direct {p1, p0, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v0, "pushMessage"

    .line 182
    invoke-virtual {p1, v0, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 183
    invoke-virtual {p1, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 190
    :goto_1
    invoke-direct {p0, p1}, Lcom/stagnationlab/sk/fcm/MyFirebaseMessagingService;->d(Landroid/content/Intent;)V

    return-void
.end method

.method static synthetic a(Lcom/stagnationlab/sk/fcm/MyFirebaseMessagingService;)V
    .locals 0

    .line 44
    invoke-direct {p0}, Lcom/stagnationlab/sk/fcm/MyFirebaseMessagingService;->e()V

    return-void
.end method

.method static synthetic a(Lcom/stagnationlab/sk/fcm/MyFirebaseMessagingService;Lcom/stagnationlab/sk/c/a;Lcom/stagnationlab/sk/models/PushMessage;)V
    .locals 0

    .line 44
    invoke-direct {p0, p1, p2}, Lcom/stagnationlab/sk/fcm/MyFirebaseMessagingService;->a(Lcom/stagnationlab/sk/c/a;Lcom/stagnationlab/sk/models/PushMessage;)V

    return-void
.end method

.method static synthetic a(Lcom/stagnationlab/sk/fcm/MyFirebaseMessagingService;Lcom/stagnationlab/sk/models/PushMessage;)V
    .locals 0

    .line 44
    invoke-direct {p0, p1}, Lcom/stagnationlab/sk/fcm/MyFirebaseMessagingService;->a(Lcom/stagnationlab/sk/models/PushMessage;)V

    return-void
.end method

.method static synthetic a(Lcom/stagnationlab/sk/fcm/MyFirebaseMessagingService;Lcom/stagnationlab/sk/models/Transaction;Lcom/stagnationlab/sk/models/RpRequest;)V
    .locals 0

    .line 44
    invoke-direct {p0, p1, p2}, Lcom/stagnationlab/sk/fcm/MyFirebaseMessagingService;->a(Lcom/stagnationlab/sk/models/Transaction;Lcom/stagnationlab/sk/models/RpRequest;)V

    return-void
.end method

.method private a(Lcom/stagnationlab/sk/models/PushMessage;)V
    .locals 3

    .line 90
    invoke-static {}, Lcom/stagnationlab/sk/a/d;->f()Lcom/stagnationlab/sk/b/b;

    move-result-object v0

    .line 92
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Push behaviour: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "MyFirebaseMessagingService"

    invoke-static {v2, v1}, Lcom/stagnationlab/sk/f;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 94
    sget-object v1, Lcom/stagnationlab/sk/b/b;->a:Lcom/stagnationlab/sk/b/b;

    if-ne v0, v1, :cond_0

    invoke-static {}, Lcom/stagnationlab/sk/fcm/a;->a()Landroid/app/Activity;

    move-result-object v0

    if-nez v0, :cond_0

    .line 97
    invoke-direct {p0}, Lcom/stagnationlab/sk/fcm/MyFirebaseMessagingService;->e()V

    return-void

    .line 101
    :cond_0
    invoke-virtual {p0}, Lcom/stagnationlab/sk/fcm/MyFirebaseMessagingService;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/stagnationlab/sk/MyApplication;

    new-instance v1, Lcom/stagnationlab/sk/fcm/MyFirebaseMessagingService$2;

    invoke-direct {v1, p0, p1}, Lcom/stagnationlab/sk/fcm/MyFirebaseMessagingService$2;-><init>(Lcom/stagnationlab/sk/fcm/MyFirebaseMessagingService;Lcom/stagnationlab/sk/models/PushMessage;)V

    invoke-virtual {v0, v1}, Lcom/stagnationlab/sk/MyApplication;->a(Lcom/stagnationlab/sk/a/a/a;)V

    return-void
.end method

.method private a(Lcom/stagnationlab/sk/models/Transaction;Lcom/stagnationlab/sk/models/RpRequest;)V
    .locals 2

    .line 156
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/stagnationlab/sk/TransactionActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "transaction"

    .line 157
    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v1, "rpRequest"

    .line 158
    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const/high16 v1, 0x10000000

    .line 159
    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    if-eqz p1, :cond_0

    .line 164
    invoke-virtual {p1}, Lcom/stagnationlab/sk/models/Transaction;->a()I

    move-result p1

    goto :goto_0

    :cond_0
    if-eqz p2, :cond_1

    .line 166
    invoke-virtual {p2}, Lcom/stagnationlab/sk/models/RpRequest;->a()I

    move-result p1

    goto :goto_0

    :cond_1
    const/4 p1, -0x1

    .line 169
    :goto_0
    invoke-direct {p0, v0, p1}, Lcom/stagnationlab/sk/fcm/MyFirebaseMessagingService;->a(Landroid/content/Intent;I)V

    return-void
.end method

.method private b()V
    .locals 0

    .line 301
    invoke-direct {p0}, Lcom/stagnationlab/sk/fcm/MyFirebaseMessagingService;->d()V

    .line 302
    invoke-direct {p0}, Lcom/stagnationlab/sk/fcm/MyFirebaseMessagingService;->c()V

    return-void
.end method

.method static synthetic b(Lcom/stagnationlab/sk/fcm/MyFirebaseMessagingService;)V
    .locals 0

    .line 44
    invoke-direct {p0}, Lcom/stagnationlab/sk/fcm/MyFirebaseMessagingService;->b()V

    return-void
.end method

.method private c()V
    .locals 4

    const-string v0, "vibrator"

    .line 306
    invoke-virtual {p0, v0}, Lcom/stagnationlab/sk/fcm/MyFirebaseMessagingService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Vibrator;

    if-nez v0, :cond_0

    return-void

    .line 312
    :cond_0
    invoke-static {p0}, Lcom/stagnationlab/sk/fcm/c;->b(Landroid/content/Context;)[J

    move-result-object v1

    if-nez v1, :cond_1

    return-void

    .line 318
    :cond_1
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x1a

    if-lt v2, v3, :cond_2

    const/4 v2, -0x1

    .line 319
    invoke-static {v1, v2}, Landroid/os/VibrationEffect;->createWaveform([JI)Landroid/os/VibrationEffect;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Vibrator;->vibrate(Landroid/os/VibrationEffect;)V

    goto :goto_0

    :cond_2
    const/4 v2, 0x1

    .line 321
    invoke-virtual {v0, v1, v2}, Landroid/os/Vibrator;->vibrate([JI)V

    :goto_0
    return-void
.end method

.method static synthetic c(Lcom/stagnationlab/sk/fcm/MyFirebaseMessagingService;)V
    .locals 0

    .line 44
    invoke-direct {p0}, Lcom/stagnationlab/sk/fcm/MyFirebaseMessagingService;->f()V

    return-void
.end method

.method private d()V
    .locals 4

    .line 326
    invoke-static {p0}, Lcom/stagnationlab/sk/fcm/c;->a(Landroid/content/Context;)Landroid/net/Uri;

    move-result-object v0

    if-nez v0, :cond_0

    return-void

    .line 336
    :cond_0
    :try_start_0
    invoke-direct {p0}, Lcom/stagnationlab/sk/fcm/MyFirebaseMessagingService;->f()V

    .line 337
    new-instance v1, Landroid/media/MediaPlayer;

    invoke-direct {v1}, Landroid/media/MediaPlayer;-><init>()V

    iput-object v1, p0, Lcom/stagnationlab/sk/fcm/MyFirebaseMessagingService;->c:Landroid/media/MediaPlayer;

    .line 339
    iget-object v1, p0, Lcom/stagnationlab/sk/fcm/MyFirebaseMessagingService;->c:Landroid/media/MediaPlayer;

    new-instance v2, Lcom/stagnationlab/sk/fcm/MyFirebaseMessagingService$3;

    invoke-direct {v2, p0}, Lcom/stagnationlab/sk/fcm/MyFirebaseMessagingService$3;-><init>(Lcom/stagnationlab/sk/fcm/MyFirebaseMessagingService;)V

    invoke-virtual {v1, v2}, Landroid/media/MediaPlayer;->setOnErrorListener(Landroid/media/MediaPlayer$OnErrorListener;)V

    .line 347
    iget-object v1, p0, Lcom/stagnationlab/sk/fcm/MyFirebaseMessagingService;->c:Landroid/media/MediaPlayer;

    new-instance v2, Lcom/stagnationlab/sk/fcm/MyFirebaseMessagingService$4;

    invoke-direct {v2, p0}, Lcom/stagnationlab/sk/fcm/MyFirebaseMessagingService$4;-><init>(Lcom/stagnationlab/sk/fcm/MyFirebaseMessagingService;)V

    invoke-virtual {v1, v2}, Landroid/media/MediaPlayer;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    .line 353
    iget-object v1, p0, Lcom/stagnationlab/sk/fcm/MyFirebaseMessagingService;->c:Landroid/media/MediaPlayer;

    invoke-virtual {p0}, Lcom/stagnationlab/sk/fcm/MyFirebaseMessagingService;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Landroid/media/MediaPlayer;->setDataSource(Landroid/content/Context;Landroid/net/Uri;)V

    .line 354
    iget-object v0, p0, Lcom/stagnationlab/sk/fcm/MyFirebaseMessagingService;->c:Landroid/media/MediaPlayer;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setAudioStreamType(I)V

    .line 355
    iget-object v0, p0, Lcom/stagnationlab/sk/fcm/MyFirebaseMessagingService;->c:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->prepare()V

    .line 356
    iget-object v0, p0, Lcom/stagnationlab/sk/fcm/MyFirebaseMessagingService;->c:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->start()V

    .line 359
    iget-object v0, p0, Lcom/stagnationlab/sk/fcm/MyFirebaseMessagingService;->c:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->getDuration()I

    move-result v0

    const/16 v1, 0x1388

    if-le v0, v1, :cond_1

    .line 361
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lcom/stagnationlab/sk/fcm/MyFirebaseMessagingService$5;

    invoke-direct {v1, p0}, Lcom/stagnationlab/sk/fcm/MyFirebaseMessagingService$5;-><init>(Lcom/stagnationlab/sk/fcm/MyFirebaseMessagingService;)V

    const-wide/16 v2, 0x1388

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 368
    invoke-direct {p0}, Lcom/stagnationlab/sk/fcm/MyFirebaseMessagingService;->f()V

    const-string v1, "MyFirebaseMessagingService"

    const-string v2, "Failed to play notification sound"

    .line 369
    invoke-static {v1, v2, v0}, Lcom/stagnationlab/sk/f;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_1
    :goto_0
    return-void
.end method

.method private d(Landroid/content/Intent;)V
    .locals 1

    const/4 v0, -0x1

    .line 194
    invoke-direct {p0, p1, v0}, Lcom/stagnationlab/sk/fcm/MyFirebaseMessagingService;->a(Landroid/content/Intent;I)V

    return-void
.end method

.method private e()V
    .locals 1

    .line 374
    iget-object v0, p0, Lcom/stagnationlab/sk/fcm/MyFirebaseMessagingService;->b:Landroid/os/PowerManager$WakeLock;

    if-eqz v0, :cond_0

    .line 375
    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    const/4 v0, 0x0

    .line 376
    iput-object v0, p0, Lcom/stagnationlab/sk/fcm/MyFirebaseMessagingService;->b:Landroid/os/PowerManager$WakeLock;

    :cond_0
    return-void
.end method

.method private f()V
    .locals 1

    .line 381
    iget-object v0, p0, Lcom/stagnationlab/sk/fcm/MyFirebaseMessagingService;->c:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    .line 382
    invoke-virtual {v0}, Landroid/media/MediaPlayer;->reset()V

    .line 383
    iget-object v0, p0, Lcom/stagnationlab/sk/fcm/MyFirebaseMessagingService;->c:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->release()V

    const/4 v0, 0x0

    .line 384
    iput-object v0, p0, Lcom/stagnationlab/sk/fcm/MyFirebaseMessagingService;->c:Landroid/media/MediaPlayer;

    :cond_0
    return-void
.end method


# virtual methods
.method public a(Lcom/google/firebase/messaging/c;)V
    .locals 3

    .line 64
    invoke-virtual {p1}, Lcom/google/firebase/messaging/c;->a()Ljava/lang/String;

    move-result-object v0

    .line 65
    invoke-virtual {p1}, Lcom/google/firebase/messaging/c;->b()Ljava/util/Map;

    move-result-object p1

    .line 66
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Push from: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "MyFirebaseMessagingService"

    invoke-static {v1, v0}, Lcom/stagnationlab/sk/f;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 67
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Push data: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/stagnationlab/sk/f;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 69
    new-instance v0, Lcom/stagnationlab/sk/models/PushMessage;

    invoke-direct {v0, p1}, Lcom/stagnationlab/sk/models/PushMessage;-><init>(Ljava/util/Map;)V

    .line 71
    invoke-virtual {v0}, Lcom/stagnationlab/sk/models/PushMessage;->d()Z

    move-result p1

    if-nez p1, :cond_0

    const-string p1, "Got push message with unexpected data"

    .line 72
    invoke-static {v1, p1}, Lcom/stagnationlab/sk/f;->d(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    :cond_0
    const-string p1, "power"

    .line 77
    invoke-virtual {p0, p1}, Lcom/stagnationlab/sk/fcm/MyFirebaseMessagingService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/os/PowerManager;

    const/4 v1, 0x1

    const-string v2, "SmartId:MyFirebaseMessagingService"

    .line 78
    invoke-virtual {p1, v1, v2}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object p1

    iput-object p1, p0, Lcom/stagnationlab/sk/fcm/MyFirebaseMessagingService;->b:Landroid/os/PowerManager$WakeLock;

    .line 79
    iget-object p1, p0, Lcom/stagnationlab/sk/fcm/MyFirebaseMessagingService;->b:Landroid/os/PowerManager$WakeLock;

    const-wide/16 v1, 0x7530

    invoke-virtual {p1, v1, v2}, Landroid/os/PowerManager$WakeLock;->acquire(J)V

    .line 81
    invoke-virtual {p0}, Lcom/stagnationlab/sk/fcm/MyFirebaseMessagingService;->getApplication()Landroid/app/Application;

    move-result-object p1

    check-cast p1, Lcom/stagnationlab/sk/MyApplication;

    new-instance v1, Lcom/stagnationlab/sk/fcm/MyFirebaseMessagingService$1;

    invoke-direct {v1, p0, v0}, Lcom/stagnationlab/sk/fcm/MyFirebaseMessagingService$1;-><init>(Lcom/stagnationlab/sk/fcm/MyFirebaseMessagingService;Lcom/stagnationlab/sk/models/PushMessage;)V

    invoke-virtual {p1, v1}, Lcom/stagnationlab/sk/MyApplication;->a(Lcom/stagnationlab/sk/e/a;)V

    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 2

    .line 55
    invoke-super {p0, p1}, Lcom/google/firebase/messaging/FirebaseMessagingService;->b(Ljava/lang/String;)V

    .line 57
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Received new FCM token: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "MyFirebaseMessagingService"

    invoke-static {v1, v0}, Lcom/stagnationlab/sk/f;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 59
    invoke-static {p0, p1}, Lcom/stagnationlab/sk/fcm/b;->a(Landroid/app/Service;Ljava/lang/String;)V

    return-void
.end method

.method public onDestroy()V
    .locals 0

    .line 390
    invoke-direct {p0}, Lcom/stagnationlab/sk/fcm/MyFirebaseMessagingService;->f()V

    .line 391
    invoke-super {p0}, Lcom/google/firebase/messaging/FirebaseMessagingService;->onDestroy()V

    return-void
.end method
