.class final Lcom/stagnationlab/sk/fcm/b$2;
.super Ljava/lang/Object;
.source "FcmTokenManager.java"

# interfaces
.implements Lcom/google/android/gms/h/c;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/stagnationlab/sk/fcm/b;->a(Landroid/app/Activity;Lcom/stagnationlab/sk/fcm/d;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/gms/h/c<",
        "Lcom/google/firebase/iid/a;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Landroid/app/Activity;

.field final synthetic b:Lcom/stagnationlab/sk/fcm/d;


# direct methods
.method constructor <init>(Landroid/app/Activity;Lcom/stagnationlab/sk/fcm/d;)V
    .locals 0

    .line 47
    iput-object p1, p0, Lcom/stagnationlab/sk/fcm/b$2;->a:Landroid/app/Activity;

    iput-object p2, p0, Lcom/stagnationlab/sk/fcm/b$2;->b:Lcom/stagnationlab/sk/fcm/d;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/google/android/gms/h/h;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/gms/h/h<",
            "Lcom/google/firebase/iid/a;",
            ">;)V"
        }
    .end annotation

    .line 52
    invoke-virtual {p1}, Lcom/google/android/gms/h/h;->b()Z

    move-result v0

    const-string v1, "FcmTokenManager"

    if-nez v0, :cond_0

    .line 53
    invoke-virtual {p1}, Lcom/google/android/gms/h/h;->e()Ljava/lang/Exception;

    move-result-object p1

    const-string v0, "getInstanceId failed"

    invoke-static {v1, v0, p1}, Lcom/stagnationlab/sk/f;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 55
    :cond_0
    invoke-virtual {p1}, Lcom/google/android/gms/h/h;->d()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/google/firebase/iid/a;

    if-nez p1, :cond_1

    const-string p1, "getInstanceId result is null"

    .line 58
    invoke-static {v1, p1}, Lcom/stagnationlab/sk/f;->c(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    const/4 p1, 0x0

    goto :goto_1

    .line 60
    :cond_1
    invoke-interface {p1}, Lcom/google/firebase/iid/a;->a()Ljava/lang/String;

    move-result-object p1

    .line 61
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Got FCM token: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/stagnationlab/sk/f;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 65
    :goto_1
    iget-object v0, p0, Lcom/stagnationlab/sk/fcm/b$2;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getApplication()Landroid/app/Application;

    move-result-object v1

    check-cast v1, Lcom/stagnationlab/sk/MyApplication;

    iget-object v2, p0, Lcom/stagnationlab/sk/fcm/b$2;->b:Lcom/stagnationlab/sk/fcm/d;

    invoke-static {v0, v1, p1, v2}, Lcom/stagnationlab/sk/fcm/b;->a(Landroid/content/Context;Lcom/stagnationlab/sk/MyApplication;Ljava/lang/String;Lcom/stagnationlab/sk/fcm/d;)V

    return-void
.end method
