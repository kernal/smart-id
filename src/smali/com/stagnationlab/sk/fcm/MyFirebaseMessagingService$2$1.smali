.class Lcom/stagnationlab/sk/fcm/MyFirebaseMessagingService$2$1;
.super Ljava/lang/Object;
.source "MyFirebaseMessagingService.java"

# interfaces
.implements Lcom/stagnationlab/sk/a/a/o;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/stagnationlab/sk/fcm/MyFirebaseMessagingService$2;->a(Lcom/stagnationlab/sk/a/a;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/stagnationlab/sk/fcm/MyFirebaseMessagingService$2;


# direct methods
.method constructor <init>(Lcom/stagnationlab/sk/fcm/MyFirebaseMessagingService$2;)V
    .locals 0

    .line 114
    iput-object p1, p0, Lcom/stagnationlab/sk/fcm/MyFirebaseMessagingService$2$1;->a:Lcom/stagnationlab/sk/fcm/MyFirebaseMessagingService$2;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/stagnationlab/sk/c/a;)V
    .locals 2

    .line 130
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Failed to get pending data: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "MyFirebaseMessagingService"

    invoke-static {v1, v0}, Lcom/stagnationlab/sk/f;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 132
    iget-object v0, p0, Lcom/stagnationlab/sk/fcm/MyFirebaseMessagingService$2$1;->a:Lcom/stagnationlab/sk/fcm/MyFirebaseMessagingService$2;

    iget-object v0, v0, Lcom/stagnationlab/sk/fcm/MyFirebaseMessagingService$2;->b:Lcom/stagnationlab/sk/fcm/MyFirebaseMessagingService;

    iget-object v1, p0, Lcom/stagnationlab/sk/fcm/MyFirebaseMessagingService$2$1;->a:Lcom/stagnationlab/sk/fcm/MyFirebaseMessagingService$2;

    iget-object v1, v1, Lcom/stagnationlab/sk/fcm/MyFirebaseMessagingService$2;->a:Lcom/stagnationlab/sk/models/PushMessage;

    invoke-static {v0, p1, v1}, Lcom/stagnationlab/sk/fcm/MyFirebaseMessagingService;->a(Lcom/stagnationlab/sk/fcm/MyFirebaseMessagingService;Lcom/stagnationlab/sk/c/a;Lcom/stagnationlab/sk/models/PushMessage;)V

    return-void
.end method

.method public a(Lcom/stagnationlab/sk/models/Transaction;Lcom/stagnationlab/sk/models/RpRequest;)V
    .locals 1

    if-eqz p1, :cond_0

    .line 119
    invoke-virtual {p1}, Lcom/stagnationlab/sk/models/Transaction;->m()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 120
    iget-object p1, p0, Lcom/stagnationlab/sk/fcm/MyFirebaseMessagingService$2$1;->a:Lcom/stagnationlab/sk/fcm/MyFirebaseMessagingService$2;

    iget-object p1, p1, Lcom/stagnationlab/sk/fcm/MyFirebaseMessagingService$2;->b:Lcom/stagnationlab/sk/fcm/MyFirebaseMessagingService;

    invoke-static {p1}, Lcom/stagnationlab/sk/fcm/MyFirebaseMessagingService;->a(Lcom/stagnationlab/sk/fcm/MyFirebaseMessagingService;)V

    return-void

    .line 125
    :cond_0
    iget-object v0, p0, Lcom/stagnationlab/sk/fcm/MyFirebaseMessagingService$2$1;->a:Lcom/stagnationlab/sk/fcm/MyFirebaseMessagingService$2;

    iget-object v0, v0, Lcom/stagnationlab/sk/fcm/MyFirebaseMessagingService$2;->b:Lcom/stagnationlab/sk/fcm/MyFirebaseMessagingService;

    invoke-static {v0, p1, p2}, Lcom/stagnationlab/sk/fcm/MyFirebaseMessagingService;->a(Lcom/stagnationlab/sk/fcm/MyFirebaseMessagingService;Lcom/stagnationlab/sk/models/Transaction;Lcom/stagnationlab/sk/models/RpRequest;)V

    return-void
.end method
