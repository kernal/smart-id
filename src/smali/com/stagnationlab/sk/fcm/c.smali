.class public Lcom/stagnationlab/sk/fcm/c;
.super Ljava/lang/Object;
.source "NotificationChannelManager.java"


# static fields
.field private static final a:[J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x4

    .line 16
    new-array v0, v0, [J

    fill-array-data v0, :array_0

    sput-object v0, Lcom/stagnationlab/sk/fcm/c;->a:[J

    return-void

    nop

    :array_0
    .array-data 8
        0x0
        0xfa
        0xfa
        0xfa
    .end array-data
.end method

.method public static a(Landroid/content/Context;)Landroid/net/Uri;
    .locals 3

    .line 41
    invoke-static {}, Lcom/stagnationlab/sk/fcm/c;->b()Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_1

    .line 42
    invoke-static {}, Lcom/stagnationlab/sk/a/d;->g()Lcom/stagnationlab/sk/b/a;

    move-result-object p0

    sget-object v0, Lcom/stagnationlab/sk/b/a;->b:Lcom/stagnationlab/sk/b/a;

    if-ne p0, v0, :cond_0

    const/4 p0, 0x2

    .line 43
    invoke-static {p0}, Landroid/media/RingtoneManager;->getDefaultUri(I)Landroid/net/Uri;

    move-result-object v1

    :cond_0
    return-object v1

    .line 46
    :cond_1
    invoke-static {p0}, Lcom/stagnationlab/sk/fcm/c;->f(Landroid/content/Context;)Landroid/app/NotificationChannel;

    move-result-object p0

    if-eqz p0, :cond_2

    .line 48
    invoke-virtual {p0}, Landroid/app/NotificationChannel;->getImportance()I

    move-result v0

    const/4 v2, 0x3

    if-lt v0, v2, :cond_2

    .line 49
    invoke-virtual {p0}, Landroid/app/NotificationChannel;->getSound()Landroid/net/Uri;

    move-result-object v1

    :cond_2
    return-object v1
.end method

.method public static a()Z
    .locals 2

    .line 96
    invoke-static {}, Lcom/stagnationlab/sk/fcm/c;->b()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/stagnationlab/sk/a/d;->g()Lcom/stagnationlab/sk/b/a;

    move-result-object v0

    sget-object v1, Lcom/stagnationlab/sk/b/a;->b:Lcom/stagnationlab/sk/b/a;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public static b()Z
    .locals 2

    .line 100
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x1a

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public static b(Landroid/content/Context;)[J
    .locals 2

    .line 55
    invoke-static {}, Lcom/stagnationlab/sk/fcm/c;->b()Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return-object v1

    .line 58
    :cond_0
    invoke-static {p0}, Lcom/stagnationlab/sk/fcm/c;->f(Landroid/content/Context;)Landroid/app/NotificationChannel;

    move-result-object p0

    if-eqz p0, :cond_3

    .line 60
    invoke-virtual {p0}, Landroid/app/NotificationChannel;->shouldVibrate()Z

    move-result v0

    if-nez v0, :cond_1

    goto :goto_0

    .line 64
    :cond_1
    invoke-virtual {p0}, Landroid/app/NotificationChannel;->getVibrationPattern()[J

    move-result-object p0

    if-nez p0, :cond_2

    .line 67
    sget-object p0, Lcom/stagnationlab/sk/fcm/c;->a:[J

    :cond_2
    return-object p0

    :cond_3
    :goto_0
    return-object v1
.end method

.method public static c(Landroid/content/Context;)V
    .locals 0

    .line 88
    invoke-static {p0}, Lcom/stagnationlab/sk/fcm/c;->e(Landroid/content/Context;)V

    return-void
.end method

.method public static d(Landroid/content/Context;)V
    .locals 0

    .line 92
    invoke-static {p0}, Lcom/stagnationlab/sk/fcm/c;->e(Landroid/content/Context;)V

    return-void
.end method

.method private static e(Landroid/content/Context;)V
    .locals 5

    .line 19
    invoke-static {}, Lcom/stagnationlab/sk/fcm/c;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "notification"

    .line 21
    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    if-nez v0, :cond_0

    return-void

    .line 27
    :cond_0
    new-instance v1, Landroid/app/NotificationChannel;

    const v2, 0x7f0e00f7

    .line 29
    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 30
    invoke-static {}, Lcom/stagnationlab/sk/a/d;->g()Lcom/stagnationlab/sk/b/a;

    move-result-object v3

    sget-object v4, Lcom/stagnationlab/sk/b/a;->b:Lcom/stagnationlab/sk/b/a;

    if-ne v3, v4, :cond_1

    const/4 v3, 0x4

    goto :goto_0

    :cond_1
    const/4 v3, 0x2

    :goto_0
    const-string v4, "TRANSACTION"

    invoke-direct {v1, v4, v2, v3}, Landroid/app/NotificationChannel;-><init>(Ljava/lang/String;Ljava/lang/CharSequence;I)V

    const v2, 0x7f0e00f6

    .line 35
    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v1, p0}, Landroid/app/NotificationChannel;->setDescription(Ljava/lang/String;)V

    .line 36
    invoke-virtual {v0, v1}, Landroid/app/NotificationManager;->createNotificationChannel(Landroid/app/NotificationChannel;)V

    :cond_2
    return-void
.end method

.method private static f(Landroid/content/Context;)Landroid/app/NotificationChannel;
    .locals 1

    .line 75
    invoke-static {}, Lcom/stagnationlab/sk/fcm/c;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "notification"

    .line 77
    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Landroid/app/NotificationManager;

    if-eqz p0, :cond_0

    const-string v0, "TRANSACTION"

    .line 80
    invoke-virtual {p0, v0}, Landroid/app/NotificationManager;->getNotificationChannel(Ljava/lang/String;)Landroid/app/NotificationChannel;

    move-result-object p0

    return-object p0

    :cond_0
    const/4 p0, 0x0

    return-object p0
.end method
