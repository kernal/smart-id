.class public Lcom/stagnationlab/sk/fcm/ClearNotificationReceiver;
.super Landroid/content/BroadcastReceiver;
.source "ClearNotificationReceiver.java"


# static fields
.field private static a:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 11
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method private static a(Landroid/content/Context;I)V
    .locals 1

    const-string v0, "notification"

    .line 39
    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Landroid/app/NotificationManager;

    invoke-virtual {p0, p1}, Landroid/app/NotificationManager;->cancel(I)V

    return-void
.end method

.method public static a(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2

    const/4 v0, -0x1

    const-string v1, "notificationId"

    .line 29
    invoke-virtual {p1, v1, v0}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    if-eq v1, v0, :cond_0

    .line 32
    invoke-static {p0, v1}, Lcom/stagnationlab/sk/fcm/ClearNotificationReceiver;->a(Landroid/content/Context;I)V

    goto :goto_0

    .line 33
    :cond_0
    sget-object v0, Lcom/stagnationlab/sk/fcm/ClearNotificationReceiver;->a:Ljava/lang/String;

    if-eqz v0, :cond_1

    invoke-static {p1}, Lcom/stagnationlab/sk/fcm/ClearNotificationReceiver;->b(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    :cond_1
    const/4 p1, 0x1

    .line 34
    invoke-static {p0, p1}, Lcom/stagnationlab/sk/fcm/ClearNotificationReceiver;->a(Landroid/content/Context;I)V

    :cond_2
    :goto_0
    return-void
.end method

.method public static a(Landroid/content/Intent;)V
    .locals 0

    .line 43
    invoke-static {p0}, Lcom/stagnationlab/sk/fcm/ClearNotificationReceiver;->b(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object p0

    sput-object p0, Lcom/stagnationlab/sk/fcm/ClearNotificationReceiver;->a:Ljava/lang/String;

    return-void
.end method

.method private static b(Landroid/content/Intent;)Ljava/lang/String;
    .locals 2

    const-string v0, "transaction"

    .line 47
    invoke-virtual {p0, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 48
    invoke-virtual {p0, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/stagnationlab/sk/models/Transaction;

    if-eqz v0, :cond_0

    .line 51
    new-instance p0, Ljava/lang/StringBuilder;

    invoke-direct {p0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "transactionUuid:"

    invoke-virtual {p0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Lcom/stagnationlab/sk/models/Transaction;->f()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0

    :cond_0
    const-string v0, "rpRequest"

    .line 55
    invoke-virtual {p0, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 56
    invoke-virtual {p0, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object p0

    check-cast p0, Lcom/stagnationlab/sk/models/RpRequest;

    if-eqz p0, :cond_1

    .line 59
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "rpRequestUuid:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/stagnationlab/sk/models/RpRequest;->i()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0

    :cond_1
    const/4 p0, 0x0

    return-object p0
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 0

    .line 19
    invoke-static {p1, p2}, Lcom/stagnationlab/sk/fcm/ClearNotificationReceiver;->a(Landroid/content/Context;Landroid/content/Intent;)V

    return-void
.end method
