.class public Lcom/stagnationlab/sk/fcm/b;
.super Ljava/lang/Object;
.source "FcmTokenManager.java"


# direct methods
.method private static a(Landroid/app/Activity;)Lcom/stagnationlab/sk/c/a;
    .locals 3

    .line 125
    invoke-static {}, Lcom/google/android/gms/common/e;->a()Lcom/google/android/gms/common/e;

    move-result-object v0

    .line 126
    invoke-virtual {v0, p0}, Lcom/google/android/gms/common/e;->a(Landroid/content/Context;)I

    move-result v1

    if-eqz v1, :cond_1

    .line 128
    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/e;->a(I)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v2, 0x2328

    .line 129
    invoke-virtual {v0, p0, v1, v2}, Lcom/google/android/gms/common/e;->a(Landroid/app/Activity;II)Landroid/app/Dialog;

    move-result-object p0

    .line 131
    invoke-virtual {p0}, Landroid/app/Dialog;->show()V

    goto :goto_0

    :cond_0
    const-string p0, "FcmTokenManager"

    const-string v0, "This device is not supported."

    .line 133
    invoke-static {p0, v0}, Lcom/stagnationlab/sk/f;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 135
    :goto_0
    new-instance p0, Lcom/stagnationlab/sk/c/a;

    sget-object v0, Lcom/stagnationlab/sk/c/b;->F:Lcom/stagnationlab/sk/c/b;

    invoke-direct {p0, v0}, Lcom/stagnationlab/sk/c/a;-><init>(Lcom/stagnationlab/sk/c/b;)V

    return-object p0

    :cond_1
    const/4 p0, 0x0

    return-object p0
.end method

.method public static a(Landroid/app/Activity;Lcom/stagnationlab/sk/fcm/d;)V
    .locals 2

    .line 38
    invoke-static {p0}, Lcom/stagnationlab/sk/fcm/b;->a(Landroid/app/Activity;)Lcom/stagnationlab/sk/c/a;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 41
    invoke-interface {p1, v0}, Lcom/stagnationlab/sk/fcm/d;->a(Lcom/stagnationlab/sk/c/a;)V

    return-void

    .line 46
    :cond_0
    invoke-static {}, Lcom/google/firebase/iid/FirebaseInstanceId;->a()Lcom/google/firebase/iid/FirebaseInstanceId;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/firebase/iid/FirebaseInstanceId;->d()Lcom/google/android/gms/h/h;

    move-result-object v0

    new-instance v1, Lcom/stagnationlab/sk/fcm/b$2;

    invoke-direct {v1, p0, p1}, Lcom/stagnationlab/sk/fcm/b$2;-><init>(Landroid/app/Activity;Lcom/stagnationlab/sk/fcm/d;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/h/h;->a(Lcom/google/android/gms/h/c;)Lcom/google/android/gms/h/h;

    return-void
.end method

.method static a(Landroid/app/Service;Ljava/lang/String;)V
    .locals 2

    .line 30
    invoke-virtual {p0}, Landroid/app/Service;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/stagnationlab/sk/MyApplication;

    new-instance v1, Lcom/stagnationlab/sk/fcm/b$1;

    invoke-direct {v1}, Lcom/stagnationlab/sk/fcm/b$1;-><init>()V

    invoke-static {p0, v0, p1, v1}, Lcom/stagnationlab/sk/fcm/b;->b(Landroid/content/Context;Lcom/stagnationlab/sk/MyApplication;Ljava/lang/String;Lcom/stagnationlab/sk/fcm/d;)V

    return-void
.end method

.method static synthetic a(Landroid/content/Context;Lcom/stagnationlab/sk/MyApplication;Ljava/lang/String;Lcom/stagnationlab/sk/fcm/d;)V
    .locals 0

    .line 25
    invoke-static {p0, p1, p2, p3}, Lcom/stagnationlab/sk/fcm/b;->b(Landroid/content/Context;Lcom/stagnationlab/sk/MyApplication;Ljava/lang/String;Lcom/stagnationlab/sk/fcm/d;)V

    return-void
.end method

.method private static b(Landroid/content/Context;Lcom/stagnationlab/sk/MyApplication;Ljava/lang/String;Lcom/stagnationlab/sk/fcm/d;)V
    .locals 1

    if-nez p2, :cond_0

    const/4 p0, 0x0

    .line 78
    invoke-static {p0}, Lcom/stagnationlab/sk/a/d;->d(Z)V

    .line 80
    new-instance p0, Lcom/stagnationlab/sk/c/a;

    sget-object p1, Lcom/stagnationlab/sk/c/b;->G:Lcom/stagnationlab/sk/c/b;

    invoke-direct {p0, p1}, Lcom/stagnationlab/sk/c/a;-><init>(Lcom/stagnationlab/sk/c/b;)V

    invoke-interface {p3, p0}, Lcom/stagnationlab/sk/fcm/d;->a(Lcom/stagnationlab/sk/c/a;)V

    return-void

    .line 85
    :cond_0
    new-instance v0, Lcom/stagnationlab/sk/fcm/b$3;

    invoke-direct {v0, p2, p3, p0}, Lcom/stagnationlab/sk/fcm/b$3;-><init>(Ljava/lang/String;Lcom/stagnationlab/sk/fcm/d;Landroid/content/Context;)V

    invoke-virtual {p1, v0}, Lcom/stagnationlab/sk/MyApplication;->a(Lcom/stagnationlab/sk/a/a/a;)V

    return-void
.end method
