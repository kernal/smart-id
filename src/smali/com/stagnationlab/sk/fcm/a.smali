.class public Lcom/stagnationlab/sk/fcm/a;
.super Ljava/lang/Object;
.source "ActivityManager.java"


# static fields
.field private static a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Landroid/app/Activity;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 9
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/stagnationlab/sk/fcm/a;->a:Ljava/util/List;

    return-void
.end method

.method public static a()Landroid/app/Activity;
    .locals 2

    .line 20
    sget-object v0, Lcom/stagnationlab/sk/fcm/a;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    sget-object v0, Lcom/stagnationlab/sk/fcm/a;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    :goto_0
    return-object v0
.end method

.method public static a(Landroid/app/Activity;)V
    .locals 1

    .line 12
    sget-object v0, Lcom/stagnationlab/sk/fcm/a;->a:Ljava/util/List;

    invoke-interface {v0, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public static b(Landroid/app/Activity;)V
    .locals 1

    .line 16
    sget-object v0, Lcom/stagnationlab/sk/fcm/a;->a:Ljava/util/List;

    invoke-interface {v0, p0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    return-void
.end method

.method public static c(Landroid/app/Activity;)Z
    .locals 1

    .line 24
    sget-object v0, Lcom/stagnationlab/sk/fcm/a;->a:Ljava/util/List;

    invoke-interface {v0, p0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result p0

    return p0
.end method
