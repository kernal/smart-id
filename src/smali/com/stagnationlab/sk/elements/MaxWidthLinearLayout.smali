.class public Lcom/stagnationlab/sk/elements/MaxWidthLinearLayout;
.super Landroid/widget/LinearLayout;
.source "MaxWidthLinearLayout.java"


# instance fields
.field private a:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .line 15
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    const/4 p1, 0x0

    .line 16
    iput p1, p0, Lcom/stagnationlab/sk/elements/MaxWidthLinearLayout;->a:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .line 20
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 21
    invoke-virtual {p0}, Lcom/stagnationlab/sk/elements/MaxWidthLinearLayout;->getContext()Landroid/content/Context;

    move-result-object p1

    sget-object v0, Lcom/stagnationlab/sk/h$a;->MaxWidthLayout:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object p1

    const/4 p2, 0x0

    const v0, 0x7fffffff

    .line 22
    invoke-virtual {p1, p2, v0}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result p2

    iput p2, p0, Lcom/stagnationlab/sk/elements/MaxWidthLinearLayout;->a:I

    .line 23
    invoke-virtual {p1}, Landroid/content/res/TypedArray;->recycle()V

    return-void
.end method


# virtual methods
.method protected onMeasure(II)V
    .locals 3

    .line 28
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 29
    invoke-virtual {p0}, Lcom/stagnationlab/sk/elements/MaxWidthLinearLayout;->getSuggestedMinimumWidth()I

    move-result v1

    .line 30
    iget v2, p0, Lcom/stagnationlab/sk/elements/MaxWidthLinearLayout;->a:I

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v2

    if-ge v0, v1, :cond_0

    goto :goto_0

    :cond_0
    if-ge v2, v0, :cond_1

    move v1, v2

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    :goto_0
    if-lez v1, :cond_2

    .line 40
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result p1

    .line 41
    invoke-static {v1, p1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result p1

    .line 43
    :cond_2
    invoke-super {p0, p1, p2}, Landroid/widget/LinearLayout;->onMeasure(II)V

    return-void
.end method

.method public setMaxWidth(I)V
    .locals 2

    int-to-float p1, p1

    .line 48
    invoke-virtual {p0}, Lcom/stagnationlab/sk/elements/MaxWidthLinearLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    const/4 v1, 0x1

    .line 47
    invoke-static {v1, p1, v0}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result p1

    float-to-int p1, p1

    iput p1, p0, Lcom/stagnationlab/sk/elements/MaxWidthLinearLayout;->a:I

    .line 50
    invoke-virtual {p0}, Lcom/stagnationlab/sk/elements/MaxWidthLinearLayout;->invalidate()V

    return-void
.end method
