.class public Lcom/stagnationlab/sk/elements/Modal;
.super Landroid/widget/RelativeLayout;
.source "Modal.java"


# instance fields
.field private a:Z

.field private b:Landroid/view/View;

.field private c:Landroid/view/ViewGroup;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3

    .line 21
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 23
    invoke-virtual {p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    sget-object v1, Lcom/stagnationlab/sk/h$a;->Modal:[I

    const/4 v2, 0x0

    invoke-virtual {v0, p2, v1, v2, v2}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object p2

    .line 31
    :try_start_0
    invoke-virtual {p2, v2, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    iput-boolean v0, p0, Lcom/stagnationlab/sk/elements/Modal;->a:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 33
    invoke-virtual {p2}, Landroid/content/res/TypedArray;->recycle()V

    .line 36
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object p1

    const p2, 0x7f0a0028

    const/4 v0, 0x1

    invoke-virtual {p1, p2, p0, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    const p1, 0x7f08007b

    .line 37
    invoke-virtual {p0, p1}, Lcom/stagnationlab/sk/elements/Modal;->findViewById(I)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lcom/stagnationlab/sk/elements/Modal;->b:Landroid/view/View;

    const p1, 0x7f08007c

    .line 38
    invoke-virtual {p0, p1}, Lcom/stagnationlab/sk/elements/Modal;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/view/ViewGroup;

    iput-object p1, p0, Lcom/stagnationlab/sk/elements/Modal;->c:Landroid/view/ViewGroup;

    .line 40
    iget-boolean p1, p0, Lcom/stagnationlab/sk/elements/Modal;->a:Z

    invoke-direct {p0, p1}, Lcom/stagnationlab/sk/elements/Modal;->setAlpha(Z)V

    return-void

    :catchall_0
    move-exception p1

    .line 33
    invoke-virtual {p2}, Landroid/content/res/TypedArray;->recycle()V

    throw p1
.end method

.method private a(Landroid/view/View;Z)V
    .locals 2

    if-eqz p2, :cond_0

    const/high16 p2, 0x3f800000    # 1.0f

    goto :goto_0

    :cond_0
    const/4 p2, 0x0

    .line 86
    :goto_0
    invoke-virtual {p1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object p1

    invoke-virtual {p0}, Lcom/stagnationlab/sk/elements/Modal;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f09000d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    int-to-long v0, v0

    invoke-virtual {p1, v0, v1}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object p1

    invoke-virtual {p1, p2}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    return-void
.end method

.method private setAlpha(Z)V
    .locals 1

    if-eqz p1, :cond_0

    const/high16 p1, 0x3f800000    # 1.0f

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    .line 79
    :goto_0
    iget-object v0, p0, Lcom/stagnationlab/sk/elements/Modal;->c:Landroid/view/ViewGroup;

    invoke-virtual {v0, p1}, Landroid/view/ViewGroup;->setAlpha(F)V

    .line 80
    iget-object v0, p0, Lcom/stagnationlab/sk/elements/Modal;->b:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setAlpha(F)V

    return-void
.end method


# virtual methods
.method public a(ZZ)V
    .locals 3

    .line 59
    iget-boolean v0, p0, Lcom/stagnationlab/sk/elements/Modal;->a:Z

    if-ne v0, p1, :cond_0

    return-void

    .line 63
    :cond_0
    iput-boolean p1, p0, Lcom/stagnationlab/sk/elements/Modal;->a:Z

    const/4 v0, 0x0

    if-eqz p2, :cond_3

    .line 66
    iget-object p2, p0, Lcom/stagnationlab/sk/elements/Modal;->b:Landroid/view/View;

    invoke-direct {p0, p2, p1}, Lcom/stagnationlab/sk/elements/Modal;->a(Landroid/view/View;Z)V

    .line 67
    iget-object p2, p0, Lcom/stagnationlab/sk/elements/Modal;->c:Landroid/view/ViewGroup;

    invoke-direct {p0, p2, p1}, Lcom/stagnationlab/sk/elements/Modal;->a(Landroid/view/View;Z)V

    .line 68
    iget-object p2, p0, Lcom/stagnationlab/sk/elements/Modal;->c:Landroid/view/ViewGroup;

    const/high16 v1, -0x3de00000    # -40.0f

    if-eqz p1, :cond_1

    const/high16 v2, -0x3de00000    # -40.0f

    goto :goto_0

    :cond_1
    const/4 v2, 0x0

    :goto_0
    invoke-virtual {p2, v2}, Landroid/view/ViewGroup;->setTranslationY(F)V

    .line 69
    iget-object p2, p0, Lcom/stagnationlab/sk/elements/Modal;->c:Landroid/view/ViewGroup;

    invoke-virtual {p2}, Landroid/view/ViewGroup;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object p2

    if-eqz p1, :cond_2

    goto :goto_1

    :cond_2
    const/high16 v0, -0x3de00000    # -40.0f

    :goto_1
    invoke-virtual {p2, v0}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    goto :goto_2

    .line 71
    :cond_3
    iget-object p2, p0, Lcom/stagnationlab/sk/elements/Modal;->c:Landroid/view/ViewGroup;

    invoke-virtual {p2, v0}, Landroid/view/ViewGroup;->setTranslationY(F)V

    .line 72
    invoke-direct {p0, p1}, Lcom/stagnationlab/sk/elements/Modal;->setAlpha(Z)V

    :goto_2
    return-void
.end method

.method public addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V
    .locals 2

    .line 45
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f08007b

    if-eq v0, v1, :cond_1

    const v1, 0x7f08007c

    if-ne v0, v1, :cond_0

    goto :goto_0

    .line 50
    :cond_0
    iget-object v0, p0, Lcom/stagnationlab/sk/elements/Modal;->c:Landroid/view/ViewGroup;

    invoke-virtual {v0, p1, p2, p3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    goto :goto_1

    .line 48
    :cond_1
    :goto_0
    invoke-super {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    :goto_1
    return-void
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .line 91
    iget-boolean v0, p0, Lcom/stagnationlab/sk/elements/Modal;->a:Z

    if-eqz v0, :cond_0

    .line 92
    invoke-super {p0, p1}, Landroid/widget/RelativeLayout;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result p1

    return p1

    :cond_0
    const/4 p1, 0x1

    return p1
.end method

.method public setVisible(Z)V
    .locals 1

    const/4 v0, 0x1

    .line 55
    invoke-virtual {p0, p1, v0}, Lcom/stagnationlab/sk/elements/Modal;->a(ZZ)V

    return-void
.end method
