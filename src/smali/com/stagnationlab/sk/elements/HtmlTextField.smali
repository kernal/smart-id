.class public Lcom/stagnationlab/sk/elements/HtmlTextField;
.super Lcom/stagnationlab/sk/elements/TextField;
.source "HtmlTextField.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .line 12
    invoke-direct {p0, p1}, Lcom/stagnationlab/sk/elements/TextField;-><init>(Landroid/content/Context;)V

    .line 14
    invoke-direct {p0, p1}, Lcom/stagnationlab/sk/elements/HtmlTextField;->a(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 18
    invoke-direct {p0, p1, p2}, Lcom/stagnationlab/sk/elements/TextField;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 20
    invoke-direct {p0, p1}, Lcom/stagnationlab/sk/elements/HtmlTextField;->a(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .line 24
    invoke-direct {p0, p1, p2, p3}, Lcom/stagnationlab/sk/elements/TextField;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 26
    invoke-direct {p0, p1}, Lcom/stagnationlab/sk/elements/HtmlTextField;->a(Landroid/content/Context;)V

    return-void
.end method

.method private a(Landroid/content/Context;)V
    .locals 2

    .line 30
    invoke-virtual {p0}, Lcom/stagnationlab/sk/elements/HtmlTextField;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {p1, p0, v0, v1}, Lcom/stagnationlab/sk/util/k;->a(Landroid/content/Context;Landroid/widget/TextView;Ljava/lang/String;Z)V

    const/4 p1, 0x0

    .line 31
    invoke-virtual {p0, p1}, Lcom/stagnationlab/sk/elements/HtmlTextField;->setHighlightColor(I)V

    return-void
.end method
