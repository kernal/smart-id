.class public Lcom/stagnationlab/sk/elements/ScaleButton;
.super Lcom/stagnationlab/sk/elements/BaseButton;
.source "ScaleButton.java"


# instance fields
.field private b:Landroid/view/animation/ScaleAnimation;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .line 13
    invoke-direct {p0, p1}, Lcom/stagnationlab/sk/elements/BaseButton;-><init>(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 17
    invoke-direct {p0, p1, p2}, Lcom/stagnationlab/sk/elements/BaseButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .line 21
    invoke-direct {p0, p1, p2, p3}, Lcom/stagnationlab/sk/elements/BaseButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method static synthetic a(Lcom/stagnationlab/sk/elements/ScaleButton;Landroid/view/animation/ScaleAnimation;)Landroid/view/animation/ScaleAnimation;
    .locals 0

    .line 9
    iput-object p1, p0, Lcom/stagnationlab/sk/elements/ScaleButton;->b:Landroid/view/animation/ScaleAnimation;

    return-object p1
.end method

.method private a(FF)V
    .locals 10

    .line 41
    iget-object v0, p0, Lcom/stagnationlab/sk/elements/ScaleButton;->b:Landroid/view/animation/ScaleAnimation;

    if-eqz v0, :cond_0

    .line 42
    invoke-virtual {v0}, Landroid/view/animation/ScaleAnimation;->cancel()V

    .line 45
    :cond_0
    new-instance v0, Landroid/view/animation/ScaleAnimation;

    const/4 v6, 0x1

    const/high16 v7, 0x3f000000    # 0.5f

    const/4 v8, 0x1

    const/high16 v9, 0x3f000000    # 0.5f

    move-object v1, v0

    move v2, p1

    move v3, p2

    move v4, p1

    move v5, p2

    invoke-direct/range {v1 .. v9}, Landroid/view/animation/ScaleAnimation;-><init>(FFFFIFIF)V

    iput-object v0, p0, Lcom/stagnationlab/sk/elements/ScaleButton;->b:Landroid/view/animation/ScaleAnimation;

    .line 55
    iget-object p1, p0, Lcom/stagnationlab/sk/elements/ScaleButton;->b:Landroid/view/animation/ScaleAnimation;

    const/4 p2, 0x1

    invoke-virtual {p1, p2}, Landroid/view/animation/ScaleAnimation;->setFillAfter(Z)V

    .line 56
    iget-object p1, p0, Lcom/stagnationlab/sk/elements/ScaleButton;->b:Landroid/view/animation/ScaleAnimation;

    const-wide/16 v0, 0x19

    invoke-virtual {p1, v0, v1}, Landroid/view/animation/ScaleAnimation;->setDuration(J)V

    .line 57
    iget-object p1, p0, Lcom/stagnationlab/sk/elements/ScaleButton;->b:Landroid/view/animation/ScaleAnimation;

    new-instance p2, Lcom/stagnationlab/sk/elements/ScaleButton$1;

    invoke-direct {p2, p0}, Lcom/stagnationlab/sk/elements/ScaleButton$1;-><init>(Lcom/stagnationlab/sk/elements/ScaleButton;)V

    invoke-virtual {p1, p2}, Landroid/view/animation/ScaleAnimation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 70
    iget-object p1, p0, Lcom/stagnationlab/sk/elements/ScaleButton;->b:Landroid/view/animation/ScaleAnimation;

    invoke-virtual {p0, p1}, Lcom/stagnationlab/sk/elements/ScaleButton;->startAnimation(Landroid/view/animation/Animation;)V

    return-void
.end method


# virtual methods
.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 4

    .line 26
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    const v1, 0x3f7d70a4    # 0.99f

    const/high16 v2, 0x3f800000    # 1.0f

    if-eqz v0, :cond_1

    const/4 v3, 0x1

    if-eq v0, v3, :cond_0

    const/4 v3, 0x3

    if-eq v0, v3, :cond_0

    goto :goto_0

    .line 33
    :cond_0
    invoke-direct {p0, v1, v2}, Lcom/stagnationlab/sk/elements/ScaleButton;->a(FF)V

    goto :goto_0

    .line 28
    :cond_1
    invoke-direct {p0, v2, v1}, Lcom/stagnationlab/sk/elements/ScaleButton;->a(FF)V

    .line 37
    :goto_0
    invoke-super {p0, p1}, Lcom/stagnationlab/sk/elements/BaseButton;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result p1

    return p1
.end method
