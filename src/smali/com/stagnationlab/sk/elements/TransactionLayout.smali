.class public Lcom/stagnationlab/sk/elements/TransactionLayout;
.super Landroid/widget/LinearLayout;
.source "TransactionLayout.java"


# instance fields
.field private a:[I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .line 15
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    const/4 p1, 0x4

    .line 12
    new-array p1, p1, [I

    iput-object p1, p0, Lcom/stagnationlab/sk/elements/TransactionLayout;->a:[I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 19
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 p1, 0x4

    .line 12
    new-array p1, p1, [I

    iput-object p1, p0, Lcom/stagnationlab/sk/elements/TransactionLayout;->a:[I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .line 23
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/4 p1, 0x4

    .line 12
    new-array p1, p1, [I

    iput-object p1, p0, Lcom/stagnationlab/sk/elements/TransactionLayout;->a:[I

    return-void
.end method

.method private a(Landroid/view/View;IIII)V
    .locals 2

    const/high16 v0, 0x40000000    # 2.0f

    .line 63
    invoke-static {p4, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 64
    invoke-static {p5, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 62
    invoke-virtual {p1, v1, v0}, Landroid/view/View;->measure(II)V

    add-int/2addr p4, p2

    add-int/2addr p5, p3

    .line 66
    invoke-virtual {p1, p2, p3, p4, p5}, Landroid/view/View;->layout(IIII)V

    return-void
.end method


# virtual methods
.method protected onLayout(ZIIII)V
    .locals 6

    .line 34
    invoke-virtual {p0}, Lcom/stagnationlab/sk/elements/TransactionLayout;->getPaddingLeft()I

    move-result p1

    .line 35
    invoke-virtual {p0}, Lcom/stagnationlab/sk/elements/TransactionLayout;->getPaddingTop()I

    move-result p2

    .line 36
    invoke-virtual {p0}, Lcom/stagnationlab/sk/elements/TransactionLayout;->getMeasuredWidth()I

    move-result p3

    sub-int/2addr p3, p1

    invoke-virtual {p0}, Lcom/stagnationlab/sk/elements/TransactionLayout;->getPaddingRight()I

    move-result p4

    sub-int/2addr p3, p4

    .line 37
    invoke-virtual {p0}, Lcom/stagnationlab/sk/elements/TransactionLayout;->getMeasuredHeight()I

    move-result p4

    sub-int/2addr p4, p2

    invoke-virtual {p0}, Lcom/stagnationlab/sk/elements/TransactionLayout;->getPaddingBottom()I

    move-result p5

    sub-int/2addr p4, p5

    .line 38
    invoke-virtual {p0}, Lcom/stagnationlab/sk/elements/TransactionLayout;->getChildCount()I

    move-result p5

    const/4 v0, 0x0

    move v1, p4

    const/4 p4, 0x0

    :goto_0
    add-int/lit8 v2, p5, -0x1

    if-ge p4, v2, :cond_0

    .line 43
    invoke-virtual {p0, p4}, Lcom/stagnationlab/sk/elements/TransactionLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getMeasuredHeight()I

    move-result v2

    sub-int/2addr v1, v2

    .line 45
    iget-object v3, p0, Lcom/stagnationlab/sk/elements/TransactionLayout;->a:[I

    aput v2, v3, p4

    add-int/lit8 p4, p4, 0x1

    goto :goto_0

    .line 48
    :cond_0
    invoke-virtual {p0}, Lcom/stagnationlab/sk/elements/TransactionLayout;->getResources()Landroid/content/res/Resources;

    move-result-object p4

    const v3, 0x7f06007a

    invoke-virtual {p4, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p4

    .line 49
    invoke-static {p4, v1}, Ljava/lang/Math;->min(II)I

    move-result p4

    .line 51
    iget-object v3, p0, Lcom/stagnationlab/sk/elements/TransactionLayout;->a:[I

    aget v4, v3, v0

    add-int/2addr v1, v4

    sub-int/2addr v1, p4

    aput v1, v3, v0

    .line 52
    aput p4, v3, v2

    move p4, p2

    const/4 p2, 0x0

    :goto_1
    if-ge p2, p5, :cond_1

    .line 56
    invoke-virtual {p0, p2}, Lcom/stagnationlab/sk/elements/TransactionLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    iget-object v0, p0, Lcom/stagnationlab/sk/elements/TransactionLayout;->a:[I

    aget v5, v0, p2

    move-object v0, p0

    move v2, p1

    move v3, p4

    move v4, p3

    invoke-direct/range {v0 .. v5}, Lcom/stagnationlab/sk/elements/TransactionLayout;->a(Landroid/view/View;IIII)V

    .line 57
    iget-object v0, p0, Lcom/stagnationlab/sk/elements/TransactionLayout;->a:[I

    aget v0, v0, p2

    add-int/2addr p4, v0

    add-int/lit8 p2, p2, 0x1

    goto :goto_1

    :cond_1
    return-void
.end method
