.class public Lcom/stagnationlab/sk/b;
.super Landroid/app/Activity;
.source "BaseActivity.java"


# instance fields
.field protected a:Landroid/os/Handler;

.field protected b:Landroid/content/Context;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 21
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method

.method public static b(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Context;
    .locals 3
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "ObsoleteSdkInt"
        }
    .end annotation

    .line 50
    new-instance v0, Ljava/util/Locale;

    invoke-direct {v0, p1}, Ljava/util/Locale;-><init>(Ljava/lang/String;)V

    .line 51
    invoke-static {v0}, Ljava/util/Locale;->setDefault(Ljava/util/Locale;)V

    .line 52
    new-instance p1, Landroid/content/res/Configuration;

    invoke-direct {p1}, Landroid/content/res/Configuration;-><init>()V

    .line 54
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x11

    if-lt v1, v2, :cond_0

    .line 55
    invoke-virtual {p1, v0}, Landroid/content/res/Configuration;->setLocale(Ljava/util/Locale;)V

    .line 56
    invoke-virtual {p1, v0}, Landroid/content/res/Configuration;->setLayoutDirection(Ljava/util/Locale;)V

    .line 57
    invoke-virtual {p0, p1}, Landroid/content/Context;->createConfigurationContext(Landroid/content/res/Configuration;)Landroid/content/Context;

    move-result-object p0

    goto :goto_0

    .line 59
    :cond_0
    iput-object v0, p1, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    .line 60
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 61
    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Landroid/content/res/Resources;->updateConfiguration(Landroid/content/res/Configuration;Landroid/util/DisplayMetrics;)V

    :goto_0
    return-object p0
.end method

.method private g()V
    .locals 4

    .line 96
    invoke-virtual {p0}, Lcom/stagnationlab/sk/b;->getWindow()Landroid/view/Window;

    move-result-object v0

    const v1, 0x680081

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    .line 104
    iget-object v0, p0, Lcom/stagnationlab/sk/b;->a:Landroid/os/Handler;

    if-eqz v0, :cond_0

    const/4 v1, 0x0

    .line 105
    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 108
    :cond_0
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/stagnationlab/sk/b;->a:Landroid/os/Handler;

    .line 109
    iget-object v0, p0, Lcom/stagnationlab/sk/b;->a:Landroid/os/Handler;

    new-instance v1, Lcom/stagnationlab/sk/b$1;

    invoke-direct {v1, p0}, Lcom/stagnationlab/sk/b$1;-><init>(Lcom/stagnationlab/sk/b;)V

    const-wide/16 v2, 0x7530

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method


# virtual methods
.method protected a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Context;
    .locals 0

    .line 43
    invoke-static {p1, p2}, Lcom/stagnationlab/sk/b;->b(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Context;

    move-result-object p1

    iput-object p1, p0, Lcom/stagnationlab/sk/b;->b:Landroid/content/Context;

    .line 45
    iget-object p1, p0, Lcom/stagnationlab/sk/b;->b:Landroid/content/Context;

    return-object p1
.end method

.method protected a()V
    .locals 1

    const/4 v0, 0x1

    .line 39
    invoke-virtual {p0, v0}, Lcom/stagnationlab/sk/b;->requestWindowFeature(I)Z

    return-void
.end method

.method protected a(I)V
    .locals 0

    .line 75
    invoke-virtual {p0}, Lcom/stagnationlab/sk/b;->b()V

    .line 76
    invoke-super {p0, p1}, Landroid/app/Activity;->setContentView(I)V

    return-void
.end method

.method protected attachBaseContext(Landroid/content/Context;)V
    .locals 1

    .line 33
    invoke-static {}, Lcom/stagnationlab/sk/MyApplication;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/stagnationlab/sk/b;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Context;

    move-result-object p1

    .line 35
    invoke-super {p0, p1}, Landroid/app/Activity;->attachBaseContext(Landroid/content/Context;)V

    return-void
.end method

.method protected b()V
    .locals 1

    .line 80
    invoke-virtual {p0}, Lcom/stagnationlab/sk/b;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f0f0007

    goto :goto_0

    :cond_0
    const v0, 0x7f0f0006

    :goto_0
    invoke-virtual {p0, v0}, Lcom/stagnationlab/sk/b;->setTheme(I)V

    return-void
.end method

.method protected c()Z
    .locals 1

    .line 84
    invoke-static {}, Lcom/stagnationlab/sk/a/a;->a()Lcom/stagnationlab/sk/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/stagnationlab/sk/a/a;->f()Z

    move-result v0

    return v0
.end method

.method protected d()V
    .locals 3

    .line 88
    invoke-virtual {p0}, Lcom/stagnationlab/sk/b;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "wakeUpDevice"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 89
    invoke-direct {p0}, Lcom/stagnationlab/sk/b;->g()V

    goto :goto_0

    .line 91
    :cond_0
    invoke-virtual {p0}, Lcom/stagnationlab/sk/b;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/high16 v1, 0x80000

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    :goto_0
    return-void
.end method

.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 2

    .line 124
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    const/16 v1, 0x52

    if-ne v0, v1, :cond_0

    const/4 p1, 0x1

    return p1

    .line 130
    :cond_0
    invoke-super {p0, p1}, Landroid/app/Activity;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result p1

    return p1
.end method

.method protected e()V
    .locals 3

    .line 117
    invoke-virtual {p0}, Lcom/stagnationlab/sk/b;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "wakeUpDevice"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 118
    invoke-direct {p0}, Lcom/stagnationlab/sk/b;->g()V

    :cond_0
    return-void
.end method

.method protected f()Z
    .locals 1

    .line 158
    invoke-static {p0}, Lcom/stagnationlab/sk/fcm/a;->c(Landroid/app/Activity;)Z

    move-result v0

    return v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 0

    .line 28
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    return-void
.end method

.method protected onDestroy()V
    .locals 2

    .line 135
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 137
    iget-object v0, p0, Lcom/stagnationlab/sk/b;->a:Landroid/os/Handler;

    if-eqz v0, :cond_0

    const/4 v1, 0x0

    .line 138
    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method protected onStart()V
    .locals 0

    .line 145
    invoke-super {p0}, Landroid/app/Activity;->onStart()V

    .line 147
    invoke-static {p0}, Lcom/stagnationlab/sk/fcm/a;->a(Landroid/app/Activity;)V

    return-void
.end method

.method protected onStop()V
    .locals 0

    .line 152
    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    .line 154
    invoke-static {p0}, Lcom/stagnationlab/sk/fcm/a;->b(Landroid/app/Activity;)V

    return-void
.end method

.method public setContentView(I)V
    .locals 0

    .line 69
    invoke-virtual {p0}, Lcom/stagnationlab/sk/b;->a()V

    .line 71
    invoke-virtual {p0, p1}, Lcom/stagnationlab/sk/b;->a(I)V

    return-void
.end method
