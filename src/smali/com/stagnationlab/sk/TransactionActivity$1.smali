.class Lcom/stagnationlab/sk/TransactionActivity$1;
.super Ljava/lang/Object;
.source "TransactionActivity.java"

# interfaces
.implements Lcom/stagnationlab/sk/a/a/a;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/stagnationlab/sk/TransactionActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Landroid/os/Bundle;

.field final synthetic b:Lcom/stagnationlab/sk/TransactionActivity;


# direct methods
.method constructor <init>(Lcom/stagnationlab/sk/TransactionActivity;Landroid/os/Bundle;)V
    .locals 0

    .line 162
    iput-object p1, p0, Lcom/stagnationlab/sk/TransactionActivity$1;->b:Lcom/stagnationlab/sk/TransactionActivity;

    iput-object p2, p0, Lcom/stagnationlab/sk/TransactionActivity$1;->a:Landroid/os/Bundle;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/stagnationlab/sk/a/a;Z)V
    .locals 0

    .line 165
    iget-object p2, p0, Lcom/stagnationlab/sk/TransactionActivity$1;->b:Lcom/stagnationlab/sk/TransactionActivity;

    invoke-static {p2}, Lcom/stagnationlab/sk/TransactionActivity;->a(Lcom/stagnationlab/sk/TransactionActivity;)Z

    move-result p2

    if-nez p2, :cond_0

    return-void

    .line 169
    :cond_0
    iget-object p2, p0, Lcom/stagnationlab/sk/TransactionActivity$1;->b:Lcom/stagnationlab/sk/TransactionActivity;

    invoke-static {p2, p1}, Lcom/stagnationlab/sk/TransactionActivity;->a(Lcom/stagnationlab/sk/TransactionActivity;Lcom/stagnationlab/sk/a/a;)Lcom/stagnationlab/sk/a/a;

    .line 171
    iget-object p2, p0, Lcom/stagnationlab/sk/TransactionActivity$1;->b:Lcom/stagnationlab/sk/TransactionActivity;

    invoke-static {p2}, Lcom/stagnationlab/sk/TransactionActivity;->b(Lcom/stagnationlab/sk/TransactionActivity;)Z

    move-result p2

    if-eqz p2, :cond_1

    iget-object p2, p0, Lcom/stagnationlab/sk/TransactionActivity$1;->b:Lcom/stagnationlab/sk/TransactionActivity;

    invoke-static {p2}, Lcom/stagnationlab/sk/TransactionActivity;->c(Lcom/stagnationlab/sk/TransactionActivity;)Z

    move-result p2

    if-nez p2, :cond_1

    .line 172
    iget-object p2, p0, Lcom/stagnationlab/sk/TransactionActivity$1;->b:Lcom/stagnationlab/sk/TransactionActivity;

    invoke-virtual {p1, p2}, Lcom/stagnationlab/sk/a/a;->a(Landroid/app/Activity;)V

    .line 173
    iget-object p1, p0, Lcom/stagnationlab/sk/TransactionActivity$1;->b:Lcom/stagnationlab/sk/TransactionActivity;

    const/4 p2, 0x1

    invoke-static {p1, p2}, Lcom/stagnationlab/sk/TransactionActivity;->a(Lcom/stagnationlab/sk/TransactionActivity;Z)Z

    .line 176
    :cond_1
    iget-object p1, p0, Lcom/stagnationlab/sk/TransactionActivity$1;->b:Lcom/stagnationlab/sk/TransactionActivity;

    const/4 p2, 0x0

    invoke-static {p1, p2}, Lcom/stagnationlab/sk/TransactionActivity;->b(Lcom/stagnationlab/sk/TransactionActivity;Z)Z

    .line 178
    iget-object p1, p0, Lcom/stagnationlab/sk/TransactionActivity$1;->b:Lcom/stagnationlab/sk/TransactionActivity;

    iget-object p2, p0, Lcom/stagnationlab/sk/TransactionActivity$1;->a:Landroid/os/Bundle;

    invoke-static {p1, p2}, Lcom/stagnationlab/sk/TransactionActivity;->a(Lcom/stagnationlab/sk/TransactionActivity;Landroid/os/Bundle;)V

    .line 180
    iget-object p1, p0, Lcom/stagnationlab/sk/TransactionActivity$1;->b:Lcom/stagnationlab/sk/TransactionActivity;

    invoke-virtual {p1}, Lcom/stagnationlab/sk/TransactionActivity;->d()V

    return-void
.end method

.method public a(Lcom/stagnationlab/sk/c/a;)V
    .locals 2

    .line 185
    iget-object p1, p0, Lcom/stagnationlab/sk/TransactionActivity$1;->b:Lcom/stagnationlab/sk/TransactionActivity;

    invoke-static {p1}, Lcom/stagnationlab/sk/TransactionActivity;->a(Lcom/stagnationlab/sk/TransactionActivity;)Z

    move-result p1

    if-nez p1, :cond_0

    return-void

    .line 189
    :cond_0
    new-instance p1, Landroid/content/Intent;

    iget-object v0, p0, Lcom/stagnationlab/sk/TransactionActivity$1;->b:Lcom/stagnationlab/sk/TransactionActivity;

    const-class v1, Lcom/stagnationlab/sk/MainActivity;

    invoke-direct {p1, v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 191
    iget-object v0, p0, Lcom/stagnationlab/sk/TransactionActivity$1;->b:Lcom/stagnationlab/sk/TransactionActivity;

    invoke-virtual {v0, p1}, Lcom/stagnationlab/sk/TransactionActivity;->startActivity(Landroid/content/Intent;)V

    .line 192
    iget-object p1, p0, Lcom/stagnationlab/sk/TransactionActivity$1;->b:Lcom/stagnationlab/sk/TransactionActivity;

    invoke-virtual {p1}, Lcom/stagnationlab/sk/TransactionActivity;->finish()V

    return-void
.end method

.method public b(Lcom/stagnationlab/sk/c/a;)V
    .locals 1

    .line 197
    iget-object v0, p0, Lcom/stagnationlab/sk/TransactionActivity$1;->b:Lcom/stagnationlab/sk/TransactionActivity;

    invoke-static {v0}, Lcom/stagnationlab/sk/TransactionActivity;->a(Lcom/stagnationlab/sk/TransactionActivity;)Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 201
    :cond_0
    iget-object v0, p0, Lcom/stagnationlab/sk/TransactionActivity$1;->b:Lcom/stagnationlab/sk/TransactionActivity;

    invoke-static {v0, p1}, Lcom/stagnationlab/sk/TransactionActivity;->a(Lcom/stagnationlab/sk/TransactionActivity;Lcom/stagnationlab/sk/c/a;)Z

    .line 202
    iget-object p1, p0, Lcom/stagnationlab/sk/TransactionActivity$1;->b:Lcom/stagnationlab/sk/TransactionActivity;

    invoke-virtual {p1}, Lcom/stagnationlab/sk/TransactionActivity;->d()V

    return-void
.end method
