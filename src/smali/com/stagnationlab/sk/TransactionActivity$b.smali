.class final enum Lcom/stagnationlab/sk/TransactionActivity$b;
.super Ljava/lang/Enum;
.source "TransactionActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/stagnationlab/sk/TransactionActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/stagnationlab/sk/TransactionActivity$b;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/stagnationlab/sk/TransactionActivity$b;

.field public static final enum b:Lcom/stagnationlab/sk/TransactionActivity$b;

.field public static final enum c:Lcom/stagnationlab/sk/TransactionActivity$b;

.field public static final enum d:Lcom/stagnationlab/sk/TransactionActivity$b;

.field private static final synthetic e:[Lcom/stagnationlab/sk/TransactionActivity$b;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .line 78
    new-instance v0, Lcom/stagnationlab/sk/TransactionActivity$b;

    const/4 v1, 0x0

    const-string v2, "LOADER"

    invoke-direct {v0, v2, v1}, Lcom/stagnationlab/sk/TransactionActivity$b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/stagnationlab/sk/TransactionActivity$b;->a:Lcom/stagnationlab/sk/TransactionActivity$b;

    new-instance v0, Lcom/stagnationlab/sk/TransactionActivity$b;

    const/4 v2, 0x1

    const-string v3, "TRANSACTION"

    invoke-direct {v0, v3, v2}, Lcom/stagnationlab/sk/TransactionActivity$b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/stagnationlab/sk/TransactionActivity$b;->b:Lcom/stagnationlab/sk/TransactionActivity$b;

    new-instance v0, Lcom/stagnationlab/sk/TransactionActivity$b;

    const/4 v3, 0x2

    const-string v4, "RP_REQUEST"

    invoke-direct {v0, v4, v3}, Lcom/stagnationlab/sk/TransactionActivity$b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/stagnationlab/sk/TransactionActivity$b;->c:Lcom/stagnationlab/sk/TransactionActivity$b;

    new-instance v0, Lcom/stagnationlab/sk/TransactionActivity$b;

    const/4 v4, 0x3

    const-string v5, "SUCCESS"

    invoke-direct {v0, v5, v4}, Lcom/stagnationlab/sk/TransactionActivity$b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/stagnationlab/sk/TransactionActivity$b;->d:Lcom/stagnationlab/sk/TransactionActivity$b;

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/stagnationlab/sk/TransactionActivity$b;

    sget-object v5, Lcom/stagnationlab/sk/TransactionActivity$b;->a:Lcom/stagnationlab/sk/TransactionActivity$b;

    aput-object v5, v0, v1

    sget-object v1, Lcom/stagnationlab/sk/TransactionActivity$b;->b:Lcom/stagnationlab/sk/TransactionActivity$b;

    aput-object v1, v0, v2

    sget-object v1, Lcom/stagnationlab/sk/TransactionActivity$b;->c:Lcom/stagnationlab/sk/TransactionActivity$b;

    aput-object v1, v0, v3

    sget-object v1, Lcom/stagnationlab/sk/TransactionActivity$b;->d:Lcom/stagnationlab/sk/TransactionActivity$b;

    aput-object v1, v0, v4

    sput-object v0, Lcom/stagnationlab/sk/TransactionActivity$b;->e:[Lcom/stagnationlab/sk/TransactionActivity$b;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 78
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/stagnationlab/sk/TransactionActivity$b;
    .locals 1

    .line 78
    const-class v0, Lcom/stagnationlab/sk/TransactionActivity$b;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/stagnationlab/sk/TransactionActivity$b;

    return-object p0
.end method

.method public static values()[Lcom/stagnationlab/sk/TransactionActivity$b;
    .locals 1

    .line 78
    sget-object v0, Lcom/stagnationlab/sk/TransactionActivity$b;->e:[Lcom/stagnationlab/sk/TransactionActivity$b;

    invoke-virtual {v0}, [Lcom/stagnationlab/sk/TransactionActivity$b;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/stagnationlab/sk/TransactionActivity$b;

    return-object v0
.end method
