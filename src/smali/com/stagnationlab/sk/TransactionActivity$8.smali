.class Lcom/stagnationlab/sk/TransactionActivity$8;
.super Ljava/lang/Object;
.source "TransactionActivity.java"

# interfaces
.implements Lcom/stagnationlab/sk/a/a/d;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/stagnationlab/sk/TransactionActivity;->o()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/stagnationlab/sk/TransactionActivity;


# direct methods
.method constructor <init>(Lcom/stagnationlab/sk/TransactionActivity;)V
    .locals 0

    .line 868
    iput-object p1, p0, Lcom/stagnationlab/sk/TransactionActivity$8;->a:Lcom/stagnationlab/sk/TransactionActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    const-string v0, "TransactionActivity"

    const-string v1, "Transaction cancelled"

    .line 871
    invoke-static {v0, v1}, Lcom/stagnationlab/sk/f;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 872
    iget-object v0, p0, Lcom/stagnationlab/sk/TransactionActivity$8;->a:Lcom/stagnationlab/sk/TransactionActivity;

    sget-object v1, Lcom/stagnationlab/sk/TransactionActivity$a;->a:Lcom/stagnationlab/sk/TransactionActivity$a;

    invoke-static {v0, v1}, Lcom/stagnationlab/sk/TransactionActivity;->a(Lcom/stagnationlab/sk/TransactionActivity;Lcom/stagnationlab/sk/TransactionActivity$a;)V

    return-void
.end method

.method public a(Lcom/stagnationlab/sk/c/a;)V
    .locals 2

    .line 877
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Failed to cancel transaction"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string v0, "TransactionActivity"

    invoke-static {v0, p1}, Lcom/stagnationlab/sk/f;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 878
    iget-object p1, p0, Lcom/stagnationlab/sk/TransactionActivity$8;->a:Lcom/stagnationlab/sk/TransactionActivity;

    invoke-static {p1}, Lcom/stagnationlab/sk/TransactionActivity;->e(Lcom/stagnationlab/sk/TransactionActivity;)V

    return-void
.end method
