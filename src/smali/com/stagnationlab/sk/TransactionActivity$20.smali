.class Lcom/stagnationlab/sk/TransactionActivity$20;
.super Landroid/os/CountDownTimer;
.source "TransactionActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/stagnationlab/sk/TransactionActivity;->z()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/stagnationlab/sk/TransactionActivity;


# direct methods
.method constructor <init>(Lcom/stagnationlab/sk/TransactionActivity;JJ)V
    .locals 0

    .line 1428
    iput-object p1, p0, Lcom/stagnationlab/sk/TransactionActivity$20;->a:Lcom/stagnationlab/sk/TransactionActivity;

    invoke-direct {p0, p2, p3, p4, p5}, Landroid/os/CountDownTimer;-><init>(JJ)V

    return-void
.end method


# virtual methods
.method public onFinish()V
    .locals 2

    .line 1441
    iget-object v0, p0, Lcom/stagnationlab/sk/TransactionActivity$20;->a:Lcom/stagnationlab/sk/TransactionActivity;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/stagnationlab/sk/TransactionActivity;->h(Lcom/stagnationlab/sk/TransactionActivity;Z)Z

    .line 1442
    iget-object v0, p0, Lcom/stagnationlab/sk/TransactionActivity$20;->a:Lcom/stagnationlab/sk/TransactionActivity;

    invoke-static {v0}, Lcom/stagnationlab/sk/TransactionActivity;->f(Lcom/stagnationlab/sk/TransactionActivity;)Lcom/stagnationlab/sk/elements/Modal;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/stagnationlab/sk/elements/Modal;->setVisible(Z)V

    .line 1443
    iget-object v0, p0, Lcom/stagnationlab/sk/TransactionActivity$20;->a:Lcom/stagnationlab/sk/TransactionActivity;

    sget-object v1, Lcom/stagnationlab/sk/TransactionActivity$a;->c:Lcom/stagnationlab/sk/TransactionActivity$a;

    invoke-static {v0, v1}, Lcom/stagnationlab/sk/TransactionActivity;->a(Lcom/stagnationlab/sk/TransactionActivity;Lcom/stagnationlab/sk/TransactionActivity$a;)V

    return-void
.end method

.method public onTick(J)V
    .locals 3

    .line 1431
    iget-object v0, p0, Lcom/stagnationlab/sk/TransactionActivity$20;->a:Lcom/stagnationlab/sk/TransactionActivity;

    invoke-static {v0}, Lcom/stagnationlab/sk/TransactionActivity;->v(Lcom/stagnationlab/sk/TransactionActivity;)Landroid/widget/TextView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {p1, p2, v1}, Lcom/stagnationlab/sk/util/l;->a(JZ)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1433
    iget-object v0, p0, Lcom/stagnationlab/sk/TransactionActivity$20;->a:Lcom/stagnationlab/sk/TransactionActivity;

    invoke-static {v0}, Lcom/stagnationlab/sk/TransactionActivity;->w(Lcom/stagnationlab/sk/TransactionActivity;)Z

    move-result v0

    if-nez v0, :cond_0

    const-wide/16 v0, 0x7918

    cmp-long v2, p1, v0

    if-gez v2, :cond_0

    .line 1434
    iget-object p1, p0, Lcom/stagnationlab/sk/TransactionActivity$20;->a:Lcom/stagnationlab/sk/TransactionActivity;

    const/4 p2, 0x1

    invoke-static {p1, p2}, Lcom/stagnationlab/sk/TransactionActivity;->g(Lcom/stagnationlab/sk/TransactionActivity;Z)Z

    .line 1435
    iget-object p1, p0, Lcom/stagnationlab/sk/TransactionActivity$20;->a:Lcom/stagnationlab/sk/TransactionActivity;

    invoke-static {p1}, Lcom/stagnationlab/sk/TransactionActivity;->f(Lcom/stagnationlab/sk/TransactionActivity;)Lcom/stagnationlab/sk/elements/Modal;

    move-result-object p1

    invoke-virtual {p1, p2}, Lcom/stagnationlab/sk/elements/Modal;->setVisible(Z)V

    :cond_0
    return-void
.end method
