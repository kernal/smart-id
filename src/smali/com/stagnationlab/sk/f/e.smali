.class public Lcom/stagnationlab/sk/f/e;
.super Ljava/lang/Object;
.source "Registration.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/stagnationlab/sk/f/e$a;
    }
.end annotation


# static fields
.field private static a:Lcom/stagnationlab/sk/a/a;


# instance fields
.field private b:Lcom/stagnationlab/sk/f/e$a;

.field private c:Z

.field private d:Z

.field private e:Z

.field private f:Lcom/stagnationlab/sk/c/a;

.field private g:Landroid/os/Handler;

.field private h:Ljava/lang/String;

.field private i:Ljava/lang/String;

.field private j:Ljava/lang/String;

.field private k:Ljava/lang/String;

.field private l:Ljava/lang/String;

.field private m:Ljava/lang/String;

.field private n:Lorg/b/a/b;

.field private o:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/stagnationlab/sk/models/LegalGuardian;",
            ">;"
        }
    .end annotation
.end field

.field private p:Lcom/stagnationlab/sk/models/Account;

.field private q:Ljava/lang/String;

.field private r:Ljava/lang/String;

.field private s:Lcom/stagnationlab/sk/models/Transaction;

.field private t:Lcom/stagnationlab/sk/models/Transaction;

.field private u:Lcom/stagnationlab/sk/f/c;

.field private v:Lcom/stagnationlab/sk/a/a/l;

.field private w:Lcom/stagnationlab/sk/a/a/r;

.field private x:Lcom/stagnationlab/sk/f/a;

.field private y:Lcom/stagnationlab/sk/f/f;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 31
    invoke-static {}, Lcom/stagnationlab/sk/a/a;->a()Lcom/stagnationlab/sk/a/a;

    move-result-object v0

    sput-object v0, Lcom/stagnationlab/sk/f/e;->a:Lcom/stagnationlab/sk/a/a;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 34
    iput-boolean v0, p0, Lcom/stagnationlab/sk/f/e;->c:Z

    .line 35
    iput-boolean v0, p0, Lcom/stagnationlab/sk/f/e;->d:Z

    .line 36
    iput-boolean v0, p0, Lcom/stagnationlab/sk/f/e;->e:Z

    .line 39
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/stagnationlab/sk/f/e;->g:Landroid/os/Handler;

    .line 64
    sget-object v0, Lcom/stagnationlab/sk/f/e$a;->a:Lcom/stagnationlab/sk/f/e$a;

    iput-object v0, p0, Lcom/stagnationlab/sk/f/e;->b:Lcom/stagnationlab/sk/f/e$a;

    return-void
.end method

.method static synthetic a(Lcom/stagnationlab/sk/f/e;Lcom/stagnationlab/sk/models/Account;)Lcom/stagnationlab/sk/models/Account;
    .locals 0

    .line 25
    iput-object p1, p0, Lcom/stagnationlab/sk/f/e;->p:Lcom/stagnationlab/sk/models/Account;

    return-object p1
.end method

.method static synthetic a(Lcom/stagnationlab/sk/f/e;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .line 25
    iput-object p1, p0, Lcom/stagnationlab/sk/f/e;->j:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic a(Lcom/stagnationlab/sk/f/e;)Ljava/util/List;
    .locals 0

    .line 25
    iget-object p0, p0, Lcom/stagnationlab/sk/f/e;->o:Ljava/util/List;

    return-object p0
.end method

.method static synthetic a(Lcom/stagnationlab/sk/f/e;Ljava/util/List;)Ljava/util/List;
    .locals 0

    .line 25
    iput-object p1, p0, Lcom/stagnationlab/sk/f/e;->o:Ljava/util/List;

    return-object p1
.end method

.method static synthetic a(Lcom/stagnationlab/sk/f/e;Lorg/b/a/b;)Lorg/b/a/b;
    .locals 0

    .line 25
    iput-object p1, p0, Lcom/stagnationlab/sk/f/e;->n:Lorg/b/a/b;

    return-object p1
.end method

.method private a(Lcom/stagnationlab/sk/c/a;)V
    .locals 1

    .line 288
    iget-object v0, p0, Lcom/stagnationlab/sk/f/e;->v:Lcom/stagnationlab/sk/a/a/l;

    if-nez v0, :cond_0

    return-void

    :cond_0
    if-nez p1, :cond_1

    .line 293
    sget-object p1, Lcom/stagnationlab/sk/f/e$a;->c:Lcom/stagnationlab/sk/f/e$a;

    iput-object p1, p0, Lcom/stagnationlab/sk/f/e;->b:Lcom/stagnationlab/sk/f/e$a;

    const/4 p1, 0x1

    .line 294
    invoke-static {p1}, Lcom/stagnationlab/sk/a/d;->b(Z)V

    .line 295
    iget-object p1, p0, Lcom/stagnationlab/sk/f/e;->v:Lcom/stagnationlab/sk/a/a/l;

    invoke-interface {p1}, Lcom/stagnationlab/sk/a/a/l;->a()V

    goto :goto_0

    .line 297
    :cond_1
    invoke-interface {v0, p1}, Lcom/stagnationlab/sk/a/a/l;->a(Lcom/stagnationlab/sk/c/a;)V

    :goto_0
    const/4 p1, 0x0

    .line 300
    iput-object p1, p0, Lcom/stagnationlab/sk/f/e;->v:Lcom/stagnationlab/sk/a/a/l;

    return-void
.end method

.method private a(Lcom/stagnationlab/sk/c/a;Ljava/lang/String;Ljava/lang/String;Lorg/b/a/b;)V
    .locals 1

    .line 306
    iget-object v0, p0, Lcom/stagnationlab/sk/f/e;->w:Lcom/stagnationlab/sk/a/a/r;

    if-nez v0, :cond_0

    return-void

    :cond_0
    if-nez p1, :cond_1

    .line 311
    iput-object p3, p0, Lcom/stagnationlab/sk/f/e;->m:Ljava/lang/String;

    .line 312
    iput-object p4, p0, Lcom/stagnationlab/sk/f/e;->n:Lorg/b/a/b;

    .line 313
    invoke-interface {v0, p2, p3, p4}, Lcom/stagnationlab/sk/a/a/r;->a(Ljava/lang/String;Ljava/lang/String;Lorg/b/a/b;)V

    goto :goto_0

    .line 315
    :cond_1
    invoke-interface {v0, p1}, Lcom/stagnationlab/sk/a/a/r;->a(Lcom/stagnationlab/sk/c/a;)V

    :goto_0
    const/4 p1, 0x0

    .line 318
    iput-object p1, p0, Lcom/stagnationlab/sk/f/e;->w:Lcom/stagnationlab/sk/a/a/r;

    return-void
.end method

.method private a(Lcom/stagnationlab/sk/f/a;)V
    .locals 4

    .line 456
    new-instance v0, Lorg/b/a/b;

    invoke-direct {v0}, Lorg/b/a/b;-><init>()V

    const/16 v1, 0x2710

    invoke-virtual {v0, v1}, Lorg/b/a/b;->b(I)Lorg/b/a/b;

    move-result-object v0

    .line 458
    iget-object v1, p0, Lcom/stagnationlab/sk/f/e;->n:Lorg/b/a/b;

    invoke-virtual {v1, v0}, Lorg/b/a/b;->a(Lorg/b/a/t;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string p1, "Registration"

    const-string v0, "Registration token will timeout, stopping create account polling"

    .line 459
    invoke-static {p1, v0}, Lcom/stagnationlab/sk/f;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 461
    new-instance p1, Lcom/stagnationlab/sk/c/a;

    sget-object v0, Lcom/stagnationlab/sk/c/b;->k:Lcom/stagnationlab/sk/c/b;

    invoke-direct {p1, v0}, Lcom/stagnationlab/sk/c/a;-><init>(Lcom/stagnationlab/sk/c/b;)V

    invoke-direct {p0, p1}, Lcom/stagnationlab/sk/f/e;->c(Lcom/stagnationlab/sk/c/a;)V

    return-void

    .line 466
    :cond_0
    iget-object v0, p0, Lcom/stagnationlab/sk/f/e;->g:Landroid/os/Handler;

    new-instance v1, Lcom/stagnationlab/sk/f/e$7;

    invoke-direct {v1, p0, p1}, Lcom/stagnationlab/sk/f/e$7;-><init>(Lcom/stagnationlab/sk/f/e;Lcom/stagnationlab/sk/f/a;)V

    const-wide/16 v2, 0x2710

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method static synthetic a(Lcom/stagnationlab/sk/f/e;Lcom/stagnationlab/sk/c/a;)V
    .locals 0

    .line 25
    invoke-direct {p0, p1}, Lcom/stagnationlab/sk/f/e;->a(Lcom/stagnationlab/sk/c/a;)V

    return-void
.end method

.method static synthetic a(Lcom/stagnationlab/sk/f/e;Lcom/stagnationlab/sk/c/a;Ljava/lang/String;Ljava/lang/String;Lorg/b/a/b;)V
    .locals 0

    .line 25
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/stagnationlab/sk/f/e;->a(Lcom/stagnationlab/sk/c/a;Ljava/lang/String;Ljava/lang/String;Lorg/b/a/b;)V

    return-void
.end method

.method static synthetic a(Lcom/stagnationlab/sk/f/e;Lcom/stagnationlab/sk/f/a;)V
    .locals 0

    .line 25
    invoke-direct {p0, p1}, Lcom/stagnationlab/sk/f/e;->a(Lcom/stagnationlab/sk/f/a;)V

    return-void
.end method

.method static synthetic a(Lcom/stagnationlab/sk/f/e;Lcom/stagnationlab/sk/models/Transaction;)V
    .locals 0

    .line 25
    invoke-direct {p0, p1}, Lcom/stagnationlab/sk/f/e;->a(Lcom/stagnationlab/sk/models/Transaction;)V

    return-void
.end method

.method private a(Lcom/stagnationlab/sk/models/Transaction;)V
    .locals 1

    .line 265
    invoke-virtual {p1}, Lcom/stagnationlab/sk/models/Transaction;->l()Z

    move-result p1

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    .line 266
    sget-object p1, Lcom/stagnationlab/sk/f/e$a;->e:Lcom/stagnationlab/sk/f/e$a;

    iput-object p1, p0, Lcom/stagnationlab/sk/f/e;->b:Lcom/stagnationlab/sk/f/e$a;

    .line 268
    iput-object v0, p0, Lcom/stagnationlab/sk/f/e;->s:Lcom/stagnationlab/sk/models/Transaction;

    .line 269
    invoke-direct {p0}, Lcom/stagnationlab/sk/f/e;->f()V

    goto :goto_0

    .line 271
    :cond_0
    sget-object p1, Lcom/stagnationlab/sk/f/e$a;->f:Lcom/stagnationlab/sk/f/e$a;

    iput-object p1, p0, Lcom/stagnationlab/sk/f/e;->b:Lcom/stagnationlab/sk/f/e$a;

    .line 272
    iput-object v0, p0, Lcom/stagnationlab/sk/f/e;->t:Lcom/stagnationlab/sk/models/Transaction;

    .line 273
    invoke-direct {p0, v0}, Lcom/stagnationlab/sk/f/e;->d(Lcom/stagnationlab/sk/c/a;)V

    :goto_0
    return-void
.end method

.method static synthetic b(Lcom/stagnationlab/sk/f/e;Lcom/stagnationlab/sk/f/a;)Lcom/stagnationlab/sk/f/a;
    .locals 0

    .line 25
    iput-object p1, p0, Lcom/stagnationlab/sk/f/e;->x:Lcom/stagnationlab/sk/f/a;

    return-object p1
.end method

.method static synthetic b(Lcom/stagnationlab/sk/f/e;Lcom/stagnationlab/sk/models/Transaction;)Lcom/stagnationlab/sk/models/Transaction;
    .locals 0

    .line 25
    iput-object p1, p0, Lcom/stagnationlab/sk/f/e;->t:Lcom/stagnationlab/sk/models/Transaction;

    return-object p1
.end method

.method static synthetic b(Lcom/stagnationlab/sk/f/e;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .line 25
    iput-object p1, p0, Lcom/stagnationlab/sk/f/e;->k:Ljava/lang/String;

    return-object p1
.end method

.method public static b()V
    .locals 1

    const/4 v0, 0x0

    .line 440
    invoke-static {v0}, Lcom/stagnationlab/sk/a/d;->b(Z)V

    return-void
.end method

.method private b(Lcom/stagnationlab/sk/c/a;)V
    .locals 2

    .line 348
    iget-object v0, p0, Lcom/stagnationlab/sk/f/e;->b:Lcom/stagnationlab/sk/f/e$a;

    sget-object v1, Lcom/stagnationlab/sk/f/e$a;->e:Lcom/stagnationlab/sk/f/e$a;

    if-ne v0, v1, :cond_0

    .line 349
    invoke-direct {p0, p1}, Lcom/stagnationlab/sk/f/e;->d(Lcom/stagnationlab/sk/c/a;)V

    goto :goto_0

    .line 351
    :cond_0
    invoke-direct {p0, p1}, Lcom/stagnationlab/sk/f/e;->c(Lcom/stagnationlab/sk/c/a;)V

    :goto_0
    return-void
.end method

.method static synthetic b(Lcom/stagnationlab/sk/f/e;)V
    .locals 0

    .line 25
    invoke-direct {p0}, Lcom/stagnationlab/sk/f/e;->f()V

    return-void
.end method

.method static synthetic b(Lcom/stagnationlab/sk/f/e;Lcom/stagnationlab/sk/c/a;)V
    .locals 0

    .line 25
    invoke-direct {p0, p1}, Lcom/stagnationlab/sk/f/e;->c(Lcom/stagnationlab/sk/c/a;)V

    return-void
.end method

.method static synthetic c(Lcom/stagnationlab/sk/f/e;Lcom/stagnationlab/sk/models/Transaction;)Lcom/stagnationlab/sk/models/Transaction;
    .locals 0

    .line 25
    iput-object p1, p0, Lcom/stagnationlab/sk/f/e;->s:Lcom/stagnationlab/sk/models/Transaction;

    return-object p1
.end method

.method static synthetic c(Lcom/stagnationlab/sk/f/e;)Ljava/lang/String;
    .locals 0

    .line 25
    iget-object p0, p0, Lcom/stagnationlab/sk/f/e;->m:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic c(Lcom/stagnationlab/sk/f/e;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .line 25
    iput-object p1, p0, Lcom/stagnationlab/sk/f/e;->h:Ljava/lang/String;

    return-object p1
.end method

.method private c(Lcom/stagnationlab/sk/c/a;)V
    .locals 0

    .line 362
    iput-object p1, p0, Lcom/stagnationlab/sk/f/e;->f:Lcom/stagnationlab/sk/c/a;

    const/4 p1, 0x1

    .line 363
    iput-boolean p1, p0, Lcom/stagnationlab/sk/f/e;->e:Z

    .line 364
    invoke-direct {p0}, Lcom/stagnationlab/sk/f/e;->g()V

    return-void
.end method

.method static synthetic c(Lcom/stagnationlab/sk/f/e;Lcom/stagnationlab/sk/c/a;)V
    .locals 0

    .line 25
    invoke-direct {p0, p1}, Lcom/stagnationlab/sk/f/e;->d(Lcom/stagnationlab/sk/c/a;)V

    return-void
.end method

.method static synthetic d()Lcom/stagnationlab/sk/a/a;
    .locals 1

    .line 25
    sget-object v0, Lcom/stagnationlab/sk/f/e;->a:Lcom/stagnationlab/sk/a/a;

    return-object v0
.end method

.method static synthetic d(Lcom/stagnationlab/sk/f/e;)Lcom/stagnationlab/sk/f/e$a;
    .locals 0

    .line 25
    iget-object p0, p0, Lcom/stagnationlab/sk/f/e;->b:Lcom/stagnationlab/sk/f/e$a;

    return-object p0
.end method

.method static synthetic d(Lcom/stagnationlab/sk/f/e;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .line 25
    iput-object p1, p0, Lcom/stagnationlab/sk/f/e;->i:Ljava/lang/String;

    return-object p1
.end method

.method private d(Lcom/stagnationlab/sk/c/a;)V
    .locals 4

    .line 391
    iget-object v0, p0, Lcom/stagnationlab/sk/f/e;->y:Lcom/stagnationlab/sk/f/f;

    if-nez v0, :cond_0

    return-void

    :cond_0
    const/4 v1, 0x0

    .line 396
    iput-object v1, p0, Lcom/stagnationlab/sk/f/e;->y:Lcom/stagnationlab/sk/f/f;

    if-eqz p1, :cond_1

    .line 399
    invoke-interface {v0, p1}, Lcom/stagnationlab/sk/f/f;->a(Lcom/stagnationlab/sk/c/a;)V

    return-void

    .line 404
    :cond_1
    sget-object p1, Lcom/stagnationlab/sk/models/AccountKey$Type;->AUTHENTICATION:Lcom/stagnationlab/sk/models/AccountKey$Type;

    invoke-virtual {p1}, Lcom/stagnationlab/sk/models/AccountKey$Type;->toString()Ljava/lang/String;

    move-result-object p1

    .line 407
    iget-object v2, p0, Lcom/stagnationlab/sk/f/e;->b:Lcom/stagnationlab/sk/f/e$a;

    sget-object v3, Lcom/stagnationlab/sk/f/e$a;->f:Lcom/stagnationlab/sk/f/e$a;

    if-ne v2, v3, :cond_2

    .line 408
    sget-object p1, Lcom/stagnationlab/sk/models/AccountKey$Type;->SIGNATURE:Lcom/stagnationlab/sk/models/AccountKey$Type;

    invoke-virtual {p1}, Lcom/stagnationlab/sk/models/AccountKey$Type;->toString()Ljava/lang/String;

    move-result-object p1

    .line 409
    iput-object v1, p0, Lcom/stagnationlab/sk/f/e;->p:Lcom/stagnationlab/sk/models/Account;

    goto :goto_0

    .line 411
    :cond_2
    iget-object v1, p0, Lcom/stagnationlab/sk/f/e;->t:Lcom/stagnationlab/sk/models/Transaction;

    .line 414
    :goto_0
    invoke-interface {v0, p1, v1}, Lcom/stagnationlab/sk/f/f;->a(Ljava/lang/String;Lcom/stagnationlab/sk/models/Transaction;)V

    return-void
.end method

.method static synthetic d(Lcom/stagnationlab/sk/f/e;Lcom/stagnationlab/sk/c/a;)V
    .locals 0

    .line 25
    invoke-direct {p0, p1}, Lcom/stagnationlab/sk/f/e;->b(Lcom/stagnationlab/sk/c/a;)V

    return-void
.end method

.method static synthetic e(Lcom/stagnationlab/sk/f/e;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .line 25
    iput-object p1, p0, Lcom/stagnationlab/sk/f/e;->l:Ljava/lang/String;

    return-object p1
.end method

.method private e()V
    .locals 3

    .line 278
    iget-object v0, p0, Lcom/stagnationlab/sk/f/e;->u:Lcom/stagnationlab/sk/f/c;

    if-nez v0, :cond_0

    return-void

    .line 282
    :cond_0
    sget-object v0, Lcom/stagnationlab/sk/f/e$a;->b:Lcom/stagnationlab/sk/f/e$a;

    iput-object v0, p0, Lcom/stagnationlab/sk/f/e;->b:Lcom/stagnationlab/sk/f/e$a;

    .line 283
    iget-object v0, p0, Lcom/stagnationlab/sk/f/e;->u:Lcom/stagnationlab/sk/f/c;

    iget-object v1, p0, Lcom/stagnationlab/sk/f/e;->h:Ljava/lang/String;

    iget-object v2, p0, Lcom/stagnationlab/sk/f/e;->i:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Lcom/stagnationlab/sk/f/c;->a(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 284
    iput-object v0, p0, Lcom/stagnationlab/sk/f/e;->u:Lcom/stagnationlab/sk/f/c;

    return-void
.end method

.method static synthetic e(Lcom/stagnationlab/sk/f/e;)V
    .locals 0

    .line 25
    invoke-direct {p0}, Lcom/stagnationlab/sk/f/e;->e()V

    return-void
.end method

.method static synthetic f(Lcom/stagnationlab/sk/f/e;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .line 25
    iput-object p1, p0, Lcom/stagnationlab/sk/f/e;->m:Ljava/lang/String;

    return-object p1
.end method

.method private f()V
    .locals 4

    .line 322
    iget-object v0, p0, Lcom/stagnationlab/sk/f/e;->q:Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 324
    :goto_0
    sget-object v1, Lcom/stagnationlab/sk/f/e;->a:Lcom/stagnationlab/sk/a/a;

    if-eqz v0, :cond_1

    iget-object v2, p0, Lcom/stagnationlab/sk/f/e;->r:Ljava/lang/String;

    goto :goto_1

    :cond_1
    iget-object v2, p0, Lcom/stagnationlab/sk/f/e;->q:Ljava/lang/String;

    :goto_1
    new-instance v3, Lcom/stagnationlab/sk/f/e$5;

    invoke-direct {v3, p0, v0}, Lcom/stagnationlab/sk/f/e$5;-><init>(Lcom/stagnationlab/sk/f/e;Z)V

    invoke-virtual {v1, v2, v3}, Lcom/stagnationlab/sk/a/a;->a(Ljava/lang/String;Lcom/stagnationlab/sk/a/a/t;)V

    return-void
.end method

.method static synthetic g(Lcom/stagnationlab/sk/f/e;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .line 25
    iput-object p1, p0, Lcom/stagnationlab/sk/f/e;->q:Ljava/lang/String;

    return-object p1
.end method

.method private g()V
    .locals 3

    .line 368
    iget-object v0, p0, Lcom/stagnationlab/sk/f/e;->x:Lcom/stagnationlab/sk/f/a;

    if-nez v0, :cond_0

    return-void

    .line 372
    :cond_0
    iget-boolean v1, p0, Lcom/stagnationlab/sk/f/e;->c:Z

    if-eqz v1, :cond_3

    iget-boolean v1, p0, Lcom/stagnationlab/sk/f/e;->e:Z

    if-nez v1, :cond_1

    goto :goto_1

    .line 376
    :cond_1
    iget-object v1, p0, Lcom/stagnationlab/sk/f/e;->f:Lcom/stagnationlab/sk/c/a;

    const/4 v2, 0x0

    if-nez v1, :cond_2

    .line 377
    sget-object v0, Lcom/stagnationlab/sk/f/e$a;->d:Lcom/stagnationlab/sk/f/e$a;

    iput-object v0, p0, Lcom/stagnationlab/sk/f/e;->b:Lcom/stagnationlab/sk/f/e$a;

    .line 378
    invoke-static {v2}, Lcom/stagnationlab/sk/a/d;->b(Z)V

    .line 379
    iget-object v0, p0, Lcom/stagnationlab/sk/f/e;->p:Lcom/stagnationlab/sk/models/Account;

    iget-boolean v1, p0, Lcom/stagnationlab/sk/f/e;->d:Z

    invoke-virtual {v0, v1}, Lcom/stagnationlab/sk/models/Account;->b(Z)V

    .line 381
    iget-object v0, p0, Lcom/stagnationlab/sk/f/e;->x:Lcom/stagnationlab/sk/f/a;

    iget-object v1, p0, Lcom/stagnationlab/sk/f/e;->p:Lcom/stagnationlab/sk/models/Account;

    iget-object v2, p0, Lcom/stagnationlab/sk/f/e;->s:Lcom/stagnationlab/sk/models/Transaction;

    invoke-interface {v0, v1, v2}, Lcom/stagnationlab/sk/f/a;->a(Lcom/stagnationlab/sk/models/Account;Lcom/stagnationlab/sk/models/Transaction;)V

    goto :goto_0

    .line 383
    :cond_2
    iput-boolean v2, p0, Lcom/stagnationlab/sk/f/e;->e:Z

    .line 384
    invoke-interface {v0, v1}, Lcom/stagnationlab/sk/f/a;->a(Lcom/stagnationlab/sk/c/a;)V

    :goto_0
    const/4 v0, 0x0

    .line 387
    iput-object v0, p0, Lcom/stagnationlab/sk/f/e;->x:Lcom/stagnationlab/sk/f/a;

    :cond_3
    :goto_1
    return-void
.end method

.method static synthetic h(Lcom/stagnationlab/sk/f/e;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .line 25
    iput-object p1, p0, Lcom/stagnationlab/sk/f/e;->r:Ljava/lang/String;

    return-object p1
.end method

.method private h()V
    .locals 3

    .line 444
    new-instance v0, Lcom/stagnationlab/sk/f/d;

    invoke-direct {v0}, Lcom/stagnationlab/sk/f/d;-><init>()V

    sget-object v1, Lcom/stagnationlab/sk/f/e;->a:Lcom/stagnationlab/sk/a/a;

    new-instance v2, Lcom/stagnationlab/sk/f/e$6;

    invoke-direct {v2, p0}, Lcom/stagnationlab/sk/f/e$6;-><init>(Lcom/stagnationlab/sk/f/e;)V

    invoke-virtual {v0, v1, v2}, Lcom/stagnationlab/sk/f/d;->a(Lcom/stagnationlab/sk/a/a;Lcom/stagnationlab/sk/f/b;)V

    return-void
.end method

.method private i()V
    .locals 2

    .line 476
    iget-object v0, p0, Lcom/stagnationlab/sk/f/e;->g:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    return-void
.end method

.method private j()Lcom/stagnationlab/sk/c/a;
    .locals 2

    .line 484
    new-instance v0, Lcom/stagnationlab/sk/c/a;

    sget-object v1, Lcom/stagnationlab/sk/c/b;->K:Lcom/stagnationlab/sk/c/b;

    invoke-direct {v0, v1}, Lcom/stagnationlab/sk/c/a;-><init>(Lcom/stagnationlab/sk/c/b;)V

    return-object v0
.end method

.method private k()Lcom/stagnationlab/sk/c/a;
    .locals 2

    .line 488
    new-instance v0, Lcom/stagnationlab/sk/c/a;

    sget-object v1, Lcom/stagnationlab/sk/c/b;->L:Lcom/stagnationlab/sk/c/b;

    invoke-direct {v0, v1}, Lcom/stagnationlab/sk/c/a;-><init>(Lcom/stagnationlab/sk/c/b;)V

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 4

    .line 418
    iget-object v0, p0, Lcom/stagnationlab/sk/f/e;->w:Lcom/stagnationlab/sk/a/a/r;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 419
    new-instance v2, Lcom/stagnationlab/sk/c/a;

    sget-object v3, Lcom/stagnationlab/sk/c/b;->J:Lcom/stagnationlab/sk/c/b;

    invoke-direct {v2, v3}, Lcom/stagnationlab/sk/c/a;-><init>(Lcom/stagnationlab/sk/c/b;)V

    invoke-interface {v0, v2}, Lcom/stagnationlab/sk/a/a/r;->a(Lcom/stagnationlab/sk/c/a;)V

    .line 420
    iput-object v1, p0, Lcom/stagnationlab/sk/f/e;->w:Lcom/stagnationlab/sk/a/a/r;

    .line 423
    :cond_0
    iget-object v0, p0, Lcom/stagnationlab/sk/f/e;->x:Lcom/stagnationlab/sk/f/a;

    if-eqz v0, :cond_1

    .line 424
    new-instance v2, Lcom/stagnationlab/sk/c/a;

    sget-object v3, Lcom/stagnationlab/sk/c/b;->J:Lcom/stagnationlab/sk/c/b;

    invoke-direct {v2, v3}, Lcom/stagnationlab/sk/c/a;-><init>(Lcom/stagnationlab/sk/c/b;)V

    invoke-interface {v0, v2}, Lcom/stagnationlab/sk/f/a;->a(Lcom/stagnationlab/sk/c/a;)V

    .line 425
    iput-object v1, p0, Lcom/stagnationlab/sk/f/e;->x:Lcom/stagnationlab/sk/f/a;

    .line 428
    :cond_1
    iget-object v0, p0, Lcom/stagnationlab/sk/f/e;->y:Lcom/stagnationlab/sk/f/f;

    if-eqz v0, :cond_2

    .line 429
    new-instance v2, Lcom/stagnationlab/sk/c/a;

    sget-object v3, Lcom/stagnationlab/sk/c/b;->J:Lcom/stagnationlab/sk/c/b;

    invoke-direct {v2, v3}, Lcom/stagnationlab/sk/c/a;-><init>(Lcom/stagnationlab/sk/c/b;)V

    invoke-interface {v0, v2}, Lcom/stagnationlab/sk/f/f;->a(Lcom/stagnationlab/sk/c/a;)V

    .line 430
    iput-object v1, p0, Lcom/stagnationlab/sk/f/e;->y:Lcom/stagnationlab/sk/f/f;

    .line 433
    :cond_2
    invoke-direct {p0}, Lcom/stagnationlab/sk/f/e;->i()V

    .line 434
    invoke-static {}, Lcom/stagnationlab/sk/f/e;->b()V

    .line 436
    sget-object v0, Lcom/stagnationlab/sk/f/e$a;->f:Lcom/stagnationlab/sk/f/e$a;

    iput-object v0, p0, Lcom/stagnationlab/sk/f/e;->b:Lcom/stagnationlab/sk/f/e$a;

    return-void
.end method

.method public a(Lcom/stagnationlab/sk/a/a/r;)V
    .locals 3

    .line 132
    iget-object v0, p0, Lcom/stagnationlab/sk/f/e;->b:Lcom/stagnationlab/sk/f/e$a;

    sget-object v1, Lcom/stagnationlab/sk/f/e$a;->c:Lcom/stagnationlab/sk/f/e$a;

    if-eq v0, v1, :cond_0

    .line 133
    invoke-direct {p0}, Lcom/stagnationlab/sk/f/e;->j()Lcom/stagnationlab/sk/c/a;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/stagnationlab/sk/a/a/r;->a(Lcom/stagnationlab/sk/c/a;)V

    return-void

    .line 137
    :cond_0
    iget-object v0, p0, Lcom/stagnationlab/sk/f/e;->w:Lcom/stagnationlab/sk/a/a/r;

    if-eqz v0, :cond_1

    .line 138
    invoke-direct {p0}, Lcom/stagnationlab/sk/f/e;->k()Lcom/stagnationlab/sk/c/a;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/stagnationlab/sk/a/a/r;->a(Lcom/stagnationlab/sk/c/a;)V

    return-void

    .line 142
    :cond_1
    iget-object v0, p0, Lcom/stagnationlab/sk/f/e;->x:Lcom/stagnationlab/sk/f/a;

    if-eqz v0, :cond_2

    .line 144
    new-instance v1, Lcom/stagnationlab/sk/c/a;

    sget-object v2, Lcom/stagnationlab/sk/c/b;->k:Lcom/stagnationlab/sk/c/b;

    invoke-direct {v1, v2}, Lcom/stagnationlab/sk/c/a;-><init>(Lcom/stagnationlab/sk/c/b;)V

    invoke-interface {v0, v1}, Lcom/stagnationlab/sk/f/a;->a(Lcom/stagnationlab/sk/c/a;)V

    const/4 v0, 0x0

    .line 147
    iput-object v0, p0, Lcom/stagnationlab/sk/f/e;->x:Lcom/stagnationlab/sk/f/a;

    .line 148
    invoke-direct {p0}, Lcom/stagnationlab/sk/f/e;->i()V

    .line 151
    :cond_2
    iput-object p1, p0, Lcom/stagnationlab/sk/f/e;->w:Lcom/stagnationlab/sk/a/a/r;

    .line 153
    sget-object p1, Lcom/stagnationlab/sk/f/e;->a:Lcom/stagnationlab/sk/a/a;

    iget-object v0, p0, Lcom/stagnationlab/sk/f/e;->j:Ljava/lang/String;

    iget-object v1, p0, Lcom/stagnationlab/sk/f/e;->k:Ljava/lang/String;

    new-instance v2, Lcom/stagnationlab/sk/f/e$2;

    invoke-direct {v2, p0}, Lcom/stagnationlab/sk/f/e$2;-><init>(Lcom/stagnationlab/sk/f/e;)V

    invoke-virtual {p1, v0, v1, v2}, Lcom/stagnationlab/sk/a/a;->a(Ljava/lang/String;Ljava/lang/String;Lcom/stagnationlab/sk/a/a/r;)V

    return-void
.end method

.method public a(Lcom/stagnationlab/sk/f/c;)V
    .locals 2

    .line 68
    iget-object v0, p0, Lcom/stagnationlab/sk/f/e;->b:Lcom/stagnationlab/sk/f/e$a;

    sget-object v1, Lcom/stagnationlab/sk/f/e$a;->a:Lcom/stagnationlab/sk/f/e$a;

    if-eq v0, v1, :cond_0

    .line 69
    invoke-direct {p0}, Lcom/stagnationlab/sk/f/e;->j()Lcom/stagnationlab/sk/c/a;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/stagnationlab/sk/f/c;->a(Lcom/stagnationlab/sk/c/a;)V

    return-void

    .line 73
    :cond_0
    iput-object p1, p0, Lcom/stagnationlab/sk/f/e;->u:Lcom/stagnationlab/sk/f/c;

    .line 74
    invoke-direct {p0}, Lcom/stagnationlab/sk/f/e;->h()V

    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 0

    .line 480
    iput-object p1, p0, Lcom/stagnationlab/sk/f/e;->l:Ljava/lang/String;

    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Lcom/stagnationlab/sk/a/a/l;)V
    .locals 2

    .line 80
    iget-object v0, p0, Lcom/stagnationlab/sk/f/e;->b:Lcom/stagnationlab/sk/f/e$a;

    sget-object v1, Lcom/stagnationlab/sk/f/e$a;->b:Lcom/stagnationlab/sk/f/e$a;

    if-eq v0, v1, :cond_0

    .line 81
    invoke-direct {p0}, Lcom/stagnationlab/sk/f/e;->j()Lcom/stagnationlab/sk/c/a;

    move-result-object p1

    invoke-interface {p3, p1}, Lcom/stagnationlab/sk/a/a/l;->a(Lcom/stagnationlab/sk/c/a;)V

    return-void

    .line 85
    :cond_0
    iget-object v0, p0, Lcom/stagnationlab/sk/f/e;->v:Lcom/stagnationlab/sk/a/a/l;

    if-eqz v0, :cond_1

    .line 86
    invoke-direct {p0}, Lcom/stagnationlab/sk/f/e;->k()Lcom/stagnationlab/sk/c/a;

    move-result-object v0

    invoke-interface {p3, v0}, Lcom/stagnationlab/sk/a/a/l;->a(Lcom/stagnationlab/sk/c/a;)V

    .line 89
    :cond_1
    iput-object p3, p0, Lcom/stagnationlab/sk/f/e;->v:Lcom/stagnationlab/sk/a/a/l;

    .line 91
    sget-object p3, Lcom/stagnationlab/sk/f/e;->a:Lcom/stagnationlab/sk/a/a;

    new-instance v0, Lcom/stagnationlab/sk/f/e$1;

    invoke-direct {v0, p0, p1, p2}, Lcom/stagnationlab/sk/f/e$1;-><init>(Lcom/stagnationlab/sk/f/e;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p3, v0}, Lcom/stagnationlab/sk/a/a;->a(Lcom/stagnationlab/sk/a/a/m;)V

    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Lcom/stagnationlab/sk/f/f;)V
    .locals 2

    .line 232
    sget-object v0, Lcom/stagnationlab/sk/models/AccountKey$Type;->SIGNATURE:Lcom/stagnationlab/sk/models/AccountKey$Type;

    invoke-virtual {v0}, Lcom/stagnationlab/sk/models/AccountKey$Type;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 234
    iget-object v0, p0, Lcom/stagnationlab/sk/f/e;->b:Lcom/stagnationlab/sk/f/e$a;

    sget-object v1, Lcom/stagnationlab/sk/f/e$a;->e:Lcom/stagnationlab/sk/f/e$a;

    if-ne v0, v1, :cond_1

    :cond_0
    if-nez p1, :cond_2

    iget-object v0, p0, Lcom/stagnationlab/sk/f/e;->b:Lcom/stagnationlab/sk/f/e$a;

    sget-object v1, Lcom/stagnationlab/sk/f/e$a;->d:Lcom/stagnationlab/sk/f/e$a;

    if-eq v0, v1, :cond_2

    .line 235
    :cond_1
    invoke-direct {p0}, Lcom/stagnationlab/sk/f/e;->j()Lcom/stagnationlab/sk/c/a;

    move-result-object p1

    invoke-interface {p3, p1}, Lcom/stagnationlab/sk/f/f;->a(Lcom/stagnationlab/sk/c/a;)V

    return-void

    .line 239
    :cond_2
    iget-object v0, p0, Lcom/stagnationlab/sk/f/e;->y:Lcom/stagnationlab/sk/f/f;

    if-eqz v0, :cond_3

    .line 240
    invoke-direct {p0}, Lcom/stagnationlab/sk/f/e;->k()Lcom/stagnationlab/sk/c/a;

    move-result-object p1

    invoke-interface {p3, p1}, Lcom/stagnationlab/sk/f/f;->a(Lcom/stagnationlab/sk/c/a;)V

    return-void

    :cond_3
    if-eqz p1, :cond_4

    .line 244
    iget-object p1, p0, Lcom/stagnationlab/sk/f/e;->t:Lcom/stagnationlab/sk/models/Transaction;

    goto :goto_0

    :cond_4
    iget-object p1, p0, Lcom/stagnationlab/sk/f/e;->s:Lcom/stagnationlab/sk/models/Transaction;

    .line 245
    :goto_0
    iput-object p3, p0, Lcom/stagnationlab/sk/f/e;->y:Lcom/stagnationlab/sk/f/f;

    .line 247
    sget-object p3, Lcom/stagnationlab/sk/f/e;->a:Lcom/stagnationlab/sk/a/a;

    new-instance v0, Lcom/stagnationlab/sk/f/e$4;

    invoke-direct {v0, p0, p1}, Lcom/stagnationlab/sk/f/e$4;-><init>(Lcom/stagnationlab/sk/f/e;Lcom/stagnationlab/sk/models/Transaction;)V

    invoke-virtual {p3, p1, p2, v0}, Lcom/stagnationlab/sk/a/a;->a(Lcom/stagnationlab/sk/models/Transaction;Ljava/lang/String;Lcom/stagnationlab/sk/a/a/x;)V

    return-void
.end method

.method public a(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/stagnationlab/sk/models/LegalGuardian;",
            ">;)V"
        }
    .end annotation

    .line 496
    iput-object p1, p0, Lcom/stagnationlab/sk/f/e;->o:Ljava/util/List;

    return-void
.end method

.method public a(Z)V
    .locals 1

    const/4 v0, 0x1

    .line 356
    iput-boolean v0, p0, Lcom/stagnationlab/sk/f/e;->c:Z

    .line 357
    iput-boolean p1, p0, Lcom/stagnationlab/sk/f/e;->d:Z

    .line 358
    invoke-direct {p0}, Lcom/stagnationlab/sk/f/e;->g()V

    return-void
.end method

.method public a(ZLcom/stagnationlab/sk/f/a;)V
    .locals 8

    .line 173
    iget-object v0, p0, Lcom/stagnationlab/sk/f/e;->b:Lcom/stagnationlab/sk/f/e$a;

    sget-object v1, Lcom/stagnationlab/sk/f/e$a;->c:Lcom/stagnationlab/sk/f/e$a;

    if-eq v0, v1, :cond_0

    .line 174
    invoke-direct {p0}, Lcom/stagnationlab/sk/f/e;->j()Lcom/stagnationlab/sk/c/a;

    move-result-object p1

    invoke-interface {p2, p1}, Lcom/stagnationlab/sk/f/a;->a(Lcom/stagnationlab/sk/c/a;)V

    return-void

    .line 178
    :cond_0
    iget-object v0, p0, Lcom/stagnationlab/sk/f/e;->x:Lcom/stagnationlab/sk/f/a;

    if-eqz v0, :cond_1

    .line 179
    invoke-direct {p0}, Lcom/stagnationlab/sk/f/e;->k()Lcom/stagnationlab/sk/c/a;

    move-result-object v0

    invoke-interface {p2, v0}, Lcom/stagnationlab/sk/f/a;->a(Lcom/stagnationlab/sk/c/a;)V

    .line 182
    :cond_1
    invoke-direct {p0}, Lcom/stagnationlab/sk/f/e;->i()V

    .line 183
    iput-object p2, p0, Lcom/stagnationlab/sk/f/e;->x:Lcom/stagnationlab/sk/f/a;

    .line 185
    sget-object v1, Lcom/stagnationlab/sk/f/e;->a:Lcom/stagnationlab/sk/a/a;

    iget-object v3, p0, Lcom/stagnationlab/sk/f/e;->j:Ljava/lang/String;

    iget-object v4, p0, Lcom/stagnationlab/sk/f/e;->k:Ljava/lang/String;

    iget-object v5, p0, Lcom/stagnationlab/sk/f/e;->l:Ljava/lang/String;

    iget-object v6, p0, Lcom/stagnationlab/sk/f/e;->m:Ljava/lang/String;

    new-instance v7, Lcom/stagnationlab/sk/f/e$3;

    invoke-direct {v7, p0, p2}, Lcom/stagnationlab/sk/f/e$3;-><init>(Lcom/stagnationlab/sk/f/e;Lcom/stagnationlab/sk/f/a;)V

    move v2, p1

    invoke-virtual/range {v1 .. v7}, Lcom/stagnationlab/sk/a/a;->a(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/stagnationlab/sk/a/a/i;)V

    return-void
.end method

.method public c()Z
    .locals 1

    .line 492
    iget-boolean v0, p0, Lcom/stagnationlab/sk/f/e;->c:Z

    return v0
.end method
