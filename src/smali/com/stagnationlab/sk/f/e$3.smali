.class Lcom/stagnationlab/sk/f/e$3;
.super Ljava/lang/Object;
.source "Registration.java"

# interfaces
.implements Lcom/stagnationlab/sk/a/a/i;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/stagnationlab/sk/f/e;->a(ZLcom/stagnationlab/sk/f/a;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/stagnationlab/sk/f/a;

.field final synthetic b:Lcom/stagnationlab/sk/f/e;


# direct methods
.method constructor <init>(Lcom/stagnationlab/sk/f/e;Lcom/stagnationlab/sk/f/a;)V
    .locals 0

    .line 191
    iput-object p1, p0, Lcom/stagnationlab/sk/f/e$3;->b:Lcom/stagnationlab/sk/f/e;

    iput-object p2, p0, Lcom/stagnationlab/sk/f/e$3;->a:Lcom/stagnationlab/sk/f/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/stagnationlab/sk/c/a;)V
    .locals 2

    .line 215
    invoke-virtual {p1}, Lcom/stagnationlab/sk/c/a;->a()Lcom/stagnationlab/sk/c/b;

    move-result-object v0

    sget-object v1, Lcom/stagnationlab/sk/c/b;->k:Lcom/stagnationlab/sk/c/b;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/stagnationlab/sk/f/e$3;->b:Lcom/stagnationlab/sk/f/e;

    .line 216
    invoke-static {v0}, Lcom/stagnationlab/sk/f/e;->c(Lcom/stagnationlab/sk/f/e;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/stagnationlab/sk/f/e$3;->b:Lcom/stagnationlab/sk/f/e;

    .line 217
    invoke-static {v0}, Lcom/stagnationlab/sk/f/e;->d(Lcom/stagnationlab/sk/f/e;)Lcom/stagnationlab/sk/f/e$a;

    move-result-object v0

    sget-object v1, Lcom/stagnationlab/sk/f/e$a;->c:Lcom/stagnationlab/sk/f/e$a;

    if-ne v0, v1, :cond_0

    .line 220
    iget-object p1, p0, Lcom/stagnationlab/sk/f/e$3;->b:Lcom/stagnationlab/sk/f/e;

    iget-object v0, p0, Lcom/stagnationlab/sk/f/e$3;->a:Lcom/stagnationlab/sk/f/a;

    invoke-static {p1, v0}, Lcom/stagnationlab/sk/f/e;->a(Lcom/stagnationlab/sk/f/e;Lcom/stagnationlab/sk/f/a;)V

    return-void

    .line 225
    :cond_0
    iget-object v0, p0, Lcom/stagnationlab/sk/f/e$3;->b:Lcom/stagnationlab/sk/f/e;

    invoke-static {v0, p1}, Lcom/stagnationlab/sk/f/e;->b(Lcom/stagnationlab/sk/f/e;Lcom/stagnationlab/sk/c/a;)V

    return-void
.end method

.method public a(Lcom/stagnationlab/sk/models/Account;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .line 196
    iget-object v0, p0, Lcom/stagnationlab/sk/f/e$3;->b:Lcom/stagnationlab/sk/f/e;

    invoke-static {v0}, Lcom/stagnationlab/sk/f/e;->a(Lcom/stagnationlab/sk/f/e;)Ljava/util/List;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/stagnationlab/sk/models/Account;->a(Ljava/util/List;)V

    .line 198
    iget-object v0, p0, Lcom/stagnationlab/sk/f/e$3;->b:Lcom/stagnationlab/sk/f/e;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/stagnationlab/sk/f/e;->a(Lcom/stagnationlab/sk/f/e;Ljava/util/List;)Ljava/util/List;

    .line 199
    iget-object v0, p0, Lcom/stagnationlab/sk/f/e$3;->b:Lcom/stagnationlab/sk/f/e;

    invoke-static {v0, v1}, Lcom/stagnationlab/sk/f/e;->a(Lcom/stagnationlab/sk/f/e;Ljava/lang/String;)Ljava/lang/String;

    .line 200
    iget-object v0, p0, Lcom/stagnationlab/sk/f/e$3;->b:Lcom/stagnationlab/sk/f/e;

    invoke-static {v0, v1}, Lcom/stagnationlab/sk/f/e;->b(Lcom/stagnationlab/sk/f/e;Ljava/lang/String;)Ljava/lang/String;

    .line 201
    iget-object v0, p0, Lcom/stagnationlab/sk/f/e$3;->b:Lcom/stagnationlab/sk/f/e;

    invoke-static {v0, v1}, Lcom/stagnationlab/sk/f/e;->e(Lcom/stagnationlab/sk/f/e;Ljava/lang/String;)Ljava/lang/String;

    .line 202
    iget-object v0, p0, Lcom/stagnationlab/sk/f/e$3;->b:Lcom/stagnationlab/sk/f/e;

    invoke-static {v0, v1}, Lcom/stagnationlab/sk/f/e;->f(Lcom/stagnationlab/sk/f/e;Ljava/lang/String;)Ljava/lang/String;

    .line 203
    iget-object v0, p0, Lcom/stagnationlab/sk/f/e$3;->b:Lcom/stagnationlab/sk/f/e;

    invoke-static {v0, v1}, Lcom/stagnationlab/sk/f/e;->a(Lcom/stagnationlab/sk/f/e;Lorg/b/a/b;)Lorg/b/a/b;

    .line 205
    iget-object v0, p0, Lcom/stagnationlab/sk/f/e$3;->b:Lcom/stagnationlab/sk/f/e;

    invoke-static {v0, p1}, Lcom/stagnationlab/sk/f/e;->a(Lcom/stagnationlab/sk/f/e;Lcom/stagnationlab/sk/models/Account;)Lcom/stagnationlab/sk/models/Account;

    .line 206
    iget-object p1, p0, Lcom/stagnationlab/sk/f/e$3;->b:Lcom/stagnationlab/sk/f/e;

    invoke-static {p1, p2}, Lcom/stagnationlab/sk/f/e;->g(Lcom/stagnationlab/sk/f/e;Ljava/lang/String;)Ljava/lang/String;

    .line 207
    iget-object p1, p0, Lcom/stagnationlab/sk/f/e$3;->b:Lcom/stagnationlab/sk/f/e;

    invoke-static {p1, p3}, Lcom/stagnationlab/sk/f/e;->h(Lcom/stagnationlab/sk/f/e;Ljava/lang/String;)Ljava/lang/String;

    .line 209
    iget-object p1, p0, Lcom/stagnationlab/sk/f/e$3;->b:Lcom/stagnationlab/sk/f/e;

    invoke-static {p1}, Lcom/stagnationlab/sk/f/e;->b(Lcom/stagnationlab/sk/f/e;)V

    return-void
.end method
