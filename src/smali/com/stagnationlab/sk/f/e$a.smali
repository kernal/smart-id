.class final enum Lcom/stagnationlab/sk/f/e$a;
.super Ljava/lang/Enum;
.source "Registration.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/stagnationlab/sk/f/e;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/stagnationlab/sk/f/e$a;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/stagnationlab/sk/f/e$a;

.field public static final enum b:Lcom/stagnationlab/sk/f/e$a;

.field public static final enum c:Lcom/stagnationlab/sk/f/e$a;

.field public static final enum d:Lcom/stagnationlab/sk/f/e$a;

.field public static final enum e:Lcom/stagnationlab/sk/f/e$a;

.field public static final enum f:Lcom/stagnationlab/sk/f/e$a;

.field private static final synthetic g:[Lcom/stagnationlab/sk/f/e$a;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .line 28
    new-instance v0, Lcom/stagnationlab/sk/f/e$a;

    const/4 v1, 0x0

    const-string v2, "UNINIT"

    invoke-direct {v0, v2, v1}, Lcom/stagnationlab/sk/f/e$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/stagnationlab/sk/f/e$a;->a:Lcom/stagnationlab/sk/f/e$a;

    new-instance v0, Lcom/stagnationlab/sk/f/e$a;

    const/4 v2, 0x1

    const-string v3, "INIT"

    invoke-direct {v0, v3, v2}, Lcom/stagnationlab/sk/f/e$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/stagnationlab/sk/f/e$a;->b:Lcom/stagnationlab/sk/f/e$a;

    new-instance v0, Lcom/stagnationlab/sk/f/e$a;

    const/4 v3, 0x2

    const-string v4, "KEYS"

    invoke-direct {v0, v4, v3}, Lcom/stagnationlab/sk/f/e$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/stagnationlab/sk/f/e$a;->c:Lcom/stagnationlab/sk/f/e$a;

    new-instance v0, Lcom/stagnationlab/sk/f/e$a;

    const/4 v4, 0x3

    const-string v5, "ACCOUNT"

    invoke-direct {v0, v5, v4}, Lcom/stagnationlab/sk/f/e$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/stagnationlab/sk/f/e$a;->d:Lcom/stagnationlab/sk/f/e$a;

    new-instance v0, Lcom/stagnationlab/sk/f/e$a;

    const/4 v5, 0x4

    const-string v6, "AUTH"

    invoke-direct {v0, v6, v5}, Lcom/stagnationlab/sk/f/e$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/stagnationlab/sk/f/e$a;->e:Lcom/stagnationlab/sk/f/e$a;

    new-instance v0, Lcom/stagnationlab/sk/f/e$a;

    const/4 v6, 0x5

    const-string v7, "END"

    invoke-direct {v0, v7, v6}, Lcom/stagnationlab/sk/f/e$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/stagnationlab/sk/f/e$a;->f:Lcom/stagnationlab/sk/f/e$a;

    const/4 v0, 0x6

    new-array v0, v0, [Lcom/stagnationlab/sk/f/e$a;

    sget-object v7, Lcom/stagnationlab/sk/f/e$a;->a:Lcom/stagnationlab/sk/f/e$a;

    aput-object v7, v0, v1

    sget-object v1, Lcom/stagnationlab/sk/f/e$a;->b:Lcom/stagnationlab/sk/f/e$a;

    aput-object v1, v0, v2

    sget-object v1, Lcom/stagnationlab/sk/f/e$a;->c:Lcom/stagnationlab/sk/f/e$a;

    aput-object v1, v0, v3

    sget-object v1, Lcom/stagnationlab/sk/f/e$a;->d:Lcom/stagnationlab/sk/f/e$a;

    aput-object v1, v0, v4

    sget-object v1, Lcom/stagnationlab/sk/f/e$a;->e:Lcom/stagnationlab/sk/f/e$a;

    aput-object v1, v0, v5

    sget-object v1, Lcom/stagnationlab/sk/f/e$a;->f:Lcom/stagnationlab/sk/f/e$a;

    aput-object v1, v0, v6

    sput-object v0, Lcom/stagnationlab/sk/f/e$a;->g:[Lcom/stagnationlab/sk/f/e$a;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 28
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/stagnationlab/sk/f/e$a;
    .locals 1

    .line 28
    const-class v0, Lcom/stagnationlab/sk/f/e$a;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/stagnationlab/sk/f/e$a;

    return-object p0
.end method

.method public static values()[Lcom/stagnationlab/sk/f/e$a;
    .locals 1

    .line 28
    sget-object v0, Lcom/stagnationlab/sk/f/e$a;->g:[Lcom/stagnationlab/sk/f/e$a;

    invoke-virtual {v0}, [Lcom/stagnationlab/sk/f/e$a;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/stagnationlab/sk/f/e$a;

    return-object v0
.end method
