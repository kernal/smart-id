.class Lcom/stagnationlab/sk/f/e$1;
.super Ljava/lang/Object;
.source "Registration.java"

# interfaces
.implements Lcom/stagnationlab/sk/a/a/m;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/stagnationlab/sk/f/e;->a(Ljava/lang/String;Ljava/lang/String;Lcom/stagnationlab/sk/a/a/l;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:Ljava/lang/String;

.field final synthetic c:Lcom/stagnationlab/sk/f/e;


# direct methods
.method constructor <init>(Lcom/stagnationlab/sk/f/e;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 91
    iput-object p1, p0, Lcom/stagnationlab/sk/f/e$1;->c:Lcom/stagnationlab/sk/f/e;

    iput-object p2, p0, Lcom/stagnationlab/sk/f/e$1;->a:Ljava/lang/String;

    iput-object p3, p0, Lcom/stagnationlab/sk/f/e$1;->b:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/stagnationlab/sk/c/a;)V
    .locals 1

    .line 126
    iget-object v0, p0, Lcom/stagnationlab/sk/f/e$1;->c:Lcom/stagnationlab/sk/f/e;

    invoke-static {v0, p1}, Lcom/stagnationlab/sk/f/e;->a(Lcom/stagnationlab/sk/f/e;Lcom/stagnationlab/sk/c/a;)V

    return-void
.end method

.method public a(Ljava/util/List;J)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;J)V"
        }
    .end annotation

    .line 94
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Keys generated, took: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string p2, "ms"

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    const-string p3, "Registration"

    invoke-static {p3, p2}, Lcom/stagnationlab/sk/f;->b(Ljava/lang/String;Ljava/lang/String;)V

    const/4 p2, 0x0

    .line 96
    invoke-interface {p1, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p2

    move-object v1, p2

    check-cast v1, Ljava/lang/String;

    const/4 p2, 0x1

    .line 97
    invoke-interface {p1, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    move-object v3, p1

    check-cast v3, Ljava/lang/String;

    .line 99
    invoke-static {}, Lcom/stagnationlab/sk/f/e;->d()Lcom/stagnationlab/sk/a/a;

    move-result-object v0

    iget-object v2, p0, Lcom/stagnationlab/sk/f/e$1;->a:Ljava/lang/String;

    iget-object v4, p0, Lcom/stagnationlab/sk/f/e$1;->b:Ljava/lang/String;

    new-instance v5, Lcom/stagnationlab/sk/f/e$1$1;

    invoke-direct {v5, p0, v1, v3}, Lcom/stagnationlab/sk/f/e$1$1;-><init>(Lcom/stagnationlab/sk/f/e$1;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual/range {v0 .. v5}, Lcom/stagnationlab/sk/a/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/stagnationlab/sk/a/a/l;)V

    .line 120
    iget-object p1, p0, Lcom/stagnationlab/sk/f/e$1;->c:Lcom/stagnationlab/sk/f/e;

    const/4 p2, 0x0

    invoke-static {p1, p2}, Lcom/stagnationlab/sk/f/e;->c(Lcom/stagnationlab/sk/f/e;Ljava/lang/String;)Ljava/lang/String;

    .line 121
    iget-object p1, p0, Lcom/stagnationlab/sk/f/e$1;->c:Lcom/stagnationlab/sk/f/e;

    invoke-static {p1, p2}, Lcom/stagnationlab/sk/f/e;->d(Lcom/stagnationlab/sk/f/e;Ljava/lang/String;)Ljava/lang/String;

    return-void
.end method
