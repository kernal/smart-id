.class public Lcom/stagnationlab/sk/f/d;
.super Ljava/lang/Object;
.source "PinGenerator.java"


# instance fields
.field private a:Lcom/stagnationlab/sk/a/a;

.field private b:Lcom/stagnationlab/sk/f/b;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:Ljava/security/SecureRandom;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(I)Ljava/lang/String;
    .locals 5

    .line 31
    iget-object v0, p0, Lcom/stagnationlab/sk/f/d;->e:Ljava/security/SecureRandom;

    int-to-double v1, p1

    const-wide/high16 v3, 0x4024000000000000L    # 10.0

    invoke-static {v3, v4, v1, v2}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v1

    double-to-int v1, v1

    invoke-virtual {v0, v1}, Ljava/security/SecureRandom;->nextInt(I)I

    move-result v0

    .line 32
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "%0"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, "d"

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const/4 v2, 0x0

    aput-object v0, v1, v2

    invoke-static {p1, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method static synthetic a(Lcom/stagnationlab/sk/f/d;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .line 10
    iput-object p1, p0, Lcom/stagnationlab/sk/f/d;->d:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic a(Lcom/stagnationlab/sk/f/d;Ljava/security/SecureRandom;)Ljava/security/SecureRandom;
    .locals 0

    .line 10
    iput-object p1, p0, Lcom/stagnationlab/sk/f/d;->e:Ljava/security/SecureRandom;

    return-object p1
.end method

.method private a()V
    .locals 4

    .line 36
    iget-object v0, p0, Lcom/stagnationlab/sk/f/d;->c:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_1

    const/4 v1, 0x5

    goto :goto_1

    :cond_1
    const/4 v1, 0x4

    .line 38
    :goto_1
    invoke-direct {p0, v1}, Lcom/stagnationlab/sk/f/d;->a(I)Ljava/lang/String;

    move-result-object v1

    .line 40
    iget-object v2, p0, Lcom/stagnationlab/sk/f/d;->a:Lcom/stagnationlab/sk/a/a;

    new-instance v3, Lcom/stagnationlab/sk/f/d$2;

    invoke-direct {v3, p0, v0, v1}, Lcom/stagnationlab/sk/f/d$2;-><init>(Lcom/stagnationlab/sk/f/d;ZLjava/lang/String;)V

    invoke-virtual {v2, v1, v3}, Lcom/stagnationlab/sk/a/a;->a(Ljava/lang/String;Lcom/stagnationlab/sk/a/a/z;)V

    return-void
.end method

.method static synthetic a(Lcom/stagnationlab/sk/f/d;)V
    .locals 0

    .line 10
    invoke-direct {p0}, Lcom/stagnationlab/sk/f/d;->a()V

    return-void
.end method

.method static synthetic b(Lcom/stagnationlab/sk/f/d;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .line 10
    iput-object p1, p0, Lcom/stagnationlab/sk/f/d;->c:Ljava/lang/String;

    return-object p1
.end method

.method private b()V
    .locals 3

    .line 60
    iget-object v0, p0, Lcom/stagnationlab/sk/f/d;->b:Lcom/stagnationlab/sk/f/b;

    iget-object v1, p0, Lcom/stagnationlab/sk/f/d;->c:Ljava/lang/String;

    iget-object v2, p0, Lcom/stagnationlab/sk/f/d;->d:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Lcom/stagnationlab/sk/f/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 61
    iput-object v0, p0, Lcom/stagnationlab/sk/f/d;->c:Ljava/lang/String;

    .line 62
    iput-object v0, p0, Lcom/stagnationlab/sk/f/d;->d:Ljava/lang/String;

    return-void
.end method

.method static synthetic b(Lcom/stagnationlab/sk/f/d;)V
    .locals 0

    .line 10
    invoke-direct {p0}, Lcom/stagnationlab/sk/f/d;->b()V

    return-void
.end method


# virtual methods
.method public a(Lcom/stagnationlab/sk/a/a;Lcom/stagnationlab/sk/f/b;)V
    .locals 0

    .line 18
    iput-object p1, p0, Lcom/stagnationlab/sk/f/d;->a:Lcom/stagnationlab/sk/a/a;

    .line 19
    iput-object p2, p0, Lcom/stagnationlab/sk/f/d;->b:Lcom/stagnationlab/sk/f/b;

    .line 21
    new-instance p2, Lcom/stagnationlab/sk/f/d$1;

    invoke-direct {p2, p0}, Lcom/stagnationlab/sk/f/d$1;-><init>(Lcom/stagnationlab/sk/f/d;)V

    invoke-virtual {p1, p2}, Lcom/stagnationlab/sk/a/a;->a(Lcom/stagnationlab/sk/a/a/s;)V

    return-void
.end method
