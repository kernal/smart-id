.class public Lcom/stagnationlab/sk/TransactionActivity;
.super Lcom/stagnationlab/sk/b;
.source "TransactionActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/stagnationlab/sk/TransactionActivity$a;,
        Lcom/stagnationlab/sk/TransactionActivity$b;
    }
.end annotation


# static fields
.field private static c:I

.field private static d:Landroid/view/animation/Interpolator;

.field private static e:I

.field private static f:I


# instance fields
.field private A:Landroid/widget/TextView;

.field private B:Landroid/os/Handler;

.field private C:Z

.field private D:Z

.field private E:Z

.field private F:Landroid/support/v7/widget/AppCompatImageView;

.field private G:Landroid/widget/ImageView;

.field private H:Landroid/widget/TextView;

.field private I:Landroid/widget/TextView;

.field private J:Landroid/widget/TextView;

.field private K:Landroid/widget/ImageView;

.field private L:Lcom/stagnationlab/sk/elements/MaxWidthLinearLayout;

.field private M:Landroid/widget/RelativeLayout;

.field private N:Landroid/widget/LinearLayout;

.field private O:Landroid/widget/RelativeLayout;

.field private P:Landroid/widget/RelativeLayout;

.field private Q:Lcom/stagnationlab/sk/PinFragment;

.field private R:Landroid/widget/ImageButton;

.field private S:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/stagnationlab/sk/g;",
            ">;"
        }
    .end annotation
.end field

.field private T:Landroid/widget/TextView;

.field private U:Landroid/widget/LinearLayout;

.field private V:Lcom/stagnationlab/sk/elements/Modal;

.field private W:[Landroid/view/View;

.field private X:Landroid/animation/ObjectAnimator;

.field private Y:F

.field private Z:F

.field private aa:Z

.field private ab:Z

.field private ac:Lcom/stagnationlab/sk/c/a;

.field private ad:Z

.field private ae:Z

.field private af:Z

.field private ag:Z

.field private g:Z

.field private h:Lcom/stagnationlab/sk/TransactionActivity$b;

.field private i:I

.field private j:I

.field private k:I

.field private l:Lcom/stagnationlab/sk/a/a;

.field private m:Lcom/stagnationlab/sk/a;

.field private n:Lcom/stagnationlab/sk/models/Transaction;

.field private o:Lcom/stagnationlab/sk/models/RpRequest;

.field private p:Landroid/widget/TextView;

.field private q:Landroid/widget/TextView;

.field private r:Landroid/widget/TextView;

.field private s:Landroid/widget/TextView;

.field private t:Landroid/widget/TextView;

.field private u:Landroid/widget/TextView;

.field private v:Landroid/widget/Button;

.field private w:Landroid/os/CountDownTimer;

.field private x:Landroid/os/CountDownTimer;

.field private y:Z

.field private z:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 68
    invoke-direct {p0}, Lcom/stagnationlab/sk/b;-><init>()V

    const/4 v0, 0x0

    .line 81
    iput-boolean v0, p0, Lcom/stagnationlab/sk/TransactionActivity;->g:Z

    .line 82
    sget-object v1, Lcom/stagnationlab/sk/TransactionActivity$b;->a:Lcom/stagnationlab/sk/TransactionActivity$b;

    iput-object v1, p0, Lcom/stagnationlab/sk/TransactionActivity;->h:Lcom/stagnationlab/sk/TransactionActivity$b;

    const/4 v1, -0x1

    .line 85
    iput v1, p0, Lcom/stagnationlab/sk/TransactionActivity;->k:I

    const/4 v1, 0x0

    .line 88
    iput-object v1, p0, Lcom/stagnationlab/sk/TransactionActivity;->n:Lcom/stagnationlab/sk/models/Transaction;

    .line 89
    iput-object v1, p0, Lcom/stagnationlab/sk/TransactionActivity;->o:Lcom/stagnationlab/sk/models/RpRequest;

    .line 102
    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    iput-object v1, p0, Lcom/stagnationlab/sk/TransactionActivity;->B:Landroid/os/Handler;

    .line 103
    iput-boolean v0, p0, Lcom/stagnationlab/sk/TransactionActivity;->C:Z

    .line 105
    iput-boolean v0, p0, Lcom/stagnationlab/sk/TransactionActivity;->E:Z

    const/4 v1, 0x0

    .line 127
    iput v1, p0, Lcom/stagnationlab/sk/TransactionActivity;->Y:F

    const/high16 v1, -0x40800000    # -1.0f

    .line 128
    iput v1, p0, Lcom/stagnationlab/sk/TransactionActivity;->Z:F

    .line 134
    iput-boolean v0, p0, Lcom/stagnationlab/sk/TransactionActivity;->ad:Z

    .line 135
    iput-boolean v0, p0, Lcom/stagnationlab/sk/TransactionActivity;->ae:Z

    .line 136
    iput-boolean v0, p0, Lcom/stagnationlab/sk/TransactionActivity;->af:Z

    .line 137
    iput-boolean v0, p0, Lcom/stagnationlab/sk/TransactionActivity;->ag:Z

    return-void
.end method

.method private A()V
    .locals 1

    .line 1449
    iget-object v0, p0, Lcom/stagnationlab/sk/TransactionActivity;->w:Landroid/os/CountDownTimer;

    if-eqz v0, :cond_0

    .line 1450
    invoke-virtual {v0}, Landroid/os/CountDownTimer;->cancel()V

    const/4 v0, 0x0

    .line 1451
    iput-object v0, p0, Lcom/stagnationlab/sk/TransactionActivity;->w:Landroid/os/CountDownTimer;

    :cond_0
    return-void
.end method

.method private B()V
    .locals 1

    .line 1456
    invoke-direct {p0}, Lcom/stagnationlab/sk/TransactionActivity;->A()V

    const/4 v0, 0x0

    .line 1457
    iput-boolean v0, p0, Lcom/stagnationlab/sk/TransactionActivity;->y:Z

    return-void
.end method

.method private C()V
    .locals 1

    .line 1461
    iget-object v0, p0, Lcom/stagnationlab/sk/TransactionActivity;->x:Landroid/os/CountDownTimer;

    if-eqz v0, :cond_0

    .line 1462
    invoke-virtual {v0}, Landroid/os/CountDownTimer;->cancel()V

    const/4 v0, 0x0

    .line 1463
    iput-object v0, p0, Lcom/stagnationlab/sk/TransactionActivity;->x:Landroid/os/CountDownTimer;

    :cond_0
    return-void
.end method

.method private D()V
    .locals 1

    .line 1468
    invoke-direct {p0}, Lcom/stagnationlab/sk/TransactionActivity;->C()V

    const/4 v0, 0x0

    .line 1469
    iput-boolean v0, p0, Lcom/stagnationlab/sk/TransactionActivity;->z:Z

    return-void
.end method

.method private a(Lcom/stagnationlab/sk/TransactionActivity$b;)Landroid/view/View;
    .locals 1

    .line 1339
    sget-object v0, Lcom/stagnationlab/sk/TransactionActivity$21;->a:[I

    invoke-virtual {p1}, Lcom/stagnationlab/sk/TransactionActivity$b;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const/4 v0, 0x1

    if-eq p1, v0, :cond_3

    const/4 v0, 0x2

    if-eq p1, v0, :cond_2

    const/4 v0, 0x3

    if-eq p1, v0, :cond_1

    const/4 v0, 0x4

    if-eq p1, v0, :cond_0

    const/4 p1, 0x0

    return-object p1

    .line 1347
    :cond_0
    iget-object p1, p0, Lcom/stagnationlab/sk/TransactionActivity;->P:Landroid/widget/RelativeLayout;

    return-object p1

    .line 1345
    :cond_1
    iget-object p1, p0, Lcom/stagnationlab/sk/TransactionActivity;->O:Landroid/widget/RelativeLayout;

    return-object p1

    .line 1343
    :cond_2
    iget-object p1, p0, Lcom/stagnationlab/sk/TransactionActivity;->N:Landroid/widget/LinearLayout;

    return-object p1

    .line 1341
    :cond_3
    iget-object p1, p0, Lcom/stagnationlab/sk/TransactionActivity;->M:Landroid/widget/RelativeLayout;

    return-object p1
.end method

.method static synthetic a(Lcom/stagnationlab/sk/TransactionActivity;Lcom/stagnationlab/sk/a/a;)Lcom/stagnationlab/sk/a/a;
    .locals 0

    .line 68
    iput-object p1, p0, Lcom/stagnationlab/sk/TransactionActivity;->l:Lcom/stagnationlab/sk/a/a;

    return-object p1
.end method

.method private a(II)V
    .locals 8

    .line 990
    new-instance v0, Landroid/animation/AnimatorSet;

    invoke-direct {v0}, Landroid/animation/AnimatorSet;-><init>()V

    const/4 v1, 0x2

    .line 991
    div-int/2addr p1, v1

    int-to-long v2, p1

    invoke-virtual {v0, v2, v3}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    int-to-long p1, p2

    .line 992
    invoke-virtual {v0, p1, p2}, Landroid/animation/AnimatorSet;->setStartDelay(J)V

    const p1, 0x3f35c28f    # 0.71f

    const p2, 0x3e851eb8    # 0.26f

    const v2, 0x3e99999a    # 0.3f

    const v3, 0x3faccccd    # 1.35f

    .line 993
    invoke-static {p1, p2, v2, v3}, Landroid/support/v4/f/b/f;->a(FFFF)Landroid/view/animation/Interpolator;

    move-result-object p1

    invoke-virtual {v0, p1}, Landroid/animation/AnimatorSet;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 995
    new-array p1, v1, [Landroid/animation/Animator;

    iget-object p2, p0, Lcom/stagnationlab/sk/TransactionActivity;->M:Landroid/widget/RelativeLayout;

    new-array v2, v1, [Landroid/animation/PropertyValuesHolder;

    new-array v3, v1, [F

    fill-array-data v3, :array_0

    const-string v4, "scaleX"

    .line 998
    invoke-static {v4, v3}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v3

    const/4 v5, 0x0

    aput-object v3, v2, v5

    new-array v3, v1, [F

    fill-array-data v3, :array_1

    const-string v6, "scaleY"

    .line 999
    invoke-static {v6, v3}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v3

    const/4 v7, 0x1

    aput-object v3, v2, v7

    .line 996
    invoke-static {p2, v2}, Landroid/animation/ObjectAnimator;->ofPropertyValuesHolder(Ljava/lang/Object;[Landroid/animation/PropertyValuesHolder;)Landroid/animation/ObjectAnimator;

    move-result-object p2

    aput-object p2, p1, v5

    iget-object p2, p0, Lcom/stagnationlab/sk/TransactionActivity;->M:Landroid/widget/RelativeLayout;

    new-array v2, v1, [Landroid/animation/PropertyValuesHolder;

    new-array v3, v1, [F

    fill-array-data v3, :array_2

    .line 1003
    invoke-static {v4, v3}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v3

    aput-object v3, v2, v5

    new-array v1, v1, [F

    fill-array-data v1, :array_3

    .line 1004
    invoke-static {v6, v1}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v1

    aput-object v1, v2, v7

    .line 1001
    invoke-static {p2, v2}, Landroid/animation/ObjectAnimator;->ofPropertyValuesHolder(Ljava/lang/Object;[Landroid/animation/PropertyValuesHolder;)Landroid/animation/ObjectAnimator;

    move-result-object p2

    aput-object p2, p1, v7

    .line 995
    invoke-virtual {v0, p1}, Landroid/animation/AnimatorSet;->playSequentially([Landroid/animation/Animator;)V

    .line 1008
    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->start()V

    return-void

    nop

    :array_0
    .array-data 4
        0x3f800000    # 1.0f
        0x3fb33333    # 1.4f
    .end array-data

    :array_1
    .array-data 4
        0x3f800000    # 1.0f
        0x3fb33333    # 1.4f
    .end array-data

    :array_2
    .array-data 4
        0x3fb33333    # 1.4f
        0x3f800000    # 1.0f
    .end array-data

    :array_3
    .array-data 4
        0x3fb33333    # 1.4f
        0x3f800000    # 1.0f
    .end array-data
.end method

.method private a(Landroid/os/Bundle;)V
    .locals 6

    const v0, 0x7f0a001f

    .line 214
    invoke-virtual {p0, v0}, Lcom/stagnationlab/sk/TransactionActivity;->a(I)V

    const v0, 0x7f080075

    .line 216
    invoke-virtual {p0, v0}, Lcom/stagnationlab/sk/TransactionActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/stagnationlab/sk/TransactionActivity;->M:Landroid/widget/RelativeLayout;

    const v0, 0x7f080097

    .line 217
    invoke-virtual {p0, v0}, Lcom/stagnationlab/sk/TransactionActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/AppCompatImageView;

    iput-object v0, p0, Lcom/stagnationlab/sk/TransactionActivity;->F:Landroid/support/v7/widget/AppCompatImageView;

    const v0, 0x7f080096

    .line 218
    invoke-virtual {p0, v0}, Lcom/stagnationlab/sk/TransactionActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/stagnationlab/sk/TransactionActivity;->G:Landroid/widget/ImageView;

    const v0, 0x7f08009a

    .line 219
    invoke-virtual {p0, v0}, Lcom/stagnationlab/sk/TransactionActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/stagnationlab/sk/TransactionActivity;->H:Landroid/widget/TextView;

    const v0, 0x7f08009d

    .line 220
    invoke-virtual {p0, v0}, Lcom/stagnationlab/sk/TransactionActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/stagnationlab/sk/TransactionActivity;->I:Landroid/widget/TextView;

    const v0, 0x7f080095

    .line 221
    invoke-virtual {p0, v0}, Lcom/stagnationlab/sk/TransactionActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/stagnationlab/sk/TransactionActivity;->J:Landroid/widget/TextView;

    const v0, 0x7f08009c

    .line 222
    invoke-virtual {p0, v0}, Lcom/stagnationlab/sk/TransactionActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/stagnationlab/sk/TransactionActivity;->t:Landroid/widget/TextView;

    const v0, 0x7f0800d7

    .line 223
    invoke-virtual {p0, v0}, Lcom/stagnationlab/sk/TransactionActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/stagnationlab/sk/TransactionActivity;->u:Landroid/widget/TextView;

    const v0, 0x7f0800d6

    .line 224
    invoke-virtual {p0, v0}, Lcom/stagnationlab/sk/TransactionActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/stagnationlab/sk/TransactionActivity;->N:Landroid/widget/LinearLayout;

    const v0, 0x7f080099

    .line 225
    invoke-virtual {p0, v0}, Lcom/stagnationlab/sk/TransactionActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/stagnationlab/sk/TransactionActivity;->O:Landroid/widget/RelativeLayout;

    const v0, 0x7f0800c4

    .line 226
    invoke-virtual {p0, v0}, Lcom/stagnationlab/sk/TransactionActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/stagnationlab/sk/TransactionActivity;->P:Landroid/widget/RelativeLayout;

    const v0, 0x7f080086

    .line 227
    invoke-virtual {p0, v0}, Lcom/stagnationlab/sk/TransactionActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/stagnationlab/sk/elements/MaxWidthLinearLayout;

    iput-object v0, p0, Lcom/stagnationlab/sk/TransactionActivity;->L:Lcom/stagnationlab/sk/elements/MaxWidthLinearLayout;

    const v0, 0x7f080047

    .line 228
    invoke-virtual {p0, v0}, Lcom/stagnationlab/sk/TransactionActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/stagnationlab/sk/TransactionActivity;->R:Landroid/widget/ImageButton;

    const v0, 0x7f08009b

    .line 229
    invoke-virtual {p0, v0}, Lcom/stagnationlab/sk/TransactionActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/stagnationlab/sk/TransactionActivity;->T:Landroid/widget/TextView;

    const v0, 0x7f080094

    .line 230
    invoke-virtual {p0, v0}, Lcom/stagnationlab/sk/TransactionActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/stagnationlab/sk/TransactionActivity;->U:Landroid/widget/LinearLayout;

    const v0, 0x7f080056

    .line 231
    invoke-virtual {p0, v0}, Lcom/stagnationlab/sk/TransactionActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/stagnationlab/sk/elements/Modal;

    iput-object v0, p0, Lcom/stagnationlab/sk/TransactionActivity;->V:Lcom/stagnationlab/sk/elements/Modal;

    const v0, 0x7f080029

    .line 232
    invoke-virtual {p0, v0}, Lcom/stagnationlab/sk/TransactionActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/stagnationlab/sk/TransactionActivity;->v:Landroid/widget/Button;

    .line 233
    iget-object v0, p0, Lcom/stagnationlab/sk/TransactionActivity;->T:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/stagnationlab/sk/TransactionActivity;->a(Landroid/view/View;Z)V

    .line 234
    iget-object v0, p0, Lcom/stagnationlab/sk/TransactionActivity;->U:Landroid/widget/LinearLayout;

    invoke-direct {p0, v0, v1}, Lcom/stagnationlab/sk/TransactionActivity;->a(Landroid/view/View;Z)V

    const/4 v0, 0x6

    .line 235
    new-array v0, v0, [Landroid/view/View;

    iget-object v2, p0, Lcom/stagnationlab/sk/TransactionActivity;->H:Landroid/widget/TextView;

    aput-object v2, v0, v1

    iget-object v2, p0, Lcom/stagnationlab/sk/TransactionActivity;->I:Landroid/widget/TextView;

    const/4 v3, 0x1

    aput-object v2, v0, v3

    const v2, 0x7f080093

    .line 238
    invoke-virtual {p0, v2}, Lcom/stagnationlab/sk/TransactionActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    const/4 v4, 0x2

    aput-object v2, v0, v4

    iget-object v2, p0, Lcom/stagnationlab/sk/TransactionActivity;->t:Landroid/widget/TextView;

    const/4 v5, 0x3

    aput-object v2, v0, v5

    iget-object v2, p0, Lcom/stagnationlab/sk/TransactionActivity;->J:Landroid/widget/TextView;

    const/4 v5, 0x4

    aput-object v2, v0, v5

    iget-object v2, p0, Lcom/stagnationlab/sk/TransactionActivity;->v:Landroid/widget/Button;

    const/4 v5, 0x5

    aput-object v2, v0, v5

    iput-object v0, p0, Lcom/stagnationlab/sk/TransactionActivity;->W:[Landroid/view/View;

    .line 244
    invoke-virtual {p0}, Lcom/stagnationlab/sk/TransactionActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    const v2, 0x7f080088

    invoke-virtual {v0, v2}, Landroid/app/FragmentManager;->findFragmentById(I)Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/stagnationlab/sk/PinFragment;

    iput-object v0, p0, Lcom/stagnationlab/sk/TransactionActivity;->Q:Lcom/stagnationlab/sk/PinFragment;

    const v0, 0x7f08003f

    .line 246
    invoke-virtual {p0, v0}, Lcom/stagnationlab/sk/TransactionActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/stagnationlab/sk/TransactionActivity;->r:Landroid/widget/TextView;

    const v0, 0x7f08008a

    .line 247
    invoke-virtual {p0, v0}, Lcom/stagnationlab/sk/TransactionActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/stagnationlab/sk/TransactionActivity;->A:Landroid/widget/TextView;

    const v0, 0x7f080098

    .line 248
    invoke-virtual {p0, v0}, Lcom/stagnationlab/sk/TransactionActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/stagnationlab/sk/TransactionActivity;->s:Landroid/widget/TextView;

    const v0, 0x7f08004a

    .line 249
    invoke-virtual {p0, v0}, Lcom/stagnationlab/sk/TransactionActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/stagnationlab/sk/TransactionActivity;->p:Landroid/widget/TextView;

    const v0, 0x7f08005f

    .line 250
    invoke-virtual {p0, v0}, Lcom/stagnationlab/sk/TransactionActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/stagnationlab/sk/TransactionActivity;->q:Landroid/widget/TextView;

    const v0, 0x7f080032

    .line 252
    invoke-virtual {p0, v0}, Lcom/stagnationlab/sk/TransactionActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/stagnationlab/sk/TransactionActivity;->K:Landroid/widget/ImageView;

    .line 254
    new-array v0, v4, [F

    fill-array-data v0, :array_0

    const-string v2, "loaderRotation"

    invoke-static {v2, v0}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v0

    .line 255
    new-array v2, v3, [Landroid/animation/PropertyValuesHolder;

    aput-object v0, v2, v1

    invoke-static {p0, v2}, Landroid/animation/ObjectAnimator;->ofPropertyValuesHolder(Ljava/lang/Object;[Landroid/animation/PropertyValuesHolder;)Landroid/animation/ObjectAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/stagnationlab/sk/TransactionActivity;->X:Landroid/animation/ObjectAnimator;

    .line 256
    iget-object v0, p0, Lcom/stagnationlab/sk/TransactionActivity;->X:Landroid/animation/ObjectAnimator;

    sget v1, Lcom/stagnationlab/sk/TransactionActivity;->e:I

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->setRepeatCount(I)V

    .line 257
    iget-object v0, p0, Lcom/stagnationlab/sk/TransactionActivity;->X:Landroid/animation/ObjectAnimator;

    sget-object v1, Lcom/stagnationlab/sk/TransactionActivity;->d:Landroid/view/animation/Interpolator;

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 258
    iget-object v0, p0, Lcom/stagnationlab/sk/TransactionActivity;->X:Landroid/animation/ObjectAnimator;

    sget v1, Lcom/stagnationlab/sk/TransactionActivity;->c:I

    int-to-long v1, v1

    invoke-virtual {v0, v1, v2}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 260
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/stagnationlab/sk/TransactionActivity;->S:Ljava/util/List;

    .line 263
    new-instance v0, Lcom/stagnationlab/sk/TransactionActivity$12;

    invoke-direct {v0, p0}, Lcom/stagnationlab/sk/TransactionActivity$12;-><init>(Lcom/stagnationlab/sk/TransactionActivity;)V

    const v1, 0x7f080028

    .line 271
    invoke-virtual {p0, v1}, Lcom/stagnationlab/sk/TransactionActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 272
    iget-object v1, p0, Lcom/stagnationlab/sk/TransactionActivity;->v:Landroid/widget/Button;

    new-instance v2, Lcom/stagnationlab/sk/TransactionActivity$22;

    invoke-direct {v2, p0}, Lcom/stagnationlab/sk/TransactionActivity$22;-><init>(Lcom/stagnationlab/sk/TransactionActivity;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v1, 0x7f080069

    .line 278
    invoke-virtual {p0, v1}, Lcom/stagnationlab/sk/TransactionActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f08003c

    .line 279
    invoke-virtual {p0, v0}, Lcom/stagnationlab/sk/TransactionActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/stagnationlab/sk/TransactionActivity$23;

    invoke-direct {v1, p0}, Lcom/stagnationlab/sk/TransactionActivity$23;-><init>(Lcom/stagnationlab/sk/TransactionActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f080037

    .line 285
    invoke-virtual {p0, v0}, Lcom/stagnationlab/sk/TransactionActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/stagnationlab/sk/TransactionActivity$24;

    invoke-direct {v1, p0}, Lcom/stagnationlab/sk/TransactionActivity$24;-><init>(Lcom/stagnationlab/sk/TransactionActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 292
    invoke-direct {p0}, Lcom/stagnationlab/sk/TransactionActivity;->l()V

    .line 293
    invoke-direct {p0, p1}, Lcom/stagnationlab/sk/TransactionActivity;->b(Landroid/os/Bundle;)V

    return-void

    :array_0
    .array-data 4
        0x0
        0x43b40000    # 360.0f
    .end array-data
.end method

.method private a(Landroid/view/View;)V
    .locals 1

    const/4 v0, 0x1

    .line 1306
    invoke-direct {p0, p1, v0}, Lcom/stagnationlab/sk/TransactionActivity;->a(Landroid/view/View;Z)V

    return-void
.end method

.method private a(Landroid/view/View;Z)V
    .locals 1

    const/4 v0, 0x0

    .line 1310
    invoke-direct {p0, p1, v0, p2}, Lcom/stagnationlab/sk/TransactionActivity;->a(Landroid/view/View;ZZ)V

    return-void
.end method

.method private a(Landroid/view/View;ZZ)V
    .locals 1

    const/4 v0, 0x0

    .line 1322
    invoke-direct {p0, p1, p2, p3, v0}, Lcom/stagnationlab/sk/TransactionActivity;->a(Landroid/view/View;ZZI)V

    return-void
.end method

.method private a(Landroid/view/View;ZZI)V
    .locals 3

    if-eqz p2, :cond_0

    const/high16 v0, 0x3f800000    # 1.0f

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz p2, :cond_1

    const/4 v1, 0x0

    .line 1328
    invoke-virtual {p1, v1}, Landroid/view/View;->setVisibility(I)V

    :cond_1
    if-eqz p3, :cond_2

    .line 1331
    invoke-virtual {p1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object p3

    int-to-long v1, p4

    invoke-virtual {p3, v1, v2}, Landroid/view/ViewPropertyAnimator;->setStartDelay(J)Landroid/view/ViewPropertyAnimator;

    move-result-object p3

    sget p4, Lcom/stagnationlab/sk/TransactionActivity;->f:I

    int-to-long v1, p4

    invoke-virtual {p3, v1, v2}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object p3

    invoke-virtual {p3, v0}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    goto :goto_1

    .line 1333
    :cond_2
    invoke-virtual {p1, v0}, Landroid/view/View;->setAlpha(F)V

    .line 1335
    :goto_1
    invoke-direct {p0, p1, p2}, Lcom/stagnationlab/sk/TransactionActivity;->c(Landroid/view/View;Z)V

    return-void
.end method

.method private a(Lcom/stagnationlab/sk/TransactionActivity$a;)V
    .locals 3

    .line 810
    iget-object v0, p0, Lcom/stagnationlab/sk/TransactionActivity;->n:Lcom/stagnationlab/sk/models/Transaction;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/stagnationlab/sk/models/Transaction;->k()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/stagnationlab/sk/TransactionActivity;->o:Lcom/stagnationlab/sk/models/RpRequest;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/stagnationlab/sk/models/RpRequest;->j()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    .line 812
    :goto_0
    new-instance v1, Lcom/stagnationlab/sk/util/m;

    iget-object v2, p0, Lcom/stagnationlab/sk/TransactionActivity;->l:Lcom/stagnationlab/sk/a/a;

    invoke-direct {v1, v2, v0}, Lcom/stagnationlab/sk/util/m;-><init>(Lcom/stagnationlab/sk/a/a;Z)V

    invoke-direct {p0, v1, p1}, Lcom/stagnationlab/sk/TransactionActivity;->a(Lcom/stagnationlab/sk/util/m;Lcom/stagnationlab/sk/TransactionActivity$a;)V

    return-void
.end method

.method static synthetic a(Lcom/stagnationlab/sk/TransactionActivity;I)V
    .locals 0

    .line 68
    invoke-direct {p0, p1}, Lcom/stagnationlab/sk/TransactionActivity;->b(I)V

    return-void
.end method

.method static synthetic a(Lcom/stagnationlab/sk/TransactionActivity;Landroid/os/Bundle;)V
    .locals 0

    .line 68
    invoke-direct {p0, p1}, Lcom/stagnationlab/sk/TransactionActivity;->a(Landroid/os/Bundle;)V

    return-void
.end method

.method static synthetic a(Lcom/stagnationlab/sk/TransactionActivity;Lcom/stagnationlab/sk/TransactionActivity$a;)V
    .locals 0

    .line 68
    invoke-direct {p0, p1}, Lcom/stagnationlab/sk/TransactionActivity;->a(Lcom/stagnationlab/sk/TransactionActivity$a;)V

    return-void
.end method

.method static synthetic a(Lcom/stagnationlab/sk/TransactionActivity;Lcom/stagnationlab/sk/models/Transaction;Lcom/stagnationlab/sk/models/RpRequest;Z)V
    .locals 0

    .line 68
    invoke-direct {p0, p1, p2, p3}, Lcom/stagnationlab/sk/TransactionActivity;->a(Lcom/stagnationlab/sk/models/Transaction;Lcom/stagnationlab/sk/models/RpRequest;Z)V

    return-void
.end method

.method static synthetic a(Lcom/stagnationlab/sk/TransactionActivity;Lcom/stagnationlab/sk/models/Transaction;Z)V
    .locals 0

    .line 68
    invoke-direct {p0, p1, p2}, Lcom/stagnationlab/sk/TransactionActivity;->a(Lcom/stagnationlab/sk/models/Transaction;Z)V

    return-void
.end method

.method static synthetic a(Lcom/stagnationlab/sk/TransactionActivity;Lcom/stagnationlab/sk/util/m;Lcom/stagnationlab/sk/TransactionActivity$a;)V
    .locals 0

    .line 68
    invoke-direct {p0, p1, p2}, Lcom/stagnationlab/sk/TransactionActivity;->a(Lcom/stagnationlab/sk/util/m;Lcom/stagnationlab/sk/TransactionActivity$a;)V

    return-void
.end method

.method static synthetic a(Lcom/stagnationlab/sk/TransactionActivity;Ljava/lang/String;)V
    .locals 0

    .line 68
    invoke-direct {p0, p1}, Lcom/stagnationlab/sk/TransactionActivity;->b(Ljava/lang/String;)V

    return-void
.end method

.method private a(Lcom/stagnationlab/sk/g$a;)V
    .locals 2

    const/4 v0, 0x0

    .line 908
    :goto_0
    iget v1, p0, Lcom/stagnationlab/sk/TransactionActivity;->i:I

    if-ge v0, v1, :cond_0

    .line 909
    iget-object v1, p0, Lcom/stagnationlab/sk/TransactionActivity;->S:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/stagnationlab/sk/g;

    invoke-virtual {v1, p1}, Lcom/stagnationlab/sk/g;->a(Lcom/stagnationlab/sk/g$a;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method private a(Lcom/stagnationlab/sk/models/RpRequest;)V
    .locals 4

    .line 782
    iget-object v0, p0, Lcom/stagnationlab/sk/TransactionActivity;->o:Lcom/stagnationlab/sk/models/RpRequest;

    invoke-virtual {p1, v0}, Lcom/stagnationlab/sk/models/RpRequest;->a(Lcom/stagnationlab/sk/models/RpRequest;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 786
    :cond_0
    iput-object p1, p0, Lcom/stagnationlab/sk/TransactionActivity;->o:Lcom/stagnationlab/sk/models/RpRequest;

    const/4 v0, 0x0

    .line 787
    iput-object v0, p0, Lcom/stagnationlab/sk/TransactionActivity;->n:Lcom/stagnationlab/sk/models/Transaction;

    .line 789
    iget-object v0, p0, Lcom/stagnationlab/sk/TransactionActivity;->J:Landroid/widget/TextView;

    const v1, 0x7f0e010d

    invoke-virtual {p0, v1}, Lcom/stagnationlab/sk/TransactionActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v0, v1}, Lcom/stagnationlab/sk/util/k;->a(Landroid/content/Context;Landroid/widget/TextView;Ljava/lang/String;)V

    .line 790
    iget-object v0, p0, Lcom/stagnationlab/sk/TransactionActivity;->I:Landroid/widget/TextView;

    .line 791
    invoke-virtual {p1}, Lcom/stagnationlab/sk/models/RpRequest;->e()Z

    move-result v1

    if-eqz v1, :cond_1

    const v1, 0x7f0e010b

    goto :goto_0

    :cond_1
    const v1, 0x7f0e010c

    :goto_0
    invoke-virtual {p0, v1}, Lcom/stagnationlab/sk/TransactionActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 790
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 794
    iget-object v0, p0, Lcom/stagnationlab/sk/TransactionActivity;->H:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/stagnationlab/sk/models/RpRequest;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/4 v0, 0x1

    .line 796
    iput-boolean v0, p0, Lcom/stagnationlab/sk/TransactionActivity;->y:Z

    .line 797
    iget-object v0, p0, Lcom/stagnationlab/sk/TransactionActivity;->t:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/stagnationlab/sk/models/RpRequest;->b()J

    move-result-wide v1

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Lcom/stagnationlab/sk/util/l;->a(JZ)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 798
    invoke-direct {p0}, Lcom/stagnationlab/sk/TransactionActivity;->y()V

    .line 800
    invoke-direct {p0}, Lcom/stagnationlab/sk/TransactionActivity;->w()V

    .line 802
    iget-object v0, p0, Lcom/stagnationlab/sk/TransactionActivity;->m:Lcom/stagnationlab/sk/a;

    invoke-virtual {p1}, Lcom/stagnationlab/sk/models/RpRequest;->e()Z

    move-result v1

    if-eqz v1, :cond_2

    const-string v1, "Authentication confirm"

    goto :goto_1

    :cond_2
    const-string v1, "Signature confirm"

    :goto_1
    invoke-virtual {v0, v1}, Lcom/stagnationlab/sk/a;->a(Ljava/lang/String;)V

    .line 804
    iget-object v0, p0, Lcom/stagnationlab/sk/TransactionActivity;->v:Landroid/widget/Button;

    .line 805
    invoke-virtual {p1}, Lcom/stagnationlab/sk/models/RpRequest;->e()Z

    move-result p1

    if-eqz p1, :cond_3

    const p1, 0x7f0e002c

    goto :goto_2

    :cond_3
    const p1, 0x7f0e002d

    :goto_2
    invoke-virtual {p0, p1}, Lcom/stagnationlab/sk/TransactionActivity;->getString(I)Ljava/lang/String;

    move-result-object p1

    .line 804
    invoke-virtual {v0, p1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method private a(Lcom/stagnationlab/sk/models/Transaction;Lcom/stagnationlab/sk/models/RpRequest;Z)V
    .locals 0

    if-eqz p1, :cond_1

    .line 365
    invoke-virtual {p1}, Lcom/stagnationlab/sk/models/Transaction;->d()Z

    move-result p2

    if-nez p2, :cond_0

    .line 366
    new-instance p1, Lcom/stagnationlab/sk/c/a;

    sget-object p2, Lcom/stagnationlab/sk/c/b;->i:Lcom/stagnationlab/sk/c/b;

    invoke-direct {p1, p2}, Lcom/stagnationlab/sk/c/a;-><init>(Lcom/stagnationlab/sk/c/b;)V

    invoke-direct {p0, p1}, Lcom/stagnationlab/sk/TransactionActivity;->b(Lcom/stagnationlab/sk/c/a;)Z

    return-void

    .line 371
    :cond_0
    invoke-virtual {p1}, Lcom/stagnationlab/sk/models/Transaction;->l()Z

    move-result p2

    invoke-direct {p0, p2}, Lcom/stagnationlab/sk/TransactionActivity;->b(Z)V

    .line 372
    invoke-direct {p0, p1, p3}, Lcom/stagnationlab/sk/TransactionActivity;->a(Lcom/stagnationlab/sk/models/Transaction;Z)V

    return-void

    :cond_1
    if-eqz p2, :cond_3

    .line 378
    invoke-virtual {p2}, Lcom/stagnationlab/sk/models/RpRequest;->d()Z

    move-result p1

    if-nez p1, :cond_2

    .line 379
    new-instance p1, Lcom/stagnationlab/sk/c/a;

    sget-object p2, Lcom/stagnationlab/sk/c/b;->i:Lcom/stagnationlab/sk/c/b;

    invoke-direct {p1, p2}, Lcom/stagnationlab/sk/c/a;-><init>(Lcom/stagnationlab/sk/c/b;)V

    invoke-direct {p0, p1}, Lcom/stagnationlab/sk/TransactionActivity;->b(Lcom/stagnationlab/sk/c/a;)Z

    return-void

    .line 384
    :cond_2
    invoke-virtual {p2}, Lcom/stagnationlab/sk/models/RpRequest;->e()Z

    move-result p1

    invoke-direct {p0, p1}, Lcom/stagnationlab/sk/TransactionActivity;->b(Z)V

    .line 385
    invoke-direct {p0, p2}, Lcom/stagnationlab/sk/TransactionActivity;->a(Lcom/stagnationlab/sk/models/RpRequest;)V

    return-void

    .line 392
    :cond_3
    new-instance p1, Lcom/stagnationlab/sk/c/a;

    sget-object p2, Lcom/stagnationlab/sk/c/b;->g:Lcom/stagnationlab/sk/c/b;

    invoke-direct {p1, p2}, Lcom/stagnationlab/sk/c/a;-><init>(Lcom/stagnationlab/sk/c/b;)V

    invoke-direct {p0, p1}, Lcom/stagnationlab/sk/TransactionActivity;->b(Lcom/stagnationlab/sk/c/a;)Z

    return-void
.end method

.method private a(Lcom/stagnationlab/sk/models/Transaction;Z)V
    .locals 2

    .line 537
    iget-object v0, p0, Lcom/stagnationlab/sk/TransactionActivity;->n:Lcom/stagnationlab/sk/models/Transaction;

    invoke-virtual {p1, v0}, Lcom/stagnationlab/sk/models/Transaction;->a(Lcom/stagnationlab/sk/models/Transaction;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 541
    :cond_0
    iget-object v0, p0, Lcom/stagnationlab/sk/TransactionActivity;->m:Lcom/stagnationlab/sk/a;

    invoke-virtual {p1}, Lcom/stagnationlab/sk/models/Transaction;->l()Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "Authentication transaction"

    goto :goto_0

    :cond_1
    const-string v1, "Signature transaction"

    :goto_0
    invoke-virtual {v0, v1}, Lcom/stagnationlab/sk/a;->a(Ljava/lang/String;)V

    .line 543
    iput-object p1, p0, Lcom/stagnationlab/sk/TransactionActivity;->n:Lcom/stagnationlab/sk/models/Transaction;

    const/4 v0, 0x0

    .line 544
    iput-boolean v0, p0, Lcom/stagnationlab/sk/TransactionActivity;->E:Z

    const/4 v0, 0x0

    .line 545
    iput-object v0, p0, Lcom/stagnationlab/sk/TransactionActivity;->o:Lcom/stagnationlab/sk/models/RpRequest;

    .line 546
    invoke-direct {p0}, Lcom/stagnationlab/sk/TransactionActivity;->s()V

    if-eqz p2, :cond_2

    .line 549
    iget-object p2, p0, Lcom/stagnationlab/sk/TransactionActivity;->l:Lcom/stagnationlab/sk/a/a;

    new-instance v0, Lcom/stagnationlab/sk/TransactionActivity$28;

    invoke-direct {v0, p0}, Lcom/stagnationlab/sk/TransactionActivity$28;-><init>(Lcom/stagnationlab/sk/TransactionActivity;)V

    invoke-virtual {p2, p1, v0}, Lcom/stagnationlab/sk/a/a;->a(Lcom/stagnationlab/sk/models/Transaction;Lcom/stagnationlab/sk/a/a/v;)V

    goto :goto_1

    .line 570
    :cond_2
    invoke-direct {p0}, Lcom/stagnationlab/sk/TransactionActivity;->m()V

    :goto_1
    return-void
.end method

.method private a(Lcom/stagnationlab/sk/util/m;Lcom/stagnationlab/sk/TransactionActivity$a;)V
    .locals 1

    .line 816
    invoke-virtual {p1}, Lcom/stagnationlab/sk/util/m;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 817
    invoke-direct {p0}, Lcom/stagnationlab/sk/TransactionActivity;->s()V

    .line 820
    :cond_0
    new-instance v0, Lcom/stagnationlab/sk/TransactionActivity$7;

    invoke-direct {v0, p0, p2}, Lcom/stagnationlab/sk/TransactionActivity$7;-><init>(Lcom/stagnationlab/sk/TransactionActivity;Lcom/stagnationlab/sk/TransactionActivity$a;)V

    invoke-virtual {p1, v0}, Lcom/stagnationlab/sk/util/m;->a(Lcom/stagnationlab/sk/a/a/o;)V

    return-void
.end method

.method private a(Z)V
    .locals 2

    .line 1211
    iget-object v0, p0, Lcom/stagnationlab/sk/TransactionActivity;->M:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    if-nez p1, :cond_0

    .line 1215
    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->cancel()V

    .line 1218
    :cond_0
    iget-object v1, p0, Lcom/stagnationlab/sk/TransactionActivity;->M:Landroid/widget/RelativeLayout;

    invoke-direct {p0, v1, p1}, Lcom/stagnationlab/sk/TransactionActivity;->a(Landroid/view/View;Z)V

    if-eqz p1, :cond_1

    .line 1221
    new-instance p1, Lcom/stagnationlab/sk/TransactionActivity$17;

    invoke-direct {p1, p0}, Lcom/stagnationlab/sk/TransactionActivity$17;-><init>(Lcom/stagnationlab/sk/TransactionActivity;)V

    invoke-virtual {v0, p1}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    goto :goto_0

    .line 1243
    :cond_1
    iget-object p1, p0, Lcom/stagnationlab/sk/TransactionActivity;->X:Landroid/animation/ObjectAnimator;

    invoke-virtual {p1}, Landroid/animation/ObjectAnimator;->cancel()V

    :goto_0
    return-void
.end method

.method private a(ZLcom/stagnationlab/sk/components/SelectControlCode;)V
    .locals 2

    .line 1253
    iget-object v0, p0, Lcom/stagnationlab/sk/TransactionActivity;->N:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1255
    iget-object v0, p0, Lcom/stagnationlab/sk/TransactionActivity;->h:Lcom/stagnationlab/sk/TransactionActivity$b;

    sget-object v1, Lcom/stagnationlab/sk/TransactionActivity$b;->a:Lcom/stagnationlab/sk/TransactionActivity$b;

    if-ne v0, v1, :cond_0

    .line 1256
    invoke-direct {p0, p1}, Lcom/stagnationlab/sk/TransactionActivity;->a(Z)V

    .line 1257
    iget-object p2, p0, Lcom/stagnationlab/sk/TransactionActivity;->N:Landroid/widget/LinearLayout;

    invoke-direct {p0, p2, p1}, Lcom/stagnationlab/sk/TransactionActivity;->b(Landroid/view/View;Z)V

    goto :goto_0

    :cond_0
    const p1, 0x7f01000d

    .line 1260
    invoke-static {p0, p1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object p1

    .line 1261
    iget-object v0, p0, Lcom/stagnationlab/sk/TransactionActivity;->O:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, p1}, Landroid/widget/RelativeLayout;->startAnimation(Landroid/view/animation/Animation;)V

    const v0, 0x7f01000e

    .line 1263
    invoke-static {p0, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 1264
    iget-object v1, p0, Lcom/stagnationlab/sk/TransactionActivity;->N:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->startAnimation(Landroid/view/animation/Animation;)V

    if-eqz p2, :cond_1

    .line 1267
    invoke-virtual {p2, v0}, Lcom/stagnationlab/sk/components/SelectControlCode;->startAnimation(Landroid/view/animation/Animation;)V

    .line 1270
    :cond_1
    new-instance p2, Lcom/stagnationlab/sk/TransactionActivity$18;

    invoke-direct {p2, p0}, Lcom/stagnationlab/sk/TransactionActivity$18;-><init>(Lcom/stagnationlab/sk/TransactionActivity;)V

    invoke-virtual {p1, p2}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 1289
    :goto_0
    sget-object p1, Lcom/stagnationlab/sk/TransactionActivity$b;->b:Lcom/stagnationlab/sk/TransactionActivity$b;

    iput-object p1, p0, Lcom/stagnationlab/sk/TransactionActivity;->h:Lcom/stagnationlab/sk/TransactionActivity$b;

    return-void
.end method

.method static synthetic a(Lcom/stagnationlab/sk/TransactionActivity;)Z
    .locals 0

    .line 68
    iget-boolean p0, p0, Lcom/stagnationlab/sk/TransactionActivity;->ad:Z

    return p0
.end method

.method static synthetic a(Lcom/stagnationlab/sk/TransactionActivity;Lcom/stagnationlab/sk/c/a;)Z
    .locals 0

    .line 68
    invoke-direct {p0, p1}, Lcom/stagnationlab/sk/TransactionActivity;->b(Lcom/stagnationlab/sk/c/a;)Z

    move-result p0

    return p0
.end method

.method static synthetic a(Lcom/stagnationlab/sk/TransactionActivity;Z)Z
    .locals 0

    .line 68
    iput-boolean p1, p0, Lcom/stagnationlab/sk/TransactionActivity;->af:Z

    return p1
.end method

.method private a(Lcom/stagnationlab/sk/c/a;)Z
    .locals 1

    .line 698
    invoke-virtual {p1}, Lcom/stagnationlab/sk/c/a;->a()Lcom/stagnationlab/sk/c/b;

    move-result-object p1

    sget-object v0, Lcom/stagnationlab/sk/c/b;->l:Lcom/stagnationlab/sk/c/b;

    if-eq p1, v0, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method static synthetic b(Lcom/stagnationlab/sk/TransactionActivity;I)I
    .locals 0

    .line 68
    iput p1, p0, Lcom/stagnationlab/sk/TransactionActivity;->k:I

    return p1
.end method

.method private b(I)V
    .locals 4

    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 900
    :goto_0
    iget v2, p0, Lcom/stagnationlab/sk/TransactionActivity;->i:I

    if-ge v1, v2, :cond_1

    .line 901
    iget-object v2, p0, Lcom/stagnationlab/sk/TransactionActivity;->S:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/stagnationlab/sk/g;

    if-ge v1, p1, :cond_0

    sget-object v3, Lcom/stagnationlab/sk/g$a;->b:Lcom/stagnationlab/sk/g$a;

    goto :goto_1

    :cond_0
    sget-object v3, Lcom/stagnationlab/sk/g$a;->a:Lcom/stagnationlab/sk/g$a;

    :goto_1
    invoke-virtual {v2, v3}, Lcom/stagnationlab/sk/g;->a(Lcom/stagnationlab/sk/g$a;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 904
    :cond_1
    iget-object v1, p0, Lcom/stagnationlab/sk/TransactionActivity;->R:Landroid/widget/ImageButton;

    if-nez p1, :cond_2

    const/4 v0, 0x4

    :cond_2
    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setVisibility(I)V

    return-void
.end method

.method private b(Landroid/os/Bundle;)V
    .locals 5

    .line 297
    invoke-virtual {p0}, Lcom/stagnationlab/sk/TransactionActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 298
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Intent: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "TransactionActivity"

    invoke-static {v2, v1}, Lcom/stagnationlab/sk/f;->d(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 300
    iput-boolean v1, p0, Lcom/stagnationlab/sk/TransactionActivity;->D:Z

    const-string v2, "rpRequest"

    const-string v3, "transaction"

    if-eqz p1, :cond_1

    .line 303
    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v4

    check-cast v4, Lcom/stagnationlab/sk/models/Transaction;

    .line 304
    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object p1

    check-cast p1, Lcom/stagnationlab/sk/models/RpRequest;

    if-nez v4, :cond_0

    if-eqz p1, :cond_1

    .line 307
    :cond_0
    invoke-direct {p0, v4, p1, v1}, Lcom/stagnationlab/sk/TransactionActivity;->a(Lcom/stagnationlab/sk/models/Transaction;Lcom/stagnationlab/sk/models/RpRequest;Z)V

    return-void

    :cond_1
    const-string p1, "isFake"

    .line 313
    invoke-virtual {v0, p1, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result p1

    .line 315
    invoke-static {p0, v0}, Lcom/stagnationlab/sk/fcm/ClearNotificationReceiver;->a(Landroid/content/Context;Landroid/content/Intent;)V

    if-eqz p1, :cond_2

    .line 319
    new-instance p1, Lcom/stagnationlab/sk/models/RpRequest;

    invoke-direct {p1}, Lcom/stagnationlab/sk/models/RpRequest;-><init>()V

    invoke-direct {p0, p1}, Lcom/stagnationlab/sk/TransactionActivity;->a(Lcom/stagnationlab/sk/models/RpRequest;)V

    return-void

    :cond_2
    const-string p1, "pushMessage"

    .line 323
    invoke-virtual {v0, p1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object p1

    check-cast p1, Lcom/stagnationlab/sk/models/PushMessage;

    if-eqz p1, :cond_3

    .line 326
    iget-object v0, p0, Lcom/stagnationlab/sk/TransactionActivity;->l:Lcom/stagnationlab/sk/a/a;

    .line 327
    invoke-virtual {p1}, Lcom/stagnationlab/sk/models/PushMessage;->a()Ljava/lang/String;

    move-result-object v1

    .line 328
    invoke-virtual {p1}, Lcom/stagnationlab/sk/models/PushMessage;->c()Ljava/lang/String;

    move-result-object v2

    .line 329
    invoke-virtual {p1}, Lcom/stagnationlab/sk/models/PushMessage;->b()Ljava/lang/String;

    move-result-object p1

    new-instance v3, Lcom/stagnationlab/sk/TransactionActivity$25;

    invoke-direct {v3, p0}, Lcom/stagnationlab/sk/TransactionActivity$25;-><init>(Lcom/stagnationlab/sk/TransactionActivity;)V

    .line 326
    invoke-virtual {v0, v1, v2, p1, v3}, Lcom/stagnationlab/sk/a/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/stagnationlab/sk/a/a/o;)V

    return-void

    :cond_3
    const-string p1, "manual"

    .line 346
    invoke-virtual {v0, p1, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result p1

    iput-boolean p1, p0, Lcom/stagnationlab/sk/TransactionActivity;->ae:Z

    const-string p1, "metaData"

    .line 347
    invoke-virtual {v0, p1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object p1

    check-cast p1, Lcom/stagnationlab/sk/models/TransactionMetaData;

    if-eqz p1, :cond_4

    .line 352
    invoke-virtual {p1}, Lcom/stagnationlab/sk/models/TransactionMetaData;->b()Z

    move-result v1

    .line 353
    invoke-virtual {p1}, Lcom/stagnationlab/sk/models/TransactionMetaData;->a()I

    move-result p1

    iput p1, p0, Lcom/stagnationlab/sk/TransactionActivity;->k:I

    .line 357
    :cond_4
    invoke-virtual {v0, v3}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object p1

    check-cast p1, Lcom/stagnationlab/sk/models/Transaction;

    .line 358
    invoke-virtual {v0, v2}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/stagnationlab/sk/models/RpRequest;

    .line 356
    invoke-direct {p0, p1, v0, v1}, Lcom/stagnationlab/sk/TransactionActivity;->a(Lcom/stagnationlab/sk/models/Transaction;Lcom/stagnationlab/sk/models/RpRequest;Z)V

    return-void
.end method

.method private b(Landroid/view/View;)V
    .locals 1

    const/4 v0, 0x1

    .line 1314
    invoke-direct {p0, p1, v0}, Lcom/stagnationlab/sk/TransactionActivity;->b(Landroid/view/View;Z)V

    return-void
.end method

.method private b(Landroid/view/View;Z)V
    .locals 1

    const/4 v0, 0x1

    .line 1318
    invoke-direct {p0, p1, v0, p2}, Lcom/stagnationlab/sk/TransactionActivity;->a(Landroid/view/View;ZZ)V

    return-void
.end method

.method private b(Ljava/lang/String;)V
    .locals 6

    .line 667
    iget-object v0, p0, Lcom/stagnationlab/sk/TransactionActivity;->A:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getLineHeight()I

    move-result v0

    int-to-float v0, v0

    .line 669
    iget-boolean v1, p0, Lcom/stagnationlab/sk/TransactionActivity;->C:Z

    if-nez v1, :cond_0

    if-nez p1, :cond_0

    return-void

    :cond_0
    const/high16 v1, 0x3f800000    # 1.0f

    const/4 v2, 0x0

    if-nez p1, :cond_1

    .line 676
    iget-object p1, p0, Lcom/stagnationlab/sk/TransactionActivity;->q:Landroid/widget/TextView;

    neg-float v3, v0

    invoke-virtual {p1, v3}, Landroid/widget/TextView;->setTranslationY(F)V

    .line 677
    iget-object p1, p0, Lcom/stagnationlab/sk/TransactionActivity;->q:Landroid/widget/TextView;

    invoke-virtual {p1}, Landroid/widget/TextView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object p1

    sget v3, Lcom/stagnationlab/sk/TransactionActivity;->f:I

    int-to-long v3, v3

    invoke-virtual {p1, v3, v4}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object p1

    invoke-virtual {p1, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object p1

    invoke-virtual {p1, v2}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    .line 678
    iget-object p1, p0, Lcom/stagnationlab/sk/TransactionActivity;->A:Landroid/widget/TextView;

    invoke-virtual {p1}, Landroid/widget/TextView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object p1

    sget v1, Lcom/stagnationlab/sk/TransactionActivity;->f:I

    int-to-long v3, v1

    invoke-virtual {p1, v3, v4}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object p1

    invoke-virtual {p1, v2}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object p1

    invoke-virtual {p1, v0}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    const/4 p1, 0x0

    .line 679
    iput-boolean p1, p0, Lcom/stagnationlab/sk/TransactionActivity;->C:Z

    return-void

    .line 683
    :cond_1
    iget-boolean v3, p0, Lcom/stagnationlab/sk/TransactionActivity;->C:Z

    if-nez v3, :cond_2

    .line 685
    iget-object v3, p0, Lcom/stagnationlab/sk/TransactionActivity;->q:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v3

    sget v4, Lcom/stagnationlab/sk/TransactionActivity;->f:I

    int-to-long v4, v4

    invoke-virtual {v3, v4, v5}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    .line 688
    :cond_2
    iget-object v3, p0, Lcom/stagnationlab/sk/TransactionActivity;->A:Landroid/widget/TextView;

    invoke-virtual {v3, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 689
    iget-object p1, p0, Lcom/stagnationlab/sk/TransactionActivity;->A:Landroid/widget/TextView;

    neg-float v0, v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setTranslationY(F)V

    .line 690
    iget-object p1, p0, Lcom/stagnationlab/sk/TransactionActivity;->A:Landroid/widget/TextView;

    invoke-virtual {p1, v2}, Landroid/widget/TextView;->setAlpha(F)V

    .line 691
    iget-object p1, p0, Lcom/stagnationlab/sk/TransactionActivity;->A:Landroid/widget/TextView;

    invoke-virtual {p1}, Landroid/widget/TextView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object p1

    sget v0, Lcom/stagnationlab/sk/TransactionActivity;->f:I

    int-to-long v3, v0

    invoke-virtual {p1, v3, v4}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object p1

    invoke-virtual {p1, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object p1

    invoke-virtual {p1, v2}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    const/4 p1, 0x1

    .line 692
    iput-boolean p1, p0, Lcom/stagnationlab/sk/TransactionActivity;->C:Z

    .line 694
    sget-object p1, Lcom/stagnationlab/sk/g$a;->c:Lcom/stagnationlab/sk/g$a;

    invoke-direct {p0, p1}, Lcom/stagnationlab/sk/TransactionActivity;->a(Lcom/stagnationlab/sk/g$a;)V

    return-void
.end method

.method private b(Z)V
    .locals 1

    const-string v0, "TransactionActivity"

    if-eqz p1, :cond_0

    const-string p1, "authentication started"

    .line 1361
    invoke-static {v0, p1}, Lcom/stagnationlab/sk/f;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    const-string p1, "signing started"

    .line 1363
    invoke-static {v0, p1}, Lcom/stagnationlab/sk/f;->a(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void
.end method

.method static synthetic b(Lcom/stagnationlab/sk/TransactionActivity;)Z
    .locals 0

    .line 68
    iget-boolean p0, p0, Lcom/stagnationlab/sk/TransactionActivity;->ag:Z

    return p0
.end method

.method static synthetic b(Lcom/stagnationlab/sk/TransactionActivity;Lcom/stagnationlab/sk/c/a;)Z
    .locals 0

    .line 68
    invoke-direct {p0, p1}, Lcom/stagnationlab/sk/TransactionActivity;->a(Lcom/stagnationlab/sk/c/a;)Z

    move-result p0

    return p0
.end method

.method static synthetic b(Lcom/stagnationlab/sk/TransactionActivity;Z)Z
    .locals 0

    .line 68
    iput-boolean p1, p0, Lcom/stagnationlab/sk/TransactionActivity;->ag:Z

    return p1
.end method

.method private b(Lcom/stagnationlab/sk/c/a;)Z
    .locals 5

    .line 702
    invoke-virtual {p1}, Lcom/stagnationlab/sk/c/a;->a()Lcom/stagnationlab/sk/c/b;

    move-result-object v0

    sget-object v1, Lcom/stagnationlab/sk/c/b;->l:Lcom/stagnationlab/sk/c/b;

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-ne v0, v1, :cond_0

    const v0, 0x7f0e00bc

    .line 703
    invoke-virtual {p0, v0}, Lcom/stagnationlab/sk/TransactionActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-array v1, v3, [Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/stagnationlab/sk/c/a;->c()I

    move-result p1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/stagnationlab/sk/TransactionActivity;->b(Ljava/lang/String;)V

    return v3

    .line 707
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/stagnationlab/sk/ErrorActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 708
    iget-object v1, p0, Lcom/stagnationlab/sk/TransactionActivity;->n:Lcom/stagnationlab/sk/models/Transaction;

    const-string v4, "transaction"

    invoke-virtual {v0, v4, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 709
    iget-object v1, p0, Lcom/stagnationlab/sk/TransactionActivity;->o:Lcom/stagnationlab/sk/models/RpRequest;

    const-string v4, "rpRequest"

    invoke-virtual {v0, v4, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v1, "error"

    .line 710
    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 711
    invoke-virtual {p0}, Lcom/stagnationlab/sk/TransactionActivity;->getIntent()Landroid/content/Intent;

    move-result-object p1

    const-string v1, "transactionUuid"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 712
    new-instance p1, Lcom/stagnationlab/sk/models/TransactionMetaData;

    iget v1, p0, Lcom/stagnationlab/sk/TransactionActivity;->k:I

    invoke-direct {p1, v3, v1}, Lcom/stagnationlab/sk/models/TransactionMetaData;-><init>(ZI)V

    const-string v1, "metaData"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 714
    invoke-virtual {p0, v0}, Lcom/stagnationlab/sk/TransactionActivity;->startActivity(Landroid/content/Intent;)V

    .line 715
    invoke-static {p0}, Lcom/stagnationlab/sk/util/n;->a(Landroid/app/Activity;)V

    .line 716
    invoke-virtual {p0}, Lcom/stagnationlab/sk/TransactionActivity;->finish()V

    return v2
.end method

.method static synthetic c(Lcom/stagnationlab/sk/TransactionActivity;Lcom/stagnationlab/sk/c/a;)Lcom/stagnationlab/sk/c/a;
    .locals 0

    .line 68
    iput-object p1, p0, Lcom/stagnationlab/sk/TransactionActivity;->ac:Lcom/stagnationlab/sk/c/a;

    return-object p1
.end method

.method private c(I)V
    .locals 3

    .line 1112
    iget-object v0, p0, Lcom/stagnationlab/sk/TransactionActivity;->h:Lcom/stagnationlab/sk/TransactionActivity$b;

    sget-object v1, Lcom/stagnationlab/sk/TransactionActivity$b;->c:Lcom/stagnationlab/sk/TransactionActivity$b;

    if-ne v0, v1, :cond_0

    .line 1114
    invoke-direct {p0}, Lcom/stagnationlab/sk/TransactionActivity;->t()V

    goto :goto_0

    .line 1116
    :cond_0
    iget-object v0, p0, Lcom/stagnationlab/sk/TransactionActivity;->h:Lcom/stagnationlab/sk/TransactionActivity$b;

    sget-object v1, Lcom/stagnationlab/sk/TransactionActivity$b;->d:Lcom/stagnationlab/sk/TransactionActivity$b;

    if-ne v0, v1, :cond_1

    const v0, 0x7f080033

    .line 1119
    invoke-virtual {p0, v0}, Lcom/stagnationlab/sk/TransactionActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 1120
    invoke-virtual {v0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    const v0, 0x7f08002f

    .line 1121
    invoke-virtual {p0, v0}, Lcom/stagnationlab/sk/TransactionActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    const/high16 v0, -0x40800000    # -1.0f

    .line 1122
    iput v0, p0, Lcom/stagnationlab/sk/TransactionActivity;->Z:F

    .line 1123
    iget-object v0, p0, Lcom/stagnationlab/sk/TransactionActivity;->X:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    .line 1126
    :cond_1
    iget-object v0, p0, Lcom/stagnationlab/sk/TransactionActivity;->h:Lcom/stagnationlab/sk/TransactionActivity$b;

    invoke-direct {p0, v0}, Lcom/stagnationlab/sk/TransactionActivity;->a(Lcom/stagnationlab/sk/TransactionActivity$b;)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2, p1}, Lcom/stagnationlab/sk/TransactionActivity;->a(Landroid/view/View;ZZI)V

    .line 1127
    iget-object v0, p0, Lcom/stagnationlab/sk/TransactionActivity;->M:Landroid/widget/RelativeLayout;

    invoke-direct {p0, v0, v2, v2, p1}, Lcom/stagnationlab/sk/TransactionActivity;->a(Landroid/view/View;ZZI)V

    .line 1128
    iget-object p1, p0, Lcom/stagnationlab/sk/TransactionActivity;->X:Landroid/animation/ObjectAnimator;

    invoke-virtual {p1}, Landroid/animation/ObjectAnimator;->start()V

    .line 1130
    sget-object p1, Lcom/stagnationlab/sk/TransactionActivity$b;->a:Lcom/stagnationlab/sk/TransactionActivity$b;

    iput-object p1, p0, Lcom/stagnationlab/sk/TransactionActivity;->h:Lcom/stagnationlab/sk/TransactionActivity$b;

    :goto_0
    return-void
.end method

.method private c(Landroid/view/View;Z)V
    .locals 2

    .line 1368
    invoke-virtual {p1, p2}, Landroid/view/View;->setEnabled(Z)V

    .line 1370
    instance-of v0, p1, Landroid/widget/Button;

    if-eqz v0, :cond_0

    .line 1371
    invoke-virtual {p1, p2}, Landroid/view/View;->setClickable(Z)V

    .line 1374
    :cond_0
    instance-of v0, p1, Landroid/view/ViewGroup;

    if-eqz v0, :cond_1

    .line 1375
    check-cast p1, Landroid/view/ViewGroup;

    const/4 v0, 0x0

    .line 1377
    :goto_0
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 1378
    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-direct {p0, v1, p2}, Lcom/stagnationlab/sk/TransactionActivity;->c(Landroid/view/View;Z)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method static synthetic c(Lcom/stagnationlab/sk/TransactionActivity;)Z
    .locals 0

    .line 68
    iget-boolean p0, p0, Lcom/stagnationlab/sk/TransactionActivity;->af:Z

    return p0
.end method

.method static synthetic c(Lcom/stagnationlab/sk/TransactionActivity;Z)Z
    .locals 0

    .line 68
    iput-boolean p1, p0, Lcom/stagnationlab/sk/TransactionActivity;->ab:Z

    return p1
.end method

.method static synthetic d(Lcom/stagnationlab/sk/TransactionActivity;)V
    .locals 0

    .line 68
    invoke-direct {p0}, Lcom/stagnationlab/sk/TransactionActivity;->B()V

    return-void
.end method

.method static synthetic d(Lcom/stagnationlab/sk/TransactionActivity;Z)Z
    .locals 0

    .line 68
    iput-boolean p1, p0, Lcom/stagnationlab/sk/TransactionActivity;->aa:Z

    return p1
.end method

.method static synthetic e(Lcom/stagnationlab/sk/TransactionActivity;)V
    .locals 0

    .line 68
    invoke-direct {p0}, Lcom/stagnationlab/sk/TransactionActivity;->x()V

    return-void
.end method

.method static synthetic e(Lcom/stagnationlab/sk/TransactionActivity;Z)Z
    .locals 0

    .line 68
    iput-boolean p1, p0, Lcom/stagnationlab/sk/TransactionActivity;->D:Z

    return p1
.end method

.method static synthetic f(Lcom/stagnationlab/sk/TransactionActivity;)Lcom/stagnationlab/sk/elements/Modal;
    .locals 0

    .line 68
    iget-object p0, p0, Lcom/stagnationlab/sk/TransactionActivity;->V:Lcom/stagnationlab/sk/elements/Modal;

    return-object p0
.end method

.method static synthetic f(Lcom/stagnationlab/sk/TransactionActivity;Z)Z
    .locals 0

    .line 68
    iput-boolean p1, p0, Lcom/stagnationlab/sk/TransactionActivity;->y:Z

    return p1
.end method

.method static synthetic g(Lcom/stagnationlab/sk/TransactionActivity;)V
    .locals 0

    .line 68
    invoke-direct {p0}, Lcom/stagnationlab/sk/TransactionActivity;->o()V

    return-void
.end method

.method static synthetic g(Lcom/stagnationlab/sk/TransactionActivity;Z)Z
    .locals 0

    .line 68
    iput-boolean p1, p0, Lcom/stagnationlab/sk/TransactionActivity;->E:Z

    return p1
.end method

.method static synthetic h(Lcom/stagnationlab/sk/TransactionActivity;)Lcom/stagnationlab/sk/PinFragment;
    .locals 0

    .line 68
    iget-object p0, p0, Lcom/stagnationlab/sk/TransactionActivity;->Q:Lcom/stagnationlab/sk/PinFragment;

    return-object p0
.end method

.method static synthetic h(Lcom/stagnationlab/sk/TransactionActivity;Z)Z
    .locals 0

    .line 68
    iput-boolean p1, p0, Lcom/stagnationlab/sk/TransactionActivity;->z:Z

    return p1
.end method

.method static synthetic i()I
    .locals 1

    .line 68
    sget v0, Lcom/stagnationlab/sk/TransactionActivity;->c:I

    return v0
.end method

.method static synthetic i(Lcom/stagnationlab/sk/TransactionActivity;)V
    .locals 0

    .line 68
    invoke-direct {p0}, Lcom/stagnationlab/sk/TransactionActivity;->r()V

    return-void
.end method

.method static synthetic j()Landroid/view/animation/Interpolator;
    .locals 1

    .line 68
    sget-object v0, Lcom/stagnationlab/sk/TransactionActivity;->d:Landroid/view/animation/Interpolator;

    return-object v0
.end method

.method static synthetic j(Lcom/stagnationlab/sk/TransactionActivity;)V
    .locals 0

    .line 68
    invoke-direct {p0}, Lcom/stagnationlab/sk/TransactionActivity;->n()V

    return-void
.end method

.method static synthetic k()I
    .locals 1

    .line 68
    sget v0, Lcom/stagnationlab/sk/TransactionActivity;->e:I

    return v0
.end method

.method static synthetic k(Lcom/stagnationlab/sk/TransactionActivity;)Lcom/stagnationlab/sk/models/Transaction;
    .locals 0

    .line 68
    iget-object p0, p0, Lcom/stagnationlab/sk/TransactionActivity;->n:Lcom/stagnationlab/sk/models/Transaction;

    return-object p0
.end method

.method static synthetic l(Lcom/stagnationlab/sk/TransactionActivity;)Landroid/widget/TextView;
    .locals 0

    .line 68
    iget-object p0, p0, Lcom/stagnationlab/sk/TransactionActivity;->r:Landroid/widget/TextView;

    return-object p0
.end method

.method private l()V
    .locals 6

    .line 454
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    :goto_0
    const/16 v3, 0xc

    if-ge v2, v3, :cond_0

    const v3, 0x7f0a0027

    .line 457
    iget-object v4, p0, Lcom/stagnationlab/sk/TransactionActivity;->L:Lcom/stagnationlab/sk/elements/MaxWidthLinearLayout;

    invoke-virtual {v0, v3, v4, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/view/ViewGroup;

    .line 460
    iget-object v4, p0, Lcom/stagnationlab/sk/TransactionActivity;->S:Ljava/util/List;

    new-instance v5, Lcom/stagnationlab/sk/g;

    invoke-direct {v5, v3}, Lcom/stagnationlab/sk/g;-><init>(Landroid/view/View;)V

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 461
    iget-object v4, p0, Lcom/stagnationlab/sk/TransactionActivity;->L:Lcom/stagnationlab/sk/elements/MaxWidthLinearLayout;

    invoke-virtual {v4, v3}, Lcom/stagnationlab/sk/elements/MaxWidthLinearLayout;->addView(Landroid/view/View;)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 464
    :cond_0
    iget-object v0, p0, Lcom/stagnationlab/sk/TransactionActivity;->Q:Lcom/stagnationlab/sk/PinFragment;

    new-instance v1, Lcom/stagnationlab/sk/TransactionActivity$26;

    invoke-direct {v1, p0}, Lcom/stagnationlab/sk/TransactionActivity$26;-><init>(Lcom/stagnationlab/sk/TransactionActivity;)V

    invoke-virtual {v0, v1}, Lcom/stagnationlab/sk/PinFragment;->a(Lcom/stagnationlab/sk/PinFragment$a;)V

    .line 486
    iget-object v0, p0, Lcom/stagnationlab/sk/TransactionActivity;->R:Landroid/widget/ImageButton;

    new-instance v1, Lcom/stagnationlab/sk/TransactionActivity$27;

    invoke-direct {v1, p0}, Lcom/stagnationlab/sk/TransactionActivity$27;-><init>(Lcom/stagnationlab/sk/TransactionActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    return-void
.end method

.method static synthetic m(Lcom/stagnationlab/sk/TransactionActivity;)Lcom/stagnationlab/sk/a/a;
    .locals 0

    .line 68
    iget-object p0, p0, Lcom/stagnationlab/sk/TransactionActivity;->l:Lcom/stagnationlab/sk/a/a;

    return-object p0
.end method

.method private m()V
    .locals 3

    .line 575
    iget-object v0, p0, Lcom/stagnationlab/sk/TransactionActivity;->l:Lcom/stagnationlab/sk/a/a;

    iget-object v1, p0, Lcom/stagnationlab/sk/TransactionActivity;->n:Lcom/stagnationlab/sk/models/Transaction;

    invoke-virtual {v1}, Lcom/stagnationlab/sk/models/Transaction;->h()Lcom/stagnationlab/sk/models/Transaction$Type;

    move-result-object v1

    new-instance v2, Lcom/stagnationlab/sk/TransactionActivity$2;

    invoke-direct {v2, p0}, Lcom/stagnationlab/sk/TransactionActivity$2;-><init>(Lcom/stagnationlab/sk/TransactionActivity;)V

    invoke-virtual {v0, v1, v2}, Lcom/stagnationlab/sk/a/a;->a(Lcom/stagnationlab/sk/models/Transaction$Type;Lcom/stagnationlab/sk/a/a/p;)V

    return-void
.end method

.method private n()V
    .locals 6

    .line 590
    iget-object v0, p0, Lcom/stagnationlab/sk/TransactionActivity;->n:Lcom/stagnationlab/sk/models/Transaction;

    invoke-virtual {v0}, Lcom/stagnationlab/sk/models/Transaction;->h()Lcom/stagnationlab/sk/models/Transaction$Type;

    move-result-object v0

    .line 592
    iget v1, p0, Lcom/stagnationlab/sk/TransactionActivity;->k:I

    const/4 v2, 0x4

    const/4 v3, -0x1

    if-ne v1, v3, :cond_1

    .line 593
    sget-object v1, Lcom/stagnationlab/sk/models/Transaction$Type;->AUTHENTICATION:Lcom/stagnationlab/sk/models/Transaction$Type;

    if-ne v0, v1, :cond_0

    const/4 v1, 0x4

    goto :goto_0

    :cond_0
    const/4 v1, 0x5

    :goto_0
    iput v1, p0, Lcom/stagnationlab/sk/TransactionActivity;->j:I

    const/16 v1, 0xc

    .line 595
    iput v1, p0, Lcom/stagnationlab/sk/TransactionActivity;->i:I

    goto :goto_1

    .line 597
    :cond_1
    iput v1, p0, Lcom/stagnationlab/sk/TransactionActivity;->i:I

    iput v1, p0, Lcom/stagnationlab/sk/TransactionActivity;->j:I

    .line 600
    :goto_1
    iget-object v1, p0, Lcom/stagnationlab/sk/TransactionActivity;->Q:Lcom/stagnationlab/sk/PinFragment;

    iget v3, p0, Lcom/stagnationlab/sk/TransactionActivity;->j:I

    iget v4, p0, Lcom/stagnationlab/sk/TransactionActivity;->i:I

    invoke-virtual {v1, v3, v4}, Lcom/stagnationlab/sk/PinFragment;->a(II)V

    .line 601
    iget-object v1, p0, Lcom/stagnationlab/sk/TransactionActivity;->s:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/stagnationlab/sk/TransactionActivity;->n:Lcom/stagnationlab/sk/models/Transaction;

    invoke-virtual {v3}, Lcom/stagnationlab/sk/models/Transaction;->g()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 602
    iget-object v1, p0, Lcom/stagnationlab/sk/TransactionActivity;->r:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/stagnationlab/sk/TransactionActivity;->n:Lcom/stagnationlab/sk/models/Transaction;

    invoke-virtual {v3}, Lcom/stagnationlab/sk/models/Transaction;->j()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 604
    iget-object v1, p0, Lcom/stagnationlab/sk/TransactionActivity;->n:Lcom/stagnationlab/sk/models/Transaction;

    invoke-virtual {v1}, Lcom/stagnationlab/sk/models/Transaction;->i()Ljava/lang/String;

    move-result-object v1

    .line 605
    iget-object v3, p0, Lcom/stagnationlab/sk/TransactionActivity;->p:Landroid/widget/TextView;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    const/4 v5, 0x0

    if-eqz v4, :cond_2

    const/16 v4, 0x8

    goto :goto_2

    :cond_2
    const/4 v4, 0x0

    :goto_2
    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 606
    iget-object v3, p0, Lcom/stagnationlab/sk/TransactionActivity;->p:Landroid/widget/TextView;

    invoke-virtual {v3, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 608
    iget-object v1, p0, Lcom/stagnationlab/sk/TransactionActivity;->q:Landroid/widget/TextView;

    sget-object v3, Lcom/stagnationlab/sk/models/Transaction$Type;->AUTHENTICATION:Lcom/stagnationlab/sk/models/Transaction$Type;

    if-ne v0, v3, :cond_3

    const v0, 0x7f0e0045

    goto :goto_3

    :cond_3
    const v0, 0x7f0e0046

    .line 611
    :goto_3
    invoke-virtual {p0, v0}, Lcom/stagnationlab/sk/TransactionActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 608
    invoke-static {p0, v1, v0}, Lcom/stagnationlab/sk/util/k;->a(Landroid/content/Context;Landroid/widget/TextView;Ljava/lang/String;)V

    .line 613
    invoke-direct {p0}, Lcom/stagnationlab/sk/TransactionActivity;->p()V

    const/4 v0, 0x1

    .line 615
    iput-boolean v0, p0, Lcom/stagnationlab/sk/TransactionActivity;->z:Z

    .line 616
    invoke-direct {p0}, Lcom/stagnationlab/sk/TransactionActivity;->z()V

    .line 620
    iget-object v0, p0, Lcom/stagnationlab/sk/TransactionActivity;->n:Lcom/stagnationlab/sk/models/Transaction;

    invoke-virtual {v0}, Lcom/stagnationlab/sk/models/Transaction;->e()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 621
    iget-object v0, p0, Lcom/stagnationlab/sk/TransactionActivity;->r:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    const v0, 0x7f0800b0

    .line 623
    invoke-virtual {p0, v0}, Lcom/stagnationlab/sk/TransactionActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/stagnationlab/sk/components/SelectControlCode;

    .line 624
    iget-object v1, p0, Lcom/stagnationlab/sk/TransactionActivity;->n:Lcom/stagnationlab/sk/models/Transaction;

    invoke-virtual {v1}, Lcom/stagnationlab/sk/models/Transaction;->n()[Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/stagnationlab/sk/TransactionActivity$3;

    invoke-direct {v2, p0, v0}, Lcom/stagnationlab/sk/TransactionActivity$3;-><init>(Lcom/stagnationlab/sk/TransactionActivity;Lcom/stagnationlab/sk/components/SelectControlCode;)V

    invoke-virtual {v0, v1, v2}, Lcom/stagnationlab/sk/components/SelectControlCode;->a([Ljava/lang/String;Lcom/stagnationlab/sk/components/SelectControlCode$a;)V

    goto :goto_4

    :cond_4
    const/4 v0, 0x0

    .line 663
    :goto_4
    invoke-direct {p0, v5, v0}, Lcom/stagnationlab/sk/TransactionActivity;->a(ZLcom/stagnationlab/sk/components/SelectControlCode;)V

    return-void
.end method

.method static synthetic n(Lcom/stagnationlab/sk/TransactionActivity;)V
    .locals 0

    .line 68
    invoke-direct {p0}, Lcom/stagnationlab/sk/TransactionActivity;->q()V

    return-void
.end method

.method private o()V
    .locals 3

    .line 861
    iget-object v0, p0, Lcom/stagnationlab/sk/TransactionActivity;->n:Lcom/stagnationlab/sk/models/Transaction;

    if-nez v0, :cond_0

    return-void

    .line 865
    :cond_0
    invoke-direct {p0}, Lcom/stagnationlab/sk/TransactionActivity;->D()V

    .line 866
    invoke-direct {p0}, Lcom/stagnationlab/sk/TransactionActivity;->s()V

    .line 868
    iget-object v0, p0, Lcom/stagnationlab/sk/TransactionActivity;->l:Lcom/stagnationlab/sk/a/a;

    iget-object v1, p0, Lcom/stagnationlab/sk/TransactionActivity;->n:Lcom/stagnationlab/sk/models/Transaction;

    new-instance v2, Lcom/stagnationlab/sk/TransactionActivity$8;

    invoke-direct {v2, p0}, Lcom/stagnationlab/sk/TransactionActivity$8;-><init>(Lcom/stagnationlab/sk/TransactionActivity;)V

    invoke-virtual {v0, v1, v2}, Lcom/stagnationlab/sk/a/a;->a(Lcom/stagnationlab/sk/models/Transaction;Lcom/stagnationlab/sk/a/a/d;)V

    return-void
.end method

.method static synthetic o(Lcom/stagnationlab/sk/TransactionActivity;)Z
    .locals 0

    .line 68
    iget-boolean p0, p0, Lcom/stagnationlab/sk/TransactionActivity;->ae:Z

    return p0
.end method

.method static synthetic p(Lcom/stagnationlab/sk/TransactionActivity;)Landroid/support/v7/widget/AppCompatImageView;
    .locals 0

    .line 68
    iget-object p0, p0, Lcom/stagnationlab/sk/TransactionActivity;->F:Landroid/support/v7/widget/AppCompatImageView;

    return-object p0
.end method

.method private p()V
    .locals 5

    .line 887
    iget-object v0, p0, Lcom/stagnationlab/sk/TransactionActivity;->S:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/stagnationlab/sk/g;

    .line 888
    invoke-virtual {v3}, Lcom/stagnationlab/sk/g;->a()Landroid/view/View;

    move-result-object v3

    iget v4, p0, Lcom/stagnationlab/sk/TransactionActivity;->i:I

    if-ge v2, v4, :cond_0

    const/4 v4, 0x0

    goto :goto_1

    :cond_0
    const/16 v4, 0x8

    :goto_1
    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 893
    :cond_1
    iget-object v0, p0, Lcom/stagnationlab/sk/TransactionActivity;->L:Lcom/stagnationlab/sk/elements/MaxWidthLinearLayout;

    iget v2, p0, Lcom/stagnationlab/sk/TransactionActivity;->i:I

    mul-int/lit8 v2, v2, 0x18

    invoke-virtual {v0, v2}, Lcom/stagnationlab/sk/elements/MaxWidthLinearLayout;->setMaxWidth(I)V

    .line 896
    invoke-direct {p0, v1}, Lcom/stagnationlab/sk/TransactionActivity;->b(I)V

    return-void
.end method

.method static synthetic q(Lcom/stagnationlab/sk/TransactionActivity;)Landroid/widget/ImageView;
    .locals 0

    .line 68
    iget-object p0, p0, Lcom/stagnationlab/sk/TransactionActivity;->G:Landroid/widget/ImageView;

    return-object p0
.end method

.method private q()V
    .locals 4

    .line 914
    iget-boolean v0, p0, Lcom/stagnationlab/sk/TransactionActivity;->aa:Z

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/stagnationlab/sk/TransactionActivity;->ab:Z

    if-nez v0, :cond_0

    goto :goto_0

    .line 918
    :cond_0
    iget-object v0, p0, Lcom/stagnationlab/sk/TransactionActivity;->B:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 920
    iget-object v0, p0, Lcom/stagnationlab/sk/TransactionActivity;->ac:Lcom/stagnationlab/sk/c/a;

    const-string v2, "TransactionActivity"

    if-nez v0, :cond_1

    const-string v0, "Transaction confirmed"

    .line 921
    invoke-static {v2, v0}, Lcom/stagnationlab/sk/f;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 922
    invoke-direct {p0}, Lcom/stagnationlab/sk/TransactionActivity;->r()V

    goto :goto_0

    .line 924
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Failed to confirm transaction: "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/stagnationlab/sk/TransactionActivity;->ac:Lcom/stagnationlab/sk/c/a;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/stagnationlab/sk/f;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 925
    iget-object v0, p0, Lcom/stagnationlab/sk/TransactionActivity;->Q:Lcom/stagnationlab/sk/PinFragment;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lcom/stagnationlab/sk/PinFragment;->a(Z)V

    .line 926
    iget-object v0, p0, Lcom/stagnationlab/sk/TransactionActivity;->Q:Lcom/stagnationlab/sk/PinFragment;

    invoke-virtual {v0}, Lcom/stagnationlab/sk/PinFragment;->a()V

    .line 928
    iget-object v0, p0, Lcom/stagnationlab/sk/TransactionActivity;->ac:Lcom/stagnationlab/sk/c/a;

    invoke-direct {p0, v0}, Lcom/stagnationlab/sk/TransactionActivity;->b(Lcom/stagnationlab/sk/c/a;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 929
    invoke-direct {p0, v2, v1}, Lcom/stagnationlab/sk/TransactionActivity;->a(ZLcom/stagnationlab/sk/components/SelectControlCode;)V

    :cond_2
    :goto_0
    return-void
.end method

.method private r()V
    .locals 6

    .line 935
    iget-object v0, p0, Lcom/stagnationlab/sk/TransactionActivity;->n:Lcom/stagnationlab/sk/models/Transaction;

    invoke-virtual {v0}, Lcom/stagnationlab/sk/models/Transaction;->h()Lcom/stagnationlab/sk/models/Transaction$Type;

    move-result-object v0

    sget-object v1, Lcom/stagnationlab/sk/models/Transaction$Type;->AUTHENTICATION:Lcom/stagnationlab/sk/models/Transaction$Type;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 936
    :goto_0
    invoke-static {p0}, Lcom/stagnationlab/sk/i;->b(Landroid/content/Context;)Z

    move-result v1

    .line 937
    sget-object v2, Lcom/stagnationlab/sk/TransactionActivity$b;->d:Lcom/stagnationlab/sk/TransactionActivity$b;

    iput-object v2, p0, Lcom/stagnationlab/sk/TransactionActivity;->h:Lcom/stagnationlab/sk/TransactionActivity$b;

    const v2, 0x7f0800c5

    .line 939
    invoke-virtual {p0, v2}, Lcom/stagnationlab/sk/TransactionActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    if-eqz v0, :cond_1

    const v3, 0x7f0e0029

    goto :goto_1

    :cond_1
    const v3, 0x7f0e0117

    :goto_1
    invoke-virtual {p0, v3}, Lcom/stagnationlab/sk/TransactionActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 941
    invoke-virtual {p0}, Lcom/stagnationlab/sk/TransactionActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f080033

    .line 944
    invoke-virtual {p0, v3}, Lcom/stagnationlab/sk/TransactionActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    const/high16 v4, 0x3f800000    # 1.0f

    .line 945
    invoke-virtual {v3, v4}, Landroid/view/View;->setAlpha(F)V

    .line 946
    iget v4, p0, Lcom/stagnationlab/sk/TransactionActivity;->Y:F

    iput v4, p0, Lcom/stagnationlab/sk/TransactionActivity;->Z:F

    if-eqz v1, :cond_2

    const/high16 v5, 0x42200000    # 40.0f

    sub-float/2addr v4, v5

    .line 949
    invoke-virtual {v3, v4}, Landroid/view/View;->setRotation(F)V

    goto :goto_2

    .line 951
    :cond_2
    invoke-virtual {v3, v4}, Landroid/view/View;->setRotation(F)V

    :goto_2
    const v3, 0x7f070079

    .line 955
    invoke-static {p0, v3}, Landroid/support/b/a/c;->a(Landroid/content/Context;I)Landroid/support/b/a/c;

    move-result-object v3

    if-eqz v3, :cond_3

    const v4, 0x7f08002f

    .line 960
    invoke-virtual {p0, v4}, Lcom/stagnationlab/sk/TransactionActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageView;

    invoke-virtual {v4, v3}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 961
    invoke-virtual {v3}, Landroid/support/b/a/c;->start()V

    :cond_3
    const v3, 0x7f090009

    .line 965
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v3

    const v4, 0x7f090008

    .line 966
    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    if-nez v1, :cond_4

    .line 969
    invoke-direct {p0, v3, v2}, Lcom/stagnationlab/sk/TransactionActivity;->a(II)V

    .line 973
    :cond_4
    iget-object v1, p0, Lcom/stagnationlab/sk/TransactionActivity;->P:Landroid/widget/RelativeLayout;

    invoke-direct {p0, v1}, Lcom/stagnationlab/sk/TransactionActivity;->b(Landroid/view/View;)V

    .line 975
    new-instance v1, Lcom/stagnationlab/sk/util/m;

    iget-object v4, p0, Lcom/stagnationlab/sk/TransactionActivity;->l:Lcom/stagnationlab/sk/a/a;

    iget-object v5, p0, Lcom/stagnationlab/sk/TransactionActivity;->n:Lcom/stagnationlab/sk/models/Transaction;

    invoke-virtual {v5}, Lcom/stagnationlab/sk/models/Transaction;->k()Z

    move-result v5

    invoke-direct {v1, v4, v5}, Lcom/stagnationlab/sk/util/m;-><init>(Lcom/stagnationlab/sk/a/a;Z)V

    .line 977
    iget-object v4, p0, Lcom/stagnationlab/sk/TransactionActivity;->B:Landroid/os/Handler;

    new-instance v5, Lcom/stagnationlab/sk/TransactionActivity$9;

    invoke-direct {v5, p0, v1}, Lcom/stagnationlab/sk/TransactionActivity$9;-><init>(Lcom/stagnationlab/sk/TransactionActivity;Lcom/stagnationlab/sk/util/m;)V

    add-int/2addr v2, v3

    add-int/lit16 v2, v2, 0x2bc

    int-to-long v1, v2

    invoke-virtual {v4, v5, v1, v2}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 983
    iget-object v1, p0, Lcom/stagnationlab/sk/TransactionActivity;->m:Lcom/stagnationlab/sk/a;

    if-eqz v0, :cond_5

    const-string v0, "Authentication success"

    goto :goto_3

    :cond_5
    const-string v0, "Signature success"

    :goto_3
    invoke-virtual {v1, v0}, Lcom/stagnationlab/sk/a;->a(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic r(Lcom/stagnationlab/sk/TransactionActivity;)V
    .locals 0

    .line 68
    invoke-direct {p0}, Lcom/stagnationlab/sk/TransactionActivity;->v()V

    return-void
.end method

.method private s()V
    .locals 1

    const/4 v0, 0x0

    .line 1108
    invoke-direct {p0, v0}, Lcom/stagnationlab/sk/TransactionActivity;->c(I)V

    return-void
.end method

.method static synthetic s(Lcom/stagnationlab/sk/TransactionActivity;)V
    .locals 0

    .line 68
    invoke-direct {p0}, Lcom/stagnationlab/sk/TransactionActivity;->u()V

    return-void
.end method

.method static synthetic t(Lcom/stagnationlab/sk/TransactionActivity;)Landroid/widget/RelativeLayout;
    .locals 0

    .line 68
    iget-object p0, p0, Lcom/stagnationlab/sk/TransactionActivity;->O:Landroid/widget/RelativeLayout;

    return-object p0
.end method

.method private t()V
    .locals 4

    .line 1135
    iget-boolean v0, p0, Lcom/stagnationlab/sk/TransactionActivity;->g:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x1

    .line 1139
    iput-boolean v0, p0, Lcom/stagnationlab/sk/TransactionActivity;->g:Z

    .line 1142
    iget-object v0, p0, Lcom/stagnationlab/sk/TransactionActivity;->W:[Landroid/view/View;

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_1

    aget-object v3, v0, v2

    .line 1143
    invoke-direct {p0, v3}, Lcom/stagnationlab/sk/TransactionActivity;->a(Landroid/view/View;)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1146
    :cond_1
    iget-object v0, p0, Lcom/stagnationlab/sk/TransactionActivity;->o:Lcom/stagnationlab/sk/models/RpRequest;

    invoke-virtual {v0}, Lcom/stagnationlab/sk/models/RpRequest;->f()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1148
    iget-object v0, p0, Lcom/stagnationlab/sk/TransactionActivity;->T:Landroid/widget/TextView;

    invoke-direct {p0, v0}, Lcom/stagnationlab/sk/TransactionActivity;->b(Landroid/view/View;)V

    .line 1149
    iget-object v0, p0, Lcom/stagnationlab/sk/TransactionActivity;->U:Landroid/widget/LinearLayout;

    invoke-direct {p0, v0}, Lcom/stagnationlab/sk/TransactionActivity;->b(Landroid/view/View;)V

    :cond_2
    const v0, 0x7f070081

    .line 1153
    invoke-static {p0, v0}, Landroid/support/b/a/c;->a(Landroid/content/Context;I)Landroid/support/b/a/c;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 1159
    iget-object v1, p0, Lcom/stagnationlab/sk/TransactionActivity;->F:Landroid/support/v7/widget/AppCompatImageView;

    invoke-virtual {v1, v0}, Landroid/support/v7/widget/AppCompatImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1161
    invoke-virtual {v0}, Landroid/support/b/a/c;->start()V

    .line 1164
    :cond_3
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lcom/stagnationlab/sk/TransactionActivity$16;

    invoke-direct {v1, p0}, Lcom/stagnationlab/sk/TransactionActivity$16;-><init>(Lcom/stagnationlab/sk/TransactionActivity;)V

    .line 1185
    invoke-virtual {p0}, Lcom/stagnationlab/sk/TransactionActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f09000a

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    int-to-long v2, v2

    .line 1164
    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method static synthetic u(Lcom/stagnationlab/sk/TransactionActivity;)Landroid/widget/TextView;
    .locals 0

    .line 68
    iget-object p0, p0, Lcom/stagnationlab/sk/TransactionActivity;->t:Landroid/widget/TextView;

    return-object p0
.end method

.method private u()V
    .locals 5

    .line 1189
    iget-boolean v0, p0, Lcom/stagnationlab/sk/TransactionActivity;->g:Z

    if-nez v0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x0

    .line 1193
    iput-boolean v0, p0, Lcom/stagnationlab/sk/TransactionActivity;->g:Z

    .line 1197
    iget-object v1, p0, Lcom/stagnationlab/sk/TransactionActivity;->W:[Landroid/view/View;

    array-length v2, v1

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v2, :cond_1

    aget-object v4, v1, v3

    .line 1198
    invoke-direct {p0, v4, v0}, Lcom/stagnationlab/sk/TransactionActivity;->b(Landroid/view/View;Z)V

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 1201
    :cond_1
    iget-object v1, p0, Lcom/stagnationlab/sk/TransactionActivity;->T:Landroid/widget/TextView;

    invoke-direct {p0, v1, v0}, Lcom/stagnationlab/sk/TransactionActivity;->a(Landroid/view/View;Z)V

    .line 1202
    iget-object v1, p0, Lcom/stagnationlab/sk/TransactionActivity;->U:Landroid/widget/LinearLayout;

    invoke-direct {p0, v1, v0}, Lcom/stagnationlab/sk/TransactionActivity;->a(Landroid/view/View;Z)V

    .line 1204
    iget-object v0, p0, Lcom/stagnationlab/sk/TransactionActivity;->F:Landroid/support/v7/widget/AppCompatImageView;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/AppCompatImageView;->setAlpha(F)V

    .line 1205
    iget-object v0, p0, Lcom/stagnationlab/sk/TransactionActivity;->F:Landroid/support/v7/widget/AppCompatImageView;

    const v1, 0x7f070080

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/AppCompatImageView;->setImageResource(I)V

    .line 1207
    iget-object v0, p0, Lcom/stagnationlab/sk/TransactionActivity;->G:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setAnimation(Landroid/view/animation/Animation;)V

    return-void
.end method

.method static synthetic v(Lcom/stagnationlab/sk/TransactionActivity;)Landroid/widget/TextView;
    .locals 0

    .line 68
    iget-object p0, p0, Lcom/stagnationlab/sk/TransactionActivity;->u:Landroid/widget/TextView;

    return-object p0
.end method

.method private v()V
    .locals 2

    .line 1248
    iget-object v0, p0, Lcom/stagnationlab/sk/TransactionActivity;->M:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    .line 1249
    iget-object v0, p0, Lcom/stagnationlab/sk/TransactionActivity;->X:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->cancel()V

    return-void
.end method

.method private w()V
    .locals 2

    .line 1293
    iget-object v0, p0, Lcom/stagnationlab/sk/TransactionActivity;->h:Lcom/stagnationlab/sk/TransactionActivity$b;

    sget-object v1, Lcom/stagnationlab/sk/TransactionActivity$b;->a:Lcom/stagnationlab/sk/TransactionActivity$b;

    if-ne v0, v1, :cond_0

    .line 1295
    iget-object v0, p0, Lcom/stagnationlab/sk/TransactionActivity;->O:Landroid/widget/RelativeLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1296
    iget-object v0, p0, Lcom/stagnationlab/sk/TransactionActivity;->h:Lcom/stagnationlab/sk/TransactionActivity$b;

    invoke-direct {p0, v0}, Lcom/stagnationlab/sk/TransactionActivity;->a(Lcom/stagnationlab/sk/TransactionActivity$b;)Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0, v1}, Lcom/stagnationlab/sk/TransactionActivity;->a(Landroid/view/View;Z)V

    goto :goto_0

    .line 1299
    :cond_0
    invoke-direct {p0}, Lcom/stagnationlab/sk/TransactionActivity;->u()V

    .line 1302
    :goto_0
    sget-object v0, Lcom/stagnationlab/sk/TransactionActivity$b;->c:Lcom/stagnationlab/sk/TransactionActivity$b;

    iput-object v0, p0, Lcom/stagnationlab/sk/TransactionActivity;->h:Lcom/stagnationlab/sk/TransactionActivity$b;

    return-void
.end method

.method static synthetic w(Lcom/stagnationlab/sk/TransactionActivity;)Z
    .locals 0

    .line 68
    iget-boolean p0, p0, Lcom/stagnationlab/sk/TransactionActivity;->E:Z

    return p0
.end method

.method private x()V
    .locals 2

    .line 1354
    invoke-static {p0}, Lcom/stagnationlab/sk/ExitActivity;->a(Landroid/app/Activity;)V

    .line 1355
    iget-object v0, p0, Lcom/stagnationlab/sk/TransactionActivity;->B:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 1356
    invoke-virtual {p0}, Lcom/stagnationlab/sk/TransactionActivity;->finish()V

    return-void
.end method

.method private y()V
    .locals 7

    .line 1384
    iget-boolean v0, p0, Lcom/stagnationlab/sk/TransactionActivity;->y:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/stagnationlab/sk/TransactionActivity;->o:Lcom/stagnationlab/sk/models/RpRequest;

    if-nez v0, :cond_0

    goto :goto_0

    .line 1388
    :cond_0
    invoke-direct {p0}, Lcom/stagnationlab/sk/TransactionActivity;->A()V

    .line 1390
    iget-object v0, p0, Lcom/stagnationlab/sk/TransactionActivity;->o:Lcom/stagnationlab/sk/models/RpRequest;

    invoke-virtual {v0}, Lcom/stagnationlab/sk/models/RpRequest;->b()J

    move-result-wide v3

    const-wide/16 v0, 0x0

    cmp-long v2, v3, v0

    if-gtz v2, :cond_1

    .line 1393
    sget-object v0, Lcom/stagnationlab/sk/TransactionActivity$a;->c:Lcom/stagnationlab/sk/TransactionActivity$a;

    invoke-direct {p0, v0}, Lcom/stagnationlab/sk/TransactionActivity;->a(Lcom/stagnationlab/sk/TransactionActivity$a;)V

    return-void

    .line 1398
    :cond_1
    new-instance v0, Lcom/stagnationlab/sk/TransactionActivity$19;

    const-wide/16 v5, 0x1f4

    move-object v1, v0

    move-object v2, p0

    invoke-direct/range {v1 .. v6}, Lcom/stagnationlab/sk/TransactionActivity$19;-><init>(Lcom/stagnationlab/sk/TransactionActivity;JJ)V

    .line 1409
    invoke-virtual {v0}, Lcom/stagnationlab/sk/TransactionActivity$19;->start()Landroid/os/CountDownTimer;

    move-result-object v0

    iput-object v0, p0, Lcom/stagnationlab/sk/TransactionActivity;->w:Landroid/os/CountDownTimer;

    :cond_2
    :goto_0
    return-void
.end method

.method private z()V
    .locals 7

    .line 1413
    iget-boolean v0, p0, Lcom/stagnationlab/sk/TransactionActivity;->z:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/stagnationlab/sk/TransactionActivity;->n:Lcom/stagnationlab/sk/models/Transaction;

    if-nez v0, :cond_0

    goto :goto_0

    .line 1417
    :cond_0
    invoke-direct {p0}, Lcom/stagnationlab/sk/TransactionActivity;->C()V

    .line 1419
    iget-object v0, p0, Lcom/stagnationlab/sk/TransactionActivity;->n:Lcom/stagnationlab/sk/models/Transaction;

    invoke-virtual {v0}, Lcom/stagnationlab/sk/models/Transaction;->b()J

    move-result-wide v3

    const-wide/16 v0, 0x0

    cmp-long v2, v3, v0

    if-gtz v2, :cond_1

    .line 1422
    iget-object v0, p0, Lcom/stagnationlab/sk/TransactionActivity;->V:Lcom/stagnationlab/sk/elements/Modal;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/stagnationlab/sk/elements/Modal;->setVisible(Z)V

    .line 1423
    sget-object v0, Lcom/stagnationlab/sk/TransactionActivity$a;->c:Lcom/stagnationlab/sk/TransactionActivity$a;

    invoke-direct {p0, v0}, Lcom/stagnationlab/sk/TransactionActivity;->a(Lcom/stagnationlab/sk/TransactionActivity$a;)V

    return-void

    .line 1428
    :cond_1
    new-instance v0, Lcom/stagnationlab/sk/TransactionActivity$20;

    const-wide/16 v5, 0x1f4

    move-object v1, v0

    move-object v2, p0

    invoke-direct/range {v1 .. v6}, Lcom/stagnationlab/sk/TransactionActivity$20;-><init>(Lcom/stagnationlab/sk/TransactionActivity;JJ)V

    .line 1445
    invoke-virtual {v0}, Lcom/stagnationlab/sk/TransactionActivity$20;->start()Landroid/os/CountDownTimer;

    move-result-object v0

    iput-object v0, p0, Lcom/stagnationlab/sk/TransactionActivity;->x:Landroid/os/CountDownTimer;

    :cond_2
    :goto_0
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;)V
    .locals 4

    .line 722
    iget-object v0, p0, Lcom/stagnationlab/sk/TransactionActivity;->Q:Lcom/stagnationlab/sk/PinFragment;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/stagnationlab/sk/PinFragment;->a(Z)V

    .line 724
    iput-boolean v1, p0, Lcom/stagnationlab/sk/TransactionActivity;->aa:Z

    .line 725
    iput-boolean v1, p0, Lcom/stagnationlab/sk/TransactionActivity;->ab:Z

    const/4 v0, 0x0

    .line 726
    iput-object v0, p0, Lcom/stagnationlab/sk/TransactionActivity;->ac:Lcom/stagnationlab/sk/c/a;

    .line 728
    iget-object v1, p0, Lcom/stagnationlab/sk/TransactionActivity;->B:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 729
    invoke-direct {p0}, Lcom/stagnationlab/sk/TransactionActivity;->D()V

    .line 730
    sget v0, Lcom/stagnationlab/sk/g;->a:I

    invoke-direct {p0, v0}, Lcom/stagnationlab/sk/TransactionActivity;->c(I)V

    .line 732
    iget-object v0, p0, Lcom/stagnationlab/sk/TransactionActivity;->B:Landroid/os/Handler;

    new-instance v1, Lcom/stagnationlab/sk/TransactionActivity$4;

    invoke-direct {v1, p0}, Lcom/stagnationlab/sk/TransactionActivity$4;-><init>(Lcom/stagnationlab/sk/TransactionActivity;)V

    sget v2, Lcom/stagnationlab/sk/TransactionActivity;->c:I

    sget v3, Lcom/stagnationlab/sk/g;->a:I

    add-int/2addr v2, v3

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 739
    iget-object v0, p0, Lcom/stagnationlab/sk/TransactionActivity;->n:Lcom/stagnationlab/sk/models/Transaction;

    invoke-virtual {v0}, Lcom/stagnationlab/sk/models/Transaction;->k()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 740
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 741
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v2, 0x1

    .line 743
    :goto_0
    iget v3, p0, Lcom/stagnationlab/sk/TransactionActivity;->j:I

    if-gt v2, v3, :cond_0

    .line 744
    rem-int/lit8 v3, v2, 0xa

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, "0"

    .line 745
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 748
    :cond_0
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 749
    new-instance p1, Lcom/stagnationlab/sk/c/a;

    sget-object v0, Lcom/stagnationlab/sk/c/b;->a:Lcom/stagnationlab/sk/c/b;

    invoke-direct {p1, v0}, Lcom/stagnationlab/sk/c/a;-><init>(Lcom/stagnationlab/sk/c/b;)V

    iput-object p1, p0, Lcom/stagnationlab/sk/TransactionActivity;->ac:Lcom/stagnationlab/sk/c/a;

    goto :goto_1

    .line 750
    :cond_1
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_2

    .line 751
    new-instance p1, Lcom/stagnationlab/sk/c/a;

    sget-object v0, Lcom/stagnationlab/sk/c/b;->l:Lcom/stagnationlab/sk/c/b;

    invoke-direct {p1, v0}, Lcom/stagnationlab/sk/c/a;-><init>(Lcom/stagnationlab/sk/c/b;)V

    iput-object p1, p0, Lcom/stagnationlab/sk/TransactionActivity;->ac:Lcom/stagnationlab/sk/c/a;

    .line 754
    :cond_2
    :goto_1
    iget-object p1, p0, Lcom/stagnationlab/sk/TransactionActivity;->B:Landroid/os/Handler;

    new-instance v0, Lcom/stagnationlab/sk/TransactionActivity$5;

    invoke-direct {v0, p0}, Lcom/stagnationlab/sk/TransactionActivity$5;-><init>(Lcom/stagnationlab/sk/TransactionActivity;)V

    sget v1, Lcom/stagnationlab/sk/TransactionActivity;->c:I

    mul-int/lit8 v2, v1, 0x2

    div-int/lit8 v2, v2, 0x3

    add-int/2addr v1, v2

    int-to-long v1, v1

    invoke-virtual {p1, v0, v1, v2}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void

    .line 765
    :cond_3
    iget-object v0, p0, Lcom/stagnationlab/sk/TransactionActivity;->l:Lcom/stagnationlab/sk/a/a;

    iget-object v1, p0, Lcom/stagnationlab/sk/TransactionActivity;->n:Lcom/stagnationlab/sk/models/Transaction;

    new-instance v2, Lcom/stagnationlab/sk/TransactionActivity$6;

    invoke-direct {v2, p0}, Lcom/stagnationlab/sk/TransactionActivity$6;-><init>(Lcom/stagnationlab/sk/TransactionActivity;)V

    invoke-virtual {v0, v1, p1, v2}, Lcom/stagnationlab/sk/a/a;->a(Lcom/stagnationlab/sk/models/Transaction;Ljava/lang/String;Lcom/stagnationlab/sk/a/a/h;)V

    return-void
.end method

.method public g()V
    .locals 4

    .line 1027
    invoke-direct {p0}, Lcom/stagnationlab/sk/TransactionActivity;->s()V

    .line 1028
    invoke-direct {p0}, Lcom/stagnationlab/sk/TransactionActivity;->B()V

    .line 1030
    iget-object v0, p0, Lcom/stagnationlab/sk/TransactionActivity;->o:Lcom/stagnationlab/sk/models/RpRequest;

    invoke-virtual {v0}, Lcom/stagnationlab/sk/models/RpRequest;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1031
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lcom/stagnationlab/sk/TransactionActivity$10;

    invoke-direct {v1, p0}, Lcom/stagnationlab/sk/TransactionActivity$10;-><init>(Lcom/stagnationlab/sk/TransactionActivity;)V

    sget v2, Lcom/stagnationlab/sk/TransactionActivity;->c:I

    mul-int/lit8 v2, v2, 0x5

    div-int/lit8 v2, v2, 0x4

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void

    .line 1041
    :cond_0
    iget-object v0, p0, Lcom/stagnationlab/sk/TransactionActivity;->l:Lcom/stagnationlab/sk/a/a;

    iget-object v1, p0, Lcom/stagnationlab/sk/TransactionActivity;->o:Lcom/stagnationlab/sk/models/RpRequest;

    new-instance v2, Lcom/stagnationlab/sk/TransactionActivity$11;

    invoke-direct {v2, p0}, Lcom/stagnationlab/sk/TransactionActivity$11;-><init>(Lcom/stagnationlab/sk/TransactionActivity;)V

    invoke-virtual {v0, v1, v2}, Lcom/stagnationlab/sk/a/a;->a(Lcom/stagnationlab/sk/models/RpRequest;Lcom/stagnationlab/sk/a/a/c;)V

    return-void
.end method

.method public h()V
    .locals 4

    .line 1055
    invoke-direct {p0}, Lcom/stagnationlab/sk/TransactionActivity;->s()V

    .line 1057
    invoke-direct {p0}, Lcom/stagnationlab/sk/TransactionActivity;->B()V

    .line 1059
    iget-object v0, p0, Lcom/stagnationlab/sk/TransactionActivity;->o:Lcom/stagnationlab/sk/models/RpRequest;

    invoke-virtual {v0}, Lcom/stagnationlab/sk/models/RpRequest;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1060
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lcom/stagnationlab/sk/TransactionActivity$13;

    invoke-direct {v1, p0}, Lcom/stagnationlab/sk/TransactionActivity$13;-><init>(Lcom/stagnationlab/sk/TransactionActivity;)V

    sget v2, Lcom/stagnationlab/sk/TransactionActivity;->c:I

    mul-int/lit8 v2, v2, 0x5

    div-int/lit8 v2, v2, 0x4

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void

    .line 1073
    :cond_0
    iget-object v0, p0, Lcom/stagnationlab/sk/TransactionActivity;->o:Lcom/stagnationlab/sk/models/RpRequest;

    invoke-virtual {v0}, Lcom/stagnationlab/sk/models/RpRequest;->f()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1074
    iget-object v0, p0, Lcom/stagnationlab/sk/TransactionActivity;->l:Lcom/stagnationlab/sk/a/a;

    iget-object v1, p0, Lcom/stagnationlab/sk/TransactionActivity;->o:Lcom/stagnationlab/sk/models/RpRequest;

    new-instance v2, Lcom/stagnationlab/sk/TransactionActivity$14;

    invoke-direct {v2, p0}, Lcom/stagnationlab/sk/TransactionActivity$14;-><init>(Lcom/stagnationlab/sk/TransactionActivity;)V

    invoke-virtual {v0, v1, v2}, Lcom/stagnationlab/sk/a/a;->a(Lcom/stagnationlab/sk/models/RpRequest;Lcom/stagnationlab/sk/a/a/g;)V

    goto :goto_0

    .line 1087
    :cond_1
    iget-object v0, p0, Lcom/stagnationlab/sk/TransactionActivity;->l:Lcom/stagnationlab/sk/a/a;

    iget-object v1, p0, Lcom/stagnationlab/sk/TransactionActivity;->o:Lcom/stagnationlab/sk/models/RpRequest;

    new-instance v2, Lcom/stagnationlab/sk/TransactionActivity$15;

    invoke-direct {v2, p0}, Lcom/stagnationlab/sk/TransactionActivity$15;-><init>(Lcom/stagnationlab/sk/TransactionActivity;)V

    invoke-virtual {v0, v1, v2}, Lcom/stagnationlab/sk/a/a;->a(Lcom/stagnationlab/sk/models/RpRequest;Lcom/stagnationlab/sk/a/a/t;)V

    :goto_0
    return-void
.end method

.method public onBackPressed()V
    .locals 2

    .line 1013
    iget-object v0, p0, Lcom/stagnationlab/sk/TransactionActivity;->h:Lcom/stagnationlab/sk/TransactionActivity$b;

    sget-object v1, Lcom/stagnationlab/sk/TransactionActivity$b;->d:Lcom/stagnationlab/sk/TransactionActivity$b;

    if-ne v0, v1, :cond_0

    .line 1014
    invoke-direct {p0}, Lcom/stagnationlab/sk/TransactionActivity;->x()V

    return-void

    .line 1018
    :cond_0
    iget-object v0, p0, Lcom/stagnationlab/sk/TransactionActivity;->h:Lcom/stagnationlab/sk/TransactionActivity$b;

    sget-object v1, Lcom/stagnationlab/sk/TransactionActivity$b;->b:Lcom/stagnationlab/sk/TransactionActivity$b;

    if-ne v0, v1, :cond_1

    .line 1019
    iget-object v0, p0, Lcom/stagnationlab/sk/TransactionActivity;->Q:Lcom/stagnationlab/sk/PinFragment;

    invoke-virtual {v0}, Lcom/stagnationlab/sk/PinFragment;->b()V

    return-void

    .line 1023
    :cond_1
    invoke-direct {p0}, Lcom/stagnationlab/sk/TransactionActivity;->x()V

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4

    .line 141
    invoke-super {p0, p1}, Lcom/stagnationlab/sk/b;->onCreate(Landroid/os/Bundle;)V

    const/4 v0, 0x1

    .line 142
    iput-boolean v0, p0, Lcom/stagnationlab/sk/TransactionActivity;->ad:Z

    .line 143
    invoke-virtual {p0}, Lcom/stagnationlab/sk/TransactionActivity;->a()V

    .line 144
    invoke-static {p0}, Lcom/stagnationlab/sk/a/d;->a(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 145
    invoke-virtual {p0}, Lcom/stagnationlab/sk/TransactionActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x2000

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    .line 149
    :cond_0
    invoke-virtual {p0}, Lcom/stagnationlab/sk/TransactionActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090007

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    sput v0, Lcom/stagnationlab/sk/TransactionActivity;->c:I

    const v0, 0x3e947ae1    # 0.29f

    const v1, 0x3f70a3d7    # 0.94f

    const v2, 0x3f666666    # 0.9f

    const v3, 0x3f266666    # 0.65f

    .line 150
    invoke-static {v0, v1, v2, v3}, Landroid/support/v4/f/b/f;->a(FFFF)Landroid/view/animation/Interpolator;

    move-result-object v0

    sput-object v0, Lcom/stagnationlab/sk/TransactionActivity;->d:Landroid/view/animation/Interpolator;

    const/4 v0, -0x1

    .line 151
    sput v0, Lcom/stagnationlab/sk/TransactionActivity;->e:I

    .line 153
    invoke-virtual {p0}, Lcom/stagnationlab/sk/TransactionActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f09000d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    sput v0, Lcom/stagnationlab/sk/TransactionActivity;->f:I

    const/4 v0, 0x0

    .line 155
    iput-boolean v0, p0, Lcom/stagnationlab/sk/TransactionActivity;->D:Z

    .line 157
    invoke-virtual {p0}, Lcom/stagnationlab/sk/TransactionActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/stagnationlab/sk/MyApplication;

    .line 159
    invoke-virtual {v0}, Lcom/stagnationlab/sk/MyApplication;->e()Lcom/stagnationlab/sk/a;

    move-result-object v1

    iput-object v1, p0, Lcom/stagnationlab/sk/TransactionActivity;->m:Lcom/stagnationlab/sk/a;

    .line 162
    new-instance v1, Lcom/stagnationlab/sk/TransactionActivity$1;

    invoke-direct {v1, p0, p1}, Lcom/stagnationlab/sk/TransactionActivity$1;-><init>(Lcom/stagnationlab/sk/TransactionActivity;Landroid/os/Bundle;)V

    invoke-virtual {v0, v1}, Lcom/stagnationlab/sk/MyApplication;->a(Lcom/stagnationlab/sk/a/a/a;)V

    return-void
.end method

.method protected onDestroy()V
    .locals 1

    .line 209
    invoke-super {p0}, Lcom/stagnationlab/sk/b;->onDestroy()V

    const/4 v0, 0x0

    .line 210
    iput-boolean v0, p0, Lcom/stagnationlab/sk/TransactionActivity;->ad:Z

    return-void
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 2

    .line 513
    invoke-super {p0, p1}, Lcom/stagnationlab/sk/b;->onNewIntent(Landroid/content/Intent;)V

    const-string v0, "reloadActivity"

    const/4 v1, 0x0

    .line 515
    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 519
    :cond_0
    iget-boolean v0, p0, Lcom/stagnationlab/sk/TransactionActivity;->D:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/stagnationlab/sk/TransactionActivity;->n:Lcom/stagnationlab/sk/models/Transaction;

    if-eqz v0, :cond_1

    .line 521
    invoke-virtual {v0}, Lcom/stagnationlab/sk/models/Transaction;->d()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    iget-object v0, p0, Lcom/stagnationlab/sk/TransactionActivity;->o:Lcom/stagnationlab/sk/models/RpRequest;

    if-eqz v0, :cond_4

    .line 522
    invoke-virtual {v0}, Lcom/stagnationlab/sk/models/RpRequest;->d()Z

    move-result v0

    if-nez v0, :cond_4

    .line 524
    :cond_2
    invoke-virtual {p0}, Lcom/stagnationlab/sk/TransactionActivity;->f()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 526
    invoke-virtual {p0, p1}, Lcom/stagnationlab/sk/TransactionActivity;->setIntent(Landroid/content/Intent;)V

    const/4 p1, 0x0

    .line 527
    invoke-direct {p0, p1}, Lcom/stagnationlab/sk/TransactionActivity;->b(Landroid/os/Bundle;)V

    goto :goto_0

    .line 530
    :cond_3
    invoke-virtual {p0}, Lcom/stagnationlab/sk/TransactionActivity;->finish()V

    .line 531
    invoke-virtual {p0, p1}, Lcom/stagnationlab/sk/TransactionActivity;->startActivity(Landroid/content/Intent;)V

    :cond_4
    :goto_0
    return-void
.end method

.method protected onPause()V
    .locals 1

    .line 442
    invoke-super {p0}, Lcom/stagnationlab/sk/b;->onPause()V

    .line 444
    iget-boolean v0, p0, Lcom/stagnationlab/sk/TransactionActivity;->af:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/stagnationlab/sk/TransactionActivity;->l:Lcom/stagnationlab/sk/a/a;

    if-eqz v0, :cond_0

    .line 445
    invoke-virtual {v0}, Lcom/stagnationlab/sk/a/a;->i()V

    const/4 v0, 0x0

    .line 446
    iput-boolean v0, p0, Lcom/stagnationlab/sk/TransactionActivity;->af:Z

    .line 449
    :cond_0
    invoke-direct {p0}, Lcom/stagnationlab/sk/TransactionActivity;->A()V

    .line 450
    invoke-direct {p0}, Lcom/stagnationlab/sk/TransactionActivity;->C()V

    return-void
.end method

.method protected onResume()V
    .locals 2

    .line 425
    invoke-super {p0}, Lcom/stagnationlab/sk/b;->onResume()V

    .line 427
    iget-boolean v0, p0, Lcom/stagnationlab/sk/TransactionActivity;->af:Z

    if-nez v0, :cond_1

    .line 428
    iget-object v0, p0, Lcom/stagnationlab/sk/TransactionActivity;->l:Lcom/stagnationlab/sk/a/a;

    const/4 v1, 0x1

    if-nez v0, :cond_0

    .line 429
    iput-boolean v1, p0, Lcom/stagnationlab/sk/TransactionActivity;->ag:Z

    goto :goto_0

    .line 431
    :cond_0
    invoke-virtual {v0, p0}, Lcom/stagnationlab/sk/a/a;->a(Landroid/app/Activity;)V

    .line 432
    iput-boolean v1, p0, Lcom/stagnationlab/sk/TransactionActivity;->af:Z

    .line 436
    :cond_1
    :goto_0
    invoke-direct {p0}, Lcom/stagnationlab/sk/TransactionActivity;->y()V

    .line 437
    invoke-direct {p0}, Lcom/stagnationlab/sk/TransactionActivity;->z()V

    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .line 417
    iget-object v0, p0, Lcom/stagnationlab/sk/TransactionActivity;->n:Lcom/stagnationlab/sk/models/Transaction;

    const-string v1, "transaction"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 418
    iget-object v0, p0, Lcom/stagnationlab/sk/TransactionActivity;->o:Lcom/stagnationlab/sk/models/RpRequest;

    const-string v1, "rpRequest"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 420
    invoke-super {p0, p1}, Lcom/stagnationlab/sk/b;->onSaveInstanceState(Landroid/os/Bundle;)V

    return-void
.end method

.method public setLoaderRotation(F)V
    .locals 3
    .annotation build Landroid/support/annotation/Keep;
    .end annotation

    .line 397
    iget-object v0, p0, Lcom/stagnationlab/sk/TransactionActivity;->K:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setRotation(F)V

    .line 398
    iput p1, p0, Lcom/stagnationlab/sk/TransactionActivity;->Y:F

    .line 400
    iget v0, p0, Lcom/stagnationlab/sk/TransactionActivity;->Z:F

    const/4 v1, 0x0

    cmpl-float v2, v0, v1

    if-ltz v2, :cond_1

    sub-float/2addr p1, v0

    cmpg-float v0, p1, v1

    if-gez v0, :cond_0

    const/high16 v0, 0x43b40000    # 360.0f

    add-float/2addr p1, v0

    :cond_0
    const/high16 v0, 0x42200000    # 40.0f

    cmpl-float p1, p1, v0

    if-ltz p1, :cond_1

    .line 410
    iget-object p1, p0, Lcom/stagnationlab/sk/TransactionActivity;->X:Landroid/animation/ObjectAnimator;

    invoke-virtual {p1}, Landroid/animation/ObjectAnimator;->cancel()V

    :cond_1
    return-void
.end method

.method public startActivity(Landroid/content/Intent;)V
    .locals 0

    .line 1103
    invoke-static {p1}, Lcom/stagnationlab/sk/ExitActivity;->a(Landroid/content/Intent;)V

    .line 1104
    invoke-super {p0, p1}, Lcom/stagnationlab/sk/b;->startActivity(Landroid/content/Intent;)V

    return-void
.end method
