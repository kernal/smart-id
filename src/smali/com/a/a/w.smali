.class public final enum Lcom/a/a/w;
.super Ljava/lang/Enum;
.source "Requirement.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/a/a/w;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/a/a/w;

.field public static final enum b:Lcom/a/a/w;

.field public static final enum c:Lcom/a/a/w;

.field private static final synthetic d:[Lcom/a/a/w;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 34
    new-instance v0, Lcom/a/a/w;

    const/4 v1, 0x0

    const-string v2, "REQUIRED"

    invoke-direct {v0, v2, v1}, Lcom/a/a/w;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/a/a/w;->a:Lcom/a/a/w;

    .line 40
    new-instance v0, Lcom/a/a/w;

    const/4 v2, 0x1

    const-string v3, "RECOMMENDED"

    invoke-direct {v0, v3, v2}, Lcom/a/a/w;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/a/a/w;->b:Lcom/a/a/w;

    .line 46
    new-instance v0, Lcom/a/a/w;

    const/4 v3, 0x2

    const-string v4, "OPTIONAL"

    invoke-direct {v0, v4, v3}, Lcom/a/a/w;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/a/a/w;->c:Lcom/a/a/w;

    const/4 v0, 0x3

    .line 28
    new-array v0, v0, [Lcom/a/a/w;

    sget-object v4, Lcom/a/a/w;->a:Lcom/a/a/w;

    aput-object v4, v0, v1

    sget-object v1, Lcom/a/a/w;->b:Lcom/a/a/w;

    aput-object v1, v0, v2

    sget-object v1, Lcom/a/a/w;->c:Lcom/a/a/w;

    aput-object v1, v0, v3

    sput-object v0, Lcom/a/a/w;->d:[Lcom/a/a/w;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 28
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/a/a/w;
    .locals 1

    .line 28
    const-class v0, Lcom/a/a/w;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/a/a/w;

    return-object p0
.end method

.method public static values()[Lcom/a/a/w;
    .locals 1

    .line 28
    sget-object v0, Lcom/a/a/w;->d:[Lcom/a/a/w;

    invoke-virtual {v0}, [Lcom/a/a/w;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/a/a/w;

    return-object v0
.end method
