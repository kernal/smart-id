.class public final Lcom/a/a/i;
.super Lcom/a/a/a;
.source "JWEAlgorithm.java"


# static fields
.field public static final b:Lcom/a/a/i;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final c:Lcom/a/a/i;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final d:Lcom/a/a/i;

.field public static final e:Lcom/a/a/i;

.field public static final f:Lcom/a/a/i;

.field public static final g:Lcom/a/a/i;

.field public static final h:Lcom/a/a/i;

.field public static final i:Lcom/a/a/i;

.field public static final j:Lcom/a/a/i;

.field public static final k:Lcom/a/a/i;

.field public static final l:Lcom/a/a/i;

.field public static final m:Lcom/a/a/i;

.field public static final n:Lcom/a/a/i;

.field public static final o:Lcom/a/a/i;

.field public static final p:Lcom/a/a/i;

.field public static final q:Lcom/a/a/i;

.field public static final r:Lcom/a/a/i;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 65
    new-instance v0, Lcom/a/a/i;

    sget-object v1, Lcom/a/a/w;->a:Lcom/a/a/w;

    const-string v2, "RSA1_5"

    invoke-direct {v0, v2, v1}, Lcom/a/a/i;-><init>(Ljava/lang/String;Lcom/a/a/w;)V

    sput-object v0, Lcom/a/a/i;->b:Lcom/a/a/i;

    .line 75
    new-instance v0, Lcom/a/a/i;

    sget-object v1, Lcom/a/a/w;->c:Lcom/a/a/w;

    const-string v2, "RSA-OAEP"

    invoke-direct {v0, v2, v1}, Lcom/a/a/i;-><init>(Ljava/lang/String;Lcom/a/a/w;)V

    sput-object v0, Lcom/a/a/i;->c:Lcom/a/a/i;

    .line 83
    new-instance v0, Lcom/a/a/i;

    sget-object v1, Lcom/a/a/w;->c:Lcom/a/a/w;

    const-string v2, "RSA-OAEP-256"

    invoke-direct {v0, v2, v1}, Lcom/a/a/i;-><init>(Ljava/lang/String;Lcom/a/a/w;)V

    sput-object v0, Lcom/a/a/i;->d:Lcom/a/a/i;

    .line 90
    new-instance v0, Lcom/a/a/i;

    sget-object v1, Lcom/a/a/w;->b:Lcom/a/a/w;

    const-string v2, "A128KW"

    invoke-direct {v0, v2, v1}, Lcom/a/a/i;-><init>(Ljava/lang/String;Lcom/a/a/w;)V

    sput-object v0, Lcom/a/a/i;->e:Lcom/a/a/i;

    .line 97
    new-instance v0, Lcom/a/a/i;

    sget-object v1, Lcom/a/a/w;->c:Lcom/a/a/w;

    const-string v2, "A192KW"

    invoke-direct {v0, v2, v1}, Lcom/a/a/i;-><init>(Ljava/lang/String;Lcom/a/a/w;)V

    sput-object v0, Lcom/a/a/i;->f:Lcom/a/a/i;

    .line 104
    new-instance v0, Lcom/a/a/i;

    sget-object v1, Lcom/a/a/w;->b:Lcom/a/a/w;

    const-string v2, "A256KW"

    invoke-direct {v0, v2, v1}, Lcom/a/a/i;-><init>(Ljava/lang/String;Lcom/a/a/w;)V

    sput-object v0, Lcom/a/a/i;->g:Lcom/a/a/i;

    .line 112
    new-instance v0, Lcom/a/a/i;

    sget-object v1, Lcom/a/a/w;->b:Lcom/a/a/w;

    const-string v2, "dir"

    invoke-direct {v0, v2, v1}, Lcom/a/a/i;-><init>(Ljava/lang/String;Lcom/a/a/w;)V

    sput-object v0, Lcom/a/a/i;->h:Lcom/a/a/i;

    .line 122
    new-instance v0, Lcom/a/a/i;

    sget-object v1, Lcom/a/a/w;->b:Lcom/a/a/w;

    const-string v2, "ECDH-ES"

    invoke-direct {v0, v2, v1}, Lcom/a/a/i;-><init>(Ljava/lang/String;Lcom/a/a/w;)V

    sput-object v0, Lcom/a/a/i;->i:Lcom/a/a/i;

    .line 131
    new-instance v0, Lcom/a/a/i;

    sget-object v1, Lcom/a/a/w;->b:Lcom/a/a/w;

    const-string v2, "ECDH-ES+A128KW"

    invoke-direct {v0, v2, v1}, Lcom/a/a/i;-><init>(Ljava/lang/String;Lcom/a/a/w;)V

    sput-object v0, Lcom/a/a/i;->j:Lcom/a/a/i;

    .line 140
    new-instance v0, Lcom/a/a/i;

    sget-object v1, Lcom/a/a/w;->c:Lcom/a/a/w;

    const-string v2, "ECDH-ES+A192KW"

    invoke-direct {v0, v2, v1}, Lcom/a/a/i;-><init>(Ljava/lang/String;Lcom/a/a/w;)V

    sput-object v0, Lcom/a/a/i;->k:Lcom/a/a/i;

    .line 149
    new-instance v0, Lcom/a/a/i;

    sget-object v1, Lcom/a/a/w;->b:Lcom/a/a/w;

    const-string v2, "ECDH-ES+A256KW"

    invoke-direct {v0, v2, v1}, Lcom/a/a/i;-><init>(Ljava/lang/String;Lcom/a/a/w;)V

    sput-object v0, Lcom/a/a/i;->l:Lcom/a/a/i;

    .line 155
    new-instance v0, Lcom/a/a/i;

    sget-object v1, Lcom/a/a/w;->c:Lcom/a/a/w;

    const-string v2, "A128GCMKW"

    invoke-direct {v0, v2, v1}, Lcom/a/a/i;-><init>(Ljava/lang/String;Lcom/a/a/w;)V

    sput-object v0, Lcom/a/a/i;->m:Lcom/a/a/i;

    .line 161
    new-instance v0, Lcom/a/a/i;

    sget-object v1, Lcom/a/a/w;->c:Lcom/a/a/w;

    const-string v2, "A192GCMKW"

    invoke-direct {v0, v2, v1}, Lcom/a/a/i;-><init>(Ljava/lang/String;Lcom/a/a/w;)V

    sput-object v0, Lcom/a/a/i;->n:Lcom/a/a/i;

    .line 167
    new-instance v0, Lcom/a/a/i;

    sget-object v1, Lcom/a/a/w;->c:Lcom/a/a/w;

    const-string v2, "A256GCMKW"

    invoke-direct {v0, v2, v1}, Lcom/a/a/i;-><init>(Ljava/lang/String;Lcom/a/a/w;)V

    sput-object v0, Lcom/a/a/i;->o:Lcom/a/a/i;

    .line 174
    new-instance v0, Lcom/a/a/i;

    sget-object v1, Lcom/a/a/w;->c:Lcom/a/a/w;

    const-string v2, "PBES2-HS256+A128KW"

    invoke-direct {v0, v2, v1}, Lcom/a/a/i;-><init>(Ljava/lang/String;Lcom/a/a/w;)V

    sput-object v0, Lcom/a/a/i;->p:Lcom/a/a/i;

    .line 181
    new-instance v0, Lcom/a/a/i;

    sget-object v1, Lcom/a/a/w;->c:Lcom/a/a/w;

    const-string v2, "PBES2-HS384+A192KW"

    invoke-direct {v0, v2, v1}, Lcom/a/a/i;-><init>(Ljava/lang/String;Lcom/a/a/w;)V

    sput-object v0, Lcom/a/a/i;->q:Lcom/a/a/i;

    .line 188
    new-instance v0, Lcom/a/a/i;

    sget-object v1, Lcom/a/a/w;->c:Lcom/a/a/w;

    const-string v2, "PBES2-HS512+A256KW"

    invoke-direct {v0, v2, v1}, Lcom/a/a/i;-><init>(Ljava/lang/String;Lcom/a/a/w;)V

    sput-object v0, Lcom/a/a/i;->r:Lcom/a/a/i;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    const/4 v0, 0x0

    .line 282
    invoke-direct {p0, p1, v0}, Lcom/a/a/a;-><init>(Ljava/lang/String;Lcom/a/a/w;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/a/a/w;)V
    .locals 0

    .line 271
    invoke-direct {p0, p1, p2}, Lcom/a/a/a;-><init>(Ljava/lang/String;Lcom/a/a/w;)V

    return-void
.end method

.method public static a(Ljava/lang/String;)Lcom/a/a/i;
    .locals 1

    .line 296
    sget-object v0, Lcom/a/a/i;->b:Lcom/a/a/i;

    invoke-virtual {v0}, Lcom/a/a/i;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 297
    sget-object p0, Lcom/a/a/i;->b:Lcom/a/a/i;

    return-object p0

    .line 298
    :cond_0
    sget-object v0, Lcom/a/a/i;->c:Lcom/a/a/i;

    invoke-virtual {v0}, Lcom/a/a/i;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 299
    sget-object p0, Lcom/a/a/i;->c:Lcom/a/a/i;

    return-object p0

    .line 300
    :cond_1
    sget-object v0, Lcom/a/a/i;->d:Lcom/a/a/i;

    invoke-virtual {v0}, Lcom/a/a/i;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 301
    sget-object p0, Lcom/a/a/i;->d:Lcom/a/a/i;

    return-object p0

    .line 302
    :cond_2
    sget-object v0, Lcom/a/a/i;->e:Lcom/a/a/i;

    invoke-virtual {v0}, Lcom/a/a/i;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 303
    sget-object p0, Lcom/a/a/i;->e:Lcom/a/a/i;

    return-object p0

    .line 304
    :cond_3
    sget-object v0, Lcom/a/a/i;->f:Lcom/a/a/i;

    invoke-virtual {v0}, Lcom/a/a/i;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 305
    sget-object p0, Lcom/a/a/i;->f:Lcom/a/a/i;

    return-object p0

    .line 306
    :cond_4
    sget-object v0, Lcom/a/a/i;->g:Lcom/a/a/i;

    invoke-virtual {v0}, Lcom/a/a/i;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 307
    sget-object p0, Lcom/a/a/i;->g:Lcom/a/a/i;

    return-object p0

    .line 308
    :cond_5
    sget-object v0, Lcom/a/a/i;->h:Lcom/a/a/i;

    invoke-virtual {v0}, Lcom/a/a/i;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 309
    sget-object p0, Lcom/a/a/i;->h:Lcom/a/a/i;

    return-object p0

    .line 310
    :cond_6
    sget-object v0, Lcom/a/a/i;->i:Lcom/a/a/i;

    invoke-virtual {v0}, Lcom/a/a/i;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 311
    sget-object p0, Lcom/a/a/i;->i:Lcom/a/a/i;

    return-object p0

    .line 312
    :cond_7
    sget-object v0, Lcom/a/a/i;->j:Lcom/a/a/i;

    invoke-virtual {v0}, Lcom/a/a/i;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 313
    sget-object p0, Lcom/a/a/i;->j:Lcom/a/a/i;

    return-object p0

    .line 314
    :cond_8
    sget-object v0, Lcom/a/a/i;->k:Lcom/a/a/i;

    invoke-virtual {v0}, Lcom/a/a/i;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 315
    sget-object p0, Lcom/a/a/i;->k:Lcom/a/a/i;

    return-object p0

    .line 316
    :cond_9
    sget-object v0, Lcom/a/a/i;->l:Lcom/a/a/i;

    invoke-virtual {v0}, Lcom/a/a/i;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 317
    sget-object p0, Lcom/a/a/i;->l:Lcom/a/a/i;

    return-object p0

    .line 318
    :cond_a
    sget-object v0, Lcom/a/a/i;->m:Lcom/a/a/i;

    invoke-virtual {v0}, Lcom/a/a/i;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 319
    sget-object p0, Lcom/a/a/i;->m:Lcom/a/a/i;

    return-object p0

    .line 320
    :cond_b
    sget-object v0, Lcom/a/a/i;->n:Lcom/a/a/i;

    invoke-virtual {v0}, Lcom/a/a/i;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 321
    sget-object p0, Lcom/a/a/i;->n:Lcom/a/a/i;

    return-object p0

    .line 322
    :cond_c
    sget-object v0, Lcom/a/a/i;->o:Lcom/a/a/i;

    invoke-virtual {v0}, Lcom/a/a/i;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 323
    sget-object p0, Lcom/a/a/i;->o:Lcom/a/a/i;

    return-object p0

    .line 324
    :cond_d
    sget-object v0, Lcom/a/a/i;->p:Lcom/a/a/i;

    invoke-virtual {v0}, Lcom/a/a/i;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 325
    sget-object p0, Lcom/a/a/i;->p:Lcom/a/a/i;

    return-object p0

    .line 326
    :cond_e
    sget-object v0, Lcom/a/a/i;->q:Lcom/a/a/i;

    invoke-virtual {v0}, Lcom/a/a/i;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 327
    sget-object p0, Lcom/a/a/i;->q:Lcom/a/a/i;

    return-object p0

    .line 328
    :cond_f
    sget-object v0, Lcom/a/a/i;->r:Lcom/a/a/i;

    invoke-virtual {v0}, Lcom/a/a/i;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 329
    sget-object p0, Lcom/a/a/i;->r:Lcom/a/a/i;

    return-object p0

    .line 331
    :cond_10
    new-instance v0, Lcom/a/a/i;

    invoke-direct {v0, p0}, Lcom/a/a/i;-><init>(Ljava/lang/String;)V

    return-object v0
.end method
