.class abstract Lcom/a/a/b;
.super Lcom/a/a/e;
.source "CommonSEHeader.java"


# instance fields
.field private final a:Ljava/net/URI;

.field private final b:Lcom/a/a/c/d;

.field private final c:Ljava/net/URI;

.field private final d:Lcom/a/a/d/c;

.field private final e:Lcom/a/a/d/c;

.field private final f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/a/a/d/a;",
            ">;"
        }
    .end annotation
.end field

.field private final g:Ljava/lang/String;


# direct methods
.method protected constructor <init>(Lcom/a/a/a;Lcom/a/a/h;Ljava/lang/String;Ljava/util/Set;Ljava/net/URI;Lcom/a/a/c/d;Ljava/net/URI;Lcom/a/a/d/c;Lcom/a/a/d/c;Ljava/util/List;Ljava/lang/String;Ljava/util/Map;Lcom/a/a/d/c;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/a/a/a;",
            "Lcom/a/a/h;",
            "Ljava/lang/String;",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/net/URI;",
            "Lcom/a/a/c/d;",
            "Ljava/net/URI;",
            "Lcom/a/a/d/c;",
            "Lcom/a/a/d/c;",
            "Ljava/util/List<",
            "Lcom/a/a/d/a;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;",
            "Lcom/a/a/d/c;",
            ")V"
        }
    .end annotation

    move-object v7, p0

    move-object/from16 v8, p10

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object/from16 v5, p12

    move-object/from16 v6, p13

    .line 149
    invoke-direct/range {v0 .. v6}, Lcom/a/a/e;-><init>(Lcom/a/a/a;Lcom/a/a/h;Ljava/lang/String;Ljava/util/Set;Ljava/util/Map;Lcom/a/a/d/c;)V

    move-object v0, p5

    .line 151
    iput-object v0, v7, Lcom/a/a/b;->a:Ljava/net/URI;

    move-object v0, p6

    .line 152
    iput-object v0, v7, Lcom/a/a/b;->b:Lcom/a/a/c/d;

    move-object/from16 v0, p7

    .line 153
    iput-object v0, v7, Lcom/a/a/b;->c:Ljava/net/URI;

    move-object/from16 v0, p8

    .line 154
    iput-object v0, v7, Lcom/a/a/b;->d:Lcom/a/a/d/c;

    move-object/from16 v0, p9

    .line 155
    iput-object v0, v7, Lcom/a/a/b;->e:Lcom/a/a/d/c;

    if-eqz v8, :cond_0

    .line 159
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v8}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, v7, Lcom/a/a/b;->f:Ljava/util/List;

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 161
    iput-object v0, v7, Lcom/a/a/b;->f:Ljava/util/List;

    :goto_0
    move-object/from16 v0, p11

    .line 164
    iput-object v0, v7, Lcom/a/a/b;->g:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .line 251
    iget-object v0, p0, Lcom/a/a/b;->g:Ljava/lang/String;

    return-object v0
.end method

.method public b()La/b/b/d;
    .locals 3

    .line 295
    invoke-super {p0}, Lcom/a/a/e;->b()La/b/b/d;

    move-result-object v0

    .line 297
    iget-object v1, p0, Lcom/a/a/b;->a:Ljava/net/URI;

    if-eqz v1, :cond_0

    .line 298
    invoke-virtual {v1}, Ljava/net/URI;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "jku"

    invoke-virtual {v0, v2, v1}, La/b/b/d;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 301
    :cond_0
    iget-object v1, p0, Lcom/a/a/b;->b:Lcom/a/a/c/d;

    if-eqz v1, :cond_1

    .line 302
    invoke-virtual {v1}, Lcom/a/a/c/d;->e()La/b/b/d;

    move-result-object v1

    const-string v2, "jwk"

    invoke-virtual {v0, v2, v1}, La/b/b/d;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 305
    :cond_1
    iget-object v1, p0, Lcom/a/a/b;->c:Ljava/net/URI;

    if-eqz v1, :cond_2

    .line 306
    invoke-virtual {v1}, Ljava/net/URI;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "x5u"

    invoke-virtual {v0, v2, v1}, La/b/b/d;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 309
    :cond_2
    iget-object v1, p0, Lcom/a/a/b;->d:Lcom/a/a/d/c;

    if-eqz v1, :cond_3

    .line 310
    invoke-virtual {v1}, Lcom/a/a/d/c;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "x5t"

    invoke-virtual {v0, v2, v1}, La/b/b/d;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 313
    :cond_3
    iget-object v1, p0, Lcom/a/a/b;->e:Lcom/a/a/d/c;

    if-eqz v1, :cond_4

    .line 314
    invoke-virtual {v1}, Lcom/a/a/d/c;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "x5t#S256"

    invoke-virtual {v0, v2, v1}, La/b/b/d;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 317
    :cond_4
    iget-object v1, p0, Lcom/a/a/b;->f:Ljava/util/List;

    if-eqz v1, :cond_5

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_5

    .line 318
    iget-object v1, p0, Lcom/a/a/b;->f:Ljava/util/List;

    const-string v2, "x5c"

    invoke-virtual {v0, v2, v1}, La/b/b/d;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 321
    :cond_5
    iget-object v1, p0, Lcom/a/a/b;->g:Ljava/lang/String;

    if-eqz v1, :cond_6

    const-string v2, "kid"

    .line 322
    invoke-virtual {v0, v2, v1}, La/b/b/d;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_6
    return-object v0
.end method
