.class public Lcom/a/a/q$a;
.super Ljava/lang/Object;
.source "JWSHeader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/a/a/q;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation


# instance fields
.field private final a:Lcom/a/a/p;

.field private b:Lcom/a/a/h;

.field private c:Ljava/lang/String;

.field private d:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private e:Ljava/net/URI;

.field private f:Lcom/a/a/c/d;

.field private g:Ljava/net/URI;

.field private h:Lcom/a/a/d/c;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field private i:Lcom/a/a/d/c;

.field private j:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/a/a/d/a;",
            ">;"
        }
    .end annotation
.end field

.field private k:Ljava/lang/String;

.field private l:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private m:Lcom/a/a/d/c;


# direct methods
.method public constructor <init>(Lcom/a/a/p;)V
    .locals 2

    .line 205
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 207
    invoke-virtual {p1}, Lcom/a/a/p;->a()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/a/a/a;->a:Lcom/a/a/a;

    invoke-virtual {v1}, Lcom/a/a/a;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 211
    iput-object p1, p0, Lcom/a/a/q$a;->a:Lcom/a/a/p;

    return-void

    .line 208
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "The JWS algorithm \"alg\" cannot be \"none\""

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method


# virtual methods
.method public a(Lcom/a/a/c/d;)Lcom/a/a/q$a;
    .locals 0

    .line 312
    iput-object p1, p0, Lcom/a/a/q$a;->f:Lcom/a/a/c/d;

    return-object p0
.end method

.method public a(Lcom/a/a/d/c;)Lcom/a/a/q$a;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 344
    iput-object p1, p0, Lcom/a/a/q$a;->h:Lcom/a/a/d/c;

    return-object p0
.end method

.method public a(Lcom/a/a/h;)Lcom/a/a/q$a;
    .locals 0

    .line 251
    iput-object p1, p0, Lcom/a/a/q$a;->b:Lcom/a/a/h;

    return-object p0
.end method

.method public a(Ljava/lang/String;)Lcom/a/a/q$a;
    .locals 0

    .line 266
    iput-object p1, p0, Lcom/a/a/q$a;->c:Ljava/lang/String;

    return-object p0
.end method

.method public a(Ljava/lang/String;Ljava/lang/Object;)Lcom/a/a/q$a;
    .locals 2

    .line 414
    invoke-static {}, Lcom/a/a/q;->f()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 418
    iget-object v0, p0, Lcom/a/a/q$a;->l:Ljava/util/Map;

    if-nez v0, :cond_0

    .line 419
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/a/a/q$a;->l:Ljava/util/Map;

    .line 422
    :cond_0
    iget-object v0, p0, Lcom/a/a/q$a;->l:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-object p0

    .line 415
    :cond_1
    new-instance p2, Ljava/lang/IllegalArgumentException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "The parameter name \""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "\" matches a registered name"

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p2
.end method

.method public a(Ljava/net/URI;)Lcom/a/a/q$a;
    .locals 0

    .line 297
    iput-object p1, p0, Lcom/a/a/q$a;->e:Ljava/net/URI;

    return-object p0
.end method

.method public a(Ljava/util/List;)Lcom/a/a/q$a;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/a/a/d/a;",
            ">;)",
            "Lcom/a/a/q$a;"
        }
    .end annotation

    .line 376
    iput-object p1, p0, Lcom/a/a/q$a;->j:Ljava/util/List;

    return-object p0
.end method

.method public a(Ljava/util/Set;)Lcom/a/a/q$a;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/a/a/q$a;"
        }
    .end annotation

    .line 282
    iput-object p1, p0, Lcom/a/a/q$a;->d:Ljava/util/Set;

    return-object p0
.end method

.method public a()Lcom/a/a/q;
    .locals 15

    .line 466
    new-instance v14, Lcom/a/a/q;

    iget-object v1, p0, Lcom/a/a/q$a;->a:Lcom/a/a/p;

    iget-object v2, p0, Lcom/a/a/q$a;->b:Lcom/a/a/h;

    iget-object v3, p0, Lcom/a/a/q$a;->c:Ljava/lang/String;

    iget-object v4, p0, Lcom/a/a/q$a;->d:Ljava/util/Set;

    iget-object v5, p0, Lcom/a/a/q$a;->e:Ljava/net/URI;

    iget-object v6, p0, Lcom/a/a/q$a;->f:Lcom/a/a/c/d;

    iget-object v7, p0, Lcom/a/a/q$a;->g:Ljava/net/URI;

    iget-object v8, p0, Lcom/a/a/q$a;->h:Lcom/a/a/d/c;

    iget-object v9, p0, Lcom/a/a/q$a;->i:Lcom/a/a/d/c;

    iget-object v10, p0, Lcom/a/a/q$a;->j:Ljava/util/List;

    iget-object v11, p0, Lcom/a/a/q$a;->k:Ljava/lang/String;

    iget-object v12, p0, Lcom/a/a/q$a;->l:Ljava/util/Map;

    iget-object v13, p0, Lcom/a/a/q$a;->m:Lcom/a/a/d/c;

    move-object v0, v14

    invoke-direct/range {v0 .. v13}, Lcom/a/a/q;-><init>(Lcom/a/a/p;Lcom/a/a/h;Ljava/lang/String;Ljava/util/Set;Ljava/net/URI;Lcom/a/a/c/d;Ljava/net/URI;Lcom/a/a/d/c;Lcom/a/a/d/c;Ljava/util/List;Ljava/lang/String;Ljava/util/Map;Lcom/a/a/d/c;)V

    return-object v14
.end method

.method public b(Lcom/a/a/d/c;)Lcom/a/a/q$a;
    .locals 0

    .line 360
    iput-object p1, p0, Lcom/a/a/q$a;->i:Lcom/a/a/d/c;

    return-object p0
.end method

.method public b(Ljava/lang/String;)Lcom/a/a/q$a;
    .locals 0

    .line 391
    iput-object p1, p0, Lcom/a/a/q$a;->k:Ljava/lang/String;

    return-object p0
.end method

.method public b(Ljava/net/URI;)Lcom/a/a/q$a;
    .locals 0

    .line 327
    iput-object p1, p0, Lcom/a/a/q$a;->g:Ljava/net/URI;

    return-object p0
.end method

.method public c(Lcom/a/a/d/c;)Lcom/a/a/q$a;
    .locals 0

    .line 454
    iput-object p1, p0, Lcom/a/a/q$a;->m:Lcom/a/a/d/c;

    return-object p0
.end method
