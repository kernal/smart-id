.class public Lcom/a/a/d/m;
.super Ljava/lang/Object;
.source "X509CertUtils.java"


# direct methods
.method public static a([B)Ljava/security/cert/X509Certificate;
    .locals 3

    const/4 v0, 0x0

    if-eqz p0, :cond_2

    .line 58
    array-length v1, p0

    if-nez v1, :cond_0

    goto :goto_0

    :cond_0
    :try_start_0
    const-string v1, "X.509"

    .line 64
    invoke-static {v1}, Ljava/security/cert/CertificateFactory;->getInstance(Ljava/lang/String;)Ljava/security/cert/CertificateFactory;

    move-result-object v1

    .line 65
    new-instance v2, Ljava/io/ByteArrayInputStream;

    invoke-direct {v2, p0}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-virtual {v1, v2}, Ljava/security/cert/CertificateFactory;->generateCertificate(Ljava/io/InputStream;)Ljava/security/cert/Certificate;

    move-result-object p0
    :try_end_0
    .catch Ljava/security/cert/CertificateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 70
    instance-of v1, p0, Ljava/security/cert/X509Certificate;

    if-nez v1, :cond_1

    return-object v0

    .line 74
    :cond_1
    check-cast p0, Ljava/security/cert/X509Certificate;

    return-object p0

    :catch_0
    :cond_2
    :goto_0
    return-object v0
.end method
