.class public Lcom/a/a/d/c;
.super Lcom/a/a/d/a;
.source "Base64URL.java"


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 0

    .line 51
    invoke-direct {p0, p1}, Lcom/a/a/d/a;-><init>(Ljava/lang/String;)V

    return-void
.end method

.method public static a(Ljava/lang/String;)Lcom/a/a/d/c;
    .locals 1

    .line 108
    sget-object v0, Lcom/a/a/d/k;->a:Ljava/nio/charset/Charset;

    invoke-virtual {p0, v0}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object p0

    invoke-static {p0}, Lcom/a/a/d/c;->a([B)Lcom/a/a/d/c;

    move-result-object p0

    return-object p0
.end method

.method public static a([B)Lcom/a/a/d/c;
    .locals 2

    .line 81
    new-instance v0, Lcom/a/a/d/c;

    const/4 v1, 0x1

    invoke-static {p0, v1}, Lcom/a/a/d/b;->a([BZ)Ljava/lang/String;

    move-result-object p0

    invoke-direct {v0, p0}, Lcom/a/a/d/c;-><init>(Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 1

    if-eqz p1, :cond_0

    .line 66
    instance-of v0, p1, Lcom/a/a/d/c;

    if-eqz v0, :cond_0

    .line 68
    invoke-virtual {p0}, Lcom/a/a/d/c;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method
