.class public final Lcom/a/a/v;
.super Ljava/lang/Object;
.source "Payload.java"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/a/a/v$a;
    }
.end annotation


# instance fields
.field private final a:Lcom/a/a/v$a;

.field private final b:La/b/b/d;

.field private final c:Ljava/lang/String;

.field private final d:[B

.field private final e:Lcom/a/a/d/c;

.field private final f:Lcom/a/a/r;

.field private final g:Lcom/a/b/b;


# direct methods
.method public constructor <init>(Lcom/a/a/d/c;)V
    .locals 1

    .line 247
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    .line 253
    iput-object v0, p0, Lcom/a/a/v;->b:La/b/b/d;

    .line 254
    iput-object v0, p0, Lcom/a/a/v;->c:Ljava/lang/String;

    .line 255
    iput-object v0, p0, Lcom/a/a/v;->d:[B

    .line 256
    iput-object p1, p0, Lcom/a/a/v;->e:Lcom/a/a/d/c;

    .line 257
    iput-object v0, p0, Lcom/a/a/v;->f:Lcom/a/a/r;

    .line 258
    iput-object v0, p0, Lcom/a/a/v;->g:Lcom/a/b/b;

    .line 260
    sget-object p1, Lcom/a/a/v$a;->d:Lcom/a/a/v$a;

    iput-object p1, p0, Lcom/a/a/v;->a:Lcom/a/a/v$a;

    return-void

    .line 250
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "The Base64URL-encoded object must not be null"

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    .line 201
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    .line 207
    iput-object v0, p0, Lcom/a/a/v;->b:La/b/b/d;

    .line 208
    iput-object p1, p0, Lcom/a/a/v;->c:Ljava/lang/String;

    .line 209
    iput-object v0, p0, Lcom/a/a/v;->d:[B

    .line 210
    iput-object v0, p0, Lcom/a/a/v;->e:Lcom/a/a/d/c;

    .line 211
    iput-object v0, p0, Lcom/a/a/v;->f:Lcom/a/a/r;

    .line 212
    iput-object v0, p0, Lcom/a/a/v;->g:Lcom/a/b/b;

    .line 214
    sget-object p1, Lcom/a/a/v$a;->b:Lcom/a/a/v$a;

    iput-object p1, p0, Lcom/a/a/v;->a:Lcom/a/a/v$a;

    return-void

    .line 204
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "The string must not be null"

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public constructor <init>([B)V
    .locals 1

    .line 224
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    .line 230
    iput-object v0, p0, Lcom/a/a/v;->b:La/b/b/d;

    .line 231
    iput-object v0, p0, Lcom/a/a/v;->c:Ljava/lang/String;

    .line 232
    iput-object p1, p0, Lcom/a/a/v;->d:[B

    .line 233
    iput-object v0, p0, Lcom/a/a/v;->e:Lcom/a/a/d/c;

    .line 234
    iput-object v0, p0, Lcom/a/a/v;->f:Lcom/a/a/r;

    .line 235
    iput-object v0, p0, Lcom/a/a/v;->g:Lcom/a/b/b;

    .line 237
    sget-object p1, Lcom/a/a/v$a;->c:Lcom/a/a/v$a;

    iput-object p1, p0, Lcom/a/a/v;->a:Lcom/a/a/v$a;

    return-void

    .line 227
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "The byte array must not be null"

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method private static a([B)Ljava/lang/String;
    .locals 2

    if-eqz p0, :cond_0

    .line 155
    new-instance v0, Ljava/lang/String;

    sget-object v1, Lcom/a/a/d/k;->a:Ljava/nio/charset/Charset;

    invoke-direct {v0, p0, v1}, Ljava/lang/String;-><init>([BLjava/nio/charset/Charset;)V

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method private static a(Ljava/lang/String;)[B
    .locals 1

    if-eqz p0, :cond_0

    .line 168
    sget-object v0, Lcom/a/a/d/k;->a:Ljava/nio/charset/Charset;

    invoke-virtual {p0, v0}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object p0

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return-object p0
.end method


# virtual methods
.method public a()[B
    .locals 1

    .line 408
    iget-object v0, p0, Lcom/a/a/v;->d:[B

    if-eqz v0, :cond_0

    return-object v0

    .line 413
    :cond_0
    iget-object v0, p0, Lcom/a/a/v;->e:Lcom/a/a/d/c;

    if-eqz v0, :cond_1

    .line 414
    invoke-virtual {v0}, Lcom/a/a/d/c;->a()[B

    move-result-object v0

    return-object v0

    .line 418
    :cond_1
    invoke-virtual {p0}, Lcom/a/a/v;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/a/a/v;->a(Ljava/lang/String;)[B

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .line 370
    iget-object v0, p0, Lcom/a/a/v;->c:Ljava/lang/String;

    if-eqz v0, :cond_0

    return-object v0

    .line 376
    :cond_0
    iget-object v0, p0, Lcom/a/a/v;->f:Lcom/a/a/r;

    if-eqz v0, :cond_2

    .line 378
    invoke-virtual {v0}, Lcom/a/a/r;->b()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 379
    iget-object v0, p0, Lcom/a/a/v;->f:Lcom/a/a/r;

    invoke-virtual {v0}, Lcom/a/a/r;->b()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 381
    :cond_1
    iget-object v0, p0, Lcom/a/a/v;->f:Lcom/a/a/r;

    invoke-virtual {v0}, Lcom/a/a/r;->g()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 384
    :cond_2
    iget-object v0, p0, Lcom/a/a/v;->b:La/b/b/d;

    if-eqz v0, :cond_3

    .line 386
    invoke-virtual {v0}, La/b/b/d;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 388
    :cond_3
    iget-object v0, p0, Lcom/a/a/v;->d:[B

    if-eqz v0, :cond_4

    .line 390
    invoke-static {v0}, Lcom/a/a/v;->a([B)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 392
    :cond_4
    iget-object v0, p0, Lcom/a/a/v;->e:Lcom/a/a/d/c;

    if-eqz v0, :cond_5

    .line 394
    invoke-virtual {v0}, Lcom/a/a/d/c;->d()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_5
    const/4 v0, 0x0

    return-object v0
.end method
