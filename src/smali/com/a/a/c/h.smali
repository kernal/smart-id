.class public final Lcom/a/a/c/h;
.super Ljava/lang/Object;
.source "KeyUse.java"


# static fields
.field public static final a:Lcom/a/a/c/h;

.field public static final b:Lcom/a/a/c/h;


# instance fields
.field private final c:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 46
    new-instance v0, Lcom/a/a/c/h;

    const-string v1, "sig"

    invoke-direct {v0, v1}, Lcom/a/a/c/h;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/a/a/c/h;->a:Lcom/a/a/c/h;

    .line 52
    new-instance v0, Lcom/a/a/c/h;

    const-string v1, "enc"

    invoke-direct {v0, v1}, Lcom/a/a/c/h;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/a/a/c/h;->b:Lcom/a/a/c/h;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    .line 67
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    if-eqz p1, :cond_0

    .line 72
    iput-object p1, p0, Lcom/a/a/c/h;->c:Ljava/lang/String;

    return-void

    .line 70
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "The key use identifier must not be null"

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public static a(Ljava/lang/String;)Lcom/a/a/c/h;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/text/ParseException;
        }
    .end annotation

    if-nez p0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 139
    :cond_0
    sget-object v0, Lcom/a/a/c/h;->a:Lcom/a/a/c/h;

    invoke-virtual {v0}, Lcom/a/a/c/h;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 140
    sget-object p0, Lcom/a/a/c/h;->a:Lcom/a/a/c/h;

    return-object p0

    .line 143
    :cond_1
    sget-object v0, Lcom/a/a/c/h;->b:Lcom/a/a/c/h;

    invoke-virtual {v0}, Lcom/a/a/c/h;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 144
    sget-object p0, Lcom/a/a/c/h;->b:Lcom/a/a/c/h;

    return-object p0

    .line 147
    :cond_2
    invoke-virtual {p0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 151
    new-instance v0, Lcom/a/a/c/h;

    invoke-direct {v0, p0}, Lcom/a/a/c/h;-><init>(Ljava/lang/String;)V

    return-object v0

    .line 148
    :cond_3
    new-instance p0, Ljava/text/ParseException;

    const/4 v0, 0x0

    const-string v1, "JWK use value must not be empty or blank"

    invoke-direct {p0, v1, v0}, Ljava/text/ParseException;-><init>(Ljava/lang/String;I)V

    throw p0
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .line 83
    iget-object v0, p0, Lcom/a/a/c/h;->c:Ljava/lang/String;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1

    if-ne p0, p1, :cond_0

    const/4 p1, 0x1

    return p1

    .line 109
    :cond_0
    instance-of v0, p1, Lcom/a/a/c/h;

    if-nez v0, :cond_1

    const/4 p1, 0x0

    return p1

    .line 110
    :cond_1
    check-cast p1, Lcom/a/a/c/h;

    .line 111
    iget-object v0, p0, Lcom/a/a/c/h;->c:Ljava/lang/String;

    iget-object p1, p1, Lcom/a/a/c/h;->c:Ljava/lang/String;

    invoke-static {v0, p1}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method public hashCode()I
    .locals 3

    const/4 v0, 0x1

    .line 117
    new-array v0, v0, [Ljava/lang/Object;

    iget-object v1, p0, Lcom/a/a/c/h;->c:Ljava/lang/String;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    invoke-static {v0}, Ljava/util/Objects;->hash([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .line 102
    invoke-virtual {p0}, Lcom/a/a/c/h;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
