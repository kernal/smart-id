.class public final Lcom/a/a/c/b;
.super Lcom/a/a/c/d;
.source "ECKey.java"


# static fields
.field public static final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Lcom/a/a/c/a;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final b:Lcom/a/a/c/a;

.field private final c:Lcom/a/a/d/c;

.field private final d:Lcom/a/a/d/c;

.field private final e:Lcom/a/a/d/c;

.field private final f:Ljava/security/PrivateKey;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .line 121
    new-instance v0, Ljava/util/HashSet;

    const/4 v1, 0x4

    new-array v1, v1, [Lcom/a/a/c/a;

    sget-object v2, Lcom/a/a/c/a;->a:Lcom/a/a/c/a;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    sget-object v2, Lcom/a/a/c/a;->b:Lcom/a/a/c/a;

    const/4 v3, 0x1

    aput-object v2, v1, v3

    sget-object v2, Lcom/a/a/c/a;->c:Lcom/a/a/c/a;

    const/4 v3, 0x2

    aput-object v2, v1, v3

    sget-object v2, Lcom/a/a/c/a;->d:Lcom/a/a/c/a;

    const/4 v3, 0x3

    aput-object v2, v1, v3

    .line 122
    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 121
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    sput-object v0, Lcom/a/a/c/b;->a:Ljava/util/Set;

    return-void
.end method

.method public constructor <init>(Lcom/a/a/c/a;Lcom/a/a/d/c;Lcom/a/a/d/c;Lcom/a/a/c/h;Ljava/util/Set;Lcom/a/a/a;Ljava/lang/String;Ljava/net/URI;Lcom/a/a/d/c;Lcom/a/a/d/c;Ljava/util/List;Ljava/security/KeyStore;)V
    .locals 15
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/a/a/c/a;",
            "Lcom/a/a/d/c;",
            "Lcom/a/a/d/c;",
            "Lcom/a/a/c/h;",
            "Ljava/util/Set<",
            "Lcom/a/a/c/f;",
            ">;",
            "Lcom/a/a/a;",
            "Ljava/lang/String;",
            "Ljava/net/URI;",
            "Lcom/a/a/d/c;",
            "Lcom/a/a/d/c;",
            "Ljava/util/List<",
            "Lcom/a/a/d/a;",
            ">;",
            "Ljava/security/KeyStore;",
            ")V"
        }
    .end annotation

    move-object v11, p0

    move-object/from16 v12, p1

    move-object/from16 v13, p2

    move-object/from16 v14, p3

    .line 706
    sget-object v1, Lcom/a/a/c/g;->a:Lcom/a/a/c/g;

    move-object v0, p0

    move-object/from16 v2, p4

    move-object/from16 v3, p5

    move-object/from16 v4, p6

    move-object/from16 v5, p7

    move-object/from16 v6, p8

    move-object/from16 v7, p9

    move-object/from16 v8, p10

    move-object/from16 v9, p11

    move-object/from16 v10, p12

    invoke-direct/range {v0 .. v10}, Lcom/a/a/c/d;-><init>(Lcom/a/a/c/g;Lcom/a/a/c/h;Ljava/util/Set;Lcom/a/a/a;Ljava/lang/String;Ljava/net/URI;Lcom/a/a/d/c;Lcom/a/a/d/c;Ljava/util/List;Ljava/security/KeyStore;)V

    if-eqz v12, :cond_2

    .line 712
    iput-object v12, v11, Lcom/a/a/c/b;->b:Lcom/a/a/c/a;

    if-eqz v13, :cond_1

    .line 718
    iput-object v13, v11, Lcom/a/a/c/b;->c:Lcom/a/a/d/c;

    if-eqz v14, :cond_0

    .line 724
    iput-object v14, v11, Lcom/a/a/c/b;->d:Lcom/a/a/d/c;

    .line 726
    invoke-static/range {p1 .. p3}, Lcom/a/a/c/b;->a(Lcom/a/a/c/a;Lcom/a/a/d/c;Lcom/a/a/d/c;)V

    .line 728
    invoke-virtual {p0}, Lcom/a/a/c/b;->f()Ljava/util/List;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/a/a/c/b;->a(Ljava/util/List;)V

    const/4 v0, 0x0

    .line 730
    iput-object v0, v11, Lcom/a/a/c/b;->e:Lcom/a/a/d/c;

    .line 732
    iput-object v0, v11, Lcom/a/a/c/b;->f:Ljava/security/PrivateKey;

    return-void

    .line 721
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "The \'y\' coordinate must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 715
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "The \'x\' coordinate must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 709
    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "The curve must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public constructor <init>(Lcom/a/a/c/a;Lcom/a/a/d/c;Lcom/a/a/d/c;Lcom/a/a/d/c;Lcom/a/a/c/h;Ljava/util/Set;Lcom/a/a/a;Ljava/lang/String;Ljava/net/URI;Lcom/a/a/d/c;Lcom/a/a/d/c;Ljava/util/List;Ljava/security/KeyStore;)V
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/a/a/c/a;",
            "Lcom/a/a/d/c;",
            "Lcom/a/a/d/c;",
            "Lcom/a/a/d/c;",
            "Lcom/a/a/c/h;",
            "Ljava/util/Set<",
            "Lcom/a/a/c/f;",
            ">;",
            "Lcom/a/a/a;",
            "Ljava/lang/String;",
            "Ljava/net/URI;",
            "Lcom/a/a/d/c;",
            "Lcom/a/a/d/c;",
            "Ljava/util/List<",
            "Lcom/a/a/d/a;",
            ">;",
            "Ljava/security/KeyStore;",
            ")V"
        }
    .end annotation

    move-object/from16 v11, p0

    move-object/from16 v12, p1

    move-object/from16 v13, p2

    move-object/from16 v14, p3

    move-object/from16 v15, p4

    .line 775
    sget-object v1, Lcom/a/a/c/g;->a:Lcom/a/a/c/g;

    move-object/from16 v0, p0

    move-object/from16 v2, p5

    move-object/from16 v3, p6

    move-object/from16 v4, p7

    move-object/from16 v5, p8

    move-object/from16 v6, p9

    move-object/from16 v7, p10

    move-object/from16 v8, p11

    move-object/from16 v9, p12

    move-object/from16 v10, p13

    invoke-direct/range {v0 .. v10}, Lcom/a/a/c/d;-><init>(Lcom/a/a/c/g;Lcom/a/a/c/h;Ljava/util/Set;Lcom/a/a/a;Ljava/lang/String;Ljava/net/URI;Lcom/a/a/d/c;Lcom/a/a/d/c;Ljava/util/List;Ljava/security/KeyStore;)V

    if-eqz v12, :cond_3

    .line 781
    iput-object v12, v11, Lcom/a/a/c/b;->b:Lcom/a/a/c/a;

    if-eqz v13, :cond_2

    .line 787
    iput-object v13, v11, Lcom/a/a/c/b;->c:Lcom/a/a/d/c;

    if-eqz v14, :cond_1

    .line 793
    iput-object v14, v11, Lcom/a/a/c/b;->d:Lcom/a/a/d/c;

    .line 795
    invoke-static/range {p1 .. p3}, Lcom/a/a/c/b;->a(Lcom/a/a/c/a;Lcom/a/a/d/c;Lcom/a/a/d/c;)V

    .line 797
    invoke-virtual/range {p0 .. p0}, Lcom/a/a/c/b;->f()Ljava/util/List;

    move-result-object v0

    invoke-direct {v11, v0}, Lcom/a/a/c/b;->a(Ljava/util/List;)V

    if-eqz v15, :cond_0

    .line 803
    iput-object v15, v11, Lcom/a/a/c/b;->e:Lcom/a/a/d/c;

    const/4 v0, 0x0

    .line 805
    iput-object v0, v11, Lcom/a/a/c/b;->f:Ljava/security/PrivateKey;

    return-void

    .line 800
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "The \'d\' coordinate must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 790
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "The \'y\' coordinate must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 784
    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "The \'x\' coordinate must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 778
    :cond_3
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "The curve must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static a(La/b/b/d;)Lcom/a/a/c/b;
    .locals 16
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/text/ParseException;
        }
    .end annotation

    move-object/from16 v0, p0

    const-string v1, "crv"

    .line 1386
    invoke-static {v0, v1}, Lcom/a/a/d/i;->b(La/b/b/d;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/a/a/c/a;->a(Ljava/lang/String;)Lcom/a/a/c/a;

    move-result-object v3

    .line 1387
    new-instance v4, Lcom/a/a/d/c;

    const-string v1, "x"

    invoke-static {v0, v1}, Lcom/a/a/d/i;->b(La/b/b/d;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v4, v1}, Lcom/a/a/d/c;-><init>(Ljava/lang/String;)V

    .line 1388
    new-instance v5, Lcom/a/a/d/c;

    const-string v1, "y"

    invoke-static {v0, v1}, Lcom/a/a/d/i;->b(La/b/b/d;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v5, v1}, Lcom/a/a/d/c;-><init>(Ljava/lang/String;)V

    .line 1391
    invoke-static/range {p0 .. p0}, Lcom/a/a/c/e;->a(La/b/b/d;)Lcom/a/a/c/g;

    move-result-object v1

    .line 1393
    sget-object v2, Lcom/a/a/c/g;->a:Lcom/a/a/c/g;

    const/4 v15, 0x0

    if-ne v1, v2, :cond_2

    const/4 v1, 0x0

    const-string v2, "d"

    .line 1399
    invoke-virtual {v0, v2}, La/b/b/d;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    if-eqz v6, :cond_0

    .line 1400
    new-instance v1, Lcom/a/a/d/c;

    invoke-static {v0, v2}, Lcom/a/a/d/i;->b(La/b/b/d;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/a/a/d/c;-><init>(Ljava/lang/String;)V

    :cond_0
    move-object v6, v1

    if-nez v6, :cond_1

    .line 1407
    :try_start_0
    new-instance v1, Lcom/a/a/c/b;

    .line 1408
    invoke-static/range {p0 .. p0}, Lcom/a/a/c/e;->b(La/b/b/d;)Lcom/a/a/c/h;

    move-result-object v6

    .line 1409
    invoke-static/range {p0 .. p0}, Lcom/a/a/c/e;->c(La/b/b/d;)Ljava/util/Set;

    move-result-object v7

    .line 1410
    invoke-static/range {p0 .. p0}, Lcom/a/a/c/e;->d(La/b/b/d;)Lcom/a/a/a;

    move-result-object v8

    .line 1411
    invoke-static/range {p0 .. p0}, Lcom/a/a/c/e;->e(La/b/b/d;)Ljava/lang/String;

    move-result-object v9

    .line 1412
    invoke-static/range {p0 .. p0}, Lcom/a/a/c/e;->f(La/b/b/d;)Ljava/net/URI;

    move-result-object v10

    .line 1413
    invoke-static/range {p0 .. p0}, Lcom/a/a/c/e;->g(La/b/b/d;)Lcom/a/a/d/c;

    move-result-object v11

    .line 1414
    invoke-static/range {p0 .. p0}, Lcom/a/a/c/e;->h(La/b/b/d;)Lcom/a/a/d/c;

    move-result-object v12

    .line 1415
    invoke-static/range {p0 .. p0}, Lcom/a/a/c/e;->i(La/b/b/d;)Ljava/util/List;

    move-result-object v13

    const/4 v14, 0x0

    move-object v2, v1

    invoke-direct/range {v2 .. v14}, Lcom/a/a/c/b;-><init>(Lcom/a/a/c/a;Lcom/a/a/d/c;Lcom/a/a/d/c;Lcom/a/a/c/h;Ljava/util/Set;Lcom/a/a/a;Ljava/lang/String;Ljava/net/URI;Lcom/a/a/d/c;Lcom/a/a/d/c;Ljava/util/List;Ljava/security/KeyStore;)V

    return-object v1

    .line 1420
    :cond_1
    new-instance v1, Lcom/a/a/c/b;

    .line 1421
    invoke-static/range {p0 .. p0}, Lcom/a/a/c/e;->b(La/b/b/d;)Lcom/a/a/c/h;

    move-result-object v7

    .line 1422
    invoke-static/range {p0 .. p0}, Lcom/a/a/c/e;->c(La/b/b/d;)Ljava/util/Set;

    move-result-object v8

    .line 1423
    invoke-static/range {p0 .. p0}, Lcom/a/a/c/e;->d(La/b/b/d;)Lcom/a/a/a;

    move-result-object v9

    .line 1424
    invoke-static/range {p0 .. p0}, Lcom/a/a/c/e;->e(La/b/b/d;)Ljava/lang/String;

    move-result-object v10

    .line 1425
    invoke-static/range {p0 .. p0}, Lcom/a/a/c/e;->f(La/b/b/d;)Ljava/net/URI;

    move-result-object v11

    .line 1426
    invoke-static/range {p0 .. p0}, Lcom/a/a/c/e;->g(La/b/b/d;)Lcom/a/a/d/c;

    move-result-object v12

    .line 1427
    invoke-static/range {p0 .. p0}, Lcom/a/a/c/e;->h(La/b/b/d;)Lcom/a/a/d/c;

    move-result-object v13

    .line 1428
    invoke-static/range {p0 .. p0}, Lcom/a/a/c/e;->i(La/b/b/d;)Ljava/util/List;

    move-result-object v14

    const/4 v0, 0x0

    move-object v2, v1

    move-object v15, v0

    invoke-direct/range {v2 .. v15}, Lcom/a/a/c/b;-><init>(Lcom/a/a/c/a;Lcom/a/a/d/c;Lcom/a/a/d/c;Lcom/a/a/d/c;Lcom/a/a/c/h;Ljava/util/Set;Lcom/a/a/a;Ljava/lang/String;Ljava/net/URI;Lcom/a/a/d/c;Lcom/a/a/d/c;Ljava/util/List;Ljava/security/KeyStore;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v1

    :catch_0
    move-exception v0

    .line 1435
    new-instance v1, Ljava/text/ParseException;

    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->getMessage()Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x0

    invoke-direct {v1, v0, v2}, Ljava/text/ParseException;-><init>(Ljava/lang/String;I)V

    throw v1

    :cond_2
    const/4 v2, 0x0

    .line 1394
    new-instance v0, Ljava/text/ParseException;

    const-string v1, "The key type \"kty\" must be EC"

    invoke-direct {v0, v1, v2}, Ljava/text/ParseException;-><init>(Ljava/lang/String;I)V

    throw v0
.end method

.method private static a(Lcom/a/a/c/a;Lcom/a/a/d/c;Lcom/a/a/d/c;)V
    .locals 1

    .line 661
    sget-object v0, Lcom/a/a/c/b;->a:Ljava/util/Set;

    invoke-interface {v0, p0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 665
    invoke-virtual {p1}, Lcom/a/a/d/c;->c()Ljava/math/BigInteger;

    move-result-object p1

    invoke-virtual {p2}, Lcom/a/a/d/c;->c()Ljava/math/BigInteger;

    move-result-object p2

    invoke-virtual {p0}, Lcom/a/a/c/a;->b()Ljava/security/spec/ECParameterSpec;

    move-result-object v0

    invoke-static {p1, p2, v0}, Lcom/a/a/a/b/b;->a(Ljava/math/BigInteger;Ljava/math/BigInteger;Ljava/security/spec/ECParameterSpec;)Z

    move-result p1

    if-eqz p1, :cond_0

    return-void

    .line 666
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "Invalid EC JWK: The \'x\' and \'y\' public coordinates are not on the "

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string p0, " curve"

    invoke-virtual {p2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {p1, p0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 662
    :cond_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "Unknown / unsupported curve: "

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {p1, p0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method private a(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/security/cert/X509Certificate;",
            ">;)V"
        }
    .end annotation

    if-nez p1, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x0

    .line 1279
    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/security/cert/X509Certificate;

    invoke-virtual {p0, p1}, Lcom/a/a/c/b;->a(Ljava/security/cert/X509Certificate;)Z

    move-result p1

    if-eqz p1, :cond_1

    return-void

    .line 1280
    :cond_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "The public subject key info of the first X.509 certificate in the chain must match the JWK type and public parameters"

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method


# virtual methods
.method public a()Lcom/a/a/d/c;
    .locals 1

    .line 1011
    iget-object v0, p0, Lcom/a/a/c/b;->c:Lcom/a/a/d/c;

    return-object v0
.end method

.method public a(Ljava/security/cert/X509Certificate;)Z
    .locals 3

    const/4 p1, 0x0

    .line 1247
    :try_start_0
    invoke-virtual {p0}, Lcom/a/a/c/b;->f()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/security/cert/X509Certificate;

    invoke-virtual {v0}, Ljava/security/cert/X509Certificate;->getPublicKey()Ljava/security/PublicKey;

    move-result-object v0

    check-cast v0, Ljava/security/interfaces/ECPublicKey;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1253
    invoke-virtual {p0}, Lcom/a/a/c/b;->a()Lcom/a/a/d/c;

    move-result-object v1

    invoke-virtual {v1}, Lcom/a/a/d/c;->c()Ljava/math/BigInteger;

    move-result-object v1

    invoke-interface {v0}, Ljava/security/interfaces/ECPublicKey;->getW()Ljava/security/spec/ECPoint;

    move-result-object v2

    invoke-virtual {v2}, Ljava/security/spec/ECPoint;->getAffineX()Ljava/math/BigInteger;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/math/BigInteger;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    return p1

    .line 1256
    :cond_0
    invoke-virtual {p0}, Lcom/a/a/c/b;->c()Lcom/a/a/d/c;

    move-result-object v1

    invoke-virtual {v1}, Lcom/a/a/d/c;->c()Ljava/math/BigInteger;

    move-result-object v1

    invoke-interface {v0}, Ljava/security/interfaces/ECPublicKey;->getW()Ljava/security/spec/ECPoint;

    move-result-object v0

    invoke-virtual {v0}, Ljava/security/spec/ECPoint;->getAffineY()Ljava/math/BigInteger;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/math/BigInteger;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    return p1

    :cond_1
    const/4 p1, 0x1

    :catch_0
    return p1
.end method

.method public c()Lcom/a/a/d/c;
    .locals 1

    .line 1023
    iget-object v0, p0, Lcom/a/a/c/b;->d:Lcom/a/a/d/c;

    return-object v0
.end method

.method public d()Z
    .locals 1

    .line 1300
    iget-object v0, p0, Lcom/a/a/c/b;->e:Lcom/a/a/d/c;

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/a/a/c/b;->f:Ljava/security/PrivateKey;

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method public e()La/b/b/d;
    .locals 3

    .line 1337
    invoke-super {p0}, Lcom/a/a/c/d;->e()La/b/b/d;

    move-result-object v0

    .line 1340
    iget-object v1, p0, Lcom/a/a/c/b;->b:Lcom/a/a/c/a;

    invoke-virtual {v1}, Lcom/a/a/c/a;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "crv"

    invoke-virtual {v0, v2, v1}, La/b/b/d;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1341
    iget-object v1, p0, Lcom/a/a/c/b;->c:Lcom/a/a/d/c;

    invoke-virtual {v1}, Lcom/a/a/d/c;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "x"

    invoke-virtual {v0, v2, v1}, La/b/b/d;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1342
    iget-object v1, p0, Lcom/a/a/c/b;->d:Lcom/a/a/d/c;

    invoke-virtual {v1}, Lcom/a/a/d/c;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "y"

    invoke-virtual {v0, v2, v1}, La/b/b/d;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1344
    iget-object v1, p0, Lcom/a/a/c/b;->e:Lcom/a/a/d/c;

    if-eqz v1, :cond_0

    .line 1345
    invoke-virtual {v1}, Lcom/a/a/d/c;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "d"

    invoke-virtual {v0, v2, v1}, La/b/b/d;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p0, p1, :cond_0

    return v0

    .line 1567
    :cond_0
    instance-of v1, p1, Lcom/a/a/c/b;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 1568
    :cond_1
    invoke-super {p0, p1}, Lcom/a/a/c/d;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    return v2

    .line 1569
    :cond_2
    check-cast p1, Lcom/a/a/c/b;

    .line 1570
    iget-object v1, p0, Lcom/a/a/c/b;->b:Lcom/a/a/c/a;

    iget-object v3, p1, Lcom/a/a/c/b;->b:Lcom/a/a/c/a;

    invoke-static {v1, v3}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/a/a/c/b;->c:Lcom/a/a/d/c;

    iget-object v3, p1, Lcom/a/a/c/b;->c:Lcom/a/a/d/c;

    .line 1571
    invoke-static {v1, v3}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/a/a/c/b;->d:Lcom/a/a/d/c;

    iget-object v3, p1, Lcom/a/a/c/b;->d:Lcom/a/a/d/c;

    .line 1572
    invoke-static {v1, v3}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/a/a/c/b;->e:Lcom/a/a/d/c;

    iget-object v3, p1, Lcom/a/a/c/b;->e:Lcom/a/a/d/c;

    .line 1573
    invoke-static {v1, v3}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/a/a/c/b;->f:Ljava/security/PrivateKey;

    iget-object p1, p1, Lcom/a/a/c/b;->f:Ljava/security/PrivateKey;

    .line 1574
    invoke-static {v1, p1}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_3

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    const/4 v0, 0x6

    .line 1580
    new-array v0, v0, [Ljava/lang/Object;

    invoke-super {p0}, Lcom/a/a/c/d;->hashCode()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/a/a/c/b;->b:Lcom/a/a/c/a;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/a/a/c/b;->c:Lcom/a/a/d/c;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/a/a/c/b;->d:Lcom/a/a/d/c;

    const/4 v2, 0x3

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/a/a/c/b;->e:Lcom/a/a/d/c;

    const/4 v2, 0x4

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/a/a/c/b;->f:Ljava/security/PrivateKey;

    const/4 v2, 0x5

    aput-object v1, v0, v2

    invoke-static {v0}, Ljava/util/Objects;->hash([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
