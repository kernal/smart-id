.class public final enum Lcom/a/a/c/f;
.super Ljava/lang/Enum;
.source "KeyOperation.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/a/a/c/f;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/a/a/c/f;

.field public static final enum b:Lcom/a/a/c/f;

.field public static final enum c:Lcom/a/a/c/f;

.field public static final enum d:Lcom/a/a/c/f;

.field public static final enum e:Lcom/a/a/c/f;

.field public static final enum f:Lcom/a/a/c/f;

.field public static final enum g:Lcom/a/a/c/f;

.field public static final enum h:Lcom/a/a/c/f;

.field private static final synthetic j:[Lcom/a/a/c/f;


# instance fields
.field private final i:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 11

    .line 53
    new-instance v0, Lcom/a/a/c/f;

    const/4 v1, 0x0

    const-string v2, "SIGN"

    const-string v3, "sign"

    invoke-direct {v0, v2, v1, v3}, Lcom/a/a/c/f;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/a/a/c/f;->a:Lcom/a/a/c/f;

    .line 59
    new-instance v0, Lcom/a/a/c/f;

    const/4 v2, 0x1

    const-string v3, "VERIFY"

    const-string v4, "verify"

    invoke-direct {v0, v3, v2, v4}, Lcom/a/a/c/f;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/a/a/c/f;->b:Lcom/a/a/c/f;

    .line 65
    new-instance v0, Lcom/a/a/c/f;

    const/4 v3, 0x2

    const-string v4, "ENCRYPT"

    const-string v5, "encrypt"

    invoke-direct {v0, v4, v3, v5}, Lcom/a/a/c/f;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/a/a/c/f;->c:Lcom/a/a/c/f;

    .line 71
    new-instance v0, Lcom/a/a/c/f;

    const/4 v4, 0x3

    const-string v5, "DECRYPT"

    const-string v6, "decrypt"

    invoke-direct {v0, v5, v4, v6}, Lcom/a/a/c/f;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/a/a/c/f;->d:Lcom/a/a/c/f;

    .line 77
    new-instance v0, Lcom/a/a/c/f;

    const/4 v5, 0x4

    const-string v6, "WRAP_KEY"

    const-string v7, "wrapKey"

    invoke-direct {v0, v6, v5, v7}, Lcom/a/a/c/f;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/a/a/c/f;->e:Lcom/a/a/c/f;

    .line 83
    new-instance v0, Lcom/a/a/c/f;

    const/4 v6, 0x5

    const-string v7, "UNWRAP_KEY"

    const-string v8, "unwrapKey"

    invoke-direct {v0, v7, v6, v8}, Lcom/a/a/c/f;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/a/a/c/f;->f:Lcom/a/a/c/f;

    .line 89
    new-instance v0, Lcom/a/a/c/f;

    const/4 v7, 0x6

    const-string v8, "DERIVE_KEY"

    const-string v9, "deriveKey"

    invoke-direct {v0, v8, v7, v9}, Lcom/a/a/c/f;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/a/a/c/f;->g:Lcom/a/a/c/f;

    .line 95
    new-instance v0, Lcom/a/a/c/f;

    const/4 v8, 0x7

    const-string v9, "DERIVE_BITS"

    const-string v10, "deriveBits"

    invoke-direct {v0, v9, v8, v10}, Lcom/a/a/c/f;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/a/a/c/f;->h:Lcom/a/a/c/f;

    const/16 v0, 0x8

    .line 47
    new-array v0, v0, [Lcom/a/a/c/f;

    sget-object v9, Lcom/a/a/c/f;->a:Lcom/a/a/c/f;

    aput-object v9, v0, v1

    sget-object v1, Lcom/a/a/c/f;->b:Lcom/a/a/c/f;

    aput-object v1, v0, v2

    sget-object v1, Lcom/a/a/c/f;->c:Lcom/a/a/c/f;

    aput-object v1, v0, v3

    sget-object v1, Lcom/a/a/c/f;->d:Lcom/a/a/c/f;

    aput-object v1, v0, v4

    sget-object v1, Lcom/a/a/c/f;->e:Lcom/a/a/c/f;

    aput-object v1, v0, v5

    sget-object v1, Lcom/a/a/c/f;->f:Lcom/a/a/c/f;

    aput-object v1, v0, v6

    sget-object v1, Lcom/a/a/c/f;->g:Lcom/a/a/c/f;

    aput-object v1, v0, v7

    sget-object v1, Lcom/a/a/c/f;->h:Lcom/a/a/c/f;

    aput-object v1, v0, v8

    sput-object v0, Lcom/a/a/c/f;->j:[Lcom/a/a/c/f;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 110
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    if-eqz p3, :cond_0

    .line 115
    iput-object p3, p0, Lcom/a/a/c/f;->i:Ljava/lang/String;

    return-void

    .line 113
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "The key operation identifier must not be null"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public static a(Ljava/util/List;)Ljava/util/Set;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/Set<",
            "Lcom/a/a/c/f;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/text/ParseException;
        }
    .end annotation

    const/4 v0, 0x0

    if-nez p0, :cond_0

    return-object v0

    .line 158
    :cond_0
    new-instance v1, Ljava/util/LinkedHashSet;

    invoke-direct {v1}, Ljava/util/LinkedHashSet;-><init>()V

    .line 160
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    if-nez v2, :cond_1

    goto :goto_0

    .line 169
    :cond_1
    invoke-static {}, Lcom/a/a/c/f;->values()[Lcom/a/a/c/f;

    move-result-object v3

    array-length v4, v3

    const/4 v5, 0x0

    const/4 v6, 0x0

    :goto_1
    if-ge v6, v4, :cond_3

    aget-object v7, v3, v6

    .line 171
    invoke-virtual {v7}, Lcom/a/a/c/f;->a()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v2, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    goto :goto_2

    :cond_2
    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    :cond_3
    move-object v7, v0

    :goto_2
    if-eqz v7, :cond_4

    .line 178
    invoke-interface {v1, v7}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 181
    :cond_4
    new-instance p0, Ljava/text/ParseException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Invalid JWK operation: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, v5}, Ljava/text/ParseException;-><init>(Ljava/lang/String;I)V

    throw p0

    :cond_5
    return-object v1
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/a/a/c/f;
    .locals 1

    .line 47
    const-class v0, Lcom/a/a/c/f;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/a/a/c/f;

    return-object p0
.end method

.method public static values()[Lcom/a/a/c/f;
    .locals 1

    .line 47
    sget-object v0, Lcom/a/a/c/f;->j:[Lcom/a/a/c/f;

    invoke-virtual {v0}, [Lcom/a/a/c/f;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/a/a/c/f;

    return-object v0
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .line 126
    iget-object v0, p0, Lcom/a/a/c/f;->i:Ljava/lang/String;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .line 136
    invoke-virtual {p0}, Lcom/a/a/c/f;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
