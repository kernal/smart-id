.class public final Lcom/a/a/c/k;
.super Lcom/a/a/c/d;
.source "OctetSequenceKey.java"


# instance fields
.field private final a:Lcom/a/a/d/c;


# direct methods
.method public constructor <init>(Lcom/a/a/d/c;Lcom/a/a/c/h;Ljava/util/Set;Lcom/a/a/a;Ljava/lang/String;Ljava/net/URI;Lcom/a/a/d/c;Lcom/a/a/d/c;Ljava/util/List;Ljava/security/KeyStore;)V
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/a/a/d/c;",
            "Lcom/a/a/c/h;",
            "Ljava/util/Set<",
            "Lcom/a/a/c/f;",
            ">;",
            "Lcom/a/a/a;",
            "Ljava/lang/String;",
            "Ljava/net/URI;",
            "Lcom/a/a/d/c;",
            "Lcom/a/a/d/c;",
            "Ljava/util/List<",
            "Lcom/a/a/d/a;",
            ">;",
            "Ljava/security/KeyStore;",
            ")V"
        }
    .end annotation

    move-object v0, p1

    .line 441
    sget-object v2, Lcom/a/a/c/g;->c:Lcom/a/a/c/g;

    move-object v1, p0

    move-object v3, p2

    move-object v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    invoke-direct/range {v1 .. v11}, Lcom/a/a/c/d;-><init>(Lcom/a/a/c/g;Lcom/a/a/c/h;Ljava/util/Set;Lcom/a/a/a;Ljava/lang/String;Ljava/net/URI;Lcom/a/a/d/c;Lcom/a/a/d/c;Ljava/util/List;Ljava/security/KeyStore;)V

    if-eqz v0, :cond_0

    move-object v1, p0

    .line 447
    iput-object v0, v1, Lcom/a/a/c/k;->a:Lcom/a/a/d/c;

    return-void

    :cond_0
    move-object v1, p0

    .line 444
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v2, "The key value must not be null"

    invoke-direct {v0, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static a(La/b/b/d;)Lcom/a/a/c/k;
    .locals 12
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/text/ParseException;
        }
    .end annotation

    .line 595
    new-instance v1, Lcom/a/a/d/c;

    const-string v0, "k"

    invoke-static {p0, v0}, Lcom/a/a/d/i;->b(La/b/b/d;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/a/a/d/c;-><init>(Ljava/lang/String;)V

    .line 598
    invoke-static {p0}, Lcom/a/a/c/e;->a(La/b/b/d;)Lcom/a/a/c/g;

    move-result-object v0

    .line 600
    sget-object v2, Lcom/a/a/c/g;->c:Lcom/a/a/c/g;

    if-ne v0, v2, :cond_0

    .line 605
    new-instance v11, Lcom/a/a/c/k;

    .line 606
    invoke-static {p0}, Lcom/a/a/c/e;->b(La/b/b/d;)Lcom/a/a/c/h;

    move-result-object v2

    .line 607
    invoke-static {p0}, Lcom/a/a/c/e;->c(La/b/b/d;)Ljava/util/Set;

    move-result-object v3

    .line 608
    invoke-static {p0}, Lcom/a/a/c/e;->d(La/b/b/d;)Lcom/a/a/a;

    move-result-object v4

    .line 609
    invoke-static {p0}, Lcom/a/a/c/e;->e(La/b/b/d;)Ljava/lang/String;

    move-result-object v5

    .line 610
    invoke-static {p0}, Lcom/a/a/c/e;->f(La/b/b/d;)Ljava/net/URI;

    move-result-object v6

    .line 611
    invoke-static {p0}, Lcom/a/a/c/e;->g(La/b/b/d;)Lcom/a/a/d/c;

    move-result-object v7

    .line 612
    invoke-static {p0}, Lcom/a/a/c/e;->h(La/b/b/d;)Lcom/a/a/d/c;

    move-result-object v8

    .line 613
    invoke-static {p0}, Lcom/a/a/c/e;->i(La/b/b/d;)Ljava/util/List;

    move-result-object v9

    const/4 v10, 0x0

    move-object v0, v11

    invoke-direct/range {v0 .. v10}, Lcom/a/a/c/k;-><init>(Lcom/a/a/d/c;Lcom/a/a/c/h;Ljava/util/Set;Lcom/a/a/a;Ljava/lang/String;Ljava/net/URI;Lcom/a/a/d/c;Lcom/a/a/d/c;Ljava/util/List;Ljava/security/KeyStore;)V

    return-object v11

    .line 602
    :cond_0
    new-instance p0, Ljava/text/ParseException;

    const/4 v0, 0x0

    const-string v1, "The key type \"kty\" must be oct"

    invoke-direct {p0, v1, v0}, Ljava/text/ParseException;-><init>(Ljava/lang/String;I)V

    throw p0
.end method


# virtual methods
.method public d()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public e()La/b/b/d;
    .locals 3

    .line 552
    invoke-super {p0}, Lcom/a/a/c/d;->e()La/b/b/d;

    move-result-object v0

    .line 555
    iget-object v1, p0, Lcom/a/a/c/k;->a:Lcom/a/a/d/c;

    invoke-virtual {v1}, Lcom/a/a/d/c;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "k"

    invoke-virtual {v0, v2, v1}, La/b/b/d;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-ne p0, p1, :cond_0

    const/4 p1, 0x1

    return p1

    .line 657
    :cond_0
    instance-of v0, p1, Lcom/a/a/c/k;

    const/4 v1, 0x0

    if-nez v0, :cond_1

    return v1

    .line 658
    :cond_1
    invoke-super {p0, p1}, Lcom/a/a/c/d;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    return v1

    .line 659
    :cond_2
    check-cast p1, Lcom/a/a/c/k;

    .line 660
    iget-object v0, p0, Lcom/a/a/c/k;->a:Lcom/a/a/d/c;

    iget-object p1, p1, Lcom/a/a/c/k;->a:Lcom/a/a/d/c;

    invoke-static {v0, p1}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method public hashCode()I
    .locals 3

    const/4 v0, 0x2

    .line 666
    new-array v0, v0, [Ljava/lang/Object;

    invoke-super {p0}, Lcom/a/a/c/d;->hashCode()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/a/a/c/k;->a:Lcom/a/a/d/c;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    invoke-static {v0}, Ljava/util/Objects;->hash([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
