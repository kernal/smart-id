.class public final Lcom/a/a/c/g;
.super Ljava/lang/Object;
.source "KeyType.java"

# interfaces
.implements La/b/b/b;
.implements Ljava/io/Serializable;


# static fields
.field public static final a:Lcom/a/a/c/g;

.field public static final b:Lcom/a/a/c/g;

.field public static final c:Lcom/a/a/c/g;

.field public static final d:Lcom/a/a/c/g;


# instance fields
.field private final e:Ljava/lang/String;

.field private final f:Lcom/a/a/w;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 73
    new-instance v0, Lcom/a/a/c/g;

    sget-object v1, Lcom/a/a/w;->b:Lcom/a/a/w;

    const-string v2, "EC"

    invoke-direct {v0, v2, v1}, Lcom/a/a/c/g;-><init>(Ljava/lang/String;Lcom/a/a/w;)V

    sput-object v0, Lcom/a/a/c/g;->a:Lcom/a/a/c/g;

    .line 79
    new-instance v0, Lcom/a/a/c/g;

    sget-object v1, Lcom/a/a/w;->a:Lcom/a/a/w;

    const-string v2, "RSA"

    invoke-direct {v0, v2, v1}, Lcom/a/a/c/g;-><init>(Ljava/lang/String;Lcom/a/a/w;)V

    sput-object v0, Lcom/a/a/c/g;->b:Lcom/a/a/c/g;

    .line 85
    new-instance v0, Lcom/a/a/c/g;

    sget-object v1, Lcom/a/a/w;->c:Lcom/a/a/w;

    const-string v2, "oct"

    invoke-direct {v0, v2, v1}, Lcom/a/a/c/g;-><init>(Ljava/lang/String;Lcom/a/a/w;)V

    sput-object v0, Lcom/a/a/c/g;->c:Lcom/a/a/c/g;

    .line 91
    new-instance v0, Lcom/a/a/c/g;

    sget-object v1, Lcom/a/a/w;->c:Lcom/a/a/w;

    const-string v2, "OKP"

    invoke-direct {v0, v2, v1}, Lcom/a/a/c/g;-><init>(Ljava/lang/String;Lcom/a/a/w;)V

    sput-object v0, Lcom/a/a/c/g;->d:Lcom/a/a/c/g;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/a/a/w;)V
    .locals 0

    .line 103
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    if-eqz p1, :cond_0

    .line 110
    iput-object p1, p0, Lcom/a/a/c/g;->e:Ljava/lang/String;

    .line 112
    iput-object p2, p0, Lcom/a/a/c/g;->f:Lcom/a/a/w;

    return-void

    .line 107
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "The key type value must not be null"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public static a(Ljava/lang/String;)Lcom/a/a/c/g;
    .locals 2

    .line 203
    sget-object v0, Lcom/a/a/c/g;->a:Lcom/a/a/c/g;

    invoke-virtual {v0}, Lcom/a/a/c/g;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 204
    sget-object p0, Lcom/a/a/c/g;->a:Lcom/a/a/c/g;

    return-object p0

    .line 205
    :cond_0
    sget-object v0, Lcom/a/a/c/g;->b:Lcom/a/a/c/g;

    invoke-virtual {v0}, Lcom/a/a/c/g;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 206
    sget-object p0, Lcom/a/a/c/g;->b:Lcom/a/a/c/g;

    return-object p0

    .line 207
    :cond_1
    sget-object v0, Lcom/a/a/c/g;->c:Lcom/a/a/c/g;

    invoke-virtual {v0}, Lcom/a/a/c/g;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 208
    sget-object p0, Lcom/a/a/c/g;->c:Lcom/a/a/c/g;

    return-object p0

    .line 209
    :cond_2
    sget-object v0, Lcom/a/a/c/g;->d:Lcom/a/a/c/g;

    invoke-virtual {v0}, Lcom/a/a/c/g;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 210
    sget-object p0, Lcom/a/a/c/g;->d:Lcom/a/a/c/g;

    return-object p0

    .line 212
    :cond_3
    new-instance v0, Lcom/a/a/c/g;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/a/a/c/g;-><init>(Ljava/lang/String;Lcom/a/a/w;)V

    return-object v0
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .line 123
    iget-object v0, p0, Lcom/a/a/c/g;->e:Ljava/lang/String;

    return-object v0
.end method

.method public b()Ljava/lang/String;
    .locals 2

    .line 189
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/a/a/c/g;->e:Ljava/lang/String;

    invoke-static {v1}, La/b/b/d;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x22

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1

    if-eqz p1, :cond_0

    .line 161
    instance-of v0, p1, Lcom/a/a/c/g;

    if-eqz v0, :cond_0

    .line 163
    invoke-virtual {p0}, Lcom/a/a/c/g;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public hashCode()I
    .locals 1

    .line 146
    iget-object v0, p0, Lcom/a/a/c/g;->e:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .line 177
    iget-object v0, p0, Lcom/a/a/c/g;->e:Ljava/lang/String;

    return-object v0
.end method
