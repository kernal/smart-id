.class public Lcom/a/a/c/j;
.super Lcom/a/a/c/d;
.source "OctetKeyPair.java"


# static fields
.field public static final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Lcom/a/a/c/a;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final b:Lcom/a/a/c/a;

.field private final c:Lcom/a/a/d/c;

.field private final d:[B

.field private final e:Lcom/a/a/d/c;

.field private final f:[B


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .line 101
    new-instance v0, Ljava/util/HashSet;

    const/4 v1, 0x4

    new-array v1, v1, [Lcom/a/a/c/a;

    sget-object v2, Lcom/a/a/c/a;->e:Lcom/a/a/c/a;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    sget-object v2, Lcom/a/a/c/a;->f:Lcom/a/a/c/a;

    const/4 v3, 0x1

    aput-object v2, v1, v3

    sget-object v2, Lcom/a/a/c/a;->g:Lcom/a/a/c/a;

    const/4 v3, 0x2

    aput-object v2, v1, v3

    sget-object v2, Lcom/a/a/c/a;->h:Lcom/a/a/c/a;

    const/4 v3, 0x3

    aput-object v2, v1, v3

    .line 102
    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 101
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    sput-object v0, Lcom/a/a/c/j;->a:Ljava/util/Set;

    return-void
.end method

.method public constructor <init>(Lcom/a/a/c/a;Lcom/a/a/d/c;Lcom/a/a/c/h;Ljava/util/Set;Lcom/a/a/a;Ljava/lang/String;Ljava/net/URI;Lcom/a/a/d/c;Lcom/a/a/d/c;Ljava/util/List;Ljava/security/KeyStore;)V
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/a/a/c/a;",
            "Lcom/a/a/d/c;",
            "Lcom/a/a/c/h;",
            "Ljava/util/Set<",
            "Lcom/a/a/c/f;",
            ">;",
            "Lcom/a/a/a;",
            "Ljava/lang/String;",
            "Ljava/net/URI;",
            "Lcom/a/a/d/c;",
            "Lcom/a/a/d/c;",
            "Ljava/util/List<",
            "Lcom/a/a/d/a;",
            ">;",
            "Ljava/security/KeyStore;",
            ")V"
        }
    .end annotation

    move-object v11, p0

    move-object v12, p1

    move-object/from16 v13, p2

    .line 534
    sget-object v1, Lcom/a/a/c/g;->d:Lcom/a/a/c/g;

    move-object v0, p0

    move-object/from16 v2, p3

    move-object/from16 v3, p4

    move-object/from16 v4, p5

    move-object/from16 v5, p6

    move-object/from16 v6, p7

    move-object/from16 v7, p8

    move-object/from16 v8, p9

    move-object/from16 v9, p10

    move-object/from16 v10, p11

    invoke-direct/range {v0 .. v10}, Lcom/a/a/c/d;-><init>(Lcom/a/a/c/g;Lcom/a/a/c/h;Ljava/util/Set;Lcom/a/a/a;Ljava/lang/String;Ljava/net/URI;Lcom/a/a/d/c;Lcom/a/a/d/c;Ljava/util/List;Ljava/security/KeyStore;)V

    if-eqz v12, :cond_2

    .line 540
    sget-object v0, Lcom/a/a/c/j;->a:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 544
    iput-object v12, v11, Lcom/a/a/c/j;->b:Lcom/a/a/c/a;

    if-eqz v13, :cond_0

    .line 550
    iput-object v13, v11, Lcom/a/a/c/j;->c:Lcom/a/a/d/c;

    .line 551
    invoke-virtual/range {p2 .. p2}, Lcom/a/a/d/c;->a()[B

    move-result-object v0

    iput-object v0, v11, Lcom/a/a/c/j;->d:[B

    const/4 v0, 0x0

    .line 553
    iput-object v0, v11, Lcom/a/a/c/j;->e:Lcom/a/a/d/c;

    .line 554
    iput-object v0, v11, Lcom/a/a/c/j;->f:[B

    return-void

    .line 547
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "The \'x\' parameter must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 541
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown / unsupported curve: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 537
    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "The curve must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public constructor <init>(Lcom/a/a/c/a;Lcom/a/a/d/c;Lcom/a/a/d/c;Lcom/a/a/c/h;Ljava/util/Set;Lcom/a/a/a;Ljava/lang/String;Ljava/net/URI;Lcom/a/a/d/c;Lcom/a/a/d/c;Ljava/util/List;Ljava/security/KeyStore;)V
    .locals 15
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/a/a/c/a;",
            "Lcom/a/a/d/c;",
            "Lcom/a/a/d/c;",
            "Lcom/a/a/c/h;",
            "Ljava/util/Set<",
            "Lcom/a/a/c/f;",
            ">;",
            "Lcom/a/a/a;",
            "Ljava/lang/String;",
            "Ljava/net/URI;",
            "Lcom/a/a/d/c;",
            "Lcom/a/a/d/c;",
            "Ljava/util/List<",
            "Lcom/a/a/d/a;",
            ">;",
            "Ljava/security/KeyStore;",
            ")V"
        }
    .end annotation

    move-object v11, p0

    move-object/from16 v12, p1

    move-object/from16 v13, p2

    move-object/from16 v14, p3

    .line 587
    sget-object v1, Lcom/a/a/c/g;->d:Lcom/a/a/c/g;

    move-object v0, p0

    move-object/from16 v2, p4

    move-object/from16 v3, p5

    move-object/from16 v4, p6

    move-object/from16 v5, p7

    move-object/from16 v6, p8

    move-object/from16 v7, p9

    move-object/from16 v8, p10

    move-object/from16 v9, p11

    move-object/from16 v10, p12

    invoke-direct/range {v0 .. v10}, Lcom/a/a/c/d;-><init>(Lcom/a/a/c/g;Lcom/a/a/c/h;Ljava/util/Set;Lcom/a/a/a;Ljava/lang/String;Ljava/net/URI;Lcom/a/a/d/c;Lcom/a/a/d/c;Ljava/util/List;Ljava/security/KeyStore;)V

    if-eqz v12, :cond_3

    .line 593
    sget-object v0, Lcom/a/a/c/j;->a:Ljava/util/Set;

    invoke-interface {v0, v12}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 597
    iput-object v12, v11, Lcom/a/a/c/j;->b:Lcom/a/a/c/a;

    if-eqz v13, :cond_1

    .line 603
    iput-object v13, v11, Lcom/a/a/c/j;->c:Lcom/a/a/d/c;

    .line 604
    invoke-virtual/range {p2 .. p2}, Lcom/a/a/d/c;->a()[B

    move-result-object v0

    iput-object v0, v11, Lcom/a/a/c/j;->d:[B

    if-eqz v14, :cond_0

    .line 610
    iput-object v14, v11, Lcom/a/a/c/j;->e:Lcom/a/a/d/c;

    .line 611
    invoke-virtual/range {p3 .. p3}, Lcom/a/a/d/c;->a()[B

    move-result-object v0

    iput-object v0, v11, Lcom/a/a/c/j;->f:[B

    return-void

    .line 607
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "The \'d\' parameter must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 600
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "The \'x\' parameter must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 594
    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown / unsupported curve: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 590
    :cond_3
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "The curve must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static a(La/b/b/d;)Lcom/a/a/c/j;
    .locals 15
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/text/ParseException;
        }
    .end annotation

    const-string v0, "crv"

    .line 793
    invoke-static {p0, v0}, Lcom/a/a/d/i;->b(La/b/b/d;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/a/a/c/a;->a(Ljava/lang/String;)Lcom/a/a/c/a;

    move-result-object v2

    .line 794
    new-instance v3, Lcom/a/a/d/c;

    const-string v0, "x"

    invoke-static {p0, v0}, Lcom/a/a/d/i;->b(La/b/b/d;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v0}, Lcom/a/a/d/c;-><init>(Ljava/lang/String;)V

    .line 797
    invoke-static {p0}, Lcom/a/a/c/e;->a(La/b/b/d;)Lcom/a/a/c/g;

    move-result-object v0

    .line 799
    sget-object v1, Lcom/a/a/c/g;->d:Lcom/a/a/c/g;

    const/4 v14, 0x0

    if-ne v0, v1, :cond_2

    const/4 v0, 0x0

    const-string v1, "d"

    .line 805
    invoke-virtual {p0, v1}, La/b/b/d;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 806
    new-instance v0, Lcom/a/a/d/c;

    invoke-static {p0, v1}, Lcom/a/a/d/i;->b(La/b/b/d;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/a/a/d/c;-><init>(Ljava/lang/String;)V

    :cond_0
    move-object v4, v0

    if-nez v4, :cond_1

    .line 813
    :try_start_0
    new-instance v0, Lcom/a/a/c/j;

    .line 814
    invoke-static {p0}, Lcom/a/a/c/e;->b(La/b/b/d;)Lcom/a/a/c/h;

    move-result-object v4

    .line 815
    invoke-static {p0}, Lcom/a/a/c/e;->c(La/b/b/d;)Ljava/util/Set;

    move-result-object v5

    .line 816
    invoke-static {p0}, Lcom/a/a/c/e;->d(La/b/b/d;)Lcom/a/a/a;

    move-result-object v6

    .line 817
    invoke-static {p0}, Lcom/a/a/c/e;->e(La/b/b/d;)Ljava/lang/String;

    move-result-object v7

    .line 818
    invoke-static {p0}, Lcom/a/a/c/e;->f(La/b/b/d;)Ljava/net/URI;

    move-result-object v8

    .line 819
    invoke-static {p0}, Lcom/a/a/c/e;->g(La/b/b/d;)Lcom/a/a/d/c;

    move-result-object v9

    .line 820
    invoke-static {p0}, Lcom/a/a/c/e;->h(La/b/b/d;)Lcom/a/a/d/c;

    move-result-object v10

    .line 821
    invoke-static {p0}, Lcom/a/a/c/e;->i(La/b/b/d;)Ljava/util/List;

    move-result-object v11

    const/4 v12, 0x0

    move-object v1, v0

    invoke-direct/range {v1 .. v12}, Lcom/a/a/c/j;-><init>(Lcom/a/a/c/a;Lcom/a/a/d/c;Lcom/a/a/c/h;Ljava/util/Set;Lcom/a/a/a;Ljava/lang/String;Ljava/net/URI;Lcom/a/a/d/c;Lcom/a/a/d/c;Ljava/util/List;Ljava/security/KeyStore;)V

    return-object v0

    .line 826
    :cond_1
    new-instance v0, Lcom/a/a/c/j;

    .line 827
    invoke-static {p0}, Lcom/a/a/c/e;->b(La/b/b/d;)Lcom/a/a/c/h;

    move-result-object v5

    .line 828
    invoke-static {p0}, Lcom/a/a/c/e;->c(La/b/b/d;)Ljava/util/Set;

    move-result-object v6

    .line 829
    invoke-static {p0}, Lcom/a/a/c/e;->d(La/b/b/d;)Lcom/a/a/a;

    move-result-object v7

    .line 830
    invoke-static {p0}, Lcom/a/a/c/e;->e(La/b/b/d;)Ljava/lang/String;

    move-result-object v8

    .line 831
    invoke-static {p0}, Lcom/a/a/c/e;->f(La/b/b/d;)Ljava/net/URI;

    move-result-object v9

    .line 832
    invoke-static {p0}, Lcom/a/a/c/e;->g(La/b/b/d;)Lcom/a/a/d/c;

    move-result-object v10

    .line 833
    invoke-static {p0}, Lcom/a/a/c/e;->h(La/b/b/d;)Lcom/a/a/d/c;

    move-result-object v11

    .line 834
    invoke-static {p0}, Lcom/a/a/c/e;->i(La/b/b/d;)Ljava/util/List;

    move-result-object v12

    const/4 v13, 0x0

    move-object v1, v0

    invoke-direct/range {v1 .. v13}, Lcom/a/a/c/j;-><init>(Lcom/a/a/c/a;Lcom/a/a/d/c;Lcom/a/a/d/c;Lcom/a/a/c/h;Ljava/util/Set;Lcom/a/a/a;Ljava/lang/String;Ljava/net/URI;Lcom/a/a/d/c;Lcom/a/a/d/c;Ljava/util/List;Ljava/security/KeyStore;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception p0

    .line 841
    new-instance v0, Ljava/text/ParseException;

    invoke-virtual {p0}, Ljava/lang/IllegalArgumentException;->getMessage()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v0, p0, v14}, Ljava/text/ParseException;-><init>(Ljava/lang/String;I)V

    throw v0

    .line 800
    :cond_2
    new-instance p0, Ljava/text/ParseException;

    const-string v0, "The key type \"kty\" must be OKP"

    invoke-direct {p0, v0, v14}, Ljava/text/ParseException;-><init>(Ljava/lang/String;I)V

    throw p0
.end method


# virtual methods
.method public d()Z
    .locals 1

    .line 714
    iget-object v0, p0, Lcom/a/a/c/j;->e:Lcom/a/a/d/c;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public e()La/b/b/d;
    .locals 3

    .line 738
    invoke-super {p0}, Lcom/a/a/c/d;->e()La/b/b/d;

    move-result-object v0

    .line 741
    iget-object v1, p0, Lcom/a/a/c/j;->b:Lcom/a/a/c/a;

    invoke-virtual {v1}, Lcom/a/a/c/a;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "crv"

    invoke-virtual {v0, v2, v1}, La/b/b/d;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 742
    iget-object v1, p0, Lcom/a/a/c/j;->c:Lcom/a/a/d/c;

    invoke-virtual {v1}, Lcom/a/a/d/c;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "x"

    invoke-virtual {v0, v2, v1}, La/b/b/d;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 744
    iget-object v1, p0, Lcom/a/a/c/j;->e:Lcom/a/a/d/c;

    if-eqz v1, :cond_0

    .line 745
    invoke-virtual {v1}, Lcom/a/a/d/c;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "d"

    invoke-virtual {v0, v2, v1}, La/b/b/d;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p0, p1, :cond_0

    return v0

    .line 849
    :cond_0
    instance-of v1, p1, Lcom/a/a/c/j;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 850
    :cond_1
    invoke-super {p0, p1}, Lcom/a/a/c/d;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    return v2

    .line 851
    :cond_2
    check-cast p1, Lcom/a/a/c/j;

    .line 852
    iget-object v1, p0, Lcom/a/a/c/j;->b:Lcom/a/a/c/a;

    iget-object v3, p1, Lcom/a/a/c/j;->b:Lcom/a/a/c/a;

    invoke-static {v1, v3}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/a/a/c/j;->c:Lcom/a/a/d/c;

    iget-object v3, p1, Lcom/a/a/c/j;->c:Lcom/a/a/d/c;

    .line 853
    invoke-static {v1, v3}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/a/a/c/j;->d:[B

    iget-object v3, p1, Lcom/a/a/c/j;->d:[B

    .line 854
    invoke-static {v1, v3}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/a/a/c/j;->e:Lcom/a/a/d/c;

    iget-object v3, p1, Lcom/a/a/c/j;->e:Lcom/a/a/d/c;

    .line 855
    invoke-static {v1, v3}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/a/a/c/j;->f:[B

    iget-object p1, p1, Lcom/a/a/c/j;->f:[B

    .line 856
    invoke-static {v1, p1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result p1

    if-eqz p1, :cond_3

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    const/4 v0, 0x4

    .line 862
    new-array v0, v0, [Ljava/lang/Object;

    invoke-super {p0}, Lcom/a/a/c/d;->hashCode()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/a/a/c/j;->b:Lcom/a/a/c/a;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/a/a/c/j;->c:Lcom/a/a/d/c;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/a/a/c/j;->e:Lcom/a/a/d/c;

    const/4 v2, 0x3

    aput-object v1, v0, v2

    invoke-static {v0}, Ljava/util/Objects;->hash([Ljava/lang/Object;)I

    move-result v0

    mul-int/lit8 v0, v0, 0x1f

    .line 863
    iget-object v1, p0, Lcom/a/a/c/j;->d:[B

    invoke-static {v1}, Ljava/util/Arrays;->hashCode([B)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    .line 864
    iget-object v1, p0, Lcom/a/a/c/j;->f:[B

    invoke-static {v1}, Ljava/util/Arrays;->hashCode([B)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method
