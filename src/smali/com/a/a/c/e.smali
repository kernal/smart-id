.class final Lcom/a/a/c/e;
.super Ljava/lang/Object;
.source "JWKMetadata.java"


# direct methods
.method static a(La/b/b/d;)Lcom/a/a/c/g;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/text/ParseException;
        }
    .end annotation

    const-string v0, "kty"

    .line 55
    invoke-static {p0, v0}, Lcom/a/a/d/i;->b(La/b/b/d;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    invoke-static {p0}, Lcom/a/a/c/g;->a(Ljava/lang/String;)Lcom/a/a/c/g;

    move-result-object p0

    return-object p0
.end method

.method static b(La/b/b/d;)Lcom/a/a/c/h;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/text/ParseException;
        }
    .end annotation

    const-string v0, "use"

    .line 72
    invoke-virtual {p0, v0}, La/b/b/d;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 73
    invoke-static {p0, v0}, Lcom/a/a/d/i;->b(La/b/b/d;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    invoke-static {p0}, Lcom/a/a/c/h;->a(Ljava/lang/String;)Lcom/a/a/c/h;

    move-result-object p0

    return-object p0

    :cond_0
    const/4 p0, 0x0

    return-object p0
.end method

.method static c(La/b/b/d;)Ljava/util/Set;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "La/b/b/d;",
            ")",
            "Ljava/util/Set<",
            "Lcom/a/a/c/f;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/text/ParseException;
        }
    .end annotation

    const-string v0, "key_ops"

    .line 92
    invoke-virtual {p0, v0}, La/b/b/d;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 93
    invoke-static {p0, v0}, Lcom/a/a/d/i;->f(La/b/b/d;Ljava/lang/String;)Ljava/util/List;

    move-result-object p0

    invoke-static {p0}, Lcom/a/a/c/f;->a(Ljava/util/List;)Ljava/util/Set;

    move-result-object p0

    return-object p0

    :cond_0
    const/4 p0, 0x0

    return-object p0
.end method

.method static d(La/b/b/d;)Lcom/a/a/a;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/text/ParseException;
        }
    .end annotation

    const-string v0, "alg"

    .line 112
    invoke-virtual {p0, v0}, La/b/b/d;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 113
    new-instance v1, Lcom/a/a/a;

    invoke-static {p0, v0}, Lcom/a/a/d/i;->b(La/b/b/d;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    invoke-direct {v1, p0}, Lcom/a/a/a;-><init>(Ljava/lang/String;)V

    return-object v1

    :cond_0
    const/4 p0, 0x0

    return-object p0
.end method

.method static e(La/b/b/d;)Ljava/lang/String;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/text/ParseException;
        }
    .end annotation

    const-string v0, "kid"

    .line 132
    invoke-virtual {p0, v0}, La/b/b/d;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 133
    invoke-static {p0, v0}, Lcom/a/a/d/i;->b(La/b/b/d;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    return-object p0

    :cond_0
    const/4 p0, 0x0

    return-object p0
.end method

.method static f(La/b/b/d;)Ljava/net/URI;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/text/ParseException;
        }
    .end annotation

    const-string v0, "x5u"

    .line 152
    invoke-virtual {p0, v0}, La/b/b/d;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 153
    invoke-static {p0, v0}, Lcom/a/a/d/i;->c(La/b/b/d;Ljava/lang/String;)Ljava/net/URI;

    move-result-object p0

    return-object p0

    :cond_0
    const/4 p0, 0x0

    return-object p0
.end method

.method static g(La/b/b/d;)Lcom/a/a/d/c;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/text/ParseException;
        }
    .end annotation

    const-string v0, "x5t"

    .line 173
    invoke-virtual {p0, v0}, La/b/b/d;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 174
    new-instance v1, Lcom/a/a/d/c;

    invoke-static {p0, v0}, Lcom/a/a/d/i;->b(La/b/b/d;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    invoke-direct {v1, p0}, Lcom/a/a/d/c;-><init>(Ljava/lang/String;)V

    return-object v1

    :cond_0
    const/4 p0, 0x0

    return-object p0
.end method

.method static h(La/b/b/d;)Lcom/a/a/d/c;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/text/ParseException;
        }
    .end annotation

    const-string v0, "x5t#S256"

    .line 194
    invoke-virtual {p0, v0}, La/b/b/d;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 195
    new-instance v1, Lcom/a/a/d/c;

    invoke-static {p0, v0}, Lcom/a/a/d/i;->b(La/b/b/d;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    invoke-direct {v1, p0}, Lcom/a/a/d/c;-><init>(Ljava/lang/String;)V

    return-object v1

    :cond_0
    const/4 p0, 0x0

    return-object p0
.end method

.method static i(La/b/b/d;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "La/b/b/d;",
            ")",
            "Ljava/util/List<",
            "Lcom/a/a/d/a;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/text/ParseException;
        }
    .end annotation

    const-string v0, "x5c"

    .line 216
    invoke-virtual {p0, v0}, La/b/b/d;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 217
    invoke-static {p0, v0}, Lcom/a/a/d/i;->d(La/b/b/d;Ljava/lang/String;)La/b/b/a;

    move-result-object p0

    invoke-static {p0}, Lcom/a/a/d/l;->a(La/b/b/a;)Ljava/util/List;

    move-result-object p0

    .line 219
    invoke-interface {p0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    return-object p0

    .line 220
    :cond_0
    new-instance p0, Ljava/text/ParseException;

    const/4 v0, 0x0

    const-string v1, "The X.509 certificate chain \"x5c\" must not be empty"

    invoke-direct {p0, v1, v0}, Ljava/text/ParseException;-><init>(Ljava/lang/String;I)V

    throw p0

    :cond_1
    const/4 p0, 0x0

    return-object p0
.end method
