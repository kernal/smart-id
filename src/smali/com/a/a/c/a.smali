.class public final Lcom/a/a/c/a;
.super Ljava/lang/Object;
.source "Curve.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field public static final a:Lcom/a/a/c/a;

.field public static final b:Lcom/a/a/c/a;

.field public static final c:Lcom/a/a/c/a;

.field public static final d:Lcom/a/a/c/a;

.field public static final e:Lcom/a/a/c/a;

.field public static final f:Lcom/a/a/c/a;

.field public static final g:Lcom/a/a/c/a;

.field public static final h:Lcom/a/a/c/a;


# instance fields
.field private final i:Ljava/lang/String;

.field private final j:Ljava/lang/String;

.field private final k:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .line 72
    new-instance v0, Lcom/a/a/c/a;

    const-string v1, "P-256"

    const-string v2, "secp256r1"

    const-string v3, "1.2.840.10045.3.1.7"

    invoke-direct {v0, v1, v2, v3}, Lcom/a/a/c/a;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/a/a/c/a;->a:Lcom/a/a/c/a;

    .line 78
    new-instance v0, Lcom/a/a/c/a;

    const-string v1, "P-256K"

    const-string v2, "secp256k1"

    const-string v3, "1.3.132.0.10"

    invoke-direct {v0, v1, v2, v3}, Lcom/a/a/c/a;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/a/a/c/a;->b:Lcom/a/a/c/a;

    .line 84
    new-instance v0, Lcom/a/a/c/a;

    const-string v1, "P-384"

    const-string v2, "secp384r1"

    const-string v3, "1.3.132.0.34"

    invoke-direct {v0, v1, v2, v3}, Lcom/a/a/c/a;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/a/a/c/a;->c:Lcom/a/a/c/a;

    .line 90
    new-instance v0, Lcom/a/a/c/a;

    const-string v1, "P-521"

    const-string v2, "secp521r1"

    const-string v3, "1.3.132.0.35"

    invoke-direct {v0, v1, v2, v3}, Lcom/a/a/c/a;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/a/a/c/a;->d:Lcom/a/a/c/a;

    .line 96
    new-instance v0, Lcom/a/a/c/a;

    const-string v1, "Ed25519"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v1, v2}, Lcom/a/a/c/a;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/a/a/c/a;->e:Lcom/a/a/c/a;

    .line 102
    new-instance v0, Lcom/a/a/c/a;

    const-string v1, "Ed448"

    invoke-direct {v0, v1, v1, v2}, Lcom/a/a/c/a;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/a/a/c/a;->f:Lcom/a/a/c/a;

    .line 108
    new-instance v0, Lcom/a/a/c/a;

    const-string v1, "X25519"

    invoke-direct {v0, v1, v1, v2}, Lcom/a/a/c/a;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/a/a/c/a;->g:Lcom/a/a/c/a;

    .line 114
    new-instance v0, Lcom/a/a/c/a;

    const-string v1, "X448"

    invoke-direct {v0, v1, v1, v2}, Lcom/a/a/c/a;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/a/a/c/a;->h:Lcom/a/a/c/a;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    const/4 v0, 0x0

    .line 145
    invoke-direct {p0, p1, v0, v0}, Lcom/a/a/c/a;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 160
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    if-eqz p1, :cond_0

    .line 166
    iput-object p1, p0, Lcom/a/a/c/a;->i:Ljava/lang/String;

    .line 168
    iput-object p2, p0, Lcom/a/a/c/a;->j:Ljava/lang/String;

    .line 170
    iput-object p3, p0, Lcom/a/a/c/a;->k:Ljava/lang/String;

    return-void

    .line 163
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "The JOSE cryptographic curve name must not be null"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public static a(Ljava/lang/String;)Lcom/a/a/c/a;
    .locals 1

    if-eqz p0, :cond_8

    .line 247
    invoke-virtual {p0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_8

    .line 251
    sget-object v0, Lcom/a/a/c/a;->a:Lcom/a/a/c/a;

    invoke-virtual {v0}, Lcom/a/a/c/a;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 252
    sget-object p0, Lcom/a/a/c/a;->a:Lcom/a/a/c/a;

    return-object p0

    .line 253
    :cond_0
    sget-object v0, Lcom/a/a/c/a;->b:Lcom/a/a/c/a;

    invoke-virtual {v0}, Lcom/a/a/c/a;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 254
    sget-object p0, Lcom/a/a/c/a;->b:Lcom/a/a/c/a;

    return-object p0

    .line 255
    :cond_1
    sget-object v0, Lcom/a/a/c/a;->c:Lcom/a/a/c/a;

    invoke-virtual {v0}, Lcom/a/a/c/a;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 256
    sget-object p0, Lcom/a/a/c/a;->c:Lcom/a/a/c/a;

    return-object p0

    .line 257
    :cond_2
    sget-object v0, Lcom/a/a/c/a;->d:Lcom/a/a/c/a;

    invoke-virtual {v0}, Lcom/a/a/c/a;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 258
    sget-object p0, Lcom/a/a/c/a;->d:Lcom/a/a/c/a;

    return-object p0

    .line 259
    :cond_3
    sget-object v0, Lcom/a/a/c/a;->e:Lcom/a/a/c/a;

    invoke-virtual {v0}, Lcom/a/a/c/a;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 260
    sget-object p0, Lcom/a/a/c/a;->e:Lcom/a/a/c/a;

    return-object p0

    .line 261
    :cond_4
    sget-object v0, Lcom/a/a/c/a;->f:Lcom/a/a/c/a;

    invoke-virtual {v0}, Lcom/a/a/c/a;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 262
    sget-object p0, Lcom/a/a/c/a;->f:Lcom/a/a/c/a;

    return-object p0

    .line 263
    :cond_5
    sget-object v0, Lcom/a/a/c/a;->g:Lcom/a/a/c/a;

    invoke-virtual {v0}, Lcom/a/a/c/a;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 264
    sget-object p0, Lcom/a/a/c/a;->g:Lcom/a/a/c/a;

    return-object p0

    .line 265
    :cond_6
    sget-object v0, Lcom/a/a/c/a;->h:Lcom/a/a/c/a;

    invoke-virtual {v0}, Lcom/a/a/c/a;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 266
    sget-object p0, Lcom/a/a/c/a;->h:Lcom/a/a/c/a;

    return-object p0

    .line 268
    :cond_7
    new-instance v0, Lcom/a/a/c/a;

    invoke-direct {v0, p0}, Lcom/a/a/c/a;-><init>(Ljava/lang/String;)V

    return-object v0

    .line 248
    :cond_8
    new-instance p0, Ljava/lang/IllegalArgumentException;

    const-string v0, "The cryptographic curve string must not be null or empty"

    invoke-direct {p0, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p0
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .line 181
    iget-object v0, p0, Lcom/a/a/c/a;->i:Ljava/lang/String;

    return-object v0
.end method

.method public b()Ljava/security/spec/ECParameterSpec;
    .locals 1

    .line 216
    invoke-static {p0}, Lcom/a/a/c/c;->a(Lcom/a/a/c/a;)Ljava/security/spec/ECParameterSpec;

    move-result-object v0

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1

    .line 233
    instance-of v0, p1, Lcom/a/a/c/a;

    if-eqz v0, :cond_0

    .line 234
    invoke-virtual {p0}, Lcom/a/a/c/a;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .line 226
    invoke-virtual {p0}, Lcom/a/a/c/a;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
