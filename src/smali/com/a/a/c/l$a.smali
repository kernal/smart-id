.class public Lcom/a/a/c/l$a;
.super Ljava/lang/Object;
.source "RSAKey.java"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/a/a/c/l;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation


# instance fields
.field private final a:Lcom/a/a/d/c;

.field private final b:Lcom/a/a/d/c;

.field private final c:Lcom/a/a/d/c;


# direct methods
.method public constructor <init>(Lcom/a/a/d/c;Lcom/a/a/d/c;Lcom/a/a/d/c;)V
    .locals 0

    .line 183
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    if-eqz p1, :cond_2

    .line 190
    iput-object p1, p0, Lcom/a/a/c/l$a;->a:Lcom/a/a/d/c;

    if-eqz p2, :cond_1

    .line 197
    iput-object p2, p0, Lcom/a/a/c/l$a;->b:Lcom/a/a/d/c;

    if-eqz p3, :cond_0

    .line 204
    iput-object p3, p0, Lcom/a/a/c/l$a;->c:Lcom/a/a/d/c;

    return-void

    .line 201
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "The factor CRT coefficient must not be null"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 194
    :cond_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "The factor CRT exponent must not be null"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 187
    :cond_2
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "The prime factor must not be null"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method static synthetic a(Lcom/a/a/c/l$a;)Lcom/a/a/d/c;
    .locals 0

    .line 149
    iget-object p0, p0, Lcom/a/a/c/l$a;->a:Lcom/a/a/d/c;

    return-object p0
.end method

.method static synthetic b(Lcom/a/a/c/l$a;)Lcom/a/a/d/c;
    .locals 0

    .line 149
    iget-object p0, p0, Lcom/a/a/c/l$a;->b:Lcom/a/a/d/c;

    return-object p0
.end method

.method static synthetic c(Lcom/a/a/c/l$a;)Lcom/a/a/d/c;
    .locals 0

    .line 149
    iget-object p0, p0, Lcom/a/a/c/l$a;->c:Lcom/a/a/d/c;

    return-object p0
.end method
