.class public final Lcom/a/a/c/l;
.super Lcom/a/a/c/d;
.source "RSAKey.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/a/a/c/l$a;
    }
.end annotation


# instance fields
.field private final a:Lcom/a/a/d/c;

.field private final b:Lcom/a/a/d/c;

.field private final c:Lcom/a/a/d/c;

.field private final d:Lcom/a/a/d/c;

.field private final e:Lcom/a/a/d/c;

.field private final f:Lcom/a/a/d/c;

.field private final g:Lcom/a/a/d/c;

.field private final h:Lcom/a/a/d/c;

.field private final i:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/a/a/c/l$a;",
            ">;"
        }
    .end annotation
.end field

.field private final j:Ljava/security/PrivateKey;


# direct methods
.method public constructor <init>(Lcom/a/a/d/c;Lcom/a/a/d/c;Lcom/a/a/d/c;Lcom/a/a/d/c;Lcom/a/a/d/c;Lcom/a/a/d/c;Lcom/a/a/d/c;Lcom/a/a/d/c;Ljava/util/List;Ljava/security/PrivateKey;Lcom/a/a/c/h;Ljava/util/Set;Lcom/a/a/a;Ljava/lang/String;Ljava/net/URI;Lcom/a/a/d/c;Lcom/a/a/d/c;Ljava/util/List;Ljava/security/KeyStore;)V
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/a/a/d/c;",
            "Lcom/a/a/d/c;",
            "Lcom/a/a/d/c;",
            "Lcom/a/a/d/c;",
            "Lcom/a/a/d/c;",
            "Lcom/a/a/d/c;",
            "Lcom/a/a/d/c;",
            "Lcom/a/a/d/c;",
            "Ljava/util/List<",
            "Lcom/a/a/c/l$a;",
            ">;",
            "Ljava/security/PrivateKey;",
            "Lcom/a/a/c/h;",
            "Ljava/util/Set<",
            "Lcom/a/a/c/f;",
            ">;",
            "Lcom/a/a/a;",
            "Ljava/lang/String;",
            "Ljava/net/URI;",
            "Lcom/a/a/d/c;",
            "Lcom/a/a/d/c;",
            "Ljava/util/List<",
            "Lcom/a/a/d/a;",
            ">;",
            "Ljava/security/KeyStore;",
            ")V"
        }
    .end annotation

    move-object/from16 v11, p0

    move-object/from16 v12, p1

    move-object/from16 v13, p2

    move-object/from16 v14, p4

    move-object/from16 v15, p5

    move-object/from16 v10, p6

    move-object/from16 v9, p7

    move-object/from16 v8, p8

    .line 1331
    sget-object v1, Lcom/a/a/c/g;->b:Lcom/a/a/c/g;

    move-object/from16 v0, p0

    move-object/from16 v2, p11

    move-object/from16 v3, p12

    move-object/from16 v4, p13

    move-object/from16 v5, p14

    move-object/from16 v6, p15

    move-object/from16 v7, p16

    move-object/from16 v8, p17

    move-object/from16 v9, p18

    move-object/from16 v10, p19

    invoke-direct/range {v0 .. v10}, Lcom/a/a/c/d;-><init>(Lcom/a/a/c/g;Lcom/a/a/c/h;Ljava/util/Set;Lcom/a/a/a;Ljava/lang/String;Ljava/net/URI;Lcom/a/a/d/c;Lcom/a/a/d/c;Ljava/util/List;Ljava/security/KeyStore;)V

    if-eqz v12, :cond_f

    .line 1339
    iput-object v12, v11, Lcom/a/a/c/l;->a:Lcom/a/a/d/c;

    if-eqz v13, :cond_e

    .line 1345
    iput-object v13, v11, Lcom/a/a/c/l;->b:Lcom/a/a/d/c;

    .line 1347
    invoke-virtual/range {p0 .. p0}, Lcom/a/a/c/l;->f()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1348
    invoke-virtual/range {p0 .. p0}, Lcom/a/a/c/l;->f()Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/security/cert/X509Certificate;

    invoke-virtual {v11, v0}, Lcom/a/a/c/l;->a(Ljava/security/cert/X509Certificate;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 1349
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "The public subject key info of the first X.509 certificate in the chain must match the JWK type and public parameters"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    :goto_0
    move-object/from16 v0, p3

    .line 1354
    iput-object v0, v11, Lcom/a/a/c/l;->c:Lcom/a/a/d/c;

    if-eqz v14, :cond_3

    if-eqz v15, :cond_3

    move-object/from16 v0, p6

    if-eqz v0, :cond_4

    move-object/from16 v1, p7

    if-eqz v1, :cond_5

    move-object/from16 v2, p8

    if-eqz v2, :cond_6

    .line 1362
    iput-object v14, v11, Lcom/a/a/c/l;->d:Lcom/a/a/d/c;

    .line 1363
    iput-object v15, v11, Lcom/a/a/c/l;->e:Lcom/a/a/d/c;

    .line 1364
    iput-object v0, v11, Lcom/a/a/c/l;->f:Lcom/a/a/d/c;

    .line 1365
    iput-object v1, v11, Lcom/a/a/c/l;->g:Lcom/a/a/d/c;

    .line 1366
    iput-object v2, v11, Lcom/a/a/c/l;->h:Lcom/a/a/d/c;

    if-eqz p9, :cond_2

    .line 1370
    invoke-static/range {p9 .. p9}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, v11, Lcom/a/a/c/l;->i:Ljava/util/List;

    goto :goto_1

    .line 1372
    :cond_2
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, v11, Lcom/a/a/c/l;->i:Ljava/util/List;

    :goto_1
    move-object/from16 v0, p10

    goto :goto_2

    :cond_3
    move-object/from16 v0, p6

    :cond_4
    move-object/from16 v1, p7

    :cond_5
    move-object/from16 v2, p8

    :cond_6
    const/4 v3, 0x0

    if-nez v14, :cond_7

    if-nez v15, :cond_7

    if-nez v0, :cond_7

    if-nez v1, :cond_7

    if-nez v2, :cond_7

    if-nez p9, :cond_7

    .line 1378
    iput-object v3, v11, Lcom/a/a/c/l;->d:Lcom/a/a/d/c;

    .line 1379
    iput-object v3, v11, Lcom/a/a/c/l;->e:Lcom/a/a/d/c;

    .line 1380
    iput-object v3, v11, Lcom/a/a/c/l;->f:Lcom/a/a/d/c;

    .line 1381
    iput-object v3, v11, Lcom/a/a/c/l;->g:Lcom/a/a/d/c;

    .line 1382
    iput-object v3, v11, Lcom/a/a/c/l;->h:Lcom/a/a/d/c;

    .line 1384
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, v11, Lcom/a/a/c/l;->i:Ljava/util/List;

    goto :goto_1

    :cond_7
    if-nez v14, :cond_9

    if-nez v15, :cond_9

    if-nez v0, :cond_9

    if-nez v1, :cond_9

    if-eqz v2, :cond_8

    goto :goto_3

    .line 1401
    :cond_8
    iput-object v3, v11, Lcom/a/a/c/l;->d:Lcom/a/a/d/c;

    .line 1402
    iput-object v3, v11, Lcom/a/a/c/l;->e:Lcom/a/a/d/c;

    .line 1403
    iput-object v3, v11, Lcom/a/a/c/l;->f:Lcom/a/a/d/c;

    .line 1404
    iput-object v3, v11, Lcom/a/a/c/l;->g:Lcom/a/a/d/c;

    .line 1405
    iput-object v3, v11, Lcom/a/a/c/l;->h:Lcom/a/a/d/c;

    .line 1406
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, v11, Lcom/a/a/c/l;->i:Ljava/util/List;

    goto :goto_1

    .line 1409
    :goto_2
    iput-object v0, v11, Lcom/a/a/c/l;->j:Ljava/security/PrivateKey;

    return-void

    :cond_9
    :goto_3
    if-eqz v14, :cond_d

    if-eqz v15, :cond_c

    if-eqz v0, :cond_b

    if-nez v1, :cond_a

    .line 1395
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Incomplete second private (CRT) representation: The second factor CRT exponent must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1397
    :cond_a
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Incomplete second private (CRT) representation: The first CRT coefficient must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1393
    :cond_b
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Incomplete second private (CRT) representation: The first factor CRT exponent must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1391
    :cond_c
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Incomplete second private (CRT) representation: The second prime factor must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1389
    :cond_d
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Incomplete second private (CRT) representation: The first prime factor must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1343
    :cond_e
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "The public exponent value must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1337
    :cond_f
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "The modulus value must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static a(La/b/b/d;)Lcom/a/a/c/l;
    .locals 24
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/text/ParseException;
        }
    .end annotation

    move-object/from16 v0, p0

    .line 2064
    new-instance v1, Lcom/a/a/d/c;

    const-string v2, "n"

    invoke-static {v0, v2}, Lcom/a/a/d/i;->b(La/b/b/d;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/a/a/d/c;-><init>(Ljava/lang/String;)V

    .line 2065
    new-instance v2, Lcom/a/a/d/c;

    const-string v3, "e"

    invoke-static {v0, v3}, Lcom/a/a/d/i;->b(La/b/b/d;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/a/a/d/c;-><init>(Ljava/lang/String;)V

    const-string v3, "kty"

    .line 2068
    invoke-static {v0, v3}, Lcom/a/a/d/i;->b(La/b/b/d;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/a/a/c/g;->a(Ljava/lang/String;)Lcom/a/a/c/g;

    move-result-object v3

    .line 2069
    sget-object v4, Lcom/a/a/c/g;->b:Lcom/a/a/c/g;

    if-ne v3, v4, :cond_9

    const-string v3, "d"

    .line 2077
    invoke-virtual {v0, v3}, La/b/b/d;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    const/4 v5, 0x0

    if-eqz v4, :cond_0

    .line 2078
    new-instance v4, Lcom/a/a/d/c;

    invoke-static {v0, v3}, Lcom/a/a/d/i;->b(La/b/b/d;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v4, v3}, Lcom/a/a/d/c;-><init>(Ljava/lang/String;)V

    move-object v3, v4

    goto :goto_0

    :cond_0
    move-object v3, v5

    :goto_0
    const-string v4, "p"

    .line 2083
    invoke-virtual {v0, v4}, La/b/b/d;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 2084
    new-instance v6, Lcom/a/a/d/c;

    invoke-static {v0, v4}, Lcom/a/a/d/i;->b(La/b/b/d;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v6, v4}, Lcom/a/a/d/c;-><init>(Ljava/lang/String;)V

    move-object v4, v6

    goto :goto_1

    :cond_1
    move-object v4, v5

    :goto_1
    const-string v6, "q"

    .line 2087
    invoke-virtual {v0, v6}, La/b/b/d;->containsKey(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 2088
    new-instance v7, Lcom/a/a/d/c;

    invoke-static {v0, v6}, Lcom/a/a/d/i;->b(La/b/b/d;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v7, v6}, Lcom/a/a/d/c;-><init>(Ljava/lang/String;)V

    goto :goto_2

    :cond_2
    move-object v7, v5

    :goto_2
    const-string v6, "dp"

    .line 2091
    invoke-virtual {v0, v6}, La/b/b/d;->containsKey(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 2092
    new-instance v8, Lcom/a/a/d/c;

    invoke-static {v0, v6}, Lcom/a/a/d/i;->b(La/b/b/d;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v8, v6}, Lcom/a/a/d/c;-><init>(Ljava/lang/String;)V

    move-object v6, v8

    goto :goto_3

    :cond_3
    move-object v6, v5

    :goto_3
    const-string v8, "dq"

    .line 2095
    invoke-virtual {v0, v8}, La/b/b/d;->containsKey(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_4

    .line 2096
    new-instance v9, Lcom/a/a/d/c;

    invoke-static {v0, v8}, Lcom/a/a/d/i;->b(La/b/b/d;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10}, Lcom/a/a/d/c;-><init>(Ljava/lang/String;)V

    goto :goto_4

    :cond_4
    move-object v9, v5

    :goto_4
    const-string v10, "qi"

    .line 2099
    invoke-virtual {v0, v10}, La/b/b/d;->containsKey(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_5

    .line 2100
    new-instance v11, Lcom/a/a/d/c;

    invoke-static {v0, v10}, Lcom/a/a/d/i;->b(La/b/b/d;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-direct {v11, v10}, Lcom/a/a/d/c;-><init>(Ljava/lang/String;)V

    goto :goto_5

    :cond_5
    move-object v11, v5

    :goto_5
    const-string v10, "oth"

    .line 2104
    invoke-virtual {v0, v10}, La/b/b/d;->containsKey(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_7

    .line 2106
    invoke-static {v0, v10}, Lcom/a/a/d/i;->d(La/b/b/d;Ljava/lang/String;)La/b/b/a;

    move-result-object v5

    .line 2107
    new-instance v10, Ljava/util/ArrayList;

    invoke-virtual {v5}, La/b/b/a;->size()I

    move-result v12

    invoke-direct {v10, v12}, Ljava/util/ArrayList;-><init>(I)V

    .line 2109
    invoke-virtual {v5}, La/b/b/a;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_6
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_8

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v12

    .line 2111
    instance-of v13, v12, La/b/b/d;

    if-eqz v13, :cond_6

    .line 2112
    check-cast v12, La/b/b/d;

    .line 2114
    new-instance v13, Lcom/a/a/d/c;

    const-string v14, "r"

    invoke-static {v12, v14}, Lcom/a/a/d/i;->b(La/b/b/d;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    invoke-direct {v13, v14}, Lcom/a/a/d/c;-><init>(Ljava/lang/String;)V

    .line 2115
    new-instance v14, Lcom/a/a/d/c;

    invoke-static {v12, v8}, Lcom/a/a/d/i;->b(La/b/b/d;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    invoke-direct {v14, v15}, Lcom/a/a/d/c;-><init>(Ljava/lang/String;)V

    .line 2116
    new-instance v15, Lcom/a/a/d/c;

    const-string v0, "t"

    invoke-static {v12, v0}, Lcom/a/a/d/i;->b(La/b/b/d;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v15, v0}, Lcom/a/a/d/c;-><init>(Ljava/lang/String;)V

    .line 2118
    new-instance v0, Lcom/a/a/c/l$a;

    invoke-direct {v0, v13, v14, v15}, Lcom/a/a/c/l$a;-><init>(Lcom/a/a/d/c;Lcom/a/a/d/c;Lcom/a/a/d/c;)V

    .line 2119
    invoke-interface {v10, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_6
    move-object/from16 v0, p0

    goto :goto_6

    :cond_7
    move-object v10, v5

    .line 2125
    :cond_8
    :try_start_0
    new-instance v20, Lcom/a/a/c/l;

    const/4 v12, 0x0

    .line 2126
    invoke-static/range {p0 .. p0}, Lcom/a/a/c/e;->b(La/b/b/d;)Lcom/a/a/c/h;

    move-result-object v13

    .line 2127
    invoke-static/range {p0 .. p0}, Lcom/a/a/c/e;->c(La/b/b/d;)Ljava/util/Set;

    move-result-object v14

    .line 2128
    invoke-static/range {p0 .. p0}, Lcom/a/a/c/e;->d(La/b/b/d;)Lcom/a/a/a;

    move-result-object v15

    .line 2129
    invoke-static/range {p0 .. p0}, Lcom/a/a/c/e;->e(La/b/b/d;)Ljava/lang/String;

    move-result-object v17

    .line 2130
    invoke-static/range {p0 .. p0}, Lcom/a/a/c/e;->f(La/b/b/d;)Ljava/net/URI;

    move-result-object v18

    .line 2131
    invoke-static/range {p0 .. p0}, Lcom/a/a/c/e;->g(La/b/b/d;)Lcom/a/a/d/c;

    move-result-object v21

    .line 2132
    invoke-static/range {p0 .. p0}, Lcom/a/a/c/e;->h(La/b/b/d;)Lcom/a/a/d/c;

    move-result-object v22

    .line 2133
    invoke-static/range {p0 .. p0}, Lcom/a/a/c/e;->i(La/b/b/d;)Ljava/util/List;

    move-result-object v23

    const/16 v19, 0x0

    move-object/from16 v0, v20

    move-object v5, v7

    move-object v7, v9

    move-object v8, v11

    move-object v9, v10

    move-object v10, v12

    move-object v11, v13

    move-object v12, v14

    move-object v13, v15

    move-object/from16 v14, v17

    move-object/from16 v15, v18

    move-object/from16 v16, v21

    move-object/from16 v17, v22

    move-object/from16 v18, v23

    invoke-direct/range {v0 .. v19}, Lcom/a/a/c/l;-><init>(Lcom/a/a/d/c;Lcom/a/a/d/c;Lcom/a/a/d/c;Lcom/a/a/d/c;Lcom/a/a/d/c;Lcom/a/a/d/c;Lcom/a/a/d/c;Lcom/a/a/d/c;Ljava/util/List;Ljava/security/PrivateKey;Lcom/a/a/c/h;Ljava/util/Set;Lcom/a/a/a;Ljava/lang/String;Ljava/net/URI;Lcom/a/a/d/c;Lcom/a/a/d/c;Ljava/util/List;Ljava/security/KeyStore;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v20

    :catch_0
    move-exception v0

    .line 2138
    new-instance v1, Ljava/text/ParseException;

    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->getMessage()Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x0

    invoke-direct {v1, v0, v2}, Ljava/text/ParseException;-><init>(Ljava/lang/String;I)V

    throw v1

    :cond_9
    const/4 v2, 0x0

    .line 2070
    new-instance v0, Ljava/text/ParseException;

    const-string v1, "The key type \"kty\" must be RSA"

    invoke-direct {v0, v1, v2}, Ljava/text/ParseException;-><init>(Ljava/lang/String;I)V

    throw v0
.end method


# virtual methods
.method public a(Ljava/security/cert/X509Certificate;)Z
    .locals 3

    const/4 p1, 0x0

    .line 1922
    :try_start_0
    invoke-virtual {p0}, Lcom/a/a/c/l;->f()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/security/cert/X509Certificate;

    invoke-virtual {v0}, Ljava/security/cert/X509Certificate;->getPublicKey()Ljava/security/PublicKey;

    move-result-object v0

    check-cast v0, Ljava/security/interfaces/RSAPublicKey;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1926
    iget-object v1, p0, Lcom/a/a/c/l;->b:Lcom/a/a/d/c;

    invoke-virtual {v1}, Lcom/a/a/d/c;->c()Ljava/math/BigInteger;

    move-result-object v1

    invoke-interface {v0}, Ljava/security/interfaces/RSAPublicKey;->getPublicExponent()Ljava/math/BigInteger;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/math/BigInteger;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    return p1

    .line 1929
    :cond_0
    iget-object v1, p0, Lcom/a/a/c/l;->a:Lcom/a/a/d/c;

    invoke-virtual {v1}, Lcom/a/a/d/c;->c()Ljava/math/BigInteger;

    move-result-object v1

    invoke-interface {v0}, Ljava/security/interfaces/RSAPublicKey;->getModulus()Ljava/math/BigInteger;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/math/BigInteger;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    return p1

    :cond_1
    const/4 p1, 0x1

    :catch_0
    return p1
.end method

.method public d()Z
    .locals 1

    .line 1952
    iget-object v0, p0, Lcom/a/a/c/l;->c:Lcom/a/a/d/c;

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/a/a/c/l;->d:Lcom/a/a/d/c;

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/a/a/c/l;->j:Ljava/security/PrivateKey;

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method public e()La/b/b/d;
    .locals 8

    .line 1986
    invoke-super {p0}, Lcom/a/a/c/d;->e()La/b/b/d;

    move-result-object v0

    .line 1989
    iget-object v1, p0, Lcom/a/a/c/l;->a:Lcom/a/a/d/c;

    invoke-virtual {v1}, Lcom/a/a/d/c;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "n"

    invoke-virtual {v0, v2, v1}, La/b/b/d;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1990
    iget-object v1, p0, Lcom/a/a/c/l;->b:Lcom/a/a/d/c;

    invoke-virtual {v1}, Lcom/a/a/d/c;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "e"

    invoke-virtual {v0, v2, v1}, La/b/b/d;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1991
    iget-object v1, p0, Lcom/a/a/c/l;->c:Lcom/a/a/d/c;

    const-string v2, "d"

    if-eqz v1, :cond_0

    .line 1992
    invoke-virtual {v1}, Lcom/a/a/d/c;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, La/b/b/d;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1994
    :cond_0
    iget-object v1, p0, Lcom/a/a/c/l;->d:Lcom/a/a/d/c;

    if-eqz v1, :cond_1

    .line 1995
    invoke-virtual {v1}, Lcom/a/a/d/c;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v3, "p"

    invoke-virtual {v0, v3, v1}, La/b/b/d;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1997
    :cond_1
    iget-object v1, p0, Lcom/a/a/c/l;->e:Lcom/a/a/d/c;

    if-eqz v1, :cond_2

    .line 1998
    invoke-virtual {v1}, Lcom/a/a/d/c;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v3, "q"

    invoke-virtual {v0, v3, v1}, La/b/b/d;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2000
    :cond_2
    iget-object v1, p0, Lcom/a/a/c/l;->f:Lcom/a/a/d/c;

    if-eqz v1, :cond_3

    .line 2001
    invoke-virtual {v1}, Lcom/a/a/d/c;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v3, "dp"

    invoke-virtual {v0, v3, v1}, La/b/b/d;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2003
    :cond_3
    iget-object v1, p0, Lcom/a/a/c/l;->g:Lcom/a/a/d/c;

    if-eqz v1, :cond_4

    .line 2004
    invoke-virtual {v1}, Lcom/a/a/d/c;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v3, "dq"

    invoke-virtual {v0, v3, v1}, La/b/b/d;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2006
    :cond_4
    iget-object v1, p0, Lcom/a/a/c/l;->h:Lcom/a/a/d/c;

    if-eqz v1, :cond_5

    .line 2007
    invoke-virtual {v1}, Lcom/a/a/d/c;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v3, "qi"

    invoke-virtual {v0, v3, v1}, La/b/b/d;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2009
    :cond_5
    iget-object v1, p0, Lcom/a/a/c/l;->i:Ljava/util/List;

    if-eqz v1, :cond_7

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_7

    .line 2011
    new-instance v1, La/b/b/a;

    invoke-direct {v1}, La/b/b/a;-><init>()V

    .line 2013
    iget-object v3, p0, Lcom/a/a/c/l;->i:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_6

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/a/a/c/l$a;

    .line 2015
    new-instance v5, La/b/b/d;

    invoke-direct {v5}, La/b/b/d;-><init>()V

    .line 2016
    invoke-static {v4}, Lcom/a/a/c/l$a;->a(Lcom/a/a/c/l$a;)Lcom/a/a/d/c;

    move-result-object v6

    invoke-virtual {v6}, Lcom/a/a/d/c;->toString()Ljava/lang/String;

    move-result-object v6

    const-string v7, "r"

    invoke-virtual {v5, v7, v6}, La/b/b/d;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2017
    invoke-static {v4}, Lcom/a/a/c/l$a;->b(Lcom/a/a/c/l$a;)Lcom/a/a/d/c;

    move-result-object v6

    invoke-virtual {v6}, Lcom/a/a/d/c;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v2, v6}, La/b/b/d;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2018
    invoke-static {v4}, Lcom/a/a/c/l$a;->c(Lcom/a/a/c/l$a;)Lcom/a/a/d/c;

    move-result-object v4

    invoke-virtual {v4}, Lcom/a/a/d/c;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v6, "t"

    invoke-virtual {v5, v6, v4}, La/b/b/d;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2020
    invoke-virtual {v1, v5}, La/b/b/a;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_6
    const-string v2, "oth"

    .line 2023
    invoke-virtual {v0, v2, v1}, La/b/b/d;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_7
    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p0, p1, :cond_0

    return v0

    .line 2256
    :cond_0
    instance-of v1, p1, Lcom/a/a/c/l;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 2257
    :cond_1
    invoke-super {p0, p1}, Lcom/a/a/c/d;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    return v2

    .line 2258
    :cond_2
    check-cast p1, Lcom/a/a/c/l;

    .line 2259
    iget-object v1, p0, Lcom/a/a/c/l;->a:Lcom/a/a/d/c;

    iget-object v3, p1, Lcom/a/a/c/l;->a:Lcom/a/a/d/c;

    invoke-static {v1, v3}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/a/a/c/l;->b:Lcom/a/a/d/c;

    iget-object v3, p1, Lcom/a/a/c/l;->b:Lcom/a/a/d/c;

    .line 2260
    invoke-static {v1, v3}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/a/a/c/l;->c:Lcom/a/a/d/c;

    iget-object v3, p1, Lcom/a/a/c/l;->c:Lcom/a/a/d/c;

    .line 2261
    invoke-static {v1, v3}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/a/a/c/l;->d:Lcom/a/a/d/c;

    iget-object v3, p1, Lcom/a/a/c/l;->d:Lcom/a/a/d/c;

    .line 2262
    invoke-static {v1, v3}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/a/a/c/l;->e:Lcom/a/a/d/c;

    iget-object v3, p1, Lcom/a/a/c/l;->e:Lcom/a/a/d/c;

    .line 2263
    invoke-static {v1, v3}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/a/a/c/l;->f:Lcom/a/a/d/c;

    iget-object v3, p1, Lcom/a/a/c/l;->f:Lcom/a/a/d/c;

    .line 2264
    invoke-static {v1, v3}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/a/a/c/l;->g:Lcom/a/a/d/c;

    iget-object v3, p1, Lcom/a/a/c/l;->g:Lcom/a/a/d/c;

    .line 2265
    invoke-static {v1, v3}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/a/a/c/l;->h:Lcom/a/a/d/c;

    iget-object v3, p1, Lcom/a/a/c/l;->h:Lcom/a/a/d/c;

    .line 2266
    invoke-static {v1, v3}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/a/a/c/l;->i:Ljava/util/List;

    iget-object v3, p1, Lcom/a/a/c/l;->i:Ljava/util/List;

    .line 2267
    invoke-static {v1, v3}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/a/a/c/l;->j:Ljava/security/PrivateKey;

    iget-object p1, p1, Lcom/a/a/c/l;->j:Ljava/security/PrivateKey;

    .line 2268
    invoke-static {v1, p1}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_3

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    const/16 v0, 0xb

    .line 2274
    new-array v0, v0, [Ljava/lang/Object;

    invoke-super {p0}, Lcom/a/a/c/d;->hashCode()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/a/a/c/l;->a:Lcom/a/a/d/c;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/a/a/c/l;->b:Lcom/a/a/d/c;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/a/a/c/l;->c:Lcom/a/a/d/c;

    const/4 v2, 0x3

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/a/a/c/l;->d:Lcom/a/a/d/c;

    const/4 v2, 0x4

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/a/a/c/l;->e:Lcom/a/a/d/c;

    const/4 v2, 0x5

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/a/a/c/l;->f:Lcom/a/a/d/c;

    const/4 v2, 0x6

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/a/a/c/l;->g:Lcom/a/a/d/c;

    const/4 v2, 0x7

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/a/a/c/l;->h:Lcom/a/a/d/c;

    const/16 v2, 0x8

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/a/a/c/l;->i:Ljava/util/List;

    const/16 v2, 0x9

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/a/a/c/l;->j:Ljava/security/PrivateKey;

    const/16 v2, 0xa

    aput-object v1, v0, v2

    invoke-static {v0}, Ljava/util/Objects;->hash([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
