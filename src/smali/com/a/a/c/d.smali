.class public abstract Lcom/a/a/c/d;
.super Ljava/lang/Object;
.source "JWK.java"

# interfaces
.implements La/b/b/b;
.implements Ljava/io/Serializable;


# instance fields
.field private final a:Lcom/a/a/c/g;

.field private final b:Lcom/a/a/c/h;

.field private final c:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Lcom/a/a/c/f;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Lcom/a/a/a;

.field private final e:Ljava/lang/String;

.field private final f:Ljava/net/URI;

.field private final g:Lcom/a/a/d/c;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field private h:Lcom/a/a/d/c;

.field private final i:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/a/a/d/a;",
            ">;"
        }
    .end annotation
.end field

.field private final j:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/security/cert/X509Certificate;",
            ">;"
        }
    .end annotation
.end field

.field private final k:Ljava/security/KeyStore;


# direct methods
.method protected constructor <init>(Lcom/a/a/c/g;Lcom/a/a/c/h;Ljava/util/Set;Lcom/a/a/a;Ljava/lang/String;Ljava/net/URI;Lcom/a/a/d/c;Lcom/a/a/d/c;Ljava/util/List;Ljava/security/KeyStore;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/a/a/c/g;",
            "Lcom/a/a/c/h;",
            "Ljava/util/Set<",
            "Lcom/a/a/c/f;",
            ">;",
            "Lcom/a/a/a;",
            "Ljava/lang/String;",
            "Ljava/net/URI;",
            "Lcom/a/a/d/c;",
            "Lcom/a/a/d/c;",
            "Ljava/util/List<",
            "Lcom/a/a/d/a;",
            ">;",
            "Ljava/security/KeyStore;",
            ")V"
        }
    .end annotation

    .line 188
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    if-eqz p1, :cond_3

    .line 194
    iput-object p1, p0, Lcom/a/a/c/d;->a:Lcom/a/a/c/g;

    .line 196
    invoke-static {p2, p3}, Lcom/a/a/c/i;->a(Lcom/a/a/c/h;Ljava/util/Set;)Z

    move-result p1

    if-eqz p1, :cond_2

    .line 201
    iput-object p2, p0, Lcom/a/a/c/d;->b:Lcom/a/a/c/h;

    .line 202
    iput-object p3, p0, Lcom/a/a/c/d;->c:Ljava/util/Set;

    .line 204
    iput-object p4, p0, Lcom/a/a/c/d;->d:Lcom/a/a/a;

    .line 205
    iput-object p5, p0, Lcom/a/a/c/d;->e:Ljava/lang/String;

    .line 207
    iput-object p6, p0, Lcom/a/a/c/d;->f:Ljava/net/URI;

    .line 208
    iput-object p7, p0, Lcom/a/a/c/d;->g:Lcom/a/a/d/c;

    .line 209
    iput-object p8, p0, Lcom/a/a/c/d;->h:Lcom/a/a/d/c;

    if-eqz p9, :cond_1

    .line 211
    invoke-interface {p9}, Ljava/util/List;->isEmpty()Z

    move-result p1

    if-nez p1, :cond_0

    goto :goto_0

    .line 212
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "The X.509 certificate chain \"x5c\" must not be empty"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 214
    :cond_1
    :goto_0
    iput-object p9, p0, Lcom/a/a/c/d;->i:Ljava/util/List;

    .line 217
    :try_start_0
    invoke-static {p9}, Lcom/a/a/d/l;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/a/a/c/d;->j:Ljava/util/List;
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    .line 222
    iput-object p10, p0, Lcom/a/a/c/d;->k:Ljava/security/KeyStore;

    return-void

    :catch_0
    move-exception p1

    .line 219
    new-instance p2, Ljava/lang/IllegalArgumentException;

    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    const-string p4, "Invalid X.509 certificate chain \"x5c\": "

    invoke-virtual {p3, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/text/ParseException;->getMessage()Ljava/lang/String;

    move-result-object p4

    invoke-virtual {p3, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p3

    invoke-direct {p2, p3, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw p2

    .line 197
    :cond_2
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "The key use \"use\" and key options \"key_opts\" parameters are not consistent, see RFC 7517, section 4.3"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 191
    :cond_3
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "The key type \"kty\" parameter must not be null"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public static b(La/b/b/d;)Lcom/a/a/c/d;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/text/ParseException;
        }
    .end annotation

    const-string v0, "kty"

    .line 559
    invoke-static {p0, v0}, Lcom/a/a/d/i;->b(La/b/b/d;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/a/a/c/g;->a(Ljava/lang/String;)Lcom/a/a/c/g;

    move-result-object v0

    .line 561
    sget-object v1, Lcom/a/a/c/g;->a:Lcom/a/a/c/g;

    if-ne v0, v1, :cond_0

    .line 563
    invoke-static {p0}, Lcom/a/a/c/b;->a(La/b/b/d;)Lcom/a/a/c/b;

    move-result-object p0

    return-object p0

    .line 565
    :cond_0
    sget-object v1, Lcom/a/a/c/g;->b:Lcom/a/a/c/g;

    if-ne v0, v1, :cond_1

    .line 567
    invoke-static {p0}, Lcom/a/a/c/l;->a(La/b/b/d;)Lcom/a/a/c/l;

    move-result-object p0

    return-object p0

    .line 569
    :cond_1
    sget-object v1, Lcom/a/a/c/g;->c:Lcom/a/a/c/g;

    if-ne v0, v1, :cond_2

    .line 571
    invoke-static {p0}, Lcom/a/a/c/k;->a(La/b/b/d;)Lcom/a/a/c/k;

    move-result-object p0

    return-object p0

    .line 573
    :cond_2
    sget-object v1, Lcom/a/a/c/g;->d:Lcom/a/a/c/g;

    if-ne v0, v1, :cond_3

    .line 575
    invoke-static {p0}, Lcom/a/a/c/j;->a(La/b/b/d;)Lcom/a/a/c/j;

    move-result-object p0

    return-object p0

    .line 579
    :cond_3
    new-instance p0, Ljava/text/ParseException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unsupported key type \"kty\" parameter: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Ljava/text/ParseException;-><init>(Ljava/lang/String;I)V

    throw p0
.end method


# virtual methods
.method public b()Ljava/lang/String;
    .locals 1

    .line 510
    invoke-virtual {p0}, Lcom/a/a/c/d;->e()La/b/b/d;

    move-result-object v0

    invoke-virtual {v0}, La/b/b/d;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public abstract d()Z
.end method

.method public e()La/b/b/d;
    .locals 4

    .line 455
    new-instance v0, La/b/b/d;

    invoke-direct {v0}, La/b/b/d;-><init>()V

    .line 457
    iget-object v1, p0, Lcom/a/a/c/d;->a:Lcom/a/a/c/g;

    invoke-virtual {v1}, Lcom/a/a/c/g;->a()Ljava/lang/String;

    move-result-object v1

    const-string v2, "kty"

    invoke-virtual {v0, v2, v1}, La/b/b/d;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 459
    iget-object v1, p0, Lcom/a/a/c/d;->b:Lcom/a/a/c/h;

    if-eqz v1, :cond_0

    .line 460
    invoke-virtual {v1}, Lcom/a/a/c/h;->a()Ljava/lang/String;

    move-result-object v1

    const-string v2, "use"

    invoke-virtual {v0, v2, v1}, La/b/b/d;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 463
    :cond_0
    iget-object v1, p0, Lcom/a/a/c/d;->c:Ljava/util/Set;

    if-eqz v1, :cond_2

    .line 465
    new-instance v2, Ljava/util/ArrayList;

    invoke-interface {v1}, Ljava/util/Set;->size()I

    move-result v1

    invoke-direct {v2, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 467
    iget-object v1, p0, Lcom/a/a/c/d;->c:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/a/a/c/f;

    .line 468
    invoke-virtual {v3}, Lcom/a/a/c/f;->a()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    const-string v1, "key_ops"

    .line 471
    invoke-virtual {v0, v1, v2}, La/b/b/d;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 474
    :cond_2
    iget-object v1, p0, Lcom/a/a/c/d;->d:Lcom/a/a/a;

    if-eqz v1, :cond_3

    .line 475
    invoke-virtual {v1}, Lcom/a/a/a;->a()Ljava/lang/String;

    move-result-object v1

    const-string v2, "alg"

    invoke-virtual {v0, v2, v1}, La/b/b/d;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 478
    :cond_3
    iget-object v1, p0, Lcom/a/a/c/d;->e:Ljava/lang/String;

    if-eqz v1, :cond_4

    const-string v2, "kid"

    .line 479
    invoke-virtual {v0, v2, v1}, La/b/b/d;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 482
    :cond_4
    iget-object v1, p0, Lcom/a/a/c/d;->f:Ljava/net/URI;

    if-eqz v1, :cond_5

    .line 483
    invoke-virtual {v1}, Ljava/net/URI;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "x5u"

    invoke-virtual {v0, v2, v1}, La/b/b/d;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 486
    :cond_5
    iget-object v1, p0, Lcom/a/a/c/d;->g:Lcom/a/a/d/c;

    if-eqz v1, :cond_6

    .line 487
    invoke-virtual {v1}, Lcom/a/a/d/c;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "x5t"

    invoke-virtual {v0, v2, v1}, La/b/b/d;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 490
    :cond_6
    iget-object v1, p0, Lcom/a/a/c/d;->h:Lcom/a/a/d/c;

    if-eqz v1, :cond_7

    .line 491
    invoke-virtual {v1}, Lcom/a/a/d/c;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "x5t#S256"

    invoke-virtual {v0, v2, v1}, La/b/b/d;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 494
    :cond_7
    iget-object v1, p0, Lcom/a/a/c/d;->i:Ljava/util/List;

    if-eqz v1, :cond_8

    const-string v2, "x5c"

    .line 495
    invoke-virtual {v0, v2, v1}, La/b/b/d;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_8
    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p0, p1, :cond_0

    return v0

    .line 825
    :cond_0
    instance-of v1, p1, Lcom/a/a/c/d;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 826
    :cond_1
    check-cast p1, Lcom/a/a/c/d;

    .line 827
    iget-object v1, p0, Lcom/a/a/c/d;->a:Lcom/a/a/c/g;

    iget-object v3, p1, Lcom/a/a/c/d;->a:Lcom/a/a/c/g;

    invoke-static {v1, v3}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/a/a/c/d;->b:Lcom/a/a/c/h;

    iget-object v3, p1, Lcom/a/a/c/d;->b:Lcom/a/a/c/h;

    .line 828
    invoke-static {v1, v3}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/a/a/c/d;->c:Ljava/util/Set;

    iget-object v3, p1, Lcom/a/a/c/d;->c:Ljava/util/Set;

    .line 829
    invoke-static {v1, v3}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/a/a/c/d;->d:Lcom/a/a/a;

    iget-object v3, p1, Lcom/a/a/c/d;->d:Lcom/a/a/a;

    .line 830
    invoke-static {v1, v3}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/a/a/c/d;->e:Ljava/lang/String;

    iget-object v3, p1, Lcom/a/a/c/d;->e:Ljava/lang/String;

    .line 831
    invoke-static {v1, v3}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/a/a/c/d;->f:Ljava/net/URI;

    iget-object v3, p1, Lcom/a/a/c/d;->f:Ljava/net/URI;

    .line 832
    invoke-static {v1, v3}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/a/a/c/d;->g:Lcom/a/a/d/c;

    iget-object v3, p1, Lcom/a/a/c/d;->g:Lcom/a/a/d/c;

    .line 833
    invoke-static {v1, v3}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/a/a/c/d;->h:Lcom/a/a/d/c;

    iget-object v3, p1, Lcom/a/a/c/d;->h:Lcom/a/a/d/c;

    .line 834
    invoke-static {v1, v3}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/a/a/c/d;->i:Ljava/util/List;

    iget-object v3, p1, Lcom/a/a/c/d;->i:Ljava/util/List;

    .line 835
    invoke-static {v1, v3}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/a/a/c/d;->j:Ljava/util/List;

    iget-object v3, p1, Lcom/a/a/c/d;->j:Ljava/util/List;

    .line 836
    invoke-static {v1, v3}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/a/a/c/d;->k:Ljava/security/KeyStore;

    iget-object p1, p1, Lcom/a/a/c/d;->k:Ljava/security/KeyStore;

    .line 837
    invoke-static {v1, p1}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public f()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/security/cert/X509Certificate;",
            ">;"
        }
    .end annotation

    .line 347
    iget-object v0, p0, Lcom/a/a/c/d;->j:Ljava/util/List;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return-object v0

    .line 351
    :cond_0
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    const/16 v0, 0xb

    .line 843
    new-array v0, v0, [Ljava/lang/Object;

    iget-object v1, p0, Lcom/a/a/c/d;->a:Lcom/a/a/c/g;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/a/a/c/d;->b:Lcom/a/a/c/h;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/a/a/c/d;->c:Ljava/util/Set;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/a/a/c/d;->d:Lcom/a/a/a;

    const/4 v2, 0x3

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/a/a/c/d;->e:Ljava/lang/String;

    const/4 v2, 0x4

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/a/a/c/d;->f:Ljava/net/URI;

    const/4 v2, 0x5

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/a/a/c/d;->g:Lcom/a/a/d/c;

    const/4 v2, 0x6

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/a/a/c/d;->h:Lcom/a/a/d/c;

    const/4 v2, 0x7

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/a/a/c/d;->i:Ljava/util/List;

    const/16 v2, 0x8

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/a/a/c/d;->j:Ljava/util/List;

    const/16 v2, 0x9

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/a/a/c/d;->k:Ljava/security/KeyStore;

    const/16 v2, 0xa

    aput-object v1, v0, v2

    invoke-static {v0}, Ljava/util/Objects;->hash([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .line 520
    invoke-virtual {p0}, Lcom/a/a/c/d;->e()La/b/b/d;

    move-result-object v0

    invoke-virtual {v0}, La/b/b/d;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
