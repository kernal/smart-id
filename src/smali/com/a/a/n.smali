.class public Lcom/a/a/n;
.super Lcom/a/a/g;
.source "JWEObject.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/a/a/n$a;
    }
.end annotation


# instance fields
.field private a:Lcom/a/a/m;

.field private b:Lcom/a/a/d/c;

.field private c:Lcom/a/a/d/c;

.field private d:Lcom/a/a/d/c;

.field private e:Lcom/a/a/d/c;

.field private f:Lcom/a/a/n$a;


# direct methods
.method public constructor <init>(Lcom/a/a/d/c;Lcom/a/a/d/c;Lcom/a/a/d/c;Lcom/a/a/d/c;Lcom/a/a/d/c;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/text/ParseException;
        }
    .end annotation

    .line 159
    invoke-direct {p0}, Lcom/a/a/g;-><init>()V

    if-eqz p1, :cond_7

    const/4 v0, 0x0

    .line 167
    :try_start_0
    invoke-static {p1}, Lcom/a/a/m;->a(Lcom/a/a/d/c;)Lcom/a/a/m;

    move-result-object v1

    iput-object v1, p0, Lcom/a/a/n;->a:Lcom/a/a/m;
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v1, 0x0

    if-eqz p2, :cond_1

    .line 174
    invoke-virtual {p2}, Lcom/a/a/d/c;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_0

    goto :goto_0

    .line 180
    :cond_0
    iput-object p2, p0, Lcom/a/a/n;->b:Lcom/a/a/d/c;

    goto :goto_1

    .line 176
    :cond_1
    :goto_0
    iput-object v1, p0, Lcom/a/a/n;->b:Lcom/a/a/d/c;

    :goto_1
    if-eqz p3, :cond_3

    .line 183
    invoke-virtual {p3}, Lcom/a/a/d/c;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_2

    goto :goto_2

    .line 189
    :cond_2
    iput-object p3, p0, Lcom/a/a/n;->c:Lcom/a/a/d/c;

    goto :goto_3

    .line 185
    :cond_3
    :goto_2
    iput-object v1, p0, Lcom/a/a/n;->c:Lcom/a/a/d/c;

    :goto_3
    if-eqz p4, :cond_6

    .line 197
    iput-object p4, p0, Lcom/a/a/n;->d:Lcom/a/a/d/c;

    if-eqz p5, :cond_5

    .line 199
    invoke-virtual {p5}, Lcom/a/a/d/c;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_4

    goto :goto_4

    .line 205
    :cond_4
    iput-object p5, p0, Lcom/a/a/n;->e:Lcom/a/a/d/c;

    goto :goto_5

    .line 201
    :cond_5
    :goto_4
    iput-object v1, p0, Lcom/a/a/n;->e:Lcom/a/a/d/c;

    .line 208
    :goto_5
    sget-object v1, Lcom/a/a/n$a;->b:Lcom/a/a/n$a;

    iput-object v1, p0, Lcom/a/a/n;->f:Lcom/a/a/n$a;

    const/4 v1, 0x5

    .line 210
    new-array v1, v1, [Lcom/a/a/d/c;

    aput-object p1, v1, v0

    const/4 p1, 0x1

    aput-object p2, v1, p1

    const/4 p1, 0x2

    aput-object p3, v1, p1

    const/4 p1, 0x3

    aput-object p4, v1, p1

    const/4 p1, 0x4

    aput-object p5, v1, p1

    invoke-virtual {p0, v1}, Lcom/a/a/n;->a([Lcom/a/a/d/c;)V

    return-void

    .line 194
    :cond_6
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "The fourth part must not be null"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    :catch_0
    move-exception p1

    .line 171
    new-instance p2, Ljava/text/ParseException;

    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    const-string p4, "Invalid JWE header: "

    invoke-virtual {p3, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/text/ParseException;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1, v0}, Ljava/text/ParseException;-><init>(Ljava/lang/String;I)V

    throw p2

    .line 163
    :cond_7
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "The first part must not be null"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public constructor <init>(Lcom/a/a/m;Lcom/a/a/v;)V
    .locals 0

    .line 111
    invoke-direct {p0}, Lcom/a/a/g;-><init>()V

    if-eqz p1, :cond_1

    .line 118
    iput-object p1, p0, Lcom/a/a/n;->a:Lcom/a/a/m;

    if-eqz p2, :cond_0

    .line 125
    invoke-virtual {p0, p2}, Lcom/a/a/n;->a(Lcom/a/a/v;)V

    const/4 p1, 0x0

    .line 127
    iput-object p1, p0, Lcom/a/a/n;->b:Lcom/a/a/d/c;

    .line 129
    iput-object p1, p0, Lcom/a/a/n;->d:Lcom/a/a/d/c;

    .line 131
    sget-object p1, Lcom/a/a/n$a;->a:Lcom/a/a/n$a;

    iput-object p1, p0, Lcom/a/a/n;->f:Lcom/a/a/n$a;

    return-void

    .line 122
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "The payload must not be null"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 115
    :cond_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "The JWE header must not be null"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public static b(Ljava/lang/String;)Lcom/a/a/n;
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/text/ParseException;
        }
    .end annotation

    .line 502
    invoke-static {p0}, Lcom/a/a/g;->a(Ljava/lang/String;)[Lcom/a/a/d/c;

    move-result-object p0

    .line 504
    array-length v0, p0

    const/4 v1, 0x0

    const/4 v2, 0x5

    if-ne v0, v2, :cond_0

    .line 509
    new-instance v0, Lcom/a/a/n;

    aget-object v4, p0, v1

    const/4 v1, 0x1

    aget-object v5, p0, v1

    const/4 v1, 0x2

    aget-object v6, p0, v1

    const/4 v1, 0x3

    aget-object v7, p0, v1

    const/4 v1, 0x4

    aget-object v8, p0, v1

    move-object v3, v0

    invoke-direct/range {v3 .. v8}, Lcom/a/a/n;-><init>(Lcom/a/a/d/c;Lcom/a/a/d/c;Lcom/a/a/d/c;Lcom/a/a/d/c;Lcom/a/a/d/c;)V

    return-object v0

    .line 506
    :cond_0
    new-instance p0, Ljava/text/ParseException;

    const-string v0, "Unexpected number of Base64URL parts, must be five"

    invoke-direct {p0, v0, v1}, Ljava/text/ParseException;-><init>(Ljava/lang/String;I)V

    throw p0
.end method

.method private b(Lcom/a/a/l;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/a/a/f;
        }
    .end annotation

    .line 334
    invoke-interface {p1}, Lcom/a/a/l;->a()Ljava/util/Set;

    move-result-object v0

    invoke-virtual {p0}, Lcom/a/a/n;->c()Lcom/a/a/m;

    move-result-object v1

    invoke-virtual {v1}, Lcom/a/a/m;->g()Lcom/a/a/i;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    const-string v1, "The \""

    if-eqz v0, :cond_1

    .line 340
    invoke-interface {p1}, Lcom/a/a/l;->b()Ljava/util/Set;

    move-result-object v0

    invoke-virtual {p0}, Lcom/a/a/n;->c()Lcom/a/a/m;

    move-result-object v2

    invoke-virtual {v2}, Lcom/a/a/m;->h()Lcom/a/a/d;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 342
    :cond_0
    new-instance v0, Lcom/a/a/f;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/a/a/n;->c()Lcom/a/a/m;

    move-result-object v1

    invoke-virtual {v1}, Lcom/a/a/m;->h()Lcom/a/a/d;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, "\" encryption method or key size is not supported by the JWE encrypter: Supported methods: "

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 343
    invoke-interface {p1}, Lcom/a/a/l;->b()Ljava/util/Set;

    move-result-object p1

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/a/a/f;-><init>(Ljava/lang/String;)V

    throw v0

    .line 336
    :cond_1
    new-instance v0, Lcom/a/a/f;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/a/a/n;->c()Lcom/a/a/m;

    move-result-object v1

    invoke-virtual {v1}, Lcom/a/a/m;->g()Lcom/a/a/i;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, "\" algorithm is not supported by the JWE encrypter: Supported algorithms: "

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337
    invoke-interface {p1}, Lcom/a/a/l;->a()Ljava/util/Set;

    move-result-object p1

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/a/a/f;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private i()V
    .locals 2

    .line 288
    iget-object v0, p0, Lcom/a/a/n;->f:Lcom/a/a/n$a;

    sget-object v1, Lcom/a/a/n$a;->a:Lcom/a/a/n$a;

    if-ne v0, v1, :cond_0

    return-void

    .line 290
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "The JWE object must be in an unencrypted state"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private j()V
    .locals 2

    .line 302
    iget-object v0, p0, Lcom/a/a/n;->f:Lcom/a/a/n$a;

    sget-object v1, Lcom/a/a/n$a;->b:Lcom/a/a/n$a;

    if-ne v0, v1, :cond_0

    return-void

    .line 304
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "The JWE object must be in an encrypted state"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private k()V
    .locals 2

    .line 318
    iget-object v0, p0, Lcom/a/a/n;->f:Lcom/a/a/n$a;

    sget-object v1, Lcom/a/a/n$a;->b:Lcom/a/a/n$a;

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lcom/a/a/n;->f:Lcom/a/a/n$a;

    sget-object v1, Lcom/a/a/n$a;->c:Lcom/a/a/n$a;

    if-ne v0, v1, :cond_0

    goto :goto_0

    .line 320
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "The JWE object must be in an encrypted or decrypted state"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    :goto_0
    return-void
.end method


# virtual methods
.method public declared-synchronized a(Lcom/a/a/k;)V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/a/a/f;
        }
    .end annotation

    monitor-enter p0

    .line 412
    :try_start_0
    invoke-direct {p0}, Lcom/a/a/n;->j()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 415
    :try_start_1
    new-instance v0, Lcom/a/a/v;

    invoke-virtual {p0}, Lcom/a/a/n;->c()Lcom/a/a/m;

    move-result-object v2

    .line 416
    invoke-virtual {p0}, Lcom/a/a/n;->d()Lcom/a/a/d/c;

    move-result-object v3

    .line 417
    invoke-virtual {p0}, Lcom/a/a/n;->e()Lcom/a/a/d/c;

    move-result-object v4

    .line 418
    invoke-virtual {p0}, Lcom/a/a/n;->f()Lcom/a/a/d/c;

    move-result-object v5

    .line 419
    invoke-virtual {p0}, Lcom/a/a/n;->g()Lcom/a/a/d/c;

    move-result-object v6

    move-object v1, p1

    .line 415
    invoke-interface/range {v1 .. v6}, Lcom/a/a/k;->a(Lcom/a/a/m;Lcom/a/a/d/c;Lcom/a/a/d/c;Lcom/a/a/d/c;Lcom/a/a/d/c;)[B

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/a/a/v;-><init>([B)V

    invoke-virtual {p0, v0}, Lcom/a/a/n;->a(Lcom/a/a/v;)V
    :try_end_1
    .catch Lcom/a/a/f; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 432
    :try_start_2
    sget-object p1, Lcom/a/a/n$a;->c:Lcom/a/a/n$a;

    iput-object p1, p0, Lcom/a/a/n;->f:Lcom/a/a/n$a;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 433
    monitor-exit p0

    return-void

    :catch_0
    move-exception p1

    .line 429
    :try_start_3
    new-instance v0, Lcom/a/a/f;

    invoke-virtual {p1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lcom/a/a/f;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0

    :catch_1
    move-exception p1

    .line 423
    throw p1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized a(Lcom/a/a/l;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/a/a/f;
        }
    .end annotation

    monitor-enter p0

    .line 363
    :try_start_0
    invoke-direct {p0}, Lcom/a/a/n;->i()V

    .line 365
    invoke-direct {p0, p1}, Lcom/a/a/n;->b(Lcom/a/a/l;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 370
    :try_start_1
    invoke-virtual {p0}, Lcom/a/a/n;->c()Lcom/a/a/m;

    move-result-object v0

    invoke-virtual {p0}, Lcom/a/a/n;->a()Lcom/a/a/v;

    move-result-object v1

    invoke-virtual {v1}, Lcom/a/a/v;->a()[B

    move-result-object v1

    invoke-interface {p1, v0, v1}, Lcom/a/a/l;->a(Lcom/a/a/m;[B)Lcom/a/a/j;

    move-result-object p1
    :try_end_1
    .catch Lcom/a/a/f; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 384
    :try_start_2
    invoke-virtual {p1}, Lcom/a/a/j;->a()Lcom/a/a/m;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 385
    invoke-virtual {p1}, Lcom/a/a/j;->a()Lcom/a/a/m;

    move-result-object v0

    iput-object v0, p0, Lcom/a/a/n;->a:Lcom/a/a/m;

    .line 388
    :cond_0
    invoke-virtual {p1}, Lcom/a/a/j;->b()Lcom/a/a/d/c;

    move-result-object v0

    iput-object v0, p0, Lcom/a/a/n;->b:Lcom/a/a/d/c;

    .line 389
    invoke-virtual {p1}, Lcom/a/a/j;->c()Lcom/a/a/d/c;

    move-result-object v0

    iput-object v0, p0, Lcom/a/a/n;->c:Lcom/a/a/d/c;

    .line 390
    invoke-virtual {p1}, Lcom/a/a/j;->d()Lcom/a/a/d/c;

    move-result-object v0

    iput-object v0, p0, Lcom/a/a/n;->d:Lcom/a/a/d/c;

    .line 391
    invoke-virtual {p1}, Lcom/a/a/j;->e()Lcom/a/a/d/c;

    move-result-object p1

    iput-object p1, p0, Lcom/a/a/n;->e:Lcom/a/a/d/c;

    .line 393
    sget-object p1, Lcom/a/a/n$a;->b:Lcom/a/a/n$a;

    iput-object p1, p0, Lcom/a/a/n;->f:Lcom/a/a/n$a;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 394
    monitor-exit p0

    return-void

    :catch_0
    move-exception p1

    .line 380
    :try_start_3
    new-instance v0, Lcom/a/a/f;

    invoke-virtual {p1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lcom/a/a/f;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0

    :catch_1
    move-exception p1

    .line 374
    throw p1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public c()Lcom/a/a/m;
    .locals 1

    .line 217
    iget-object v0, p0, Lcom/a/a/n;->a:Lcom/a/a/m;

    return-object v0
.end method

.method public d()Lcom/a/a/d/c;
    .locals 1

    .line 229
    iget-object v0, p0, Lcom/a/a/n;->b:Lcom/a/a/d/c;

    return-object v0
.end method

.method public e()Lcom/a/a/d/c;
    .locals 1

    .line 241
    iget-object v0, p0, Lcom/a/a/n;->c:Lcom/a/a/d/c;

    return-object v0
.end method

.method public f()Lcom/a/a/d/c;
    .locals 1

    .line 253
    iget-object v0, p0, Lcom/a/a/n;->d:Lcom/a/a/d/c;

    return-object v0
.end method

.method public g()Lcom/a/a/d/c;
    .locals 1

    .line 265
    iget-object v0, p0, Lcom/a/a/n;->e:Lcom/a/a/d/c;

    return-object v0
.end method

.method public h()Ljava/lang/String;
    .locals 3

    .line 456
    invoke-direct {p0}, Lcom/a/a/n;->k()V

    .line 458
    new-instance v0, Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/a/a/n;->a:Lcom/a/a/m;

    invoke-virtual {v1}, Lcom/a/a/m;->e()Lcom/a/a/d/c;

    move-result-object v1

    invoke-virtual {v1}, Lcom/a/a/d/c;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/16 v1, 0x2e

    .line 459
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 461
    iget-object v2, p0, Lcom/a/a/n;->b:Lcom/a/a/d/c;

    if-eqz v2, :cond_0

    .line 463
    invoke-virtual {v2}, Lcom/a/a/d/c;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 466
    :cond_0
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 468
    iget-object v2, p0, Lcom/a/a/n;->c:Lcom/a/a/d/c;

    if-eqz v2, :cond_1

    .line 470
    invoke-virtual {v2}, Lcom/a/a/d/c;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 473
    :cond_1
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 475
    iget-object v2, p0, Lcom/a/a/n;->d:Lcom/a/a/d/c;

    invoke-virtual {v2}, Lcom/a/a/d/c;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 477
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 479
    iget-object v1, p0, Lcom/a/a/n;->e:Lcom/a/a/d/c;

    if-eqz v1, :cond_2

    .line 481
    invoke-virtual {v1}, Lcom/a/a/d/c;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 484
    :cond_2
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
