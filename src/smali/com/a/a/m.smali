.class public final Lcom/a/a/m;
.super Lcom/a/a/b;
.source "JWEHeader.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/a/a/m$a;
    }
.end annotation


# static fields
.field private static final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final b:Lcom/a/a/d;

.field private final c:Lcom/a/a/c/d;

.field private final d:Lcom/a/a/c;

.field private final e:Lcom/a/a/d/c;

.field private final f:Lcom/a/a/d/c;

.field private final g:Lcom/a/a/d/c;

.field private final h:I

.field private final i:Lcom/a/a/d/c;

.field private final j:Lcom/a/a/d/c;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 97
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    const-string v1, "alg"

    .line 99
    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    const-string v1, "enc"

    .line 100
    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    const-string v1, "epk"

    .line 101
    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    const-string v1, "zip"

    .line 102
    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    const-string v1, "jku"

    .line 103
    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    const-string v1, "jwk"

    .line 104
    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    const-string v1, "x5u"

    .line 105
    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    const-string v1, "x5t"

    .line 106
    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    const-string v1, "x5t#S256"

    .line 107
    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    const-string v1, "x5c"

    .line 108
    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    const-string v1, "kid"

    .line 109
    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    const-string v1, "typ"

    .line 110
    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    const-string v1, "cty"

    .line 111
    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    const-string v1, "crit"

    .line 112
    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    const-string v1, "apu"

    .line 113
    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    const-string v1, "apv"

    .line 114
    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    const-string v1, "p2s"

    .line 115
    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    const-string v1, "p2c"

    .line 116
    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    const-string v1, "iv"

    .line 117
    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    const-string v1, "authTag"

    .line 118
    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 120
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    sput-object v0, Lcom/a/a/m;->a:Ljava/util/Set;

    return-void
.end method

.method public constructor <init>(Lcom/a/a/a;Lcom/a/a/d;Lcom/a/a/h;Ljava/lang/String;Ljava/util/Set;Ljava/net/URI;Lcom/a/a/c/d;Ljava/net/URI;Lcom/a/a/d/c;Lcom/a/a/d/c;Ljava/util/List;Ljava/lang/String;Lcom/a/a/c/d;Lcom/a/a/c;Lcom/a/a/d/c;Lcom/a/a/d/c;Lcom/a/a/d/c;ILcom/a/a/d/c;Lcom/a/a/d/c;Ljava/util/Map;Lcom/a/a/d/c;)V
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/a/a/a;",
            "Lcom/a/a/d;",
            "Lcom/a/a/h;",
            "Ljava/lang/String;",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/net/URI;",
            "Lcom/a/a/c/d;",
            "Ljava/net/URI;",
            "Lcom/a/a/d/c;",
            "Lcom/a/a/d/c;",
            "Ljava/util/List<",
            "Lcom/a/a/d/a;",
            ">;",
            "Ljava/lang/String;",
            "Lcom/a/a/c/d;",
            "Lcom/a/a/c;",
            "Lcom/a/a/d/c;",
            "Lcom/a/a/d/c;",
            "Lcom/a/a/d/c;",
            "I",
            "Lcom/a/a/d/c;",
            "Lcom/a/a/d/c;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;",
            "Lcom/a/a/d/c;",
            ")V"
        }
    .end annotation

    move-object/from16 v14, p0

    move-object/from16 v15, p2

    move-object/from16 v13, p13

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p3

    move-object/from16 v3, p4

    move-object/from16 v4, p5

    move-object/from16 v5, p6

    move-object/from16 v6, p7

    move-object/from16 v7, p8

    move-object/from16 v8, p9

    move-object/from16 v9, p10

    move-object/from16 v10, p11

    move-object/from16 v11, p12

    move-object/from16 v12, p21

    move-object v14, v13

    move-object/from16 v13, p22

    .line 844
    invoke-direct/range {v0 .. v13}, Lcom/a/a/b;-><init>(Lcom/a/a/a;Lcom/a/a/h;Ljava/lang/String;Ljava/util/Set;Ljava/net/URI;Lcom/a/a/c/d;Ljava/net/URI;Lcom/a/a/d/c;Lcom/a/a/d/c;Ljava/util/List;Ljava/lang/String;Ljava/util/Map;Lcom/a/a/d/c;)V

    .line 846
    invoke-virtual/range {p1 .. p1}, Lcom/a/a/a;->a()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/a/a/a;->a:Lcom/a/a/a;

    invoke-virtual {v1}, Lcom/a/a/a;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    if-eqz v15, :cond_2

    if-eqz v14, :cond_1

    .line 854
    invoke-virtual/range {p13 .. p13}, Lcom/a/a/c/d;->d()Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    .line 855
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Ephemeral public key should not be a private key"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    :goto_0
    move-object/from16 v0, p0

    move-object v1, v14

    .line 858
    iput-object v15, v0, Lcom/a/a/m;->b:Lcom/a/a/d;

    .line 860
    iput-object v1, v0, Lcom/a/a/m;->c:Lcom/a/a/c/d;

    move-object/from16 v1, p14

    .line 861
    iput-object v1, v0, Lcom/a/a/m;->d:Lcom/a/a/c;

    move-object/from16 v1, p15

    .line 862
    iput-object v1, v0, Lcom/a/a/m;->e:Lcom/a/a/d/c;

    move-object/from16 v1, p16

    .line 863
    iput-object v1, v0, Lcom/a/a/m;->f:Lcom/a/a/d/c;

    move-object/from16 v1, p17

    .line 864
    iput-object v1, v0, Lcom/a/a/m;->g:Lcom/a/a/d/c;

    move/from16 v1, p18

    .line 865
    iput v1, v0, Lcom/a/a/m;->h:I

    move-object/from16 v1, p19

    .line 866
    iput-object v1, v0, Lcom/a/a/m;->i:Lcom/a/a/d/c;

    move-object/from16 v1, p20

    .line 867
    iput-object v1, v0, Lcom/a/a/m;->j:Lcom/a/a/d/c;

    return-void

    :cond_2
    move-object/from16 v0, p0

    .line 851
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "The encryption method \"enc\" parameter must not be null"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_3
    move-object/from16 v0, p0

    .line 847
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "The JWE algorithm cannot be \"none\""

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public static a(La/b/b/d;Lcom/a/a/d/c;)Lcom/a/a/m;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/text/ParseException;
        }
    .end annotation

    .line 1174
    invoke-static {p0}, Lcom/a/a/e;->a(La/b/b/d;)Lcom/a/a/a;

    move-result-object v0

    .line 1176
    instance-of v1, v0, Lcom/a/a/i;

    if-eqz v1, :cond_15

    .line 1181
    invoke-static {p0}, Lcom/a/a/m;->b(La/b/b/d;)Lcom/a/a/d;

    move-result-object v1

    .line 1183
    new-instance v2, Lcom/a/a/m$a;

    check-cast v0, Lcom/a/a/i;

    invoke-direct {v2, v0, v1}, Lcom/a/a/m$a;-><init>(Lcom/a/a/i;Lcom/a/a/d;)V

    invoke-virtual {v2, p1}, Lcom/a/a/m$a;->h(Lcom/a/a/d/c;)Lcom/a/a/m$a;

    move-result-object p1

    .line 1186
    invoke-virtual {p0}, La/b/b/d;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_14

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    const-string v2, "alg"

    .line 1188
    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    goto :goto_0

    :cond_0
    const-string v2, "enc"

    .line 1190
    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    goto :goto_0

    :cond_1
    const-string v2, "typ"

    .line 1192
    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1193
    new-instance v2, Lcom/a/a/h;

    invoke-static {p0, v1}, Lcom/a/a/d/i;->b(La/b/b/d;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Lcom/a/a/h;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1, v2}, Lcom/a/a/m$a;->a(Lcom/a/a/h;)Lcom/a/a/m$a;

    move-result-object p1

    goto :goto_0

    :cond_2
    const-string v2, "cty"

    .line 1194
    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1195
    invoke-static {p0, v1}, Lcom/a/a/d/i;->b(La/b/b/d;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/a/a/m$a;->a(Ljava/lang/String;)Lcom/a/a/m$a;

    move-result-object p1

    goto :goto_0

    :cond_3
    const-string v2, "crit"

    .line 1196
    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 1197
    new-instance v2, Ljava/util/HashSet;

    invoke-static {p0, v1}, Lcom/a/a/d/i;->f(La/b/b/d;Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v2, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    invoke-virtual {p1, v2}, Lcom/a/a/m$a;->a(Ljava/util/Set;)Lcom/a/a/m$a;

    move-result-object p1

    goto :goto_0

    :cond_4
    const-string v2, "jku"

    .line 1198
    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 1199
    invoke-static {p0, v1}, Lcom/a/a/d/i;->c(La/b/b/d;Ljava/lang/String;)Ljava/net/URI;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/a/a/m$a;->a(Ljava/net/URI;)Lcom/a/a/m$a;

    move-result-object p1

    goto :goto_0

    :cond_5
    const-string v2, "jwk"

    .line 1200
    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 1201
    invoke-static {p0, v1}, Lcom/a/a/d/i;->g(La/b/b/d;Ljava/lang/String;)La/b/b/d;

    move-result-object v1

    invoke-static {v1}, Lcom/a/a/c/d;->b(La/b/b/d;)Lcom/a/a/c/d;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/a/a/m$a;->a(Lcom/a/a/c/d;)Lcom/a/a/m$a;

    move-result-object p1

    goto/16 :goto_0

    :cond_6
    const-string v2, "x5u"

    .line 1202
    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 1203
    invoke-static {p0, v1}, Lcom/a/a/d/i;->c(La/b/b/d;Ljava/lang/String;)Ljava/net/URI;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/a/a/m$a;->b(Ljava/net/URI;)Lcom/a/a/m$a;

    move-result-object p1

    goto/16 :goto_0

    :cond_7
    const-string v2, "x5t"

    .line 1204
    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 1205
    new-instance v2, Lcom/a/a/d/c;

    invoke-static {p0, v1}, Lcom/a/a/d/i;->b(La/b/b/d;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Lcom/a/a/d/c;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1, v2}, Lcom/a/a/m$a;->a(Lcom/a/a/d/c;)Lcom/a/a/m$a;

    move-result-object p1

    goto/16 :goto_0

    :cond_8
    const-string v2, "x5t#S256"

    .line 1206
    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 1207
    new-instance v2, Lcom/a/a/d/c;

    invoke-static {p0, v1}, Lcom/a/a/d/i;->b(La/b/b/d;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Lcom/a/a/d/c;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1, v2}, Lcom/a/a/m$a;->b(Lcom/a/a/d/c;)Lcom/a/a/m$a;

    move-result-object p1

    goto/16 :goto_0

    :cond_9
    const-string v2, "x5c"

    .line 1208
    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_a

    .line 1209
    invoke-static {p0, v1}, Lcom/a/a/d/i;->d(La/b/b/d;Ljava/lang/String;)La/b/b/a;

    move-result-object v1

    invoke-static {v1}, Lcom/a/a/d/l;->a(La/b/b/a;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/a/a/m$a;->a(Ljava/util/List;)Lcom/a/a/m$a;

    move-result-object p1

    goto/16 :goto_0

    :cond_a
    const-string v2, "kid"

    .line 1210
    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_b

    .line 1211
    invoke-static {p0, v1}, Lcom/a/a/d/i;->b(La/b/b/d;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/a/a/m$a;->b(Ljava/lang/String;)Lcom/a/a/m$a;

    move-result-object p1

    goto/16 :goto_0

    :cond_b
    const-string v2, "epk"

    .line 1212
    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_c

    .line 1213
    invoke-static {p0, v1}, Lcom/a/a/d/i;->g(La/b/b/d;Ljava/lang/String;)La/b/b/d;

    move-result-object v1

    invoke-static {v1}, Lcom/a/a/c/d;->b(La/b/b/d;)Lcom/a/a/c/d;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/a/a/m$a;->b(Lcom/a/a/c/d;)Lcom/a/a/m$a;

    move-result-object p1

    goto/16 :goto_0

    :cond_c
    const-string v2, "zip"

    .line 1214
    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_d

    .line 1215
    new-instance v2, Lcom/a/a/c;

    invoke-static {p0, v1}, Lcom/a/a/d/i;->b(La/b/b/d;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Lcom/a/a/c;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1, v2}, Lcom/a/a/m$a;->a(Lcom/a/a/c;)Lcom/a/a/m$a;

    move-result-object p1

    goto/16 :goto_0

    :cond_d
    const-string v2, "apu"

    .line 1216
    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_e

    .line 1217
    new-instance v2, Lcom/a/a/d/c;

    invoke-static {p0, v1}, Lcom/a/a/d/i;->b(La/b/b/d;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Lcom/a/a/d/c;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1, v2}, Lcom/a/a/m$a;->c(Lcom/a/a/d/c;)Lcom/a/a/m$a;

    move-result-object p1

    goto/16 :goto_0

    :cond_e
    const-string v2, "apv"

    .line 1218
    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_f

    .line 1219
    new-instance v2, Lcom/a/a/d/c;

    invoke-static {p0, v1}, Lcom/a/a/d/i;->b(La/b/b/d;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Lcom/a/a/d/c;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1, v2}, Lcom/a/a/m$a;->d(Lcom/a/a/d/c;)Lcom/a/a/m$a;

    move-result-object p1

    goto/16 :goto_0

    :cond_f
    const-string v2, "p2s"

    .line 1220
    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_10

    .line 1221
    new-instance v2, Lcom/a/a/d/c;

    invoke-static {p0, v1}, Lcom/a/a/d/i;->b(La/b/b/d;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Lcom/a/a/d/c;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1, v2}, Lcom/a/a/m$a;->e(Lcom/a/a/d/c;)Lcom/a/a/m$a;

    move-result-object p1

    goto/16 :goto_0

    :cond_10
    const-string v2, "p2c"

    .line 1222
    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_11

    .line 1223
    invoke-static {p0, v1}, Lcom/a/a/d/i;->a(La/b/b/d;Ljava/lang/String;)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/a/a/m$a;->a(I)Lcom/a/a/m$a;

    move-result-object p1

    goto/16 :goto_0

    :cond_11
    const-string v2, "iv"

    .line 1224
    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_12

    .line 1225
    new-instance v2, Lcom/a/a/d/c;

    invoke-static {p0, v1}, Lcom/a/a/d/i;->b(La/b/b/d;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Lcom/a/a/d/c;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1, v2}, Lcom/a/a/m$a;->f(Lcom/a/a/d/c;)Lcom/a/a/m$a;

    move-result-object p1

    goto/16 :goto_0

    :cond_12
    const-string v2, "tag"

    .line 1226
    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_13

    .line 1227
    new-instance v2, Lcom/a/a/d/c;

    invoke-static {p0, v1}, Lcom/a/a/d/i;->b(La/b/b/d;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Lcom/a/a/d/c;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1, v2}, Lcom/a/a/m$a;->g(Lcom/a/a/d/c;)Lcom/a/a/m$a;

    move-result-object p1

    goto/16 :goto_0

    .line 1229
    :cond_13
    invoke-virtual {p0, v1}, La/b/b/d;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, Lcom/a/a/m$a;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/a/a/m$a;

    move-result-object p1

    goto/16 :goto_0

    .line 1233
    :cond_14
    invoke-virtual {p1}, Lcom/a/a/m$a;->a()Lcom/a/a/m;

    move-result-object p0

    return-object p0

    .line 1177
    :cond_15
    new-instance p0, Ljava/text/ParseException;

    const/4 p1, 0x0

    const-string v0, "The algorithm \"alg\" header parameter must be for encryption"

    invoke-direct {p0, v0, p1}, Ljava/text/ParseException;-><init>(Ljava/lang/String;I)V

    throw p0
.end method

.method public static a(Lcom/a/a/d/c;)Lcom/a/a/m;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/text/ParseException;
        }
    .end annotation

    .line 1288
    invoke-virtual {p0}, Lcom/a/a/d/c;->d()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p0}, Lcom/a/a/m;->a(Ljava/lang/String;Lcom/a/a/d/c;)Lcom/a/a/m;

    move-result-object p0

    return-object p0
.end method

.method public static a(Ljava/lang/String;Lcom/a/a/d/c;)Lcom/a/a/m;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/text/ParseException;
        }
    .end annotation

    .line 1271
    invoke-static {p0}, Lcom/a/a/d/i;->a(Ljava/lang/String;)La/b/b/d;

    move-result-object p0

    invoke-static {p0, p1}, Lcom/a/a/m;->a(La/b/b/d;Lcom/a/a/d/c;)Lcom/a/a/m;

    move-result-object p0

    return-object p0
.end method

.method private static b(La/b/b/d;)Lcom/a/a/d;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/text/ParseException;
        }
    .end annotation

    const-string v0, "enc"

    .line 1134
    invoke-static {p0, v0}, Lcom/a/a/d/i;->b(La/b/b/d;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    invoke-static {p0}, Lcom/a/a/d;->a(Ljava/lang/String;)Lcom/a/a/d;

    move-result-object p0

    return-object p0
.end method

.method public static f()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 912
    sget-object v0, Lcom/a/a/m;->a:Ljava/util/Set;

    return-object v0
.end method


# virtual methods
.method public bridge synthetic a()Ljava/lang/String;
    .locals 1

    .line 80
    invoke-super {p0}, Lcom/a/a/b;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public b()La/b/b/d;
    .locals 3

    .line 1078
    invoke-super {p0}, Lcom/a/a/b;->b()La/b/b/d;

    move-result-object v0

    .line 1080
    iget-object v1, p0, Lcom/a/a/m;->b:Lcom/a/a/d;

    if-eqz v1, :cond_0

    .line 1081
    invoke-virtual {v1}, Lcom/a/a/d;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "enc"

    invoke-virtual {v0, v2, v1}, La/b/b/d;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1084
    :cond_0
    iget-object v1, p0, Lcom/a/a/m;->c:Lcom/a/a/c/d;

    if-eqz v1, :cond_1

    .line 1085
    invoke-virtual {v1}, Lcom/a/a/c/d;->e()La/b/b/d;

    move-result-object v1

    const-string v2, "epk"

    invoke-virtual {v0, v2, v1}, La/b/b/d;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1088
    :cond_1
    iget-object v1, p0, Lcom/a/a/m;->d:Lcom/a/a/c;

    if-eqz v1, :cond_2

    .line 1089
    invoke-virtual {v1}, Lcom/a/a/c;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "zip"

    invoke-virtual {v0, v2, v1}, La/b/b/d;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1092
    :cond_2
    iget-object v1, p0, Lcom/a/a/m;->e:Lcom/a/a/d/c;

    if-eqz v1, :cond_3

    .line 1093
    invoke-virtual {v1}, Lcom/a/a/d/c;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "apu"

    invoke-virtual {v0, v2, v1}, La/b/b/d;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1096
    :cond_3
    iget-object v1, p0, Lcom/a/a/m;->f:Lcom/a/a/d/c;

    if-eqz v1, :cond_4

    .line 1097
    invoke-virtual {v1}, Lcom/a/a/d/c;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "apv"

    invoke-virtual {v0, v2, v1}, La/b/b/d;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1100
    :cond_4
    iget-object v1, p0, Lcom/a/a/m;->g:Lcom/a/a/d/c;

    if-eqz v1, :cond_5

    .line 1101
    invoke-virtual {v1}, Lcom/a/a/d/c;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "p2s"

    invoke-virtual {v0, v2, v1}, La/b/b/d;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1104
    :cond_5
    iget v1, p0, Lcom/a/a/m;->h:I

    if-lez v1, :cond_6

    .line 1105
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "p2c"

    invoke-virtual {v0, v2, v1}, La/b/b/d;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1108
    :cond_6
    iget-object v1, p0, Lcom/a/a/m;->i:Lcom/a/a/d/c;

    if-eqz v1, :cond_7

    .line 1109
    invoke-virtual {v1}, Lcom/a/a/d/c;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "iv"

    invoke-virtual {v0, v2, v1}, La/b/b/d;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1112
    :cond_7
    iget-object v1, p0, Lcom/a/a/m;->j:Lcom/a/a/d/c;

    if-eqz v1, :cond_8

    .line 1113
    invoke-virtual {v1}, Lcom/a/a/d/c;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "tag"

    invoke-virtual {v0, v2, v1}, La/b/b/d;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_8
    return-object v0
.end method

.method public synthetic c()Lcom/a/a/a;
    .locals 1

    .line 80
    invoke-virtual {p0}, Lcom/a/a/m;->g()Lcom/a/a/i;

    move-result-object v0

    return-object v0
.end method

.method public g()Lcom/a/a/i;
    .locals 1

    .line 923
    invoke-super {p0}, Lcom/a/a/b;->c()Lcom/a/a/a;

    move-result-object v0

    check-cast v0, Lcom/a/a/i;

    return-object v0
.end method

.method public h()Lcom/a/a/d;
    .locals 1

    .line 934
    iget-object v0, p0, Lcom/a/a/m;->b:Lcom/a/a/d;

    return-object v0
.end method

.method public i()Lcom/a/a/c;
    .locals 1

    .line 958
    iget-object v0, p0, Lcom/a/a/m;->d:Lcom/a/a/c;

    return-object v0
.end method
