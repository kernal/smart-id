.class public final Lcom/a/a/q;
.super Lcom/a/a/b;
.source "JWSHeader.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/a/a/q$a;
    }
.end annotation


# static fields
.field private static final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 86
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    const-string v1, "alg"

    .line 88
    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    const-string v1, "jku"

    .line 89
    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    const-string v1, "jwk"

    .line 90
    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    const-string v1, "x5u"

    .line 91
    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    const-string v1, "x5t"

    .line 92
    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    const-string v1, "x5t#S256"

    .line 93
    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    const-string v1, "x5c"

    .line 94
    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    const-string v1, "kid"

    .line 95
    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    const-string v1, "typ"

    .line 96
    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    const-string v1, "cty"

    .line 97
    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    const-string v1, "crit"

    .line 98
    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 100
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    sput-object v0, Lcom/a/a/q;->a:Ljava/util/Set;

    return-void
.end method

.method public constructor <init>(Lcom/a/a/p;Lcom/a/a/h;Ljava/lang/String;Ljava/util/Set;Ljava/net/URI;Lcom/a/a/c/d;Ljava/net/URI;Lcom/a/a/d/c;Lcom/a/a/d/c;Ljava/util/List;Ljava/lang/String;Ljava/util/Map;Lcom/a/a/d/c;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/a/a/p;",
            "Lcom/a/a/h;",
            "Ljava/lang/String;",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/net/URI;",
            "Lcom/a/a/c/d;",
            "Ljava/net/URI;",
            "Lcom/a/a/d/c;",
            "Lcom/a/a/d/c;",
            "Ljava/util/List<",
            "Lcom/a/a/d/a;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;",
            "Lcom/a/a/d/c;",
            ")V"
        }
    .end annotation

    .line 539
    invoke-direct/range {p0 .. p13}, Lcom/a/a/b;-><init>(Lcom/a/a/a;Lcom/a/a/h;Ljava/lang/String;Ljava/util/Set;Ljava/net/URI;Lcom/a/a/c/d;Ljava/net/URI;Lcom/a/a/d/c;Lcom/a/a/d/c;Ljava/util/List;Ljava/lang/String;Ljava/util/Map;Lcom/a/a/d/c;)V

    .line 541
    invoke-virtual {p1}, Lcom/a/a/p;->a()Ljava/lang/String;

    move-result-object p1

    sget-object p2, Lcom/a/a/a;->a:Lcom/a/a/a;

    invoke-virtual {p2}, Lcom/a/a/a;->a()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_0

    return-void

    .line 542
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "The JWS algorithm \"alg\" cannot be \"none\""

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public static a(La/b/b/d;Lcom/a/a/d/c;)Lcom/a/a/q;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/text/ParseException;
        }
    .end annotation

    .line 631
    invoke-static {p0}, Lcom/a/a/e;->a(La/b/b/d;)Lcom/a/a/a;

    move-result-object v0

    .line 633
    instance-of v1, v0, Lcom/a/a/p;

    if-eqz v1, :cond_c

    .line 637
    new-instance v1, Lcom/a/a/q$a;

    check-cast v0, Lcom/a/a/p;

    invoke-direct {v1, v0}, Lcom/a/a/q$a;-><init>(Lcom/a/a/p;)V

    invoke-virtual {v1, p1}, Lcom/a/a/q$a;->c(Lcom/a/a/d/c;)Lcom/a/a/q$a;

    move-result-object p1

    .line 640
    invoke-virtual {p0}, La/b/b/d;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_b

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    const-string v2, "alg"

    .line 642
    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    goto :goto_0

    :cond_0
    const-string v2, "typ"

    .line 644
    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 645
    new-instance v2, Lcom/a/a/h;

    invoke-static {p0, v1}, Lcom/a/a/d/i;->b(La/b/b/d;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Lcom/a/a/h;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1, v2}, Lcom/a/a/q$a;->a(Lcom/a/a/h;)Lcom/a/a/q$a;

    move-result-object p1

    goto :goto_0

    :cond_1
    const-string v2, "cty"

    .line 646
    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 647
    invoke-static {p0, v1}, Lcom/a/a/d/i;->b(La/b/b/d;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/a/a/q$a;->a(Ljava/lang/String;)Lcom/a/a/q$a;

    move-result-object p1

    goto :goto_0

    :cond_2
    const-string v2, "crit"

    .line 648
    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 649
    new-instance v2, Ljava/util/HashSet;

    invoke-static {p0, v1}, Lcom/a/a/d/i;->f(La/b/b/d;Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v2, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    invoke-virtual {p1, v2}, Lcom/a/a/q$a;->a(Ljava/util/Set;)Lcom/a/a/q$a;

    move-result-object p1

    goto :goto_0

    :cond_3
    const-string v2, "jku"

    .line 650
    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 651
    invoke-static {p0, v1}, Lcom/a/a/d/i;->c(La/b/b/d;Ljava/lang/String;)Ljava/net/URI;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/a/a/q$a;->a(Ljava/net/URI;)Lcom/a/a/q$a;

    move-result-object p1

    goto :goto_0

    :cond_4
    const-string v2, "jwk"

    .line 652
    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 653
    invoke-static {p0, v1}, Lcom/a/a/d/i;->g(La/b/b/d;Ljava/lang/String;)La/b/b/d;

    move-result-object v1

    invoke-static {v1}, Lcom/a/a/c/d;->b(La/b/b/d;)Lcom/a/a/c/d;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/a/a/q$a;->a(Lcom/a/a/c/d;)Lcom/a/a/q$a;

    move-result-object p1

    goto :goto_0

    :cond_5
    const-string v2, "x5u"

    .line 654
    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 655
    invoke-static {p0, v1}, Lcom/a/a/d/i;->c(La/b/b/d;Ljava/lang/String;)Ljava/net/URI;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/a/a/q$a;->b(Ljava/net/URI;)Lcom/a/a/q$a;

    move-result-object p1

    goto/16 :goto_0

    :cond_6
    const-string v2, "x5t"

    .line 656
    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 657
    new-instance v2, Lcom/a/a/d/c;

    invoke-static {p0, v1}, Lcom/a/a/d/i;->b(La/b/b/d;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Lcom/a/a/d/c;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1, v2}, Lcom/a/a/q$a;->a(Lcom/a/a/d/c;)Lcom/a/a/q$a;

    move-result-object p1

    goto/16 :goto_0

    :cond_7
    const-string v2, "x5t#S256"

    .line 658
    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 659
    new-instance v2, Lcom/a/a/d/c;

    invoke-static {p0, v1}, Lcom/a/a/d/i;->b(La/b/b/d;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Lcom/a/a/d/c;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1, v2}, Lcom/a/a/q$a;->b(Lcom/a/a/d/c;)Lcom/a/a/q$a;

    move-result-object p1

    goto/16 :goto_0

    :cond_8
    const-string v2, "x5c"

    .line 660
    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 661
    invoke-static {p0, v1}, Lcom/a/a/d/i;->d(La/b/b/d;Ljava/lang/String;)La/b/b/a;

    move-result-object v1

    invoke-static {v1}, Lcom/a/a/d/l;->a(La/b/b/a;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/a/a/q$a;->a(Ljava/util/List;)Lcom/a/a/q$a;

    move-result-object p1

    goto/16 :goto_0

    :cond_9
    const-string v2, "kid"

    .line 662
    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_a

    .line 663
    invoke-static {p0, v1}, Lcom/a/a/d/i;->b(La/b/b/d;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/a/a/q$a;->b(Ljava/lang/String;)Lcom/a/a/q$a;

    move-result-object p1

    goto/16 :goto_0

    .line 665
    :cond_a
    invoke-virtual {p0, v1}, La/b/b/d;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, Lcom/a/a/q$a;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/a/a/q$a;

    move-result-object p1

    goto/16 :goto_0

    .line 669
    :cond_b
    invoke-virtual {p1}, Lcom/a/a/q$a;->a()Lcom/a/a/q;

    move-result-object p0

    return-object p0

    .line 634
    :cond_c
    new-instance p0, Ljava/text/ParseException;

    const/4 p1, 0x0

    const-string v0, "The algorithm \"alg\" header parameter must be for signatures"

    invoke-direct {p0, v0, p1}, Ljava/text/ParseException;-><init>(Ljava/lang/String;I)V

    throw p0
.end method

.method public static a(Lcom/a/a/d/c;)Lcom/a/a/q;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/text/ParseException;
        }
    .end annotation

    .line 725
    invoke-virtual {p0}, Lcom/a/a/d/c;->d()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p0}, Lcom/a/a/q;->a(Ljava/lang/String;Lcom/a/a/d/c;)Lcom/a/a/q;

    move-result-object p0

    return-object p0
.end method

.method public static a(Ljava/lang/String;Lcom/a/a/d/c;)Lcom/a/a/q;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/text/ParseException;
        }
    .end annotation

    .line 708
    invoke-static {p0}, Lcom/a/a/d/i;->a(Ljava/lang/String;)La/b/b/d;

    move-result-object p0

    invoke-static {p0, p1}, Lcom/a/a/q;->a(La/b/b/d;Lcom/a/a/d/c;)Lcom/a/a/q;

    move-result-object p0

    return-object p0
.end method

.method public static f()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 579
    sget-object v0, Lcom/a/a/q;->a:Ljava/util/Set;

    return-object v0
.end method


# virtual methods
.method public bridge synthetic a()Ljava/lang/String;
    .locals 1

    .line 69
    invoke-super {p0}, Lcom/a/a/b;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic b()La/b/b/d;
    .locals 1

    .line 69
    invoke-super {p0}, Lcom/a/a/b;->b()La/b/b/d;

    move-result-object v0

    return-object v0
.end method

.method public synthetic c()Lcom/a/a/a;
    .locals 1

    .line 69
    invoke-virtual {p0}, Lcom/a/a/q;->g()Lcom/a/a/p;

    move-result-object v0

    return-object v0
.end method

.method public g()Lcom/a/a/p;
    .locals 1

    .line 591
    invoke-super {p0}, Lcom/a/a/b;->c()Lcom/a/a/a;

    move-result-object v0

    check-cast v0, Lcom/a/a/p;

    return-object v0
.end method
