.class public final Lcom/a/a/p;
.super Lcom/a/a/a;
.source "JWSAlgorithm.java"


# static fields
.field public static final b:Lcom/a/a/p;

.field public static final c:Lcom/a/a/p;

.field public static final d:Lcom/a/a/p;

.field public static final e:Lcom/a/a/p;

.field public static final f:Lcom/a/a/p;

.field public static final g:Lcom/a/a/p;

.field public static final h:Lcom/a/a/p;

.field public static final i:Lcom/a/a/p;

.field public static final j:Lcom/a/a/p;

.field public static final k:Lcom/a/a/p;

.field public static final l:Lcom/a/a/p;

.field public static final m:Lcom/a/a/p;

.field public static final n:Lcom/a/a/p;

.field public static final o:Lcom/a/a/p;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 65
    new-instance v0, Lcom/a/a/p;

    sget-object v1, Lcom/a/a/w;->a:Lcom/a/a/w;

    const-string v2, "HS256"

    invoke-direct {v0, v2, v1}, Lcom/a/a/p;-><init>(Ljava/lang/String;Lcom/a/a/w;)V

    sput-object v0, Lcom/a/a/p;->b:Lcom/a/a/p;

    .line 71
    new-instance v0, Lcom/a/a/p;

    sget-object v1, Lcom/a/a/w;->c:Lcom/a/a/w;

    const-string v2, "HS384"

    invoke-direct {v0, v2, v1}, Lcom/a/a/p;-><init>(Ljava/lang/String;Lcom/a/a/w;)V

    sput-object v0, Lcom/a/a/p;->c:Lcom/a/a/p;

    .line 77
    new-instance v0, Lcom/a/a/p;

    sget-object v1, Lcom/a/a/w;->c:Lcom/a/a/w;

    const-string v2, "HS512"

    invoke-direct {v0, v2, v1}, Lcom/a/a/p;-><init>(Ljava/lang/String;Lcom/a/a/w;)V

    sput-object v0, Lcom/a/a/p;->d:Lcom/a/a/p;

    .line 83
    new-instance v0, Lcom/a/a/p;

    sget-object v1, Lcom/a/a/w;->b:Lcom/a/a/w;

    const-string v2, "RS256"

    invoke-direct {v0, v2, v1}, Lcom/a/a/p;-><init>(Ljava/lang/String;Lcom/a/a/w;)V

    sput-object v0, Lcom/a/a/p;->e:Lcom/a/a/p;

    .line 89
    new-instance v0, Lcom/a/a/p;

    sget-object v1, Lcom/a/a/w;->c:Lcom/a/a/w;

    const-string v2, "RS384"

    invoke-direct {v0, v2, v1}, Lcom/a/a/p;-><init>(Ljava/lang/String;Lcom/a/a/w;)V

    sput-object v0, Lcom/a/a/p;->f:Lcom/a/a/p;

    .line 95
    new-instance v0, Lcom/a/a/p;

    sget-object v1, Lcom/a/a/w;->c:Lcom/a/a/w;

    const-string v2, "RS512"

    invoke-direct {v0, v2, v1}, Lcom/a/a/p;-><init>(Ljava/lang/String;Lcom/a/a/w;)V

    sput-object v0, Lcom/a/a/p;->g:Lcom/a/a/p;

    .line 102
    new-instance v0, Lcom/a/a/p;

    sget-object v1, Lcom/a/a/w;->b:Lcom/a/a/w;

    const-string v2, "ES256"

    invoke-direct {v0, v2, v1}, Lcom/a/a/p;-><init>(Ljava/lang/String;Lcom/a/a/w;)V

    sput-object v0, Lcom/a/a/p;->h:Lcom/a/a/p;

    .line 109
    new-instance v0, Lcom/a/a/p;

    sget-object v1, Lcom/a/a/w;->c:Lcom/a/a/w;

    const-string v2, "ES256K"

    invoke-direct {v0, v2, v1}, Lcom/a/a/p;-><init>(Ljava/lang/String;Lcom/a/a/w;)V

    sput-object v0, Lcom/a/a/p;->i:Lcom/a/a/p;

    .line 115
    new-instance v0, Lcom/a/a/p;

    sget-object v1, Lcom/a/a/w;->c:Lcom/a/a/w;

    const-string v2, "ES384"

    invoke-direct {v0, v2, v1}, Lcom/a/a/p;-><init>(Ljava/lang/String;Lcom/a/a/w;)V

    sput-object v0, Lcom/a/a/p;->j:Lcom/a/a/p;

    .line 121
    new-instance v0, Lcom/a/a/p;

    sget-object v1, Lcom/a/a/w;->c:Lcom/a/a/w;

    const-string v2, "ES512"

    invoke-direct {v0, v2, v1}, Lcom/a/a/p;-><init>(Ljava/lang/String;Lcom/a/a/w;)V

    sput-object v0, Lcom/a/a/p;->k:Lcom/a/a/p;

    .line 128
    new-instance v0, Lcom/a/a/p;

    sget-object v1, Lcom/a/a/w;->c:Lcom/a/a/w;

    const-string v2, "PS256"

    invoke-direct {v0, v2, v1}, Lcom/a/a/p;-><init>(Ljava/lang/String;Lcom/a/a/w;)V

    sput-object v0, Lcom/a/a/p;->l:Lcom/a/a/p;

    .line 135
    new-instance v0, Lcom/a/a/p;

    sget-object v1, Lcom/a/a/w;->c:Lcom/a/a/w;

    const-string v2, "PS384"

    invoke-direct {v0, v2, v1}, Lcom/a/a/p;-><init>(Ljava/lang/String;Lcom/a/a/w;)V

    sput-object v0, Lcom/a/a/p;->m:Lcom/a/a/p;

    .line 142
    new-instance v0, Lcom/a/a/p;

    sget-object v1, Lcom/a/a/w;->c:Lcom/a/a/w;

    const-string v2, "PS512"

    invoke-direct {v0, v2, v1}, Lcom/a/a/p;-><init>(Ljava/lang/String;Lcom/a/a/w;)V

    sput-object v0, Lcom/a/a/p;->n:Lcom/a/a/p;

    .line 148
    new-instance v0, Lcom/a/a/p;

    sget-object v1, Lcom/a/a/w;->c:Lcom/a/a/w;

    const-string v2, "EdDSA"

    invoke-direct {v0, v2, v1}, Lcom/a/a/p;-><init>(Ljava/lang/String;Lcom/a/a/w;)V

    sput-object v0, Lcom/a/a/p;->o:Lcom/a/a/p;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    const/4 v0, 0x0

    .line 229
    invoke-direct {p0, p1, v0}, Lcom/a/a/a;-><init>(Ljava/lang/String;Lcom/a/a/w;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/a/a/w;)V
    .locals 0

    .line 218
    invoke-direct {p0, p1, p2}, Lcom/a/a/a;-><init>(Ljava/lang/String;Lcom/a/a/w;)V

    return-void
.end method

.method public static a(Ljava/lang/String;)Lcom/a/a/p;
    .locals 1

    .line 243
    sget-object v0, Lcom/a/a/p;->b:Lcom/a/a/p;

    invoke-virtual {v0}, Lcom/a/a/p;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 244
    sget-object p0, Lcom/a/a/p;->b:Lcom/a/a/p;

    return-object p0

    .line 245
    :cond_0
    sget-object v0, Lcom/a/a/p;->c:Lcom/a/a/p;

    invoke-virtual {v0}, Lcom/a/a/p;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 246
    sget-object p0, Lcom/a/a/p;->c:Lcom/a/a/p;

    return-object p0

    .line 247
    :cond_1
    sget-object v0, Lcom/a/a/p;->d:Lcom/a/a/p;

    invoke-virtual {v0}, Lcom/a/a/p;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 248
    sget-object p0, Lcom/a/a/p;->d:Lcom/a/a/p;

    return-object p0

    .line 249
    :cond_2
    sget-object v0, Lcom/a/a/p;->e:Lcom/a/a/p;

    invoke-virtual {v0}, Lcom/a/a/p;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 250
    sget-object p0, Lcom/a/a/p;->e:Lcom/a/a/p;

    return-object p0

    .line 251
    :cond_3
    sget-object v0, Lcom/a/a/p;->f:Lcom/a/a/p;

    invoke-virtual {v0}, Lcom/a/a/p;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 252
    sget-object p0, Lcom/a/a/p;->f:Lcom/a/a/p;

    return-object p0

    .line 253
    :cond_4
    sget-object v0, Lcom/a/a/p;->g:Lcom/a/a/p;

    invoke-virtual {v0}, Lcom/a/a/p;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 254
    sget-object p0, Lcom/a/a/p;->g:Lcom/a/a/p;

    return-object p0

    .line 255
    :cond_5
    sget-object v0, Lcom/a/a/p;->h:Lcom/a/a/p;

    invoke-virtual {v0}, Lcom/a/a/p;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 256
    sget-object p0, Lcom/a/a/p;->h:Lcom/a/a/p;

    return-object p0

    .line 257
    :cond_6
    sget-object v0, Lcom/a/a/p;->i:Lcom/a/a/p;

    invoke-virtual {v0}, Lcom/a/a/p;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 258
    sget-object p0, Lcom/a/a/p;->i:Lcom/a/a/p;

    return-object p0

    .line 259
    :cond_7
    sget-object v0, Lcom/a/a/p;->j:Lcom/a/a/p;

    invoke-virtual {v0}, Lcom/a/a/p;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 260
    sget-object p0, Lcom/a/a/p;->j:Lcom/a/a/p;

    return-object p0

    .line 261
    :cond_8
    sget-object v0, Lcom/a/a/p;->k:Lcom/a/a/p;

    invoke-virtual {v0}, Lcom/a/a/p;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 262
    sget-object p0, Lcom/a/a/p;->k:Lcom/a/a/p;

    return-object p0

    .line 263
    :cond_9
    sget-object v0, Lcom/a/a/p;->l:Lcom/a/a/p;

    invoke-virtual {v0}, Lcom/a/a/p;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 264
    sget-object p0, Lcom/a/a/p;->l:Lcom/a/a/p;

    return-object p0

    .line 265
    :cond_a
    sget-object v0, Lcom/a/a/p;->m:Lcom/a/a/p;

    invoke-virtual {v0}, Lcom/a/a/p;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 266
    sget-object p0, Lcom/a/a/p;->m:Lcom/a/a/p;

    return-object p0

    .line 267
    :cond_b
    sget-object v0, Lcom/a/a/p;->n:Lcom/a/a/p;

    invoke-virtual {v0}, Lcom/a/a/p;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 268
    sget-object p0, Lcom/a/a/p;->n:Lcom/a/a/p;

    return-object p0

    .line 269
    :cond_c
    sget-object v0, Lcom/a/a/p;->o:Lcom/a/a/p;

    invoke-virtual {v0}, Lcom/a/a/p;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 270
    sget-object p0, Lcom/a/a/p;->o:Lcom/a/a/p;

    return-object p0

    .line 272
    :cond_d
    new-instance v0, Lcom/a/a/p;

    invoke-direct {v0, p0}, Lcom/a/a/p;-><init>(Ljava/lang/String;)V

    return-object v0
.end method
