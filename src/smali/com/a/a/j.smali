.class public final Lcom/a/a/j;
.super Ljava/lang/Object;
.source "JWECryptoParts.java"


# instance fields
.field private final a:Lcom/a/a/m;

.field private final b:Lcom/a/a/d/c;

.field private final c:Lcom/a/a/d/c;

.field private final d:Lcom/a/a/d/c;

.field private final e:Lcom/a/a/d/c;


# direct methods
.method public constructor <init>(Lcom/a/a/m;Lcom/a/a/d/c;Lcom/a/a/d/c;Lcom/a/a/d/c;Lcom/a/a/d/c;)V
    .locals 0

    .line 110
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 112
    iput-object p1, p0, Lcom/a/a/j;->a:Lcom/a/a/m;

    .line 114
    iput-object p2, p0, Lcom/a/a/j;->b:Lcom/a/a/d/c;

    .line 116
    iput-object p3, p0, Lcom/a/a/j;->c:Lcom/a/a/d/c;

    if-eqz p4, :cond_0

    .line 123
    iput-object p4, p0, Lcom/a/a/j;->d:Lcom/a/a/d/c;

    .line 125
    iput-object p5, p0, Lcom/a/a/j;->e:Lcom/a/a/d/c;

    return-void

    .line 120
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "The cipher text must not be null"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method


# virtual methods
.method public a()Lcom/a/a/m;
    .locals 1

    .line 136
    iget-object v0, p0, Lcom/a/a/j;->a:Lcom/a/a/m;

    return-object v0
.end method

.method public b()Lcom/a/a/d/c;
    .locals 1

    .line 148
    iget-object v0, p0, Lcom/a/a/j;->b:Lcom/a/a/d/c;

    return-object v0
.end method

.method public c()Lcom/a/a/d/c;
    .locals 1

    .line 160
    iget-object v0, p0, Lcom/a/a/j;->c:Lcom/a/a/d/c;

    return-object v0
.end method

.method public d()Lcom/a/a/d/c;
    .locals 1

    .line 171
    iget-object v0, p0, Lcom/a/a/j;->d:Lcom/a/a/d/c;

    return-object v0
.end method

.method public e()Lcom/a/a/d/c;
    .locals 1

    .line 183
    iget-object v0, p0, Lcom/a/a/j;->e:Lcom/a/a/d/c;

    return-object v0
.end method
