.class public Lcom/a/a/r;
.super Lcom/a/a/g;
.source "JWSObject.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/a/a/r$a;
    }
.end annotation


# instance fields
.field private final a:Lcom/a/a/q;

.field private final b:Ljava/lang/String;

.field private c:Lcom/a/a/d/c;

.field private d:Lcom/a/a/r$a;


# direct methods
.method public constructor <init>(Lcom/a/a/d/c;Lcom/a/a/d/c;Lcom/a/a/d/c;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/text/ParseException;
        }
    .end annotation

    .line 143
    invoke-direct {p0}, Lcom/a/a/g;-><init>()V

    if-eqz p1, :cond_2

    const/4 v0, 0x0

    .line 151
    :try_start_0
    invoke-static {p1}, Lcom/a/a/q;->a(Lcom/a/a/d/c;)Lcom/a/a/q;

    move-result-object v1

    iput-object v1, p0, Lcom/a/a/r;->a:Lcom/a/a/q;
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz p2, :cond_1

    .line 163
    new-instance v1, Lcom/a/a/v;

    invoke-direct {v1, p2}, Lcom/a/a/v;-><init>(Lcom/a/a/d/c;)V

    invoke-virtual {p0, v1}, Lcom/a/a/r;->a(Lcom/a/a/v;)V

    .line 165
    invoke-static {p1, p2}, Lcom/a/a/r;->a(Lcom/a/a/d/c;Lcom/a/a/d/c;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/a/a/r;->b:Ljava/lang/String;

    if-eqz p3, :cond_0

    .line 171
    iput-object p3, p0, Lcom/a/a/r;->c:Lcom/a/a/d/c;

    .line 173
    sget-object v1, Lcom/a/a/r$a;->b:Lcom/a/a/r$a;

    iput-object v1, p0, Lcom/a/a/r;->d:Lcom/a/a/r$a;

    const/4 v1, 0x3

    .line 175
    new-array v1, v1, [Lcom/a/a/d/c;

    aput-object p1, v1, v0

    const/4 p1, 0x1

    aput-object p2, v1, p1

    const/4 p1, 0x2

    aput-object p3, v1, p1

    invoke-virtual {p0, v1}, Lcom/a/a/r;->a([Lcom/a/a/d/c;)V

    return-void

    .line 168
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "The third part must not be null"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 160
    :cond_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "The second part must not be null"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    :catch_0
    move-exception p1

    .line 155
    new-instance p2, Ljava/text/ParseException;

    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Invalid JWS header: "

    invoke-virtual {p3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/text/ParseException;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1, v0}, Ljava/text/ParseException;-><init>(Ljava/lang/String;I)V

    throw p2

    .line 147
    :cond_2
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "The first part must not be null"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method private static a(Lcom/a/a/d/c;Lcom/a/a/d/c;)Ljava/lang/String;
    .locals 1

    .line 204
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/a/a/d/c;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 p0, 0x2e

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/a/a/d/c;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static b(Ljava/lang/String;)Lcom/a/a/r;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/text/ParseException;
        }
    .end annotation

    .line 414
    invoke-static {p0}, Lcom/a/a/g;->a(Ljava/lang/String;)[Lcom/a/a/d/c;

    move-result-object p0

    .line 416
    array-length v0, p0

    const/4 v1, 0x0

    const/4 v2, 0x3

    if-ne v0, v2, :cond_0

    .line 421
    new-instance v0, Lcom/a/a/r;

    aget-object v1, p0, v1

    const/4 v2, 0x1

    aget-object v2, p0, v2

    const/4 v3, 0x2

    aget-object p0, p0, v3

    invoke-direct {v0, v1, v2, p0}, Lcom/a/a/r;-><init>(Lcom/a/a/d/c;Lcom/a/a/d/c;Lcom/a/a/d/c;)V

    return-object v0

    .line 418
    :cond_0
    new-instance p0, Ljava/text/ParseException;

    const-string v0, "Unexpected number of Base64URL parts, must be three"

    invoke-direct {p0, v0, v1}, Ljava/text/ParseException;-><init>(Ljava/lang/String;I)V

    throw p0
.end method

.method private h()V
    .locals 2

    .line 271
    iget-object v0, p0, Lcom/a/a/r;->d:Lcom/a/a/r$a;

    sget-object v1, Lcom/a/a/r$a;->b:Lcom/a/a/r$a;

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lcom/a/a/r;->d:Lcom/a/a/r$a;

    sget-object v1, Lcom/a/a/r$a;->c:Lcom/a/a/r$a;

    if-ne v0, v1, :cond_0

    goto :goto_0

    .line 273
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "The JWS object must be in a signed or verified state"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    :goto_0
    return-void
.end method


# virtual methods
.method public declared-synchronized a(Lcom/a/a/s;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/a/a/f;
        }
    .end annotation

    monitor-enter p0

    .line 348
    :try_start_0
    invoke-direct {p0}, Lcom/a/a/r;->h()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 353
    :try_start_1
    invoke-virtual {p0}, Lcom/a/a/r;->c()Lcom/a/a/q;

    move-result-object v0

    invoke-virtual {p0}, Lcom/a/a/r;->d()[B

    move-result-object v1

    invoke-virtual {p0}, Lcom/a/a/r;->e()Lcom/a/a/d/c;

    move-result-object v2

    invoke-interface {p1, v0, v1, v2}, Lcom/a/a/s;->a(Lcom/a/a/q;[BLcom/a/a/d/c;)Z

    move-result p1
    :try_end_1
    .catch Lcom/a/a/f; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz p1, :cond_0

    .line 368
    :try_start_2
    sget-object v0, Lcom/a/a/r$a;->c:Lcom/a/a/r$a;

    iput-object v0, p0, Lcom/a/a/r;->d:Lcom/a/a/r$a;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 371
    :cond_0
    monitor-exit p0

    return p1

    :catch_0
    move-exception p1

    .line 363
    :try_start_3
    new-instance v0, Lcom/a/a/f;

    invoke-virtual {p1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lcom/a/a/f;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0

    :catch_1
    move-exception p1

    .line 357
    throw p1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public c()Lcom/a/a/q;
    .locals 1

    .line 182
    iget-object v0, p0, Lcom/a/a/r;->a:Lcom/a/a/q;

    return-object v0
.end method

.method public d()[B
    .locals 2

    .line 221
    iget-object v0, p0, Lcom/a/a/r;->b:Ljava/lang/String;

    sget-object v1, Lcom/a/a/d/k;->a:Ljava/nio/charset/Charset;

    invoke-virtual {v0, v1}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v0

    return-object v0
.end method

.method public e()Lcom/a/a/d/c;
    .locals 1

    .line 233
    iget-object v0, p0, Lcom/a/a/r;->c:Lcom/a/a/d/c;

    return-object v0
.end method

.method public f()Lcom/a/a/r$a;
    .locals 1

    .line 244
    iget-object v0, p0, Lcom/a/a/r;->d:Lcom/a/a/r$a;

    return-object v0
.end method

.method public g()Ljava/lang/String;
    .locals 2

    .line 394
    invoke-direct {p0}, Lcom/a/a/r;->h()V

    .line 396
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/a/a/r;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x2e

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/a/a/r;->c:Lcom/a/a/d/c;

    invoke-virtual {v1}, Lcom/a/a/d/c;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
