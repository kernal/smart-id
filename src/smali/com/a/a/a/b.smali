.class public Lcom/a/a/a/b;
.super Lcom/a/a/a/a/n;
.source "DirectEncrypter.java"

# interfaces
.implements Lcom/a/a/l;


# direct methods
.method public constructor <init>(Ljavax/crypto/SecretKey;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/a/a/u;
        }
    .end annotation

    .line 85
    invoke-direct {p0, p1}, Lcom/a/a/a/a/n;-><init>(Ljavax/crypto/SecretKey;)V

    return-void
.end method

.method public constructor <init>([B)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/a/a/u;
        }
    .end annotation

    .line 103
    new-instance v0, Ljavax/crypto/spec/SecretKeySpec;

    const-string v1, "AES"

    invoke-direct {v0, p1, v1}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/a/a/a/b;-><init>(Ljavax/crypto/SecretKey;)V

    return-void
.end method


# virtual methods
.method public a(Lcom/a/a/m;[B)Lcom/a/a/j;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/a/a/f;
        }
    .end annotation

    .line 129
    invoke-virtual {p1}, Lcom/a/a/m;->g()Lcom/a/a/i;

    move-result-object v0

    .line 131
    sget-object v1, Lcom/a/a/i;->h:Lcom/a/a/i;

    invoke-virtual {v0, v1}, Lcom/a/a/i;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 136
    invoke-virtual {p1}, Lcom/a/a/m;->h()Lcom/a/a/d;

    move-result-object v0

    .line 138
    invoke-virtual {v0}, Lcom/a/a/d;->c()I

    move-result v1

    invoke-virtual {p0}, Lcom/a/a/a/b;->d()Ljavax/crypto/SecretKey;

    move-result-object v2

    invoke-interface {v2}, Ljavax/crypto/SecretKey;->getEncoded()[B

    move-result-object v2

    invoke-static {v2}, Lcom/a/a/d/d;->b([B)I

    move-result v2

    if-ne v1, v2, :cond_0

    const/4 v0, 0x0

    .line 144
    invoke-virtual {p0}, Lcom/a/a/a/b;->d()Ljavax/crypto/SecretKey;

    move-result-object v1

    invoke-virtual {p0}, Lcom/a/a/a/b;->c()Lcom/a/a/b/b;

    move-result-object v2

    invoke-static {p1, p2, v1, v0, v2}, Lcom/a/a/a/a/k;->a(Lcom/a/a/m;[BLjavax/crypto/SecretKey;Lcom/a/a/d/c;Lcom/a/a/b/b;)Lcom/a/a/j;

    move-result-object p1

    return-object p1

    .line 139
    :cond_0
    new-instance p1, Lcom/a/a/u;

    invoke-virtual {v0}, Lcom/a/a/d;->c()I

    move-result p2

    invoke-direct {p1, p2, v0}, Lcom/a/a/u;-><init>(ILcom/a/a/a;)V

    throw p1

    .line 132
    :cond_1
    new-instance p1, Lcom/a/a/f;

    sget-object p2, Lcom/a/a/a/b;->a:Ljava/util/Set;

    invoke-static {v0, p2}, Lcom/a/a/a/a/e;->a(Lcom/a/a/i;Ljava/util/Collection;)Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Lcom/a/a/f;-><init>(Ljava/lang/String;)V

    throw p1
.end method
