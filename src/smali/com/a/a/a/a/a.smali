.class public Lcom/a/a/a/a/a;
.super Ljava/lang/Object;
.source "AAD.java"


# direct methods
.method public static a(Lcom/a/a/d/c;)[B
    .locals 1

    .line 66
    invoke-virtual {p0}, Lcom/a/a/d/c;->toString()Ljava/lang/String;

    move-result-object p0

    const-string v0, "ASCII"

    invoke-static {v0}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object p0

    return-object p0
.end method

.method public static a(Lcom/a/a/m;)[B
    .locals 0

    .line 51
    invoke-virtual {p0}, Lcom/a/a/m;->e()Lcom/a/a/d/c;

    move-result-object p0

    invoke-static {p0}, Lcom/a/a/a/a/a;->a(Lcom/a/a/d/c;)[B

    move-result-object p0

    return-object p0
.end method

.method public static a([B)[B
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/a/a/d/g;
        }
    .end annotation

    .line 85
    invoke-static {p0}, Lcom/a/a/d/d;->b([B)I

    move-result p0

    const/16 v0, 0x8

    .line 86
    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    int-to-long v1, p0

    invoke-virtual {v0, v1, v2}, Ljava/nio/ByteBuffer;->putLong(J)Ljava/nio/ByteBuffer;

    move-result-object p0

    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object p0

    return-object p0
.end method
