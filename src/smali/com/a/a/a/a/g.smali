.class abstract Lcom/a/a/a/a/g;
.super Ljava/lang/Object;
.source "BaseJWEProvider.java"

# interfaces
.implements Lcom/a/a/o;


# instance fields
.field private final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Lcom/a/a/i;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Lcom/a/a/d;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lcom/a/a/b/b;


# direct methods
.method public constructor <init>(Ljava/util/Set;Ljava/util/Set;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "Lcom/a/a/i;",
            ">;",
            "Ljava/util/Set<",
            "Lcom/a/a/d;",
            ">;)V"
        }
    .end annotation

    .line 67
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    new-instance v0, Lcom/a/a/b/b;

    invoke-direct {v0}, Lcom/a/a/b/b;-><init>()V

    iput-object v0, p0, Lcom/a/a/a/a/g;->c:Lcom/a/a/b/b;

    if-eqz p1, :cond_1

    .line 73
    invoke-static {p1}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object p1

    iput-object p1, p0, Lcom/a/a/a/a/g;->a:Ljava/util/Set;

    if-eqz p2, :cond_0

    .line 80
    iput-object p2, p0, Lcom/a/a/a/a/g;->b:Ljava/util/Set;

    return-void

    .line 77
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "The supported encryption methods must not be null"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 70
    :cond_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "The supported JWE algorithm set must not be null"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method


# virtual methods
.method public a()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Lcom/a/a/i;",
            ">;"
        }
    .end annotation

    .line 87
    iget-object v0, p0, Lcom/a/a/a/a/g;->a:Ljava/util/Set;

    return-object v0
.end method

.method public b()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Lcom/a/a/d;",
            ">;"
        }
    .end annotation

    .line 94
    iget-object v0, p0, Lcom/a/a/a/a/g;->b:Ljava/util/Set;

    return-object v0
.end method

.method public c()Lcom/a/a/b/b;
    .locals 1

    .line 101
    iget-object v0, p0, Lcom/a/a/a/a/g;->c:Lcom/a/a/b/b;

    return-object v0
.end method
