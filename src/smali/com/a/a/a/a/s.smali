.class public abstract Lcom/a/a/a/a/s;
.super Lcom/a/a/a/a/g;
.source "RSACryptoProvider.java"


# static fields
.field public static final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Lcom/a/a/i;",
            ">;"
        }
    .end annotation
.end field

.field public static final b:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Lcom/a/a/d;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 70
    sget-object v0, Lcom/a/a/a/a/k;->a:Ljava/util/Set;

    sput-object v0, Lcom/a/a/a/a/s;->b:Ljava/util/Set;

    .line 74
    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0}, Ljava/util/LinkedHashSet;-><init>()V

    .line 75
    sget-object v1, Lcom/a/a/i;->b:Lcom/a/a/i;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 76
    sget-object v1, Lcom/a/a/i;->c:Lcom/a/a/i;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 77
    sget-object v1, Lcom/a/a/i;->d:Lcom/a/a/i;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 78
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    sput-object v0, Lcom/a/a/a/a/s;->a:Ljava/util/Set;

    return-void
.end method

.method protected constructor <init>()V
    .locals 2

    .line 87
    sget-object v0, Lcom/a/a/a/a/s;->a:Ljava/util/Set;

    sget-object v1, Lcom/a/a/a/a/k;->a:Ljava/util/Set;

    invoke-direct {p0, v0, v1}, Lcom/a/a/a/a/g;-><init>(Ljava/util/Set;Ljava/util/Set;)V

    return-void
.end method


# virtual methods
.method public bridge synthetic a()Ljava/util/Set;
    .locals 1

    .line 58
    invoke-super {p0}, Lcom/a/a/a/a/g;->a()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic b()Ljava/util/Set;
    .locals 1

    .line 58
    invoke-super {p0}, Lcom/a/a/a/a/g;->b()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic c()Lcom/a/a/b/b;
    .locals 1

    .line 58
    invoke-super {p0}, Lcom/a/a/a/a/g;->c()Lcom/a/a/b/b;

    move-result-object v0

    return-object v0
.end method
