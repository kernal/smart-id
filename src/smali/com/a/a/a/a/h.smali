.class public abstract Lcom/a/a/a/a/h;
.super Ljava/lang/Object;
.source "BaseJWSProvider.java"


# instance fields
.field private final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Lcom/a/a/p;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Lcom/a/a/b/a;


# direct methods
.method public constructor <init>(Ljava/util/Set;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "Lcom/a/a/p;",
            ">;)V"
        }
    .end annotation

    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    new-instance v0, Lcom/a/a/b/a;

    invoke-direct {v0}, Lcom/a/a/b/a;-><init>()V

    iput-object v0, p0, Lcom/a/a/a/a/h;->b:Lcom/a/a/b/a;

    if-eqz p1, :cond_0

    .line 62
    invoke-static {p1}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object p1

    iput-object p1, p0, Lcom/a/a/a/a/h;->a:Ljava/util/Set;

    return-void

    .line 59
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "The supported JWS algorithm set must not be null"

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method


# virtual methods
.method public a()Lcom/a/a/b/a;
    .locals 1

    .line 76
    iget-object v0, p0, Lcom/a/a/a/a/h;->b:Lcom/a/a/b/a;

    return-object v0
.end method
