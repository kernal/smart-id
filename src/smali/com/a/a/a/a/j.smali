.class public final Lcom/a/a/a/a/j;
.super Ljava/lang/Object;
.source "CompositeKey.java"


# instance fields
.field private final a:Ljavax/crypto/SecretKey;

.field private final b:Ljavax/crypto/SecretKey;

.field private final c:Ljavax/crypto/SecretKey;

.field private final d:I


# direct methods
.method public constructor <init>(Ljavax/crypto/SecretKey;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/a/a/u;
        }
    .end annotation

    .line 76
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 78
    iput-object p1, p0, Lcom/a/a/a/a/j;->a:Ljavax/crypto/SecretKey;

    .line 80
    invoke-interface {p1}, Ljavax/crypto/SecretKey;->getEncoded()[B

    move-result-object p1

    .line 82
    array-length v0, p1

    const-string v1, "AES"

    const/4 v2, 0x0

    const/16 v3, 0x20

    if-ne v0, v3, :cond_0

    .line 86
    new-instance v0, Ljavax/crypto/spec/SecretKeySpec;

    const/16 v3, 0x10

    const-string v4, "HMACSHA256"

    invoke-direct {v0, p1, v2, v3, v4}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BIILjava/lang/String;)V

    iput-object v0, p0, Lcom/a/a/a/a/j;->b:Ljavax/crypto/SecretKey;

    .line 87
    new-instance v0, Ljavax/crypto/spec/SecretKeySpec;

    invoke-direct {v0, p1, v3, v3, v1}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BIILjava/lang/String;)V

    iput-object v0, p0, Lcom/a/a/a/a/j;->c:Ljavax/crypto/SecretKey;

    .line 88
    iput v3, p0, Lcom/a/a/a/a/j;->d:I

    goto :goto_0

    .line 90
    :cond_0
    array-length v0, p1

    const/16 v4, 0x30

    if-ne v0, v4, :cond_1

    .line 94
    new-instance v0, Ljavax/crypto/spec/SecretKeySpec;

    const/16 v3, 0x18

    const-string v4, "HMACSHA384"

    invoke-direct {v0, p1, v2, v3, v4}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BIILjava/lang/String;)V

    iput-object v0, p0, Lcom/a/a/a/a/j;->b:Ljavax/crypto/SecretKey;

    .line 95
    new-instance v0, Ljavax/crypto/spec/SecretKeySpec;

    invoke-direct {v0, p1, v3, v3, v1}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BIILjava/lang/String;)V

    iput-object v0, p0, Lcom/a/a/a/a/j;->c:Ljavax/crypto/SecretKey;

    .line 96
    iput v3, p0, Lcom/a/a/a/a/j;->d:I

    goto :goto_0

    .line 99
    :cond_1
    array-length v0, p1

    const/16 v4, 0x40

    if-ne v0, v4, :cond_2

    .line 103
    new-instance v0, Ljavax/crypto/spec/SecretKeySpec;

    const-string v4, "HMACSHA512"

    invoke-direct {v0, p1, v2, v3, v4}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BIILjava/lang/String;)V

    iput-object v0, p0, Lcom/a/a/a/a/j;->b:Ljavax/crypto/SecretKey;

    .line 104
    new-instance v0, Ljavax/crypto/spec/SecretKeySpec;

    invoke-direct {v0, p1, v3, v3, v1}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BIILjava/lang/String;)V

    iput-object v0, p0, Lcom/a/a/a/a/j;->c:Ljavax/crypto/SecretKey;

    .line 105
    iput v3, p0, Lcom/a/a/a/a/j;->d:I

    :goto_0
    return-void

    .line 109
    :cond_2
    new-instance p1, Lcom/a/a/u;

    const-string v0, "Unsupported AES/CBC/PKCS5Padding/HMAC-SHA2 key length, must be 256, 384 or 512 bits"

    invoke-direct {p1, v0}, Lcom/a/a/u;-><init>(Ljava/lang/String;)V

    throw p1
.end method


# virtual methods
.method public a()Ljavax/crypto/SecretKey;
    .locals 1

    .line 132
    iget-object v0, p0, Lcom/a/a/a/a/j;->b:Ljavax/crypto/SecretKey;

    return-object v0
.end method

.method public b()I
    .locals 1

    .line 143
    iget v0, p0, Lcom/a/a/a/a/j;->d:I

    return v0
.end method

.method public c()Ljavax/crypto/SecretKey;
    .locals 1

    .line 154
    iget-object v0, p0, Lcom/a/a/a/a/j;->c:Ljavax/crypto/SecretKey;

    return-object v0
.end method
