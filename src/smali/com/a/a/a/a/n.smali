.class public abstract Lcom/a/a/a/a/n;
.super Lcom/a/a/a/a/g;
.source "DirectCryptoProvider.java"


# static fields
.field public static final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Lcom/a/a/i;",
            ">;"
        }
    .end annotation
.end field

.field public static final b:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Lcom/a/a/d;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final c:Ljavax/crypto/SecretKey;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 71
    sget-object v0, Lcom/a/a/a/a/k;->a:Ljava/util/Set;

    sput-object v0, Lcom/a/a/a/a/n;->b:Ljava/util/Set;

    .line 75
    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0}, Ljava/util/LinkedHashSet;-><init>()V

    .line 76
    sget-object v1, Lcom/a/a/i;->h:Lcom/a/a/i;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 77
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    sput-object v0, Lcom/a/a/a/a/n;->a:Ljava/util/Set;

    return-void
.end method

.method protected constructor <init>(Ljavax/crypto/SecretKey;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/a/a/u;
        }
    .end annotation

    .line 123
    sget-object v0, Lcom/a/a/a/a/n;->a:Ljava/util/Set;

    invoke-interface {p1}, Ljavax/crypto/SecretKey;->getEncoded()[B

    move-result-object v1

    invoke-static {v1}, Lcom/a/a/d/d;->a([B)I

    move-result v1

    invoke-static {v1}, Lcom/a/a/a/a/n;->a(I)Ljava/util/Set;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/a/a/a/a/g;-><init>(Ljava/util/Set;Ljava/util/Set;)V

    .line 125
    iput-object p1, p0, Lcom/a/a/a/a/n;->c:Ljavax/crypto/SecretKey;

    return-void
.end method

.method private static a(I)Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/Set<",
            "Lcom/a/a/d;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/a/a/u;
        }
    .end annotation

    .line 94
    sget-object v0, Lcom/a/a/a/a/k;->b:Ljava/util/Map;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/util/Set;

    if-eqz p0, :cond_0

    return-object p0

    .line 97
    :cond_0
    new-instance p0, Lcom/a/a/u;

    const-string v0, "The Content Encryption Key length must be 128 bits (16 bytes), 192 bits (24 bytes), 256 bits (32 bytes), 384 bits (48 bytes) or 512 bites (64 bytes)"

    invoke-direct {p0, v0}, Lcom/a/a/u;-><init>(Ljava/lang/String;)V

    throw p0
.end method


# virtual methods
.method public bridge synthetic a()Ljava/util/Set;
    .locals 1

    .line 58
    invoke-super {p0}, Lcom/a/a/a/a/g;->a()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic b()Ljava/util/Set;
    .locals 1

    .line 58
    invoke-super {p0}, Lcom/a/a/a/a/g;->b()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic c()Lcom/a/a/b/b;
    .locals 1

    .line 58
    invoke-super {p0}, Lcom/a/a/a/a/g;->c()Lcom/a/a/b/b;

    move-result-object v0

    return-object v0
.end method

.method public d()Ljavax/crypto/SecretKey;
    .locals 1

    .line 136
    iget-object v0, p0, Lcom/a/a/a/a/n;->c:Ljavax/crypto/SecretKey;

    return-object v0
.end method
