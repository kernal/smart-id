.class public Lcom/a/a/a/a/k;
.super Ljava/lang/Object;
.source "ContentCryptoProvider.java"


# static fields
.field public static final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Lcom/a/a/d;",
            ">;"
        }
    .end annotation
.end field

.field public static final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Integer;",
            "Ljava/util/Set<",
            "Lcom/a/a/d;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .line 56
    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0}, Ljava/util/LinkedHashSet;-><init>()V

    .line 57
    sget-object v1, Lcom/a/a/d;->b:Lcom/a/a/d;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 58
    sget-object v1, Lcom/a/a/d;->c:Lcom/a/a/d;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 59
    sget-object v1, Lcom/a/a/d;->d:Lcom/a/a/d;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 60
    sget-object v1, Lcom/a/a/d;->g:Lcom/a/a/d;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 61
    sget-object v1, Lcom/a/a/d;->h:Lcom/a/a/d;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 62
    sget-object v1, Lcom/a/a/d;->i:Lcom/a/a/d;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 63
    sget-object v1, Lcom/a/a/d;->e:Lcom/a/a/d;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 64
    sget-object v1, Lcom/a/a/d;->f:Lcom/a/a/d;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 65
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    sput-object v0, Lcom/a/a/a/a/k;->a:Ljava/util/Set;

    .line 67
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 68
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 69
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    .line 70
    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    .line 71
    new-instance v4, Ljava/util/HashSet;

    invoke-direct {v4}, Ljava/util/HashSet;-><init>()V

    .line 72
    new-instance v5, Ljava/util/HashSet;

    invoke-direct {v5}, Ljava/util/HashSet;-><init>()V

    .line 73
    sget-object v6, Lcom/a/a/d;->g:Lcom/a/a/d;

    invoke-interface {v1, v6}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 74
    sget-object v6, Lcom/a/a/d;->h:Lcom/a/a/d;

    invoke-interface {v2, v6}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 75
    sget-object v6, Lcom/a/a/d;->i:Lcom/a/a/d;

    invoke-interface {v3, v6}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 76
    sget-object v6, Lcom/a/a/d;->b:Lcom/a/a/d;

    invoke-interface {v3, v6}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 77
    sget-object v6, Lcom/a/a/d;->e:Lcom/a/a/d;

    invoke-interface {v3, v6}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 78
    sget-object v6, Lcom/a/a/d;->c:Lcom/a/a/d;

    invoke-interface {v4, v6}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 79
    sget-object v6, Lcom/a/a/d;->d:Lcom/a/a/d;

    invoke-interface {v5, v6}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 80
    sget-object v6, Lcom/a/a/d;->f:Lcom/a/a/d;

    invoke-interface {v5, v6}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    const/16 v6, 0x80

    .line 81
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v1

    invoke-interface {v0, v6, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/16 v1, 0xc0

    .line 82
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/16 v1, 0x100

    .line 83
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v3}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/16 v1, 0x180

    .line 84
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/16 v1, 0x200

    .line 85
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v5}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 86
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    sput-object v0, Lcom/a/a/a/a/k;->b:Ljava/util/Map;

    return-void
.end method

.method public static a(Lcom/a/a/m;[BLjavax/crypto/SecretKey;Lcom/a/a/d/c;Lcom/a/a/b/b;)Lcom/a/a/j;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/a/a/f;
        }
    .end annotation

    .line 163
    invoke-virtual {p0}, Lcom/a/a/m;->h()Lcom/a/a/d;

    move-result-object v0

    invoke-static {p2, v0}, Lcom/a/a/a/a/k;->a(Ljavax/crypto/SecretKey;Lcom/a/a/d;)V

    .line 166
    invoke-static {p0, p1}, Lcom/a/a/a/a/m;->a(Lcom/a/a/m;[B)[B

    move-result-object p1

    .line 169
    invoke-static {p0}, Lcom/a/a/a/a/a;->a(Lcom/a/a/m;)[B

    move-result-object v4

    .line 175
    invoke-virtual {p0}, Lcom/a/a/m;->h()Lcom/a/a/d;

    move-result-object v0

    sget-object v1, Lcom/a/a/d;->b:Lcom/a/a/d;

    invoke-virtual {v0, v1}, Lcom/a/a/d;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 176
    invoke-virtual {p0}, Lcom/a/a/m;->h()Lcom/a/a/d;

    move-result-object v0

    sget-object v1, Lcom/a/a/d;->c:Lcom/a/a/d;

    invoke-virtual {v0, v1}, Lcom/a/a/d;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 177
    invoke-virtual {p0}, Lcom/a/a/m;->h()Lcom/a/a/d;

    move-result-object v0

    sget-object v1, Lcom/a/a/d;->d:Lcom/a/a/d;

    invoke-virtual {v0, v1}, Lcom/a/a/d;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto/16 :goto_2

    .line 186
    :cond_0
    invoke-virtual {p0}, Lcom/a/a/m;->h()Lcom/a/a/d;

    move-result-object v0

    sget-object v1, Lcom/a/a/d;->g:Lcom/a/a/d;

    invoke-virtual {v0, v1}, Lcom/a/a/d;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 187
    invoke-virtual {p0}, Lcom/a/a/m;->h()Lcom/a/a/d;

    move-result-object v0

    sget-object v1, Lcom/a/a/d;->h:Lcom/a/a/d;

    invoke-virtual {v0, v1}, Lcom/a/a/d;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 188
    invoke-virtual {p0}, Lcom/a/a/m;->h()Lcom/a/a/d;

    move-result-object v0

    sget-object v1, Lcom/a/a/d;->i:Lcom/a/a/d;

    invoke-virtual {v0, v1}, Lcom/a/a/d;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    goto :goto_1

    .line 198
    :cond_1
    invoke-virtual {p0}, Lcom/a/a/m;->h()Lcom/a/a/d;

    move-result-object v0

    sget-object v1, Lcom/a/a/d;->e:Lcom/a/a/d;

    invoke-virtual {v0, v1}, Lcom/a/a/d;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 199
    invoke-virtual {p0}, Lcom/a/a/m;->h()Lcom/a/a/d;

    move-result-object v0

    sget-object v1, Lcom/a/a/d;->f:Lcom/a/a/d;

    invoke-virtual {v0, v1}, Lcom/a/a/d;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    goto :goto_0

    .line 210
    :cond_2
    new-instance p1, Lcom/a/a/f;

    .line 211
    invoke-virtual {p0}, Lcom/a/a/m;->h()Lcom/a/a/d;

    move-result-object p0

    sget-object p2, Lcom/a/a/a/a/k;->a:Ljava/util/Set;

    .line 210
    invoke-static {p0, p2}, Lcom/a/a/a/a/e;->a(Lcom/a/a/d;Ljava/util/Collection;)Ljava/lang/String;

    move-result-object p0

    invoke-direct {p1, p0}, Lcom/a/a/f;-><init>(Ljava/lang/String;)V

    throw p1

    .line 201
    :cond_3
    :goto_0
    invoke-virtual {p4}, Lcom/a/a/b/b;->b()Ljava/security/SecureRandom;

    move-result-object v0

    invoke-static {v0}, Lcom/a/a/a/a/b;->a(Ljava/security/SecureRandom;)[B

    move-result-object v0

    .line 205
    invoke-virtual {p4}, Lcom/a/a/b/b;->d()Ljava/security/Provider;

    move-result-object v6

    .line 206
    invoke-virtual {p4}, Lcom/a/a/b/b;->e()Ljava/security/Provider;

    move-result-object v7

    move-object v1, p0

    move-object v2, p2

    move-object v3, p3

    move-object v4, v0

    move-object v5, p1

    .line 203
    invoke-static/range {v1 .. v7}, Lcom/a/a/a/a/b;->a(Lcom/a/a/m;Ljavax/crypto/SecretKey;Lcom/a/a/d/c;[B[BLjava/security/Provider;Ljava/security/Provider;)Lcom/a/a/a/a/f;

    move-result-object p1

    goto :goto_3

    .line 190
    :cond_4
    :goto_1
    new-instance v0, Lcom/a/a/d/e;

    invoke-virtual {p4}, Lcom/a/a/b/b;->b()Ljava/security/SecureRandom;

    move-result-object v1

    invoke-static {v1}, Lcom/a/a/a/a/c;->a(Ljava/security/SecureRandom;)[B

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/a/a/d/e;-><init>(Ljava/lang/Object;)V

    .line 194
    invoke-virtual {p4}, Lcom/a/a/b/b;->d()Ljava/security/Provider;

    move-result-object p4

    .line 192
    invoke-static {p2, v0, p1, v4, p4}, Lcom/a/a/a/a/c;->a(Ljavax/crypto/SecretKey;Lcom/a/a/d/e;[B[BLjava/security/Provider;)Lcom/a/a/a/a/f;

    move-result-object p1

    .line 196
    invoke-virtual {v0}, Lcom/a/a/d/e;->a()Ljava/lang/Object;

    move-result-object p2

    move-object v0, p2

    check-cast v0, [B

    goto :goto_3

    .line 179
    :cond_5
    :goto_2
    invoke-virtual {p4}, Lcom/a/a/b/b;->b()Ljava/security/SecureRandom;

    move-result-object v0

    invoke-static {v0}, Lcom/a/a/a/a/b;->a(Ljava/security/SecureRandom;)[B

    move-result-object v0

    .line 183
    invoke-virtual {p4}, Lcom/a/a/b/b;->d()Ljava/security/Provider;

    move-result-object v5

    .line 184
    invoke-virtual {p4}, Lcom/a/a/b/b;->e()Ljava/security/Provider;

    move-result-object v6

    move-object v1, p2

    move-object v2, v0

    move-object v3, p1

    .line 181
    invoke-static/range {v1 .. v6}, Lcom/a/a/a/a/b;->a(Ljavax/crypto/SecretKey;[B[B[BLjava/security/Provider;Ljava/security/Provider;)Lcom/a/a/a/a/f;

    move-result-object p1

    .line 215
    :goto_3
    new-instance p2, Lcom/a/a/j;

    .line 218
    invoke-static {v0}, Lcom/a/a/d/c;->a([B)Lcom/a/a/d/c;

    move-result-object v4

    .line 219
    invoke-virtual {p1}, Lcom/a/a/a/a/f;->a()[B

    move-result-object p4

    invoke-static {p4}, Lcom/a/a/d/c;->a([B)Lcom/a/a/d/c;

    move-result-object v5

    .line 220
    invoke-virtual {p1}, Lcom/a/a/a/a/f;->b()[B

    move-result-object p1

    invoke-static {p1}, Lcom/a/a/d/c;->a([B)Lcom/a/a/d/c;

    move-result-object v6

    move-object v1, p2

    move-object v2, p0

    move-object v3, p3

    invoke-direct/range {v1 .. v6}, Lcom/a/a/j;-><init>(Lcom/a/a/m;Lcom/a/a/d/c;Lcom/a/a/d/c;Lcom/a/a/d/c;Lcom/a/a/d/c;)V

    return-object p2
.end method

.method public static a(Lcom/a/a/d;Ljava/security/SecureRandom;)Ljavax/crypto/SecretKey;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/a/a/f;
        }
    .end annotation

    .line 105
    sget-object v0, Lcom/a/a/a/a/k;->a:Ljava/util/Set;

    invoke-interface {v0, p0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 109
    invoke-virtual {p0}, Lcom/a/a/d;->c()I

    move-result p0

    invoke-static {p0}, Lcom/a/a/d/d;->c(I)I

    move-result p0

    new-array p0, p0, [B

    .line 111
    invoke-virtual {p1, p0}, Ljava/security/SecureRandom;->nextBytes([B)V

    .line 113
    new-instance p1, Ljavax/crypto/spec/SecretKeySpec;

    const-string v0, "AES"

    invoke-direct {p1, p0, v0}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V

    return-object p1

    .line 106
    :cond_0
    new-instance p1, Lcom/a/a/f;

    sget-object v0, Lcom/a/a/a/a/k;->a:Ljava/util/Set;

    invoke-static {p0, v0}, Lcom/a/a/a/a/e;->a(Lcom/a/a/d;Ljava/util/Collection;)Ljava/lang/String;

    move-result-object p0

    invoke-direct {p1, p0}, Lcom/a/a/f;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method private static a(Ljavax/crypto/SecretKey;Lcom/a/a/d;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/a/a/u;
        }
    .end annotation

    .line 131
    :try_start_0
    invoke-virtual {p1}, Lcom/a/a/d;->c()I

    move-result v0

    invoke-interface {p0}, Ljavax/crypto/SecretKey;->getEncoded()[B

    move-result-object p0

    invoke-static {p0}, Lcom/a/a/d/d;->b([B)I

    move-result p0

    if-ne v0, p0, :cond_0

    return-void

    .line 132
    :cond_0
    new-instance p0, Lcom/a/a/u;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "The Content Encryption Key (CEK) length for "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, " must be "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/a/a/d;->c()I

    move-result p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, " bits"

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/a/a/u;-><init>(Ljava/lang/String;)V

    throw p0
    :try_end_0
    .catch Lcom/a/a/d/g; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception p0

    .line 135
    new-instance p1, Lcom/a/a/u;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "The Content Encryption Key (CEK) is too long: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/a/a/d/g;->getMessage()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {p1, p0}, Lcom/a/a/u;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public static a(Lcom/a/a/m;Lcom/a/a/d/c;Lcom/a/a/d/c;Lcom/a/a/d/c;Lcom/a/a/d/c;Ljavax/crypto/SecretKey;Lcom/a/a/b/b;)[B
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/a/a/f;
        }
    .end annotation

    .line 253
    invoke-virtual {p0}, Lcom/a/a/m;->h()Lcom/a/a/d;

    move-result-object v0

    invoke-static {p5, v0}, Lcom/a/a/a/a/k;->a(Ljavax/crypto/SecretKey;Lcom/a/a/d;)V

    .line 256
    invoke-static {p0}, Lcom/a/a/a/a/a;->a(Lcom/a/a/m;)[B

    move-result-object v4

    .line 262
    invoke-virtual {p0}, Lcom/a/a/m;->h()Lcom/a/a/d;

    move-result-object v0

    sget-object v1, Lcom/a/a/d;->b:Lcom/a/a/d;

    invoke-virtual {v0, v1}, Lcom/a/a/d;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 263
    invoke-virtual {p0}, Lcom/a/a/m;->h()Lcom/a/a/d;

    move-result-object v0

    sget-object v1, Lcom/a/a/d;->c:Lcom/a/a/d;

    invoke-virtual {v0, v1}, Lcom/a/a/d;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 264
    invoke-virtual {p0}, Lcom/a/a/m;->h()Lcom/a/a/d;

    move-result-object v0

    sget-object v1, Lcom/a/a/d;->d:Lcom/a/a/d;

    invoke-virtual {v0, v1}, Lcom/a/a/d;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto/16 :goto_2

    .line 275
    :cond_0
    invoke-virtual {p0}, Lcom/a/a/m;->h()Lcom/a/a/d;

    move-result-object v0

    sget-object v1, Lcom/a/a/d;->g:Lcom/a/a/d;

    invoke-virtual {v0, v1}, Lcom/a/a/d;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 276
    invoke-virtual {p0}, Lcom/a/a/m;->h()Lcom/a/a/d;

    move-result-object v0

    sget-object v1, Lcom/a/a/d;->h:Lcom/a/a/d;

    invoke-virtual {v0, v1}, Lcom/a/a/d;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 277
    invoke-virtual {p0}, Lcom/a/a/m;->h()Lcom/a/a/d;

    move-result-object v0

    sget-object v1, Lcom/a/a/d;->i:Lcom/a/a/d;

    invoke-virtual {v0, v1}, Lcom/a/a/d;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    goto :goto_1

    .line 287
    :cond_1
    invoke-virtual {p0}, Lcom/a/a/m;->h()Lcom/a/a/d;

    move-result-object v0

    sget-object v1, Lcom/a/a/d;->e:Lcom/a/a/d;

    invoke-virtual {v0, v1}, Lcom/a/a/d;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 288
    invoke-virtual {p0}, Lcom/a/a/m;->h()Lcom/a/a/d;

    move-result-object v0

    sget-object v1, Lcom/a/a/d;->f:Lcom/a/a/d;

    invoke-virtual {v0, v1}, Lcom/a/a/d;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    goto :goto_0

    .line 301
    :cond_2
    new-instance p1, Lcom/a/a/f;

    .line 302
    invoke-virtual {p0}, Lcom/a/a/m;->h()Lcom/a/a/d;

    move-result-object p0

    sget-object p2, Lcom/a/a/a/a/k;->a:Ljava/util/Set;

    .line 301
    invoke-static {p0, p2}, Lcom/a/a/a/a/e;->a(Lcom/a/a/d;Ljava/util/Collection;)Ljava/lang/String;

    move-result-object p0

    invoke-direct {p1, p0}, Lcom/a/a/f;-><init>(Ljava/lang/String;)V

    throw p1

    .line 297
    :cond_3
    :goto_0
    invoke-virtual {p6}, Lcom/a/a/b/b;->d()Ljava/security/Provider;

    move-result-object v6

    .line 298
    invoke-virtual {p6}, Lcom/a/a/b/b;->e()Ljava/security/Provider;

    move-result-object v7

    move-object v0, p0

    move-object v1, p5

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    .line 290
    invoke-static/range {v0 .. v7}, Lcom/a/a/a/a/b;->a(Lcom/a/a/m;Ljavax/crypto/SecretKey;Lcom/a/a/d/c;Lcom/a/a/d/c;Lcom/a/a/d/c;Lcom/a/a/d/c;Ljava/security/Provider;Ljava/security/Provider;)[B

    move-result-object p1

    goto :goto_3

    .line 281
    :cond_4
    :goto_1
    invoke-virtual {p2}, Lcom/a/a/d/c;->a()[B

    move-result-object p2

    .line 282
    invoke-virtual {p3}, Lcom/a/a/d/c;->a()[B

    move-result-object p3

    .line 284
    invoke-virtual {p4}, Lcom/a/a/d/c;->a()[B

    move-result-object v0

    .line 285
    invoke-virtual {p6}, Lcom/a/a/b/b;->d()Ljava/security/Provider;

    move-result-object p6

    move-object p1, p5

    move-object p4, v4

    move-object p5, v0

    .line 279
    invoke-static/range {p1 .. p6}, Lcom/a/a/a/a/c;->a(Ljavax/crypto/SecretKey;[B[B[B[BLjava/security/Provider;)[B

    move-result-object p1

    goto :goto_3

    .line 268
    :cond_5
    :goto_2
    invoke-virtual {p2}, Lcom/a/a/d/c;->a()[B

    move-result-object v2

    .line 269
    invoke-virtual {p3}, Lcom/a/a/d/c;->a()[B

    move-result-object v3

    .line 271
    invoke-virtual {p4}, Lcom/a/a/d/c;->a()[B

    move-result-object v5

    .line 272
    invoke-virtual {p6}, Lcom/a/a/b/b;->d()Ljava/security/Provider;

    move-result-object v6

    .line 273
    invoke-virtual {p6}, Lcom/a/a/b/b;->e()Ljava/security/Provider;

    move-result-object v7

    move-object v1, p5

    .line 266
    invoke-static/range {v1 .. v7}, Lcom/a/a/a/a/b;->a(Ljavax/crypto/SecretKey;[B[B[B[BLjava/security/Provider;Ljava/security/Provider;)[B

    move-result-object p1

    .line 308
    :goto_3
    invoke-static {p0, p1}, Lcom/a/a/a/a/m;->b(Lcom/a/a/m;[B)[B

    move-result-object p0

    return-object p0
.end method
