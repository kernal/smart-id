.class public Lcom/a/a/a/a/l;
.super Ljava/lang/Object;
.source "CriticalHeaderParamsDeferral.java"


# instance fields
.field private a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcom/a/a/a/a/l;->a:Ljava/util/Set;

    return-void
.end method


# virtual methods
.method public a(Lcom/a/a/m;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/a/a/f;
        }
    .end annotation

    .line 126
    invoke-virtual {p0, p1}, Lcom/a/a/a/a/l;->a(Lcom/a/a/e;)Z

    move-result p1

    if-eqz p1, :cond_0

    return-void

    .line 127
    :cond_0
    new-instance p1, Lcom/a/a/f;

    const-string v0, "Unsupported critical header parameter(s)"

    invoke-direct {p1, v0}, Lcom/a/a/f;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public a(Ljava/util/Set;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    if-nez p1, :cond_0

    .line 84
    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object p1

    iput-object p1, p0, Lcom/a/a/a/a/l;->a:Ljava/util/Set;

    goto :goto_0

    .line 86
    :cond_0
    iput-object p1, p0, Lcom/a/a/a/a/l;->a:Ljava/util/Set;

    :goto_0
    return-void
.end method

.method public a(Lcom/a/a/e;)Z
    .locals 2

    .line 104
    invoke-virtual {p1}, Lcom/a/a/e;->d()Ljava/util/Set;

    move-result-object p1

    const/4 v0, 0x1

    if-eqz p1, :cond_2

    .line 106
    invoke-interface {p1}, Ljava/util/Set;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    .line 111
    :cond_0
    iget-object v1, p0, Lcom/a/a/a/a/l;->a:Ljava/util/Set;

    if-eqz v1, :cond_1

    invoke-interface {v1, p1}, Ljava/util/Set;->containsAll(Ljava/util/Collection;)Z

    move-result p1

    if-eqz p1, :cond_1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :cond_2
    :goto_0
    return v0
.end method
