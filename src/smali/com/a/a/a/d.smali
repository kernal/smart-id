.class public Lcom/a/a/a/d;
.super Lcom/a/a/a/a/u;
.source "RSASSAVerifier.java"

# interfaces
.implements Lcom/a/a/s;


# instance fields
.field private final b:Lcom/a/a/a/a/l;

.field private final c:Ljava/security/interfaces/RSAPublicKey;


# direct methods
.method public constructor <init>(Ljava/security/interfaces/RSAPublicKey;)V
    .locals 1

    const/4 v0, 0x0

    .line 85
    invoke-direct {p0, p1, v0}, Lcom/a/a/a/d;-><init>(Ljava/security/interfaces/RSAPublicKey;Ljava/util/Set;)V

    return-void
.end method

.method public constructor <init>(Ljava/security/interfaces/RSAPublicKey;Ljava/util/Set;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/security/interfaces/RSAPublicKey;",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 112
    invoke-direct {p0}, Lcom/a/a/a/a/u;-><init>()V

    .line 69
    new-instance v0, Lcom/a/a/a/a/l;

    invoke-direct {v0}, Lcom/a/a/a/a/l;-><init>()V

    iput-object v0, p0, Lcom/a/a/a/d;->b:Lcom/a/a/a/a/l;

    if-eqz p1, :cond_0

    .line 118
    iput-object p1, p0, Lcom/a/a/a/d;->c:Ljava/security/interfaces/RSAPublicKey;

    .line 120
    iget-object p1, p0, Lcom/a/a/a/d;->b:Lcom/a/a/a/a/l;

    invoke-virtual {p1, p2}, Lcom/a/a/a/a/l;->a(Ljava/util/Set;)V

    return-void

    .line 115
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "The public RSA key must not be null"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method


# virtual methods
.method public a(Lcom/a/a/q;[BLcom/a/a/d/c;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/a/a/f;
        }
    .end annotation

    .line 155
    iget-object v0, p0, Lcom/a/a/a/d;->b:Lcom/a/a/a/a/l;

    invoke-virtual {v0, p1}, Lcom/a/a/a/a/l;->a(Lcom/a/a/e;)Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    .line 159
    :cond_0
    invoke-virtual {p1}, Lcom/a/a/q;->g()Lcom/a/a/p;

    move-result-object p1

    invoke-virtual {p0}, Lcom/a/a/a/d;->a()Lcom/a/a/b/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/a/a/b/a;->a()Ljava/security/Provider;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/a/a/a/a/t;->a(Lcom/a/a/p;Ljava/security/Provider;)Ljava/security/Signature;

    move-result-object p1

    .line 162
    :try_start_0
    iget-object v0, p0, Lcom/a/a/a/d;->c:Ljava/security/interfaces/RSAPublicKey;

    invoke-virtual {p1, v0}, Ljava/security/Signature;->initVerify(Ljava/security/PublicKey;)V
    :try_end_0
    .catch Ljava/security/InvalidKeyException; {:try_start_0 .. :try_end_0} :catch_1

    .line 169
    :try_start_1
    invoke-virtual {p1, p2}, Ljava/security/Signature;->update([B)V

    .line 170
    invoke-virtual {p3}, Lcom/a/a/d/c;->a()[B

    move-result-object p2

    invoke-virtual {p1, p2}, Ljava/security/Signature;->verify([B)Z

    move-result p1
    :try_end_1
    .catch Ljava/security/SignatureException; {:try_start_1 .. :try_end_1} :catch_0

    return p1

    :catch_0
    return v1

    :catch_1
    move-exception p1

    .line 165
    new-instance p2, Lcom/a/a/f;

    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "Invalid public RSA key: "

    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/security/InvalidKeyException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p3

    invoke-direct {p2, p3, p1}, Lcom/a/a/f;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw p2
.end method
