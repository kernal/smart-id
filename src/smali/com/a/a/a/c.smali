.class public Lcom/a/a/a/c;
.super Lcom/a/a/a/a/s;
.source "RSAEncrypter.java"

# interfaces
.implements Lcom/a/a/l;


# instance fields
.field private final c:Ljava/security/interfaces/RSAPublicKey;

.field private final d:Ljavax/crypto/SecretKey;


# direct methods
.method public constructor <init>(Ljava/security/interfaces/RSAPublicKey;)V
    .locals 1

    const/4 v0, 0x0

    .line 101
    invoke-direct {p0, p1, v0}, Lcom/a/a/a/c;-><init>(Ljava/security/interfaces/RSAPublicKey;Ljavax/crypto/SecretKey;)V

    return-void
.end method

.method public constructor <init>(Ljava/security/interfaces/RSAPublicKey;Ljavax/crypto/SecretKey;)V
    .locals 1

    .line 132
    invoke-direct {p0}, Lcom/a/a/a/a/s;-><init>()V

    if-eqz p1, :cond_2

    .line 137
    iput-object p1, p0, Lcom/a/a/a/c;->c:Ljava/security/interfaces/RSAPublicKey;

    if-eqz p2, :cond_1

    .line 140
    invoke-interface {p2}, Ljavax/crypto/SecretKey;->getAlgorithm()Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_0

    invoke-interface {p2}, Ljavax/crypto/SecretKey;->getAlgorithm()Ljava/lang/String;

    move-result-object p1

    const-string v0, "AES"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 143
    iput-object p2, p0, Lcom/a/a/a/c;->d:Ljavax/crypto/SecretKey;

    goto :goto_0

    .line 141
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "The algorithm of the content encryption key (CEK) must be AES"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_1
    const/4 p1, 0x0

    .line 146
    iput-object p1, p0, Lcom/a/a/a/c;->d:Ljavax/crypto/SecretKey;

    :goto_0
    return-void

    .line 135
    :cond_2
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "The public RSA key must not be null"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method


# virtual methods
.method public a(Lcom/a/a/m;[B)Lcom/a/a/j;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/a/a/f;
        }
    .end annotation

    .line 166
    invoke-virtual {p1}, Lcom/a/a/m;->g()Lcom/a/a/i;

    move-result-object v0

    .line 167
    invoke-virtual {p1}, Lcom/a/a/m;->h()Lcom/a/a/d;

    move-result-object v1

    .line 171
    iget-object v2, p0, Lcom/a/a/a/c;->d:Ljavax/crypto/SecretKey;

    if-eqz v2, :cond_0

    goto :goto_0

    .line 176
    :cond_0
    invoke-virtual {p0}, Lcom/a/a/a/c;->c()Lcom/a/a/b/b;

    move-result-object v2

    invoke-virtual {v2}, Lcom/a/a/b/b;->b()Ljava/security/SecureRandom;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/a/a/a/a/k;->a(Lcom/a/a/d;Ljava/security/SecureRandom;)Ljavax/crypto/SecretKey;

    move-result-object v2

    .line 181
    :goto_0
    sget-object v1, Lcom/a/a/i;->b:Lcom/a/a/i;

    invoke-virtual {v0, v1}, Lcom/a/a/i;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 183
    iget-object v0, p0, Lcom/a/a/a/c;->c:Ljava/security/interfaces/RSAPublicKey;

    invoke-virtual {p0}, Lcom/a/a/a/c;->c()Lcom/a/a/b/b;

    move-result-object v1

    invoke-virtual {v1}, Lcom/a/a/b/b;->c()Ljava/security/Provider;

    move-result-object v1

    invoke-static {v0, v2, v1}, Lcom/a/a/a/a/r;->a(Ljava/security/interfaces/RSAPublicKey;Ljavax/crypto/SecretKey;Ljava/security/Provider;)[B

    move-result-object v0

    invoke-static {v0}, Lcom/a/a/d/c;->a([B)Lcom/a/a/d/c;

    move-result-object v0

    goto :goto_1

    .line 185
    :cond_1
    sget-object v1, Lcom/a/a/i;->c:Lcom/a/a/i;

    invoke-virtual {v0, v1}, Lcom/a/a/i;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 187
    iget-object v0, p0, Lcom/a/a/a/c;->c:Ljava/security/interfaces/RSAPublicKey;

    invoke-virtual {p0}, Lcom/a/a/a/c;->c()Lcom/a/a/b/b;

    move-result-object v1

    invoke-virtual {v1}, Lcom/a/a/b/b;->c()Ljava/security/Provider;

    move-result-object v1

    invoke-static {v0, v2, v1}, Lcom/a/a/a/a/v;->a(Ljava/security/interfaces/RSAPublicKey;Ljavax/crypto/SecretKey;Ljava/security/Provider;)[B

    move-result-object v0

    invoke-static {v0}, Lcom/a/a/d/c;->a([B)Lcom/a/a/d/c;

    move-result-object v0

    goto :goto_1

    .line 189
    :cond_2
    sget-object v1, Lcom/a/a/i;->d:Lcom/a/a/i;

    invoke-virtual {v0, v1}, Lcom/a/a/i;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 191
    iget-object v0, p0, Lcom/a/a/a/c;->c:Ljava/security/interfaces/RSAPublicKey;

    invoke-virtual {p0}, Lcom/a/a/a/c;->c()Lcom/a/a/b/b;

    move-result-object v1

    invoke-virtual {v1}, Lcom/a/a/b/b;->c()Ljava/security/Provider;

    move-result-object v1

    invoke-static {v0, v2, v1}, Lcom/a/a/a/a/w;->a(Ljava/security/interfaces/RSAPublicKey;Ljavax/crypto/SecretKey;Ljava/security/Provider;)[B

    move-result-object v0

    invoke-static {v0}, Lcom/a/a/d/c;->a([B)Lcom/a/a/d/c;

    move-result-object v0

    .line 198
    :goto_1
    invoke-virtual {p0}, Lcom/a/a/a/c;->c()Lcom/a/a/b/b;

    move-result-object v1

    invoke-static {p1, p2, v2, v0, v1}, Lcom/a/a/a/a/k;->a(Lcom/a/a/m;[BLjavax/crypto/SecretKey;Lcom/a/a/d/c;Lcom/a/a/b/b;)Lcom/a/a/j;

    move-result-object p1

    return-object p1

    .line 195
    :cond_3
    new-instance p1, Lcom/a/a/f;

    sget-object p2, Lcom/a/a/a/c;->a:Ljava/util/Set;

    invoke-static {v0, p2}, Lcom/a/a/a/a/e;->a(Lcom/a/a/i;Ljava/util/Collection;)Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Lcom/a/a/f;-><init>(Ljava/lang/String;)V

    throw p1
.end method
