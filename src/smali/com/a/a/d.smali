.class public final Lcom/a/a/d;
.super Lcom/a/a/a;
.source "EncryptionMethod.java"


# static fields
.field public static final b:Lcom/a/a/d;

.field public static final c:Lcom/a/a/d;

.field public static final d:Lcom/a/a/d;

.field public static final e:Lcom/a/a/d;

.field public static final f:Lcom/a/a/d;

.field public static final g:Lcom/a/a/d;

.field public static final h:Lcom/a/a/d;

.field public static final i:Lcom/a/a/d;


# instance fields
.field private final j:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 63
    new-instance v0, Lcom/a/a/d;

    sget-object v1, Lcom/a/a/w;->a:Lcom/a/a/w;

    const/16 v2, 0x100

    const-string v3, "A128CBC-HS256"

    invoke-direct {v0, v3, v1, v2}, Lcom/a/a/d;-><init>(Ljava/lang/String;Lcom/a/a/w;I)V

    sput-object v0, Lcom/a/a/d;->b:Lcom/a/a/d;

    .line 71
    new-instance v0, Lcom/a/a/d;

    sget-object v1, Lcom/a/a/w;->c:Lcom/a/a/w;

    const-string v3, "A192CBC-HS384"

    const/16 v4, 0x180

    invoke-direct {v0, v3, v1, v4}, Lcom/a/a/d;-><init>(Ljava/lang/String;Lcom/a/a/w;I)V

    sput-object v0, Lcom/a/a/d;->c:Lcom/a/a/d;

    .line 79
    new-instance v0, Lcom/a/a/d;

    sget-object v1, Lcom/a/a/w;->a:Lcom/a/a/w;

    const/16 v3, 0x200

    const-string v4, "A256CBC-HS512"

    invoke-direct {v0, v4, v1, v3}, Lcom/a/a/d;-><init>(Ljava/lang/String;Lcom/a/a/w;I)V

    sput-object v0, Lcom/a/a/d;->d:Lcom/a/a/d;

    .line 87
    new-instance v0, Lcom/a/a/d;

    sget-object v1, Lcom/a/a/w;->c:Lcom/a/a/w;

    const-string v4, "A128CBC+HS256"

    invoke-direct {v0, v4, v1, v2}, Lcom/a/a/d;-><init>(Ljava/lang/String;Lcom/a/a/w;I)V

    sput-object v0, Lcom/a/a/d;->e:Lcom/a/a/d;

    .line 95
    new-instance v0, Lcom/a/a/d;

    sget-object v1, Lcom/a/a/w;->c:Lcom/a/a/w;

    const-string v4, "A256CBC+HS512"

    invoke-direct {v0, v4, v1, v3}, Lcom/a/a/d;-><init>(Ljava/lang/String;Lcom/a/a/w;I)V

    sput-object v0, Lcom/a/a/d;->f:Lcom/a/a/d;

    .line 103
    new-instance v0, Lcom/a/a/d;

    sget-object v1, Lcom/a/a/w;->b:Lcom/a/a/w;

    const-string v3, "A128GCM"

    const/16 v4, 0x80

    invoke-direct {v0, v3, v1, v4}, Lcom/a/a/d;-><init>(Ljava/lang/String;Lcom/a/a/w;I)V

    sput-object v0, Lcom/a/a/d;->g:Lcom/a/a/d;

    .line 111
    new-instance v0, Lcom/a/a/d;

    sget-object v1, Lcom/a/a/w;->c:Lcom/a/a/w;

    const-string v3, "A192GCM"

    const/16 v4, 0xc0

    invoke-direct {v0, v3, v1, v4}, Lcom/a/a/d;-><init>(Ljava/lang/String;Lcom/a/a/w;I)V

    sput-object v0, Lcom/a/a/d;->h:Lcom/a/a/d;

    .line 119
    new-instance v0, Lcom/a/a/d;

    sget-object v1, Lcom/a/a/w;->b:Lcom/a/a/w;

    const-string v3, "A256GCM"

    invoke-direct {v0, v3, v1, v2}, Lcom/a/a/d;-><init>(Ljava/lang/String;Lcom/a/a/w;I)V

    sput-object v0, Lcom/a/a/d;->i:Lcom/a/a/d;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 196
    invoke-direct {p0, p1, v0, v1}, Lcom/a/a/d;-><init>(Ljava/lang/String;Lcom/a/a/w;I)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/a/a/w;I)V
    .locals 0

    .line 168
    invoke-direct {p0, p1, p2}, Lcom/a/a/a;-><init>(Ljava/lang/String;Lcom/a/a/w;)V

    .line 170
    iput p3, p0, Lcom/a/a/d;->j:I

    return-void
.end method

.method public static a(Ljava/lang/String;)Lcom/a/a/d;
    .locals 1

    .line 222
    sget-object v0, Lcom/a/a/d;->b:Lcom/a/a/d;

    invoke-virtual {v0}, Lcom/a/a/d;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 224
    sget-object p0, Lcom/a/a/d;->b:Lcom/a/a/d;

    return-object p0

    .line 226
    :cond_0
    sget-object v0, Lcom/a/a/d;->c:Lcom/a/a/d;

    invoke-virtual {v0}, Lcom/a/a/d;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 228
    sget-object p0, Lcom/a/a/d;->c:Lcom/a/a/d;

    return-object p0

    .line 230
    :cond_1
    sget-object v0, Lcom/a/a/d;->d:Lcom/a/a/d;

    invoke-virtual {v0}, Lcom/a/a/d;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 232
    sget-object p0, Lcom/a/a/d;->d:Lcom/a/a/d;

    return-object p0

    .line 234
    :cond_2
    sget-object v0, Lcom/a/a/d;->g:Lcom/a/a/d;

    invoke-virtual {v0}, Lcom/a/a/d;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 236
    sget-object p0, Lcom/a/a/d;->g:Lcom/a/a/d;

    return-object p0

    .line 238
    :cond_3
    sget-object v0, Lcom/a/a/d;->h:Lcom/a/a/d;

    invoke-virtual {v0}, Lcom/a/a/d;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 240
    sget-object p0, Lcom/a/a/d;->h:Lcom/a/a/d;

    return-object p0

    .line 242
    :cond_4
    sget-object v0, Lcom/a/a/d;->i:Lcom/a/a/d;

    invoke-virtual {v0}, Lcom/a/a/d;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 244
    sget-object p0, Lcom/a/a/d;->i:Lcom/a/a/d;

    return-object p0

    .line 246
    :cond_5
    sget-object v0, Lcom/a/a/d;->e:Lcom/a/a/d;

    invoke-virtual {v0}, Lcom/a/a/d;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 248
    sget-object p0, Lcom/a/a/d;->e:Lcom/a/a/d;

    return-object p0

    .line 250
    :cond_6
    sget-object v0, Lcom/a/a/d;->f:Lcom/a/a/d;

    invoke-virtual {v0}, Lcom/a/a/d;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 252
    sget-object p0, Lcom/a/a/d;->f:Lcom/a/a/d;

    return-object p0

    .line 256
    :cond_7
    new-instance v0, Lcom/a/a/d;

    invoke-direct {v0, p0}, Lcom/a/a/d;-><init>(Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public c()I
    .locals 1

    .line 208
    iget v0, p0, Lcom/a/a/d;->j:I

    return v0
.end method
