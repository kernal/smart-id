.class public final Lcom/a/a/h;
.super Ljava/lang/Object;
.source "JOSEObjectType.java"

# interfaces
.implements La/b/b/b;
.implements Ljava/io/Serializable;


# static fields
.field public static final a:Lcom/a/a/h;

.field public static final b:Lcom/a/a/h;

.field public static final c:Lcom/a/a/h;


# instance fields
.field private final d:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 57
    new-instance v0, Lcom/a/a/h;

    const-string v1, "JOSE"

    invoke-direct {v0, v1}, Lcom/a/a/h;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/a/a/h;->a:Lcom/a/a/h;

    .line 63
    new-instance v0, Lcom/a/a/h;

    const-string v1, "JOSE+JSON"

    invoke-direct {v0, v1}, Lcom/a/a/h;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/a/a/h;->b:Lcom/a/a/h;

    .line 69
    new-instance v0, Lcom/a/a/h;

    const-string v1, "JWT"

    invoke-direct {v0, v1}, Lcom/a/a/h;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/a/a/h;->c:Lcom/a/a/h;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    .line 83
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    if-eqz p1, :cond_0

    .line 89
    iput-object p1, p0, Lcom/a/a/h;->d:Ljava/lang/String;

    return-void

    .line 86
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "The object type must not be null"

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method


# virtual methods
.method public b()Ljava/lang/String;
    .locals 2

    .line 155
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/a/a/h;->d:Ljava/lang/String;

    invoke-static {v1}, La/b/b/d;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x22

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1

    if-eqz p1, :cond_0

    .line 127
    instance-of v0, p1, Lcom/a/a/h;

    if-eqz v0, :cond_0

    .line 129
    invoke-virtual {p0}, Lcom/a/a/h;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public hashCode()I
    .locals 1

    .line 112
    iget-object v0, p0, Lcom/a/a/h;->d:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .line 143
    iget-object v0, p0, Lcom/a/a/h;->d:Ljava/lang/String;

    return-object v0
.end method
