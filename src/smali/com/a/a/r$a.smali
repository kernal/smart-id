.class public final enum Lcom/a/a/r$a;
.super Ljava/lang/Enum;
.source "JWSObject.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/a/a/r;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/a/a/r$a;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/a/a/r$a;

.field public static final enum b:Lcom/a/a/r$a;

.field public static final enum c:Lcom/a/a/r$a;

.field private static final synthetic d:[Lcom/a/a/r$a;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 50
    new-instance v0, Lcom/a/a/r$a;

    const/4 v1, 0x0

    const-string v2, "UNSIGNED"

    invoke-direct {v0, v2, v1}, Lcom/a/a/r$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/a/a/r$a;->a:Lcom/a/a/r$a;

    .line 56
    new-instance v0, Lcom/a/a/r$a;

    const/4 v2, 0x1

    const-string v3, "SIGNED"

    invoke-direct {v0, v3, v2}, Lcom/a/a/r$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/a/a/r$a;->b:Lcom/a/a/r$a;

    .line 62
    new-instance v0, Lcom/a/a/r$a;

    const/4 v3, 0x2

    const-string v4, "VERIFIED"

    invoke-direct {v0, v4, v3}, Lcom/a/a/r$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/a/a/r$a;->c:Lcom/a/a/r$a;

    const/4 v0, 0x3

    .line 44
    new-array v0, v0, [Lcom/a/a/r$a;

    sget-object v4, Lcom/a/a/r$a;->a:Lcom/a/a/r$a;

    aput-object v4, v0, v1

    sget-object v1, Lcom/a/a/r$a;->b:Lcom/a/a/r$a;

    aput-object v1, v0, v2

    sget-object v1, Lcom/a/a/r$a;->c:Lcom/a/a/r$a;

    aput-object v1, v0, v3

    sput-object v0, Lcom/a/a/r$a;->d:[Lcom/a/a/r$a;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 44
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/a/a/r$a;
    .locals 1

    .line 44
    const-class v0, Lcom/a/a/r$a;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/a/a/r$a;

    return-object p0
.end method

.method public static values()[Lcom/a/a/r$a;
    .locals 1

    .line 44
    sget-object v0, Lcom/a/a/r$a;->d:[Lcom/a/a/r$a;

    invoke-virtual {v0}, [Lcom/a/a/r$a;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/a/a/r$a;

    return-object v0
.end method
