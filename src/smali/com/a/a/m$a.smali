.class public Lcom/a/a/m$a;
.super Ljava/lang/Object;
.source "JWEHeader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/a/a/m;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation


# instance fields
.field private final a:Lcom/a/a/i;

.field private final b:Lcom/a/a/d;

.field private c:Lcom/a/a/h;

.field private d:Ljava/lang/String;

.field private e:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private f:Ljava/net/URI;

.field private g:Lcom/a/a/c/d;

.field private h:Ljava/net/URI;

.field private i:Lcom/a/a/d/c;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field private j:Lcom/a/a/d/c;

.field private k:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/a/a/d/a;",
            ">;"
        }
    .end annotation
.end field

.field private l:Ljava/lang/String;

.field private m:Lcom/a/a/c/d;

.field private n:Lcom/a/a/c;

.field private o:Lcom/a/a/d/c;

.field private p:Lcom/a/a/d/c;

.field private q:Lcom/a/a/d/c;

.field private r:I

.field private s:Lcom/a/a/d/c;

.field private t:Lcom/a/a/d/c;

.field private u:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private v:Lcom/a/a/d/c;


# direct methods
.method public constructor <init>(Lcom/a/a/i;Lcom/a/a/d;)V
    .locals 2

    .line 280
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 282
    invoke-virtual {p1}, Lcom/a/a/i;->a()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/a/a/a;->a:Lcom/a/a/a;

    invoke-virtual {v1}, Lcom/a/a/a;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 286
    iput-object p1, p0, Lcom/a/a/m$a;->a:Lcom/a/a/i;

    if-eqz p2, :cond_0

    .line 292
    iput-object p2, p0, Lcom/a/a/m$a;->b:Lcom/a/a/d;

    return-void

    .line 289
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "The encryption method \"enc\" parameter must not be null"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 283
    :cond_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "The JWE algorithm \"alg\" cannot be \"none\""

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method


# virtual methods
.method public a(I)Lcom/a/a/m$a;
    .locals 1

    if-ltz p1, :cond_0

    .line 576
    iput p1, p0, Lcom/a/a/m$a;->r:I

    return-object p0

    .line 574
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "The PBES2 count parameter must not be negative"

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public a(Lcom/a/a/c/d;)Lcom/a/a/m$a;
    .locals 0

    .line 404
    iput-object p1, p0, Lcom/a/a/m$a;->g:Lcom/a/a/c/d;

    return-object p0
.end method

.method public a(Lcom/a/a/c;)Lcom/a/a/m$a;
    .locals 0

    .line 513
    iput-object p1, p0, Lcom/a/a/m$a;->n:Lcom/a/a/c;

    return-object p0
.end method

.method public a(Lcom/a/a/d/c;)Lcom/a/a/m$a;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 436
    iput-object p1, p0, Lcom/a/a/m$a;->i:Lcom/a/a/d/c;

    return-object p0
.end method

.method public a(Lcom/a/a/h;)Lcom/a/a/m$a;
    .locals 0

    .line 343
    iput-object p1, p0, Lcom/a/a/m$a;->c:Lcom/a/a/h;

    return-object p0
.end method

.method public a(Ljava/lang/String;)Lcom/a/a/m$a;
    .locals 0

    .line 358
    iput-object p1, p0, Lcom/a/a/m$a;->d:Ljava/lang/String;

    return-object p0
.end method

.method public a(Ljava/lang/String;Ljava/lang/Object;)Lcom/a/a/m$a;
    .locals 2

    .line 629
    invoke-static {}, Lcom/a/a/m;->f()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 633
    iget-object v0, p0, Lcom/a/a/m$a;->u:Ljava/util/Map;

    if-nez v0, :cond_0

    .line 634
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/a/a/m$a;->u:Ljava/util/Map;

    .line 637
    :cond_0
    iget-object v0, p0, Lcom/a/a/m$a;->u:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-object p0

    .line 630
    :cond_1
    new-instance p2, Ljava/lang/IllegalArgumentException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "The parameter name \""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "\" matches a registered name"

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p2
.end method

.method public a(Ljava/net/URI;)Lcom/a/a/m$a;
    .locals 0

    .line 389
    iput-object p1, p0, Lcom/a/a/m$a;->f:Ljava/net/URI;

    return-object p0
.end method

.method public a(Ljava/util/List;)Lcom/a/a/m$a;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/a/a/d/a;",
            ">;)",
            "Lcom/a/a/m$a;"
        }
    .end annotation

    .line 468
    iput-object p1, p0, Lcom/a/a/m$a;->k:Ljava/util/List;

    return-object p0
.end method

.method public a(Ljava/util/Set;)Lcom/a/a/m$a;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/a/a/m$a;"
        }
    .end annotation

    .line 374
    iput-object p1, p0, Lcom/a/a/m$a;->e:Ljava/util/Set;

    return-object p0
.end method

.method public a()Lcom/a/a/m;
    .locals 26

    move-object/from16 v0, p0

    .line 681
    new-instance v24, Lcom/a/a/m;

    move-object/from16 v1, v24

    iget-object v2, v0, Lcom/a/a/m$a;->a:Lcom/a/a/i;

    iget-object v3, v0, Lcom/a/a/m$a;->b:Lcom/a/a/d;

    iget-object v4, v0, Lcom/a/a/m$a;->c:Lcom/a/a/h;

    iget-object v5, v0, Lcom/a/a/m$a;->d:Ljava/lang/String;

    iget-object v6, v0, Lcom/a/a/m$a;->e:Ljava/util/Set;

    iget-object v7, v0, Lcom/a/a/m$a;->f:Ljava/net/URI;

    iget-object v8, v0, Lcom/a/a/m$a;->g:Lcom/a/a/c/d;

    iget-object v9, v0, Lcom/a/a/m$a;->h:Ljava/net/URI;

    iget-object v10, v0, Lcom/a/a/m$a;->i:Lcom/a/a/d/c;

    iget-object v11, v0, Lcom/a/a/m$a;->j:Lcom/a/a/d/c;

    iget-object v12, v0, Lcom/a/a/m$a;->k:Ljava/util/List;

    iget-object v13, v0, Lcom/a/a/m$a;->l:Ljava/lang/String;

    iget-object v14, v0, Lcom/a/a/m$a;->m:Lcom/a/a/c/d;

    iget-object v15, v0, Lcom/a/a/m$a;->n:Lcom/a/a/c;

    move-object/from16 v25, v1

    iget-object v1, v0, Lcom/a/a/m$a;->o:Lcom/a/a/d/c;

    move-object/from16 v16, v1

    iget-object v1, v0, Lcom/a/a/m$a;->p:Lcom/a/a/d/c;

    move-object/from16 v17, v1

    iget-object v1, v0, Lcom/a/a/m$a;->q:Lcom/a/a/d/c;

    move-object/from16 v18, v1

    iget v1, v0, Lcom/a/a/m$a;->r:I

    move/from16 v19, v1

    iget-object v1, v0, Lcom/a/a/m$a;->s:Lcom/a/a/d/c;

    move-object/from16 v20, v1

    iget-object v1, v0, Lcom/a/a/m$a;->t:Lcom/a/a/d/c;

    move-object/from16 v21, v1

    iget-object v1, v0, Lcom/a/a/m$a;->u:Ljava/util/Map;

    move-object/from16 v22, v1

    iget-object v1, v0, Lcom/a/a/m$a;->v:Lcom/a/a/d/c;

    move-object/from16 v23, v1

    move-object/from16 v1, v25

    invoke-direct/range {v1 .. v23}, Lcom/a/a/m;-><init>(Lcom/a/a/a;Lcom/a/a/d;Lcom/a/a/h;Ljava/lang/String;Ljava/util/Set;Ljava/net/URI;Lcom/a/a/c/d;Ljava/net/URI;Lcom/a/a/d/c;Lcom/a/a/d/c;Ljava/util/List;Ljava/lang/String;Lcom/a/a/c/d;Lcom/a/a/c;Lcom/a/a/d/c;Lcom/a/a/d/c;Lcom/a/a/d/c;ILcom/a/a/d/c;Lcom/a/a/d/c;Ljava/util/Map;Lcom/a/a/d/c;)V

    return-object v24
.end method

.method public b(Lcom/a/a/c/d;)Lcom/a/a/m$a;
    .locals 0

    .line 498
    iput-object p1, p0, Lcom/a/a/m$a;->m:Lcom/a/a/c/d;

    return-object p0
.end method

.method public b(Lcom/a/a/d/c;)Lcom/a/a/m$a;
    .locals 0

    .line 452
    iput-object p1, p0, Lcom/a/a/m$a;->j:Lcom/a/a/d/c;

    return-object p0
.end method

.method public b(Ljava/lang/String;)Lcom/a/a/m$a;
    .locals 0

    .line 483
    iput-object p1, p0, Lcom/a/a/m$a;->l:Ljava/lang/String;

    return-object p0
.end method

.method public b(Ljava/net/URI;)Lcom/a/a/m$a;
    .locals 0

    .line 419
    iput-object p1, p0, Lcom/a/a/m$a;->h:Ljava/net/URI;

    return-object p0
.end method

.method public c(Lcom/a/a/d/c;)Lcom/a/a/m$a;
    .locals 0

    .line 528
    iput-object p1, p0, Lcom/a/a/m$a;->o:Lcom/a/a/d/c;

    return-object p0
.end method

.method public d(Lcom/a/a/d/c;)Lcom/a/a/m$a;
    .locals 0

    .line 543
    iput-object p1, p0, Lcom/a/a/m$a;->p:Lcom/a/a/d/c;

    return-object p0
.end method

.method public e(Lcom/a/a/d/c;)Lcom/a/a/m$a;
    .locals 0

    .line 558
    iput-object p1, p0, Lcom/a/a/m$a;->q:Lcom/a/a/d/c;

    return-object p0
.end method

.method public f(Lcom/a/a/d/c;)Lcom/a/a/m$a;
    .locals 0

    .line 591
    iput-object p1, p0, Lcom/a/a/m$a;->s:Lcom/a/a/d/c;

    return-object p0
.end method

.method public g(Lcom/a/a/d/c;)Lcom/a/a/m$a;
    .locals 0

    .line 606
    iput-object p1, p0, Lcom/a/a/m$a;->t:Lcom/a/a/d/c;

    return-object p0
.end method

.method public h(Lcom/a/a/d/c;)Lcom/a/a/m$a;
    .locals 0

    .line 669
    iput-object p1, p0, Lcom/a/a/m$a;->v:Lcom/a/a/d/c;

    return-object p0
.end method
