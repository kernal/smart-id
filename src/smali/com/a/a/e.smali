.class public abstract Lcom/a/a/e;
.super Ljava/lang/Object;
.source "Header.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final f:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:Lcom/a/a/a;

.field private final b:Lcom/a/a/h;

.field private final c:Ljava/lang/String;

.field private final d:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private final g:Lcom/a/a/d/c;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 80
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 81
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    sput-object v0, Lcom/a/a/e;->f:Ljava/util/Map;

    return-void
.end method

.method protected constructor <init>(Lcom/a/a/a;Lcom/a/a/h;Ljava/lang/String;Ljava/util/Set;Ljava/util/Map;Lcom/a/a/d/c;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/a/a/a;",
            "Lcom/a/a/h;",
            "Ljava/lang/String;",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;",
            "Lcom/a/a/d/c;",
            ")V"
        }
    .end annotation

    .line 112
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    if-eqz p1, :cond_2

    .line 118
    iput-object p1, p0, Lcom/a/a/e;->a:Lcom/a/a/a;

    .line 120
    iput-object p2, p0, Lcom/a/a/e;->b:Lcom/a/a/h;

    .line 121
    iput-object p3, p0, Lcom/a/a/e;->c:Ljava/lang/String;

    if-eqz p4, :cond_0

    .line 125
    new-instance p1, Ljava/util/HashSet;

    invoke-direct {p1, p4}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    invoke-static {p1}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object p1

    iput-object p1, p0, Lcom/a/a/e;->d:Ljava/util/Set;

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    .line 127
    iput-object p1, p0, Lcom/a/a/e;->d:Ljava/util/Set;

    :goto_0
    if-eqz p5, :cond_1

    .line 132
    new-instance p1, Ljava/util/HashMap;

    invoke-direct {p1, p5}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    invoke-static {p1}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object p1

    iput-object p1, p0, Lcom/a/a/e;->e:Ljava/util/Map;

    goto :goto_1

    .line 134
    :cond_1
    sget-object p1, Lcom/a/a/e;->f:Ljava/util/Map;

    iput-object p1, p0, Lcom/a/a/e;->e:Ljava/util/Map;

    .line 137
    :goto_1
    iput-object p6, p0, Lcom/a/a/e;->g:Lcom/a/a/d/c;

    return-void

    .line 115
    :cond_2
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "The algorithm \"alg\" header parameter must not be null"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public static a(La/b/b/d;)Lcom/a/a/a;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/text/ParseException;
        }
    .end annotation

    const-string v0, "alg"

    .line 359
    invoke-static {p0, v0}, Lcom/a/a/d/i;->b(La/b/b/d;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 363
    sget-object v1, Lcom/a/a/a;->a:Lcom/a/a/a;

    invoke-virtual {v1}, Lcom/a/a/a;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 365
    sget-object p0, Lcom/a/a/a;->a:Lcom/a/a/a;

    return-object p0

    :cond_0
    const-string v1, "enc"

    .line 366
    invoke-virtual {p0, v1}, La/b/b/d;->containsKey(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_1

    .line 368
    invoke-static {v0}, Lcom/a/a/i;->a(Ljava/lang/String;)Lcom/a/a/i;

    move-result-object p0

    return-object p0

    .line 371
    :cond_1
    invoke-static {v0}, Lcom/a/a/p;->a(Ljava/lang/String;)Lcom/a/a/p;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public a(Ljava/lang/String;)Ljava/lang/Object;
    .locals 1

    .line 213
    iget-object v0, p0, Lcom/a/a/e;->e:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public b()La/b/b/d;
    .locals 3

    .line 281
    new-instance v0, La/b/b/d;

    iget-object v1, p0, Lcom/a/a/e;->e:Ljava/util/Map;

    invoke-direct {v0, v1}, La/b/b/d;-><init>(Ljava/util/Map;)V

    .line 284
    iget-object v1, p0, Lcom/a/a/e;->a:Lcom/a/a/a;

    invoke-virtual {v1}, Lcom/a/a/a;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "alg"

    invoke-virtual {v0, v2, v1}, La/b/b/d;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 286
    iget-object v1, p0, Lcom/a/a/e;->b:Lcom/a/a/h;

    if-eqz v1, :cond_0

    .line 287
    invoke-virtual {v1}, Lcom/a/a/h;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "typ"

    invoke-virtual {v0, v2, v1}, La/b/b/d;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 290
    :cond_0
    iget-object v1, p0, Lcom/a/a/e;->c:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v2, "cty"

    .line 291
    invoke-virtual {v0, v2, v1}, La/b/b/d;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 294
    :cond_1
    iget-object v1, p0, Lcom/a/a/e;->d:Ljava/util/Set;

    if-eqz v1, :cond_2

    invoke-interface {v1}, Ljava/util/Set;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    .line 295
    new-instance v1, Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/a/a/e;->d:Ljava/util/Set;

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    const-string v2, "crit"

    invoke-virtual {v0, v2, v1}, La/b/b/d;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_2
    return-object v0
.end method

.method public c()Lcom/a/a/a;
    .locals 1

    .line 165
    iget-object v0, p0, Lcom/a/a/e;->a:Lcom/a/a/a;

    return-object v0
.end method

.method public d()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 199
    iget-object v0, p0, Lcom/a/a/e;->d:Ljava/util/Set;

    return-object v0
.end method

.method public e()Lcom/a/a/d/c;
    .locals 1

    .line 326
    iget-object v0, p0, Lcom/a/a/e;->g:Lcom/a/a/d/c;

    if-nez v0, :cond_0

    .line 329
    invoke-virtual {p0}, Lcom/a/a/e;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/a/a/d/c;->a(Ljava/lang/String;)Lcom/a/a/d/c;

    move-result-object v0

    :cond_0
    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .line 311
    invoke-virtual {p0}, Lcom/a/a/e;->b()La/b/b/d;

    move-result-object v0

    invoke-virtual {v0}, La/b/b/d;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
